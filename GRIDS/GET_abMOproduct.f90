subroutine GET_abMOproduct_GRIDs (Xpt,Ypt,Zpt,aMO,bMO,abMOproduct_grids)
!*******************************************************************************
!     Date last modified:                                                      *
!     Authors:                                                                 *
!     Description:                                                             *
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule

      implicit none
!
      integer :: aMO,bMO
      double precision :: Xpt,Ypt,Zpt,abMOproduct_grids

! Local scalars:
      integer :: NPab,IJ,IPMab,Mu,Nu
      double precision :: S_ab,PV_ba,K_ab
!
! Local array:
      double precision, dimension(:), allocatable :: AOprod
      type PM_xx
        integer :: IJ
        double precision :: Pxx
      end type
      type (PM_xx), dimension(:), allocatable :: PM_exch_ab
!
! Begin:

      if(aMO.eq.bMO)return
!
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)
!
      allocate(PM_exch_ab(MATlen))
      allocate(AOprod(1:MATlen))

      call Gaussian_products (Xpt, Ypt, Zpt, AOprod, MATlen)

      call BLD_PM_exch (CMO%coeff, Nbasis)
      
      S_ab=ZERO
      do IPMab=1,NPab
        IJ=PM_exch_ab(IPMab)%IJ
        S_ab=S_ab+PM_exch_ab(IPMab)%Pxx*AOprod(IJ)
      end do
      
      abMOproduct_grids=dabs(S_ab)

      deallocate (AOprod)

      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine BLD_PM_exch (CMO, Nbasis)
!*******************************************************************************
!     Date last modified: March 15, 2011                                       *
!     Authors: R.A. Poirier                                                    *
!     Description: Build P_ab and P_ba                                         *
!*******************************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer :: Nbasis
      double precision :: CMO(Nbasis,Nbasis)
!
! Local scalars:
      double precision :: Temp_ab
      double precision :: threshold
      parameter (threshold=1.0D-06)
!
! Local function:
!
! Begin:
      call PRG_manager ('enter','BLD_PM_exch', 'UTILITY')
!
      NPab=ZERO
      do Mu=1,Nbasis
      do Nu=1,Mu
        if(Mu.ne.Nu)then
          Temp_ab=CMO(Mu,aMO)*CMO(Nu,bMO)+CMO(Nu,aMO)*CMO(Mu,bMO)
        else
          Temp_ab=CMO(Mu,aMO)*CMO(Nu,bMO)
        end if
        if(dabs(Temp_ab).gt.threshold)then
          NPab=NPab+1
          PM_exch_ab(NPab)%Pxx=Temp_ab
          PM_exch_ab(NPab)%IJ=Mu*(Mu-1)/2+Nu
        end if
      end do ! Nu
      end do ! Mu
!
! End of routine BLD_PM_exch
      call PRG_manager ('exit', 'BLD_PM_exch', 'UTILITY')
      return
      end subroutine BLD_PM_exch
! END CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine GET_abMOproduct_GRIDs      
