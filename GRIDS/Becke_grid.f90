      subroutine BLD_Becke_GRID
!**************************************************************************************
!     Date last modified: 6th June ,2005                                              *
!     Author: Aisha                                                                   *
!     Description:                                                                    *
!**************************************************************************************

! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE type_elements
      USE type_radial_grids
      USE NI_defaults
!     USE module_grid_points
      USE N_integration

      implicit none
! Local scalars:
      integer :: Iatom,Ifound,MAXpts,Z_num
      double precision :: EXI

! Local parameters:

! Begin:
      call PRG_manager ('enter', 'BLD_Becke_GRID', 'UTILITY')

      RADIAL_grid='BECKE'

      call BLD_BeckeW_XI
      call BLD_grid_location ! Defines Nfound and LZfound

      num_regions = 1
      allocate(Nap_grid(Nfound,num_regions), NRpoints_region(Nfound,num_regions))
      allocate(Radial_points(Nfound,45),Radial_weights(Nfound,45),NApoints_atom(Nfound))
      Ifound = 0

      MAXpts=45*194
      allocate(Egridpts(Nfound,MAXpts))

      do Z_num=1,Nelements
        if(.not.LZfound(Z_num))cycle
        Ifound=GRID_loc(Z_num)
        if(Z_num.eq.1)then  ! H atom
          EXI=0.35*Angstrom_to_Bohr
          NRpoints=20
          NRpoints_region(Ifound,num_regions)=NRpoints
          Nap_grid(Ifound,num_regions)=110
          NApoints_atom(Ifound)=110*NRpoints
          NApts_atom=NApoints_atom(Ifound)
          allocate(grid_points(1:NApts_atom))
          call Radial_weight_point

          Rpoints(1:NRpoints)=Radial_points(Ifound,1:NRpoints)
          allocate(Apoints_temp(NApts_atom))
          call Angular_grid110 (Rpoints, NRpoints, NApts_atom, Apoints_temp)
          grid_points(1:NApts_atom)=Apoints_temp(1:NApts_atom)

          deallocate(Apoints_temp)

        else if(Z_num.GT.1)then
          if(Z_num.LT.11)then ! from He to Ne
            NRpoints = 35
            NRpoints_region(Ifound,num_regions) = NRpoints
          else if(Z_num.GT.10.and.Z_num.LT.19)then  ! from Na to Ar              
            NRpoints = 40
            NRpoints_region(Ifound,num_regions) = NRpoints
          else if (Z_num.GT.18.and.Z_num.LT.36)then  ! from Na to Kr
            NRpoints = 45    
            NRpoints_region(Ifound,num_regions) = NRpoints
          else
            write(UNIout,'(a)')'ERROR> Becke grid not available past Kr'
            stop 'ERROR> Becke grid not available past Kr'
          end if

          EXI = 0.50D0 *BS_RADII(Z_num)*Angstrom_to_Bohr

          Nap_grid(Ifound,num_regions) = 194
          NApoints_atom(Ifound) = 194*NRpoints
          NApts_atom=NApoints_atom(Ifound)
          allocate(grid_points(1:NApts_atom))
          call Radial_weight_point
          Rpoints(1:NRpoints) = Radial_points(Ifound,1:NRpoints)
          allocate(Apoints_temp(NApts_atom))
          call Angular_grid194(Rpoints,NRpoints,NApts_atom,Apoints_temp)
          grid_points(1:NApts_atom)= Apoints_temp(1:NApts_atom)
          deallocate(Apoints_temp)
      end if ! Z_num

      call RAweights (Ifound)
      Egridpts(Ifound,1:NApts_atom)= grid_points(1:NApts_atom)

      deallocate(grid_points)

      end do ! Z_num 

      deallocate(Radial_points, Radial_weights)       
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine Radial_weight_point
!******************************************************************
!   Date last modified: Oct 11th, 2001                            *
!   Author: Aisha                                                 *
!   Description:                                                  *
!******************************************************************
! Modules:
      USE program_constants
      USE type_elements
      USE type_molecule
      USE N_integration

       implicit none

!
! Local arrays:
      double precision,dimension(NRpoints) :: X
!
! Local scalars:
      Integer  :: IRpoint
      double precision :: temp,dr,R_weight,factor2,factor3

!    !Becke- mapping/Jacobian, as in Gill paper!
      do IRpoint=1,NRpoints
      X(IRpoint)=cos(dble(IRpoint)*PI_val/dble(NRpoints+1))
      Radial_points(Ifound,IRpoint)=EXI*(1.0D0+X(IRpoint))/(1.0D0-X(IRpoint))
      factor2=(2.0D0*PI_val)*((1.0D0 + X(IRpoint))**2.5)*EXI**3
      factor3=dble(NRpoints+1)*(1.0D0 - X(IRpoint))**3.5
      Radial_weights(Ifound,IRpoint)=factor2/factor3
      end do
      end subroutine Radial_weight_point
! END CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine BLD_Becke_GRID
