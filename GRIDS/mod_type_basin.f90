      MODULE module_type_basin
!**************************************************************************************************
!     Date last modified: July 22, 2014                                              Version 2.0  *
!     Author: Ahmad and R.A. Poirier                                                              *
!     Description: Contains grid information                                                      * 
!**************************************************************************************************
!
      implicit none
!
      type :: type_basin
        double precision    :: X
        double precision    :: Y
        double precision    :: Z
        logical             :: assigned
        integer             :: atom_basin 
     end type type_basin
 
      type :: type_crit_pt
        double precision    :: X
        double precision    :: Y
        double precision    :: Z
        integer             :: atom_basin 
     end type type_crit_pt
 
      type(type_basin), dimension(:), allocatable :: basin
      type(type_crit_pt), dimension(:), allocatable :: crit_pt

      end MODULE module_type_basin     
