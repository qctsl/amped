      subroutine MENU_RADIAL
!***********************************************************************
!     Date last modified: January 21, 1997                 Version 1.0 *
!     Author: J.D. Xidos, R.A. Poirier                                 *
!     Description: Set up One Electron Properties                      *
!***********************************************************************
! Modules:
      USE program_parser
      USE program_constants
      USE MENU_gets
      USE QM_defaults
      USE NI_defaults
      USE module_grid_points

      implicit none
!
! Local scalars:
      integer :: length
      logical :: done
      character(len=MAX_string) :: string
!
! Begin:
      call PRG_manager ('enter', 'MENU_RADIAL', 'UTILITY')
      done=.false.
!
! Menu:
      do while (.not.done)
      call new_token (' RADIAL:')
!
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      ' Command RADIAL:', &
      '   Purpose: Calculate One-Electron Properties', &
      '   Syntax :', &
      '   GIll = <command> set values for Gill MultiExp', &
      '   GRid = <string>, Grid used for numerical integration (Pople, Becke, Gill...)', &
      '   end'
!
! GIll
      else if(token(1:2).EQ.'GI')then
        RADIAL_grid = 'GILL'
        call MENU_GILL

! GRid
      else if (token(1:2).eq. 'GR') then
         call GET_value (RADIAL_grid, length)
         if(index(RADIAL_grid,'PO').ne.0)then
           RADIAL_grid='SG1'
         else if(index(RADIAL_grid,'NT').ne.0)then
           RADIAL_grid='NTA'
         else if(index(RADIAL_grid,'TA').ne.0)then
           RADIAL_grid='TA'
         else if(index(RADIAL_grid,'BEC').ne.0)then
           RADIAL_grid='BECKE'
         else if(index(RADIAL_grid,'SG0').ne.0)then
           RADIAL_grid='SG0'
         else if(index(RADIAL_grid,'SG1').ne.0)then
           RADIAL_grid='SG1'
         else if(index(RADIAL_grid,'SG2').ne.0)then
           RADIAL_grid='SG2'
         else if(index(RADIAL_grid,'BI').ne.0)then
           RADIAL_grid='BIG'
         else if(index(RADIAL_grid,'BEN').ne.0)then ! Benchmark
           RADIAL_grid='BIG'
         else if(index(RADIAL_grid,'GI').ne.0)then ! Benchmark
           RADIAL_grid='GILL'
         else if(index(RADIAL_grid,'IA').ne.0)then ! Benchmark
           RADIAL_grid='IAWAD'
         else
           write(UNIout,'(2a)')'Integration method is not available: ',RADIAL_grid(1:16)
           stop 'Integration method is not available'
         end if

      else
         call MENU_end (done)
      end if
!
      end do !(.not.done)
!
! End of routine MENU_RADIAL
      call PRG_manager ('exit', 'MENU_RADIAL', 'UTILITY')
      return
      end
      subroutine MENU_GILL
!***********************************************************************
!     Date last modified: March 29, 2004                   Version 1.0 *
!     Author: R.A. Poirier and A. El-Sherbiny                          *
!     Description: Set up One Electron Properties                      *
!***********************************************************************
! Modules:
      USE NI_defaults
      USE program_parser
      USE MENU_gets
      USE program_constants

      implicit none

! Local scalars:
      integer :: nlist
      logical done

! Begin:
      call PRG_manager ('enter', 'MENU_GILL', 'UTILITY')
      done=.false.

! Menu:
      do while (.not.done)

      call new_token (' GILL:')
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      ' Command Gill:', &
      '   Purpose: Modified defaults for Gill MultiExp', &
      '   Syntax :', &
      '   RPoints or NRpoints= <integer>, Number of radial points (20, 25 or 30)', &
      '   APoints or NApoints= <integer list> Numner of angular points for each region', &
      '   end'

! RPoints or NRpoints (Number of radial points)
      else if(token(1:2).EQ.'RP'.or.token(1:2).EQ.'NR')then
         call GET_value (NRPoints_Gill)

! APoints or NApoints (Number of angular points)
      else if (token(1:2).eq. 'AP'.or.token(1:2).EQ.'NA')then
        call GET_Ilist (NApoints_Gill, nlist)
        write(UNIout,'(a,i4,a,i4,a,i4,a)')'Using the angular grid: (',&
          NApoints_Gill(1),',',NApoints_Gill(2),',',NApoints_Gill(3),' )'
      else
         call MENU_end (done)
      end if

      end do !(.not.done)

! End of routine MENU_GILL
      call PRG_manager ('exit', 'MENU_GILL', 'UTILITY')
      return
      end
