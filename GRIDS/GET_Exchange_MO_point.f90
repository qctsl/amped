       subroutine GET_Exchange_MO_point (Xpt, Ypt, Zpt, K_grids, KHF_grids)
!*******************************************************************************
!     Date last modified: May 24 2017                                          *
!     Authors: R.A. Poirier and I. Awad                                        *
!     Description: Compute the Exchange at a grid point                        *
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE GSCF_work

      implicit none
!
      double precision :: K_grids,KHF_grids,Xpt,Ypt,Zpt

! Local scalars:
      integer :: aMO,bMO,NPab,NPba,JJ,Lopen,Lopen1,Kopen
      double precision :: Kab
      double precision, dimension(:,:), allocatable :: occ_Kab, Occ_KHFab
           
! Begin:
!
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)
      
      allocate (occ_Kab(1:CMO%NoccMO,1:CMO%NoccMO))
      allocate (occ_KHFab(1:CMO%NoccMO,1:CMO%NoccMO))
      
      call occupancy_Kab (occ_Kab, Occ_KHFab, CMO%NoccMO)
!    
      K_grids=ZERO
      KHF_grids=ZERO        
      do aMO=1,CMO%NoccMO
      do bMO=1,aMO
        call GET_Kab_MO_point (Xpt, Ypt, Zpt, aMO, bMO, Kab)
        K_grids=K_grids+occ_Kab(aMO,bMO)*Kab
        KHF_grids=KHF_grids+occ_KHFab(aMO,bMO)*Kab
      end do ! bMO
      end do ! aMO
      
      deallocate (occ_Kab)
      deallocate (occ_KHFab)
          
      return
      end subroutine GET_Exchange_MO_point 
      subroutine GET_Exchange_MO_pointold (Xpt, Ypt, Zpt, K_grids, KHF_grids)
!*******************************************************************************
!     Date last modified: May 24 2017                                          *
!     Authors: R.A. Poirier and I. Awad                                        *
!     Description: Compute the Exchange at a grid point                        *
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE GSCF_work

      implicit none
!
      double precision :: K_grids,KHF_grids,Xpt,Ypt,Zpt

! Local scalars:
      integer :: aMO,bMO,NPab,NPba,JJ,Lopen,Lopen1,Kopen
      double precision :: Kab
!
! Begin:
!
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)
!    
      if (wavefunction.eq.'RHF') then 

      K_grids=ZERO
      KHF_grids=ZERO        
      do aMO=1,CMO%NoccMO
      do bMO=1,aMO
        call GET_Kab_MO_point (Xpt, Ypt, Zpt, aMO, bMO, Kab)
        if(aMO.eq.bMO)then
          KHF_grids=KHF_grids+Kab
        else
          K_grids=K_grids+TWO*Kab
          KHF_grids=KHF_grids+TWO*Kab
        end if
      end do ! bMO
     end do ! aMO
     
     end if ! RHF

     if (wavefunction.eq.'ROHF') then 

! Calculate the ROHF energy: 
! Add the closed-shell contribution

      K_grids=ZERO
      KHF_grids=ZERO
      do aMO=1,CMO_ROHF%NFcoreMO
        do bMO=1,aMO
          call GET_Kab_MO_point (Xpt, Ypt, Zpt, aMO, bMO, Kab)
          if(aMO.eq.bMO)then
            KHF_grids=KHF_grids+Kab
          else
            K_grids=K_grids+TWO*Kab
            KHF_grids=KHF_grids+TWO*Kab
          end if
        end do ! bMO
      end do ! aMO
      
    
! Add the closed-shell - open-shell contributions,
      if(Nopen.gt.0)then
        do aMO=1,CMO_ROHF%NFcoreMO
          do Kopen=1,Nopen
            bMO=Kopen+CMO_ROHF%NFcoreMO  ! note: aMO .ne. bMO 
            call GET_Kab_MO_point (Xpt, Ypt, Zpt, aMO, bMO, Kab)
            JJ=Kopen*(Kopen+1)/2+1
            K_grids=K_grids+TWO*BETACC(JJ)*Kab
            KHF_grids=KHF_grids+TWO*BETACC(JJ)*Kab
          end do ! Kopen
        end do
      end if ! 
      
! Add the open-shell - open-shell contributions,
      if(Nopen.gt.0)then
        do Kopen=1,Nopen
          aMO=Kopen+CMO_ROHF%NFcoreMO
          do Lopen=1,Nopen
            bMO=Lopen+CMO_ROHF%NFcoreMO
            call GET_Kab_MO_point (Xpt, Ypt, Zpt, aMO, bMO, Kab)  
            if(Lopen.lt.Kopen)then
             JJ=Kopen*(Kopen+1)/2+1+Lopen
             K_grids=K_grids+BETACC(JJ)*Kab
             KHF_grids=KHF_grids+BETACC(JJ)*Kab
            elseif(Lopen.gt.Kopen)then
             JJ=Lopen*(Lopen+1)/2+1+Kopen
             K_grids=K_grids+BETACC(JJ)*Kab
             KHF_grids=KHF_grids+BETACC(JJ)*Kab
            else                                   ! Lopen.eq.Kopen
             JJ=Kopen*(Kopen+1)/2+1+Lopen
             KHF_grids=KHF_grids+BETACC(JJ)*Kab
            end if
          end do ! K
        end do ! I
      end if ! 
      end if

      return
      end subroutine GET_Exchange_MO_pointold
