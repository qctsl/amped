      module iawad_grid_parameters
!*****************************************************************************************************************
!     Date last modified: April,17 2004                                                            Version 2.0  *
!     Author:  Aisha                                                                                      *
!     Description: Define the parameters used to define the roots and weights for the Gill/Chien grids.          *
!                   The values of "x" correspond to -lnx in the Gill/Chien.
!                   The values of "a" correspond to a/x in the Gill/Chien.
!*****************************************************************************************************************
! Modules:

      USE program_files

      implicit none
!
! Scalars: 
      double precision, dimension(:), allocatable :: R_value_IAWAD
      double precision, dimension(:), allocatable :: a_values_IAWAD
      double precision, dimension(:), allocatable :: X_points_IAWAD

      double precision, parameter, dimension(20) :: X_points_20_IAWAD = &
(/0.029703983063936533, &
0.09864400257018408, &
0.20449544857301558, &
0.34431235102017693, &
0.5146810906002901, &
0.7120485731203674, &
0.9329732638428723, &
1.1742776546702969, &
1.4331100703849127, &
1.7069332430267574, &
1.9934538593271578, &
2.2904961000123034, &
2.5958045409270976, &
2.9067346670257397, &
3.2197443606482308, &
3.529524440437859, &
3.82750033108418, &
4.099409577694636, &
4.322303845906152, &
4.46429779897659/)                     


      double precision, parameter, dimension(20) :: a_values_20_IAWAD = &
(/0.049686117822674826, &
0.0878298796949264, &
0.12337631524551336, &
0.15568286123252043, &
0.18445825429830418, &
0.20970055149120137, &
0.23161834858392083, &
0.25051523607818293, &
0.2667281506579971, &
0.2805376050864326, &
0.29214502524656255, &
0.30156682587471934, &
0.3086210499043861, &
0.3126542883277498, &
0.3125249311087074, &
0.3055946609280241, &
0.28816051743025467, &
0.2516190683099532, &
0.1889162920652105, &
0.08991394105102875/) 

CONTAINS !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                      
      subroutine R_values_IAWAD_J (Nmax,NRpoints)
!**************************************************************************************
!     Date last modified: October 16, 2007                                            *
!     Author: R.A. Poirier                                                            *
!     Description: R_values used for MultiExp grids                                   *
!**************************************************************************************
! Modules:

      integer :: Nmax,NRpoints
!
! R_values optimized w.r.t Coulomb:
      write(UNIout,'(a)')'Using R values optimized for Coulomb'
      select case (NRpoints)
      case(20)
        a_values_IAWAD(1:NRpoints)=a_values_20_IAWAD(1:NRpoints)
        X_points_IAWAD(1:NRpoints)=X_points_20_IAWAD(1:NRpoints)
        R_value_IAWAD(1) = 1.0000
        R_value_IAWAD(2) = 0.5882
        R_value_IAWAD(3) = 1.7000
        R_value_IAWAD(4) = 2.0513
        R_value_IAWAD(5) = 1.5385
        R_value_IAWAD(6) = 1.2330
        R_value_IAWAD(7) = 1.0420
        R_value_IAWAD(8) = 0.7560
        R_value_IAWAD(9) = 0.6940
        R_value_IAWAD(10) = 0.6838
        R_value_IAWAD(11) = 2.0080
        R_value_IAWAD(12) = 1.8510
        R_value_IAWAD(13) = 2.5714
        R_value_IAWAD(14) = 1.5790
        R_value_IAWAD(15) = 1.4450
        R_value_IAWAD(16) = 1.3540
        R_value_IAWAD(17) = 1.2650
        R_value_IAWAD(18) = 1.3333
        R_value_IAWAD(19) = 0.0D0
        R_value_IAWAD(20) = 0.0D0
        R_value_IAWAD(21) = 0.0D0
        R_value_IAWAD(22) = 0.0D0
        R_value_IAWAD(23) = 0.0D0
        R_value_IAWAD(24) = 0.0D0
        R_value_IAWAD(25) = 0.0D0
        R_value_IAWAD(26) = 0.0D0
        R_value_IAWAD(27) = 0.0D0
        R_value_IAWAD(28) = 0.0D0
        R_value_IAWAD(29) = 0.7070D0  !Cu
        R_value_IAWAD(30) = 0.7010D0  !Zn
        R_value_IAWAD(32) = 0.7390D0  !Ge
        R_value_IAWAD(33) = 0.7660D0  !As
        R_value_IAWAD(34) = 0.7500D0  !Se
        R_value_IAWAD(35) = 0.7400D0  !Br
        R_value_IAWAD(51) = 0.9920D0  !Sb
        R_value_IAWAD(52) = 0.9920D0  !Te
        R_value_IAWAD(53) = 0.800D0   !I
     
      end select !NRpoints

      return
      end subroutine R_values_IAWAD_J
      subroutine R_values_IAWAD_N (Nmax, NRpoints)
!**************************************************************************************
!     Date last modified: October 16, 2007                                            *
!     Author: R.A. Poirier                                                            *
!     Description: R_values used for MultiExp grids                                   *
!**************************************************************************************
! Modules:

      integer :: Nmax,NRpoints
!
! R_values optimized w.r.t Number of electrons
      write(UNIout,'(a)')'Using R values optimized for number of electrons'
      select case (NRpoints)
      case(20)
        a_values_IAWAD(1:NRpoints)=a_values_20_IAWAD(1:NRpoints)
        X_points_IAWAD(1:NRpoints)=X_points_20_IAWAD(1:NRpoints)
        R_value_IAWAD(1) = 1.000
        R_value_IAWAD(2) = 0.5882
        R_value_IAWAD(3) = 3.0769
        R_value_IAWAD(4) = 2.0513
        R_value_IAWAD(5) = 1.5385
        R_value_IAWAD(6) = 1.2308
        R_value_IAWAD(7) = 1.0256
        R_value_IAWAD(8) = 0.8791
        R_value_IAWAD(9) = 0.7692
        R_value_IAWAD(10) = 0.6838
        R_value_IAWAD(11) = 4.0909/2.0
        R_value_IAWAD(12) = 3.1579
        R_value_IAWAD(13) = 2.5714
        R_value_IAWAD(14) = 1.5877
        R_value_IAWAD(15) = 1.4500
        R_value_IAWAD(16) = 1.3600
        R_value_IAWAD(17) = 1.2730
        R_value_IAWAD(18) = 1.3333
        R_value_IAWAD(19) = 0.0D0
        R_value_IAWAD(20) = 0.0D0
        R_value_IAWAD(21) = 0.0D0
        R_value_IAWAD(22) = 0.0D0
        R_value_IAWAD(23) = 0.0D0
        R_value_IAWAD(24) = 0.0D0
        R_value_IAWAD(25) = 0.0D0
        R_value_IAWAD(26) = 0.0D0
        R_value_IAWAD(27) = 0.0D0
        R_value_IAWAD(28) = 0.0D0
        R_value_IAWAD(29) = 0.7070D0  !Cu
        R_value_IAWAD(30) = 0.7010D0  !Zn
        R_value_IAWAD(32) = 0.7390D0  !Ge
        R_value_IAWAD(33) = 0.7660D0  !As
        R_value_IAWAD(34) = 0.7500D0  !Se
        R_value_IAWAD(35) = 0.7400D0  !Br
        R_value_IAWAD(51) = 0.9920D0  !Sb
        R_value_IAWAD(52) = 0.9920D0  !Te
        R_value_IAWAD(53) = 0.800D0   !I

      end select !NRpoints

      return
      end subroutine R_values_IAWAD_N
      end module iawad_grid_parameters
