      subroutine GRID_PRINT_manager (ObjName, Modality)
!********************************************************************************
!     Date last modified: April 24, 2008                           Version 2.0 *
!     Author: R.A. Poirier                                                      *
!     Description: Calls the appropriate routine to "Command" the object nameIN *
!     Command can be "PRINT" or "KILL"                                          *
!     Pwhat can be "OBJECT", "DOCUMENTATION" or "TABLE"                         *
!********************************************************************************
! Modules:
      USE program_files
      USE program_objects

      implicit none
!
! Input scalars:
      character*(*), intent(IN) :: ObjName,Modality
!
! Local scalars:
!
! Begin:
      call PRG_manager ('enter', 'GRID_PRINT_manager', 'UTILITY')

      SelectObject:   select case (ObjName)
      case ('DENSITY')
      case ('DEL_DENSITY')
      case ('DEL2_DENSITY')
      case ('RADIAL_DENSITY')
      case ('DEL_RADIAL_DENSITY')
      case ('DEL2_RADIAL_DENSITY')
      case ('LYP')
      case ('KINETIC')
      case ('VNE')
      case ('COULOMB')
      case ('COULOMB_MO')
      case ('EXCHANGE')
      case ('VELOCITY')
      case ('VELOCITIES')
      case ('ELECTROSTATICPOTL')
      case ('KAB')
      case ('JAB')
      case ('JBA')
      case ('JAB_AVER')
      case ('MBD')
      case ('EAMD')
      case ('RAMD')
      case ('RADIAL')
      case default
        write(UNIout,*)'GRID_PRINT_manager>'
        write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                       '" for Modality "',Modality(1:len_trim(Modality)),'"'
      end select SelectObject
!
! end of function GRID_PRINT_manager
      call PRG_manager ('exit', 'GRID_PRINT_manager', 'UTILITY')
      return
      end subroutine GRID_PRINT_manager
