      subroutine BLD_IAWAD_GRID
!**************************************************************************************
!     Date last modified:  Oct. 25th, 05                                              *
!     Author: Aisha                                                                   *
!     Description: Optimization of R_values used for MultiExp grids                   *
!**************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE type_elements
      USE type_radial_grids
      USE module_grid_points
      USE NI_defaults
      USE iawad_grid_parameters
      USE N_integration

      implicit none

! Local scalars:
      integer :: Iatom,Ifound,Z_num,NR1,NR2,NA1,NA2,KRpoint
      integer :: NRpoint1,NRpoint2,NRpoint3 ! Number of radial points per region  
      integer :: NApoint1,NApoint2,NApoint3 ! Number of angular points per region  
      integer :: MAXpts
      logical :: LSkip ! true if second entry
      integer :: NAPoints_IAWAD(3)
      double precision :: factorR

! Local parameters:

! Begin:
      call PRG_manager ('enter', 'BLD_IAWAD_GRID', 'UTILITY')
      LSkip=.true.

      RADIAL_grid='IAWAD'

 !     call BLD_BeckeW_XI
      call BLD_grid_location ! Defines Nfound and LZfound

      NRpoints = 20
      NApoints_IAWAD(1:3)=NApoints_Gill(1:3)
      if(.not.allocated(R_value_IAWAD))then
        LSkip=.false. ! Must be first entry
        allocate (a_values_IAWAD(NRpoints), X_points_IAWAD(NRpoints), R_value_IAWAD(Nelements))
      select case (NRpoints)
      case(20)
        call R_values_IAWAD_N (Nelements, NRpoints)
        NRpoint1 = 6
        NRpoint2 = 6
        NRpoint3 = 8
      end select !NRpoints

      end if ! .not.allocated(R_value)

      if(MUN_prtlev.gt.0.or..not.LSkip)then
        write(UNIout,'(a,i3,a,3i4)')'The ',NRpoints,' radial points are divided into ',NRpoint1,NRpoint2,NRpoint3
        write(UNIout,'(a,3i4,a)')'with angular points ',NApoints_IAWAD(1),NApoints_IAWAD(2),NApoints_IAWAD(3),' respectively.'
        write(UNIout,'(a)')'The R_values are optimized for Li, Si, P, S, Cl'
      end if
 
      num_regions = 3
      if(.not.allocated(Nap_grid))then
        allocate(Nap_grid(Nfound,num_regions), NRpoints_region(Nfound,num_regions))
        allocate(Radial_points(Nfound,NRpoints),Radial_weights(Nfound,NRpoints))
      end if
      if(.not.allocated(NApoints_atom))then
        allocate(NApoints_atom(Nfound))
      end if

      Nap_grid(1:Nfound,1) = NApoints_IAWAD(1) 
      Nap_grid(1:Nfound,2) = NApoints_IAWAD(2)
      Nap_grid(1:Nfound,3) = NApoints_IAWAD(3)

      NApoint1 = NRpoint1*NApoints_IAWAD(1)
      NApoint2 = NRpoint2*NApoints_IAWAD(2)
      NApoint3 = NRpoint3*NApoints_IAWAD(3)
      MAXpts = NApoint1+NApoint2+NApoint3

      allocate(Egridpts(Nfound,MAXpts))
      do Z_num=1,Nelements
        if(.not.LZfound(Z_num))cycle
           Ifound=GRID_loc(Z_num)
        do KRpoint =1,NRpoints
          factorR=1.4725
          !R_value_IAWAD(Z_num)=1.0
          Radial_points(Ifound,KRpoint)=X_points_IAWAD(KRpoint)*R_value_IAWAD(Z_num)*factorR
          Radial_weights(Ifound,KRpoint)=a_values_IAWAD(KRpoint)*X_points_IAWAD(KRpoint)**2*(R_value_IAWAD(Z_num)*factorR)**3
        end do !KRpoint
        NApoints_atom(Ifound) = MAXpts
        NApts_atom=MAXPts
        allocate(grid_points(NApts_atom))

        NRpoints_region(Ifound,1) = NRpoint1
        NRpoints_region(Ifound,2) = NRpoint2
        NRpoints_region(Ifound,3) = NRpoint3

        Rpoints(1:NRpoint1) = Radial_points(Ifound,1:NRpoint1)
        allocate(Apoints_temp(NApoint1))
        if(NApoints_IAWAD(1).eq.6)then
          call Angular_grid6 (Rpoints, NRpoint1, NApoint1, Apoints_temp)
        else if(NApoints_IAWAD(1).eq.86)then
          call Angular_grid86 (Rpoints, NRpoint1, NApoint1, Apoints_temp)
        else
          write(UNIout,'(a)')'Number of Angular point for region 1 not available'
          call PRG_stop ('Number of Angular point for region 1 not available')
        end if

        grid_points(1:NApoint1)= Apoints_temp(1:NApoint1)
        deallocate(Apoints_temp)

        NR1=NRpoint1+1
        NR2=NRpoint1+NRpoint2
        Rpoints(1:NRpoint2) = Radial_points(Ifound,NR1:NR2)
        allocate(Apoints_temp(NApoint2))
        if(NApoints_IAWAD(2).eq.86)then
          call Angular_grid86 (Rpoints, NRpoint2, NApoint2, Apoints_temp)
        else if(NApoints_IAWAD(2).eq.110)then
          call Angular_grid110 (Rpoints, NRpoint2, NApoint2, Apoints_temp)
        else
          write(UNIout,'(a)')'Number of Angular point for region 2 not available'
          call PRG_stop ('Number of Angular point for region 2 not available')
        end if

        NA1=NApoint1+1
        NA2=NApoint1+NApoint2
        grid_points(NA1:NA2) = Apoints_temp(1:NApoint2)
        deallocate(Apoints_temp)

        NR1=NR1+NRpoint2
        NR2=NR2+NRpoint3
        Rpoints(1:NRpoint3) = Radial_points(Ifound,NR1:NR2)
        allocate(Apoints_temp(NApoint3))
 
        if(NApoints_IAWAD(3).eq.110)then
          call Angular_grid110 (Rpoints, NRpoint3, NApoint3, Apoints_temp)
        else if(NApoints_IAWAD(3).eq.194)then
          call Angular_grid194 (Rpoints,NRpoint3,NApoint3,Apoints_temp)
        else if(NApoints_IAWAD(3).eq.302)then
          call Angular_grid302 (Rpoints,NRpoint3,NApoint3,Apoints_temp)
        else
          write(UNIout,'(a)')'Number of Angular point for region 3 not available'
          call PRG_stop ('Number of Angular point for region 3 not available')
        end if

        NA1=NA1+NApoint2
        NA2=NA2+NApoint3
        grid_points(NA1:NA2) = Apoints_temp(1:NApoint3)
        deallocate(Apoints_temp)

      call RAweights (Ifound)
      Egridpts(Ifound,1:NApts_atom)= grid_points(1:NApts_atom)

      deallocate(grid_points)

      end do ! Z_num

!     deallocate(Radial_points,Radial_weights,NRpoints_region,Nap_grid)       
!     deallocate(a_values,X_points)
! do NOT Deallocate R_value in case optimizing
      call PRG_manager ('exit', 'BLD_IAWAD_GRID', 'UTILITY')
      return
      end subroutine BLD_IAWAD_GRID

