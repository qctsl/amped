     subroutine BLD_grid_location
!**************************************************************************************
!     Date last modified: Oct. 2, 2009                                                *
!     Author: R.A. Poirier                                                            *
!     Desciption:                                                                     *
!**************************************************************************************
! Modules:
      USE type_molecule
      USE type_elements
      USE module_grid_points

      implicit none

! Local scalars:
      integer :: Iatom,Ielement,Z_num
!
! Begin:
      if(.not.allocated(Grid_loc))then ! First call to BLD_grids
        allocate(Grid_loc(Nelements))
      else
        deallocate(Grid_loc)
        allocate(Grid_loc(Nelements))
      end if
      Grid_loc(1:Nelements)=0

      Nfound=0 ! will be the number of elements
      if(.not.allocated(LZfound))then ! First call to BLD_grids
        allocate(LZfound(Nelements))
      else
        deallocate(LZfound)
        allocate(LZfound(Nelements))
      end if
      LZfound(1:Nelements)=.false.

      do Iatom=1,Natoms
        Z_num=CARTESIAN(Iatom)%Atomic_number
        if(Z_num.le.0)cycle
        if(LZfound(Z_num))cycle
        Nfound=Nfound+1
        LZfound(Z_num)=.true.
        Grid_loc(Z_num)=Nfound
      end do

      return
      end subroutine BLD_grid_location
      subroutine RAweights (Ifound)
!************************************************************************************
!     Date last modified Oct. 2, 2009                                               *
!     Author: R.A. Poirier                                                          *
!     Description: Combines the angular and radial weigths                          *
!************************************************************************************
! Modules:
      USE module_grid_points
      USE type_radial_grids
      USE N_integration

      implicit none

! Input scalar:
      integer :: Ifound
! Local scalars:
      integer :: IApoint,Iregion,KRpoint,TApoints,AIbegn,AIend,RIbegn,RIend,Nap_sphere,Nrp_region

      TApoints=0
! Loop over the radial points/regions
      RIend = 0
      AIend = 0
!     write(6,*)'num_regions: ',num_regions
      do Iregion=1,num_regions
        Nrp_region=NRpoints_region(Ifound,Iregion)
        RIbegn = RIend+1
        RIend = RIbegn+Nrp_region-1
!     write(6,*)'Nrp_region,RIbegn,RIend: ',Nrp_region,RIbegn,RIend
        do KRpoint=RIbegn,RIend
          Nap_sphere=Nap_grid(Ifound,Iregion)
          AIbegn=AIend+1
          AIend=AIbegn+Nap_sphere-1
!     write(6,*)'KRpoint,Nap_sphere,AIbegn,AIend: ',KRpoint,Nap_sphere,AIbegn,AIend
          do IApoint=AIbegn,AIend ! Sum over the angular points in the region
            TApoints=TApoints+1
            grid_points(TApoints)%A=grid_points(TApoints)%w
            grid_points(TApoints)%R=Radial_weights(Ifound,KRpoint)
            grid_points(TApoints)%w=grid_points(TApoints)%w*Radial_weights(Ifound,KRpoint)
          end do ! IApoint
        end do ! KRpoint
      end do ! Iregion
      return
      end subroutine RAweights
