      subroutine MENU_plotting
!***********************************************************************
!     Date last modified: February 12, 2003               Version 2.0  *
!     Authors: G. Jansen & J. G. Angyan, R.A. Poirier                  *
!     Desciption: set-up menu for Grid command                         *
!***********************************************************************
! Modules:
      USE program_parser
      USE menu_gets
      USE type_plotting
      USE type_molecule

      implicit none
!
! Local scalars:
      integer :: lenstr
      logical :: done
!
! Local arrays:
!
! Begin:
      call PRG_manager ('enter', 'MENU_plotting', 'UTILITY')
!
! Defaults:
! Menu:
      done=.false.

      do while (.not.done)
      call new_token ('PLOtting:')
!
      if(token(1:4).eq.'HELP')then
      write(UNIout,'(a)') &
      ' Command "PLOtting":                              ', &
      '   Define plotting information      ', &
      '   Syntax : PLOtting                                ', &
      '   AIMlist = < list > The atoms for which properties are desired',&
      '   ATomPrint = < logical > Print atom in molecule contributions, Default is false',&
      '   FORmat    = < command > Command to specify formats', &
      '   GRadlenPrint = < logical > Print the gradient length on a mesh, Default is false',&
      '   MOleculePrint = < logical > Print total molecular contributions, Default is true', & 
      '   ORbitalPrint = < list > Print contributions from specified MOs, Default is false', & 
      '   PLotFilename = < character > Change Mathematica input file to PlotFilename.',&
      '                                 Default is plot_MoleculeName.dat', &
      '   end'

! AIMlist
      else if(token(1:3).eq.'AIM')then
        call GET_value(AIMprint, NAIMprint)
 
! ATomprint
      else if(token(1:2).eq.'AT')then
        call GET_value(ATomPrint)
 
! GRadlenPrint
      else if(token(1:2).eq.'GR')then
        call GET_value(GrdLenPrint)
 
! FORmat
      else if(token(1:3).eq.'FOR')then
        call MENU_format

! MOleculeprint
      else if(token(1:2).eq.'MO')then
        call GET_value(MOleculePrint)

! ORbitalPrint
      else if(token(1:2).eq.'OR')then
        call GET_value (MOprint, NMOprint)
        LMOprint =.true.

! PLotfilename
      else if(token(1:2).eq.'PL')then
        call GET_value (PlotFilename,lenstr)
        LPlotNamed =.true.
  
      else
         call MENU_end (done)
      end if

      end do ! while

!
! end of routine  MENU_plotting
      call PRG_manager ('exit', 'MENU_plotting', 'UTILITY')
      return
      end subroutine MENU_plotting
      subroutine MENU_format
!***********************************************************************
!     Date last modified: February 12, 2003               Version 2.0  *
!     Authors: G. Jansen & J. G. Angyan, R.A. Poirier                  *
!     Desciption: set-up menu for Grid command                         *
!***********************************************************************
! Modules:
      USE program_parser
      USE menu_gets
      USE type_plotting

      implicit none
!
! Local scalars:
      integer :: lenstr
      logical :: done
!
! Begin:
      call PRG_manager ('enter', 'MENU_format', 'UTILITY')
!
! Menu:
      done=.false.

      do while (.not.done)
      call new_token ('FORmat:')
!
      if(token(1:4).eq.'HELP')then
      write(UNIout,'(a)') &
      ' Command "FORmat":                              ', &
      '   Define the formats for the different grids    ', &
      '   Syntax : FORmat                                ', &
      '   DELrho     <string> Format for DEL density         (e.g., f20.6)', &      	  
      '   DEL2rho    <string> Format for DEL2 density        (e.g., f20.6)', &      	  
      '   DRAdial    <string> Format for DEL radial density  (e.g., f20.6)', &      	  
      '   D2Radial   <string> Format for DEL2 radial density (e.g., f20.6)', &      	  
      '   GRidpoints <string> Format for grid points         (e.g., f12.6)', &
      '   RADial     <string> Format for radial density      (e.g., f20.6)', &      	  
      '   RHO        <string> Format for density             (e.g., f20.6)', &      	  
      '   end'

!
! DELrho
      else if(token(1:3).eq.'DEL')then
        call GET_value(fmt_drho, lenstr)
!
! DEL2rho
      else if(token(1:4).eq.'DEL2')then
        call GET_value(fmt_d2rho, lenstr)
!
! DRAdial
      else if(token(1:3).eq.'DRA')then
        call GET_value(fmt_dr2rho, lenstr)
!
! D2Radial
      else if(token(1:3).eq.'D2R')then
        call GET_value(fmt_d2r2rho, lenstr)
!
! RADial
      else if(token(1:3).eq.'RAD')then
        call GET_value(fmt_r2rho, lenstr)
!
! GRidpoints
      else if(token(1:2).eq.'GR')then
        call GET_value(fmt_GridPts, lenstr)
!
! RHO
      else if(token(1:3).eq.'RHO')then
        call GET_value(fmt_rho, lenstr)

      else
         call MENU_end (done)
      end if

      end do ! while

      Pfmt_rho='(3'//trim(fmt_GridPts)//','//trim(fmt_rho)//')'
      Pfmt_drho='(3'//trim(fmt_GridPts)//',3'//trim(fmt_drho)//')'
      Pfmt_d2rho='(3'//trim(fmt_GridPts)//',6'//trim(fmt_d2rho)//')'
      Pfmt1_d2rho='('//trim(fmt_GridPts)//',6'//trim(fmt_d2rho)//')'
      Pfmt2_d2rho='(2'//trim(fmt_GridPts)//',6'//trim(fmt_d2rho)//')'
      Pfmt_Eig='(3'//trim(fmt_GridPts)//',4'//trim(fmt_rho)//')'
      Pfmt_r2rho='(3'//trim(fmt_GridPts)//','//trim(fmt_r2rho)//')'
      Pfmt1_r2rho='('//trim(fmt_GridPts)//','//trim(fmt_r2rho)//')'
      Pfmt2_r2rho='(2'//trim(fmt_GridPts)//','//trim(fmt_r2rho)//')'
      Pfmt_dRad='(4'//trim(fmt_dr2rho)//'/)'
      Pfmt_d2Rad='(6'//trim(fmt_d2r2rho)//'/)'
!
! end of routine  MENU_format
      call PRG_manager ('exit', 'MENU_format', 'UTILITY')
      return
      end
