      MODULE type_radial_grids
!**************************************************************************************************
!     Date last modified: February 7, 2010                                                        *
!     Author: R.A. Poirier                                                                        *
!     Description: Used to build the radial grids and weights                                     * 
!**************************************************************************************************
!
      implicit none
!
      double precision, dimension (:,:), allocatable :: Radial_points ! Radial points for each atom
      double precision, dimension (:,:), allocatable :: Radial_weights ! Radial weights for each atom
!
      end MODULE type_radial_grids
