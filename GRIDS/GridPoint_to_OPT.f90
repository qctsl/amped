      subroutine GridPoint_to_OPT
!********************************************************************
!     Date last modified: August 15, 2012                           *
!     Authors: R.A. Poirier                                         *
!     Description: Calculate del charge density at point.           *
!********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE QM_defaults
      USE QM_objects
      USE OPT_objects
      USE type_density
      USE module_grid_points

      implicit none
!
! Local scalars:
      integer :: Ipoint
!
! Begin:
      call PRG_manager ('enter','GridPoint_to_OPT', 'UTILITY')
!
      Noptpr=3
      if(.not.allocated(PARset))then
        allocate (OPT_par(Noptpr))
        allocate (PARset(Noptpr))
      else
        allocate (OPT_par(Noptpr))
        deallocate (PARset)
        allocate (PARset(Noptpr))
      end if

        Ipoint=1
        PARset(1)=grid_points(Ipoint)%x
        PARset(2)=grid_points(Ipoint)%y
        PARset(3)=grid_points(Ipoint)%z
!
! End of routine GridPoint_to_OPT
      call PRG_manager ('exit', 'GridPoint_to_OPT', 'UTILITY')
      return
      end subroutine GridPoint_to_OPT
      subroutine OPT_TO_GridPoint
!********************************************************************
!     Date last modified: August 15, 2012                           *
!     Authors: R.A. Poirier                                         *
!     Description: Calculate del charge density at point.           *
!********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE QM_defaults
      USE QM_objects
      USE OPT_objects
      USE type_density
      USE module_grid_points

      implicit none
!
! Local scalars:
      integer :: Ipoint
!
! Begin:
      call PRG_manager ('enter','OPT_TO_GridPoint', 'UTILITY')
!
        Ipoint=1
        grid_points(Ipoint)%x=PARset(1)
        grid_points(Ipoint)%y=PARset(2)
        grid_points(Ipoint)%z=PARset(3)
!
! End of routine OPT_TO_GridPoint
      call PRG_manager ('exit', 'OPT_TO_GridPoint', 'UTILITY')
      return
      end subroutine OPT_TO_GridPoint
