      subroutine BLD_EAMD_mesh
!****************************************************************************************
!     Date last modified: April 26, 2019                                                *
!     Authors: JWH                                                                      *
!     Description: Calculate the momentum-balance density on a mesh.                    *
!****************************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE module_grid_points
      USE type_plotting
      USE type_Weights

      implicit none
!
! Local scalars:
      integer :: Ipoint
      double precision :: X,Y,Z
      double precision :: EMaa_pt,EMHFaa_pt
      double precision :: EMbb_pt,EMHFbb_pt
      double precision :: EMab_pt,EMHFab_pt
      double precision :: EMba_pt,EMHFba_pt
      double precision :: AMaa_pt,AMHFaa_pt
      double precision :: AMbb_pt,AMHFbb_pt
      double precision :: AMab_pt,AMHFab_pt
      double precision :: AMba_pt,AMHFba_pt
!
! Local array:
      double precision, allocatable,dimension(:) :: EMaa,EMHFaa
      double precision, allocatable,dimension(:) :: EMbb,EMHFbb
      double precision, allocatable,dimension(:) :: EMab,EMHFab
      double precision, allocatable,dimension(:) :: EMba,EMHFba
!
      double precision, allocatable,dimension(:) :: AMaa,AMHFaa
      double precision, allocatable,dimension(:) :: AMbb,AMHFbb
      double precision, allocatable,dimension(:) :: AMab,AMHFab
      double precision, allocatable,dimension(:) :: AMba,AMHFba
!
! Begin:
      call PRG_manager ('enter', 'BLD_EAMD_mesh', 'UTILITY')
!
      call GET_object ('QM', '2RDM', 'AO')
!
      allocate (EMaa(1:NGridPoints))
      allocate (EMHFaa(1:NGridPoints))
      allocate (EMbb(1:NGridPoints))
      allocate (EMHFbb(1:NGridPoints))
!
      allocate (EMab(1:NGridPoints))
      allocate (EMHFab(1:NGridPoints))
      allocate (EMba(1:NGridPoints))
      allocate (EMHFba(1:NGridPoints))
!
      allocate (AMaa(1:NGridPoints))
      allocate (AMHFaa(1:NGridPoints))
      allocate (AMbb(1:NGridPoints))
      allocate (AMHFbb(1:NGridPoints))
!
      allocate (AMab(1:NGridPoints))
      allocate (AMHFab(1:NGridPoints))
      allocate (AMba(1:NGridPoints))
      allocate (AMHFba(1:NGridPoints))
!
      EMaa=ZERO
      EMHFaa=ZERO
!
      EMbb=ZERO
      EMHFbb=ZERO
!
      EMab=ZERO
      EMHFab=ZERO
!
      EMba=ZERO
      EMHFba=ZERO
!
      AMaa=ZERO
      AMHFaa=ZERO
!
      AMbb=ZERO
      AMHFbb=ZERO
!
      AMab=ZERO
      AMHFab=ZERO
!
      AMba=ZERO
      AMHFba=ZERO
!
      ! grid_points may of been destroyed between MENU and here, so rebuild mesh
      ! Really should always be done just before use
      if(LMesh)then
        call BLD_GRID_MESH
      else
        call BLD_GRID_RADIAL
      end if
!
      do Ipoint=1,NGridPoints
!
        X = grid_points(Ipoint)%x
        Y = grid_points(Ipoint)%y
        Z = grid_points(Ipoint)%z
!
! same-spin
        call GET_EAMDss_point (X, Y, Z, EMaa_pt, EMHFaa_pt, AMaa_pt, AMHFaa_pt,&
                                        EMbb_pt, EMHFbb_pt, AMbb_pt, AMHFbb_pt) 
!
        EMaa(Ipoint) = EMaa_pt
!
        EMHFaa(Ipoint) = EMHFaa_pt
!
        EMbb(Ipoint) = EMbb_pt
!
        EMHFbb(Ipoint) = EMHFbb_pt
!
        AMaa(Ipoint) = AMaa_pt
!
        AMHFaa(Ipoint) = AMHFaa_pt
!
        AMbb(Ipoint) = AMbb_pt
!
        AMHFbb(Ipoint) = AMHFbb_pt
!
! opposite-spin
        call GET_EAMDos_point (X, Y, Z, EMab_pt, EMHFab_pt, AMab_pt, AMHFab_pt,&
                                        EMba_pt, EMHFba_pt, AMba_pt, AMHFba_pt) 
!
        EMab(Ipoint) = EMab_pt
!
        EMHFab(Ipoint) = EMHFab_pt
!
        EMba(Ipoint) = EMba_pt
!
        EMHFba(Ipoint) = EMHFba_pt
!
        AMab(Ipoint) = AMab_pt
!
        AMHFab(Ipoint) = AMHFab_pt
!
        AMba(Ipoint) = AMba_pt
!
        AMHFba(Ipoint) = AMHFba_pt
!
      end do ! Ipoint
!
! Print the atom in molecule EM density
!      if(AtomPrint)then
!        do Iatom=1,Natoms
!          Znum=CARTESIAN(Iatom)%Atomic_number
!          if(Znum.le.0)cycle
!          PlotFileID=' '
!          PlotFunction=' '
!          write(PlotFunction,'(a,i4)')'EM for atom ',Iatom
!          write(PlotFileID,'(a)')'EM_ATOMIC'
!          call BLD_plot_file_ATOMIC (PlotFileID, NGridPoints, Iatom, Plotfile_unit)
!          call PRT_MESH_ATOMIC (EMTotal_atom, Iatom, Natoms, NGridPoints)
!          close (unit=Plotfile_unit)
!        end do ! Iatom
!      end if
!
! Print the EM density 
! same-spin
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'EMD_aa_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular EM density'
        call PRT_GRID_MOL (EMaa, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular EM density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (EMaa, NGridPoints, 'F21.14')
      end if
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'EMD_HF_aa_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular EM density'
        call PRT_GRID_MOL (EMHFaa, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular EM density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (EMHFaa, NGridPoints, 'F21.14')
      end if
!
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'EMD_bb_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular EM density'
        call PRT_GRID_MOL (EMbb, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular EM density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (EMbb, NGridPoints, 'F21.14')
      end if
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'EMD_HF_bb_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular EM density'
        call PRT_GRID_MOL (EMHFbb, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular EM density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (EMHFbb, NGridPoints, 'F21.14')
      end if
!
! opposite-spin
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'EMD_ab_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular EM density'
        call PRT_GRID_MOL (EMab, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular EM density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (EMab, NGridPoints, 'F21.14')
      end if
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'EMD_HF_ab_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular EM density'
        call PRT_GRID_MOL (EMHFab, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular EM density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (EMHFab, NGridPoints, 'F21.14')
      end if
!
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'EMD_ba_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular EM density'
        call PRT_GRID_MOL (EMba, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular EM density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (EMba, NGridPoints, 'F21.14')
      end if
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'EMD_HF_ba_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular EM density'
        call PRT_GRID_MOL (EMHFba, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular EM density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (EMHFba, NGridPoints, 'F21.14')
      end if
!
! Print the AM density
! same-spin
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'AMD_aa_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular AM density'
        call PRT_GRID_MOL (AMaa, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular AM density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (AMaa, NGridPoints, 'F21.14')
      end if
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'AMD_HF_aa_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular AM density'
        call PRT_GRID_MOL (AMHFaa, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular AM density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (AMHFaa, NGridPoints, 'F21.14')
      end if
!
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'AMD_bb_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular AM density'
        call PRT_GRID_MOL (AMbb, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular AM density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (AMbb, NGridPoints, 'F21.14')
      end if
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'AMD_HF_bb_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular AM density'
        call PRT_GRID_MOL (AMHFbb, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular AM density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (AMHFbb, NGridPoints, 'F21.14')
      end if
!
! opposite-spin
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'AMD_ab_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular AM density'
        call PRT_GRID_MOL (AMab, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular AM density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (AMab, NGridPoints, 'F21.14')
      end if
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'AMD_HF_ab_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular AM density'
        call PRT_GRID_MOL (AMHFab, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular AM density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (AMHFab, NGridPoints, 'F21.14')
      end if
!
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'AMD_ba_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular AM density'
        call PRT_GRID_MOL (AMba, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular AM density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (AMba, NGridPoints, 'F21.14')
      end if
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'AMD_HF_ba_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular AM density'
        call PRT_GRID_MOL (AMHFba, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular AM density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (AMHFba, NGridPoints, 'F21.14')
      end if
!
! End of routine BLD_EAMD_mesh
      call PRG_manager ('exit', 'BLD_EAMD_mesh', 'UTILITY')
      return
      end subroutine BLD_EAMD_mesh
