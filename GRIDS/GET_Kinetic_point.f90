      subroutine GET_Tp_spin_point (xpt, ypt, zpt, Tpa_grid, TpHFa_grid, Tpb_grid, TpHFb_grid)
!*******************************************************************************
!     Date last modified: October 8, 2022                                      *
!     Authors: JWH                                                             *
!     Description: Calculate the positive definite kinetic energy at a point.  *
!*******************************************************************************
! Modules:
      USE QM_objects
      USE module_grid_points
      USE program_constants

      implicit none

! Input scalars:
      double precision, intent(IN) :: xpt,ypt,zpt
!
! Output scalars:
      double precision :: Tpa_grid, TpHFa_grid
      double precision :: Tpb_grid, TpHFb_grid
!
! Local arrays:
      double precision, dimension(:), allocatable :: dxdxGprod,dydyGprod,dzdzGprod ! d/dx d/dx Gprod
      double precision, dimension(:), allocatable :: d2Gprod
!
! Local functions
      double precision :: TraceAB
!
! Begin: 
!
      call GET_object ('QM', 'SPIN_DENSITY_1MATRIX', 'AO')
!
      allocate (dxdxGprod(MATlen))
      allocate (dydyGprod(MATlen))
      allocate (dzdzGprod(MATlen))
      allocate (d2Gprod(MATlen))
!
      Tpa_grid   = zero
      TpHFa_grid = zero
!
      Tpb_grid   = zero
      TpHFb_grid = zero
!
      call dGAdGB_products (xpt, ypt, zpt, dxdxGprod, dydyGprod, dzdzGprod, MATlen)
      d2Gprod = 0.5D0*(dxdxGprod  + dydyGprod + dzdzGprod)
!
      Tpa_grid   = TraceAB (PM0_alpha, d2Gprod, NBasis)
      TpHFa_grid = TraceAB (PM0HF_alpha, d2Gprod, NBasis)
!
      Tpb_grid   = TraceAB (PM0_beta, d2Gprod, NBasis)
      TpHFb_grid = TraceAB (PM0HF_beta, d2Gprod, NBasis)
!
! End of routine GET_Tp_spin_point
      return
      end subroutine GET_Tp_spin_point
      subroutine GET_Ts_spin_point (xpt, ypt, zpt, Tsa_grid, TsHFa_grid, Tsb_grid, TsHFb_grid)
!*******************************************************************************
!     Date last modified: October 8, 2022                                      *
!     Authors: JWH                                                             *
!     Description: Calculate the Schrodinger kinetic energy at a point.        *
!*******************************************************************************
! Modules:
      USE QM_objects
      USE module_grid_points
      USE program_constants

      implicit none

! Input scalars:
      double precision, intent(IN) :: xpt,ypt,zpt
!
! Output scalars:
      double precision :: Tsa_grid, TsHFa_grid
      double precision :: Tsb_grid, TsHFb_grid
!
! Local arrays:
      double precision, dimension(:), allocatable :: dxdxGprod,dydyGprod,dzdzGprod ! d/dx d/dx Gprod
      double precision, dimension(:), allocatable :: d2Gprod
!
! Local functions
      double precision :: TraceAB
!
! Begin: 
!
      call GET_object ('QM', 'SPIN_DENSITY_1MATRIX', 'AO')
!
      allocate (dxdxGprod(MATlen))
      allocate (dydyGprod(MATlen))
      allocate (dzdzGprod(MATlen))
      allocate (d2Gprod(MATlen))
!
      Tsa_grid   = zero
      TsHFa_grid = zero
!
      Tsb_grid   = zero
      TsHFb_grid = zero
!
      call GAd2GB_products (xpt, ypt, zpt, dxdxGprod, dydyGprod, dzdzGprod, MATlen)
      d2Gprod = 0.5D0*(dxdxGprod  + dydyGprod + dzdzGprod)
!
      Tsa_grid   = TraceAB (PM0_alpha, d2Gprod, NBasis)
      TsHFa_grid = TraceAB (PM0HF_alpha, d2Gprod, NBasis)
!
      Tsb_grid   = TraceAB (PM0_beta, d2Gprod, NBasis)
      TsHFb_grid = TraceAB (PM0HF_beta, d2Gprod, NBasis)
!
! End of routine GET_Ts_spin_point
      return
      end subroutine GET_Ts_spin_point
