      MODULE module_grid_points
!**************************************************************************************************
!     Date last modified: July 22, 2014                                              Version 2.0  *
!     Author: Ahmad and R.A. Poirier                                                              *
!     Description: Contains grid information                                                      * 
!**************************************************************************************************
!
      implicit none
!
! For grid points or mesh
      integer :: NgridPoints   ! Total No. of points
      integer :: NX,NY,NZ      ! No. of points for MESH
      integer :: NRegions  ! Used to space into radial regions
      integer :: MAXCycles  ! Maximum number of cycles for finding critical points

      double precision :: MAX_r
      double precision :: GRID_MIN(3),GRID_MESH(3)
      double precision :: Ralpha,Rbeta,Rgama
!
! For 2e density
      double precision :: test_pt(3)

! For radial grids
      integer :: Nfound     ! Number of different atom types found in a molecule, 2 in CH4 e.g.
      integer :: MAX_Angular_points  ! Maximum number of angular points, maximum value of NApts_atom
      integer :: NApts_atom ! Total number of angular points for a given atom
      integer :: NApts_Iatom, NApts_Jatom ! Total number of angular points for each atom (double int)
      integer :: CP_atom
      logical :: LSG0
      character(len=16) :: RADIAL_grid  ! SG1, Pople, Becke... 
      character(len=6) :: CPRS ! Rank and Signature a of critical point (R,S)

      type  :: type_grid_points
        double precision    :: X
        double precision    :: Y
        double precision    :: Z
        double precision    :: W ! Total
        double precision    :: A ! Angular
        double precision    :: R ! Radial
      end type type_grid_points

      type(type_grid_points), dimension(:,:), allocatable :: Egridpts ! Grid points by element
      type(type_grid_points), dimension(:), allocatable :: grid_points

      logical, dimension(:), allocatable :: LZfound ! True when an atom type is found in a molecule
      integer, dimension(:), allocatable :: Grid_loc ! 
      integer, dimension(:), allocatable :: NApoints_atom ! Number of total points for each atom type
!
! For mesh
      logical :: LMesh ! user defined mesh
      
      character(len=3) :: KineticMethod

      end MODULE module_grid_points
