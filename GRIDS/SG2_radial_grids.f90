     subroutine BLD_SG2_GRID
!************************************************************************************
!     Date last modified Oct. 2, 2009                                               *
!     Author: R.A. Poirier                                                          *
!     Description:                                                                  *
!************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE type_elements
      USE type_radial_grids
      USE NI_defaults
      USE N_integration

      implicit none
!
! Local scalars:
      integer :: KRpoint,Iatom,Znum,NA1,NA2,NR1,NR2
      integer :: NRpoint1,NRpoint2,NRpoint3,NRpoint4,NRpoint5 ! Number of radial points per region  
      integer :: NLpoint1,NLpoint2,NLpoint3,NLpoint4,NLpoint5 ! Number of Lebedev points per region  
      integer :: NApoint1,NApoint2,NApoint3,NApoint4,NApoint5 ! Total number of angular points per region  
      integer :: Ifound,MAXpts
      integer :: I
      double precision :: alpha,hval,term,Xi
      
! Local arrays:
      double precision, dimension(18) :: Alpha_values
      data Alpha_values/2.6d0,3.2d0, &
                        3.2d0,2.4d0,2.4d0,2.2d0,2.2d0,2.2d0,2.2d0,3.2d0, &
                        3.2d0,2.4d0,2.5d0,2.3d0,2.5d0,2.5d0,2.5d0,3.2d0/
      double precision, dimension(18) :: X_min,X_max
      data X_min/-2.31297d0,-2.20434d0, &
                 -2.20434d0,-2.34942d0,-2.34942d0,-2.38592d0,-2.38592d0,-2.38592d0,-2.38592d0,-2.20434d0, &
                 -2.20434d0,-2.34942d0,-2.33119d0,-2.36767d0,-2.33119d0,-2.33119d0,-2.33119d0,-2.20434d0/
      data X_max/1.4d0,0.903183d0, &
                 1.23375d0,1.46196d0,1.39237d0,1.40339d0,1.32367d0,1.2909d0,1.38204d0,0.955423d0, &
                 1.25753d0,1.5402d0,1.50346d0,1.53498d0,1.36072d0,1.31739d0,1.27694d0,0.994133d0/
!
! Begin:
      call PRG_manager ('enter', 'BLD_SG2_GRID', 'UTILITY')
!
! Check if parameters are available
      do Iatom=1,Natoms
        if(CARTESIAN(Iatom)%Atomic_number.gt.18)then
          write(UNIout,'(a)')'ERROR> BLD_SG2_Radial_Grids: Parameters available only up to Argon'
          stop 'ERROR> BLD_SG2_Radial_Grids: Parameters available only up to Chlorine'
        end if
      end do

      call BLD_grid_location ! Defines Nfound and LZfound
!
      NRpoints=75
      if(allocated(Radial_points))then
        deallocate(Radial_points,Radial_weights)
      end if 
      allocate(Radial_points(Nfound,NRpoints),Radial_weights(Nfound,NRpoints))

      call BLD_BeckeW_XI
!
! Generate the radial grid and weights
      do Znum=1,Nelements
        if(.not.LZfound(Znum))cycle
        Ifound=GRID_loc(Znum)
        Alpha=Alpha_values(Znum)
        hval=(X_max(Znum)-X_min(Znum))/dble(NRpoints-1)
! The inner limit: the first radial point starting from the nucleus:
      Xi=X_min(Znum)
      do KRpoint=1,NRpoints
        Radial_points(Ifound,KRpoint)=dexp(alpha*Xi-dexp(-Xi))
        term=dexp(THREE*alpha*Xi-THREE*dexp(-Xi))
        Radial_weights(Ifound,KRpoint)=term*(alpha+dexp(-Xi))*hval
        Xi=X_min(Znum)+dble(KRpoint)*hval
      end do ! KRpoint
      end do ! Znum
!
      num_regions=5
      if(allocated(Nap_grid))then
        deallocate(Nap_grid,NRpoints_region,NAPoints_atom)
      end if 
      allocate(Nap_grid(Nfound,num_regions), NRpoints_region(Nfound,num_regions), NApoints_atom(Nfound))
!
! Number of radial grids by region
      MAXpts=0

      do Znum=1,Nelements
      if(.not.LZfound(Znum))cycle
      call Define_SG2_parameters
      Ifound=GRID_loc(Znum)
      Nap_grid(Ifound,1)=NLpoint1
      Nap_grid(Ifound,2)=NLpoint2
      Nap_grid(Ifound,3)=NLpoint3
      Nap_grid(Ifound,4)=NLpoint4
      Nap_grid(Ifound,5)=NLpoint5
      NRpoints_region(Ifound,1)=NRpoint1
      NRpoints_region(Ifound,2)=NRpoint2
      NRpoints_region(Ifound,3)=NRpoint3
      NRpoints_region(Ifound,4)=NRpoint4
      NRpoints_region(Ifound,5)=NRpoint5

      NApoint1=NLpoint1*NRpoint1
      NApoint2=NLpoint2*NRpoint2
      NApoint3=NLpoint3*NRpoint3
      NApoint4=NLpoint4*NRpoint4
      NApoint5=NLpoint5*NRpoint5
!
      NApoints_atom(Ifound)=NApoint1+NApoint2+NApoint3+NApoint4+NApoint5
      if(NApoints_atom(Ifound).gt.MAXpts)MAXpts=NApoints_atom(Ifound)
      end do ! Znum

      MAX_Angular_points=MAXpts
!
      if(allocated(Egridpts))then
        deallocate(Egridpts)
      end if 
      allocate(Egridpts(Nfound,MAXpts))
!
      do Ifound=1,Nfound
      NRpoint1=NRpoints_region(Ifound,1)
      NRpoint2=NRpoints_region(Ifound,2)
      NRpoint3=NRpoints_region(Ifound,3)
      NRpoint4=NRpoints_region(Ifound,4)
      NRpoint5=NRpoints_region(Ifound,5)
      NLpoint1=Nap_grid(Ifound,1)
      NLpoint2=Nap_grid(Ifound,2)
      NLpoint3=Nap_grid(Ifound,3)
      NLpoint4=Nap_grid(Ifound,4)
      NLpoint5=Nap_grid(Ifound,5)
      NApoint1=NLpoint1*NRpoint1
      NApoint2=NLpoint2*NRpoint2
      NApoint3=NLpoint3*NRpoint3
      NApoint4=NLpoint4*NRpoint4
      NApoint5=NLpoint5*NRpoint5

      NApts_atom=NApoints_atom(Ifound)
      if(allocated(grid_points))then
        deallocate(grid_points)
      end if
      allocate(grid_points(NApts_atom))

! Radial region 1
      Rpoints(1:NRpoint1)=Radial_points(Ifound,1:NRpoint1)
!     write(6,*)'Region 1:'
!     write(6,*)NRpoint1,NLpoint1,NApoint1
!     write(6,*)'from ',Rpoints(1),'to ',Rpoints(NRpoint1),' ',NApoint1
      allocate(Apoints_temp(NApoint1))
      call Define_angular_grids (NLpoint1, NRpoint1, NApoint1)
      grid_points(1:NApoint1)=Apoints_temp(1:NApoint1)
      deallocate(Apoints_temp)

! Radial region 2
      NR1=NRpoint1+1
      NR2=NRpoint1+NRpoint2
      Rpoints(1:NRpoint2)=Radial_points(Ifound,NR1:NR2)
!     write(6,*)'Region 2:'
!     write(6,*)NRpoint2,NLpoint2,NApoint2
!     write(6,*)'from ',Rpoints(1),'to ',Rpoints(NRpoint2),' ',NApoint2
      allocate(Apoints_temp(NApoint2))
      call Define_angular_grids (NLpoint2, NRpoint2, NApoint2)
      NA1=NApoint1+1
      NA2=NApoint1+NApoint2
      grid_points(NA1:NA2)=Apoints_temp(1:NApoint2)
      deallocate(Apoints_temp)

! Radial region 3
      NR1=NR1+NRpoint2
      NR2=NR2+NRpoint3
      Rpoints(1:NRpoint3)=Radial_points(Ifound,NR1:NR2)
!     write(6,*)'Region 3:'
!     write(6,*)NRpoint3,NLpoint3,NApoint3
!     write(6,*)'from ',Rpoints(1),'to ',Rpoints(NRpoint3),' ',NApoint3
      allocate(Apoints_temp(NApoint3))
      call Define_angular_grids (NLpoint3, NRpoint3, NApoint3)
      NA1=NA1+NApoint2
      NA2=NA2+NApoint3
      grid_points(NA1:NA2)=Apoints_temp(1:NApoint3)
      deallocate(Apoints_temp)

! Radial region 4
      NR1=NR1+NRpoint3
      NR2=NR2+NRpoint4
      Rpoints(1:NRpoint4)=Radial_points(Ifound,NR1:NR2)
!     write(6,*)'Region 4:'
!     write(6,*)NRpoint4,NLpoint4,NApoint4
!     write(6,*)'from ',Rpoints(1),'to ',Rpoints(NRpoint4),' ',NApoint4
      allocate(Apoints_temp(NApoint4))
      call Define_angular_grids (NLpoint4, NRpoint4, NApoint4)
      NA1=NA1+NApoint3
      NA2=NA2+NApoint4
      grid_points(NA1:NA2)=Apoints_temp(1:NApoint4)
      deallocate(Apoints_temp)

! Radial region 5
      NR1=NR1+NRpoint4
      NR2=NRpoints
      Rpoints(1:NRpoint5)=Radial_points(Ifound,NR1:NR2)
!     write(6,*)'Region 5:'
!     write(6,*)NRpoint5,NLpoint5,NApoint5
!     write(6,*)'from ',Rpoints(1),'to ',Rpoints(NRpoint5),' ',NApoint5
      allocate(Apoints_temp(NApoint5))
      call Define_angular_grids (NLpoint5, NRpoint5, NApoint5)
      NA1=NA1+NApoint4
      NA2=NApts_atom
      grid_points(NA1:NA2)=Apoints_temp(1:NApoint5)
      deallocate(Apoints_temp)

      call RAweights (Ifound)
      Egridpts(Ifound,1:NApts_atom)=grid_points(1:NApts_atom)

      deallocate(grid_points)
      end do ! Ifound
!
      deallocate(Radial_points,Radial_weights)

      call PRG_manager ('exit', 'BLD_SG2_GRID', 'UTILITY')
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine Define_SG2_parameters
!************************************************************************************
!     Date last modified Oct. 2, 2009                                               *
!     Author: R.A. Poirier                                                          *
!     Description:                                                                  *
!************************************************************************************
! Modules:

      RADIAL_grid='SG2'
      select case (Znum)
        case(1,2) ! Hydrogen, Neon
        NRpoint1=35
        NRpoint2=12
        NRpoint3=16
        NRpoint4=7
        NRpoint5=5
        NLpoint1=6
        NLpoint2=110
        NLpoint3=302
        NLpoint4=86
        NLpoint5=26
        case(3,4) ! Lithium, Berylium
        NRpoint1=35
        NRpoint2=12
        NRpoint3=17
        NRpoint4=7
        NRpoint5=4
        NLpoint1=6
        NLpoint2=110
        NLpoint3=302
        NLpoint4=86
        NLpoint5=50
        case(5,6) ! Boron, Carbon
        NRpoint1=35
        NRpoint2=12
        NRpoint3=17
        NRpoint4=7
        NRpoint5=4
        NLpoint1=6
        NLpoint2=110
        NLpoint3=302
        NLpoint4=146
        NLpoint5=26
        case(7) ! Nitrogen
        NRpoint1=35
        NRpoint2=12
        NRpoint3=17
        NRpoint4=7
        NRpoint5=4
        NLpoint1=6
        NLpoint2=110
        NLpoint3=302
        NLpoint4=86
        NLpoint5=26
        case(8) ! Oxygen
        NRpoint1=30
        NRpoint2=14
        NRpoint3=18
        NRpoint4=8
        NRpoint5=5
        NLpoint1=6
        NLpoint2=110
        NLpoint3=302
        NLpoint4=146
        NLpoint5=50
        case(9,10) ! Fluorine, Neon
        NRpoint1=26
        NRpoint2=16
        NRpoint3=19
        NRpoint4=8
        NRpoint5=6
        NLpoint1=6
        NLpoint2=110
        NLpoint3=302
        NLpoint4=110
        NLpoint5=50
        case(11,12) ! Sodium, Magnesium
        NRpoint1=35
        NRpoint2=12
        NRpoint3=17
        NRpoint4=7
        NRpoint5=4
        NLpoint1=6
        NLpoint2=110
        NLpoint3=302
        NLpoint4=86
        NLpoint5=50
        case(13) ! Aluminium
        NRpoint1=32
        NRpoint2=15
        NRpoint3=17
        NRpoint4=7
        NRpoint5=4
        NLpoint1=6
        NLpoint2=110
        NLpoint3=302
        NLpoint4=146
        NLpoint5=86
        case(14) ! Silicon
        NRpoint1=32
        NRpoint2=15
        NRpoint3=17
        NRpoint4=7
        NRpoint5=4
        NLpoint1=6
        NLpoint2=110
        NLpoint3=302
        NLpoint4=146
        NLpoint5=50
        case(15,16) ! Phosphorous, Sulfur
        NRpoint1=30
        NRpoint2=14
        NRpoint3=17
        NRpoint4=7
        NRpoint5=7
        NLpoint1=6
        NLpoint2=110
        NLpoint3=302
        NLpoint4=146
        NLpoint5=38
        case(17,18) ! Chlorine, Argon
        NRpoint1=26
        NRpoint2=16
        NRpoint3=19
        NRpoint4=8
        NRpoint5=6
        NLpoint1=6
        NLpoint2=110
        NLpoint3=302
        NLpoint4=110
        NLpoint5=50
        case default
      end select
      return
      end subroutine Define_SG2_parameters
      subroutine Define_angular_grids (NLpoints, NRpoints, NApoints)
!************************************************************************************
!     Date last modified Oct. 2, 2009                                               *
!     Author: R.A. Poirier                                                          *
!     Description:                                                                  *
!************************************************************************************
! Modules:
      integer :: NLpoints,NRpoints,NApoints

      select case (NLpoints)
        case(6)
          call Angular_grid6 (Rpoints, NRpoints, NApoints, Apoints_temp)
        case(26)
          call Angular_grid26 (Rpoints, NRpoints, NApoints, Apoints_temp)
        case(38)
          call Angular_grid38 (Rpoints, NRpoints, NApoints, Apoints_temp)
        case(50)
          call Angular_grid50 (Rpoints, NRpoints, NApoints, Apoints_temp)
        case(86)
          call Angular_grid86 (Rpoints, NRpoints, NApoints, Apoints_temp)
        case(110)
          call Angular_grid110 (Rpoints, NRpoints, NApoints, Apoints_temp)
        case(146)
          call Angular_grid146 (Rpoints, NRpoints, NApoints, Apoints_temp)
        case(302)
          call Angular_grid302 (Rpoints, NRpoints, NApoints, Apoints_temp)
        case default
          write(UNIout,'(a,i6)')'Define_angular_grids: Need to add angular grid ',NLpoints
          stop 'ERROR> Define_angular_grids: Need to add angular grid '
      end select
      return
      end subroutine Define_angular_grids
! END CONTAINS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine BLD_SG2_GRID
