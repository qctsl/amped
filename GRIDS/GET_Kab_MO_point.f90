      subroutine GET_Kab_MO_point (Xpt, Ypt, Zpt, aMO, bMO, Kab)
!*******************************************************************************
!     Date last modified: May 24 2017                                          *
!     Authors: R.A. Poirier and I. Awad                                        *
!     Description: Compute the Exchange at a grid point                        *
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE GSCF_work

      implicit none
!
      double precision :: Xpt,Ypt,Zpt

! Local scalars:
      integer :: aMO,bMO,NPab,NPba
      double precision :: S_ab,Kab,PV_ba
!
! Local array:
      double precision, dimension(:), allocatable :: AOprod
      double precision, dimension(:), allocatable :: Vpotl
      type PM_xx
        integer :: IJ
        double precision :: Pxx
      end type
      type (PM_xx), dimension(:), allocatable :: PM_exch_ab
      type (PM_xx), dimension(:), allocatable :: PM_exch_ba

!
! Begin:
!
      allocate(PM_exch_ab(MATlen))
      allocate(PM_exch_ba(MATlen))
      allocate(AOprod(1:MATlen))
      allocate(Vpotl(1:MATlen))

      call I1E_Vint (Vpotl, AOprod, MATlen, Xpt, Ypt, Zpt)      
      call BLD_PM_exch (CMO%coeff, Nbasis)
      call BLD_S_ab_PV_ba
      Kab=S_ab*PV_ba

      deallocate (AOprod)
      deallocate (Vpotl)
      deallocate (PM_exch_ab)
      deallocate (PM_exch_ba)

      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine BLD_PM_exch (CMO, Nbasis)
!*******************************************************************************
!     Date last modified: March 15, 2011                                       *
!     Authors: R.A. Poirier                                                    *
!     Description: Build P_ab and P_ba                                         *
!*******************************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer :: Nbasis
      double precision :: CMO(Nbasis,Nbasis)
!
! Local scalars:
      integer :: Mu,Nu
      double precision :: Temp_ab
      double precision :: Temp_aa,Temp_bb,threshold
      parameter (threshold=1.0D-12)
!
! Local function:
!
! Begin:
      call PRG_manager ('enter','BLD_PM_exch', 'UTILITY')
!
      NPab=ZERO
      NPba=ZERO
      do Mu=1,Nbasis
      do Nu=1,Mu
        if(Mu.ne.Nu)then ! Temp_ab=Temp_ba!
          Temp_ab=CMO(Mu,aMO)*CMO(Nu,bMO)+CMO(Nu,aMO)*CMO(Mu,bMO)
        else
          Temp_ab=CMO(Mu,aMO)*CMO(Nu,bMO)
        end if
        if(dabs(Temp_ab).gt.threshold)then
          NPab=NPab+1
          PM_exch_ab(NPab)%Pxx=Temp_ab
          PM_exch_ab(NPab)%IJ=Mu*(Mu-1)/2+Nu
        end if
      end do ! Nu
      end do ! Mu
!
! End of routine BLD_PM_exch
      call PRG_manager ('exit', 'BLD_PM_exch', 'UTILITY')
      return
      end subroutine BLD_PM_exch
      subroutine BLD_S_ab_PV_ba
!*******************************************************************************
!     Date last modified: March 15, 2011                                       *
!     Authors: R.A. Poirier                                                    *
!     Description: Calculate the charge density for each MO at                 *
!                  point Xcoor, Ycoor, Zcoor                                   *
!*******************************************************************************
! Modules:

      implicit none
!
! Input scalars:
!
! Local scalars:
      integer :: IJ,IPMab,IPMba,Mu,Nu
!
! Local function:
!
! Begin:
      call PRG_manager ('enter','BLD_S_ab_PV_ba', 'UTILITY')
!
      S_ab=ZERO
      PV_ba=ZERO
      do IPMab=1,NPab
        IJ=PM_exch_ab(IPMab)%IJ
        S_ab=S_ab+PM_exch_ab(IPMab)%Pxx*AOprod(IJ)
        PV_ba=PV_ba+PM_exch_ab(IPMab)%Pxx*Vpotl(IJ) ! note: PM_exch_ab= PM_exch_ba
      end do
!
! End of routine BLD_S_ab_PV_ba
      call PRG_manager ('exit', 'BLD_S_ab_PV_ba', 'UTILITY')
      return
      end subroutine BLD_S_ab_PV_ba
! END CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine GET_Kab_MO_point      
