      subroutine BLD_extra_mesh
!****************************************************************************************
!     Date last modified: October 24, 2024                                              *
!     Authors: JWH                                                                      *
!     Description: Calculate the extracule density on a mesh.                           *
!****************************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE module_grid_points
      USE type_plotting
      USE type_Weights

      implicit none
!
! Local scalars:
      integer :: Ipoint
      double precision :: X,Y,Z
      double precision :: extaa_pt,extHFaa_pt
      double precision :: extbb_pt,extHFbb_pt
      double precision :: extab_pt,extHFab_pt
      double precision :: extba_pt,extHFba_pt
!
! Local array:
      double precision, allocatable,dimension(:) :: extaa, extHFaa
      double precision, allocatable,dimension(:) :: extbb, extHFbb
      double precision, allocatable,dimension(:) :: extab, extHFab
      double precision, allocatable,dimension(:) :: extba, extHFba
!
! Begin:
      call PRG_manager ('enter', 'BLD_extra_mesh', 'UTILITY')
!
      call GET_object ('QM', '2RDM', 'AO')
!
      allocate (extaa(1:NGridPoints))
      allocate (extHFaa(1:NGridPoints))
      allocate (extbb(1:NGridPoints))
      allocate (extHFbb(1:NGridPoints))
!
      allocate (extab(1:NGridPoints))
      allocate (extHFab(1:NGridPoints))
      allocate (extba(1:NGridPoints))
      allocate (extHFba(1:NGridPoints))
!
      extaa = zero
      extHFaa = zero
!
      extbb = zero
      extHFbb = zero
!
      extab = zero
      extHFab = zero
!
      extba = zero
      extHFba = zero
!
      ! grid_points may of been destroyed between MENU and here, so rebuild mesh
      ! Really should always be done just before use
      if(LMesh)then
        call BLD_GRID_MESH
      else
        call BLD_GRID_RADIAL
      end if
!
      do Ipoint = 1, NGridPoints
!      
        X = grid_points(Ipoint)%x
        Y = grid_points(Ipoint)%y
        Z = grid_points(Ipoint)%z
!
! same-spin
        call GET_extra_ss_point (X, Y, Z, extaa_pt, extHFaa_pt, extbb_pt, extHFbb_pt)
!
        extaa(Ipoint) = extaa_pt
!
        extHFaa(Ipoint) = extHFaa_pt
!
        extbb(Ipoint) = extbb_pt
!
        extHFbb(Ipoint) = extHFbb_pt
!
! opposite-spin
        call GET_extra_os_point (X, Y, Z, extab_pt, extHFab_pt, extba_pt, extHFba_pt)
!
        extab(Ipoint) = extab_pt
!
        extHFab(Ipoint) = extHFab_pt
!
        extba(Ipoint) = extba_pt
!
        extHFba(Ipoint) = extHFba_pt
!
      end do ! Ipoint
!
! Print the atom in molecule ext density
!      if(AtomPrint)then
!        do Iatom=1,Natoms
!          Znum=CARTESIAN(Iatom)%Atomic_number
!          if(Znum.le.0)cycle
!          PlotFileID=' '
!          PlotFunction=' '
!          write(PlotFunction,'(a,i4)')'ext for atom ',Iatom
!          write(PlotFileID,'(a)')'ext_ATOMIC'
!          call BLD_plot_file_ATOMIC (PlotFileID, NGridPoints, Iatom, Plotfile_unit)
!          call PRT_MESH_ATOMIC (extTotal_atom, Iatom, Natoms, NGridPoints)
!          close (unit=Plotfile_unit)
!        end do ! Iatom
!      end if
!
! Print the ext density 
! same-spin
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'extra_aa_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular extracule density'
        call PRT_GRID_MOL (extaa, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular extracule density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (extaa, NGridPoints, 'F21.14')
      end if
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'extra_HF_aa_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular extracule density'
        call PRT_GRID_MOL (extHFaa, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular extracule density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (extHFaa, NGridPoints, 'F21.14')
      end if
!
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'extra_bb_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular extracule density'
        call PRT_GRID_MOL (extbb, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular extracule density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (extbb, NGridPoints, 'F21.14')
      end if
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'extra_HF_bb_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular extracule density'
        call PRT_GRID_MOL (extHFbb, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular extracule density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (extHFbb, NGridPoints, 'F21.14')
      end if
!
! opposite-spin
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'extra_ab_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular extracule density'
        call PRT_GRID_MOL (extab, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular extracule density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (extab, NGridPoints, 'F21.14')
      end if
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'extra_HF_ab_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular extracule density'
        call PRT_GRID_MOL (extHFab, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular extracule density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (extHFab, NGridPoints, 'F21.14')
      end if
!
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'extra_ba_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular extracule density'
        call PRT_GRID_MOL (extba, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular extracule density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (extba, NGridPoints, 'F21.14')
      end if
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'extra_HF_ba_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular extracule density'
        call PRT_GRID_MOL (extHFba, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular extracule density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (extHFba, NGridPoints, 'F21.14')
      end if
!
! End of routine BLD_extra_mesh
      call PRG_manager ('exit', 'BLD_extra_mesh', 'UTILITY')
      return
      end subroutine BLD_extra_mesh
