      subroutine SET_defaults_PLOT
!********************************************************************************************************
!     Date last modified: March 31, 2000                                                   Version 2.0  *
!     Author: R.A. Poirier                                                                              *
!     Description: Set code execution defaults.                                                         *
!********************************************************************************************************
! Modules:
      USE type_plotting

      implicit none
!
! Begin:
      PlotCutoff=1.0D-06
      Plotfile_unit=6
      LPlotNamed =.false.
      ATomPrint=.false.
      MOleculePrint=.true.
      GrdLenPrint =.false.
      PlotFunction='Density'
!
! Formats
      fmt_GridPts='f16.6'
      fmt_rho='f20.6'
      fmt_drho='f12.6'
      fmt_d2rho='f18.6'
      fmt_r2rho='f12.6'
      fmt_dr2rho='f10.6'
      fmt_d2r2rho='f18.6'

      Pfmt_rho='(3'//trim(fmt_GridPts)//','//trim(fmt_rho)//')'
      Pfmt1_rho='('//trim(fmt_GridPts)//','//trim(fmt_rho)//')'
      Pfmt2_rho='(2'//trim(fmt_GridPts)//','//trim(fmt_rho)//')'

      Pfmt_drho='(3'//trim(fmt_GridPts)//',3'//trim(fmt_drho)//')'

      Pfmt_d2rho='(3'//trim(fmt_GridPts)//',6'//trim(fmt_d2rho)//')'
      Pfmt1_d2rho='('//trim(fmt_GridPts)//',6'//trim(fmt_d2rho)//')'
      Pfmt2_d2rho='(2'//trim(fmt_GridPts)//',6'//trim(fmt_d2rho)//')'
      Pfmt_Eig='(3'//trim(fmt_GridPts)//',4'//trim(fmt_rho)//')'

      Pfmt_r2rho='(3'//trim(fmt_GridPts)//','//trim(fmt_r2rho)//')'
      Pfmt1_r2rho='('//trim(fmt_GridPts)//','//trim(fmt_r2rho)//')'
      Pfmt2_r2rho='(2'//trim(fmt_GridPts)//','//trim(fmt_r2rho)//')'

      Pfmt_dRad='(4'//trim(fmt_dr2rho)//'/)'
      Pfmt_d2Rad='(6'//trim(fmt_d2r2rho)//'/)'

!
! End of routine SET_defaults_PLOT
      return
      end subroutine SET_defaults_PLOT
