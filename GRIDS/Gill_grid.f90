      subroutine BLD_GILL_GRID
!**************************************************************************************
!     Date last modified:  Oct. 25th, 05                                              *
!     Author: Aisha                                                                   *
!     Description: Optimization of R_values used for MultiExp grids                   *
!**************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE type_elements
      USE type_radial_grids
      USE module_grid_points
      USE NI_defaults
      USE MultiExp_parameters
      USE N_integration

      implicit none

! Local scalars:
      integer :: Iatom,Ifound,Z_num,NR1,NR2,NA1,NA2,KRpoint
      integer :: NRpoint1,NRpoint2,NRpoint3 ! Number of radial points per region  
      integer :: NApoint1,NApoint2,NApoint3 ! Number of angular points per region  
      integer :: MAXpts
      logical :: LSkip ! true if second entry

! Local parameters:

! Begin:
      call PRG_manager ('enter', 'BLD_GILL_GRID', 'UTILITY')
      LSkip=.true.

      RADIAL_grid='GILL'

      call BLD_BeckeW_XI
      call BLD_grid_location ! Defines Nfound and LZfound

      NRpoints = NRpoints_Gill
      if(.not.allocated(R_value))then
        LSkip=.false. ! Must be first entry
        allocate (a_values(NRpoints), X_points(NRpoints), R_value(Nelements))
      select case (NRpoints)
      case(20)
        call R_values_Gill_N (Nelements, NRpoints)
        NRpoint1 = 6
        NRpoint2 = 6
        NRpoint3 = 8
      case(25)
        call R_values_Gill_N (Nelements, NRpoints)
        NRpoint1 = 6
        NRpoint2 = 8
        NRpoint3 = 11
      case(30)
        call R_values_Gill_N (Nelements, NRpoints)
        NRpoint1 = 6
        NRpoint2 = 10
        NRpoint3 = 14
      end select !NRpoints

      end if ! .not.allocated(R_value)

      if(MUN_prtlev.gt.0.or..not.LSkip)then
        write(UNIout,'(a,i3,a,3i4)')'The ',NRpoints,' radial points are divided into ',NRpoint1,NRpoint2,NRpoint3
        write(UNIout,'(a,3i4,a)')'with angular points ',NApoints_Gill(1),NApoints_Gill(2),NApoints_Gill(3),' respectively.'
        write(UNIout,'(a)')'The R_values are optimized for Li, Si, P, S, Cl'
      end if
 
      num_regions = 3
      if(.not.allocated(Nap_grid))then
        allocate(Nap_grid(Nfound,num_regions), NRpoints_region(Nfound,num_regions))
        allocate(Radial_points(Nfound,NRpoints),Radial_weights(Nfound,NRpoints))
      end if
      if(.not.allocated(NApoints_atom))then
        allocate(NApoints_atom(Nfound))
      end if

      Nap_grid(1:Nfound,1) = NApoints_Gill(1) 
      Nap_grid(1:Nfound,2) = NApoints_Gill(2)
      Nap_grid(1:Nfound,3) = NApoints_Gill(3)

      NApoint1 = NRpoint1*NApoints_Gill(1)
      NApoint2 = NRpoint2*NApoints_Gill(2)
      NApoint3 = NRpoint3*NApoints_Gill(3)
      MAXpts = NApoint1+NApoint2+NApoint3

      allocate(Egridpts(Nfound,MAXpts))
      do Z_num=1,Nelements
        if(.not.LZfound(Z_num))cycle
           Ifound=GRID_loc(Z_num)
        do KRpoint =1,NRpoints
          Radial_points(Ifound,KRpoint)=R_value(Z_num)*X_points(KRpoint)
          Radial_weights(Ifound,KRpoint)=a_values(KRpoint)*R_value(Z_num)**3
        end do !KRpoint
        NApoints_atom(Ifound) = MAXpts
        NApts_atom=MAXPts
        allocate(grid_points(NApts_atom))

        NRpoints_region(Ifound,1) = NRpoint1
        NRpoints_region(Ifound,2) = NRpoint2
        NRpoints_region(Ifound,3) = NRpoint3

        Rpoints(1:NRpoint1) = Radial_points(Ifound,1:NRpoint1)
        allocate(Apoints_temp(NApoint1))
        if(NApoints_Gill(1).eq.6)then
          call Angular_grid6 (Rpoints, NRpoint1, NApoint1, Apoints_temp)
        else if(NApoints_Gill(1).eq.86)then
          call Angular_grid86 (Rpoints, NRpoint1, NApoint1, Apoints_temp)
        else
          write(UNIout,'(a)')'Number of Angular point for region 1 not available'
          call PRG_stop ('Number of Angular point for region 1 not available')
        end if

        grid_points(1:NApoint1)= Apoints_temp(1:NApoint1)
        deallocate(Apoints_temp)

        NR1=NRpoint1+1
        NR2=NRpoint1+NRpoint2
        Rpoints(1:NRpoint2) = Radial_points(Ifound,NR1:NR2)
        allocate(Apoints_temp(NApoint2))
        if(NApoints_Gill(2).eq.86)then
          call Angular_grid86 (Rpoints, NRpoint2, NApoint2, Apoints_temp)
        else if(NApoints_Gill(2).eq.110)then
          call Angular_grid110 (Rpoints, NRpoint2, NApoint2, Apoints_temp)
        else
          write(UNIout,'(a)')'Number of Angular point for region 2 not available'
          call PRG_stop ('Number of Angular point for region 2 not available')
        end if

        NA1=NApoint1+1
        NA2=NApoint1+NApoint2
        grid_points(NA1:NA2) = Apoints_temp(1:NApoint2)
        deallocate(Apoints_temp)

        NR1=NR1+NRpoint2
        NR2=NR2+NRpoint3
        Rpoints(1:NRpoint3) = Radial_points(Ifound,NR1:NR2)
        allocate(Apoints_temp(NApoint3))
 
        if(NApoints_Gill(3).eq.110)then
          call Angular_grid110 (Rpoints, NRpoint3, NApoint3, Apoints_temp)
        else if(NApoints_Gill(3).eq.194)then
          call Angular_grid194 (Rpoints,NRpoint3,NApoint3,Apoints_temp)
        else if(NApoints_Gill(3).eq.302)then
          call Angular_grid302 (Rpoints,NRpoint3,NApoint3,Apoints_temp)
        else
          write(UNIout,'(a)')'Number of Angular point for region 3 not available'
          call PRG_stop ('Number of Angular point for region 3 not available')
        end if

        NA1=NA1+NApoint2
        NA2=NA2+NApoint3
        grid_points(NA1:NA2) = Apoints_temp(1:NApoint3)
        deallocate(Apoints_temp)

      call RAweights (Ifound)
      Egridpts(Ifound,1:NApts_atom)= grid_points(1:NApts_atom)

      deallocate(grid_points)

      end do ! Z_num

!     deallocate(Radial_points,Radial_weights,NRpoints_region,Nap_grid)       
!     deallocate(a_values,X_points)
! do NOT Deallocate R_value in case optimizing
      call PRG_manager ('exit', 'BLD_GILL_GRID', 'UTILITY')
      return
      end subroutine BLD_GILL_GRID
      subroutine R_values_Gill_J (Nmax)
!**************************************************************************************
!     Date last modified: October 16, 2007                                            *
!     Author: R.A. Poirier                                                            *
!     Description: R_values used for MultiExp grids                                   *
!**************************************************************************************
! Modules:
      USE program_files
      USE MultiExp_parameters

      integer :: Nmax
!
! R_values optimized w.r.t Coulomb:
      write(UNIout,'(a)')'Using R values optimized for Coulomb'
      select case (NRpoints)
      case(20)
        a_values(1:NRpoints)=a_values_20(1:NRpoints)
        X_points(1:NRpoints)=X_points_20(1:NRpoints)
        R_value(1) = 1.0000
        R_value(2) = 0.5882
        R_value(3) = 1.7000
        R_value(4) = 2.0513
        R_value(5) = 1.5385
        R_value(6) = 1.2330
        R_value(7) = 1.0420
        R_value(8) = 0.7560
        R_value(9) = 0.6940
        R_value(10) = 0.6838
        R_value(11) = 2.0080
        R_value(12) = 1.8510
        R_value(13) = 2.5714
        R_value(14) = 1.5790
        R_value(15) = 1.4450
        R_value(16) = 1.3540
        R_value(17) = 1.2650
        R_value(18) = 1.3333
        R_value(19) = 0.0D0
        R_value(20) = 0.0D0
        R_value(21) = 0.0D0
        R_value(22) = 0.0D0
        R_value(23) = 0.0D0
        R_value(24) = 0.0D0
        R_value(25) = 0.0D0
        R_value(26) = 0.0D0
        R_value(27) = 0.0D0
        R_value(28) = 0.0D0
        R_value(29) = 0.7070D0  !Cu
        R_value(30) = 0.7010D0  !Zn
        R_value(32) = 0.7390D0  !Ge
        R_value(33) = 0.7660D0  !As
        R_value(34) = 0.7500D0  !Se
        R_value(35) = 0.7400D0  !Br
        R_value(51) = 0.9920D0  !Sb
        R_value(52) = 0.9920D0  !Te
        R_value(53) = 0.800D0   !I
     case(25)
        a_values(1:NRpoints)=a_values_25(1:NRpoints)
        X_points(1:NRpoints)=X_points_25(1:NRpoints)
        R_value(1) = 1.3390
        R_value(2) = 0.5882
        R_value(3) = 2.4080
        R_value(4) = 2.0513
        R_value(5) = 1.5385
        R_value(6) = 1.2308
        R_value(7) = 1.6720
        R_value(8) = 0.8791
        R_value(9) = 1.0260
        R_value(10) = 1.0260
        R_value(11) = 2.1690
        R_value(12) = 1.9980
        R_value(13) = 2.5714
        R_value(14) = 1.7060
        R_value(15) = 1.5560
        R_value(16) = 1.4580
        R_value(17) = 1.3650
        R_value(18) = 1.3333
        R_value(19) = 0.0D0
        R_value(20) = 0.0D0
        R_value(21) = 0.0D0
        R_value(22) = 0.0D0
        R_value(23) = 0.0D0
        R_value(24) = 0.0D0
        R_value(25) = 0.0D0
        R_value(26) = 0.0D0
        R_value(27) = 0.0D0
        R_value(28) = 0.0D0
        R_value(29) = 0.9980D0  !Cu
        R_value(30) = 0.8050D0  !Zn
        R_value(32) = 1.1460000 !Ge
        R_value(33) = 1.1130D0  !As
        R_value(34) = 1.0730D0  !Se
        R_value(35) = 1.0350D0  !Br
        R_value(51) = 0.9920D0  !Sb
        R_value(52) = 1.0320D0  !Te
        R_value(53) = 1.0100D0  !I acc =6.15487319 or 0.7590000 acc=6.01670761
      case(30)
        a_values(1:NRpoints)=a_values_30(1:NRpoints)
        X_points(1:NRpoints)=X_points_30(1:NRpoints)
        R_value(1) = 1.0000
        R_value(2) = 0.5882
        R_value(3) = 3.3540
        R_value(4) = 2.0513
        R_value(5) = 1.5385
        R_value(6) = 1.2308
        R_value(7) = 1.0256
        R_value(8) = 0.8791
        R_value(9) = 0.7692
        R_value(10) = 0.6838
        R_value(11) = 4.0909
        R_value(12) = 3.1579
        R_value(13) = 2.5714
        R_value(14) = 1.6420
        R_value(15) = 1.4670
        R_value(16) = 1.3970
        R_value(17) = 1.3220
        R_value(18) = 1.3333
        R_value(19) = 0.0D0
        R_value(20) = 0.0D0
        R_value(21) = 0.0D0
        R_value(22) = 0.0D0
        R_value(23) = 0.0D0
        R_value(24) = 0.0D0
        R_value(25) = 0.0D0
        R_value(26) = 0.0D0
        R_value(27) = 0.0D0
        R_value(28) = 0.0D0
        R_value(29) = 0.8980D0 !Cu
        R_value(30) = 1.0970D0 !Zn
        R_value(32) = 1.6270D0 !Ge or 0.9840000
        R_value(33) = 1.581000 !AS or 0.9110D0  
        R_value(34) = 1.5270D0 !Se the smallest R_value
        R_value(35) = 1.4780D0 !Br
        R_value(51) = 0.9920D0  !Sb
        R_value(52) = 1.4720D0 !Te
        R_value(53) = 1.4440D0 !I or 0.9770000
      end select !NRpoints

      return
      end subroutine R_values_Gill_J
      subroutine R_values_Gill_N (Nmax, NRpoints)
!**************************************************************************************
!     Date last modified: October 16, 2007                                            *
!     Author: R.A. Poirier                                                            *
!     Description: R_values used for MultiExp grids                                   *
!**************************************************************************************
! Modules:
      USE program_files
      USE MultiExp_parameters

      integer :: Nmax,NRpoints
!
! R_values optimized w.r.t Number of electrons
      write(UNIout,'(a)')'Using R values optimized for number of electrons'
      select case (NRpoints)
      case(20)
        a_values(1:NRpoints)=a_values_20(1:NRpoints)
        X_points(1:NRpoints)=X_points_20(1:NRpoints)
        R_value(1) = 1.0000
        R_value(2) = 0.5882
        R_value(3) = 3.0769
        R_value(4) = 2.0513
        R_value(5) = 1.5385
        R_value(6) = 1.2308
        R_value(7) = 1.0256
        R_value(8) = 0.8791
        R_value(9) = 0.7692
        R_value(10) = 0.6838
        R_value(11) = 4.0909
        R_value(12) = 3.1579
        R_value(13) = 2.5714
        R_value(14) = 1.5877
        R_value(15) = 1.4500
        R_value(16) = 1.3600
        R_value(17) = 1.2730
        R_value(18) = 1.3333
        R_value(19) = 0.0D0
        R_value(20) = 0.0D0
        R_value(21) = 0.0D0
        R_value(22) = 0.0D0
        R_value(23) = 0.0D0
        R_value(24) = 0.0D0
        R_value(25) = 0.0D0
        R_value(26) = 0.0D0
        R_value(27) = 0.0D0
        R_value(28) = 0.0D0
        R_value(29) = 0.7070D0  !Cu
        R_value(30) = 0.7010D0  !Zn
        R_value(32) = 0.7390D0  !Ge
        R_value(33) = 0.7660D0  !As
        R_value(34) = 0.7500D0  !Se
        R_value(35) = 0.7400D0  !Br
        R_value(51) = 0.9920D0  !Sb
        R_value(52) = 0.9920D0  !Te
        R_value(53) = 0.800D0   !I
      case(25)
        a_values(1:NRpoints)=a_values_25(1:NRpoints)
        X_points(1:NRpoints)=X_points_25(1:NRpoints)
        R_value(1) = 1.0000
        R_value(2) = 0.5882
        R_value(3) = 2.9770
        R_value(4) = 2.0513
        R_value(5) = 1.5385
        R_value(6) = 1.2308
        R_value(7) = 1.0256
        R_value(8) = 0.8791
        R_value(9) = 0.7360
        R_value(10) = 0.6838
        R_value(11) = 4.0909
        R_value(12) = 3.1579
        R_value(13) = 2.5714
        R_value(14) = 1.7260
        R_value(15) = 1.5930
        R_value(16) = 1.4930
        R_value(17) = 1.3840
        R_value(18) = 1.3333
        R_value(19) = 0.0D0
        R_value(20) = 0.0D0
        R_value(21) = 0.0D0
        R_value(22) = 0.0D0
        R_value(23) = 0.0D0
        R_value(24) = 0.0D0
        R_value(25) = 0.0D0
        R_value(26) = 0.0D0
        R_value(27) = 0.0D0
        R_value(28) = 0.0D0
        R_value(29) = 0.9980D0  !Cu
        R_value(30) = 0.8050D0  !Zn
        R_value(32) = 1.1460000 !Ge
        R_value(33) = 1.1130D0  !As
        R_value(34) = 1.0730D0  !Se
        R_value(35) = 1.0350D0  !Br
        R_value(51) = 0.9920D0  !Sb
        R_value(52) = 1.0320D0  !Te
        R_value(53) = 1.0100D0  !I acc =6.15487319 or 0.7590000 acc=6.01670761
      case(30)
        a_values(1:NRpoints)=a_values_30(1:NRpoints)
        X_points(1:NRpoints)=X_points_30(1:NRpoints)
        R_value(1) = 0.9995
!       R_value(1) = 1.0000
        R_value(2) = 0.5882
        R_value(3) = 3.3540
        R_value(4) = 2.0513
        R_value(5) = 1.5385
        R_value(6) = 1.2376
!       R_value(6) = 1.2308
        R_value(7) = 1.0256
        R_value(8) = 0.98093
!       R_value(8) = 0.8791
!       R_value(9) = 0.7692
        R_value(9) = 0.7750
        R_value(10) = 0.6838
        R_value(11) = 4.0909
        R_value(12) = 3.1579
        R_value(13) = 2.5714
        R_value(14) = 1.6420
        R_value(15) = 1.4670
        R_value(16) = 1.4038
!       R_value(16) = 1.3970
        R_value(17) = 1.0000
!       R_value(17) = 1.3220
        R_value(18) = 1.3333
        R_value(19) = 0.0D0
        R_value(20) = 0.0D0
        R_value(21) = 0.0D0
        R_value(22) = 0.0D0
        R_value(23) = 0.0D0
        R_value(24) = 0.0D0
        R_value(25) = 0.0D0
        R_value(26) = 0.0D0
        R_value(27) = 0.0D0
        R_value(28) = 0.0D0
        R_value(29) = 0.8980D0 !Cu
        R_value(30) = 1.0970D0 !Zn
        R_value(32) = 1.6270D0 !Ge or 0.9840000
        R_value(33) = 1.581000 !AS or 0.9110D0  
        R_value(34) = 1.5270D0 !Se the smallest R_value
        R_value(35) = 1.4780D0 !Br
        R_value(51) = 0.9920D0  !Sb
        R_value(52) = 1.4720D0 !Te
        R_value(53) = 1.4440D0 !I or 0.9770000
      end select !NRpoints

      return
      end subroutine R_values_Gill_N
