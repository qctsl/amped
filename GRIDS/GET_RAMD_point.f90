      subroutine GET_RAMDss_point (Xpt, Ypt, Zpt, RAMaa_grid, RAMHFaa_grid, RAMbb_grid, RAMHFbb_grid)
!*******************************************************************************
!     Date last modified: April 24 2019                                        *
!     Authors: JWH                                                             *
!     Description: Compute the momentum-balance density at a point.            *
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE INT_objects

      implicit none
!
! Input scalars:
      double precision, intent(IN) :: Xpt,Ypt,Zpt
!
! Output scalars:
      double precision, intent(OUT) :: RAMaa_grid,RAMHFaa_grid
      double precision, intent(OUT) :: RAMbb_grid,RAMHFbb_grid
!
! Local scalars:
      double precision :: rv(3)
!           
! Begin:
!
      call GET_object ('QM', '2RDM', 'AO')
!    
      RAMaa_grid=ZERO
      RAMHFaa_grid=ZERO        
!
      RAMbb_grid=ZERO
      RAMHFbb_grid=ZERO        
!
      rv(1) = Xpt
      rv(2) = Ypt
      rv(3) = Zpt
!
      call BLD_RAMDint (rv)
!
      RAMaa_grid = dot_product(TRDM_AOaa,ramdAO)
      RAMHFaa_grid = dot_product(TRDM_HF_AOaa,ramdAO)
!
      RAMbb_grid = dot_product(TRDM_AObb,ramdAO)
      RAMHFbb_grid = dot_product(TRDM_HF_AObb,ramdAO)
!
      return
      end subroutine GET_RAMDss_point 
      subroutine GET_RAMDos_point (Xpt, Ypt, Zpt, RAMab_grid, RAMHFab_grid, RAMba_grid, RAMHFba_grid)
!*******************************************************************************
!     Date last modified: April 24 2019                                        *
!     Authors: JWH                                                             *
!     Description: Compute the momentum-balance density at a point.            *
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE INT_objects

      implicit none
!
! Input scalars:
      double precision, intent(IN) :: Xpt,Ypt,Zpt
!
! Output scalars:
      double precision, intent(OUT) :: RAMab_grid,RAMHFab_grid
      double precision, intent(OUT) :: RAMba_grid,RAMHFba_grid
!
! Local scalars:
      double precision :: rv(3)
!           
! Begin:
!
      call GET_object ('QM', '2RDM', 'AO')
!    
      RAMab_grid=ZERO
      RAMHFab_grid=ZERO        
!
      RAMba_grid=ZERO
      RAMHFba_grid=ZERO        
!
      rv(1) = Xpt
      rv(2) = Ypt
      rv(3) = Zpt
!
      call BLD_RAMDint (rv)
!
      RAMab_grid = dot_product(TRDM_AOab,ramdAO)
      RAMHFab_grid = dot_product(TRDM_HF_AOab,ramdAO)
!
      RAMba_grid = dot_product(TRDM_AOba,ramdAO)
      RAMHFba_grid = dot_product(TRDM_HF_AOba,ramdAO)
!
      return
      end subroutine GET_RAMDos_point 
