      subroutine GET_TDF_Vee_OS_point (Rv, rf, Nact, tdf)
!*******************************************************************************
!     Date last modified: October 21, 2021                                     *
!     Authors: JWH                                                             *
!     Description: Compute the opposite-spin two-electron-density functional   *
!                  value at the given point. (e-e repulsion component)         * 
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE INT_objects

      implicit none
!
! Input scalars:
      double precision, intent(IN) :: rf
      integer, intent(IN) :: Nact
!
! Input arrays:
      double precision, intent(IN) :: Rv(3)
!
! Output scalars:
      double precision, intent(OUT) :: tdf
!
! Local scalars:
      double precision :: rho, rhoa, rhob, rhoHFa, rhoHFb
      double precision :: lambda, u_scale, intg, wts, g, cusp
      double precision :: R2RDMab_grid, R2RDMba_grid, q, u
      integer :: I_ux, I_uy, I_uz, N_u_pts
!
! Local arrays:
      double precision :: umag(3), uv(3), r1v(3), r2v(3)
      double precision, allocatable, dimension(:) :: u_pts, u_wts
!           
! Begin:
!
      q = q_TDF
!
! Could have a get 2RDM here, but then it wouldn't be passed in
!    
      tdf = zero
!
      call GET_spin_den_point (Rv(1), Rv(2), Rv(3), rhoa, rhoHFa, rhob, rhoHFb)
!
      rho = rhoa + rhob
!
      if(rho.gt.1.0D-12)then
!
      lambda = q*rho**F1_3
!
! grid parameters (use density at R to determine these)          
      N_u_pts = 6
      !u_scale = 2.0D0
      u_scale = max(min(4.6D0/lambda - 0.3D0, 2.0D0),1.0D0)
      allocate(u_pts(N_u_pts), u_wts(N_u_pts))
      call BLD_MK_grid(N_u_pts, u_scale, u_pts, u_wts)
!
! Integrate over u
      do I_ux = 1, N_u_pts
        umag(1) = u_pts(I_ux)
        do I_uy = 1, N_u_pts
          umag(2) = u_pts(I_uy)
          do I_uz = 1, N_u_pts
            umag(3) = u_pts(I_uz)
!
            intg = 0.0D0
!
            wts = u_wts(I_ux)*u_wts(I_uy)*u_wts(I_uz)
!
            u = dsqrt(dot_product(umag,umag))
!
            g      = dexp(-(lambda*u)**2)
            cusp   = dexp(u)
!
! (+,+,+)
            uv = umag
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_ab_point (r1v, r2v, Nact, R2RDMab_grid, R2RDMba_grid)
!
            intg = R2RDMab_grid + R2RDMba_grid
!
! (+,+,-)
            uv(3) = -umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_ab_point (r1v, r2v, Nact, R2RDMab_grid, R2RDMba_grid)
!
            intg = intg + R2RDMab_grid + R2RDMba_grid
!
! (+,-,+)
            uv(1) =  umag(1)
            uv(2) = -umag(2)
            uv(3) =  umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_ab_point (r1v, r2v, Nact, R2RDMab_grid, R2RDMba_grid)
!
            intg = intg + R2RDMab_grid + R2RDMba_grid
!
! (-,+,+)
            uv(1) = -umag(1)
            uv(2) =  umag(2)
            uv(3) =  umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_ab_point (r1v, r2v, Nact, R2RDMab_grid, R2RDMba_grid)
!
            intg = intg + R2RDMab_grid + R2RDMba_grid
!
! (+,-,-)
            uv(1) =  umag(1)
            uv(2) = -umag(2)
            uv(3) = -umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_ab_point (r1v, r2v, Nact, R2RDMab_grid, R2RDMba_grid)
!
            intg = intg + R2RDMab_grid + R2RDMba_grid
!
! (-,+,-)
            uv(1) = -umag(1)
            uv(2) =  umag(2)
            uv(3) = -umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_ab_point (r1v, r2v, Nact, R2RDMab_grid, R2RDMba_grid)
!
            intg = intg + R2RDMab_grid + R2RDMba_grid
!
! (-,-,+)
            uv(1) = -umag(1)
            uv(2) = -umag(2)
            uv(3) =  umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_ab_point (r1v, r2v, Nact, R2RDMab_grid, R2RDMba_grid)
!
            intg = intg + R2RDMab_grid + R2RDMba_grid
!
! (-,-,-)
            uv = -umag
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_ab_point (r1v, r2v, Nact, R2RDMab_grid, R2RDMba_grid)
!
            intg = intg + R2RDMab_grid + R2RDMba_grid
!
            ! 2e cusp energy
            tdf = tdf + wts*intg*g*(rf*cusp - 1.0D0)/u
            ! 2e energy
            !tdf = tdf + wts*intg/u
            ! electron pairs
            !tdf = tdf + wts*intg
!
          end do ! I_uz
        end do ! I_uy
      end do ! I_ux
!
      end if ! rho > tol
!
      end subroutine GET_TDF_Vee_OS_point
      subroutine GET_TDFapp_Vee_OS_point (Rv, Nact, tdf)
!*******************************************************************************
!     Date last modified: October 21, 2021                                     *
!     Authors: JWH                                                             *
!     Description: Compute the taylor-expansion approximate opposite-spin      *
!                  two-electron density functional value at the given point.   * 
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE INT_objects

      implicit none
!
! Input scalars:
      integer, intent(IN) :: Nact
!
! Input arrays:
      double precision, intent(IN) :: Rv(3)
!
! Output scalars:
      double precision, intent(OUT) :: tdf
!
! Local scalars:
      double precision :: rho, rhoa, rhob, rhoHFa, rhoHFb, pi
      double precision :: lam, lam2, erfo, expo, sqrtpi
      double precision :: R2RDMab_grid, R2RDMba_grid, q, G
!
! Functions:
      double precision, external :: derf
!           
! Begin:
      pi = PI_VAL
      sqrtpi = dsqrt(pi)
!
      q = q_TDF
!
! Could have a get 2RDM here, but then it wouldn't be passed in
!    
!
      call GET_spin_den_point (Rv(1), Rv(2), Rv(3), rhoa, rhoHFa, rhob, rhoHFb)
!
      rho  = rhoa + rhob
!
      tdf = zero
!
      if(rho.gt.1.0D-10)then
        lam  = q*rho**F1_3
        lam2 = lam**2
!
        expo = dexp(-0.25D0/lam2)
        erfo = one + derf(0.5D0/lam)
!
        call GET_R2RDM_ab_point (Rv, Rv, Nact, R2RDMab_grid, R2RDMba_grid)
!
        G = R2RDMab_grid + R2RDMba_grid
!
        tdf = G*two*pi*(two*lam*(sqrtpi*lam - one)*expo + sqrtpi*(sqrtpi*lam - one&
            - two*lam2)*erfo)/(lam2*(two*lam*expo&
            + sqrtpi*(one + two*lam2)*erfo))
!
      end if
!
      end subroutine GET_TDFapp_Vee_OS_point
