      subroutine BLD_Velocities_GRID (Modality)
!****************************************************************************************
!     Date last modified: February 22, 2017                                             *
!     Authors: R.A. Poirier                                                             *
!     Description: Calculate velocities on a set of grid points                         *
!****************************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE module_grid_points
      USE type_plotting

      implicit none
!
! Input scalars:
      character(*) :: Modality
!
! Local scalars:
      integer :: Iatom,Ipoint,IRegion,Znum,Ifound,NRpoints
      double precision :: XApt,YApt,ZApt,WApt,Bweight
      double precision :: Vx,Vy,Vz,TVx,TVy,TVz
      double precision :: r_val,StepSize
      character(len=32) :: Pfmt3
! Local array:
      double precision, dimension(:), allocatable  :: Regions
      double precision, dimension(:), allocatable  :: VxbyR,VybyR,VzbyR
      integer, dimension(:), allocatable  :: NRbyR
!
! Begin:
      call PRG_manager ('enter', 'BLD_VELOCITIES_GRID', 'UTILITY')
!
      RADIAL_grid=Modality(1:len_trim(Modality))
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)
      call BLD_BeckeW_XI

      StepSize=MAX_r/NRegions ! Need larger regions to avoid zero's
      allocate(Regions(NRegions))
      do IRegion=1,NRegions
        Regions(IRegion)=dble(IRegion)*StepSize
      end do ! IRegions
!
! Get a temporary file to store plotting data	  
      Pfmt3='(3'//trim(fmt_GridPts)//','//trim(fmt_r2rho)//')'
      PlotFileID=' '
      PlotFunction=' '
      write(PlotFunction,'(a)')'VELOCITIES '
      write(PlotFileID,'(a)')'VELOCITIES_MOLECULE'
!
      TVx=ZERO
      TVy=ZERO
      TVz=ZERO
!
      do Iatom=1,Natoms
      Znum=CARTESIAN(Iatom)%Atomic_number
      if(Znum.le.0)cycle
      Ifound=GRID_loc(Znum)
      NApts_atom=NApoints_atom(Ifound)
      allocate(NRbyR(NRegions))
      allocate(VxbyR(NRegions))
      allocate(VybyR(NRegions))
      allocate(VzbyR(NRegions))
      NRbyR=ZERO
      VxbyR=ZERO
      VybyR=ZERO
      VzbyR=ZERO
      VzbyR=ZERO
      if(MoleculePrint.or.AtomPrint)then
        call BLD_plot_file_MOL ('VELOCITIES_GRID', NApts_atom, Plotfile_unit)
      else
        Plotfile_unit=UNIout
      end if
!
! Moving grid points on cartesian coordinate grid
      do Ipoint=1,NApts_atom
        XApt=Egridpts(Ifound,Ipoint)%X
        YApt=Egridpts(Ifound,Ipoint)%Y
        ZApt=Egridpts(Ifound,Ipoint)%Z
        r_val=dsqrt(XApt**2+YApt**2+ZApt**2)
        XApt=XApt+CARTESIAN(Iatom)%X
        YApt=YApt+CARTESIAN(Iatom)%Y
        ZApt=ZApt+CARTESIAN(Iatom)%Z
        WApt=Egridpts(Ifound,Ipoint)%w
        call BeckeW1 (XApt, YApt, ZApt, Iatom, Bweight)
        call Velocities_GRID_point

        TVx=TVx+Vx
        TVy=TVy+Vy
        TVz=TVz+Vz
        do IRegion=1,NRegions
        if(r_val.le.Regions(IRegion))then
          NRbyR(IRegion)=NRbyR(IRegion)+1
          VxbyR(IRegion)=VxbyR(IRegion)+Vx
          VybyR(IRegion)=VybyR(IRegion)+Vy
          VzbyR(IRegion)=VzbyR(IRegion)+Vz
          exit
        else 
        end if
        end do ! IRegion
        if(r_val.gt.Regions(NRegions))then
          NRbyR(NRegions)=NRbyR(NRegions)+1
          VxbyR(NRegions)=VxbyR(NRegions)+Vx
          VybyR(NRegions)=VybyR(NRegions)+Vy
          VzbyR(NRegions)=VzbyR(NRegions)+Vz
        end if
      end do ! Ipoint
      write(Plotfile_unit,*)'Iatom: ',Iatom
      write(Plotfile_unit,'(a)')'    Iregion  Region     Npoints   Vx          Vy          Vz'
        call flush(Plotfile_unit)
      NRpoints=0
      do IRegion=1,NRegions
        NRpoints=NRpoints+NRbyR(IRegion)
        VxbyR(IRegion)=FourPi*VxbyR(IRegion)
        VybyR(IRegion)=FourPi*VybyR(IRegion)
        VzbyR(IRegion)=FourPi*VzbyR(IRegion)
        write(Plotfile_unit,'(i8,f12.6,i8,3f12.6)')IRegion,Regions(IRegion),NRbyR(IRegion), &
              VxbyR(IRegion),VybyR(IRegion),VzbyR(IRegion)
        call flush(Plotfile_unit)
      end do
! Deallocate
      deallocate(NRbyR)
      deallocate(VxbyR)
      deallocate(VybyR)
      deallocate(VzbyR)
      end do ! Iatom
      deallocate(Regions)
!
      TVx=TVx*FourPi
      TVy=TVy*FourPi
      TVz=TVz*FourPi
      write(UNIout,'(a,f14.6)')'TVx: ',TVx
      write(UNIout,'(a,f14.6)')'TVy: ',TVy
      write(UNIout,'(a,f14.6)')'TVz: ',TVz
      call flush(Plotfile_unit)
!
      if(MoleculePrint.or.AtomPrint)close (unit=Plotfile_unit)
!
! End of routine BLD_Velocities_GRID
      call PRG_manager ('exit', 'BLD_VELOCITIES_GRID', 'UTILITY')
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine Velocities_GRID_point
!****************************************************************************************
!     Date last modified: February 22, 2017                                             *
!     Authors: R.A. Poirier                                                             *
!     Description: Calculate the Velocities at a point (Xpt, Ypt, Zpt)                  *
!****************************************************************************************
! Modules:

      implicit none

! Input scalars:
!
! Output scalars:
!
! Local scalars:
!
! Local functions:
      double precision :: TraceAB
      double precision, dimension(:), allocatable  :: dAOprodX,dAOprodY,dAOprodZ
!
! Begin: 
      allocate (dAOprodX(1:MATlen))
      allocate (dAOprodY(1:MATlen))
      allocate (dAOprodZ(1:MATlen))
      call Velocity_Gaussian_prod (XApt, YApt, ZApt, dAOprodX, dAOprodY, dAOprodZ, MATlen)
      Vx=TraceAB (PM0, dAOprodX, NBasis)
      Vx=Vx*Bweight*WApt
      Vy=TraceAB (PM0, dAOprodY, NBasis)
      Vy=Vy*Bweight*WApt
      Vz=TraceAB (PM0, dAOprodZ, NBasis)
      Vz=Vz*Bweight*WApt
      deallocate (dAOprodX)
      deallocate (dAOprodY)
      deallocate (dAOprodZ)
!
! End of routine Velocities_GRID_point
      return
      end subroutine Velocities_GRID_point

! END CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine BLD_Velocities_GRID
