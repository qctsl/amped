      subroutine BLD_Exchange_GRID (Modality)
!****************************************************************************************
!     Date last modified: February 22, 2017                                             *
!     Authors: R.A. Poirier                                                             *
!     Description: Calculate the Exchange energy on a set of grid points                *
!****************************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE module_grid_points
      USE type_plotting

      implicit none
!
! Input scalars:
      character(*) :: Modality
!
! Local scalars:
      integer :: Iatom,Ipoint,IRegion,Znum,Ifound,NRpoints
      double precision :: XApt,YApt,ZApt,WApt,Bweight
      double precision :: Vpotl,TotalK
      double precision :: KgridA,KbyA
      double precision :: r_val,StepSize
      character(len=32) :: Pfmt3
! Local array:
      double precision, dimension(:), allocatable  :: Regions
      double precision, dimension(:), allocatable  :: KbyR
      integer, dimension(:), allocatable  :: NRbyR
!
! Begin:
      call PRG_manager ('enter', 'BLD_EXCHANGE_GRID', 'UTILITY')
!
      RADIAL_grid=Modality(1:len_trim(Modality))
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)
      call BLD_BeckeW_XI

      StepSize=MAX_r/NRegions ! Need larger regions to avoid zero's
      allocate(Regions(NRegions))
      do IRegion=1,NRegions
        Regions(IRegion)=dble(IRegion)*StepSize
      end do ! IRegions
!
! Get a temporary file to store plotting data	  
      Pfmt3='(3'//trim(fmt_GridPts)//','//trim(fmt_r2rho)//')'
      PlotFileID=' '
      PlotFunction=' '
      write(PlotFunction,'(a)')'EXCHANGE_ENERGY '
      write(PlotFileID,'(a)')'EXCHANGE_MOLECULE'
!
      TotalK=ZERO
!
      do Iatom=1,Natoms
      Znum=CARTESIAN(Iatom)%Atomic_number
      if(Znum.le.0)cycle
      Ifound=GRID_loc(Znum)
      NApts_atom=NApoints_atom(Ifound)
      KbyA=ZERO
      allocate(KbyR(NRegions))
      allocate(NRbyR(NRegions))
      KbyR=ZERO
      NRbyR=ZERO
      if(MoleculePrint.or.AtomPrint)then
        call BLD_plot_file_MOL ('EXCHANGE_GRID', NApts_atom, Plotfile_unit)
      else
        Plotfile_unit=UNIout
      end if
!
! Moving grid points on cartesian coordinate grid
      do Ipoint=1,NApts_atom
        XApt=Egridpts(Ifound,Ipoint)%X
        YApt=Egridpts(Ifound,Ipoint)%Y
        ZApt=Egridpts(Ifound,Ipoint)%Z
        r_val=dsqrt(XApt**2+YApt**2+ZApt**2)
        XApt=XApt+CARTESIAN(Iatom)%X
        YApt=YApt+CARTESIAN(Iatom)%Y
        ZApt=ZApt+CARTESIAN(Iatom)%Z
        WApt=Egridpts(Ifound,Ipoint)%w
        call BeckeW1 (XApt, YApt, ZApt, Iatom, Bweight)
        call GET_Exchange_MO_point (XApt, YApt, ZApt, KgridA)
        KgridA=KgridA*Bweight*WApt
        TotalK=TotalK+KgridA
        do IRegion=1,NRegions
        if(r_val.le.Regions(IRegion))then
          KbyR(IRegion)=KbyR(IRegion)+KgridA
          NRbyR(IRegion)=NRbyR(IRegion)+1
          exit
        else 
        end if
        end do ! IRegion
        if(r_val.gt.Regions(NRegions))then
          KbyR(NRegions)=KbyR(NRegions)+KgridA
          NRbyR(NRegions)=NRbyR(NRegions)+1
        end if
      end do ! Ipoint
      write(Plotfile_unit,*)'Iatom: ',Iatom
      write(Plotfile_unit,'(a)')'    Iregion  Region     Npoints   KgridA'
        call flush(Plotfile_unit)
      NRpoints=0
      do IRegion=1,NRegions
        KbyR(IRegion)=FourPi*KbyR(IRegion)
        KbyA=KbyA+KbyR(IRegion)
        NRpoints=NRpoints+NRbyR(IRegion)
        if(NRbyR(IRegion).eq.0)cycle
        write(Plotfile_unit,'(i8,f12.6,i8,f12.6)')IRegion,Regions(IRegion),NRbyR(IRegion),KbyR(IRegion)
        call flush(Plotfile_unit)
      end do
! Deallocate
      deallocate(KbyR)
      deallocate(NRbyR)
      KbyA=PT5*KbyA
      write(Plotfile_unit,'(a,i8)')'NApts_atom: ',NApts_atom
      write(Plotfile_unit,'(a)')'    Atom  Npoints     KgridA'
      write(Plotfile_unit,'(2i8,f12.6)')Iatom,NRpoints,KbyA
      call flush(Plotfile_unit)
      end do ! Iatom
      deallocate(Regions)
!
      TotalK=TotalK*FourPi
      write(UNIout,'(a,f12.6)')'TotalK: ',TotalK
      call flush(Plotfile_unit)
!
      if(MoleculePrint.or.AtomPrint)close (unit=Plotfile_unit)
!
! End of routine BLD_EXCHANGE_GRID
      call PRG_manager ('exit', 'BLD_EXCHANGE_GRID', 'UTILITY')
      return
      end subroutine BLD_Exchange_GRID
