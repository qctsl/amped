      subroutine BLD_Coulomb_GRID (Modality)
!****************************************************************************************
!     Date last modified: February 22, 2017                                             *
!     Authors: R.A. Poirier                                                             *
!     Description: Calculate the Coulomb energy on a set of grid points                 *
!****************************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE module_grid_points
      USE type_plotting

      implicit none
!
! Input scalars:
      character(*) :: Modality
!
! Local scalars:
      integer :: Iatom,Ipoint,IRegion,Znum,Ifound,NRpoints
      double precision :: XApt,YApt,ZApt,WApt,Bweight
      double precision :: Vpotl,TVpotl
      double precision :: VpotlbyA
      double precision :: r_val,StepSize
      double precision :: rho
      character(len=32) :: Pfmt3
! Local array:
      double precision, dimension(:), allocatable  :: Regions
      double precision, dimension(:), allocatable  :: VpotlbyR
      integer, dimension(:), allocatable  :: NRbyR
!
! Begin:
      call PRG_manager ('enter', 'BLD_COULOMB_GRID', 'UTILITY')
!
      RADIAL_grid=Modality(1:len_trim(Modality))
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)
      call BLD_BeckeW_XI

      StepSize=MAX_r/NRegions ! Need larger regions to avoid zero's
      allocate(Regions(NRegions))
      do IRegion=1,NRegions
        Regions(IRegion)=dble(IRegion)*StepSize
      end do ! IRegions
!
! Get a temporary file to store plotting data	  
      Pfmt3='(3'//trim(fmt_GridPts)//','//trim(fmt_r2rho)//')'
      PlotFileID=' '
      PlotFunction=' '
      write(PlotFunction,'(a)')'COULOMB_ENERGY '
      write(PlotFileID,'(a)')'COULOMB_MOLECULE'
!
      TVpotl=ZERO
!
      do Iatom=1,Natoms
      Znum=CARTESIAN(Iatom)%Atomic_number
      if(Znum.le.0)cycle
      Ifound=GRID_loc(Znum)
      NApts_atom=NApoints_atom(Ifound)
      VpotlbyA=ZERO
      allocate(VpotlbyR(NRegions))
      allocate(NRbyR(NRegions))
      VpotlbyR=ZERO
      NRbyR=ZERO
      if(MoleculePrint.or.AtomPrint)then
        call BLD_plot_file_MOL ('COULOMB_GRID', NApts_atom, Plotfile_unit)
      else
        Plotfile_unit=UNIout
      end if
!
! Moving grid points on cartesian coordinate grid
      do Ipoint=1,NApts_atom
        XApt=Egridpts(Ifound,Ipoint)%X
        YApt=Egridpts(Ifound,Ipoint)%Y
        ZApt=Egridpts(Ifound,Ipoint)%Z
        r_val=dsqrt(XApt**2+YApt**2+ZApt**2)
        XApt=XApt+CARTESIAN(Iatom)%X
        YApt=YApt+CARTESIAN(Iatom)%Y
        ZApt=ZApt+CARTESIAN(Iatom)%Z
        WApt=Egridpts(Ifound,Ipoint)%w
        call BeckeW1 (XApt, YApt, ZApt, Iatom, Bweight)
        call GET_rho_Atom (XApt, YApt, ZApt, rho)  ! Used for Ecorr and Vpotl
        call Vpotl_GRID_POINT

        TVpotl=TVpotl+Vpotl
        do IRegion=1,NRegions
        if(r_val.le.Regions(IRegion))then
          VpotlbyR(IRegion)=VpotlbyR(IRegion)+Vpotl
          NRbyR(IRegion)=NRbyR(IRegion)+1
          exit
        else 
        end if
        end do ! IRegion
        if(r_val.gt.Regions(NRegions))then
          VpotlbyR(NRegions)=VpotlbyR(NRegions)+Vpotl
          NRbyR(NRegions)=NRbyR(NRegions)+1
        end if
      end do ! Ipoint
      write(Plotfile_unit,*)'Iatom: ',Iatom
      write(Plotfile_unit,'(a)')'    Iregion  Region     Npoints   Vpotl'
        call flush(Plotfile_unit)
      NRpoints=0
      do IRegion=1,NRegions
        VpotlbyR(IRegion)=FourPi*VpotlbyR(IRegion)
        VpotlbyA=VpotlbyA+VpotlbyR(IRegion)
        NRpoints=NRpoints+NRbyR(IRegion)
        write(Plotfile_unit,'(i8,f12.6,i8,f12.6)')IRegion,Regions(IRegion),NRbyR(IRegion),VpotlbyR(IRegion)
        call flush(Plotfile_unit)
      end do
! Deallocate
      deallocate(VpotlbyR)
      deallocate(NRbyR)
      VpotlbyA=PT5*VpotlbyA
      write(Plotfile_unit,'(a,i8)')'NApts_atom: ',NApts_atom
      write(Plotfile_unit,'(a)')'    Atom  Npoints     Vpotl'
      write(Plotfile_unit,'(2i8,f12.6)')Iatom,NRpoints,VpotlbyA
      call flush(Plotfile_unit)
      end do ! Iatom
      deallocate(Regions)
!
      TVpotl=PT5*TVpotl*FourPi
      write(UNIout,'(a,f12.6)')'TVpotl: ',TVpotl
      call flush(Plotfile_unit)
!
      if(MoleculePrint.or.AtomPrint)close (unit=Plotfile_unit)
!
! End of routine BLD_COULOMB_GRID
      call PRG_manager ('exit', 'BLD_COULOMB_GRID', 'UTILITY')
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine Vpotl_GRID_POINT
!****************************************************************************************
!     Date last modified: February 22, 2017                                             *
!     Authors: R.A. Poirier                                                             *
!     Description: Calculate the VPOTL at a point (Xpt, Ypt, Zpt)                       *
!****************************************************************************************
! Modules:

      implicit none
!
! Local array:
      double precision, dimension(:), allocatable  :: V12dr2
!
! Local functions:
      double precision :: TraceAB
!
! Begin: 
      allocate (V12dr2(1:MATlen)) 
      call I1E_V12dr2 (V12dr2, MATlen, XApt, YApt, ZApt) ! new routine without AOprod uses OEPXYZ!!!
      Vpotl=-TraceAB (PM0, V12dr2, NBasis)
      Vpotl=Vpotl*rho*Bweight*WApt
      deallocate (V12dr2) 
!
! End of routine Vpotl_GRID_POINT
      return
      end subroutine Vpotl_GRID_POINT
! END CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine BLD_Coulomb_GRID
