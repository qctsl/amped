      subroutine plot_GRIDS
!*******************************************************************************
!     Date last modified: December 31, 2015                                    *
!     Authors:                                                                 *
!     Description:                                                             * 
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE type_plotting
      USE module_grid_points

      implicit none
!
! Local scalars:
      integer :: Iatom,IApoint,IAIM,Znum,Ifound,NGridPointsI
!
! Local array:
      double precision, allocatable,dimension(:) :: GRIDS_mol
      double precision, allocatable,dimension(:,:) :: GRIDS_atom
!
! Begin:
      call PRG_manager ('enter', 'PLOT_GRIDS', 'UTILITY')
!
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
      
      NGridPoints=0
      do Iatom=1,Natoms
         Znum=CARTESIAN(Iatom)%Atomic_number
         if(Znum.le.0)cycle
         Ifound=GRID_loc(Znum)
         NApts_atom=NApoints_atom(Ifound)       
         NGridPoints=NGridPoints+NApts_atom 
      end do   
      
      allocate(GRIDS_mol(NGridPoints))
      allocate(GRIDS_atom(Natoms,NGridPoints))
      allocate(grid_points(NGridPoints))
      
      GRIDS_atom=ZERO
      GRIDS_mol=ZERO
      
      NGridPointsI=0
      do Iatom=1,Natoms
         Znum=CARTESIAN(Iatom)%Atomic_number
         if(Znum.le.0)cycle
         Ifound=GRID_loc(Znum)
         NApts_atom=NApoints_atom(Ifound)
         
         do IApoint=1,NApts_atom
             NGridPointsI=NGridPointsI+1
             grid_points(NGridPointsI)%x=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
             grid_points(NGridPointsI)%y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
             grid_points(NGridPointsI)%z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
             GRIDS_mol(NGridPointsI)=dsqrt(grid_points(NGridPointsI)%x**2+ &
                                        &  grid_points(NGridPointsI)%y**2+ &
                                        &  grid_points(NGridPointsI)%z**2)
             GRIDS_atom(Iatom,NGridPointsI)=GRIDS_mol(NGridPointsI)
         end do
      end do   

 !     allocate(Egridpts(1:Natoms,1:NGridPoints))
 
 
 ! Print the atom in molecule K energy 
      if(AtomPrint)then
      do Iatom=1,Natoms
        Znum=CARTESIAN(Iatom)%Atomic_number
        if(Znum.le.0)cycle
        Ifound=GRID_loc(Znum)
        NApts_atom=NApoints_atom(Ifound)
        PlotFileID=' '
        PlotFunction=' '
        write(PlotFunction,'(a,i4,2a)')'Grid Locations Atomic for atom ',Iatom,' using ', RADIAL_grid
        write(PlotFileID,'(a)')'Grid_Locations_ATOMIC_'//RADIAL_grid
        call BLD_plot_file_ATOMIC (PlotFileID, NGridPoints, Iatom, Plotfile_unit)
        call PRT_MESH_ATOMIC (GRIDS_atom, Iatom, Natoms, NGridPoints)
        close (unit=Plotfile_unit)
      end do ! Iatom
     end if
     
! Print the molecular radial density 
     if(MoleculePrint)then
       PlotFileID=' '
       write(PlotFileID,'(a)')'Grid_Locations_'//RADIAL_grid
       call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
       write(PlotFunction,'(2a)')'Molecular Grid Locations using ', RADIAL_grid
       call PRT_GRID_MOL (GRIDS_mol, NGridPoints, fmt_rho)
       close (unit=Plotfile_unit)
      else ! print to standard output
       write(PlotFunction,'(2a)')'Molecular Grid Locations using ', RADIAL_grid
       Plotfile_unit=UNIout
       call PRT_GRID_MOL (GRIDS_mol, NGridPoints, fmt_rho)
      end if

!
! Now multiply by 4pi and get total exchange
      do Iatom=1,Natoms
      Znum=CARTESIAN(Iatom)%Atomic_number
      if(Znum.le.0)cycle
    
      end do ! Iatom
!

! End of routine ENERGY_K_NI

      call PRG_manager ('exit', 'PLOT_GRIDS', 'UTILITY')
      return
      end subroutine plot_GRIDS     
