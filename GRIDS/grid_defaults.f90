      subroutine SET_defaults_GRIDS
!********************************************************************************************************
!     Date last modified: March 31, 2000                                                   Version 2.0  *
!     Author: R.A. Poirier                                                                              *
!     Description: Set code execution defaults.                                                         *
!********************************************************************************************************
! Modules:
      USE module_grid_points

      implicit none
!
! Begin:
      RADIAL_grid='SG1'
      GRID_min(1)=-1.0D0
      GRID_min(2)=-1.0D0
      GRID_min(3)=-1.0D0
      GRID_mesh(1)=0.10D0
      GRID_mesh(2)=0.10D0
      GRID_mesh(3)=0.10D0

      Nx=20
      Ny=20
      Nz=20
      
      Ralpha=0.0d0
      Rbeta=0.0d0
      Rgama=0.0d0 ! α, β, γ, rotation about axes x, y, z
!
      NRegions=40
      MAX_r=4.0D0
      MAXCycles=100
!
      test_pt(1) = 0.0D0
      test_pt(2) = 0.0D0
      test_pt(3) = 0.0D0
!
      KineticMethod='PLS' ! SCH or PLS
!
! End of routine SET_defaults_GRIDS
      return
      end subroutine SET_defaults_GRIDS
