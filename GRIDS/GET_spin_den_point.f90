       subroutine GET_spin_den_point (xpt, ypt, zpt, rhoa_grid, rhoHFa_grid, rhob_grid, rhoHFb_grid)
!*******************************************************************************
!     Date last modified: April 24, 2019                                       *
!     Authors: JWH                                                             *
!     Description: Compute the spin density at a point.                        *
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE INT_objects

      implicit none
!
! Input scalars:
      double precision, intent(IN) :: xpt, ypt, zpt
!
! Output scalars:
      double precision, intent(OUT) :: rhoa_grid,rhoHFa_grid
      double precision, intent(OUT) :: rhob_grid,rhoHFb_grid
!
! Local arrays:
      double precision, dimension(:), allocatable :: AOprod
!
! Functions:
      double precision :: traclo
!
! Begin:
!
      call GET_object ('QM', 'SPIN_DENSITY_1MATRIX', 'AO')
!
      allocate(AOprod(MATlen))
!    
      rhoa_grid=ZERO
      rhoHFa_grid=ZERO        
!
      rhob_grid=ZERO
      rhoHFb_grid=ZERO        
!
      call Gaussian_products (xpt, ypt, zpt, AOprod, MATlen)
!
      rhoa_grid = traclo(AOprod, PM0_alpha, Nbasis, MATlen)
      rhoHFa_grid = traclo(AOprod, PM0HF_alpha, Nbasis, MATlen)
!
      rhob_grid = traclo(AOprod, PM0_beta,  Nbasis, MATlen)
      rhoHFb_grid = traclo(AOprod, PM0HF_beta,  Nbasis, MATlen)
!
      return
      end subroutine GET_spin_den_point 
