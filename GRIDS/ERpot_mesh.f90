      subroutine BLD_ERpot_mesh
!****************************************************************************************
!     Date last modified: September 26, 2022                                            *
!     Authors: JWH                                                                      *
!     Description: Calculate the electron-electron repulsion potential on a mesh.       *
!****************************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE module_grid_points
      USE type_plotting
      USE type_Weights

      implicit none
!
! Local scalars:
      integer :: Ipoint
      double precision :: X,Y,Z
      double precision :: Veeaa_pt,VeeHFaa_pt
      double precision :: Veebb_pt,VeeHFbb_pt
      double precision :: Veeab_pt,VeeHFab_pt
      double precision :: Veeba_pt,VeeHFba_pt
!
! Local array:
      double precision, allocatable,dimension(:) :: Veeaa,VeeHFaa
      double precision, allocatable,dimension(:) :: Veebb,VeeHFbb
      double precision, allocatable,dimension(:) :: Veeab,VeeHFab
      double precision, allocatable,dimension(:) :: Veeba,VeeHFba
!
! Begin:
      call PRG_manager ('enter', 'BLD_ERpot_mesh', 'UTILITY')
!
      call GET_object ('QM', '2RDM', 'AO')
!
      ! grid_points may of been destroyed between MENU and here, so rebuild mesh
      ! Really should always be done just before use
      if(LMesh)then
        call BLD_GRID_MESH
      else
        call BLD_GRID_RADIAL
      end if
!
      allocate (Veeaa(1:NGridPoints))
      allocate (VeeHFaa(1:NGridPoints))
      allocate (Veebb(1:NGridPoints))
      allocate (VeeHFbb(1:NGridPoints))
!
      allocate (Veeab(1:NGridPoints))
      allocate (VeeHFab(1:NGridPoints))
      allocate (Veeba(1:NGridPoints))
      allocate (VeeHFba(1:NGridPoints))
!
      Veeaa = ZERO
      VeeHFaa = ZERO
!
      Veebb = ZERO
      VeeHFbb = ZERO
!
      Veeab = ZERO
      VeeHFab = ZERO
!
      Veeba = ZERO
      VeeHFba = ZERO
!
      do Ipoint = 1, NGridPoints
!
        X = grid_points(Ipoint)%x
        Y = grid_points(Ipoint)%y
        Z = grid_points(Ipoint)%z
!
! same-spin
        call GET_ERpotss_point (X, Y, Z, Veeaa_pt, VeeHFaa_pt, Veebb_pt, VeeHFbb_pt)
!
        Veeaa(Ipoint) = Veeaa_pt
!
        VeeHFaa(Ipoint) = VeeHFaa_pt
!
        Veebb(Ipoint) = Veebb_pt
!
        VeeHFbb(Ipoint) = VeeHFbb_pt
!
! opposite-spin
        call GET_ERpotos_point (X, Y, Z, Veeab_pt, VeeHFab_pt, Veeba_pt, VeeHFba_pt)
!
        Veeab(Ipoint) = Veeab_pt
!
        VeeHFab(Ipoint) = VeeHFab_pt
!
        Veeba(Ipoint) = Veeba_pt
!
        VeeHFba(Ipoint) = VeeHFba_pt
!
      end do ! Ipoint
!
! Print the atom in molecule Vee density
!      if(AtomPrint)then
!        do Iatom=1,Natoms
!          Znum=CARTESIAN(Iatom)%Atomic_number
!          if(Znum.le.0)cycle
!          PlotFileID=' '
!          PlotFunction=' '
!          write(PlotFunction,'(a,i4)')'Vee for atom ',Iatom
!          write(PlotFileID,'(a)')'Vee_ATOMIC'
!          call BLD_plot_file_ATOMIC (PlotFileID, NGridPoints, Iatom, Plotfile_unit)
!          call PRT_MESH_ATOMIC (VeeTotal_atom, Iatom, Natoms, NGridPoints)
!          close (unit=Plotfile_unit)
!        end do ! Iatom
!      end if
!
! Print the Vee density 
! same-spin
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'Vee_aa_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular Vee density'
        call PRT_GRID_MOL (Veeaa, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular Vee density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (Veeaa, NGridPoints, 'F21.14')
      end if
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'Vee_aa_HF_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular Vee density'
        call PRT_GRID_MOL (VeeHFaa, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular Vee density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (VeeHFaa, NGridPoints, 'F21.14')
      end if
!
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'Vee_bb_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular Vee density'
        call PRT_GRID_MOL (Veebb, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular Vee density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (Veebb, NGridPoints, 'F21.14')
      end if
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'Vee_bb_HF_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular Vee density'
        call PRT_GRID_MOL (VeeHFbb, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular Vee density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (VeeHFbb, NGridPoints, 'F21.14')
      end if
!
! opposite-spin
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'Vee_ab_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular Vee density'
        call PRT_GRID_MOL (Veeab, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular Vee density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (Veeab, NGridPoints, 'F21.14')
      end if
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'Vee_ab_HF_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular Vee density'
        call PRT_GRID_MOL (VeeHFab, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular Vee density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (VeeHFab, NGridPoints, 'F21.14')
      end if
!
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'Vee_ba_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular Vee density'
        call PRT_GRID_MOL (Veeba, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular Vee density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (Veeba, NGridPoints, 'F21.14')
      end if
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'Vee_ba_HF_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular Vee density'
        call PRT_GRID_MOL (VeeHFba, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular Vee density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (VeeHFba, NGridPoints, 'F21.14')
      end if
!
! End of routine BLD_ERpot_mesh
      call PRG_manager ('exit', 'BLD_ERpot_mesh', 'UTILITY')
      return
      end subroutine BLD_ERpot_mesh
      subroutine BLD_Jcoul_mesh
!****************************************************************************************
!     Date last modified: November 14, 2024                                             *
!     Authors: JWH                                                                      *
!     Description: Calculate the Coulomb repulsion potential on a mesh (nonSIE version).*
!****************************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE module_grid_points
      USE type_plotting
      USE type_Weights

      implicit none
!
! Local scalars:
      integer :: Ipoint
      double precision :: X,Y,Z
      double precision :: Jcoul_pt
!
! Local array:
      double precision, allocatable,dimension(:) :: Jcoul
!
! Begin:
      call PRG_manager ('enter', 'BLD_Jcoul_mesh', 'UTILITY')
!
      call GET_object ('QM', 'CMO', Wavefunction)
!
      ! grid_points may of been destroyed between MENU and here, so rebuild mesh
      ! Really should always be done just before use
      if(LMesh)then
        call BLD_GRID_MESH
      else
        call BLD_GRID_RADIAL
      end if
!
      allocate (Jcoul(1:NGridPoints))
!
      Jcoul = zero
!
      do Ipoint = 1, NGridPoints
!
        X = grid_points(Ipoint)%x
        Y = grid_points(Ipoint)%y
        Z = grid_points(Ipoint)%z
!
! same-spin
        call GET_coul_point (X, Y, Z, Jcoul_pt)
!
        Jcoul(Ipoint) = Jcoul_pt
!
      end do ! Ipoint
!
! Print the Jcoul density 
!
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'Vee_aa_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular Jcoul density'
        call PRT_GRID_MOL (Jcoul, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular Jcoul density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (Jcoul, NGridPoints, 'F21.14')
      end if
!
! End of routine BLD_Jcoul_mesh
      call PRG_manager ('exit', 'BLD_Jcoul_mesh', 'UTILITY')
      return
      end subroutine BLD_Jcoul_mesh
      subroutine BLD_Kexch_mesh
!****************************************************************************************
!     Date last modified: November 14, 2024                                             *
!     Authors: JWH                                                                      *
!     Description: Calculate the Coulomb repulsion potential on a mesh (nonSIE version).*
!****************************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE module_grid_points
      USE type_plotting
      USE type_Weights

      implicit none
!
! Local scalars:
      integer :: Ipoint
      double precision :: X,Y,Z
      double precision :: Kexch_pt
!
! Local array:
      double precision, allocatable,dimension(:) :: Kexch
!
! Begin:
      call PRG_manager ('enter', 'BLD_Kexch_mesh', 'UTILITY')
!
      call GET_object ('QM', 'CMO', Wavefunction)
!
      ! grid_points may of been destroyed between MENU and here, so rebuild mesh
      ! Really should always be done just before use
      if(LMesh)then
        call BLD_GRID_MESH
      else
        call BLD_GRID_RADIAL
      end if
!
      allocate (Kexch(1:NGridPoints))
!
      Kexch = zero
!
      do Ipoint = 1, NGridPoints
!
        X = grid_points(Ipoint)%x
        Y = grid_points(Ipoint)%y
        Z = grid_points(Ipoint)%z
!
! same-spin
        call GET_exch_point (X, Y, Z, Kexch_pt)
!
        Kexch(Ipoint) = Kexch_pt
!
      end do ! Ipoint
!
! Print the Kexch density 
!
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'Vee_aa_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular Kexch density'
        call PRT_GRID_MOL (Kexch, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular Kexch density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (Kexch, NGridPoints, 'F21.14')
      end if
!
! End of routine BLD_Kexch_mesh
      call PRG_manager ('exit', 'BLD_Kexch_mesh', 'UTILITY')
      return
      end subroutine BLD_Kexch_mesh
