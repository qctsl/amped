      Subroutine BLD_TA_GRID
!**************************************************************************************
!     Date last modified: 6th June ,2005                                              *
!     Author: Aisha                                                                   *
!     Description:                                                                    *
!**************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE type_radial_grids
      USE NI_defaults
      USE module_grid_points
      USE type_elements
      USE N_integration
      USE type_Weights

      implicit none

      external :: Angular_grid194,Angular_grid50,Angular_grid14,Angular_grid302

! Local scalars:
      integer :: Iatom,Ifound,MAXpts,Z_num,NR1,NR2
      integer :: NRpoint1,NRpoint2,NRpoint3 ! Number of radial points per region  
      integer :: NApoint1,NApoint2,NApoint3 ! Number of angular points per region  

! Local parameters:

! Begin:
      call PRG_manager ('enter', 'BLD_TA_GRID', 'UTILITY')

! The following is the numerical integration algorithm developed
! by Treutler and Ahlrichs specifically grid number three in
! their paper: J. Chem. Phys. 102(1), 346, 1995. 

      RADIAL_grid='TA'

      call BLD_BeckeW_XI
      XI = DSQRT(XI)
      call BLD_grid_location ! Defines Nfound and LZfound

      num_regions = 3
      allocate(Nap_grid(Nfound,num_regions), NApoints_atom(Nfound))
      Nap_grid(1:Nfound,1) = 14
      Nap_grid(1:Nfound,2) = 50

      allocate(NRpoints_region(Nfound,num_regions))
      allocate(Radial_points(Nfound,45),Radial_weights(Nfound,45))

      MAXpts=2202
      allocate(Egridpts(Nfound,MAXpts))

      do Z_num=1,Nelements
        if(.not.LZfound(Z_num))cycle
        Ifound=GRID_loc(Z_num)

        if(Z_num.lt.3)then  ! H or He 
          NRpoints = 30            
          NRpoint1 = NRpoints/3
          NRpoint2 = NRpoints/2 - NRpoint1
          NRpoint3 = NRpoints-(NRpoint1+NRpoint2)
          NApoint1 = 14*NRpoint1
          NApoint2 = 50*NRpoint2
          NApoint3 = 194*NRpoint3
          NApoints_atom(Ifound) = NApoint1+NApoint2+NApoint3
          NApts_atom=NApoints_atom(Ifound)
          Nap_grid(Ifound,3) = 194

          call Integrate (Ifound, Z_num, NRpoint1, NRpoint2, NRpoint3, &
                          NApoint1, NApoint2, NApoint3, Angular_grid14, Angular_grid50, Angular_grid194)
        else if(Z_num.gt.2)then
          if(Z_num.lt.11)then  !Li-Ne
            NRpoints = 35
          else if (Z_num.gt.10.and.Z_num.LT.19)then !Na-Ar
            NRpoints = 40
          else if(Z_num.gt.18.and.Z_num.LT.37)then !K-Kr
            NRpoints = 45
          else
            write(UNIout,'(a)')'Value of R not available beyond Kr'
            call PRG_stop ('Value of R not available beyond Kr')
          end if !Z_num
          NRpoint1 = NRpoints/3
          NRpoint2 = NRpoints/2 - NRpoint1
          NRpoint3 = NRpoints-(NRpoint1+NRpoint2)
          NApoint1 = 14*NRpoint1
          NApoint2 = 50*NRpoint2
          NApoint3 = 302*NRpoint3
          NApoints_atom(Ifound) = NApoint1+NApoint2+NApoint3
          NApts_atom=NApoints_atom(Ifound)
          Nap_grid(Ifound,3) = 302

          call Integrate (Ifound, Z_num, NRpoint1, NRpoint2, NRpoint3, &
                          NApoint1, NApoint2, NApoint3, Angular_grid14, Angular_grid50, Angular_grid302)
         end if ! Z_num

         call RAweights (Ifound)
         Egridpts(Ifound,1:NApts_atom)= grid_points(1:NApts_atom)

         deallocate(grid_points)

      end do !Z_num
      deallocate(Radial_points,Radial_weights,NRpoints_region,Nap_grid)       

      return
      end subroutine BLD_TA_GRID
