      subroutine BLD_spin_density_mesh
!****************************************************************************************
!     Date last modified: April 26, 2019                                                *
!     Authors: JWH                                                                      *
!     Description: Calculate the alpha and beta spin-density on a mesh.                 *
!****************************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE module_grid_points
      USE type_plotting
      USE type_Weights

      implicit none
!
! Local scalars:
      integer :: Ipoint
      double precision :: X,Y,Z, rhoa_pt, rhob_pt
      double precision :: rhoHFa_pt, rhoHFb_pt
!
! Local arrays:
      double precision, dimension(:), allocatable :: AOprod
      double precision, dimension(:), allocatable :: rhoa, rhob
      double precision, dimension(:), allocatable :: rhoHFa, rhoHFb
!
! Begin:
!
      call PRG_manager ('enter', 'BLD_SPIN_DENSITY_MESH', 'UTILITY')
!
      call GET_object ('QM', 'SPIN_DENSITY_1MATRIX', 'AO')
!
      ! grid_points may of been destroyed between MENU and here, so rebuild mesh
      ! Really should always be done just before use
      if(LMesh)then
        call BLD_GRID_MESH
      else
        call BLD_GRID_RADIAL
      end if
!
      allocate (AOprod(1:MATlen))
!
      allocate(rhoa(NGridPoints))
      allocate(rhob(NGridPoints))
!
      allocate(rhoHFa(NGridPoints))
      allocate(rhoHFb(NGridPoints))
!
      rhoa = zero
      rhob = zero
!
      rhoHFa = zero
      rhoHFb = zero
!
      do Ipoint=1,NGridPoints
        X = grid_points(Ipoint)%x
        Y = grid_points(Ipoint)%y
        Z = grid_points(Ipoint)%z
!
        call GET_spin_den_point (X, Y, Z, rhoa_pt, rhoHFa_pt, rhob_pt, rhoHFb_pt)
!
        rhoa(Ipoint) = rhoa_pt
        rhob(Ipoint) = rhob_pt
!
        rhoHFa(Ipoint) = rhoHFa_pt
        rhoHFb(Ipoint) = rhoHFb_pt
!
      end do ! Ipoint
!
! Print the atom in molecule radial density
!      if(AtomPrint)then
!      do Iatom=1,Natoms
!        Znum=CARTESIAN(Iatom)%Atomic_number
!        if(Znum.le.0)cycle
!        PlotFileID=' '
!        PlotFunction=' '
!        write(PlotFunction,'(a,i4)')'Radial Density Atomic for atom ',Iatom
!        write(PlotFileID,'(a)')'RADIAL_DENSITY_ATOMIC'
!        call BLD_plot_file_ATOMIC (PlotFileID, NGridPoints, Iatom, Plotfile_unit)
!        call PRT_MESH_ATOMIC (Rden_ATOM, Iatom, Natoms, NGridPoints)
!        close (unit=Plotfile_unit)
!      end do ! Iatom
!     end if

! Print the molecular spin-density 
! correlated
! alpha
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'alpha_spin_density_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'molecular alpha spin-density'
        call PRT_GRID_MOL (rhoa, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'molecular alpha spin-density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (rhoa, NGridPoints, 'F21.14')
      end if
!
! beta
      if(MoleculePrint)then
       PlotFileID=' '
       write(PlotFileID,'(a)')'beta_spin_density_mesh'
       call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
       write(PlotFunction,'(a)')'molecular beta spin-density'
       call PRT_GRID_MOL (rhob, NGridPoints, 'F21.14')
       close (unit=Plotfile_unit)
      else ! print to standard output
       write(PlotFunction,'(a)')'molecular beta spin-density'
       Plotfile_unit=UNIout
       call PRT_GRID_MOL (rhob, NGridPoints, 'F21.14')
      end if
!
! Hartree-Fock
! alpha
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'HFalpha_spin_density_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'molecular HF alpha spin-density'
        call PRT_GRID_MOL (rhoHFa, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'molecular HF alpha spin-density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (rhoHFa, NGridPoints, 'F21.14')
      end if
!
! beta
      if(MoleculePrint)then
       PlotFileID=' '
       write(PlotFileID,'(a)')'HFbeta_spin_density_mesh'
       call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
       write(PlotFunction,'(a)')'molecular HF beta spin-density'
       call PRT_GRID_MOL (rhoHFb, NGridPoints, 'F21.14')
       close (unit=Plotfile_unit)
      else ! print to standard output
       write(PlotFunction,'(a)')'molecular beta spin-density'
       Plotfile_unit=UNIout
       call PRT_GRID_MOL (rhoHFb, NGridPoints, 'F21.14')
      end if

      deallocate (AOprod,rhoa,rhob)
      deallocate (rhoHFa,rhoHFb)
!
! End of routine BLD_SPIN_DENSITY_mesh
      call PRG_manager ('exit', 'BLD_SPIN_DENSITY_MESH', 'UTILITY')
      return
      end subroutine BLD_spin_density_mesh
