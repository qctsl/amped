     subroutine BLD_SG1_GRID
!************************************************************************************
!     Date last modified Oct. 2, 2009                                               *
!     Author: R.A. Poirier                                                          *
!     Description:                                                                  *
!************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE type_elements
      USE type_radial_grids
      USE NI_defaults
      USE N_integration

      implicit none
!
! Local scalars:
      integer :: KRpoint,IApoint,Iatom,Z_num,NA1,NA2,NR1,NR2
      integer :: NRpoint1,NRpoint2,NRpoint3,NRpoint4,NRpoint5 ! Number of radial points per region  
      integer :: NApoint1,NApoint2,NApoint3,NApoint4,NApoint5 ! Number of angular points per region  
      integer :: Ifound,MAXpts
      double precision :: Radius1,Radius2,Radius3,Radius4
      
! Local arrays:
! Rpoints: the radial points,  Radial_weights: the weight of the radial points
! Radius: the atomic radii of the atoms from H to- Ar
! Alpha1, Alpha2, Alpha3, and Alpha4 are program_constants which divide the
! atomic space into different subregions
       double precision, dimension (30) :: Alpha1
       double precision, dimension (30) :: Alpha2
       double precision, dimension (30) :: Alpha3
       double precision, dimension (30) :: Alpha4
! Begin:
      call PRG_manager ('enter', 'BLD_SG1_GRID', 'UTILITY')

      RADIAL_grid='SG1'

      call BLD_BeckeW_XI
!
! Check if parameters are available
      do Iatom=1,Natoms
        if(CARTESIAN(Iatom)%Atomic_number.gt.30)then
        write(UNIout,'(a)')'ERROR> BLD_SG1_Radial_Grids: Parameters available only up to Zinc'
        stop 'ERROR> BLD_SG1_Radial_Grids: Parameters available only up to Zinc'
        else if(CARTESIAN(Iatom)%Atomic_number.gt.18)then
        write(UNIout,'(a)')'WARNING> BLD_SG1_Radial_Grids: Parameters available only up to Argon'
        write(UNIout,'(a)')'Parameters for K to Zn are estimated from Bragg-Slater radii'
        end if
      end do

      call BLD_grid_location ! Defines Nfound and LZfound

      if(allocated(Radial_points))then
        deallocate(Radial_points,Radial_weights)
      end if 
      allocate(Radial_points(Nfound,50),Radial_weights(Nfound,50))

      NRpoints = 50
      do Z_num=1,Nelements
        if(.not.LZfound(Z_num))cycle
           Ifound=GRID_loc(Z_num)
! The inner limit: the first radial point starting from the nucleus:
      do KRpoint=1,NRpoints
        Radial_points(Ifound,KRpoint)= Radii(Z_num)*dble(KRpoint)**2/dble(NRpoints+1-KRpoint)**2
        Radial_weights(Ifound,KRpoint)=(TWO*(Radii(Z_num)**THREE)*dble(NRpoints+1)* &
        (dble(KRpoint)**5))/(dble(NRpoints+1-KRpoint)**7)
      end do ! KRpoint
      end do ! Z_num


      num_regions = 5
      if(allocated(Nap_grid))then
        deallocate(Nap_grid,NRpoints_region,NAPoints_atom)
      end if 
      allocate(Nap_grid(Nfound,num_regions), NRpoints_region(Nfound,num_regions), NApoints_atom(Nfound))

      Nap_grid(1:Nfound,1) = 6
      Nap_grid(1:Nfound,2) = 38
      Nap_grid(1:Nfound,3) = 86
      Nap_grid(1:Nfound,4) = 194
      Nap_grid(1:Nfound,5) = 86
   
! Alpha's and Radii are from a paper by: Peter M. W. Gill, Benny G. Johnson
! and John A. Pople, Chemical Physics letters, Vol. 209, no. 5,6, page 506, 1993.
! The Alpha's values for the H atom
       Alpha1(1) = 0.2500
       Alpha2(1) = 0.5000
       Alpha3(1) = 1.0000
       Alpha4(1) = 4.5000

! The Alpha's values for the He atom
       Alpha1(2) = 0.2500
       Alpha2(2) = 0.5000
       Alpha3(2) = 1.0000
       Alpha4(2) = 4.5000

! The Alpha's values for the atoms from Li to Ne
       do Iatom = 3,10
         Alpha1(Iatom) = 0.1667
         Alpha2(Iatom) = 0.5000
         Alpha3(Iatom) = 0.9000
         Alpha4(Iatom) = 3.5000
       end do

! The Alpha's values for the atoms from Na to Ar
       do Iatom = 11,18
         Alpha1(Iatom) = 0.1000
         Alpha2(Iatom) = 0.4000
         Alpha3(Iatom) = 0.8000
         Alpha4(Iatom) = 2.5000
       end do ! Iatom


! The Alpha's values for the atoms from K to Zn (Assume to be same as Na to Ar)
       do Iatom = 19,30
         Alpha1(Iatom) = 0.1000
         Alpha2(Iatom) = 0.4000
         Alpha3(Iatom) = 0.8000
         Alpha4(Iatom) = 2.5000
       end do ! Iatom
      MAXpts=0

      do Z_num=1,Nelements
        if(.not.LZfound(Z_num))cycle
           Ifound=GRID_loc(Z_num)
           Radius1 = Radii(Z_num)*Alpha1(Z_num)
           Radius2 = Radii(Z_num)*Alpha2(Z_num)
           Radius3 = Radii(Z_num)*Alpha3(Z_num)
           Radius4 = Radii(Z_num)*Alpha4(Z_num)

! Radial points of the first region:
      NRpoint1 = 0
      KRpoint = 1
      Do while(Radial_points(Ifound,KRpoint).LE.Radius1)
        NRpoint1 = NRpoint1 + 1
        KRpoint  = KRpoint + 1
      end do !

! Radial points of the second region:
      NRpoint2 = 0
      Do while(Radial_points(Ifound,KRpoint).LE.Radius2)
        NRpoint2 = NRpoint2 + 1
        KRpoint  = KRpoint + 1
      end do

! Radial points of the third region:
      NRpoint3 = 0
      Do while (Radial_points(Ifound,KRpoint).LE.Radius3)
        NRpoint3 = NRpoint3 + 1
        KRpoint  = KRpoint + 1
      end do

! Radial points of the fourth region:
      NRpoint4 = 0
      Do while (Radial_points(Ifound,KRpoint).LE.Radius4)
        NRpoint4 = NRpoint4 + 1
        KRpoint  = KRpoint + 1
      end do

! Radial points of the fifth region:

      NRpoint5 = NRpoints-(NRpoint1+NRpoint2+NRpoint3+NRpoint4)
      
      NRpoints_region(Ifound,1) = NRpoint1
      NRpoints_region(Ifound,2) = NRpoint2
      NRpoints_region(Ifound,3) = NRpoint3
      NRpoints_region(Ifound,4) = NRpoint4
      NRpoints_region(Ifound,5) = NRpoint5

      NApoint1 = 6*NRpoint1
      NApoint2 = 38*NRpoint2
      NApoint3 = 86*NRpoint3
      NApoint4 = 194*NRpoint4
      NApoint5 = 86*NRpoint5
      NApoints_atom(Ifound)=NApoint1+NApoint2+NApoint3+NApoint4+NApoint5
      if(NApoints_atom(Ifound).gt.MAXpts)MAXpts=NApoints_atom(Ifound)
      end do ! Z_num

      MAX_Angular_points=MAXpts
      if(allocated(Egridpts))then
        deallocate(Egridpts)
      end if 
      allocate(Egridpts(Nfound,MAXpts))
      do Ifound=1,Nfound
      NRpoint1=NRpoints_region(Ifound,1)
      NRpoint2=NRpoints_region(Ifound,2)
      NRpoint3=NRpoints_region(Ifound,3)
      NRpoint4=NRpoints_region(Ifound,4)
      NRpoint5=NRpoints_region(Ifound,5)
      NApoint1 = 6*NRpoint1
      NApoint2 = 38*NRpoint2
      NApoint3 = 86*NRpoint3
      NApoint4 = 194*NRpoint4
      NApoint5 = 86*NRpoint5

      NApts_atom=NApoints_atom(Ifound)
      if(allocated(grid_points))then
        deallocate(grid_points)
      end if
      allocate(grid_points(NApts_atom))

! Radial region 1
      Rpoints(1:NRpoint1) = Radial_points(Ifound,1:NRpoint1)
!     write(6,*)'Region 1:'
!     write(6,*)'from ',Rpoints(1),'to ',Rpoints(NRpoint1),' ',NRpoint1,NApoint1
      allocate(Apoints_temp(NApoint1))
      call Angular_grid6 (Rpoints, NRpoint1, NApoint1, Apoints_temp)
      grid_points(1:NApoint1)= Apoints_temp(1:NApoint1)
      deallocate(Apoints_temp)

! Radial region 2
      NR1=NRpoint1+1
      NR2=NRpoint1+NRpoint2
      Rpoints(1:NRpoint2) = Radial_points(Ifound,NR1:NR2)
!     write(6,*)'Region 2:'
!     write(6,*)'from ',Rpoints(1),'to ',Rpoints(NRpoint2),' ',NRpoint2,NApoint2
      allocate(Apoints_temp(NApoint2))
      call Angular_grid38 (Rpoints, NRpoint2, NApoint2, Apoints_temp)
      NA1=NApoint1+1
      NA2=NApoint1+NApoint2
      grid_points(NA1:NA2) = Apoints_temp(1:NApoint2)
      deallocate(Apoints_temp)

! Radial region 3
      NR1=NR1+NRpoint2
      NR2=NR2+NRpoint3
      Rpoints(1:NRpoint3) = Radial_points(Ifound,NR1:NR2)
!     write(6,*)'Region 3:'
!     write(6,*)'from ',Rpoints(1),'to ',Rpoints(NRpoint3),' ',NRpoint3,NApoint3
      allocate(Apoints_temp(NApoint3))
      call Angular_grid86 (Rpoints, NRpoint3, NApoint3, Apoints_temp)
      NA1=NA1+NApoint2
      NA2=NA2+NApoint3
      grid_points(NA1:NA2) = Apoints_temp(1:NApoint3)
      deallocate(Apoints_temp)

! Radial region 4
      NR1=NR1+NRpoint3
      NR2=NR2+NRpoint4
      Rpoints(1:NRpoint4) = Radial_points(Ifound,NR1:NR2)
!     write(6,*)'Region 4:'
!     write(6,*)'from ',Rpoints(1),'to ',Rpoints(NRpoint4),' ',NRpoint4,NApoint4
      allocate(Apoints_temp(NApoint4))
      call Angular_grid194 (Rpoints, NRpoint4, NApoint4, Apoints_temp)
      NA1=NA1+NApoint3
      NA2=NA2+NApoint4
      grid_points(NA1:NA2) = Apoints_temp(1:NApoint4)
      deallocate(Apoints_temp)

! Radial region 5
      NR1=NR1+NRpoint4
      NR2=50
      Rpoints(1:NRpoint5) = Radial_points(Ifound,NR1:NR2)
!     write(6,*)'Region 5:'
!     write(6,*)'from ',Rpoints(1),'to ',Rpoints(NRpoint5),' ',NRpoint5,NApoint5
      allocate(Apoints_temp(NApoint5))
      call Angular_grid86 (Rpoints, NRpoint5, NApoint5, Apoints_temp)
      NA1=NA1+NApoint4
      NA2=NApts_atom
      grid_points(NA1:NA2)= Apoints_temp(1:NApoint5)
      deallocate(Apoints_temp)

      call RAweights (Ifound)
      Egridpts(Ifound,1:NApts_atom)= grid_points(1:NApts_atom)

      deallocate(grid_points)
      end do ! Ifound
!     deallocate(NRpoints_region)
!     deallocate(Radial_points,Radial_weights)

      call PRG_manager ('exit', 'BLD_SG1_GRID', 'UTILITY')
      return
      end subroutine BLD_SG1_GRID
