      subroutine BLD_2RDM_mesh
!****************************************************************************************
!     Date last modified: May 26, 2021                                                  *
!     Authors: JWH                                                                      *
!     Description: Calculate the two-electron density on a mesh for a test electron.    *
!****************************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE module_grid_points
      USE type_plotting
      USE type_Weights

      implicit none
!
! Local scalars:
      integer :: Ipoint
      double precision :: X,Y,Z
      double precision :: TRDMaa_pt
      double precision :: TRDMbb_pt
      double precision :: TRDMab_pt
      double precision :: TRDMba_pt
!
! Local array:
      double precision, allocatable,dimension(:) :: TRDMaa_tot
      double precision, allocatable,dimension(:) :: TRDMbb_tot
      double precision, allocatable,dimension(:) :: TRDMab_tot
      double precision, allocatable,dimension(:) :: TRDMba_tot
!
! Begin:
      call PRG_manager ('enter', 'BLD_2RDM_mesh', 'UTILITY')
!
      call GET_object ('QM', '2RDM', 'MO')
!
      if(LMesh)then
        call BLD_GRID_MESH
      else
        call BLD_GRID_RADIAL
      end if
!
      allocate (TRDMaa_tot(1:NGridPoints))
      allocate (TRDMbb_tot(1:NGridPoints))
      allocate (TRDMab_tot(1:NGridPoints))
      allocate (TRDMba_tot(1:NGridPoints))
!
      TRDMaa_tot = zero
      TRDMbb_tot = zero
      TRDMab_tot = zero
      TRDMba_tot = zero
!
      do Ipoint = 1, NGridPoints
!
        X = grid_points(Ipoint)%x
        Y = grid_points(Ipoint)%y
        Z = grid_points(Ipoint)%z
!
! alpha-alpha
        call GET_2RDM_point (X, Y, Z, test_pt, TRDM_MOaa, CMO%coeff, TRDMaa_pt)
!
        TRDMaa_tot(Ipoint) = TRDMaa_pt
!
! beta-beta
        call GET_2RDM_point (X, Y, Z, test_pt, TRDM_MObb, CMO%coeff, TRDMbb_pt)
!
        TRDMbb_tot(Ipoint) = TRDMbb_pt
!
! alpha-beta
        call GET_2RDM_point (X, Y, Z, test_pt, TRDM_MOab, CMO%coeff, TRDMab_pt)
!
        TRDMab_tot(Ipoint) = TRDMab_pt
!
! beta-alpha
        call GET_2RDM_point (X, Y, Z, test_pt, TRDM_MOba, CMO%coeff, TRDMba_pt)
!
        TRDMba_tot(Ipoint) = TRDMba_pt
!
      end do ! Ipoint
!
! Print the TRDM density 
! same-spin
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'2e_density_aa_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular 2e density'
        call PRT_GRID_MOL (TRDMaa_tot, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular 2e density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (TRDMaa_tot, NGridPoints, 'F21.14')
      end if
!
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'2e_density_bb_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular 2e density'
        call PRT_GRID_MOL (TRDMbb_tot, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular 2e density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (TRDMbb_tot, NGridPoints, 'F21.14')
      end if
!
! opposite-spin
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'2e_density_ab_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular 2e density'
        call PRT_GRID_MOL (TRDMab_tot, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular 2e density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (TRDMab_tot, NGridPoints, 'F21.14')
      end if
!
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'2e_density_ba_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular 2e density'
        call PRT_GRID_MOL (TRDMba_tot, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular 2e density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (TRDMba_tot, NGridPoints, 'F21.14')
      end if
!
! End of routine BLD_2RDM_mesh
      call PRG_manager ('exit', 'BLD_2RDM_mesh', 'UTILITY')
      return
      end subroutine BLD_2RDM_mesh
