     subroutine Angular_grid6 (Rpoints, NRpoint, NApoints, Apoints_temp)
!******************************************************************
!   Date last modified: Oct.16th, 2001                            *
!   Author: Aisha                                                 *
!   Description: This grid returns 6 angular points               *
!******************************************************************
! Modules:
      USE module_grid_points

      implicit none

! Input scalars:
      integer, intent (in) :: NRpoint,NApoints

! Input arrays:
      double precision, dimension(NRpoint), intent (in)   :: Rpoints

! Output arrays:
      type(type_grid_points),dimension(NApoints) :: Apoints_temp

! Local scalars:
      integer          :: IApoint,KRpoint
      double precision :: radius

! Local parameters:
      double precision, parameter  :: A1 = 0.16666666666666670D0

! Begin:
      IApoint=0
      do KRpoint = 1,NRpoint
        Radius = Rpoints(KRpoint)
        call grid6 (A1 ,IApoint ,radius, NApoints, Apoints_temp)
      end do  !KRpoint loop

      end subroutine Angular_grid6
      subroutine Angular_grid38 (Rpoints, NRpoint, NApoints, Apoints_temp)
!******************************************************************
!   Date last modified: Oct.16th, 2001                            *
!   Author: Aisha                                                 *
!   Description: This grid returns 38 angular points              *
!******************************************************************
! Modules:
      USE module_grid_points

      implicit none

! Input scalars:
      integer, intent (in) :: NRpoint, NApoints

! Input arrays:
      double precision,dimension(NRpoint)    :: Rpoints

! Output arrays:
      type(type_grid_points),dimension(NApoints) :: Apoints_temp

! Local scalars:
      Integer          :: IApoint,KRpoint
      double precision :: radius

! Local parameters:
      double precision, parameter  :: p1 = 0.45970084338098310D0
      double precision, parameter  :: A1 = 0.009523809523809524D0
      double precision, parameter  :: A3 = 0.03214285714285714D0
      double precision, parameter  :: C  = 0.02857142857142857D0

! Begin:
      IApoint = 0
      do KRpoint = 1,NRpoint
        radius = Rpoints(KRpoint)
        call grid6 (A1, IApoint, radius, NApoints, Apoints_temp)
        call grid8 (A3, IApoint, radius, NApoints, Apoints_temp)
        call grid_C24 (C, p1, IApoint, radius, NApoints, Apoints_temp)
      end do !KRpoint loop

      end subroutine Angular_grid38
      subroutine Angular_grid26 (Rpoints, NRpoint, NApoints, Apoints_temp)
!******************************************************************
!   Date last modified: Aug, 29th 2006                            *
!   Author: Aisha                                                 *
!   Description: This grid returns 26 angular points              *
!******************************************************************
! Modules:
      USE module_grid_points

      implicit none

! Input scalars:
      integer, intent (in) :: NRpoint, NApoints

! Input arrays:
      double precision,dimension(NRpoint)    :: Rpoints

! Output arrays:
      type(type_grid_points),dimension(NApoints) :: Apoints_temp

! Local scalars:
      Integer          :: IApoint,KRpoint
      double precision :: radius

! Local parameters:

      double precision, parameter  :: A1 = 0.4761904761904762D-1
      double precision, parameter  :: A2 = 0.3809523809523810D-1
      double precision, parameter  :: A3 = 0.3214285714285714D-1


! Begin:
      IApoint = 0
      do KRpoint = 1,NRpoint
        radius = Rpoints(KRpoint)
        call grid6 (A1, IApoint, radius, NApoints, Apoints_temp)
        call grid12 (A2, IApoint, radius, NApoints, Apoints_temp)
        call grid8 (A3, IApoint, radius, NApoints, Apoints_temp)
      end do !KRpoint loop

      end subroutine Angular_grid26
      subroutine Angular_grid18 (Rpoints, NRpoint, NApoints, Apoints_temp)
!******************************************************************
!   Date last modified: Aug, 29th 2006                            *
!   Author: Aisha                                                 *
!   Description: This grid returns 18 angular points              *
!******************************************************************
! Modules:
      USE module_grid_points

      implicit none

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! I got the values of A1,A2 from Andrew and using this grid gave a wron answer!
! Defenitely A1, A2 are wrong and Angular_grid18 should not be used!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Input scalars:
      integer, intent (in) :: NRpoint, NApoints

! Input arrays:
      double precision,dimension(NRpoint)    :: Rpoints

! Output arrays:
      type(type_grid_points),dimension(NApoints) :: Apoints_temp

! Local scalars:
      Integer          :: IApoint,KRpoint
      double precision :: radius

! Local parameters:
      double precision, parameter  :: A1 = 0.418879020478639064D-2
      double precision, parameter  :: A2 = 0.837758040957278127D-2

! Begin:
      IApoint = 0
      do KRpoint = 1,NRpoint
        radius = Rpoints(KRpoint)
        call grid6 (A1, IApoint, radius, NApoints, Apoints_temp)
        call grid12 (A2, IApoint, radius, NApoints, Apoints_temp)
      end do !KRpoint loop
      end subroutine Angular_grid18
      subroutine Angular_grid74 (Rpoints, NRpoint, NApoints, Apoints_temp)
!******************************************************************
!   Date last modified: Aug, 29th 2006                            *
!   Author: Aisha                                                 *
!   Description: This grid returns 74 angular points              *
!******************************************************************
! Modules:
      USE module_grid_points

      implicit none

! Input scalars:
      integer, intent (in) :: NRpoint, NApoints

! Input arrays:
      double precision,dimension(NRpoint)    :: Rpoints

! Output arrays:
      type(type_grid_points),dimension(NApoints) :: Apoints_temp

! Local scalars:
      Integer          :: IApoint,KRpoint
      double precision :: radius

! Local parameters:

      double precision, parameter  :: A1 = 0.5130671797338464D-3
      double precision, parameter  :: A2 = 0.1660406956574204D-1
      double precision, parameter  :: A3 = -0.2958603896103896D-1
      double precision, parameter  :: B1 = 0.2657620708215946D-1
      double precision, parameter  :: l1 = 0.4803844614152614D+0
      double precision, parameter  :: C  = 0.1652217099371571D-1
      double precision, parameter  :: p1 = 0.3207726489807764D+0

! Begin:
      IApoint = 0
      do KRpoint = 1,NRpoint
        radius = Rpoints(KRpoint)
        call grid6 (A1, IApoint, radius, NApoints, Apoints_temp)
        call grid12 (A2, IApoint, radius, NApoints, Apoints_temp)
        call grid8 (A3, IApoint, radius, NApoints, Apoints_temp)
        call grid_B24 (B1, l1, IApoint, radius, NApoints, Apoints_temp)
        call grid_C24 (C, p1, IApoint, radius, NApoints, Apoints_temp)
      end do !KRpoint loop
      end subroutine Angular_grid74
      subroutine Angular_grid170 (Rpoints, NRpoint, NApoints, Apoints_temp)
!******************************************************************
!   Date last modified: Aug, 29th 2006                            *
!   Author: Aisha                                                 *
!   Description: This grid returns 170 angular points              *
!******************************************************************
! Modules:
      USE module_grid_points

      implicit none

! Input scalars:
      integer, intent (in) :: NRpoint, NApoints

! Input arrays:
      double precision,dimension(NRpoint)    :: Rpoints

! Output arrays:
      type(type_grid_points),dimension(NApoints) :: Apoints_temp

! Local scalars:
      Integer          :: IApoint,KRpoint
      double precision :: radius

! Local parameters:

      double precision, parameter  :: A1 = 0.5544842902037365D-2
      double precision, parameter  :: A2 = 0.6071332770670752D-2 
      double precision, parameter  :: A3 = 0.6383674773515093D-2

      double precision, parameter  :: B1 = 0.5183387587747790D-2
      double precision, parameter  :: l1 =0.2551252621114134D+0 

      double precision, parameter  :: B2 = 0.6317929009813725D-2
      double precision, parameter  :: l2 = 0.6743601460362766D+0

      double precision, parameter  :: B3 = 0.6201670006589077D-2
      double precision, parameter  :: l3 = 0.4318910696719410D+0

      double precision, parameter  :: C  = 0.5477143385137348D-2 
      double precision, parameter  :: p1 = 0.2613931360335988D+0 

      double precision, parameter  :: D  = 0.5968383987681156D-2
      double precision, parameter  :: r1 = 0.4990453161796037D+0
      double precision, parameter  :: u1 = 0.1446630744325115D+0

! Begin:
      IApoint = 0
      do KRpoint = 1,NRpoint
        radius = Rpoints(KRpoint)
        call grid6 (A1, IApoint, radius, NApoints, Apoints_temp)
        call grid12 (A2, IApoint, radius, NApoints, Apoints_temp)
        call grid8 (A3, IApoint, radius, NApoints, Apoints_temp)
        call grid_B24 (B1, l1, IApoint, radius, NApoints, Apoints_temp)
        call grid_B24 (B2, l2, IApoint, radius, NApoints, Apoints_temp)
        call grid_B24 (B3, l3, IApoint, radius, NApoints, Apoints_temp)
        call grid_C24 (C, p1, IApoint, radius, NApoints, Apoints_temp)
        call grid48 (D, r1, u1, IApoint, radius, NApoints, Apoints_temp)
      end do !KRpoint loop
      end subroutine Angular_grid170
      subroutine Angular_grid146 (Rpoints, NRpoint, NApoints, Apoints_temp)
!******************************************************************
!   Date last modified: Aug, 29th 2006                            *
!   Author: Aisha                                                 *
!   Description: This grid returns 146 angular points              *
!******************************************************************
! Modules:
      USE module_grid_points

      implicit none

! Input scalars:
      integer, intent (in) :: NRpoint, NApoints

! Input arrays:
      double precision,dimension(NRpoint)    :: Rpoints

! Output arrays:
      type(type_grid_points),dimension(NApoints) :: Apoints_temp

! Local scalars:
      Integer          :: IApoint,KRpoint
      double precision :: radius

! Local parameters:

      double precision, parameter  :: A1 = 0.5996313688621381D-3
      double precision, parameter  :: A2 = 0.7372999718620756D-2
      double precision, parameter  :: A3 = 0.7210515360144488D-2 

      double precision, parameter  :: B1 = 0.7116355493117555D-2
      double precision, parameter  :: l1 = 0.6764410400114264D+0

      double precision, parameter  :: B2 = 0.6753829486314477D-2
      double precision, parameter  :: l2 = 0.4174961227965453D+0

      double precision, parameter  :: B3 = 0.7574394159054034D-2
      double precision, parameter  :: l3 = 0.1574676672039082D+0

      double precision, parameter  :: D  = 0.6991087353303262D-2 
      double precision, parameter  :: r1 = 0.1403553811713183D+0 
      double precision, parameter  :: u1 = 0.4493328323269557D+0

! Begin:
      IApoint = 0
      do KRpoint = 1,NRpoint
        radius = Rpoints(KRpoint)
        call grid6 (A1, IApoint, radius, NApoints, Apoints_temp)
        call grid12 (A2, IApoint, radius, NApoints, Apoints_temp)
        call grid8 (A3, IApoint, radius, NApoints, Apoints_temp)
        call grid_B24 (B1, l1, IApoint, radius, NApoints, Apoints_temp)
        call grid_B24 (B2, l2, IApoint, radius, NApoints, Apoints_temp)
        call grid_B24 (B3, l3, IApoint, radius, NApoints, Apoints_temp)
        call grid48 (D, r1, u1, IApoint, radius, NApoints, Apoints_temp)
      end do !KRpoint loop
      end subroutine Angular_grid146
      subroutine Angular_grid86 (Rpoints, NRpoint, NApoints, Apoints_temp)
!******************************************************************
!   Date last modified: June,4th, 2003                            *
!   Author: Aisha                                                 *
!   Description: This grid returns 86 angular points              *
!******************************************************************
! Modules:
      USE module_grid_points

      implicit none

! Input scalars:
      integer, intent (in) :: NRpoint,NApoints

! Input arrays:
      double precision, dimension(NRpoint)    :: Rpoints

! Output arrays:

      type(type_grid_points),dimension(NApoints) :: Apoints_temp

! Local scalars:
      integer          ::  IApoint,KRpoint
      double precision :: radius

! Local parameters:
      double precision, parameter ::  l1 = 0.3696028464541502D0
      double precision, parameter ::  l2 = 0.6943540066026664D0
      double precision, parameter ::  p1 = 0.927330657151D0
      double precision, parameter ::  A1 = 0.011544011544011540D0
      double precision, parameter ::  A3 = 0.011943909085856280D0
      double precision, parameter ::  C  = 0.011812303746904480D0
      double precision, parameter ::  B1 = 0.011110555710603400D0
      double precision, parameter ::  B2 = 0.011876501294537140D0

! Begin:
      IApoint = 0
      do KRpoint = 1,NRpoint
        radius = Rpoints(KRpoint)
        call grid6 (A1 ,IApoint ,radius, NApoints, Apoints_temp)
        call grid8 (A3, IApoint, radius, NApoints, Apoints_temp)
        call grid_B24 (B1, l1, IApoint, radius, NApoints, Apoints_temp)
        call grid_B24 (B2, l2, IApoint, radius, NApoints, Apoints_temp)
        call grid_C24 (C, p1, IApoint, radius, NApoints, Apoints_temp)
      end do !KRpoint loop
      end subroutine Angular_grid86
      subroutine Angular_grid194 (Rpoints, NRpoint, NApoints, Apoints_temp)
!***************************************************************************************
!     Date last modified: June, 5th, 2003                                              *
!     Author: Aisha                                                                    *
!     Desciption: This grid returns 194 angular points                                 *
!***************************************************************************************
! Modules:
      USE module_grid_points

      implicit none

! Input scalars:
      integer, intent (in) :: NRpoint,NApoints

! Input arrays:
      double precision, dimension(NRpoint)     :: Rpoints

! Output arrays:

      type(type_grid_points),dimension(NApoints) :: Apoints_temp

! Local scalars:
      integer          ::  IApoint,KRpoint
      double precision  :: radius

! Local parameters:
      double precision, parameter :: l1 = 0.44469331787174370D0
      double precision, parameter :: l2 = 0.28924656275754390D0
      double precision, parameter :: l3 = 0.67129734426952260D0
      double precision, parameter :: l4 = 0.12993354476500670D0
      double precision, parameter :: p1 = 0.34577021976112830D0
      double precision, parameter :: r1 = 0.83603601548245890D0
      double precision, parameter :: u1 = 0.1590417105383530D0
      double precision, parameter :: A1 = 0.0017823404472446110D0
      double precision, parameter :: A2 = 0.0057169059499771020D0
      double precision, parameter :: A3 = 0.0055733831788487380D0
      double precision, parameter :: B1 = 0.0055187714672736140D0
      double precision, parameter :: B2 = 0.0051582377118053830D0
      double precision, parameter :: B3 = 0.0056087040825879970D0
      double precision, parameter :: B4 = 0.0041067770281693940D0
      double precision, parameter :: C1 = 0.0050518460646148080D0
      double precision, parameter :: D = 0.0055302489162330940D0

! Begin:
      IApoint = 0

      
      do KRpoint = 1, NRpoint
        radius = Rpoints(KRpoint)
        call grid6 (A1 ,IApoint ,radius, NApoints , Apoints_temp)
        call grid12 (A2, IApoint, radius, NApoints, Apoints_temp)
        call grid8 (A3, IApoint, radius, NApoints, Apoints_temp)
        call grid_B24 (B1, l1, IApoint, radius, NApoints, Apoints_temp)
        call grid_B24 (B2, l2, IApoint, radius, NApoints, Apoints_temp)
        call grid_B24 (B3, l3, IApoint, radius, NApoints, Apoints_temp)
        call grid_B24 (B4, l4, IApoint, radius, NApoints, Apoints_temp)
        call grid_C24 (C1, p1, IApoint, radius, NApoints, Apoints_temp)
        call grid48 (D, r1, u1, IApoint, radius, NApoints, Apoints_temp)
      end do  !KRpoint loop
      end subroutine Angular_grid194
      subroutine Angular_grid50(Rpoints, NRpoint, NApoints, Apoints_temp)
!******************************************************************
!   Date last modified: Oct.16th, 2001                            *
!   Author: Aisha                                                 *
!   Description: This grid returns 50 angular points              *
!******************************************************************
! Modules:
      USE module_grid_points

      implicit none

! Input scalars:
      integer, intent (in) :: NRpoint,NApoints

! Input arrays:
      double precision,dimension(NRpoint)    :: Rpoints

! Output arrays:
      type(type_grid_points),dimension(NApoints) :: Apoints_temp

! Local scalars:
      Integer          :: IApoint,KRpoint
      double precision :: radius

! Local parameters:
      double precision, parameter  :: l1 = 0.30151134457776360D0
      double precision, parameter  :: A1 = 0.01269841269841270D0
      double precision, parameter  :: A2 = 0.02257495590828924D0
      double precision, parameter  :: A3 = 0.02109375000000000D0
      double precision, parameter  :: B1 = 0.02017333553791887D0

! Begin:
      IApoint = 0
      do KRpoint = 1,NRpoint
        radius = Rpoints(KRpoint)
        call grid6 (A1 ,IApoint ,radius, NApoints, Apoints_temp)
        call grid12 (A2, IApoint, radius, NApoints, Apoints_temp)
        call grid8 (A3, IApoint, radius, NApoints, Apoints_temp)
        call grid_B24 (B1, l1, IApoint, radius, NApoints, Apoints_temp)
      end do  ! kRpoint loop
      end subroutine Angular_grid50

      subroutine Angular_grid110(Rpoints, NRpoint, NApoints, Apoints_temp)
!******************************************************************
!   Date last modified: Oct.16th, 2001                            *
!   Author: Aisha                                                 *
!   Description: This grid returns 110 angular points             *
!******************************************************************
! Modules:
      USE module_grid_points

      implicit none

! Input scalars:
      integer, intent (in) :: NRpoint,NApoints

! Input arrays:
      double precision, dimension(NRpoint)    :: Rpoints

! Output arrays:
      type(type_grid_points),dimension(NApoints) :: Apoints_temp

! Local scalars:
      integer          ::  IApoint,KRpoint
      double precision ::  radius

! Local parameters:
      double precision, parameter ::  l1 = 0.18511563534473620D0
      double precision, parameter ::  l2 = 0.69042104838229220D0
      double precision, parameter ::  L3 = 0.39568947305594190D0
      double precision, parameter ::  p1 = 0.47836902881215020D0
      double precision, parameter ::  A1 = 0.0038282704949371620D0
      double precision, parameter ::  A3 = 0.0097937375124875120D0
      double precision, parameter ::  B1 = 0.0082117372831911110D0
      double precision, parameter ::  B2 = 0.0099428148911781030D0
      double precision, parameter ::  B3 = 0.0095954713360709630D0
      double precision, parameter ::  C1 = 0.0096949963616630280D0

! Begin:
      IApoint = 0
      do KRpoint = 1,NRpoint
        radius = Rpoints(KRpoint)
        call grid6 (A1, IApoint, radius, NApoints, Apoints_temp)
        call grid8 (A3, IApoint, radius, NApoints, Apoints_temp)
        call grid_B24 (B1, l1, IApoint, radius, NApoints, Apoints_temp)
        call grid_B24 (B2, l2, IApoint, radius, NApoints, Apoints_temp)
        call grid_B24 (B3, l3, IApoint, radius, NApoints, Apoints_temp)
        call grid_C24 (C1, p1, IApoint, radius, NApoints, Apoints_temp)
      end do !KRpoint loop
      end subroutine Angular_grid110

      subroutine Angular_grid14 (Rpoints, NRpoint, NApoints, Apoints_temp)
!******************************************************************
!   Date last modified: Oct.16th, 2001                            *
!   Author: Aisha                                                 *
!   Description: This grid returns 14 angular points              *
!******************************************************************
! Modules:
      USE module_grid_points

      implicit none

! Input scalars:
      integer, intent (in) :: NRpoint,NApoints

! Input arrays:
      double precision, dimension(NRpoint)    :: Rpoints

! Output arrays:
      type(type_grid_points),dimension(NApoints) :: Apoints_temp

! Local scalars:
      integer          ::  IApoint,KRpoint
      double precision :: radius

! Local parameters:
      double precision, parameter  :: A1 = 0.06666666666666667D0
      double precision, parameter  :: A3 = 0.07500000000000000D0

! Begin:
      IApoint = 0
      do KRpoint = 1, NRpoint
      radius = Rpoints(KRpoint)
        call grid6 (A1 ,IApoint ,radius, NApoints, Apoints_temp)
        call grid8 (A3, IApoint, radius, NApoints, Apoints_temp)
      end do !KRpoint loop
      end subroutine Angular_grid14

      subroutine Angular_grid302 (Rpoints, NRpoint, NApoints, Apoints_temp)
!***************************************************************************************
!     Date last modified: June, 6th, 2003                                              *
!     Author: Aisha                                                                    *
!     Desciption: This grid returns 302 angular points                                 *
!***************************************************************************************


! Modules:
      USE module_grid_points

      implicit none

! Input scalars:
      integer, intent (in) :: NRpoint,NApoints

! Input arrays:
      double precision, dimension(NRpoint)     :: Rpoints

! Output arrays:
      type(type_grid_points),dimension(NApoints) :: Apoints_temp

! Local scalars:
      integer          ::  IApoint,KRpoint
      double precision  :: radius

! Local parameters:
      double precision, parameter :: l1 = 0.3515640345570105
      double precision, parameter :: l2 = 0.6566329410219612
      double precision, parameter :: l3 = 0.4729054132581005
      double precision, parameter :: l4 = 0.9618308522614784D-1
      double precision, parameter :: l5 = 0.2219645236294178
      double precision, parameter :: l6 = 0.7011766416089545
      double precision, parameter :: p1 = 0.2644152887060663
      double precision, parameter :: p2 = 0.5718955891878961

      double precision, parameter :: r1 = 0.2510034751770465
      double precision, parameter :: u1 = 0.8000727494073952

      double precision, parameter :: r2 = 0.1233548532583327
      double precision, parameter :: u2 = 0.4127724083168531
      double precision, parameter :: A1 = 0.8545911725128148D-3
      double precision, parameter :: A3 = 0.3599119285025571D-2
      double precision, parameter :: B1 = 0.3449788424305883D-2
      double precision, parameter :: B2 = 0.3604822601419882D-2
      double precision, parameter :: B3 = 0.3576729661743367D-2
      double precision, parameter :: B4 = 0.2352101413689164D-2
      double precision, parameter :: B5 = 0.3108953122413675D-2
      double precision, parameter :: B6 = 0.3650045807677255D-2
      double precision, parameter :: C1  = 0.2982344963171804D-2
      double precision, parameter :: C2  = 0.3600820932216460D-2
      double precision, parameter :: D1  = 0.3571540554273387D-2
      double precision, parameter :: D2  = 0.3392312205006170D-2

! Begin:
      IApoint = 0
      do KRpoint = 1, NRpoint
        radius = Rpoints(KRpoint)
        call grid6 (A1, IApoint, radius, NApoints, Apoints_temp)
        call grid8 (A3, IApoint, radius, NApoints, Apoints_temp)
        call grid_B24 (B1, l1, IApoint, radius, NApoints, Apoints_temp)
        call grid_B24 (B2, l2, IApoint, radius, NApoints, Apoints_temp)
        call grid_B24 (B3, l3, IApoint, radius, NApoints, Apoints_temp)
        call grid_B24 (B4, l4, IApoint, radius, NApoints, Apoints_temp)
        call grid_B24 (B5, l5, IApoint, radius, NApoints, Apoints_temp)
        call grid_B24 (B6, l6, IApoint, radius, NApoints, Apoints_temp)
        call grid_C24 (C1, p1, IApoint, radius, NApoints, Apoints_temp)
        call grid_C24 (C2, p2, IApoint, radius, NApoints, Apoints_temp)
        call grid48 (D1, r1, u1, IApoint, radius, NApoints, Apoints_temp)
        call grid48 (D2, r2, u2, IApoint, radius, NApoints, Apoints_temp)
      end do  !KRpoint loop
      end subroutine Angular_grid302

      subroutine Angular_gridBench (Rpoints, NRpoint, NApoints, Apoints_temp)
!***************************************************************************************
!     Date last modified: June, 6th, 2003                                              *
!     Author: Aisha                                                                    *
!     Desciption: This grid returns large angular grid                                 *
!***************************************************************************************


! Modules:
      USE module_grid_points

      implicit none

! Input scalars:
      integer, intent (in) :: NRpoint,NApoints

! Input arrays:
      double precision, dimension(NRpoint)     :: Rpoints

! Output arrays:
      type(type_grid_points),dimension(NApoints) :: Apoints_temp

! Local scalars:
      integer :: IApoint,KRpoint
      double precision :: radius

! Local parameters:

       double precision, parameter :: l1  = 0.3712636449657089D-1
       double precision, parameter :: l2  = 0.9140060412262223D-1 
       double precision, parameter :: l3  = 0.1531077852469906D+0
       double precision, parameter :: l4  = 0.2180928891660612D+0 
       double precision, parameter :: l5  = 0.2839874532200175D+0 
       double precision, parameter :: l6  = 0.3491177600963764D+0
       double precision, parameter :: l7  = 0.4121431461444309D+0
       double precision, parameter :: l8  = 0.4718993627149127D+0
       double precision, parameter :: l9  = 0.5273145452842337D+0
       double precision, parameter :: l10 = 0.6209475332444019D+0 
       double precision, parameter :: l11 = 0.6569722711857291D+0
       double precision, parameter :: l12 = 0.6841788309070143D+0 
       double precision, parameter :: l13 = 0.7012604330123631D+0 

       double precision, parameter :: p1  =0.1072382215478166D+0       
       double precision, parameter :: p2  =0.2582068959496968D+0       
       double precision, parameter :: p3  = 0.4172752955306717D+0      
       double precision, parameter :: p4  = 0.5700366911792503D+0      
       
       double precision, parameter :: r1  = 0.9827986018263947D+0
       double precision, parameter :: u1  = 0.1771774022615325D+0
       double precision, parameter :: r2  = 0.9624249230326228D+0
       double precision, parameter :: u2  = 0.2475716463426288D+0
       double precision, parameter :: r3  = 0.9402007994128811D+0
       double precision, parameter :: u3  = 0.3354616289066489D+0
       double precision, parameter :: r4  = 0.9320822040143202D+0
       double precision, parameter :: u4  = 0.3173615246611977D+0
       double precision, parameter :: r5  = 0.9043674199393299D+0
       double precision, parameter :: u5  = 0.4090268427085357D+0
       double precision, parameter :: r6  = 0.8912407560074747D+0
       double precision, parameter :: u6  = 0.3854291150669224D+0
       double precision, parameter :: r7  =0.8676435628462708D+0 
       double precision, parameter :: u7  =0.4932221184851285D+0 
       double precision, parameter :: r8  =0.8581979986041619D+0 
       double precision, parameter :: u8  =0.4785320675922435D+0 
       double precision, parameter :: r9  =0.8396753624049856D+0 
       double precision, parameter :: u9  =0.4507422593157064D+0 
       double precision, parameter :: r10 =0.8165288564022188D+0 
       double precision, parameter :: u10 =0.5632123020762100D+0 
       double precision, parameter :: r11 =0.8015469370783529D+0 
       double precision, parameter :: u11 =0.5434303569693900D+0 
       double precision, parameter :: r12 =0.7773563069070351D+0 
       double precision, parameter :: u12 =0.5123518486419871D+0 
       double precision, parameter :: r13 =0.7661621213900394D+0 
       double precision, parameter :: u13 =0.6394279634749102D+0 
       double precision, parameter :: r14 =0.7553584143533510D+0 
       double precision, parameter :: u14 =0.6269805509024392D+0 
       double precision, parameter :: r15 = 0.7344305757559503D+0
       double precision, parameter :: u15 =0.6031161693096310D+0 
       double precision, parameter :: r16 = 0.7043837184021765D+0
       double precision, parameter :: u16 =0.5693702498468441D+0 


      double precision, parameter :: A1  = 0.1105189233267572D-3
      double precision, parameter :: A2  = 0.9205232738090741D-3
      double precision, parameter :: A3  = 0.9133159786443561D-3

      double precision, parameter :: B1  = 0.3690421898017899D-3
      double precision, parameter :: B2  = 0.5603990928680660D-3
      double precision, parameter :: B3  = 0.6865297629282609D-3
      double precision, parameter :: B4  = 0.7720338551145630D-3
      double precision, parameter :: B5  = 0.8301545958894795D-3
      double precision, parameter :: B6  = 0.8686692550179628D-3
      double precision, parameter :: B7  = 0.8927076285846890D-3
      double precision, parameter :: B8  = 0.9060820238568219D-3
      double precision, parameter :: B9  = 0.9119777254940867D-3
      double precision, parameter :: B10 = 0.9128720138604181D-3
      double precision, parameter :: B11 = 0.9130714935691735D-3
      double precision, parameter :: B12 = 0.9152873784554116D-3
      double precision, parameter :: B13 = 0.9187436274321654D-3

      double precision, parameter :: C1  = 0.5176977312965694D-3
      double precision, parameter :: C2  = 0.7331143682101417D-3
      double precision, parameter :: C3  = 0.8463232836379928D-3
      double precision, parameter :: C4  = 0.9031122694253992D-3

      double precision, parameter :: D1  =0.6485778453163257D-3
      double precision, parameter :: D2  =0.7435030910982369D-3
      double precision, parameter :: D3  =0.7998527891839054D-3
      double precision, parameter :: D4  =0.8101731497468018D-3
      double precision, parameter :: D5  =0.8483389574594331D-3
      double precision, parameter :: D6  =0.8556299257311812D-3
      double precision, parameter :: D7  =0.8803208679738260D-3
      double precision, parameter :: D8  =0.8811048182425720D-3
      double precision, parameter :: D9  =0.8850282341265444D-3
      double precision, parameter :: D10 =0.9021342299040653D-3
      double precision, parameter :: D11 =0.9010091677105086D-3
      double precision, parameter :: D12 =0.9022692938426915D-3
      double precision, parameter :: D13 =0.9158016174693465D-3
      double precision, parameter :: D14 =0.9131578003189435D-3
      double precision, parameter :: D15 =0.9107813579482705D-3
      double precision, parameter :: D16 =0.9105760258970126D-3


! Begin:
      IApoint = 0
      do KRpoint = 1, NRpoint
        radius = Rpoints(KRpoint)
        call grid6 (A1, IApoint, radius, NApoints, Apoints_temp)
        call grid12 (A2, IApoint, radius, NApoints, Apoints_temp)
        call grid8 (A3, IApoint, radius, NApoints, Apoints_temp)
        call grid_B24 (B1, l1, IApoint, radius, NApoints, Apoints_temp)
        call grid_B24 (B2, l2, IApoint, radius, NApoints, Apoints_temp)
        call grid_B24 (B3, l3, IApoint, radius, NApoints, Apoints_temp)
        call grid_B24 (B4, l4, IApoint, radius, NApoints, Apoints_temp)
        call grid_B24 (B5, l5, IApoint, radius, NApoints, Apoints_temp)
        call grid_B24 (B6, l6, IApoint, radius, NApoints, Apoints_temp)
        call grid_B24 (B7, l7, IApoint, radius, NApoints, Apoints_temp)
        call grid_B24 (B8, l8, IApoint, radius, NApoints, Apoints_temp)
        call grid_B24 (B9, l9, IApoint, radius, NApoints, Apoints_temp)
        call grid_B24 (B10, l10, IApoint, radius, NApoints, Apoints_temp)
        call grid_B24 (B11, l11, IApoint, radius, NApoints, Apoints_temp)
        call grid_B24 (B12, l12, IApoint, radius, NApoints, Apoints_temp)
        call grid_B24 (B13, l13, IApoint, radius, NApoints, Apoints_temp)

        call grid_C24 (C1, p1, IApoint, radius, NApoints, Apoints_temp)
        call grid_C24 (C2, p2, IApoint, radius, NApoints, Apoints_temp)
        call grid_C24 (C3, p3, IApoint, radius, NApoints, Apoints_temp)
        call grid_C24 (C4, p4, IApoint, radius, NApoints, Apoints_temp)


        call grid48 (D1, r1, u1, IApoint, radius, NApoints, Apoints_temp)
        call grid48 (D2, r2, u2, IApoint, radius, NApoints, Apoints_temp)
        call grid48 (D3, r3, u3, IApoint, radius, NApoints, Apoints_temp)
        call grid48 (D4, r4, u4, IApoint, radius, NApoints, Apoints_temp)
        call grid48 (D5, r5, u5, IApoint, radius, NApoints, Apoints_temp)
        call grid48 (D6, r6, u6, IApoint, radius, NApoints, Apoints_temp)
        call grid48 (D7, r7, u7, IApoint, radius, NApoints, Apoints_temp)
        call grid48 (D8, r8, u8, IApoint, radius, NApoints, Apoints_temp)
        call grid48 (D9, r9, u9, IApoint, radius, NApoints, Apoints_temp)
        call grid48 (D10, r10, u10, IApoint, radius, NApoints, Apoints_temp)
        call grid48 (D11, r11, u11, IApoint, radius, NApoints, Apoints_temp)
        call grid48 (D12, r12, u12, IApoint, radius, NApoints, Apoints_temp)
        call grid48 (D13, r13, u13, IApoint, radius, NApoints, Apoints_temp)
        call grid48 (D14, r14, u14, IApoint, radius, NApoints, Apoints_temp)
        call grid48 (D15, r15, u15, IApoint, radius, NApoints, Apoints_temp)
        call grid48 (D16, r16, u16, IApoint, radius, NApoints, Apoints_temp)
      end do
      end subroutine Angular_gridBench

      subroutine grid6 (A1, IApoint ,radius, NApoints, Apoints_temp)
!******************************************************************
!   Date last modified: June,4th, 2003                            *
!   Author: Aisha                                                 *
!   Description:                                                  *
!******************************************************************
! Modules:
      USE module_grid_points

      implicit none

! Input scalars:
      double precision  :: A1
      integer, intent (in out) :: IApoint
      double precision, intent (in) :: radius
      integer, intent (in) :: NApoints

! Output array:
      type(type_grid_points),dimension(NApoints) :: Apoints_temp

! Local scalars:
      integer :: KRpoint, JApoint

! Local arrays:

      type(type_grid_points), dimension(6)     :: ApointA1

! Begin:
      ApointA1%X = (/0.0D0,0.0D0,0.0D0,0.0D0,radius,-radius/)
      ApointA1%Y = (/0.0D0,0.0D0,radius,-radius,0.0D0,0.0D0/)
      ApointA1%Z = (/radius,-radius,0.0D0,0.0D0,0.0D0,0.0D0/)
      do JApoint = 1,6
        IApoint = IApoint + 1
        Apoints_temp(IApoint)%X = ApointA1(JApoint)%X 
        Apoints_temp(IApoint)%Y = ApointA1(JApoint)%Y 
        Apoints_temp(IApoint)%Z = ApointA1(JApoint)%Z
        Apoints_temp(IApoint)%w = A1
      end do  !JApoint loop
      return
      end  subroutine grid6
      subroutine grid8 (A3, IApoint, radius, NApoints, Apoints_temp)
!******************************************************************
!   Date last modified: June,4th, 2003                            *
!   Author: Aisha                                                 *
!   Description:                                                  *
!******************************************************************
! Modules:
      USE module_grid_points

      implicit none

! Input scalars:
      double precision  :: A3
      integer, intent (in out) :: IApoint
      double precision, intent (in) :: radius
      integer :: NApoints

! Output arrays:
      type(type_grid_points),dimension(NApoints) :: Apoints_temp

! Local scalars:
      double precision :: s
      integer :: KRpoint,JApoint

! Local arrays:
      type(type_grid_points), dimension(8)     :: ApointA3

! Begin:

      s = 1.0D0/DSQRT(3.0D0)
      s = s*radius
      ApointA3%X = (/s,s,s,s,-s,-s,-s,-s/)
      ApointA3%Y = (/s,s,-s,-s,s,s,-s,-s/)
      ApointA3%Z = (/s,-s,s,-s,s,-s,s,-s/)
      do JApoint = 1,8
        IApoint = IApoint + 1
        Apoints_temp(IApoint)%X = ApointA3(JApoint)%X
        Apoints_temp(IApoint)%Y = ApointA3(JApoint)%Y
        Apoints_temp(IApoint)%Z = ApointA3(JApoint)%Z
        Apoints_temp(IApoint)%W = A3

      end do  !JApoint loop
      return
      end  subroutine grid8

      subroutine grid12 (A2, IApoint, radius, NApoints, Apoints_temp)
!******************************************************************
!   Date last modified: June,4th, 2003                            *
!   Author: Aisha                                                 *
!   Description:                                                  *
!******************************************************************
! Modules:
      USE module_grid_points

      implicit none

! Input scalars:
      double precision  :: A2
      integer, intent (in out) :: IApoint
      double precision, intent (in) :: radius
      integer, intent (in) :: NApoints

! Output arrays:
      type(type_grid_points),dimension(NApoints) :: Apoints_temp

! Local scalars:
      integer :: KRpoint, JApoint
      double precision :: s

! Local arrays:
      type(type_grid_points), dimension(12) :: ApointA2

! Begin:
      s = 1.0D0/DSQRT(2.0D0)
      s = s*radius
      ApointA2%X = (/s,s,-s,-s,s,s,-s,-s,0.0D0,0.0D0,0.0D0,0.0D0/)
      ApointA2%Y = (/s, -s,s,-s,0.0D0,0.0D0,0.0D0,0.0D0,s,s,-s,-s/)
      ApointA2%Z = (/0.0D0,0.0D0,0.0D0,0.0D0,s,-s,s,-s,s,-s,s,-s/)
      do JApoint = 1,12
        IApoint = IApoint + 1
        Apoints_temp(IApoint)%X = ApointA2(JApoint)%X
        Apoints_temp(IApoint)%Y = ApointA2(JApoint)%Y
        Apoints_temp(IApoint)%Z = ApointA2(JApoint)%Z
        Apoints_temp(IApoint)%W = A2
      end do  !JApoint loop
      return
      end  subroutine grid12
      subroutine grid_C24 (C, p1, IApoint, radius, NApoints, Apoints_temp)
!******************************************************************
!   Date last modified: June,4th, 2003                            *
!   Author: Aisha                                                 *
!   Description:                                                  *
!******************************************************************
! Modules:
      USE module_grid_points

      implicit none

! Input scalars:
      double precision, intent (in) :: C, p1
      integer, intent (in out) :: IApoint
      double precision, intent (in) :: radius
      integer, intent (in) :: NApoints

! Output arrays:
      type(type_grid_points),dimension(NApoints) :: Apoints_temp

! Local scalars:
      double precision :: q1,p,q
      integer :: JApoint

! Local arrays:
      type(type_grid_points), dimension(24)     :: ApointC

! Begin:
      q1 = DSQRT(1.0D0 - p1**2)
      p = p1*radius
      q = q1*radius
      ApointC%X=(/p,p,-p,-p,p,p,-p,-p,0.0D0,0.0D0,0.0D0,0.0D0,q,q,-q,-q,q,q,-q,-q,0.0D0,0.0D0,0.0D0,0.0D0/)
      ApointC%Y=(/q,-q,q,-q,0.0D0,0.0D0,0.0D0,0.0D0,P,P,-P,-P,P,-P,P,-P,0.0D0,0.0D0,0.0D0,0.0D0,q,-q,-q,q/)
      ApointC%Z=(/0.0D0,0.0D0,0.0D0,0.0D0,q,-q,q,-q,q,-q,q,-q,0.0D0,0.0D0,0.0D0,0.0D0,p,-p,p,-p,p,p,-p,-p/)
      do JApoint = 1,24
        IApoint = IApoint + 1
        Apoints_temp(IApoint)%X = ApointC(JApoint)%X
        Apoints_temp(IApoint)%Y = ApointC(JApoint)%Y
        Apoints_temp(IApoint)%Z = ApointC(JApoint)%Z
        Apoints_temp(IApoint)%W = C
      end do  !JApoint loop
      return
      end  subroutine grid_C24
      subroutine grid_B24  (B, l1, IApoint, radius, NApoints, Apoints_temp)
!******************************************************************
!   Date last modified: June,4th, 2003                            *
!   Author: Aisha                                                 *
!   Description:                                                  *
!******************************************************************
! Modules:
      USE module_grid_points

      implicit none

! Input scalars:
      double precision, intent (in) :: B,l1
      integer, intent (in out) :: IApoint
      double precision, intent (in) :: radius
      integer, intent (in) :: NApoints

! Output arrays:
      type(type_grid_points),dimension(NApoints) :: Apoints_temp

! Local scalars:
      double precision :: l,m,m1
      integer :: KRpoint,JApoint

! Local arrays:
      type(type_grid_points), dimension(24)     :: ApointB

! Begin:
      m1 = DSQRT(1.0D0 - 2.0D0*l1**2)
      l = l1*radius
      m = m1*radius
      ApointB%X = (/l ,l ,l, l,-l,-l,-l,-l, l, l, l, l,-l,-l,-l,-l, m, m, m, m,-m,-m,-m,-m/)
      ApointB%y = (/l ,l,-l,-l, l,-l, l,-l, m, m,-m,-m, m,-m, m,-m, l, l,-l,-l, l,-l, l,-l/)
      ApointB%Z = (/m,-m, m,-m, m, m,-m,-m, l,-l, l,-l, l, l,-l,-l, l,-l, l,-l, l, l,-l,-l/)
      do JApoint = 1,24
        IApoint = IApoint+1
        Apoints_temp(IApoint)%X = ApointB(JApoint)%X
        Apoints_temp(IApoint)%Y = ApointB(JApoint)%Y
        Apoints_temp(IApoint)%Z = ApointB(JApoint)%Z
        Apoints_temp(IApoint)%W = B
      end do !JApoint loop
      return
      end  subroutine grid_B24
      subroutine grid48 (D, r1, u1, IApoint, radius, NApoints, Apoints_temp)
!******************************************************************
!   Date last modified: June,4th, 2003                            *
!   Author: Aisha                                                 *
!   Description:                                                  *
!******************************************************************
! Modules:
      USE module_grid_points

      implicit none

! Input scalars:
      double precision, intent (in) :: D,r1,u1
      integer, intent (in out) :: IApoint
      double precision, intent (in) :: radius
      integer, intent (in) :: NApoints

! Output arrays:
      type(type_grid_points),dimension(NApoints) :: Apoints_temp

! Local scalars:
      integer :: JApoint
      double precision :: r,u,w

! Local arrays:
      type(type_grid_points), dimension(48)     :: ApointD

! Begin:
      w = DSQRT (1.0D0 - r1**2 - u1**2)
      r = r1*radius
      u = u1*radius
      w = w*radius
      ApointD%X=(/r,r,r,r,-r,-r,-r,-r,r,r,r,r,-r,-r,-r,-r,u,u,u,u,-u,-u,-u,-u, &
      u,u,u,u,-u,-u,-u,-u,w,w,w,w,-w,-w,-w,-w,w,w,w,w,-w,-w,-w,-w/)

      ApointD%Y=(/u,u,-u,-u,u,-u,u,-u,w,w,-w,-w,w,w,-w,-w,r,r,-r,-r,r,r,-r,-r,w,w, &
      -w,-w,w,w,-w,-w,u,u,-u,-u,u,u,-u,-u,r,r,-r,-r,r,r,-r,-r/)

      ApointD%Z=(/w,-w,w,-w,w,w,-w,-w,u,-u,u,-u,u,-u,u,-u,w,-w,w,-w,w,-w,w,-w, &
      r,-r,r,-r,r,-r,r,-r,r,-r,r,-r,r,-r,r,-r,u,-u,u,-u,u,-u,u,-u/)
      do JApoint = 1,48
        IApoint = IApoint + 1
        Apoints_temp(IApoint)%X = ApointD(JApoint)%X
        Apoints_temp(IApoint)%Y = ApointD(JApoint)%Y
        Apoints_temp(IApoint)%Z = ApointD(JApoint)%Z
        Apoints_temp(IApoint)%W = D
      end do !JApoint loop
      return
      end  subroutine grid48
      subroutine nuclear_rotation
!************************************************************************************
!     Date last modified Jan. 18, 2005                                              *
!     Author: Aisha                                                                 *
!     Description:                                                                  *
!************************************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE module_grid_points

      implicit none

! Local scalars:
      integer :: Iatom
      logical :: nuc_rot
      type(type_grid_points),dimension(:), allocatable :: coord_rot
      double precision, dimension(3,3)  :: rot_inv

! begin
      allocate(coord_rot(Natoms))

      nuc_rot = .true.
      if(nuc_rot) then
!       call  Rotation_invariance
      end if

        do Iatom =1,Natoms
           coord_rot(Iatom)%X = rot_inv(1,1)*CARTESIAN(Iatom)%X+ &
           rot_inv(1,2)*CARTESIAN(Iatom)%Y+rot_inv(1,3)*CARTESIAN(Iatom)%Z

           coord_rot(Iatom)%Y = rot_inv(2,1)*CARTESIAN(Iatom)%X+ &
           rot_inv(2,2)*CARTESIAN(Iatom)%Y+rot_inv(2,3)*CARTESIAN(Iatom)%Z

           coord_rot(Iatom)%Z = rot_inv(3,1)*CARTESIAN(Iatom)%X+ &
           rot_inv(3,2)*CARTESIAN(Iatom)%Y+rot_inv(3,3)*CARTESIAN(Iatom)%Z
         end do
  
        do Iatom =1, Natoms
          CARTESIAN(Iatom)%X = coord_rot(Iatom)%X  
          CARTESIAN(Iatom)%Y = coord_rot(Iatom)%Y  
          CARTESIAN(Iatom)%Z = coord_rot(Iatom)%Z  
        end do

       deallocate(coord_rot)
      end subroutine nuclear_rotation       
      subroutine Rotation_invariance
!**************************************************************************************
!     Date last modified: Dec. 18, 2002                                               *
!     Author: Aisha                                                                   *
!     Description:                                                                    *
!**************************************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE module_grid_points

      implicit none
!
! Local scalars:
      integer :: Ifound
      integer :: i,j,Iatom,IApoint,Z_num
      double precision :: scalar
      double precision, dimension(3) :: diag_mat
      double precision, dimension(3) :: centr_charge,coord_T
      double precision, dimension(3,3) :: mat1,mat2,nuc_mom
      double precision, dimension(3,3)  :: rot_inv ! For angular grids to be rotationally invariant

      nuc_mom (1:3,1:3) =ZERO
      centr_charge (1:3)=ZERO
      mat1 (1:3,1:3)=ZERO
      rot_inv(1:3,1:3)=ZERO
      do i = 1, 3
        mat1(i,i)=1.0D0
        rot_inv(i,i)=1.0D0
      end do ! i loop

      call CENTER_OF_CHARGE (centr_charge)

      do Iatom = 1, Natoms
        Z_num = CARTESIAN(Iatom)%Atomic_number
        if(Z_num.le.0)cycle
        Ifound=GRID_loc(Z_num)
          coord_T(1) = CARTESIAN(Iatom)%X - centr_charge(1)
          coord_T(2) = CARTESIAN(Iatom)%Y - centr_charge(2)
          coord_T(3) = CARTESIAN(Iatom)%Z - centr_charge(3)
          scalar = coord_T(1)**2 + coord_T(2)**2 + coord_T(3)**2
        do i = 1, 3
        do j = 1, 3
          mat2(i,j) = coord_T(i) * coord_T(j)
        end do ! i loop
        end do ! j loop
        nuc_mom(1:3,1:3) = nuc_mom + dble(Z_num) * (scalar*mat1 - mat2)
      end do ! Iatom loop

      call MATRIX_diagonalize (nuc_mom, rot_inv, diag_mat, 3, 2, .false.)

      do IApoint = 1, NApts_atom
        grid_points(IApoint)%X = rot_inv(1,1)*Egridpts(Ifound,IApoint)%X+ &
        rot_inv(1,2)*Egridpts(Ifound,IApoint)%Y+rot_inv(1,3)*Egridpts(Ifound,IApoint)%Z

        grid_points(IApoint)%Y = rot_inv(2,1)*Egridpts(Ifound,IApoint)%X+ &
        rot_inv(2,2)*Egridpts(Ifound,IApoint)%Y+rot_inv(2,3)*Egridpts(Ifound,IApoint)%Z

        grid_points(IApoint)%Z = rot_inv(3,1)*Egridpts(Ifound,IApoint)%X+ &
        rot_inv(3,2)*Egridpts(Ifound,IApoint)%Y+rot_inv(3,3)*Egridpts(Ifound,IApoint)%Z
      end do !IApoint

      return
      end subroutine Rotation_invariance
