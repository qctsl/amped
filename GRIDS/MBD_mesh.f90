      subroutine BLD_MBD_mesh
!****************************************************************************************
!     Date last modified: April 26, 2019                                                *
!     Authors: JWH                                                                      *
!     Description: Calculate the momentum-balance density on a mesh.                    *
!****************************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE module_grid_points
      USE type_plotting
      USE type_Weights

      implicit none
!
! Local scalars:
      integer :: Ipoint
      double precision :: X,Y,Z
      double precision :: MBaa_pt,MBHFaa_pt
      double precision :: MBbb_pt,MBHFbb_pt
      double precision :: MBab_pt,MBHFab_pt
      double precision :: MBba_pt,MBHFba_pt
!
! Local array:
      double precision, allocatable,dimension(:) :: MBaa,MBHFaa
      double precision, allocatable,dimension(:) :: MBbb,MBHFbb
      double precision, allocatable,dimension(:) :: MBab,MBHFab
      double precision, allocatable,dimension(:) :: MBba,MBHFba
!
! Begin:
      call PRG_manager ('enter', 'BLD_MBD_mesh', 'UTILITY')
!
      call GET_object ('QM', '2RDM', 'AO')
!
! Create 2rdms designed for fast calculation of MBD
       call BLD_TRDM_MB
!
      allocate (MBaa(1:NGridPoints))
      allocate (MBHFaa(1:NGridPoints))
      allocate (MBbb(1:NGridPoints))
      allocate (MBHFbb(1:NGridPoints))
!
      allocate (MBab(1:NGridPoints))
      allocate (MBHFab(1:NGridPoints))
      allocate (MBba(1:NGridPoints))
      allocate (MBHFba(1:NGridPoints))
!
      MBaa = zero
      MBHFaa = zero
!
      MBbb = zero
      MBHFbb = zero
!
      MBab = zero
      MBHFab = zero
!
      MBba = zero
      MBHFba = zero
!
      ! grid_points may of been destroyed between MENU and here, so rebuild mesh
      ! Really should always be done just before use
      if(LMesh)then
        call BLD_GRID_MESH
      else
        call BLD_GRID_RADIAL
      end if
!
      do Ipoint = 1,NGridPoints
        X = grid_points(Ipoint)%x
        Y = grid_points(Ipoint)%y
        Z = grid_points(Ipoint)%z
! same-spin
        call GET_MBDss_point (X, Y, Z, MBaa_pt, MBHFaa_pt, MBbb_pt, MBHFbb_pt)
!
        MBaa(Ipoint) = MBaa_pt
!
        MBHFaa(Ipoint) = MBHFaa_pt
!
        MBbb(Ipoint) = MBbb_pt
!
        MBHFbb(Ipoint) = MBHFbb_pt
!
! opposite-spin
        call GET_MBDos_point (X, Y, Z, MBab_pt, MBHFab_pt, MBba_pt, MBHFba_pt)
!
        MBab(Ipoint) = MBab_pt
!
        MBHFab(Ipoint) = MBHFab_pt
!
        MBba(Ipoint) = MBba_pt
!
        MBHFba(Ipoint) = MBHFba_pt
!
      end do ! Ipoint
!
!
! Print the atom in molecule MB density
!      if(AtomPrint)then
!        do Iatom=1,Natoms
!          Znum=CARTESIAN(Iatom)%Atomic_number
!          if(Znum.le.0)cycle
!          PlotFileID=' '
!          PlotFunction=' '
!          write(PlotFunction,'(a,i4)')'MB for atom ',Iatom
!          write(PlotFileID,'(a)')'MB_ATOMIC'
!          call BLD_plot_file_ATOMIC (PlotFileID, NGridPoints, Iatom, Plotfile_unit)
!          call PRT_MESH_ATOMIC (MBTotal_atom, Iatom, Natoms, NGridPoints)
!          close (unit=Plotfile_unit)
!        end do ! Iatom
!      end if
!
! Print the MB density 
! same-spin
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'MBD_aa_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular MB density'
        call PRT_GRID_MOL (MBaa, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular MB density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (MBaa, NGridPoints, 'F21.14')
      end if
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'MBD_HF_aa_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular MB density'
        call PRT_GRID_MOL (MBHFaa, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular MB density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (MBHFaa, NGridPoints, 'F21.14')
      end if
!
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'MBD_bb_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular MB density'
        call PRT_GRID_MOL (MBbb, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular MB density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (MBbb, NGridPoints, 'F21.14')
      end if
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'MBD_HF_bb_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular MB density'
        call PRT_GRID_MOL (MBHFbb, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular MB density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (MBHFbb, NGridPoints, 'F21.14')
      end if
!
! opposite-spin
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'MBD_ab_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular MB density'
        call PRT_GRID_MOL (MBab, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular MB density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (MBab, NGridPoints, 'F21.14')
      end if
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'MBD_HF_ab_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular MB density'
        call PRT_GRID_MOL (MBHFab, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular MB density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (MBHFab, NGridPoints, 'F21.14')
      end if
!
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'MBD_ba_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular MB density'
        call PRT_GRID_MOL (MBba, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular MB density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (MBba, NGridPoints, 'F21.14')
      end if
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'MBD_HF_ba_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular MB density'
        call PRT_GRID_MOL (MBHFba, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular MB density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (MBHFba, NGridPoints, 'F21.14')
      end if
!
!
! End of routine BLD_MBD_mesh
      call PRG_manager ('exit', 'BLD_MBD_mesh', 'UTILITY')
      return
      end subroutine BLD_MBD_mesh
