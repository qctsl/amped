      subroutine PRT_GRID_MOL (Property, length, Format)
!********************************************************************
!     Date last modified: August 1, 2014                            *
!     Authors: R.A. Poirier                                         *
!     Description: Print the property on a MESH to a plot file      *
!********************************************************************
! Modules:
      USE program_files
      USE module_grid_points
      USE type_plotting

      implicit none
!
! Input scalars:
      integer :: length
      double precision :: Property(length)
      character(*) :: Format
!
! Local scalars:
      integer :: Ipoint
      character(len=32) :: Pfmt1
      character(len=32) :: Pfmt2
      character(len=32) :: Pfmt3

!
! Begin:
      call PRG_manager ('enter','PRT_GRID_MOL', 'UTILITY')

      Pfmt1='('//trim(fmt_GridPts)//','//trim(Format)//')'
      Pfmt2='(2'//trim(fmt_GridPts)//','//trim(Format)//')'
      Pfmt3='(3'//trim(fmt_GridPts)//','//trim(Format)//')'

      write(Plotfile_unit,'(a)')PlotFunction(1:len_trim(PlotFunction))
      
      if(RALPHA.ne.0.or.RBETA.ne.0.or.RGAMA.ne.0)then
        write(Plotfile_unit,'(a,f8.6,a,f8.6,a,f8.6)')'Grid rotated: alpha = ',RALPHA,' beta = ',RBETA,' gamma = ',RGAMA
      end if
!
      if(LMesh)then
!
      if(NX.eq.0.and.NY.eq.0)then
        write(Plotfile_unit,'(a,2f12.6)')'X and Y fixed to: ',grid_points(1)%x,grid_points(1)%y
        write(Plotfile_unit,'(2a)')'Z, ',PlotFunction
        do Ipoint=1,NGridPoints
!         if(Property(Ipoint).le.PlotCutoff)cycle
          write(Plotfile_unit,Pfmt1)grid_points(Ipoint)%z,Property(Ipoint) 
        end do
      else if(NX.eq.0.and.NZ.eq.0)then
        write(Plotfile_unit,'(a,2f12.6)')'X and Z fixed to: ',grid_points(1)%x,grid_points(1)%z
        write(Plotfile_unit,'(2a)')'Y, ',PlotFunction
        do Ipoint=1,NGridPoints
!         if(Property(Ipoint).le.PlotCutoff)cycle
          write(Plotfile_unit,Pfmt1)grid_points(Ipoint)%y,Property(Ipoint) 
        end do
      else if(NY.eq.0.and.NZ.eq.0)then
        write(Plotfile_unit,'(a,2f12.6)')'Y and Z fixed to: ',grid_points(1)%y,grid_points(1)%z
        write(Plotfile_unit,'(2a)')'X, ',PlotFunction
        do Ipoint=1,NGridPoints
!         if(Property(Ipoint).le.PlotCutoff)cycle
          write(Plotfile_unit,Pfmt1)grid_points(Ipoint)%x,Property(Ipoint) 
        end do
      else if(NX.eq.0)then
        write(Plotfile_unit,'(a,2f12.6)')'X fixed to: ',grid_points(1)%x
        write(Plotfile_unit,'(2a)')'Y,Z, ',PlotFunction
        do Ipoint=1,NGridPoints
!         if(Property(Ipoint).le.PlotCutoff)cycle
          write(Plotfile_unit,Pfmt2)grid_points(Ipoint)%y,grid_points(Ipoint)%z,Property(Ipoint) 
        end do
      else if(NY.eq.0)then
        write(Plotfile_unit,'(a,2f12.6)')'Y fixed to: ',grid_points(1)%y
        write(Plotfile_unit,'(2a)')'X,Z, ',PlotFunction
        do Ipoint=1,NGridPoints
!         if(Property(Ipoint).le.PlotCutoff)cycle
          write(Plotfile_unit,Pfmt2)grid_points(Ipoint)%x,grid_points(Ipoint)%z,Property(Ipoint) 
        end do
      else if(NZ.eq.0)then
        write(Plotfile_unit,'(a,2f12.6)')'Z fixed to: ',grid_points(1)%z
        write(Plotfile_unit,'(2a)')'X,Y, ',PlotFunction
        do Ipoint=1,NGridPoints
!         if(Property(Ipoint).le.PlotCutoff)cycle
          write(Plotfile_unit,Pfmt2)grid_points(Ipoint)%x,grid_points(Ipoint)%y,Property(Ipoint) 
        end do
      else
        write(Plotfile_unit,'(a)')'Full 3D Mesh used'
        write(Plotfile_unit,'(2a)')'X,Y,Z, ',PlotFunction
        do Ipoint=1,NGridPoints
!         if(Property(Ipoint).le.PlotCutoff)cycle
          write(Plotfile_unit,Pfmt3)grid_points(Ipoint)%x,grid_points(Ipoint)%y,grid_points(Ipoint)%z,&
          Property(Ipoint) 
        end do
      end if
!
      else ! No mesh defined using integration grid
!
        write(Plotfile_unit,'(a)')'Printing on integration grid'
        write(Plotfile_unit,'(2a)')'X,Y,Z, ',PlotFunction
        do Ipoint=1,NGridPoints
!         if(Property(Ipoint).le.PlotCutoff)cycle
          write(Plotfile_unit,Pfmt3)grid_points(Ipoint)%x,grid_points(Ipoint)%y,grid_points(Ipoint)%z,&
          Property(Ipoint) 
        end do
!
      end if
!
! End of routine PRT_GRID_MOL
      call PRG_manager ('exit', 'PRT_GRID_MOL', 'UTILITY')
      return
      end subroutine PRT_GRID_MOL
      subroutine PRT_MESH_ATOMIC (Rden, Iatom, Natoms, length)
!********************************************************************
!     Date last modified: August 15, 2012                           *
!     Authors: R.A. Poirier                                         *
!     Description: Calculate the charge density at point.           *
!********************************************************************
! Modules:
      USE program_files
      USE module_grid_points
      USE type_plotting

      implicit none
!
! Input scalars:
      integer :: Iatom,Natoms,length
      double precision :: Rden(Natoms,length)
! Local scalars:
      integer :: Ipoint
!
! Begin:
      call PRG_manager ('enter','PRT_MESH_ATOMIC', 'UTILITY')

      write(Plotfile_unit,'(a)')PlotFunction(1:len_trim(PlotFunction))
      
      if(Ralpha.ne.0.or.Rbeta.ne.0.or.Rgama.ne.0)then
        write(Plotfile_unit,'(a,f8.6,a,f8.6,a,f8.6)')'Full 3D Mesh used. alpha=',Ralpha,' beta=',Rbeta,' gamma=',Rgama
        write(Plotfile_unit,'(a)')'X,Y,Z,Function:'
        do Ipoint=1,NGridPoints
        write(Plotfile_unit,Pfmt_r2rho)grid_points(Ipoint)%x,grid_points(Ipoint)%y,grid_points(Ipoint)%z,Rden(Iatom,Ipoint) 
        end do
      else if(NX.eq.0.and.NY.eq.0)then
        write(Plotfile_unit,'(a,2f12.6)')'X and Y fixed to: ',grid_points(1)%x,grid_points(1)%y
        write(Plotfile_unit,'(a)')'Z,Function:'
        do Ipoint=1,NGridPoints
        write(Plotfile_unit,Pfmt1_r2rho)grid_points(Ipoint)%z,Rden(Iatom,Ipoint) 
        end do
      else if(NX.eq.0.and.NZ.eq.0)then
        write(Plotfile_unit,'(a,2f12.6)')'X and Z fixed to: ',grid_points(1)%x,grid_points(1)%z
        write(Plotfile_unit,'(a)')'Y,Function:'
        do Ipoint=1,NGridPoints
        write(Plotfile_unit,Pfmt1_r2rho)grid_points(Ipoint)%y,Rden(Iatom,Ipoint) 
        end do
      else if(NY.eq.0.and.NZ.eq.0)then
        write(Plotfile_unit,'(a,2f12.6)')'Y and Z fixed to: ',grid_points(1)%y,grid_points(1)%z
        write(Plotfile_unit,'(a)')'X,Function:'
        do Ipoint=1,NGridPoints
        write(Plotfile_unit,Pfmt1_r2rho)grid_points(Ipoint)%x,Rden(Iatom,Ipoint) 
        end do
      else if(NX.eq.0)then
        write(Plotfile_unit,'(a,2f12.6)')'X fixed to: ',grid_points(1)%x
        write(Plotfile_unit,'(a)')'Y,Z,Function:'
        do Ipoint=1,NGridPoints
        write(Plotfile_unit,Pfmt2_r2rho)grid_points(Ipoint)%y,grid_points(Ipoint)%z,Rden(Iatom,Ipoint) 
        end do
      else if(NY.eq.0)then
        write(Plotfile_unit,'(a,2f12.6)')'Y fixed to: ',grid_points(1)%y
        write(Plotfile_unit,'(a)')'X,Z,Function:'
        do Ipoint=1,NGridPoints
        write(Plotfile_unit,Pfmt2_r2rho)grid_points(Ipoint)%x,grid_points(Ipoint)%z,Rden(Iatom,Ipoint) 
        end do
      else if(NZ.eq.0)then
        write(Plotfile_unit,'(a,2f12.6)')'Z fixed to: ',grid_points(1)%z
        write(Plotfile_unit,'(a)')'X,Y,Function:'
        do Ipoint=1,NGridPoints
        write(Plotfile_unit,Pfmt2_r2rho)grid_points(Ipoint)%x,grid_points(Ipoint)%y,Rden(Iatom,Ipoint) 
        end do
      else
        write(Plotfile_unit,'(a)')'Full 3D Mesh used'
        write(Plotfile_unit,'(a)')'X,Y,Z,Function:'
        do Ipoint=1,NGridPoints
        write(Plotfile_unit,Pfmt_r2rho)grid_points(Ipoint)%x,grid_points(Ipoint)%y,grid_points(Ipoint)%z,Rden(Iatom,Ipoint) 
        end do
      end if
!
! End of routine PRT_MESH_ATOMIC
      call PRG_manager ('exit', 'PRT_MESH_ATOMIC', 'UTILITY')
      return
      end subroutine PRT_MESH_ATOMIC
      subroutine BLD_plot_file_ATOMIC (FileID, NGridpts, Iatom, File_unit)
!**********************************************************************
!     Date last modified: October 10, 2013                            *
!     Authors: R.A. Poirier                                           *
!     Description: Create a filename for plotting data and get a unit.*
!**********************************************************************
! Modules:
      USE program_files
      USE type_molecule
      USE type_elements
      USE module_grid_points
      USE QM_defaults
      USE type_plotting

      implicit none
!
! Input/Output variables
      integer, intent(IN) :: NGridpts,Iatom
      integer, intent(OUT) :: File_unit
      character*(*) :: FileID
      character(len=64) :: Basis_file_name
!
! Local scalars:
      integer :: lenstr1,lenstr2,Znum
      logical :: Lerror
      character(len=32):: abc,Ctemp,CNGridPts
!
! Begin:
      call PRG_manager ('enter','BLD_plot_file_ATOMIC', 'UTILITY')

      if(.not.LPlotNamed)then
        call BLD_FileID
        Znum=CARTESIAN(Iatom)%Atomic_number
        write(Ctemp,'(a,i8)')'_',NGridpts
        call RemoveBlanks (Ctemp, CNGridPts, lenstr1)
        write(Ctemp,'(a,i8,2a)')'_atom_',Iatom,'_',element_symbols(Znum)
        call RemoveBlanks (Ctemp, abc, lenstr2)
        PlotFilename='plot_'//trim(FileID)//CNGridPts(1:lenstr1)//'_'//trim(Basic_FileID) &
                    //abc(1:lenstr2)//'.dat'
      end if

      call GET_unit (PlotFilename, File_unit, Lerror)
      PRG_file(File_unit)%status='REPLACE '
      PRG_file(File_unit)%name=PlotFilename
      PRG_file(File_unit)%form='FORMATTED'
      PRG_file(File_unit)%type=FileID
      call FILE_open (File_unit)

      write(File_unit,'(2a)')'Results calculated at: ',trim(Level_of_Theory)//'/'//trim(Basis_set_name)
!
! End of routine BLD_plot_file_ATOMIC
      call PRG_manager ('exit', 'BLD_plot_file_ATOMIC', 'UTILITY')
      return
      end subroutine BLD_plot_file_ATOMIC
      subroutine BLD_plot_file_MOL (FileID, NGridpts, File_unit)
!**********************************************************************
!     Date last modified: October 10, 2013                            *
!     Authors: R.A. Poirier                                           *
!     Description: Create a filename for plotting data and get a unit.*
!**********************************************************************
! Modules:
      USE program_files
      USE type_molecule
      USE type_basis_set
      USE module_grid_points
      USE QM_defaults
      USE type_plotting

      implicit none
!
! Input/Output variables
      integer, intent(IN) :: NGridpts
      integer, intent(OUT) :: File_unit
      character*(*) :: FileID
!
! Local scalars:
      integer :: lenstr
      logical :: Lerror
      character(len=32):: Ctemp,CNGridPts
      character(len=4) :: MESHID
      character(len=64) :: Basis_file_name
!
! Begin:
      call PRG_manager ('enter','BLD_plot_file_MOL', 'UTILITY')

      call BLD_MESH_ID (MESHID)
      if(.not.LPlotNamed)then
        write(Ctemp,'(a,i8,a)')'_',NGridpts,MESHID
        call RemoveBlanks (Ctemp, CNGridPts, lenstr)
        call BLD_FileID
        PlotFilename='plot_'//trim(FileID)//CNGridPts(1:lenstr)//'_'//trim(Basic_FileID)//'.dat'
      end if

      call GET_unit (PlotFilename, File_unit, Lerror)
      PRG_file(File_unit)%status='REPLACE '
      PRG_file(File_unit)%name=PlotFilename
      PRG_file(File_unit)%form='FORMATTED'
      PRG_file(File_unit)%type=FileID
      call FILE_open (File_unit)

      write(File_unit,'(2a)')'Results calculated at: ',trim(Level_of_Theory)//'/'//trim(Basis%name)
!
! End of routine BLD_plot_file_MOL
      call PRG_manager ('exit', 'BLD_plot_file_MOL', 'UTILITY')
      return
      end subroutine BLD_plot_file_MOL
      subroutine BLD_plot_file_MO (FileID, NGridpts, Iatom, IMO, File_unit)
!**********************************************************************
!     Date last modified: October 10, 2013                            *
!     Authors: R.A. Poirier                                           *
!     Description: Create a filename for plotting data and get a unit.*
!**********************************************************************
! Modules:
      USE program_files
      USE type_molecule
      USE type_elements
      USE module_grid_points
      USE QM_defaults
      USE type_plotting

      implicit none
!
! Input/Output variables
      integer, intent(IN) :: Iatom,IMO,NGridpts
      integer, intent(OUT) :: File_unit
      character*(*) :: FileID
      character(len=64) :: Basis_file_name
!
! Local scalars:
      integer :: lenstr1,lenstr2,Znum
      logical :: Lerror
      character(len=48):: filename
      character(len=32):: abc,Ctemp,CNGridPts
!
! Begin:
      call PRG_manager ('enter','BLD_plot_file_MO', 'UTILITY')

      if(LPlotNamed)then
        filename=PlotFilename
      else
        call BLD_FileID
        Znum=CARTESIAN(Iatom)%Atomic_number
        write(Ctemp,'(a,i8)')'_',NGridpts
        call RemoveBlanks (Ctemp, CNGridPts, lenstr1)
        write(Ctemp,'(a,i8,3a,i8)')'_atom_',Iatom,'_',element_symbols(Znum),'_MO_',IMO
        call RemoveBlanks (Ctemp, abc, lenstr2)
        filename='plot_'//trim(FileID)//CNGridPts(1:lenstr1)//'_'//abc(1:lenstr2)//'_'//trim(Basic_FileID)//'.dat'
      end if

      call GET_unit (filename, File_unit, Lerror)
      PRG_file(File_unit)%status='REPLACE '
      PRG_file(File_unit)%name=filename
      PRG_file(File_unit)%form='FORMATTED'
      PRG_file(File_unit)%type=FileID
      call FILE_open (File_unit)

      write(File_unit,'(2a)')'Results calculated at: ',trim(Level_of_Theory)//'/'//trim(Basis_set_name)
!
! End of routine BLD_plot_file_MO
      call PRG_manager ('exit', 'BLD_plot_file_MO', 'UTILITY')
      return
      end subroutine BLD_plot_file_MO
      subroutine BLD_MESH_ID (MESH_ID)
!********************************************************************
!     Date last modified: August 1, 2014                            *
!     Authors: R.A. Poirier                                         *
!     Description: Print the property on a MESH to a plot file      *
!********************************************************************
! Modules:
      USE module_grid_points

      implicit none
!
! Input scalars:
      character(*) :: MESH_ID
!
! Local scalars:
!
! Begin:
      call PRG_manager ('enter','BLD_MESH_ID', 'UTILITY')

      if(NX.eq.0.and.NY.eq.0)then
        MESH_ID='_Z'
      else if(NX.eq.0.and.NZ.eq.0)then
        MESH_ID='_Y'
      else if(NY.eq.0.and.NZ.eq.0)then
        MESH_ID='_X'
      else if(NX.eq.0)then
        MESH_ID='_YZ'
      else if(NY.eq.0)then
        MESH_ID='_XZ'
      else if(NZ.eq.0)then
        MESH_ID='_XY'
      else
        MESH_ID='_XYZ'
      end if
!
! End of routine BLD_MESH_ID
      call PRG_manager ('exit', 'BLD_MESH_ID', 'UTILITY')
      return
      end subroutine BLD_MESH_ID
      subroutine GET_file_unit (File_status, File_name, File_form, File_type, File_unit)
!********************************************************************
!     Date last modified: August 1, 2014                            *
!     Authors: R.A. Poirier                                         *
!     Description: Build the basic file ID.                         *
!********************************************************************
! Modules:
      USE program_files

      implicit none
!
! Input scalars:
      character(*) :: File_status,File_name,File_form,File_type
      integer :: File_unit
!
! Local scalars:
      logical :: Lerror
!
! Begin:
      call PRG_manager ('enter','GET_file_unit', 'UTILITY')
!
      call GET_unit (File_name, File_unit, Lerror)
      PRG_file(File_unit)%status=File_status
      PRG_file(File_unit)%name=File_name
      PRG_file(File_unit)%form=File_form
      PRG_file(File_unit)%type=File_type
      call FILE_open (File_unit)
!
! End of routine GET_file_unit
      call PRG_manager ('exit', 'GET_file_unit', 'UTILITY')
      return
      end subroutine GET_file_unit
