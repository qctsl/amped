      subroutine BLD_NTA_GRID
!**************************************************************************************
!     Date last modified: 6th June ,2005                                              *
!     Author: Aisha                                                                   *
!     Description:                                                                    *
!**************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE type_radial_grids
      USE NI_defaults
      USE module_grid_points
      USE type_elements
      USE N_integration
      USE type_Weights

      implicit none

      external :: Angular_grid6,Angular_grid86,Angular_grid194,Angular_grid110

! Local scalars:
      integer :: Iatom,Ifound,MAXpts,Z_num,NR1,NR2
      integer :: NRpoint1,NRpoint2,NRpoint3 ! Number of radial points per region  
      integer :: NApoint1,NApoint2,NApoint3 ! Number of angular points per region  


! Local parameters:

! Begin:
      call PRG_manager ('enter', 'BLD_NTA_GRID', 'UTILITY')

! The following is the numerical integration algorithm developed
! by Treutler and Ahlrichs specifically grid number three in
! their paper: J. Chem. Phys. 102(1), 346, 1995. We changed the
! number of radial points per region to 6,8, and the rest in the 
! third region. We used the angular grids, 6,86, 110 for H, He 
! atoms, and 6,110,194 for the rest of the atoms.

      RADIAL_grid='NTA'

      call BLD_BeckeW_XI
      XI = DSQRT(XI)
      call BLD_grid_location ! Defines Nfound and LZfound

      num_regions = 3
      allocate(Nap_grid(Nfound,num_regions), NApoints_atom(Nfound))

      allocate(NRpoints_region(Nfound,num_regions))
      allocate(Radial_points(Nfound,45),Radial_weights(Nfound,45))

      MAXpts=6930
      allocate(Egridpts(Nfound,MAXpts))

      NRpoint1 = 6
      NRpoint2 = 8
      Nap_grid(1:Nfound,1) = 6
      NApoint1 = 6*NRpoint1

      do Z_num=1,Nelements
        if(.not.LZfound(Z_num))cycle
         Ifound=GRID_loc(Z_num)

        if(Z_num.lt.3)then  ! H or He 
          NRpoints = 30            
          NRpoint3 = NRpoints-(NRpoint1+NRpoint2)
          Nap_grid(Ifound,2) = 86
          Nap_grid(Ifound,3) = 110
          NApoint2 = 86*NRpoint2
          NApoint3 = 110*NRpoint3
          NApoints_atom(Ifound) = NApoint1+NApoint2+NApoint3
          NApts_atom=NApoints_atom(Ifound)

          call Integrate (Ifound, Z_num, NRpoint1, NRpoint2, NRpoint3, &
                          NApoint1, NApoint2, NApoint3, Angular_grid6, Angular_grid86, Angular_grid110)

        else if(Z_num.gt.2)then
          if(Z_num.lt.11)then  !Li-Ne
            NRpoints = 35
          else if (Z_num.gt.10.and.Z_num.LT.19)then !Na-Ar
            NRpoints = 40
          else if(Z_num.gt.18.and.Z_num.LT.37)then !K-Kr
            NRpoints = 45
          else
            write(UNIout,'(a)')'Value of R not available beyond Kr'
            call PRG_stop ('Value of R not available beyond Kr')
          end if !Z_num

        NRpoint3 = NRpoints-(NRpoint1+NRpoint2)
        Nap_grid(Ifound,2) = 110
        Nap_grid(Ifound,3) = 194
        NApoint2 = 110*NRpoint2
        NApoint3 = 194*NRpoint3
        NApoints_atom(Ifound) = NApoint1+NApoint2+NApoint3
        NApts_atom=NApoints_atom(Ifound)

        call Integrate (Ifound, Z_num, NRpoint1, NRpoint2, NRpoint3, &
                        NApoint1, NApoint2, NApoint3, Angular_grid6, Angular_grid110, Angular_grid194)
        end if !Z_num

        call RAweights (Ifound)
        Egridpts(Ifound,1:NApts_atom)= grid_points(1:NApts_atom)

        deallocate(grid_points)
      end do !Z_num

!     deallocate(Radial_points,Radial_weights,NRpoints_region,Nap_grid)       

      return
      end subroutine BLD_NTA_GRID

      subroutine Integrate (Ifound, Z_num, NRpoint1, NRpoint2, NRpoint3, &
                            NApoint1, NApoint2, NApoint3, Angular_gridN1, Angular_gridN2, Angular_gridN3)
!**************************************************************************************
!     Date last modified: Sept 26th ,2001                                             *
!     Author: Aisha                                                                   *
!     Description:                                                                    *
!**************************************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE type_radial_grids
      USE module_grid_points
      USE N_integration

      implicit none

! Local scalars:
      integer :: KRpoint,Ifound,Z_num,NR1,NR2,NA1,NA2
      integer :: NRpoint1,NRpoint2,NRpoint3
      integer :: NApoint1,NApoint2,NApoint3
      double precision :: factor1,factor2,factor3

! Local arrays:
      double precision, dimension(36)       :: EXI
      double precision, dimension(NRpoints) :: X
      external :: Angular_gridN1,Angular_gridN2,Angular_gridN3

! EXI values are given in the reference given above
! by Treutler and Ahlrichs.

! I used EXI(Ni) = 1.2, I am not sure if this value is right,
! if you need the precise value, get EXI(Ni) from the journal!!

!Begin:
      EXI = Angstrom_to_Bohr*(/0.8,0.9,1.8,1.4,1.3,1.1,0.9,0.9,0.9,1.1,1.4,1.3,1.3,1.2,1.1,1.0,1.0,1.0,&
     1.5,1.4,1.3,1.2,1.2,1.2,1.2,1.2,1.2,1.2,1.1,1.1,1.1,1.0,0.9,0.9,0.9,0.9/)
      do KRpoint = 1,NRpoints
        X(KRpoint) = cos(dble(KRpoint)*PI_val/dble(NRpoints+1))
        Radial_points(Ifound,KRpoint) = - (EXI(Z_num)/LOG(2.0D0))*((1.0D0+X(KRpoint))**0.6)*LOG((1.0D0-X(KRpoint))/2.0D0)
        factor1 = PI_val*((1.0D0+X(KRpoint))**(3*0.60D0))/(dble(NRpoints+1)*(LOG(2.0D0))**3)
        factor2 = DSQRT((1.0D0+X(KRpoint))/(1.0D0-X(KRpoint)))*(LOG((1.0D0-X(KRpoint))/2.0D0))**2
        factor3 = DSQRT( (1.0D0-X(KRpoint))/(1.0D0+X(KRpoint)) )*(LOG((1.0D0-X(KRpoint))/2.0D0))**3
        Radial_weights(Ifound,KRpoint) = factor1 * (factor2 - 0.60D0*factor3) * EXI(Z_num)**3
      end do

      allocate(grid_points(NApts_atom))
        NRpoints_region(Ifound,1) = NRpoint1
        NRpoints_region(Ifound,2) = NRpoint2
        NRpoints_region(Ifound,3) = NRpoint3

      Rpoints(1:NRpoint1) = Radial_points(Ifound,1:NRpoint1)
      allocate(Apoints_temp(NApoint1))
      call Angular_gridN1(Rpoints, NRpoint1, NApoint1, Apoints_temp)
      grid_points(1:NApoint1)= Apoints_temp(1:NApoint1)
      deallocate(Apoints_temp)

      NR1=NRpoint1+1
      NR2=NRpoint1+NRpoint2
      Rpoints(1:NRpoint2) = Radial_points(Ifound,NR1:NR2)
      allocate(Apoints_temp(NApoint2))
      call Angular_gridN2(Rpoints, NRpoint2, NApoint2, Apoints_temp)
      NA1=NApoint1+1
      NA2=NApoint1+NApoint2
      grid_points(NA1:NA2) = Apoints_temp(1:NApoint2)
      deallocate(Apoints_temp)

      NR1=NR1+NRpoint2
      NR2=NR2+NRpoint3
      Rpoints(1:NRpoint3) = Radial_points(Ifound,NR1:NR2)
      allocate(Apoints_temp(NApoint3))
      call Angular_gridN3(Rpoints, NRpoint3, NApoint3, Apoints_temp)
      NA1=NA1+NApoint2
      NA2=NA2+NApoint3
      grid_points(NA1:NA2) = Apoints_temp(1:NApoint3)
      deallocate(Apoints_temp)

      return
      end subroutine Integrate
