      subroutine GET_RF_OS_point (Rv, Nact, rf)
!*******************************************************************************
!     Date last modified: October 21, 2021                                     *
!     Authors: JWH                                                             *
!     Description: Compute the opposite-spin two-electron-density cusp-        *
!                  insertion renormalization function at the given point.      *
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE INT_objects

      implicit none
!
! Input scalars:
      integer, intent(IN) :: Nact
!
! Input arrays:
      double precision, intent(IN) :: Rv(3)
!
! Output scalars:
      double precision, intent(OUT) :: rf
!
! Local scalars:
      double precision :: num, den, g, u, cusp, intg, q, lambda
      double precision :: rho, rhoa, rhoHFa, rhob, rhoHFb
      double precision :: u_scale, R2RDMab_grid, R2RDMba_grid, wts
      integer :: I_ux, I_uy, I_uz, N_u_pts
! 
! Local arrays:
      double precision :: uv(3), r1v(3), r2v(3), umag(3)
      double precision, allocatable, dimension(:) :: u_pts, u_wts
!
! Begin:
!
      q = q_TDF
!
      rf = zero
!
      call GET_spin_den_point (Rv(1), Rv(2), Rv(3), rhoa, rhoHFa, rhob, rhoHFb)
!
      rho = rhoa + rhob
!
      if(rho.gt.1.0D-12)then
!
! Current correlation length ansatz
      lambda = q*rho**F1_3
!
! grid parameters (use density at R to determine these)          
      N_u_pts = 6
      !u_scale = 2.0D0
      u_scale = max(min(4.6D0/lambda - 0.3D0, 2.0D0),1.0D0)
      allocate(u_pts(N_u_pts), u_wts(N_u_pts))
      call BLD_MK_grid(N_u_pts, u_scale, u_pts, u_wts)
!
! Integrate over u
      num = 0.0D0
      den = 0.0D0
!
      do I_ux = 1, N_u_pts
        umag(1) = u_pts(I_ux)
        do I_uy = 1, N_u_pts
          umag(2) = u_pts(I_uy)
          do I_uz = 1, N_u_pts
            umag(3) = u_pts(I_uz)
!
            intg = 0.0D0
!
            wts = u_wts(I_ux)*u_wts(I_uy)*u_wts(I_uz)
!
            u = dsqrt(dot_product(umag,umag))
!
            g      = dexp(-(lambda*u)**2)
            cusp   = dexp(u)
!            cusp   = (1.0D0 + lambda - dexp(-0.5D0*lambda*u))/lambda
!            cusp   = cusp**2
!
! (+,+,+)
            uv = umag
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_ab_point (r1v, r2v, Nact, R2RDMab_grid, R2RDMba_grid)
!
            intg = R2RDMab_grid + R2RDMba_grid
!
! (+,+,-)
            uv(3) = -umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_ab_point (r1v, r2v, Nact, R2RDMab_grid, R2RDMba_grid)
!
            intg = intg + R2RDMab_grid + R2RDMba_grid
!
! (+,-,+)
            uv(1) =  umag(1)
            uv(2) = -umag(2)
            uv(3) =  umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_ab_point (r1v, r2v, Nact, R2RDMab_grid, R2RDMba_grid)
!
            intg = intg + R2RDMab_grid + R2RDMba_grid
!
! (-,+,+)
            uv(1) = -umag(1)
            uv(2) =  umag(2)
            uv(3) =  umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_ab_point (r1v, r2v, Nact, R2RDMab_grid, R2RDMba_grid)
!
            intg = intg + R2RDMab_grid + R2RDMba_grid
!
! (+,-,-)
            uv(1) =  umag(1)
            uv(2) = -umag(2)
            uv(3) = -umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_ab_point (r1v, r2v, Nact, R2RDMab_grid, R2RDMba_grid)
!
            intg = intg + R2RDMab_grid + R2RDMba_grid
!
! (-,+,-)
            uv(1) = -umag(1)
            uv(2) =  umag(2)
            uv(3) = -umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_ab_point (r1v, r2v, Nact, R2RDMab_grid, R2RDMba_grid)
!
            intg = intg + R2RDMab_grid + R2RDMba_grid
!
! (-,-,+)
            uv(1) = -umag(1)
            uv(2) = -umag(2)
            uv(3) =  umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_ab_point (r1v, r2v, Nact, R2RDMab_grid, R2RDMba_grid)
!
            intg = intg + R2RDMab_grid + R2RDMba_grid
!
! (-,-,-)
            uv = -umag
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_ab_point (r1v, r2v, Nact, R2RDMab_grid, R2RDMba_grid)
!
            intg = intg + R2RDMab_grid + R2RDMba_grid
!
            intg = intg*g
!
            num = num + wts*intg
            den = den + wts*intg*cusp
!
          end do ! I_uz
        end do ! I_uy
      end do ! I_ux
!
      if(num.gt.1.0D-16)then
        rf = num/den
      end if
!
      end if ! rho > tol
!
      end subroutine GET_RF_OS_point
      subroutine GET_RF_VGL_OS_point (Rv, Nact, rf, rfgrad, rflap)
!*******************************************************************************
!     Date last modified: December 17, 2021                                    *
!     Authors: JWH                                                             *
!     Description: Compute the opposite-spin two-electron-density cusp-        *
!                  insertion renormalization function at the given point.      *
!                  And, the gradient and Laplacian.                            *
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE INT_objects

      implicit none
!
! Input scalars:
      integer, intent(IN) :: Nact
!
! Input arrays:
      double precision, intent(IN) :: Rv(3)
!
! Output scalars:
      double precision, intent(OUT) :: rf, rfgrad(3), rflap
!
! Local scalars:
      double precision :: u, cusp, q, lambda
      double precision :: rho, drho(3), drholam, drho2lam, rho1_3
      double precision :: u_scale, R2RDMab_grid, R2RDMba_grid, wts
      double precision :: R2RDMab_rfgrad(3), R2RDMba_rfgrad(3)
      double precision :: R2RDMab_rflap, R2RDMba_rflap
      double precision :: val, grad(3), lap, d2rho, dl2cusp, dlcusp
      double precision :: num, den, dden(3), dnum(3), d2num, d2den, dR2cusp, dRcusp(3)
      double precision :: numint, denint, ddenint(3), dnumint(3), d2numint, d2denint
      integer :: I_ux, I_uy, I_uz, N_u_pts
! 
! Local arrays:
      double precision :: uv(3), r1v(3), r2v(3), umag(3)
      double precision, allocatable, dimension(:) :: u_pts, u_wts
!
! Begin:
!
      q = q_TDF
!
      rf     = zero
      rfgrad = zero
      rflap  = zero
!
      call GET_den_VGL_point (Rv, Nact, CMO%coeff, rho, drho, d2rho) 
!
      if(rho.gt.1.0D-12)then
!
! Current correlation length ansatz
      rho1_3   = rho**F1_3
!
      lambda   =  q*rho1_3
      drholam  =  q/(3.0D0*rho1_3**2)
      drho2lam = -2.0D0*q/(9.0D0*rho1_3**5)
!
! grid parameters (use density at R to determine these)          
      N_u_pts = 20
      u_scale = 2.0D0
      allocate(u_pts(N_u_pts), u_wts(N_u_pts))
      call BLD_MK_grid(N_u_pts, u_scale, u_pts, u_wts)
!
! Integrate over u
      num   = zero
      den   = zero
      dnum  = zero
      dden  = zero
      d2num = zero
      d2den = zero
!
      do I_ux = 1, N_u_pts
        umag(1) = u_pts(I_ux)
        do I_uy = 1, N_u_pts
          umag(2) = u_pts(I_uy)
          do I_uz = 1, N_u_pts
            umag(3) = u_pts(I_uz)
!
            numint   = zero
            denint   = zero
            dnumint  = zero
            ddenint  = zero
            d2numint = zero
            d2denint = zero
!
            wts = u_wts(I_ux)*u_wts(I_uy)*u_wts(I_uz)
!
            u = dsqrt(dot_product(umag,umag))
!
!            g      = dexp(-(lambda*u)**2)
! Trying bumpless cusp
            cusp   = (1.0D0 + lambda - dexp(-0.5D0*lambda*u))/lambda
! 
            dlcusp  = 0.5D0*(dexp(-0.5D0*lambda*u)*(2.0D0 + u*lambda) - 2.0D0)/lambda**2
            dl2cusp = 0.25D0*(8.0D0 - dexp(-0.5D0*lambda*u)*(8.0D0 + u*lambda*(4.0D0 + u*lambda)))&
                     /lambda**3 
            dRcusp  = dlcusp*drholam*drho
            dR2cusp = (dl2cusp*drholam**2 + dlcusp*drho2lam)*dot_product(drho,drho)&
                     + dlcusp*drholam*d2rho
!
! (+,+,+)
            uv = umag
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_VGLRF_OS_point (r1v, r2v, Nact, R2RDMab_grid, R2RDMba_grid,&
                                                           R2RDMab_rfgrad, R2RDMba_rfgrad,&
                                                           R2RDMab_rflap, R2RDMba_rflap)
!
            val  = R2RDMab_grid + R2RDMba_grid
            grad = R2RDMab_rfgrad + R2RDMba_rfgrad
            lap  = R2RDMab_rflap + R2RDMba_rflap
!
            numint  = numint + val
            denint  = denint + val*cusp**2
            dnumint = dnumint + grad
            ddenint = ddenint + grad*cusp**2 + 2.0D0*val*cusp*dRcusp
            d2numint = d2numint + lap
            d2denint = d2denint + lap*cusp**2 + 4.0D0*cusp*dot_product(grad,dRcusp)&
                          + 2.0D0*val*dot_product(dRcusp,dRcusp)&
                          + 2.0D0*val*cusp*dR2cusp
!
! (+,+,-)
            uv(3) = -umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_VGLRF_OS_point (r1v, r2v, Nact, R2RDMab_grid, R2RDMba_grid,&
                                                           R2RDMab_rfgrad, R2RDMba_rfgrad,&
                                                           R2RDMab_rflap, R2RDMba_rflap)
!
            val  = R2RDMab_grid + R2RDMba_grid
            grad = R2RDMab_rfgrad + R2RDMba_rfgrad
            lap  = R2RDMab_rflap + R2RDMba_rflap
!
            numint  = numint + val
            denint  = denint + val*cusp**2
            dnumint = dnumint + grad
            ddenint = ddenint + grad*cusp**2 + 2.0D0*val*cusp*dRcusp
            d2numint = d2numint + lap
            d2denint = d2denint + lap*cusp**2 + 4.0D0*cusp*dot_product(grad,dRcusp)&
                          + 2.0D0*val*dot_product(dRcusp,dRcusp)&
                          + 2.0D0*val*cusp*dR2cusp
!
! (+,-,+)
            uv(1) =  umag(1)
            uv(2) = -umag(2)
            uv(3) =  umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_VGLRF_OS_point (r1v, r2v, Nact, R2RDMab_grid, R2RDMba_grid,&
                                                           R2RDMab_rfgrad, R2RDMba_rfgrad,&
                                                           R2RDMab_rflap, R2RDMba_rflap)
!
            val  = R2RDMab_grid + R2RDMba_grid
            grad = R2RDMab_rfgrad + R2RDMba_rfgrad
            lap  = R2RDMab_rflap + R2RDMba_rflap
!
            numint  = numint + val
            denint  = denint + val*cusp**2
            dnumint = dnumint + grad
            ddenint = ddenint + grad*cusp**2 + 2.0D0*val*cusp*dRcusp
            d2numint = d2numint + lap
            d2denint = d2denint + lap*cusp**2 + 4.0D0*cusp*dot_product(grad,dRcusp)&
                          + 2.0D0*val*dot_product(dRcusp,dRcusp)&
                          + 2.0D0*val*cusp*dR2cusp
!
! (-,+,+)
            uv(1) = -umag(1)
            uv(2) =  umag(2)
            uv(3) =  umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_VGLRF_OS_point (r1v, r2v, Nact, R2RDMab_grid, R2RDMba_grid,&
                                                           R2RDMab_rfgrad, R2RDMba_rfgrad,&
                                                           R2RDMab_rflap, R2RDMba_rflap)
!
            val  = R2RDMab_grid + R2RDMba_grid
            grad = R2RDMab_rfgrad + R2RDMba_rfgrad
            lap  = R2RDMab_rflap + R2RDMba_rflap
!
            numint  = numint + val
            denint  = denint + val*cusp**2
            dnumint = dnumint + grad
            ddenint = ddenint + grad*cusp**2 + 2.0D0*val*cusp*dRcusp
            d2numint = d2numint + lap
            d2denint = d2denint + lap*cusp**2 + 4.0D0*cusp*dot_product(grad,dRcusp)&
                          + 2.0D0*val*dot_product(dRcusp,dRcusp)&
                          + 2.0D0*val*cusp*dR2cusp
!
! (+,-,-)
            uv(1) =  umag(1)
            uv(2) = -umag(2)
            uv(3) = -umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_VGLRF_OS_point (r1v, r2v, Nact, R2RDMab_grid, R2RDMba_grid,&
                                                           R2RDMab_rfgrad, R2RDMba_rfgrad,&
                                                           R2RDMab_rflap, R2RDMba_rflap)
!
            val  = R2RDMab_grid + R2RDMba_grid
            grad = R2RDMab_rfgrad + R2RDMba_rfgrad
            lap  = R2RDMab_rflap + R2RDMba_rflap
!
            numint  = numint + val
            denint  = denint + val*cusp**2
            dnumint = dnumint + grad
            ddenint = ddenint + grad*cusp**2 + 2.0D0*val*cusp*dRcusp
            d2numint = d2numint + lap
            d2denint = d2denint + lap*cusp**2 + 4.0D0*cusp*dot_product(grad,dRcusp)&
                          + 2.0D0*val*dot_product(dRcusp,dRcusp)&
                          + 2.0D0*val*cusp*dR2cusp
!
! (-,+,-)
            uv(1) = -umag(1)
            uv(2) =  umag(2)
            uv(3) = -umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_VGLRF_OS_point (r1v, r2v, Nact, R2RDMab_grid, R2RDMba_grid,&
                                                           R2RDMab_rfgrad, R2RDMba_rfgrad,&
                                                           R2RDMab_rflap, R2RDMba_rflap)
!
            val  = R2RDMab_grid + R2RDMba_grid
            grad = R2RDMab_rfgrad + R2RDMba_rfgrad
            lap  = R2RDMab_rflap + R2RDMba_rflap
!
            numint  = numint + val
            denint  = denint + val*cusp**2
            dnumint = dnumint + grad
            ddenint = ddenint + grad*cusp**2 + 2.0D0*val*cusp*dRcusp
            d2numint = d2numint + lap
            d2denint = d2denint + lap*cusp**2 + 4.0D0*cusp*dot_product(grad,dRcusp)&
                          + 2.0D0*val*dot_product(dRcusp,dRcusp)&
                          + 2.0D0*val*cusp*dR2cusp
!
! (-,-,+)
            uv(1) = -umag(1)
            uv(2) = -umag(2)
            uv(3) =  umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_VGLRF_OS_point (r1v, r2v, Nact, R2RDMab_grid, R2RDMba_grid,&
                                                           R2RDMab_rfgrad, R2RDMba_rfgrad,&
                                                           R2RDMab_rflap, R2RDMba_rflap)
!
            val  = R2RDMab_grid + R2RDMba_grid
            grad = R2RDMab_rfgrad + R2RDMba_rfgrad
            lap  = R2RDMab_rflap + R2RDMba_rflap
!
            numint  = numint + val
            denint  = denint + val*cusp**2
            dnumint = dnumint + grad
            ddenint = ddenint + grad*cusp**2 + 2.0D0*val*cusp*dRcusp
            d2numint = d2numint + lap
            d2denint = d2denint + lap*cusp**2 + 4.0D0*cusp*dot_product(grad,dRcusp)&
                          + 2.0D0*val*dot_product(dRcusp,dRcusp)&
                          + 2.0D0*val*cusp*dR2cusp
!
! (-,-,-)
            uv = -umag
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_VGLRF_OS_point (r1v, r2v, Nact, R2RDMab_grid, R2RDMba_grid,&
                                                           R2RDMab_rfgrad, R2RDMba_rfgrad,&
                                                           R2RDMab_rflap, R2RDMba_rflap)
!
            val  = R2RDMab_grid + R2RDMba_grid
            grad = R2RDMab_rfgrad + R2RDMba_rfgrad
            lap  = R2RDMab_rflap + R2RDMba_rflap
!
            numint  = numint + val
            denint  = denint + val*cusp**2
            dnumint = dnumint + grad
            ddenint = ddenint + grad*cusp**2 + 2.0D0*val*cusp*dRcusp
            d2numint = d2numint + lap
            d2denint = d2denint + lap*cusp**2 + 4.0D0*cusp*dot_product(grad,dRcusp)&
                          + 2.0D0*val*dot_product(dRcusp,dRcusp)&
                          + 2.0D0*val*cusp*dR2cusp
!
! Trying bumpless cusp
!            intg = intg*g
!
            num   = num   + wts*numint
            den   = den   + wts*denint
            dnum  = dnum  + wts*dnumint
            dden  = dden  + wts*ddenint
            d2num = d2num + wts*d2numint
            d2den = d2den + wts*d2denint
!
          end do ! I_uz
        end do ! I_uy
      end do ! I_ux
!
      if(num.gt.1.0D-16)then
        rf     = num/den
        rfgrad = (dnum*den - num*dden)/den**2
        rflap  = (d2num*den**2 - num*den*d2den&
                + 2.0D0*(num*dot_product(dden,dden) - den*dot_product(dden,dnum)))/den**3
      end if
!
      end if ! rho > tol
!
      end subroutine GET_RF_VGL_OS_point
      subroutine GET_RFapp_OS_point (Rv, rf)
!*******************************************************************************
!     Date last modified: October 21, 2021                                     *
!     Authors: JWH                                                             *
!     Description: Compute the taylor-expansion approximate opposite-spin      *
!                  two-electron-density cusp-insertion renormalization function*
!                  at the given point.                                         *
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE INT_objects

      implicit none
!
! Input arrays:
      double precision, intent(IN) :: Rv(3)
!
! Output scalars:
      double precision, intent(OUT) :: rf
!
! Local scalars:
      double precision :: q, lam, lam2, pi, sqrtpi, expo
      double precision :: rho, rhoa, rhoHFa, rhob, rhoHFb
! 
! Functions:
      double precision, external :: derf
!           
! Begin:
      pi = PI_VAL
      sqrtpi = dsqrt(pi)
!
      q = q_TDF
!
      call GET_spin_den_point (Rv(1), Rv(2), Rv(3), rhoa, rhoHFa, rhob, rhoHFb)
!
      rho = rhoa + rhob
!
      rf = zero
      if(rho.gt.1.0D-10)then
!
! Current correlation length ansatz
        lam  = q*rho**F1_3
        lam2 = lam**2
        expo = dexp(-0.25D0/lam2)
!
        rf = two*sqrtpi*lam2*expo/(two*lam*expo + sqrtpi&
                                   *(one + two*lam2)*(one + derf(0.5D0/lam)))
      end if
!
      end subroutine GET_RFapp_OS_point
