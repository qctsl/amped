      subroutine BLD_GRID_objects
!*****************************************************************************************************
!       Date last modified April 6, 2017                                                             *
!       Author: R.A. Poirier                                                                         *
!       Description:  Builds a complete list of all the objects which can be created in MUNgauss     *
!       which belong to the "class" GRID                                                             *
!*****************************************************************************************************
! Modules:
      USE program_objects

      implicit none

! Local scalar:
      integer :: Iobject
!
! Begin:
      OBJ_GRID(1:Max_objects)%modality = 'NONE'
      OBJ_GRID(1:Max_objects)%class = 'GRID'
      OBJ_GRID(1:Max_objects)%depend = .true.
      NGRIDobjects = 0
!
! RADIAL
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'RADIAL'
      OBJ_GRID(NGRIDobjects)%modality = 'SG1'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_SG1_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'RADIAL'
      OBJ_GRID(NGRIDobjects)%modality = 'SG2'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_SG2_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'RADIAL'
      OBJ_GRID(NGRIDobjects)%modality = 'BIG'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_BIG_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'RADIAL'
      OBJ_GRID(NGRIDobjects)%modality = 'GILL'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_Gill_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'RADIAL'
      OBJ_GRID(NGRIDobjects)%modality = 'BECKE'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_Becke_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'RADIAL'
      OBJ_GRID(NGRIDobjects)%modality = 'NTA'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_NTA_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'RADIAL'
      OBJ_GRID(NGRIDobjects)%modality = 'TA'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_TA_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'RADIAL'
      OBJ_GRID(NGRIDobjects)%modality = 'SG0'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_SG0_GRID'
!
! Modality=MESH
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'DENSITY'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'DENSITY_MESH'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'SPIN_DENSITY'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_SPIN_DENSITY_MESH'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = '2RDM'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_2RDM_MESH'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'TDF'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'CFT_TDF_MESH'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'ODF'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'CFT_ODF_MESH'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'OD'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'CFT_OD_MESH'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'ODUGRAD'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'CFT_ODUGRAD_OS_MESH'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'ODULAP'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'CFT_ODULAP_OS_MESH'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'TDFVEEOS'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_TDF_VEE_OS_MESH'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'TDFTOS'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_TDF_T_OS_MESH'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'OSEC'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_OSEC_MESH'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'RFOS'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_RF_OS_MESH'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'RFAOS'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_RFAPP_OS_MESH'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'DENSITY'
      OBJ_GRID(NGRIDobjects)%modality = 'ATOMIC'
      OBJ_GRID(NGRIDobjects)%routine = 'DENSITY_ATOMIC'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'DEL_DENSITY'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'DEL_DENSITY_MESH'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'DEL2_DENSITY'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'DEL2_DENSITY_MESH'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'RADIAL_DENSITY'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'RADIAL_DENSITY_MESH'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'RADIAL_DENSITY_MO'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'RADIAL_DENSITY_MO_MESH'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'RADIAL_DENSITY_MO_BY_ATOM'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'RADIAL_DENSITY_MO_BY_ATOM_MESH'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'WEIGHTS'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'WEIGHTS_MESH'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'WEIGHTS_WAWB'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'WEIGHTS_WAWB_MESH'

      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'GRIDS'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_GRID_MESH'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'DENSITY_MO'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'MO_DENSITY_MESH'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'DENSITY_MO_BY_ATOM'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'DENSITY_MO_BY_ATOM_MESH'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'DEL_DENSITY'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'DEL_DENSITY_MESH'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'GRADLEN_DENSITY_OVER_DENSITY'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'GRADLEN_DENSITY_OVER_DENSITY_MESH'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'DEL2_DENSITY'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'DEL2_DENSITY_MESH'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'DEL_RADIAL_DENSITY'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'DEL_RADIAL_DENSITY_MESH'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'DEL_RADIAL_DENSITY'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'DEL_RADIAL_DENSITY_MESH'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'DEL_RADIAL_DENSITY'
      OBJ_GRID(NGRIDobjects)%modality = 'NUMERICAL'
      OBJ_GRID(NGRIDobjects)%routine = 'DEL_RADIAL_DENSITY_NUMERICAL'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'DEL2_RADIAL_DENSITY'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'DEL2_RADIAL_DENSITY_MESH'
      
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'PLOT'
      OBJ_GRID(NGRIDobjects)%modality = 'POINTS'
      OBJ_GRID(NGRIDobjects)%routine = 'plot_GRIDS'
      
!
! Modality=BOND
!      NGRIDobjects = NGRIDobjects + 1
!      OBJ_GRID(NGRIDobjects)%name = 'DENSITY'
!      OBJ_GRID(NGRIDobjects)%modality = 'BOND'
!      OBJ_GRID(NGRIDobjects)%routine = 'DENSITY_BOND'
      
! Modality=BOND
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'DENSITY'
      OBJ_GRID(NGRIDobjects)%modality = 'BOND'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_ENERGY_BOND_MESH("DENSITY")'   
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'DENSITY'
      OBJ_GRID(NGRIDobjects)%modality = 'ATOM'
      OBJ_GRID(NGRIDobjects)%routine = 'DENSITY_ATOM'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'RADIAL_DENSITY'
      OBJ_GRID(NGRIDobjects)%modality = 'BOND'
      OBJ_GRID(NGRIDobjects)%routine = 'RADIAL_DENSITY_BOND'
!
! Modality=SHELL
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'DENSITY'
      OBJ_GRID(NGRIDobjects)%modality = 'SHELL'
      OBJ_GRID(NGRIDobjects)%routine = 'Numerical_Integration_shell_energy("DENSITY")'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'PROGRIDATOM'
      OBJ_GRID(NGRIDobjects)%modality = 'SG1'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_ProMol_Atoms'
!
! LYP
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'LYP'
      OBJ_GRID(NGRIDobjects)%modality = 'SG1'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_LYP_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'LYP'
      OBJ_GRID(NGRIDobjects)%modality = 'BIG'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_LYP_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'LYP'
      OBJ_GRID(NGRIDobjects)%modality = 'GILL'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_LYP_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'LYP'
      OBJ_GRID(NGRIDobjects)%modality = 'BECKE'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_LYP_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'LYP'
      OBJ_GRID(NGRIDobjects)%modality = 'NTA'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_LYP_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'LYP'
      OBJ_GRID(NGRIDobjects)%modality = 'TA'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_LYP_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'LYP'
      OBJ_GRID(NGRIDobjects)%modality = 'SG0'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_LYP_GRID'
!
! COULOMB
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'COULOMB'
      OBJ_GRID(NGRIDobjects)%modality = 'SG1'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_COULOMB_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'COULOMB'
      OBJ_GRID(NGRIDobjects)%modality = 'BIG'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_COULOMB_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'COULOMB'
      OBJ_GRID(NGRIDobjects)%modality = 'GILL'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_COULOMB_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'COULOMB'
      OBJ_GRID(NGRIDobjects)%modality = 'BECKE'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_COULOMB_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'COULOMB'
      OBJ_GRID(NGRIDobjects)%modality = 'NTA'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_COULOMB_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'COULOMB'
      OBJ_GRID(NGRIDobjects)%modality = 'TA'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_COULOMB_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'COULOMB'
      OBJ_GRID(NGRIDobjects)%modality = 'SG0'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_COULOMB_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'COULOMB'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_COULOMB_MESH'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'COULOMB'
      OBJ_GRID(NGRIDobjects)%modality = 'SHELL'
      OBJ_GRID(NGRIDobjects)%routine = 'Numerical_Integration_shell_energy("COULOMB")'
!      
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'COULOMB'
      OBJ_GRID(NGRIDobjects)%modality = 'BOND'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_ENERGY_BOND_MESH("COULOMB")'
!
! COULOMB_MO
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'COULOMB_MO'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_COULOMB_MO_MESH'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'COULOMB_MO'
      OBJ_GRID(NGRIDobjects)%modality = 'BOND'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_ENERGY_BOND_MESH("MOCOULOMB")'
! 
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'COULOMB_MO'
      OBJ_GRID(NGRIDobjects)%modality = 'SHELL'
      OBJ_GRID(NGRIDobjects)%routine = 'Numerical_Integration_shell_energy("MOCOULOMB")'
!
! KINETIC
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'TPOS'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_TP_SPIN_MESH'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'TSCH'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_TS_SPIN_MESH'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'KINETIC'
      OBJ_GRID(NGRIDobjects)%modality = 'SG1'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_KINETIC_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'KINETIC'
      OBJ_GRID(NGRIDobjects)%modality = 'BIG'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_KINETIC_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'KINETIC'
      OBJ_GRID(NGRIDobjects)%modality = 'GILL'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_KINETIC_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'KINETIC'
      OBJ_GRID(NGRIDobjects)%modality = 'BECKE'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_KINETIC_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'KINETIC'
      OBJ_GRID(NGRIDobjects)%modality = 'NTA'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_KINETIC_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'KINETIC'
      OBJ_GRID(NGRIDobjects)%modality = 'TA'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_KINETIC_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'KINETIC'
      OBJ_GRID(NGRIDobjects)%modality = 'SG0'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_KINETIC_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'KINETIC'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_KINETIC_MESH'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'KINETIC'
      OBJ_GRID(NGRIDobjects)%modality = 'BOND'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_ENERGY_BOND_MESH("KINETIC")' 
! Modality=SHELL
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'KINETIC'
      OBJ_GRID(NGRIDobjects)%modality = 'SHELL'
      OBJ_GRID(NGRIDobjects)%routine = 'Numerical_Integration_shell_energy("KINETIC")'
!
! VNE
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'VNE'
      OBJ_GRID(NGRIDobjects)%modality = 'SG1'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_VNE_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'VNE'
      OBJ_GRID(NGRIDobjects)%modality = 'BIG'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_VNE_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'VNE'
      OBJ_GRID(NGRIDobjects)%modality = 'GILL'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_VNE_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'VNE'
      OBJ_GRID(NGRIDobjects)%modality = 'BECKE'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_VNE_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'VNE'
      OBJ_GRID(NGRIDobjects)%modality = 'NTA'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_VNE_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'VNE'
      OBJ_GRID(NGRIDobjects)%modality = 'TA'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_VNE_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'VNE'
      OBJ_GRID(NGRIDobjects)%modality = 'SG0'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_VNE_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'VNE'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      !OBJ_GRID(NGRIDobjects)%routine = 'BLD_VNE_MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_VNE_SPIN_MESH'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'VNE'
      OBJ_GRID(NGRIDobjects)%modality = 'BOND'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_ENERGY_BOND_MESH("VNE_")'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'VNE'
      OBJ_GRID(NGRIDobjects)%modality = 'SHELL'
      OBJ_GRID(NGRIDobjects)%routine = 'Numerical_Integration_shell_energy("VNE_")'
!
! VELOCITIES
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'VELOCITIES'
      OBJ_GRID(NGRIDobjects)%modality = 'SG1'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_VELOCITIES_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'VELOCITIES'
      OBJ_GRID(NGRIDobjects)%modality = 'BIG'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_VELOCITIES_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'VELOCITIES'
      OBJ_GRID(NGRIDobjects)%modality = 'GILL'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_VELOCITIES_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'VELOCITIES'
      OBJ_GRID(NGRIDobjects)%modality = 'BECKE'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_VELOCITIES_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'VELOCITIES'
      OBJ_GRID(NGRIDobjects)%modality = 'NTA'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_VELOCITIES_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'VELOCITIES'
      OBJ_GRID(NGRIDobjects)%modality = 'TA'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_VELOCITIES_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'VELOCITIES'
      OBJ_GRID(NGRIDobjects)%modality = 'SG0'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_VELOCITIES_GRID'
!
! EXCHANGE
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'EXCHANGE'
      OBJ_GRID(NGRIDobjects)%modality = 'SG1'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_EXCHANGE_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'EXCHANGE'
      OBJ_GRID(NGRIDobjects)%modality = 'BIG'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_EXCHANGE_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'EXCHANGE'
      OBJ_GRID(NGRIDobjects)%modality = 'GILL'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_EXCHANGE_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'EXCHANGE'
      OBJ_GRID(NGRIDobjects)%modality = 'BECKE'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_EXCHANGE_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'EXCHANGE'
      OBJ_GRID(NGRIDobjects)%modality = 'NTA'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_EXCHANGE_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'EXCHANGE'
      OBJ_GRID(NGRIDobjects)%modality = 'TA'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_EXCHANGE_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'EXCHANGE'
      OBJ_GRID(NGRIDobjects)%modality = 'SG0'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_EXCHANGE_GRID'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'EXCHANGE'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_EXCHANGE_MESH'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'EXCHANGE'
      OBJ_GRID(NGRIDobjects)%modality = 'BOND'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_ENERGY_BOND_MESH("EXCHANGE")' 
! Modality=SHELL
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'EXCHANGE'
      OBJ_GRID(NGRIDobjects)%modality = 'SHELL'
      OBJ_GRID(NGRIDobjects)%routine = 'Numerical_Integration_shell_energy("EXCHANGE")'   
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'ENERGY'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_ENERGYTOT_MESH'   
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'GRIDS'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_GRID_MESH'
!
! ABMOPRODUCT
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'ABMOPRODUCT'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_abMOproduct_MESH'
!
! JAB
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'JAB'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_JAB_MESH'
!
! JBA
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'JBA'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_JBA_MESH'
!
! JAB_AVER
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'JAB_AVER'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_JAB_AVERAGE_MESH'
!
! KAB
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'KAB'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_KAB_MESH'
!
! ELECTROSTATICPOTL
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'ELECTROSTATICPOTL'
      OBJ_GRID(NGRIDobjects)%modality = 'BOND'
      OBJ_GRID(NGRIDobjects)%routine = 'ELECTROSTATICPOTL_BOND'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'ELECTROSTATICPOTL'
      OBJ_GRID(NGRIDobjects)%modality = 'ATOM'
      OBJ_GRID(NGRIDobjects)%routine = 'ELECTROSTATICPOTL_ATOM'
!
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'ELECTROSTATICPOTL'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'ELECTROSTATICPOTL_MESH'
!
! Modality ABIM
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'LDMATRIX'
      OBJ_GRID(NGRIDobjects)%modality = 'ABIM'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_LDMatrix_ABIM'
!
! Modality QTAIM
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'LDMATRIX'
      OBJ_GRID(NGRIDobjects)%modality = 'QTAIM'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_LDMatrix_QTAIM'
!
! Modality=RADIAL
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'CRITICAL_POINTS'
      OBJ_GRID(NGRIDobjects)%modality = 'RADIAL'
      OBJ_GRID(NGRIDobjects)%routine = 'Find_CP_RADIAL'
!
! Modality=MESH
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'MBD'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_MBD_mesh'
!
! Modality=MESH
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'EAMD'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_EAMD_mesh'
!
! Modality=MESH
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'RAMD'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_RAMD_mesh'
!
! Modality=MESH
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'EXTRA'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_extra_mesh'
!
! Modality=MESH
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = 'VEE'
      OBJ_GRID(NGRIDobjects)%modality = 'MESH'
      OBJ_GRID(NGRIDobjects)%routine = 'BLD_ERpot_mesh'
!
! Dummy
      NGRIDobjects = NGRIDobjects + 1
      OBJ_GRID(NGRIDobjects)%name = '?'
      OBJ_GRID(NGRIDobjects)%modality = '?'
      OBJ_GRID(NGRIDobjects)%routine = '?'
!
      do Iobject=1,NGRIDobjects
        OBJ_GRID(Iobject)%exist=.false.
        OBJ_GRID(Iobject)%Current=.false.
      end do

      return
      end subroutine BLD_GRID_objects
