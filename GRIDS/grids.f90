      subroutine BLD_GRID_MESH
!*********************************************************************
!     Date last modified: February 18, 2000                          *
!     Authors: R.A. Poirier                                          *
!     Description:                                                   *
!                 Calculate the charge density at point.             *
!*********************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE module_grid_points
      USE matrix_print

      implicit none
!
! Local scalars:
      integer :: Ipoint,Ix,Iy,Iz
      double precision :: R11,R12,R13,R21,R22,R23,R31,R32,R33
      double precision, dimension(:,:), allocatable :: RMAT,grid_points_norotate,grid_points_rotate
!
! Begin:
      call PRG_manager ('enter','BLD_GRID_MESH', 'UTILITY')
!
      NGridPoints=(NX+1)*(NY+1)*(NZ+1)
      write(UNIout,'(a,i8)')'Total number of grid points: ',NGridPoints

      if(.not.allocated(grid_points))then
        allocate (grid_points(NgridPoints))
      else
        deallocate (grid_points)
        allocate (grid_points(NgridPoints))
      end if
      
      allocate (grid_points_norotate(NgridPoints,3))
      allocate (grid_points_rotate(NgridPoints,3))
!
! Define grid rotation matrix
! Extrinsic rotation with Euler angles alpha, beta, and gamma about x, y, and z axes.
      allocate (RMAT(3,3))
      RMAT(1,1)=dcos(Rgama)*dcos(Rbeta)
      RMAT(1,2)=dcos(Rgama)*dsin(Rbeta)*dsin(Ralpha)-dsin(Rgama)*dcos(Ralpha)
      RMAT(1,3)=dcos(Rgama)*dsin(Rbeta)*dcos(Ralpha)+dsin(Rgama)*dsin(Ralpha)
        
      RMAT(2,1)=dsin(Rgama)*dcos(Rbeta)
      RMAT(2,2)=dsin(Rgama)*dsin(Rbeta)*dsin(Ralpha)+dcos(Rgama)*dcos(Ralpha)
      RMAT(2,3)=dsin(Rgama)*dsin(Rbeta)*dcos(Ralpha)-dcos(Rgama)*dsin(Ralpha)
        
      RMAT(3,1)=-dsin(Rbeta)
      RMAT(3,2)=dcos(Rbeta)*dsin(Ralpha)
      RMAT(3,3)=dcos(Rbeta)*dcos(Ralpha)
      
      write(UNIout,'(a)') 'The Rotation matrix is:'
      call PRT_matrix(RMAT,3,3)
      
      Ipoint=0
      do Ix=0,NX 
      do Iy=0,NY
      do Iz=0,NZ
        Ipoint=Ipoint+1
        grid_points_norotate(Ipoint,1)=GRID_MIN(1)+Ix*GRID_MESH(1)
        grid_points_norotate(Ipoint,2)=GRID_MIN(2)+Iy*GRID_MESH(2)
        grid_points_norotate(Ipoint,3)=GRID_MIN(3)+Iz*GRID_MESH(3) 
      end do ! Iz
      end do ! Iy
      end do ! Ix
      
      grid_points_rotate=transpose(matmul(RMAT,transpose(grid_points_norotate)))
      
      grid_points(1:NgridPoints)%x=grid_points_rotate(1:NgridPoints,1)
      grid_points(1:NgridPoints)%y=grid_points_rotate(1:NgridPoints,2)
      grid_points(1:NgridPoints)%z=grid_points_rotate(1:NgridPoints,3)
      
      deallocate (grid_points_norotate,grid_points_rotate,RMAT)
!
! End of routine BLD_GRID_MESH
      call PRG_manager ('exit', 'BLD_GRID_MESH', 'UTILITY')
      return
      end subroutine BLD_GRID_MESH
      subroutine BLD_GRID_RADIAL
!******************************************************************************
!     Date last modified: July 21, 2014                                       *
!     Authors: R.A. Poirier                                                   *
!     Description: Generate a molecular grid using the atomic numerical grids *
!******************************************************************************
! Modules:
      USE program_files
      USE type_molecule
      USE module_grid_points
      USE NI_defaults

      implicit none
!
! Local scalars:
      integer :: Aatom,AGridpt,IAfound,Iatom,Igridpoint,NApts,ZA
!
! Begin:
      call PRG_manager ('enter','BLD_GRID_RADIAL', 'UTILITY')
!
!      call GET_object ('MOL', 'GRIDS', RADIAL_grid)
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
!
! Determine the total number of grid points
      NgridPoints=0
      do Iatom=1,Natoms
        ZA=CARTESIAN(Iatom)%Atomic_number
        if(ZA.le.0)cycle ! Skip dummy atoms
        IAfound=GRID_loc(ZA)
        NApts=NApoints_atom(IAfound)
        NgridPoints=NgridPoints+NApts
      end do ! Iatom

! Allocate the array
      if(.not.allocated(grid_points))then
        allocate (grid_points(NgridPoints))
      else
        deallocate (grid_points)
        allocate (grid_points(NgridPoints))
      end if

! Copy the atomic grid points to grid_points
      IgridPoint=0
      do Aatom=1,Natoms
        ZA=CARTESIAN(Aatom)%Atomic_number
        if(ZA.le.0)cycle
         IAfound=GRID_loc(ZA)
         NApts=NApoints_atom(IAfound)
        do AGridPt=1,NApts
          IgridPoint=IgridPoint+1
          grid_points(IgridPoint)%X=Egridpts(IAfound,AGridPt)%X+CARTESIAN(Aatom)%X
          grid_points(IgridPoint)%Y=Egridpts(IAfound,AGridPt)%Y+CARTESIAN(Aatom)%Y
          grid_points(IgridPoint)%Z=Egridpts(IAfound,AGridPt)%Z+CARTESIAN(Aatom)%Z
        end do ! NApts
      end do ! Aatom
!
! End of routine BLD_GRID_RADIAL
      call PRG_manager ('exit', 'BLD_GRID_RADIAL', 'UTILITY')
      return
      end subroutine BLD_GRID_RADIAL
