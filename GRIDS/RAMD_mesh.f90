      subroutine BLD_RAMD_mesh
!****************************************************************************************
!     Date last modified: April 26, 2019                                                *
!     Authors: JWH                                                                      *
!     Description: Calculate the momentum-balance density on a mesh.                    *
!****************************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE module_grid_points
      USE type_plotting
      USE type_Weights

      implicit none
!
! Local scalars:
      integer :: Ipoint
      double precision :: X,Y,Z
      double precision :: RAMaa_pt,RAMHFaa_pt
      double precision :: RAMbb_pt,RAMHFbb_pt
      double precision :: RAMab_pt,RAMHFab_pt
      double precision :: RAMba_pt,RAMHFba_pt
!
! Local array:
      double precision, allocatable,dimension(:) :: RAMaa, RAMHFaa
      double precision, allocatable,dimension(:) :: RAMbb, RAMHFbb
      double precision, allocatable,dimension(:) :: RAMab, RAMHFab
      double precision, allocatable,dimension(:) :: RAMba, RAMHFba
!
! Begin:
      call PRG_manager ('enter', 'BLD_RAMD_mesh', 'UTILITY')
!
      call GET_object ('QM', '2RDM', 'AO')
!
      allocate (RAMaa(1:NGridPoints))
      allocate (RAMHFaa(1:NGridPoints))
      allocate (RAMbb(1:NGridPoints))
      allocate (RAMHFbb(1:NGridPoints))
!
      allocate (RAMab(1:NGridPoints))
      allocate (RAMHFab(1:NGridPoints))
      allocate (RAMba(1:NGridPoints))
      allocate (RAMHFba(1:NGridPoints))
!
      RAMaa = zero
      RAMHFaa = zero
!
      RAMbb = zero
      RAMHFbb = zero
!
      RAMab = zero
      RAMHFab = zero
!
      RAMba = zero
      RAMHFba = zero
!
      ! grid_points may of been destroyed between MENU and here, so rebuild mesh
      ! Really should always be done just before use
      if(LMesh)then
        call BLD_GRID_MESH
      else
        call BLD_GRID_RADIAL
      end if
!
      do Ipoint = 1, NGridPoints
!
        X = grid_points(Ipoint)%x
        Y = grid_points(Ipoint)%y
        Z = grid_points(Ipoint)%z
!
! same-spin
        call GET_RAMDss_point (X, Y, Z, RAMaa_pt, RAMHFaa_pt, RAMbb_pt, RAMHFbb_pt)
!
        RAMaa(Ipoint) = RAMaa_pt
!
        RAMHFaa(Ipoint) = RAMHFaa_pt
!
        RAMbb(Ipoint) = RAMbb_pt
!
        RAMHFbb(Ipoint) = RAMHFbb_pt
!
! opposite-spin
        call GET_RAMDos_point (X, Y, Z, RAMab_pt, RAMHFab_pt, RAMba_pt, RAMHFba_pt)
!
        RAMab(Ipoint) = RAMab_pt
!
        RAMHFab(Ipoint) = RAMHFab_pt
!
        RAMba(Ipoint) = RAMba_pt
!
        RAMHFba(Ipoint) = RAMHFba_pt
!
      end do ! Ipoint
!
! Print the atom in molecule RAM density
!      if(AtomPrint)then
!        do Iatom=1,Natoms
!          Znum=CARTESIAN(Iatom)%Atomic_number
!          if(Znum.le.0)cycle
!          PlotFileID=' '
!          PlotFunction=' '
!          write(PlotFunction,'(a,i4)')'RAM for atom ',Iatom
!          write(PlotFileID,'(a)')'RAM_ATOMIC'
!          call BLD_plot_file_ATOMIC (PlotFileID, NGridPoints, Iatom, Plotfile_unit)
!          call PRT_MESH_ATOMIC (RAMTotal_atom, Iatom, Natoms, NGridPoints)
!          close (unit=Plotfile_unit)
!        end do ! Iatom
!      end if
!
! Print the RAM density 
! same-spin
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'RAMD_aa_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular RAM density'
        call PRT_GRID_MOL (RAMaa, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular RAM density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (RAMaa, NGridPoints, 'F21.14')
      end if
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'RAMD_HF_aa_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular RAM density'
        call PRT_GRID_MOL (RAMHFaa, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular RAM density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (RAMHFaa, NGridPoints, 'F21.14')
      end if
!
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'RAMD_bb_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular RAM density'
        call PRT_GRID_MOL (RAMbb, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular RAM density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (RAMbb, NGridPoints, 'F21.14')
      end if
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'RAMD_HF_bb_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular RAM density'
        call PRT_GRID_MOL (RAMHFbb, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular RAM density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (RAMHFbb, NGridPoints, 'F21.14')
      end if
!
! opposite-spin
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'RAMD_ab_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular RAM density'
        call PRT_GRID_MOL (RAMab, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular RAM density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (RAMab, NGridPoints, 'F21.14')
      end if
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'RAMD_HF_ab_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular RAM density'
        call PRT_GRID_MOL (RAMHFab, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular RAM density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (RAMHFab, NGridPoints, 'F21.14')
      end if
!
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'RAMD_ba_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular RAM density'
        call PRT_GRID_MOL (RAMba, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular RAM density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (RAMba, NGridPoints, 'F21.14')
      end if
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'RAMD_HF_ba_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular RAM density'
        call PRT_GRID_MOL (RAMHFba, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular RAM density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (RAMHFba, NGridPoints, 'F21.14')
      end if
!
! End of routine BLD_RAMD_mesh
      call PRG_manager ('exit', 'BLD_RAMD_mesh', 'UTILITY')
      return
      end subroutine BLD_RAMD_mesh
