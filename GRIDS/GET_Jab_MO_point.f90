      subroutine GET_Jab_MO_point (Xpt, Ypt, Zpt, aMO, bMO, Jab, Jba)
!*******************************************************************************
!     Date last modified:                                                      *
!     Authors: R.A. Poirier and I. Awad                                        *
!     Description:                                                             *
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE GSCF_work

      implicit none
!
      double precision :: Xpt,Ypt,Zpt

! Local scalars:
      integer :: aMO,bMO,NPaa,NPbb
      double precision :: PV_aa,S_aa,Jab,PV_bb,S_bb,Jba,JabAverage
!
! Local array:
      double precision, dimension(:), allocatable :: AOprod
      double precision, dimension(:), allocatable :: Vpotl
      type PM_xx
        integer :: IJ
        double precision :: Pxx
      end type
      type (PM_xx), dimension(:), allocatable :: PM_aa
      type (PM_xx), dimension(:), allocatable :: PM_bb

!
! Begin:
!
      allocate(PM_aa(MATlen))
      allocate(PM_bb(MATlen))
      allocate(AOprod(1:MATlen))
      allocate(Vpotl(1:MATlen))

      call I1E_Vint (Vpotl, AOprod, MATlen, Xpt, Ypt, Zpt)   

      call BLD_PM_Coulomb (CMO%coeff, Nbasis)
      call BLD_S_aa_PV_bb
      Jab=S_aa*PV_bb 
      Jba=S_bb*PV_aa  
      
      deallocate (AOprod)
      deallocate (Vpotl)
      deallocate (PM_aa)
      deallocate (PM_bb)

      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine BLD_PM_Coulomb (CMO, Nbasis)
!*******************************************************************************
!     Date last modified: March 15, 2011                                       *
!     Authors: R.A. Poirier                                                    *
!     Description: Build P_ab and P_ba                                         *
!*******************************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer :: Nbasis
      double precision :: CMO(Nbasis,Nbasis)
!
! Local scalars:
      integer :: Mu,Nu
      double precision :: Temp_aa,Temp_bb,threshold
      parameter (threshold=1.0D-12)   ! 10-06 Too large!!!
!
! Local function:
!
! Begin:
      call PRG_manager ('enter','BLD_PM_COULOMB', 'UTILITY')
!
      Temp_aa=ZERO
      Temp_bb=ZERO
      NPaa=0
      NPbb=0
      do Mu=1,Nbasis
      do Nu=1,Mu
        if(Mu.ne.Nu)then
          Temp_aa=TWO*CMO(Mu,aMO)*CMO(Nu,aMO)
          Temp_bb=TWO*CMO(Mu,bMO)*CMO(Nu,bMO)
        else
          Temp_aa=CMO(Mu,aMO)*CMO(Nu,aMO)
          Temp_bb=CMO(Mu,bMO)*CMO(Nu,bMO)
        end if
        if(dabs(Temp_aa).gt.threshold)then
          NPaa=NPaa+1
          PM_aa(NPaa)%Pxx=Temp_aa
          PM_aa(NPaa)%IJ=Mu*(Mu-1)/2+Nu
        end if
        if(dabs(Temp_bb).gt.threshold)then
          NPbb=NPbb+1
          PM_bb(NPbb)%Pxx=Temp_bb
          PM_bb(NPbb)%IJ=Mu*(Mu-1)/2+Nu
        end if
      end do ! Nu
      end do ! Mu
!
! End of routine BLD_PM_Coulomb
      call PRG_manager ('exit', 'BLD_PM_COULOMB', 'UTILITY')
      return
      end subroutine BLD_PM_Coulomb
      subroutine BLD_S_aa_PV_bb
!*******************************************************************************
!     Date last modified: March 15, 2011                                       *
!     Authors: R.A. Poirier                                                    *
!     Description:                                                             *
!     Calculate the charge density for each MO at point Xcoor, Ycoor, Zcoor    *
!*******************************************************************************
! Modules:

      implicit none
!
! Input scalars:
!
! Local scalars:
      integer :: IJ,Mu,Nu,IPMaa,IPMbb
!
! Local function:
!
! Begin:
      call PRG_manager ('enter','BLD_S_aa_PV_bb', 'UTILITY')
!
      S_aa=ZERO
      S_bb=ZERO
      PV_aa=ZERO
      PV_bb=ZERO
      do IPMaa=1,NPaa
        IJ=PM_aa(IPMaa)%IJ
        S_aa=S_aa+PM_aa(IPMaa)%Pxx*AOprod(IJ)
        PV_aa=PV_aa+PM_aa(IPMaa)%Pxx*Vpotl(IJ)
      end do
      do IPMbb=1,NPbb
        IJ=PM_bb(IPMbb)%IJ
        S_bb=S_bb+PM_bb(IPMbb)%Pxx*AOprod(IJ)
        PV_bb=PV_bb+PM_bb(IPMbb)%Pxx*Vpotl(IJ)
      end do
!
! End of routine BLD_S_aa_PV_bb
      call PRG_manager ('exit', 'BLD_S_aa_PV_bb', 'UTILITY')
      return
      end subroutine BLD_S_aa_PV_bb
! END CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine GET_Jab_MO_point 
