      subroutine MENU_grids
!***********************************************************************
!     Date last modified: February 12, 2003               Version 2.0  *
!     Authors: G. Jansen & J. G. Angyan, R.A. Poirier                  *
!     Desciption: set-up menu for Grid command                         *
!***********************************************************************
! Modules:
      USE program_constants
      USE program_parser
      USE menu_gets
      USE module_grid_points
      USE type_molecule

      implicit none
!
! Local scalars:
      integer :: lenlist,lenstr,Iatom
      integer, parameter :: MAX_GridPoints=100000
      logical :: LRadial,LPoints,LPolar,LNuclei,done
      type(type_grid_points), dimension(:), allocatable :: temp_grid
!
! Local arrays:
      double precision :: XYZ(3)
!
! Begin:
      call PRG_manager ('enter', 'MENU_grids', 'UTILITY')
!
! Defaults:
! Menu:
      LMesh=.false.
      LRadial=.false.
      LPoints=.false.
      LPolar=.false.
      LNuclei=.false.
      done=.false.

      NGridPoints=0
      NX=0
      NY=0
      NZ=0

      allocate (temp_grid(MAX_GridPoints))

      do while (.not.done)
      call new_token ('GRIDs:')
!
      if(token(1:4).eq.'HELP')then
      write(UNIout,'(a)') &
      ' Command "GRIDS":                              ', &
      '   Define a grid for computing density, ...      ', &
      '   Syntax : GRIDs                                ', &
      '   MAx_r < real > Maximum radius used for LYP', &
      '   MEsh < command > Command to specify gird on a MESH or at a set of points', &
      '   POInt = <real list>, coordinate of property', &
      '   POInt = ( 0.0 0.0 0.0 ) {default}', &
      '   POLar < command > Command to specify Polar grid', &
      '   RAdial < command > Command to specify RADIAL grid', &
      '   REgions < integer > Number of regions used for LYP', &
      '   NUclei < logical > Logical, if true the density is calculated at all the nuclear coordiantes', &
      '   end'

! MAx_r
      else if(token(1:2).eq.'MA')then
        call GET_value (MAX_r)

! MEsh
      else if(token(1:2).eq.'ME')then
        call MENU_MESH
        LMesh=.true.

! NUclei
      else if(token(1:2).eq.'NU')then
        call GET_value (LNuclei)
        LNuclei=.true.

! POInt
      else if(token(1:3).EQ.'POI')then
        call GET_value (XYZ, lenlist)
        write(UNIout,'(a,3F15.6)')' Point set to: ',XYZ
! Set NX, NY and NZ to 1 to make sure printed correctly
        NX=1
        NY=1
        NZ=1
        NGridPoints=NGridPoints+1
        temp_grid(NGridPoints)%x=XYZ(1)
        temp_grid(NGridPoints)%y=XYZ(2)
        temp_grid(NGridPoints)%z=XYZ(3)
        LPoints=.true.

! RAdial
      else if(token(1:2).eq.'RA')then
        call MENU_RADIAL
        LRadial=.true.
 
! REgions
      else if(token(1:2).eq.'RE')then
        call GET_value (NRegions)
      else
         call MENU_end (done)
      end if

      end do ! while

      if(LRadial)then
       call BLD_GRID_RADIAL ! Not sure this is needed?????
      else if(LMesh)then
        call BLD_GRID_MESH
      else if(LPoints.and.NGridPoints.gt.0)then
        write(UNIout,'(a,i8)')'Total number of grid points: ',NGridPoints
        if(.not.allocated (grid_points))then
          allocate (grid_points(NGridPoints))
        else
          deallocate (grid_points)
          allocate (grid_points(NGridPoints))
        end if
        grid_points(1:NGridPoints)=temp_grid(1:NGridPoints)
      end if

      if(LNuclei)then
        NX=1
        NY=1
        NZ=1
        allocate (grid_points(NReal_atoms))
        NGridPoints=0
        do Iatom=1,Natoms
          if(CARTESIAN(Iatom)%Atomic_Number.le.0)cycle
          NGridPoints=NGridPoints+1
          grid_points(NGridPoints)%x=CARTESIAN(Iatom)%x
          grid_points(NGridPoints)%y=CARTESIAN(Iatom)%y
          grid_points(NGridPoints)%z=CARTESIAN(Iatom)%z
        end do
        write(UNIout,'(a,i8)')'Total number of grid points: ',NGridPoints
        write(UNIout,'(a,i8)')'The density will be calculated at all the nuclei'
      end if
!
! end of routine  MENU_grids
      call PRG_manager ('exit', 'MENU_grids', 'UTILITY')
      return
      end subroutine MENU_grids
