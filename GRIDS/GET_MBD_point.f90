      subroutine GET_MBDss_point (Xpt, Ypt, Zpt, MBaa_grid, MBHFaa_grid, MBbb_grid, MBHFbb_grid)
!*******************************************************************************
!     Date last modified: April 24 2019                                        *
!     Authors: JWH                                                             *
!     Description: Compute the momentum-balance density at a point.            *
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE INT_objects

      implicit none
!
! Input scalars:
      double precision, intent(IN) :: Xpt,Ypt,Zpt
!
! Output scalars:
      double precision, intent(OUT) :: MBaa_grid,MBHFaa_grid
      double precision, intent(OUT) :: MBbb_grid,MBHFbb_grid
!
! Local scalars:
      double precision :: rv(3)
!           
! Begin:
!
      call GET_object ('QM', '2RDM', 'AO')
!    
      MBaa_grid=ZERO
      MBHFaa_grid=ZERO        
!
      MBbb_grid=ZERO
      MBHFbb_grid=ZERO        
!
      rv(1) = Xpt
      rv(2) = Ypt
      rv(3) = Zpt
!
      call BLD_MBDint (rv)
!
      MBaa_grid = dot_product(TRDM_MBaa,emdAO)
      MBHFaa_grid = dot_product(TRDM_HF_MBaa,emdAO)
!
      MBbb_grid = dot_product(TRDM_MBbb,emdAO)
      MBHFbb_grid = dot_product(TRDM_HF_MBbb,emdAO)
!
      return
      end subroutine GET_MBDss_point 
      subroutine GET_MBDos_point (Xpt, Ypt, Zpt, MBab_grid, MBHFab_grid, MBba_grid, MBHFba_grid)
!*******************************************************************************
!     Date last modified: April 24 2019                                        *
!     Authors: JWH                                                             *
!     Description: Compute the momentum-balance density at a point.            *
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE INT_objects

      implicit none
!
! Input scalars:
      double precision, intent(IN) :: Xpt,Ypt,Zpt
!
! Output scalars:
      double precision, intent(OUT) :: MBab_grid,MBHFab_grid
      double precision, intent(OUT) :: MBba_grid,MBHFba_grid
!
! Local scalars:
      double precision :: rv(3)
!           
! Begin:
!
      call GET_object ('QM', '2RDM', 'AO')
!    
      MBab_grid=ZERO
      MBHFab_grid=ZERO        
!
      MBba_grid=ZERO
      MBHFba_grid=ZERO        
!
      rv(1) = Xpt
      rv(2) = Ypt
      rv(3) = Zpt
!
      call BLD_MBDint (rv)
!
      MBab_grid = dot_product(TRDM_MBab,emdAO)
      MBHFab_grid = dot_product(TRDM_HF_MBab,emdAO)
!
      MBba_grid = dot_product(TRDM_MBba,emdAO)
      MBHFba_grid = dot_product(TRDM_HF_MBba,emdAO)
!
      return
      end subroutine GET_MBDos_point 
