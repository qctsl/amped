      subroutine BLD_SG0_GRID
!************************************************************************************
!     Date last modified Sept 26th ,2001                                            *
!     Author: Aisha                                                                 *
!     Description:                                                                  *
!************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE type_elements
      USE type_radial_grids
      USE NI_defaults
      USE module_grid_points
      USE QM_objects
      USE N_integration
      USE MultiExp_parameters

      implicit none
!
! Local scalars:
      integer :: IApoint,Iatom,Z_num,NA,NR,NA1,NR1,NA2,NR2
      integer :: Ifound,MAXpts,NA_sphere,IR,NApt,NRpt,KRpoint
      integer :: JRpoint,JApoint
      double precision, dimension(:,:), allocatable :: Rweights

! Local parameters:
     external :: Angular_grid6,Angular_grid38,Angular_grid86,Angular_grid194,Angular_grid50,Angular_grid110,&
                 Angular_grid64,Angular_grid302,Angular_gridBench,Angular_grid26,Angular_grid74,&
                 Angular_grid146,Angular_grid170
!    external :: Angular_grid18 ! Does not work!!
! Begin:
      call PRG_manager ('enter', 'BLD_SG0_GRID', 'UTILITY')

      RADIAL_grid='SG0'
      LSG0=.true.

      do Iatom = 1, Natoms
        if(CARTESIAN(Iatom)%Atomic_number.GT.18)then
        write(UNIout,'(a)')'ERROR> BLD_SG0_GRID: Parameters available only up to Argon'
        stop 'ERROR> BLD_SG0_GRID: Parameters available only up to Argon'
        end if
      end do

      call BLD_BeckeW_XI
      call BLD_grid_location ! Defines Nfound and LZfound

      MAXpts = 1536   ! Maximum number of points 
      allocate(Egridpts(Nfound,MAXpts))

      allocate(Rweights(Nfound,MAXpts))
      allocate(NApoints_atom(Nfound),R_value(Nelements))
      R_value(1:18) =(/1.30,1.20,1.95,2.20,1.45,1.20,1.10,1.10,1.20,1.10,2.30,2.20,2.10,1.30,1.30,1.10,1.45,1.4/)

      do Z_num=1,Nelements
        if(.not.LZfound(Z_num))cycle
        Ifound=GRID_loc(Z_num)
          if(Z_num .gt.0 .and. Z_num.le.10)then
            NRpoints = 23
            allocate(Radial_points(Nfound,NRpoints),Radial_weights(Nfound,NRpoints))
            do KRpoint =1,NRpoints
              Radial_points(Ifound,KRpoint)=R_value(Z_num)*X_points_23(KRpoint)
              Radial_weights(Ifound,KRpoint)=a_values_23(KRpoint)*R_value(Z_num)**3
            end do !KRpoint

          else if(Z_num .gt.10 .and. Z_num.le.18)then
            NRpoints = 26
            allocate(Radial_points(Nfound,NRpoints),Radial_weights(Nfound,NRpoints))
            do KRpoint =1,NRpoints
              Radial_points(Ifound,KRpoint)=R_value(Z_num)*X_points_26(KRpoint)
              Radial_weights(Ifound,KRpoint)=a_values_26(KRpoint)*R_value(Z_num)**3
            end do !KRpoint
          end if

           select case(Z_num)

           CASE(1:3)   !>>>>>>>>>>>>>>>>H, He, Li
           NR2 = 0
           NA2 = 0
           NApt = 0
           NRpt = 0
           JApoint = 1
           JRpoint = 1
           NRpoints = 23
           NApts_atom = 1438
           NApoints_atom(Ifound) = NApts_atom

           allocate(grid_points(NApts_atom))

           NR = 6
           NA_sphere = 6
           NA = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid6)
           NR  = 3
           NA_sphere = 26
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid26)
           NR  = 1
           NA_sphere = 26
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid26)         
           NR  = 1
           NA_sphere = 38
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid38)         
           NR  = 1
           NA_sphere = 74
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid74)         
           NR  = 1
           NA_sphere = 110
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid110)         
           NR  = 6
           NA_sphere = 146          
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid146)         
           NR  = 1
           NA_sphere = 86
           NA  = NR*NA_sphere 
           call BLD_SG0_ANG(Angular_grid86)         
           NR  = 1
           NA_sphere = 50
           NA  = 50
           call BLD_SG0_ANG(Angular_grid50)         
           NR  = 1
           NA_sphere = 38
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid38)         
           NR  = 1
           NA_sphere = 26
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid26)         
             write(6,'(a,X,I4,X,I6)')' NR2,NA2',NR2,NA2
           if(NApt.ne.NApoints_atom(Ifound))then
             stop 'ERROR> NIM_SG0: NApoints is different than the one given by SG0 paper'
           end if

           CASE(4)   !>>>>>>>>>>>>>>>>Be
           NR2 = 0
           NA2 = 0
           NApt = 0
           NRpt = 0
           JApoint = 1
           JRpoint = 1
           NRpoints = 23
           NApts_atom = 1414
           NApoints_atom(Ifound) = NApts_atom
           allocate(grid_points(NApts_atom))
           NR = 4
           NA_sphere = 6
           NA = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid6)         
           NR  = 2
           NA_sphere = 26
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid26)         
           NR  = 1
           NA_sphere = 26
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid26)         
           NR  = 2
           NA_sphere = 38
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid38)         
           NR  = 1
           NA_sphere = 74
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid74)         
           NR  = 1
           NA_sphere = 86
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid86)         
           NR  = 2
           NA_sphere = 110
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid110)         
           NR  = 5
           NA_sphere = 146
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid146)         
           NR  = 1
           NA_sphere = 50
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid50)         
           NR  = 1
           NA_sphere = 38
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid38)         
           NR  = 1
           NA_sphere = 26
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid26)         
           NR  = 2
           NA_sphere = 6
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid6)         
           write(6,'(a,X,I4,X,I6)')' NR2,NA2',NR2,NA2
           if(NApt.ne.NApoints_atom(Ifound))then
             stop 'ERROR> NIM_SG0: NApoints is different than the one given by SG0 paper'
           end if

           CASE(5)   !>>>>>>>>>>>>>>>>B
           NR2 = 0
           NA2 = 0
           NApt = 0
           NRpt = 0
           JApoint = 1
           JRpoint = 1
           NRpoints = 23
           NApts_atom = 1426
           NApoints_atom(Ifound) = NApts_atom
           allocate(grid_points(NApts_atom))
           NR = 4
           NA_sphere = 6
           NA = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid6)
           NR  = 4
           NA_sphere = 26
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid26)
           NR  = 3
           NA_sphere = 38
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid38)         
           NR  = 3
           NA_sphere = 86
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid86)         
           NR  = 6
           NA_sphere = 146          
           NA = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid146)         
           NR  = 1
           NA_sphere = 38
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid38)         
           NR  = 2
           NA_sphere = 6
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid6)         
           write(6,'(a,X,I4,X,I6)')' NR2,NA2',NR2,NA2
           if(NApt.ne.NApoints_atom(Ifound))then
         stop 'ERROR> NIM_SG0: NApoints is different than the one given by SG0 paper'
         end if

           CASE(6)   !>>>>>>>>>>>>>>>> C
           NR2 = 0
           NA2 = 0
           NApt = 0
           NRpt = 0
           JApoint = 1
           JRpoint = 1
           NRpoints = 23
           NApts_atom = 1414
           NApoints_atom(Ifound) = NApts_atom
           allocate(grid_points(NApts_atom))
           NR = 6
           NA_sphere = 6
           NA = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid6)         
           NR  = 2
           NA_sphere = 26
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid26)         
           NR  = 1
           NA_sphere = 26
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid26)         
           NR  = 2
           NA_sphere = 38
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid38)         
           NR  = 2
           NA_sphere = 50
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid50)         
           NR  = 1
           NA_sphere = 86
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid86)         
           NR  = 1
           NA_sphere = 110
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid110)         
           NR  = 1
           NA_sphere = 146
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid146)         
           NR  = 2
           NA_sphere = 170
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid170)         
           NR  = 2
           NA_sphere = 146
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid146)         
           NR  = 1
           NA_sphere = 86
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid86)         
           NR  = 1
           NA_sphere = 38
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid38)         
           NR  = 1
           NA_sphere = 26
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid26)         
           write(6,'(a,X,I4,X,I6)')' NR2,NA2',NR2,NA2
           if(NApt.ne.NApoints_atom(Ifound))then
             stop 'ERROR> NIM_SG0: NApoints is different than the one given by SG0 paper'
           end if

           CASE(7)   !>>>>>>>>>>>>>>>N
           NR2 = 0
           NA2 = 0
           NApt = 0
           NRpt = 0
           JApoint = 1
           JRpoint = 1
           NRpoints = 23
           NApts_atom = 1438
           NApoints_atom(Ifound) = NApts_atom
           allocate(grid_points(NApts_atom))
           NR = 6
           NA_sphere = 6
           NA = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid6)         
           NR  = 3
           NA_sphere = 26
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid26)         
           NR  = 1
           NA_sphere = 26
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid26)         
           NR  = 2
           NA_sphere = 38
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid38)         
           NR  = 2
           NA_sphere = 74
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid74)         
           NR  = 1
           NA_sphere = 110
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid110)         
           NR  = 2
           NA_sphere = 170
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid170)         
           NR  = 3
           NA_sphere = 146
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid146)         
           NR  = 1
           NA_sphere = 86
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid86)         
           NR  = 2
           NA_sphere = 50
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid50)         
           write(6,'(a,X,I4,X,I6)')' NR2,NA2',NR2,NA2
           if(NApt.ne.NApoints_atom(Ifound))then
             stop 'ERROR> NIM_SG0: NApoints is different than the one given by SG0 paper'
           end if

           CASE(8)   !>>>>>>>>>>>>>>> O
           NR2 = 0
           NA2 = 0
           NApt = 0
           NRpt = 0
           JApoint = 1
           JRpoint = 1
           NRpoints = 23
           NApts_atom = 1162
           NApoints_atom(Ifound) = NApts_atom
           allocate(grid_points(NApts_atom))
           NR = 5
           NA_sphere = 6
           NA = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid6)         
           NR  = 1
           NA_sphere = 26
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid26)         
           NR  = 2
           NA_sphere = 26
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid26)         
           NR  = 1
           NA_sphere = 38
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid38)         
           NR  = 4
           NA_sphere = 50
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid50)         
           NR  = 1
           NA_sphere = 86
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid86)         
           NR  = 5
           NA_sphere = 110
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid110)         
           NR  = 1
           NA_sphere = 86
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid86)         
           NR  = 1
           NA_sphere = 50
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid50)         
           NR  = 1
           NA_sphere = 38
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid38)         
           NR  = 1
           NA_sphere =6
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid6)         
           write(6,'(a,X,I4,X,I6)')' NR2,NA2',NR2,NA2
           if(NApt.ne.NApoints_atom(Ifound))then
             stop 'ERROR> NIM_SG0: NApoints is different than the one given by SG0 paper'
           end if

           CASE(9)   !>>>>>>>>>>>> F
           NR2 = 0
           NA2 = 0
           NApt = 0
           NRpt = 0
           JApoint = 1
           JRpoint = 1
           NRpoints = 23
           NApts_atom = 1494
           NApoints_atom(Ifound) = NApts_atom
           allocate(grid_points(NApts_atom))
           NR = 4
           NA_sphere = 6
           NA = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid6)         
           NR  = 2
           NA_sphere = 38
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid38)         
           NR  = 4
           NA_sphere = 50
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid50)         
           NR  = 2
           NA_sphere = 74
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid74)         
           NR  = 2
           NA_sphere = 110
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid110)         
           NR  = 2
           NA_sphere = 146
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid146)         
           NR  = 2
           NA_sphere = 110
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid110)         
           NR  = 3
           NA_sphere = 86
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid86)         
           NR  = 1
           NA_sphere = 50
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid50)         
           NR  = 1
           NA_sphere = 6
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid6)         
           write(6,'(a,X,I4,X,I6)')' NR2,NA2',NR2,NA2
           if(NApt.ne.NApoints_atom(Ifound))then
             stop 'ERROR> NIM_SG0: NApoints is different than the one given by SG0 paper'
           end if
           CASE(11)   !>>>>>>>>>>>>>>>> Na
           NR2 = 0
           NA2 = 0
           NApt = 0
           NRpt = 0
           JApoint = 1
           JRpoint = 1
           NRpoints = 26
           NApts_atom = 1344
           NApoints_atom(Ifound) = NApts_atom
           allocate(grid_points(NApts_atom))
           NR = 6
           NA_sphere = 6
           NA = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid6)         
           NR  = 5 
           NA_sphere = 26
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid26)         
           NR  = 1
           NA_sphere = 38
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid38)         
           NR  = 2
           NA_sphere = 50
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid50)         
           NR  = 8
           NA_sphere = 110
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid110)         
           NR  = 2
           NA_sphere = 74
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid74)         
           NR  = 2
           NA_sphere = 6
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid6)         
           if(NApt.ne.NApoints_atom(Ifound))then
             stop 'ERROR> NIM_SG0: NApoints is different than the one given by SG0 paper'
           end if
           write(6,'(a,X,I4,X,I6)')' NR2,NA2',NR2,NA2

           CASE(12)   !>>>>>>>>>>>>>>>>Mg
           NR2 = 0
           NA2 = 0
           NApt = 0
           NRpt = 0
           JApoint = 1
           JRpoint = 1
           NRpoints = 26
           NApts_atom = 1492
           NApoints_atom(Ifound) = NApts_atom
           allocate(grid_points(NApts_atom))
           NR = 5
           NA_sphere = 6
           NA = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid6)         
           NR  = 4
           NA_sphere = 26
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid26)         
           NR  = 2
           NA_sphere = 38
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid38)         
           NR  = 2
           NA_sphere = 50
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid50)         
           NR  = 1
           NA_sphere = 74
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid74)         
           NR  = 2
           NA_sphere = 110
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid110)         
           NR  = 4
           NA_sphere = 146
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid146)         
           NR  = 1
           NA_sphere = 110
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid110)         
           NR  = 1
           NA_sphere = 86
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid86)         
           NR  = 2
           NA_sphere = 38
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid38)         
           NR  = 1
           NA_sphere = 26
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid26)         
           NR  = 1
           NA_sphere = 6
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid6)         
           write(6,'(a,X,I4,X,I6)')' NR2,NA2',NR2,NA2
           if(NApt.ne.NApoints_atom(Ifound))then
             stop 'ERROR> NIM_SG0: NApoints is different than the one given by SG0 paper'
           end if

           CASE(13)   !>>>>>>>>>>>>>>>>Al
           NR2 = 0
           NA2 = 0
           NApt = 0
           NRpt = 0
           JApoint = 1
           JRpoint = 1
           NRpoints = 26
           NApts_atom = 1520
           NApoints_atom(Ifound) = NApts_atom
           allocate(grid_points(NApts_atom))
           NR = 6
           NA_sphere = 6
           NA = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid6)         
           NR  = 3
           NA_sphere = 26
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid26)         
           NR  = 2
           NA_sphere = 38
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid38)         
           NR  = 2
           NA_sphere = 50
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid50)         
           NR  = 1
           NA_sphere = 74
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid74)         
           NR  = 1
           NA_sphere = 86
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid86)         
           NR  = 2
           NA_sphere = 146
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid146)         
           NR  = 2
           NA_sphere = 170
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid170)         
           NR  = 2
           NA_sphere = 110
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid110)         
           NR  = 1
           NA_sphere = 86
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid86)         
           NR  = 1
           NA_sphere = 74
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid74)         
           NR  = 2
           NA_sphere = 26
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid26)         
           NR  = 1
           NA_sphere = 6
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid6)         

           write(6,'(a,X,I4,X,I6)')' NR2,NA2',NR2,NA2
           if(NApt.ne.NApoints_atom(Ifound))then
             stop 'ERROR> NIM_SG0: NApoints is different than the one given by SG0 paper'
           end if

           CASE(14:15)   !>>>>>>>>>>>>>>>>Si,P
           NR2 = 0
           NA2 = 0
           NApt = 0
           NRpt = 0
           JApoint = 1
           JRpoint = 1
           NRpoints = 26
           NApts_atom = 1528
           NApoints_atom(Ifound) = NApts_atom
           allocate(grid_points(NApts_atom))
           NR = 5
           NA_sphere = 6
           NA = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid6)         
           NR  = 4
           NA_sphere = 26
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid26)         
           NR  = 4
           NA_sphere = 38
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid38)         
           NR  = 3
           NA_sphere = 50
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid50)         
           NR  = 1
           NA_sphere = 74
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid74)         
           NR  = 2
           NA_sphere = 110
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid110)         
           NR  = 1
           NA_sphere = 146
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid146)         
           NR  = 3
           NA_sphere = 170
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid170)         
           NR  = 1
           NA_sphere = 86
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid86)         
           NR  = 1
           NA_sphere = 50
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid50)         
           NR  = 1
           NA_sphere =6
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid6)         

           write(6,'(a,X,I4,X,I6)')' NR2,NA2',NR2,NA2
           if(NApt.ne.NApoints_atom(Ifound))then
             stop 'ERROR> NIM_SG0: NApoints is different than the one given by SG0 paper'
           end if

           CASE(16)   !>>>>>>>>>>>>>>>>S
           NR2 = 0
           NA2 = 0
           NApt = 0
           NRpt = 0
           JApoint = 1
           JRpoint = 1
           NRpoints = 26
           NApts_atom = 1464
           NApoints_atom(Ifound) = NApts_atom
           allocate(grid_points(NApts_atom))
           NR = 4
           NA_sphere = 6
           NA = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid6)         
           NR  = 9
           NA_sphere = 26
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid26)         
           NR  = 2
           NA_sphere = 38
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid38)         
           NR  = 1
           NA_sphere = 50
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid50)         
           NR  = 2
           NA_sphere = 74
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid74)         
           NR  = 1
           NA_sphere = 110
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid110)         
           NR  = 3
           NA_sphere = 170
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid170)         
           NR  = 1
           NA_sphere = 146
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid146)         
           NR  = 1
           NA_sphere = 110
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid110)         
           NR  = 1
           NA_sphere = 50
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid50)         
           NR  = 1
           NA_sphere =6
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid6)         

           write(6,'(a,X,I4,X,I6)')' NR2,NA2',NR2,NA2
           if(NApt.ne.NApoints_atom(Ifound))then
             stop 'ERROR> NIM_SG0: NApoints is different than the one given by SG0 paper'
           end if

           CASE(17)   !>>>>>>>>>>>>>>>>Cl
           NR2 = 0
           NA2 = 0
           NApt = 0
           NRpt = 0
           JApoint = 1
           JRpoint = 1
           NRpoints = 26
           NApts_atom = 1536
           NApoints_atom(Ifound) = NApts_atom
           allocate(grid_points(NApts_atom))
           NR = 4
           NA_sphere = 6
           NA = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid6)         
           NR  = 9
           NA_sphere = 26
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid26)         
           NR  = 2
           NA_sphere = 38
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid38)         
           NR  = 1
           NA_sphere = 50
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid50)         
           NR  = 1
           NA_sphere = 74
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid74)         
           NR  = 2
           NA_sphere = 110
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid110)         
           NR  = 3
           NA_sphere = 170
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid170)         
           NR  = 1
           NA_sphere = 146
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid146)         
           NR  = 1
           NA_sphere = 110
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid110)         
           NR  = 1
           NA_sphere = 86
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid86)         
           NR  = 1
           NA_sphere =6
           NA  = NR*NA_sphere
           call BLD_SG0_ANG(Angular_grid6)         

           write(6,'(a,X,I4,X,I6)')' NR2,NA2',NR2,NA2
           if(NApt.ne.NApoints_atom(Ifound))then
             stop 'ERROR> NIM_SG0: NApoints is different than the one given by SG0 paper'
           end if

        case default
          write(UNIout,'(a,i8)')'SG0 grid not available for atom: ',Z_num
          stop
        end select

      call RAweights (Ifound)
      Egridpts(Ifound,1:NApts_atom)= grid_points(1:NApts_atom)
      do IApoint=1,NApts_atom
        Egridpts(Ifound,IApoint)%w=Egridpts(Ifound,IApoint)%w*Rweights(Ifound,IApoint)
      end do ! IApoint

      deallocate (grid_points)

      deallocate (Radial_points, Radial_weights)
      end do ! Z_num

CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine BLD_SG0_ANG (Angular_gridN)
!************************************************************************************
!     Date last modified Sept 26th ,2001                                            *
!     Author: Aisha                                                                 *
!     Description:                                                                  *
!************************************************************************************

      implicit none
!
! Local parameters:
      integer :: IRpoint,IApoint
      double precision :: Rtemp
      external :: Angular_gridN
! Begin:
      call PRG_manager ('enter', 'BLD_SG0_ANG', 'UTILITY')

      NR1 = NR2+1
      NA1 = NA2+1           
      NR2 = NR1+NR-1
      NA2 = NA1+NA-1
      Rpoints(1:NR) = Radial_points(Ifound,NR1:NR2)
      allocate(Apoints_temp(NA))
      call Angular_gridN (Rpoints, NR, NA, Apoints_temp)
      grid_points(NA1:NA2)= Apoints_temp(1:NA)
      deallocate(Apoints_temp)
      NRpt = NRpt + NR
      do IRpoint = JRpoint,NRpt
        Rtemp=Radial_weights(Ifound,IRpoint)
        NApt = NApt + NA_sphere
        do IApoint = JApoint, NApt
          Rweights(Ifound,IApoint)=Rtemp
        end do
        JApoint = NApt+1
      end do
      JRpoint = NRpt+1               
      call PRG_manager ('exit', 'BLD_SG0_ANG', 'UTILITY')
      return
      end  subroutine BLD_SG0_ANG
      end subroutine BLD_SG0_GRID
