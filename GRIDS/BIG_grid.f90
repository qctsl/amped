      subroutine BLD_BIG_GRID
!************************************************************************************
!     Date last modified Sept 26th ,2001                                            *
!     Author: Aisha                                                                 *
!     Description:                                                                  *
!************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE type_radial_grids
      USE NI_defaults
      USE module_grid_points
      USE QM_objects
      USE N_integration

      implicit none
!
! Local scalars:
      integer :: KRpoint,IApoint,Iatom,Z_num,NA1,NA2,NR1,NR2
      integer :: Ifound,NApoints
      double precision :: Radius

! Local parameters:

! Begin:
      call PRG_manager ('enter', 'BLD_BIG_GRID', 'UTILITY')

      RADIAL_grid='BIG'

       do Iatom = 1, Natoms
         if(CARTESIAN(Iatom)%Atomic_number.GT.18)then
         write(UNIout,'(a)')'ERROR> BLD_BIG_GRID: Parameters available only up to Argon'
         stop 'ERROR> BLD_BIG_GRID: Parameters available only up to Argon'
         end if
       end do

      call BLD_BeckeW_XI
      call BLD_grid_location

      NRpoints = 100
      NApoints = 100*1202
      write(UNIout,'(a,i8)')'Number of angular points: ',NApoints
      num_regions = 1
      if(.not.allocated(grid_points))then
        allocate(grid_points(NApoints))
      else
        deallocate(grid_points)
        allocate(grid_points(NApoints))
      end if
      if(.not.allocated(Nap_grid))then
        allocate(Nap_grid(Nfound,num_regions), NRpoints_region(Nfound,num_regions), NApoints_atom(Nfound))
        allocate(Radial_points(Nfound,100), Radial_weights(Nfound,100))
        allocate(Egridpts(Nfound,NApoints))
      else
        deallocate(Nap_grid, NRpoints_region, NApoints_atom)
        deallocate(Radial_points, Radial_weights)
        deallocate(Egridpts)
        allocate(Nap_grid(Nfound,num_regions), NRpoints_region(Nfound,num_regions), NApoints_atom(Nfound))
        allocate(Radial_points(Nfound,100), Radial_weights(Nfound,100))
        allocate(Egridpts(Nfound,NApoints))
      end if

      Nap_grid(1:Nfound,1) = 1202

       Ifound = 0
      do Z_num=1,18
        if(.not.LZfound(Z_num))cycle
           Ifound=GRID_loc(Z_num)

      do KRpoint= 1,NRpoints
        Radial_points(Ifound,KRpoint)= Radii(Z_num)*dble(KRpoint)**2/dble(NRpoints+1-KRpoint)**2
        Radial_weights(Ifound,KRpoint)=(TWO*(Radii(Z_num)**THREE)*dble(NRpoints+1)* &
        (dble(KRpoint)**5))/(dble(NRpoints+1-KRpoint)**7)
!       write(6,*)'i,ri ',KRpoint,Radial_points(Ifound,KRpoint)
      end do

      NRpoints_region(Ifound,1) = 100
      NApoints_atom(Ifound) = NApoints
      NApts_atom=NApoints_atom(Ifound)

      Rpoints(1:100) = Radial_points(Ifound,1:100)
      allocate(Apoints_temp(NApoints))
      call Angular_gridBench(Rpoints, 100, NApoints, Apoints_temp)
      grid_points(1:NApoints)= Apoints_temp(1:NApoints)
      deallocate(Apoints_temp)

      call RAweights (Ifound)
      Egridpts(Ifound,1:NApts_atom)= grid_points(1:NApts_atom)
      end do !Z_num

      deallocate(grid_points)
!     deallocate(Radial_points,Radial_weights,NRpoints_region,Nap_grid)

      call PRG_manager ('exit', 'BLD_BIG_GRID', 'UTILITY')
      return
      end subroutine BLD_BIG_GRID
