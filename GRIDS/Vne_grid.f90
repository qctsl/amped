      subroutine BLD_VNE_GRID (Modality)
!****************************************************************************************
!     Date last modified: February 22, 2017                                             *
!     Authors: R.A. Poirier                                                             *
!     Description: Calculate the Vne energy on a set of grid points                     *
!****************************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE module_grid_points
      USE type_plotting

      implicit none
!
! Input scalars:
      character(*) :: Modality
!
! Local scalars:
      integer :: Iatom,Ipoint,IRegion,Znum,Ifound,NRpoints
      double precision :: XApt,YApt,ZApt,WApt,WeightA
      double precision :: Vne,TVne
      double precision :: VnebyA
      double precision :: r_val,StepSize
      double precision :: rho
      character(len=32) :: Pfmt3
! Local array:
      double precision, dimension(:), allocatable  :: Regions
      double precision, dimension(:), allocatable  :: VnebyR
      integer, dimension(:), allocatable  :: NRbyR
!
! Begin:
      call PRG_manager ('enter', 'BLD_VNE_GRID', 'UTILITY')
!
      RADIAL_grid=Modality(1:len_trim(Modality))
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)
      call BLD_BeckeW_XI

      StepSize=MAX_r/NRegions ! Need larger regions to avoid zero's
      allocate(Regions(NRegions))
      do IRegion=1,NRegions
        Regions(IRegion)=dble(IRegion)*StepSize
      end do ! IRegions
!
! Get a temporary file to store plotting data	  
      Pfmt3='(3'//trim(fmt_GridPts)//','//trim(fmt_r2rho)//')'
      PlotFileID=' '
      PlotFunction=' '
      write(PlotFunction,'(a)')'VNE_ENERGY '
      write(PlotFileID,'(a)')'VNE_MOLECULE'
!
      TVne=ZERO
!
      do Iatom=1,Natoms
      Znum=CARTESIAN(Iatom)%Atomic_number
      if(Znum.le.0)cycle
      Ifound=GRID_loc(Znum)
      NApts_atom=NApoints_atom(Ifound)
      VnebyA=ZERO
      allocate(VnebyR(NRegions))
      allocate(NRbyR(NRegions))
      VnebyR=ZERO
      NRbyR=ZERO
      if(MoleculePrint.or.AtomPrint)then
        call BLD_plot_file_MOL ('VNE_GRID', NApts_atom, Plotfile_unit)
      else
        Plotfile_unit=UNIout
      end if
!
! Moving grid points on cartesian coordinate grid
      do Ipoint=1,NApts_atom
        XApt=Egridpts(Ifound,Ipoint)%X
        YApt=Egridpts(Ifound,Ipoint)%Y
        ZApt=Egridpts(Ifound,Ipoint)%Z
        r_val=dsqrt(XApt**2+YApt**2+ZApt**2)
        XApt=XApt+CARTESIAN(Iatom)%X
        YApt=YApt+CARTESIAN(Iatom)%Y
        ZApt=ZApt+CARTESIAN(Iatom)%Z
        WApt=Egridpts(Ifound,Ipoint)%w
        call BeckeW1 (XApt, YApt, ZApt, Iatom, WeightA)
        call GET_rho_Atom (XApt, YApt, ZApt, rho)  ! Used for Ecorr and Vne
        call Vne_GRID_point

        TVne=TVne+Vne
        do IRegion=1,NRegions
        if(r_val.le.Regions(IRegion))then
          VnebyR(IRegion)=VnebyR(IRegion)+Vne
          NRbyR(IRegion)=NRbyR(IRegion)+1
          exit
        else 
        end if
        end do ! IRegion
        if(r_val.gt.Regions(NRegions))then
          VnebyR(NRegions)=VnebyR(NRegions)+Vne
          NRbyR(NRegions)=NRbyR(NRegions)+1
        end if
      end do ! Ipoint
      write(Plotfile_unit,*)'Iatom: ',Iatom
      write(Plotfile_unit,'(a)')'    Iregion  Region     Npoints   Vne'
        call flush(Plotfile_unit)
      NRpoints=0
      do IRegion=1,NRegions
        VnebyR(IRegion)=FourPi*VnebyR(IRegion)
        VnebyA=VnebyA+VnebyR(IRegion)
        NRpoints=NRpoints+NRbyR(IRegion)
        write(Plotfile_unit,'(i8,f12.6,i8,f12.6)')IRegion,Regions(IRegion),NRbyR(IRegion),VnebyR(IRegion)
        call flush(Plotfile_unit)
      end do
! Deallocate
      deallocate(VnebyR)
      deallocate(NRbyR)
      VnebyA=PT5*VnebyA
      write(Plotfile_unit,'(a,i8)')'NApts_atom: ',NApts_atom
      write(Plotfile_unit,'(a)')'    Atom  Npoints     Vne'
      write(Plotfile_unit,'(2i8,f12.6)')Iatom,NRpoints,VnebyA
      call flush(Plotfile_unit)
      end do ! Iatom
      deallocate(Regions)
!
      TVne=PT5*TVne*FourPi
      write(UNIout,'(a,f12.6)')'TVne: ',TVne
      call flush(Plotfile_unit)
!
      if(MoleculePrint.or.AtomPrint)close (unit=Plotfile_unit)
!
! End of routine BLD_VNE_GRID
      call PRG_manager ('exit', 'BLD_VNE_GRID', 'UTILITY')
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine Vne_GRID_point
!****************************************************************************************
!     Date last modified: December 31, 2015                                             *
!     Authors: Ahmad I. Alrawashdeh                                                     *
!     Description: Calculate the velcoity at a point (Xpt, Ypt, Zpt)                    *
!****************************************************************************************
! Modules:

      implicit none

!
! Local scalars:
      integer :: Jatom,ZnumJ
      double precision :: temp
!
! Local functions:
!
! Local arrays:

! Begin: 
      Vne=ZERO
      do Jatom=1,Natoms
        ZnumJ = CARTESIAN(Jatom)%Atomic_number
        if(ZnumJ.le.0)cycle
        temp = ZnumJ*rho/    &
        dsqrt((XApt-CARTESIAN(Jatom)%X)**2 + &
              (YApt-CARTESIAN(Jatom)%Y)**2 + &
              (ZApt-CARTESIAN(Jatom)%Z)**2)
        Vne=Vne+temp
      end do ! Jatom

      Vne=-TWO*Vne*WeightA*WApt
!
! End of routine Vne_GRID_point
      return
      end subroutine Vne_GRID_point
! END CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine BLD_VNE_GRID
