      subroutine GET_TDF_T_OS_point (Rv, rf, rfgrad, rflap, Nact, tdf)
!*******************************************************************************
!     Date last modified: October 21, 2021                                     *
!     Authors: JWH                                                             *
!     Description: Compute the opposite-spin two-electron-density functional   *
!                  value at the given point. (Kinetic energy component)        * 
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE INT_objects

      implicit none
!
! Input scalars:
      double precision, intent(IN) :: rf, rfgrad(3), rflap
      integer, intent(IN) :: Nact
!
! Input arrays:
      double precision, intent(IN) :: Rv(3)
!
! Output scalars:
      double precision, intent(OUT) :: tdf
!
! Local scalars:
      double precision :: rho, d2rf, d2rho, drho2lam, dlcusp, dl2cusp, val
      double precision :: lambda, u_scale, wts, g, cusp, lap, uint, uint0, Rint, Rint0, Rlap, ulap
      double precision :: R2RDMab_grid, R2RDMba_grid, q, u, dR2cusp, ducusp, du2cusp, drholam, rho1_3
      double precision :: R2RDMab_ulap, R2RDMba_ulap
      double precision :: R2RDMab_Rlap, R2RDMba_Rlap
      integer :: I_ux, I_uy, I_uz, N_u_pts
!
! Local arrays:
      double precision :: umag(3), uv(3), r1v(3), r2v(3), du(3), dRcusp(3), drf(3), drho(3)
      double precision :: R2RDMab_ugrad(3), R2RDMba_ugrad(3), ugrad(3), Rgrad(3)
      double precision :: R2RDMab_Rgrad(3), R2RDMba_Rgrad(3)
      double precision, allocatable, dimension(:) :: u_pts, u_wts
!           
! Begin:
!
      q = q_TDF
!
! Could have a get 2RDM here, but then it wouldn't be passed in
!    
      tdf = zero
!
      call GET_den_VGL_point (Rv, Nact, CMO%coeff, rho, drho, d2rho) 
!
      if(rho.gt.1.0D-12)then
!
      rho1_3 = rho**F1_3
!
      lambda   =  q*rho1_3
      drholam  =  q/(3.0D0*rho1_3**2)
      drho2lam = -2.0D0*q/(9.0D0*rho1_3**5)
!
! grid parameters (use density at R to determine these)          
      N_u_pts = 50
      u_scale = 2.0D0
      allocate(u_pts(N_u_pts), u_wts(N_u_pts))
      call BLD_MK_grid(N_u_pts, u_scale, u_pts, u_wts)
!
! u independent quantities
!
! Phi^1/2 left factor included
      d2rf = 0.5D0*rflap - 0.25D0*dot_product(rfgrad,rfgrad)/rf
      drf  = 0.5D0*rfgrad
!
! Integrate over u
      do I_ux = 1, N_u_pts
        umag(1) = u_pts(I_ux)
        do I_uy = 1, N_u_pts
          umag(2) = u_pts(I_uy)
          do I_uz = 1, N_u_pts
            umag(3) = u_pts(I_uz)
!
            uint  = 0.0D0
            uint0 = 0.0D0
            Rint  = 0.0D0
            Rint0 = 0.0D0
!
            wts = u_wts(I_ux)*u_wts(I_uy)*u_wts(I_uz)
!
            u = dsqrt(dot_product(umag,umag))
!
! trying bumpless cusp
            cusp    = (1.0D0 + lambda - dexp(-0.5D0*lambda*u))/lambda
            ducusp  = 0.5D0*dexp(-0.5D0*lambda*u)
            du2cusp = -0.25D0*lambda*dexp(-0.5D0*lambda*u) + 2.0D0*ducusp/u
            dlcusp  = 0.5D0*(dexp(-0.5D0*lambda*u)*(2.0D0 + u*lambda) - 2.0D0)/lambda**2
            dl2cusp = 0.25D0*(8.0D0 - dexp(-0.5D0*lambda*u)*(8.0D0 + u*lambda*(4.0D0 + u*lambda)))&
                     /lambda**3 
            dRcusp  = dlcusp*drholam*drho
            dR2cusp = (dl2cusp*drholam**2 + dlcusp*drho2lam)*dot_product(drho,drho)&
                     + dlcusp*drholam*d2rho
!
! (+,+,+)
            uv = umag
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_VGL_OS_point(r1v, r2v, Nact, R2RDMab_grid, R2RDMba_grid,&
                                                        R2RDMab_ugrad, R2RDMba_ugrad,&
                                                        R2RDMab_ulap, R2RDMba_ulap,&
                                                        R2RDMab_Rgrad, R2RDMba_Rgrad,&
                                                        R2RDMab_Rlap, R2RDMba_Rlap)
!
            du = uv/u
!
            val   = R2RDMab_grid  + R2RDMba_grid
            ulap  = R2RDMab_ulap  + R2RDMba_ulap
            ugrad = R2RDMab_ugrad + R2RDMba_ugrad
            Rlap  = R2RDMab_Rlap  + R2RDMba_Rlap
            Rgrad = R2RDMab_Rgrad + R2RDMba_Rgrad
!
            uint = uint + ulap*cusp + 2.0D0*dot_product(ugrad,du)*ducusp + val*du2cusp
!
            Rint = Rint + Rlap*rf*cusp + val*d2rf*cusp + val*rf*dR2cusp&
                        + 2.0D0*(rf*dot_product(Rgrad,dRcusp)&
                             + cusp*dot_product(Rgrad,drf)&
                             +  val*dot_product(drf,dRcusp))
!
            uint0 = uint0 + ulap
            Rint0 = Rint0 + Rlap
!
! (+,+,-)
            uv(3) = -umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_VGL_OS_point(r1v, r2v, Nact, R2RDMab_grid, R2RDMba_grid,&
                                                        R2RDMab_ugrad, R2RDMba_ugrad,&
                                                        R2RDMab_ulap, R2RDMba_ulap,&
                                                        R2RDMab_Rgrad, R2RDMba_Rgrad,&
                                                        R2RDMab_Rlap, R2RDMba_Rlap)
!
            du = uv/u
!
            val   = R2RDMab_grid + R2RDMba_grid
            ulap  = R2RDMab_ulap + R2RDMba_ulap
            ugrad = R2RDMab_ugrad + R2RDMba_ugrad
            Rlap  = R2RDMab_Rlap + R2RDMba_Rlap
            Rgrad = R2RDMab_Rgrad + R2RDMba_Rgrad
!
            uint = uint + ulap*cusp + 2.0D0*dot_product(ugrad,du)*ducusp + val*du2cusp
!
            Rint = Rint + Rlap*rf*cusp + val*d2rf*cusp + val*rf*dR2cusp&
                        + 2.0D0*(rf*dot_product(Rgrad,dRcusp)&
                             + cusp*dot_product(Rgrad,drf)&
                             +  val*dot_product(drf,dRcusp))
!
            uint0 = uint0 + ulap
            Rint0 = Rint0 + Rlap
!
! (+,-,+)
            uv(1) =  umag(1)
            uv(2) = -umag(2)
            uv(3) =  umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_VGL_OS_point(r1v, r2v, Nact, R2RDMab_grid, R2RDMba_grid,&
                                                        R2RDMab_ugrad, R2RDMba_ugrad,&
                                                        R2RDMab_ulap, R2RDMba_ulap,&
                                                        R2RDMab_Rgrad, R2RDMba_Rgrad,&
                                                        R2RDMab_Rlap, R2RDMba_Rlap)
!
            du = uv/u
!
            val   = R2RDMab_grid  + R2RDMba_grid
            ulap  = R2RDMab_ulap  + R2RDMba_ulap
            ugrad = R2RDMab_ugrad + R2RDMba_ugrad
            Rlap  = R2RDMab_Rlap  + R2RDMba_Rlap
            Rgrad = R2RDMab_Rgrad + R2RDMba_Rgrad
!
            uint = uint + ulap*cusp + 2.0D0*dot_product(ugrad,du)*ducusp + val*du2cusp
!
            Rint = Rint + Rlap*rf*cusp + val*d2rf*cusp + val*rf*dR2cusp&
                        + 2.0D0*(rf*dot_product(Rgrad,dRcusp)&
                             + cusp*dot_product(Rgrad,drf)&
                             +  val*dot_product(drf,dRcusp))
!
            uint0 = uint0 + ulap
            Rint0 = Rint0 + Rlap
!
! (-,+,+)
            uv(1) = -umag(1)
            uv(2) =  umag(2)
            uv(3) =  umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_VGL_OS_point(r1v, r2v, Nact, R2RDMab_grid, R2RDMba_grid,&
                                                        R2RDMab_ugrad, R2RDMba_ugrad,&
                                                        R2RDMab_ulap, R2RDMba_ulap,&
                                                        R2RDMab_Rgrad, R2RDMba_Rgrad,&
                                                        R2RDMab_Rlap, R2RDMba_Rlap)
!
            du = uv/u
!
            val   = R2RDMab_grid  + R2RDMba_grid
            ulap  = R2RDMab_ulap  + R2RDMba_ulap
            ugrad = R2RDMab_ugrad + R2RDMba_ugrad
            Rlap  = R2RDMab_Rlap  + R2RDMba_Rlap
            Rgrad = R2RDMab_Rgrad + R2RDMba_Rgrad
!
            uint = uint + ulap*cusp + 2.0D0*dot_product(ugrad,du)*ducusp + val*du2cusp
!
            Rint = Rint + Rlap*rf*cusp + val*d2rf*cusp + val*rf*dR2cusp&
                        + 2.0D0*(rf*dot_product(Rgrad,dRcusp)&
                             + cusp*dot_product(Rgrad,drf)&
                             +  val*dot_product(drf,dRcusp))
!
            uint0 = uint0 + ulap
            Rint0 = Rint0 + Rlap
!
! (+,-,-)
            uv(1) =  umag(1)
            uv(2) = -umag(2)
            uv(3) = -umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_VGL_OS_point(r1v, r2v, Nact, R2RDMab_grid, R2RDMba_grid,&
                                                        R2RDMab_ugrad, R2RDMba_ugrad,&
                                                        R2RDMab_ulap, R2RDMba_ulap,&
                                                        R2RDMab_Rgrad, R2RDMba_Rgrad,&
                                                        R2RDMab_Rlap, R2RDMba_Rlap)
!
            du = uv/u
!
            val   = R2RDMab_grid  + R2RDMba_grid
            ulap  = R2RDMab_ulap  + R2RDMba_ulap
            ugrad = R2RDMab_ugrad + R2RDMba_ugrad
            Rlap  = R2RDMab_Rlap  + R2RDMba_Rlap
            Rgrad = R2RDMab_Rgrad + R2RDMba_Rgrad
!
            uint = uint + ulap*cusp + 2.0D0*dot_product(ugrad,du)*ducusp + val*du2cusp
!
            Rint = Rint + Rlap*rf*cusp + val*d2rf*cusp + val*rf*dR2cusp&
                        + 2.0D0*(rf*dot_product(Rgrad,dRcusp)&
                             + cusp*dot_product(Rgrad,drf)&
                             +  val*dot_product(drf,dRcusp))
!
            uint0 = uint0 + ulap
            Rint0 = Rint0 + Rlap
!
! (-,+,-)
            uv(1) = -umag(1)
            uv(2) =  umag(2)
            uv(3) = -umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_VGL_OS_point(r1v, r2v, Nact, R2RDMab_grid, R2RDMba_grid,&
                                                        R2RDMab_ugrad, R2RDMba_ugrad,&
                                                        R2RDMab_ulap, R2RDMba_ulap,&
                                                        R2RDMab_Rgrad, R2RDMba_Rgrad,&
                                                        R2RDMab_Rlap, R2RDMba_Rlap)
!
            du = uv/u
!
            val   = R2RDMab_grid  + R2RDMba_grid
            ulap  = R2RDMab_ulap  + R2RDMba_ulap
            ugrad = R2RDMab_ugrad + R2RDMba_ugrad
            Rlap  = R2RDMab_Rlap  + R2RDMba_Rlap
            Rgrad = R2RDMab_Rgrad + R2RDMba_Rgrad
!
            uint = uint + ulap*cusp + 2.0D0*dot_product(ugrad,du)*ducusp + val*du2cusp
!
            Rint = Rint + Rlap*rf*cusp + val*d2rf*cusp + val*rf*dR2cusp&
                        + 2.0D0*(rf*dot_product(Rgrad,dRcusp)&
                             + cusp*dot_product(Rgrad,drf)&
                             +  val*dot_product(drf,dRcusp))
!
            uint0 = uint0 + ulap
            Rint0 = Rint0 + Rlap
!
! (-,-,+)
            uv(1) = -umag(1)
            uv(2) = -umag(2)
            uv(3) =  umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_VGL_OS_point(r1v, r2v, Nact, R2RDMab_grid, R2RDMba_grid,&
                                                        R2RDMab_ugrad, R2RDMba_ugrad,&
                                                        R2RDMab_ulap, R2RDMba_ulap,&
                                                        R2RDMab_Rgrad, R2RDMba_Rgrad,&
                                                        R2RDMab_Rlap, R2RDMba_Rlap)
!
            du = uv/u
!
            val   = R2RDMab_grid  + R2RDMba_grid
            ulap  = R2RDMab_ulap  + R2RDMba_ulap
            ugrad = R2RDMab_ugrad + R2RDMba_ugrad
            Rlap  = R2RDMab_Rlap  + R2RDMba_Rlap
            Rgrad = R2RDMab_Rgrad + R2RDMba_Rgrad
!
            uint = uint + ulap*cusp + 2.0D0*dot_product(ugrad,du)*ducusp + val*du2cusp
!
            Rint = Rint + Rlap*rf*cusp + val*d2rf*cusp + val*rf*dR2cusp&
                        + 2.0D0*(rf*dot_product(Rgrad,dRcusp)&
                             + cusp*dot_product(Rgrad,drf)&
                             +  val*dot_product(drf,dRcusp))
!
            uint0 = uint0 + ulap
            Rint0 = Rint0 + Rlap
!
! (-,-,-)
            uv = -umag
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_VGL_OS_point(r1v, r2v, Nact, R2RDMab_grid, R2RDMba_grid,&
                                                        R2RDMab_ugrad, R2RDMba_ugrad,&
                                                        R2RDMab_ulap, R2RDMba_ulap,&
                                                        R2RDMab_Rgrad, R2RDMba_Rgrad,&
                                                        R2RDMab_Rlap, R2RDMba_Rlap)
!
            du = uv/u
!
            val   = R2RDMab_grid  + R2RDMba_grid
            ulap  = R2RDMab_ulap  + R2RDMba_ulap
            ugrad = R2RDMab_ugrad + R2RDMba_ugrad
            Rlap  = R2RDMab_Rlap  + R2RDMba_Rlap
            Rgrad = R2RDMab_Rgrad + R2RDMba_Rgrad
!
            uint = uint + ulap*cusp + 2.0D0*dot_product(ugrad,du)*ducusp + val*du2cusp
!
            Rint = Rint + Rlap*rf*cusp + val*d2rf*cusp + val*rf*dR2cusp&
                        + 2.0D0*(rf*dot_product(Rgrad,dRcusp)&
                             + cusp*dot_product(Rgrad,drf)&
                             +  val*dot_product(drf,dRcusp))
!
            uint0 = uint0 + ulap
            Rint0 = Rint0 + Rlap
!
! Total
            !tdf = tdf + wts*(rf*intg - intg0)
            !uint = rf*cusp*uint
            !Rint = cusp*Rint
            !tdf = tdf + wts*((uint + 0.25D0*Rint) - (uint0 + 0.25D0*Rint0))
            ! test with total kinetic
            tdf = tdf + wts*(uint0 + 0.25D0*Rint0)
!
          end do ! I_uz
        end do ! I_uy
      end do ! I_ux
!
      end if ! rho > tol
!
      end subroutine GET_TDF_T_OS_point
      subroutine GET_1eDF_T_point (Rv, Nact, tdf)
!*******************************************************************************
!     Date last modified: February 15, 2022                                    *
!     Authors: JWH                                                             *
!     Description: Compute the kinetic energy density (del2 version)           *
!                  value at the given point. (Kinetic energy component)        * 
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE INT_objects

      implicit none
!
! Input scalars:
      integer, intent(IN) :: Nact
!
! Input arrays:
      double precision, intent(IN) :: Rv(3)
!
! Output scalars:
      double precision, intent(OUT) :: tdf
!
! Local scalars:
      double precision :: rho, d2rho, rdmlap
!
! Local arrays:
      double precision :: drho(3)
!           
! Begin:
!
! Could have a get 2RDM here, but then it wouldn't be passed in
!    
      tdf = zero
!
      call GET_den_VGL_point (Rv, Nact, CMO%coeff, rho, drho, d2rho) 
!
      if(rho.gt.1.0D-12)then
!
        call GET_1RDM_lap_point (Rv, Nact, rdmlap) 
!
        tdf = tdf + rdmlap
!
      end if
!
      end subroutine GET_1eDF_T_point
