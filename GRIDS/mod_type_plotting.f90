      MODULE type_plotting
!*******************************************************************************************************
!     Date last modified: July 22, 2014                                                   Version 2.0  *
!     Author: Ahmad and R.A. Poirier                                                                   *
!     Description: Define plotting information                                                         *
!*******************************************************************************************************
!
      implicit none
!
        integer :: Plotfile_unit
        integer :: NMOprint
        integer, parameter :: MaxMOprint=10
        integer, dimension(:) :: MOprint(MaxMOprint)
        logical :: MOleculePrint,AtomPrint,GrdLenPrint,LPlotNamed,LMOprint
        double precision :: PlotCutoff
        character(len=128) :: PlotFilename  
        character(len=64) :: PlotFunction  
        character(len=64) :: PlotFileID  
        character(len=8) :: fmt_GridPts  
        character(len=8) :: fmt_rho  
        character(len=8) :: fmt_drho  
        character(len=8) :: fmt_d2rho  
        character(len=8) :: fmt_r2rho  
        character(len=8) :: fmt_dr2rho  
        character(len=8) :: fmt_d2r2rho  

        character(len=32) :: Pfmt_rho  
        character(len=32) :: Pfmt1_rho  
        character(len=32) :: Pfmt2_rho  
        character(len=32) :: Pfmt_drho  
!
        character(len=32) :: Pfmt_d2rho  
        character(len=32) :: Pfmt1_d2rho  
        character(len=32) :: Pfmt2_d2rho  
        character(len=32) :: Pfmt_Eig  
!
        character(len=32) :: Pfmt_r2rho  
        character(len=32) :: Pfmt1_r2rho  
        character(len=32) :: Pfmt2_r2rho  
        character(len=32) :: Pfmt_dRad  
        character(len=32) :: Pfmt_d2Rad  

      end MODULE type_plotting     
