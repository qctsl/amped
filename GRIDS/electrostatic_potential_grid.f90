      subroutine ElectrostaticPotl_MESH
!****************************************************************************************
!     Date last modified: August 15, 2012                                               *
!     Authors: R.A. Poirier                                                             *
!     Description:                                                                      *
!                 Calculate the charge density on a set of grid points (mesh)           *
!****************************************************************************************
! Modules:
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE module_grid_points
      USE type_plotting

      implicit none
!
! Local scalars:
      integer :: Iatom,Ipoint,ZnumA
      double precision :: Xpt,Ypt,Zpt
      double precision, dimension(:), allocatable  :: V12dr2,ElectrostaticPotl
      double precision :: ElPotl_r
      double precision :: MAX_EPotl
      parameter (MAX_EPotl=10.0D0)
!
! Begin:
      call PRG_manager ('enter','ElectrostaticPotl_MESH', 'UTILITY')
!
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)
!
      allocate (V12dr2(1:MATlen)) ! NOT needed
      allocate(ElectrostaticPotl(NGridPoints))
!
! Get a temporary file to store plotting data	  
        PlotFileID=' '
        PlotFunction=' '
        write(PlotFunction,'(a)')'Electrostatic Potential '
        write(PlotFileID,'(a)')'ELECTROSTATIC_POTENTIAL_MOLECULE'

      if(MoleculePrint.or.AtomPrint)then
        call BLD_plot_file_MOL ('ElectrostaticPotl_MESH', NGridPoints, Plotfile_unit)
      else
        Plotfile_unit=UNIout
      end if

      do Ipoint=1,NGridPoints
! First compute the nuclear component at the grid point
        Xpt=grid_points(Ipoint)%x
        Ypt=grid_points(Ipoint)%y
        Zpt=grid_points(Ipoint)%z
        ElectroStaticPotl(Ipoint)=ElPotl_r (Xpt, Ypt, Zpt, V12dr2, MATlen)
        if(ElectroStaticPotl(Ipoint).gt.MAX_EPotl)ElectroStaticPotl(Ipoint)=MAX_EPotl
      end do ! Ipoint

      call PRT_GRID_MOL (ElectrostaticPotl, NGridPoints, fmt_r2rho)
!
      if(MoleculePrint.or.AtomPrint)close (unit=Plotfile_unit)

      deallocate (V12dr2) ! NOT needed
      deallocate(ElectrostaticPotl)
!
! End of routine ElectrostaticPotl_MESH
      call PRG_manager ('exit', 'ElectrostaticPotl_MESH', 'UTILITY')
      return
      end subroutine ElectrostaticPotl_MESH
      function ElPotl_r (Xpt, Ypt, Zpt, V12dr2, Nelem) result(ElStaticPotl)
!**************************************************************************************************
!     Date last modified: December 10, 2015                                                       *
!     Authors: R.A. Poirier                                                                       *
!     Description:                                                                                *
!                 Calculate the electrostatic potential at a point (Xpt, Ypt, Zpt)                *
!**************************************************************************************************
! Modules:
      USE program_constants
      USE QM_defaults
      USE QM_objects
      USE type_molecule

      implicit none
!
! Input scalars:
      integer :: Nelem
      double precision :: ElStaticPotl
      double precision, intent(IN) :: Xpt,Ypt,Zpt
      double precision :: V12dr2(Nelem)
!
! Local scalars:
      integer :: Iatom,ZnumA
      double precision :: r_iA,sumA,Vpotl
!
! Local functions:
      double precision :: TraceAB
!
! Begin:
      call PRG_manager ('enter','ElPotl_r', 'UTILITY')
!
        sumA=ZERO
        do Iatom=1,Natoms
          ZnumA=CARTESIAN(Iatom)%Atomic_number
          if(ZnumA.le.0)cycle
          r_iA=dsqrt((Xpt-CARTESIAN(Iatom)%X)**2+(Ypt-CARTESIAN(Iatom)%Y)**2+(Zpt-CARTESIAN(Iatom)%Z)**2)
          if(r_iA.le.ZERO)cycle
          sumA=sumA+dble(ZnumA)/r_iA
        end do ! Iatom
        call I1E_V12dr2 (V12dr2, Nelem, Xpt, Ypt, Zpt) ! new routine without AOprod uses OEPXYZ!!!
        Vpotl=-TraceAB (PM0, V12dr2, NBasis)
        ElStaticPotl=sumA-Vpotl
!
! End of routine ElPotl_r
      call PRG_manager ('exit', 'ElPotl_r', 'UTILITY')
      return
      end
      subroutine ElectrostaticPotl_BOND
!********************************************************************
!     Date last modified: July 31, 2014                           *
!     Authors: R.A. Poirier                                         *
!     Description: Calculate the density belonging to bonds         *
!********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE module_grid_points
      USE type_plotting

      implicit none
!
! Local scalars:
      double precision, dimension(:), allocatable  :: V12dr2,EPotl
      integer :: Aatom,Batom,Ipoint,ZA,ZB,lenstr
      double precision :: BweightA,BweightB,WAB,ElectrostaticPotl
      double precision :: XVal,YVal,ZVal,ABcharge
      character(len=32):: Ctemp
      double precision :: GET_density_at_xyz,ElPotl_r
      double precision :: MinWAB
      parameter (MinWAB=1.0D-02)
!
! Begin:
      call PRG_manager ('enter','ElectrostaticPotl_BOND', 'UTILITY')
!
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)
!
      PlotFileID=' '
      allocate (V12dr2(1:MATlen)) ! NOT needed
      allocate(EPotl(NGridPoints))
! Now build a mesh based on the radial grids
      call BLD_BeckeW_XI
!
      do Aatom=1,Natoms
        ZA=CARTESIAN(Aatom)%Atomic_number
        if(ZA.le.0)cycle
      do Batom=Aatom,Natoms
        if(Aatom.eq.Batom)cycle
        ZB=CARTESIAN(Batom)%Atomic_number
        if(ZB.le.0)cycle
!
! A-B (should add A-B + B-A)
      EPotl(1:NGridPoints)=ZERO
      do Ipoint=1,NGridPoints
        XVal=grid_points(Ipoint)%x
        YVal=grid_points(Ipoint)%y
        ZVal=grid_points(Ipoint)%z
        ElectroStaticPotl=ElPotl_r (XVal, YVal, ZVal, V12dr2, MATlen)
          call GET_Weight1 (XVal, YVal, ZVal, Aatom, BweightA)
          call GET_Weight1 (XVal, YVal, ZVal, Batom, BweightB)
          WAB=dsqrt(BweightA*BweightB)
          EPotl(Ipoint)=WAB*ElectroStaticPotl 
        end do ! Ipoint
        write(Ctemp,'(a,i4,a,i4)')'ElectrostaticPotl_BOND_',Aatom,'_',Batom
        call RemoveBlanks (Ctemp, PlotFileID, lenstr)
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a,i4,a,i4)')'Electrostatic Potential for Bond between atom ',Aatom,' and ',Batom
        call PRT_GRID_MOL (EPotl, NGridPoints, fmt_rho)
        close (unit=Plotfile_unit)
      end do ! Batom
      end do ! Aatom

      deallocate (V12dr2) ! NOT needed
      deallocate(EPotl)
!
! End of routine ElectrostaticPotl_BOND
      call PRG_manager ('exit', 'ElectrostaticPotl_BOND', 'UTILITY')
      return
      end subroutine ElectrostaticPotl_BOND
      subroutine ElectrostaticPotl_ATOM
!********************************************************************
!     Date last modified: July 31, 2014                           *
!     Authors: R.A. Poirier                                         *
!     Description: Calculate the density belonging to bonds         *
!********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE QM_defaults
      USE QM_objects
!     USE type_density
      USE module_grid_points
      USE type_plotting

      implicit none
!
! Local scalars:
      integer :: Aatom,IAIMprint,Ipoint,Npoints,ZA,lenstr
      double precision :: BweightA,rho
      double precision :: XVal,YVal,ZVal
      double precision :: GET_density_at_xyz
      double precision :: ElPotl_r
      character(len=32):: Ctemp
      double precision, dimension(:), allocatable  :: V12dr2,ElectrostaticPotl
!
! Begin:
      call PRG_manager ('enter','ElectrostaticPotl_ATOM', 'UTILITY')
!
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)
!
      allocate (V12dr2(1:MATlen)) ! NOT needed
      PlotFileID=' '
! Now build a mesh based on the radial grids
      call BLD_BeckeW_XI
!
      do IAIMprint=1,NAIMprint
      Aatom=AIMprint(IAIMprint)
      allocate(ElectrostaticPotl(NGridPoints))
        ZA=CARTESIAN(Aatom)%Atomic_number
        if(ZA.le.0)cycle
      Npoints=0
      do Ipoint=1,NGridPoints
        XVal=grid_points(Ipoint)%x
        YVal=grid_points(Ipoint)%y
        ZVal=grid_points(Ipoint)%z
         call GET_Weight1 (XVal, YVal, ZVal, Aatom, BweightA)
         rho=BweightA*GET_density_at_xyz (XVal, YVal, ZVal)
!        if(rho.lt.1.0D-06)cycle
!        if(rho.gt.1.0D-01)cycle
         Npoints=Npoints+1
         ElectroStaticPotl(Npoints)=ElPotl_r (XVal, YVal, ZVal, V12dr2, MATlen)
      end do ! Ipoint

      Ctemp(1:32)=' '
      PlotFileID=' '
      write(Ctemp,'(a,i4)')'ElPotl_ATOM_',Aatom
      call RemoveBlanks (Ctemp, PlotFileID, lenstr)
      call BLD_plot_file_MOL (PlotFileID, Npoints, Plotfile_unit)
      write(PlotFunction,'(a,i4)')'Electrostatic Potential for Atom ',Aatom
      call PRT_GRID_MOL (ElectroStaticPotl, Npoints, fmt_rho)
      close (unit=Plotfile_unit)
      deallocate(ElectroStaticPotl)
      end do ! Aatom

!
! End of routine ElectrostaticPotl_ATOM
      call PRG_manager ('exit', 'ElectrostaticPotl_ATOM', 'UTILITY')
      return
      end subroutine ElectrostaticPotl_ATOM
