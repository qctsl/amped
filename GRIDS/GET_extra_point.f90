      subroutine GET_extra_ss_point (Xpt, Ypt, Zpt, extaa_grid, extHFaa_grid, extbb_grid, extHFbb_grid)
!*******************************************************************************
!     Date last modified: April 24 2019                                        *
!     Authors: JWH                                                             *
!     Description: Compute the extracule density at a point.                   *
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE INT_objects

      implicit none
!
! Input scalars:
      double precision, intent(IN) :: Xpt,Ypt,Zpt
!
! Output scalars:
      double precision, intent(OUT) :: extaa_grid,extHFaa_grid
      double precision, intent(OUT) :: extbb_grid,extHFbb_grid
!
! Local scalars:
      double precision :: rv(3)
!           
! Begin:
!
      call GET_object ('QM', '2RDM', 'AO')
!    
      extaa_grid=ZERO
      extHFaa_grid=ZERO        
!
      extbb_grid=ZERO
      extHFbb_grid=ZERO        
!
      rv(1) = Xpt
      rv(2) = Ypt
      rv(3) = Zpt
!
      call BLD_extraint (rv)
!
      extaa_grid = dot_product(TRDM_AOaa,extAO)
      extHFaa_grid = dot_product(TRDM_HF_AOaa,extAO)
!
      extbb_grid = dot_product(TRDM_AObb,extAO)
      extHFbb_grid = dot_product(TRDM_HF_AObb,extAO)
!
      return
      end subroutine GET_extra_ss_point 
      subroutine GET_extra_os_point (Xpt, Ypt, Zpt, extab_grid, extHFab_grid, extba_grid, extHFba_grid)
!*******************************************************************************
!     Date last modified: October 24, 2024                                     *
!     Authors: JWH                                                             *
!     Description: Compute the extracule density at a point.                   *
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE INT_objects

      implicit none
!
! Input scalars:
      double precision, intent(IN) :: Xpt,Ypt,Zpt
!
! Output scalars:
      double precision, intent(OUT) :: extab_grid,extHFab_grid
      double precision, intent(OUT) :: extba_grid,extHFba_grid
!
! Local scalars:
      double precision :: rv(3)
!           
! Begin:
!
      call GET_object ('QM', '2RDM', 'AO')
!    
      extab_grid=ZERO
      extHFab_grid=ZERO        
!
      extba_grid=ZERO
      extHFba_grid=ZERO        
!
      rv(1) = Xpt
      rv(2) = Ypt
      rv(3) = Zpt
!
      call BLD_extraint (rv)
!
      extab_grid = dot_product(TRDM_AOab,extAO)
      extHFab_grid = dot_product(TRDM_HF_AOab,extAO)
!
      extba_grid = dot_product(TRDM_AOba,extAO)
      extHFba_grid = dot_product(TRDM_HF_AOba,extAO)
!
      return
      end subroutine GET_extra_os_point 
