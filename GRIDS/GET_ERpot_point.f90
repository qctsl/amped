      subroutine GET_ERpotss_point (Xpt, Ypt, Zpt, Veeaa_grid, VeeHFaa_grid, Veebb_grid, VeeHFbb_grid)
!*******************************************************************************
!     Date last modified: April 24 2019                                        *
!     Authors: JWH                                                             *
!     Description: Compute the two-electron energy density at a point.         *
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE INT_objects

      implicit none
!
! Input scalars:
      double precision, intent(IN) :: Xpt,Ypt,Zpt
!
! Output scalars:
      double precision, intent(OUT) :: Veeaa_grid,VeeHFaa_grid
      double precision, intent(OUT) :: Veebb_grid,VeeHFbb_grid
!
! Local scalars:
      double precision :: rv(3)
!           
! Begin:
!
      call GET_object ('QM', '2RDM', 'AO')
!    
      Veeaa_grid=ZERO
      VeeHFaa_grid=ZERO        
!
      Veebb_grid=ZERO
      VeeHFbb_grid=ZERO        
!
      rv(1) = Xpt
      rv(2) = Ypt
      rv(3) = Zpt
!
      call BLD_ERpot4AO (rv)
!
      Veeaa_grid = dot_product(TRDM_AOaa,ERpotAO)
      VeeHFaa_grid = dot_product(TRDM_HF_AOaa,ERpotAO)
!
      Veebb_grid = dot_product(TRDM_AObb,ERpotAO)
      VeeHFbb_grid = dot_product(TRDM_HF_AObb,ERpotAO)
!
      end subroutine GET_ERpotss_point 
      subroutine GET_ERpotos_point (Xpt, Ypt, Zpt, Veeab_grid, VeeHFab_grid, Veeba_grid, VeeHFba_grid)
!*******************************************************************************
!     Date last modified: April 24 2019                                        *
!     Authors: JWH                                                             *
!     Description: Compute the two-electron energy density at a point.         *
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE INT_objects

      implicit none
!
! Input scalars:
      double precision, intent(IN) :: Xpt,Ypt,Zpt
!
! Output scalars:
      double precision, intent(OUT) :: Veeab_grid,VeeHFab_grid
      double precision, intent(OUT) :: Veeba_grid,VeeHFba_grid
!
! Local scalars:
      double precision :: rv(3)
!           
! Begin:
!
      call GET_object ('QM', '2RDM', 'AO')
!    
      Veeab_grid=ZERO
      VeeHFab_grid=ZERO        
!
      Veeba_grid=ZERO
      VeeHFba_grid=ZERO        
!
      rv(1) = Xpt
      rv(2) = Ypt
      rv(3) = Zpt
!
      call BLD_ERpot4AO (rv)
!
      Veeab_grid = dot_product(TRDM_AOab,ERpotAO)
      VeeHFab_grid = dot_product(TRDM_HF_AOab,ERpotAO)
!
      Veeba_grid = dot_product(TRDM_AOba,ERpotAO)
      VeeHFba_grid = dot_product(TRDM_HF_AOba,ERpotAO)
!
      end subroutine GET_ERpotos_point 
      subroutine GET_exch_point (Xpt, Ypt, Zpt, K_grid)
!*******************************************************************************
!     Date last modified: August 30, 2023                                      *
!     Authors: JWH                                                             *
!     Description: Compute the exchange energy density at a point.             *
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE INT_objects

      implicit none
!
! Input scalars:
      double precision, intent(IN) :: Xpt,Ypt,Zpt
!
! Output scalars:
      double precision, intent(OUT) :: K_grid
!
! Local scalars:
      integer :: aMO, bMO
!
! Local arrays:
      double precision :: rv(3)
      double precision, allocatable, dimension(:,:) :: ERpotMO, Kfocc, Jfocc
      double precision, allocatable, dimension(:) :: phi_rv
!           
! Begin:
!
      call GET_object ('QM', 'CMO', Wavefunction)
!    
      allocate(Kfocc(Nbasis,Nbasis),Jfocc(Nbasis,Nbasis))
      allocate(phi_rv(Nbasis))
      call BLD_occ_and_coupling(Jfocc, Kfocc)
!
      K_grid=ZERO
!
      rv(1) = Xpt
      rv(2) = Ypt
      rv(3) = Zpt
!      
      allocate(ERpotMO(Nbasis,Nbasis))
!
      call BLD_ERpotMO(rv, Nbasis, CMO%coeff, ERpotMO)
      call GET_MO_point(rv, Nbasis, CMO%coeff, phi_rv)
!
      do aMO = 1, CMO%NoccMO
        do bMO = 1, CMO%NoccMO
          K_grid = K_grid - Kfocc(aMO,bMO)*phi_rv(aMO)*phi_rv(bMO)*ERpotMO(aMO,bMO)
        end do ! bMO
      end do ! aMO
!
      deallocate(Kfocc, Jfocc, ERpotMO, phi_rv)
!
      end subroutine GET_exch_point 
      subroutine GET_coul_point (Xpt, Ypt, Zpt, J_grid)
!*******************************************************************************
!     Date last modified: August 30, 2023                                      *
!     Authors: JWH                                                             *
!     Description: Compute the Coulomb energy density at a point.              *
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE INT_objects

      implicit none
!
! Input scalars:
      double precision, intent(IN) :: Xpt,Ypt,Zpt
!
! Output scalars:
      double precision, intent(OUT) :: J_grid
!
! Local scalars:
      integer :: aMO, bMO
!
! Local arrays:
      double precision :: rv(3)
      double precision, allocatable, dimension(:,:) :: ERpotMO, Kfocc, Jfocc
      double precision, allocatable, dimension(:) :: phi_rv
!           
! Begin:
!
      call GET_object ('QM', 'CMO', Wavefunction)
!    
      allocate(Kfocc(Nbasis,Nbasis),Jfocc(Nbasis,Nbasis))
      allocate(phi_rv(Nbasis))
      call BLD_occ_and_coupling(Jfocc, Kfocc)
!
      J_grid = zero
!
      rv(1) = Xpt
      rv(2) = Ypt
      rv(3) = Zpt
!      
      allocate(ERpotMO(Nbasis,Nbasis))
!
      call BLD_ERpotMO(rv, Nbasis, CMO%coeff, ERpotMO)
      call GET_MO_point(rv, Nbasis, CMO%coeff, phi_rv)
!
      do aMO = 1, CMO%NoccMO
        do bMO = 1, CMO%NoccMO
          J_grid = J_grid + Jfocc(aMO,bMO)*phi_rv(aMO)**2*ERpotMO(bMO,bMO)
        end do ! bMO
      end do ! aMO
!
      deallocate(Kfocc, Jfocc, ERpotMO)
!
      end subroutine GET_coul_point 
      subroutine BLD_occ_and_coupling (Jfocc, Kfocc)
!*******************************************************************************
!     Date last modified: August 30, 2023                                      *
!     Authors: JWH                                                             *
!     Description: Combine the coupling constants and occupancies to get the   *
!                  2-RDM elements from RHF/ROHF decomposed into Coulomb and    * 
!                  exchange.                                                   *
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE INT_objects
      USE GSCF_work

      implicit none
!
! Output arrays:
      double precision, intent(OUT) :: Jfocc(Nbasis,Nbasis), Kfocc(Nbasis,Nbasis)
!
! Local scalars:
      integer :: aMO, bMO, AAindex, ABindex, Aopen, Bopen
!
! Begin:
!
      Jfocc = zero 
      Kfocc = zero 
!
      if(wavefunction.eq.'RHF')then 
!
        do aMO = 1, CMO%NoccMO
          Jfocc(aMO,aMO) = one 
          do bMO = 1, aMO-1
!
            Jfocc(aMO,bMO) = two
            Jfocc(bMO,aMO) = two
            Kfocc(aMO,bMO) = one
            Kfocc(bMO,aMO) = one
!
          end do ! bMO 
        end do ! aMO 
!
      else if(wavefunction.eq.'ROHF')then
!
! closed-closed   
        do aMO = 1, CMO_ROHF%NFcoreMO
          Jfocc(aMO,aMO) = one 
          do bMO = 1, aMO-1
!            
            Jfocc(aMO,bMO) = two
            Jfocc(bMO,aMO) = two
            Kfocc(aMO,bMO) = one
            Kfocc(bMO,aMO) = one
!
          end do ! bMO
        end do ! bMO
!
        if(Nopen.gt.0)then
! open-closed
        do aMO = CMO_ROHF%NFcoreMO+1, CMO%NoccMO
          Aopen = aMO - CMO_ROHF%NFcoreMO
          AAindex = Aopen*(Aopen+1)/2 + 1
          do bMO = 1, CMO_ROHF%NFcoreMO
!
            Jfocc(aMO,bMO) = alphCC(AAindex)
            Jfocc(bMO,aMO) = alphCC(AAindex)
            Kfocc(aMO,bMO) = betaCC(AAindex)
            Kfocc(bMO,aMO) = betaCC(AAindex)
!
          end do ! bMO closed
        end do ! aMO open
!
! open-open
        do aMO = CMO_ROHF%NFcoreMO+1, CMO%NoccMO
          Aopen = aMO - CMO_ROHF%NFcoreMO
          AAindex = Aopen*(Aopen+1)/2 + 1 + Aopen
          Jfocc(aMO,aMO) = 0.5D0*alphCC(AAindex)
          do bMO = CMO_ROHF%NFcoreMO+1, aMO-1
            Bopen = bMO - CMO_ROHF%NFcoreMO
            ABindex = Aopen*(Aopen+1)/2 + 1 + Bopen
            Jfocc(aMO,bMO) = alphCC(ABindex)
            Jfocc(bMO,aMO) = alphCC(ABindex)
            Kfocc(aMO,bMO) = betaCC(ABindex)
            Kfocc(bMO,aMO) = betaCC(ABindex)
          end do ! bMO open
        end do ! aMO open
!
        end if ! Nopen > 0
      end if
!
      end subroutine BLD_occ_and_coupling
