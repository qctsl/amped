      subroutine GET_GRID_object (class, Objname, Modality)
!*******************************************************************************
!     Date last modified: June 21, 2001                            Version 2.0 *
!     Author: R.A. Poirier                                                     *
!     Description: Objects belonging to the class GRID                         *
!*******************************************************************************
! Modules:
      USE program_files
      USE program_objects

      implicit none
!
! Input scalars:
      character*(*) :: class
      character*(*) :: Objname
      character*(*) :: Modality
!
! Local scalars:
      integer :: Object_number
!
! Begin:
      call PRG_manager ('enter', 'GET_GRID_object', 'UTILITY')
!
      call GET_object_number (OBJ_GRID, NGRIDobjects, FirstGRID, Objname, Modality, Object_number)

      if(OBJ_GRID(Object_number)%current)then
        call PRG_manager ('exit', 'GET_GRID_object', 'UTILITY')
        return
      end if

      OBJ_GRID(Object_number)%exist=.true.
      OBJ_GRID(Object_number)%Current=.true.
      Object(ObjNum)%exist=.true.
      Object(ObjNum)%Current=.true.

      select_Object: select case (Objname)

      case ('DENSITY') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
        case ('POINT')
          call DENSITY_MESH
        case ('MESH')
          call DENSITY_MESH
!        case ('BOND')
!          call DENSITY_BOND
        case ('ATOM')
          call DENSITY_ATOM
        case ('ATOMIC')
          call DENSITY_ATOMIC
        case ('SHELL')
!          call Numerical_Integration_shell
          call Numerical_Integration_shell_energy('DENSITY')
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select

      case ('SPIN_DENSITY') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
        case ('MESH')
          call BLD_spin_density_mesh
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select

      case ('VEE') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
        case ('MESH')
          call BLD_ERpot_mesh
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select
        
      case ('2RDM') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
        case ('MESH')
          call BLD_2RDM_mesh
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select
        
      case ('TDF') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
        case ('MESH')
          call CFT_TDF_mesh
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select
        
      case ('ODF') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
        case ('MESH')
          call CFT_ODF_mesh
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select
        
      case ('OD') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
        case ('MESH')
          call CFT_OD_mesh
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select
        
      case ('ODUGRAD') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
        case ('MESH')
          call CFT_ODugrad_os_mesh
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select
        
      case ('ODULAP') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
        case ('MESH')
          call CFT_ODulap_os_mesh
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select
        
      case ('PLOT') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
        case ('POINTS')
          call plot_GRIDS
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select

      case ('DENSITY_MO') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
        case ('MESH')
          call MO_DENSITY_MESH
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select

      case ('DENSITY_MO_BY_ATOM') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
        case ('MESH')
          call DENSITY_MO_BY_ATOM_MESH
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select

      case ('DEL_DENSITY') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
        case ('POINT')
          call DEL_DENSITY_MESH
        case ('MESH')
          call DEL_DENSITY_MESH
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select

      case ('GRADLEN_DENSITY_OVER_DENSITY') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
        case ('MESH')
          call GRADLEN_DENSITY_OVER_DENSITY_MESH
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select

      case ('DEL2_DENSITY') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
        case ('POINT')
          call DEL2_DENSITY_MESH
        case ('MESH')
          call DEL2_DENSITY_MESH
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select

      case ('RADIAL_DENSITY') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
        case ('MESH')
          call RADIAL_DENSITY_MESH
        case ('BOND')
          call RADIAL_DENSITY_BOND
        case ('ATOMIC')
          call RADIAL_DENSITY_ATOMIC
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select

      case ('DEL_RADIAL_DENSITY') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
        case ('POINT', 'MESH')
          call DEL_RADIAL_DENSITY_MESH
        case ('NUMERICAL')
          call DEL_RADIAL_DENSITY_NUMERICAL
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select

      case ('DEL2_RADIAL_DENSITY') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
        case ('MESH')
          call DEL2_RADIAL_DENSITY_MESH
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select

      case ('RADIAL_DENSITY_MO') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
        case ('MESH')
          call RADIAL_DENSITY_MO_MESH
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select

      case ('RADIAL_DENSITY_MO_BY_ATOM') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
        case ('MESH')
          call RADIAL_DENSITY_MO_BY_ATOM_MESH
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select

      case ('WEIGHTS') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
        case ('MESH')
          call WEIGHTS_MESH
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select

      case ('WEIGHTS_WAWB') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
        case ('MESH')
          call WEIGHTS_WAWB_MESH
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select

      case ('RADIAL') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      select case (Modality)
      case ('SG1')
        call BLD_SG1_GRID
      case ('SG2')
        call BLD_SG2_GRID
      case ('BIG')
        call BLD_BIG_GRID
      case ('GILL')
        call BLD_Gill_GRID
      case ('BECKE')
        call BLD_Becke_GRID
      case ('NTA')
        call BLD_NTA_GRID
      case ('TA')
        call BLD_TA_GRID
      case ('SG0')
        call BLD_SG0_GRID
      case default
        write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                       '" for Modality "',Modality(1:len_trim(Modality)),'"'
        stop 'No such object'
      end select

      case ('LYP') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      select case (Modality)
      case ('SG1')
        call BLD_LYP_GRID ('SG1')
      case ('BIG')
        call BLD_LYP_GRID ('BIG')
      case ('GILL')
        call BLD_LYP_GRID ('GILL')
      case ('BECKE')
        call BLD_LYP_GRID ('BECKE')
      case ('NTA')
        call BLD_LYP_GRID ('NTA')
      case ('TA')
        call BLD_LYP_GRID ('TA')
      case ('SG0')
        call BLD_LYP_GRID ('SG0')
      case default
        write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                       '" for Modality "',Modality(1:len_trim(Modality)),'"'
        stop 'No such object'
      end select

      case ('COULOMB') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      select case (Modality)
      case ('SG1')
        call BLD_Coulomb_GRID ('SG1')
      case ('BIG')
        call BLD_Coulomb_GRID ('BIG')
      case ('GILL')
        call BLD_Coulomb_GRID ('GILL')
      case ('BECKE')
        call BLD_Coulomb_GRID ('BECKE')
      case ('NTA')
        call BLD_Coulomb_GRID ('NTA')
      case ('TA')
        call BLD_Coulomb_GRID ('TA')
      case ('SG0')
        call BLD_Coulomb_GRID ('SG0')
      case ('SHELL')
        call Numerical_Integration_shell_energy('COULOMB')
      case default
        write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                       '" for Modality "',Modality(1:len_trim(Modality)),'"'
        stop 'No such object'
      end select
      
      case ('JCOUL') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
        case ('MESH')
          call BLD_Jcoul_mesh
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select

      case ('COULOMB_MO') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      select case (Modality)
      case ('SHELL')
        call Numerical_Integration_shell_energy('MOCOULOMB')
      case default
        write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                       '" for Modality "',Modality(1:len_trim(Modality)),'"'
        stop 'No such object'
      end select

      case ('TPOS') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      select case (Modality) 
      case ('MESH')
        call BLD_Tp_spin_mesh
      case default
        write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                       '" for Modality "',Modality(1:len_trim(Modality)),'"'
        stop 'No such object'
      end select

      case ('TSCH') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      select case (Modality) 
      case ('MESH')
        call BLD_Ts_spin_mesh
      case default
        write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                       '" for Modality "',Modality(1:len_trim(Modality)),'"'
        stop 'No such object'
      end select

      case ('VNE') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      select case (Modality)
      case ('SG1')
        call BLD_VNE_GRID ('SG1')
      case ('BIG')
        call BLD_VNE_GRID ('BIG')
      case ('GILL')
        call BLD_VNE_GRID ('GILL')
      case ('BECKE')
        call BLD_VNE_GRID ('BECKE')
      case ('NTA')
        call BLD_VNE_GRID ('NTA')
      case ('TA')
        call BLD_VNE_GRID ('TA')
      case ('SG0')
        call BLD_VNE_GRID ('SG0')
      case ('MESH')
        call BLD_Vne_spin_mesh
      case ('SHELL')                                                             
        call Numerical_Integration_shell_energy('VNE_')
      case default
        write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                       '" for Modality "',Modality(1:len_trim(Modality)),'"'
        stop 'No such object'
      end select

      case ('EXCHANGE') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      select case (Modality)
      case ('SG1')
        call BLD_EXCHANGE_GRID ('SG1')
      case ('BIG')
!       call BLD_EXCHANGE_GRID ('BIG')
      case ('GILL')
!       call BLD_EXCHANGE_GRID ('GILL')
      case ('BECKE')
!       call BLD_EXCHANGE_GRID ('BECKE')
      case ('NTA')
!       call BLD_EXCHANGE_GRID ('NTA')
      case ('TA')
!       call BLD_EXCHANGE_GRID ('TA')
      case ('SG0')
!       call BLD_EXCHANGE_GRID ('SG0')
      case ('SHELL')
        call Numerical_Integration_shell_energy('EXCHANGE')
      case default
        write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                       '" for Modality "',Modality(1:len_trim(Modality)),'"'
        stop 'No such object'
      end select

      case ('KEXCH') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
        case ('MESH')
          call BLD_Kexch_mesh
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select

      case ('VELOCITIES') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      select case (Modality)
      case ('SG1')
        call BLD_VELOCITIES_GRID ('SG1')
      case ('BIG')
        call BLD_VELOCITIES_GRID ('BIG')
      case ('GILL')
        call BLD_VELOCITIES_GRID ('GILL')
      case ('BECKE')
        call BLD_VELOCITIES_GRID ('BECKE')
      case ('NTA')
        call BLD_VELOCITIES_GRID ('NTA')
      case ('TA')
        call BLD_VELOCITIES_GRID ('TA')
      case ('SG0')
        call BLD_VELOCITIES_GRID ('SG0')
      case default
        write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                       '" for Modality "',Modality(1:len_trim(Modality)),'"'
        stop 'No such object'
      end select

      case ('PROMOLATOM') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      select case (Modality)
      case ('SG1')
        call BLD_ProMol_Atoms
      case default
        write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                       '" for Modality "',Modality(1:len_trim(Modality)),'"'
        stop 'No such object'
      end select

      case ('ELECTROSTATICPOTL') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
        case ('MESH')
          call ELECTROSTATICPOTL_MESH
        case ('BOND')
          call ELECTROSTATICPOTL_BOND
        case ('ATOM')
          call ELECTROSTATICPOTL_ATOM
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select

      case ('MBD') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      select case (Modality)
      case ('MESH')
        call BLD_MBD_mesh
      case default
        write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                       '" for Modality "',Modality(1:len_trim(Modality)),'"'
        stop 'No such object'
      end select

      case ('RAMD') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      select case (Modality)
      case ('MESH')
        call BLD_RAMD_mesh
      case default
        write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                       '" for Modality "',Modality(1:len_trim(Modality)),'"'
        stop 'No such object'
      end select

      case ('EAMD') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      select case (Modality)
      case ('MESH')
        call BLD_EAMD_mesh
      case default
        write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                       '" for Modality "',Modality(1:len_trim(Modality)),'"'
        stop 'No such object'
      end select

      case ('EXTRA') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      select case (Modality)
      case ('MESH')
        call BLD_extra_mesh
      case default
        write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                       '" for Modality "',Modality(1:len_trim(Modality)),'"'
        stop 'No such object'
      end select

      case ('LDMATRIX') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
        case ('QTAIM')
          call BLD_LDMatrix_QTAIM
        case ('ABIM')
          call BLD_LDMatrix_ABIM
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select

      case ('CRITICAL_POINTS') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
        case ('RADIAL')
          call Find_CP_Radial_density
          call KILL_object ('QM', 'CRITICAL_POINTS', 'RADIAL')
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select

      case default
        write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                       '" for Modality "',Modality(1:len_trim(Modality)),'"'
        stop 'No such object'
      end select select_Object

      call PRG_manager ('exit', 'GET_GRID_object', 'UTILITY')
      return
      end subroutine GET_GRID_object
