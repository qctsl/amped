      subroutine MENU_MESH
!***********************************************************************
!     Date last modified: February 12, 2003               Version 2.0  *
!     Authors: G. Jansen & J. G. Angyan, R.A. Poirier                  *
!     Desciption: set-up menu for Grid command                         *
!***********************************************************************
! Modules:
      USE program_parser
      USE menu_gets
      USE module_grid_points
      USE program_constants

      implicit none
!
! Local scalars:
      integer :: lenlist, lenstr
      logical :: done
!
! Local arrays:
      double precision :: XYZ(3)
!
! Begin:
      call PRG_manager ('enter', 'MENU_MESH', 'UTILITY')
!
! Defaults:
! Menu:
      LMesh=.false.
      done=.false.

      NGridPoints=0
      NX=0
      NY=0
      NZ=0

      do while (.not.done)
      call new_token ('MEsh:')
!
      if(token(1:4).eq.'HELP')then
      write(UNIout,'(a)') &
      ' Command "MEsh":                              ', &
      '   Define grid points on a MESH                ', &
      '   Syntax : MEsh                                ', &
      '   Nx        = <integer> Number of points along the X-axis  ', &
      '   Ny        = <integer> Number of points along the Y-axis  ', &
      '   Nz        = <integer> Number of points along the Z-axis  ', &
      '   ORigin    = <( X0  Y0  Z0 )>, Point where the grid starts ', &
      '   STepSize  = <( dX  dY  dZ )>, The step size or ', &
      '   MEsh      = <( dX  dY  dZ )>, The step size ', &
      '   test      = <( x y z )>, location of electron 2 for 2e density ', &
      '   alpha     = <real> Euler rotation angle about x-axis in degrees ', &
      '   beta      = <real> Euler rotation angle about y-axis in degrees ', &
      '   gamma     = <real> Euler rotation angle about z-axis in degrees ', &
      '   end'

! NX
      else if(token(1:2).eq.'NX')then
        call GET_value (Nx)
        LMesh=.true.

! NY
      else if(token(1:2).eq.'NY')then
        call GET_value (Ny)
        LMesh=.true.

! NZ
      else if(token(1:2).eq.'NZ')then
        call GET_value (Nz)
        LMesh=.true.
! ALPHA
      else if(token(1:2).eq.'AL')then
        call GET_value (Ralpha)
        Ralpha = Ralpha*pi_val/180.0D0
        LMesh=.true.

! BETA
      else if(token(1:2).eq.'BE')then
        call GET_value (Rbeta)
        Rbeta = Rbeta*pi_val/180.0D0
        LMesh=.true.

! GAMA
      else if(token(1:2).eq.'GA')then
        call GET_value (Rgama)
        Rgama = Rgama*pi_val/180.0D0
        LMesh=.true.
        
! ORigin
      else if(token(1:2).eq.'OR')then
        call GET_value (GRID_min, lenlist)
        LMesh=.true.

! MEsh
      else if(token(1:2).eq.'ME')then
        call GET_value (GRID_mesh,lenlist)
        LMesh=.true.
      
! TEst point
      else if(token(1:2).eq.'TE')then
        call GET_value (test_pt,lenlist)
      
! STepSize
      else if(token(1:2).eq.'ST')then
        call GET_value (GRID_mesh,lenlist)
        LMesh=.true.
      
      else
         call MENU_end (done)
      end if

      end do ! while
!
! end of routine  MENU_MESH
      call PRG_manager ('exit', 'MENU_MESH', 'UTILITY')
      return
      end subroutine MENU_MESH
