      subroutine GET_2RDM_point (Xpt, Ypt, Zpt, tp, TWORDM, MO, TRDM_grid)
!*******************************************************************************
!     Date last modified: May 25, 2021                                         *
!     Authors: JWH                                                             *
!     Description: Compute the two-electron density at a point, given the      *
!                  coordinates of a test electron.                             *
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE INT_objects

      implicit none
!
! Input scalars:
      double precision, intent(IN) :: Xpt,Ypt,Zpt,tp(3)
      double precision, intent(IN) :: TWORDM(Nbasis**4)
      double precision, intent(IN) :: MO(Nbasis,Nbasis)
!
! Output scalars:
      double precision, intent(OUT) :: TRDM_grid
!
! Local scalars:
      double precision :: rv(3)
!
! Local arrays:
      
!           
! Begin:
!
! Don't think this makes sense
      call GET_object ('QM', '2RDM', 'MO')
!    
      TRDM_grid=ZERO
!
      rv(1) = Xpt
      rv(2) = Ypt
      rv(3) = Zpt
!
      allocate(MO4prod(Nbasis**4))
      call BLD_MO4prod(rv,tp,MO)
!
      TRDM_grid = dot_product(TWORDM,MO4prod)
!
      deallocate(MO4prod)
!
      return
      end subroutine GET_2RDM_point
      subroutine GET_R2RDM_ab_point (rv, tp, Nact, R2RDMab_grid, R2RDMba_grid)
!*******************************************************************************
!     Date last modified: October 18, 2021                                     *
!     Authors: JWH                                                             *
!     Description: Compute the opposite-spin two-electron density at a point   *
!                  from a JKL-cumulant functional, given the coordinates of    *
!                  a test electron.                                            *
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE INT_objects

      implicit none
!
! Input scalars:
      integer, intent(IN) :: Nact
!
! Input arrays:
      double precision, intent(IN) :: rv(3),tp(3)
!
! Output scalars:
      double precision, intent(OUT) :: R2RDMab_grid, R2RDMba_grid
!
! Local scalars:
      integer :: pMO, qMO
      double precision :: J, L
!
! Begin:
!
      R2RDMab_grid = ZERO
      R2RDMba_grid = ZERO
!
      allocate(MO4prodJ(Nbasis,Nbasis),MO4prodK(Nbasis,Nbasis))
      call BLD_MO4prodJK(rv,tp,Nact,CMO%coeff) ! assumed NOs copied over
!
      do pMO = 1, Nact
        do qMO = 1, Nact
          J = MO4prodJ(pMO,qMO)
          L = MO4prodK(pMO,qMO)
!
          R2RDMab_grid = R2RDMab_grid + R2RDMJab(pMO,qMO)*J + R2RDMLab(pMO,qMO)*L
          R2RDMba_grid = R2RDMba_grid + R2RDMJba(pMO,qMO)*J + R2RDMLba(pMO,qMO)*L
!
        end do ! qMO
      end do ! pMO
!
      deallocate(MO4prodJ,MO4prodK)
!
      end subroutine GET_R2RDM_ab_point
      subroutine GET_R2RDM_VGL_OS_point (rv, tp, Nact, R2RDMab_grid, R2RDMba_grid,&
                                                       R2RDMab_ugrad, R2RDMba_ugrad,& 
                                                       R2RDMab_ulap,  R2RDMba_ulap,&
                                                       R2RDMab_Rgrad, R2RDMba_Rgrad,& 
                                                       R2RDMab_Rlap,  R2RDMba_Rlap)
!*******************************************************************************
!     Date last modified: December 10, 2021                                    *
!     Authors: JWH                                                             *
!     Description: Compute the value, gradient, and Laplacian of the opposite- *
!                  spin two-electron density at a point from a JKL-cumulant    *
!                  functional, given the coordinates of a test electron.       *
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE INT_objects

      implicit none
!
! Input scalars:
      integer, intent(IN) :: Nact
!
! Input arrays:
      double precision, intent(IN) :: rv(3),tp(3)
!
! Output arrays:
      double precision, intent(OUT) :: R2RDMab_grid, R2RDMba_grid
      double precision, intent(OUT) :: R2RDMab_ugrad(3), R2RDMba_ugrad(3)
      double precision, intent(OUT) :: R2RDMab_ulap, R2RDMba_ulap
      double precision, intent(OUT) :: R2RDMab_Rgrad(3), R2RDMba_Rgrad(3)
      double precision, intent(OUT) :: R2RDMab_Rlap, R2RDMba_Rlap
!
! Local arrays:
      ! may need to include K later
      double precision :: dJ(3), dL(3)
!
! Local scalars:
      integer :: pMO, qMO
      double precision :: GabJpq, GbaJpq, GabLpq, GbaLpq, J, L, d2J, d2L
!
! Begin:
!
      R2RDMab_grid  = zero
      R2RDMba_grid  = zero
      R2RDMab_ugrad = zero
      R2RDMba_ugrad = zero
      R2RDMab_ulap  = zero
      R2RDMba_ulap  = zero
      R2RDMab_Rgrad = zero
      R2RDMba_Rgrad = zero
      R2RDMab_Rlap  = zero
      R2RDMba_Rlap  = zero
!
      allocate(MO4prodJ(Nbasis,Nbasis),MO4prodK(Nbasis,Nbasis))
      allocate(MO4ugradJ(Nbasis,Nbasis,3),MO4ugradL(Nbasis,Nbasis,3))
      allocate(MO4ulapJ(Nbasis,Nbasis),MO4ulapL(Nbasis,Nbasis))
      allocate(MO4RgradJ(Nbasis,Nbasis,3),MO4RgradL(Nbasis,Nbasis,3))
      allocate(MO4RlapJ(Nbasis,Nbasis),MO4RlapL(Nbasis,Nbasis))
!
      call BLD_MO4prodVGL_JL(rv, tp, Nact, CMO%coeff) ! assumed NOs copied over
!
      do pMO = 1, Nact
        do qMO = 1, Nact
!
          GabJpq = R2RDMJab(pMO,qMO)
          GabLpq = R2RDMLab(pMO,qMO)
          GbaJpq = R2RDMJba(pMO,qMO)
          GbaLpq = R2RDMLba(pMO,qMO)
!
          J = MO4prodJ(pMO,qMO)
          L = MO4prodK(pMO,qMO)
          dJ(1:3) = MO4ugradJ(pMO,qMO,1:3)
          dL(1:3) = MO4ugradL(pMO,qMO,1:3)
          d2J = MO4ulapJ(pMO,qMO)
          d2L = MO4ulapL(pMO,qMO)
!
          R2RDMab_grid = R2RDMab_grid + GabJpq*J + GabLpq*L 
          R2RDMba_grid = R2RDMba_grid + GbaJpq*J + GbaLpq*L 
!
          R2RDMab_ugrad(1:3) = R2RDMab_ugrad(1:3) + GabJpq*dJ(1:3) + GabLpq*dL(1:3) 
          R2RDMba_ugrad(1:3) = R2RDMba_ugrad(1:3) + GbaJpq*dJ(1:3) + GbaLpq*dL(1:3) 
!
          R2RDMab_ulap = R2RDMab_ulap + GabJpq*d2J + GabLpq*d2L 
          R2RDMba_ulap = R2RDMba_ulap + GbaJpq*d2J + GbaLpq*d2L 
!
          dJ(1:3) = MO4RgradJ(pMO,qMO,1:3)
          dL(1:3) = MO4RgradL(pMO,qMO,1:3)
          d2J = MO4RlapJ(pMO,qMO)
          d2L = MO4RlapL(pMO,qMO)
!
          R2RDMab_Rgrad(1:3) = R2RDMab_Rgrad(1:3) + GabJpq*dJ(1:3) + GabLpq*dL(1:3) 
          R2RDMba_Rgrad(1:3) = R2RDMba_Rgrad(1:3) + GbaJpq*dJ(1:3) + GbaLpq*dL(1:3) 
!
          R2RDMab_Rlap = R2RDMab_Rlap + GabJpq*d2J + GabLpq*d2L 
          R2RDMba_Rlap = R2RDMba_Rlap + GbaJpq*d2J + GbaLpq*d2L 
!
        end do ! qMO
      end do ! pMO
!
      deallocate(MO4prodJ,MO4prodK,MO4ugradJ,MO4ugradL,MO4ulapJ,MO4ulapL)
      deallocate(MO4RgradJ,MO4RgradL,MO4RlapJ,MO4RlapL)
!
      end subroutine GET_R2RDM_VGL_OS_point
      subroutine GET_R2RDM_VGLRF_OS_point (rv, tp, Nact, R2RDMab_grid, R2RDMba_grid,&
                                                         R2RDMab_rfgrad, R2RDMba_rfgrad,& 
                                                         R2RDMab_rflap,  R2RDMba_rflap)
!*******************************************************************************
!     Date last modified: December 20, 2021                                    *
!     Authors: JWH                                                             *
!     Description: Compute the value, gradient, and Laplacian of the opposite- *
!                  spin two-electron density at a point from a JKL-cumulant    *
!                  functional, given the coordinates of a test electron.       *
!                  (For the derivatives of the renormalization function)       *
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE INT_objects

      implicit none
!
! Input scalars:
      integer, intent(IN) :: Nact
!
! Input arrays:
      double precision, intent(IN) :: rv(3),tp(3)
!
! Output arrays:
      double precision, intent(OUT) :: R2RDMab_grid, R2RDMba_grid
      double precision, intent(OUT) :: R2RDMab_rfgrad(3), R2RDMba_rfgrad(3)
      double precision, intent(OUT) :: R2RDMab_rflap, R2RDMba_rflap
!
! Local arrays:
      ! may need to include K later
      double precision :: dJ(3), dL(3)
!
! Local scalars:
      integer :: pMO, qMO
      double precision :: GabJpq, GbaJpq, GabLpq, GbaLpq, J, L, d2J, d2L
!
! Begin:
!
      R2RDMab_grid = ZERO
      R2RDMba_grid = ZERO
      R2RDMab_rfgrad = ZERO
      R2RDMba_rfgrad = ZERO
      R2RDMab_rflap  = ZERO
      R2RDMba_rflap  = ZERO
!
! here
      allocate(MO4prodJ(Nbasis,Nbasis),MO4prodK(Nbasis,Nbasis))
      allocate(MO4rfgradJ(Nbasis,Nbasis,3),MO4rfgradL(Nbasis,Nbasis,3))
      allocate(MO4rflapJ(Nbasis,Nbasis),MO4rflapL(Nbasis,Nbasis))
!
      call BLD_MO4prodVGLRF_JL(rv,tp,Nact,CMO%coeff) ! assumed NOs copied over
!
      do pMO = 1, Nact
        do qMO = 1, Nact
!
          GabJpq = R2RDMJab(pMO,qMO)
          GabLpq = R2RDMLab(pMO,qMO)
          GbaJpq = R2RDMJba(pMO,qMO)
          GbaLpq = R2RDMLba(pMO,qMO)
!
          J = MO4prodJ(pMO,qMO)
          L = MO4prodK(pMO,qMO)
          dJ(1:3) = MO4rfgradJ(pMO,qMO,1:3)
          dL(1:3) = MO4rfgradL(pMO,qMO,1:3)
          d2J = MO4rflapJ(pMO,qMO)
          d2L = MO4rflapL(pMO,qMO)
!
          R2RDMab_grid = R2RDMab_grid + GabJpq*J + GabLpq*L 
          R2RDMba_grid = R2RDMba_grid + GbaJpq*J + GbaLpq*L 
!
          R2RDMab_rfgrad(1:3) = R2RDMab_rfgrad(1:3) + GabJpq*dJ(1:3) + GabLpq*dL(1:3) 
          R2RDMba_rfgrad(1:3) = R2RDMba_rfgrad(1:3) + GbaJpq*dJ(1:3) + GbaLpq*dL(1:3) 
!
          R2RDMab_rflap = R2RDMab_rflap + GabJpq*d2J + GabLpq*d2L 
          R2RDMba_rflap = R2RDMba_rflap + GbaJpq*d2J + GbaLpq*d2L 
!
        end do ! qMO
      end do ! pMO
!
      deallocate(MO4prodJ,MO4prodK,MO4rfgradJ,MO4rfgradL,MO4rflapJ,MO4rflapL)
!
      end subroutine GET_R2RDM_VGLRF_OS_point
      subroutine GET_R2RDM_PS_point (rv, tp, Nact, R2RDMaa_grid, R2RDMbb_grid)
!*******************************************************************************
!     Date last modified: October 18, 2021                                     *
!     Authors: JWH                                                             *
!     Description: Compute the parallel-spin two-electron density at a point   *
!                  from a JKL-cumulant functional, given the coordinates of    *
!                  a test electron.                                            *
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE INT_objects

      implicit none
!
! Input scalars:
      integer, intent(IN) :: Nact
!
! Input arrays:
      double precision, intent(IN) :: rv(3),tp(3)
!
! Output scalars:
      double precision, intent(OUT) :: R2RDMaa_grid, R2RDMbb_grid
!
! Local scalars:
      integer :: pMO, qMO
      double precision :: JmK
!
! Begin:
!
      R2RDMaa_grid=ZERO
      R2RDMbb_grid=ZERO
!
      allocate(MO4prodJ(Nbasis,Nbasis),MO4prodK(Nbasis,Nbasis))
      call BLD_MO4prodJK(rv,tp,Nact,CMO%coeff) ! assumed NOs copied over
!
      do pMO = 1, Nact
        do qMO = 1, Nact
          JmK = MO4prodJ(pMO,qMO) - MO4prodK(pMO,qMO)
!
          R2RDMaa_grid = R2RDMaa_grid + R2RDMaa(pMO,qMO)*JmK
          R2RDMbb_grid = R2RDMbb_grid + R2RDMbb(pMO,qMO)*JmK
!
        end do ! qMO
      end do ! pMO
!
      deallocate(MO4prodJ,MO4prodK)
!
      end subroutine GET_R2RDM_PS_point
      subroutine GET_1RDM_lap_point (rv, Nact, rdmlap)
!*******************************************************************************
!     Date last modified: February 15, 2022                                    *
!     Authors: JWH                                                             *
!     Description: Compute the laplacian of the 1-rdm                          *
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE INT_objects

      implicit none
!
! Input scalars:
      integer, intent(IN) :: Nact
!
! Input arrays:
      double precision, intent(IN) :: rv(3)
!
! Output arrays:
      double precision, intent(OUT) :: rdmlap
!
! Local scalars:
      integer :: pMO
!
! Begin:
!
      rdmlap = zero
!
! here
      allocate(MOd2prod(Nbasis))
!
      call BLD_MOd2prod(rv,Nact,CMO%coeff) ! assumed NOs copied over
!
      do pMO = 1, Nact
!
! Assuming closed-shell (just for testing)
        rdmlap = rdmlap + on(pMO)*MOd2prod(pMO)
!
      end do ! pMO
!
      deallocate(MOd2prod)
!
      end subroutine GET_1RDM_lap_point
