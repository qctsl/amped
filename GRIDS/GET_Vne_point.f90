      subroutine GET_Vne_GRID (XApt, YApt, ZApt, Vne_grid)
!*******************************************************************************
!     Date last modified: March 26, 2019                                       *
!     Authors: Ibrahim Awad                                                    *
!     Description: Calculate the Vne energy density on a grid.                 *
!*******************************************************************************
! Modules:
!
      USE program_constants
      USE type_molecule

      implicit none 
      
      double precision :: Vne_grid,XApt,YApt,ZApt,R_iJ,GET_density_at_xyz
      integer :: ZnumJ, Jatom
      
      Vne_grid=ZERO
      do Jatom=1,Natoms
          ZnumJ = CARTESIAN(Jatom)%Atomic_number
          if(ZnumJ.le.0)cycle
         R_iJ=dsqrt((XApt-CARTESIAN(Jatom)%X)**2 + &
               (YApt-CARTESIAN(Jatom)%Y)**2 + &
               (ZApt-CARTESIAN(Jatom)%Z)**2)
         if(R_iJ.le.1.0D-10)cycle
         Vne_grid=Vne_grid-ZnumJ*GET_density_at_xyz(XApt, YApt, ZApt)/R_iJ
        end do ! Jatom     
      
      end subroutine GET_Vne_GRID
      subroutine GET_Vne_spin_point (xpt, ypt, zpt, Vnea_grid, VneHFa_grid, Vneb_grid, VneHFb_grid)
!*******************************************************************************
!     Date last modified: October 7, 2022                                      *
!     Authors: Ibrahim Awad                                                    *
!     Description: Calculate the Vne energy density on a grid, broken into     *
!                  spin components.                                            *
!*******************************************************************************
! Modules:
!
      USE program_constants
      USE type_molecule

      implicit none 
      
! Input scalars:
      double precision, intent(IN) :: xpt, ypt, zpt
!
! Output scalars:
      double precision, intent(OUT) :: Vnea_grid, VneHFa_grid
      double precision, intent(OUT) :: Vneb_grid, VneHFb_grid
!
! Local arrays:
!
! Local scalars:
      double precision :: R_iA
      double precision :: rhoa_grid, rhob_grid, rhoHFa_grid, rhoHFb_grid 
      integer :: ZnumA, Aatom
!      
!
! Begin:
!
      Vnea_grid   = zero
      VneHFa_grid = zero
      Vneb_grid   = zero
      VneHFb_grid = zero
!
      rhoa_grid   = zero
      rhoHFa_grid = zero
      rhob_grid   = zero
      rhoHFb_grid = zero
!
      call GET_spin_den_point(xpt, ypt, zpt, rhoa_grid, rhoHFa_grid, rhob_grid, rhoHFb_grid)
!
      do Aatom = 1,Natoms
        ZnumA = CARTESIAN(Aatom)%Atomic_number
        if(ZnumA.le.0)cycle
        R_iA = dsqrt((xpt-CARTESIAN(Aatom)%X)**2 + &
                     (ypt-CARTESIAN(Aatom)%Y)**2 + &
                     (zpt-CARTESIAN(Aatom)%Z)**2)
        if(R_iA.le.1.0D-10)cycle
!
        Vnea_grid   =   Vnea_grid - dble(ZnumA)*rhoa_grid/R_iA
        VneHFa_grid = VneHFa_grid - dble(ZnumA)*rhoHFa_grid/R_iA
        Vneb_grid   =   Vneb_grid - dble(ZnumA)*rhob_grid/R_iA
        VneHFb_grid = VneHFb_grid - dble(ZnumA)*rhoHFb_grid/R_iA
!
      end do ! Aatom     
      
      end subroutine GET_Vne_spin_point
