      subroutine BLD_Tp_spin_mesh
!****************************************************************************************
!     Date last modified: October 7, 2022                                               *
!     Authors: JWH                                                                      *
!     Description: Calculate the alpha and beta contributions to Tp on a mesh.         *
!****************************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE module_grid_points
      USE type_plotting
      USE type_Weights

      implicit none
!
! Local scalars:
      integer :: Ipoint
      double precision :: X,Y,Z, Tpa_pt, Tpb_pt
      double precision :: TpHFa_pt, TpHFb_pt
!
! Local arrays:
      double precision, dimension(:), allocatable :: AOprod
      double precision, dimension(:), allocatable :: Tpa, Tpb
      double precision, dimension(:), allocatable :: TpHFa, TpHFb
!
! Begin:
      call PRG_manager ('enter', 'BLD_Tp_spin_mesh', 'UTILITY')
!
      call GET_object ('QM', 'SPIN_DENSITY_1MATRIX', 'AO')
!
      ! grid_points may of been destroyed between MENU and here, so rebuild mesh
      ! Really should always be done just before use
      if(LMesh)then
        call BLD_GRID_MESH
      else
        call BLD_GRID_RADIAL
      end if
!
      allocate(Tpa(NGridPoints))
      allocate(Tpb(NGridPoints))
!
      allocate(TpHFa(NGridPoints))
      allocate(TpHFb(NGridPoints))
!
      Tpa = zero
      Tpb = zero
!
      TpHFa = zero
      TpHFb = zero
!
      do Ipoint=1,NGridPoints
!
        X = grid_points(Ipoint)%x
        Y = grid_points(Ipoint)%y
        Z = grid_points(Ipoint)%z
!
        call GET_Tp_spin_point (X, Y, Z, Tpa_pt, TpHFa_pt, Tpb_pt, TpHFb_pt)
!
        Tpa(Ipoint) = Tpa_pt
        Tpb(Ipoint) = Tpb_pt
!
!
        TpHFa(Ipoint) = TpHFa_pt
        TpHFb(Ipoint) = TpHFb_pt
!
      end do ! Ipoint
!
! Print the atom in molecule radial density
!      if(AtomPrint)then
!      do Iatom=1,Natoms
!        Znum=CARTESIAN(Iatom)%Atomic_number
!        if(Znum.le.0)cycle
!        PlotFileID=' '
!        PlotFunction=' '
!        write(PlotFunction,'(a,i4)')'Radial Density Atomic for atom ',Iatom
!        write(PlotFileID,'(a)')'RADIAL_DENSITY_ATOMIC'
!        call BLD_plot_file_ATOMIC (PlotFileID, NGridPoints, Iatom, Plotfile_unit)
!        call PRT_MESH_ATOMIC (Rden_ATOM, Iatom, Natoms, NGridPoints)
!        close (unit=Plotfile_unit)
!      end do ! Iatom
!     end if

! Print the molecular spin-density 
! correlated
! alpha
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'Tp_alpha_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'molecular Tp alpha density'
        call PRT_GRID_MOL (Tpa, NGridPoints, 'F30.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'molecular Tp alpha density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (Tpa, NGridPoints, 'F30.14')
      end if
!
! beta
      if(MoleculePrint)then
       PlotFileID=' '
       write(PlotFileID,'(a)')'Tp_beta_mesh'
       call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
       write(PlotFunction,'(a)')'molecular Tp beta density'
       call PRT_GRID_MOL (Tpb, NGridPoints, 'F30.14')
       close (unit=Plotfile_unit)
      else ! print to standard output
       write(PlotFunction,'(a)')'molecular Tp beta density'
       Plotfile_unit=UNIout
       call PRT_GRID_MOL (Tpb, NGridPoints, 'F30.14')
      end if
!
! Hartree-Fock
! alpha
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'Tp_alpha_HF_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'molecular HF Tp alpha density'
        call PRT_GRID_MOL (TpHFa, NGridPoints, 'F30.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'molecular HF Tp alpha density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (TpHFa, NGridPoints, 'F30.14')
      end if
!
! beta
      if(MoleculePrint)then
       PlotFileID=' '
       write(PlotFileID,'(a)')'Tp_beta_HF_mesh'
       call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
       write(PlotFunction,'(a)')'molecular HF Tp beta density'
       call PRT_GRID_MOL (TpHFb, NGridPoints, 'F30.14')
       close (unit=Plotfile_unit)
      else ! print to standard output
       write(PlotFunction,'(a)')'molecular HF Tp beta density'
       Plotfile_unit=UNIout
       call PRT_GRID_MOL (TpHFb, NGridPoints, 'F30.14')
      end if
!
! End of routine BLD_Tp_spin_mesh
      call PRG_manager ('exit', 'BLD_Tp_spin_mesh', 'UTILITY')
      return
      end subroutine BLD_Tp_spin_mesh
      subroutine BLD_Ts_spin_mesh
!****************************************************************************************
!     Date last modified: October 7, 2022                                               *
!     Authors: JWH                                                                      *
!     Description: Calculate the alpha and beta contributions to Ts on a mesh.         *
!****************************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE module_grid_points
      USE type_plotting
      USE type_Weights

      implicit none
!
! Local scalars:
      integer :: Ipoint
      double precision :: X,Y,Z, Tsa_pt, Tsb_pt
      double precision :: TsHFa_pt, TsHFb_pt
!
! Local arrays:
      double precision, dimension(:), allocatable :: AOprod
      double precision, dimension(:), allocatable :: Tsa, Tsb
      double precision, dimension(:), allocatable :: TsHFa, TsHFb
!
! Begin:
      call PRG_manager ('enter', 'BLD_Ts_spin_mesh', 'UTILITY')
!
      call GET_object ('QM', 'SPIN_DENSITY_1MATRIX', 'AO')
!
      allocate(Tsa(NGridPoints))
      allocate(Tsb(NGridPoints))
!
      allocate(TsHFa(NGridPoints))
      allocate(TsHFb(NGridPoints))
!
      Tsa = zero
      Tsb = zero
!
      TsHFa = zero
      TsHFb = zero
!
      do Ipoint = 1,NGridPoints
!
        X = grid_points(Ipoint)%x
        Y = grid_points(Ipoint)%y
        Z = grid_points(Ipoint)%z
!
        call GET_Ts_spin_point (X, Y, Z, Tsa_pt, TsHFa_pt, Tsb_pt, TsHFb_pt)
!
        Tsa(Ipoint) = Tsa_pt
        Tsb(Ipoint) = Tsb_pt
!
        TsHFa(Ipoint) = TsHFa_pt
        TsHFb(Ipoint) = TsHFb_pt
!
      end do ! Ipoint
!
! Print the atom in molecule radial density
!      if(AtomPrint)then
!      do Iatom=1,Natoms
!        Znum=CARTESIAN(Iatom)%Atomic_number
!        if(Znum.le.0)cycle
!        PlotFileID=' '
!        PlotFunction=' '
!        write(PlotFunction,'(a,i4)')'Radial Density Atomic for atom ',Iatom
!        write(PlotFileID,'(a)')'RADIAL_DENSITY_ATOMIC'
!        call BLD_plot_file_ATOMIC (PlotFileID, NGridPoints, Iatom, Plotfile_unit)
!        call PRT_MESH_ATOMIC (Rden_ATOM, Iatom, Natoms, NGridPoints)
!        close (unit=Plotfile_unit)
!      end do ! Iatom
!     end if

! Print the molecular spin-density 
! correlated
! alpha
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'Ts_alpha_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'molecular Ts alpha density'
        call PRT_GRID_MOL (Tsa, NGridPoints, 'F30.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'molecular Ts alpha density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (Tsa, NGridPoints, 'F30.14')
      end if
!
! beta
      if(MoleculePrint)then
       PlotFileID=' '
       write(PlotFileID,'(a)')'Ts_beta_mesh'
       call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
       write(PlotFunction,'(a)')'molecular Ts beta density'
       call PRT_GRID_MOL (Tsb, NGridPoints, 'F30.14')
       close (unit=Plotfile_unit)
      else ! print to standard output
       write(PlotFunction,'(a)')'molecular Ts beta density'
       Plotfile_unit=UNIout
       call PRT_GRID_MOL (Tsb, NGridPoints, 'F30.14')
      end if
!
! Hartree-Fock
! alpha
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'Ts_alpha_HF_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'molecular HF Ts alpha density'
        call PRT_GRID_MOL (TsHFa, NGridPoints, 'F30.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'molecular HF Ts alpha density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (TsHFa, NGridPoints, 'F30.14')
      end if
!
! beta
      if(MoleculePrint)then
       PlotFileID=' '
       write(PlotFileID,'(a)')'Ts_beta_HF_mesh'
       call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
       write(PlotFunction,'(a)')'molecular HF Ts beta density'
       call PRT_GRID_MOL (TsHFb, NGridPoints, 'F30.14')
       close (unit=Plotfile_unit)
      else ! print to standard output
       write(PlotFunction,'(a)')'molecular HF Ts beta density'
       Plotfile_unit=UNIout
       call PRT_GRID_MOL (TsHFb, NGridPoints, 'F30.14')
      end if
!
! End of routine BLD_Ts_spin_mesh
      call PRG_manager ('exit', 'BLD_Ts_spin_mesh', 'UTILITY')
      return
      end subroutine BLD_Ts_spin_mesh
