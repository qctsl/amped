       subroutine GET_COULOMB_MO_point(Xpt,Ypt,Zpt,J_grids,JHF_grids)
!*******************************************************************************
!     Date last modified: May 24 2017                                          *
!     Authors: R.A. Poirier and I. Awad                                        *
!     Description: Compute the Exchange at a grid point                        *
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE module_grid_points
      USE GSCF_work

      implicit none
!
      double precision :: J_grids,JHF_grids,Xpt,Ypt,Zpt

! Local scalars:
      integer :: aMO,bMO
      double precision :: JabF,Jab,Jba
      double precision, dimension(:,:), allocatable :: occ_Jab, Occ_JHFab
           
! Begin:
!
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)
      
      allocate (occ_Jab(1:CMO%NoccMO,1:CMO%NoccMO))
      allocate (occ_JHFab(1:CMO%NoccMO,1:CMO%NoccMO))
      
      call occupancy_Jab (occ_Jab, Occ_JHFab, CMO%NoccMO)
!    
      J_grids=ZERO
      JHF_grids=ZERO        
      do aMO=1,CMO%NoccMO
      do bMO=1,aMO
        call GET_Jab_MO_point (Xpt, Ypt, Zpt, aMO, bMO, Jab, Jba)
        J_grids=J_grids-0.50d0*occ_Jab(aMO,bMO)*(Jab+Jba)
        JHF_grids=JHF_grids-0.50d0*occ_JHFab(aMO,bMO)*(Jab+Jba)
      end do ! bMO
      end do ! aMO
      
      deallocate (occ_Jab)
      deallocate (occ_JHFab)
          
      return
      end subroutine GET_COULOMB_MO_point 
