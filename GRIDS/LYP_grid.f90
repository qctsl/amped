      subroutine BLD_LYP_GRID (Modality)
!****************************************************************************************
!     Date last modified: December 31, 2015                                             *
!     Authors: Ahmad I. Alrawashdeh                                                     *
!     Description: Calculate the LYP correlation energy on a set of grid points (mesh)  *
!***************************************************************************/*************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE module_grid_points
      USE type_plotting

      implicit none
!
! Input scalars:
      character(*) :: Modality
!
! Local scalars:
      integer :: Iatom,Ipoint,IRegion,Znum,Ifound,NRpoints
      double precision :: XApt,YApt,ZApt,WApt,Bweight,LYPcorrEn,TEcorr
      double precision :: Telec,Nelec_r
      double precision :: NelecbyA
      double precision :: EcorrbyA
      double precision :: r_val,StepSize
      double precision :: rho
      character(len=32) :: Pfmt3
! Local array:
      double precision, dimension(:), allocatable  :: EcorrbyR
      double precision, dimension(:), allocatable  :: Regions
      double precision, dimension(:), allocatable  :: NelecbyR
      double precision, dimension(:), allocatable  :: rhobyR
      integer, dimension(:), allocatable  :: NRbyR
!
! Begin:
      call PRG_manager ('enter', 'BLD_LYP_GRID', 'UTILITY')
!
      RADIAL_grid=Modality(1:len_trim(Modality))
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)
      call BLD_BeckeW_XI

      StepSize=MAX_r/NRegions ! Need larger regions to avoid zero's
      allocate(Regions(NRegions))
      do IRegion=1,NRegions
        Regions(IRegion)=dble(IRegion)*StepSize
      end do ! IRegions
!
! Get a temporary file to store plotting data	  
      Pfmt3='(3'//trim(fmt_GridPts)//','//trim(fmt_r2rho)//')'
      PlotFileID=' '
      PlotFunction=' '
      write(PlotFunction,'(a)')'LYP CORR_ENERGY '
      write(PlotFileID,'(a)')'LYP_CORR_ENERGY_MOLECULE'
!
      TEcorr=ZERO
      Telec=ZERO
!
      do Iatom=1,Natoms
      Znum=CARTESIAN(Iatom)%Atomic_number
      if(Znum.le.0)cycle
      Ifound=GRID_loc(Znum)
      NApts_atom=NApoints_atom(Ifound)
      EcorrbyA=ZERO
      NelecbyA=ZERO
      allocate(EcorrbyR(NRegions))
      allocate(NRbyR(NRegions))
      allocate(NelecbyR(NRegions))
      allocate(rhobyR(NRegions))
      EcorrbyR=ZERO
      NRbyR=ZERO
      NelecbyR=ZERO
      rhobyR=ZERO
      if(MoleculePrint.or.AtomPrint)then
        call BLD_plot_file_MOL ('LYP_GRID', NApts_atom, Plotfile_unit)
      else
        Plotfile_unit=UNIout
      end if
!
! Moving grid points on cartesian coordinate grid
      do Ipoint=1,NApts_atom
        XApt=Egridpts(Ifound,Ipoint)%X
        YApt=Egridpts(Ifound,Ipoint)%Y
        ZApt=Egridpts(Ifound,Ipoint)%Z
        r_val=dsqrt(XApt**2+YApt**2+ZApt**2)
        XApt=XApt+CARTESIAN(Iatom)%X
        YApt=YApt+CARTESIAN(Iatom)%Y
        ZApt=ZApt+CARTESIAN(Iatom)%Z
        WApt=Egridpts(Ifound,Ipoint)%w
        call BeckeW1 (XApt, YApt, ZApt, Iatom, Bweight)
        call GET_rho_Atom (XApt, YApt, ZApt, rho)  ! Used for Ecorr
        call E_LYP_GRID

        TEcorr=TEcorr+LYPcorrEn
        Telec=Telec+Nelec_r
        do IRegion=1,NRegions
        if(r_val.le.Regions(IRegion))then
          EcorrbyR(IRegion)=EcorrbyR(IRegion)+LYPcorrEn
          NRbyR(IRegion)=NRbyR(IRegion)+1
          NelecbyR(IRegion)=NelecbyR(IRegion)+Nelec_r
          rhobyR(IRegion)=rhobyR(IRegion)+rho
          exit
        else 
        end if
        end do ! IRegion
        if(r_val.gt.Regions(NRegions))then
          EcorrbyR(NRegions)=EcorrbyR(NRegions)+LYPcorrEn
          NRbyR(NRegions)=NRbyR(NRegions)+1
          NelecbyR(NRegions)=NelecbyR(NRegions)+Nelec_r
          rhobyR(NRegions)=rhobyR(NRegions)+rho
        end if
      end do ! Ipoint
      write(Plotfile_unit,*)'Iatom: ',Iatom
      write(Plotfile_unit,'(a)')'    Iregion  Region     Npoints   Ecorr      Nelec'
        call flush(Plotfile_unit)
      NRpoints=0
      do IRegion=1,NRegions
        NRpoints=NRpoints+NRbyR(IRegion)
        EcorrbyR(IRegion)=FourPi*EcorrbyR(IRegion)
        NelecbyR(IRegion)=FourPi*NelecbyR(IRegion)
        EcorrbyA=EcorrbyA+EcorrbyR(IRegion)
        NelecbyA=NelecbyA+NelecbyR(IRegion)
        write(Plotfile_unit,'(i8,f12.6,i8,2f12.6)')IRegion,Regions(IRegion),NRbyR(IRegion), &
              EcorrbyR(IRegion),NelecbyR(IRegion)
        call flush(Plotfile_unit)
      end do
      write(Plotfile_unit,'(a)')'    Iregion  Region          rho'
      do IRegion=1,NRegions
        write(Plotfile_unit,'(i8,f12.6,f16.6)')IRegion,Regions(IRegion),rhobyR(IRegion)
        call flush(Plotfile_unit)
      end do
! Deallocate LYP_Ecorr: 
      deallocate(EcorrbyR)
      deallocate(NRbyR)
      deallocate(NelecbyR)
      deallocate(rhobyR)
      write(Plotfile_unit,'(a,i8)')'NApts_atom: ',NApts_atom
      write(Plotfile_unit,'(a)')'    Atom  Npoints     Ecorr      Nelec'
      write(Plotfile_unit,'(2i8,3f12.6)')Iatom,NRpoints,EcorrbyA,NelecbyA
      call flush(Plotfile_unit)
      end do ! Iatom
      deallocate(Regions)
!
      TEcorr=TEcorr*FourPi
      Telec=Telec*FourPi
      write(UNIout,'(a,f12.6)')'TEcorr: ',TEcorr
      write(UNIout,'(a,f12.6)')'Telec: ',Telec
      call flush(Plotfile_unit)
!
      if(MoleculePrint.or.AtomPrint)close (unit=Plotfile_unit)
!
! End of routine BLD_LYP_GRID
      call PRG_manager ('exit', 'BLD_LYP_GRID', 'UTILITY')
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine E_LYP_GRID
!***************************************************************************************
!     Date last modified: December 31, 2015                                            *
!     Authors: Ahmad I. Alrawashdeh                                                    *
!     Description: Calculate the LYP correlation energy at a point (Xpt, Ypt, Zpt)     *
!***************************************************************************************
! Modules:

      implicit none

! Input scalars:
!
! Output scalars:
!
! Local scalars:
      double precision :: CF,tw,drho_X,drho_Y,drho_Z,drho_sq,d2rho,Hess(3,3)
      double precision :: TWeights,Trho1_3,Trho4_3,TrhoM1_3,TrhoM2_3,Tbterm,Texp,TDenom
      double precision, parameter :: a=0.049D0
      double precision, parameter :: b=0.132D0
      double precision, parameter :: c=0.2533D0
      double precision, parameter :: d=0.349D0
      double precision, parameter :: TmT=1.0D-10
      double precision, parameter :: F1_3=1.0D0/3.0D0
      double precision, parameter :: F2_3=2.0D0/3.0D0
      double precision, parameter :: F4_3=4.0D0/3.0D0
      double precision, parameter :: F5_3=5.0D0/3.0D0
      double precision, parameter :: F1_4=1.0D0/4.0D0
      double precision, parameter :: F1_8=1.0D0/8.0D0
      double precision, parameter :: F1_9=1.0D0/9.0D0
      double precision, parameter :: F1_18=1.0D0/18.0D0
!
! Begin: 
      CF=0.3D0*(THREE*PI_VAL**2)**F2_3
!
! Get density (rho), gradient (squared) and Laplacian of rho at Ipoint:
        call GET_drho_Atom (XApt, YApt, ZApt, drho_X, drho_Y, drho_Z)
        drho_sq=drho_X**2+drho_Y**2+drho_Z**2    ! gradient (squared)
        call GET_d2rho_Atom (XApt, YApt, ZApt, Hess)
        d2rho=Hess(1,1)+Hess(2,2)+Hess(3,3)
!
! Calculate the LYP correlation enrergy at Ipoint:
        TWeights=Bweight*WApt
        Nelec_r=rho*TWeights
        if(rho.lt.TmT)then
          Trho1_3=-a/(d+rho**F1_3)
          Trho4_3=rho**F4_3
          LYPcorrEn=Tweights*Trho1_3*Trho4_3
        else
          tw=F1_8*(drho_sq/rho-d2rho)
          TrhoM1_3=rho**(-F1_3)
          TrhoM2_3=rho**(-F2_3)
          Tbterm=b*TrhoM2_3*(CF*rho**F5_3-TWO*tw+(F1_9*tw+F1_18*d2rho))
          Texp=dexp(-c*TrhoM1_3)
          TDenom=-a/(ONE+d*TrhoM1_3)
          LYPcorrEn=TWeights*TDenom*(rho+Tbterm*Texp)
        end if
!
! End of routine E_LYP_GRID
      return
      end subroutine E_LYP_GRID 
! END CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine BLD_LYP_GRID
