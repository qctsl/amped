       subroutine GET_EAMDss_point (Xpt, Ypt, Zpt, EMaa_grid, EMHFaa_grid, AMaa_grid, AMHFaa_grid,&
                                                   EMbb_grid, EMHFbb_grid, AMbb_grid, AMHFbb_grid)
!*******************************************************************************
!     Date last modified: April 24 2019                                        *
!     Authors: JWH                                                             *
!     Description: Compute the equi and anti momentum density at a point.      *
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE INT_objects

      implicit none
!
! Input scalars:
      double precision, intent(IN) :: Xpt,Ypt,Zpt
!
! Output scalars:
      double precision, intent(OUT) :: EMaa_grid,EMHFaa_grid
      double precision, intent(OUT) :: AMaa_grid,AMHFaa_grid
      double precision, intent(OUT) :: EMbb_grid,EMHFbb_grid
      double precision, intent(OUT) :: AMbb_grid,AMHFbb_grid
!
! Local scalars:
      double precision :: rv(3)
!           
! Begin:
!
      call GET_object ('QM', '2RDM', 'AO')
!    
      EMaa_grid=ZERO
      EMHFaa_grid=ZERO        
      AMaa_grid=ZERO
      AMHFaa_grid=ZERO        
!
      EMbb_grid=ZERO
      EMHFbb_grid=ZERO        
      AMbb_grid=ZERO
      AMHFbb_grid=ZERO        
!
      rv(1) = Xpt
      rv(2) = Ypt
      rv(3) = Zpt
!
      call BLD_EAMDint (rv)
!
      EMaa_grid = dot_product(TRDM_AOaa,emdAO)
      EMHFaa_grid = dot_product(TRDM_HF_AOaa,emdAO)
!
      AMaa_grid = dot_product(TRDM_AOaa,amdAO)
      AMHFaa_grid = dot_product(TRDM_HF_AOaa,amdAO)
!
      EMbb_grid = dot_product(TRDM_AObb,emdAO)
      EMHFbb_grid = dot_product(TRDM_HF_AObb,emdAO)
!
      AMbb_grid = dot_product(TRDM_AObb,amdAO)
      AMHFbb_grid = dot_product(TRDM_HF_AObb,amdAO)
!
      return
      end subroutine GET_EAMDss_point 
      subroutine GET_EAMDos_point (Xpt, Ypt, Zpt, EMab_grid, EMHFab_grid, AMab_grid, AMHFab_grid,&
                                                  EMba_grid, EMHFba_grid, AMba_grid, AMHFba_grid)
!*******************************************************************************
!     Date last modified: April 24 2019                                        *
!     Authors: JWH                                                             *
!     Description: Compute the equi and anti momentum density at a point.      *
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE INT_objects

      implicit none
!
! Input scalars:
      double precision, intent(IN) :: Xpt,Ypt,Zpt
!
! Output scalars:
      double precision, intent(OUT) :: EMab_grid,EMHFab_grid
      double precision, intent(OUT) :: AMab_grid,AMHFab_grid
      double precision, intent(OUT) :: EMba_grid,EMHFba_grid
      double precision, intent(OUT) :: AMba_grid,AMHFba_grid
!
! Local scalars:
      double precision :: rv(3)
!           
! Begin:
!
      call GET_object ('QM', '2RDM', 'AO')
!    
      EMab_grid=ZERO
      EMHFab_grid=ZERO        
      AMab_grid=ZERO
      AMHFab_grid=ZERO        
!
      EMba_grid=ZERO
      EMHFba_grid=ZERO        
      AMba_grid=ZERO
      AMHFba_grid=ZERO        
!
      rv(1) = Xpt
      rv(2) = Ypt
      rv(3) = Zpt
!
      call BLD_EAMDint (rv)
!
      EMab_grid = dot_product(TRDM_AOab,emdAO)
      EMHFab_grid = dot_product(TRDM_HF_AOab,emdAO)
!
      AMab_grid = dot_product(TRDM_AOab,amdAO)
      AMHFab_grid = dot_product(TRDM_HF_AOab,amdAO)
!
      EMba_grid = dot_product(TRDM_AOba,emdAO)
      EMHFba_grid = dot_product(TRDM_HF_AOba,emdAO)
!
      AMba_grid = dot_product(TRDM_AOba,amdAO)
      AMHFba_grid = dot_product(TRDM_HF_AOba,amdAO)
!
      return
      end subroutine GET_EAMDos_point 
