      subroutine BLD_Vne_spin_mesh
!****************************************************************************************
!     Date last modified: October 7, 2022                                               *
!     Authors: JWH                                                                      *
!     Description: Calculate the alpha and beta contributions to Vne on a mesh.         *
!****************************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE module_grid_points
      USE type_plotting
      USE type_Weights

      implicit none
!
! Local scalars:
      integer :: Ipoint
      double precision :: X,Y,Z, Vnea_pt, Vneb_pt
      double precision :: VneHFa_pt, VneHFb_pt
!
! Local arrays:
      double precision, dimension(:), allocatable :: AOprod
      double precision, dimension(:), allocatable :: Vnea, Vneb
      double precision, dimension(:), allocatable :: VneHFa, VneHFb
!
! Begin:
      call PRG_manager ('enter', 'BLD_Vne_spin_mesh', 'UTILITY')
!
      call GET_object ('QM', 'SPIN_DENSITY_1MATRIX', 'AO')
!
      ! grid_points may of been destroyed between MENU and here, so rebuild mesh
      ! Really should always be done just before use
      if(LMesh)then
        call BLD_GRID_MESH
      else
        call BLD_GRID_RADIAL
      end if
!
      allocate(Vnea(NGridPoints))
      allocate(Vneb(NGridPoints))
!
      allocate(VneHFa(NGridPoints))
      allocate(VneHFb(NGridPoints))
!
      Vnea = ZERO
      Vneb = ZERO
!
      VneHFa = ZERO
      VneHFb = ZERO
!
      do Ipoint = 1, NGridPoints
!
        X = grid_points(Ipoint)%x
        Y = grid_points(Ipoint)%y
        Z = grid_points(Ipoint)%z
!
        call GET_Vne_spin_point (X, Y, Z, Vnea_pt, VneHFa_pt, Vneb_pt, VneHFb_pt)
!
!
        Vnea(Ipoint) = Vnea_pt
        Vneb(Ipoint) = Vneb_pt
!
        VneHFa(Ipoint) = VneHFa_pt
        VneHFb(Ipoint) = VneHFb_pt
!
      end do ! Ipoint
!
! Print the atom in molecule radial density
!      if(AtomPrint)then
!      do Iatom=1,Natoms
!        Znum=CARTESIAN(Iatom)%Atomic_number
!        if(Znum.le.0)cycle
!        PlotFileID=' '
!        PlotFunction=' '
!        write(PlotFunction,'(a,i4)')'Radial Density Atomic for atom ',Iatom
!        write(PlotFileID,'(a)')'RADIAL_DENSITY_ATOMIC'
!        call BLD_plot_file_ATOMIC (PlotFileID, NGridPoints, Iatom, Plotfile_unit)
!        call PRT_MESH_ATOMIC (Rden_ATOM, Iatom, Natoms, NGridPoints)
!        close (unit=Plotfile_unit)
!      end do ! Iatom
!     end if

! Print the molecular spin-density 
! correlated
! alpha
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'Vne_alpha_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'molecular Vne alpha density'
        call PRT_GRID_MOL (Vnea, NGridPoints, 'F30.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'molecular Vne alpha density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (Vnea, NGridPoints, 'F30.14')
      end if
!
! beta
      if(MoleculePrint)then
       PlotFileID=' '
       write(PlotFileID,'(a)')'Vne_beta_mesh'
       call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
       write(PlotFunction,'(a)')'molecular Vne beta density'
       call PRT_GRID_MOL (Vneb, NGridPoints, 'F30.14')
       close (unit=Plotfile_unit)
      else ! print to standard output
       write(PlotFunction,'(a)')'molecular Vne beta density'
       Plotfile_unit=UNIout
       call PRT_GRID_MOL (Vneb, NGridPoints, 'F30.14')
      end if
!
! Hartree-Fock
! alpha
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'Vne_alpha_HF_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'molecular HF Vne alpha density'
        call PRT_GRID_MOL (VneHFa, NGridPoints, 'F30.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'molecular HF Vne alpha density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (VneHFa, NGridPoints, 'F30.14')
      end if
!
! beta
      if(MoleculePrint)then
       PlotFileID=' '
       write(PlotFileID,'(a)')'Vne_beta_HF_mesh'
       call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
       write(PlotFunction,'(a)')'molecular HF Vne beta density'
       call PRT_GRID_MOL (VneHFb, NGridPoints, 'F30.14')
       close (unit=Plotfile_unit)
      else ! print to standard output
       write(PlotFunction,'(a)')'molecular HF Vne beta density'
       Plotfile_unit=UNIout
       call PRT_GRID_MOL (VneHFb, NGridPoints, 'F30.14')
      end if

      deallocate (Vnea,Vneb)
      deallocate (VneHFa,VneHFb)
!
! End of routine BLD_Vne_spin_mesh
      call PRG_manager ('exit', 'BLD_Vne_spin_mesh', 'UTILITY')
      return
      end subroutine BLD_Vne_spin_mesh
