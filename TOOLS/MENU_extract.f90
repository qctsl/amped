      subroutine MENU_extract
!*************************************************************************************************
!     Date last modified:  July 14, 2010                                                         *
!     Author:  R.A. Poirier                                                                      *
!     Description: MENU for reading files, i.e., Gaussian output file                            *
!*************************************************************************************************
! Modules
      USE program_codes
      USE program_parser
      USE menu_gets
      USE type_basis_set
      USE type_molecule

      implicit none
!
! Local scalars:
      integer :: Ichar,Ifile,Iposn,lenName,lenstr,Nfiles,READ_unit
      logical :: done,Lerror
      character(len=MAX_length) EXtract,File_name,File_type,string,ORient
      character(len=MAX_string) FileIN,FileOUT
      integer, parameter :: MAX_names=2000
      character(len=MAX_length) :: File_list(MAX_names)
!      type (basis_set), target :: GAU_basis
!      type (atom_definition), dimension(:), allocatable, target :: MOL_atomsGAU
!      type (molecule_definition), target ::  moleculeGAU
!      type (cartesian_coordinates), dimension(:), allocatable, target :: CartesianGAU
!
! Begin:
      call PRG_manager ('enter', 'MENU_extract', 'UTILITY')
!
      Nfiles=0
      done=.false.
      gaumolinfo=.false.
      gaudensityinfo=.false.
      gauMOinfo=.false.
      do while (.not.done)
      call new_token (' EXTRACT:')
      if     (token(1:4).eq.'HELP')then
      write(UNIout,'(A)') &
      ' Command "EXTRACT":', &
      '   Purpose: Read a file and extract data', &
      '   Syntax EXTRACT                                  ', &
      '   EXtract = <string>, i.e., Density matrix (also implies ORientation=INput)', &
      '   FIletype = <string>, i.e., Gaussian (only option)', &
!      '   ORientation = <string>, i.e., INput or STandard', &
      '   NAme (file) = <string>, i.e., GS_CH4_HF631Gd.out ', &
      '   end'

! EXtract
      else if(token(1:2).eq.'EX')then
        call GET_value (string, lenstr)
        EXtract(1:)=string(1:lenstr)//'       '
        if(EXtract(1:2).eq.'AR')then
          EXtract(1:)='ARCHIVE'
        else if(Extract(1:2).eq.'DE')then
          EXtract(1:)='DENSITY'
          gaumolinfo=.true. ! will also get MOL information from the Gaussian output file 
          gaudensityinfo=.true.
          gauMOinfo=.true.
!        else if(Extract(1:2).eq.'MO')then
!          EXtract(1:)='MOLECULE'
!          gaumolinfo=.true. ! will get MOL information from the Gaussian output file
        else
          write(UNIout,'(a)')'ERROR> MENU_extract: EXtract option unknown'
          stop 'ERROR> MENU_extract: EXtract option unknown'
        end if
!
! FIletype
      else if(token(1:2).eq.'FI')then
        call GET_value (string, lenstr)
        File_type(1:)=string(1:lenstr)//'       '
        if(File_type(1:2).eq.'GA')then
          File_type(1:)='gaussian'
        else
          write(UNIout,'(a)')'ERROR> MENU_extract: File type option unknown'
          stop 'ERROR> MENU_extract: File type option unknown'
        end if
!
! ORientation
!      else if(token(1:2).eq.'OR')then
!        gaumolinfo=.true.
!        call GET_value (string, lenstr)
!        Gaussian%ORient(1:)=string(1:lenstr)//'       '
!        if(Gaussian%ORient(1:2).eq.'ST')then
!          Gaussian%ORient(1:)='STANDARD'
!        else if(Gaussian%ORient(1:2).eq.'IN')then
!          Gaussian%ORient(1:)='INPUT'
!        end if
! NAme
      else if(token(1:2).eq.'NA')then
        call GET_value (string, lenstr)
        Gaussian%file_input=string(1:lenstr)//'       '
!        write(UNIout,'(a)')'Will extract Gaussian information from file: ',Gaussian%file_input(1:lenstr)
!        write(UNIout,'(a)')
      else
         call MENU_end (done)
      end if

      end do ! while

! end of routine MENU_extract
      call PRG_manager ('exit', 'MENU_extract', 'UTILITY')

      return
      end subroutine MENU_extract
