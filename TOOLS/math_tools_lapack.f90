      subroutine cholesky_decomp(N,R)
!***********************************************************************
!     Date last modified: September 2, 2020                            *
!     Author: JWH                                                      *
!     Description: Cholesky decompose a matrix (LAPACK wrapper)        *
!***********************************************************************

      implicit none

! Input variables

      integer,intent(in)            :: N
      double precision,intent(inout):: R(N,N)

! Local variables

      integer                       :: info

      call dpotrf('U',N,R,N,info)

      if(info /= 0) then
        print*,'Problem in cholesky decomp (dpotrf)!!'
      endif

      end subroutine cholesky_decomp
      subroutine diagonalize_matrix(N,A,e)
!***********************************************************************
!     Date last modified: September 21, 2018                           *
!     Author: T2                                                       *
!     Description: Diagonalize a square matrix (LAPACK wrapper)        *
!***********************************************************************

      implicit none

! Input variables

      integer,intent(in)            :: N
      double precision,intent(inout):: A(N,N)
      double precision,intent(out)  :: e(N)

! Local variables

      integer                       :: lwork,info
      double precision,allocatable  :: work(:)

! Memory allocation

      allocate(work(3*N))
      lwork = size(work)

      call dsyev('V','U',N,A,N,e,work,lwork,info)

      if(info /= 0) then
        print*,'Problem in diagonalize_matrix (dsyev)!!'
      endif

      end subroutine diagonalize_matrix
      subroutine invert_matrix(N,A,B)
!***********************************************************************
!     Date last modified: September 21, 2018                           *
!     Author: T2                                                       *
!     Description: Returns inverse of square matrix A in B             *
!                  (LAPACK wrapper)                                    *
!***********************************************************************

      implicit none

      integer,intent(in)             :: N
      double precision, intent(in)   :: A(N,N)
      double precision, intent(out)  :: B(N,N)

      integer                        :: info,lwork
      integer, allocatable           :: ipiv(:)
      double precision,allocatable   :: work(:)

      allocate (ipiv(N),work(N*N))
      lwork = size(work)

      B(1:N,1:N) = A(1:N,1:N)

      call dgetrf(N,N,B,N,ipiv,info)

      if (info /= 0) then

        print*,info
        stop 'error in invert (dgetrf)!!'

      endif

      call dgetri(N,B,N,ipiv,work,lwork,info)

      if (info /= 0) then

        print *,  info
        stop 'error in invert (dgetri)!!'

      endif

      deallocate(ipiv,work)

      end subroutine invert_matrix
      subroutine linear_solve(N,A,b,x,rcond)
!***********************************************************************
!     Date last modified: September 21, 2018                           *
!     Author: T2                                                       *
!     Description: Solve linear system Ax=b where A is a NxN matrix    *
!                  and x and b are vectors of size N (LAPACK wrapper)  *
!***********************************************************************

      implicit none

      integer,intent(in)             :: N
      double precision,intent(in)    :: A(N,N),b(N)
      double precision,intent(out)   :: x(N)
      double precision               :: rcond ! remove T2s intent(in)

      integer                        :: info,lwork
      double precision               :: ferr,berr
      integer,allocatable            :: ipiv(:),iwork(:)
      double precision,allocatable   :: AF(:,:),work(:)

      lwork = 3*N
      allocate(AF(N,N),ipiv(N),work(lwork),iwork(N))

      call dsysvx('N','U',N,1,A,N,AF,N,ipiv,b,N,x,N,rcond,ferr,berr,work,lwork,iwork,info)

      end subroutine linear_solve
      subroutine prepend(N,M,A,b)
!***********************************************************************
!     Date last modified: September 21, 2018                           *
!     Author: T2                                                       *
!     Description: Prepend the vector b of size N to the matrix A of   *
!                  size NxM.                                           *
!***********************************************************************

      implicit none

! Input variables

      integer,intent(in)            :: N,M
      double precision,intent(in)   :: b(N)

! Local variables

      integer                       :: i,j

! Output variables

      double precision,intent(inout)  :: A(N,M)

      do i=1,N
        do j=M-1,1,-1
          A(i,j+1) = A(i,j)
        enddo
        A(i,1) = b(i)
      enddo

      end subroutine prepend
      subroutine DIIS_extrapolation(n_err,n_vec,n_diis,error,vec,error_in,vec_inout)
!***********************************************************************
!     Date last modified: September 20, 2018                           *
!     Author: JWH                                                      *
!     Description: Extrapolate a vector according to DIIS algorithm.   *
!                  (Stolen from T2! "quantum package")                 * 
!***********************************************************************

      implicit none
!
! Input:
!
      integer,intent(in)            :: n_err,n_vec 
      double precision,intent(in)   :: error_in(n_err)
!
! Output:
!
      integer,intent(inout)         :: n_diis
      double precision,intent(inout):: vec_inout(n_vec),error(n_err,n_diis),vec(n_vec,n_diis)
!
! Local scalars:
!
      double precision :: rcond
!
! Local arrays:
!
      double precision, allocatable :: A(:,:), b(:), c(:)
!
! Begin:
!
      call PRG_manager('enter','DIIS_extrapolation','UTILITY')
!
      allocate(A(n_diis+1,n_diis+1),b(n_diis+1),c(n_diis+1))
!
! Update DIIS "history"
!
      call prepend(n_err,n_diis,error,error_in)
      call prepend(n_vec,n_diis,vec,vec_inout)
!
!  Build A matrix

      A(1:n_diis,1:n_diis) = matmul(transpose(error),error)
!
      A(1:n_diis,n_diis+1) = -1d0
      A(n_diis+1,1:n_diis) = -1d0
      A(n_diis+1,n_diis+1) = +0d0
!
! Build b matrix

      b(1:n_diis) = +0d0
      b(n_diis+1) = -1d0
!
! Solve linear system for c
!
      call linear_solve(n_diis+1,A,b,c,rcond)
!
! Extrapolate

      if(rcond.gt.1d-14)then
!
        vec_inout(:) = matmul(c(1:n_diis),transpose(vec(:,1:n_diis)))
!
      else
!
        n_diis = 0
!
      endif
!
! end routine DIIS_extrapolation
      call PRG_manager ('exit', 'DIIS_extrapolation', 'UTILITY')
      end subroutine DIIS_extrapolation
      subroutine ADAt(N,A,D,B)
!***********************************************************************
!     Date last modified: September 26, 2018                           *
!     Author: JWH                                                      *
!     Description: Perform B = A.D.At where A is a NxN matrix and D is * 
!                  a diagonal matrix given ! as a vector of length N.  *
!                  (Stolen from T2! "quantum package")                 * 
!***********************************************************************

      implicit none

! Input variables

      integer,intent(in)            :: N
      double precision,intent(in)   :: A(N,N),D(N)

! Local viaruabkes

      integer                       :: i,j,k

! Output variables

      double precision,intent(out)  :: B(N,N)

      B = 0d0

      do i=1,N
        do j=1,N
          do k=1,N
            B(i,k) = B(i,k) + A(i,j)*D(j)*A(k,j)
          enddo
        enddo
      enddo

      end subroutine ADAt
      subroutine AD(N,A,D)
!***********************************************************************
!     Date last modified: September 26, 2018                           *
!     Author: JWH                                                      *
!     Description: Perform A = A.D where A is a NxN matrix and D is    * 
!                  a diagonal matrix given ! as a vector of length N.  *
!                  (Stolen from T2! "quantum package")                 * 
!***********************************************************************

      implicit none

      integer,intent(in)            :: N
      integer                       :: i,j,k
      double precision,intent(in)   :: D(N)
      double precision,intent(inout):: A(N,N)

      do i=1,N
        do j=1,N
          A(i,j) = A(i,j)*D(j)
        enddo
      enddo

      end subroutine AD
      function Kronecker_delta(i,j) result(delta)
!***********************************************************************
!     Date last modified: September 26, 2018                           *
!     Author: JWH                                                      *
!     Description: Kronecker delta                                     * 
!                  (Stolen from T2! "quantum package")                 * 
!***********************************************************************

      implicit none

! Input variables

      integer,intent(in)            :: i,j

! Output variables

      double precision              :: delta

      if(i == j) then
        delta = 1d0
      else
        delta = 0d0
      endif

      end function Kronecker_delta
      subroutine DA(N,D,A)
!***********************************************************************
!     Date last modified: September 26, 2018                           *
!     Author: JWH                                                      *
!     Description: Perform A <- D.A where A is a NxN matrix and D is a *
!                  diagonal matrix given as a vector of length N.      * 
!                  (Stolen from T2! "quantum package")                 * 
!***********************************************************************

      implicit none

      integer,intent(in)            :: N
      integer                       :: i,j,k
      double precision,intent(in)   :: D(N)
      double precision,intent(inout):: A(N,N)

      do i=1,N
        do j=1,N
          A(i,j) = D(i)*A(i,j)
        enddo
      enddo

      end subroutine DA
      function trace_matrix(n,A) result(Tr)
!***********************************************************************
!     Date last modified: September 26, 2018                           *
!     Author: JWH                                                      *
!     Description: Calculate trace of the square matrix A.             *
!                  (Stolen from T2! "quantum package")                 * 
!***********************************************************************

      implicit none

! Input variables

      integer,intent(in)            :: n
      double precision,intent(in)   :: A(n,n)

! Local variables

      integer                       :: i

! Output variables

      double precision              :: Tr

      Tr = 0d0
      do i=1,n
        Tr = Tr + A(i,i)
      enddo

      end function trace_matrix
      subroutine orthogonalization_matrix(ortho_type,nBas,S,X)
!***********************************************************************
!     Date last modified: July 16, 2019                                *
!     Author: JWH                                                      *
!     Description: Compute the orthogonalization matrix X.             *
!                  (Stolen from T2! "quantum package")                 * 
!***********************************************************************

      implicit none

! Input variables

      integer,intent(in)            :: nBas,ortho_type
      double precision,intent(in)   :: S(nBas,nBas)

! Local variables

      logical                       :: debug
      double precision,allocatable  :: UVec(:,:),Uval(:)
      double precision,parameter    :: thresh = 1d-6

      integer                       :: i

! Output variables

      double precision,intent(out)  :: X(nBas,nBas)

      debug = .false.

! Type of orthogonalization ortho_type
!
!  1 = Lowdin
!  2 = Canonical
!  3 = SVD
!

      allocate(Uvec(nBas,nBas),Uval(nBas))

      if(ortho_type == 1) then

        write(*,*)
        write(*,*) ' Lowdin orthogonalization'
        write(*,*)

        Uvec = S
        call diagonalize_matrix(nBas,Uvec,Uval)

        do i=1,nBas

          if(Uval(i) > thresh) then

            Uval(i) = 1d0/sqrt(Uval(i))

          else

            write(*,*) 'Eigenvalue',i,'too small for Lowdin orthogonalization'

          endif

        enddo

        call ADAt(nBas,Uvec,Uval,X)

      elseif(ortho_type == 2) then

        write(*,*)
        write(*,*) 'Canonical orthogonalization'
        write(*,*)

        Uvec = S
        call diagonalize_matrix(nBas,Uvec,Uval)

        do i=1,nBas

          if(Uval(i) > thresh) then

            Uval(i) = 1d0/sqrt(Uval(i))

          else

            write(*,*) ' Eigenvalue',i,'too small for canonical orthogonalization'

          endif

        enddo

        call AD(nBas,Uvec,Uval)
        X = Uvec

      elseif(ortho_type == 3) then

        write(*,*)
        write(*,*) ' SVD-based orthogonalization NYI'
        write(*,*)
      endif

! Print results

      if(debug) then

        write(*,'(A28)') '----------------------'
        write(*,'(A28)') 'Orthogonalization matrix'
        write(*,'(A28)') '----------------------'
        call matout(nBas,nBas,X)
        write(*,*)

      endif

      end subroutine orthogonalization_matrix
