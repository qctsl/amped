      subroutine GaussianReader (READ_unit, PM0Gau, MATlenIn, Lerror)
!***********************************************************************
!     Date last modified: September 24, 2013                           *
!     Authors: Peter Warburton and R.A. Poirier                        *
!     Desciption: Read the molecule/density matrix from a Gaussian     *
!     output                                                           *
!***********************************************************************
! Modules:
      USE program_files
      USE program_codes
      USE type_basis_set
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE menu_bld_basis
      USE type_elements
      USE matrix_print

      implicit none

! Input Scalars:
      integer :: MATlenIN
      logical :: Lerror
!
! Input Arrays:
      double precision :: PM0Gau(MATlenIN)

! Local Scalar:
      integer :: READ_unit,GAU_unit,totline,localIatom,counter,revcount,sread,gaucharge
      integer :: gaumult,loop,scanpos,garbage,lcount,loop2,matrixline,row,col,bascount
      integer :: nelec  
      integer :: impcount ! import count
      logical :: G09,G03,G98,G16,gen,gread,dcart,fullpop,gf,next2,next1,newatm,Lunrestricted
      logical :: tracker,chrgfnd,nosym,soon,full,alpha,beta
      character*3 version,loopcheck
      character*33 preversion
      character*80 text,temptext,tempbasis
      character*5  ortext
      character(len=MAX_formula) lformula
      integer, allocatable, dimension(:) :: mglabel,impbeg,impend,firstatom
      double precision, allocatable, dimension(:,:) :: rhomat, rhomatb
      character*77, allocatable, dimension(:) :: coordtext
      character*80, allocatable, dimension(:) :: tempimport, temp2import

      type (molecule_definition), target ::  GAUmolecule
      type (cartesian_coordinates), dimension(:), allocatable, target :: CartesianGAU

      call PRG_manager ('enter', 'GaussianReader', 'UTILITY')
!
      Lerror=.false.
      gaumolinfo=.true. ! will also get MOL information from the Gaussian output file
      gaudensityinfo=.true.
      gauMOinfo=.true.

      call INITIALIZE_GAUSSIAN_READ 
      if(Lerror)then
        call PRG_manager ('enter', 'GaussianReader', 'UTILITY')
        return
      end if

      if(gaumolinfo)then
       call READ_TITLE_CARD
       call GAUSSIAN_BASIS_SET
! if we have general basis set info, let's do the first reduction of the allocation
! and remap the temporary basis set info into the reduced array and start keeping track
! of the begining and end line for each atoms basis set
       if(gen) then
        call REALLOCATE_BASIS_SET
       end if ! gen
      end if ! gaumolinfo
! read gaussian molecule info
      call GAUSSIAN_MOLECULE_INFO
! MUNgauss only requires basis set by element, not by atom, so we'll finally create
! the actual imported basis set array.  
      if(gen) then
       call REDUCE_BASIS_SET 
      end if !gen
!
! set MUNgauss basis set
      tempbasis=BASIS%name
      write(6,*)'tempbasis: ',tempbasis
! read Gaussian file for density matrix info and resort for MUNgauss
!     call READ_MO_COEFFICIENTS  ! Should be an option and local!!!

! read Gaussian file for density matrix info and resort for MUNgauss
      call READ_DENSITY_MATRIX

      call PRG_manager ('exit', 'GaussianReader', 'UTILITY')

CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine GET_GAUSSIAN_MATRIX (Pmat, Nbasis, mglabel, tracker, counter)
!***********************************************************************
!     Date last modified: September 10, 2013                           *
!     Authors: Peter Warburton and R.A. Poirier                        *
!     Desciption: Read the density matrix from a Gaussian output       *
!***********************************************************************
! Modules:

      implicit none

      integer :: Nbasis
      double precision :: Pmat(Nbasis,Nbasis)
      logical :: tracker
      integer :: counter
!
! Local scalars:
      integer :: highorb,totgroup,colgrp,group,unique,elemcheck,maxshell,readloop,grouploop
      integer :: imin,tempstore,sort
      integer, allocatable, dimension(:) :: gaushell,gaulabel,gauatom,mglabel
      integer, allocatable, dimension(:) :: elemgroup,elemtrack
      character*21 text21
      character*6  text6
      character*5, allocatable, dimension(:) ::  gauelement
      character*2, allocatable, dimension(:) ::  gauorbital
      logical :: newelem

      call PRG_manager ('enter', 'GET_GAUSSIAN_MATRIX', 'UTILITY')

      allocate(elemgroup(NBasis))
      allocate(gaushell(NBasis))
      allocate(gaulabel(NBasis))
      allocate(gauorbital(NBasis))
      allocate(gauatom(NBasis))
      allocate(gauelement(NBasis))     
      allocate(elemtrack(Nbasis))
!
      elemgroup=0
      mglabel=0
      highorb=0
      totgroup=ceiling(Nbasis/5.)
      colgrp=0
      group=0
      unique=1
      elemtrack=0
      elemcheck=0
      maxshell=0
      do readloop=1,totgroup
       do row=((readloop-1)*5+1),Nbasis
        newelem=.TRUE.
        if(readloop.eq.1) then
         read(READ_unit,'(2i4,a3,i2,a2,a6,5(f10.5))') &
             gaulabel(row),gauatom(row),gauelement(row), &
             gaushell(row),gauorbital(row),text6, & 
             (Pmat(row,col),col=colgrp*5+1,min(colgrp*5+5,Nbasis))
         counter=counter+1
         if(gauatom(row).eq.0) gauatom(row) = gauatom(row-1)
         if(gauelement(row).eq.'    ') gauelement(row) = gauelement(row-1)
         if(gaushell(row).gt.maxshell) maxshell=gaushell(row)
         select case(gauorbital(row))
          case('PX','PY','PZ')
           gauorbital(row)='P '
           if(highorb.eq.0) highorb=1
          case('XX','XY','XZ','YY','YZ','ZZ')
           gauorbital(row)='D '
           highorb=2
          case default
           gauorbital(row)='S '
         end select
         elemgroup(row)=CARTESIANGAU(gauatom(row))%Atomic_number
         if(row.eq.1) elemtrack(row)=elemgroup(row)
         do grouploop=1,unique
          if(elemgroup(row).eq.elemtrack(grouploop)) newelem=.FALSE.
         end do
         if(newelem) then
          unique=unique+1
          elemtrack(unique)=elemgroup(row)
         end if
        else
         read(READ_unit,'(a21,5(f10.5))')text21, &
             (Pmat(row,col),col=colgrp*5+1,min(colgrp*5+5,Nbasis))
         counter=counter+1
        end if
       end do
       read(READ_unit,'(a80)')text
       colgrp=colgrp+1
       counter=counter+1
      end do
!  Resort the Gaussian orbitals to match MUNgauss basis sorting FDPS
!  General sort by atomic number from lowest to highest
      do grouploop=1,unique-1
       imin=grouploop
       do loop=grouploop+1,unique 
        if(elemtrack(loop).lt.elemtrack(imin)) imin=loop
       end do
       if(imin.ne.grouploop) then
        tempstore=elemtrack(grouploop)
        elemtrack(grouploop)=elemtrack(imin)
        elemtrack(imin)=tempstore
       end if
      end do         

!  General sort by FDPS

!  Sort by F orbitals, if there are any
!  Assign the F orbitals of the lowest atomic number first, then continue to higher Z
      sort=0
      if (highorb.eq.3) then
       do loop=1,unique
        do grouploop=1,maxshell
         do readloop=1,Nbasis
          if(gaushell(readloop).ne.grouploop) cycle
          if(elemgroup(readloop).ne.elemtrack(loop)) cycle
          if(gauorbital(readloop).ne.'F ') cycle
          sort=sort+1
          mglabel(readloop)=sort        
         end do
        end do
       end do
      end if
!  Sort by D orbitals, if there are any
!  Assign the D orbitals of the lowest atomic number first, then continue to higher Z
      sort=0
      if (highorb.ge.2) then
       do loop=1,unique
        do grouploop=1,maxshell
         do readloop=1,Nbasis
          if(gaushell(readloop).ne.grouploop) cycle
          if(elemgroup(readloop).ne.elemtrack(loop)) cycle
          if(gauorbital(readloop).ne.'D ') cycle
          sort=sort+1
          mglabel(readloop)=sort        
         end do
        end do
       end do
      end if
!  Sort by P orbitals, if there are any
!  Assign the P orbitals of the lowest atomic number first, then continue to higher Z
      if (highorb.ge.1) then
       do loop=1,unique
        do grouploop=1,maxshell
         do readloop=1,Nbasis
          if(gaushell(readloop).ne.grouploop) cycle
          if(elemgroup(readloop).ne.elemtrack(loop)) cycle
          if(gauorbital(readloop).ne.'P ') cycle
          sort=sort+1
          mglabel(readloop)=sort        
         end do
        end do
       end do
      end if
!  Assign the S orbitals of the lowest atomic number first, then continue to higher Z
      do loop=1,unique
       do grouploop=1,maxshell
        do readloop=1,Nbasis
         if(gaushell(readloop).ne.grouploop) cycle
         if(elemgroup(readloop).ne.elemtrack(loop)) cycle
         if(gauorbital(readloop).ne.'S ') cycle
         sort=sort+1
         mglabel(readloop)=sort        
        end do
       end do
      end do
!  Gaussian density matrix labels printing
      if(tracker) then
       write(UNIout,'(a)') 'Gaussian density matrix basis function mapping'
       write(UNIout,'(a)') 'GaussianLabel, MUNgauss Label, Atom #, Element, Shell, Orbital'
       do readloop=1,Nbasis
        write(UNIout,'(i7,i16,i12,a10,i8,a8)') &
             gaulabel(readloop),mglabel(readloop),gauatom(readloop), &
             gauelement(readloop),gaushell(readloop),gauorbital(readloop) 
       end do 
      end if
!
      deallocate(gauatom)
      deallocate(gauelement)
      deallocate(gaushell)
      deallocate(gauorbital)
      deallocate(gaulabel)
!
      call PRG_manager ('exit', 'GET_GAUSSIAN_MATRIX', 'UTILITY')
      end subroutine GET_GAUSSIAN_MATRIX      
      subroutine INITIALIZE_GAUSSIAN_READ 
!***********************************************************************
!     Date last modified: July 15, 2016                                *
!     Authors: P. L. Warburton                                         *
!     Desciption: Moves imported basis set info to smaller array       *
!***********************************************************************
! Modules:

      implicit none
! Local scalar:
      integer :: Iiostat

      call PRG_manager ('enter', 'INITIALIZE_GAUSSIAN_READ', 'UTILITY')

!  SETTING OF INITIAL VARIABLES
      totline=0
      nelec=0
      G09 = .FALSE.
      G03 = .FALSE.
      G98 = .FALSE.
      G16 = .FALSE.
      gen=.FALSE.
      full=.false.
      alpha=.false.
      beta=.false.
      gread=.false.
      dcart=.false.
      fullpop=.false.
      gf=.false.
      next2=.false.
      next1=.false.
      newatm=.false.
      Lunrestricted = .FALSE.
      tracker=.false.
      chrgfnd=.false.
      nosym=.false.
      allocate(coordtext(500))
      localIatom=1
!     molecule=>GAUmolecule
!     BASIS=>current_basis
      write(UNIout,'(a)')
      write(UNIout,'(a)') '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
      write(UNIout,'(a)') ' Gaussian Output File Reader - Density Matrix, Molecular Orbital Coefficients  '
      write(UNIout,'(a)') '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
      write(UNIout,'(a)')
      lformula=' '
! READ FORMAT SECTION
! DETERMINATION OF GAUSSIAN VERSION
      read(READ_unit,'(a34,a3)', iostat=Iiostat)preversion,version
      if(Iiostat.ne.0)then
        write(UNIout,'(a)')'The Gaussian log file in empty'
        Lerror=.true.
      else
        if (version.eq.'g03')G03=.TRUE.
        if (version.eq.'g09')G09=.TRUE.
        if (version.eq.'g16')G16=.TRUE.
      end if
!
      call PRG_manager ('exit', 'INITIALIZE_GAUSSIAN_READ', 'UTILITY')

      end subroutine INITIALIZE_GAUSSIAN_READ 
      subroutine REALLOCATE_BASIS_SET 
!***********************************************************************
!     Date last modified: July 15, 2016                                *
!     Authors: P. L. Warburton                                         *
!     Desciption: Moves imported basis set info to smaller array       *
!***********************************************************************
! Modules:

      implicit none

      call PRG_manager ('enter', 'REALLOCATE_BASIS_SET', 'UTILITY')

      allocate(impend(localIatom-1))
      allocate(impbeg(localIatom-1))
      impend(:)=0
      impbeg(:)=0
      bascount=1
      impbeg(bascount)=1
      allocate(temp2import(impcount))
      do loop=1,impcount
       if(index(tempimport(loop),'****').ne.0) then
        impend(bascount)=loop
        impbeg(bascount+1)=loop+1
        if(bascount.lt.(localIatom-1)) bascount=bascount+1
       end if
       temp2import(loop)=tempimport(loop)
      end do
      deallocate(tempimport)    
      call PRG_manager ('exit', 'REALLOCATE_BASIS_SET', 'UTILITY')

      end subroutine REALLOCATE_BASIS_SET
      subroutine READ_TITLE_CARD
!***********************************************************************
!     Date last modified: July 15, 2016                                *
!     Authors: P. L. Warburton                                         *
!     Desciption: Reads part of Gaussian file title card section       *
!***********************************************************************
! Modules:

      implicit none

      logical :: realsoon
      character(len=64) :: GTitle

      call PRG_manager ('enter', 'READ_TITLE_CARD', 'UTILITY')

       counter=1
       revcount=2
       realsoon=.false.
       firstloop: do ! loop until find molecule coordinates
        read(READ_unit,'(a80)')text
        counter=counter+1
        if (index(text,'#').ne.0) then
         GTitle=trim(text)
         write(UNIout,'(a)') '///////////////////////////////////////////////////////////////////////////////'
         write(UNIout,'(a)') '                      Gaussian Output File Route Section                       '
         write(UNIout,'(a)')GTitle
         nosym=.true.
         if (index(text,'nosym').ne.0) nosym=.true.
         if (index(text,'NOSYM').ne.0) nosym=.true.
          if(nosym) then
           ortext='Input'
          else
           ortext='Stand'
          end if
         sread=index(GTitle,'/')
         temptext=text(index(text,'#')+1:sread-1)
         write(UNIout,'(a33,a)') 'Route card method appears to be: ', trim(temptext)
         temptext=text(sread+1:len(text))
         sread=index(temptext,' ')
         tempbasis=temptext(1:sread)
         write(UNIout,'(a36,a)') 'Route card basis set appears to be: ', trim(tempbasis)
         if (index(tempbasis,'gen').ne.0) gen=.true.
         if (index(tempbasis,'GEN').ne.0) gen=.true.
         if (gen) write(UNIout,'(a)') 'For general basis input the output must contain the gfinput keyword'
         if (index(text,'GFinput').ne.0) gf=.true.
         if (index(text,'gfinput').ne.0) gf=.true.
         if (index(text,'GFINPUT').ne.0) gf=.true.
         if (gen) then
          if (gf) then 
           write(UNIout,'(a)') 'It does contain gfinput!'
          else
           call PRG_stop ('gfinput required for general basis set read! You must rerun the Gaussian job!')
          end if ! gf
         end if ! gen
         write(UNIout,'(a)') 'The Gaussian job requires the pop=full and 6d keywords'
         if (index(text,'6d').ne.0) dcart=.true.
         if (index(text,'6D').ne.0) dcart=.true.
         if (index(text,'pop').ne.0.and.index(text,'full').ne.0) fullpop=.true.
         if (index(text,'POP').ne.0.and.index(text,'full').ne.0) fullpop=.true.
         if (index(text,'pop').ne.0.and.index(text,'FULL').ne.0) fullpop=.true.
         if (index(text,'POP').ne.0.and.index(text,'FULL').ne.0) fullpop=.true.
         if (fullpop.and.dcart) then 
          write(UNIout,'(a)') 'It does contain both required keywords!'
         else
           if(.not.dcart) call PRG_stop ('6d keyword required! You must rerun the Gaussian job!')
           if(.not.fullpop) call PRG_stop ('pop=full keyword required! You must rerun the Gaussian job!')
         end if ! fullpop/dcart           
         write(UNIout,'(a)') '\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\'
         write(UNIout,'(a)')    
        end if ! index(text,'#')
        if(soon) revcount=revcount-1
        if(index(text,'/99;').ne.0) soon=.true.
        if(revcount.eq.1) then
         if(index(text,'----').eq.0) then
          revcount=2
          soon=.false.
         end if
        end if
        if(revcount.eq.0) then
         GTitle=trim(text)
         write(UNIout,'(a)') '///////////////////////////////////////////////////////////////////////////////'
         write(UNIout,'(a)') '                         Gaussian Title Card Section                           '
         write(UNIout,'(a)')GTitle
        end if ! revcount
        if (index(text,'Charge').ne.0.and..not.chrgfnd) then
         read(text(10:12),'(i3)') gaucharge
         read(text(28:30),'(i3)') gaumult
         chrgfnd=.TRUE.
         Molecule%charge=gaucharge
         multiplicity=gaumult
         write(UNIout,'(a10,i3,a16,i3)') ' CHARGE = ',Molecule%charge,' MULTIPLICITY = ',multiplicity
         nelec=nelec-gaucharge
!         write(*,*) 'Initial number of electrons based on charge: ',nelec
        end if ! Charge
        if (index(text,ortext).ne.0) then
         do loop=1,4
          read(READ_unit,'(a80)')text
          counter=counter+1
         end do  
         secondloop: do !loop until end of molecule info
          read(READ_unit,'(a3,a77)')loopcheck,coordtext(localIatom) 
          counter=counter+1
          if(loopcheck.eq.' --')exit firstloop ! found the end of molecule info
          localIatom=localIatom+1                
         end do secondloop
        end if ! ortext
       end do firstloop
      call PRG_manager ('exit', 'READ_TITLE_CARD', 'UTILITY')

      end subroutine READ_TITLE_CARD
      subroutine READ_MO_COEFFICIENTS
!***********************************************************************
!     Date last modified: July 15, 2016                                *
!     Authors: P. L. Warburton                                         *
!     Desciption: Reads part of Gaussian file title card section       *
!***********************************************************************
! Modules:

      implicit none

      integer :: Ierror

      call PRG_manager ('enter', 'READ_MO_COEFFICIENTS', 'UTILITY')

!  READ MO Coefficient INFO
     if(.not.gaumolinfo) counter=1
     if(gaudensityinfo) then
      fourthloop: do ! loop until find MO coefficients
       read(READ_unit,'(a80)')text
       counter=counter+1
       if(index(text,'Population analysis using').ne.0) then
        write(UNIout,'(a)') '///////////////////////////////////////////////////////////////////////////////'
        write(UNIout,'(a)') text
        write(UNIout,'(a)') '\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\'
        write(UNIout,'(a)')  
       end if  
       if((index(text,'MOLECULAR ORBITAL COEFFICENTS').ne.0).or.(index(text,'Molecular Orbital Coefficients').ne.0)) then      
        matrixline = counter + 1
        if((index(text,'ALPHA').ne.0).or.(index(text,'Alpha').ne.0)) then 
         Lunrestricted=.TRUE.
         alpha=.true.
         beta=.true.
        else
         full=.true.
        end if
        write(*,*) 'MOFull ',full,'MOAlpha ',alpha,'MOBeta ',beta
       end if
       if(counter.eq.matrixline)exit fourthloop ! found MO coefficients
       if (index(text,'Normal termination').ne.0) call PRG_stop ('Got to end of Gaussian output without finding MO coefficients')
      end do fourthloop 
      write(*,*) text
      flush(6)
! allocate MO arrays.  Since we just want these for plotting purposes, regardless
! of the Gaussian job we will treat full MO coefficient matrices as an 'RHF guess' object 
! and alpha and beta arrays as 'UHF' objects
      if(full) then
       write(UNIout,'(a)') 'Using CMOG (RHF guess MO object) to store the MOs.  Keep in mind when plotting!'
       if(.not.associated(CMOG%coeff))then
        allocate (CMOG%coeff(Nbasis,Nbasis), CMOG%eigval(Nbasis), CMOG%occupancy(Nbasis))
        CMOG%occupancy(:)=0
       else
! Must always build guess (could be new molecule...
        deallocate (CMOG%coeff, CMOG%eigval, CMOG%occupancy)
        allocate (CMOG%coeff(Nbasis,Nbasis), CMOG%eigval(Nbasis), CMOG%occupancy(Nbasis))
        CMOG%occupancy(:)=ZERO
       end if ! .not.associated
      end if !full
      if(alpha) then
       write(UNIout,'(a)') 'Using CMOA and CMOB (UHF MO objects) to store the MOs.  Keep in mind when plotting!'
       if(.not.associated(CMOA%coeff))then
        allocate (CMOA%coeff(Nbasis,Nbasis), CMOA%eigval(1:Nbasis), CMOA%occupancy(1:Nbasis), &
                  CMOB%coeff(Nbasis,Nbasis), CMOB%eigval(1:Nbasis), CMOB%occupancy(1:Nbasis))
        CMOA%occupancy(:)=ZERO
        CMOB%occupancy(:)=ZERO
       else
! Must always build guess (could be new molecule...
        deallocate (CMOA%coeff, CMOA%eigval, CMOA%occupancy,CMOB%coeff, CMOB%eigval, CMOB%occupancy)
        allocate (CMOA%coeff(Nbasis,Nbasis), CMOA%eigval(1:Nbasis), CMOA%occupancy(1:Nbasis), &
                  CMOB%coeff(Nbasis,Nbasis), CMOB%eigval(1:Nbasis), CMOB%occupancy(1:Nbasis))
        CMOA%occupancy(:)=ZERO
        CMOB%occupancy(:)=ZERO
       end if ! .not.associated
      end if ! alpha
     end if ! gaudensityinfo

! reset logicals for later density matrix read
     Lunrestricted=.false.
     full=.false.
     alpha=.false.
     beta=.false.
     call PRG_manager ('exit', 'READ_MO_COEFFICIENTS', 'UTILITY')

     end subroutine READ_MO_COEFFICIENTS
      subroutine READ_DENSITY_MATRIX
!***********************************************************************
!     Date last modified: July 15, 2016                                *
!     Authors: P. L. Warburton                                         *
!     Desciption: Reads part of Gaussian file title card section       *
!***********************************************************************
! Modules:

      implicit none

      call PRG_manager ('enter', 'READ_DENSITY_MATRIX', 'UTILITY')

!  READ DENSITY MATRIX INFO
     if(.not.gaumolinfo) counter=1
     if(gaudensityinfo) then
      fifthloop: do ! loop until find a density matrix
       read(READ_unit,'(a80)')text
       counter=counter+1
       if((index(text,'DENSITY MATRIX').ne.0).or.(index(text,'Density Matrix').ne.0)) then      
        matrixline = counter + 1
        if((index(text,'ALPHA').ne.0).or.(index(text,'Alpha').ne.0)) then 
         Lunrestricted=.TRUE.
         alpha=.true.
         beta=.true.
        else
         full=.true.
        end if
        write(*,*) 'DMFull ',full,'DMAlpha ',alpha,'DMBeta ',beta
       end if
       if(counter.eq.matrixline)exit fifthloop ! found a density matrix
       if (index(text,'Normal termination').ne.0) call PRG_stop ('Got to end of Gaussian output without finding density matrix')
      end do fifthloop


      write(*,*) text

      allocate (rhomat(Nbasis,Nbasis))
      allocate (mglabel(Nbasis))
      allocate (rhomatb(Nbasis,Nbasis))
      rhomat(:,:)=0.0d0
      rhomatb(:,:)=0.0d0
      mglabel(:)=0
      call GET_GAUSSIAN_MATRIX (rhomat, Nbasis, mglabel, tracker, counter)
      if(Lunrestricted) then
       tracker=.false.
       read(READ_unit,'(a80)')text
       counter=counter+1
       if(index(text,'BETA DENSITY MATRIX.').ne.0)then
        matrixline = counter + 1
       else if (index(text,'Beta Density Matrix:').ne.0) then
        matrixline = counter + 1
       end if
       call GET_GAUSSIAN_MATRIX (rhomatb, Nbasis, mglabel,tracker,counter)
       rhomat=rhomat+rhomatb
      end if ! Lunrestricted
! Density matrix consolodation and fill upper diagonal
      do row = 1, Nbasis
       do col = 1, row
        rhomat(col,row) = rhomat(row,col)
       end do
      end do
! remap density matix from Gaussian ordering to MUNgauss ordering
! will reuse rhomatb as the mapped matrix so it should be zeroed again
      rhomatb(:,:)=0.0d0
      do row=1,Nbasis
       do col=1,Nbasis
        rhomatb(mglabel(row),mglabel(col))=rhomat(row,col)
       end do
      end do
      call SQS_to_UPT (rhomatb, Nbasis, PM0Gau, MATlenIN)
     end if ! gaudensityinfo
     call PRG_manager ('exit', 'READ_DENSITY_MATRIX', 'UTILITY')
     end subroutine READ_DENSITY_MATRIX
     subroutine GAUSSIAN_BASIS_SET
!***********************************************************************
!     Date last modified: July 15, 2016                                *
!     Authors: P. L. Warburton                                         *
!     Desciption: Reads Gaussian file basis set from gfinput print     *
!***********************************************************************
! Modules:

      implicit none

      call PRG_manager ('enter', 'GAUSSIAN_BASIS_SET', 'UTILITY')

       thirdloop: do 
        read(READ_unit,'(a80)')text
        counter=counter+1
        if(index(text,'Standard basis:').ne.0) then 
         text=trim(text(18:80))
         scanpos=scan(text," ",.false.)
!        BASIS%name=text(1:scanpos-1)
         Basis_set_name=text(1:scanpos-1)
         write(UNIout,'(a14,a)') 'Basis set is: ', trim(Basis_set_name)
         exit thirdloop
        end if ! Standard basis
! general basis set initialization
        if(index(text,'General basis').ne.0) then
         write(UNIout,'(a)') 'Confirmed general basis set used!'
         write(UNIout,'(a)') 'The full basis set information read from the output file is:'
!        BASIS%name='IMPORT'
         Basis_set_name='IMPORT'
         allocate(tempimport(10000))
         tempimport(:)=' '
         allocate(firstatom(92))
         firstatom(:)=0
         impcount=0
         genloop: do 
          read(READ_unit,'(a80)')text
          counter=counter+1
          if(index(text,'AO basis set in the form of general').ne.0) then
! general basis set read from file
           genloop2: do
            read(READ_unit,'(a80)')text
            impcount=impcount+1
            counter=counter+1
            tempimport(impcount)=trim(text)
            if(len_trim(text).eq.0) exit thirdloop
            write(UNIout,'(a75,i4)') tempimport(impcount),impcount
           end do genloop2
          end if ! AO basis set
         end do genloop
        end if ! general basis
       end do thirdloop

      call PRG_manager ('exit', 'GAUSSIAN_BASIS_SET', 'UTILITY')

      end subroutine GAUSSIAN_BASIS_SET
      subroutine REDUCE_BASIS_SET
!***********************************************************************
!     Date last modified: July 15, 2016                                *
!     Authors: P. L. Warburton                                         *
!     Desciption: Reduces the basis set info to the required elements  *
!***********************************************************************
! Modules:

      implicit none

      call PRG_manager ('enter', 'REDUCE_BASIS_SET', 'UTILITY')

      write(UNIout,'(a)') '///////////////////////////////////////////////////////////////////////////////'
      write(UNIout,'(a)') '          Reduced imported basis set information required by MUNgauss          ' 
      do loop=1,92
       if(firstatom(loop).eq.0) cycle
       totline=totline+impend(firstatom(loop))-impbeg(firstatom(loop))+1
      end do !loop
      allocate(basisimport(totline))
      lcount=0
      do loop=1,92
       if(firstatom(loop).eq.0) cycle
       do loop2=impbeg(firstatom(loop)),impend(firstatom(loop))
        lcount=lcount+1
        if(loop2.eq.impbeg(firstatom(loop))) then
         write(basisimport(lcount),'(1x,i2,1x,a)') loop,element_names(loop)
        else
         basisimport(lcount)=temp2import(loop2)
        end if         
        write(UNIout,'(a80)') basisimport(lcount)
       end do !loop2
      end do !loop
      write(UNIout,'(a)') '\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\'
      write(UNIout,'(a)') 
      call PRG_manager ('exit', 'REDUCE_BASIS_SET', 'UTILITY')
      end subroutine REDUCE_BASIS_SET
      subroutine GAUSSIAN_MOLECULE_INFO
!***********************************************************************
!     Date last modified: July 15, 2016                                *
!     Authors: P. L. Warburton                                         *
!     Desciption: Reads Gaussian file basis set from gfinput print     *
!***********************************************************************
! Modules:

      implicit none
!
      call PRG_manager ('enter', 'GAUSSIAN_MOLECULE_INFO', 'UTILITY')
!
      Natoms=localIatom-1
      allocate(CARTESIANGAU(Natoms))
      do loop=1,Natoms
       read(coordtext(loop),'(i5,i11,i12,4x,3(f12.6))') localIatom,CartesianGAU(loop)%Atomic_number, &
            garbage, CartesianGAU(loop)%X, CartesianGAU(loop)%Y, CartesianGAU(loop)%Z
       nelec=nelec+CartesianGAU(loop)%Atomic_number
!       write(*,*) 'atom number ',loop,' number electroms ',nelec
! if general basis set, want to keep track of the first instance of each element occurring
       if (gen) then
        if (firstatom(CARTESIANGAU(loop)%Atomic_number).eq.0) then
         firstatom(CARTESIANGAU(loop)%Atomic_number)=loop
        end if
       end if
! Gaussian default units are Angstroms.  MUNgauss defaults to a.u.,so we'll convert
       CARTESIANGAU(loop)%X=CartesianGAU(loop)%X/Bohr_to_Angstrom
       CARTESIANGAU(loop)%Y=CartesianGAU(loop)%Y/Bohr_to_Angstrom
       CARTESIANGAU(loop)%Z=CartesianGAU(loop)%Z/Bohr_to_Angstrom
       CARTESIANGAU(loop)%ELEMENT=element_names(CARTESIANGAU(loop)%Atomic_number)
      end do ! loop over atoms

      call CHEMICAL_FORMULA(lformula)
      MOLECULE%FORMULA=lformula
      write(UNIout,'(a26,a)') 'The molecular formula is: ',MOLECULE%FORMULA
      deallocate(coordtext)

      if(ortext.eq.'Stand') then
       write(UNIout,'(a)') 'Using the Gaussian STANDARD orientation'
      else
       write(UNIout,'(a)') 'Using the Gaussian INPUT orientation'
      end if
      write(UNIout,'(a)') '\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\'
      write(UNIout,'(a)') 
      call PRG_manager ('exit', 'GAUSSIAN_MOLECULE_INFO', 'UTILITY')
      end subroutine GAUSSIAN_MOLECULE_INFO
! END CONTAINS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine GaussianReader
