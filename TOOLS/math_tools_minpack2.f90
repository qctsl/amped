      subroutine solve_TR_subproblem(n,A,g,delta,tol,lambda,f,x)
!***********************************************************************
!     Date last modified: September 14, 2020                           *
!     Author: JWH                                                      *
!     Description: Wrapper for minipack-2 trust-region subproblem      *
!                  solver.                                             *
!***********************************************************************

      implicit none

! Input variables
!
      integer, intent(in)            :: n
      double precision, intent(in)   :: A(n,n), g(n), delta, tol, lambda
!
! Local variables
!
      double precision, allocatable, dimension(:)  :: work1, work2, work3
      double precision              :: atol
      integer                       :: info, iter, itmax
!
! Output variables
!
      double precision, intent(out)  :: f, x(n)
!
! Begin:
!
      allocate(work1(n),work2(n),work3(n))
      atol = 0.0D0
      itmax = 100
!
      call dgqt(n,A,n,g,delta,tol,atol,itmax,lambda,f,x,info,iter,work1,work2,work3)
!
      if(info == 4)then
        print*,'Failure to converge to subproblem solution (dgqt)!!'
      end if
!
      end subroutine solve_TR_subproblem
      subroutine dgqt(n,a,lda,b,delta,rtol,atol,itmax,par,f,x,info,iter,z,wa1,wa2)
!***********************************************************************
!     Date last modified: September 14, 2020                           *
!     Author: JWH (converted from FORTRAN 77)                          *
!     Description: See description below, taken from:                  *
!                  https://ftp.mcs.anl.gov/pub/MINPACK-2/              *
!***********************************************************************

      integer n, lda, itmax, info
      double precision delta, rtol, atol, par, f
      double precision a(lda,n), b(n), x(n), z(n), wa1(n), wa2(n)
!     ***********
!
!     Subroutine dgqt
!
!     Given an n by n symmetric matrix A, an n-vector b, and a
!     positive number delta, this subroutine determines a vector
!     x which approximately minimizes the quadratic function
!
!           f(x) = (1/2)*x'*A*x + b'*x
!
!     subject to the Euclidean norm constraint
!
!           norm(x) <= delta.
!
!     This subroutine computes an approximation x and a Lagrange
!     multiplier par such that either par is zero and
!
!            norm(x) <= (1+rtol)*delta,
!
!     or par is positive and
!
!            abs(norm(x) - delta) <= rtol*delta.
!
!     If xsol is the solution to the problem, the approximation x
!     satisfies
!
!            f(x) <= ((1 - rtol)**2)*f(xsol)
!
!     The subroutine statement is
!
!       subroutine dgqt(n,a,lda,b,delta,rtol,atol,itmax,
!                        par,f,x,info,z,wa1,wa2)
!
!     where
!
!       n is an integer variable.
!         On entry n is the order of A.
!         On exit n is unchanged.
!
!       a is a double precision array of dimension (lda,n).
!         On entry the full upper triangle of a must contain the
!            full upper triangle of the symmetric matrix A.
!         On exit the array contains the matrix A.
!
!       lda is an integer variable.
!         On entry lda is the leading dimension of the array a.
!         On exit lda is unchanged.
!
!       b is an double precision array of dimension n.
!         On entry b specifies the linear term in the quadratic.
!         On exit b is unchanged.
!
!       delta is a double precision variable.
!         On entry delta is a bound on the Euclidean norm of x.
!         On exit delta is unchanged.
!
!       rtol is a double precision variable.
!         On entry rtol is the relative accuracy desired in the
!            solution. Convergence occurs if
!
!              f(x) <= ((1 - rtol)**2)*f(xsol)
!
!         On exit rtol is unchanged.
!
!       atol is a double precision variable.
!         On entry atol is the absolute accuracy desired in the
!            solution. Convergence occurs when
!
!              norm(x) <= (1 + rtol)*delta
!
!              max(-f(x),-f(xsol)) <= atol
!
!         On exit atol is unchanged.
!
!       itmax is an integer variable.
!         On entry itmax specifies the maximum number of iterations.
!         On exit itmax is unchanged.
!
!       par is a double precision variable.
!         On entry par is an initial estimate of the Lagrange
!            multiplier for the constraint norm(x) <= delta.
!         On exit par contains the final estimate of the multiplier.
!
!       f is a double precision variable.
!         On entry f need not be specified.
!         On exit f is set to f(x) at the output x.
!
!       x is a double precision array of dimension n.
!         On entry x need not be specified.
!         On exit x is set to the final estimate of the solution.
!
!       info is an integer variable.
!         On entry info need not be specified.
!         On exit info is set as follows:
!
!            info = 1  The function value f(x) has the relative
!                      accuracy specified by rtol.
!
!            info = 2  The function value f(x) has the absolute
!                      accuracy specified by atol.
!
!            info = 3  Rounding errors prevent further progress.
!                      On exit x is the best available approximation.
!
!            info = 4  Failure to converge after itmax iterations.
!                      On exit x is the best available approximation.
!
!       z is a double precision work array of dimension n.
!
!       wa1 is a double precision work array of dimension n.
!
!       wa2 is a double precision work array of dimension n.
!
!     Subprograms called
!
!       MINPACK-2  ......  destsv
!
!       LAPACK  .........  dpotrf
!
!       Level 1 BLAS  ...  dasum, daxpy, dcopy, ddot, dnrm2, dscal
!
!       Level 2 BLAS  ...  dtrmv, dtrsv
!
!     MINPACK-2 Project. July 1994.
!     Argonne National Laboratory and University of Minnesota.
!     Brett M. Averick, Richard Carter, and Jorge J. More'
!
!     ***********
      double precision one, p001, p5, zero
      parameter (zero=0.0d0,p001=1.0d-3,p5=0.5d0,one=1.0d0)

      logical rednc
      integer indef, iter, j
      double precision alpha, anorm, bnorm, parc, parf, parl, pars
      double precision paru, prod, rxnorm, rznorm, temp, xnorm
      double precision dasum, ddot, dnrm2
      external daxpy, dcopy, ddot, destsv, dnrm2, dpotrf, dscal, dtrsv

!     Initialization.

      parf = zero
      xnorm = zero
      rxnorm = zero
      rednc = .false.
      do j = 1, n
         x(j) = zero
         z(j) = zero
      end do 

!     Copy the diagonal and save A in its lower triangle.

      call dcopy(n,a,lda+1,wa1,1)
      do j = 1, n - 1
         call dcopy(n-j,a(j,j+1),lda,a(j+1,j),1)
      end do  

!     Calculate the l1-norm of A, the Gershgorin row sums,
!     and the l2-norm of b.

      anorm = zero
      do j = 1, n
         wa2(j) = dasum(n,a(1,j),1)
         anorm = max(anorm,wa2(j))
      end do  
      do j = 1, n
         wa2(j) = wa2(j) - abs(wa1(j))
      end do 
      bnorm = dnrm2(n,b,1)

!     Calculate a lower bound, pars, for the domain of the problem.
!     Also calculate an upper bound, paru, and a lower bound, parl,
!     for the Lagrange multiplier.

      pars = -anorm
      parl = -anorm
      paru = -anorm
      do j = 1, n
         pars = max(pars,-wa1(j))
         parl = max(parl,wa1(j)+wa2(j))
         paru = max(paru,-wa1(j)+wa2(j))
      end do
      parl = max(zero,bnorm/delta-parl,pars)
      paru = max(zero,bnorm/delta+paru)

!     If the input par lies outside of the interval (parl,paru),
!     set par to the closer endpoint.

      par = max(par,parl)
      par = min(par,paru)

!     Special case: parl = paru.

      paru = max(paru,(one+rtol)*parl)

!     Beginning of an iteration.

      info = 0
      do iter = 1, itmax

!       Safeguard par.

        if (par .le. pars .and. paru .gt. zero) par = max(p001,sqrt(parl/paru))*paru
!       write (2,1000) parl,paru,pars,par
!1000   format(4d12.3)

!       Copy the lower triangle of A into its upper triangle and
!       compute A + par*I.

        do j = 1, n - 1
           call dcopy(n-j,a(j+1,j),1,a(j,j+1),lda)
        end do 
        do j = 1, n
          a(j,j) = wa1(j) + par
        end do 
!
!       Attempt the  Cholesky factorization of A without referencing
!       the lower triangular part.

        call dpotrf('U',n,a,lda,indef)

!       Case 1: A + par*I is positive definite.

        if (indef .eq. 0) then

!         Compute an approximate solution x and save the
!         last value of par with A + par*I positive definite.

          parf = par
          call dcopy(n,b,1,wa2,1)
          call dtrsv('U','T','N',n,a,lda,wa2,1)
          rxnorm = dnrm2(n,wa2,1)
          call dtrsv('U','N','N',n,a,lda,wa2,1)
          call dcopy(n,wa2,1,x,1)
          call dscal(n,-one,x,1)
          xnorm = dnrm2(n,x,1)

!         Test for convergence.

          if (abs(xnorm-delta) .le. rtol*delta .or.&
             (par .eq. zero .and. xnorm .le. (one+rtol)*delta))then
            info = 1
          end if
!
!         Compute a direction of negative curvature and use this
!         information to improve pars.

          call destsv(n,a,lda,rznorm,z)
          pars = max(pars,par-rznorm**2)

!         Compute a negative curvature solution of the form
!         x + alpha*z where norm(x+alpha*z) = delta.

          rednc = .false.
          if (xnorm .lt. delta) then

!         Compute alpha

            prod = ddot(n,z,1,x,1)/delta
            temp = (delta-xnorm)*((delta+xnorm)/delta)
            alpha = temp/(abs(prod)+sqrt(prod**2+temp/delta))
            alpha = sign(alpha,prod)

!         Test to decide if the negative curvature step
!         produces a larger reduction than with z = 0.
!
            rznorm = abs(alpha)*rznorm
            if ((rznorm/delta)**2+par*(xnorm/delta)**2 .le. par) rednc = .true.
!
!           Test for convergence.

            if (p5*(rznorm/delta)**2 .le. rtol*(one-p5*rtol)*(par+(rxnorm/delta)**2)) then
              info = 1
            else if (p5*(par+(rxnorm/delta)**2) .le. (atol/delta)/delta .and. info .eq. 0) then
              info = 2
            else if (xnorm .eq. zero) then
              info = 1
            end if
          end if

!         Compute the Newton correction parc to par.

          if (xnorm .eq. zero) then
            parc = -par
          else
            call dcopy(n,x,1,wa2,1)
            temp = one/xnorm
            call dscal(n,temp,wa2,1)
            call dtrsv('U','T','N',n,a,lda,wa2,1)
            temp = dnrm2(n,wa2,1)
            parc = (((xnorm-delta)/delta)/temp)/temp
          end if

!           Update parl or paru.

          if (xnorm .gt. delta) parl = max(parl,par)
          if (xnorm .lt. delta) paru = min(paru,par)
!
        else
!
!         Case 2: A + par*I is not positive definite.

!         Use the rank information from the Cholesky
!         decomposition to update par.

          if (indef .gt. 1) then

!           Restore column indef to A + par*I.

            call dcopy(indef-1,a(indef,1),lda,a(1,indef),1)
            a(indef,indef) = wa1(indef) + par

!           Compute parc.

            call dcopy(indef-1,a(1,indef),1,wa2,1)
            call dtrsv('U','T','N',indef-1,a,lda,wa2,1)
            a(indef,indef) = a(indef,indef) - dnrm2(indef-1,wa2,1)**2
            call dtrsv('U','N','N',indef-1,a,lda,wa2,1)
          end if
!
          wa2(indef) = -one
          temp = dnrm2(indef,wa2,1)
          parc = -(a(indef,indef)/temp)/temp
          pars = max(pars,par,par+parc)

!         If necessary, increase paru slightly.
!         This is needed because in some exceptional situations
!         paru is the optimal value of par.

          paru = max(paru,(one+rtol)*pars)
!
        end if

!       Use pars to update parl.

        parl = max(parl,pars)

!       Test for termination.

        if (info .eq. 0) then
          if (iter .eq. itmax) info = 4
          if (paru .le. (one+p5*rtol)*pars) info = 3
          if (paru .eq. zero) info = 2
        end if

!       If exiting, store the best approximation and restore
!       the upper triangle of A.

        if (info .ne. 0) then

!       Compute the best current estimates for x and f.

          par = parf
          f = -p5*(rxnorm**2+par*xnorm**2)
          if (rednc) then
            f = -p5*((rxnorm**2+par*delta**2)-rznorm**2)
            call daxpy(n,alpha,z,1,x,1)
          end if

!         Restore the upper triangle of A.

          do j = 1, n - 1
            call dcopy(n-j,a(j+1,j),1,a(j,j+1),lda)
          end do
          call dcopy(n,wa1,1,a,lda+1)

          return
!
        end if

!       Compute an improved estimate for par.

        par = max(parl,par+parc)

!       End of an iteration.

      end do 

      end subroutine dgqt
      subroutine destsv(n,r,ldr,svmin,z)
!***********************************************************************
!     Date last modified: September 14, 2020                           *
!     Author: JWH (converted from FORTRAN 77)                          *
!     Description: See description below, taken from:                  *
!                  https://ftp.mcs.anl.gov/pub/MINPACK-2/              *
!***********************************************************************
      integer ldr, n
      double precision svmin
      double precision r(ldr,n), z(n)
!     **********
!
!     Subroutine destsv
!
!     Given an n by n upper triangular matrix R, this subroutine
!     estimates the smallest singular value and the associated
!     singular vector of R.
!
!     In the algorithm a vector e is selected so that the solution
!     y to the system R'*y = e is large. The choice of sign for the
!     components of e cause maximal local growth in the components
!     of y as the forward substitution proceeds. The vector z is
!     the solution of the system R*z = y, and the estimate svmin
!     is norm(y)/norm(z) in the Euclidean norm.
!
!     The subroutine statement is
!
!       subroutine estsv(n,r,ldr,svmin,z)
!
!     where
!
!       n is an integer variable.
!         On entry n is the order of R.
!         On exit n is unchanged.
!
!       r is a double precision array of dimension (ldr,n)
!         On entry the full upper triangle must contain the full
!            upper triangle of the matrix R.
!         On exit r is unchanged.
!
!       ldr is an integer variable.
!         On entry ldr is the leading dimension of r.
!         On exit ldr is unchanged.
!
!       svmin is a double precision variable.
!         On entry svmin need not be specified.
!         On exit svmin contains an estimate for the smallest
!            singular value of R.
!
!       z is a double precision array of dimension n.
!         On entry z need not be specified.
!         On exit z contains a singular vector associated with the
!            estimate svmin such that norm(R*z) = svmin and
!            norm(z) = 1 in the Euclidean norm.
!
!     Subprograms called
!
!       Level 1 BLAS ... dasum, daxpy, dnrm2, dscal
!
!     MINPACK-2 Project. October 1993.
!     Argonne National Laboratory
!     Brett M. Averick and Jorge J. More'.
!
!     **********
      double precision one, p01, zero
      parameter (zero=0.d0,p01=1.0d-2,one=1.0d0)

      integer i, j
      double precision e, s, sm, temp, w, wm, ynorm, znorm

      double precision dasum, dnrm2
      external dasum, daxpy, dnrm2, dscal

      do i = 1, n
         z(i) = zero
      end do

!     This choice of e makes the algorithm scale invariant.

      e = abs(r(1,1))
      if (e .eq. zero) then
         svmin = zero
         z(1) = one

         return

      end if

!     Solve R'*y = e.

      do i = 1, n
        e = sign(e,-z(i))

!        Scale y. The factor of 0.01 reduces the number of scalings.

        if (abs(e-z(i)) .gt. abs(r(i,i))) then
          temp = min(p01,abs(r(i,i))/abs(e-z(i)))
          call dscal(n,temp,z,1)
          e = temp*e
        end if

!        Determine the two possible choices of y(i).

        if (r(i,i) .eq. zero) then
          w = one
          wm = one
        else
          w = (e-z(i))/r(i,i)
          wm = -(e+z(i))/r(i,i)
        end if

!        Choose y(i) based on the predicted value of y(j) for j > i.

        s = abs(e-z(i))
        sm = abs(e+z(i))
        do j = i + 1, n
          sm = sm + abs(z(j)+wm*r(i,j))
        end do  
        if (i .lt. n) then
          call daxpy(n-i,w,r(i,i+1),ldr,z(i+1),1)
          s = s + dasum(n-i,z(i+1),1)
        end if
        if (s .lt. sm) then
          temp = wm - w
          w = wm
          if (i .lt. n) call daxpy(n-i,temp,r(i,i+1),ldr,z(i+1),1)
        end if
        z(i) = w
      end do 
      ynorm = dnrm2(n,z,1)

!     Solve R*z = y.

      do j = n, 1, -1

!       Scale z.

        if (abs(z(j)) .gt. abs(r(j,j))) then
          temp = min(p01,abs(r(j,j))/abs(z(j)))
          call dscal(n,temp,z,1)
          ynorm = temp*ynorm
        end if
        if (r(j,j) .eq. zero) then
          z(j) = one
        else
          z(j) = z(j)/r(j,j)
        end if
        temp = -z(j)
        call daxpy(j-1,temp,r(1,j),1,z,1)
      end do  

!     Compute svmin and normalize z.

      znorm = one/dnrm2(n,z,1)
      svmin = ynorm*znorm
      call dscal(n,znorm,z,1)

      end subroutine destsv
