/* read a line from input. return 0 if <carriage return> */
#include <stdio.h>
int carret_(char line[], unsigned int* lenline)
{
   /*   printf("Cmd, H or <CR>\n");   */
        gets(line);
        *lenline = strlen(line);
        switch(*line)
        {       case 0x00 :     return 1;    break;
                case 0x20 :     return 0;    break;
                default :       return 0;
        }
}
