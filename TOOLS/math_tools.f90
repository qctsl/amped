      subroutine VEC_product (V, &
                              A, &
                              B, &
                              T, &
                              IOP)
!***********************************************************************
!     Date last modified: June 26, 1992                                *
!     Author:                                                          *
!     Description:  MUN Version.                                       *
!     If IOP = 0, evaluate V = A x B only.                             *
!     If IOP = 1, return !V! in T and normalize V.                     *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer :: IOP
      double precision :: T
!
! Input arrays:
      double precision A(3),B(3),V(3)
!
! Local scalar:
      double precision TENM5
!
      parameter (TENM5=1.0D-5)
!
! Begin:
      V(1)=A(2)*B(3)-B(2)*A(3)
      V(2)=A(3)*B(1)-B(3)*A(1)
      V(3)=A(1)*B(2)-B(1)*A(2)
      if(IOP.NE.0)then
         T=DSQRT(V(1)*V(1)+V(2)*V(2)+V(3)*V(3))
         if(T.GT.TENM5)then
            V(1)=V(1)/T
            V(2)=V(2)/T
            V(3)=V(3)/T
         end if
      end if
!
! End of routine VEC_product
      return
      end subroutine VEC_product
      double precision function GET_angle (ATOM1, ATOM2, ATOM3)
!****************************************************************************************************************************
!     Date last modified: August 26, 1999                                                                      Version 1.0  *
!     Author: R. A. Poirier                                                                                                 *
!     Description: Find the angle ATOM1-ATOM2-ATOM3                                                                         *
!****************************************************************************************************************************
! Modules:
      USE program_constants
      USE type_molecule

      implicit none
!
! Input scalars:
      integer ATOM1,ATOM2,ATOM3
!
! Local scalars:
      double precision AI,AJ,AK,TERM
!
! Local parameters:
      double precision TENM5
      parameter (TENM5=1.0D-50)
!
! Begin:
!      call PRG_manager ('enter', 'GET_angle', 'UTILITY')
!
      AI=DSQRT((CARTESIAN(ATOM2)%X-CARTESIAN(ATOM3)%X)**2+(CARTESIAN(ATOM2)%Y-CARTESIAN(ATOM3)%Y)**2+ &
       (CARTESIAN(ATOM2)%Z-CARTESIAN(ATOM3)%Z)**2)
      AJ=DSQRT((CARTESIAN(ATOM1)%X-CARTESIAN(ATOM3)%X)**2+(CARTESIAN(ATOM1)%Y-CARTESIAN(ATOM3)%Y)**2+ &
       (CARTESIAN(ATOM1)%Z-CARTESIAN(ATOM3)%Z)**2)
      AK=DSQRT((CARTESIAN(ATOM1)%X-CARTESIAN(ATOM2)%X)**2+(CARTESIAN(ATOM1)%Y-CARTESIAN(ATOM2)%Y)**2+ &
       (CARTESIAN(ATOM1)%Z-CARTESIAN(ATOM2)%Z)**2)
!
      TERM=(AI*AI+AK*AK-AJ*AJ)/(TWO*AI*AK)
      if(DABS(TERM).GE.ONE)TERM=DSIGN(ONE,TERM)
      if(DABS(TERM).LT.TENM5)then
        GET_angle=PI_val/TWO    ! 90.0
      else
        GET_angle=DATAN(DSQRT(ONE-TERM*TERM)/TERM)
         if(TERM.LT.ZERO)GET_angle=GET_angle+PI_val
      end if
!
! End of routine GET_angle
!      call PRG_manager ('exit', 'GET_angle', 'UTILITY')
      return
      end
      double precision function GET_torsion (ATOM1, ATOM2, ATOM3, ATOM4)
!*****************************************************************************************************************
!     Date last modified: August 26, 1999                                                           Version 1.0  *
!     Author: R. A. Poirier                                                                                      *
!     Description: Find the torsion ATOM1-ATOM2-ATOM3-ATOM4                                                      *
!*****************************************************************************************************************
! Modules:
      USE program_constants
      USE type_molecule

      implicit none
!
! Input scalars:
      integer ATOM1,ATOM2,ATOM3,ATOM4
!
! Local scalars:
      double precision CUV,CZX,R1,R2,R3,S,SCALAR,SI3
      double precision T
!
! Local arrays:
      double precision V1(3),V2(3),U(3),V(3),W(3),X(3),Z(3)
!
! Local parameters:
      double precision HPI,TENM11
      parameter (TENM11=1.0D-11,HPI=PI_VAL/TWO)
!
! Begin:
      call PRG_manager ('enter', 'GET_torsion', 'UTILITY')
!
! Torsion Angles.
         CALL VEC (U, R1, ATOM1, ATOM2)
         CALL VEC (V, R2, ATOM3, ATOM2)
         CALL VEC (W, R3, ATOM3, ATOM4)
         CALL VEC_product (Z, U, V, T, 1)
         CALL VEC_product (X, W, V, T, 1)
         CZX=-DOT_product(Z,X)                      ! |Z| |X| COS(theta)
         CALL VEC_product (U, Z, X, T, 0)
         CUV=DOT_product(U,V)
         S=HPI
         if(DABS(CZX).GE.TENM11)then
           SI3=DSQRT(U(1)*U(1)+U(2)*U(2)+U(3)*U(3)) ! |Z| |X| SIN(theta)
           S=DATAN(SI3/CZX)                         ! SI3/CZX = SIN(theta)/COS(theta)
           if(CZX.LT.ZERO)S=S+PI_VAL
         end if
         if(CUV.LT.ZERO)S=-S
!
         GET_torsion=-S
!
! End of routine GET_torsion
      call PRG_manager ('exit', 'GET_torsion', 'UTILITY')
      return
      end
      subroutine NVSQRT (AMAT, &
                         AMHALF, &
                         AEIGVC, &
                         AEIGVL, &
                         Ibasis)
!***********************************************************************
!     Date last modified: June 26, 1992                                *
!     Author: R.A. Poirier                                             *
!     Description:  U. of T. Version                                   *
!     This is a routine which calculates the inverse square root       *
!     of the matrix AMAT.  It returns with AMAT**(-1/2) in AMHALF,     *
!     the eigenvectors of AMAT in AEIGVC, and the eigenvalues of AMAT  *
!     in AEIGVL.                                                       *
!     SCRMAT,SCRVEC are used for scratch.                              *
!     Ibasis is the dimension of AMAT.                                 *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants

      implicit none
!
! Input scalars:
      integer Ibasis
!
! Input arrays:
      double precision AMAT(Ibasis,IBASIS)
!
! Output scalars:
!
! Output arrays:
      double precision AMHALF(Ibasis,IBASIS),AEIGVC(IBASIS,IBASIS),AEIGVL(IBASIS)
!
      double precision, dimension(:,:), allocatable :: SCRMAT
      double precision, dimension(:), allocatable :: SCRVEC
!
! Local scalars:
      integer I,J
!
! Begin:
      call PRG_manager ('enter', 'NVSQRT', 'UTILITY')
!
      allocate (SCRMAT(IBASIS,IBASIS),SCRVEC(IBASIS))
!
      call MATRIX_diagonalize (AMAT, AEIGVC, AEIGVL, Ibasis, 2, .true.)
      do I=1,Ibasis
        if(AEIGVL(I).le.ZERO)then
          write(UNIout,'(A,I4,A)')'ERROR> NVSQRT: EIGENVALUE ',I,' IS ZERO OR NEGATIVE'
          stop 'ERROR> PROJECT_MO: NVSQRT'
        end if
        SCRVEC(I)=ONE/DSQRT(AEIGVL(I))
      end do ! I

      do I=1,Ibasis
      do J=1,Ibasis
        SCRMAT(J,I)=AEIGVC(J,I)*SCRVEC(I)
      end do ! J
      end do ! I
      AMHALF=matmul(SCRMAT, transpose(AEIGVC))

      deallocate (SCRMAT, SCRVEC)
!
      call PRG_manager ('exit', 'NVSQRT', 'UTILITY')
      return
!
! End of routine NVSQRT
      call PRG_manager ('exit', 'NVSQRT', 'UTILITY')
      return
      end
      subroutine BLD_SMhalf (Smat, & ! Overlap matrix
                             SMhalf, & ! S-1/2 (result)
                             Nbasis)
!***********************************************************************
!     Date last modified: June 16, 2005                                *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!     Calculates the inverse square root of the overlap matrix, Smat.  *
!     It returns with S*(-1/2) in SMhalf.                              *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants

      implicit none
!
! Input scalars:
      integer :: Nbasis
!
! Input arrays:
      double precision Smat(Nbasis*(Nbasis+1)/2)
!
! Output scalars:
      integer :: MATlen
!
! Output arrays:
      double precision SMhalf(Nbasis,Nbasis)
!
! Local work arrays:
      double precision, dimension(:), allocatable :: SCRVEC
      double precision, dimension(:), allocatable :: Eigval
      double precision, dimension(:,:), allocatable :: Eigvec
      double precision, dimension(:,:), allocatable :: SQSM
      double precision, dimension(:,:), allocatable :: SCRMAT
!
! Local scalars:
      integer Ibasis,Jbasis
      logical :: Lerror
!
! Begin:
      call PRG_manager ('enter', 'BLD_SMHALF', 'UTILITY')
!
! Allocate work arrays:
      allocate (Eigvec(Nbasis,Nbasis), Eigval(Nbasis), SCRMAT(Nbasis,Nbasis), SCRVEC(Nbasis), SQSM(Nbasis,Nbasis))
      Lerror=.FALSE.
      MATlen=Nbasis*(Nbasis+1)/2
      call UPT_to_SQS (Smat, MATlen, SQSM, Nbasis)
      call MATRIX_diagonalize (SQSM, Eigvec, Eigval, Nbasis, 2, .true.)
      do Ibasis=1,Nbasis
        if(Eigval(Ibasis).LE.ZERO)then
          Lerror=.TRUE.
          exit
        end if
        SCRVEC(Ibasis)=ONE/DSQRT(Eigval(Ibasis))
      end do ! Ibasis

      if(.not.Lerror)then
      do Ibasis=1,Nbasis
      do Jbasis=1,Nbasis
        SCRMAT(Jbasis,Ibasis)=Eigvec(Jbasis,Ibasis)*SCRVEC(Ibasis)
      end do ! Jbasis
      end do ! Ibasis
      SMhalf=matmul(SCRMAT, transpose(Eigvec))
      else
        write(UNIout,'(A,I4,A)')'ERROR> BLD_SMhalf: Eigenvalue ',Ibasis,' is zero or negative'
      end if

      deallocate (Eigvec, Eigval, SCRMAT, SCRVEC, SQSM)
!
! End of routine BLD_SMhalf
      call PRG_manager ('exit', 'BLD_SMHALF', 'UTILITY')
      return
      end
      subroutine BLD_SMone (Smat, & ! Overlap matrix
                            SMone, & ! S-1 (result)
                            Nbasis)
!***********************************************************************
!     Date last modified: June 16, 2005                                *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!     Calculates the inverse of the overlap matrix, Smat.              *
!     It returns with S*(-1) in SMone.                                 *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants

      implicit none
!
! Input scalars:
      integer :: Nbasis
!
! Input arrays:
      double precision Smat(Nbasis,Nbasis)
!
! Output scalars:
!
! Output arrays:
      double precision SMone(Nbasis,Nbasis)
!
! Local work arrays:
      double precision, dimension(:), allocatable :: SCRVEC
      double precision, dimension(:), allocatable :: Eigval
      double precision, dimension(:,:), allocatable :: Eigvec
      double precision, dimension(:,:), allocatable :: SCRMAT
!
! Local scalars:
      integer Ibasis,Jbasis
      logical :: Lerror
!
! Begin:
      call PRG_manager ('enter', 'BLD_SMONE', 'UTILITY')
!
! Allocate work arrays:
      allocate (Eigvec(Nbasis,Nbasis),Eigval(Nbasis), SCRMAT(Nbasis,Nbasis), SCRVEC(Nbasis))
      Lerror=.FALSE.
      call MATRIX_diagonalize (Smat, Eigvec, Eigval, Nbasis, 2, .true.)
      do Ibasis=1,Nbasis
        if(Eigval(Ibasis).LE.ZERO)then
          Lerror=.TRUE.
          exit
        end if
        SCRVEC(Ibasis)=ONE/Eigval(Ibasis)
      end do ! Ibasis

      if(.not.Lerror)then
      do Ibasis=1,Nbasis
      do Jbasis=1,Nbasis
        SCRMAT(Jbasis,Ibasis)=Eigvec(Jbasis,Ibasis)*SCRVEC(Ibasis)
      end do ! Jbasis
      end do ! Ibasis
      SMone=matmul(SCRMAT, transpose(Eigvec))
      else
        write(UNIout,'(A,I4,A)')'ERROR> BLD_SMone: Eigenvalue ',Ibasis,' is zero or negative'
      end if
!
! End of routine BLD_SMone
      call PRG_manager ('exit', 'BLD_SMONE', 'UTILITY')
      return
      end
      subroutine BLD_SPhalf (Smat, & ! Overlap matrix
                             SPhalf, & ! S-1/2 (result)
                             Nbasis)
!***********************************************************************
!     Date last modified: June 16, 2005                                *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!     Calculates the inverse square root of the overlap matrix, Smat.  *
!     It returns with S*(-1/2) in SPhalf.                              *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants

      implicit none
!
! Input scalars:
      integer :: Nbasis
!
! Input arrays:
      double precision Smat(Nbasis*(Nbasis+1)/2)
!
! Output scalars:
      integer :: MATlen
!
! Output arrays:
      double precision SPhalf(Nbasis,Nbasis)
!
! Local work arrays:
      double precision, dimension(:), allocatable :: SCRVEC
      double precision, dimension(:), allocatable :: Eigval
      double precision, dimension(:,:), allocatable :: Eigvec
      double precision, dimension(:,:), allocatable :: SQSM
      double precision, dimension(:,:), allocatable :: SCRMAT
!
! Local scalars:
      integer Ibasis,Jbasis
      logical :: Lerror
!
! Begin:
      call PRG_manager ('enter', 'BLD_SPHALF', 'UTILITY')
!
! Allocate work arrays:
      allocate (Eigvec(Nbasis,Nbasis), Eigval(Nbasis), SCRMAT(Nbasis,Nbasis), SCRVEC(Nbasis), SQSM(Nbasis,Nbasis))
      Lerror=.FALSE.
      MATlen=Nbasis*(Nbasis+1)/2
      call UPT_to_SQS (Smat, MATlen, SQSM, Nbasis)
      call MATRIX_diagonalize (SQSM, Eigvec, Eigval, Nbasis, 2, .true.)
      do Ibasis=1,Nbasis
        if(Eigval(Ibasis).LE.ZERO)then
          Lerror=.TRUE.
          exit
        end if
        SCRVEC(Ibasis)=DSQRT(Eigval(Ibasis))
      end do ! Ibasis

      if(.not.Lerror)then
      do Ibasis=1,Nbasis
      do Jbasis=1,Nbasis
        SCRMAT(Jbasis,Ibasis)=Eigvec(Jbasis,Ibasis)*SCRVEC(Ibasis)
      end do ! Jbasis
      end do ! Ibasis
      SPhalf=matmul(SCRMAT, transpose(Eigvec))
      else
        write(UNIout,'(A,I4,A)')'ERROR> BLD_SPhalf: Eigenvalue ',Ibasis,' is zero or negative'
      end if

      deallocate (Eigvec, Eigval, SCRMAT, SCRVEC, SQSM)
!
! End of routine BLD_SPhalf
      call PRG_manager ('exit', 'BLD_SPHALF', 'UTILITY')
      return
      end
      subroutine UPT_TO_SQS (A, &
                             M, &
                             B, &
                             N)
!**********************************************************************************
!     Date last modified: June 26, 1992                                           *
!     Author:                                                                     *
!     Description: Place linear array (upper triangle) in square symmetric form.  *
!**********************************************************************************
! Modules:
      USE program_files

      implicit none
!
! Input scalars:
      integer M,N
!
! Input array:
      double precision A(M)
!
! Output array:
      double precision B(N,N)
!
! Local scalars:
      integer I,IX,J,JX,K
!
! Begin:
      call PRG_manager ('enter', 'UPT_TO_SQS', 'UTILITY')
!
      K=N*(N+1)/2
      if(M.LT.K)then
        write(UNIout,'(A,I4)')'ERROR> UPT_TO_SQS: DIMENSION M IS TOO SMALL, M =',M
        stop ' Error_UPT_TO_SQS> Dimension error'
      end if
!
      do J=1,N
      JX=N-J+1
      do I=1,JX
        IX=JX-I+1
        B(IX,JX)=A(K)
        K=K-1
      end do ! I
      end do ! J
!
      do J=1,N
      do I=1,J
        B(J,I)=B(I,J)
      end do ! I
      end do ! J
!
! End of routine UPT_TO_SQS
      call PRG_manager ('exit', 'UPT_TO_SQS', 'UTILITY')
      return
      end
      subroutine SQS_TO_UPT (B, &
                             N, &
                             A, &
                             M)
!**********************************************************************************
!     Date last modified: December 1, 2004                                        *
!     Author:                                                                     *
!     Description: Place square symmetric array in linear (upper triangle) form.  *
!**********************************************************************************
! Modules:
      USE program_files
      USE program_constants

      implicit none
!
! Input scalars:
      integer M,N
!
! Input array:
      double precision B(N,N)
!
! Output array:
      double precision A(M)
!
! Local scalars:
      integer I,IX,J,JX,K
!
! Begin:
      call PRG_manager ('enter', 'SQS_TO_UPT', 'UTILITY')
!
      K=N*(N+1)/2
      if(M.LT.K)then
        write(UNIout,'(A,I4)')'ERROR> SQS_TO_UPT: DIMENSION M IS TOO SMALL, M =',M
        stop ' Error_SQS_TO_UPT> Dimension error'
      end if
!
      do J=1,N
      JX=N-J+1
      do I=1,JX
        IX=JX-I+1
        A(K)=B(IX,JX)
        K=K-1
      end do ! I
      end do ! J
!
! End of routine SQS_TO_UPT
      call PRG_manager ('exit', 'SQS_TO_UPT', 'UTILITY')
      return
      end
      subroutine BLD_IJNDX (IJNDX, Nbasis)
!***********************************************************************
!     Date last modified: September 17, 1991               Version 1.0 *
!     Author:                                                          *
!     Description: Build the indexing array.                           *
!***********************************************************************
! Modules:
      USE program_manager

      implicit none
!
! Input scalar:
      integer :: Nbasis
!
! Output array:
      integer :: IJNDX(Nbasis+1)
!
! Includes:
!
! Local scalar:
      integer Ibasis
!
! Begin:
      call PRG_manager ('enter', 'BLD_IJNDX', 'UTILITY')
!
! Initialize indexing array.
      do Ibasis=1,Nbasis+1
        IJNDX(Ibasis)=(Ibasis*(Ibasis-1))/2
      end do
!
! End of routine BLD_IJNDX
      call PRG_manager ('exit', 'BLD_IJNDX', 'UTILITY')
      return
      end
      subroutine INVERT (A, &
                         MA, &
                         MAMAX, &
                         IER)
!***********************************************************************
!     Date last modified: June 26, 1992                                *
!     Author: M. R. Peterson, R. A. Poirier                            *
!     Description:                                                     *
!     Invert matrix A in place.                                        *
!     This routine was modified from the U. of T. Fortran Library      *
!     program 'MINVRD'.                                                *
!                                                                      *
!     A is (MAMAX x MAMAX), but only portion MA x MA is used.          *
!     DETA returns the determinant of A.                               *
!     IER is 0 for a normal return, -2 if the inversion fails.         *
!     IR and IC are work arrays (Size: MA).                            *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants

      implicit none
!
! Input scalars:
      integer MA,IER,MAMAX
!
! Input arrays:
      double precision A(MAMAX,MAMAX)
!
! Work arrays:
      integer, dimension(:), allocatable :: IR
      integer, dimension(:), allocatable :: IC
!
! Local scalars:
      integer I,ICOL,IROW,J,K,L,M
      double precision DETA,PIV,PIV1,T,TEMP,TENM14,THRES
!
      parameter (TENM14=1.0D-14)
!
! Begin:
      call PRG_manager ('enter', 'INVERT', 'UTILITY')
!
! RAP July 22, 1997 (problems with DIIS B-matrix inversion)
!      THRES=1.0D-20
!
      allocate (IR(MA), IC(MA))
!
     ICOL=1
     IROW=1
      IER=0
      do I=1,MA
        IR(I)=0
        IC(I)=0
      end do
      DETA=ONE
      do M=1,MA
! Find largest (absolute) element of A left.
        TEMP=ZERO
        do K=1,MA
          if(IR(K).EQ.0)then
            do L=1,MA
              if(IC(L).EQ.0)then
                T=DABS(A(K,L))
                if(T.GE.TEMP)then
                  I=K
                  J=L
                  TEMP=T
                end if
              end if
            end do ! L
          end if
        end do ! K
! First time, set error threshold.
        if(M.EQ.1)THRES=TEMP*TENM14
! Pivot element is A(I,J).
        PIV=A(I,J)
        DETA=DETA*PIV
        IR(I)=J
        IC(J)=I
! Check for a near-zero pivot element.
        if(TEMP.LE.THRES)then
! Have we already found a zero pivot?
          if(IER.EQ.0)then
            IER=-2
            IROW=J
            ICOL=I
          end if
        else
          PIV=ONE/PIV
          do K=1,MA
            A(I,K)=A(I,K)*PIV
          end do ! K
          A(I,J)=PIV
          do K=1,MA
            if(K.NE.I)then
              PIV1=A(K,J)
              do L=1,MA
                A(K,L)=A(K,L)-PIV1*A(I,L)
              end do ! L
              A(K,J)=PIV1
            end if
          end do ! K
          do K=1,MA
            A(K,J)=-PIV*A(K,J)
          end do ! K
          A(I,J)=PIV
        end if
      end do ! M
! Restore original row and column order.
      do I=1,MA
        K=IC(I)
        M=IR(I)
        if(K.NE.I)then
          DETA=-DETA
          do L=1,MA
            TEMP=A(K,L)
            A(K,L)=A(I,L)
            A(I,L)=TEMP
          end do ! L
          do L=1,MA
            TEMP=A(L,M)
            A(L,M)=A(L,I)
            A(L,I)=TEMP
          end do ! L
          IC(M)=K
          IR(K)=M
        end if
      end do ! I
!
      if(IER.NE.0)then ! Matrix Inversion Error.
! IROW and ICOL are the row and column for a singular matrix:
! Row 'IROW' is dependent:  Row 'IROW' = Sum  A(K,ICOL) * Row 'K'
! for K=1,2,...,MA,  K.NE.IROW.
        write(UNIout,'(A/A/)')'ERROR INVERT> Matrix inversion - remove linear dependence:', &
          ' Term   Contribution to dependence'
        do J=1,MA
          if(J.EQ.IROW)write(UNIout,'(a,I4,a)')' *',J,' *     DEPendENT TERM'
          if(J.NE.IROW)write(UNIout,'(2X,I4,9X,F12.5)')J,A(J,ICOL)
        end do
        stop 'ERROR INVERT> MATRIX INVERSION - LINEAR DEPendENCE'
      end if

      deallocate (IR, IC)
!
! End of routine INVERT
      call PRG_manager ('exit', 'INVERT', 'UTILITY')
      return
      end
      double precision FUNCTION TORSION_IN(I,J,K,L)
!*********************************************************************
! Date last modified: 6 July, 1999                       Version 1.0 *
! Author: Michelle Shaw                                              *
! Description: Calculate torsion angle given four atoms and their    *
!              Cartesian coordinates.                                *
!*********************************************************************
! Modules:
      USE program_constants

      implicit none
!
! Input scalars:
      integer I, J, K, L

! Local scalars:
      double precision R, T, lenS, CUTOFF,RU, RV, RW, RX, RY, RS
!
! Local arrays:
      double precision U(3), V(3), W(3), X(3), Y(3), S(3)

! Begin:
!      call PRG_manager ('enter', 'TORSION_IN', 'UTILITY')

      CUTOFF = 1.0D-11

! After the atoms have been selected, the vectorss must be calculated (length and
!  direction)...
      call VEC (U,RU,I,J)
      call VEC (V,RV,K,J)
      call VEC (W,RW,K,L)

! Now, compute the two cross products IJxJK and JKxKL...
      call VEC_product(X,U,V,RX,1)
      call VEC_product(Y,W,V,RY,1)


! The scalar product will be calculated between X and Y.  Also, the cross product between
!  X and Y to form the vector Z will be taken.
      R = -DOT_Product(X,Y)
      call VEC_product(S,X,Y,RS,0)
      T = DOT_Product(S,V)

! Set the default value of the angle RIC_torsion(iTOR) to Pi/2.  If the torsion angle is 90
! degrees, the scalar product of X and Y will be zero.
      TORSION_IN = PI_VAL/TWO
      if(DABS(R).ge.CUTOFF) then
        lenS = DSQRT(S(1)*S(1) + S(2)*S(2) + S(3)*S(3))
        TORSION_IN = DATAN(lenS/R)
        if(R.lt.ZERO)TORSION_IN = TORSION_IN + PI_VAL
      end if
      if(T.lt.ZERO)TORSION_IN = -TORSION_IN
!      if(dabs(TORSION_IN).ge.(3.1D0))TORSION_IN = dabs(TORSION_IN)

! End of routine TORSION_IN
!      call PRG_manager ('exit', 'TORSION_IN', 'UTILITY')
      RETURN
      END
      double precision FUNCTION OOPBEND_IN(I,J,K,L)
!*********************************************************************
! Date last modified: 6 July, 1999                       Version 1.0 *
! Author: Michelle Shaw                                              *
! Description: Calculate out-of-plane bend angle given four atoms    *
!              and their Cartesian coordinates.                      *
!*********************************************************************
! Modules:
      USE program_constants

      implicit none
!
! Input scalars:
      integer I, J, K, L

! Local scalars:
      double precision R, c_theta, CUTOFF, DOT_Product, RU, RV, RW, RY
!
! Local arrays:
      double precision U(3), V(3), W(3), Y(3)
!
! Begin:
!      call PRG_manager ('enter', 'OOPBEND_IN', 'UTILITY')

! After the atoms have been selected, the vectors must be calculated (length and
!   direction)...

      call VEC (U,RU,I,J)
      call VEC (V,RV,J,K)
      call VEC (W,RW,J,L)

! Now, compute the two cross product JKxJL...
      call VEC_product(Y,V,W,RY,1)

! The scalar product will be calculated between U and Y.
      R = DOT_Product(U,Y)

! Now calculate the out-of-plane bend angle.  If the atoms are coplanar, this angle should be zero.
      c_theta = dsqrt(ONE - R*R)
      OOPBEND_IN = -DACOS(c_theta)

! End of routine OOPBEND_IN
!      call PRG_manager ('exit', 'OOPBEND_IN', 'UTILITY')
      RETURN
      END
      double precision function GET_angleV2(I, K, J)
!***********************************************************************
! Date last modified: 7 June, 1999                         Version 1.0 *
! Author: Michelle Shaw                                                *
! Description: Calculates the angle between three atoms.               *
!              Output (GET_angleV2) is angle between defined atoms.       *
!***********************************************************************
! Modules:
      USE program_constants

      implicit none

! Input scalars:
      integer I, J, K

! Local scalars:
      double precision cos_theta, RU, RV, U_dot_V

! Local arrays:
      double precision U(3), V(3)

! Begin:
!      call PRG_manager ('enter', 'GET_angleV2', 'UTILITY')

      GET_angleV2 = ZERO
      call VEC (U, RU, I, K)
      call VEC (V, RV, J, K)
      cos_theta = doT_product (U,V)
      if(dabs(cos_theta).gt.ONE)cos_theta = dsign(ONE,cos_theta)
      GET_angleV2 = dacos(cos_theta)

! End of function GET_angleV2
!      call PRG_manager ('exit', 'GET_angleV2', 'UTILITY')
      return
      end
      function TraceMatrix (A, Nelem)
!***********************************************************************
!     Date last modified: July 07, 1992                                *
!     Author: R.A. Poirier                                             *
!     Description: Trace of symmetric matrix A                         *
!***********************************************************************
! Modules:
      USE program_constants

      implicit none
!
! Input scalars:
      integer Nelem
!
! Input arrays:
      double precision :: A(Nelem,Nelem)
!
! Local scalars:
      integer :: I
      double precision :: TraceMatrix
!
! Begin:
      TraceMatrix=ZERO
      do I=1,Nelem
        TraceMatrix=TraceMatrix+A(I,I)
      end do ! I
!
! End of function TraceMatrix
      return
      end function TraceMatrix

      function GET_DETERMINANT (MATRIX, N)
!***********************************************************************
!     Date last modified:                                              *
!     Author: Unknown                                                  *
!     Description: Compute the determinant of a matrix                 *
!***********************************************************************

      implicit none

      integer :: N
      double precision :: GET_DETERMINANT
      integer :: MATRIX(N,N)

      integer :: J
      double precision, dimension(:,:), allocatable :: WORK_MATRIX
      double precision :: DET

! Check that MATRIX is 2-dimensional
      if(size(shape(MATRIX)).ne.2)then
        write(*,*) 'PROBLEM WITH DETERMINANT SIZE'
        write(*,*) MATRIX
      endif !(shape(shape(MATRIX)).ne.2)then
! Check that both dimension start with indice 1
      if(LBOUND(MATRIX,1).ne.1)then
        write(*,*) 'PROBLEM WITH DETERMINANT SIZE'
        write(*,*) MATRIX
      endif !(LBOUND(MATRIX,1).ne.1)then
      if(LBOUND(MATRIX,2).ne.1)then
        write(*,*) 'PROBLEM WITH DETERMINANT SIZE'
        write(*,*) MATRIX
      endif !(LBOUND(MATRIX,1).ne.1)then
! Check that MATRIX is a square matrix of dimension N
      N=UBOUND(MATRIX,1)
      if(UBOUND(MATRIX,2).ne.N)then
        write(*,*) 'PROBLEM WITH DETERMINANT SIZE'
        write(*,*) MATRIX
      endif !(LBOUND(MATRIX,1).ne.1)then

      allocate(WORK_MATRIX(N,N))
      WORK_MATRIX=dble(MATRIX)

!     write(6,*)'Before call LUDCMP'
!     call flush(6)
      call LUDCMP (WORK_MATRIX, N, DET)
!     write(6,*)'After call LUDCMP'
!     call flush(6)
      do J=1,N
        DET=DET*WORK_MATRIX(J,J)
      enddo ! J=1,N
      GET_DETERMINANT=DET

      deallocate(WORK_MATRIX)

      return

CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine LUDCMP (MATRIX, N, DET)
!***********************************************************************
!     Date last modified:                                              *
!     Author: Unknown                                                  *
!     Description: Compute the determinant of a matrix                 *
!***********************************************************************

      implicit none

      integer :: N
      double precision :: MATRIX(N,N)
      double precision :: DET

      integer :: I,IMAX,J,K
      double precision :: SMALL
      double precision :: AAMAX,DUM,SUMM,VAL
      double precision, dimension(:), allocatable :: VV
      integer, dimension(:), allocatable :: INDX

      parameter(SMALL=1.0e-20)

      allocate(INDX(n))
      allocate(VV(n))

!     write(6,*)'After First loops'
!     write(6,*)'MATRIX ',MATRIX
!     call flush(6)
      IMAX=0
      do I=1,N
        AAMAX=0.0
        do J=1,N
          VAL=abs(MATRIX(I,J))
          if(VAL.gt.AAMAX) AAMAX=VAL
        enddo ! J=1,N
!     write(6,*)'AAMAX ',AAMAX
!     call flush(6)
        if(AAMAX.eq.0.0D0)then
          VV(I)=0.0D0
          else
          VV(I)=1.0/AAMAX
        end if
      enddo ! I=1,N
!     write(6,*)'After First loops'
!     call flush(6)

      DET=1.0

      do J=1,N
        do I=2,J-1
          SUMM=MATRIX(I,J)
          do K=1,I-1
            SUMM=SUMM-MATRIX(I,K)*MATRIX(K,J)
          enddo ! K=1,I-1
          MATRIX(I,J)=SUMM
        enddo ! I=1,J-1

!     write(6,*)'After second loops'
!     call flush(6)
        AAMAX=0.0D0
        do I=J,N
          SUMM=MATRIX(I,J)
          do K=1,J-1
            SUMM=SUMM-MATRIX(I,K)*MATRIX(K,J)
          enddo ! K=1,J-1
          MATRIX(I,J)=SUMM
          DUM=VV(I)*abs(SUMM)
          if(DUM.ge.AAMAX)then
            IMAX=I
            AAMAX=DUM
          endif ! (DUM.ge.AAMAX)
        enddo ! I=J,N
!     write(6,*)'After third loops'
!     call flush(6)

        if(J.ne.IMAX)then
          do K=1,N
            DUM=MATRIX(IMAX,K)
            MATRIX(IMAX,K)=MATRIX(J,K)
            MATRIX(J,K)=DUM
          enddo ! K=1,N
          DET=-DET
          VV(IMAX)=VV(J)
        endif ! (J.ne.IMAX)

        INDX(J)=IMAX
        if(MATRIX(J,J).eq.0.0D0) MATRIX(J,J)=SMALL
        if(J.ne.N)then
          DUM=1.0/MATRIX(J,J)
          do I=J+1,N
            MATRIX(I,J)=MATRIX(I,J)*DUM
          enddo ! I=J+1,N
        endif ! (J.ne.N)

      enddo ! J=1,N

      deallocate(INDX)
      deallocate(VV)

      return

      end subroutine LUDCMP
      end function GET_DETERMINANT
      subroutine MATRIX_diagonalize (MA, &
                                     Mvec, &
                                     EIGvals, &
                                     Nelem, &
                                     IEIG, &
                                     Lsort) ! Sort the eigenvectors
!***********************************************************************
!     Date last modified: September 25, 2012                           *
!     Author: Modified by R.A. Poirier                                 *
!     Description:                                                     *
!     This routine is a translation of the ALGOL procedure TRED2,      *
!     Num. Math. 11, 181-195(1968) by Martin, Reinsch and Wilkinson.   *
!     Handbook for Auto. Comp., Vol.II - Linear Algebra, 212-226(1971).*
!     This routine is a modified version of Argonne laboratories'      *
!     EISPACK subprogram 'TRED2', and was taken from the U. of Toronto *
!     subroutine library, with permission.  Modifications to allow     *
!     optional omission of the eigenvectors made by M. Peterson.       *
!                                                                      *
!     This routine reduces a real symmetric matrix to a symmetric      *
!     tridiagonal matrix using and accumulating orthogonal             *
!     similarity transformations.                                      *
!                                                                      *
!     On input:                                                        *
!       MA contains the real symmetric input matrix.  Only the         *
!          lower triangle of the matrix need be supplied;              *
!        IEIG is +-1 for just eigenvalues, +-2 for eigenvectors also.  *
!          If IEIG<0, A is not copied to Z (A and Z then must coincide *
!          or input the matrix to be diagonalized as matrix Z).        *
!       MA and Mvec may coincide.  If distinct, MA is unaltered.       *
!       To save a copy step, the matrix may be input as Mvec           *
!       (with IEIG<0).                                                 *
!                                                                      *
!     At end (before call to TQL12):                                   *
!     EIGvecs contains the diagonal elements of the tridiagonal matrix *
!     EIGvecs contains the subdiagonal elements of the tridiagonal     *
!     matrix in its last N-1 positions.  EIGvecs(1) is set to zero;    *
!     Mvec contains the orthogonal transformation matrix produced      *
!          in the reduction (if IEIG=+-2, else Mvec is destroyed);     *
!        ICODE has absolute value of IEIG.                             *
!***********************************************************************
! Modules:
      USE program_constants

      implicit none
!
! Input scalars:
      integer IEIG,N,NMAX,Nelem
      logical Lsort
!
! Input arrays:
      double precision, dimension(Nelem,Nelem) ::  MA
      double precision, dimension(Nelem,Nelem) ::  MVEC
      double precision, dimension(Nelem) ::  EIGvals
!
! Work arrays:
      double precision, dimension(:), allocatable :: WORK
!
! Local scalars:
      integer I,ICODE,II,J,JP1,K,L
      double precision F,G,H,HH,Tscale
!
! Begin:
      call PRG_manager ('enter', 'MATRIX_diagonalize', 'UTILITY')
!
      allocate (WORK(Nelem))

      if(IEIG.GE.0)then
        do J=1,Nelem
          do I=J,Nelem
            Mvec(I,J)=MA(I,J)
          end do
        end do
      end if
      ICODE=IABS(IEIG)
      if(Nelem.NE.1)then
! *** For I=Nelem step -1 until 2 do -- ***
        do II=2,Nelem
          I=Nelem+2-II
          L=I-1
          H=ZERO
          Tscale=ZERO
          if(L.GE.2)then
! Scale row (ALGOL TOL then not needed).
            do K=1,L
              Tscale=Tscale+DABS(Mvec(I,K))
            end do
          end if
          if(Tscale.EQ.0.or.L.LT.2)then
!
            WORK(I)=Mvec(I,L)
            EIGvals(I)=H
          else
!
            do K=1,L
              F=Mvec(I,K)/Tscale
              Mvec(I,K)=F
              H=H+F*F
            end do
!
            G=-DSIGN(DSQRT(H),F)
            WORK(I)=Tscale*G
            H=H-F*G
            Mvec(I,L)=F-G
            F=ZERO
!
            do J=1,L
! The following statement need only be executed if eigenvectors are
! needed, but most often ICODE is 2, so do it anyway.
              Mvec(J,I)=Mvec(I,J)/(Tscale*H)
              G=ZERO
! Form element of A*U.
              do K=1,J
                G=G+Mvec(J,K)*Mvec(I,K)
              end do
!
              JP1=J+1
              if(L.GE.JP1)then
!
                do K=JP1,L
                  G=G+Mvec(K,J)*Mvec(I,K)
                end do
              end if
!
! Form element of P.
              WORK(J)=G/H
                F=F+WORK(J)*Mvec(I,J)
              end do
!
! Form reduced MA.
              HH=F/(H+H)
              do J=1,L
                F=Mvec(I,J)
                G=WORK(J)-HH*F
                WORK(J)=G
                do K=1,J
                  Mvec(J,K)=Mvec(J,K)-F*WORK(K)-G*Mvec(I,K)
                end do ! K
              end do ! J
!
              do K=1,L
                Mvec(I,K)=Tscale*Mvec(I,K)
              end do
!
            EIGvals(I)=H
          end if
        end do ! I1
      end if
!
      EIGvals(1)=ZERO
      WORK(1)=ZERO
! Accumulation of transformation matrices.
      if(ICODE.NE.2)then
! Alternate final loop for eigenvalues only.
        do I=1,Nelem
          EIGvals(I)=Mvec(I,I)
        end do
      else
        do I=1,Nelem
          L=I-1
          if(EIGvals(I).NE.ZERO)then
            do J=1,L
              G=ZERO
              do K=1,L
                G=G+Mvec(I,K)*Mvec(K,J)
              end do
              do K=1,L
                Mvec(K,J)=Mvec(K,J)-G*Mvec(K,I)
              end do
            end do
          end if
          EIGvals(I)=Mvec(I,I)
          Mvec(I,I)=ONE
          if(L.GE.1)then
            do J=1,L
              Mvec(I,J)=ZERO
              Mvec(J,I)=ZERO
            end do
         end if
        end do
      end if
      call TQL12_new

      deallocate (WORK)
!
! End of routine MATRIX_diagonalize
      call PRG_manager ('exit', 'MATRIX_diagonalize', 'UTILITY')
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine TQL12_new
!***********************************************************************
!     Date last modified: June 26, 1992                                *
!     Author: Mike Peterson, U. of T. Chemistry Dept., Toronto.        *
!     Description:                                                     *
!     This routine is a translation of the ALGOL procedure TQL2,       *
!     Num. Math. 11, 293-306(1968) by Bowdler, Martin, Reinsch and     *
!     Wilkinson.                                                       *
!     Handbook for Auto. Comp., Vol.II - Linear Algebra, 227-240(1971).*
!     This routine is a modified version of Argonne laboratories'      *
!     EISPACK subprogram 'TQL2', and was taken from the U. of Toronto  *
!     subroutine library, with permission.  Modifications to allow     *
!     optional omission of the eigenvectors made by M. Peterson.       *
!                                                                      *
!     This routine finds the eigenvalues and eigenvectors of a         *
!     symmetric tridiagonal matrix by the QL method.                   *
!     The eigenvectors of a full symmetric matrix can also be found    *
!     if TRED12 has been used to reduce this full matrix to            *
!     tridiagonal form.                                                *
!                                                                      *
!     On input:                                                        *
!        Nelem is the order of the matrix;                                 *
!        NMAX is the declared dimension of Z;                          *
!        Z contains the transformation matrix produced in the          *
!          reduction by TRED12, if performed.  If the eigenvectors     *
!          of a tridiagonal matrix are desired, Z must contain the     *
!          identity matrix;                                            *
!        D contains the diagonal elements of the input matrix;         *
!        E contains the subdiagonal elements of the input matrix       *
!          in its last N-1 positions.  WORK(1) is arbitrary;              *
!        ICODE is 1 for just eigenvalues, 2 for eigenvectors also.     *
!                                                                      *
!     On output:                                                       *
!        D contains the eigenvalues in ascending order;                *
!        E has been destroyed;                                         *
!        Z contains orthonormal eigenvectors of the symmetric          *
!          tridiagonal (or full) matrix.                               *
!                                                                      *
!     RELACC is a machine dependent parameter specifying the relative  *
!     precision of floating point arithmetic.                          *
!***********************************************************************
! Modules:
      USE program_files

      implicit none
!
! Input scalars:
!
! Input arrays:
!
! Local scalars:
      integer I,II,J,K,L,L1,M,MML
      double precision B,C,F,G,H,P,R,RELACC,S,SIXTEN
!
      parameter (SIXTEN=16.0D0)
!
! Begin:
      call PRG_manager ('enter', 'TQL12_new', 'UTILITY')
!
      if(Nelem.GT.1)then
!?IBM
!     RELACC=SIXTEN**(-13)
!??
!?CDC/CRAY
!     RELACC=TWO**(-47)
!??
!?VAX
!     RELACC=TWO**(-55)    (Value given by IMSL FN IMACH).
      RELACC=SIXTEN**(-13)
!??
      do I=2,Nelem
        WORK(I-1)=WORK(I)
      end do
!
      F=ZERO
      B=ZERO
      WORK(Nelem)=ZERO
!
      do L=1,Nelem
         J=0
         H=RELACC*(DABS(EIGvals(L))+DABS(WORK(L)))
         if(B.LT.H)B=H
!* Look for small sub-diagonal element.
         M=L
         do WHILE(M.LE.Nelem.and.DABS(WORK(M)).GT.B)
           M=M+1
         end do
         if(M.NE.L)then
  130      if(J.EQ.30)GO TO 400
           J=J+1
! Form shift.
           L1=L+1
           G=EIGvals(L)
           P=(EIGvals(L1)-G)/(TWO*WORK(L))
           R=DSQRT(P*P+ONE)
           EIGvals(L)=WORK(L)/(P+DSIGN(R,P))
           H=G-EIGvals(L)
           do I=L1,Nelem
             EIGvals(I)=EIGvals(I)-H
           end do
           F=F+H
! QL Transformation.
           P=EIGvals(M)
           C=ONE
           S=ZERO
           MML=M-L
! *** For I=M-1 step -1 until L do -- ***
           do II=1,MML
             I=M-II
             G=C*WORK(I)
             H=C*P
             if(DABS(P).GE.DABS(WORK(I)))then
               C=WORK(I)/P
               R=DSQRT(C*C+ONE)
               WORK(I+1)=S*P*R
               S=C/R
               C=ONE/R
             else
               C=P/WORK(I)
               R=DSQRT(C*C+ONE)
               WORK(I+1)=S*WORK(I)*R
               S=ONE/R
               C=C*S
             end if
             P=C*EIGvals(I)-S*G
             EIGvals(I+1)=H+S*(C*G+S*EIGvals(I))
! Form vector.
             if(ICODE.EQ.2)then
               do K=1,Nelem
                 H=Mvec(K,I+1)
                 Mvec(K,I+1)=S*Mvec(K,I)+C*H
                 Mvec(K,I)=C*Mvec(K,I)-S*H
               end do
             end if
           end do
           WORK(L)=S*P
           EIGvals(L)=C*P
           if(DABS(WORK(L)).GT.B)GO TO 130
         end if
         EIGvals(L)=EIGvals(L)+F
      end do
! Order eigenvalues and eigenvectors.
      if(Lsort)then
      do II=2,Nelem
        I=II-1
        K=I
        P=EIGvals(I)
        do J=II,Nelem
          if(EIGvals(J).LT.P)then
            K=J
            P=EIGvals(J)
          end if
        end do
        if(K.NE.I)then
          EIGvals(K)=EIGvals(I)
          EIGvals(I)=P
          if(ICODE.EQ.2)then
            do J=1,Nelem
              P=Mvec(J,I)
              Mvec(J,I)=Mvec(J,K)
              Mvec(J,K)=P
            end do
          end if
        end if
      end do
      end if ! Lsort
!
      end if ! Nelem.GT.1
!
! End of routine TQL12_new
      call PRG_manager ('exit', 'TQL12_new', 'UTILITY')
      return
!
! Error exit.
! No convergence to an eigenvalue after 30 iterations.
  400 write(UNIout,'(a,i6,a)')'ERROR> TQL12_new: EIGENVALUE',L,' NOT FOUND IN 30 ITERATIONS IN TRED12/TQL12'
      stop ' Error_TQL12_new> Did not converge'
      end subroutine TQL12_new
      end subroutine MATRIX_diagonalize
      function TraceAB (A, B, Nelem)
!***********************************************************************
!     Date last modified: November 6, 2012                             *
!     Author: R.A. Poirier                                             *
!     Description: Trace of a product of symmetric matrices A and B    *
!                  stored in linear vectors.                           *
!***********************************************************************
! Modules:
      USE program_constants

      implicit none
!     
! Input scalars:
      integer :: Nelem
!
! Input arrays:
      double precision :: A(Nelem*(Nelem+1)/2),B(Nelem*(Nelem+1)/2)
!         
! Local scalars:
      integer :: I,J,K
      double precision :: TraceAB
!       
! Begin:
      TraceAB=ZERO
      K=0 
      do J=1,Nelem
      do I=1,J
        K=K+1
        TraceAB=TraceAB+TWO*A(K)*B(K)
      end do ! I
      TraceAB=TraceAB-A(K)*B(K)
      end do ! J
!
! End of function TraceAB
      return
      end
      function GET_DETERMINANTI (MATRIX, N)
!***********************************************************************
!     Date last modified:                                              *
!     Author: Unknown                                                  *
!     Description: Compute the determinant of a matrix                 *
!***********************************************************************

      implicit none

      integer :: GET_DETERMINANTI
      integer :: MATRIX(N,N)

      integer, dimension(:,:), allocatable :: WORK_MATRIX
      integer :: DET
      integer :: N,J

! Check that MATRIX is 2-dimensional
      if(size(shape(MATRIX)).ne.2)then
        write(*,*) 'PROBLEM WITH DETERMINANT SIZE'
        write(*,*) MATRIX
      endif !(shape(shape(MATRIX)).ne.2)then
! Check that both dimension start with indice 1
      if(LBOUND(MATRIX,1).ne.1)then
        write(*,*) 'PROBLEM WITH DETERMINANT SIZE'
        write(*,*) MATRIX
      endif !(LBOUND(MATRIX,1).ne.1)then
      if(LBOUND(MATRIX,2).ne.1)then
        write(*,*) 'PROBLEM WITH DETERMINANT SIZE'
        write(*,*) MATRIX
      endif !(LBOUND(MATRIX,1).ne.1)then
! Check that MATRIX is a square matrix of dimension N
      N=UBOUND(MATRIX,1)
      if(UBOUND(MATRIX,2).ne.N)then
        write(*,*) 'PROBLEM WITH DETERMINANT SIZE'
        write(*,*) MATRIX
      endif !(LBOUND(MATRIX,1).ne.1)then

      allocate(WORK_MATRIX(N,N))
      WORK_MATRIX=MATRIX

      call LUDCMP(WORK_MATRIX,N,DET)
      do J=1,N
        DET=DET*WORK_MATRIX(J,J)
      enddo ! J=1,N
      GET_DETERMINANTI=DET

      deallocate(WORK_MATRIX)

      return

CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine LUDCMP (MATRIX,N,D)
!***********************************************************************
!     Date last modified:                                              *
!     Author: Unknown                                                  *
!     Description: Compute the determinant of a matrix                 *
!***********************************************************************

      implicit none

      integer, dimension(:,:), allocatable :: MATRIX
      integer :: D
      integer :: N

      double precision :: SMALL

      integer, dimension(:), allocatable :: VV
      integer, dimension(:), allocatable :: INDX
      integer :: AAMAX,DUM,SUMM,VAL
      integer :: I,IMAX,J,K

      parameter(SMALL=1.0e-20)

      allocate(INDX(n))
      allocate(VV(n))

      IMAX=0
      do I=1,N
        AAMAX=0
        do J=1,N
          VAL=abs(MATRIX(I,J))
          if(VAL.gt.AAMAX) AAMAX=VAL
        enddo ! J=1,N
        VV(I)=1/AAMAX
      enddo ! I=1,N

      D=1

      do J=1,N

        do I=1,J-1
          SUMM=MATRIX(i,j)
          do K=1,I-1
            SUMM=SUMM-MATRIX(I,K)*MATRIX(K,J)
          enddo ! K=1,I-1
          MATRIX(I,J)=SUMM
        enddo ! I=1,J-1

        AAMAX=0
        do I=J,N
          SUMM=MATRIX(I,J)
          do K=1,J-1
            SUMM=SUMM-MATRIX(I,K)*MATRIX(K,J)
          enddo ! K=1,J-1
          MATRIX(I,J)=SUMM
          DUM=VV(I)*abs(SUMM)
          if(DUM.ge.AAMAX)then
            IMAX=I
            AAMAX=DUM
          endif ! (DUM.ge.AAMAX)
        enddo ! I=J,N
        if(J.ne.IMAX)then
          do K=1,N
            DUM=MATRIX(IMAX,K)
            MATRIX(IMAX,K)=MATRIX(J,K)
            MATRIX(J,K)=DUM
          enddo ! K=1,N
          D=-D
          VV(IMAX)=VV(J)
        endif ! (J.ne.IMAX)
        INDX(J)=IMAX
        if(MATRIX(J,J).eq.0)then
          write(6,*)'Diagonal of the matrix is zero' !  MATRIX(J,J)=SMALL
          stop
        end if
        if(J.ne.N)then
          DUM=1/MATRIX(J,J)
          do I=J+1,N
            MATRIX(I,J)=MATRIX(I,J)*DUM
          enddo ! I=J+1,N
        endif ! (J.ne.N)

      enddo ! J=1,N

      deallocate(INDX)
      deallocate(VV)

      return

      end subroutine LUDCMP
      end function GET_DETERMINANTI
!
function alngam ( xvalue, ifault )
!*****************************************************************************80
!
!! ALNGAM computes the logarithm of the gamma function.
!
!  Modified:
!
!    13 January 2008
!
!  Author:
!
!    Original FORTRAN77 version by Allan Macleod.
!    FORTRAN90 version by John Burkardt.
!
!  Reference:
!
!    Allan Macleod,
!    Algorithm AS 245,
!    A Robust and Reliable Algorithm for the Logarithm of the Gamma Function,
!    Applied Statistics,
!    Volume 38, Number 2, 1989, pages 397-402.
!
!  Parameters:
!
!    Input, real ( kind = 8 ) XVALUE, the argument of the Gamma function.
!
!    Output, integer ( kind = 4 ) IFAULT, error flag.
!    0, no error occurred.
!    1, XVALUE is less than or equal to 0.
!    2, XVALUE is too big.
!
!    Output, real ( kind = 8 ) ALNGAM, the logarithm of the gamma function of X.
!
  implicit none

  real ( kind = 8 ) alngam
  real ( kind = 8 ), parameter :: alr2pi = 0.918938533204673D+00
  integer ( kind = 4 ) ifault
  real ( kind = 8 ), dimension ( 9 ) :: r1 = (/ &
    -2.66685511495D+00, &
    -24.4387534237D+00, &
    -21.9698958928D+00, &
     11.1667541262D+00, &
     3.13060547623D+00, &
     0.607771387771D+00, &
     11.9400905721D+00, &
     31.4690115749D+00, &
     15.2346874070D+00 /)
  real ( kind = 8 ), dimension ( 9 ) :: r2 = (/ &
    -78.3359299449D+00, &
    -142.046296688D+00, &
     137.519416416D+00, &
     78.6994924154D+00, &
     4.16438922228D+00, &
     47.0668766060D+00, &
     313.399215894D+00, &
     263.505074721D+00, &
     43.3400022514D+00 /)
  real ( kind = 8 ), dimension ( 9 ) :: r3 = (/ &
    -2.12159572323D+05, &
     2.30661510616D+05, &
     2.74647644705D+04, &
    -4.02621119975D+04, &
    -2.29660729780D+03, &
    -1.16328495004D+05, &
    -1.46025937511D+05, &
    -2.42357409629D+04, &
    -5.70691009324D+02 /)
  real ( kind = 8 ), dimension ( 5 ) :: r4 = (/ &
     0.279195317918525D+00, &
     0.4917317610505968D+00, &
     0.0692910599291889D+00, &
     3.350343815022304D+00, &
     6.012459259764103D+00 /)
  real ( kind = 8 ) x
  real ( kind = 8 ) x1
  real ( kind = 8 ) x2
  real ( kind = 8 ), parameter :: xlge = 5.10D+05
  real ( kind = 8 ), parameter :: xlgst = 1.0D+30
  real ( kind = 8 ) xvalue
  real ( kind = 8 ) y

  x = xvalue
  alngam = 0.0D+00
!
!  Check the input.
!
  if ( xlgst <= x ) then
    ifault = 2
    return
  end if

  if ( x <= 0.0D+00 ) then
    ifault = 1
    return
  end if

  ifault = 0
!
!  Calculation for 0 < X < 0.5 and 0.5 <= X < 1.5 combined.
!
  if ( x < 1.5D+00 ) then

    if ( x < 0.5D+00 ) then

      alngam = - log ( x )
      y = x + 1.0D+00
!
!  Test whether X < machine epsilon.
!
      if ( y == 1.0D+00 ) then
        return
      end if

    else

      alngam = 0.0D+00
      y = x
      x = ( x - 0.5D+00 ) - 0.5D+00

    end if

    alngam = alngam + x * (((( &
        r1(5)   * y &
      + r1(4) ) * y &
      + r1(3) ) * y &
      + r1(2) ) * y &
      + r1(1) ) / (((( &
                  y &
      + r1(9) ) * y &
      + r1(8) ) * y &
      + r1(7) ) * y &
      + r1(6) )

    return

  end if
!
!  Calculation for 1.5 <= X < 4.0.
!
  if ( x < 4.0D+00 ) then

    y = ( x - 1.0D+00 ) - 1.0D+00

    alngam = y * (((( &
        r2(5)   * x &
      + r2(4) ) * x &
      + r2(3) ) * x &
      + r2(2) ) * x &
      + r2(1) ) / (((( &
                  x &
      + r2(9) ) * x &
      + r2(8) ) * x &
      + r2(7) ) * x &
      + r2(6) )
!
!  Calculation for 4.0 <= X < 12.0.
!
  else if ( x < 12.0D+00 ) then

    alngam = (((( &
        r3(5)   * x &
      + r3(4) ) * x &
      + r3(3) ) * x &
      + r3(2) ) * x &
      + r3(1) ) / (((( &
                  x &
      + r3(9) ) * x &
      + r3(8) ) * x &
      + r3(7) ) * x &
      + r3(6) )
!
!  Calculation for 12.0 <= X.
!
  else

    y = log ( x )
    alngam = x * ( y - 1.0D+00 ) - 0.5D+00 * y + alr2pi

    if ( x <= xlge ) then

      x1 = 1.0D+00 / x
      x2 = x1 * x1

      alngam = alngam + x1 * ( ( &
             r4(3)   * &
        x2 + r4(2) ) * &
        x2 + r4(1) ) / ( ( &
        x2 + r4(5) ) * &
        x2 + r4(4) )

    end if

  end if

  return
end
function alnorm ( x, upper )

!*****************************************************************************80
!
!! ALNORM computes the cumulative density of the standard normal distribution.
!
!  Modified:
!
!    13 January 2008
!
!  Author:
!
!    Original FORTRAN77 version by David Hill.
!    FORTRAN90 version by John Burkardt.
!
!  Reference:
!
!    David Hill,
!    Algorithm AS 66:
!    The Normal Integral,
!    Applied Statistics,
!    Volume 22, Number 3, 1973, pages 424-427.
!
!  Parameters:
!
!    Input, real ( kind = 8 ) X, is one endpoint of the semi-infinite interval
!    over which the integration takes place.
!
!    Input, logical UPPER, determines whether the upper or lower
!    interval is to be integrated:
!    .TRUE.  => integrate from X to + Infinity;
!    .FALSE. => integrate from - Infinity to X.
!
!    Output, real ( kind = 8 ) ALNORM, the integral of the standard normal
!    distribution over the desired interval.
!
  implicit none

  real ( kind = 8 ), parameter :: a1 = 5.75885480458D+00
  real ( kind = 8 ), parameter :: a2 = 2.62433121679D+00
  real ( kind = 8 ), parameter :: a3 = 5.92885724438D+00
  real ( kind = 8 ) alnorm
  real ( kind = 8 ), parameter :: b1 = -29.8213557807D+00
  real ( kind = 8 ), parameter :: b2 = 48.6959930692D+00
  real ( kind = 8 ), parameter :: c1 = -0.000000038052D+00
  real ( kind = 8 ), parameter :: c2 = 0.000398064794D+00
  real ( kind = 8 ), parameter :: c3 = -0.151679116635D+00
  real ( kind = 8 ), parameter :: c4 = 4.8385912808D+00
  real ( kind = 8 ), parameter :: c5 = 0.742380924027D+00
  real ( kind = 8 ), parameter :: c6 = 3.99019417011D+00
  real ( kind = 8 ), parameter :: con = 1.28D+00
  real ( kind = 8 ), parameter :: d1 = 1.00000615302D+00
  real ( kind = 8 ), parameter :: d2 = 1.98615381364D+00
  real ( kind = 8 ), parameter :: d3 = 5.29330324926D+00
  real ( kind = 8 ), parameter :: d4 = -15.1508972451D+00
  real ( kind = 8 ), parameter :: d5 = 30.789933034D+00
  real ( kind = 8 ), parameter :: ltone = 7.0D+00
  real ( kind = 8 ), parameter :: p = 0.398942280444D+00
  real ( kind = 8 ), parameter :: q = 0.39990348504D+00
  real ( kind = 8 ), parameter :: r = 0.398942280385D+00
  logical up
  logical upper
  real ( kind = 8 ), parameter :: utzero = 18.66D+00
  real ( kind = 8 ) x
  real ( kind = 8 ) y
  real ( kind = 8 ) z

  up = upper
  z = x

  if ( z < 0.0D+00 ) then
    up = .not. up
    z = - z
  end if

  if ( ltone < z .and. ( ( .not. up ) .or. utzero < z ) ) then

    if ( up ) then
      alnorm = 0.0D+00
    else
      alnorm = 1.0D+00
    end if

    return

  end if

  y = 0.5D+00 * z * z

  if ( z <= con ) then

    alnorm = 0.5D+00 - z * ( p - q * y &
      / ( y + a1 + b1 &
      / ( y + a2 + b2 & 
      / ( y + a3 ))))

  else

    alnorm = r * exp ( - y ) &
      / ( z + c1 + d1 &
      / ( z + c2 + d2 &
      / ( z + c3 + d3 &
      / ( z + c4 + d4 &
      / ( z + c5 + d5 &
      / ( z + c6 ))))))

  end if

  if ( .not. up ) then
    alnorm = 1.0D+00 - alnorm
  end if

  return
end
subroutine gamma_inc_values ( n_data, a, x, fx )

!*****************************************************************************80
!
!! GAMMA_INC_VALUES returns some values of the incomplete Gamma function.
!
!  Discussion:
!
!    The (normalized) incomplete Gamma function P(A,X) is defined as:
!
!      PN(A,X) = 1/Gamma(A) * Integral ( 0 <= T <= X ) T**(A-1) * exp(-T) dT.
!
!    With this definition, for all A and X,
!
!      0 <= PN(A,X) <= 1
!
!    and
!
!      PN(A,INFINITY) = 1.0
!
!    In Mathematica, the function can be evaluated by:
!
!      1 - GammaRegularized[A,X]
!
!  Modified:
!
!    20 November 2004
!
!  Author:
!
!    John Burkardt
!
!  Reference:
!
!    Milton Abramowitz, Irene Stegun,
!    Handbook of Mathematical Functions,
!    National Bureau of Standards, 1964,
!    ISBN: 0-486-61272-4,
!    LC: QA47.A34.
!
!    Stephen Wolfram,
!    The Mathematica Book,
!    Fourth Edition,
!    Cambridge University Press, 1999,
!    ISBN: 0-521-64314-7,
!    LC: QA76.95.W65.
!
!  Parameters:
!
!    Input/output, integer ( kind = 4 ) N_DATA.  The user sets N_DATA to 0 
!    before the first call.  On each call, the routine increments N_DATA by 1,
!    and returns the corresponding data; when there is no more data, the
!    output value of N_DATA will be 0 again.
!
!    Output, real ( kind = 8 ) A, the parameter of the function.
!
!    Output, real ( kind = 8 ) X, the argument of the function.
!
!    Output, real ( kind = 8 ) FX, the value of the function.
!
  implicit none

  integer ( kind = 4 ), parameter :: n_max = 20

  real ( kind = 8 ) a
  real ( kind = 8 ), save, dimension ( n_max ) :: a_vec = (/ &
    0.10D+00, &
    0.10D+00, &
    0.10D+00, &
    0.50D+00, &
    0.50D+00, &
    0.50D+00, &
    0.10D+01, &
    0.10D+01, &
    0.10D+01, &
    0.11D+01, &
    0.11D+01, &
    0.11D+01, &
    0.20D+01, &
    0.20D+01, &
    0.20D+01, &
    0.60D+01, &
    0.60D+01, &
    0.11D+02, &
    0.26D+02, &
    0.41D+02 /)
  real ( kind = 8 ) fx
  real ( kind = 8 ), save, dimension ( n_max ) :: fx_vec = (/ &
    0.7382350532339351D+00, &
    0.9083579897300343D+00, &
    0.9886559833621947D+00, &
    0.3014646416966613D+00, &
    0.7793286380801532D+00, &
    0.9918490284064973D+00, &
    0.9516258196404043D-01, &
    0.6321205588285577D+00, &
    0.9932620530009145D+00, &
    0.7205974576054322D-01, &
    0.5891809618706485D+00, &
    0.9915368159845525D+00, &
    0.1018582711118352D-01, &
    0.4421745996289254D+00, &
    0.9927049442755639D+00, &
    0.4202103819530612D-01, &
    0.9796589705830716D+00, &
    0.9226039842296429D+00, &
    0.4470785799755852D+00, &
    0.7444549220718699D+00 /)
  integer ( kind = 4 ) n_data
  real ( kind = 8 ) x
  real ( kind = 8 ), save, dimension ( n_max ) :: x_vec = (/ &
    0.30D-01, &
    0.30D+00, &
    0.15D+01, &
    0.75D-01, &
    0.75D+00, &
    0.35D+01, &
    0.10D+00, &
    0.10D+01, &
    0.50D+01, &
    0.10D+00, & 
    0.10D+01, &
    0.50D+01, &
    0.15D+00, &
    0.15D+01, &
    0.70D+01, &
    0.25D+01, &
    0.12D+02, &
    0.16D+02, &
    0.25D+02, &
    0.45D+02 /)

  if ( n_data < 0 ) then
    n_data = 0
  end if

  n_data = n_data + 1

  if ( n_max < n_data ) then
    n_data = 0
    a = 0.0D+00
    x = 0.0D+00
    fx = 0.0D+00
  else
    a = a_vec(n_data)
    x = x_vec(n_data)
    fx = fx_vec(n_data)
  end if

  return
end
function gammad ( x, p, ifault )

!*****************************************************************************80
!
!! GAMMAD computes the Incomplete Gamma Integral
!
!  Auxiliary functions:
!
!    ALOGAM = logarithm of the gamma function, 
!    ALNORM = algorithm AS66
!
!  Modified:
!
!    20 January 2008
!
!  Author:
!
!    Original FORTRAN77 version by B Shea.
!    FORTRAN90 version by John Burkardt.
!
!  Reference:
!
!    B Shea,
!    Algorithm AS 239:
!    Chi-squared and Incomplete Gamma Integral,
!    Applied Statistics,
!    Volume 37, Number 3, 1988, pages 466-473.
!
!  Parameters:
!
!    Input, real ( kind = 8 ) X, P, the parameters of the incomplete 
!    gamma ratio.  0 <= X, and 0 < P.
!
!    Output, integer ( kind = 4 ) IFAULT, error flag.
!    0, no error.
!    1, X < 0 or P <= 0.
!
!    Output, real ( kind = 8 ) GAMMAD, the value of the incomplete 
!    Gamma integral.
!
  implicit none

  real ( kind = 8 ) a
  real ( kind = 8 ) alnorm
  real ( kind = 8 ) alngam
  real ( kind = 8 ) an
  real ( kind = 8 ) arg
  real ( kind = 8 ) b
  real ( kind = 8 ) c
  real ( kind = 8 ), parameter :: elimit = - 88.0D+00
  real ( kind = 8 ) gammad
  integer ( kind = 4 ) ifault
  real ( kind = 8 ), parameter :: oflo = 1.0D+37
  real ( kind = 8 ) p
  real ( kind = 8 ), parameter :: plimit = 1000.0D+00
  real ( kind = 8 ) pn1
  real ( kind = 8 ) pn2
  real ( kind = 8 ) pn3
  real ( kind = 8 ) pn4
  real ( kind = 8 ) pn5
  real ( kind = 8 ) pn6
  real ( kind = 8 ) rn
  real ( kind = 8 ), parameter :: tol = 1.0D-14
  logical upper
  real ( kind = 8 ) x
  real ( kind = 8 ), parameter :: xbig = 1.0D+08

  gammad = 0.0D+00
!
!  Check the input.
!
  if ( x < 0.0D+00 ) then
    ifault = 1
    return
  end if

  if ( p <= 0.0D+00 ) then
    ifault = 1
    return
  end if

  ifault = 0

  if ( x == 0.0D+00 ) then
    gammad = 0.0D+00
    return
  end if
!
!  If P is large, use a normal approximation.
!
  if ( plimit < p ) then

    pn1 = 3.0D+00 * sqrt ( p ) * ( ( x / p )**( 1.0D+00 / 3.0D+00 ) &
    + 1.0D+00 / ( 9.0D+00 * p ) - 1.0D+00 )

    upper = .false.
    gammad = alnorm ( pn1, upper )
    return

  end if
!
!  If X is large set GAMMAD = 1.
!
  if ( xbig < x ) then
    gammad = 1.0D+00
    return
  end if
!
!  Use Pearson's series expansion.
!  (Note that P is not large enough to force overflow in ALOGAM).
!  No need to test IFAULT on exit since P > 0.
!
  if ( x <= 1.0D+00 .or. x < p ) then

    arg = p * log ( x ) - x - alngam ( p + 1.0D+00, ifault )
    c = 1.0D+00
    gammad = 1.0D+00
    a = p

    do

      a = a + 1.0D+00
      c = c * x / a
      gammad = gammad + c

      if ( c <= tol ) then
        exit
      end if

    end do

    arg = arg + log ( gammad )

    if ( elimit <= arg ) then
      gammad = exp ( arg )
    else
      gammad = 0.0D+00
    end if
!
!  Use a continued fraction expansion.
!
  else 

    arg = p * log ( x ) - x - alngam ( p, ifault )
    a = 1.0D+00 - p
    b = a + x + 1.0D+00
    c = 0.0D+00
    pn1 = 1.0D+00
    pn2 = x
    pn3 = x + 1.0D+00
    pn4 = x * b
    gammad = pn3 / pn4

    do

      a = a + 1.0D+00
      b = b + 2.0D+00
      c = c + 1.0D+00
      an = a * c
      pn5 = b * pn3 - an * pn1
      pn6 = b * pn4 - an * pn2

      if ( pn6 /= 0.0D+00 ) then

        rn = pn5 / pn6

        if ( abs ( gammad - rn ) <= min ( tol, tol * rn ) ) then
          exit
        end if

        gammad = rn

      end if

      pn1 = pn3
      pn2 = pn4
      pn3 = pn5
      pn4 = pn6
!
!  Re-scale terms in continued fraction if terms are large.
!
      if ( oflo <= abs ( pn5 ) ) then
        pn1 = pn1 / oflo
        pn2 = pn2 / oflo
        pn3 = pn3 / oflo
        pn4 = pn4 / oflo
      end if

    end do

    arg = arg + log ( gammad )

    if ( elimit <= arg ) then
      gammad = 1.0D+00 - exp ( arg )
    else
      gammad = 1.0D+00
    end if

  end if

  return
end
! Incomplete gamma function taken from http://arxiv.org/pdf/1306.1754.pdf
function daw ( xx )

!*****************************************************************************80
!
!! DAW evaluates Dawson's integral function.
!
!  Discussion:
!
!    This routine evaluates Dawson's integral,
!
!      F(x) = exp ( - x * x ) * Integral ( 0 <= t <= x ) exp ( t * t ) dt
!
!    for a real argument x.
!
!  Licensing:
!
!    This code is distributed under the GNU LGPL license.
!
!  Modified:
!
!    03 April 2007
!
!  Author:
!
!    Original FORTRAN77 version by William Cody.
!    FORTRAN90 version by John Burkardt.
!
!  Reference:
!
!    William Cody, Kathleen Paciorek, Henry Thacher,
!    Chebyshev Approximations for Dawson's Integral,
!    Mathematics of Computation,
!    Volume 24, Number 109, January 1970, pages 171-178.
!
!  Parameters:
!
!    Input, real ( kind = 8 ) XX, the argument of the function.
!
!    Output, real ( kind = 8 ) DAW, the value of the function.
!
  implicit none

  real ( kind = 8 ) daw
  real ( kind = 8 ) frac
  integer ( kind = 4 ) i
  real ( kind = 8 ) one225
  real ( kind = 8 ) p1(10)
  real ( kind = 8 ) p2(10)
  real ( kind = 8 ) p3(10)
  real ( kind = 8 ) p4(10)
  real ( kind = 8 ) q1(10)
  real ( kind = 8 ) q2(9)
  real ( kind = 8 ) q3(9)
  real ( kind = 8 ) q4(9)
  real ( kind = 8 ) six25
  real ( kind = 8 ) sump
  real ( kind = 8 ) sumq
  real ( kind = 8 ) two5
  real ( kind = 8 ) w2
  real ( kind = 8 ) x
  real ( kind = 8 ) xlarge
  real ( kind = 8 ) xmax
  real ( kind = 8 ) xsmall
  real ( kind = 8 ) xx
  real ( kind = 8 ) y
!
!  Mathematical constants.
!
  data six25 / 6.25D+00 /
  data one225 / 12.25d0 /
  data two5 / 25.0d0 /
!
!  Machine-dependent constants
!
  data xsmall /1.05d-08/
  data xlarge /9.49d+07/
  data xmax /2.24d+307/
!
!  Coefficients for R(9,9) approximation for  |x| < 2.5
!
  data p1/-2.69020398788704782410d-12, 4.18572065374337710778d-10, &
          -1.34848304455939419963d-08, 9.28264872583444852976d-07, &
          -1.23877783329049120592d-05, 4.07205792429155826266d-04, &
          -2.84388121441008500446d-03, 4.70139022887204722217d-02, &
          -1.38868086253931995101d-01, 1.00000000000000000004d+00/
  data q1/ 1.71257170854690554214d-10, 1.19266846372297253797d-08, &
           4.32287827678631772231d-07, 1.03867633767414421898d-05, &
           1.78910965284246249340d-04, 2.26061077235076703171d-03, &
           2.07422774641447644725d-02, 1.32212955897210128811d-01, &
           5.27798580412734677256d-01, 1.00000000000000000000d+00/
!
!  Coefficients for R(9,9) approximation in J-fraction form
!  for  x in [2.5, 3.5)
!
  data p2/-1.70953804700855494930d+00,-3.79258977271042880786d+01, &
           2.61935631268825992835d+01, 1.25808703738951251885d+01, &
          -2.27571829525075891337d+01, 4.56604250725163310122d+00, &
          -7.33080089896402870750d+00, 4.65842087940015295573d+01, &
          -1.73717177843672791149d+01, 5.00260183622027967838d-01/
  data q2/ 1.82180093313514478378d+00, 1.10067081034515532891d+03, &
          -7.08465686676573000364d+00, 4.53642111102577727153d+02, &
           4.06209742218935689922d+01, 3.02890110610122663923d+02, &
           1.70641269745236227356d+02, 9.51190923960381458747d+02, &
           2.06522691539642105009d-01/
!
!  Coefficients for R(9,9) approximation in J-fraction form
!  for  x in [3.5, 5.0]
!
  data p3/-4.55169503255094815112d+00,-1.86647123338493852582d+01, &
          -7.36315669126830526754d+00,-6.68407240337696756838d+01, &
           4.84507265081491452130d+01, 2.69790586735467649969d+01, &
          -3.35044149820592449072d+01, 7.50964459838919612289d+00, &
          -1.48432341823343965307d+00, 4.99999810924858824981d-01/
  data q3/ 4.47820908025971749852d+01, 9.98607198039452081913d+01, &
           1.40238373126149385228d+01, 3.48817758822286353588d+03, &
          -9.18871385293215873406d+00, 1.24018500009917163023d+03, &
          -6.88024952504512254535d+01,-2.31251575385145143070d+00, &
           2.50041492369922381761d-01/
!
!  Coefficients for R(9,9) approximation in J-fraction form
!  for 5.0 < |x|.
!
  data p4/-8.11753647558432685797d+00,-3.84043882477454453430d+01, &
          -2.23787669028751886675d+01,-2.88301992467056105854d+01, &
          -5.99085540418222002197d+00,-1.13867365736066102577d+01, &
          -6.52828727526980741590d+00,-4.50002293000355585708d+00, &
          -2.50000000088955834952d+00, 5.00000000000000488400d-01/
  data q4/ 2.69382300417238816428d+02, 5.04198958742465752861d+01, &
           6.11539671480115846173d+01, 2.08210246935564547889d+02, &
           1.97325365692316183531d+01,-1.22097010558934838708d+01, &
          -6.99732735041547247161d+00,-2.49999970104184464568d+00, &
           7.49999999999027092188d-01/

  x = xx

  if ( xlarge < abs ( x ) ) then

    if ( abs ( x ) <= xmax ) then
      daw = 0.5D+00 / x
    else
      daw = 0.0D+00
    end if

  else if ( abs ( x ) < xsmall ) then

    daw = x

  else

    y = x * x
!
!  ABS(X) < 2.5.
!
    if ( y < six25 ) then

      sump = p1(1)
      sumq = q1(1)
      do i = 2, 10
        sump = sump * y + p1(i)
        sumq = sumq * y + q1(i)
      end do

      daw = x * sump / sumq
!
!  2.5 <= ABS(X) < 3.5.
!
    else if ( y < one225 ) then

      frac = 0.0D+00
      do i = 1, 9
        frac = q2(i) / ( p2(i) + y + frac )
      end do

      daw = ( p2(10) + frac ) / x
!
!  3.5 <= ABS(X) < 5.0.
!
    else if ( y < two5 ) then

      frac = 0.0D+00
      do i = 1, 9
        frac = q3(i) / ( p3(i) + y + frac )
      end do

      daw = ( p3(10) + frac ) / x

    else
!
!  5.0 <= ABS(X) <= XLARGE.
!
      w2 = 1.0D+00 / x / x

      frac = 0.0D+00
      do i = 1, 9
        frac = q4(i) / ( p4(i) + y + frac )
      end do
      frac = p4(10) + frac

      daw = ( 0.5D+00 + 0.5D+00 * w2 * frac ) / x

    end if

  end if

  return
end
!
      function TEIindex(N,a,b,c,d)
      ! Row-major indexing for 4-index integrals

      integer :: TEIindex
      integer :: a,b,c,d
      integer :: N
!
      TEIindex = N**3*(a-1) + N**2*(b-1) + N*(c-1) + d
!
      end function TEIindex
!
      function bico(n,k)
      ! binomial coefficient

      integer :: bico
      integer :: n,k
      integer :: factorial
!
      bico = 1
      do i = n-k+1, n
        bico = i*bico
      end do
      bico = bico/factorial(k)
!
      end function bico
!
      function factorial(n)
      ! factorial

      integer :: factorial
      integer :: n
      integer :: i
!
      factorial = 1
      do i = 1, n
        factorial = factorial*i
      end do
!
      end function factorial
!
      subroutine BLD_MK_grid(N_pts,r_scale,r_pts,r_wts)
!****************************************************************
!     Date: September 29, 2009                                  !
!     Author: JWH                                               !
!     Description: Setup a Mura and Knowles grid of r points.   !
!                  Integrates from zero to infinity.            !
!     See J. Comput. Chem. 2002, 24, 732-740.                   !
!****************************************************************
      implicit none
!
! Input:
      integer, intent(IN) :: N_pts
      double precision, intent(IN) :: r_scale
! Output:
      double precision, dimension(:), intent(OUT) :: r_pts(N_pts), r_wts(N_pts)
! Local:
      integer :: I_pt
      double precision :: xi
!
! Begin:
!
! set up quadrature grid(Taken from IntraculeInput.C)
      do I_pt=1,N_pts
        xi=dble(I_pt)/dble(N_pts+1)
        r_pts(I_pt) = -dlog(1.0D0-xi**3)*r_scale
        r_wts(I_pt) = (3.0D0*xi**2*(dlog(1.0D0-xi**3))**2*r_scale**3)&
                      /(dble(N_pts+1)*(1.0D0-xi**3)*r_pts(I_pt)**2)
      end do
!
!      write(6,*)'MK quadrature grid'
!      write(6,*)'pt        r         weight'
!      do I_pt= 1, N_pts
!        write(6,'(I3,X,2F20.14)')I_pt,r_pts(I_pt),r_wts(I_pt)
!      end do
!
      end subroutine BLD_MK_grid
!
