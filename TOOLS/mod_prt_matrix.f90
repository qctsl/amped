      module matrix_print
!****************************************************************************************************************************
!     Date last modified: February 16, 2000                                                                    Version 2.0  *
!     Author: R.A. Poirier                                                                                                  *
!     Description: Scalars and arrays used to read-in the basis set                                                         *
!****************************************************************************************************************************
! Modules:
      USE program_files
!     USE type_header

      implicit none

      interface PRT_MATRIX
        module procedure PRT_matrixR
        module procedure PRT_matrixI
        module procedure PRT_matrixB
        module procedure PRT_matrix2e
        module procedure PRT_matrixL
      end interface

CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine PRT_matrixR (Array,  & ! Array to be printed
                              Nrow,   & ! Dimension (NROWS)
                              Ncol)  ! Dimension (NCOLUMNS)
!***********************************************************************
!     Date last modified: June 26, 1992                                *
!     Author:                                                          *
!     Description: Matrix print routine.                               *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer, intent(in) :: Nrow,Ncol
!
! Input array:
      double precision, dimension(:,:), intent(in) :: Array
!
! Local scalars:
      integer Irow,ifLAG,ILOWER,IUPPER,Jcol
!
! Begin:
      call PRG_manager ('enter', 'PRT_matrixR', 'UTILITY')
!
!     if(DUMP_to_file)then
!       write(header_unit,fmt=file_format)((Array(Irow,Jcol),Irow=1,Nrow),Jcol=1,Ncol)
!     else

      ifLAG=1
      ILOWER=1
 Icol:do
      IUPPER=ILOWER+9
      if(IUPPER.GE.Ncol)then
         IUPPER=Ncol
         ifLAG=0
      end if

      write(UNIout,'(3X,10(9X,I3))')(Jcol,Jcol=ILOWER,IUPPER)
      do Irow=1,Nrow
!        write(UNIout,'(1X,I3,2X,10G12.6)')Irow,(Array(Irow,Jcol),Jcol=ILOWER,IUPPER)
         write(UNIout,'(1X,I3,2X,10F12.6)')Irow,(Array(Irow,Jcol),Jcol=ILOWER,IUPPER)
      end do ! Irow
      write(UNIout,'(X)')

      if(ifLAG.eq.0)exit Icol
      ILOWER=ILOWER+10
      end do Icol

!     end if ! DUMP_to_file
!
! End of routine PRT_matrixR
      call PRG_manager ('exit', 'PRT_matrixR', 'UTILITY')
      return
      end subroutine PRT_matrixR
      subroutine PRT_matrixI (Array,   & ! Array to be printed
                              Nrow,   & ! Dimension (NROWS)
                              Ncol)  ! Dimension (NCOLUMNS)
!***********************************************************************
!     Date last modified: June 26, 1992                                *
!     Author:                                                          *
!     Description: Matrix print routine.                               *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer, intent(in) :: Nrow,Ncol
!
! Input array:
      integer, dimension(:,:), intent(in) :: Array
!
! Local scalars:
      integer Irow,ifLAG,ILOWER,IUPPER,Jcol
!
! Begin:
      call PRG_manager ('enter', 'PRT_matrixI', 'UTILITY')
!
      ifLAG=1
      ILOWER=1
 Icol:do
      IUPPER=ILOWER+9
      if(IUPPER.GE.Ncol)then
         IUPPER=Ncol
         ifLAG=0
      end if

      write(UNIout,'(3X,10(9X,I3))')(Jcol,Jcol=ILOWER,IUPPER)
      do Irow=1,Nrow
         write(UNIout,'(I3,10I12)')Irow,(Array(Irow,Jcol),Jcol=ILOWER,IUPPER)
      end do
      write(UNIout,'(X)')

      if(ifLAG.eq.0)exit Icol
      ILOWER=ILOWER+10

      end do Icol
!
! End of routine PRT_matrixI
      call PRG_manager ('exit', 'PRT_matrixI', 'UTILITY')
      return
      end subroutine PRT_matrixI
      subroutine PRT_matrixB (Array,  & ! Boolean array to be printed
                              Nrow,   & ! Dimension (NROWS)
                              Ncol)  ! Dimension (NCOLUMNS)
!***********************************************************************
!     Date last modified: June 26, 1992                                *
!     Author:                                                          *
!     Description: Matrix print routine.                               *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer, intent(in) :: Nrow,Ncol
!
! Input array:
      logical, dimension(:,:), intent(in) :: Array
!
! Local scalars:
      integer Irow,ifLAG,ILOWER,IUPPER,Jcol
!
! Begin:
      call PRG_manager ('enter', 'PRT_matrixB', 'UTILITY')
!
      ifLAG=1
      ILOWER=1
 Icol:do
      IUPPER=ILOWER+19
      if(IUPPER.GE.Ncol)then
         IUPPER=Ncol
         ifLAG=0
      end if

      write(UNIout,'(6X,20(I4))')(Jcol,Jcol=ILOWER,IUPPER)
      do Irow=1,Nrow
         write(UNIout,'(1X,I3,2X,20L4)')Irow,(Array(Irow,Jcol),Jcol=ILOWER,IUPPER)
      end do
      write(UNIout,'(X)')

      if(ifLAG.eq.0)exit Icol
      ILOWER=ILOWER+20

      end do Icol
!
! End of routine PRT_matrixB
      call PRG_manager ('exit', 'PRT_matrixB', 'UTILITY')
      return
      end subroutine PRT_matrixB
      subroutine PRT_matrixL (Array, &
                         Nelem, &
                         Nrow)  !
!***********************************************************************
!     Date last modified: January 26, 2005                             *
!     Author: R.A. Poirier and D. Keefe                                *
!     Description: Print a matrix store in linear form.                *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer Nelem,Nrow
!
! Input array:
      double precision, dimension(:), intent(in) :: Array
!
! Local scalars:
      integer I,ifLAG,ILOWER,IUPPER,J
!
! Begin:
      call PRG_manager ('enter', 'PRT_matrixL', 'UTILITY')
!
      ifLAG=1
      ILOWER=1
 Icol:do
      IUPPER=ILOWER+9
      if(IUPPER.GE.Nrow)then
         IUPPER=Nrow
         ifLAG=0
      end if

      write(UNIout,1000)(J,J=ILOWER,IUPPER)
      write(UNIout,'(X)')
      do I=1,Nrow
      write(UNIout,1002)I,(Array(max0(I,J)*(max0(I,J)-1)/2+min0(I,J)),J=ILOWER,IUPPER)
      end do ! I
      write(UNIout,'(X)')

      if(ifLAG.eq.0)exit Icol
      ILOWER=ILOWER+10

      end do Icol
!
 1000 format(3X,10(9X,I3))
 1002 format(1X,I3,2X,10F12.6)
!
! End of routine PRT_matrixL
      call PRG_manager ('exit', 'PRT_matrixL', 'UTILITY')
      return
      end subroutine PRT_matrixL
      subroutine PRT_matrix2e (XMAT, &
                               Ndim1, &
                               Ndim2, &
                               NELEM)
!***********************************************************************
!     Date last modified: February 9, 1994                             *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version.                                       *
!     Print a set of matrices stored in linear form: XMAT(NELEM)       *
!     NELEM = Ndim2* (Ndim1*(Ndim1+1)/2)
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer Ndim1,Ndim2,NELEM
!
! Input arrays:
      double precision XMAT(NELEM)
!
! Local scalars:
      integer Ibasis,IELEM,IFLAG,IJ,ILOWER,IUPPER,Jbasis,KFOCK
!
! Local arrays:
      double precision BUFF(10)
!
! Begin:
      call PRG_manager ('enter', 'PRT_matrix2e', 'UTILITY')
!
      do KFOCK=1,Ndim2
      write(UNIout,*)' MATRIX NUMBER: ',KFOCK
      IFLAG=1
      ILOWER=1
 Icol:do
      IUPPER=ILOWER+9
      if(IUPPER.GE.Ndim1)then
         IUPPER=Ndim1
         IFLAG=0
      end if
      write(UNIout,'(3X,10(9X,I3))')(Jbasis,Jbasis=ILOWER,IUPPER)
      write(UNIout,'(X)')
!
      do Ibasis=1,Ndim1
      IELEM=0
      do Jbasis=ILOWER,IUPPER
      IELEM=IELEM+1
      if(Ibasis.GE.Jbasis)then
        IJ=Ibasis*(Ibasis-1)/2+Jbasis
      else
        IJ=Jbasis*(Jbasis-1)/2+Ibasis
      end if
      BUFF(IELEM)=XMAT((IJ-1)*Ndim2+KFOCK)
      end do ! Jbasis
      write(UNIout,'(1X,I3,2X,10F12.6)')Ibasis,(BUFF(Jbasis),Jbasis=1,IELEM)
      end do ! Ibasis
!
      write(UNIout,'(X)')

      if(ifLAG.eq.0)exit Icol
      ILOWER=ILOWER+10

      end do Icol
      end do ! KFOCK
!
! End of routine PRT_matrix2e
      call PRG_manager ('exit', 'PRT_matrix2e', 'UTILITY')
      return
      end subroutine PRT_matrix2e
      end module matrix_print
