      module program_codes
!*************************************************************************************************
!     Date last modified:  February 29, 2008                                                     *
!     Author:  R.A. Poirier                                                                      *
!     Description: Controls codes.                                                               *
!*************************************************************************************************
! Modules:
      USE program_constants

      implicit none

      logical :: gaumolinfo, gaudensityinfo, gauMOinfo
      logical :: LGTBAS1,L321G
!
! Gaussian defaults
      type type_code
        character(len=MAX_string) :: Version
        character(len=MAX_string) :: setenv
        character(len=MAX_string) :: file_input
        character(len=MAX_string) :: file_output
        character(len=MAX_string) :: theory_basis
        character(len=MAX_string) :: orient
        character(len=MAX_string) :: route
      end type type_code

      type (type_code) :: Gaussian

! MUNgauss defaults
      type type_MUNgauss
        character(len=MAX_string) :: Version
        character(len=MAX_string) :: setenv
        character(len=MAX_string) :: file_input
        character(len=MAX_string) :: file_output
        character(len=MAX_string) :: theory_basis
      end type type_MUNgauss

      type (type_code) :: MUNgauss

      end module program_codes
