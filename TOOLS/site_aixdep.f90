      subroutine GETMACH (MACHNAM, lenstr)
! -------------------------------------------------------------------- *
! Comments: gets MACHINE name
! -------------------------------------------------------------------- *
! Modules:
      USE program_files

      implicit none

      integer lenstr
      character*(*) MachNam
      character(len=132) String
      integer length, rcode

      call sysinf (String, 'hostname', length, rcode)

      if(rcode.eq.0)then
        MACHNAM(1:) = String(1:length)//'           '
        lenstr=length
      else
       write(UNIout,'(a)')' GETMACH: no machine name '
       MACHNAM(1:)='UNKNOWN'
       lenstr=0
      end if
      return
      end
      subroutine EXEPRG (prgnam, inpnam, outnam, LPipe)
!*****************************************************************************************************************
!     Date last modified: July 11, 2005.                                                            Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: executes a program (UNIX version)                                                             *
!     author: G. Jansen,  30 jul 91                                                                              *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE program_constants

      implicit none
!
! Input scalars:
      logical :: LPipe
      character*(*) prgnam,inpnam,outnam
!
! Local function:
      integer system

! Local scalar:
      integer :: lenprg,leninp,lenout
      integer retcod
      character(len=2*MAX_string) command

! Begin:
      lenprg=len_trim(prgnam)
      if(prgnam(1:3).eq.'   ')then
        write(uniout,'(a)')'error_EXEPRG> no program name'
        stop 'error_EXEPRG> no program name'
      else
        if(lenprg.gt.MAX_string)then
          write(uniout,'(a)')'error_EXEPRG> command name too long'
          call PRG_stop ('error_EXEPRG> command name too long')
        end if
      end if

      if(inpnam(1:3).ne.'   ')then
        leninp=len_trim(inpnam)
        if(leninp.gt.MAX_string)then
          write(uniout,'(a)')'error_EXEPRG> command name too long'
          call PRG_stop ('error_EXEPRG> command name too long')
        end if
      end if

      if(outnam(1:3).ne.'   ')then
        lenout=len_trim(outnam)
        if(lenout.gt.MAX_string)then
          write(uniout,'(a)')'error_EXEPRG> command name too long'
          call PRG_stop ('error_EXEPRG> command name too long')
        end if
      end if

      if(LPipe)then
        command(1:)=trim(prgnam)//' < '//trim(inpnam)//' > '//trim(outnam)
      else
        command(1:)=trim(prgnam)//' '//trim(inpnam)//' '//trim(outnam)
      end if

      retcod = system(trim(command))

      if(retcod.lt.0)then
        write(uniout,'(3a)')'ERROR> EXEPRG: execution of "', &
                             trim(command),'" failed'
        stop 'error_EXEPRG> execution of program failed'
      end if

! End of Routine EXEPRG
      return
      end
