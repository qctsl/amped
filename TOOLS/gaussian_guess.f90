      subroutine GET_gaussian_guess (UNIT_log)
!*************************************************************************************
!     Date last modified:  April 2017                                                *
!     Author: R.A. Poirier                                                           *
!     Description: Generate a Gaussian Input file                                    *
!*************************************************************************************
! Modules
      USE program_constants
      USE program_codes
      USE program_files
      USE type_molecule
      USE type_basis_set
!     USE CML_extract

      implicit none
!
! Local Scalars:
      integer :: Iatom,lenName,retcode,UNIT_com,UNIT_log
      logical :: Lerror
      double precision :: Xbohr,Ybohr,Zbohr
      character(len=32) :: COM_file,LOG_file
!
      COM_file='Gaussian_guess.com'
      lenName=len_trim(COM_file)
      call GET_unit (COM_file, UNIT_com, Lerror)
      PRG_file(UNIT_com)%status='NEW '
      PRG_file(UNIT_com)%name=COM_file(1:lenName)
      PRG_file(UNIT_com)%form='FORMATTED'
      PRG_file(UNIT_com)%type='gaussian'
      call FILE_open (UNIT_com)
!
      write(UNIT_com,'(a)')'%mem=1024MB'
      if(LGTBAS1.and.index(BASIS%name,'6-31G').ne.0)then
        write(UNIT_com,'(3a)')'#N HF/GTBAS1 nosym GFinput pop=full 6d density'
      else
        write(UNIT_com,'(3a)')'#N HF/',BASIS%name(1:len_trim(BASIS%name)),' nosym GFinput pop=full 6d density'
      end if
      write(UNIT_com,'(a)')
      write(UNIT_com,'(a)')MOL_title(1:len_trim(MOL_title))
      write(UNIT_com,'(a)')
      write(UNIT_com,'(2i2)')MOLECULE%charge,Multiplicity

      do Iatom=1,Natoms
        Xbohr=CARTESIAN(Iatom)%X*bohr_to_Angstrom
        Ybohr=CARTESIAN(Iatom)%Y*bohr_to_Angstrom
        Zbohr=CARTESIAN(Iatom)%Z*bohr_to_Angstrom
        write(UNIT_com,'(i4,3f12.6)')CARTESIAN(Iatom)%Atomic_number,Xbohr,Ybohr,Zbohr
      end do ! Iatom
      write(UNIT_com,'(a)')
!
      LOG_file='Gaussian_guess.log'
      call GET_unit (LOG_file, UNIT_log, Lerror)
      lenName=len_trim(LOG_file)
      PRG_file(UNIT_log)%status='NEW '
      PRG_file(UNIT_log)%name=LOG_file(1:lenName)
      PRG_file(UNIT_log)%form='FORMATTED'
      PRG_file(UNIT_log)%type='gaussian'
      PRG_file(UNIT_log)%action='READWRITE'
      call FILE_open (UNIT_log)

      retcode=system('module load gaussian/g16.b01')
      call EXEPRG ('g16', COM_file, LOG_file, .true.)
      PRG_file(UNIT_log)%status='OLD '
!
      call FILE_delete (COM_file, Lerror)
!
! end of routine GET_gaussian_guess
      return
      end subroutine GET_gaussian_guess
      subroutine GET_COM_unit
!************************************************************************
!     Date last modified: June, 1997                        Version 1.0 *
!     Author: R.A. Poirier                                              *
!     Description: Generate the bonds                                   *
!************************************************************************
! Modules:
      USE type_molecule
      USE program_files
      USE QM_objects
!     USE CCCDB_CML

      implicit none
!
! Input scalar:
!
! Local scalar:
      integer :: IStart,IStartP1,Iend,IendM1
      logical :: Lerror
      character(len=128) :: File_name
      character(len=12) :: Number  ! Is for example 10_123456789
!
! Begin:
      call PRG_manager ('enter', 'GET_COM_unit', 'UTILITY')
!
      call BLD_FileID
      IStart=index(MOL_title, '>')
      IStartP1=IStart+1
      Iend=index(MOL_title, '<')
      IendM1=Iend-1
      if(IStartP1.eq.0.or.IendM1.eq.0)then
        write(UNIout,'(a)')'No GDB13 number provided: '
        write(UNIout,'(2a)')' Number: ', Number
        stop 'No GDB13 number provided'
      end if
      Number(1:)=MOL_title(IStartP1:IendM1)
      if(len_trim(Number).ne.12)then
        write(UNIout,'(a)')'GDB13 number must be 12 digit long!'
        write(UNIout,'(2a)')' Number: ', Number
        stop 'GDB13 number must be 12 digit long'
      end if
      File_name(1:)='Gaussian_GDB13_'//Number(1:12)//'_'//Basic_FileID(1:len_trim(Basic_FileID))//'.com'
      call GET_unit ('COM', COM_unit, Lerror)
      PRG_file(COM_unit)%status='NEW '
      PRG_file(COM_unit)%name=File_name
      PRG_file(COM_unit)%form='FORMATTED'
      PRG_file(COM_unit)%type='COM '

      call FILE_open (COM_unit)
!
      write(UNIout,'(3a)')'Gaussian input file: ',File_name(1:len_trim(File_name)),' open'
      MOL_title(IStart:Iend)=' '
!
! End of routine GET_COM_unit
      call PRG_manager ('exit', 'GET_COM_unit', 'UTILITY')
      return
      end subroutine GET_COM_unit
