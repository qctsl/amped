/**********************************************************************/
/* This routine gets strings containing system relevant information,  */
/* i.e. USER, HOME, PATH, SHELL, TERM, CWD ( Current Working Direc-   */
/* tory ), DATE.                                                      */
/*                                                                    */
/* author: G. Jansen                                                  */
/* date: 23 May 1991                                                  */
/**********************************************************************/

#include <sys/param.h>
#include <time.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#ifndef NULL
#define NULL 0
#endif

sysinf_(env_var_value, env_var_name, env_var_length , ret_code)

char env_var_value[];       /* value of environment variable (output) */
char env_var_name[];          /* name of environment variable (input) */
unsigned int *env_var_length; /* length of env. variable (output) */
unsigned int *ret_code;     /* return code 
                                 = -1 : env_var_name empty
                                 =  0 : successful execution
                                 =  1 : env_var_name not found
                            */
{
  char *getenv();          /* standard function */
  unsigned int strlen();   /* standard function */
  int strncmp();           /* standard function */
  int getpid();            /* standard function */

  char *varpnt;         /* pointer to environment variable */
  size_t     temp;

/* Empty env_var_name: */
  if ( !strncmp(env_var_name," ",1) ) {
    sprintf(env_var_value,"%s"," ");
    *env_var_length = 0;
    *ret_code = -1;
  }

/* Machine: */
  else if ( !strncmp(env_var_name,"HOSTNAME",8) ||
            !strncmp(env_var_name,"hostname",8) ) {
  temp=32;
  *ret_code=gethostname (env_var_value, temp);
  *env_var_length = strlen(env_var_value);
  }

/* User: */
  else if ( !strncmp(env_var_name,"USER",4) ||
            !strncmp(env_var_name,"user",4) ) {
    varpnt = getenv("USER");
    sprintf(env_var_value,"%s",varpnt);
    *env_var_length = strlen(varpnt);
    *ret_code = 0;
  }

/* Home directory of user: */
  else if ( !strncmp(env_var_name,"HOME",4) ||
            !strncmp(env_var_name,"home",4) ) {
    varpnt = getenv("HOME");
    sprintf(env_var_value,"%s",varpnt);
    *env_var_length = strlen(varpnt);
    *ret_code = 0;
  }

/* Path for shell (sh): */
  else if ( !strncmp(env_var_name,"PATH",4) ||
            !strncmp(env_var_name,"path",4) ) {
    varpnt = getenv("PATH");
    sprintf(env_var_value,"%s",varpnt);
    *env_var_length = strlen(varpnt);
    *ret_code = 0;
  }

/* Login shell: */
  else if ( !strncmp(env_var_name,"SHELL",5) ||
            !strncmp(env_var_name,"shell",5) ) {
    varpnt = getenv("SHELL");
    sprintf(env_var_value,"%s",varpnt);
    *env_var_length = strlen(varpnt);
    *ret_code = 0;
  }

/* Terminal: */
  else if ( !strncmp(env_var_name,"TERM",4) ||
            !strncmp(env_var_name,"term",4) ) {
    varpnt = getenv("TERM");
    sprintf(env_var_value,"%s",varpnt);
    *env_var_length = strlen(varpnt);
    *ret_code = 0;
  }

/* Current working directory: */
  else if ( !strncmp(env_var_name,"CWD",3) ||
            !strncmp(env_var_name,"cwd",3) ) {
    getwd(env_var_value);
    *env_var_length = strlen(env_var_value);
    *ret_code = 0;
  }

/* Date: */
  else if ( !strncmp(env_var_name,"DATE",4) ||
            !strncmp(env_var_name,"date",4) ) {
    time_t now;
    now=time(NULL);
    sprintf(env_var_value,"%s", ctime(&now));
    *env_var_length = 24;
    *ret_code = 0;
  }

  /* Process ID: */
    else if ( !strncmp(env_var_name,"PID",3) ||
              !strncmp(env_var_name,"PID",3) ) {
      sprintf(env_var_value,"%i", getpid());
      *env_var_length = strlen(env_var_value);
      *ret_code = 0;
    }

/* Unknown env_var_name: */
  else {
    sprintf(env_var_value,"%s"," ");
    *env_var_length = 0;
    *ret_code = 1;
  }
}
