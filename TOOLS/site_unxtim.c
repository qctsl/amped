/**********************************************************************/
/* This routine gets the amount of time the calling program has been  */
/* running in seconds                                                 */
/*                                                                    */
/* author: G. Jansen                                                  */
/* date: 05 Jun 1991                                                  */
/**********************************************************************/

#include <sys/param.h>
#include <sys/resource.h>
#include <sys/types.h>
#include <stdio.h>

int unxtim_ (time_seconds , ret_code)

double *time_seconds; /* run time in seconds */
int    *ret_code;     /* return code 
                                 =  0 : successful execution
                                 = -1 : run time not available 
                       */

{
  struct rusage cpu ;

  if (getrusage(RUSAGE_SELF, &cpu) == -1 ) {
    *time_seconds = 0;
    *ret_code = -1;
  }
  else { 
    *time_seconds= ((double)cpu.ru_utime.tv_sec)
                   + ((double)cpu.ru_utime.tv_usec)/ (1000000.0) ;
/*  printf ("  %g \n", *time_seconds); */
    *ret_code = 0;
  }
}
