#define UNIX 1
#define PC 2

#define MACHINE UNIX

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#if machine==UNIX
# include <malloc.h>
#elif machine==PC
# include <alloc.h>
# include <mem.h>
#endif

#define NB_IMPL 5
#define LEN_IDF 10
#define AFF_OK 1
#define AFF_BAD 2
#define AFF_CONT 3

typedef enum { NON, OUI } BOOL;

typedef enum
{	RIEN,
	AJOUTER,
	CHERCHER,
	SUPPRIMER,
	LISTER,
	ROUTE,
	FICHIER,
	CIRCULAIRE,
	FIN
} OPERATION;

typedef unsigned short int tNumero;
typedef char tIdf[LEN_IDF];

typedef struct
{	unsigned int utilise : 1;
}tEtat;

typedef struct sLien
{	tNumero implique[NB_IMPL];
	int nb_impl;
	struct sLien *droite;
}tLien;

typedef struct
{ tNumero numero;
	tIdf org;
	tLien *lien;
	tEtat etat;
}tRegle;

typedef struct sElement
{ struct sElement *pred;
	struct sElement *suiv;
	tRegle regle;
}tElement;

tNumero nb_racine=0;
BOOL ref_circ=OUI;
BOOL DEBUG=NON;

/*############################################################################
############################################################################*/
BOOL LireRegle(char *chaine, tIdf idf, tIdf impl)
{
	char temp[80],*ptc,*pti;
	char *separateur=" \t\r\n";

	if(chaine==NULL)
	{	printf("Regle ? ");
		if(*(ptc=gets(temp))==0x00)		return NON;
	}
	else	ptc=chaine;
	for(pti=idf; strchr(separateur, *ptc)==NULL; ptc++, pti++)
	{	*pti=*ptc; }
	*pti=0x00;
	for( ; strchr(separateur, *ptc)!=NULL; ptc++)
	{ if(*ptc==0x00)	return NON; }
	for(pti=impl; strchr(separateur, *ptc)==NULL; ptc++, pti++)
	{ *pti=*ptc; }
	*pti=0x00;

	return OUI;
}

/*############################################################################
############################################################################*/
void AfficherRegle(const tRegle *regle)
{
	tLien *ptlien;
	int i;

	printf("Regle(%d) : %s -> ",regle->numero, regle->org);
	for(ptlien=regle->lien; ptlien; ptlien=ptlien->droite)
	{	for(i=0; i<ptlien->nb_impl; i++)
		{	printf("[%d] ",ptlien->implique[i]);
		}
		printf("- ");
	}
	printf("\n");
}

/*############################################################################
############################################################################*/
tElement *ChercherRegle(tElement *tete, tNumero num)
{
	tElement *pte;

	for(pte=tete; pte; pte=pte->suiv)
	{ if(pte->regle.numero == num)		return pte;
	}
	return pte;
}

/*############################################################################
############################################################################*/
tLien *CreerLien(void)
{
	tLien *ptl;
	if((ptl=malloc(sizeof(tLien))) == NULL)
	{	printf("Erreur allocation memoire\n");
		return NULL;
	}
	else
	{ ptl->droite=NULL;
		ptl->nb_impl=0;
		return ptl;
	}
}

/*############################################################################
############################################################################*/
BOOL Lier(tElement *tete, tNumero org, tNumero impl)
{
	tElement *pte;
	tLien *ptl;
	int i;

	if((pte=ChercherRegle(tete, org)) != NULL)
	{ if(pte->regle.lien == NULL)
		{	if((ptl=pte->regle.lien=CreerLien()) == NULL)		return NON;
		}
		else
		{	for(ptl=pte->regle.lien; ; ptl=ptl->droite)
			{	for(i=0; i<ptl->nb_impl; i++)
					if(ptl->implique[i] == impl)		return OUI;
				if(ptl->droite == NULL)		break;
			}
			if(ptl->nb_impl == NB_IMPL)
			{	if((ptl->droite=CreerLien()) == NULL)		return NON;
				ptl=ptl->droite;
			}
		}
		ptl->implique[ptl->nb_impl++] = impl;
	}
	return OUI;
}

/*############################################################################
############################################################################*/
tElement *Inserer(tElement *tete, tElement *pte ,tElement *nouveau)
{
	if(tete == pte)
	{ tete->pred = nouveau;
		nouveau->suiv = tete;
		tete = nouveau;
	}
	else
	{	pte->pred->suiv = nouveau;
		nouveau->pred = pte->pred;
		nouveau->suiv = pte;
		pte->pred = nouveau;
	}

	return tete;
}

/*############################################################################
############################################################################*/
void AjouterApres(tElement *pte, tElement *nouveau)
{
	pte->suiv = nouveau;
	nouveau->pred = pte;
	nouveau->suiv = NULL;
}

/*############################################################################
############################################################################*/
tElement *Ajouter(tElement *tete, tIdf idf, tNumero *numero)
{
	tElement *nouveau;

	if((nouveau = (tElement*)malloc(sizeof(tElement))) == NULL)
	{	printf("Impossible d'ajouter un el�ement : me�moire insuffisante\n");
		return tete;
	}
	strcpy(nouveau->regle.org, idf);
	nouveau->regle.lien = NULL;
	nouveau->regle.etat.utilise = NON;

	if(tete==NULL)
	{	tete=nouveau;
		*numero = nouveau->regle.numero = nb_racine++;
		tete->pred = tete->suiv = NULL;
	}
	else
	{	tElement *pte;
		int cmp;
		for(pte=tete; ; pte=pte->suiv)
		{	cmp = strcmp(pte->regle.org, nouveau->regle.org);
			if(cmp == 0)
			{	if(DEBUG)	printf("L'element %s existe deja\n", nouveau->regle.org);
				free(nouveau);
				*numero = pte->regle.numero;
				break;
			}
			else if(cmp > 0)
			{ if(DEBUG)	printf("Inserer %s : %d\n", idf, nb_racine);
				*numero = nouveau->regle.numero = nb_racine++;
				tete = Inserer(tete, pte, nouveau);
				break;
			}
			if(pte->suiv == NULL)
			{ if(DEBUG)	printf("AjouterApres %s : %d\n", idf, nb_racine);
				*numero = nouveau->regle.numero = nb_racine++;
				AjouterApres(pte, nouveau);
				break;
			}
		}
	}

	return tete;
}

/*############################################################################
############################################################################*/
void SupprimerLien(tLien *ptl)
{
	if(ptl->droite)		SupprimerLien(ptl->droite);
	else	free(ptl);
}

/*############################################################################
############################################################################*/
tElement *Supprimer(tElement *tete, tRegle *regle)
{
	tElement *pte;

	for(pte=tete; pte; pte=pte->suiv)
	{	if(strcmp(regle->org, pte->regle.org) == 0)
		{	if(pte==tete)
			{	tete=tete->suiv;
				tete->pred=NULL;
			}
			else
			{	pte->pred->suiv = pte->suiv;
				if(pte->suiv)		pte->suiv->pred = pte->pred;
			}
			if(pte->regle.lien)		SupprimerLien(pte->regle.lien);
			free(pte);
			printf("%s supprime\n", regle->org);
			return tete;
		}
	}
	printf("%s non trouve\n", regle->org);

	return tete;
}

/*############################################################################
############################################################################*/
tElement *Chercher(tElement *tete, tIdf idf)
{
	tElement *pte;
	for(pte=tete; pte; pte=pte->suiv)
	{	if(strcmp(idf, pte->regle.org) == 0)		return pte;
	}
	return NULL;
}

/*############################################################################
############################################################################*/
void Terminer(tElement *tete)
{
	tElement *pte,*suivant;

	if((pte=tete)==NULL)	return;
	do
	{	suivant=pte->suiv;
		if(DEBUG)	printf("Supprimer : %s\n",pte->regle.org);
		if(pte->regle.lien)		SupprimerLien(pte->regle.lien);
		free(pte);
		pte=suivant;
	}while(pte);
}

/*############################################################################
############################################################################*/
void Lister(tElement *tete)
{
	tElement *pte;
	for(pte=tete; pte; pte=pte->suiv)
	{	AfficherRegle(&(pte->regle));
	}
}

/*############################################################################
############################################################################*/
void AfficherRoute(tIdf org, int aff)
{
	static char tab[80];

	if(tab[0]==0x00)	sprintf(tab,"%s",org);
	else	if(org)	sprintf(tab+strlen(tab),"-> %s",org);

	switch(aff)
	{ /*===============*/
		case AFF_OK :
		{	printf("  #-Route : %s\n",tab);
		}break;
		/*===============*/
		case AFF_BAD :
		{	char *ptc,*inter;
			for(ptc=inter=tab; ; )
			{	if(ptc==0x00)		break;
				if((inter=strchr(inter,'-')) == NULL)
				{	*ptc=0x00;
					break;
				}
				ptc=inter;
				inter++;
			}
		}break;
		/*===============*/
		case AFF_CONT :		break;
		/*===============*/
	}
}

/*############################################################################
############################################################################*/
void ChercherRoute(tElement *tete, tIdf org, tIdf dest)
{
	static tElement *spte=NULL;
	tElement *pte,*npte;
	tLien *ptl;
	int i;

	if(spte->regle.org != org)		pte=Chercher(tete, org);
	else	pte=spte;
	if(pte==NULL)
	{	printf("La regle %s n'existe pas\n",org);
		return;
	}
	if(Chercher(tete,dest)==NULL)
	{	printf("La regle %s n'existe pas\n",dest);
		return;
	}

	pte->regle.etat.utilise = OUI;
	if(pte!=NULL)
	{	for(ptl=pte->regle.lien; ptl; ptl=ptl->droite)
		{	for(i=0; i<ptl->nb_impl; i++)
			{	spte = npte = ChercherRegle(tete, ptl->implique[i]);
				if(npte->regle.etat.utilise)
				{	if(ref_circ)		printf("  #--Reference circulaire : ");
					AfficherRoute(org,AFF_CONT);
					if(ref_circ)		AfficherRoute(npte->regle.org,AFF_OK);
					else		AfficherRoute(npte->regle.org,AFF_CONT);
					AfficherRoute(NULL,AFF_BAD);
					AfficherRoute(NULL,AFF_BAD);
				}
				else
				{	npte->regle.etat.utilise = OUI;
					if(strcmp(dest, npte->regle.org)==0)
					{	AfficherRoute(org,AFF_CONT);
						AfficherRoute(dest,AFF_OK);
						AfficherRoute(NULL,AFF_BAD);
						AfficherRoute(NULL,AFF_BAD);
					}
					else
					{	if(npte->regle.lien)
						{	AfficherRoute(org,AFF_CONT);
							ChercherRoute(tete, npte->regle.org, dest);
							AfficherRoute(NULL,AFF_BAD);
						}
						else		AfficherRoute(dest,AFF_BAD);
					}
					npte->regle.etat.utilise = NON;
				}
			}
		}
	}
	pte->regle.etat.utilise = NON;
}

/*############################################################################
############################################################################*/
void main(void)
{
	tElement *tete;
	OPERATION op;
	char cmd[80];
	int len;

	tete=NULL;
	for(op=RIEN; op!=FIN; )
	{ if(op==RIEN)
		{	fflush(stdin);	printf("--> ");		gets(cmd);
			if((len=strlen(cmd)) == 0)		continue;
			if(strncmp(cmd, "inserer", len) == 0)					op=AJOUTER;
			else if(strncmp(cmd, "supprimer", len) == 0)	op=SUPPRIMER;
			else if(strncmp(cmd, "lister", len) == 0)			op=LISTER;
			else if(strncmp(cmd, "chercher", len) == 0)		op=CHERCHER;
			else if(strncmp(cmd, "route", len) == 0)			op=ROUTE;
			else if(strncmp(cmd, "fichier", len) == 0)		op=FICHIER;
			else if(strncmp(cmd, "circulaire", len) == 0)	op=CIRCULAIRE;
			else if(strncmp(cmd, "terminer", len) == 0)		op=FIN;
			else printf("Commande %s inconnue\n",cmd);
		}
		switch(op)
		{ /*==============*/
			case AJOUTER :
			{	tIdf orig,impli;
				tNumero num_orig,num_impli;
				if(LireRegle(NULL,orig,impli))
				{ printf("Ajouter %s -> %s\n",orig,impli);
					tete = Ajouter(tete, orig, &num_orig);
					tete = Ajouter(tete, impli, &num_impli);
					Lier(tete, num_orig, num_impli);
				}
				else	op=RIEN;
			}break;
			/*==============*/
			case FICHIER :
			{ FILE *pf;
				tIdf orig,impli;
				char fichier[80],*ptc,temp[80];
				tNumero num_orig,num_impli;

				printf("Nom du fichier ? ");
				if(*gets(fichier)==0x00)
				{	op=RIEN;
					break;
				}
				if((pf=fopen(fichier,"rt")) == NULL)
				{	printf("Fichier %s introuvable\n",fichier);
					op=RIEN;
					break;
				}
				for( ; ;)
				{	ptc=fgets(temp,80,pf);
					if(ptc==NULL)		break;
					if(LireRegle(temp,orig,impli))
					{ printf("Ajouter %s -> %s\n",orig,impli);
						tete = Ajouter(tete, orig, &num_orig);
						tete = Ajouter(tete, impli, &num_impli);
						Lier(tete, num_orig, num_impli);
					}
				}
				fclose(pf);
				op=RIEN;
			}break;
			/*==============*/
			case CHERCHER :
			{	tIdf org;
				tElement *pte;
				printf("Chercher nom ? ");
				if(*gets(org) == 0x00)	op=RIEN;
				else
				{	if((pte=Chercher(tete, org)) != NULL)
						AfficherRegle(&(pte->regle));
					else	printf("%s n'existe pas\n", org);
				}
			}break;
			/*==============*/
			case LISTER :
			{	if(tete)		Lister(tete);
				else	printf("La liste est vide\n");
				op=RIEN;
			}break;
			/*==============*/
			case SUPPRIMER :
			{	tRegle regle;
				printf("Supprimer : nom ? ");
				if(*gets(regle.org) == 0x00)	op=RIEN;
				else		tete=Supprimer(tete, &regle);
			}break;
			/*==============*/
			case ROUTE :
			{ tIdf org,impl;
				if(LireRegle(NULL,org,impl))
				{	ChercherRoute(tete,org,impl);
				}
				else	op=RIEN;
			}break;
			/*==============*/
			case CIRCULAIRE :
				ref_circ = !ref_circ;
				op=RIEN;
			break;
			/*==============*/
			case FIN :
				Terminer(tete);
				tete=NULL;
			break;
			/*==============*/
		}
	}
}
