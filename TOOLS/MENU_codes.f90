      subroutine MENU_gaussian
!***********************************************************************
!     Date last modified: June 10, 2004                                *
!     Author: R. A. Poirier                                            *
!     Description: Set parameters for Gausian execution.               *
!***********************************************************************
! Modules:
      USE program_parser
      USE program_codes
      USE menu_gets
      USE type_elements

      implicit none
!
! Local scalars:
      integer :: Ipos,lenstr,leninp,lenout,READ_unit
      logical done,lrun,Lerror
      character(len=138) :: MUNgauss_in,MUNgauss_out
!
! Begin:
      call PRG_manager ('enter', 'MENU_gaussian', 'UTILITY')
      done=.false.
      lrun=.false.
!
! Menu:
      do while (.not.done)
      call new_token ('Gaussian:')
!
      if(token(1:4).eq.'HELP')then
         write(uniout,'(a)') &
      ' Command Gaussian:', &
      '  Purpose: Set Gaussian parameters ', &
      '  Syntax:', &
      '  GAUssian', &
      '  INput_file =  <string>: Input file name (default=Gaussian.com)', &
      '  OUtput_file =  <string>: Output file name (default=Gaussian.log)', &
      '  THeory_basis or Level_basis=  <string>: Theory/basis set (default=HF/6-31G(d))', &
      '  ROUte =  <string>: Gaussian computation (default='')', &
      '  end'
!
      else if(token(1:2).EQ.'IN' )then
        call GET_value (Gaussian%file_input, lenstr)
        MUNgauss_in(1:)=Gaussian%file_input(1:lenstr)//'_ext.dat'
        MUNgauss_out(1:)=Gaussian%file_input(1:lenstr)//'_ext.lis'
        Ipos=index(Gaussian%file_input, '.COM')
        if(Ipos.gt.0)then
          Gaussian%file_input(Ipos:)='.com'
        else
          Gaussian%file_input(lenstr+1:)='.com'
        end if

      else if(token(1:2).EQ.'OU' )then
        call GET_value (Gaussian%file_output, lenstr)
        Ipos=index(Gaussian%file_output, '.LOG')
        if(Ipos.gt.0)then
          Gaussian%file_output(Ipos:)='.log'
        else
          Gaussian%file_output(lenstr+1:)='.log'
        end if

      else if(token(1:2).EQ.'TH'.or.token(1:2).EQ.'LE')then
        call GET_value (Gaussian%theory_basis, lenstr)

      else if(token(1:3).EQ.'ROU')then
        call GET_value (Gaussian%route, lenstr)

      else
        call MENU_end (done)

      end if
!
      end do !(.not.done)

      call BLD_Gaussian_input
      call PRG_Gaussian_set
      leninp=len_trim(Gaussian%file_input)
      lenout=len_trim(Gaussian%file_output)
      call EXEPRG ('g09', Gaussian%file_input, Gaussian%file_output, .true.)

      lenstr=len_trim(MUNgauss_out)
      write(6,*)MUNgauss_out(1:lenstr)
      call STREAM (MUNgauss_in)
      UNITin=curuni
!
! End of routine MENU_gaussian
      call PRG_manager ('exit', 'MENU_gaussian', 'UTILITY')
      return
      end subroutine MENU_gaussian
      subroutine MENU_MUNgauss
!***********************************************************************
!     Date last modified: June 10, 2004                                *
!     Author: R. A. Poirier                                            *
!     Description: Set parameters for Gausian execution.               *
!***********************************************************************
! Modules:
      USE program_parser
      USE program_codes
      USE menu_gets
      USE type_elements

      implicit none
!
! Local scalars:
      integer :: Ipos,lenstr
      logical done,lrun
!
! Begin:
      call PRG_manager ('enter', 'MENU_MUNgauss', 'UTILITY')
      done=.false.
      lrun=.false.
!
! Menu:
      do while (.not.done)
      call new_token ('MUNgauss:')
!
      if(token(1:4).eq.'HELP')then
         write(uniout,'(a)') &
      ' Command MUNgauss:', &
      '  Purpose: Set MUNgauss parameters ', &
      '  Syntax:', &
      '  GAUssian', &
      '  INput_file =  <string>: Input file name (default=MUNgauss.dat)', &
      '  OUtput_file =  <string>: Output file name (default=MUNgauss.lis)', &
      '  THeory_basis or Level_basis=  <string>: Theory/basis set (default=HF/6-31G(d))', &
      '  end'
!
      else if(token(1:2).EQ.'IN' )then
        call GET_value (MUNgauss%file_input, lenstr)
        Ipos=index(MUNgauss%file_input, '.DAT')
        if(Ipos.gt.0)then
          MUNgauss%file_input(Ipos:)='.dat'
        else
          MUNgauss%file_input(lenstr+1:)='.dat'
        end if

      else if(token(1:2).EQ.'OU' )then
        call GET_value (MUNgauss%file_output, lenstr)
        Ipos=index(MUNgauss%file_output, '.LIS')
        if(Ipos.gt.0)then
          MUNgauss%file_output(Ipos:)='.lis'
        else
          MUNgauss%file_output(lenstr+1:)='.lis'
        end if

      else if(token(1:2).EQ.'TH'.or.token(1:2).EQ.'LE')then
        call GET_value (MUNgauss%theory_basis, lenstr)

      else
        call MENU_end (done)

      end if
!
      end do !(.not.done)
!
! End of routine MENU_MUNgauss
      call PRG_manager ('exit', 'MENU_MUNgauss', 'UTILITY')
      return
      end subroutine MENU_MUNgauss
      subroutine PRG_Gaussian_set
!*****************************************************************************************************************
!     Date last modified: July 17, 2002                                                             Version 1.2  *
!     Author: R.A. Poirier                                                                                       *
!     Desciption: Dump archive to disk                                                                           *
!*****************************************************************************************************************
! Modules:
      USE program_codes
      USE program_files

      implicit none
!
! Input scalars:

! Local scalars:
      integer :: retcod,system
!
! Begin:
      call PRG_manager ('enter', 'PRG_Gaussian_set', 'UTILITY')

      write(6,*)Gaussian%Version
      select case (Gaussian%Version)
      case ('g03')
      retcod=system('export g09root=/usr/local/gaussian')
      retcod=system('export GAUSS_SCRDIR=/globalscratch/$USER/')
      retcod=system('. $g09root/g09/bsd/g09.profile')
      case default
        write(UNIout,*)'PRG_Gaussian_set> Only Gaussian 03 available'
        write(UNIout,*)'Must modify the defaults and routine PRG_Gaussian_set'
        call PRG_stop ('PRG_Gaussian_set> Only Gaussian 03 available')
      end select
!
! end of routine  PRG_Gaussian_set
      call PRG_manager ('exit', 'PRG_Gaussian_set', 'UTILITY')
      return
      end
      subroutine BLD_Gaussian_input
!****************************************************************************************
!     Date last modified: July 17, 2002                                    Version 1.2  *
!     Author: R.A. Poirier                                                              *
!     Desciption: Dump archive to disk                                                  *
!****************************************************************************************
! Modules:
      USE program_codes
      USE program_files
      USE program_constants
!     USE OPT_defaults
      USE type_molecule
      USE type_basis_set

      implicit none
!
! Input scalars:

! local scalars:
      character(len=132) ligne,string
      integer lenobj,prim,lenfrm,lenlin
!
! Local scalars:
      integer B1,B2,F1,F2,first,lenstr
      integer fillen,iostat,unitO,unit
      logical :: lexist,lerror
      character(len=MAX_string) filnam,totnam,outnam
      character(len=MAX_string) formO
      character*(20) status
      logical ERROR,lopen,eof
      integer lrun
!
! Begin:
      call PRG_manager ('enter', 'BLD_Gaussian_input', 'UTILITY')
!
      filnam=Gaussian%file_input
      outnam=Gaussian%file_output
      totnam = BLD_FILE_name (filnam)
!
! Check if formatted file exits:
      inquire (file=totnam, exist=Lexist, opened=Lopen, number=unitO, form=formO)
!
! If the file already exist read until end_of_file found
      status ='NEW     '
      if(Lexist)then
        status ='OLD     '
      end if
!
      call GET_unit ('GAU_INPUT', unit, Lerror)
      write(UNIout,*)'Gaussian input file connected to unit ',unit
      PRG_file(unit)%status=status
      PRG_file(unit)%name=filnam
      call FILE_open (unit)
      PRG_file(unit)%status='OLD '

      if(Lexist)then
        write(UNIout,*)'ERROR> Gaussian file already exist - will not overwrite'
        write(UNIout,'(2a)')'File name: ',filnam
        call PRG_stop ('Gaussian file already exist - will not overwrite')
      end if
!
      call STR_size (BASIS%name, B1, B2)
!
! For TS
!     write(unit,'(3a)')'#N ',Gaussian%theory_basis,'OPT=(TS,CalcFC) FREQ'
! For GS
!       write(unit,'(3a)')'#N ',Gaussian%theory_basis,'OPT FREQ'
      write(unit,'(4a)')'#N ',trim(Gaussian%theory_basis),' ',trim(Gaussian%route)
!
!     write(unit,'(3a)')'#N ',Gaussian%theory_basis,'OPT FREQ PUNCH=DERIVATIVES'
!     write(unit,'(3a)')'#N ',Gaussian%theory_basis,'FREQ PUNCH=DERIVATIVES'
      write(unit,*)
      write(unit,'(a)')MOL_TITLE(1:len_trim(MOL_title))
      write(unit,*)
      write(unit,'(2i2)')Molecule%CHARGE,multiplicity
!
      IF(MOLINP(1:4).EQ.'CART')then ! Cartesian coordinates
!
      else if(MOLINP(1:4).EQ.'FREE')then
        call CLEAN_Zmatrix
        call PRT_FZMATRIX (unit)
        write(unit,*)
        call PRT_FZVALUES (unit, 1)
        write(unit,*)
      end if ! MOLINP

      call XYZ_output(unit)
      write(unit,*)

      close (unit)
!
! end of routine  BLD_Gaussian_input
      call PRG_manager ('exit', 'BLD_Gaussian_input', 'UTILITY')
      return
      end
