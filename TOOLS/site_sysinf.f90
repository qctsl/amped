      subroutine sysinf(env_var_value, env_var_name, env_var_length , ret_code)
!************************************************************************
!     Date last modified:                                               *
!     Author: Ibrahim Awad                                              *
!     Description: this routine gets strings containing system relevant *
!                 information, i.e. USER, HOME, PATH, SHELL, TERM, CWD  *
!                 (Current Working Directory ), Date, ...etc            *
!************************************************************************
! Modules:

      implicit none

      character (len=*), intent(out) :: env_var_value
      integer, intent(out) :: ret_code, env_var_length
      character (len=*), intent(in) :: env_var_name
            
      ret_code = 0
      env_var_length = 0
      env_var_value = ""

      select case (TRIM(env_var_name))
          case ("USER","user")
              CALL get_environment_variable("USER",env_var_value)
          case ("HOME","home")
              CALL get_environment_variable("HOME",env_var_value)
          case ("PATH","path")
              CALL get_environment_variable("PATH",env_var_value)
          case ("SHELL","shell")
              CALL get_environment_variable("SHELL",env_var_value)
          case ("TERM","term")
              CALL get_environment_variable("TERM",env_var_value)
          case ("HOSTNAME","hostname")
              ret_code = hostnm(env_var_value)
          case ("DATE","date")
              call fdate(env_var_value)
          case ("CWD","cwd")
              ret_code = getcwd(env_var_value)
!             env_var_length=len_trim(env_var_value)
          case ("PID","pid")
              write (env_var_value, "(I0)") getpid()
          case ("UID","uid")
              write (env_var_value, "(I0)") getuid()
          case ("GID","gid")
              write (env_var_value, "(I0)") getgid()
          case default
              write(6,*) "Error: env_var_name is not available ", env_var_name
              ret_code = 1          
      end select              
      env_var_length=len_trim(env_var_value)
      return
      end subroutine sysinf
