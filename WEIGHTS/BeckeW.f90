      subroutine BeckeWs (Apoints, NApoint, Katom, Bweights)
!*******************************************************************************
!     Date last modified: June 25th, 2001 Version 2.0                          *
!     Author: Aisha / R.A. Poirier                                             *
!     Desciption:  Gets the Becke weights for all the angular points in        *
!                  Apoints for atom Katom.                                     *
!*******************************************************************************
! Modules:
      USE program_constants
      USE type_elements
      USE type_molecule
      USE module_grid_points
      USE type_Weights

      implicit none

! Input scalars:
      integer, intent (in) :: Katom,NApoint

! Input arrays:
      type(type_grid_points),dimension(NApoint) :: Apoints

! Output arrays:
      double precision, dimension(NApoint) :: Bweights

! Local scalars:
      integer   :: Iatom,Jatom,IApoint
      double precision :: Pn,a_ij,u_ij,r_i,r_j,R_ij
      double precision :: f1,f2,f3,mu_ij,Skij,Skji
      double precision :: Rx,Ry,Rz

! Local arrays:
       double precision, dimension(:), allocatable :: Pr

 ! Begin:
      allocate(Pr(Natoms))
      Bweights(1:NApoint)=0.0D0
 
      do IApoint=1,NApoint
        Pr(1:Natoms)=1.0D0
        Pn=0.0D0
        Rx=Apoints(IApoint)%X
        Ry=Apoints(IApoint)%Y
        Rz=Apoints(IApoint)%Z
        do Iatom=1,Natoms
          if(CARTESIAN(Iatom)%Atomic_number.le.0)cycle
          r_i=dsqrt((Rx-CARTESIAN(Iatom)%X)**2+(Ry-CARTESIAN(Iatom)%Y)**2+(Rz-CARTESIAN(Iatom)%Z)**2)
          do Jatom=Iatom+1,Natoms
            if(CARTESIAN(Jatom)%Atomic_number.le.0)cycle
!           if(Jatom.eq.Iatom)cycle
            r_j=dsqrt((Rx-CARTESIAN(Jatom)%X)**2+(Ry-CARTESIAN(Jatom)%Y)**2+(Rz-CARTESIAN(Jatom)%Z)**2)
            R_ij=DISMAT(Iatom,Jatom)
            mu_ij=(r_i-r_j)/R_ij
            u_ij=(XI(Iatom,Jatom)-1.0D0)/(XI(Iatom,Jatom)+1.0D0)
            a_ij=u_ij/(u_ij**2-1.0D0)
            mu_ij=mu_ij+a_ij*(1.0D0-mu_ij**2)
!
! Becke function
            f1=1.50D0*mu_ij-0.50D0*mu_ij**3
            f2=1.50D0*f1-0.50D0*f1**3
            f3=1.50D0*f2-0.50D0*f2**3
            Skij=0.50D0*(1.0D0-f3)
            Skji=0.50D0*(1.0D0+f3)

            Pr(Iatom)=Pr(Iatom)*Skij
            Pr(Jatom)=Pr(Jatom)*Skji
          end do ! Jatom loop
          Pn=Pn+Pr(Iatom)
        end do! Iatom loop
        if(Pn.ne.ZERO)Bweights(IApoint)=Pr(Katom)/Pn
      end do ! IApoint loop
      deallocate(Pr)
      return
      end subroutine BeckeWs
      subroutine BeckeW1 (Rx, Ry, Rz, Katom, BWeight)
!**************************************************************************
!     Date last modified: September 9, 2010                               *
!     Author: R.A. Poirier                                                *
!     Desciption: Compute the Becke weight for a single grid point        *
!**************************************************************************
! Modules:
      USE program_constants
      USE type_elements
      USE type_molecule
      USE module_grid_points
      USE type_Weights

      implicit none

! Input scalars:
      integer, intent(in) :: Katom
      double precision, intent(in) :: Rx,Ry,Rz

! Input arrays:

! Output scalar:
      double precision,intent(out) :: BWeight

! Local scalars:
      integer   :: Iatom,Jatom
      double precision :: Pn,a_ij,u_ij,r_i,r_j,R_ij
      double precision :: f1,f2,f3,mu_ij,Skij,Skji

! Local arrays:
       double precision, dimension(:), allocatable :: Pr

 ! Begin:
      allocate(Pr(Natoms))
      BWeight=0.0D0
 
        Pr(1:Natoms)=1.0D0
        Pn=0.0D0
        do Iatom=1,Natoms
          if(CARTESIAN(Iatom)%Atomic_number.le.0)cycle
          r_i=dsqrt((Rx-CARTESIAN(Iatom)%X)**2+(Ry-CARTESIAN(Iatom)%Y)**2+(Rz-CARTESIAN(Iatom)%Z)**2)
          do Jatom=Iatom+1,Natoms
            if(CARTESIAN(Jatom)%Atomic_number.le.0)cycle
!           if(Jatom.eq.Iatom)cycle
            r_j=dsqrt((Rx-CARTESIAN(Jatom)%X)**2+(Ry-CARTESIAN(Jatom)%Y)**2+(Rz-CARTESIAN(Jatom)%Z)**2)
            R_ij=DISMAT(Iatom,Jatom)
            mu_ij=(r_i-r_j)/R_ij
            u_ij=(XI(Iatom,Jatom)-1.0D0)/(XI(Iatom,Jatom)+1.0D0)
            a_ij=u_ij/(u_ij**2-1.0D0)
            mu_ij=mu_ij+a_ij*(1.0D0-mu_ij**2)
! Becke function
            f1=1.50D0*mu_ij-0.50D0*mu_ij**3
            f2=1.50D0*f1-0.50D0*f1**3
            f3=1.50D0*f2-0.50D0*f2**3
            Skij=0.50D0*(1.0D0-f3)
            Skji=0.50D0*(1.0D0+f3)

            Pr(Iatom)=Pr(Iatom)*Skij
            Pr(Jatom)=Pr(Jatom)*Skji
          end do ! Jatom loop
          Pn=Pn+Pr(Iatom)
        end do! Iatom loop
        if(Pn.ne.ZERO)BWeight=Pr(Katom)/Pn
      deallocate(Pr)

      return
      end subroutine BeckeW1
      subroutine BLD_BeckeW_XI
!************************************************************************************
!     Date last modified Oct. 2, 2009                                               *
!     Author: R.A. Poirier                                                          *
!     Description:                                                                  *
!************************************************************************************
! Modules:
      USE program_files
      USE type_molecule
      USE type_elements
      USE type_Weights

      implicit none

! Local scalars:
      integer :: Iatom,Jatom,Z_numI,Z_numJ
      double precision :: BSRI,BSRJ
      logical :: LWarning

      if(.not.allocated(XI))then
      allocate(XI(Natoms,Natoms))

      LWarning=.false.
      XI(1:Natoms,1:Natoms) = 1.0D0
      if(XI_type.gt.0)then
      do Iatom=1,Natoms
        Z_numI=CARTESIAN(Iatom)%Atomic_number
        if(Z_numI.le.0) cycle
        BSRI=BS_RADII(Z_numI)
        if(XI_type.eq.1.and.Z_numI.eq.1)BSRI=0.35 ! To keep Becke happy
        do Jatom=1,Natoms
          Z_numJ=CARTESIAN(Jatom)%Atomic_number
          if(Z_numJ.le.0) cycle
          BSRJ=BS_RADII(Z_numJ)
          if(XI_type.eq.1.and.Z_numJ.eq.1)BSRJ=0.35 ! To keep Becke happy
          XI(Iatom,Jatom)=BSRI/BSRJ
          if(XI(Iatom,Jatom).gt.2.4)then
            XI(Iatom,Jatom)=2.4
            LWarning=.true.
          else if(XI(Iatom,Jatom).lt.0.417)then
            LWarning=.true.
            XI(Iatom,Jatom)=0.417
          end if
        end do ! Jatom
      end do ! Iatom
      end if ! XI_type.gt.0

      end if ! .not.allocated
      if(LWarning)then
        !write(UNIout,'(a)')'WARNING> some ratio of Bragg-Slater radii set to 2.4 or 0.417'
      end if
      return
      end subroutine BLD_BeckeW_XI
      subroutine BeckeWdels (XVal, YVal, ZVal, Iatom, BWeight, DEL_W, DEL2_W)
!**************************************************************************
!     Date last modified: April 3, 2014                                   *
!     Author: P.L Warburton                                               *
!     Desciption: Compute the Becke weight and its analytical derivatives *
!     for all all atoms at a single grid point.                           *
!     Currently only the weight and derivatives for Iatom are returned    *
!**************************************************************************
! Modules:
      USE program_constants
      USE type_elements
      USE type_molecule
      USE module_grid_points
      USE type_Weights

      implicit none

! Input scalars:
      integer :: Iatom
      double precision :: XVal,YVal,ZVal

! Input arrays:

! Output scalar:
      double precision :: BWeight
      
! Output arrays:
      double precision :: DEL_W(3),DEL2_W(3,3)

! Local scalars:
      integer   :: Qatom,Ratom,c1,c2,Satom,QRcount,RScount
      double precision :: R_QR,muiQR,aQR,uQR,dnfactor1,dnfactor2,omdivs,omdivss
      double precision :: sumomega,firstsum,shhh,dshhh,d2shhh,doublesum
      double precision :: h,h2,h3,dh,dh2,dh3,d2h,d2h2,d2h3
! Local arrays:
       double precision, dimension(:), allocatable :: omega,ri 
       double precision, dimension(:,:), allocatable :: nuiQR,siQR,dsdnu,d2sdnu2,delom,rix
       double precision, dimension(:,:,:), allocatable :: delnu,delom2
       double precision, dimension(:,:,:,:), allocatable :: del2nu
       double precision, dimension(:), allocatable :: sumdelom
       double precision, dimension(:,:), allocatable :: sumdelom2
       double precision, dimension(:), allocatable :: BeckeWA
       double precision, dimension(:,:), allocatable :: delBWA
       double precision, dimension(:,:,:), allocatable :: del2BWA
!
! Begin:
      if(Natoms.eq.1)then
        BWeight=ONE
        DEL_W=ZERO
        DEL2_W=ZERO
        return
      end if
      allocate(rix(Natoms,3))      
      allocate(ri(Natoms))
      allocate(nuiQR(Natoms,Natoms))
      allocate(delnu(Natoms,Natoms,3))
      allocate(del2nu(Natoms,Natoms,3,3))
      allocate(siQR(Natoms,Natoms))
      allocate(dsdnu(Natoms,Natoms))
      allocate(d2sdnu2(Natoms,Natoms))
      allocate(omega(Natoms))
      allocate(BeckeWA(Natoms))
      allocate(delom(Natoms,3))
      allocate(delom2(Natoms,3,3))
      allocate(sumdelom(3))
      allocate(sumdelom2(3,3))
      allocate(delBWA(Natoms,3))
      allocate(del2BWA(Natoms,3,3))

      rix(1:Natoms,1:3)=0.0D0
      ri(1:Natoms)=0.0D0
      nuiQR(1:Natoms,1:Natoms)=0.0D0
      siQR(1:Natoms,1:Natoms)=0.0D0
! initialize all derivatives as ZERO
      dsdnu(1:Natoms,1:Natoms)=0.0D0
      d2sdnu2(1:Natoms,1:Natoms)=0.0D0
      delnu(1:Natoms,1:Natoms,1:3)=0.0D0
      del2nu(1:Natoms,1:Natoms,1:3,1:3)=0.0D0 
      delom(1:Natoms,1:3)=0.0d0
      sumdelom(1:3)=0.0d0
      delom2(1:Natoms,1:3,1:3)=0.0d0
      sumdelom2(1:3,1:3)=0.0d0
      delBWA(1:Natoms,1:3)=0.0d0
      del2BWA(1:Natoms,1:3,1:3)=0.0d0
! since omega is a product, we initialize it as 1.0D0 to allow for the first multiply
      omega(1:Natoms)=1.0D0
! Becke weights of atoms are initialized to ZERO
      BeckeWA(1:Natoms)=0.0D0
      BWeight=0.0D0
      sumomega=0.0D0
      firstsum=0.0D0
      doublesum=0.0D0
! Distance related information for each atom to point of interest
! Notice that any dummy atoms are included in the list.  We will cull dummies
! only when it's really necessary to do so.	
      do Qatom=1,Natoms
        rix(Qatom,1)=XVal-CARTESIAN(Qatom)%X  
        rix(Qatom,2)=YVal-CARTESIAN(Qatom)%Y  
        rix(Qatom,3)=ZVal-CARTESIAN(Qatom)%Z  
        ri(Qatom)=dsqrt(rix(Qatom,1)**2+rix(Qatom,2)**2+rix(Qatom,3)**2)  
        if(dabs(rix(Qatom,1)).le.1.0D-15) rix(Qatom,1)=dsign(1.0D-15,rix(Qatom,1))         
        if(dabs(rix(Qatom,2)).le.1.0D-15) rix(Qatom,2)=dsign(1.0D-15,rix(Qatom,2))         
        if(dabs(rix(Qatom,3)).le.1.0D-15) rix(Qatom,3)=dsign(1.0D-15,rix(Qatom,3))         
        if(ri(Qatom).le.1.0D-15) ri(Qatom)=1.0D-15   
      end do ! Qatom        
! XI, uQR, aQR, muiQR, nuiQR, siQR and derivatives for each atom pair Q and R
      uQR=0.0D0
      aQR=0.0D0
      muiQR=0.0D0
      do Qatom=1,Natoms
       if(CARTESIAN(Qatom)%Atomic_number.le.0)cycle
        do Ratom=1,Natoms
         if(Ratom.eq.Qatom) cycle
         if(CARTESIAN(Ratom)%Atomic_number.le.0)cycle     
         R_QR=DISMAT(Qatom,Ratom)
         uQR=(XI(Qatom,Ratom)-1.0D0)/(XI(Qatom,Ratom)+1.0D0)
         aQR=uQR/(uQR**2-1.0D0)
         muiQR=(ri(Qatom)-ri(Ratom))/R_QR
         nuiQR(Qatom,Ratom)=muiQR+aQR*(1.0D0-muiQR**2)
         dnfactor1=(1.0D0-2.0D0*aQR*muiQR)/R_QR
         dnfactor2=-2.0D0*aQR/R_QR**2
! nu derivatives with respect to cartesians
         do c1=1,3
         delnu(Qatom,Ratom,c1) = dnfactor1* &
                                  ((rix(Qatom,c1)/ri(Qatom))-(rix(Ratom,c1)/ri(Ratom)))
         do c2=1,3
         if(c1.eq.c2) then
          del2nu(Qatom,Ratom,c1,c2)=(1.0D0/ri(Qatom)-rix(Qatom,c1)**2/ri(Qatom)**3 &
                                    -1.0D0/ri(Ratom)+rix(Ratom,c1)**2/ri(Ratom)**3) &
                                    *dnfactor1
          del2nu(Qatom,Ratom,c1,c2)=del2nu(Qatom,Ratom,c1,c2)+dnfactor2 &
                                    *((rix(Qatom,c1)/ri(Qatom))-(rix(Ratom,c1)/ri(Ratom)))**2
         else 
          del2nu(Qatom,Ratom,c1,c2)=(rix(Ratom,c1)*rix(Ratom,c2)/ri(Ratom)**3 &
                                    -rix(Qatom,c1)*rix(Qatom,c2)/ri(Qatom)**3)*dnfactor1
          del2nu(Qatom,Ratom,c1,c2)=del2nu(Qatom,Ratom,c1,c2)+dnfactor2 &
                                    *((rix(Qatom,c1)/ri(Qatom))-(rix(Ratom,c1)/ri(Ratom))) &
                                    *((rix(Qatom,c2)/ri(Qatom))-(rix(Ratom,c2)/ri(Ratom)))
         end if
         end do ! c2
        end do ! c1
! s and it's derivatives for each atom pair
        h=1.5D0*nuiQR(Qatom,Ratom)-0.5D0*nuiQR(Qatom,Ratom)**3
        dh=1.5D0-1.5D0*nuiQR(Qatom,Ratom)**2    
        d2h=-3.0D0*nuiQR(Qatom,Ratom)
        h2=1.5D0*h-0.5D0*h**3
        dh2=1.5D0*dh-1.5D0*h**2*dh    
        d2h2=1.5D0*d2h-1.5D0*h**2*d2h-3.0D0*h*dh**2
        h3=1.5D0*h2-0.5D0*h2**3
        dh3=1.5D0*dh2-1.5D0*h2**2*dh2    
        d2h3=1.5D0*d2h2-1.5D0*h2**2*d2h2-3.0D0*h2*dh2**2
        shhh=0.5D0-0.5D0*h3
        dshhh=-0.5D0*dh3
        d2shhh=-0.5D0*d2h3
        siQR(Qatom,Ratom)=shhh
        dsdnu(Qatom,Ratom)=dshhh
        d2sdnu2(Qatom,Ratom)=d2shhh
! end of s and derivatives
        omega(Qatom)=omega(Qatom)*siQR(Qatom,Ratom)
       end do ! Ratom
       sumomega=sumomega+omega(Qatom)
      end do! Qatom  
! Since we have all the siQR and derivatives we need, we no longer need to store muiQR, nuiQR or distances    
      deallocate(nuiQR)    
      deallocate(rix)      
      deallocate(ri)
! Becke weight for each atom 
      do Qatom=1,Natoms
       if(CARTESIAN(Qatom)%Atomic_number.le.0) cycle
       BeckeWA(Qatom)=omega(Qatom)/sumomega
      end do ! Qatom      
! omega derivatives with respect to cartesian coordinates joint approach
        do c1=1,3
        do c2=1,3
          do Qatom=1,Natoms
          firstsum=0.0D0
          doublesum=0.0D0
          QRcount=0
          RScount=0
          if(CARTESIAN(Qatom)%Atomic_number.le.0) cycle
          do Ratom=1,Natoms
           if(CARTESIAN(Ratom)%Atomic_number.le.0) cycle
           if(Ratom.eq.Qatom) cycle
           QRcount=QRcount+1
            if(siQR(Qatom,Ratom).eq.0.0D0) cycle
            omdivs=omega(Qatom)/siQR(Qatom,Ratom)
            firstsum=firstsum+omdivs*(d2sdnu2(Qatom,Ratom)*delnu(Qatom,Ratom,c1) &
                     *delnu(Qatom,Ratom,c2)+dsdnu(Qatom,Ratom)*del2nu(Qatom,Ratom,c1,c2))
            if(c1.eq.c2) then
              delom(Qatom,c1)=delom(Qatom,c1)+omdivs*dsdnu(Qatom,Ratom)*delnu(Qatom,Ratom,c1)
            endif
            do Satom=1,Natoms
            if(Satom.eq.Qatom) cycle
            if(Satom.eq.Ratom) cycle
            RScount=RScount+1
            if(siQR(Qatom,Satom).eq.0.0D0) cycle
            omdivss=omega(Qatom)/(siQR(Qatom,Ratom)*siQR(Qatom,Satom))
            if(c1.eq.c2) then
            doublesum=doublesum+omdivss*dsdnu(Qatom,Ratom)*dsdnu(Qatom,Satom) &
                      *delnu(Qatom,Ratom,c1)*delnu(Qatom,Satom,c1)
            else
            doublesum=doublesum+0.5D0*omdivss*dsdnu(Qatom,Ratom)*dsdnu(Qatom,Satom) &
                      *(delnu(Qatom,Ratom,c1)*delnu(Qatom,Satom,c2)+delnu(Qatom,Ratom,c2) &
                      *delnu(Qatom,Satom,c1))
            end if
            end do ! S atom
            end do ! R atom
           if(c1.eq.c2) sumdelom(c1)=sumdelom(c1)+delom(Qatom,c1)
           delom2(Qatom,c1,c2)=firstsum+doublesum
           sumdelom2(c1,c2)=sumdelom2(c1,c2)+firstsum+doublesum
           end do ! Q atom 
        end do ! c2 
        end do ! c1

! Becke weight first derivatives with respect to cartesian directions
      do Qatom=1,Natoms
       if(CARTESIAN(Qatom)%Atomic_number.le.0)cycle
       do c1=1,3
         delBWA(Qatom,c1)=(delom(Qatom,c1)-BeckeWA(Qatom)*sumdelom(c1))/sumomega
       end do ! c1      
      end do ! Qatom
! Becke weight second derivatives with respect to cartesian directions
      do Qatom=1,Natoms
       if(CARTESIAN(Qatom)%Atomic_number.le.0)cycle
       do c1=1,3
        do c2=1,3
         if(c2.eq.c1) then
          del2BWA(Qatom,c1,c2)=(delom2(Qatom,c1,c2)-BeckeWA(Qatom)*sumdelom2(c1,c2) &
                                   -2.0D0*delBWA(Qatom,c1)*sumdelom(c1))/sumomega
          else
          del2BWA(Qatom,c1,c2)=(delom2(Qatom,c1,c2)-BeckeWA(Qatom)*sumdelom2(c1,c2) &
                                   -delBWA(Qatom,c1)*sumdelom(c2)-delBWA(Qatom,c2)*sumdelom(c1)) &
                                   /sumomega 
         end if
        end do ! c2
       end do ! c1      
      end do ! Qatom
! Right now we only want to return the Becke wight and derivatives for Iatom we passed in.
! In the future it may make sense to call this subroutine once and return the weights and
! derivatives for ALL the atoms for the point of interest.
      Bweight=BeckeWA(Iatom)
      do c1=1,3
      DEL_W(c1)=delBWA(Iatom,c1)
       do c2=1,3
       DEL2_W(c1,c2)=del2BWA(Iatom,c1,c2)
       end do
      end do

      deallocate(delnu)
      deallocate(del2nu)
      deallocate(siQR)
      deallocate(dsdnu)
      deallocate(d2sdnu2)
      deallocate(omega)
      deallocate(delom)
      deallocate(delom2)
      deallocate(sumdelom)
      deallocate(sumdelom2)
      deallocate(BeckeWA)
      deallocate(delBWA)
      deallocate(del2BWA)

      return

      end subroutine BeckeWdels
      subroutine BeckeWdels_allatoms (XVal, YVal, ZVal)
!**************************************************************************
!     Date last modified: April 3, 2014                                   *
!     Author: P.L Warburton                                               *
!     Desciption: Compute the Becke weight and its analytical derivatives *
!     for all all atoms at a single grid point.                           *
!     Weight and derivatives for all atoms are stored for the grid point  *
!**************************************************************************
! Modules:
      USE program_constants
      USE type_elements
      USE type_molecule
      USE module_grid_points
      USE type_Weights

      implicit none

! Input scalars:
!      integer, intent(in) :: Iatom
      double precision, intent(in) :: XVal,YVal,ZVal

! Input arrays:

! Output scalar:
!      double precision,intent(out) :: BWeight
      
! Output arrays:
!      double precision :: DEL_W(3),DEL2_W(3,3)

! Local scalars:
      integer   :: Qatom,Ratom,c1,c2,Satom,QRcount,RScount
      double precision :: R_QR,muiQR,aQR,uQR,dnfactor1,dnfactor2,omdivs,omdivss
      double precision :: sumomega,firstsum,shhh,dshhh,d2shhh,doublesum
      double precision :: h,h2,h3,dh,dh2,dh3,d2h,d2h2,d2h3
! Local arrays:
       double precision, dimension(:), allocatable :: omega,ri 
       double precision, dimension(:,:), allocatable :: nuiQR,siQR,dsdnu,d2sdnu2,delom,rix
       double precision, dimension(:,:,:), allocatable :: delnu,delom2
       double precision, dimension(:,:,:,:), allocatable :: del2nu
       double precision, dimension(:), allocatable :: sumdelom
       double precision, dimension(:,:), allocatable :: sumdelom2
!
! Begin:
!      if(Natoms.eq.1)then
!        BWeight=ONE
!        DEL_W=ZERO
!        DEL2_W=ZERO
!        return
!      end if
      allocate(rix(Natoms,3))      
      allocate(ri(Natoms))
      allocate(nuiQR(Natoms,Natoms))
      allocate(delnu(Natoms,Natoms,3))
      allocate(del2nu(Natoms,Natoms,3,3))
      allocate(siQR(Natoms,Natoms))
      allocate(dsdnu(Natoms,Natoms))
      allocate(d2sdnu2(Natoms,Natoms))
      allocate(omega(Natoms))
      if(.not.allocated(BWA)) allocate(BWA(Natoms))
      allocate(delom(Natoms,3))
      allocate(delom2(Natoms,3,3))
      allocate(sumdelom(3))
      allocate(sumdelom2(3,3))
      if(.not.allocated(dBWA))allocate(dBWA(Natoms,3))
      if(.not.allocated(d2BWA))allocate(d2BWA(Natoms,3,3))

      rix(1:Natoms,1:3)=0.0D0
      ri(1:Natoms)=0.0D0
      nuiQR(1:Natoms,1:Natoms)=0.0D0
      siQR(1:Natoms,1:Natoms)=0.0D0
! initialize all derivatives as ZERO
      dsdnu(1:Natoms,1:Natoms)=0.0D0
      d2sdnu2(1:Natoms,1:Natoms)=0.0D0
      delnu(1:Natoms,1:Natoms,1:3)=0.0D0
      del2nu(1:Natoms,1:Natoms,1:3,1:3)=0.0D0 
      delom(1:Natoms,1:3)=0.0d0
      sumdelom(1:3)=0.0d0
      delom2(1:Natoms,1:3,1:3)=0.0d0
      sumdelom2(1:3,1:3)=0.0d0
      dBWA(1:Natoms,1:3)=0.0d0
      d2BWA(1:Natoms,1:3,1:3)=0.0d0
! since omega is a product, we initialize it as 1.0D0 to allow for the first multiply
      omega(1:Natoms)=1.0D0
! Becke weights of atoms are initialized to ZERO
      BWA(1:Natoms)=0.0D0
!      BWeight=0.0D0
      sumomega=0.0D0
      firstsum=0.0D0
      doublesum=0.0D0
! Distance related information for each atom to point of interest
! Notice that any dummy atoms are included in the list.  We will cull dummies
! only when it's really necessary to do so.	
      do Qatom=1,Natoms
        rix(Qatom,1)=XVal-CARTESIAN(Qatom)%X  
        rix(Qatom,2)=YVal-CARTESIAN(Qatom)%Y  
        rix(Qatom,3)=ZVal-CARTESIAN(Qatom)%Z  
        ri(Qatom)=dsqrt(rix(Qatom,1)**2+rix(Qatom,2)**2+rix(Qatom,3)**2)  
        if(dabs(rix(Qatom,1)).le.1.0D-15) rix(Qatom,1)=dsign(1.0D-15,rix(Qatom,1))         
        if(dabs(rix(Qatom,2)).le.1.0D-15) rix(Qatom,2)=dsign(1.0D-15,rix(Qatom,2))         
        if(dabs(rix(Qatom,3)).le.1.0D-15) rix(Qatom,3)=dsign(1.0D-15,rix(Qatom,3))         
        if(ri(Qatom).le.1.0D-15) ri(Qatom)=1.0D-15   
      end do ! Qatom        
! XI, uQR, aQR, muiQR, nuiQR, siQR and derivatives for each atom pair Q and R
      uQR=0.0D0
      aQR=0.0D0
      muiQR=0.0D0
      do Qatom=1,Natoms
       if(CARTESIAN(Qatom)%Atomic_number.le.0)cycle
        do Ratom=1,Natoms
         if(Ratom.eq.Qatom) cycle
         if(CARTESIAN(Ratom)%Atomic_number.le.0)cycle     
         R_QR=DISMAT(Qatom,Ratom)
         uQR=(XI(Qatom,Ratom)-1.0D0)/(XI(Qatom,Ratom)+1.0D0)
         aQR=uQR/(uQR**2-1.0D0)
         muiQR=(ri(Qatom)-ri(Ratom))/R_QR
         nuiQR(Qatom,Ratom)=muiQR+aQR*(1.0D0-muiQR**2)
         dnfactor1=(1.0D0-2.0D0*aQR*muiQR)/R_QR
         dnfactor2=-2.0D0*aQR/R_QR**2
! nu derivatives with respect to cartesians
         do c1=1,3
         delnu(Qatom,Ratom,c1) = dnfactor1* &
                                  ((rix(Qatom,c1)/ri(Qatom))-(rix(Ratom,c1)/ri(Ratom)))
         do c2=1,3
         if(c1.eq.c2) then
          del2nu(Qatom,Ratom,c1,c2)=(1.0D0/ri(Qatom)-rix(Qatom,c1)**2/ri(Qatom)**3 &
                                    -1.0D0/ri(Ratom)+rix(Ratom,c1)**2/ri(Ratom)**3) &
                                    *dnfactor1
          del2nu(Qatom,Ratom,c1,c2)=del2nu(Qatom,Ratom,c1,c2)+dnfactor2 &
                                    *((rix(Qatom,c1)/ri(Qatom))-(rix(Ratom,c1)/ri(Ratom)))**2
         else 
          del2nu(Qatom,Ratom,c1,c2)=(rix(Ratom,c1)*rix(Ratom,c2)/ri(Ratom)**3 &
                                    -rix(Qatom,c1)*rix(Qatom,c2)/ri(Qatom)**3)*dnfactor1
          del2nu(Qatom,Ratom,c1,c2)=del2nu(Qatom,Ratom,c1,c2)+dnfactor2 &
                                    *((rix(Qatom,c1)/ri(Qatom))-(rix(Ratom,c1)/ri(Ratom))) &
                                    *((rix(Qatom,c2)/ri(Qatom))-(rix(Ratom,c2)/ri(Ratom)))
         end if
         end do ! c2
        end do ! c1
! s and it's derivatives for each atom pair
        h=1.5D0*nuiQR(Qatom,Ratom)-0.5D0*nuiQR(Qatom,Ratom)**3
        dh=1.5D0-1.5D0*nuiQR(Qatom,Ratom)**2    
        d2h=-3.0D0*nuiQR(Qatom,Ratom)
        h2=1.5D0*h-0.5D0*h**3
        dh2=1.5D0*dh-1.5D0*h**2*dh    
        d2h2=1.5D0*d2h-1.5D0*h**2*d2h-3.0D0*h*dh**2
        h3=1.5D0*h2-0.5D0*h2**3
        dh3=1.5D0*dh2-1.5D0*h2**2*dh2    
        d2h3=1.5D0*d2h2-1.5D0*h2**2*d2h2-3.0D0*h2*dh2**2
        shhh=0.5D0-0.5D0*h3
        dshhh=-0.5D0*dh3
        d2shhh=-0.5D0*d2h3
        siQR(Qatom,Ratom)=shhh
        dsdnu(Qatom,Ratom)=dshhh
        d2sdnu2(Qatom,Ratom)=d2shhh
! end of s and derivatives
        omega(Qatom)=omega(Qatom)*siQR(Qatom,Ratom)
       end do ! Ratom
       sumomega=sumomega+omega(Qatom)
      end do! Qatom  
! Since we have all the siQR and derivatives we need, we no longer need to store muiQR, nuiQR or distances    
      deallocate(nuiQR)    
      deallocate(rix)      
      deallocate(ri)
! Becke weight for each atom 
      do Qatom=1,Natoms
       if(CARTESIAN(Qatom)%Atomic_number.le.0) cycle
       BWA(Qatom)=omega(Qatom)/sumomega
      end do ! Qatom      
! omega derivatives with respect to cartesian coordinates joint approach
        do c1=1,3
        do c2=1,3
          do Qatom=1,Natoms
          firstsum=0.0D0
          doublesum=0.0D0
          QRcount=0
          RScount=0
          if(CARTESIAN(Qatom)%Atomic_number.le.0) cycle
          do Ratom=1,Natoms
           if(CARTESIAN(Ratom)%Atomic_number.le.0) cycle
           if(Ratom.eq.Qatom) cycle
           QRcount=QRcount+1
            if(siQR(Qatom,Ratom).eq.0.0D0) cycle
            omdivs=omega(Qatom)/siQR(Qatom,Ratom)
            firstsum=firstsum+omdivs*(d2sdnu2(Qatom,Ratom)*delnu(Qatom,Ratom,c1) &
                     *delnu(Qatom,Ratom,c2)+dsdnu(Qatom,Ratom)*del2nu(Qatom,Ratom,c1,c2))
            if(c1.eq.c2) then
              delom(Qatom,c1)=delom(Qatom,c1)+omdivs*dsdnu(Qatom,Ratom)*delnu(Qatom,Ratom,c1)
            endif
            do Satom=1,Natoms
            if(Satom.eq.Qatom) cycle
            if(Satom.eq.Ratom) cycle
            RScount=RScount+1
            if(siQR(Qatom,Satom).eq.0.0D0) cycle
            omdivss=omega(Qatom)/(siQR(Qatom,Ratom)*siQR(Qatom,Satom))
            if(c1.eq.c2) then
            doublesum=doublesum+omdivss*dsdnu(Qatom,Ratom)*dsdnu(Qatom,Satom) &
                      *delnu(Qatom,Ratom,c1)*delnu(Qatom,Satom,c1)
            else
            doublesum=doublesum+0.5D0*omdivss*dsdnu(Qatom,Ratom)*dsdnu(Qatom,Satom) &
                      *(delnu(Qatom,Ratom,c1)*delnu(Qatom,Satom,c2)+delnu(Qatom,Ratom,c2) &
                      *delnu(Qatom,Satom,c1))
            end if
            end do ! S atom
            end do ! R atom
           if(c1.eq.c2) sumdelom(c1)=sumdelom(c1)+delom(Qatom,c1)
           delom2(Qatom,c1,c2)=firstsum+doublesum
           sumdelom2(c1,c2)=sumdelom2(c1,c2)+firstsum+doublesum
           end do ! Q atom 
        end do ! c2 
        end do ! c1

! Becke weight first derivatives with respect to cartesian directions
      do Qatom=1,Natoms
       if(CARTESIAN(Qatom)%Atomic_number.le.0)cycle
       do c1=1,3
         dBWA(Qatom,c1)=(delom(Qatom,c1)-BWA(Qatom)*sumdelom(c1))/sumomega
       end do ! c1      
      end do ! Qatom
! Becke weight second derivatives with respect to cartesian directions
      do Qatom=1,Natoms
       if(CARTESIAN(Qatom)%Atomic_number.le.0)cycle
       do c1=1,3
        do c2=1,3
         if(c2.eq.c1) then
          d2BWA(Qatom,c1,c2)=(delom2(Qatom,c1,c2)-BWA(Qatom)*sumdelom2(c1,c2) &
                                   -2.0D0*dBWA(Qatom,c1)*sumdelom(c1))/sumomega
          else
          d2BWA(Qatom,c1,c2)=(delom2(Qatom,c1,c2)-BWA(Qatom)*sumdelom2(c1,c2) &
                                   -dBWA(Qatom,c1)*sumdelom(c2)-dBWA(Qatom,c2)*sumdelom(c1)) &
                                   /sumomega 
         end if
        end do ! c2
       end do ! c1      
      end do ! Qatom


      deallocate(delnu)
      deallocate(del2nu)
      deallocate(siQR)
      deallocate(dsdnu)
      deallocate(d2sdnu2)
      deallocate(omega)
      deallocate(delom)
      deallocate(delom2)
      deallocate(sumdelom)
      deallocate(sumdelom2)

      return

      end subroutine BeckeWdels_allatoms
      subroutine Becke_dW (XVal, YVal, ZVal, del_W, BeckeWA)
!**************************************************************************
!     Date last modified: April 3, 2014                                   *
!     Author: P.L Warburton                                               *
!     Desciption: Compute the Becke weight and its first derivative       *
!     for all all atoms at a single grid point.                           *
!     Currently only the weight and derivatives for Iatom are returned    *
!**************************************************************************
! Modules:
      USE program_constants
      USE type_elements
      USE type_molecule
      USE module_grid_points
      USE type_Weights

      implicit none

! Input scalars:
      double precision, intent(in) :: XVal,YVal,ZVal

! Local scalars:
      integer   :: Qatom,Ratom,c1,c2
      double precision :: R_QR,muiQR,aQR,uQR,dnfactor1,dnfactor2,omdivs
      double precision :: sumomega,shhh,dshhh
      double precision :: h,h2,h3,dh,dh2,dh3
!
! Local arrays:
       double precision, dimension(:), allocatable :: omega,ri 
       double precision, dimension(:,:), allocatable :: nuiQR,siQR,dsdnu,delom,rix
       double precision, dimension(:,:,:), allocatable :: delnu
       double precision, dimension(:), allocatable :: sumdelom
! Output arrays:
       double precision, dimension(Natoms,3) :: del_W
       double precision, dimension(Natoms) :: BeckeWA
!
! Begin:
!     if(.not.allocated(BeckeWA))allocate(BeckeWA(Natoms))
      if(Natoms.eq.1)then
        BeckeWA(1)=ONE
        del_W(1,1:3)=ZERO
        return
      end if

      allocate(rix(Natoms,3))      
      allocate(ri(Natoms))
      allocate(nuiQR(Natoms,Natoms))
      allocate(delnu(Natoms,Natoms,3))
      allocate(siQR(Natoms,Natoms))
      allocate(dsdnu(Natoms,Natoms))
      allocate(omega(Natoms))
      allocate(delom(Natoms,3))
      allocate(sumdelom(3))

      rix(1:Natoms,1:3)=0.0D0
      ri(1:Natoms)=0.0D0
      nuiQR(1:Natoms,1:Natoms)=0.0D0
      siQR(1:Natoms,1:Natoms)=0.0D0
! initialize all derivatives as ZERO
      dsdnu(1:Natoms,1:Natoms)=0.0D0
      delnu(1:Natoms,1:Natoms,1:3)=0.0D0
      delom(1:Natoms,1:3)=0.0d0
      sumdelom(1:3)=0.0d0
      del_W(1:Natoms,1:3)=0.0d0
! since omega is a product, we initialize it as 1.0D0 to allow for the first multiply
      omega(1:Natoms)=1.0D0
! Becke weights of atoms are initialized to ZERO
      BeckeWA(1:Natoms)=0.0D0
      sumomega=0.0D0
! Distance related information for each atom to point of interest
! Notice that any dummy atoms are included in the list.  We will cull dummies
! only when it's really necessary to do so.	
      do Qatom=1,Natoms
        rix(Qatom,1)=XVal-CARTESIAN(Qatom)%X  
        rix(Qatom,2)=YVal-CARTESIAN(Qatom)%Y  
        rix(Qatom,3)=ZVal-CARTESIAN(Qatom)%Z  
        ri(Qatom)=dsqrt(rix(Qatom,1)**2+rix(Qatom,2)**2+rix(Qatom,3)**2)  
        if(dabs(rix(Qatom,1)).le.1.0D-15) rix(Qatom,1)=dsign(1.0D-15,rix(Qatom,1))         
        if(dabs(rix(Qatom,2)).le.1.0D-15) rix(Qatom,2)=dsign(1.0D-15,rix(Qatom,2))         
        if(dabs(rix(Qatom,3)).le.1.0D-15) rix(Qatom,3)=dsign(1.0D-15,rix(Qatom,3))         
        if(ri(Qatom).le.1.0D-15) ri(Qatom)=1.0D-15   
      end do ! Qatom        
! XI, uQR, aQR, muiQR, nuiQR, siQR and derivatives for each atom pair Q and R
      uQR=0.0D0
      aQR=0.0D0
      muiQR=0.0D0
      do Qatom=1,Natoms
       if(CARTESIAN(Qatom)%Atomic_number.le.0)cycle
        do Ratom=1,Natoms
         if(Ratom.eq.Qatom) cycle
         if(CARTESIAN(Ratom)%Atomic_number.le.0)cycle     
         R_QR=DISMAT(Qatom,Ratom)
         uQR=(XI(Qatom,Ratom)-1.0D0)/(XI(Qatom,Ratom)+1.0D0)
         aQR=uQR/(uQR**2-1.0D0)
         muiQR=(ri(Qatom)-ri(Ratom))/R_QR
         nuiQR(Qatom,Ratom)=muiQR+aQR*(1.0D0-muiQR**2)
         dnfactor1=(1.0D0-2.0D0*aQR*muiQR)/R_QR
         dnfactor2=-2.0D0*aQR/R_QR**2
! nu derivatives with respect to cartesians
         do c1=1,3
         delnu(Qatom,Ratom,c1) = dnfactor1* &
                                  ((rix(Qatom,c1)/ri(Qatom))-(rix(Ratom,c1)/ri(Ratom)))
        end do ! c1
! s and it's derivatives for each atom pair
        h=1.5D0*nuiQR(Qatom,Ratom)-0.5D0*nuiQR(Qatom,Ratom)**3
        dh=1.5D0-1.5D0*nuiQR(Qatom,Ratom)**2    
        h2=1.5D0*h-0.5D0*h**3
        dh2=1.5D0*dh-1.5D0*h**2*dh    
        h3=1.5D0*h2-0.5D0*h2**3
        dh3=1.5D0*dh2-1.5D0*h2**2*dh2    
        shhh=0.5D0-0.5D0*h3
        dshhh=-0.5D0*dh3
        siQR(Qatom,Ratom)=shhh
        dsdnu(Qatom,Ratom)=dshhh
! end of s and derivatives
        omega(Qatom)=omega(Qatom)*siQR(Qatom,Ratom)
       end do ! Ratom
       sumomega=sumomega+omega(Qatom)
      end do! Qatom  
! Since we have all the siQR and derivatives we need, we no longer need to store muiQR, nuiQR or distances    
      deallocate(nuiQR)    
      deallocate(rix)      
      deallocate(ri)
! Becke weight for each atom 
      do Qatom=1,Natoms
       if(CARTESIAN(Qatom)%Atomic_number.le.0) cycle
       BeckeWA(Qatom)=omega(Qatom)/sumomega
      end do ! Qatom      
! omega derivatives with respect to cartesian coordinates joint approach
        do c1=1,3
        do c2=1,3
          do Qatom=1,Natoms
          if(CARTESIAN(Qatom)%Atomic_number.le.0) cycle
          do Ratom=1,Natoms
           if(CARTESIAN(Ratom)%Atomic_number.le.0) cycle
           if(Ratom.eq.Qatom) cycle
            if(siQR(Qatom,Ratom).eq.0.0D0) cycle
            omdivs=omega(Qatom)/siQR(Qatom,Ratom)
            if(c1.eq.c2) then
              delom(Qatom,c1)=delom(Qatom,c1)+omdivs*dsdnu(Qatom,Ratom)*delnu(Qatom,Ratom,c1)
            endif
            end do ! R atom
           if(c1.eq.c2) sumdelom(c1)=sumdelom(c1)+delom(Qatom,c1)
           end do ! Q atom 
        end do ! c2 
        end do ! c1

! Becke weight first derivatives with respect to cartesian directions
      do Qatom=1,Natoms
       if(CARTESIAN(Qatom)%Atomic_number.le.0)cycle
       do c1=1,3
         del_W(Qatom,c1)=(delom(Qatom,c1)-BeckeWA(Qatom)*sumdelom(c1))/sumomega
       end do ! c1      
      end do ! Qatom

      deallocate(delnu)
      deallocate(siQR)
      deallocate(dsdnu)
      deallocate(omega)
      deallocate(delom)
      deallocate(sumdelom)

      return

      end subroutine Becke_dW
