      subroutine Fermi_Weights(Apoints, NApoint, Katom, Nweights)
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Desciption:  Gets the Fermi-Dirac weights for all the angular points in  *
!                  Apoints for atom Katom.                                     *
!*******************************************************************************
! Modules:
      USE program_constants
      USE type_elements
      USE type_molecule
      USE module_grid_points
      USE type_Weights

      implicit none

! Input scalars:
      integer, intent (in) :: Katom, NApoint

! Input arrays:
      type(type_grid_points),dimension(NApoint) :: Apoints

! Output arrays:
      double precision, dimension(NApoint) :: Nweights

! Local scalars:
      integer   :: Iatom, Jatom, IApoint
      double precision :: TWeight,r_i,r_j,r_iN,r_jN
      double precision :: ExpFact_ij, ExpFact_ji, Skij, Skji, K_Const
      double precision :: Rx,Ry,Rz

! Local arrays:
       double precision, dimension(:), allocatable :: Pr

 ! Begin:
      allocate(Pr(Natoms))
      Nweights(1:NApoint)=0.0D0
 
      do IApoint=1,NApoint
        Pr(1:Natoms)=1.0D0
        TWeight=0.0D0
        do Iatom=1,Natoms
          if(CARTESIAN(Iatom)%Atomic_number.le.0)cycle
          r_i=dsqrt((Rx-CARTESIAN(Iatom)%X)**2+(Ry-CARTESIAN(Iatom)%Y)**2+(Rz-CARTESIAN(Iatom)%Z)**2)
          r_iN=r_i-r_aveg(CARTESIAN(Iatom)%Atomic_number)
          do Jatom=Iatom+1,Natoms
            if(CARTESIAN(Jatom)%Atomic_number.le.0)cycle
            r_j=dsqrt((Rx-CARTESIAN(Jatom)%X)**2+(Ry-CARTESIAN(Jatom)%Y)**2+(Rz-CARTESIAN(Jatom)%Z)**2)
            r_jN=r_j-r_aveg(CARTESIAN(Jatom)%Atomic_number)
!           NewWeight            
            K_Const=7.0d0
            ExpFact_ij=K_Const*(r_iN-r_jN)/(r_iN+r_jN)
            ExpFact_ji=-ExpFact_ij               
            Skij=1.0d0/(dexp(ExpFact_ij)+1)
            Skji=1.0d0/(dexp(ExpFact_ji)+1)
            Pr(Iatom)=Pr(Iatom)*Skij
            Pr(Jatom)=Pr(Jatom)*Skji
          end do ! Jatom loop
          TWeight=TWeight+Pr(Iatom)
        end do! Iatom loop

        if(TWeight.ne.ZERO) NWeights(IApoint)=Pr(Katom)/TWeight
      end do ! IApoint loop
      deallocate(Pr)
      return
      end subroutine Fermi_Weights
      subroutine Fermi_Weight(Rx, Ry, Rz, Katom, NWeight)
!**************************************************************************
!     Date last modified: September 9, 2010                               *
!     Author: Ibrahim Awad                                                *
!     Desciption: Compute the Fermi-Dirac weight for a single grid point  *
!**************************************************************************
! Modules:
      USE program_constants
      USE type_elements
      USE type_molecule
      USE module_grid_points
      USE type_Weights

      implicit none

! Input scalars:
      integer, intent(in) :: Katom
      double precision, intent(in) :: Rx,Ry,Rz

! Output scalar:
      double precision,intent(out) :: NWeight

! Local scalars:
      integer   :: Iatom,Jatom
      double precision :: TWeight,r_i,r_j,r_iN,r_jN
      double precision :: ExpFact_ij, ExpFact_ji, Skij, Skji, K_Const

! Local arrays:
       double precision, dimension(:), allocatable :: Pr

 ! Begin:
      allocate(Pr(Natoms))
!     r_aveg=r_aveg*1.889725989
      NWeight=0.0D0
 
        Pr(1:Natoms)=1.0D0
        TWeight=0.0D0
        do Iatom=1,Natoms
          if(CARTESIAN(Iatom)%Atomic_number.le.0)cycle
          r_i=dsqrt((Rx-CARTESIAN(Iatom)%X)**2+(Ry-CARTESIAN(Iatom)%Y)**2+(Rz-CARTESIAN(Iatom)%Z)**2)
          r_iN=r_i-r_aveg(CARTESIAN(Iatom)%Atomic_number)
          do Jatom=Iatom+1,Natoms
            if(CARTESIAN(Jatom)%Atomic_number.le.0)cycle
            r_j=dsqrt((Rx-CARTESIAN(Jatom)%X)**2+(Ry-CARTESIAN(Jatom)%Y)**2+(Rz-CARTESIAN(Jatom)%Z)**2)
            r_jN=r_j-r_aveg(CARTESIAN(Jatom)%Atomic_number)
!           NewWeight            
            K_Const=7.0d0
            ExpFact_ij=K_Const*(r_iN-r_jN)/(r_iN+r_jN)
            ExpFact_ji=-ExpFact_ij               
            Skij=1.0d0/(dexp(ExpFact_ij)+1)
            Skji=1.0d0/(dexp(ExpFact_ji)+1)
            Pr(Iatom)=Pr(Iatom)*Skij
            Pr(Jatom)=Pr(Jatom)*Skji
          end do ! Jatom loop
          TWeight=TWeight+Pr(Iatom)
        end do! Iatom loop

        if(TWeight.ne.ZERO) NWeight=Pr(Katom)/TWeight
      deallocate(Pr)

      return
      end subroutine Fermi_Weight
