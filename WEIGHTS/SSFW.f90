      subroutine SSFWs (Apoints, NApoint, Katom, Bweights)
!**************************************************************************
!     Date last modified: June 25th, 2001 Version 2.0  *
!     Author: Aisha
!     Desciption:
!**************************************************************************
! Modules:
      USE NI_defaults
      USE program_constants
      USE type_elements
      USE type_molecule
      USE module_grid_points
      USE GRAPH_objects
      USE type_molecule
      USE type_Weights

      implicit none

! Input scalars:
      integer, intent (in) :: Katom, NApoint

! Input arrays:
      type(type_grid_points),dimension(NApoint) :: Apoints

! Output arrays:
      double precision, dimension(NApoint) :: Bweights

! Local scalars:
      integer   :: Iatom,Jatom,IApoint
      double precision   :: Pn,a_ij,u_ij,r_i,r_j,R_ij,r_ik
      double precision   :: f1,f2,f3,mu_ij,mu_ij_a,Sk,a

! Local arrays:
      logical :: nuc_rot
      integer :: num_neighbors, Ineighbor,Jneighbor
      integer, dimension(1) :: min_loc
      double precision :: distance
      integer, dimension(:), allocatable ::neighbors,temp
      double precision, dimension(:), allocatable :: Pr
      
! Begin:
! I should include every atom with its neighbors,  
      write(6,*)'SSFW_frag'
 
      call GET_object ('GRAPH:CONVAL')
      call GET_object ('MOLECULE:ATOMIC_DISTANCES')
      distance = 8.0D0
      Ineighbor = 1
      num_neighbors = 0
      allocate(temp(Natoms))
      do Iatom = 1,Natoms
        if(CARTESIAN(Iatom)%Atomic_number .eq. 0)cycle
          if(Dismat(Katom,Iatom) .gt. distance)cycle
            temp(Ineighbor) = Iatom
            num_neighbors = num_neighbors + 1
            Ineighbor = Ineighbor + 1
      end do

      allocate(neighbors(num_neighbors))
      neighbors(1:num_neighbors) = temp(1:num_neighbors)
      allocate (Pr(Natoms))
      Pr(1:Natoms) = 0.0D0
      Bweights(1:NApoint) = 0.0D0

IApoint_loop:      do IApoint = 1,NApoint
                     Pn = 0.0D0
Ineighbor_loop:      do Ineighbor = 1, num_neighbors
                       Iatom = neighbors(Ineighbor) 
                       Pr(Iatom) = 1.0D0
                       r_ik=DSQRT((Apoints(IApoint)%X-CARTESIAN(Katom)%X)**2 + &
                                  (Apoints(IApoint)%Y-CARTESIAN(Katom)%Y)**2 + &
                                  (Apoints(IApoint)%Z-CARTESIAN(Katom)%Z)**2)
           if(connect(Katom,Iatom).eq.1 .and. r_ik.lt.0.180D0*Dismat(Katom,Iatom))then
              write(6,*)'Katom,Iatom', Katom,Iatom
              Bweights(IApoint) = 1.0D0
              cycle IApoint_loop
              cycle Ineighbor_loop
           end if
           write(6,*)'IApoint', IApoint
                       r_i=DSQRT((Apoints(IApoint)%X-CARTESIAN(Iatom)%X)**2 + &
                                 (Apoints(IApoint)%Y-CARTESIAN(Iatom)%Y)**2 + &
                                 (Apoints(IApoint)%Z-CARTESIAN(Iatom)%Z)**2)
            do Jneighbor = 1, num_neighbors
              Jatom = neighbors(Jneighbor)  
               if(Jatom.NE.Iatom)then
                 r_j=DSQRT((Apoints(IApoint)%X-CARTESIAN(Jatom)%X)**2 + &
                           (Apoints(IApoint)%Y-CARTESIAN(Jatom)%Y)**2 + &
                           (Apoints(IApoint)%Z-CARTESIAN(Jatom)%Z)**2)
                 R_ij=DSQRT((CARTESIAN(Iatom)%X-CARTESIAN(Jatom)%X)**2+ &
                            (CARTESIAN(Iatom)%Y-CARTESIAN(Jatom)%Y)**2+ &
                            (CARTESIAN(Iatom)%Z-CARTESIAN(Jatom)%Z)**2)
            mu_ij = (r_i - r_j) / R_ij
            u_ij = (XI(Iatom,Jatom) - 1.0D0)/(XI(Iatom,Jatom) + 1.0D0)
            a_ij = u_ij/(u_ij**2-1.0D0)
            mu_ij = mu_ij + a_ij*(1.0D0-mu_ij**2)
            mu_ij_a = mu_ij/SSFa
            f1 = (1.0D0/16.0D0)*(35*mu_ij_a - 35*mu_ij_a**3 + 21*mu_ij_a**5 - 5*mu_ij_a**7)
            if(mu_ij.le.-SSFa)then
              SK = 1.0D0
            else if(mu_ij.ge.SSFa)then
              SK = 0.0D0
            else
              Sk= 0.50D0 * (1.0D0 - f1)
            end if
            Pr(Iatom)= Pr(Iatom)*Sk
          end if   !Jatom if
        end do       
        Pn = Pn + Pr(Iatom)
      end do         Ineighbor_loop
      Bweights(IApoint) = Pr(Katom)/Pn
      end do         IApoint_loop
      deallocate(Pr)
      return
      end subroutine SSFWs
      subroutine SSFW_old (Apoints, NApoint, Katom, Bweights)
!**************************************************************************
!     Date last modified: June 25th, 2001 Version 2.0  *
!     Author: Aisha
!     Desciption:
!**************************************************************************
! Modules:
      USE NI_defaults
      USE program_constants
      USE type_elements
      USE type_molecule
      USE module_grid_points
      USE type_Weights


      implicit none

! Input scalars:
      integer, intent (in) :: Katom, NApoint

! Input arrays:
      type(type_grid_points),dimension(NApoint) :: Apoints

! Output arrays:
      double precision, dimension(NApoint) :: Bweights

! Local scalars:
      integer   :: Iatom,Jatom,IApoint
      double precision   :: Pn,a_ij,u_ij,r_i,r_j,R_ij
      double precision   :: f1,f2,f3,mu_ij,mu_ij_a,Sk,a

! Local arrays:
      logical :: nuc_rot
      double precision, dimension(:), allocatable :: Pr

! Begin:

      write(6,*)'SSFW_all'
      allocate (Pr(Natoms))
      Bweights(1:NApoint) = 0.0D0
      do IApoint = 1,NApoint
      Pn = 0.0D0
      do Iatom = 1, Natoms
      Pr(Iatom) = 1.0D0
      if(CARTESIAN(Iatom)%Atomic_number .GT. 0)then
           r_i=DSQRT((Apoints(IApoint)%X-CARTESIAN(Iatom)%X)**2 + &
                     (Apoints(IApoint)%Y-CARTESIAN(Iatom)%Y)**2 + &
                     (Apoints(IApoint)%Z-CARTESIAN(Iatom)%Z)**2)
           do Jatom= 1,Natoms
             if(CARTESIAN(Jatom)%Atomic_number.GT.0)then
               if(Jatom.NE.Iatom)then
                 r_j=DSQRT((Apoints(IApoint)%X-CARTESIAN(Jatom)%X)**2 + &
                           (Apoints(IApoint)%Y-CARTESIAN(Jatom)%Y)**2 + &
                           (Apoints(IApoint)%Z-CARTESIAN(Jatom)%Z)**2)
                 R_ij=DSQRT((CARTESIAN(Iatom)%X-CARTESIAN(Jatom)%X)**2+ &
                            (CARTESIAN(Iatom)%Y-CARTESIAN(Jatom)%Y)**2+ &
                            (CARTESIAN(Iatom)%Z-CARTESIAN(Jatom)%Z)**2)
            mu_ij = (r_i - r_j) / R_ij
            u_ij = (XI(Iatom,Jatom) - 1.0D0)/(XI(Iatom,Jatom) + 1.0D0)
            a_ij = u_ij/(u_ij**2-1.0D0)
            mu_ij = mu_ij + a_ij*(1.0D0-mu_ij**2)
            mu_ij_a = mu_ij/SSFa
            f1 = (1.0D0/16.0D0)*(35*mu_ij_a - 35*mu_ij_a**3 + 21*mu_ij_a**5 - 5*mu_ij_a**7)
            if(mu_ij.le.-SSFa)then
              SK = 1.0D0
            else if(mu_ij.ge.SSFa)then
              SK = 0.0D0
            else
              Sk= 0.50D0 * (1.0D0 - f1)
            end if
            Pr(Iatom)= Pr(Iatom) * Sk
          end if   !Jatom if
          end if   !Jatom if
        end do     !Jatom loop
        Pn = Pn + Pr(Iatom)
      end if    !Iatom if
      end do    !Iatom loop
      Bweights(IApoint) = Pr(Katom)/Pn
      end do        ! IApoint loop
      deallocate(Pr)
      return
      end subroutine SSFW_old
