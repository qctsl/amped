      module type_Weights
!**************************************************************************************
!     Date last modified: July 31, 2014                                               *
!     Author: R. Poirier                                                              *
!     Desciption: Contains the gradient of the Becke weight (for now)                 *
!**************************************************************************************
! Modules:

      implicit none

! Scalars:
      integer :: XI_type  ! Definition of XI to be used for Becke weights
      logical :: LdW_analytical  ! True for analytical first derivative of the Weight
      logical :: Ld2W_analytical ! True for analytical second derivative of the Weight
      character(len=16) :: DEN_partitioning  ! Becke (default), IAWAD, SSF
      character(len=16) :: Type_Hirshfeld  ! Simple, Iterative or Radial
      double precision :: Steepness ! Used for IAWAD weights
      integer, allocatable :: MAP_Ang_toRad(:,:) ! Used for Hirshfeld

! Arrays:
! Used for Becke weights
      double precision, dimension(:,:), allocatable :: XI
      double precision, dimension(:), allocatable :: BWA
      double precision, dimension(:,:), allocatable :: dBWA
      double precision, dimension(:,:,:), allocatable :: d2BWA

! Used for IAWAD weights
      character(len=16) :: Last_Core ! Min, Max1, Max2, r_Average (default)

      double precision, dimension (18) :: LastLimitWeight
      
      double precision, dimension (18), parameter :: Min_RDEN =(/ &
      0.0000,0.0000, &
      1.77050,1.09369,0.797649,0.619763,0.500878,0.417366,0.354495,0.305776, &
      3.40265,2.10027,1.633440,1.34635,1.14513,0.99901,0.887497,0.795634/)

      double precision, dimension (18), parameter :: Max2_RDEN =(/ &
      1.00784,0.576972, &
      3.08074,2.05757,1.533950,1.214930,1.00110,0.849414,0.736764,0.650147, &
      0.00000,2.42357,2.12579,1.87480,1.66778,1.48923,1.35608,1.239880/)

      double precision, dimension (18), parameter :: Max1_RDEN =(/ &
      0.000000,0.000000, &     
      0.365006,0.266294,0.210314,0.173981,0.148748,0.130085,0.115806,0.104507,  &
      0.574636,0.506016,0.452781,0.409292,0.372404,0.342173,0.315775,0.293196/)

      double precision, dimension (18), parameter :: r_aveg =(/ &
      0.00000,0.00000, &
      1.14660,0.83013,0.65175,0.53690,0.45661,0.39719, 0.35151, 0.31527, &
      1.59791,1.38154,1.24024,1.12594,1.03142,0.9516,0.88353,0.82465/)

      end module type_Weights
