     subroutine Weights_Hirshfeld (Apoints, NApoint, Katom, Nweights)
!**************************************************************************
!     Date last modified:                                                 *
!     Author:                                                             *
!     Desciption: Compute the New weight for NApoint grid points          *
!**************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE type_elements
      USE type_molecule
      USE type_plotting
      USE type_Weights
      USE module_grid_points
      USE module_type_ProMol

      implicit none

! Input scalars:
      integer, intent(in) :: Katom,NApoint

! Input arrays:
      type(type_grid_points),dimension(NApoint) :: Apoints

! Output array:
      double precision, dimension(NApoint) :: NWeights

! Local scalars:
      integer :: IApoint,Aatom,ZnumA,ZnumK
      double precision :: rhoA,rhoK,sumRho,sumPot,WeightPot
      double precision :: rhoRadA,rhoRadK,sumRhoRad,WeightRH
      double precision :: Rx,Ry,Rz,riA,riK,riAsq,riKsq
!
! Local arrays:

! Begin:
      write(6,*)'Katom: ',Katom
      NWeights=ZERO
      WeightPot=ZERO
      WeightRH=ZERO
      do IApoint=1,NApoint
        Rx=Apoints(IApoint)%X
        Ry=Apoints(IApoint)%Y
        Rz=Apoints(IApoint)%Z
!       write(6,*)'Iapoint,Rx,Ry,Rz: ',Iapoint,Rx,Ry,Rz
!       call flush(6)
        sumRho=ZERO
        sumPot=ZERO
        sumRhoRad=ZERO
        ZnumK=CARTESIAN(Katom)%Atomic_number
        riKsq=(Rx-CARTESIAN(Katom)%X)**2+(Ry-CARTESIAN(Katom)%Y)**2+(Rz-CARTESIAN(Katom)%Z)**2
        riK=dsqrt(riKsq)
        rhoK=ProMol_fits(ZnumK)%coeff1*dexp(-ProMol_fits(ZnumK)%exp1*riK) + &
             ProMol_fits(ZnumK)%coeff2*dexp(-ProMol_fits(ZnumK)%exp2*riK) + &
             ProMol_fits(ZnumK)%coeff3*dexp(-ProMol_fits(ZnumK)%exp3*riK) + &
             ProMol_fits(ZnumK)%coeff4*dexp(-ProMol_fits(ZnumK)%exp4*riK) + &
             ProMol_fits(ZnumK)%coeff5*dexp(-ProMol_fits(ZnumK)%exp5*riK)
        rhoRadK=(ONE+riKsq)*rhoK
        write(6,'(a,2f12.6)')'riKsq,rhoRadK: ',riKsq,rhoRadK
        do Aatom=1,Natoms
          ZnumA=CARTESIAN(Aatom)%Atomic_number
          if(ZnumA.le.0)cycle
          riAsq=(Rx-CARTESIAN(Aatom)%X)**2+(Ry-CARTESIAN(Aatom)%Y)**2+(Rz-CARTESIAN(Aatom)%Z)**2
          riA=dsqrt(riAsq)
          rhoA=ProMol_fits(ZnumA)%coeff1*dexp(-ProMol_fits(ZnumA)%exp1*riA) + &
              ProMol_fits(ZnumA)%coeff2*dexp(-ProMol_fits(ZnumA)%exp2*riA) + &
              ProMol_fits(ZnumA)%coeff3*dexp(-ProMol_fits(ZnumA)%exp3*riA) + &
              ProMol_fits(ZnumA)%coeff4*dexp(-ProMol_fits(ZnumA)%exp4*riA) + &
              ProMol_fits(ZnumA)%coeff5*dexp(-ProMol_fits(ZnumA)%exp5*riA)
          rhoRadA=(ONE+riAsq)*rhoA
          write(6,'(a,2f12.6)')'riAsq,rhoRadA: ',riAsq,rhoRadA
          sumRho=sumRho+rhoA
          sumRhoRad=sumRhoRad+rhoRadA
        end do ! Aatom
        write(6,'(a,2f12.6)')'sumrhoRad: ',sumrhoRad

      if(Type_Hirshfeld(1:1).eq.'R')then
        WeightRH=rhoRadK/sumRhoRad
        NWeights(IApoint)=WeightRH
      else
        NWeights(IApoint)=rhoK/sumRho
      end if
      write(6,'(a,1PE12.5,4E12.5)')'rhoK,riK,sumPot,NWeight,WeightPot: ',rhoK,riK,sumPot,NWeights(IApoint),WeightPot
      write(UNIout,'(a,3f12.6,2f12.6)')'Rx,Ry,Rz,NWeight,WeightRH: ',Rx,Ry,Rz,NWeights(IApoint),WeightRH
!
      end do ! IApoint
!
      return
      end subroutine Weights_Hirshfeld
      subroutine Weight_Hirshfeld (Rx, Ry, Rz, Katom, NWeight)
!**************************************************************************
!     Date last modified:                                                 *
!     Author:                                                             *
!     Desciption: Compute the New weight for a single grid point          *
!**************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE type_elements
      USE type_molecule
      USE type_Weights
      USE module_type_ProMol

      implicit none

! Input scalars:
      integer, intent(in) :: Katom
      double precision, intent(in) :: Rx,Ry,Rz

! Output scalar:
      double precision, intent(out) :: NWeight

! Local scalars:
      integer :: Aatom,ZnumA,ZnumK
      double precision :: rhoA,rhoK,sumRho,sumPot,WeightPot
      double precision :: riA,riK
      double precision :: rhoRadA,rhoRadK,sumRhoRad,WeightRH
      double precision :: riAsq,riKsq
!
! Local arrays:

! Begin:
      write(6,*)'Katom: ',Katom
!     write(6,*)'Rx,Ry,Rz: ',Rx,Ry,Rz
      call flush(6)
      NWeight=ZERO
      WeightPot=ZERO
      WeightRH=ZERO
      sumRho=ZERO
      sumPot=ZERO
      sumRhoRad=ZERO
      ZnumK=CARTESIAN(Katom)%Atomic_number
      riKsq=(Rx-CARTESIAN(Katom)%X)**2+(Ry-CARTESIAN(Katom)%Y)**2+(Rz-CARTESIAN(Katom)%Z)**2
      riK=dsqrt(riKsq)
      rhoK=ProMol_fits(ZnumK)%coeff1*dexp(-ProMol_fits(ZnumK)%exp1*riK) + &
           ProMol_fits(ZnumK)%coeff2*dexp(-ProMol_fits(ZnumK)%exp2*riK) + &
           ProMol_fits(ZnumK)%coeff3*dexp(-ProMol_fits(ZnumK)%exp3*riK) + &
           ProMol_fits(ZnumK)%coeff4*dexp(-ProMol_fits(ZnumK)%exp4*riK) + &
           ProMol_fits(ZnumK)%coeff5*dexp(-ProMol_fits(ZnumK)%exp5*riK)
      rhoRadK=(ONE+riKsq)*rhoK
      write(6,'(a,2f12.6)')'riKsq,rhoRadK: ',riKsq,rhoRadK
      do Aatom=1,Natoms
        ZnumA=CARTESIAN(Aatom)%Atomic_number
        if(ZnumA.le.0)cycle
        riAsq=(Rx-CARTESIAN(Aatom)%X)**2+(Ry-CARTESIAN(Aatom)%Y)**2+(Rz-CARTESIAN(Aatom)%Z)**2
        riA=dsqrt(riAsq)
        rhoA=ProMol_fits(ZnumA)%coeff1*dexp(-ProMol_fits(ZnumA)%exp1*riA) + &
             ProMol_fits(ZnumA)%coeff2*dexp(-ProMol_fits(ZnumA)%exp2*riA) + &
             ProMol_fits(ZnumA)%coeff3*dexp(-ProMol_fits(ZnumA)%exp3*riA) + &
             ProMol_fits(ZnumA)%coeff4*dexp(-ProMol_fits(ZnumA)%exp4*riA) + &
             ProMol_fits(ZnumA)%coeff5*dexp(-ProMol_fits(ZnumA)%exp5*riA)
        rhoRadA=(ONE+riAsq)*rhoA
        write(6,'(a,2f12.6)')'riAsq,rhoRadA: ',riAsq,rhoRadA
        sumRho=sumRho+rhoA
        sumRhoRad=sumRhoRad+rhoRadA
      end do ! Aatom
      write(6,'(a,2f12.6)')'sumrhoRad: ',sumrhoRad

      if(Type_Hirshfeld(1:1).eq.'R')then
        WeightRH=rhoRadK/sumRhoRad
        NWeight=WeightRH
      else
        NWeight=rhoK/sumRho
      end if
      write(6,'(a,3f12.6,2f12.6)')'rhoRadK,riKsq,NWeight,WeightRH: ',rhoRadK,riKsq,NWeight,WeightRH
      write(UNIout,'(a,3f12.6,2f12.6)')'Rx,Ry,Rz,NWeight,WeightRH: ',Rx,Ry,Rz,NWeight,WeightRH
!
      return
      end subroutine Weight_Hirshfeld
      subroutine INI_ProMol
!**************************************************************************
!     Date last modified:                                                 *
!     Author:                                                             *
!     Desciption: Compute the New weight for a single grid point          *
!**************************************************************************
! Modules:
      USE module_type_ProMol

      implicit none

! Input scalars:

! Output scalar:

! Local scalars:

! Local arrays:

 ! Begin:
      call BLD_ProMol_fits
      return
      end subroutine INI_ProMol
