      subroutine BLD_ProMol_Atoms
!**************************************************************************
!     Date last modified:                                                 *
!     Author:                                                             *
!     Desciption: Compute the New weight for a single grid point          *
!**************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE QM_defaults
      USE type_elements
      USE type_molecule
      USE type_plotting
      USE module_type_ProMol

      implicit none

! Local scalars:
      integer :: Iatom,Ifound,IApoint,KRpoint,Znum
      integer :: File_unit,Save_UNIout
      double precision :: rho,rho_rad
      character(len=64) :: File_name

! Local function:
      double precision :: GET_density_at_xyz

! Begin:
      call GET_object ('QM', 'CMO', Wavefunction)

!     NRpoints=310
!     Rdel=0.01D0
!     Rstart=0.0D0
      
      if(Natoms.ne.1)then
        stop 'For ProMolecule must have a single atom'
      end if
      PlotFileID=' '
      write(PlotFileID,'(a)')'DENSITY'
      call BLD_FileID
      File_name='PROMolecule_SG1_'//trim(Basic_FileID)//'_'//trim(PlotFileID)//'.dat'
      call GET_file_unit ('REPLACE', File_name, 'FORMATTED', PlotFileID, File_unit)
      write(PlotFunction,'(a)')'ProMolecule Density'
      Save_UNIout=UNIout
      UNIout=File_unit

      Iatom=1
      Znum = CARTESIAN(Iatom)%Atomic_number
!
      do KRpoint= 1,NRpoints
        rho=GET_density_at_xyz (ZERO, ZERO, Rstart)
        rho_rad=rho*Rstart**2
        write(UNIout,'(3f14.8)')Rstart,rho,rho_rad
        Rstart=Rstart+Rdel
      end do ! KRpoint

      close (unit=File_unit)
      UNIout=Save_UNIout

      return
      end subroutine BLD_ProMol_Atoms
