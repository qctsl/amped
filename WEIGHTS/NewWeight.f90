      subroutine NewWeights (Apoints, NApoint, Katom, Nweights)
!**************************************************************************
!     Date last modified:                                                 *
!     Author:                                                             *
!     Desciption: Compute the New weight for a single grid point          *
!**************************************************************************
! Modules:
      USE program_constants
      USE type_elements
      USE type_molecule
      USE type_Weights
      USE module_grid_points

      implicit none

! Input scalars:
      integer, intent(in) :: Katom,NApoint

! Input arrays:
      type(type_grid_points),dimension(NApoint) :: Apoints

! Output scalar:
      double precision, dimension(NApoint) :: NWeights

! Local scalars:
      
      integer   :: Aatom,Batom,IApoint
      double precision :: TWeight,r_iA,r_iB,r_iAN,r_iBN
      double precision :: SK_iAB,SK_iBA,alph_iA,alph_iB,f1,f2,f3,mu_iAB,R_AB
      double precision :: Rx,Ry,Rz

! Local arrays:
       double precision, dimension(:), allocatable :: Pr

! Begin:
      allocate(Pr(Natoms))

      NWeights(1:NApoint)=0.0D0

      do IApoint=1,NApoint
        Pr(1:Natoms)=1.0D0
        TWeight=0.0D0
        Rx=Apoints(IApoint)%X
        Ry=Apoints(IApoint)%Y
        Rz=Apoints(IApoint)%Z
        do Aatom=1,Natoms
          if(CARTESIAN(Aatom)%Atomic_number.le.0)cycle
          r_iA=dsqrt((Rx-CARTESIAN(Aatom)%X)**2+(Ry-CARTESIAN(Aatom)%Y)**2+(Rz-CARTESIAN(Aatom)%Z)**2)
          r_iAN=r_iA-LastLimitWeight(CARTESIAN(Aatom)%Atomic_number)

          do Batom=Aatom+1,Natoms
            if(CARTESIAN(Batom)%Atomic_number.le.0)cycle
            r_iB=dsqrt((Rx-CARTESIAN(Batom)%X)**2+(Ry-CARTESIAN(Batom)%Y)**2+(Rz-CARTESIAN(Batom)%Z)**2)
            r_iBN=r_iB-LastLimitWeight(CARTESIAN(Batom)%Atomic_number)
            
!  If SK_iBA and SK_iAB are zero excludes cores.
          if(r_iAN.lt.1.0D-06)then ! Why not zero?  Could be a values set in the menu.
            SK_iAB=1.0D0
            SK_iBA=0.0D0
           else if(r_iBN.lt.1.0D-06)then
            SK_iAB=0.0D0
            SK_iBA=1.0D0
          else
            R_AB=DISMAT(Aatom,Batom)
            alph_iA=r_iAN/r_iA
            alph_iB=r_iBN/r_iB
            mu_iAB=(alph_iA*r_iA-alph_iB*r_iB)/dsqrt((alph_iA*r_iA)**2+(alph_iB*r_iB)**2-alph_iA*alph_iB*(r_iA**2+r_iB**2-R_AB**2))
! Why 3 iterations, 1 should do it.
            f1=(Steepness+1.0d0)*mu_iAB-Steepness*mu_iAB**3
            f1=(Steepness+1.0d0)*f1-Steepness*f1**3
            f1=(Steepness+1.0d0)*f1-Steepness*f1**3
            SK_iAB=0.50D0*(1.0D0-f1)
            SK_iBA=0.50D0*(1.0D0+f1)
          end if
            Pr(Aatom)=Pr(Aatom)*SK_iAB
            Pr(Batom)=Pr(Batom)*SK_iBA
          end do ! Batom loop
          TWeight=TWeight+Pr(Aatom)
        end do! Aatom loop
        if(TWeight.ne.ZERO) NWeights(IApoint)=Pr(Katom)/TWeight
      end do ! IApoint loop
      deallocate(Pr)
      return
end subroutine NewWeights
      subroutine NewWeight (Rx, Ry, Rz, Katom, NWeight)
!**************************************************************************
!     Date last modified:                                                 *
!     Author:                                                             *
!     Desciption: Compute the New weight for a single grid point          *
!**************************************************************************
! Modules:
      USE program_constants
      USE type_elements
      USE type_molecule
      USE type_Weights
      USE NI_defaults

      implicit none

! Input scalars:
      integer, intent(in) :: Katom
      double precision, intent(in) :: Rx,Ry,Rz

! Output scalar:
      double precision,intent(out) :: NWeight

! Local scalars:
      integer   :: Aatom,Batom
      double precision :: TWeight,r_iA,r_iB,r_iAN,r_iBN
      double precision :: SK_iAB,SK_iBA,alph_iA,alph_iB,f1,f2,f3,mu_iAB,R_AB

! Local arrays:
       double precision, dimension(:), allocatable :: Pr

 ! Begin:
      allocate(Pr(Natoms))

      NWeight=0.0D0
 
        Pr(1:Natoms)=1.0D0
        TWeight=0.0D0
        do Aatom=1,Natoms
          if(CARTESIAN(Aatom)%Atomic_number.le.0)cycle
          r_iA=dsqrt((Rx-CARTESIAN(Aatom)%X)**2+(Ry-CARTESIAN(Aatom)%Y)**2+(Rz-CARTESIAN(Aatom)%Z)**2)
          r_iAN=r_iA-LastLimitWeight(CARTESIAN(Aatom)%Atomic_number)
          do Batom=Aatom+1,Natoms
            if(CARTESIAN(Batom)%Atomic_number.le.0)cycle
            r_iB=dsqrt((Rx-CARTESIAN(Batom)%X)**2+(Ry-CARTESIAN(Batom)%Y)**2+(Rz-CARTESIAN(Batom)%Z)**2)
            r_iBN=r_iB-LastLimitWeight(CARTESIAN(Batom)%Atomic_number)

          if(r_iAN.lt.1.0D-06)then
            SK_iAB=1.0D0
            SK_iBA=0.0D0
           else if(r_iBN.lt.1.0D-06)then
            SK_iAB=0.0D0
            SK_iBA=1.0D0
          else
            R_AB=DISMAT(Aatom,Batom)
            alph_iA=r_iAN/r_iA
            alph_iB=r_iBN/r_iB
            mu_iAB=(alph_iA*r_iA-alph_iB*r_iB)/dsqrt((alph_iA*r_iA)**2+(alph_iB*r_iB)**2-alph_iA*alph_iB*(r_iA**2+r_iB**2-R_AB**2))
            f1=(Steepness+1.0d0)*mu_iAB-Steepness*mu_iAB**3
            f1=(Steepness+1.0d0)*f1-Steepness*f1**3
            f1=(Steepness+1.0d0)*f1-Steepness*f1**3
            SK_iAB=0.50D0*(1.0D0-f1)
            SK_iBA=0.50D0*(1.0D0+f1)
          end if

            Pr(Aatom)=Pr(Aatom)*SK_iAB
            Pr(Batom)=Pr(Batom)*SK_iBA
          end do ! Batom loop
          TWeight=TWeight+Pr(Aatom)
        end do! Aatom loop
        !if (katom.eq.ONE) then
        !Pr(katom)=1.0d0
        !else
        !Pr(katom)=0.0
        !end if
        if(TWeight.ne.ZERO) NWeight=Pr(Katom)/TWeight
      deallocate(Pr)
      return
      end subroutine NewWeight
 
