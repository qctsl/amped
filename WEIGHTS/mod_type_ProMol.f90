      module module_type_ProMol
!************************************************************************************
!     Date last modified August 12, 2016                                            *
!     Author: R. Poirier                                                            *
!     Description:                                                                  *
!************************************************************************************
! Modules:
      USE type_elements
!
      implicit none
!
      integer :: NRpoints
      double precision :: Rdel,RStart

      type  :: type_ProMol
        double precision :: exp1
        double precision :: exp2
        double precision :: exp3
        double precision :: exp4
        double precision :: exp5
        double precision :: coeff1
        double precision :: coeff2
        double precision :: coeff3
        double precision :: coeff4
        double precision :: coeff5
      end type type_ProMol

      type(type_ProMol) :: ProMol_fits(Nelements)

CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine BLD_ProMol_fits
!************************************************************************************
!     Date last modified August 12, 2016                                            *
!     Author: R. Poirier                                                            *
!     Description:                                                                  *
!************************************************************************************

      implicit none

! NOTE: These fits are not good enough.  They are only good for small r.
! Hydrogen (2.4D-04)
      ProMol_fits(1)%exp1=5.594291944
      ProMol_fits(1)%exp2=1.970099042
      ProMol_fits(1)%exp3=0.0D0
      ProMol_fits(1)%exp4=0.0D0
      ProMol_fits(1)%exp5=0.0D0
      ProMol_fits(1)%coeff1=0.072354073
      ProMol_fits(1)%coeff2=0.304096381
      ProMol_fits(1)%coeff3=0.0D0
      ProMol_fits(1)%coeff4=0.0D0
      ProMol_fits(1)%coeff5=0.0D0
! Lithium (8.16D-05)
      ProMol_fits(3)%exp1=5.404860155D0
      ProMol_fits(3)%exp2=6.21793503D0
      ProMol_fits(3)%exp3=1.417677661D0
      ProMol_fits(3)%exp4=0.0D0
      ProMol_fits(3)%exp5=0.0D0
      ProMol_fits(3)%coeff1=12.12908096D0
      ProMol_fits(3)%coeff2=7.831177278D0
      ProMol_fits(3)%coeff3=0.02732459D0
      ProMol_fits(3)%coeff4=0.0D0
      ProMol_fits(3)%coeff5=0.0D0
! Carbon (2.50D-04)
      ProMol_fits(6)%exp1=11.4100197D0
      ProMol_fits(6)%exp2=3.942891257D0
      ProMol_fits(6)%exp3=2.674639D0
      ProMol_fits(6)%exp4=0.0D0
      ProMol_fits(6)%exp5=0.0D0
      ProMol_fits(6)%coeff1=119.3072038D0
      ProMol_fits(6)%coeff2=8.525087754D0
      ProMol_fits(6)%coeff3=1.105343623D0
      ProMol_fits(6)%coeff4=0.0D0
      ProMol_fits(6)%coeff5=0.0D0
! Nitrogen (5.60D-04)
      ProMol_fits(7)%exp1=13.37760607D0
      ProMol_fits(7)%exp2=4.442055184D0
      ProMol_fits(7)%exp3=2.766003042D0
      ProMol_fits(7)%exp4=0.0D0
      ProMol_fits(7)%exp5=0.0D0
      ProMol_fits(7)%coeff1=192.9012637D0
      ProMol_fits(7)%coeff2=22.96765124D0
      ProMol_fits(7)%coeff3=0.997203423D0
      ProMol_fits(7)%coeff4=0.0D0
      ProMol_fits(7)%coeff5=0.0D0
! Oxygen (8.16D-04)
      ProMol_fits(8)%exp1=15.90729906D0
      ProMol_fits(8)%exp2=4.933664099D0
      ProMol_fits(8)%exp3=3.009879151D0
      ProMol_fits(8)%exp4=0.0D0
      ProMol_fits(8)%exp5=0.0D0
      ProMol_fits(8)%coeff1=321.078677D0
      ProMol_fits(8)%coeff2=40.37561402D0
      ProMol_fits(8)%coeff3=1.48051993D0
      ProMol_fits(8)%coeff4=0.0D0
      ProMol_fits(8)%coeff5=0.0D0
! Fluorine (1.20D-03)
      ProMol_fits(9)%exp1=18.26781664D0
      ProMol_fits(9)%exp2=5.85011285D0
      ProMol_fits(9)%exp3=3.60590683D0
      ProMol_fits(9)%exp4=0.0D0
      ProMol_fits(9)%exp5=0.0D0
      ProMol_fits(9)%coeff1=495.9137893D0
      ProMol_fits(9)%coeff2=95.88791757D0
      ProMol_fits(9)%coeff3=5.431680408D0
      ProMol_fits(9)%coeff4=0.0D0
      ProMol_fits(9)%coeff5=0.0D0
! Na (4.82D-04)
      ProMol_fits(11)%exp1=21.58571775D0
      ProMol_fits(11)%exp2=5.255068656D0
      ProMol_fits(11)%exp3=8.162435779D0
      ProMol_fits(11)%exp4=2.114601088D0
      ProMol_fits(11)%exp5=1.170510916D0
      ProMol_fits(11)%coeff1=820.8369407D0
      ProMol_fits(11)%coeff2=54.38405493D0
      ProMol_fits(11)%coeff3=534.7482566D0
      ProMol_fits(11)%coeff4=0.269099682D0
      ProMol_fits(11)%coeff5=0.01152656D0
! Mg (3.48D-04)
      ProMol_fits(11)%exp1=23.6361786D0
      ProMol_fits(11)%exp2=6.250625927D0
      ProMol_fits(11)%exp3=9.119704111D0
      ProMol_fits(11)%exp4=2.503232274D0
      ProMol_fits(11)%exp5=1.780492706D0
      ProMol_fits(11)%coeff1=1080.884659D0
      ProMol_fits(11)%coeff2=128.0114993D0
      ProMol_fits(11)%coeff3=935.0303085D0
      ProMol_fits(11)%coeff4=0.363672676D0
      ProMol_fits(11)%coeff5=0.019606125D0
! Al (3.56D-04)
      ProMol_fits(11)%exp1=25.67847258D0
      ProMol_fits(11)%exp2=7.094174759D0
      ProMol_fits(11)%exp3=9.965013382D0
      ProMol_fits(11)%exp4=3.022298495D0
      ProMol_fits(11)%exp5=2.063793217D0
      ProMol_fits(11)%coeff1=1390.130812D0
      ProMol_fits(11)%coeff2=223.2722408D0
      ProMol_fits(11)%coeff3=1553.292939D0
      ProMol_fits(11)%coeff4=0.560355064D0
      ProMol_fits(11)%coeff5=0.022104051D0
! Si (7.11D-03)
      ProMol_fits(11)%exp1=30.26168281D0
      ProMol_fits(11)%exp2=7.106402377D0
      ProMol_fits(11)%exp3=6.247657364D0
      ProMol_fits(11)%exp4=2.495502927D0
      ProMol_fits(11)%exp5=4.707035674D0
      ProMol_fits(11)%coeff1=1941.512025D0
      ProMol_fits(11)%coeff2=690.0769638D0
      ProMol_fits(11)%coeff3=-280.8772616D0
      ProMol_fits(11)%coeff4=0.016249338D0
      ProMol_fits(11)%coeff5=1.317271118D0
! P  (8.09D-03)
      ProMol_fits(11)%exp1=32.79162484D0
      ProMol_fits(11)%exp2=7.154146552D0
      ProMol_fits(11)%exp3=6.689915533D0
      ProMol_fits(11)%exp4=2.436285557D0
      ProMol_fits(11)%exp5=5.364290029D0
      ProMol_fits(11)%coeff1=2428.483743D0
      ProMol_fits(11)%coeff2=1417.580628D0
      ProMol_fits(11)%coeff3=-893.4315463D0
      ProMol_fits(11)%coeff4=0.58582444D0
      ProMol_fits(11)%coeff5=84.36381546D0
! S  (9.82D-04)
      ProMol_fits(11)%exp1=32.0334757D0
      ProMol_fits(11)%exp2=140.4136643D0
      ProMol_fits(11)%exp3=11.58856045D0
      ProMol_fits(11)%exp4=2.619871402D0
      ProMol_fits(11)%exp5=4.253376201D0
      ProMol_fits(11)%coeff1=2681.788997D0
      ProMol_fits(11)%coeff2=-10.09419911D0
      ProMol_fits(11)%coeff3=5403.076699D0
      ProMol_fits(11)%coeff4=0.235044011D0
      ProMol_fits(11)%coeff5=5.901821168D0
! Cl (1.47D-03)
      ProMol_fits(11)%exp1=33.50715957D0
      ProMol_fits(11)%exp2=140.4137291D0
      ProMol_fits(11)%exp3=12.81161611D0
      ProMol_fits(11)%exp4=3.040210567D0
      ProMol_fits(11)%exp5=4.834736279D0
      ProMol_fits(11)%coeff1=3166.190434D0
      ProMol_fits(11)%coeff2=-10.0941979D0
      ProMol_fits(11)%coeff3=9807.994224D0
      ProMol_fits(11)%coeff4=0.18101653D0
      ProMol_fits(11)%coeff5=6.288433155D0

      end  subroutine BLD_ProMol_fits
! END CONTAINS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end module module_type_ProMol
