      subroutine BinaryWeight (Rx, Ry, Rz, Katom, Weight)
!**************************************************************************
!     Date last modified:                                                 *
!     Author:                                                             *
!     Desciption: Compute the Binary weight for a single grid point          *
!**************************************************************************
! Modules:
      USE program_constants
      USE type_elements
      USE type_molecule
      USE type_Weights
      USE NI_defaults

      implicit none

! Input scalars:
      integer, intent(IN) :: Katom
      double precision, intent(IN) :: Rx,Ry,Rz

! Output scalar:
      double precision,intent(OUT) :: Weight

! Local scalars:
      integer   :: Aatom
      double precision :: RKx,RKy,RKz
      double precision :: r_iK,r_iA

! Local arrays:

 ! Begin:
        RKx=CARTESIAN(Katom)%X
        RKy=CARTESIAN(Katom)%Y
        RKz=CARTESIAN(Katom)%Z
        r_iK=dsqrt((Rx-RKx)**2+(Ry-RKy)**2+(Rz-RKz)**2)
        do Aatom=1,Natoms
          if(CARTESIAN(Aatom)%Atomic_number.le.0)cycle
          r_iA=dsqrt((Rx-CARTESIAN(Aatom)%X)**2+(Ry-CARTESIAN(Aatom)%Y)**2+(Rz-CARTESIAN(Aatom)%Z)**2)
          if(r_iK.lt.r_iA)then
            Weight=ONE
          else if(r_iK.eq.r_iA)then
            Weight=PT5
          else
            Weight=ZERO
          end if 
        end do! Aatom loop

      return
      end subroutine BinaryWeight
 
