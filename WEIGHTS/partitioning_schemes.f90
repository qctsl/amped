      subroutine GET_weight1 (Rx, Ry, Rz, Katom, Weight)
!********************************************************************************
!       Date last modified: October 25, 2013                                    *
!       Author: R. A. Poirier                                                   *
!       Description: Computes the weight that is used to partition the density. *
!       Return the weight for a single grid point.                              *
!********************************************************************************        
! Modules:
      USE program_files
      USE type_Weights

      implicit none

! Input scalars:
      integer, intent(IN) :: Katom
      double precision, intent(IN) :: Rx,Ry,Rz
      double precision, intent(OUT) :: Weight
!
      select case(DEN_partitioning)
        case('BECKE')
          call BeckeW1 (Rx, Ry, Rz, Katom, Weight) 
       case('IAWAD')
         call NewWeight (Rx, Ry, Rz, Katom, Weight) 
       case('Fermi_Dirac')
         call Fermi_Weight (Rx, Ry, Rz, Katom, Weight)
       case('ABSw')
         call ABSWeight (Rx, Ry, Rz, Katom, Weight)
       case('TFVC')
         call TFVC1 (Rx, Ry, Rz, Katom, Weight)
        case('SSF')
!         call SSFW1 (Rx, Ry, Rz, Katom, Weight) 
          write(UNIout,'(a)')'Partitioning scheme not available yet'
        case('LESP')
!         call LESPW1 (Rx, Ry, Rz, Katom, Weight) 
          write(UNIout,'(a)')'Partitioning scheme not available yet'
        case('NUCPOT')
!         call NUCPOTW1 (Rx, Ry, Rz, Katom, Weight) 
          write(UNIout,'(a)')'Partitioning scheme not available yet'
        case('LRAD')
!         call LRADW1 (Rx, Ry, Rz, Katom, Weight) 
       case('BINARY')
         call BinaryWeight (Rx, Ry, Rz, Katom, Weight) 
       case('HIRSHFELD')
         call Weight_Hirshfeld (Rx, Ry, Rz, Katom, Weight) 
        case default
          write(UNIout,'(a)')'Not a valid partitioning scheme'
          stop
        end select

      end subroutine GET_weight1
      subroutine GET_weights (Ang_points, NApoints, Iatom, Bweights)
!********************************************************************************
!       Date last modified: December 8, 2013                                    *
!       Author: R. A. Poirier                                                   *
!       Description: Computes the weight that is used to partition the density. *
!       Returns the weights for all grid points.                                *
!********************************************************************************        
! Modules:
      USE program_files
      USE module_grid_points
      USE type_Weights

      implicit none

! Input scalars:
      integer, intent (in) :: Iatom,NApoints

! Input arrays:
      type(type_grid_points), dimension(NApoints) :: Ang_points

! Output arrays:
      double precision, dimension(NApoints) :: Bweights
!
      select case(DEN_partitioning)
        case('BECKE')
          call BeckeWs (Ang_points, NApoints, Iatom, Bweights)
        case('SSF')
            call SSFWs (Ang_points, NApoints, Iatom, Bweights)
        case('IAWAD')
          call NewWeights (Ang_points, NApoints, Iatom, Bweights)
        case('Fermi_Dirac')
          call Fermi_Weights (Ang_points, NApoints, Iatom, Bweights)
        case('ABSw')
          call ABSWeights (Ang_points, NApoints, Iatom, Bweights)
        case('TFVC')
         call TFVCs (Ang_points, NApoints, Iatom, Bweights)
        case('LESP')
!         call LESPWs (Ang_points, NApoints, Iatom, Bweights)
          write(UNIout,'(a)')'Partitioning scheme not available yet'
        case('NUCPOT')
!         call NUCPOTWs (Ang_points, NApoints, Iatom, Bweights)
          write(UNIout,'(a)')'Partitioning scheme not available yet'
        case('LRAD')
!         call LRADWs (Ang_points, NApoints, Iatom, Bweights)
       case('HIRSHFELD')
          call Weights_Hirshfeld (Ang_points, NApoints, Iatom, Bweights)
        case default
          write(UNIout,'(a)')'Not a valid partitioning scheme'
          stop
      end select

      end subroutine GET_weights
      subroutine GET_dWeights (Rx, Ry, Rz, dWeight, WeightA, Natoms)
!********************************************************************************
!       Date last modified: October 25, 2013                                    *
!       Author: R. A. Poirier                                                   *
!       Description: Computes the gradient of the weight and the weight that is 
!       used to partition the density.                                          *
!       Return dW and W for all the atoms for the specified grid point.         *
!********************************************************************************        
! Modules:
      USE program_files
      USE type_Weights

      implicit none

! Input scalars:
      integer :: Natoms
      double precision, intent(IN) :: Rx,Ry,Rz
! Output arrays:
      double precision, dimension(Natoms,3), intent(OUT) :: dWeight
      double precision, dimension(Natoms), intent(OUT) :: WeightA
!
      select case(DEN_partitioning)
        case('BECKE')
          call Becke_dW (Rx, Ry, Rz, dWeight, WeightA)  ! Analytical
       case('IAWAD')
          call GET_dWN_NUMERICAL (Rx, Ry, Rz, dWeight, WeightA, Natoms)
        case('SSF')
!         call SSFW1 (Rx, Ry, Rz, Katom, Weight) 
          write(UNIout,'(a)')'Partitioning scheme not available yet'
        case('LESP')
!         call LESPW1 (Rx, Ry, Rz, Katom, Weight) 
          write(UNIout,'(a)')'Partitioning scheme not available yet'
        case('NUCPOT')
!         call NUCPOTW1 (Rx, Ry, Rz, Katom, Weight) 
          write(UNIout,'(a)')'Partitioning scheme not available yet'
        case('LRAD')
!         call LRADW1 (Rx, Ry, Rz, Katom, Weight) 
          write(UNIout,'(a)')'Partitioning scheme not available yet'
        case default
          write(UNIout,'(a)')'Not a valid partitioning scheme'
          stop
        end select

      end subroutine GET_dWeights
      subroutine GET_d2Weights (Rx, Ry, Rz, Iatom, WeightA, dWeight, d2Weight)
!********************************************************************************
!       Date last modified: October 25, 2013                                    *
!       Author: R. A. Poirier                                                   *
!       Description: Computes the gradient of the weight and the weight that is *
!       used to partition the density.                                          *
!       Returns dW, d2W and W for all the atoms for the specified grid point.   *
!********************************************************************************        
! Modules:
      USE program_files
      USE type_Weights

      implicit none

! Input scalars:
      integer :: Iatom
      double precision, intent(IN) :: Rx,Ry,Rz
! Output arrays:
      double precision, intent(OUT) :: WeightA
      double precision, dimension(3), intent(OUT) :: dWeight
      double precision, dimension(3,3), intent(OUT) :: d2Weight
!
      select case(DEN_partitioning)
        case('BECKE')
          call BeckeWdels (Rx, Ry, Rz, Iatom, WeightA, dWeight, d2Weight) 
       case('IAWAD')
!        call GET_weight1 (Rx, Ry, Rz, Iatom, WeightA)
!        call GET_dW_NUMERICAL (Rx, Ry, Rz, Iatom, dWeight)
!        call GET_d2W_NUMERICAL (Rx, Ry, Rz, Iatom, d2Weight)
          write(UNIout,'(a)')'Partitioning scheme not available yet'
        case('SSF')
!         call SSFW1 (Rx, Ry, Rz, Katom, Weight) 
          write(UNIout,'(a)')'Partitioning scheme not available yet'
        case('LESP')
!         call LESPW1 (Rx, Ry, Rz, Katom, Weight) 
          write(UNIout,'(a)')'Partitioning scheme not available yet'
        case('NUCPOT')
!         call NUCPOTW1 (Rx, Ry, Rz, Katom, Weight) 
          write(UNIout,'(a)')'Partitioning scheme not available yet'
        case('LRAD')
!         call LRADW1 (Rx, Ry, Rz, Katom, Weight) 
          write(UNIout,'(a)')'Partitioning scheme not available yet'
        case default
          write(UNIout,'(a)')'Not a valid partitioning scheme'
          stop
        end select

      end subroutine GET_d2Weights
      subroutine GET_dWN_NUMERICAL (Xpt, Ypt, Zpt, DEL_Ws, Weights, Natoms)
!********************************************************************
!     Date last modified: August 15, 2012                           *
!     Authors: R.A. Poirier                                         *
!     Description: Calculate del charge density at point.           *
!********************************************************************
! Modules:
      USE program_files
      USE module_grid_points

      implicit none
!
! Input scalars:
      integer :: Iatom,Natoms
      double precision :: Xpt,Ypt,Zpt
! Output arrays:
      double precision, dimension(Natoms,3) :: DEL_Ws
      double precision, dimension(Natoms) :: Weights
!
! Local arrays:
      double precision :: Weight1
      double precision :: DEL_W(3), XYZ0(3)
!
! Begin:
      call PRG_manager ('enter','GET_dWN_NUMERICAL', 'UTILITY')
!
!     Numerical_First
      XYZ0(1)=Xpt
      XYZ0(2)=Ypt
      XYZ0(3)=Zpt
      do Iatom=1,Natoms
        call GET_dW_NUMERICAL (Xpt, Ypt, Zpt, Iatom, DEL_W)
        DEL_Ws(Iatom,1)=DEL_W(1)
        DEL_Ws(Iatom,2)=DEL_W(2)
        DEL_Ws(Iatom,3)=DEL_W(3)
        call GET_weight1 (Xpt, Ypt, Zpt, Iatom, Weight1) ! For any weight
        Weights(Iatom)=Weight1
      end do ! Iatom
!
! End of routine GET_dWN_NUMERICAL
      call PRG_manager ('exit', 'GET_dWN_NUMERICAL', 'UTILITY')
      return
      end subroutine GET_dWN_NUMERICAL
      subroutine GET_dW_NUMERICAL (Xpt, Ypt, Zpt, Iatom, DEL_W)
!********************************************************************
!     Date last modified: August 15, 2012                           *
!     Authors: R.A. Poirier                                         *
!     Description: Calculate del charge density at point.           *
!********************************************************************
! Modules:
      USE program_files
      USE module_grid_points

      implicit none
!
! Input scalars:
      integer :: Iatom
      double precision :: DEL_W(3)
      double precision :: Xpt,Ypt,Zpt
!
! Local arrays:
      integer :: Iparam,Nparam
      double precision :: Fn_plus,Fn_minus,NGstep,Step
      double precision :: XYZ(3),XYZ0(3)
!
! Begin:
      call PRG_manager ('enter','GET_dW_NUMERICAL', 'UTILITY')
!
!     Numerical_First
! Save grid point        
      XYZ0(1)=Xpt
      XYZ0(2)=Ypt
      XYZ0(3)=Zpt
      XYZ=XYZ0

      NGstep=0.001
!     write(UNIout,'(a,f12.6)')'NGstep: ',NGstep

! Compute first derivative:      
      Step=NGstep
      Nparam=3
      do Iparam=1,Nparam
         XYZ(Iparam)=XYZ0(Iparam)+Step            ! Change the (i)th component 
         call GET_Weight1(XYZ(1), XYZ(2), XYZ(3), Iatom, Fn_plus)
         XYZ(Iparam)=XYZ0(Iparam)-Step            ! Change the (i)th component
         call GET_Weight1(XYZ(1), XYZ(2), XYZ(3), Iatom, Fn_minus)
         XYZ(Iparam)=XYZ0(Iparam)                  ! Reset XYZ

      DEL_W(Iparam)=(Fn_plus-Fn_minus)/(2.0D0*Step)      ! Compute the first derivative
      end do
! End of routine GET_dW_NUMERICAL
      call PRG_manager ('exit', 'GET_dW_NUMERICAL', 'UTILITY')
      return
      end subroutine GET_dW_NUMERICAL
      subroutine GET_d2W_NUMERICAL (Rx, Ry, Rz, Iatom, DEL2_W)
!********************************************************************
!     Date last modified: August 15, 2012                           *
!     Authors: R.A. Poirier                                         *
!     Description: Calculate del charge density at point.           *
!********************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE module_grid_points

      implicit none
!
! Input scalars:
      integer :: Ipoint,Iatom
      double precision :: Rx,Ry,Rz
      double precision :: DEL2_W(3,3)
!
! Local arrays:
      integer :: Iparam,Jparam,Nparam
      double precision :: Fn_pp,Fn_mm,Fn_pm,Fn_mp,NGstep,Step,DStep2
      double precision :: XYZ(3),XYZ0(3)
!
! Begin:
      call PRG_manager ('enter','GET_d2W_NUMERICAL', 'UTILITY')
!
      XYZ0(1)=Rx
      XYZ0(2)=Ry
      XYZ0(3)=Rz
      XYZ=XYZ0

      NGstep=0.001

      Step=NGstep
      DStep2=(TWO*Step)**2
      Nparam=3
      do Iparam=1,Nparam
      call GET_Weight1 (XYZ(1), XYZ(2), XYZ(3), Iatom, Fn_pm)
      XYZ(Iparam)=XYZ0(Iparam)+TWO*Step
      call GET_Weight1 (XYZ(1), XYZ(2), XYZ(3), Iatom, Fn_pp)
      XYZ(Iparam)=XYZ0(Iparam)-TWO*Step
      call GET_Weight1 (XYZ(1), XYZ(2), XYZ(3), Iatom, Fn_mm)
      DEL2_W(Iparam,Iparam)=(Fn_pp+Fn_mm-TWO*Fn_pm)/DStep2
      XYZ(Iparam)=XYZ0(Iparam)                  ! Reset XYZ
      do Jparam=1,Iparam
      if(Iparam.eq.Jparam)cycle
         XYZ(Iparam)=XYZ0(Iparam)+Step            ! Change the (i)th component 
         XYZ(Jparam)=XYZ0(Jparam)+Step            ! Change the (j)th component 
         call GET_Weight1 (XYZ(1), XYZ(2), XYZ(3), Iatom, Fn_pp)
         XYZ(Iparam)=XYZ0(Iparam)-Step            ! Change the (i)th component 
         XYZ(Jparam)=XYZ0(Jparam)-Step            ! Change the (j)th component 
         call GET_Weight1 (XYZ(1), XYZ(2), XYZ(3), Iatom, Fn_mm)
         XYZ(Iparam)=XYZ0(Iparam)+Step            ! Change the (i)th component 
         XYZ(Jparam)=XYZ0(Jparam)-Step            ! Change the (j)th component 
         call GET_Weight1 (XYZ(1), XYZ(2), XYZ(3), Iatom, Fn_pm)
         XYZ(Iparam)=XYZ0(Iparam)-Step            ! Change the (i)th component 
         XYZ(Jparam)=XYZ0(Jparam)+Step            ! Change the (j)th component 
         call GET_Weight1 (XYZ(1), XYZ(2), XYZ(3), Iatom, Fn_mp)
         DEL2_W(Iparam,Jparam)=(Fn_pp+Fn_mm-Fn_pm-Fn_mp)/DStep2
         DEL2_W(Jparam,Iparam)=DEL2_W(Iparam,Jparam)
         XYZ(Iparam)=XYZ0(Iparam)                  ! Reset XYZ
         XYZ(Jparam)=XYZ0(Jparam)                  ! Reset XYZ
      end do ! Jparam
      end do ! Iparam
!
! End of routine GET_d2W_NUMERICAL
      call PRG_manager ('exit', 'GET_d2W_NUMERICAL', 'UTILITY')
      return
      end subroutine GET_d2W_NUMERICAL
