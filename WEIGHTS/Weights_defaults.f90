      subroutine SET_defaults_Weights
!*******************************************************************************************************
!     Date last modified: August 4, 2014                                                   Version 2.0  *
!     Author: R.A. Poirier                                                                             *
!     Description: Set defaults for the weights (e.g., Becke)                                          *
!*******************************************************************************************************
! Modules:
      USE type_Weights

      implicit none
!
! Begin:
      XI_type=1
      DEN_partitioning='BECKE '
      LdW_analytical=.true.
      Ld2W_analytical=.true.
      Steepness=0.5D0
      Type_Hirshfeld='SIMPLE'
      Last_Core="r_Average"
      LastLimitWeight=r_aveg
!
! End of routine SET_defaults_Weights
      return
      end subroutine SET_defaults_Weights
