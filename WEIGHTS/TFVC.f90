      subroutine TFVCs (Apoints, NApoint, Katom, Bweights)
!*******************************************************************************
!     Date last modified: June 25th, 2001 Version 2.0                          *
!     Author: Aisha / R.A. Poirier                                             *
!     Desciption:  Gets the Becke weights for all the angular points in        *
!                  Apoints for atom Katom.                                     *
!*******************************************************************************
! Modules:
      USE program_constants
      USE type_elements
      USE type_molecule
      USE module_grid_points
      USE type_Weights

      implicit none

! Input scalars:
      integer, intent (in) :: Katom,NApoint

! Input arrays:
      type(type_grid_points),dimension(NApoint) :: Apoints

! Output arrays:
      double precision, dimension(NApoint) :: Bweights

! Local scalars:
      integer   :: Iatom,Jatom,IApoint
      double precision :: Pn,a_ij,u_ij,r_i,r_j,R_ij
      double precision :: f1,f2,f3,mu_ij,Skij,Skji,SkijB
      double precision :: Rx,Ry,Rz

! Local arrays:
       double precision, dimension(:), allocatable :: Pr

 ! Begin:
      allocate(Pr(Natoms))
      Bweights(1:NApoint)=0.0D0
 
      do IApoint=1,NApoint
        Pr(1:Natoms)=1.0D0
        Pn=0.0D0
        Rx=Apoints(IApoint)%X
        Ry=Apoints(IApoint)%Y
        Rz=Apoints(IApoint)%Z
        do Iatom=1,Natoms
          if(CARTESIAN(Iatom)%Atomic_number.le.0)cycle
          r_i=dsqrt((Rx-CARTESIAN(Iatom)%X)**2+(Ry-CARTESIAN(Iatom)%Y)**2+(Rz-CARTESIAN(Iatom)%Z)**2)
          do Jatom=Iatom+1,Natoms
            if(CARTESIAN(Jatom)%Atomic_number.le.0)cycle
!           if(Jatom.eq.Iatom)cycle
            r_j=dsqrt((Rx-CARTESIAN(Jatom)%X)**2+(Ry-CARTESIAN(Jatom)%Y)**2+(Rz-CARTESIAN(Jatom)%Z)**2)
            R_ij=DISMAT(Iatom,Jatom)
            mu_ij=(r_i-r_j)/R_ij
            !u_ij=(XI(Iatom,Jatom)-1.0D0)/(XI(Iatom,Jatom)+1.0D0)
            !a_ij=u_ij/(u_ij**2-1.0D0)
            !mu_ij=mu_ij+a_ij*(1.0D0-mu_ij**2)
            mu_ij=(1.0d0+mu_ij-XI(Iatom,Jatom)*(1.0d0-mu_ij))/(1.0d0+mu_ij+XI(Iatom,Jatom)*(1.0d0-mu_ij))
!
! Becke function
            f1=1.50D0*mu_ij-0.50D0*mu_ij**3
            f2=1.50D0*f1-0.50D0*f1**3
            f3=1.50D0*f2-0.50D0*f2**3
            Skij=0.50D0*(1.0D0-f3)
            Skji=0.50D0*(1.0D0+f3)

            Pr(Iatom)=Pr(Iatom)*Skij
            Pr(Jatom)=Pr(Jatom)*Skji
          end do ! Jatom loop
          Pn=Pn+Pr(Iatom)
        end do! Iatom loop
        if(Pn.ne.ZERO)Bweights(IApoint)=Pr(Katom)/Pn
      end do ! IApoint loop
      deallocate(Pr)
      return
      end subroutine TFVCs
      subroutine TFVC1 (Rx, Ry, Rz, Katom, BWeight)
!**************************************************************************
!     Date last modified: September 9, 2010                               *
!     Author: R.A. Poirier                                                *
!     Desciption: Compute the Becke weight for a single grid point        *
!**************************************************************************
! Modules:
      USE program_constants
      USE type_elements
      USE type_molecule
      USE module_grid_points
      USE type_Weights

      implicit none

! Input scalars:
      integer, intent(in) :: Katom
      double precision, intent(in) :: Rx,Ry,Rz

! Input arrays:

! Output scalar:
      double precision,intent(out) :: BWeight

! Local scalars:
      integer   :: Iatom,Jatom
      double precision :: Pn,a_ij,u_ij,r_i,r_j,R_ij
      double precision :: f1,f2,f3,mu_ij,Skij,Skji,SkijB

! Local arrays:
       double precision, dimension(:), allocatable :: Pr

 ! Begin:
      allocate(Pr(Natoms))
      BWeight=0.0D0
 
        Pr(1:Natoms)=1.0D0
        Pn=0.0D0
        do Iatom=1,Natoms
          if(CARTESIAN(Iatom)%Atomic_number.le.0)cycle
          r_i=dsqrt((Rx-CARTESIAN(Iatom)%X)**2+(Ry-CARTESIAN(Iatom)%Y)**2+(Rz-CARTESIAN(Iatom)%Z)**2)
          do Jatom=Iatom+1,Natoms
            if(CARTESIAN(Jatom)%Atomic_number.le.0)cycle
!           if(Jatom.eq.Iatom)cycle
            r_j=dsqrt((Rx-CARTESIAN(Jatom)%X)**2+(Ry-CARTESIAN(Jatom)%Y)**2+(Rz-CARTESIAN(Jatom)%Z)**2)
            R_ij=DISMAT(Iatom,Jatom)
            mu_ij=(r_i-r_j)/R_ij
            !u_ij=(XI(Iatom,Jatom)-1.0D0)/(XI(Iatom,Jatom)+1.0D0)
            !a_ij=u_ij/(u_ij**2-1.0D0)
            !mu_ij=mu_ij+a_ij*(1.0D0-mu_ij**2)
            mu_ij=(1.0d0+mu_ij-XI(Iatom,Jatom)*(1.0d0-mu_ij))/(1.0d0+mu_ij+XI(Iatom,Jatom)*(1.0d0-mu_ij))
! Becke function
            f1=1.50D0*mu_ij-0.50D0*mu_ij**3
            f2=1.50D0*f1-0.50D0*f1**3
            f3=1.50D0*f2-0.50D0*f2**3
            Skij=0.50D0*(1.0D0-f3)
            Skji=0.50D0*(1.0D0+f3)

            Pr(Iatom)=Pr(Iatom)*Skij
            Pr(Jatom)=Pr(Jatom)*Skji
          end do ! Jatom loop
          Pn=Pn+Pr(Iatom)
        end do! Iatom loop
        if(Pn.ne.ZERO)BWeight=Pr(Katom)/Pn
      deallocate(Pr)

      return
      end subroutine TFVC1
