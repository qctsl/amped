      subroutine MENU_Partitioning
!***********************************************************************
!     Date last modified: January 21, 1997                 Version 1.0 *
!     Author: J.D. Xidos, R.A. Poirier                                 *
!     Description: Set up One Electron Properties                      *
!***********************************************************************
! Modules:
      USE program_parser
      USE program_constants
      USE MENU_gets
      USE QM_defaults
      USE type_Weights

      implicit none
!
! Local scalars:
      integer :: length
      logical :: done
!
! Begin:
      call PRG_manager ('enter', 'MENU_Partitioning', 'UTILITY')
      done=.false.
!
! Menu:
      do while (.not.done)
      call new_token (' PArtitioning:')
!
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      ' Command PArtitioning:', &
      '   Purpose: Define the partitioning scheme (weights)', &
      '   Syntax :', &
      '   dW = <logical>, false for numerical first derivatives of Becke weight', &
      '   d2W = <logical>, false for numerical second derivatives of Becke weight', &
      '   d2W = <logical>, false for numerical second derivatives of Becke weight', &
      '   Hirshfeld = <command>, Define the type of Hirshfeld to use', &
      '   SCheme = <string>  Becke, IAWAD, SSF, ...', &
      '   STeepness = <real>  a values between 0 and 0.5 (for IAWAD only)', &
      '   XI = <integer>  0 (Identity matrix), ', &
      '                   1 (BS for H is set to 0.35, default) ', &
      '                   2 (BS for H is set to 0.25) ', &
      '   end'
!
! dW
      else if(token(1:2).eq.'DW')then
         call GET_value (LdW_analytical)
!
! d2W
      else if(token(1:3).eq.'D2W')then
         call GET_value (Ld2W_analytical)
!
! Hirshfeld
      else if(token(1:1).eq.'H')then
           DEN_partitioning = 'HIRSHFELD'
           call MENU_HIRSHFELD
!
! SCheme
      else if(token(1:2).EQ.'SC')then
         call GET_value (DEN_partitioning, length)
         if(DEN_partitioning(1:1).eq.'B')then
           DEN_partitioning = 'BECKE'
         else if(DEN_partitioning(1:1).eq.'I')then
           DEN_partitioning = 'IAWAD'
           LdW_analytical=.false.
           Ld2W_analytical=.false.
         else if(DEN_partitioning(1:1).eq.'F')then
           DEN_partitioning = 'Fermi_Dirac'
         else if(DEN_partitioning(1:1).eq.'A')then
           DEN_partitioning = 'ABSw'
         else if(DEN_partitioning(1:1).eq.'T')then
           DEN_partitioning = 'TFVC'
         else if(DEN_partitioning(1:1).eq.'S')then
           DEN_partitioning = 'SSF'
         else if(DEN_partitioning(1:2).eq.'LE')then
           DEN_partitioning = 'LESP'
         else if(DEN_partitioning(1:2).eq.'NU')then
           DEN_partitioning = 'NUCPOT'
         else if(DEN_partitioning(1:2).eq.'LR')then
           DEN_partitioning = 'LRAD'
         else if(DEN_partitioning(1:2).eq.'BI')then
           DEN_partitioning = 'BINARY'
         end if
         write(UNIout,'(2a)')' Partitioning scheme set to: ',DEN_partitioning
!
! STeepness
      else if(token(1:2).eq.'ST')then
         call GET_value (Steepness)

! COre "last core"
      else if(token(1:2).eq.'CO')then
         call GET_value (Last_Core, length)
         if(Last_Core(1:2).eq.'MI')then
           Last_Core = 'MIN'
           LastLimitWeight = Min_RDEN
         else if(Last_Core(1:1).eq.'A')then
           Last_Core = 'r_Average'
           LastLimitWeight = r_aveg
         else if(Last_Core(1:4).eq.'MAX1')then
           Last_Core = 'MAX1'
           LastLimitWeight = Max1_RDEN
         else if(Last_Core(1:4).eq.'MAX2')then
           Last_Core = 'MAX2'
           LastLimitWeight = Max2_RDEN
         end if
         write(UNIout,'(2a)')' Last atomic core set to: ',Last_Core
!
! XI
      else if(token(1:2).eq.'XI')then
        call GET_value (XI_type)
        select case(XI_type)
          case(0)
          write(UNIout,'(a)')'NOTE: XI has been set to the identity matrix '
          case(1)
          write(UNIout,'(a)')'NOTE: The Bragg-Slater radius of H has been set to 0.35 and XI has been adjusted'
          case(2)
          write(UNIout,'(a)')'NOTE: The Bragg-Slater radius of H has been set to 0.25 and XI has been adjusted'
          case default
          write(UNIout,'(a,i6)')'Specified option for XI not valid, can be 0 or 1 ',XI_type
        end select

      else
         call MENU_end (done)
      end if
!
      end do !(.not.done)
!
! End of routine MENU_Partitioning
      call PRG_manager ('exit', 'MENU_Partitioning', 'UTILITY')
      return
      end
      subroutine MENU_HIRSHFELD
!***********************************************************************
!     Date last modified: January 21, 1997                 Version 1.0 *
!     Author: J.D. Xidos, R.A. Poirier                                 *
!     Description: Set up One Electron Properties                      *
!***********************************************************************
! Modules:
      USE program_parser
      USE MENU_gets
      USE type_Weights
      USE module_type_ProMol

      implicit none
!
! Local scalars:
      integer :: length
      logical :: done
!
! Begin:
      call PRG_manager ('enter', 'MENU_HIRSHFELD', 'UTILITY')
      done=.false.
!
! Menu:
      call INI_ProMol

      do while (.not.done)
      call new_token (' HIrshfeld:')
!
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      ' Command HIrshfeld:', &
      '   Purpose: Define the type of Hirshfeld to use', &
      '   Syntax :', &
      '   RDel = <real>  Step size of Radial point', &
      '   NRpoints = <integer>>  The number of Radial points', &
      '   RStart = <real>  Starting value of Radial point', &
      '   SCheme = <string>  Simple, Iterative, Radial', &
      '   end'
!
! SCheme
      else if(token(1:1).EQ.'S')then
         call GET_value (Type_Hirshfeld, length)
         if(Type_Hirshfeld(1:1).eq.'S')then
           Type_Hirshfeld = 'SIMPLE'
         else if(Type_Hirshfeld(1:1).eq.'I')then
           Type_Hirshfeld = 'ITERATIVE'
         else if(Type_Hirshfeld(1:1).eq.'R')then
           Type_Hirshfeld = 'RADIAL'
         end if
         write(UNIout,'(2a)')' Partitioning scheme set to Hirshfeld: ',Type_Hirshfeld
!
! RDel
      else if(token(1:2).eq.'RD')then
         call GET_value (RDel)
!
! NRpoints
      else if(token(1:2).eq.'NR')then
         call GET_value (NRpoints)
!
! RStart
      else if(token(1:2).eq.'RS')then
         call GET_value (RStart)

      else
         call MENU_end (done)
      end if
!
      end do !(.not.done)
!
! End of routine MENU_HIRSHFELD
      call PRG_manager ('exit', 'MENU_HIRSHFELD', 'UTILITY')
      return
      end
