#!/bin/bash

# 0) choose compiler (sun, gnu, intel, pgi or "")
#    default: cmake auto discovery

export COMPILER="sun"

# 1) define locations for 
#    * Test results       (default: "/globalscratch/${USER}/mgauss_tests")
#    * MUNgauss directory (default: "${HOME}/MUNgauss")
#    * build directory    (default: "${HOME}/MUNgauss/build")
#    * MUNgauss binary    (default: "${HOME}/MUNgauss/build/mgauss")
#

#export SCRATCH_DIR="${HOME}/public_html/mungauss"
export MGAUSS_DIR="${HOME}/MUNgauss"
#export BUILD_DIR="${MGAUSS_DIR}/build_${COMPILER}" 
export MGAUSS_BIN="${BUILD_DIR}/mgauss"

# 2) set number of threads (1 or 4)
#    When using 1 thread, adjust the "-pe" option in the SGE header. 
THREADS=4

# 3) Force clean build of mgauss?
# (builds automatically if MGAUSS_BIN does not exist)
BUILD="YES"

# 4) locations of compilers
export SUNSTUDIO=/opt/sun/sunstudio12
export PATH=${SUNSTUDIO}/bin:${PATH}
export LD_LIBRARY_PATH=${SUNSTUDIO}/rtlibs/amd64


################################################################################ 
#                NO CHANGES NEED TO BE DONE BELOW THIS LINE                    #
################################################################################ 

## SET DEFAULTS IF NOT DEFINED EXTERNALLY
# directory for output
test "${SCRATCH_DIR}" == "" && export SCRATCH_DIR="/globalscratch/${USER}/mgauss_tests"
# location of MUNgauss sources (and test cases)
test "${MGAUSS_DIR}" == ""  && export MGAUSS_DIR="${HOME}/MUNgauss"
# build directory
if   [ "${BUILD_DIR}" == "" -a "${COMPILER}" == "" ] ; then
    export BUILD_DIR="${MGAUSS_DIR}/build"
elif [ "${BUILD_DIR}" == "" ] ; then
    export BUILD_DIR="${MGAUSS_DIR}/build_${COMPILER}"
fi
# location of MUNgauss binary
test "${MGAUSS_BIN}" == ""  && export MGAUSS_BIN="${BUILD_DIR}/mgauss"

##################################################
## DETERMINE WHAT WE TEST (branch name, revision)
## AND SET TITLE AND OUTPUT DIR BASED ON THAT
##################################################
cd ${MGAUSS_DIR}

# determine name of current branch
CURRENT_GIT_BRANCH="`git rev-parse --abbrev-ref HEAD`"
BRANCH="${CURRENT_GIT_BRANCH##*/}"                        # remove prefixes like "personal/USER/" 
BRANCH="`echo \"$BRANCH\" | tr '[:lower:]' '[:upper:]'`"  # set branch name to UPPER CASE
export BRANCH
# determine revision code (short)
export CURRENT_GIT_REV="`git rev-parse --short HEAD`"
# determine hostname
if [ -f /usr/local/lib/site ] ; then
  export HOST="`cat /usr/local/lib/site`"
else
  export HOST="$HOSTNAME"
fi

# set title & subtitle
export TITLE="MUNgauss ${BRANCH} branch on `date -Idate`"
export SUBTITLE="Revision ${CURRENT_GIT_REV} on ${HOST}"
test "${COMPILER}" != "" && export SUBTITLE="${SUBTITLE} with ${COMPILER} compiler" 

# set output dir
test "${COMPILER}" != "" && SCRATCH_DIR="${SCRATCH_DIR}/mgauss_test_${BRANCH}_`date -Idate`_${COMPILER}_${HOST}"
test "${COMPILER}" == "" && SCRATCH_DIR="${SCRATCH_DIR}/mgauss_test_${BRANCH}_`date -Idate`_${HOST}"
export RESULT_DIR="${SCRATCH_DIR}"

########################################
## TESTING
########################################
echo "$TITLE"
echo "$SUBTITLE"
echo -e "################################################################################\n"

# create RESULT_DIR if not already there
test -d ${RESULT_DIR} || mkdir -p ${RESULT_DIR}
test -d ${BUILD_DIR}  || mkdir -p ${BUILD_DIR}

## BUILD
####################
test -f $MGAUSS_BIN || BUILD="YES"
MAKE_FLAGS="-j ${THREADS}" # run make in parallel

if [ "$BUILD"=="YES" ] ; then
  cd ${BUILD_DIR}
  case "$COMPILER" in
  sun)
    cmake .. -DCMAKE_Fortran_COMPILER="${SUNSTUDIO}/bin/f95"
    ;; 
  gnu)
    cmake .. -DCMAKE_Fortran_COMPILER="`which gfortran`"
    ;;
  intel)
    cmake .. -DCMAKE_Fortran_COMPILER="`which ifort`"
    ;;
  pgi)
    cmake .. -DCMAKE_Fortran_COMPILER="`which pgf95`"
    ;;
  *)
    cmake ..
  ;;
  esac
  
  make clean
  make $MAKE_FLAGS 2>&1 | tee ${RESULT_DIR}/make_mungauss.log
  cd ${MGAUSS_DIR}
fi

# RUN TESTS
####################
case "$THREADS" in
4)
    echo "Running 4 tests in parallel"
    nosetests --with-xunit -A '(set==1 or set==5) and not bench'\
              --xunit-file=${RESULT_DIR}/nosetests_mgauss_1.xml &
    nosetests --with-xunit            -A 'set==2 and not bench' \
              --xunit-file=${RESULT_DIR}/nosetests_mgauss_2.xml &
    nosetests --with-xunit            -A 'set==3 and not bench' \
              --xunit-file=${RESULT_DIR}/nosetests_mgauss_3.xml &
    nosetests --with-xunit            -A 'set==4 and not bench' \
              --xunit-file=${RESULT_DIR}/nosetests_mgauss_4.xml &
    wait
    ;;
*)
    echo "Running tests in serial"
    nosetests -a '!bench' --with-xunit --xunit-file=${RESULT_DIR}/nosetests.xml MunGauss_Test
    ;;
esac

# generate the report
cd ${RESULT_DIR}

${MGAUSS_DIR}/generate_test_report.py -t "$TITLE" -s "$SUBTITLE" -H report.html nosetests*.xml
sed -i "s%http://code.jquery.com/jquery-1.9.1.min.js%../jquery-1.9.1.min.js%" report.html
chmod -R g-w,o+rX ./

