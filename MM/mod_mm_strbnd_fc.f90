      module strbnd_fc
!***********************************************************
! Date last modified: 3 October, 1999          Version 1.0 *
! Author: Michelle Shaw                                    *
! Description: Stretch-bend force program_constants.               *
!***********************************************************
      implicit none

      double precision, dimension(52) :: STR_BND_FC_IJK
      double precision, dimension(52) :: STR_BND_FC_KJI

! Values for equilibrium angles taken from J. Comp. Chem., Vol. 17,
!  No.5&6, pp.553-586, Supplementary index, appendixBB (1996)
! Atom types are as follows:
! 1:  alkyl carbon (sp3)
! 2:  vinylic, generic sp2 carbon
! 4:  allenic, generic sp carbon
! 5:  hydrogen attached to carbon or silicon
! 20: carbon in a 4-membered ring
! 22: carbon in a 3-membered ring
! 30: olefinic carbon in a 4-membered ring
! 37:  aromatic carbon

      DATA STR_BND_FC_IJK/ &
!     111     112     115     1137    212     215     515     5120    5122    5137
      0.206,  0.136,  0.227,  0.152,  0.282,  0.234,  0.115,  0.069,  0.055,  0.074, &

!     121     122     122(2)  125     1237    222     225     225(1)  2237    525
      0.250,  0.203,  0.222,  0.215,  0.246,  0.250,  0.207,  0.267,  0.143,  0.140, &

!     5237    1205    12020   5205    52020   52030   202020  202030  1225    12222
      0.153,  0.290,  0.179,  0.182,  0.101,  0.108,  0.283,  0.340,  0.067,  0.199, &

!     5225    52222   222222  53020   53030   203030  13737   23737   53737   373737
      0.254,  0.181,  0.000,  0.251,  0.267,  0.413,  0.485,  0.321,  0.279,  -0.411, &

! For the next 12 (default) force program_constants, 1,2 refer to row in periodic table where
!    element is found and * means any atom.
!     *1*     *11     *12     *2*     *21     *22     111     112     212     121
      0.15,   0.10,   0.05,   0.00,   0.00,   0.00,   0.30,   0.30,   0.50,   0.30, &

!     122     222
      0.25,   0.25/

      DATA STR_BND_FC_KJI/ &
!     111     112     115     1137    212     215     515     5120    5122    5137
      0.206,  0.197,  0.070,  0.260,  0.282,  0.088,  0.115,  0.327,  0.267,  0.287, &

!     121     122     122(2)  125     1237    222     225     225(1)  2237    525
      0.250,  0.207,  0.269,  0.128,  0.260,  0.219,  0.157,  0.159,  0.172,  0.140, &

!     5237    1205    12020   5205    52020   52030   202020  202030  1225    12222
      0.288,  0.098,  0.004,  0.182,  0.079,  0.123,  0.283,  0.529,  0.174,  0.039, &

!     5225    52222   222222  53020   53030   203030  13737   23737   53737   373737
      0.254,  0.108,  0.000,  0.007,  0.054,  0.705,  0.311,  0.235,  0.250,  -0.411, &

! For the next 12 (default) force program_constants, 1,2 refer to row in periodic table where
!    element is found and * means any atom.
!     *1*     *11     *12     *2*     *21     *22     111     112     212     121
      0.15,   0.30,   0.35,   0.00,   0.15,   0.15,   0.30,   0.50,   0.50,   0.30, &

!     122     222
      0.25,   0.25/

      end module strbnd_fc
