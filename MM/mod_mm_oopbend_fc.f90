      module oopbend_fc
!****************************************************************
! Date last modified: 5 September, 1999             Version 1.0 *
! Author: Michelle Shaw                                         *
! Description: Out-of-plane bend parameters for hydrocarbons    *
!****************************************************************
! Modules:
      USE program_constants

      implicit none

      double precision, dimension(14) :: OOPBEND_FORCE_CONST
      double precision :: FC_conv_factor=ANGSTROM_TO_BOHR/FCONV !mdyne*A/rad**2 to hartrees/rad**2

! Values for torsional parameters taken from J. Comp. Chem., Vol. 17, No.5&6, pp. 553-586
!  Supplementary material, appendixBB (1996)
! Atom types are as follows:
! 1:  alkyl carbon (sp3)
! 2:  vinylic, generic sp2 carbon
! 5:  hydrogen attached to carbon or silicon
! 20: carbon in 4-membered ring
! 22: carbon in 3-membered ring
! 30: olefinic carbon in 4-membered ring
! 37:  aromatic carbon

      DATA OOPBEND_FORCE_CONST/ &
!     1212    1222    1225    12237   2225    2255    22537   5302030  1373737  2373737
      0.030,  0.027,  0.013,  0.032,  0.013,  0.006,  0.017,  0.008,   0.040,   0.031, &
!
!     5373737 *2**    *30**   *37**
      0.015,  0.020,  0.010,  0.035/

      end module oopbend_fc
