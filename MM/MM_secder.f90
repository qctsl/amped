      subroutine BLD_MM_2ND_DERIVATIVE
!*******************************************************************************
! Date last modified: 4 April, 2000                                Version 1.0 *
! Author: Michelle Shaw                                                        *
! Description: Combines 2nd_derivative contributions for all interactions in   *
!              an object called MM_2nd_derivative.                             *
!*******************************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE GRAPH_objects
      USE mm_second_derivatives
      USE matrix_print

      implicit none
!
! Local scalars:
      integer Ntotal
!
! Local functions:

! Begin:
      call PRG_manager ('enter', 'BLD_MM_2ND_DERIVATIVE', 'UTILITY')
!
! Object:
      if(.not.allocated(MM_2nd_derivative))then
        call GET_object ('MM', 'PARAMETER_CONTRIBUTIONS', 'MMFF94')
        allocate (MM_2nd_derivative(RIC%NIcoordinates, RIC%NIcoordinates))
      end if

      if(RIC%NBonds.gt.0)then
        call SECDER_STR
      end if
      if(RIC%NAngles.gt.0)then
        call SECDER_BND
      end if
      if(RIC%NTorsions.gt.0)then
        call SECDER_TOR
      end if
      if(RIC%Noopbends.gt.0)then
        call SECDER_OBND
      end if
      if(RIC%Nnonbonded.gt.0)then
        call SECDER_VDW
        call SECDER_ELEC
      end if
      if(RIC%NAngles.gt.0)then
        call SECDER_STB
      end if

! Parameters are combined here, starting with the stretches, then the bends, and finally the
!    torsions.

      MM_2nd_derivative = ZERO

      Ntotal=RIC%Nbonds
      MM_2nd_derivative(1:Ntotal,1:Ntotal)=F_stretch(1:Ntotal,1:Ntotal)+ &
                                F_str_bend(1:Ntotal,1:Ntotal)+ &
                                F_str_bend(1:Ntotal,Ntotal+1:2*Ntotal)+ &
                                F_str_bend(Ntotal+1:2*Ntotal,1:Ntotal)+ &
                                F_str_bend(Ntotal+1:2*Ntotal,Ntotal+1:2*Ntotal)
      if(RIC%Nangles.gt.0)then
        MM_2nd_derivative(Ntotal+1:Ntotal+RIC%Nangles,Ntotal+1:Ntotal+RIC%Nangles)= &
               F_bend(1:RIC%Nangles,1:RIC%Nangles)+ &
               F_str_bend(2*Ntotal+1:2*Ntotal+RIC%Nangles,2*Ntotal+1:2*Ntotal+RIC%Nangles)
        MM_2nd_derivative(1:Ntotal,Ntotal+1:Ntotal+RIC%Nangles)= &
               F_str_bend(1:Ntotal,2*Ntotal+1:2*Ntotal+RIC%Nangles)+ &
               F_str_bend(Ntotal+1:2*Ntotal,2*Ntotal+1:2*Ntotal+RIC%Nangles)
        MM_2nd_derivative(Ntotal+1:Ntotal+RIC%Nangles,1:Ntotal)= &
               F_str_bend(2*Ntotal+1:2*Ntotal+RIC%Nangles,1:Ntotal)+ &
               F_str_bend(2*Ntotal+1:2*Ntotal+RIC%Nangles,Ntotal+1:2*Ntotal)
        Ntotal=Ntotal+RIC%Nangles
      end if

      if(RIC%Ntorsions.gt.0)then
        MM_2nd_derivative(Ntotal+1:Ntotal+RIC%Ntorsions,Ntotal+1:Ntotal+RIC%Ntorsions)= &
        F_torsion(1:RIC%Ntorsions,1:RIC%Ntorsions)
        Ntotal=Ntotal+RIC%Ntorsions
      end if

      if(RIC%Noopbends.gt.0)then
        MM_2nd_derivative(Ntotal+1:Ntotal+RIC%Noopbends,Ntotal+1:Ntotal+RIC%Noopbends)= &
                F_oopbend(1:RIC%Noopbends,1:RIC%Noopbends)
        Ntotal=Ntotal+RIC%Noopbends
      end if
!
      if(RIC%Nnonbonded.gt.0) then
        MM_2nd_derivative(Ntotal+1:Ntotal+RIC%Nnonbonded,Ntotal+1:Ntotal+RIC%Nnonbonded)= &
        F_VderWs(1:RIC%Nnonbonded,1:RIC%Nnonbonded)+F_electrostatic(1:RIC%Nnonbonded,1:RIC%Nnonbonded)
      end if

! Print object information:
!      write(UNIout,*)'Second derivative contributions: '
!      if(Ntotal.gt.0) then
!        write(UNIout,*)'Number of stretch second derivatives: ',Nstretchs
!        write(UNIout,*)'Stretch contributions: '
!        call PRT_matrix (F_stretch, Ntotal, Ntotal)
!      end if
!      if(NGANGLE.gt.0) then
!        write(UNIout,*)'Number of bend second derivatives: ',Nbends
!        write(UNIout,*)'Bend contributions: '
!        call PRT_matrix (F_bend, NGANGLE, NGANGLE)
!        write(UNIout,*)'Number of stretch-bend second derivatives: ',Nstr_bends
!        write(UNIout,*)'Stretch-Bend contributions: '
!        call PRT_matrix (F_str_bend, (2*Ntotal+NGANGLE), (2*Ntotal+NGANGLE))
!      end if
!      if(NGTORSION.gt.0) then
!        write(UNIout,*)'Number of torsion second derivatives: ',Ntorsions
!        write(UNIout,*)'Torsion contributions: '
!        call PRT_matrix (F_torsion, NGTORSION, NGTORSION)
!      end if
!      if(NOOPBEND.gt.0) then
!        write(UNIout,*)'Number of out-of-plane bend second derivatives: ',Noopbends
!        write(UNIout,*)'Out-of-plane bend contributions: '
!        call PRT_matrix (F_oopbend, NOOPBEND, NOOPBEND)
!      end if
!      if(NELEC.gt.0) then
!        write(UNIout,*)'Number of nonbonded second derivatives: ',Nelectrostatic
!        write(UNIout,*)'Nonbonded contributions: '
!        call PRT_matrix ((F_VderWs+F_electrostatic), NELEC, NELEC)
!      end if
!      write(UNIout,*)'MM_2nd_derivative: '
!      call PRT_matrix (MM_2nd_derivative, MAXDIM, MAXDIM)

! End of routine BLD_MM_2ND_DERIVATIVE
      call PRG_manager ('exit', 'BLD_MM_2ND_DERIVATIVE', 'UTILITY')
      return
      end subroutine BLD_MM_2ND_DERIVATIVE
