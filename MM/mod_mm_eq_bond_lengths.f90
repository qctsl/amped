      module eq_bond_lengths

!*****************************************************************
!  Date last modified: 10 July, 1999                version 1.0  *
!  Author: Michelle Shaw                                         *
!  Description: Equilibrium bond lengths for Carbon and Hydrogen *
!*****************************************************************
! Modules:
      USE program_constants

      implicit none

      double precision, dimension(34) :: EQUILIBRIUM_BOND_LENGTH_SINGLE
      double precision, dimension(1) :: EQUILIBRIUM_BOND_LENGTH_DOUBLE
      double precision, dimension(12) :: EQUILIBRIUM_BOND_LENGTH_TRIPLE
      double precision, dimension(2) :: EQ_BOND_LENGTH_AROMATIC

!     Data obtained from two sources:
!        1. CRC Handbook of Chemistry and Physics, 71-st Edition, p. 9-1(H-H bond)
!        2. J. Comp. Chem., v.17, no.5&6, pp. 553-586 (1996) appendixB
!        3. J. Comp. Chem., v.10, no.8, pp.982-1012 (C.1-C.1 bond)
      DATA EQUILIBRIUM_BOND_LENGTH_SINGLE/ &
!     H         C.1        C.2       C.3       C.ar    C.3M    C.4M    C.O4M
      0.746,                                           1.082,  1.093,  1.086, & !H
      1.065,    1.380,    1.415,    1.459,    1.424,   1.426,  1.436,  1.282, & !C.1
      1.083,              1.430,    1.482,    1.449,   1.448,  1.465,  1.331, & !C.2
      1.093,                        1.508,             1.482,  1.504,         & !C.3
      1.084,                        1.486,    1.436,   1.471,  1.516,         & !C.ar
                                                      1.499,  1.484,  1.513, & !C.3M
                                                              1.526,  1.507, & !C.4M
                                                                      1.343/  !C.O4M

      DATA EQUILIBRIUM_BOND_LENGTH_DOUBLE/1.333/ !C.2

      DATA EQUILIBRIUM_BOND_LENGTH_TRIPLE/1.200, 11*0.0D0/ !C.1
!                                    C.2    C.ar
      DATA EQ_BOND_LENGTH_AROMATIC/1.297,1.374/

      end module eq_bond_lengths
