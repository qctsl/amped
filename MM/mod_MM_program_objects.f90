      subroutine BLD_MM_objects
!****************************************************************************************************************
!       Date last modified June 23, 2000                                                                        *
!       Author: Darryl Reid and R.A. Poirier                                                                    *
!       Description:  Builds a complete list of all the objects which can be created in MUNgauss                *
!       When adding a class or object within a class ensure it is done in alphabetical order                    *
!****************************************************************************************************************
! Modules:
      USE program_objects

      implicit none

! Local scalar:
      integer :: Iobject
!
! Begin:
      OBJ_MM(1:Max_objects)%modality = 'other'
      OBJ_MM(1:Max_objects)%class = 'MM'
      OBJ_MM(1:Max_objects)%depend = .true.
      NMMobjects = 0
!
! Class Molecular Mechanics (MM)
      NMMobjects = NMMobjects + 1
      OBJ_MM(NMMobjects)%name = 'ENERGY'
      OBJ_MM(NMMobjects)%modality = 'MMFF94'
      OBJ_MM(NMMobjects)%routine = 'CLC_ENERGY_MM'
!
      NMMobjects = NMMobjects + 1
      OBJ_MM(NMMobjects)%name = 'ATOM_TYPES'
      OBJ_MM(NMMobjects)%modality = 'MMFF94'
      OBJ_MM(NMMobjects)%routine = 'BLD_MM_ATOM_TYPES'
!
      NMMobjects = NMMobjects + 1
      OBJ_MM(NMMobjects)%name = 'ENERGY_CONTRIBUTIONS'
      OBJ_MM(NMMobjects)%modality = 'MMFF94'
      OBJ_MM(NMMobjects)%routine = 'BLD_MM_ENERGIES'
!
      NMMobjects = NMMobjects + 1
      OBJ_MM(NMMobjects)%name = 'PARAMETER_CONTRIBUTIONS'
      OBJ_MM(NMMobjects)%modality = 'MMFF94'
      OBJ_MM(NMMobjects)%routine = 'BLD_MM_PARAMETERS'
      OBJ_MM(NMMobjects)%depend = .false.
!
      NMMobjects = NMMobjects + 1
      OBJ_MM(NMMobjects)%name = 'GRADIENTS'
      OBJ_MM(NMMobjects)%modality = 'MMFF94'
      OBJ_MM(NMMobjects)%routine = 'BLD_MM_GRADIENTS'
!
      NMMobjects = NMMobjects + 1
      OBJ_MM(NMMobjects)%name = 'FORCE_CONSTANT'
      OBJ_MM(NMMobjects)%modality = 'MMFF94'
      OBJ_MM(NMMobjects)%routine = 'BLD_MM_2ND_DERIVATIVE'
!
! Dummy Class
      NMMobjects = NMMobjects + 1
      OBJ_MM(NMMobjects)%name = '?'
      OBJ_MM(NMMobjects)%modality = '?'
      OBJ_MM(NMMobjects)%routine = '?'
!
      do Iobject=1,NMMobjects
        OBJ_MM(Iobject)%exist=.false.
        OBJ_MM(Iobject)%Current=.false.
      end do

      return
      end subroutine BLD_MM_objects
