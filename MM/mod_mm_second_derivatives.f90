      module mm_second_derivatives
!*****************************************************************************
! Date last modified: 31 March, 2000                             Version 1.0 *
! Author: Michelle Shaw                                                      *
! Description: Contains objects and initialization routines for the second   *
!              derivatives of the MM energy.                                 *
!*****************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE mm_parameters
      USE GRAPH_objects
      USE atom_type
      USE nobond_params

      implicit none

      double precision, dimension(:,:), allocatable :: F_stretch
      double precision, dimension(:,:), allocatable :: F_bend
      double precision, dimension(:,:), allocatable :: F_torsion
      double precision, dimension(:,:), allocatable :: F_oopbend
      double precision, dimension(:,:), allocatable :: F_VderWs
      double precision, dimension(:,:), allocatable :: F_electrostatic
      double precision, dimension(:,:), allocatable :: F_str_bend

      double precision, dimension(:,:), allocatable :: MM_2nd_derivative

CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine SECDER_STR
!**************************************************************************
! Date last modified: 31 March, 2000                          Version 1.0 *
! Author: Michelle Shaw                                                   *
! Description: Calculates the second derivatives of the stretch energy.   *
!**************************************************************************
      implicit none
!
! Local scalars:
      integer iSTR
      integer iATM,jATM,Nstretchs
      double precision cs,term,C,DIFF,V_Stretch_2nd_derivative
!
! Begin:
      call PRG_manager ('enter', 'SECDER_STR', 'UTILITY')

      call GET_object ('MM', 'PARAMETER_CONTRIBUTIONS', 'MMFF94')
!
! Allocate work arrays:
      Nstretchs=RIC%Nbonds
      if(.not.allocated(F_stretch))then
        allocate (F_stretch(Nstretchs,Nstretchs))
      end if

! Initialize objects:
      F_stretch=ZERO

! The second derivative of the stretch energy with respect to the input bond length will be
! calculated...
      cs = -TWO
      term = (7.0D0/12.0D0)*(cs**2)
      do iSTR=1,Nstretchs
        C = 143.9325D0*MMstretch(iSTR)%k_ij
        DIFF = (RIC%bond(iSTR)%value*Bohr_to_Angstrom) - MMstretch(iSTR)%R_ij
        V_Stretch_2nd_derivative = C*(ONE + cs*DIFF + term*(DIFF**2)) + &
                                   TWO*C*DIFF*(cs+TWO*term*DIFF) + term*C*(DIFF**2)
        F_stretch(iSTR,iSTR)=V_Stretch_2nd_derivative*(Cal_to_J*(Bohr_to_Angstrom**2)/hartree_to_kJ)
      end do

! End of routine SECDER_STR
      call PRG_manager ('exit', 'SECDER_STR', 'UTILITY')
      return
      end subroutine SECDER_STR
      subroutine SECDER_BND
!**************************************************************************
! Date last modified: 1 April, 200                            Version 1.0 *
! Author: Michelle Shaw                                                   *
! Description: Calculates the second derivatives of the bend energy.      *
!**************************************************************************
      implicit none

! Local scalars:
      integer iBND
      integer iATM,jATM,kATM,Nbends
      double precision A, DIFF, cb,V_Bend_2nd_derivative
!
! Begin:
      call PRG_manager ('enter', 'SECDER_BND', 'UTILITY')

! Allocate work arrays:
      Nbends=RIC%NAngles
      if(.not.allocated(F_bend))then
        allocate (F_bend(Nbends,Nbends))
      end if

! Initialize objects:
      F_bend=ZERO

! The second derivative of the bend energy with respect to a change in input angle is calculated...
      cb = -7.0D-03
      do iBND=1,Nbends
        DIFF = (RIC%angle(iBND)%value/Deg_to_Radian) - MMbend(iBND)%theta_ijk
        A = 0.043844D0*MMbend(iBND)%k_ijk
        V_Bend_2nd_derivative = A*(ONE + cb*DIFF) + 2.0D0*A*DIFF*cb
        F_bend(iBND,iBND)=V_Bend_2nd_derivative*(Cal_to_J/((Deg_to_Radian**2)*hartree_to_kJ))
      end do

! End of routine SECDER_BND
      call PRG_manager ('exit', 'SECDER_BND', 'UTILITY')
      return
      end subroutine SECDER_BND
      subroutine SECDER_TOR
!**************************************************************************
! Date last modified: 1 April, 2000                           Version 1.0 *
! Author: Michelle Shaw                                                   *
! Description: Calculates the second derivatives of the torsion energy.   *
!**************************************************************************
      implicit none
!
! Local scalars:
      integer iTOR,Ntorsions
      double precision V_Torsion_2nd_derivative
!
! Begin:
      call PRG_manager ('enter', 'SECDER_TOR', 'UTILITY')

! Allocate work arrays:
      Ntorsions=RIC%Ntorsions
      if(.not.allocated(F_torsion))then
        allocate (F_torsion(Ntorsions,Ntorsions))
      end if

! Initialize objects:
      F_torsion=ZERO

! The second derivative of the torsion energy with respect to a change in input angle is calculated...
      do iTOR=1,Ntorsions
        V_Torsion_2nd_derivative = -(MMtorsion(iTOR)%V1/TWO)*(DCOS(RIC%torsion(iTOR)%value)) + &
                                   MMtorsion(iTOR)%V2*(TWO*DCOS(TWO*RIC%torsion(iTOR)%value)) - &
                                   1.5D0*MMtorsion(iTOR)%V3*(THREE*DCOS(THREE*RIC%torsion(iTOR)%value))
        V_Torsion_2nd_derivative = dabs(V_Torsion_2nd_derivative)            ! ???????
        F_torsion(iTOR,iTOR)=V_Torsion_2nd_derivative*(Cal_to_J/hartree_to_kJ)
      end do

! End of routine SECDER_TOR
      call PRG_manager ('exit', 'SECDER_TOR', 'UTILITY')
      return
      end subroutine SECDER_TOR
      subroutine SECDER_OBND
!**************************************************************************
! Date last modified: 1 April, 2000                           Version 1.0 *
! Author: Michelle Shaw                                                   *
! Description: Calculates the second derivatives of the out-of-plane bend *
!               energy.                                                   *
!**************************************************************************
      implicit none
!
! Local scalars:
      integer iOBND,Noopbends
      double precision V_Oopbend_2nd_derivative
!
! Begin:
      call PRG_manager ('enter', 'SECDER_OBND', 'UTILITY')

! Allocate work arrays:
      Noopbends=RIC%Noopbends
      if(.not.allocated(F_oopbend))then
        allocate (F_oopbend(Noopbends,Noopbends))
      end if

! Initialize objects:
      F_oopbend=ZERO

! The second derivative of the out-of-plane bend energy is calculated here...
      do iOBND=1,Noopbends
        V_Oopbend_2nd_derivative = 0.043844*MMoopbend_k_ijkl(iOBND)
        F_oopbend(iOBND,iOBND)=V_Oopbend_2nd_derivative*(Cal_to_J/((Deg_to_Radian**2)*hartree_to_kJ))
        if(F_oopbend(iOBND,iOBND).eq.ZERO)F_oopbend(iOBND,iOBND)=0.002 ! FOr now (RAP)
      end do

! End of routine SECDER_OBND
      call PRG_manager ('exit', 'SECDER_OBND', 'UTILITY')
      return
      end subroutine SECDER_OBND
      subroutine SECDER_STB
!**************************************************************************
! Date last modified: 1 April, 2000                           Version 1.0 *
! Author: Michelle Shaw                                                   *
! Description: Calculates the second derivatives of the stretch-bend      *
!              energy.                                                    *
!**************************************************************************
      implicit none
!
! Local scalars:
      integer iSTRBND, jSTRBND
      integer iSTR,iBND,jSTR,jBND,irange,jrange,Nstr_bends
      double precision V_Stretch_bend_2nd_derivative
!
! Begin:
      call PRG_manager ('enter', 'SECDER_STB', 'UTILITY')

! Allocate work arrays:
      Nstr_bends=2*RIC%Nbonds+RIC%Nangles
      if(.not.allocated(F_str_bend))then
        allocate (F_str_bend(Nstr_bends,Nstr_bends))
      end if

      F_str_bend=ZERO

! The second derivative of the stretch-bend energy with respect to the input bond length will be
! calculated...
        do iSTRBND=1,Nstr_bends
          do jSTRBND=1,Nstr_bends
      V_Stretch_bend_2nd_derivative = ZERO
      iSTR = 0
      iBND = 0
      jSTR = 0
      jBND = 0

! Choose range...
      if(iSTRBND.le.RIC%Nbonds)then
        irange = 1
      else if(iSTRBND.gt.RIC%Nbonds.and.iSTRBND.le.2*RIC%Nbonds)then
        irange = 2
      else if(iSTRBND.gt.2*RIC%Nbonds.and.iSTRBND.le.2*RIC%Nbonds+RIC%Nangles)then
        irange = 3
      else
        irange = 0
      end if
      if(jSTRBND.le.RIC%Nbonds)then
        jrange = 1
      else if(jSTRBND.gt.RIC%Nbonds.and.jSTRBND.le.2*RIC%Nbonds)then
        jrange = 2
      else if(jSTRBND.gt.2*RIC%Nbonds.and.jSTRBND.le.2*RIC%Nbonds+RIC%Nangles)then
        jrange = 3
      else
        jrange = 0
      end if

      IDIM:SELECT CASE(irange)
        CASE(1) IDIM
          iSTR = iSTRBND !stretch index for ij bond (obtained from iSTRBND)
          JDIM:SELECT CASE(jrange)
            CASE(1) JDIM
!ij-ij second derivative
              V_Stretch_bend_2nd_derivative = ZERO
            CASE(2) JDIM
!ij-jk second derivative
              V_Stretch_bend_2nd_derivative = ZERO
            CASE(3) JDIM
!ij-angle second derivative
              jBND = jSTRBND - 2*RIC%Nbonds !bend index for angle (obtained from jSTRBND)
              if((BOND(iSTR)%ATOM1.eq.ANGLE(jBND)%ATOM1).and. &
                 (BOND(iSTR)%ATOM2.eq.ANGLE(jBND)%ATOM2))then
                V_Stretch_bend_2nd_derivative = 2.51210D0*MMstr_bend(jBND)%k_ijk
              else if((BOND(iSTR)%ATOM2.eq.ANGLE(jBND)%ATOM1).and. &
                      (BOND(iSTR)%ATOM1.eq.ANGLE(jBND)%ATOM2))then
                V_Stretch_bend_2nd_derivative = 2.51210D0*MMstr_bend(jBND)%k_ijk
              else
                V_Stretch_bend_2nd_derivative = ZERO
              end if
            CASE DEFAULT JDIM
              write(UNIout,'(a)')'Error in second dimension of stretch-bend second derivative matrix'
          end SELECT JDIM
        CASE(2) IDIM
          iSTR = iSTRBND - RIC%Nbonds !stretch index for jk bond (obtained from iSTRBND)
          JDIM2:SELECT CASE(jrange)
            CASE(1) JDIM2
!jk-ij second derivative
              V_Stretch_bend_2nd_derivative = ZERO
            CASE(2) JDIM2
!jk-jk second derivative
              V_Stretch_bend_2nd_derivative = ZERO
            CASE(3) JDIM2
!jk-angle second derivative
              jBND = jSTRBND - 2*RIC%Nbonds
              if((BOND(iSTR)%ATOM1.eq.ANGLE(jBND)%ATOM3).and. &
                 (BOND(iSTR)%ATOM2.eq.ANGLE(jBND)%ATOM2))then
                V_Stretch_bend_2nd_derivative = 2.51210D0*MMstr_bend(jBND)%k_kji
              else if((BOND(iSTR)%ATOM2.eq.ANGLE(jBND)%ATOM3).and. &
                      (BOND(iSTR)%ATOM1.eq.ANGLE(jBND)%ATOM2))then
                V_Stretch_bend_2nd_derivative = 2.51210D0*MMstr_bend(jBND)%k_kji
              else
                V_Stretch_bend_2nd_derivative = ZERO
              end if
            CASE DEFAULT JDIM2
              write(UNIout,'(a)')'Error in second dimension of stretch-bend second derivative matrix'
          end SELECT JDIM2
        CASE(3) IDIM
          iBND = iSTRBND - 2*RIC%Nbonds
          JDIM3:SELECT CASE(jrange)
            CASE(1) JDIM3
!angle-ij second derivative
              jSTR = jSTRBND
              if((BOND(jSTR)%ATOM1.eq.ANGLE(iBND)%ATOM1).and. &
                 (BOND(jSTR)%ATOM2.eq.ANGLE(iBND)%ATOM2))then
                V_Stretch_bend_2nd_derivative = 2.51210D0*MMstr_bend(iBND)%k_ijk
              else if((BOND(jSTR)%ATOM2.eq.ANGLE(iBND)%ATOM1).and. &
                      (BOND(jSTR)%ATOM1.eq.ANGLE(iBND)%ATOM2))then
                V_Stretch_bend_2nd_derivative = 2.51210D0*MMstr_bend(iBND)%k_ijk
              else
                V_Stretch_bend_2nd_derivative = ZERO
              end if
            CASE(2) JDIM3
!angle-jk second derivative
              jSTR = jSTRBND - RIC%Nbonds
              if((BOND(jSTR)%ATOM1.eq.ANGLE(iBND)%ATOM3).and. &
                 (BOND(jSTR)%ATOM2.eq.ANGLE(iBND)%ATOM2))then
                V_Stretch_bend_2nd_derivative = 2.51210D0*MMstr_bend(iBND)%k_kji
              else if((BOND(jSTR)%ATOM2.eq.ANGLE(iBND)%ATOM3).and. &
                      (BOND(jSTR)%ATOM1.eq.ANGLE(iBND)%ATOM2))then
                V_Stretch_bend_2nd_derivative = 2.51210D0*MMstr_bend(iBND)%k_kji
              else
                V_Stretch_bend_2nd_derivative = ZERO
              end if
            CASE(3) JDIM3
!angle-angle second derivative
              V_Stretch_bend_2nd_derivative = ZERO
            CASE DEFAULT JDIM3
              write(UNIout,'(a)')'Error in second dimension of stretch-bend second derivative matrix'
          end SELECT JDIM3
        CASE DEFAULT IDIM
          write(UNIout,'(a)')'Error in first dimension of stretch-bend second derivative matrix'
      end SELECT IDIM
            F_str_bend(iSTRBND, jSTRBND)= &
            V_Stretch_bend_2nd_derivative*((Cal_to_J*Bohr_to_Angstrom)/(Deg_to_Radian*hartree_to_kJ)) 
          end do
        end do

! End of routine SECDER_STB
      call PRG_manager ('exit', 'SECDER_STB', 'UTILITY')
      return
      end subroutine SECDER_STB
      subroutine SECDER_VDW
!****************************************************************************
! Date last modified: 1 April, 2000                             Version 1.0 *
! Author: Michelle Shaw                                                     *
! Description: Calculate (analytical) second derivatives of van der Waals   *
!              energies and store them in F_VderWs.               *
!****************************************************************************
      implicit none
!
! Local scalars:
      integer iELEC
      integer iATM,jATM,NVderWs
      double precision eps,term1,term2,t1,t2,term3,term4,t4,R_ij,R0_ij,R_7,R0_7,V_Vanderwaals_2nd_derivative
!
! Begin:
      call PRG_manager ('enter', 'SECDER_VDW', 'UTILITY')

! Allocate work arrays:
      NVderWs=RIC%Nnonbonded
      if(.not.allocated(F_VderWs))then
        allocate (F_VderWs(NVderWs,NVderWs))
      end if

! Calculate the second derivatives of the van der Waals energies obtained from Maple using the energy
!   energy expression from MMFF94.

      F_VderWs=ZERO
        t1 = 89.92376264D0
        t2 = 176.2505748D0
        t4 = -75.53596062D0
        do iELEC=1,NVderWs
      if(pair_IJ(iELEC)%is_vdw.eq.1)then
        R0_ij=MMVderW(iELEC)%R_IJ
        R0_7=R0_IJ**7
        eps=MMVderW(iELEC)%epsilon_IJ
        R_ij = RIC%nonbonded(iELEC)%value*Bohr_to_Angstrom
        R_7=R_ij**7
        term1 = R0_7*((1.12D0*R0_7)/(R_7 + 0.12D0*R0_7) - 2.0D0)/(R_ij + 0.07D0*R0_IJ)**9
        term2 = (R0_IJ**14)*(R_ij**6)/(R_ij + 0.07D0*R0_IJ)**8*(R_7+0.12D0*R0_7)**2
        term3 = (R0_IJ**14)*(R_ij**12)/(R_ij+0.07D0*R0_IJ)**7*(R_7+0.12D0*R0_7)**3
        term4 = (R0_IJ**14)*(R_ij**5)/(R_ij + 0.07D0*R0_IJ)**7*(R_7+0.12D0*R0_7)**2
        V_Vanderwaals_2nd_derivative=eps*(t1*term1 + t2*term2 + t2*term3 + t4*term4)
      end if
          F_VderWs(iELEC,iELEC)=V_Vanderwaals_2nd_derivative* &
                                                    ((Cal_to_J*Bohr_to_Angstrom**2)/hartree_to_kJ)
        end do

! End of routine SECDER_VDW
      call PRG_manager ('exit', 'SECDER_VDW', 'UTILITY')
      return
      end subroutine SECDER_VDW
      subroutine SECDER_ELEC
!****************************************************************************
! Date last modified: 1 April, 2000                             Version 1.0 *
! Author: Michelle Shaw                                                     *
! Description: Calculate (analytical) second derivatives of electrostatic   *
!              energies and store them in F_electrostatic.    *
!****************************************************************************
      implicit none
!
! Local scalars:
      integer iELEC
      integer iATM,jATM,Nelectrostatic
      double precision eterm1,dist_ij,V_Electrostatic_2nd_derivative
!
! Begin:
      call PRG_manager ('enter', 'SECDER_ELEC', 'UTILITY')

! Allocate work arrays:
      Nelectrostatic=RIC%Nnonbonded
      if(.not.allocated(F_electrostatic))then
        allocate (F_electrostatic(Nelectrostatic,Nelectrostatic))
      end if

! Calculate the second derivatives of the van der Waals energies obtained from Maple using the energy
!   energy expression from MMFF94.
      F_electrostatic=ZERO

        do iELEC=1,Nelectrostatic
      if(pair_IJ(iELEC)%is_elec.eq.1)then
        dist_ij = RIC%nonbonded(iELEC)%value*Bohr_to_Angstrom
        eterm1 = 332.0716D0*MMelectrostatic(iELEC)%q_I* &
                 MMelectrostatic(iELEC)%q_J*n*(n+1)/(dielec_const*(dist_ij+delta)**(n+2))
        V_Electrostatic_2nd_derivative = eterm1
        if(pair_IJ(iELEC)%is_tors.eq.1)then
          V_Electrostatic_2nd_derivative = V_Electrostatic_2nd_derivative*scalef_14
        end if
      end if
      F_electrostatic(iELEC, iELEC)=V_Electrostatic_2nd_derivative* &
                                 ((Cal_to_J*Bohr_to_Angstrom**2)/hartree_to_kJ)

! Scale second derivatives if the nonbonded atoms are separated by three  bonds.
          if(pair_IJ(iELEC)%is_tors.eq.1)then
            F_electrostatic(iELEC, iELEC)=F_electrostatic(iELEC, iELEC)*scalef_14
          end if
        end do

! End of routine SECDER_ELEC
      call PRG_manager ('exit', 'SECDER_ELEC', 'UTILITY')
      return
      end subroutine SECDER_ELEC

      end module mm_second_derivatives
