      module mm_parameters
!*********************************************************************************
! Date last modified: 22 March, 2000                                 Version 1.0 *
! Author: Michelle Shaw                                                          *
! Description: Contains object and initialization routines for stretch           *
!              parameters.                                                       *
!*********************************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE mm_interaction_type_params
      USE nobond_params
      USE GRAPH_objects
      USE atom_type

      implicit none

      type stretch_contributions
        double precision :: k_ij !stretch force constant
        double precision :: R_ij !equilibrium bond length
      end type stretch_contributions

      type bend_contributions
        double precision :: k_ijk !bend force constant
        double precision :: theta_ijk !equilibrium angle
      end type bend_contributions

      type torsion_contributions
        double precision :: V1, V2, V3 !torsion energy expression parameters
      end type torsion_contributions

      type vanderwaals_contributions
        double precision :: R_IJ, epsilon_IJ
      end type vanderwaals_contributions

      type electrostatic_contributions
        double precision :: q_I, q_j
      end type electrostatic_contributions

      type stretch_bend_contributions
        double precision :: k_ijk, k_kji !stretch-bend force program_constants
      end type stretch_bend_contributions

      type (stretch_contributions), dimension(:), allocatable :: MMstretch
      type (bend_contributions), dimension(:), allocatable :: MMbend
      type (torsion_contributions), dimension(:), allocatable :: MMtorsion
      type (vanderwaals_contributions), dimension(:), allocatable :: MMVderW
      type (electrostatic_contributions), dimension(:), allocatable :: MMelectrostatic
      type (stretch_bend_contributions), dimension(:), allocatable :: MMstr_bend
      double precision, dimension(:), allocatable :: MMoopbend_k_ijkl !out-of-plane bend force constant

      CONTAINS

      subroutine GETBOND_PARAMS
!*****************************************************************
! Date last modified: 29 March, 2000                 Version 1.0 *
! Author: Michelle Shaw                                          *
! Description: Get equilibrium bond lengths                      *
!*****************************************************************
! Modules:
      USE program_files
      USE eq_bond_lengths
      USE force_program_constants_stretch

      implicit none

! Local scalars:
      integer iSTR, iATM, jATM, type_i, type_j, type_bond
!
! Local functions:
!
! Begin:
      call PRG_manager ('enter', 'GETBOND_PARAMS', 'UTILITY')


! Allocate work arrays:
      if(.not.allocated(MMstretch))then
        allocate(MMstretch(RIC%Nbonds))
      end if

! Calculation of the individual stretch energy contributions is done here...
! The information about each bond must be collected.  The equilibrium bond distance must be
!  retrieved from the equilibrium bond length module.
      do iSTR = 1,RIC%Nbonds
        iATM = BOND(iSTR)%ATOM1
        jATM = BOND(iSTR)%ATOM2
        type_i = TATOM(iATM)%type_mm
        type_j = TATOM(jATM)%type_mm
        type_bond = BOND(iSTR)%type

        write(UNIout,'(a,3i8)')'GETBOND_PARAMS: type_i, type_j, type_bond: ',type_i,type_j,type_bond

        typeI:SELECT CASE(CARTESIAN(iATM)%Atomic_number)
          CASE (6) typeI
            typeJ1:SELECT CASE(CARTESIAN(jATM)%Atomic_number)
              CASE (6) typeJ1 ! Case of C-C bond:
                call CC_BOND_PARM(type_i, type_j, type_bond, MMstretch(iSTR)%R_ij, MMstretch(iSTR)%k_ij)
              CASE (1) typeJ1 ! Case of C-H bond:
                call HC_BOND_PARM (type_i, MMstretch(iSTR)%R_ij, MMstretch(iSTR)%k_ij)
              CASE DEFAULT typeJ1 ! Bond is not currently defined:
                write(UNIout,'(a)')'GETBOND_PARAMS> Atom type not supported or is a dummy'
                MMstretch(iSTR)%R_ij = ZERO
                MMstretch(iSTR)%k_ij = ZERO
            end SELECT typeJ1
          CASE (1) typeI
            typeJ2:SELECT CASE(type_j)
              CASE (6) typeJ2 ! Case of H-C bond:
                call HC_BOND_PARM(type_j, MMstretch(iSTR)%R_ij, MMstretch(iSTR)%k_ij)
              CASE (1) typeJ2 ! Case of H-H bond:
                MMstretch(iSTR)%R_ij = EQUILIBRIUM_BOND_LENGTH_SINGLE(1)
                MMstretch(iSTR)%k_ij = FORCE_CONSTANTS_SINGLE_BOND(1)
              CASE DEFAULT typeJ2 ! Bond is not currently defined:
                write(UNIout,'(a)')'GETBOND_PARAMS> Atom type not supported or is a dummy'
                MMstretch(iSTR)%R_ij = ZERO
                MMstretch(iSTR)%k_ij = ZERO
            end SELECT typeJ2
          CASE DEFAULT typeI ! Bond is not currently defined:
            write(UNIout,'(a)')'GETBOND_PARAMS> Atom type not supported or is a dummy'
            MMstretch(iSTR)%R_ij = ZERO
            MMstretch(iSTR)%k_ij = ZERO
        end SELECT typeI

! Printing for debugging and things that are not accessible via an output statement:
      end do

! End of routine GETBOND_PARAMS
      call PRG_manager ('exit', 'GETBOND_PARAMS', 'UTILITY')
      return
      end subroutine GETBOND_PARAMS
      subroutine GETANG_PARAMS
!*****************************************************************
! Date last modified: 23 March, 2000                 Version 1.0 *
! Author: Michelle Shaw                                          *
! Description: Obtain equilibrium angles.                        *
!*****************************************************************
! Modules:
      USE program_files
      USE Eq_Angles
      USE FC_Angles

      implicit none

! Local scalars:
      integer case_angle,Iatom,Jatom,Katom,IBend,type_i,type_j,type_k,ANG_IN_RING,bond_ik,bond_kj
      integer ZatomI,ZatomJ,ZatomK
!
! Begin:
      call PRG_manager ('enter', 'GETANG_PARAMS', 'UTILITY')
!
! Allocate work arrays:
      if(.not.allocated(MMbend))then
        allocate(MMbend(RIC%Nangles))
      end if

!      ANGTYP = .false.

! Next, the equilibrium angle bend must be obtained based on the type of atoms involved in the angle.
      do IBend = 1,RIC%Nangles
        Iatom = ANGLE(IBend)%ATOM1
        Katom = ANGLE(IBend)%ATOM2 ! Katom is the central atom
        Jatom = ANGLE(IBend)%ATOM3
        ZatomI = CARTESIAN(Iatom)%Atomic_number
        if(ZatomI.eq.0)cycle
        ZatomJ = CARTESIAN(Jatom)%Atomic_number
        if(ZatomJ.eq.0)cycle
        ZatomK = CARTESIAN(Katom)%Atomic_number
        if(ZatomK.eq.0)cycle

        type_i = TATOM(Iatom)%type_mm
        type_j = TATOM(Jatom)%type_mm
        type_k = TATOM(Katom)%type_mm

! Check if angle is in ring... (temporary!)
        if(type_i.eq.22.and.type_j.eq.22.and.type_k.eq.22)then
          ANG_IN_RING = 9
        else if(type_i.eq.20.and.type_j.eq.20.and.type_k.eq.20)then
          ANG_IN_RING = 16
        else if(type_i.eq.30.and.type_j.eq.30.and.type_k.eq.30)then
          ANG_IN_RING = 16
        else
          ANG_IN_RING = 0
        end if
! NOTE: 12100=110*110
        case_angle = 12100*(ZatomI-1)+110*(ZatomK-1)+(ZatomJ-1)

        ANGLEtypeS:SELECT CASE(case_angle)
          CASE(550) ANGLEtypeS
            call HCH_ANG_PARM (type_k, MMbend(IBend)%theta_ijk, MMbend(IBend)%k_ijk)
          CASE(555) ANGLEtypeS
            call HCC_ANG_PARM (type_j, type_k, MMbend(IBend)%theta_ijk, MMbend(IBend)%k_ijk)
          CASE (61050) ANGLEtypeS
            call CCH_ANG_PARM (type_i, type_k, MMbend(IBend)%theta_ijk, MMbend(IBend)%k_ijk)
          CASE (61055) ANGLEtypeS
            bond_ik = BT(Iatom, Katom)
            bond_kj = BT(Jatom, Katom)
            call CCC_ANG_PARM (type_i, type_j, type_k, bond_ik, bond_kj, ANG_IN_RING, &
                               MMbend(IBend)%theta_ijk, MMbend(IBend)%k_ijk)
          CASE DEFAULT ANGLEtypeS
            write(UNIout,'(a)')'Atom types not supported or are dummies.'
            MMbend(IBend)%theta_ijk = ZERO
            MMbend(IBend)%k_ijk = ZERO
        end SELECT ANGLEtypeS
!        if(MMbend(IBend)%theta_ijk.ge.(175.0D0*Deg_to_Radian).and.
!    .      MMbend(IBend)%theta_ijk.le.PI_VAL)then
!          ANGTYP(IBend) = .true.
!        end if
! Printing for debugging and things that are not accessible via an output statement:
      end do

! End of routine GETANG_PARAMS
      call PRG_manager ('exit', 'GETANG_PARAMS', 'UTILITY')
      return
      end subroutine GETANG_PARAMS

      subroutine GETTOR_PARAMS
!****************************************************************************
! Date last modified: 23 March, 2000                            Version 1.0 *
! Author: Michelle Shaw                                                     *
! Description: Calculates the force program_constants for the angle torsion and     *
!              places them in a derived type called MMtorsion.              *
!****************************************************************************
! Modules:
      USE program_files
      USE tor_params

      implicit none

! Local scalars:
      integer Iatom,Jatom,Katom,Latom,type_i,type_j,type_k,type_l,Itorsion,torsion_atom_case
      integer ZatomI,ZatomJ,ZatomK,ZatomL
!
! Begin:
      call PRG_manager ('enter', 'GETTOR_PARAMS', 'UTILITY')
!
! Allocate work arrays:
      if(.not.allocated(MMtorsion))then
        allocate(MMtorsion(RIC%Ntorsions))
      end if

! Now the bond types are needed to distinguish between aromatic sp2 carbons and aliphatic sp2
!  carbons...
! If the atom in question is bonded to another atom by an aromatic bond,then this is its type, but
!  if not, it is considered to be aliphatic sp2...
      do Itorsion = 1,RIC%Ntorsions
        Iatom = TORSION(Itorsion)%ATOM1
        Jatom = TORSION(Itorsion)%ATOM2
        Katom = TORSION(Itorsion)%ATOM3
        Latom = TORSION(Itorsion)%ATOM4
        ZatomI = CARTESIAN(Iatom)%Atomic_number
        if(ZatomI.eq.0)cycle
        ZatomJ = CARTESIAN(Jatom)%Atomic_number
        if(ZatomJ.eq.0)cycle
        ZatomK = CARTESIAN(Katom)%Atomic_number
        if(ZatomK.eq.0)cycle
        ZatomL = CARTESIAN(Latom)%Atomic_number
        if(ZatomK.eq.0)cycle

        type_i = TATOM(Iatom)%type_mm
        type_j = TATOM(Jatom)%type_mm
        type_k = TATOM(Katom)%type_mm
        type_l = TATOM(Latom)%type_mm

! NOTE: 131000=110*110*110, 12100=110*110
        torsion_atom_case=1331000*(ZatomI-1)+12100*(ZatomJ-1)+110*(ZatomK-1)+(ZatomL-1)

        TORStypeS:SELECT CASE (torsion_atom_case)
          CASE(61050) TORStypeS
            call GETTOR_PARMS_HCCH(Itorsion, type_j, type_k)
          CASE(61055) TORStypeS
            call GETTOR_PARMS_HCCC(Itorsion, type_j, type_k, type_l)
          CASE (6716050) TORStypeS
            call GETTOR_PARMS_CCCH(Itorsion, type_i, type_j, type_k)
          CASE (6716055) TORStypeS
            call GETTOR_PARMS_CCCC(Itorsion, type_i, type_j, type_k, type_l)
          CASE DEFAULT TORStypeS
            write(UNIout,'(a)')'Atom type not supported or is a dummy.'
        end SELECT TORStypeS
! Printing for debugging and things that are not accessible via an output statement:
!        end if
      end do

! End of routine GETTOR_PARAMS
      call PRG_manager ('exit', 'GETTOR_PARAMS', 'UTILITY')
      return
      end subroutine GETTOR_PARAMS

      subroutine GETOBND_PARAMS
!****************************************************************************
! Date last modified: 23 March, 2000                            Version 1.0 *
! Author: Michelle Shaw                                                     *
! Description: Calculates the force program_constants for the out-of-plane bend     *
!              and places them in an array oopbend_k_ijkl.                  *
!****************************************************************************
! Modules:
      USE program_files
      USE oopbend_fc
      USE redundant_coordinates

      implicit none

! Local scalars:
      integer IoopBend,Iatom,Jatom,Katom,Latom,type_i,type_j,type_k,type_l,case_obend
      integer ZatomI,ZatomJ,ZatomK,ZatomL
      double precision fc
!
! Begin:
      call PRG_manager ('enter', 'GETOBND_PARAMS', 'UTILITY')


! Allocate work arrays:
      if(.not.allocated(MMoopbend_k_ijkl))then
        allocate (MMoopbend_k_ijkl(RIC%Noopbends))
      end if

! Zero input array:
      MMoopbend_k_ijkl = ZERO

! Determining out-of-plane bend force program_constants is based on the types of the atoms (i, j, k, l)
!   involved in the bend.
! The central atom is Jatom.
      do IoopBend = 1,RIC%Noopbends
        Iatom = oopbend(IoopBend)%ATOM1
        Jatom = oopbend(IoopBend)%ATOM2
        Katom = oopbend(IoopBend)%ATOM3
        Latom = oopbend(IoopBend)%ATOM4
        ZatomI = CARTESIAN(Iatom)%Atomic_number
        if(ZatomI.eq.0)cycle
        ZatomJ = CARTESIAN(Jatom)%Atomic_number
        if(ZatomJ.eq.0)cycle
        ZatomK = CARTESIAN(Katom)%Atomic_number
        if(ZatomK.eq.0)cycle
        ZatomL = CARTESIAN(Latom)%Atomic_number
        if(ZatomK.eq.0)cycle

        type_i = TATOM(Iatom)%type_mm
        type_j = TATOM(Jatom)%type_mm
        type_k = TATOM(Katom)%type_mm
        type_l = TATOM(Latom)%type_mm

        case_obend = 1331000*(ZatomI-1)+12100*(ZatomJ-1)+110*(ZatomK-1)+(ZatomL-1)

        OOPBEND_CASE:SELECT CASE(case_obend)
! HCCH out-of-plane bend
          CASE(61050) OOPBEND_CASE
            call GETOOPBEND_HCCH(type_j, type_k, fc)
! CCHH out-of-plane bend
          CASE(6715500) OOPBEND_CASE
            call GETOOPBEND_HCCH(type_i, type_j, fc)
! HCCC out-of-plane bend
          CASE(61055) OOPBEND_CASE
            call GETOOPBEND_HCCC(type_i, type_j, type_k, type_l, fc)
! CCCH out-of-plane bend
          CASE(6716050) OOPBEND_CASE
            call GETOOPBEND_HCCC(type_i, type_j, type_k, type_l, fc)
! CCHC out-of-plane bend
          CASE(6715505) OOPBEND_CASE
            call GETOOPBEND_HCCC(type_i, type_j, type_k, type_l, fc)
! CCCC out-of-plane bend
          CASE(6716055) OOPBEND_CASE
            call GETOOPBEND_CCCC(type_i, type_j, type_k, type_l, fc)
         CASE DEFAULT OOPBEND_CASE
            write(UNIout,'(a)')'Out-of-plane bend not supported.'
            write(UNIout,'(a,4i8)')'i, j, k, l: ',type_i,type_j,type_k,type_l
            fc = ZERO
        end SELECT OOPBEND_CASE
        MMoopbend_k_ijkl(IoopBend) = fc
! Printing for debugging and things that are not accessible via an output statement:
      end do

! End of routine GETOBND_PARAMS
      call PRG_manager ('exit', 'GETOBND_PARAMS', 'UTILITY')
      return
      end subroutine GETOBND_PARAMS

      subroutine GETVDW_PARAMS
!*************************************************************************
! Date last modified: 26 March, 2000                         Version 1.0 *
! Author: Michelle Shaw                                                  *
! Description: Calculate the minimum energy separation and well depth    *
!              to be used in the van der Waals energy determination.     *
!*************************************************************************
      USE program_files
! formulas are based on MMFF94 force field, see :
!        Halgren, T. A., J. Comp. Chem, vol. 17 (5&6) p. 520 (1996)
!
! Local scalars:
      integer iELEC,Iatom,Jatom
      double precision num,denom,G_I,N_I,G_J,N_J,alpha_I,alpha_J,A_I,A_J,sum_IJ,gamma_IJ,R_II,R_JJ
!
! Begin:
      call PRG_manager ('enter', 'GETVDW_PARAMS', 'UTILITY')
!
! Allocate work arrays:
      if(.not.allocated(MMVderW))then
        allocate (MMVderW(max0(1,RIC%Nnonbonded)))
      end if

! Call subroutine minE_sep to compute the minimum energy separations and calculate the well depth.
! Place both into the object MMVderW for use in the energy expression.
      do iELEC = 1,RIC%Nnonbonded
        Iatom = pair_IJ(iELEC)%I
        Jatom = pair_IJ(iELEC)%J
        if(pair_IJ(iELEC)%is_vdw.eq.1)then
          PARMI:SELECT CASE(TATOM(Iatom)%type_mm)
            CASE(1) PARMI
              A_I = A_scalef(1)
              alpha_I = alpha(1)
              G_I = G_scalef(1)
              N_I = N_valE(1)
            CASE(2) PARMI
              A_I = A_scalef(2)
              G_I = G_scalef(2)
              N_I = N_valE(2)
              alpha_I = alpha(2)
            CASE(4) PARMI
              A_I = A_scalef(3)
              G_I = G_scalef(3)
              N_I = N_valE(3)
              alpha_I = alpha(3)
            CASE(5) PARMI
              A_I = A_scalef(4)
              G_I = G_scalef(4)
              N_I = N_valE(4)
              alpha_I = alpha(4)
            CASE(20) PARMI
              A_I = A_scalef(5)
              G_I = G_scalef(5)
              N_I = N_valE(5)
              alpha_I = alpha(5)
            CASE(22) PARMI
              A_I = A_scalef(6)
              G_I = G_scalef(6)
              N_I = N_valE(6)
              alpha_I = alpha(6)
            CASE(30) PARMI
              A_I = A_scalef(7)
              G_I = G_scalef(7)
              N_I = N_valE(7)
              alpha_I = alpha(7)
            CASE(37) PARMI
              A_I = A_scalef(8)
              G_I = G_scalef(8)
              N_I = N_valE(8)
              alpha_I = alpha(8)
            CASE DEFAULT PARMI
              write(UNIout,'(a)')'Wrong atom type!'
          end SELECT PARMI
          PARMJ:SELECT CASE(TATOM(Jatom)%type_mm)
            CASE(1) PARMJ
              A_J = A_scalef(1)
              G_J = G_scalef(1)
              N_J = N_valE(1)
              alpha_J = alpha(1)
            CASE(2) PARMJ
              A_J = A_scalef(2)
              G_J = G_scalef(2)
              N_J = N_valE(2)
              alpha_J = alpha(2)
            CASE(4) PARMJ
              A_J = A_scalef(3)
              G_J = G_scalef(3)
              N_J = N_valE(3)
              alpha_J = alpha(3)
            CASE(5) PARMJ
              A_J = A_scalef(4)
              G_J = G_scalef(4)
              N_J = N_valE(4)
              alpha_J = alpha(4)
            CASE(20) PARMJ
              A_J = A_scalef(5)
              G_J = G_scalef(5)
              N_J = N_valE(5)
              alpha_J = alpha(5)
            CASE(22) PARMJ
              A_J = A_scalef(6)
              G_J = G_scalef(6)
              N_J = N_valE(6)
              alpha_J = alpha(6)
            CASE(30) PARMJ
              A_J = A_scalef(7)
              G_J = G_scalef(7)
              N_J = N_valE(7)
              alpha_J = alpha(7)
            CASE(37) PARMJ
              A_J = A_scalef(8)
              G_J = G_scalef(8)
              N_J = N_valE(8)
              alpha_J = alpha(8)
            CASE DEFAULT PARMJ
              write(UNIout,'(a)')'Wrong atom type!'
          end SELECT PARMJ
! R_IJ calculation:
          R_II = A_I*(alpha_I**0.25D0)
          R_JJ = A_J*(alpha_J**0.25D0)
          if(R_II.eq.R_JJ)then
            MMVderW(iELEC)%R_IJ = R_II
          else
            sum_IJ = (R_II + R_JJ)
            gamma_IJ = (R_II - R_JJ)/sum_IJ
            MMVderW(iELEC)%R_IJ = 0.5D0*sum_IJ*(ONE + B_scalef*(ONE-dexp(-beta*(gamma_IJ**2))))
          end if
! epsilon_IJ calculation:
          num = 181.16D0*G_I*G_J*alpha_I*alpha_J
          denom = ( dsqrt(alpha_I/N_I) + dsqrt(alpha_J/N_J))*(MMVderW(iELEC)%R_IJ**6)

          MMVderW(iELEC)%epsilon_IJ = num/denom
        else
          MMVderW(iELEC)%R_IJ = ZERO
          MMVderW(iELEC)%epsilon_IJ = ZERO
        end if
! Printing for debugging and things that are not accessible via an output statement:
      end do

! End of routine GETVDW_PARAMS
      call PRG_manager ('exit', 'GETVDW_PARAMS', 'UTILITY')
      return
      end subroutine GETVDW_PARAMS

      subroutine GETELEC_PARAMS
!***************************************************************************
! Date last modified: 12 February, 2000                        Version 1.0 *
! Author: Michelle Shaw                                                    *
! Description: Calculate and store in derived type MMelectrostatic charges *
!              for each atom i and j in a pair.                            *
!***************************************************************************
      implicit none

! Local scalars:
      double precision w_IK, w_JK, q0_I, q0_J, bciI, bciJ
      integer iELEC, iCTR, Iatom, Jatom, Iatom_type, Jatom_type, Katom_type,Natoms
!
! Begin:
      call PRG_manager ('enter','GETELEC_PARAMS', 'UTILITY')
!
! Allocate work arrays:
      if(.not.allocated(MMelectrostatic))then
        allocate (MMelectrostatic(max0(1,RIC%Nnonbonded)))
      end if

      Natoms=size(Connect, 1)
! Zero scalars:
      w_IK = ZERO
      w_JK = ZERO
      q0_I = ZERO
      q0_J = ZERO
      bciI = ZERO
      bciJ = ZERO
      Iatom = 0
      Jatom = 0
      Iatom_type = 0
      Jatom_type = 0
      Katom_type = 0

! get partial bond charge increments from nonbonded parameters based on atoms I and K, and their
!  orientation, i.e. w_KI = -w_IK.
      do iELEC = 1,RIC%Nnonbonded
        Iatom = pair_IJ(iELEC)%I
        Jatom = pair_IJ(iELEC)%J
        Iatom_type = TATOM(Iatom)%type_mm
        Jatom_type = TATOM(Jatom)%type_mm
        do iCTR = 1,Natoms
          if(CONNECT(Iatom,iCTR).eq.1)then
            Katom_type = TATOM(iCTR)%type_mm
            call GET_WIK(Iatom_type,Katom_type,w_IK,q0_I) ! q0_I should be determined once only (in advance)
            bciI = bciI + w_IK
          end if
          if(CONNECT(Jatom,iCTR).eq.1)then
            Katom_type = TATOM(iCTR)%type_mm
            call GET_WIK(Jatom_type,Katom_type,w_JK,q0_J)
            bciJ = bciJ + w_JK
          end if
        end do
        MMelectrostatic(iELEC)%q_I = q0_I + bciI
        MMelectrostatic(iELEC)%q_J = q0_J + bciJ
        bciI = ZERO
        bciJ = ZERO
! Printing for debugging and things that are not accessible via an output statement:
      end do

! End of routine GETELEC_PARAMS:
      call PRG_manager ('exit', 'GETELEC_PARAMS', 'UTILITY')
      return
      end subroutine GETELEC_PARAMS

      subroutine GETSTRBND_PARAMS
!****************************************************************************
! Date last modified: 23 March, 2000                            Version 1.0 *
! Author: Michelle Shaw                                                     *
! Description: Calculates the force program_constants for the coupling of a bond    *
!              stretch to an angle bend and places them in a derived type   *
!              stretch_bend.                                                *
!****************************************************************************
      USE program_files
      implicit none

! Local scalars:
      integer Iatom,Jatom,Katom,ZatomI,ZatomJ,ZatomK,type_i,type_j,type_k
      integer IBend,case_strbnd,bond_ik,bond_kj
      double precision stb_fc_ijk, stb_fc_kji
!
! Begin:
      call PRG_manager ('enter', 'GETSTRBND_PARAMS', 'UTILITY')

! Allocate work arrays;
      if(.not.allocated(MMstr_bend))then
        allocate (MMstr_bend(RIC%Nangles))
      end if

! Next, the angle bend force constant must be obtained based on the type of atoms involved
! in the angle.
      do IBend=1,RIC%Nangles
        Iatom = ANGLE(IBend)%ATOM1
        Katom = ANGLE(IBend)%ATOM2 ! Katom is the central atom...
        Jatom = ANGLE(IBend)%ATOM3
        ZatomI = CARTESIAN(Iatom)%Atomic_number
        if(ZatomI.eq.0)cycle
        ZatomJ = CARTESIAN(Jatom)%Atomic_number
        if(ZatomJ.eq.0)cycle
        ZatomK = CARTESIAN(Katom)%Atomic_number
        if(ZatomK.eq.0)cycle

        type_i = TATOM(Iatom)%type_mm
        type_j = TATOM(Jatom)%type_mm
        type_k = TATOM(Katom)%type_mm

        case_strbnd = 12100*(ZatomI-1)+110*(ZatomK-1)+(ZatomJ-1)

        bond_ik = BT(Iatom, Katom)
        bond_kj = BT(Jatom, Katom)

        ANGLEtypeS:SELECT CASE (case_strbnd)
          CASE(550) ANGLEtypeS
            call GET_STB_FC_HCH(type_k, stb_fc_ijk, stb_fc_kji)
            MMstr_bend(IBend)%k_ijk = stb_fc_ijk
            MMstr_bend(IBend)%k_kji = stb_fc_kji

          CASE(555) ANGLEtypeS
            call GET_STB_FC_HCC(type_j, type_k, bond_kj, stb_fc_ijk, stb_fc_kji)
            MMstr_bend(IBend)%k_ijk = stb_fc_ijk
            MMstr_bend(IBend)%k_kji = stb_fc_kji

          CASE (61050) ANGLEtypeS
            call GET_STB_FC_CCH(type_i, type_k, bond_ik, stb_fc_ijk, stb_fc_kji)
            MMstr_bend(IBend)%k_ijk = stb_fc_ijk
            MMstr_bend(IBend)%k_kji = stb_fc_kji

          CASE (61055) ANGLEtypeS
            call GET_STB_FC_CCC(type_i, type_j, type_k, bond_ik, bond_kj, stb_fc_ijk,stb_fc_kji)
            MMstr_bend(IBend)%k_ijk = stb_fc_ijk
            MMstr_bend(IBend)%k_kji = stb_fc_kji

          CASE DEFAULT ANGLEtypeS
            write(UNIout,'(a)')'Atom type not supported or is a dummy.'
            MMstr_bend(IBend)%k_ijk = ZERO
            MMstr_bend(IBend)%k_kji = ZERO
        end SELECT ANGLEtypeS

! Printing for debugging and things that are not accessible via an output statement:
      end do

! End of routine GETSTRBND_PARAMS
      call PRG_manager ('exit', 'GETSTRBND_PARAMS', 'UTILITY')
      return
      end subroutine GETSTRBND_PARAMS


      end module mm_parameters

      subroutine GET_WIK (iATM_type,kATM_type,w_IK,q0_I)
!*****************************************************************************
! Date last modified: 12 February, 2000                          Version 1.0 *
! Author: Michelle Shaw                                                      *
! Description: Gets w_IK values for I = i,j for use in determining q_I,q_J.  *
!*****************************************************************************
! Modules:
      USE program_files
      USE nobond_params

      implicit none

! Local scalars:
      double precision w_IK, q0_I
      integer iATM_type, kATM_type
!
! Begin:
! Get value for w_IK from atom types for i or j.

      SEL_BCI_K:SELECT CASE(kATM_type)
        CASE(1) SEL_BCI_K
          q0_I = fchg(1)
          SEL_BCI_1I:SELECT CASE(iATM_type)
            CASE(1) SEL_BCI_1I
              w_IK = bci_ki(1)
            CASE(2) SEL_BCI_1I
              w_IK = bci_ki(2)
            CASE(4) SEL_BCI_1I
              w_IK = bci_ki(3)
            CASE(5) SEL_BCI_1I
              w_IK = bci_ki(4)
            CASE(20) SEL_BCI_1I
              w_IK = bci_ki(5)
            CASE(22) SEL_BCI_1I
              w_IK = bci_ki(6)
            CASE(30) SEL_BCI_1I
              w_IK = bci_ki(7)
            CASE(37) SEL_BCI_1I
              w_IK = bci_ki(8)
            CASE DEFAULT SEL_BCI_1I
              Write(UNIout,*)'Bad atom type for atom i.'
          end SELECT SEL_BCI_1I
        CASE(2) SEL_BCI_K
          q0_I = fchg(2)
          SEL_BCI_2I:SELECT CASE(iATM_type)
            CASE(1) SEL_BCI_2I
              w_IK = - bci_ki(2)
            CASE(2) SEL_BCI_2I
              w_IK = bci_ki(9)
            CASE(4) SEL_BCI_2I
              w_IK = bci_ki(10)
            CASE(5) SEL_BCI_2I
              w_IK = bci_ki(11)
            CASE(20) SEL_BCI_2I
              w_IK = bci_ki(12)
            CASE(22) SEL_BCI_2I
               w_IK = bci_ki(13)
            CASE(30) SEL_BCI_2I
              w_IK = bci_ki(14)
            CASE(37) SEL_BCI_2I
              w_IK = bci_ki(15)
            CASE DEFAULT SEL_BCI_2I
              write(UNIout,'(a)')'Bad atom type for atom i.'
          end SELECT SEL_BCI_2I
        CASE(4) SEL_BCI_K
          q0_I = fchg(3)
          SEL_BCI_4I:SELECT CASE(iATM_type)
            CASE(1) SEL_BCI_4I
              w_IK = - bci_ki(3)
            CASE(2) SEL_BCI_4I
              w_IK = - bci_ki(10)
            CASE(4) SEL_BCI_4I
              w_IK = bci_ki(16)
            CASE(5) SEL_BCI_4I
              w_IK = bci_ki(17)
            CASE(20) SEL_BCI_4I
              w_IK = bci_ki(18)
            CASE(22) SEL_BCI_4I
              w_IK = bci_ki(19)
            CASE(30) SEL_BCI_4I
              w_IK = bci_ki(20)
            CASE(37) SEL_BCI_4I
              w_IK = bci_ki(21)
            CASE DEFAULT SEL_BCI_4I
              write(UNIout,'(a)')'Bad atom type for atom i.'
          end SELECT SEL_BCI_4I
        CASE(5) SEL_BCI_K
          q0_I = fchg(4)
          SEL_BCI_5I:SELECT CASE(iATM_type)
            CASE(1) SEL_BCI_5I
              w_IK = - bci_ki(4)
            CASE(2) SEL_BCI_5I
              w_IK = - bci_ki(11)
            CASE(4) SEL_BCI_5I
              w_IK = - bci_ki(17)
            CASE(5) SEL_BCI_5I
              w_IK = bci_ki(22)
            CASE(20) SEL_BCI_5I
              w_IK = bci_ki(23)
            CASE(22) SEL_BCI_5I
              w_IK = bci_ki(24)
            CASE(30) SEL_BCI_5I
              w_IK = bci_ki(25)
            CASE(37) SEL_BCI_5I
              w_IK = bci_ki(26)
            CASE DEFAULT SEL_BCI_5I
              write(UNIout,'(a)')'Bad atom type for atom i.'
          end SELECT SEL_BCI_5I
        CASE(20) SEL_BCI_K
          q0_I = fchg(5)
          SEL_BCI_20I:SELECT CASE(iATM_type)
            CASE(1) SEL_BCI_20I
              w_IK = - bci_ki(5)
            CASE(2) SEL_BCI_20I
              w_IK = - bci_ki(12)
            CASE(4) SEL_BCI_20I
              w_IK = - bci_ki(18)
            CASE(5) SEL_BCI_20I
              w_IK = - bci_ki(23)
            CASE(20) SEL_BCI_20I
              w_IK = bci_ki(27)
            CASE(22) SEL_BCI_20I
              w_IK = bci_ki(28)
            CASE(30) SEL_BCI_20I
              w_IK = bci_ki(29)
            CASE(37) SEL_BCI_20I
              w_IK = bci_ki(30)
            CASE DEFAULT SEL_BCI_20I
              write(UNIout,'(a)')'Bad atom type for atom i.'
          end SELECT SEL_BCI_20I
        CASE(22) SEL_BCI_K
          q0_I = fchg(6)
          SEL_BCI_22I:SELECT CASE(iATM_type)
            CASE(1) SEL_BCI_22I
              w_IK = - bci_ki(6)
            CASE(2) SEL_BCI_22I
              w_IK = - bci_ki(13)
            CASE(4) SEL_BCI_22I
              w_IK = - bci_ki(19)
            CASE(5) SEL_BCI_22I
              w_IK = - bci_ki(24)
            CASE(20) SEL_BCI_22I
              w_IK = - bci_ki(28)
            CASE(22) SEL_BCI_22I
              w_IK = bci_ki(31)
            CASE(30) SEL_BCI_22I
              w_IK = bci_ki(32)
            CASE(37) SEL_BCI_22I
              w_IK = bci_ki(33)
            CASE DEFAULT SEL_BCI_22I
              write(UNIout,'(a)')'Bad atom type for atom i.'
          end SELECT SEL_BCI_22I
        CASE(30) SEL_BCI_K
          q0_I = fchg(7)
          SEL_BCI_30I:SELECT CASE(iATM_type)
            CASE(1) SEL_BCI_30I
              w_IK = - bci_ki(7)
            CASE(2) SEL_BCI_30I
              w_IK = - bci_ki(14)
            CASE(4) SEL_BCI_30I
              w_IK = - bci_ki(20)
            CASE(5) SEL_BCI_30I
              w_IK = - bci_ki(25)
            CASE(20) SEL_BCI_30I
              w_IK = - bci_ki(29)
            CASE(22) SEL_BCI_30I
              w_IK = - bci_ki(32)
            CASE(30) SEL_BCI_30I
              w_IK = bci_ki(34)
            CASE(37) SEL_BCI_30I
              w_IK = bci_ki(35)
            CASE DEFAULT SEL_BCI_30I
              write(UNIout,'(a)')'Bad atom type for atom i.'
          end SELECT SEL_BCI_30I
        CASE(37) SEL_BCI_K
          q0_I = fchg(8)
          SEL_BCI_37I:SELECT CASE(iATM_type)
            CASE(1) SEL_BCI_37I
              w_IK = - bci_ki(8)
            CASE(2) SEL_BCI_37I
              w_IK = - bci_ki(15)
            CASE(4) SEL_BCI_37I
              w_IK = - bci_ki(21)
            CASE(5) SEL_BCI_37I
              w_IK = - bci_ki(26)
            CASE(20) SEL_BCI_37I
              w_IK = - bci_ki(30)
            CASE(22) SEL_BCI_37I
              w_IK = - bci_ki(33)
            CASE(30) SEL_BCI_37I
              w_IK = - bci_ki(35)
            CASE(37) SEL_BCI_37I
              w_IK = bci_ki(36)
            CASE DEFAULT SEL_BCI_37I
              write(UNIout,'(a)')'Bad atom type for atom i.'
          end SELECT SEL_BCI_37I
        CASE DEFAULT SEL_BCI_K
          write(UNIout,'(a)')'Bad atom type for atom i.'
      end SELECT SEL_BCI_K

! End of routine GET_WIK:
      return
      end subroutine GET_WIK

      subroutine BLD_MM_PARAMETERS
!*****************************************************************************
! Date last modified: 4 April, 2000                              Version 1.0 *
! Author: Michelle Shaw                                                      *
! Description: Get parameters for the different interactions and put them in *
!              an array called MM_parameter.                                *
!*****************************************************************************
! Modules:
      USE program_files
      USE mm_parameters
      USE program_constants
      USE GRAPH_objects
      USE redundant_coordinates

      implicit none
!
! Local scalars:
!
! Local functions:
!
! Begin:
      call PRG_manager ('enter', 'BLD_MM_PARAMETERS', 'UTILITY')
!
      call GET_object ('MOL','INTERNAL', 'RIC')
!
      call GET_object ('MM', 'ATOM_TYPES', 'MMFF94')

      call GETBOND_PARAMS
      call GETANG_PARAMS
      call GETTOR_PARAMS
      call GETOBND_PARAMS
      call GETVDW_PARAMS
      call GETELEC_PARAMS
      call GETSTRBND_PARAMS
!
! End of routine BLD_MM_PARAMETERS
      call PRG_manager ('exit', 'BLD_MM_PARAMETERS', 'UTILITY')
      return
      end subroutine BLD_MM_PARAMETERS
