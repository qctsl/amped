      module FF_parameters
!**********************************************************************************
! Date last modified: March 22, 2005                                  Version 1.0 *
! Author: R. A. Poirier                                                           *
! Description: Contains object and initialization routines for stretch parameters.*                                                       !**********************************************************************************
! Modules:
      USE program_manager
      USE program_defaults
      USE program_constants
      USE type_molecule
      USE mm_interaction_type_params
      USE nobond_params
      USE GRAPH_objects
      USE atom_type

      implicit none

      type torsion_contributions
        double precision :: V1, V2, V3 !torsion energy expression parameters
      end type torsion_contributions

      type (torsion_contributions), dimension(:), allocatable :: MMtorsion

CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine GETTOR_PARAMS
!****************************************************************************
! Date last modified: 23 March, 2000                            Version 1.0 *
! Author: R.A. Poirier and Michelle Shaw                                    *
! Description: Calculates the force program_constants for the angle torsion and     *
!              places them in a derived type called MMtorsion.              *
!****************************************************************************
! Modules:

      implicit none

! Local scalars:
      integer Iatom,Jatom,Katom,Latom,TypeI,TypeJ,TypeK,TypeL,Itorsion,torsion_case
      integer ZatomI,ZatomJ,ZatomK,ZatomL,Ztemp
      logical Ldebug
!
! Begin:
      call PRG_manager ('enter', 'GETTOR_PARAMS', 'UTILITY')
      Ldebug=Local_Debug
!
! Allocate work arrays:
      if(.not.allocated(MMtorsion))then
        allocate(MMtorsion(NGTorsion))
      end if

! Now the bond types are needed to distinguish between aromatic sp2 carbons and aliphatic sp2
!  carbons...
! If the atom in question is bonded to another atom by an aromatic bond,then this is its type, but
!  if not, it is considered to be aliphatic sp2...
      do Itorsion = 1,NGTorsion
        Iatom = TORSION(Itorsion)%ATOM1
        Jatom = TORSION(Itorsion)%ATOM2
        Katom = TORSION(Itorsion)%ATOM3
        Latom = TORSION(Itorsion)%ATOM4
        ZatomI = CARTESIANS%Atomic_number(Iatom)
        if(ZatomI.eq.0)cycle
        ZatomJ = CARTESIANS%Atomic_number(Jatom)
        if(ZatomJ.eq.0)cycle
        ZatomK = CARTESIANS%Atomic_number(Katom)
        if(ZatomK.eq.0)cycle
        ZatomL = CARTESIANS%Atomic_number(Latom)
        if(ZatomK.eq.0)cycle
        if(ZatomI.gt.ZatomL)then ! Want Zl > Zi change to L-K-J-I
          Ztemp=ZatomI
          ZatomI=ZatomL
          ZatomL=Ztemp
          Ztemp=ZatomK
          ZatomK=ZatomJ
          ZatomJ=Ztemp
        end if

        TypeI = vertex(Iatom)%type
        TypeJ = vertex(Jatom)%type
        TypeK = vertex(Katom)%type
        TypeL = vertex(Latom)%type

! NOTE: 131000=110*110*110, 12100=110*110
        torsion_case=1331000*(ZatomI-1)+12100*(ZatomJ-1)+110*(ZatomK-1)+(ZatomL-1)

        TORStypeS:select case (torsion_case)
          case(61050) TORStypeS
            call GETTOR_PARMS_HCCH (Itorsion, TypeJ, TypeK)
          case(61055) TORStypeS
            call GETTOR_PARMS_HCCC (Itorsion, TypeJ, TypeK, TypeL)
          case (6716050) TORStypeS
            call GETTOR_PARMS_CCCC (Itorsion, TypeI, TypeJ, TypeK, TypeL)
          case DEFAULT TORStypeS
            write(UNIout,'(a)')'Atom type not supported or is a dummy.'
        end select TORStypeS
! Printing for debugging and things that are not accessible via an output statement:
!        if(Ldebug)then
          write(UNIout,'(a,f12,6)')'V1: ',MMtorsion(Itorsion)%V1
          write(UNIout,'(a,f12,6)')'V2: ',MMtorsion(Itorsion)%V2
          write(UNIout,'(a,f12,6)')'V3: ',MMtorsion(Itorsion)%V3
!        end if
      end do

      if(Ldebug)then
        write(UNIout,'(a,i8)')'debug_GETTOR_PARAMS > NGTorsion: ',NGTorsion
      end if

! End of routine GETTOR_PARAMS
      call PRG_manager ('exit', 'GETTOR_PARAMS', 'UTILITY')
      return
      end subroutine GETTOR_PARAMS
      end module FF_parameters
