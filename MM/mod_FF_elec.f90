      module FF_parameters
!**********************************************************************************
! Date last modified: March 22, 2005                                  Version 1.0 *
! Author: R. A. Poirier                                                           *
! Description: Contains object and initialization routines for stretch parameters.*                                                       !**********************************************************************************
! Modules:
      USE program_manager
      USE program_defaults
      USE program_constants
      USE type_molecule
      USE mm_interaction_type_params
      USE nobond_params
      USE GRAPH_objects
      USE atom_type

      implicit none

      type electrostatic_contributions
        double precision :: q_I, q_j
      end type electrostatic_contributions

      type (electrostatic_contributions), dimension(:), allocatable :: MMelectrostatic

CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine GETELEC_PARAMS
!***************************************************************************
! Date last modified: 12 February, 2000                        Version 1.0 *
! Author: Michelle Shaw                                                    *
! Description: Calculate and store in derived type MMelectrostatic charges *
!              for each atom i and j in a pair.                            *
!***************************************************************************
      implicit none

! Local scalars:
      double precision w_IK, w_JK, q0_I, q0_J, bciI, bciJ
      integer iELEC, iCTR, Iatom, Jatom, Iatom_type, Jatom_type, Katom_type,Natoms
      logical Ldebug
!
! Begin:
      call PRG_manager ('enter','GETELEC_PARAMS', 'UTILITY')
      Ldebug=Local_Debug
!
! Allocate work arrays:
      if(.not.allocated(MMelectrostatic))then
        allocate (MMelectrostatic(max0(1,Nnonbond)))
      end if

      Natoms=size(Connect, 1)
! Zero scalars:
      w_IK = ZERO
      w_JK = ZERO
      q0_I = ZERO
      q0_J = ZERO
      bciI = ZERO
      bciJ = ZERO

! get partial bond charge increments from nonbonded parameters based on atoms I and K, and their
!  orientation, i.e. w_KI = -w_IK.
      do iELEC = 1,Nnonbond
        Iatom = pair_IJ(iELEC)%I
        Jatom = pair_IJ(iELEC)%J
        Iatom_type = vertex(Iatom)%type
        Jatom_type = vertex(Jatom)%type
        do iCTR = 1,Natoms
          if(CONNECT(Iatom,iCTR).eq.1)then
            Katom_type = vertex(iCTR)%type
            call GET_WIK(Iatom_type,Katom_type,w_IK,q0_I)
            bciI = bciI + w_IK
          end if
          if(CONNECT(Jatom,iCTR).eq.1)then
            Katom_type = vertex(iCTR)%type
            call GET_WIK(Jatom_type,Katom_type,w_JK,q0_J)
            bciJ = bciJ + w_JK
          end if
        end do
        MMelectrostatic(iELEC)%q_I = q0_I + bciI
        MMelectrostatic(iELEC)%q_J = q0_J + bciJ
        bciI = ZERO
        bciJ = ZERO
! Printing for debugging and things that are not accessible via an output statement:
        if(Ldebug)then
          write(UNIout,'(a,i8,f12.6)')'I, charge on I: ',Iatom_type,MMelectrostatic(iELEC)%q_I
          write(UNIout,'(a,i8,f12.6)')'J, charge on J: ',Jatom_type,MMelectrostatic(iELEC)%q_J
        end if
      end do

      if(Ldebug)then
        write(UNIout,'(a,i8,f12.6,i8,f12.6)')'debug_GETELEC_PARAMS > I, q_I, J, q_J: ',pair_IJ(1)%I, &
                       MMelectrostatic(1)%q_I,pair_IJ(1)%J,MMelectrostatic(1)%q_J
      end if

! End of routine GETELEC_PARAMS:
      call PRG_manager ('exit', 'GETELEC_PARAMS', 'UTILITY')
      return
      end subroutine GETELEC_PARAMS
      end module FF_parameters
