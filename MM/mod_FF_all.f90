      module FF_parameters
!**********************************************************************************
! Date last modified: March 22, 2005                                  Version 1.0 *
! Author: R. A. Poirier                                                           *
! Description: Contains object and initialization routines for stretch parameters.*                                                       !**********************************************************************************
! Modules:
      USE program_manager
      USE program_defaults
      USE program_constants
      USE type_molecule
      USE TypeInternal_coordinates
      USE mm_interaction_type_params
      USE nobond_params
      USE GRAPH_objects
      USE atom_type

      implicit none

      type stretch_contributions
        double precision :: k_ij !stretch force constant
        double precision :: R_ij !equilibrium bond length
      end type stretch_contributions

      type bend_contributions
        double precision :: k_ijk !bend force constant
        double precision :: theta_ijk !equilibrium angle
      end type bend_contributions

      type torsion_contributions
        double precision :: V1, V2, V3 !torsion energy expression parameters
      end type torsion_contributions

      type vanderwaals_contributions
        double precision :: R_IJ, epsilon_IJ
      end type vanderwaals_contributions

      type electrostatic_contributions
        double precision :: q_I, q_j
      end type electrostatic_contributions

      type stretch_bend_contributions
        double precision :: k_ijk, k_kji !stretch-bend force program_constants
      end type stretch_bend_contributions

      type (stretch_contributions), dimension(:), allocatable :: MMstretch
      type (bend_contributions), dimension(:), allocatable :: MMbend
      type (torsion_contributions), dimension(:), allocatable :: MMtorsion
      type (vanderwaals_contributions), dimension(:), allocatable :: MMVderW
      type (electrostatic_contributions), dimension(:), allocatable :: MMelectrostatic
      type (stretch_bend_contributions), dimension(:), allocatable :: MMstr_bend
      double precision, dimension(:), allocatable :: MMoopbend_k_ijkl !out-of-plane bend force constant

CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine GETBOND_PARAMS
!*****************************************************************
! Date last modified: 29 March, 2005                 Version 1.0 *
! Author: R.A. Poirier and M. Shaw                               *
! Description: Get equilibrium bond lengths                      *
!*****************************************************************
! Modules:

      implicit none

! Local scalars:
      integer :: case_bond,IBond,Iatom,Jatom,Ring_size,RsizeI,RsizeJ,type_bond
      integer :: TypeX,TypeY,ZatomI,ZatomJ,Zmax,Zmin
      double precision :: R_ij,K_ij
      logical Ldebug
!
! Local functions:
!
! Begin:
      call PRG_manager ('enter', 'GETBOND_PARAMS', 'UTILITY')
      Ldebug=Local_Debug

! REMEMBER TO CONVERT TO A.U.!!!!!!!!!!!!!!!!!!!!!
! Allocate work arrays:
      if(.not.allocated(MMstretch))then
        allocate(MMstretch(RIC%Nbonds))
      end if

! The information about each bond must be collected.
      do IBond = 1,RIC%Nbonds
        Iatom = BOND(IBond)%ATOM1
        Jatom = BOND(IBond)%ATOM2
        type_bond = BOND(IBond)%type
        Ring_size=BOND(IBond)%ring_size
        ZatomI=CARTESIANS%Atomic_number(Iatom)
        ZatomJ=CARTESIANS%Atomic_number(Jatom)
        if(ZatomI.ge.ZatomJ)then
          Zmax=ZatomI
          TypeX=vertex(Iatom)%type
          RsizeI=vertex(Iatom)%type
          Zmin=ZatomJ
          TypeY=vertex(Jatom)%type
          RsizeJ=vertex(Jatom)%type
        else
          Zmax=ZatomJ
          TypeX=vertex(Jatom)%type
          RsizeI=vertex(Jatom)%type
          Zmin=ZatomI
          TypeY=vertex(Iatom)%type
          RsizeJ=vertex(Iatom)%type
        end if
        case_bond=110*(Zmin-1)+(Zmax-1)
        write(UNIout,*)'GETBOND_PARAMS: TypeX,TypeY,type_bond: ',TypeX,TypeY,type_bond

! All values currently defaults to C-C
        SBOND:select case(case_bond)
          case (0) SBOND ! H-H (done)
            MMstretch(IBond)%R_ij=0.746D0
            MMstretch(IBond)%k_ij=0.05168D0
! Li-X bonds
          case (220) SBOND ! Li-H
            MMstretch(IBond)%R_ij=1.508D0
            MMstretch(IBond)%k_ij=4.258D0
          case (222) SBOND ! Li-Li
            MMstretch(IBond)%R_ij=1.508D0
            MMstretch(IBond)%k_ij=4.258D0
! Be-X bonds
          case (330) SBOND ! Be-H
            MMstretch(IBond)%R_ij=1.508D0
            MMstretch(IBond)%k_ij=4.258D0
! B-X bonds
          case (440) SBOND ! B-H
            MMstretch(IBond)%R_ij=1.508D0
            MMstretch(IBond)%k_ij=4.258D0
! C-X bonds
          case (550) SBOND ! C-H
            call MM_CH_BONDS (TypeX, TypeY, RsizeI, R_ij, K_ij)
            MMstretch(IBond)%R_ij=R_ij
            MMstretch(IBond)%k_ij=K_ij
          case (555) SBOND ! C-C
            typeCC:select case(type_bond)
              case (1) typeCC ! Single bond
                call MM_C1C_BONDS (TypeX, TypeY, R_ij, K_ij)
              case (2) typeCC ! Double bond
                call MM_C2C_BONDS (TypeX, TypeY, Ring_size, R_ij, K_ij)
              case (3) typeCC ! Triple bond
                call MM_C3C_BONDS (TypeX, TypeY, R_ij, K_ij)
              case (13,23,14,24,15,25,16,26,66) typeCC ! rings, ..., Aromatic bond
                call MM_CaC_BONDS (TypeX, TypeY, R_ij, K_ij)
            end select typeCC
            MMstretch(IBond)%R_ij=R_ij
            MMstretch(IBond)%k_ij=K_ij
! N-X bonds
          case (660) SBOND ! N-H
            call MM_NH_BONDS (TypeX, TypeY, R_ij, K_ij)
            MMstretch(IBond)%R_ij=R_ij
            MMstretch(IBond)%k_ij=K_ij
          case (665) SBOND ! N-C
            typeNC:select case(type_bond)
              case (1) typeNC ! Single bond
                call MM_N1C_BONDS (TypeX, TypeY, R_ij, K_ij)
              case (2) typeNC ! Double bond
                call MM_N2C_BONDS (TypeX, TypeY, R_ij, K_ij)
              case (3) typeNC ! Triple bond
                call MM_N3C_BONDS (TypeX, TypeY, R_ij, K_ij)
              case (13,23,14,24,15,25,16,26,66) typeNC ! rings, ..., Aromatic bond
!               call MM_NaC_BONDS (TypeX, TypeY, R_ij, K_ij)
            end select typeNC
            MMstretch(IBond)%R_ij=R_ij
            MMstretch(IBond)%k_ij=K_ij
          case (666) SBOND ! N-N
            typeNN:select case(type_bond)
              case (1) typeNN ! Single bond
!               call MM_N1N_BONDS (TypeX, TypeY, R_ij, K_ij)
              case (2) typeNN ! Double bond
!               call MM_N2N_BONDS (TypeX, TypeY, R_ij, K_ij)
              case (3) typeNN ! Triple bond
!               call MM_N3N_BONDS (TypeX, TypeY, R_ij, K_ij)
              case (26) typeNN ! Aromatic bond
!               call MM_NaN_BONDS (TypeX, TypeY, R_ij, K_ij)
            end select typeNN
            MMstretch(IBond)%R_ij=R_ij
            MMstretch(IBond)%k_ij=K_ij
! O-X bonds
          case (770) SBOND ! O-H
            call MM_OH_BONDS (TypeX, TypeY, R_ij, K_ij)
            MMstretch(IBond)%R_ij=R_ij
            MMstretch(IBond)%k_ij=K_ij
          case (775) SBOND ! O-C
            call MM_OC_BONDS (TypeX, TypeY, R_ij, K_ij)
            MMstretch(IBond)%R_ij=R_ij
            MMstretch(IBond)%k_ij=K_ij
          case (776) SBOND ! O-N
!           call MM_ON_BONDS (TypeX, TypeY, R_ij, K_ij)
            MMstretch(IBond)%R_ij=R_ij
            MMstretch(IBond)%k_ij=K_ij
          case (777) SBOND ! O-O (done)
!           call MM_OO_BONDS (TypeX, TypeY, R_ij, K_ij)
            MMstretch(IBond)%R_ij=1.449D0
            MMstretch(IBond)%k_ij=4.088D0
! F-X bonds
          case (880) SBOND ! F-H
            MMstretch(IBond)%R_ij=1.508D0
            MMstretch(IBond)%k_ij=4.258D0
          case (885) SBOND ! F-C(sp3)
            MMstretch(IBond)%R_ij=1.360D0
            MMstretch(IBond)%k_ij=6.011D0
          case (886) SBOND ! F-N
            MMstretch(IBond)%R_ij=1.508D0
            MMstretch(IBond)%k_ij=4.258D0
          case (887) SBOND ! F-O
            MMstretch(IBond)%R_ij=1.508D0
            MMstretch(IBond)%k_ij=4.258D0
          case (888) SBOND ! F-F
            MMstretch(IBond)%R_ij=1.508D0
            MMstretch(IBond)%k_ij=4.258D0
! Si-X bonds
          case (1430) SBOND ! Si-H (done)
            MMstretch(IBond)%R_ij=1.485D0
            MMstretch(IBond)%k_ij=2.254D0
          case (1435) SBOND ! Si-C (SP3)
            MMstretch(IBond)%R_ij=1.830D0
            MMstretch(IBond)%k_ij=2.866D0
! P-X bonds
          case (1540) SBOND ! P-H (done)
            MMstretch(IBond)%R_ij=1.415D0
            MMstretch(IBond)%k_ij=2.959D0
          case (1545) SBOND ! P-C
            MMstretch(IBond)%R_ij=1.508D0
            MMstretch(IBond)%k_ij=4.258D0
          case (1546) SBOND ! P-N
            MMstretch(IBond)%R_ij=1.508D0
            MMstretch(IBond)%k_ij=4.258D0
          case (1547) SBOND ! P-O (done)
            MMstretch(IBond)%R_ij=1.630D0
            MMstretch(IBond)%k_ij=5.243D0
          case (1554) SBOND ! P-P
            MMstretch(IBond)%R_ij=1.630D0
            MMstretch(IBond)%k_ij=5.243D0
! S-X bonds
          case (1650) SBOND ! S-H
            MMstretch(IBond)%R_ij=1.508D0
            MMstretch(IBond)%k_ij=4.258D0
          case (1655) SBOND ! S-C
            MMstretch(IBond)%R_ij=1.508D0
            MMstretch(IBond)%k_ij=4.258D0
          case (1656) SBOND ! S-N
            MMstretch(IBond)%R_ij=1.508D0
            MMstretch(IBond)%k_ij=4.258D0
          case (1657) SBOND ! S-O
            MMstretch(IBond)%R_ij=1.508D0
            MMstretch(IBond)%k_ij=4.258D0
          case (1664) SBOND ! S-P
            MMstretch(IBond)%R_ij=1.508D0
            MMstretch(IBond)%k_ij=4.258D0
          case (1665) SBOND ! S-S
            MMstretch(IBond)%R_ij=1.508D0
            MMstretch(IBond)%k_ij=4.258D0
! Cl-X bonds
          case (1760) SBOND ! Cl-H
            MMstretch(IBond)%R_ij=1.508D0
            MMstretch(IBond)%k_ij=4.258D0
          case (1765) SBOND ! Cl-C(SP3)
            MMstretch(IBond)%R_ij=1.773D0
            MMstretch(IBond)%k_ij=2.974D0
          case (1776) SBOND ! Cl-Cl
            MMstretch(IBond)%R_ij=1.508D0
            MMstretch(IBond)%k_ij=4.258D0
! Default   
          case default SBOND ! Bond is not currently defined:
            write(UNIout,*)'GETBOND_PARAMS> Atom type not supported or is a dummy'
            MMstretch(IBond)%R_ij = ZERO
            MMstretch(IBond)%k_ij = ZERO
        end select SBOND

! Printing for debugging and things that are not accessible via an output statement:
!        if(Ldebug)then
          write(6,*)'equilibrium bond length: ',MMstretch(IBond)%R_ij
          write(6,*)'bond force constant: ',MMstretch(IBond)%k_ij
!        end if
      end do ! IBond

! End of routine GETBOND_PARAMS
      call PRG_manager ('exit', 'GETBOND_PARAMS', 'UTILITY')
      return
      end subroutine GETBOND_PARAMS
      subroutine MM_C1C_BONDS (TypeX, TypeY, R_ij, K_ij)
!*****************************************************************************
! Date last modified: March 8, 2005                              Version 1.0 *
! Author: R. A. Poirier                                                      *
! Description: Get equilibrium bond lengths and force program_constants for CC single*
!              bonds                                                         *
!*****************************************************************************
! Modules:

      implicit none

! Local scalars:
      integer, intent(IN) :: TypeX,TypeY
      double precision, intent(OUT) :: R_ij,K_ij
      logical Ldebug
!
! Begin:
      call PRG_manager ('enter', 'MM_C1C_BONDS', 'UTILITY')
      Ldebug=Local_Debug

      StypeX:select case(TypeX)
        case (20) StypeX ! CH3-C
          StypeY: select case (TypeY)
            case (20) StypeY ! CH3-CH3 (done)
              R_ij=1.508D0
              k_ij=4.258D0
            case (35) StypeY ! CH3-CH2,C (done)
              R_ij=1.508D0
              k_ij=4.258D0
            case (45) StypeY ! CH3-CH,C2 (done)
              R_ij=1.508D0
              k_ij=4.258D0
            case (250) StypeY ! CH3-C=CH,C (done)
              R_ij=1.508D0
              k_ij=4.258D0
            case (300) StypeY ! CH3-C=C-C2 (done)
              R_ij=1.482D0
              k_ij=4.539D0
            case (1500) StypeY ! CH3-C-(3)-C (done)
              R_ij=1.459D0
              k_ij=4.707D0
          end select StypeY
        case (35) StypeX ! CH2-C2 (done)
          R_ij=1.508D0
          k_ij=4.258D0
        case (45) StypeX ! CH-C3 (done)
          R_ij=1.508D0
          k_ij=4.258D0
        case (50) StypeX ! C-C4 (done)
          R_ij=1.508D0
          k_ij=4.258D0
        case default StypeX ! Bond is not currently defined:
          write(UNIout,*)'MM_C1C_BONDS> Atom type not supported or is a dummy'
          R_ij = ZERO
          k_ij = ZERO
      end select StypeX

! End of routine MM_C1C_BONDS
      call PRG_manager ('exit', 'MM_C1C_BONDS', 'UTILITY')
      return
      end subroutine MM_C1C_BONDS
      subroutine MM_C2C_BONDS (TypeX, TypeY, Ring_size, R_ij, K_ij)
!*****************************************************************************
! Date last modified: March 8, 2005                              Version 1.0 *
! Author: R. A. Poirier                                                      *
! Description: Get equilibrium bond lengths and force program_constants for CC bonds *
!*****************************************************************************
! Modules:

      implicit none

! Local scalars:
      integer, intent(IN) :: Ring_size,TypeX,TypeY
      double precision, intent(OUT) :: R_ij,K_ij
      logical Ldebug
!
! Begin:
      call PRG_manager ('enter', 'MM_C2C_BONDS', 'UTILITY')
      Ldebug=Local_Debug

      RSIZE:select case(Ring_size)
      case (0) RSIZE
      R0typeX:select case(TypeX)
        case (150) R0typeX ! CH2=C
          R_ij=1.508D0
          k_ij=4.258D0
        case (250) R0typeX ! CH,C=C
          R_ij=1.508D0
          k_ij=4.258D0
        case (300) R0typeX ! C2,C=C
          R_ij=1.508D0
          k_ij=4.258D0
        case default R0typeX ! Bond is not currently defined:
          write(UNIout,*)'MM_C2C_BONDS> Atom type not supported or is a dummy'
          R_ij = ZERO
          k_ij = ZERO
      end select R0typeX
      case (3) RSIZE
      R3typeX:select case(TypeX)
        case (150) R3typeX ! CH2=C
          R_ij=1.508D0
          k_ij=4.258D0
        case (250) R3typeX ! CH,C=C
          R_ij=1.508D0
          k_ij=4.258D0
        case (300) R3typeX ! C2,C=C
          R_ij=1.508D0
          k_ij=4.258D0
        case default R3typeX ! Bond is not currently defined:
          write(UNIout,*)'MM_C2C_BONDS> Atom type not supported or is a dummy'
          R_ij = ZERO
          k_ij = ZERO
      end select R3typeX
      case (4) RSIZE
      R4typeX:select case(TypeX)
        case (150) R4typeX ! CH2=C
          R_ij=1.508D0
          k_ij=4.258D0
        case (250) R4typeX ! CH,C=C
          R_ij=1.508D0
          k_ij=4.258D0
        case (300) R4typeX ! C2,C=C
          R_ij=1.508D0
          k_ij=4.258D0
        case default R4typeX ! Bond is not currently defined:
          write(UNIout,*)'MM_C2C_BONDS> Atom type not supported or is a dummy'
          R_ij = ZERO
          k_ij = ZERO
      end select R4typeX
      case (5) RSIZE
      R5typeX:select case(TypeX)
        case (150) R5typeX ! CH2=C
          R_ij=1.508D0
          k_ij=4.258D0
        case (250) R5typeX ! CH,C=C
          R_ij=1.508D0
          k_ij=4.258D0
        case (300) R5typeX ! C2,C=C
          R_ij=1.508D0
          k_ij=4.258D0
        case default R5typeX ! Bond is not currently defined:
          write(UNIout,*)'MM_C2C_BONDS> Atom type not supported or is a dummy'
          R_ij = ZERO
          k_ij = ZERO
      end select R5typeX
      case (6) RSIZE
      R6typeX:select case(TypeX)
        case (150) R6typeX ! CH2=C
          R_ij=1.508D0
          k_ij=4.258D0
        case (250) R6typeX ! CH,C=C
          R_ij=1.508D0
          k_ij=4.258D0
        case (300) R6typeX ! C2,C=C
          R_ij=1.508D0
          k_ij=4.258D0
        case default R6typeX ! Bond is not currently defined:
          write(UNIout,*)'MM_C2C_BONDS> Atom type not supported or is a dummy'
          R_ij = ZERO
          k_ij = ZERO
      end select R6typeX
      end select RSIZE

! End of routine MM_C2C_BONDS
      call PRG_manager ('exit', 'MM_C2C_BONDS', 'UTILITY')
      return
      end subroutine MM_C2C_BONDS
      subroutine MM_C3C_BONDS (TypeX, TypeY, R_ij, K_ij)
!*****************************************************************************
! Date last modified: March 8, 2005                              Version 1.0 *
! Author: R. A. Poirier                                                      *
! Description: Get equilibrium bond lengths and force program_constants for CC bonds *
!*****************************************************************************
! Modules:

      implicit none

! Local scalars:
      integer, intent(IN) :: TypeX,TypeY
      double precision, intent(OUT) :: R_ij,K_ij
      logical Ldebug
!
! Begin:
      call PRG_manager ('enter', 'MM_C3C_BONDS', 'UTILITY')
      Ldebug=Local_Debug

      StypeX:select case(TypeX)
        case (1000) StypeX ! H-C-(3)-C?
          R_ij=1.508D0
          k_ij=4.258D0
        case (1500) StypeX ! C-C-(3)-C?
          R_ij=1.508D0
          k_ij=4.258D0
        case default StypeX ! Bond is not currently defined:
          write(UNIout,*)'MM_C3C_BONDS> Atom type not supported or is a dummy'
          R_ij = ZERO
          k_ij = ZERO
      end select StypeX

! End of routine MM_C3C_BONDS
      call PRG_manager ('exit', 'MM_C3C_BONDS', 'UTILITY')
      return
      end subroutine MM_C3C_BONDS
      subroutine MM_CaC_BONDS (TypeX, TypeY, R_ij, K_ij)
!*****************************************************************************
! Date last modified: March 8, 2005                              Version 1.0 *
! Author: R. A. Poirier                                                      *
! Description: Get equilibrium bond lengths and force program_constants for CC bonds *
!*****************************************************************************
! Modules:

      implicit none

! Local scalars:
      integer, intent(IN) :: TypeX,TypeY
      double precision, intent(OUT) :: R_ij,K_ij
      logical Ldebug
!
! Begin:
      call PRG_manager ('enter', 'MM_CaC_BONDS', 'UTILITY')
      Ldebug=Local_Debug

      StypeX:select case(TypeX)
        case (150) StypeX ! CH2=C
          R_ij=1.508D0
          k_ij=4.258D0
        case (250) StypeX ! CH,C=C
          R_ij=1.508D0
          k_ij=4.258D0
        case (300) StypeX ! C2,C=C
          R_ij=1.508D0
          k_ij=4.258D0
        case (1000) StypeX ! H-C-(3)-C?
          R_ij=1.508D0
          k_ij=4.258D0
        case (1500) StypeX ! C-C-(3)-C?
          R_ij=1.508D0
          k_ij=4.258D0
        case default StypeX ! Bond is not currently defined:
          write(UNIout,*)'MM_CaC_BONDS> Atom type not supported or is a dummy'
          R_ij = ZERO
          k_ij = ZERO
      end select StypeX

! End of routine MM_CaC_BONDS
      call PRG_manager ('exit', 'MM_CaC_BONDS', 'UTILITY')
      return
      end subroutine MM_CaC_BONDS
      subroutine MM_CH_BONDS (TypeX, TypeY, RsizeI, R_ij, K_ij)
!*****************************************************************************
! Date last modified: March 8, 2005                              Version 1.0 *
! Author: R. A. Poirier                                                      *
! Description: Get equilibrium bond lengths and force program_constants for CH bonds *
!*****************************************************************************
! Modules:

      implicit none

! Local scalars:
      integer, intent(IN) :: RsizeI,TypeX,TypeY
      double precision, intent(OUT) :: R_ij,K_ij
      logical Ldebug
!
! Begin:
      call PRG_manager ('enter', 'MM_CH_BONDS', 'UTILITY')
      Ldebug=Local_Debug

      StypeX:select case(TypeX)
        case (0) StypeX ! C-H4 (done)
          R_ij=1.093D0
          k_ij=4.766D0
        case (20) StypeX ! CH3-C
          StypeY: select case (TypeY)
            case (20) StypeY ! CH3-CH3
              R_ij=1.093D0
              k_ij=4.766D0
            case (35) StypeY ! CH3-CH2,C
              R_ij=1.093D0
              k_ij=4.766D0
            case (45) StypeY ! CH3-CH,C2
              R_ij=1.093D0
              k_ij=4.766D0
            case (250) StypeY ! CH3-C=CH,C
              R_ij=1.093D0
              k_ij=4.766D0
            case (300) StypeY ! CH3-C=C-C2
              R_ij=1.093D0
              k_ij=4.766D0
            case (1500) StypeY ! CH3-C-(3)-C
              R_ij=1.093D0
              k_ij=4.766D0
          end select StypeY
        case (35) StypeX ! CH2-C2
          R_ij=1.093D0
          k_ij=4.766D0
        case (45) StypeX ! CH-C3
          R_ij=1.093D0
          k_ij=4.766D0
        case (150) StypeX ! CH2=C
          R_ij=1.083D0
          k_ij=4.258D0
        case (250) StypeX ! CH,C=C
          R_ij=1.083D0
          k_ij=4.258D0
        case (1000) StypeX ! H-C-(3)-C?
          R_ij=1.065D0
          k_ij=4.258D0
        case default StypeX ! Bond is not currently defined:
          write(UNIout,*)'MM_CH_BONDS> Atom type not supported or is a dummy'
          R_ij = ZERO
          k_ij = ZERO
      end select StypeX

! End of routine MM_CH_BONDS
      call PRG_manager ('exit', 'MM_CH_BONDS', 'UTILITY')
      return
      end subroutine MM_CH_BONDS
      subroutine MM_N1C_BONDS (TypeX, TypeY, R_ij, K_ij)
!******************************************************************************
! Date last modified: March 8, 2005                               Version 1.0 *
! Author: R. A. Poirier                                                       *
! Description: Get equilibrium bond lengths and force program_constants for NC single *
!              bonds                                                          *
!******************************************************************************
! Modules:

      implicit none

! Local scalars:
      integer, intent(IN) :: TypeX,TypeY
      double precision, intent(OUT) :: R_ij,K_ij
      logical Ldebug
!
! Begin:
      call PRG_manager ('enter', 'MM_N1C_BONDS', 'UTILITY')
      Ldebug=Local_Debug

      StypeX:select case(TypeX)
        case (15) StypeX ! N-C,H2
          R_ij=1.508D0
          k_ij=4.258D0
        case (25) StypeX ! N-C2,H
          R_ij=1.508D0
          k_ij=4.258D0
        case (30) StypeX ! N-C3
          R_ij=1.508D0
          k_ij=4.258D0
      end select StypeX

! End of routine MM_N1C_BONDS
      call PRG_manager ('exit', 'MM_N1C_BONDS', 'UTILITY')
      return
      end subroutine MM_N1C_BONDS
      subroutine MM_N2C_BONDS (TypeX, TypeY, R_ij, K_ij)
!******************************************************************************
! Date last modified: March 8, 2005                               Version 1.0 *
! Author: R. A. Poirier                                                       *
! Description: Get equilibrium bond lengths and force program_constants for NC double *
!              bonds                                                          *
!******************************************************************************
! Modules:

      implicit none

! Local scalars:
      integer, intent(IN) :: TypeX,TypeY
      double precision, intent(OUT) :: R_ij,K_ij
      logical Ldebug
!
! Begin:
      call PRG_manager ('enter', 'MM_N2C_BONDS', 'UTILITY')
      Ldebug=Local_Debug

      StypeX:select case(TypeX)
        case (100) StypeX ! N=C,H
          R_ij=1.508D0
          k_ij=4.258D0
        case (150) StypeX ! N=C,C
          R_ij=1.508D0
          k_ij=4.258D0
      end select StypeX

! End of routine MM_N2C_BONDS
      call PRG_manager ('exit', 'MM_N2C_BONDS', 'UTILITY')
      return
      end subroutine MM_N2C_BONDS
      subroutine MM_N3C_BONDS (TypeN, TypeC, R_ij, K_ij)
!******************************************************************************
! Date last modified: March 8, 2005                               Version 1.0 *
! Author: R. A. Poirier                                                       *
! Description: Get equilibrium bond lengths and force program_constants for NC triple *
!              bonds                                                          *
!******************************************************************************
! Modules:

      implicit none

! Local scalars:
      integer, intent(IN) :: TypeN,TypeC
      double precision, intent(OUT) :: R_ij,K_ij
      logical Ldebug
!
! Begin:
      call PRG_manager ('enter', 'MM_N3C_BONDS', 'UTILITY')
      Ldebug=Local_Debug

      StypeN:select case(TypeN)
        case (500) StypeN ! N-(3)-C
          R_ij=1.508D0
          k_ij=4.258D0
      end select StypeN

! End of routine MM_N3C_BONDS
      call PRG_manager ('exit', 'MM_N3C_BONDS', 'UTILITY')
      return
      end subroutine MM_N3C_BONDS
      subroutine MM_OH_BONDS (TypeO, TypeH, R_ij, K_ij)
!*****************************************************************************
! Date last modified: March 8, 2005                              Version 1.0 *
! Author: R. A. Poirier                                                      *
! Description: Get equilibrium bond lengths and force program_constants for OH bonds *
!*****************************************************************************
! Modules:

      implicit none

! Local scalars:
      integer, intent(IN) :: TypeO,TypeH
      double precision, intent(OUT) :: R_ij,K_ij
      logical Ldebug
!
! Begin:
      call PRG_manager ('enter', 'MM_OH_BONDS', 'UTILITY')
      Ldebug=Local_Debug

      StypeO:select case(TypeO)
        case (0) StypeO ! O-H2
          R_ij=1.508D0
          k_ij=4.258D0
        case (10) StypeO ! O-H,C
          R_ij=1.508D0
          k_ij=4.258D0
      end select StypeO

! End of routine MM_OH_BONDS
      call PRG_manager ('exit', 'MM_OH_BONDS', 'UTILITY')
      return
      end subroutine MM_OH_BONDS
      subroutine MM_OC_BONDS (TypeO, TypeC, R_ij, K_ij)
!*****************************************************************************
! Date last modified: March 8, 2005                              Version 1.0 *
! Author: R. A. Poirier                                                      *
! Description: Get equilibrium bond lengths and force program_constants for OC bonds *
!*****************************************************************************
! Modules:

      implicit none

! Local scalars:
      integer, intent(IN) :: TypeO,TypeC
      double precision, intent(OUT) :: R_ij,K_ij
      logical Ldebug
!
! Begin:
      call PRG_manager ('enter', 'MM_OC_BONDS', 'UTILITY')
      Ldebug=Local_Debug

      StypeO:select case(TypeO)
        case (10) StypeO ! O-C,H
          R_ij=1.508D0
          k_ij=4.258D0
        case (15) StypeO ! O-C,C
          R_ij=1.508D0
          k_ij=4.258D0
        case (50) StypeO ! O=C
          R_ij=1.508D0
          k_ij=4.258D0
      end select StypeO

! End of routine MM_OC_BONDS
      call PRG_manager ('exit', 'MM_OC_BONDS', 'UTILITY')
      return
      end subroutine MM_OC_BONDS
      subroutine MM_NH_BONDS (TypeX, TypeY, R_ij, K_ij)
!*****************************************************************************
! Date last modified: March 8, 2005                              Version 1.0 *
! Author: R. A. Poirier                                                      *
! Description: Get equilibrium bond lengths and force program_constants for NH bonds *
!*****************************************************************************
! Modules:

      implicit none

! Local scalars:
      integer, intent(IN) :: TypeX,TypeY
      double precision, intent(OUT) :: R_ij,K_ij
      logical Ldebug
!
! Begin:
      call PRG_manager ('enter', 'MM_NH_BONDS', 'UTILITY')
      Ldebug=Local_Debug

      StypeX:select case(TypeX)
        case (0) StypeX ! N-H3
          R_ij=1.508D0
          k_ij=4.258D0
        case (15) StypeX ! N-H2,C
          R_ij=1.508D0
          k_ij=4.258D0
        case (25) StypeX ! N-H,C2
          R_ij=1.508D0
          k_ij=4.258D0
      end select StypeX

! End of routine MM_NH_BONDS
      call PRG_manager ('exit', 'MM_NH_BONDS', 'UTILITY')
      return
      end subroutine MM_NH_BONDS
      subroutine MM_XX_BONDS (TypeX, TypeY, R_ij, K_ij)
!*****************************************************************************
! Date last modified: March 8, 2005                              Version 1.0 *
! Author: R. A. Poirier                                                      *
! Description: Get equilibrium bond lengths and force program_constants for XX bonds *
!*****************************************************************************
! Modules:

      implicit none

! Local scalars:
      integer, intent(IN) :: TypeX,TypeY
      double precision, intent(OUT) :: R_ij,K_ij
      logical Ldebug
!
! Begin:
      call PRG_manager ('enter', 'MM_XX_BONDS', 'UTILITY')
      Ldebug=Local_Debug

      StypeX:select case(TypeX)
        case (15) StypeX
          R_ij=1.508D0
          k_ij=4.258D0
        case (25) StypeX
          R_ij=1.508D0
          k_ij=4.258D0
        case (30) StypeX
          R_ij=1.508D0
          k_ij=4.258D0
        case (100) StypeX
          R_ij=1.508D0
          k_ij=4.258D0
        case (150) StypeX
          R_ij=1.508D0
          k_ij=4.258D0
        case (500) StypeX
          R_ij=1.508D0
          k_ij=4.258D0
      end select StypeX

! End of routine MM_XX_BONDS
      call PRG_manager ('exit', 'MM_XX_BONDS', 'UTILITY')
      return
      end subroutine MM_XX_BONDS
      subroutine GETANG_PARAMS
!*****************************************************************
! Date last modified: 23 March, 2000                 Version 1.0 *
! Author: R.A. Poirier and Michelle Shaw                         *
! Description: Obtain equilibrium angles.                        *
!*****************************************************************
! Modules:

      implicit none

! Local scalars:
      integer case_angle,Iatom,Jatom,Katom,IBend,TypeK,bond_ik,bond_kj
      integer Ring_size,ZatomI,ZatomJ,ZatomK,Ztemp
      double precision :: theta_ijk,k_ijk
      logical Ldebug
!
! Begin:
      call PRG_manager ('enter', 'GETANG_PARAMS', 'UTILITY')
      Ldebug=Local_Debug
!
! Allocate work arrays:
      if(.not.allocated(MMbend))then
        allocate(MMbend(RIC%Nangles))
      end if

!      ANGTYP = .false.

! Next, the equilibrium angle bend must be obtained based on the type of atoms involved in the angle.
      do IBend = 1,RIC%Nangles
        Iatom = ANGLE(IBend)%ATOM1
        Katom = ANGLE(IBend)%ATOM2 ! Katom is the central atom
        Jatom = ANGLE(IBend)%ATOM3
        ZatomI = CARTESIANS%Atomic_number(Iatom)
        if(ZatomI.eq.0)cycle
        ZatomJ = CARTESIANS%Atomic_number(Jatom)
        if(ZatomJ.eq.0)cycle
        ZatomK = CARTESIANS%Atomic_number(Katom)
        if(ZatomK.eq.0)cycle
        if(ZatomI.gt.ZatomJ)then ! Want Zj > Zi
          Ztemp=ZatomI
          ZatomI=ZatomJ
          ZatomJ=Ztemp
        end if

        Ring_size=ANGLE(IBend)%ring_size

        TypeK=vertex(Katom)%type ! Get atom type for central atom

! NOTE: 12100=110*110
        case_angle = 12100*(ZatomI-1)+110*(ZatomK-1)+(ZatomJ-1)

        theta_ijk = ZERO
        k_ijk = ZERO
        ANGLEtypeS:select case(case_angle)
          case(550) ANGLEtypeS ! H-C-H
            call HCH_ANG_PARM (TypeK, theta_ijk, k_ijk)
          case(555) ANGLEtypeS ! H-C-C
            call HCC_ANG_PARM (TypeJ, TypeK, theta_ijk, k_ijk)
          case(556) ANGLEtypeS ! H-C-N
            call HCN_ANG_PARM (TypeJ, TypeK, theta_ijk, k_ijk)
          case(557) ANGLEtypeS ! H-C-O
            call HCO_ANG_PARM (TypeJ, TypeK, theta_ijk, k_ijk)
          case(558) ANGLEtypeS ! H-C-F
            call HCF_ANG_PARM (TypeJ, TypeK, theta_ijk, k_ijk)
          case(566) ANGLEtypeS ! H-C-Cl
            call HCCl_ANG_PARM (TypeJ, TypeK, theta_ijk, k_ijk)
          case(660) ANGLEtypeS ! H-N-H
            call HNH_ANG_PARM (TypeK, theta_ijk, k_ijk)
          case(770) ANGLEtypeS ! H-O-H
            call HOH_ANG_PARM (TypeK, theta_ijk, k_ijk)
          case (61055) ANGLEtypeS ! C-C-C
            select case (Ring_size)
              case (0)
                call CCC_ANGLES_R0 (TypeK, theta_ijk, k_ijk)
              case (3)
                call CCC_ANGLES_R3 (TypeK, theta_ijk, k_ijk)
              case (4)
                call CCC_ANGLES_R4 (TypeK, theta_ijk, k_ijk)
              case (5)
                call CCC_ANGLES_R5 (TypeK, theta_ijk, k_ijk)
              case (6)
                call CCC_ANGLES_R6 (TypeK, theta_ijk, k_ijk)
              case (7)
                call CCC_ANGLES_R7 (TypeK, theta_ijk, k_ijk)
              case (8)
                call CCC_ANGLES_R8 (TypeK, theta_ijk, k_ijk)
              end select 
          case (61056) ANGLEtypeS ! C-C-N
            select case (Ring_size)
              case (0)
                call CCN_ANGLES_R0 (TypeK, theta_ijk, k_ijk)
              case (3)
                call CCN_ANGLES_R3 (TypeK, theta_ijk, k_ijk)
              case (4)
                call CCN_ANGLES_R4 (TypeK, theta_ijk, k_ijk)
              case (5)
                call CCN_ANGLES_R5 (TypeK, theta_ijk, k_ijk)
              case (6)
                call CCN_ANGLES_R6 (TypeK, theta_ijk, k_ijk)
              case (7)
                call CCN_ANGLES_R7 (TypeK, theta_ijk, k_ijk)
              case (8)
                call CCN_ANGLES_R8 (TypeK, theta_ijk, k_ijk)
              end select 
          case (61057) ANGLEtypeS ! C-C-O
            select case (Ring_size)
              case (0)
                call CCO_ANGLES_R0 (TypeK, theta_ijk, k_ijk)
              case (3)
                call CCO_ANGLES_R3 (TypeK, theta_ijk, k_ijk)
              case (4)
                call CCO_ANGLES_R4 (TypeK, theta_ijk, k_ijk)
              case (5)
                call CCO_ANGLES_R5 (TypeK, theta_ijk, k_ijk)
              case (6)
                call CCO_ANGLES_R6 (TypeK, theta_ijk, k_ijk)
              case (7)
                call CCO_ANGLES_R7 (TypeK, theta_ijk, k_ijk)
              case (8)
                call CCO_ANGLES_R8 (TypeK, theta_ijk, k_ijk)
              end select 
          case (61058) ANGLEtypeS ! C-C-F
            call CCF_ANGLES (TypeK, theta_ijk, k_ijk)
          case (61066) ANGLEtypeS ! C-C-Cl
            call CCCl_ANGLES (TypeK, theta_ijk, k_ijk)
          case DEFAULT ANGLEtypeS
            write(UNIout,*)'GETANG_PARAMS> ERROR'
            write(UNIout,*)'Atom types not supported or are dummies.'
            call PRG_stop ('Atom types not supported or are dummies')
        end select ANGLEtypeS
        MMbend(IBend)%theta_ijk=theta_ijk
        MMbend(IBend)%k_ijk=k_ijk
!        if(MMbend(IBend)%theta_ijk.ge.(175.0D0*Deg_to_Radian).and.
!    .      MMbend(IBend)%theta_ijk.le.PI_VAL)then
!          ANGTYP(IBend) = .true.
!        end if
! Printing for debugging and things that are not accessible via an output statement:
!        if(Ldebug)then
          write(UNIout,*)'equilibrium angle: ',MMbend(IBend)%theta_ijk
          write(UNIout,*)'angle force constant: ',MMbend(IBend)%k_ijk
!        end if
      end do

      if(Ldebug)then
        write(UNIout,*)'debug_GETANG_PARAMS > RIC%Nangles: ',RIC%Nangles
      end if

! End of routine GETANG_PARAMS
      call PRG_manager ('exit', 'GETANG_PARAMS', 'UTILITY')
      return
      end subroutine GETANG_PARAMS
      subroutine GETTOR_PARAMS
!****************************************************************************
! Date last modified: 23 March, 2000                            Version 1.0 *
! Author: R.A. Poirier and Michelle Shaw                                    *
! Description: Calculates the force program_constants for the angle torsion and     *
!              places them in a derived type called MMtorsion.              *
!****************************************************************************
! Modules:

      implicit none

! Local scalars:
      integer Iatom,Jatom,Katom,Latom,TypeI,TypeJ,TypeK,TypeL,Itorsion,torsion_case
      integer ZatomI,ZatomJ,ZatomK,ZatomL
      logical Ldebug
!
! Begin:
      call PRG_manager ('enter', 'GETTOR_PARAMS', 'UTILITY')
      Ldebug=Local_Debug
!
! Allocate work arrays:
      if(.not.allocated(MMtorsion))then
        allocate(MMtorsion(RIC%Ntorsions))
      end if

! Now the bond types are needed to distinguish between aromatic sp2 carbons and aliphatic sp2
!  carbons...
! If the atom in question is bonded to another atom by an aromatic bond,then this is its type, but
!  if not, it is considered to be aliphatic sp2...
      do Itorsion = 1,RIC%Ntorsions
        Iatom = TORSION(Itorsion)%ATOM1
        Jatom = TORSION(Itorsion)%ATOM2
        Katom = TORSION(Itorsion)%ATOM3
        Latom = TORSION(Itorsion)%ATOM4
        ZatomI = CARTESIANS%Atomic_number(Iatom)
        if(ZatomI.eq.0)cycle
        ZatomJ = CARTESIANS%Atomic_number(Jatom)
        if(ZatomJ.eq.0)cycle
        ZatomK = CARTESIANS%Atomic_number(Katom)
        if(ZatomK.eq.0)cycle
        ZatomL = CARTESIANS%Atomic_number(Latom)
        if(ZatomK.eq.0)cycle
        if(ZatomI.gt.ZatomL)then ! Want Zl > Zi change to L-K-J-I
          Ztemp=ZatomI
          ZatomI=ZatomL
          ZatomL=Ztemp
          Ztemp=ZatomK
          ZatomK=ZatomJ
          ZatomJ=Ztemp
        end if

        TypeI = vertex(Iatom)%type
        TypeJ = vertex(Jatom)%type
        TypeK = vertex(Katom)%type
        TypeL = vertex(Latom)%type

! NOTE: 131000=110*110*110, 12100=110*110
        torsion_case=1331000*(ZatomI-1)+12100*(ZatomJ-1)+110*(ZatomK-1)+(ZatomL-1)

        TORStypeS:select case (torsion_case)
          case(61050) TORStypeS
            call GETTOR_PARMS_HCCH (Itorsion, TypeJ, TypeK)
          case(61055) TORStypeS
            call GETTOR_PARMS_HCCC (Itorsion, TypeJ, TypeK, TypeL)
          case (6716050) TORStypeS
            call GETTOR_PARMS_CCCC (Itorsion, TypeI, TypeJ, TypeK, TypeL)
          case DEFAULT TORStypeS
            write(UNIout,*)'Atom type not supported or is a dummy.'
        end select TORStypeS
! Printing for debugging and things that are not accessible via an output statement:
!        if(Ldebug)then
          write(UNIout,*)'V1: ',MMtorsion(Itorsion)%V1
          write(UNIout,*)'V2: ',MMtorsion(Itorsion)%V2
          write(UNIout,*)'V3: ',MMtorsion(Itorsion)%V3
!        end if
      end do

      if(Ldebug)then
        write(UNIout,*)'debug_GETTOR_PARAMS > RIC%Ntorsions: ',RIC%Ntorsions
      end if

! End of routine GETTOR_PARAMS
      call PRG_manager ('exit', 'GETTOR_PARAMS', 'UTILITY')
      return
      end subroutine GETTOR_PARAMS
      subroutine GETOBND_PARAMS
!****************************************************************************
! Date last modified: 23 March, 2000                            Version 1.0 *
! Author: Michelle Shaw                                                     *
! Description: Calculates the force program_constants for the out-of-plane bend     *
!              and places them in an array oopbend_k_ijkl.                  *
!****************************************************************************
! Modules:
      USE redundant_coordinates

      implicit none

! Local scalars:
      integer IoopBend,Iatom,Jatom,Katom,Latom,TypeI,TypeJ,TypeK,TypeL,case_obend
      integer ZatomI,ZatomJ,ZatomK,ZatomL
      double precision fc
      logical Ldebug
!
! Begin:
      call PRG_manager ('enter', 'GETOBND_PARAMS', 'UTILITY')
      Ldebug=Local_Debug


! Allocate work arrays:
      if(.not.allocated(MMoopbend_k_ijkl))then
        allocate (MMoopbend_k_ijkl(RIC%Noopbends))
      end if

! Zero input array:
      MMoopbend_k_ijkl = ZERO

! Determining out-of-plane bend force program_constants is based on the types of the atoms (i, j, k, l)
! The central atom is Jatom.
      do IoopBend = 1,RIC%Noopbends
        Iatom = oopbend(IoopBend)%ATOM1
        Jatom = oopbend(IoopBend)%ATOM2
        Katom = oopbend(IoopBend)%ATOM3
        Latom = oopbend(IoopBend)%ATOM4
        ZatomI = CARTESIANS%Atomic_number(Iatom)
        if(ZatomI.eq.0)cycle
        ZatomJ = CARTESIANS%Atomic_number(Jatom)
        if(ZatomJ.eq.0)cycle
        ZatomK = CARTESIANS%Atomic_number(Katom)
        if(ZatomK.eq.0)cycle
        ZatomL = CARTESIANS%Atomic_number(Latom)
        if(ZatomK.eq.0)cycle

        TypeI = vertex(Iatom)%type
        TypeJ = vertex(Jatom)%type
        TypeK = vertex(Katom)%type
        TypeL = vertex(Latom)%type

        case_obend = 1331000*(ZatomI-1)+12100*(ZatomJ-1)+110*(ZatomK-1)+(ZatomL-1)

        OOPBend_case:select case(case_obend)
! HCCH out-of-plane bend
          case(61050) OOPBend_case
            call GETOOPBend_HCCH (TypeJ, TypeK, fc)
! CCHH out-of-plane bend
          case(6715500) OOPBend_case
            call GETOOPBend_HCCH (TypeI, TypeJ, fc)
! HCCC, CCCH or CCHC out-of-plane bend
          case(61055, 6716050, 6715505) OOPBend_case
            call GETOOPBend_HCCC (TypeI, TypeJ, TypeK, TypeL, fc)
! CCCC out-of-plane bend
          case(6716055) OOPBend_case
            call GETOOPBend_CCCC (TypeI, TypeJ, TypeK, TypeL, fc)
         case DEFAULT OOPBend_case
            write(UNIout,*)'Out-of-plane bend not supported.'
            write(UNIout,*)'i, j, k, l: ',TypeI,' ',TypeJ,' ',TypeK,' ',TypeL
            fc = ZERO
        end select OOPBend_case
        MMoopbend_k_ijkl(IoopBend) = fc
! Printing for debugging and things that are not accessible via an output statement:
!        if(Ldebug)then
          write(UNIout,*)'out-of-plane bend force constant: ',MMoopbend_k_ijkl(IoopBend)
!        end if
      end do

      if(Ldebug)then
        write(UNIout,*)'debug_GETOBND_PARAMS > RIC%Noopbends: ',RIC%Noopbends
      end if

! End of routine GETOBND_PARAMS
      call PRG_manager ('exit', 'GETOBND_PARAMS', 'UTILITY')
      return
      end subroutine GETOBND_PARAMS
      subroutine GETVDW_PARAMS
!*************************************************************************
! Date last modified: 26 March, 2000                         Version 1.0 *
! Author: Michelle Shaw                                                  *
! Description: Calculate the minimum energy separation and well depth    *
!              to be used in the van der Waals energy determination.     *
!*************************************************************************
! formulas are based on MMFF94 force field, see :
!        Halgren, T. A., J. Comp. Chem, vol. 17 (5&6) p. 520 (1996)
!
! Local scalars:
      integer iELEC,Iatom,Jatom
      double precision num,denom,G_I,N_I,G_J,N_J,alpha_I,alpha_J,A_I,A_J,sum_IJ,gamma_IJ,R_II,R_JJ
      logical Ldebug
!
! Begin:
      call PRG_manager ('enter', 'GETVDW_PARAMS', 'UTILITY')
      Ldebug=Local_Debug
!
! Allocate work arrays:
      if(.not.allocated(MMVderW))then
        allocate (MMVderW(max0(1,RIC%Nnonbonded)))
      end if

! Call subroutine minE_sep to compute the minimum energy separations and calculate the well depth.
! Place both into the object MMVderW for use in the energy expression.
      do iELEC = 1,RIC%Nnonbonded
        Iatom = pair_IJ(iELEC)%I
        Jatom = pair_IJ(iELEC)%J
        if(pair_IJ(iELEC)%is_vdw.eq.1)then
          PARMI:select case(vertex(Iatom)%type)
            case(1) PARMI
              A_I = A_scalef(1)
              alpha_I = alpha(1)
              G_I = G_scalef(1)
              N_I = N_valE(1)
            case(2) PARMI
              A_I = A_scalef(2)
              G_I = G_scalef(2)
              N_I = N_valE(2)
              alpha_I = alpha(2)
            case(4) PARMI
              A_I = A_scalef(3)
              G_I = G_scalef(3)
              N_I = N_valE(3)
              alpha_I = alpha(3)
            case(5) PARMI
              A_I = A_scalef(4)
              G_I = G_scalef(4)
              N_I = N_valE(4)
              alpha_I = alpha(4)
            case(20) PARMI
              A_I = A_scalef(5)
              G_I = G_scalef(5)
              N_I = N_valE(5)
              alpha_I = alpha(5)
            case(22) PARMI
              A_I = A_scalef(6)
              G_I = G_scalef(6)
              N_I = N_valE(6)
              alpha_I = alpha(6)
            case(30) PARMI
              A_I = A_scalef(7)
              G_I = G_scalef(7)
              N_I = N_valE(7)
              alpha_I = alpha(7)
            case(37) PARMI
              A_I = A_scalef(8)
              G_I = G_scalef(8)
              N_I = N_valE(8)
              alpha_I = alpha(8)
            case DEFAULT PARMI
              write(UNIout,*)'Wrong atom type!'
          end select PARMI
          PARMJ:select case(vertex(Jatom)%type)
            case(1) PARMJ
              A_J = A_scalef(1)
              G_J = G_scalef(1)
              N_J = N_valE(1)
              alpha_J = alpha(1)
            case(2) PARMJ
              A_J = A_scalef(2)
              G_J = G_scalef(2)
              N_J = N_valE(2)
              alpha_J = alpha(2)
            case(4) PARMJ
              A_J = A_scalef(3)
              G_J = G_scalef(3)
              N_J = N_valE(3)
              alpha_J = alpha(3)
            case(5) PARMJ
              A_J = A_scalef(4)
              G_J = G_scalef(4)
              N_J = N_valE(4)
              alpha_J = alpha(4)
            case(20) PARMJ
              A_J = A_scalef(5)
              G_J = G_scalef(5)
              N_J = N_valE(5)
              alpha_J = alpha(5)
            case(22) PARMJ
              A_J = A_scalef(6)
              G_J = G_scalef(6)
              N_J = N_valE(6)
              alpha_J = alpha(6)
            case(30) PARMJ
              A_J = A_scalef(7)
              G_J = G_scalef(7)
              N_J = N_valE(7)
              alpha_J = alpha(7)
            case(37) PARMJ
              A_J = A_scalef(8)
              G_J = G_scalef(8)
              N_J = N_valE(8)
              alpha_J = alpha(8)
            case DEFAULT PARMJ
              write(UNIout,*)'Wrong atom type!'
          end select PARMJ
!          if(Ldebug)then
            write(UNIout,*)'atom type of I: ',vertex(Iatom)%type
            write(UNIout,*)'debug_GETVDW_PARAMS > A_I, N_I, G_I, alpha_I: ',A_I,' ',N_I,' ',G_I,' ',alpha_I
            write(UNIout,*)'atom type of J: ',vertex(Jatom)%type
            write(UNIout,*)'debug_GETVDW_PARAMS > A_J, N_J, G_J, alpha_J: ',A_J,' ',N_J,' ',G_J,' ',alpha_J
!          end if
! R_IJ calculation:
          R_II = A_I*(alpha_I**0.25D0)
          R_JJ = A_J*(alpha_J**0.25D0)
          if(R_II.eq.R_JJ)then
            MMVderW(iELEC)%R_IJ = R_II
          else
            sum_IJ = (R_II + R_JJ)
            gamma_IJ = (R_II - R_JJ)/sum_IJ
            MMVderW(iELEC)%R_IJ = 0.5D0*sum_IJ*(ONE + B_scalef*(ONE-dexp(-beta*(gamma_IJ**2))))
          end if
! epsilon_IJ calculation:
          num = 181.16D0*G_I*G_J*alpha_I*alpha_J
          denom = ( dsqrt(alpha_I/N_I) + dsqrt(alpha_J/N_J))*(MMVderW(iELEC)%R_IJ**6)

          MMVderW(iELEC)%epsilon_IJ = num/denom
        else
          MMVderW(iELEC)%R_IJ = ZERO
          MMVderW(iELEC)%epsilon_IJ = ZERO
        end if
! Printing for debugging and things that are not accessible via an output statement:
         if(Ldebug)then
          write(UNIout,*)'minimum energy separation: ',MMVderW(iELEC)%R_IJ
          write(UNIout,'(a,f20.12)')'Well depth: ',MMVderW(iELEC)%epsilon_IJ
         end if
      end do

! End of routine GETVDW_PARAMS
      call PRG_manager ('exit', 'GETVDW_PARAMS', 'UTILITY')
      return
      end subroutine GETVDW_PARAMS
      subroutine GETELEC_PARAMS
!***************************************************************************
! Date last modified: 12 February, 2000                        Version 1.0 *
! Author: Michelle Shaw                                                    *
! Description: Calculate and store in derived type MMelectrostatic charges *
!              for each atom i and j in a pair.                            *
!***************************************************************************
      implicit none

! Local scalars:
      double precision w_IK, w_JK, q0_I, q0_J, bciI, bciJ
      integer iELEC, iCTR, Iatom, Jatom, Iatom_type, Jatom_type, Katom_type,Natoms
      logical Ldebug
!
! Begin:
      call PRG_manager ('enter','GETELEC_PARAMS', 'UTILITY')
      Ldebug=Local_Debug
!
! Allocate work arrays:
      if(.not.allocated(MMelectrostatic))then
        allocate (MMelectrostatic(max0(1,RIC%Nnonbonded)))
      end if

      Natoms=size(Connect, 1)
! Zero scalars:
      w_IK = ZERO
      w_JK = ZERO
      q0_I = ZERO
      q0_J = ZERO
      bciI = ZERO
      bciJ = ZERO

! get partial bond charge increments from nonbonded parameters based on atoms I and K, and their
!  orientation, i.e. w_KI = -w_IK.
      do iELEC = 1,RIC%Nnonbonded
        Iatom = pair_IJ(iELEC)%I
        Jatom = pair_IJ(iELEC)%J
        Iatom_type = vertex(Iatom)%type
        Jatom_type = vertex(Jatom)%type
        do iCTR = 1,Natoms
          if(CONNECT(Iatom,iCTR).eq.1)then
            Katom_type = vertex(iCTR)%type
            call GET_WIK(Iatom_type,Katom_type,w_IK,q0_I)
            bciI = bciI + w_IK
          end if
          if(CONNECT(Jatom,iCTR).eq.1)then
            Katom_type = vertex(iCTR)%type
            call GET_WIK(Jatom_type,Katom_type,w_JK,q0_J)
            bciJ = bciJ + w_JK
          end if
        end do
        MMelectrostatic(iELEC)%q_I = q0_I + bciI
        MMelectrostatic(iELEC)%q_J = q0_J + bciJ
        bciI = ZERO
        bciJ = ZERO
! Printing for debugging and things that are not accessible via an output statement:
        if(Ldebug)then
          write(UNIout,*)'I, charge on I: ',Iatom_type,' ',MMelectrostatic(iELEC)%q_I
          write(UNIout,*)'J, charge on J: ',Jatom_type,' ',MMelectrostatic(iELEC)%q_J
        end if
      end do

      if(Ldebug)then
        write(UNIout,*)'debug_GETELEC_PARAMS > I, q_I, J, q_J: ',pair_IJ(1)%I, &
                       MMelectrostatic(1)%q_I, pair_IJ(1)%J, MMelectrostatic(1)%q_J
      end if

! End of routine GETELEC_PARAMS:
      call PRG_manager ('exit', 'GETELEC_PARAMS', 'UTILITY')
      return
      end subroutine GETELEC_PARAMS

      subroutine GETSTRBND_PARAMS
!****************************************************************************
! Date last modified: 23 March, 2000                            Version 1.0 *
! Author: Michelle Shaw                                                     *
! Description: Calculates the force program_constants for the coupling of a bond    *
!              stretch to an angle bend and places them in a derived type   *
!              stretch_bend.                                                *
!****************************************************************************
      implicit none

! Local scalars:
      integer Iatom,Jatom,Katom,ZatomI,ZatomJ,ZatomK,TypeI,TypeJ,TypeK
      integer IBend,case_strbnd,bond_ik,bond_kj
      double precision stb_fc_ijk, stb_fc_kji
      logical Ldebug
!
! Begin:
      call PRG_manager ('enter', 'GETSTRBND_PARAMS', 'UTILITY')
      Ldebug=Local_Debug

! Allocate work arrays;
      if(.not.allocated(MMstr_bend))then
        allocate (MMstr_bend(RIC%Nangles))
      end if

! Next, the angle bend force constant must be obtained based on the type of atoms involved
! in the angle.
      do IBend=1,RIC%Nangles
        Iatom = ANGLE(IBend)%ATOM1
        Katom = ANGLE(IBend)%ATOM2 ! Katom is the central atom...
        Jatom = ANGLE(IBend)%ATOM3
        ZatomI = CARTESIANS%Atomic_number(Iatom)
        if(ZatomI.eq.0)cycle
        ZatomJ = CARTESIANS%Atomic_number(Jatom)
        if(ZatomJ.eq.0)cycle
        ZatomK = CARTESIANS%Atomic_number(Katom)
        if(ZatomK.eq.0)cycle

        TypeI = vertex(Iatom)%type
        TypeJ = vertex(Jatom)%type
        TypeK = vertex(Katom)%type

        case_strbnd = 12100*(ZatomI-1)+110*(ZatomK-1)+(ZatomJ-1)

        bond_ik = BT(Iatom, Katom)
        bond_kj = BT(Jatom, Katom)

        ANGLEtypeS:select case (case_strbnd)
          case(550) ANGLEtypeS
            call GET_STB_FC_HCH(TypeK, stb_fc_ijk, stb_fc_kji)
            MMstr_bend(IBend)%k_ijk = stb_fc_ijk
            MMstr_bend(IBend)%k_kji = stb_fc_kji

          case(555) ANGLEtypeS
            call GET_STB_FC_HCC(TypeJ, TypeK, bond_kj, stb_fc_ijk, stb_fc_kji)
            MMstr_bend(IBend)%k_ijk = stb_fc_ijk
            MMstr_bend(IBend)%k_kji = stb_fc_kji

          case (61050) ANGLEtypeS
            call GET_STB_FC_CCH(TypeI, TypeK, bond_ik, stb_fc_ijk, stb_fc_kji)
            MMstr_bend(IBend)%k_ijk = stb_fc_ijk
            MMstr_bend(IBend)%k_kji = stb_fc_kji

          case (61055) ANGLEtypeS
            call GET_STB_FC_CCC(TypeI, TypeJ, TypeK, bond_ik, bond_kj, stb_fc_ijk,stb_fc_kji)
            MMstr_bend(IBend)%k_ijk = stb_fc_ijk
            MMstr_bend(IBend)%k_kji = stb_fc_kji

          case DEFAULT ANGLEtypeS
            write(UNIout,*)'Atom type not supported or is a dummy.'
            MMstr_bend(IBend)%k_ijk = ZERO
            MMstr_bend(IBend)%k_kji = ZERO
        end select ANGLEtypeS

! Printing for debugging and things that are not accessible via an output statement:
        if(Ldebug)then
          write(UNIout,*)'force constant for ijk angle: ',MMstr_bend(IBend)%k_ijk
          write(UNIout,*)'force constant for kji angle: ',MMstr_bend(IBend)%k_kji
        end if
      end do

      if(Ldebug)then
        write(UNIout,*)'debug_GETSTRBND_PARAMS > RIC%Nbonds: ',RIC%Nbonds
        write(UNIout,*)'debug_GETSTRBND_PARAMS > RIC%Nangles: ',RIC%Nangles
      end if

! End of routine GETSTRBND_PARAMS
      call PRG_manager ('exit', 'GETSTRBND_PARAMS', 'UTILITY')
      return
      end subroutine GETSTRBND_PARAMS
      end module FF_parameters
      subroutine CCC_ANGLES_R0 (TypeK, & !type for atom k (central atom)
                               theta_ijk, k_ijk)
!***************************************************************************
! Date last modified: 24 August, 1999                          Version 1.0 *
! Author: R.A. Poirier                                                     *
! Description: Get equilibrium angles and force program_constants for a CCC angle. *
!              Not part of a ring                                          *
!***************************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer :: TypeK
      double precision :: k_ijk,theta_ijk

! Local scalars:
      logical Ldebug
!
! Begin:
! This is where the parameters are selected for both equilibrium angles and force program_constants.
! All values simply defaulted to 1-1-1 case
      StypeK:select case (TypeK)
        case(35) StypeK ! H2-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(36) StypeK ! O=CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(45) StypeK ! H,C-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(50) StypeK ! C2-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(53) StypeK ! H,O-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(58) StypeK ! C,O-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(250) StypeK ! H-CC=C or aromatic
          theta_ijk=109.608
          k_ijk=0.851
        case(300) StypeK ! C-CC=C or aromatic
          theta_ijk=109.608
          k_ijk=0.851
        case(1500) StypeK ! CC-(3)-C
          theta_ijk=109.608
          k_ijk=0.851
      case DEFAULT StypeK
         call PRG_stop ('CCC_ANGLES_R0> iATOM: Illegal force field atom type!')
      end select StypeK

! End of function CCC_ANGLES_R0
      return
      end
      subroutine CCC_ANGLES_R3 (TypeK, & !type for atom k (central atom)
                               theta_ijk, k_ijk)
!***************************************************************************
! Date last modified: 24 August, 1999                          Version 1.0 *
! Author: R.A. Poirier                                                     *
! Description: Get equilibrium angles and force program_constants for a CCC angle. *
!              Part of a 3-membered ring                                   *
!***************************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer :: TypeK
      double precision :: k_ijk,theta_ijk

! Local scalars:
      logical Ldebug
!
! Begin:
! This is where the parameters are selected for both equilibrium angles and force program_constants.
! All values simply defaulted to 1-1-1 case
      StypeK:select case (TypeK)
        case(35) StypeK ! H2-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(36) StypeK ! O=CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(45) StypeK ! H,C-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(50) StypeK ! C2-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(53) StypeK ! H,O-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(58) StypeK ! C,O-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(250) StypeK ! H-CC=C or aromatic
          theta_ijk=109.608
          k_ijk=0.851
        case(300) StypeK ! C-CC=C or aromatic
          theta_ijk=109.608
          k_ijk=0.851
      case DEFAULT StypeK
         call PRG_stop ('CCC_ANGLES_R3> iATOM: Illegal force field atom type!')
      end select StypeK

! End of function CCC_ANGLES_R3
      return
      end
      subroutine CCC_ANGLES_R4 (TypeK, & !type for atom k (central atom)
                               theta_ijk, k_ijk)
!***************************************************************************
! Date last modified: 24 August, 1999                          Version 1.0 *
! Author: R.A. Poirier                                                     *
! Description: Get equilibrium angles and force program_constants for a CCC angle. *
!              Part of a 4-membered ring                                   *
!***************************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer :: TypeK
      double precision :: k_ijk,theta_ijk

! Local scalars:
      logical Ldebug
!
! Begin:
! This is where the parameters are selected for both equilibrium angles and force program_constants.
! All values simply defaulted to 1-1-1 case
      StypeK:select case (TypeK)
        case(35) StypeK ! H2-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(36) StypeK ! O=CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(45) StypeK ! H,C-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(50) StypeK ! C2-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(53) StypeK ! H,O-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(58) StypeK ! C,O-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(250) StypeK ! H-CC=C or aromatic
          theta_ijk=109.608
          k_ijk=0.851
        case(300) StypeK ! C-CC=C or aromatic
          theta_ijk=109.608
          k_ijk=0.851
        case(1500) StypeK ! CC-(3)-C
          theta_ijk=109.608
          k_ijk=0.851
      case DEFAULT StypeK
         call PRG_stop ('CCC_ANGLES_R4> iATOM: Illegal force field atom type!')
      end select StypeK

! End of function CCC_ANGLES_R4
      return
      end
      subroutine CCC_ANGLES_R5 (TypeK, & !type for atom k (central atom)
                               theta_ijk, k_ijk)
!***************************************************************************
! Date last modified: 24 August, 1999                          Version 1.0 *
! Author: R.A. Poirier                                                     *
! Description: Get equilibrium angles and force program_constants for a CCC angle. *
!              Part of a 5-membered ring                                   *
!***************************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer :: TypeK
      double precision :: k_ijk,theta_ijk

! Local scalars:
      logical Ldebug
!
! Begin:
! This is where the parameters are selected for both equilibrium angles and force program_constants.
! All values simply defaulted to 1-1-1 case
      StypeK:select case (TypeK)
        case(35) StypeK ! H2-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(36) StypeK ! O=CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(45) StypeK ! H,C-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(50) StypeK ! C2-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(53) StypeK ! H,O-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(58) StypeK ! C,O-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(250) StypeK ! H-CC=C or aromatic
          theta_ijk=109.608
          k_ijk=0.851
        case(300) StypeK ! C-CC=C or aromatic
          theta_ijk=109.608
          k_ijk=0.851
        case(1500) StypeK ! CC-(3)-C
          theta_ijk=109.608
          k_ijk=0.851
      case DEFAULT StypeK
         call PRG_stop ('CCC_ANGLES_R5> iATOM: Illegal force field atom type!')
      end select StypeK

! End of function CCC_ANGLES_R5
      return
      end
      subroutine CCC_ANGLES_R6 (TypeK, & !type for atom k (central atom)
                               theta_ijk, k_ijk)
!***************************************************************************
! Date last modified: 24 August, 1999                          Version 1.0 *
! Author: R.A. Poirier                                                     *
! Description: Get equilibrium angles and force program_constants for a CCC angle. *
!              Part of a 6-membered ring                                   *
!***************************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer :: TypeK
      double precision :: k_ijk,theta_ijk

! Local scalars:
      logical Ldebug
!
! Begin:
! This is where the parameters are selected for both equilibrium angles and force program_constants.
! All values simply defaulted to 1-1-1 case
      StypeK:select case (TypeK)
        case(35) StypeK ! H2-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(36) StypeK ! O=CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(45) StypeK ! H,C-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(50) StypeK ! C2-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(53) StypeK ! H,O-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(58) StypeK ! C,O-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(250) StypeK ! H-CC=C or aromatic
          theta_ijk=109.608
          k_ijk=0.851
        case(300) StypeK ! C-CC=C or aromatic
          theta_ijk=109.608
          k_ijk=0.851
        case(1500) StypeK ! CC-(3)-C
          theta_ijk=109.608
          k_ijk=0.851
      case DEFAULT StypeK
         call PRG_stop ('CCC_ANGLES_R6> iATOM: Illegal force field atom type!')
      end select StypeK

! End of function CCC_ANGLES_R6
      return
      end
      subroutine CCC_ANGLES_R7 (TypeK, & !type for atom k (central atom)
                               theta_ijk, k_ijk)
!***************************************************************************
! Date last modified: 24 August, 1999                          Version 1.0 *
! Author: R.A. Poirier                                                     *
! Description: Get equilibrium angles and force program_constants for a CCC angle. *
!              Part of a 7-membered ring                                   *
!***************************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer :: TypeK
      double precision :: k_ijk,theta_ijk

! Local scalars:
      logical Ldebug
!
! Begin:
! This is where the parameters are selected for both equilibrium angles and force program_constants.
! All values simply defaulted to 1-1-1 case
      StypeK:select case (TypeK)
        case(35) StypeK ! H2-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(36) StypeK ! O=CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(45) StypeK ! H,C-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(50) StypeK ! C2-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(53) StypeK ! H,O-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(58) StypeK ! C,O-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(250) StypeK ! H-CC=C or aromatic
          theta_ijk=109.608
          k_ijk=0.851
        case(300) StypeK ! C-CC=C or aromatic
          theta_ijk=109.608
          k_ijk=0.851
        case(1500) StypeK ! CC-(3)-C
          theta_ijk=109.608
          k_ijk=0.851
      case DEFAULT StypeK
         call PRG_stop ('CCC_ANGLES_R7> iATOM: Illegal force field atom type!')
      end select StypeK

! End of function CCC_ANGLES_R7
      return
      end
      subroutine CCC_ANGLES_R8 (TypeK, & !type for atom k (central atom)
                               theta_ijk, k_ijk)
!***************************************************************************
! Date last modified: 24 August, 1999                          Version 1.0 *
! Author: R.A. Poirier                                                     *
! Description: Get equilibrium angles and force program_constants for a CCC angle. *
!              Part of a 8-membered ring                                   *
!***************************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer :: TypeK
      double precision :: k_ijk,theta_ijk

! Local scalars:
      logical Ldebug
!
! Begin:
! This is where the parameters are selected for both equilibrium angles and force program_constants.
! All values simply defaulted to 1-1-1 case
      StypeK:select case (TypeK)
        case(35) StypeK ! H2-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(36) StypeK ! O=CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(45) StypeK ! H,C-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(50) StypeK ! C2-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(53) StypeK ! H,O-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(58) StypeK ! C,O-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(250) StypeK ! H-CC=C or aromatic
          theta_ijk=109.608
          k_ijk=0.851
        case(300) StypeK ! C-CC=C or aromatic
          theta_ijk=109.608
          k_ijk=0.851
        case(1500) StypeK ! CC-(3)-C
          theta_ijk=109.608
          k_ijk=0.851
      case DEFAULT StypeK
         call PRG_stop ('CCC_ANGLES_R8> iATOM: Illegal force field atom type!')
      end select StypeK

! End of function CCC_ANGLES_R8
      return
      end
      subroutine CCO_ANGLES_R0 (TypeK, & !type for atom k (central atom)
                               theta_ijk, k_ijk)
!***************************************************************************
! Date last modified: 24 August, 1999                          Version 1.0 *
! Author: R.A. Poirier                                                     *
! Description: Get equilibrium angles and force program_constants for a CCO angle. *
!              Not part of a ring                                          *
!***************************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer :: TypeK
      double precision :: k_ijk,theta_ijk

! Local scalars:
      logical Ldebug
!
! Begin:
! This is where the parameters are selected for both equilibrium angles and force program_constants.
! All values simply defaulted to 1-1-1 case
      StypeK:select case (TypeK)
        case(43) StypeK ! H2-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(400) StypeK ! O=CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(53) StypeK ! H,C-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(58) StypeK ! C2-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(59) StypeK ! H,O-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(64) StypeK ! C,O-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(310) StypeK ! H-CC=O or H-OC=C
          theta_ijk=109.608
          k_ijk=0.851
        case(360) StypeK ! C-OC=C or C-CC=O
          theta_ijk=109.608
          k_ijk=0.851
        case(1900) StypeK ! OC-(3)-C
          theta_ijk=109.608
          k_ijk=0.851
      case DEFAULT StypeK
         call PRG_stop ('CCO_ANGLES_R0> iATOM: Illegal force field atom type!')
      end select StypeK

! End of function CCO_ANGLES_R0
      return
      end
      subroutine CCO_ANGLES_R3 (TypeK, & !type for atom k (central atom)
                               theta_ijk, k_ijk)
!***************************************************************************
! Date last modified: 24 August, 1999                          Version 1.0 *
! Author: R.A. Poirier                                                     *
! Description: Get equilibrium angles and force program_constants for a CCO angle. *
!              Part of a 3-membered ring                                   *
!***************************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer :: TypeK
      double precision :: k_ijk,theta_ijk

! Local scalars:
      logical Ldebug
!
! Begin:
! This is where the parameters are selected for both equilibrium angles and force program_constants.
! All values simply defaulted to 1-1-1 case
      StypeK:select case (TypeK)
        case(43) StypeK ! H2-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(400) StypeK ! O=CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(53) StypeK ! H,C-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(58) StypeK ! C2-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(59) StypeK ! H,O-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(64) StypeK ! C,O-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(310) StypeK ! H-CC=O or H-OC=C
          theta_ijk=109.608
          k_ijk=0.851
        case(360) StypeK ! C-OC=C or C-CC=O
          theta_ijk=109.608
          k_ijk=0.851
        case(1900) StypeK ! OC-(3)-C
          theta_ijk=109.608
          k_ijk=0.851
      case DEFAULT StypeK
         call PRG_stop ('CCO_ANGLES_R3> iATOM: Illegal force field atom type!')
      end select StypeK

! End of function CCO_ANGLES_R3
      return
      end
      subroutine CCO_ANGLES_R4 (TypeK, & !type for atom k (central atom)
                               theta_ijk, k_ijk)
!***************************************************************************
! Date last modified: 24 August, 1999                          Version 1.0 *
! Author: R.A. Poirier                                                     *
! Description: Get equilibrium angles and force program_constants for a CCO angle. *
!              Part of a 4-membered ring                                   *
!***************************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer :: TypeK
      double precision :: k_ijk,theta_ijk

! Local scalars:
      logical Ldebug
!
! Begin:
! This is where the parameters are selected for both equilibrium angles and force program_constants.
! All values simply defaulted to 1-1-1 case
      StypeK:select case (TypeK)
        case(43) StypeK ! H2-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(400) StypeK ! O=CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(53) StypeK ! H,C-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(58) StypeK ! C2-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(59) StypeK ! H,O-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(64) StypeK ! C,O-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(310) StypeK ! H-CC=O or H-OC=C
          theta_ijk=109.608
          k_ijk=0.851
        case(360) StypeK ! C-OC=C or C-CC=O
          theta_ijk=109.608
          k_ijk=0.851
        case(1900) StypeK ! OC-(3)-C
          theta_ijk=109.608
          k_ijk=0.851
      case DEFAULT StypeK
         call PRG_stop ('CCO_ANGLES_R4> iATOM: Illegal force field atom type!')
      end select StypeK

! End of function CCO_ANGLES_R4
      return
      end
      subroutine CCO_ANGLES_R5 (TypeK, & !type for atom k (central atom)
                               theta_ijk, k_ijk)
!***************************************************************************
! Date last modified: 24 August, 1999                          Version 1.0 *
! Author: R.A. Poirier                                                     *
! Description: Get equilibrium angles and force program_constants for a CCO angle. *
!              Part of a 5-membered ring                                   *
!***************************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer :: TypeK
      double precision :: k_ijk,theta_ijk

! Local scalars:
      logical Ldebug
!
! Begin:
! This is where the parameters are selected for both equilibrium angles and force program_constants.
! All values simply defaulted to 1-1-1 case
      StypeK:select case (TypeK)
        case(43) StypeK ! H2-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(400) StypeK ! O=CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(53) StypeK ! H,C-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(58) StypeK ! C2-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(59) StypeK ! H,O-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(64) StypeK ! C,O-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(310) StypeK ! H-CC=O or H-OC=C
          theta_ijk=109.608
          k_ijk=0.851
        case(360) StypeK ! C-OC=C or C-CC=O
          theta_ijk=109.608
          k_ijk=0.851
        case(1900) StypeK ! OC-(3)-C
          theta_ijk=109.608
          k_ijk=0.851
      case DEFAULT StypeK
         call PRG_stop ('CCO_ANGLES_R5> iATOM: Illegal force field atom type!')
      end select StypeK

! End of function CCO_ANGLES_R5
      return
      end
      subroutine CCO_ANGLES_R6 (TypeK, & !type for atom k (central atom)
                               theta_ijk, k_ijk)
!***************************************************************************
! Date last modified: 24 August, 1999                          Version 1.0 *
! Author: R.A. Poirier                                                     *
! Description: Get equilibrium angles and force program_constants for a CCO angle. *
!              Part of a 6-membered ring                                   *
!***************************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer :: TypeK
      double precision :: k_ijk,theta_ijk

! Local scalars:
      logical Ldebug
!
! Begin:
! This is where the parameters are selected for both equilibrium angles and force program_constants.
! All values simply defaulted to 1-1-1 case
      StypeK:select case (TypeK)
        case(43) StypeK ! H2-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(400) StypeK ! O=CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(53) StypeK ! H,C-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(58) StypeK ! C2-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(59) StypeK ! H,O-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(64) StypeK ! C,O-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(310) StypeK ! H-CC=O or H-OC=C
          theta_ijk=109.608
          k_ijk=0.851
        case(360) StypeK ! C-OC=C or C-CC=O
          theta_ijk=109.608
          k_ijk=0.851
        case(1900) StypeK ! OC-(3)-C
          theta_ijk=109.608
          k_ijk=0.851
      case DEFAULT StypeK
         call PRG_stop ('CCO_ANGLES_R6> iATOM: Illegal force field atom type!')
      end select StypeK

! End of function CCO_ANGLES_R6
      return
      end
      subroutine CCO_ANGLES_R7 (TypeK, & !type for atom k (central atom)
                               theta_ijk, k_ijk)
!***************************************************************************
! Date last modified: 24 August, 1999                          Version 1.0 *
! Author: R.A. Poirier                                                     *
! Description: Get equilibrium angles and force program_constants for a CCO angle. *
!              Part of a 7-membered ring                                   *
!***************************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer :: TypeK
      double precision :: k_ijk,theta_ijk

! Local scalars:
      logical Ldebug
!
! Begin:
! This is where the parameters are selected for both equilibrium angles and force program_constants.
! All values simply defaulted to 1-1-1 case
      StypeK:select case (TypeK)
        case(43) StypeK ! H2-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(400) StypeK ! O=CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(53) StypeK ! H,C-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(58) StypeK ! C2-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(59) StypeK ! H,O-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(64) StypeK ! C,O-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(310) StypeK ! H-CC=O or H-OC=C
          theta_ijk=109.608
          k_ijk=0.851
        case(360) StypeK ! C-OC=C or C-CC=O
          theta_ijk=109.608
          k_ijk=0.851
        case(1900) StypeK ! OC-(3)-C
          theta_ijk=109.608
          k_ijk=0.851
      case DEFAULT StypeK
         call PRG_stop ('CCO_ANGLES_R7> iATOM: Illegal force field atom type!')
      end select StypeK

! End of function CCO_ANGLES_R7
      return
      end
      subroutine CCO_ANGLES_R8 (TypeK, & !type for atom k (central atom)
                               theta_ijk, k_ijk)
!***************************************************************************
! Date last modified: 24 August, 1999                          Version 1.0 *
! Author: R.A. Poirier                                                     *
! Description: Get equilibrium angles and force program_constants for a CCO angle. *
!              Part of a 8-membered ring                                   *
!***************************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer :: TypeK
      double precision :: k_ijk,theta_ijk

! Local scalars:
      logical Ldebug
!
! Begin:
! This is where the parameters are selected for both equilibrium angles and force program_constants.
! All values simply defaulted to 1-1-1 case
      StypeK:select case (TypeK)
        case(43) StypeK ! H2-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(400) StypeK ! O=CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(53) StypeK ! H,C-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(58) StypeK ! C2-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(59) StypeK ! H,O-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(64) StypeK ! C,O-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(310) StypeK ! H-CC=O or H-OC=C
          theta_ijk=109.608
          k_ijk=0.851
        case(360) StypeK ! C-OC=C or C-CC=O
          theta_ijk=109.608
          k_ijk=0.851
        case(1900) StypeK ! OC-(3)-C
          theta_ijk=109.608
          k_ijk=0.851
      case DEFAULT StypeK
         call PRG_stop ('CCO_ANGLES_R8> iATOM: Illegal force field atom type!')
      end select StypeK

! End of function CCO_ANGLES_R8
      return
      end
