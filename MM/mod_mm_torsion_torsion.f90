      module torsion_torsion
!********************************************************************************
! Date last modified: 21 March, 2000                                Version 1.0 *
! Author: Michelle Shaw                                                         *
! Description: Contains object and initialization routine for torsion-torsion   *
!              energy contributions.                                            *
!********************************************************************************
! Modules:
      USE program_manager

      implicit none

      double precision, dimension(:,:), allocatable :: ENERGIES_TORTOR

      CONTAINS

      subroutine CLCTORTOR
!*****************************************************************
! Date last modified: 21 March, 2000                 Version 1.0 *
! Author: Michelle Shaw                                          *
! Description: Calculate individual contributions to the         *
!              torsion-torsion portion of the MM energy for      *
!              select atoms in the molecule                      *
!*****************************************************************
! Modules:
      USE program_constants
      USE GRAPH_objects

      implicit none

! Begin:
      call PRG_manager ('enter', 'CLCTORTOR', 'UTILITY')
      Ldebug=Local_Debug

      call GET_object ('MOL', 'GRAPH', 'TORSION_ATOMS')

! allocate work arrays if not already allocated:
      if(.not.allocated(ENERGIES_TORTOR)) allocate(ENERGIES_TORTOR(NGTORSION, NGTORSION))

! Calculation of the individual torsion-torsion energy contributions is done here...

      ENERGIES_TORTOR = ZERO

! End of routine CLCTORTOR
      call PRG_manager ('exit', 'CLCTORTOR', 'UTILITY')
      return
      end subroutine CLCTORTOR

      end module torsion_torsion

