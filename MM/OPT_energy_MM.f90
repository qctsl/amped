      subroutine OPT_energy_MM
!***********************************************************************
!     Date last modified: January 28, 1998                 Version 2.0 *
!     Author: R. A. Poirier                                            *
!     Description: Prepare parameters and gradients for optimization.  *
!***********************************************************************
! Modules:
      USE OPT_defaults
      USE OPT_objects
      USE mm_class_energy

      implicit none
!
! Local functions:
!
! Begin:
      call PRG_manager ('enter', 'OPT_energy_MM', 'UTILITY')
!
      call GET_object ('MM', 'ENERGY', 'MMFF94') !Get an energy for wavefunction
      OPT_function%value=E_MM%total
!
! End of routine OPT_energy_MM
      call PRG_manager ('exit', 'OPT_energy_MM', 'UTILITY')
      return
      end

