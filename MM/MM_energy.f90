      subroutine CLC_ENERGY_MM
!**********************************************************************
! Author: Michelle Shaw                                   Version 1.0 *
! Date last modified: 18 March, 2000                                  *
! Description: This is the routine that computes the total MM energy. *
!**********************************************************************
! Modules:
      USE program_files
      USE mm_class_energy
      USE QM_objects

      implicit none

!
! Begin:
      call PRG_manager ('enter', 'CLC_ENERGY_MM', 'ENERGY%MMFF94')

!     call GET_object ('MOL', 'INTERNAL', 'RIC') ! Build coordinate set
!
! Object:
      call GET_object ('MM', 'PARAMETER_CONTRIBUTIONS', 'MMFF94') ! Gets RIC

! Calculate total energy from contributions due to stretches, bends, ...
      if(RIC%NBonds.gt.0)then
        call MMVStretch
      end if
      if(RIC%NAngles.gt.0)then
        call MMVBend
      end if
      if(RIC%NTorsions.gt.0)then
        call MMVTorsion
      end if
      if(RIC%Noopbends.gt.0)then
        call MMVOopbend
      end if
      if(RIC%Nnonbonded.gt.0)then
        call MMVVDW
      end if
      if(RIC%Nnonbonded.gt.0)then
        call MMVElectronic
      end if
      if(RIC%NAngles.gt.0)then
        call MMVStrBnd
      end if

! Compute total energy:
      E_MM%total=E_MM%stretch+E_MM%bend+E_MM%torsion+E_MM%oopbend+E_MM%VderW+E_MM%electrostatic+E_MM%stretch_bend
! Must decide how to deal with this
!     ENERGY_total_MM=E_MM%total
      write(UNIout,'(a,1PE20.8)')'Total energy: ',E_MM%total

! Print object information:
      write(UNIout,'(/a)')'Energy: '
      if(RIC%Nbonds.gt.0)then
        write(UNIout,'(a,i8)')'Number of stretch contributions = ',RIC%Nbonds
        write(UNIout,'(a,1PE20.8)')'Total stretch energy = ',E_MM%stretch
      end if ! RIC%Nbonds

      if(RIC%Nangles.gt.0)then
        write(UNIout,'(a,i8)')'Number of bend contributions = ',RIC%Nangles
        write(UNIout,'(a,1PE20.8)')'Total bend energy = ',E_MM%bend
      end if ! RIC%Nangles

      if(RIC%Ntorsions.gt.0)then
        write(UNIout,'(a,i8)')'Number of torsion contributions = ',RIC%Ntorsions
        write(UNIout,'(a,1PE20.8)')'Total torsion energy = ',E_MM%torsion
      end if ! RIC%Ntorsions

      if(RIC%Noopbends.gt.0)then
        write(UNIout,'(a,i8)')'Number of out-of-plane bend contributions = ',RIC%Noopbends
        write(UNIout,'(a,1PE20.8)')'Total out-of-plane bend energy = ',E_MM%oopbend
      end if ! RIC%Noopbends

      if(RIC%Nbonds.gt.0.and.RIC%Nangles.gt.0)then
        write(UNIout,'(a,i8)')'Number of stretch-bend contributions = ',RIC%Nangles
        write(UNIout,'(a,1PE20.8)')'Total stretch-bend energy = ',E_MM%stretch_bend
      end if ! RIC%Nbonds and RIC%Nangles

      if(RIC%Nnonbonded.gt.0)then
        write(UNIout,'(a,i8)')'Number of nonbonded contributions = ',RIC%Nnonbonded
        write(UNIout,'(a,1PE20.8)')'Total nonbonded energy = ',E_MM%VderW + &
                                                               E_MM%electrostatic
      end if ! RIC%Nnonbonded
!
! End of routine CLC_ENERGY_MM
      call PRG_manager ('exit', 'CLC_ENERGY_MM', 'ENERGY%MMFF94')
      return
      end subroutine CLC_ENERGY_MM
