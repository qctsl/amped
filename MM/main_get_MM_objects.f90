      subroutine GET_MM_object (class, Objname, Modality)
!**************************************************************************
!     Date last modified: October 5, 2000                     Version 2.0 *
!     Author: R.A. Poirier                                                *
!     Description: MM objects.                                           *
!**************************************************************************
! Modules:
      USE program_files
      USE program_objects

      implicit none
!
! Input scalar:
      character*(*) :: class
      character*(*) :: Objname
      character*(*) :: Modality
!
! Local scalars:
      integer :: Object_number
!
! Begin:
      call PRG_manager ('enter', 'GET_MM_object', 'UTILITY')
!
     call GET_object_number (OBJ_MM, NMMobjects, FirstMM, Objname, Modality, Object_number)

      if(OBJ_MM(Object_number)%current)then
        call PRG_manager ('exit', 'GET_MM_object', 'UTILITY')
        return
      end if

      OBJ_MM(Object_number)%exist=.true.
      OBJ_MM(Object_number)%Current=.true.
      Object(ObjNum)%exist=.true.
      Object(ObjNum)%Current=.true.

      select_Object: select case (Objname)
      case ('ATOM_TYPES')
        call BLD_MM_atom_types
      case ('ENERGY')
        call CLC_ENERGY_MM
      case ('PARAMETER_CONTRIBUTIONS')
        call BLD_MM_PARAMETERS
      case ('GRADIENTS')
        call BLD_MM_GRADIENTS
      case ('FORCE_CONSTANT')
        call BLD_MM_2nd_derivative
      case default
        write(UNIout,*)'ERROR> GET_MM_object> No such object "', &
                        Objname(1:len_trim(Objname)),'" for class "', &
                        class(1:len_trim(class)),'"'
        stop 'No such object'
      end select select_Object
!
      call PRG_manager ('exit', 'GET_MM_object', 'UTILITY')
      return
      end subroutine GET_MM_object
