      subroutine RIC_GRD_MM
!*****************************************************************
! Date last modified: 1 April, 2000                  Version 1.0 *
! Author: Michelle Shaw                                          *
! Description: Combine gradients for stretches, bends, and       *
!              torsions into an array called PARGRD.             *
!*****************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE GRAPH_objects
      USE mm_gradients
      USE redundant_coordinates
      USE OPT_defaults
      USE OPT_objects
      USE type_molecule

      implicit none
!
! Local scalars:
      integer Ntotal
!
! Begin:
      call PRG_manager ('enter', 'RIC_GRD_MM', 'UTILITY')
!
! For NOW!
      call GET_object ('MM', 'GRADIENTS', 'MMFF94')

!      if(OPT_coord%nonbonded)then
!        NRIC=RIC%NIcoordinates
!      else
!        NRIC=RIC%NIcoordinates-RIC%Nnonbonded
!      end if

! Gradients are combined here, starting with stretches,then bends, and finally torsions.
      Ntotal=0
      if(RIC%Nbonds.gt.0.and.OPT_coord%bonds)then
        pargrd(1:RIC%Nbonds)=G_stretch(1:RIC%Nbonds)
        Ntotal=RIC%Nbonds
      end if

      if(RIC%Nangles.gt.0.and.OPT_coord%angles)then
        pargrd(Ntotal+1:Ntotal+RIC%Nangles)=G_bend(1:RIC%Nangles)
        Ntotal=Ntotal+RIC%Nangles
      end if

      if(RIC%Ntorsions.gt.0.and.OPT_coord%torsions)then
        pargrd(Ntotal+1:Ntotal+RIC%Ntorsions)=G_torsion(1:RIC%Ntorsions)
        Ntotal=Ntotal+RIC%Ntorsions
      end if

      if(RIC%Noopbends.gt.0.and.OPT_coord%oopbends)then
        pargrd(Ntotal+1:Ntotal+RIC%Noopbends)=G_oopbend(1:RIC%Noopbends)
        Ntotal=Ntotal+RIC%Noopbends
      end if

      if(RIC%Nnonbonded.gt.0.and.OPT_coord%nonbonded)then
        pargrd(Ntotal+1:Ntotal+RIC%Nnonbonded)=G_VderWs(1:RIC%Nnonbonded)+ &
                                               G_electrostatic(1:RIC%Nnonbonded)
        Ntotal=Ntotal+RIC%Nnonbonded
      end if

      if(Ntotal.ne.Noptpr)then
        write(UNIout,'(a)')'RIC_GRD_MM> ERROR: Ntotal .ne. Noptpr'
        write(UNIout,'(a,i8)')'RIC_GRD_MM> Ntotal: ',Ntotal
        write(UNIout,'(a,i8)')'RIC_GRD_MM> Noptpr: ',Noptpr
        stop 'RIC_GRD_MM> ERROR: Ntotal .ne. Noptpr'
      end if

! End of routine RIC_GRD_MM
      call PRG_manager ('exit', 'RIC_GRD_MM', 'UTILITY')
      return
      end subroutine RIC_GRD_MM
