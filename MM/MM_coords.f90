      double precision function TORSION_IN(I,J,K,L)
!*********************************************************************
! Date last modified: 6 July, 1999                       Version 1.0 *
! Author: Michelle Shaw                                              *
! Description: Calculate torsion angle given four atoms and their    *
!              Cartesian coordinates.                                *
!*********************************************************************
! Modules:
      USE program_manager
      USE program_constants

      implicit none
!
! Input scalars:
      integer I, J, K, L

! Local scalars:
      double precision R, T, lenS, CUTOFF,RU, RV, RW, RX, RY, RS
      logical Ldebug
!
! Local arrays:
      double precision U(3), V(3), W(3), X(3), Y(3), S(3)

! Begin:
!      call PRG_manager ('enter', 'TORSION_IN', 'UTILITY')
!      Ldebug=Local_Debug

      CUTOFF = 1.0D-11

! After the atoms have been selected, the vectorss must be calculated (length and
!  direction)...
      call VEC (U,RU,I,J)
      call VEC (V,RV,K,J)
      call VEC (W,RW,K,L)

! Now, compute the two cross products IJxJK and JKxKL...
      call VEC_product(X,U,V,RX,1)
      call VEC_product(Y,W,V,RY,1)


! The scalar product will be calculated between X and Y.  Also, the cross product between
!  X and Y to form the vector Z will be taken.
      R = -DOT_Product(X,Y)
      call VEC_product(S,X,Y,RS,0)
      T = DOT_Product(S,V)

! Set the default value of the angle RIC_torsion(iTOR) to Pi/2.  If the torsion angle is 90
! degrees, the scalar product of X and Y will be zero.
      TORSION_IN = PI_VAL/TWO
      if(DABS(R).ge.CUTOFF) then
        lenS = DSQRT(S(1)*S(1) + S(2)*S(2) + S(3)*S(3))
        TORSION_IN = DATAN(lenS/R)
        if(R.lt.ZERO)TORSION_IN = TORSION_IN + PI_VAL
      end if
      if(T.lt.ZERO)TORSION_IN = -TORSION_IN
!      if(dabs(TORSION_IN).ge.(3.1D0))TORSION_IN = dabs(TORSION_IN)

! End of routine TORSION_IN
!      call PRG_manager ('exit', 'TORSION_IN', 'UTILITY')
      return
      END
      double precision function OOPBEND_IN(I,J,K,L)
!*********************************************************************
! Date last modified: 6 July, 1999                       Version 1.0 *
! Author: Michelle Shaw                                              *
! Description: Calculate out-of-plane bend angle given four atoms    *
!              and their Cartesian coordinates.                      *
!*********************************************************************
! Modules:
      USE program_manager
      USE program_constants

      implicit none
!
! Input scalars:
      integer I, J, K, L

! Local scalars:
      double precision R, c_theta, CUTOFF, DOT_Product, RU, RV, RW, RY
!      logical Ldebug
!
! Local arrays:
      double precision U(3), V(3), W(3), Y(3)
!
! Begin:
!      call PRG_manager ('enter', 'OOPBEND_IN', 'UTILITY')
!      Ldebug=Local_Debug

! After the atoms have been selected, the vectors must be calculated (length and
!   direction)...

      call VEC (U,RU,I,J)
      call VEC (V,RV,J,K)
      call VEC (W,RW,J,L)

! Now, compute the two cross product JKxJL...
      call VEC_product(Y,V,W,RY,1)

! The scalar product will be calculated between U and Y.
      R = DOT_Product(U,Y)

! Now calculate the out-of-plane bend angle.  If the atoms are coplanar, this angle should be zero.
      c_theta = dsqrt(ONE - R*R)
      OOPBEND_IN = -DACOS(c_theta)

! End of routine OOPBEND_IN
!      call PRG_manager ('exit', 'OOPBEND_IN', 'UTILITY')
      return
      END
