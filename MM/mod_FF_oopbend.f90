      module FF_parameters
!**********************************************************************************
! Date last modified: March 22, 2005                                  Version 1.0 *
! Author: R. A. Poirier                                                           *
! Description: Contains object and initialization routines for stretch parameters.*                                                       !**********************************************************************************
! Modules:
      USE program_manager
      USE program_defaults
      USE program_constants
      USE type_molecule
      USE mm_interaction_type_params
      USE nobond_params
      USE GRAPH_objects
      USE atom_type

      implicit none

      double precision, dimension(:), allocatable :: MMoopbend_k_ijkl !out-of-plane bend force constant

CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine GETOBND_PARAMS
!****************************************************************************
! Date last modified: 23 March, 2000                            Version 1.0 *
! Author: Michelle Shaw                                                     *
! Description: Calculates the force program_constants for the out-of-plane bend     *
!              and places them in an array oopbend_k_ijkl.                  *
!****************************************************************************
! Modules:
      USE redundant_coordinates

      implicit none

! Local scalars:
      integer IoopBend,Iatom,Jatom,Katom,Latom,TypeI,TypeJ,TypeK,TypeL,case_obend
      integer ZatomI,ZatomJ,ZatomK,ZatomL
      double precision fc
      logical Ldebug
!
! Begin:
      call PRG_manager ('enter', 'GETOBND_PARAMS', 'UTILITY')
      Ldebug=Local_Debug


! Allocate work arrays:
      if(.not.allocated(MMoopbend_k_ijkl))then
        allocate (MMoopbend_k_ijkl(Noopbend))
      end if

! Zero input array:
      MMoopbend_k_ijkl = ZERO

! Determining out-of-plane bend force program_constants is based on the types of the atoms (i, j, k, l)
! The central atom is Jatom.
      do IoopBend = 1,Noopbend
        Iatom = oopbend(IoopBend)%ATOM1
        Jatom = oopbend(IoopBend)%ATOM2
        Katom = oopbend(IoopBend)%ATOM3
        Latom = oopbend(IoopBend)%ATOM4
        ZatomI = CARTESIANS%Atomic_number(Iatom)
        if(ZatomI.eq.0)cycle
        ZatomJ = CARTESIANS%Atomic_number(Jatom)
        if(ZatomJ.eq.0)cycle
        ZatomK = CARTESIANS%Atomic_number(Katom)
        if(ZatomK.eq.0)cycle
        ZatomL = CARTESIANS%Atomic_number(Latom)
        if(ZatomK.eq.0)cycle

        TypeI = vertex(Iatom)%type
        TypeJ = vertex(Jatom)%type
        TypeK = vertex(Katom)%type
        TypeL = vertex(Latom)%type

        case_obend = 1331000*(ZatomI-1)+12100*(ZatomJ-1)+110*(ZatomK-1)+(ZatomL-1)

        OOPBend_case:select case(case_obend)
! HCCH out-of-plane bend
          case(61050) OOPBend_case
            call GETOOPBend_HCCH (TypeJ, TypeK, fc)
! CCHH out-of-plane bend
          case(6715500) OOPBend_case
            call GETOOPBend_HCCH (TypeI, TypeJ, fc)
! HCCC, CCCH or CCHC out-of-plane bend
          case(61055, 6716050, 6715505) OOPBend_case
            call GETOOPBend_HCCC (TypeI, TypeJ, TypeK, TypeL, fc)
! CCCC out-of-plane bend
          case(6716055) OOPBend_case
            call GETOOPBend_CCCC (TypeI, TypeJ, TypeK, TypeL, fc)
         case DEFAULT OOPBend_case
            write(UNIout,'(a)')'Out-of-plane bend not supported.'
            write(UNIout,'(a,4i8)')'i, j, k, l: ',TypeI,TypeJ,TypeK,TypeL
            fc = ZERO
        end select OOPBend_case
        MMoopbend_k_ijkl(IoopBend) = fc
! Printing for debugging and things that are not accessible via an output statement:
!        if(Ldebug)then
          write(UNIout,'(a,f12.6)')'out-of-plane bend force constant: ',MMoopbend_k_ijkl(IoopBend)
!        end if
      end do

      if(Ldebug)then
        write(UNIout,'(a,i8)')'debug_GETOBND_PARAMS > Noopbend: ',Noopbend
      end if

! End of routine GETOBND_PARAMS
      call PRG_manager ('exit', 'GETOBND_PARAMS', 'UTILITY')
      return
      end subroutine GETOBND_PARAMS
      end module FF_parameters
