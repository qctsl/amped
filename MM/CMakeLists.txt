#
# libMM.a
#
set( libMM_SRC
    mod_MM_program_objects.f90
    mod_mm_eq_bond_lengths.f90
    mod_mm_fconst_stretch.f90
    mod_mm_eq_angles.f90
    mod_mm_fconst_angles.f90
    mod_mm_tor_params.f90
    mod_mm_oopbend_fc.f90
    mod_mm_strbnd_fc.f90
    mod_mm_inter_type_params.f90
    mod_mm_nobond_params.f90
    mod_mm_atom_type.f90
    mod_mm_parameters.f90
    mod_mm_class_energy.f90
    mod_mm_gradients.f90
    mod_mm_second_derivatives.f90
    MM_print_manager.f90
    main_get_MM_objects.f90
    MENU_MM.f90
    OPT_energy_MM.f90
    BLD_MM_atom_types.f90
    MM_gradients.f90
    MM_hessian.f90
    MM_coords.f90
    MM_energy.f90
    MM_fconst.f90
    MM_secder.f90
    RIC_GRD_MM.f90
) # libMM_SRC

add_library( MM     STATIC	${libMM_SRC}        )

target_link_libraries( MM        MODS MOL OPT)
