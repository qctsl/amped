      subroutine MENU_MM
!***********************************************************************
!     Date last modified: June 24, 1992                                *
!     Author: F. Colonna and R. Poirier                                *
!     Description: Set up molecular mechanics calculation.             *
!***********************************************************************
! Modules:
      USE program_files
      USE program_parser
      USE menu_gets
      USE mod_type_hessian
      USE OPT_defaults
      USE OPT_objects
      USE mm_interaction_type_params

      implicit none
!
! Local scalars:
      logical done,lrun
!
! Begin:
      call PRG_manager ('enter', 'MENU_MM', 'UTILITY')
!
! Defaults:
      done=.false.
      lrun=.false.

      OPT_parameters%name=' '  ! Always use Redundant Internal Coordinates with MM
      OPT_parameters%name(1:3)='RIC'  ! Always use Redundant Internal Coordinates with MM
      OPT_function%name=' '
      OPT_function%name(1:2)='MM'
      Hessian_method(1:10)='ANALYTICAL'
!
! Menu:
      do while (.not.done)
      call new_token (' MM:')
!
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      ' Command MM:', &
      '   Purpose: Perform a Molecular Mechanics Calculation ', &
      '   Syntax :', &
      '   BOnds command, define bond types', &
      '   RUN command: Will cause execution', &
      '   end'
!
! BOnds
      else if(token(1:2).EQ.'BO')then
        call set_bond_type

      else if(token(1:3).EQ.'RUN')then
        lrun=.true.

      else
        call MENU_end (done)
      end if
!
      end do !(.not.done)
!
      if(lrun)call GET_object ('MM','ENERGY','MMFF94')
!
! End of routine MENU_MM
      call PRG_manager ('exit', 'MENU_MM', 'UTILITY')
      return
      END
      subroutine SET_bond_type
!***********************************************************************
!     Date last modified: June 24, 1992                                *
!     Author: F. Colonna and R. Poirier                                *
!     Description: Set up molecular mechanics calculation.             *
!***********************************************************************
! Modules:
      USE program_files
      USE program_parser
      USE menu_gets
      USE mod_type_hessian
      USE OPT_objects
      USE GRAPH_objects
      USE mm_interaction_type_params

      implicit none
!
! Local scalars:
      integer I,Itype,nlist
      double precision type
      logical done,found
!
! Local arrays:
      integer ATOMS(2)
!
! Begin:
      call PRG_manager ('enter', 'SET_bond_type', 'UTILITY')
!
! Defaults:
      done=.false.
!
      call GET_object ('MOL', 'GRAPH', 'BOND_ATOMS')
!
! Menu:
      do while (.not.done)
      call new_token (' MM:')
!
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      ' Command MM:', &
      '   Purpose: Perform a Molecular Mechanics Calculation ', &
      '   Syntax :', &
      '   AToms <integer list>, atoms defining the bond', &
      '   TYpe <real>, bond type, i.e., 1.0, 1.5, ..., 3.0', &
      '   end'
!
! AToms
      else if(token(1:2).EQ.'AT')then
        call GET_value (atoms, nlist)

      else if(token(1:2).EQ.'TY')then
        call GET_value (type)
        Itype=DINT(type*10.0D0)
        select case (Itype)
          case (10)
          Itype=1
          case (15)
          Itype=2
          case (20)
          Itype=3
          case (25)
          Itype=4
          case (30)
          Itype=5
          case default
          write(UNIout,*)'ERROR> SET_bond_type: illegal Bond type ', type
          stop 'ERROR> SET_bond_type: illegal Bond type'
        end select
        found=.false.
        do I=1,NGBONDS
        if(bond(I)%atom1.eq.ATOMS(1).and.bond(I)%atom2.eq.ATOMS(2))then
          Bond(I)%Type=Itype
          write(UNIout,'(a,i5,a,i5)')'Bond_type ',I,' set to ',Itype
          found=.true.
        else if(bond(I)%atom1.eq.ATOMS(2).and.bond(I)%atom2.eq.ATOMS(1))then
          Bond(I)%Type=Itype
          write(UNIout,'(a,i5,a,i5)')'Bond_type ',I,' set to ',Itype
          found=.true.
        end if
        end do ! I
        if(.not.found)then
          write(UNIout,*)'ERROR> SET_bond_type: illegal atoms ', ATOMS
          stop 'ERROR> SET_bond_type: illegal atoms '
        end if
      else
        call MENU_end (done)
      end if
!
      end do !(.not.done)
!
! End of routine SET_bond_type
      call PRG_manager ('exit', 'SET_bond_type', 'UTILITY')
      return
      END
