      subroutine HC_BOND_PARM (type_j, eq_bond_length, force)
!**********************************************************************
! Date last modified: 21 September, 1999                  Version 1.0 *
! Author: Michelle Shaw                                               *
! Description: Get equilibrium bond lengths and force program_constants for   *
!              a H-C stretch.                                         *
!**********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE eq_bond_lengths
      USE force_program_constants_stretch

      implicit none
!
! Input scalars:
      integer type_j
      double precision force, eq_bond_length

! Local scalars:
!
! Begin:
! This is where the parameters are selected for both equilibrium bond lengths and force program_constants.
      force = ZERO
      eq_bond_length = ZERO

      BOND_PARM:SELECT CASE (type_j)
        CASE(1) BOND_PARM !H-C(sp3)
          force = FORCE_CONSTANTS_SINGLE_BOND(20)
          eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(20)
        CASE(2) BOND_PARM
          force = FORCE_CONSTANTS_SINGLE_BOND(13)
          eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(13)
        CASE(4) BOND_PARM !H-C(sp)
          force = FORCE_CONSTANTS_SINGLE_BOND(5)
          eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(5)
        CASE(20) BOND_PARM !H-C(4-M ring), C is sp3
          force = FORCE_CONSTANTS_SINGLE_BOND(3)
          eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(3)
        CASE(22) BOND_PARM !H-C(3-M ring), C is sp2
          force = FORCE_CONSTANTS_SINGLE_BOND(2)
          eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(2)
        CASE(30) BOND_PARM !H-C(olefinic, 4-M ring)
          force = FORCE_CONSTANTS_SINGLE_BOND(4)
          eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(4)
        CASE(37)
          force = FORCE_CONSTANTS_SINGLE_BOND(24)
          eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(24)
        CASE DEFAULT BOND_PARM
          write(UNIout,'(a)')'HC_BOND_PARM> Bond not supported'
          call PRG_stop ('HC_BOND_PARM> Bond not supported')
      end SELECT BOND_PARM

! End of function HC_BOND_PARM
      return
      END
      subroutine CC_BOND_PARM (type_i, type_j, type_bond, eq_bond_length, force)
!**********************************************************************
! Date last modified: 24 August, 1999                     Version 1.0 *
! Author: Michelle Shaw                                               *
! Description: Get equilibrium bond lengths and force program_constants for   *
!              a C-C stretch.                                         *
!**********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE eq_bond_lengths
      USE force_program_constants_stretch

      implicit none
!
! Input scalars:
      integer type_i,type_j,type_bond
      double precision force,eq_bond_length

! Local scalars:
!
! Begin:
! This is where the parameters are selected for both equilibrium angles and force program_constants.
      force = ZERO
      eq_bond_length = ZERO

      BOND_PARMI:SELECT CASE(type_i)
        CASE(1) BOND_PARMI
          BOND_PARMJ1:SELECT CASE(type_j)
            CASE(1) BOND_PARMJ1
              force = FORCE_CONSTANTS_SINGLE_BOND(21)
              eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(21)
            CASE(2) BOND_PARMJ1
              force = FORCE_CONSTANTS_SINGLE_BOND(15)
              eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(15)
            CASE(4) BOND_PARMJ1
              force = FORCE_CONSTANTS_SINGLE_BOND(8)
              eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(8)
            CASE(20) BOND_PARMJ1
              force = FORCE_CONSTANTS_SINGLE_BOND(23)
              eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(23)
            CASE(22) BOND_PARMJ1
              force = FORCE_CONSTANTS_SINGLE_BOND(22)
              eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(22)
            CASE(37) BOND_PARMJ1
              force = FORCE_CONSTANTS_SINGLE_BOND(25)
              eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(25)
            CASE DEFAULT BOND_PARMJ1
              write(UNIout,'(a)')'CC_BOND_PARM> jATOM: Illegal force field atom type!'
              stop
          end SELECT BOND_PARMJ1
        CASE(2) BOND_PARMI
          BOND_PARMJ2:SELECT CASE(type_j)
            CASE(1) BOND_PARMJ2 !C(sp2)-C(sp3)
              force = FORCE_CONSTANTS_SINGLE_BOND(15)
              eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(15)
            CASE(2) BOND_PARMJ2
              if(type_bond.eq.1) then !C(sp2)-C(sp2)
                force = FORCE_CONSTANTS_SINGLE_BOND(14)
                eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(14)
              else if(type_bond.eq.3) then !C(sp2)=C(sp2)
                force = FORCE_CONSTANTS_DOUBLE_BOND(1)
                eq_bond_length = EQUILIBRIUM_BOND_LENGTH_DOUBLE(1)
              else !C(sp2)-C(sp2), conjugated bond
                write(UNIout,'(a)')'CC_BOND_PARM> defaulting to C(sp2)-C(sp2) single bond parameters.'
                force = FORCE_CONSTANTS_SINGLE_BOND(14)
                eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(14)
              end if
            CASE(4) BOND_PARMJ2 !C(sp2)-C(sp)
              if(type_bond.eq.1) then
                force = FORCE_CONSTANTS_SINGLE_BOND(7)
                eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(7)
              else
! Conjugated bond
                force = FORCE_CONSTANTS_AROMATIC_BOND(1)
                eq_bond_length = EQ_BOND_LENGTH_AROMATIC(1)
              end if
            CASE(20) BOND_PARMJ2 !C(sp2)-C(4M ring), C(sp3) in ring
              force = FORCE_CONSTANTS_SINGLE_BOND(18)
              eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(18)
            CASE(22) BOND_PARMJ2 !C(sp2)-C(3M ring), any C in ring
              force = FORCE_CONSTANTS_SINGLE_BOND(27) !or (17)?
              eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(27)
            CASE(30) BOND_PARMJ2 !C(sp2)-C(olefinic 4M ring)
              force = FORCE_CONSTANTS_SINGLE_BOND(19)
              eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(19)
            CASE(37) BOND_PARMJ2 !C(sp2)-C(aromatic)
              force = FORCE_CONSTANTS_SINGLE_BOND(16)
              eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(16)
            CASE DEFAULT BOND_PARMJ2
              write(UNIout,'(a)')'CC_BOND_PARM> jATOM: Illegal force field atom type!'
              stop
          end SELECT BOND_PARMJ2
        CASE(4) BOND_PARMI
          BOND_PARMJ3:SELECT CASE(type_j)
            CASE(1) BOND_PARMJ3 !C(sp)-C(sp3)
              force = FORCE_CONSTANTS_SINGLE_BOND(8)
              eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(8)
            CASE(2) BOND_PARMJ3 !C(sp)-C(sp2)
              if(type_bond.eq.1) then !C(sp)-C(sp2) single bond
                force = FORCE_CONSTANTS_SINGLE_BOND(7)
                eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(7)
              else !C(sp)-C(sp2) conjugated bond
                force = FORCE_CONSTANTS_AROMATIC_BOND(1)
                eq_bond_length = EQ_BOND_LENGTH_AROMATIC(1)
              end if
            CASE(4) BOND_PARMJ3
              if(type_bond.eq.1) then !C(sp)-C(sp), single bond
                force = FORCE_CONSTANTS_SINGLE_BOND(6)
                eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(6)
              else if(type_bond.eq.5) then !C(sp)-C(sp) triple bond
                force = FORCE_CONSTANTS_TRIPLE_BOND(1)
                eq_bond_length = EQUILIBRIUM_BOND_LENGTH_TRIPLE(1)
              else !C(sp)-C(sp) bond
                write(UNIout,'(a)')'CC_BOND_PARM> defaulting to triple bond.'
                force = FORCE_CONSTANTS_TRIPLE_BOND(1)
                eq_bond_length = EQUILIBRIUM_BOND_LENGTH_TRIPLE(1)
              end if
            CASE(20) BOND_PARMJ3 !C(sp)-C(4M ring)
              force = FORCE_CONSTANTS_TRIPLE_BOND(11)
              eq_bond_length = EQUILIBRIUM_BOND_LENGTH_TRIPLE(11)
            CASE(22) BOND_PARMJ3 !C(sp)-C(3M ring)
              force = FORCE_CONSTANTS_TRIPLE_BOND(10)
              eq_bond_length = EQUILIBRIUM_BOND_LENGTH_TRIPLE(10)
            CASe(30) BOND_PARMJ3 !C(sp)-C(olefinic 4M ring)
              force = FORCE_CONSTANTS_TRIPLE_BOND(12)
              eq_bond_length = EQUILIBRIUM_BOND_LENGTH_TRIPLE(12)
            CASE(37) BOND_PARMJ3 !C(sp)-C(aromatic)
              force = FORCE_CONSTANTS_SINGLE_BOND(9)
              eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(9)
            CASE DEFAULT BOND_PARMJ3
              write(UNIout,'(a)')'CC_BOND_PARM> jATOM: Illegal force field atom type or bond type not supported!'
              stop
          end SELECT BOND_PARMJ3
        CASE(20) BOND_PARMI
          BOND_PARMJ4:SELECT CASE(type_j)
            CASE(1) BOND_PARMJ4 !C(4M ring)-C(sp3)
              force = FORCE_CONSTANTS_SINGLE_BOND(23)
              eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(23)
            CASE(2) BOND_PARMJ4 !C(4M ring)-C(sp2)
              force = FORCE_CONSTANTS_SINGLE_BOND(18)
              eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(18)
            CASE(4) BOND_PARMJ4 !C(4M ring)-C(sp)
              force = FORCE_CONSTANTS_TRIPLE_BOND(11)
              eq_bond_length = EQUILIBRIUM_BOND_LENGTH_TRIPLE(11)
            CASE(20) BOND_PARMJ4 !C(4M ring)-C(4M ring)
              force = FORCE_CONSTANTS_SINGLE_BOND(32)
              eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(32)
            CASE(22) BOND_PARMJ4 !C(4M ring)-C(3M ring), any C in 3M ring
              force = FORCE_CONSTANTS_SINGLE_BOND(30)
              eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(30)
            CASE(30) BOND_PARMJ4 !C(4M ring)-C(olefinic 4M ring)
              force = FORCE_CONSTANTS_SINGLE_BOND(33)
              eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(33)
            CASE(37) BOND_PARMJ4 !C(4M ring)-C(aromatic)
              force = FORCE_CONSTANTS_SINGLE_BOND(28)
              eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(28)
            CASE DEFAULT BOND_PARMJ4
              write(UNIout,'(a)')'CC_BOND_PARM> jATOM: Illegal force field atom type or bond type not supported!'
              stop
          end SELECT BOND_PARMJ4
        CASE(22) BOND_PARMI
          BOND_PARMJ5:SELECT CASE(type_j)
            CASE(1) BOND_PARMJ5 !C(3M ring)-C(sp3)
              force = FORCE_CONSTANTS_SINGLE_BOND(22)
              eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(22)
            CASE(2) BOND_PARMJ5 !C(3M ring)-C(sp2)
              force = FORCE_CONSTANTS_SINGLE_BOND(17)
              eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(17)
            CASE(4) BOND_PARMJ5 !C(3M ring)-C(sp)
              force = FORCE_CONSTANTS_TRIPLE_BOND(10)
              eq_bond_length = EQUILIBRIUM_BOND_LENGTH_TRIPLE(10)
            CASE(20) BOND_PARMJ5 !C(3M ring)-C(4M ring)
              force = FORCE_CONSTANTS_SINGLE_BOND(30)
              eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(30)
            CASE(22) BOND_PARMJ5 !C(3M ring)-C(3M ring)
              if(type_bond.eq.3) then !C(3M ring)-C(3M ring), double bond
                force = FORCE_CONSTANTS_DOUBLE_BOND(1)
                eq_bond_length = EQUILIBRIUM_BOND_LENGTH_DOUBLE(1)
              else !C(3M ring)-C(3M ring), single bond
                force = FORCE_CONSTANTS_SINGLE_BOND(29)
                eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(29)
              end if
            CASE(30) BOND_PARMJ5 !C(3M ring)-C(olefinic 4M ring)
              force = FORCE_CONSTANTS_SINGLE_BOND(31)
              eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(31)
            CASE(37) BOND_PARMJ5 !C(3M ring)-C(aromatic)
              force = FORCE_CONSTANTS_SINGLE_BOND(27)
              eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(27)
            CASE DEFAULT BOND_PARMJ5
              write(UNIout,'(a)')'CC_BOND_PARM> jATOM: Illegal force field atom type or bond type not supported!'
              stop
          end SELECT BOND_PARMJ5
        CASE(30) BOND_PARMI
          BOND_PARMJ6:SELECT CASE(type_j)
            CASE(2) BOND_PARMJ6 !C(olefinic 4M ring)-C(sp2)
              force = FORCE_CONSTANTS_SINGLE_BOND(19)
              eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(19)
            CASE(4) BOND_PARMJ6 !C(olefinic 4M ring)-C(sp)
              force = FORCE_CONSTANTS_TRIPLE_BOND(12)
              eq_bond_length = EQUILIBRIUM_BOND_LENGTH_TRIPLE(12)
            CASE(20) BOND_PARMJ6 !C(olefinic 4M ring)-C(4M ring)
              force = FORCE_CONSTANTS_SINGLE_BOND(33)
               eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(33)
            CASE(22) BOND_PARMJ6 !C(olefinic 4M ring)-C(3M ring)
              force = FORCE_CONSTANTS_SINGLE_BOND(31)
              eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(31)
            CASE(30) BOND_PARMJ6 !C(olefinic 4M ring)-C(olefinic 4M ring)
              force = FORCE_CONSTANTS_SINGLE_BOND(34)
              eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(34)
            CASE DEFAULT BOND_PARMJ6
              write(UNIout,'(a)')'CC_BOND_PARM> jATOM: Illegal force field atom type or bond type not supported!'
              stop
          end SELECT BOND_PARMJ6
        CASE(37) BOND_PARMI
          BOND_PARMJ7:SELECT CASE(type_j)
            CASE(1) BOND_PARMJ7 !C(aromatic)-C(sp3)
              force = FORCE_CONSTANTS_SINGLE_BOND(25)
              eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(25)
            CASE(2) BOND_PARMJ7 !C(aromatic)-C(sp2)
              force = FORCE_CONSTANTS_SINGLE_BOND(16)
              eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(16)
            CASE(4) BOND_PARMJ7 !C(aromatic)-C(sp)
              force = FORCE_CONSTANTS_SINGLE_BOND(9)
              eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(9)
            CASE(20) BOND_PARMJ7 !C(aromatic)-C(4M ring)
              force = FORCE_CONSTANTS_SINGLE_BOND(28)
              eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(28)
            CASE(22) BOND_PARMJ7 !C(aromatic)-C(3M ring), any C in 3M ring
              force = FORCE_CONSTANTS_SINGLE_BOND(17)
              eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(17)
            CASE(37) BOND_PARMJ7 !C(aromatic)-C(aromatic), single bond
              if(type_bond.eq.1) then
                force = FORCE_CONSTANTS_SINGLE_BOND(26)
                eq_bond_length = EQUILIBRIUM_BOND_LENGTH_SINGLE(26)
              else !C(aromatic)-C(aromatic), aromatic bond
                force = FORCE_CONSTANTS_AROMATIC_BOND(2)
                eq_bond_length = EQ_BOND_LENGTH_AROMATIC(2)
              end if
            CASE DEFAULT BOND_PARMJ7
              write(UNIout,'(a)')'CC_BOND_PARM> jATOM: Illegal force field atom type!'
              stop
          end SELECT BOND_PARMJ7
        CASE DEFAULT BOND_PARMI
          write(UNIout,'(a)')'CC_BOND_PARM> iATOM: Illegal force field atom type!'
          call PRG_stop ('CC_BOND_PARM> iATOM: Illegal force field atom type!')
      end SELECT BOND_PARMI

! End of function CC_BOND_PARM
      return
      END
      subroutine HCH_ANG_PARM (type_k, & !type for atom k
                              eq_angle, force_angle)
!**********************************************************************
! Date last modified: 24 August, 1999                     Version 1.0 *
! Author: Michelle Shaw                                               *
! Description: Get equilibrium angles and force program_constants for a HCH   *
!              angle.                                                 *
!**********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE Eq_Angles
      USE FC_Angles

      implicit none
!
! Input scalars:
      integer type_k
      double precision force_angle, eq_angle

!
! Begin:
! This is where the parameters are selected for both equilibrium bond lengths and force program_constants.
      force_angle = ZERO
      eq_angle = ZERO

      ANG_PARM:SELECT CASE (type_k)
        CASE(1) ANG_PARM !H-C(sp3)-H
          force_angle = FCONST_ANGLES(13)
          eq_angle = EQUILIBRIUM_ANGLES(13)
        CASE(2) ANG_PARM !H-C(sp2)-H
          force_angle = FCONST_ANGLES(28)
          eq_angle = EQUILIBRIUM_ANGLES(28)
        CASE(20) ANG_PARM !H-C(4-M ring)-H, C is sp3
          force_angle = FCONST_ANGLES(74)
          eq_angle = EQUILIBRIUM_ANGLES(74)
        CASE(22) ANG_PARM !H-C(3-M ring)-H, C is sp3
          force_angle = FCONST_ANGLES(98)
          eq_angle = EQUILIBRIUM_ANGLES(98)
        CASE DEFAULT ANG_PARM
          write(UNIout,'(a)')'HCH_ANG_PARM> Angle not supported.'
      end SELECT ANG_PARM

! End of function HCH_ANG_PARM
      return
      END

      subroutine HCC_ANG_PARM (type_j, & !type for atom j
                               type_k, & !type for atom k
                               eq_angle, force_angle)
!**********************************************************************
! Date last modified: 24 August, 1999                     Version 1.0 *
! Author: Michelle Shaw                                               *
! Description: Get equilibrium angles and force program_constants for a HCC   *
!              angle.                                                 *
!**********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE Eq_Angles
      USE FC_Angles

      implicit none
!
! Input scalars:
      integer type_j, type_k, type_bond_jk
      double precision force_angle, eq_angle

!
! Begin:
! This is where the parameters are selected for both equilibrium angles and force program_constants.
      force_angle = ZERO
      eq_angle = ZERO

      ANG_PARMK:SELECT CASE(type_k)
        CASE(1) ANG_PARMK
          ANG_PARMJ1:SELECT CASE(type_j)
            CASE(1) ANG_PARMJ1 !H-C(sp3)-C(sp3)
              force_angle = FCONST_ANGLES(4)
              eq_angle = EQUILIBRIUM_ANGLES(4)
            CASE(2) ANG_PARMJ1 !H-C(sp3)-C(sp2)
              force_angle = FCONST_ANGLES(8)
              eq_angle = EQUILIBRIUM_ANGLES(8)
            CASE(4) ANG_PARMJ1 !H-C(sp3)-C(sp)
              force_angle = FCONST_ANGLES(11)
              eq_angle = EQUILIBRIUM_ANGLES(11)
            CASE(20) ANG_PARMJ1 !H-C(sp3)-C(4M ring)
              force_angle = FCONST_ANGLES(45)
              eq_angle = EQUILIBRIUM_ANGLES(45)
            CASE(22) ANG_PARMJ1 !H-C(sp3)-C(3M ring)
              force_angle = FCONST_ANGLES(46)
              eq_angle = EQUILIBRIUM_ANGLES(46)
            CASE(30) ANG_PARMJ1 !H-C(sp3)-C(olefinic 4M ring)
              force_angle = FCONST_ANGLES(45)
              eq_angle = EQUILIBRIUM_ANGLES(45)
            CASE(37) ANG_PARMJ1 !H-C(sp3)-C(aromatic)
              force_angle = FCONST_ANGLES(14)
              eq_angle = EQUILIBRIUM_ANGLES(14)
            CASE DEFAULT ANG_PARMJ1
              write(UNIout,'(a)')'HCC_ANG_PARM> jATOM: Illegal force field atom type!'
              stop
          end SELECT ANG_PARMJ1
        CASE(2) ANG_PARMK
          ANG_PARMJ2:SELECT CASE(type_j)
            CASE(1) ANG_PARMJ2 !H-C(sp2)-C(sp3)
              force_angle = FCONST_ANGLES(19)
              eq_angle = EQUILIBRIUM_ANGLES(19)
            CASE(2) ANG_PARMJ2
              force_angle = FCONST_ANGLES(23)
              eq_angle = EQUILIBRIUM_ANGLES(23)
            CASE(4) ANG_PARMJ2 !H-C(sp2)-C(sp)
              force_angle = FCONST_ANGLES(26)
              eq_angle = EQUILIBRIUM_ANGLES(26)
            CASE(22) ANG_PARMJ2 !H-C(sp2)-C(3M ring), any C in ring
              force_angle = FCONST_ANGLES(58)
              eq_angle = EQUILIBRIUM_ANGLES(58)
            CASE(30) ANG_PARMJ2 !H-C(sp2)-C(olefinic 4M ring)
              force_angle = FCONST_ANGLES(59)
              eq_angle = EQUILIBRIUM_ANGLES(59)
            CASE(37) ANG_PARMJ2 !H-C(sp2)-C(aromatic)
              force_angle = FCONST_ANGLES(29)
              eq_angle = EQUILIBRIUM_ANGLES(29)
            CASE DEFAULT ANG_PARMJ2
              write(UNIout,'(a)')'HCC_ANG_PARM> jATOM: Illegal force field atom type!'
              stop
          end SELECT ANG_PARMJ2
        CASE(4) ANG_PARMK !only case: H-C(sp)-C(sp)-, triple C-C bond
          force_angle = FCONST_ANGLES(33)
          eq_angle = EQUILIBRIUM_ANGLES(33)
        CASE(20) ANG_PARMK
          ANG_PARMJ4:SELECT CASE(type_j)
            CASE(1) ANG_PARMJ4 !H-C(4M ring)-C(sp3)
              force_angle = FCONST_ANGLES(66)
              eq_angle = EQUILIBRIUM_ANGLES(66)
            CASE(2) ANG_PARMJ4 !H-C(4M ring)-C(sp2)
              force_angle = FCONST_ANGLES(70)
              eq_angle = EQUILIBRIUM_ANGLES(70)
            CASE(4) ANG_PARMJ4 !H-C(4M ring)-C(sp)
              force_angle = FCONST_ANGLES(72)
              eq_angle = EQUILIBRIUM_ANGLES(72)
            CASE(20) ANG_PARMJ4 !H-C(4M ring)-C(4M ring)
              force_angle = FCONST_ANGLES(75)
              eq_angle = EQUILIBRIUM_ANGLES(75)
            CASE(30) ANG_PARMJ4 !H-C(4M ring)-C(olefinic 4M ring)
              force_angle = FCONST_ANGLES(76)
              eq_angle = EQUILIBRIUM_ANGLES(76)
            CASE(37) ANG_PARMJ4 !H-C(4M ring)-C(aromatic)
              force_angle = FCONST_ANGLES(77)
              eq_angle = EQUILIBRIUM_ANGLES(77)
            CASE DEFAULT ANG_PARMJ4
              write(UNIout,'(a)')'HCC_ANG_PARM> jATOM: Illegal force field atom type or angle type not supported!'
              stop
          end SELECT ANG_PARMJ4
        CASE(22) ANG_PARMK
          ANG_PARMJ5:SELECT CASE(type_j)
            CASE(1) ANG_PARMJ5 !H-C(3M ring)-C(sp3)
              force_angle = FCONST_ANGLES(89)
              eq_angle = EQUILIBRIUM_ANGLES(89)
            CASE(2) ANG_PARMJ5 !H-C(3M ring)-C(sp2)
              force_angle = FCONST_ANGLES(94)
              eq_angle = EQUILIBRIUM_ANGLES(94)
            CASE(4) ANG_PARMJ5 !H-C(3M ring)-C(sp)
              force_angle = FCONST_ANGLES(96)
              eq_angle = EQUILIBRIUM_ANGLES(96)
            CASE(20) ANG_PARMJ5 !H-C(3M ring)-C(4M ring)
              force_angle = FCONST_ANGLES(99)
              eq_angle = EQUILIBRIUM_ANGLES(99)
            CASE(22) ANG_PARMJ5 !H-C(3M ring)-C(3M ring)
              force_angle = FCONST_ANGLES(100)
              eq_angle = EQUILIBRIUM_ANGLES(100)
            CASE(30) ANG_PARMJ5 !H-C(3M ring)-C(olefinic 4M ring)
              force_angle = FCONST_ANGLES(99)
              eq_angle = EQUILIBRIUM_ANGLES(99)
            CASE(37) ANG_PARMJ5 !H-C(3M ring)-C(aromatic)
              force_angle = FCONST_ANGLES(101)
              eq_angle = EQUILIBRIUM_ANGLES(101)
            CASE DEFAULT ANG_PARMJ5
              write(UNIout,'(a)')'HCC_ANG_PARM> jATOM: Illegal force field atom type or angle type not supported!'
              stop
          end SELECT ANG_PARMJ5
        CASE(30) ANG_PARMK
          ANG_PARMJ6:SELECT CASE(type_j)
            CASE(20) ANG_PARMJ6 !H-C(olefinic 4M ring)-C(4M ring)
              force_angle = FCONST_ANGLES(115)
              eq_angle = EQUILIBRIUM_ANGLES(115)
            CASE(30) ANG_PARMJ6 !H-C(olefinic 4M ring)-C(olefinic 4M ring)
              force_angle = FCONST_ANGLES(116)
              eq_angle = EQUILIBRIUM_ANGLES(116)
            CASE DEFAULT ANG_PARMJ6
              write(UNIout,'(a)')'HCC_ANG_PARM> jATOM: Illegal force field atom type or angle type not supported!'
              stop
          end SELECT ANG_PARMJ6
        CASE(37) ANG_PARMK !only one case: H-C(aromatic)-C(aromatic)
          force_angle = FCONST_ANGLES(38)
          eq_angle = EQUILIBRIUM_ANGLES(38)
        CASE DEFAULT ANG_PARMK
          write(UNIout,'(a)')'HCC_ANG_PARM> kATOM: Illegal force field atom type!'
          stop
      end SELECT ANG_PARMK

! End of function HCC_ANG_PARM
      return
      END

      subroutine CCH_ANG_PARM (type_i,&  !type for atom i
                               type_k,&  !type for atom k
                               eq_angle, force_angle)
!**********************************************************************
! Date last modified: 24 August, 1999                     Version 1.0 *
! Author: Michelle Shaw                                               *
! Description: Get equilibrium angles and force program_constants for a CCH   *
!              angle.                                                 *
!**********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE Eq_Angles
      USE FC_Angles

      implicit none
!
! Input scalars:
      integer type_i, type_k
      double precision force_angle, eq_angle

! Local scalars:
!
! Begin:
! This is where the parameters are selected for both equilibrium angles and force program_constants.
      force_angle = ZERO
      eq_angle = ZERO

      ANG_PARMK:SELECT CASE(type_k)
        CASE(1) ANG_PARMK
          ANG_PARMI1:SELECT CASE(type_i)
            CASE(1) ANG_PARMI1 !C(sp3)-C(sp3)-H
              force_angle = FCONST_ANGLES(4)
              eq_angle = EQUILIBRIUM_ANGLES(4)
            CASE(2) ANG_PARMI1 !C(sp2)-C(sp3)-H
              force_angle = FCONST_ANGLES(8)
              eq_angle = EQUILIBRIUM_ANGLES(8)
            CASE(4) ANG_PARMI1 !C(sp)-C(sp3)-H
              force_angle = FCONST_ANGLES(11)
              eq_angle = EQUILIBRIUM_ANGLES(11)
            CASE(20) ANG_PARMI1 !C(4M ring)-C(sp3)-H
              force_angle = FCONST_ANGLES(45)
              eq_angle = EQUILIBRIUM_ANGLES(45)
            CASE(22) ANG_PARMI1 !C(3M ring)-C(sp3)-H
              force_angle = FCONST_ANGLES(46)
              eq_angle = EQUILIBRIUM_ANGLES(46)
            CASE(30) ANG_PARMI1 !C(olefinic 4M ring)-C(sp3)-H
              force_angle = FCONST_ANGLES(45)
              eq_angle = EQUILIBRIUM_ANGLES(45)
            CASE(37) ANG_PARMI1 !C(aromatic)-C(sp3)-H
              force_angle = FCONST_ANGLES(14)
              eq_angle = EQUILIBRIUM_ANGLES(14)
            CASE DEFAULT ANG_PARMI1
              write(UNIout,'(a)')'CCH_ANG_PARM> iATOM: Illegal force field atom type!'
              stop
          end SELECT ANG_PARMI1
        CASE(2) ANG_PARMK
          ANG_PARMI2:SELECT CASE(type_i)
            CASE(1) ANG_PARMI2 !C(sp3)-C(sp2)-H
              force_angle = FCONST_ANGLES(19)
              eq_angle = EQUILIBRIUM_ANGLES(19)
            CASE(2) ANG_PARMI2 !C(sp2)-C(sp2)-H
              force_angle = FCONST_ANGLES(23)
              eq_angle = EQUILIBRIUM_ANGLES(23)
            CASE(4) ANG_PARMI2 !C(sp)-C(sp2)-H
              force_angle = FCONST_ANGLES(26)
              eq_angle = EQUILIBRIUM_ANGLES(26)
            CASE(22) ANG_PARMI2 !C(3M ring)-C(sp2)-H, any C in ring
              force_angle = FCONST_ANGLES(58)
              eq_angle = EQUILIBRIUM_ANGLES(58)
            CASE(30) ANG_PARMI2 !C(olefinic 4M ring)-C(sp2)-H
              force_angle = FCONST_ANGLES(59)
              eq_angle = EQUILIBRIUM_ANGLES(59)
            CASE(37) ANG_PARMI2 !C(aromatic)-C(sp2)-H
              force_angle = FCONST_ANGLES(29)
              eq_angle = EQUILIBRIUM_ANGLES(29)
            CASE DEFAULT ANG_PARMI2
              write(UNIout,'(a)')'CCH_ANG_PARM> iATOM: Illegal force field atom type!'
              stop
          end SELECT ANG_PARMI2
        CASE(4) ANG_PARMK !only case: -C(sp)-C(sp)-H, triple C-C bond
          force_angle = FCONST_ANGLES(33)
          eq_angle = EQUILIBRIUM_ANGLES(33)
        CASE(20) ANG_PARMK
          ANG_PARMI4:SELECT CASE(type_i)
            CASE(1) ANG_PARMI4 !C(sp3)-C(4M ring)-H
              force_angle = FCONST_ANGLES(66)
              eq_angle = EQUILIBRIUM_ANGLES(66)
            CASE(2) ANG_PARMI4 !C(sp2)-C(4M ring)-H
              force_angle = FCONST_ANGLES(70)
              eq_angle = EQUILIBRIUM_ANGLES(70)
            CASE(4) ANG_PARMI4 !C(sp)-C(4M ring)-H
              force_angle = FCONST_ANGLES(72)
              eq_angle = EQUILIBRIUM_ANGLES(72)
            CASE(20) ANG_PARMI4 !C(4M ring)-C(4M ring)-H
              force_angle = FCONST_ANGLES(75)
              eq_angle = EQUILIBRIUM_ANGLES(75)
            CASE(30) ANG_PARMI4 !C(olefinic 4M ring)-C(4M ring)-H
              force_angle = FCONST_ANGLES(76)
              eq_angle = EQUILIBRIUM_ANGLES(76)
            CASE(37) ANG_PARMI4 !C(aromatic)-C(4M ring)-H
              force_angle = FCONST_ANGLES(77)
              eq_angle = EQUILIBRIUM_ANGLES(77)
            CASE DEFAULT ANG_PARMI4
              write(UNIout,'(a)')'CCH_ANG_PARM> iATOM: Illegal force field atom type or angle type not supported!'
              stop
          end SELECT ANG_PARMI4
        CASE(22) ANG_PARMK
          ANG_PARMI5:SELECT CASE(type_i)
            CASE(1) ANG_PARMI5 !C(sp3)-C(3M ring)-H
              force_angle = FCONST_ANGLES(89)
              eq_angle = EQUILIBRIUM_ANGLES(89)
            CASE(2) ANG_PARMI5 !C(sp2)-C(3M ring)-H
              force_angle = FCONST_ANGLES(94)
              eq_angle = EQUILIBRIUM_ANGLES(94)
            CASE(4) ANG_PARMI5 !C(sp)-C(3M ring)-H
              force_angle = FCONST_ANGLES(96)
              eq_angle = EQUILIBRIUM_ANGLES(96)
            CASE(20) ANG_PARMI5 !C(4M ring)-C(3M ring)-H
              force_angle = FCONST_ANGLES(99)
              eq_angle = EQUILIBRIUM_ANGLES(99)
            CASE(22) ANG_PARMI5 !C(3M ring)-C(3M ring)-H
              force_angle = FCONST_ANGLES(100)
              eq_angle = EQUILIBRIUM_ANGLES(100)
            CASE(30) ANG_PARMI5 !C(olefinic 4M ring)-C(3M ring)-H
              force_angle = FCONST_ANGLES(99)
              eq_angle = EQUILIBRIUM_ANGLES(99)
            CASE(37) ANG_PARMI5 !C(aromatic)-C(3M ring)-H
              force_angle = FCONST_ANGLES(101)
              eq_angle = EQUILIBRIUM_ANGLES(101)
            CASE DEFAULT ANG_PARMI5
              write(UNIout,'(a)')'CCH_ANG_PARM> iATOM: Illegal force field atom type or angle type not supported!'
              stop
          end SELECT ANG_PARMI5
        CASE(30) ANG_PARMK
          ANG_PARMI6:SELECT CASE(type_i)
            CASE(20) ANG_PARMI6 !C(4M ring)-C(olefinic 4M ring)-H
              force_angle = FCONST_ANGLES(115)
              eq_angle = EQUILIBRIUM_ANGLES(115)
            CASE(30) ANG_PARMI6 !C(olefinic 4M ring)-C(olefinic 4M ring)-H
              force_angle = FCONST_ANGLES(116)
              eq_angle = EQUILIBRIUM_ANGLES(116)
            CASE DEFAULT ANG_PARMI6
              write(UNIout,'(a)')'CCH_ANG_PARM> iATOM: Illegal force field atom type or angle type not supported!'
              stop
          end SELECT ANG_PARMI6
        CASE(37) ANG_PARMK !only one case: C(aromatic)-C(aromatic)-H
          force_angle = FCONST_ANGLES(38)
          eq_angle = EQUILIBRIUM_ANGLES(38)
        CASE DEFAULT ANG_PARMK
          write(UNIout,'(a)')'CCH_ANG_PARM> kATOM: Illegal force field atom type!'
          stop
      end SELECT ANG_PARMK

! End of function CCH_ANG_PARM
      return
      END

      subroutine CCC_ANG_PARM (type_i, & !type for atom i
                               type_j, & !type for atom j
                               type_k, & !type for atom k
                               bond_ik, & !type for bond ik
                               bond_kj, & !type for bond kj
                               ANG_IN_RING, & !parameter for ring angle is in
                               eq_angle, force_angle)
!**********************************************************************
! Date last modified: 24 August, 1999                     Version 1.0 *
! Author: Michelle Shaw                                               *
! Description: Get equilibrium angles and force program_constants for a CCC   *
!              angle.                                                 *
!**********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE Eq_Angles
      USE FC_Angles

      implicit none
!
! Input scalars:
      integer type_i, type_j, type_k, bond_ik, bond_kj, ANG_IN_RING
      double precision force_angle, eq_angle

! Local scalars:
!
! Begin:
! This is where the parameters are selected for both equilibrium angles and force program_constants.
      force_angle = ZERO
      eq_angle = ZERO

      ANG_PARMI:SELECT CASE (type_i)
        CASE(1) ANG_PARMI
          ANG_PARMK1:SELECT CASE (type_k)
            CASE(1) ANG_PARMK1
              ANG_PARMJ1:SELECT CASE (type_j)
                CASE(1) ANG_PARMJ1 !C(sp3)-C(sp3)-C(sp3)
                  force_angle = FCONST_ANGLES(1)
                  eq_angle = EQUILIBRIUM_ANGLES(1)
                CASE(2) ANG_PARMJ1 !C(sp3)-C(sp3)-C(sp2)
                  force_angle = FCONST_ANGLES(2)
                  eq_angle = EQUILIBRIUM_ANGLES(2)
                CASE(4) ANG_PARMJ1 !C(sp3)-C(sp3)-C(sp)
                  force_angle = FCONST_ANGLES(3)
                  eq_angle = EQUILIBRIUM_ANGLES(3)
                CASE(20) ANG_PARMJ1 !C(sp3)-C(sp3)-C(4M ring)
                  force_angle = FCONST_ANGLES(40)
                  eq_angle = EQUILIBRIUM_ANGLES(40)
                CASE(22) ANG_PARMJ1 !C(sp3)-C(sp3)-C(3M ring)
                  force_angle = FCONST_ANGLES(41)
                  eq_angle = EQUILIBRIUM_ANGLES(41)
                CASE(30) ANG_PARMJ1 !C(sp3)-C(sp3)-C(olefinic 4M ring)
                  force_angle = FCONST_ANGLES(40)
                  eq_angle = EQUILIBRIUM_ANGLES(40)
                CASE(37) ANG_PARMJ1 !C(sp3)-C(sp3)-C(aromatic)
                  force_angle = FCONST_ANGLES(5)
                  eq_angle = EQUILIBRIUM_ANGLES(5)
                CASE DEFAULT ANG_PARMJ1
                  write(UNIout,'(a)')'CCC_ANG_PARM> jATOM: Illegal force field atom type!'
                  stop
              end SELECT ANG_PARMJ1
            CASE(2) ANG_PARMK1
              ANG_PARMJ2:SELECT CASE (type_j)
                CASE(1) ANG_PARMJ2 !C(sp3)-C(sp2)-C(sp3)
                  force_angle = FCONST_ANGLES(16)
                  eq_angle = EQUILIBRIUM_ANGLES(16)
                CASE(2) ANG_PARMJ2 !C(sp3)-C(sp2)-C(sp2)
                  force_angle = FCONST_ANGLES(17)
                  eq_angle = EQUILIBRIUM_ANGLES(17)
                CASE(4) ANG_PARMJ2 !C(sp3)-C(sp2)-C(sp)
                  force_angle = FCONST_ANGLES(18)
                  eq_angle = EQUILIBRIUM_ANGLES(18)
                CASE(20) ANG_PARMJ2 !C(sp3)-C(sp2)-C(4M ring)
                  force_angle = FCONST_ANGLES(51)
                  eq_angle = EQUILIBRIUM_ANGLES(51)
                CASE(22) ANG_PARMJ2 !C(sp3)-C(sp2)-C(3M ring)
                  force_angle = FCONST_ANGLES(52)
                  eq_angle = EQUILIBRIUM_ANGLES(52)
                CASE(30) ANG_PARMJ2 !C(sp3)-C(sp2)-C(olefinic 4M ring)
                  force_angle = FCONST_ANGLES(53)
                  eq_angle = EQUILIBRIUM_ANGLES(53)
                CASE(37) ANG_PARMJ2 !C(sp3)-C(sp2)-C(aromatic)
                  force_angle = FCONST_ANGLES(20)
                  eq_angle = EQUILIBRIUM_ANGLES(20)
                CASE DEFAULT ANG_PARMJ2
                  write(UNIout,'(a)')'CCC_ANG_PARM> jATOM: Illegal force field atom type!'
                  stop
              end SELECT ANG_PARMJ2
            CASE(4) ANG_PARMK1
              ANG_PARMJ3:SELECT CASE(type_j)
                CASE(4) ANG_PARMJ3 !C(sp3)-C(sp)-C(sp)
                  force_angle = FCONST_ANGLES(30)
                  eq_angle = EQUILIBRIUM_ANGLES(30)
                CASE DEFAULT ANG_PARMJ3
                  write(UNIout,'(a)')'CCC_ANG_PARM> jATOM: Illegal force field atom type!'
                  stop
              end SELECT ANG_PARMJ3
            CASE(20) ANG_PARMK1
              ANG_PARMJ4:SELECT CASE (type_j)
                CASE(1) ANG_PARMJ4 !C(sp3)-C(4M ring)-C(sp3)
                  force_angle = FCONST_ANGLES(65)
                  eq_angle = EQUILIBRIUM_ANGLES(65)
                CASE(20) ANG_PARMJ4 !C(sp3)-C(4M ring)-C(4M ring)
                  force_angle = FCONST_ANGLES(67)
                  eq_angle = EQUILIBRIUM_ANGLES(67)
                CASE(22) ANG_PARMJ4 !C(sp3)-C(4M ring)-C(3M ring)
                  force_angle = FCONST_ANGLES(68)
                  eq_angle = EQUILIBRIUM_ANGLES(68)
                CASE(30) ANG_PARMJ4 !C(sp3)-C(4M ring)-C(olefinic 4M ring)
                  force_angle = FCONST_ANGLES(124)
                  eq_angle = EQUILIBRIUM_ANGLES(124)
                CASE(37) ANG_PARMJ4 !C(sp3)-C(4M ring)-C(aromatic)
                  force_angle = FCONST_ANGLES(69)
                  eq_angle = EQUILIBRIUM_ANGLES(69)
                CASE DEFAULT ANG_PARMJ4
                  write(UNIout,'(a)')'CCC_ANG_PARM> jATOM: Illegal force field atom type!'
                  stop
              end SELECT ANG_PARMJ4
            CASE(22) ANG_PARMK1
              ANG_PARMJ5:SELECT CASE (type_j)
                CASE(1) ANG_PARMJ5 !C(sp3)-C(3M ring)-C(sp3)
                  force_angle = FCONST_ANGLES(86)
                  eq_angle = EQUILIBRIUM_ANGLES(86)
                CASE(2) ANG_PARMJ5 !C(sp3)-C(3M ring)-C(sp2)
                  force_angle = FCONST_ANGLES(87)
                  eq_angle = EQUILIBRIUM_ANGLES(87)
                CASE(4) ANG_PARMJ5 !C(sp3)-C(3M ring)-C(sp)
                  force_angle = FCONST_ANGLES(88)
                  eq_angle = EQUILIBRIUM_ANGLES(88)
                CASE(22) ANG_PARMJ5 !C(sp3)-C(3M ring)-C(3M ring)
                  force_angle = FCONST_ANGLES(90)
                  eq_angle = EQUILIBRIUM_ANGLES(90)
                CASE(37) ANG_PARMJ5 !C(sp3)-C(3M ring)-C(aromatic)
                  force_angle = FCONST_ANGLES(91)
                  eq_angle = EQUILIBRIUM_ANGLES(91)
                CASE DEFAULT ANG_PARMJ5
                  write(UNIout,'(a)')'CCC_ANG_PARM> jATOM: Illegal force field atom type!'
                  stop
              end SELECT ANG_PARMJ5
            CASE(30) ANG_PARMK1
              write(UNIout,'(a)')'CCC_ANG_PARM> Angle type not supported.'
            CASE(37) ANG_PARMK1
              ANG_PARMJ0:SELECT CASE(type_j)
                CASE(37) ANG_PARMJ0 !C(sp3)-C(aromatic)-C(aromatic)
                  force_angle = FCONST_ANGLES(35)
                  eq_angle = EQUILIBRIUM_ANGLES(35)
                CASE DEFAULT ANG_PARMJ0
                  write(UNIout,'(a)')'CCC_ANG_PARM> jATOM: Illegal force field atom type!'
                  stop
              end SELECT ANG_PARMJ0
            CASE DEFAULT ANG_PARMK1
              write(UNIout,'(a)')'CCC_ANG_PARM> kATOM: Illegal force field atom type!'
              stop
          end SELECT ANG_PARMK1
        CASE(2) ANG_PARMI
          ANG_PARMK2:SELECT CASE (type_k)
            CASE(1) ANG_PARMK2
              ANG_PARMJ7:SELECT CASE (type_j)
                CASE(1) ANG_PARMJ7 !C(sp2)-C(sp3)-C(sp3)
                  force_angle = FCONST_ANGLES(2)
                  eq_angle = EQUILIBRIUM_ANGLES(2)
                CASE(2) ANG_PARMJ7 !C(sp2)-C(sp3)-C(sp2)
                  force_angle = FCONST_ANGLES(6)
                  eq_angle = EQUILIBRIUM_ANGLES(6)
                CASE(4) ANG_PARMJ7 !C(sp2)-C(sp3)-C(sp)
                  force_angle = FCONST_ANGLES(7)
                  eq_angle = EQUILIBRIUM_ANGLES(7)
                CASE(20) ANG_PARMJ7 !C(sp2)-C(sp3)-C(4M ring)
                  force_angle = FCONST_ANGLES(42)
                  eq_angle = EQUILIBRIUM_ANGLES(42)
                CASE(22) ANG_PARMJ7 !C(sp2)-C(sp3)-C(3M ring)
                  force_angle = FCONST_ANGLES(43)
                  eq_angle = EQUILIBRIUM_ANGLES(43)
                CASE(30) ANG_PARMJ7 !C(sp2)-C(sp3)-C(olefinic 4M ring)
                  force_angle = FCONST_ANGLES(42)
                  eq_angle = EQUILIBRIUM_ANGLES(42)
                CASE(37) ANG_PARMJ7 !C(sp2)-C(sp3)-C(aromatic)
                  force_angle = FCONST_ANGLES(9)
                  eq_angle = EQUILIBRIUM_ANGLES(9)
                CASE DEFAULT ANG_PARMJ7
                  write(UNIout,'(a)')'CCC_ANG_PARM> jATOM: Illegal force field atom type!'
                  stop
              end SELECT ANG_PARMJ7
            CASE(2) ANG_PARMK2
              ANG_PARMJ8:SELECT CASE (type_j)
                CASE(1) ANG_PARMJ8 !C(sp2)-C(sp2)-C(sp3)
                  force_angle = FCONST_ANGLES(17)
                  eq_angle = EQUILIBRIUM_ANGLES(17)
                CASE(2) ANG_PARMJ8 !C(sp2)-C(sp2)-C(sp2)
                  force_angle = FCONST_ANGLES(21)
                  eq_angle = EQUILIBRIUM_ANGLES(21)
                CASE(4) ANG_PARMJ8 !C(sp2)-C(sp2)-C(sp)
                  force_angle = FCONST_ANGLES(22)
                  eq_angle = EQUILIBRIUM_ANGLES(22)
                CASE(20) ANG_PARMJ8 !C(sp2)-C(sp2)-C(4M ring)
                  force_angle = FCONST_ANGLES(54)
                  eq_angle = EQUILIBRIUM_ANGLES(54)
                CASE(22) ANG_PARMJ8 !C(sp2)-C(sp2)-C(3M ring)
                  force_angle = FCONST_ANGLES(55)
                  eq_angle = EQUILIBRIUM_ANGLES(55)
                CASE(30) ANG_PARMJ8 !C(sp2)-C(sp2)-C(olefinic 4M ring)
                  force_angle = FCONST_ANGLES(54)
                  eq_angle = EQUILIBRIUM_ANGLES(54)
                CASE(37) ANG_PARMJ8 !C(sp2)-C(sp2)-C(aromatic)
! Angle type = 2
                  if(bond_ik.eq.1.and.bond_kj.eq.1) then
                    force_angle = FCONST_ANGLES(24)
                    eq_angle = EQUILIBRIUM_ANGLES(24)
! Angle type = 1
                  else if(bond_ik.eq.1.or.bond_kj.eq.1) then
                    force_angle = FCONST_ANGLES(128)
                    eq_angle = EQUILIBRIUM_ANGLES(128)
                  else
                    write(UNIout,'(a)')'CCC_BOND_PARM> Angle type unsupported.'
                    stop
                  end if
                CASE DEFAULT ANG_PARMJ8
                  write(UNIout,'(a)')'CCC_ANG_PARM> jATOM: Illegal force field atom type!'
                  stop
              end SELECT ANG_PARMJ8
            CASE(4) ANG_PARMK2
              ANG_PARMJ50:SELECT CASE(type_j)
                CASE(2) ANG_PARMJ50 !C(sp2)=C(sp)=C(sp2)
                  force_angle = FCONST_ANGLES(31)
                  eq_angle = EQUILIBRIUM_ANGLES(31)
                CASE(4) ANG_PARMJ50 !C(sp2)-C(sp)-C(sp)
                  force_angle = FCONST_ANGLES(32)
                  eq_angle = EQUILIBRIUM_ANGLES(32)
                CASE(30) ANG_PARMJ50 !C(sp2)-C(sp)-C(olefinic 4M ring)
                  force_angle = FCONST_ANGLES(64)
                  eq_angle = EQUILIBRIUM_ANGLES(64)
                CASE DEFAULT ANG_PARMJ50
                  write(UNIout,'(a)')'CCC_ANG_PARM> jATOM: Illegal force field atom type!'
                  stop
              end SELECT ANG_PARMJ50
            CASE(20) ANG_PARMK2
              ANG_PARMJ9:SELECT CASE (type_j)
                CASE(20) ANG_PARMJ9 !C(sp2)-C(4M ring)-C(4M ring)
                  force_angle = FCONST_ANGLES(71)
                  eq_angle = EQUILIBRIUM_ANGLES(71)
                CASE DEFAULT ANG_PARMJ9
                  write(UNIout,'(a)')'CCC_ANG_PARM> jATOM: Illegal force field atom type!'
                  stop
              end SELECT ANG_PARMJ9
            CASE(22) ANG_PARMK2
              ANG_PARMJ10:SELECT CASE (type_j)
                CASE(1) ANG_PARMJ10 !C(sp2)-C(3M ring)-C(sp3)
                  force_angle = FCONST_ANGLES(87)
                  eq_angle = EQUILIBRIUM_ANGLES(87)
                CASE(2) ANG_PARMJ10 !C(sp2)-C(3M ring)-C(sp2)
                  force_angle = FCONST_ANGLES(92)
                  eq_angle = EQUILIBRIUM_ANGLES(92)
                CASE(4) ANG_PARMJ10 !C(sp2)-C(3M ring)-C(sp)
                  force_angle = FCONST_ANGLES(93)
                  eq_angle = EQUILIBRIUM_ANGLES(93)
                CASE(22) ANG_PARMJ10 !C(sp2)-C(3M ring)-C(3M ring)
                  force_angle = FCONST_ANGLES(95)
                  eq_angle = EQUILIBRIUM_ANGLES(95)
                CASE DEFAULT ANG_PARMJ10
                  write(UNIout,'(a)')'CCC_ANG_PARM> jATOM: Illegal force field atom type!'
                  stop
              end SELECT ANG_PARMJ10
            CASE(30) ANG_PARMK2
              ANG_PARMJ11:SELECT CASE (type_j)
                CASE(20) ANG_PARMJ11 !C(sp2)-C(olefinic 4M ring)-C(4M ring)
                  force_angle = FCONST_ANGLES(111)
                  eq_angle = EQUILIBRIUM_ANGLES(111)
                CASE(22) ANG_PARMJ11 !C(sp2)-C(olefinic 4M ring)-C(3M ring)
                  force_angle = FCONST_ANGLES(112)
                  eq_angle = EQUILIBRIUM_ANGLES(112)
                CASE(30) ANG_PARMJ11 !C(sp2)-C(olefinic 4M ring)-C(olefinic 4M ring)
                  force_angle = FCONST_ANGLES(113)
                  eq_angle = EQUILIBRIUM_ANGLES(113)
                CASE DEFAULT ANG_PARMJ11
                  write(UNIout,'(a)')'CCC_ANG_PARM> jATOM: Illegal force field atom type!'
                  stop
              end SELECT ANG_PARMJ11
            CASE(37) ANG_PARMK2
              if(type_j.eq.37) then
                force_angle = FCONST_ANGLES(36) !C(sp2)-C(aromatic)-C(aromatic)
                eq_angle = EQUILIBRIUM_ANGLES(36)
              end if
            CASE DEFAULT ANG_PARMK2
              write(UNIout,'(a)')'CCC_ANG_PARM> kATOM: Illegal force field atom type!'
              stop
          end SELECT ANG_PARMK2
        CASE(4) ANG_PARMI
          ANG_PARMK3:SELECT CASE (type_k)
            CASE(1) ANG_PARMK3
              ANG_PARMJ12:SELECT CASE (type_j)
                CASE(1) ANG_PARMJ12 !C(sp)-C(sp3)-C(sp3)
                  force_angle = FCONST_ANGLES(3)
                  eq_angle = EQUILIBRIUM_ANGLES(3)
                CASE(2) ANG_PARMJ12 !C(sp)-C(sp3)-C(sp2)
                  force_angle = FCONST_ANGLES(7)
                  eq_angle = EQUILIBRIUM_ANGLES(7)
                CASE(4) ANG_PARMJ12 !C(sp)-C(sp3)-C(sp)
                  force_angle = FCONST_ANGLES(10)
                  eq_angle = EQUILIBRIUM_ANGLES(10)
                CASE(22) ANG_PARMJ12 !C(sp)-C(sp3)-C(3M ring)
                  force_angle = FCONST_ANGLES(44)
                  eq_angle = EQUILIBRIUM_ANGLES(44)
                CASE(37) ANG_PARMJ12 !C(sp)-C(sp3)-C(aromatic)
                  force_angle = FCONST_ANGLES(12)
                  eq_angle = EQUILIBRIUM_ANGLES(12)
                CASE DEFAULT ANG_PARMJ12
                  write(UNIout,'(a)')'CCC_ANG_PARM> jATOM: Illegal force field atom type!'
                  stop
              end SELECT ANG_PARMJ12
            CASE(2) ANG_PARMK3
              ANG_PARMJ13:SELECT CASE (type_j)
                CASE(1) ANG_PARMJ13 !C(sp)-C(sp2)-C(sp3)
                  force_angle = FCONST_ANGLES(18)
                  eq_angle = EQUILIBRIUM_ANGLES(18)
                CASE(2) ANG_PARMJ13 !C(sp)-C(sp2)-C(sp2)
                  force_angle = FCONST_ANGLES(22)
                  eq_angle = EQUILIBRIUM_ANGLES(22)
                CASE(4) ANG_PARMJ13 !C(sp)-C(sp2)-C(sp)
                  force_angle = FCONST_ANGLES(25)
                  eq_angle = EQUILIBRIUM_ANGLES(25)
                CASE(30) ANG_PARMJ13 !C(sp)-C(sp2)-C(olefinic 4M ring)
                  force_angle = FCONST_ANGLES(57)
                  eq_angle = EQUILIBRIUM_ANGLES(57)
                CASE(37) ANG_PARMJ13 !C(sp)-C(sp2)-C(aromatic)
                  force_angle = FCONST_ANGLES(27)
                  eq_angle = EQUILIBRIUM_ANGLES(27)
                CASE DEFAULT ANG_PARMJ13
                  write(UNIout,'(a)')'CCC_ANG_PARM> jATOM: Illegal force field atom type!'
                  stop
              end SELECT ANG_PARMJ13
            CASE(4) ANG_PARMK3
              ANG_PARMJ14:SELECT CASE (type_j)
                CASE(1) ANG_PARMJ14!C(sp)-C(sp)-C(sp3)
                  force_angle = FCONST_ANGLES(30)
                  eq_angle = EQUILIBRIUM_ANGLES(30)
                CASE(2) ANG_PARMJ14 !C(sp)-C(sp)-C(sp2)
                  force_angle = FCONST_ANGLES(32)
                  eq_angle = EQUILIBRIUM_ANGLES(32)
                CASE(37) ANG_PARMJ14 !C(sp)-C(sp)-C(aromatic)
                  force_angle = FCONST_ANGLES(34)
                  eq_angle = EQUILIBRIUM_ANGLES(34)
                CASE DEFAULT ANG_PARMJ14
                  write(UNIout,'(a)')'CCC_ANG_PARM> jATOM: Illegal force field atom type!'
                  stop
              end SELECT ANG_PARMJ14
            CASE(20) ANG_PARMK3
              ANG_PARMJ15:SELECT CASE (type_j)
                CASE(20) ANG_PARMJ15 !C(sp)-C(4M ring)-C(4M ring)
                  force_angle = FCONST_ANGLES(73)
                  eq_angle = EQUILIBRIUM_ANGLES(73)
                CASE DEFAULT ANG_PARMJ15
                  write(UNIout,'(a)')'CCC_ANG_PARM> jATOM: Illegal force field atom type!'
                  stop
              end SELECT ANG_PARMJ15
            CASE(22) ANG_PARMK3
              ANG_PARMJ16:SELECT CASE (type_j)
                CASE(1) ANG_PARMJ16 !C(sp)-C(3M ring)-C(sp3)
                  force_angle = FCONST_ANGLES(88)
                  eq_angle = EQUILIBRIUM_ANGLES(88)
                CASE(2) ANG_PARMJ16 !C(sp)-C(3M ring)-C(sp2)
                  force_angle = FCONST_ANGLES(93)
                  eq_angle = EQUILIBRIUM_ANGLES(93)
                CASE(22) ANG_PARMJ16 !C(sp)-C(3M ring)-C(3M ring)
                  force_angle = FCONST_ANGLES(97)
                  eq_angle = EQUILIBRIUM_ANGLES(97)
                CASE DEFAULT ANG_PARMJ16
                  write(UNIout,'(a)')'CCC_ANG_PARM> jATOM: Illegal force field atom type!'
                  stop
              end SELECT ANG_PARMJ16
            CASE(30) ANG_PARMK3
              ANG_PARMJ17:SELECT CASE (type_j)
                CASE(20) ANG_PARMJ17 !C(sp)-C(olefinic 4M ring)-C(4M ring)
                  force_angle = FCONST_ANGLES(114)
                  eq_angle = EQUILIBRIUM_ANGLES(114)
                CASE DEFAULT ANG_PARMJ17
                  write(UNIout,'(a)')'CCC_ANG_PARM> jATOM: Illegal force field atom type!'
                  stop
              end SELECT ANG_PARMJ17
            CASE(37) ANG_PARMK3
              ANG_PARMJ18:SELECT CASE(type_j)
                CASE(37) ANG_PARMJ18 !C(sp)-C(aromatic)-C(aromatic)
                  force_angle = FCONST_ANGLES(37)
                  eq_angle = EQUILIBRIUM_ANGLES(37)
                CASE DEFAULT ANG_PARMJ18
                  write(UNIout,'(a)')'CCC_ANG_PARM> jATOM: Illegal force field atom type!'
                  stop
              end SELECT ANG_PARMJ18
            CASE DEFAULT ANG_PARMK3
              write(UNIout,'(a)')'CCC_ANG_PARM> kATOM: Illegal force field atom type!'
              stop
          end SELECT ANG_PARMK3
        CASE(20) ANG_PARMI
          ANG_PARMK4:SELECT CASE (type_k)
            CASE(1) ANG_PARMK4
              ANG_PARMJ19:SELECT CASE (type_j)
                CASE(1) ANG_PARMJ19 !C(4M ring)-C(sp3)-C(sp3)
                  force_angle = FCONST_ANGLES(40)
                  eq_angle = EQUILIBRIUM_ANGLES(40)
                CASE(2) ANG_PARMJ19 !C(4M ring)-C(sp3)-C(sp2)
                  force_angle = FCONST_ANGLES(42)
                  eq_angle = EQUILIBRIUM_ANGLES(42)
                CASE(20) ANG_PARMJ19 !C(4M ring)-C(sp3)-C(4M ring)
                  force_angle = FCONST_ANGLES(47)
                  eq_angle = EQUILIBRIUM_ANGLES(47)
                CASE(37) ANG_PARMJ19 !C(4M ring)-C(sp3)-C(aromatic)
                  force_angle = FCONST_ANGLES(48)
                  eq_angle = EQUILIBRIUM_ANGLES(48)
                CASE DEFAULT ANG_PARMJ19
                  write(UNIout,'(a)')'CCC_ANG_PARM> jATOM: Illegal force field atom type!'
                  stop
              end SELECT ANG_PARMJ19
            CASE(2) ANG_PARMK4
              ANG_PARMJ20:SELECT CASE (type_j)
                CASE(1) ANG_PARMJ20 !C(4M ring)-C(sp2)-C(sp3)
                  force_angle = FCONST_ANGLES(51)
                  eq_angle = EQUILIBRIUM_ANGLES(51)
                CASE(2) ANG_PARMJ20 !C(4M ring)-C(sp2)-C(sp2)
                  force_angle = FCONST_ANGLES(54)
                  eq_angle = EQUILIBRIUM_ANGLES(54)
                CASE DEFAULT ANG_PARMJ20
                  write(UNIout,'(a)')'CCC_ANG_PARM> jATOM: Illegal force field atom type!'
                  stop
              end SELECT ANG_PARMJ20
            CASE(4) ANG_PARMK4
              write(UNIout,'(a)')'CCC_ANG_PARM> Angle type not supported!'
              stop
            CASE(20) ANG_PARMK4
              ANG_PARMJ22:SELECT CASE (type_j)
                CASE(1) ANG_PARMJ22 !C(4M ring)-C(4M ring)-C(sp3)
                  force_angle = FCONST_ANGLES(67)
                  eq_angle = EQUILIBRIUM_ANGLES(67)
                CASE(2) ANG_PARMJ22 !C(4M ring)-C(4M ring)-C(sp2)
                  force_angle = FCONST_ANGLES(71)
                  eq_angle = EQUILIBRIUM_ANGLES(71)
                CASE(4) ANG_PARMJ22 !C(4M ring)-C(4M ring)-C(sp)
                  force_angle = FCONST_ANGLES(73)
                  eq_angle = EQUILIBRIUM_ANGLES(73)
                CASE(20) ANG_PARMJ22 !C(4M ring)-C(4M ring)-C(4M ring)
                  IN_RING1:SELECT CASE (ANG_IN_RING)
                    CASE(16) IN_RING1 !angle inside 4M ring
                      force_angle = FCONST_ANGLES(79)
                      eq_angle = EQUILIBRIUM_ANGLES(79)
                    CASE DEFAULT IN_RING1 !angle outside 4M ring
                      force_angle = FCONST_ANGLES(78)
                      eq_angle = EQUILIBRIUM_ANGLES(78)
                  end SELECT IN_RING1
                CASE(22) ANG_PARMJ22 !C(4M ring)-C(4M ring)-C(3M ring)
                  IN_RING2:SELECT CASE (ANG_IN_RING)
                    CASE(9) IN_RING2 !angle inside 4M ring
                      force_angle = FCONST_ANGLES(81)
                      eq_angle = EQUILIBRIUM_ANGLES(81)
                    CASE DEFAULT IN_RING2 !angle outside 4M ring
                      force_angle = FCONST_ANGLES(80)
                      eq_angle = EQUILIBRIUM_ANGLES(80)
                  end SELECT IN_RING2
                CASE(30) ANG_PARMJ22 !C(4M ring)-C(4M ring)-C(olefinic 4M ring)
                  IN_RING3:SELECT CASE (ANG_IN_RING)
                    CASE(16) IN_RING3 !angle inside 4M ring
                      force_angle = FCONST_ANGLES(83)
                      eq_angle = EQUILIBRIUM_ANGLES(83)
                    CASE DEFAULT IN_RING3 !angle outside 4M ring
                      force_angle = FCONST_ANGLES(82)
                      eq_angle = EQUILIBRIUM_ANGLES(82)
                  end SELECT IN_RING3
                CASE(37) ANG_PARMJ22 !C(4M ring)-C(4M ring)-C(aromatic)
                  IN_RING4:SELECT CASE(ANG_IN_RING)
                    CASE(16) IN_RING4 !angle inside 4M ring
                      force_angle = FCONST_ANGLES(85)
                      eq_angle = EQUILIBRIUM_ANGLES(85)
                    CASE DEFAULT IN_RING4 !angle outside 4M ring
                      force_angle = FCONST_ANGLES(84)
                      eq_angle = EQUILIBRIUM_ANGLES(84)
                  end SELECT IN_RING4
                CASE DEFAULT ANG_PARMJ22
                  write(UNIout,'(a)')'CCC_ANG_PARM> jATOM: Illegal force field atom type!'
                  stop
              end SELECT ANG_PARMJ22
            CASE(22) ANG_PARMK4
              ANG_PARMJ23:SELECT CASE (type_j)
                CASE(22) ANG_PARMJ23 !C(4M ring)-C(3M ring)-C(3M ring)
                  IN_RING5:SELECT CASE(ANG_IN_RING)
                    CASE (16) IN_RING5 !angle inside 4M ring
                      force_angle = FCONST_ANGLES(103)
                      eq_angle = EQUILIBRIUM_ANGLES(103)
                    CASE DEFAULT IN_RING5 !angle outside 4M ring
                      force_angle = FCONST_ANGLES(102)
                      eq_angle = EQUILIBRIUM_ANGLES(102)
                  end SELECT IN_RING5
                CASE DEFAULT ANG_PARMJ23
                  write(UNIout,'(a)')'CCC_ANG_PARM> jATOM: Illegal force field atom type!'
                  stop
              end SELECT ANG_PARMJ23
            CASE(30) ANG_PARMK4
              ANG_PARMJ24:SELECT CASE (type_j)
                CASE(2) ANG_PARMJ24 !C(4M ring)-C(olefinic 4M ring)-C(sp2)
                  force_angle = FCONST_ANGLES(111)
                  eq_angle = EQUILIBRIUM_ANGLES(111)
                CASE(4) ANG_PARMJ24 !C(4M ring)-C(olefinic 4M ring)-C(sp)
                  force_angle = FCONST_ANGLES(114)
                  eq_angle = EQUILIBRIUM_ANGLES(114)
                CASE(30) ANG_PARMJ24 !C(4M ring)-C(olefinic 4M ring)-C(olefinic 4M ring)
                  IN_RING6:SELECT CASE(ANG_IN_RING)
                    CASE(16) IN_RING6  !angle type = 4
                      force_angle = FCONST_ANGLES(117)
                      eq_angle = EQUILIBRIUM_ANGLES(117)
                    CASE DEFAULT IN_RING6  !angle type = 7
                      force_angle = FCONST_ANGLES(125)
                      eq_angle = EQUILIBRIUM_ANGLES(125)
                  end SELECT IN_RING6
                CASE DEFAULT ANG_PARMJ24
                  write(UNIout,'(a)')'CCC_ANG_PARM> jATOM: Illegal force field atom type!'
                  stop
              end SELECT ANG_PARMJ24
            CASE(37) ANG_PARMK4
              ANG_PARMJ25:SELECT CASE (type_j)
                CASE(37) ANG_PARMJ25 !C(4M ring)-C(aromatic)-C(aromatic)
                  IN_RING7:SELECT CASE(ANG_IN_RING)
                    CASE (16) IN_RING7
                      force_angle = FCONST_ANGLES(121)
                      eq_angle = EQUILIBRIUM_ANGLES(121)
                    CASE DEFAULT IN_RING7
                      force_angle = FCONST_ANGLES(120)
                      eq_angle = EQUILIBRIUM_ANGLES(120)
                  end SELECT IN_RING7
                CASE DEFAULT ANG_PARMJ25
                  write(UNIout,'(a)')'CCC_ANG_PARM> jATOM: Illegal force field atom type!'
                  stop
              end SELECT ANG_PARMJ25
            CASE DEFAULT ANG_PARMK4
              write(UNIout,'(a)')'CCC_ANG_PARM> kATOM: Illegal force field atom type!'
              stop
          end SELECT ANG_PARMK4
        CASE(22) ANG_PARMI
          ANG_PARMK5:SELECT CASE (type_k)
            CASE(1) ANG_PARMK5
              ANG_PARMJ26:SELECT CASE (type_j)
                CASE(1) ANG_PARMJ26 !C(3M ring)-C(sp3)-C(sp3)
                  force_angle = FCONST_ANGLES(40)
                  eq_angle = EQUILIBRIUM_ANGLES(40)
                CASE(2) ANG_PARMJ26 !C(3M ring)-C(sp3)-C(sp2)
                  force_angle = FCONST_ANGLES(43)
                  eq_angle = EQUILIBRIUM_ANGLES(43)
                CASE(4) ANG_PARMJ26 !C(3M ring)-C(sp3)-C(sp)
                  force_angle = FCONST_ANGLES(44)
                  eq_angle = EQUILIBRIUM_ANGLES(44)
                CASE(22) ANG_PARMJ26 !C(3M ring)-C(sp3)-C(3M ring)
                  force_angle = FCONST_ANGLES(49)
                  eq_angle = EQUILIBRIUM_ANGLES(49)
                CASE(37) ANG_PARMJ26 !C(3M ring)-C(sp3)-C(aromatic)
                  force_angle = FCONST_ANGLES(50)
                  eq_angle = EQUILIBRIUM_ANGLES(50)
                CASE DEFAULT ANG_PARMJ26
                  write(UNIout,'(a)')'CCC_ANG_PARM> jATOM: Illegal force field atom type!'
                  stop
              end SELECT ANG_PARMJ26
            CASE(2) ANG_PARMK5
              ANG_PARMJ27:SELECT CASE (type_j)
                CASE(1) ANG_PARMJ27 !C(3M ring)-C(sp2)-C(sp3)
                  force_angle = FCONST_ANGLES(52)
                  eq_angle = EQUILIBRIUM_ANGLES(52)
                CASE(2) ANG_PARMJ27 !C(3M ring)-C(sp2)-C(sp2)
                  force_angle = FCONST_ANGLES(55)
                  eq_angle = EQUILIBRIUM_ANGLES(55)
                CASE(22) ANG_PARMJ27 !C(3M ring)-C(sp2)-C(3M ring)
                  IN_RING8:SELECT CASE (ANG_IN_RING)
                    CASE(9) IN_RING8
                      force_angle = FCONST_ANGLES(61)
                      eq_angle = EQUILIBRIUM_ANGLES(61)
                    CASE DEFAULT IN_RING8
                      force_angle = FCONST_ANGLES(60)
                      eq_angle = EQUILIBRIUM_ANGLES(60)
                  end SELECT IN_RING8
                CASE(37) ANG_PARMJ27 !C(3M ring)-C(sp2)-C(aromatic)
                  force_angle = FCONST_ANGLES(62)
                  eq_angle = EQUILIBRIUM_ANGLES(62)
                CASE DEFAULT ANG_PARMJ27
                  write(UNIout,'(a)')'CCC_ANG_PARM> jATOM: Illegal force field atom type!'
                  stop
              end SELECT ANG_PARMJ27
            CASE(4) ANG_PARMK5
              write(UNIout,'(a)')'CCC_ANG_PARM> kATOM: Angle type not supported!'
              stop
            CASE(20) ANG_PARMK5
              ANG_PARMJ29:SELECT CASE (type_j)
                CASE(1) ANG_PARMJ29 !C(3M ring)-C(4M ring)-C(sp3)
                  force_angle = FCONST_ANGLES(68)
                  eq_angle = EQUILIBRIUM_ANGLES(68)
                CASE(20) ANG_PARMJ29 !C(3M ring)-C(4M ring)-C(4M ring)
                  IN_RING9:SELECT CASE (ANG_IN_RING)
                    CASE(9) IN_RING9
                      force_angle = FCONST_ANGLES(81)
                      eq_angle = EQUILIBRIUM_ANGLES(81)
                    CASE DEFAULT IN_RING9
                      force_angle = FCONST_ANGLES(80)
                      eq_angle = EQUILIBRIUM_ANGLES(80)
                  end SELECT IN_RING9
                CASE(22) ANG_PARMJ29 !C(3M ring)-C(4M ring)-C(3M ring)
                  IN_RING10:SELECT CASE (ANG_IN_RING)
                    CASE(16) IN_RING10
                      force_angle = FCONST_ANGLES(127)
                      eq_angle = EQUILIBRIUM_ANGLES(127)
                    CASE DEFAULT IN_RING10
                      force_angle = FCONST_ANGLES(126)
                      eq_angle = EQUILIBRIUM_ANGLES(126)
                  end SELECT IN_RING10
                CASE DEFAULT ANG_PARMJ29
                  write(UNIout,'(a)')'CCC_ANG_PARM> jATOM: Illegal force field atom type!'
                  stop
              end SELECT ANG_PARMJ29
            CASE(22) ANG_PARMK5
              ANG_PARMJ30:SELECT CASE (type_j)
                CASE(1) ANG_PARMJ30 !C(3M ring)-C(3M ring)-C(sp3)
                  force_angle = FCONST_ANGLES(90)
                  eq_angle = EQUILIBRIUM_ANGLES(90)
                CASE(2) ANG_PARMJ30 !C(3M ring)-C(3M ring)-C(sp2)
                  force_angle = FCONST_ANGLES(95)
                  eq_angle = EQUILIBRIUM_ANGLES(95)
                CASE(4) ANG_PARMJ30 !C(3M ring)-C(3M ring)-C(sp)
                  force_angle = FCONST_ANGLES(97)
                  eq_angle = EQUILIBRIUM_ANGLES(97)
                CASE(20) ANG_PARMJ30 !C(3M ring)-C(3M ring)-C(4M ring)
                  IN_RING11:SELECT CASE(ANG_IN_RING)
                    CASE (16) IN_RING11
                      force_angle = FCONST_ANGLES(103)
                      eq_angle = EQUILIBRIUM_ANGLES(103)
                    CASE DEFAULT IN_RING11
                      force_angle = FCONST_ANGLES(102)
                      eq_angle = EQUILIBRIUM_ANGLES(102)
                  end SELECT IN_RING11
                CASE(22) ANG_PARMJ30 !C(3M ring)-C(3M ring)-C(3M ring)
                  IN_RING12:SELECT CASE(ANG_IN_RING)
                    CASE (16) IN_RING12
                      force_angle = FCONST_ANGLES(106)
                      eq_angle = EQUILIBRIUM_ANGLES(106)
                    CASE(9) IN_RING12
                      force_angle = FCONST_ANGLES(105)
                      eq_angle = EQUILIBRIUM_ANGLES(105)
                    CASE DEFAULT IN_RING12
                      force_angle = FCONST_ANGLES(104)
                      eq_angle = EQUILIBRIUM_ANGLES(104)
                  end SELECT IN_RING12
                CASE(30) ANG_PARMJ30 !C(3M ring)-C(3M ring)-C(olefinic 4M ring)
                  force_angle = FCONST_ANGLES(107)
                  eq_angle = EQUILIBRIUM_ANGLES(107)
                CASE(37) ANG_PARMJ30 !C(3M ring)-C(3M ring)-C(aromatic)
                  force_angle = FCONST_ANGLES(108)
                  eq_angle = EQUILIBRIUM_ANGLES(108)
                CASE DEFAULT ANG_PARMJ30
                  write(UNIout,'(a)')'CCC_ANG_PARM> jATOM: Illegal force field atom type!'
                  stop
              end SELECT ANG_PARMJ30
            CASE(30) ANG_PARMK5
              ANG_PARMJ31:SELECT CASE (type_j)
                CASE(2) ANG_PARMJ31 !C(3M ring)-C(olefinic 4M ring)-C(sp2)
                  force_angle = FCONST_ANGLES(112)
                  eq_angle = EQUILIBRIUM_ANGLES(112)
                CASE(22) ANG_PARMJ31 !C(3M ring)-C(olefinic 4M ring)-C(3M ring)
                  force_angle = FCONST_ANGLES(118)
                  eq_angle = EQUILIBRIUM_ANGLES(118)
                CASE DEFAULT ANG_PARMJ31
                  write(UNIout,'(a)')'CCC_ANG_PARM> jATOM: Illegal force field atom type!'
                  stop
              end SELECT ANG_PARMJ31
            CASE(37) ANG_PARMK5
              ANG_PARMJ32:SELECT CASE (type_j)
                CASE(37) ANG_PARMJ32 !C(3M ring)-C(aromatic)-C(aromatic)
                  IN_RING13:SELECT CASE(ANG_IN_RING)
                    CASE (16) IN_RING13
                      force_angle = FCONST_ANGLES(123)
                      eq_angle = EQUILIBRIUM_ANGLES(123)
                    CASE DEFAULT IN_RING13
                      force_angle = FCONST_ANGLES(122)
                      eq_angle = EQUILIBRIUM_ANGLES(122)
                  end SELECT IN_RING13
                CASE DEFAULT ANG_PARMJ32
                  write(UNIout,'(a)')'CCC_ANG_PARM> jATOM: Illegal force field atom type!'
                  stop
              end SELECT ANG_PARMJ32
            CASE DEFAULT ANG_PARMK5
              write(UNIout,'(a)')'CCC_ANG_PARM> kATOM: Illegal force field atom type!'
              stop
          end SELECT ANG_PARMK5
        CASE(30) ANG_PARMI
          ANG_PARMK6:SELECT CASE (type_k)
            CASE(1) ANG_PARMK6
              ANG_PARMJ33:SELECT CASE (type_j)
                CASE(1) ANG_PARMJ33 !C(olefinic 4M ring)-C(sp3)-C(sp3)
                  force_angle = FCONST_ANGLES(40)
                  eq_angle = EQUILIBRIUM_ANGLES(40)
                CASE(2) ANG_PARMJ33 !C(olefinic 4M ring)-C(sp3)-C(sp2)
                  force_angle = FCONST_ANGLES(42)
                  eq_angle = EQUILIBRIUM_ANGLES(42)
                CASE(37) ANG_PARMJ33 !C(olefinic 4M ring)-C(sp3)-C(aromatic)
                  force_angle = FCONST_ANGLES(48)
                  eq_angle = EQUILIBRIUM_ANGLES(48)
                CASE DEFAULT ANG_PARMJ33
                  write(UNIout,'(a)')'CCC_ANG_PARM> jATOM: Illegal force field atom type!'
                  stop
              end SELECT ANG_PARMJ33
            CASE(2) ANG_PARMK6
              ANG_PARMJ34:SELECT CASE (type_j)
                CASE(1) ANG_PARMJ34 !C(olefinic 4M ring)-C(sp2)-C(sp3)
                  force_angle = FCONST_ANGLES(53)
                  eq_angle = EQUILIBRIUM_ANGLES(53)
                CASE(4) ANG_PARMJ34 !C(olefinic 4M ring)-C(sp2)-C(sp)
                  force_angle = FCONST_ANGLES(57)
                  eq_angle = EQUILIBRIUM_ANGLES(57)
                CASE(37) ANG_PARMJ34 !C(olefinic 4M ring)-C(sp2)-C(aromatic)
                  force_angle = FCONST_ANGLES(63)
                  eq_angle = EQUILIBRIUM_ANGLES(63)
                CASE DEFAULT ANG_PARMJ34
                  write(UNIout,'(a)')'CCC_ANG_PARM> jATOM: Illegal force field atom type!'
                  stop
              end SELECT ANG_PARMJ34
            CASE(4) ANG_PARMK6
              ANG_PARMJ35:SELECT CASE (type_j)
                CASE(2) ANG_PARMJ35 !C(olefinic 4M ring)-C(sp)-C(sp2)
                  force_angle = FCONST_ANGLES(64)
                  eq_angle = EQUILIBRIUM_ANGLES(64)
                CASE DEFAULT ANG_PARMJ35
                  write(UNIout,'(a)')'CCC_ANG_PARM> jATOM: Illegal force field atom type!'
                  stop
              end SELECT ANG_PARMJ35
            CASE(20) ANG_PARMK6
              ANG_PARMJ36:SELECT CASE (type_j)
                CASE(1) ANG_PARMJ36 !C(olefinic 4M ring)-C(4M ring)-C(sp3)
                  force_angle = FCONST_ANGLES(124)
                  eq_angle = EQUILIBRIUM_ANGLES(124)
                CASE(20) ANG_PARMJ36 !C(olefinic 4M ring)-C(4M ring)-C(4M ring)
                  IN_RING14:SELECT CASE (ANG_IN_RING)
                    CASE(16) IN_RING14
                      force_angle = FCONST_ANGLES(83)
                      eq_angle = EQUILIBRIUM_ANGLES(83)
                    CASE DEFAULT IN_RING14
                      force_angle = FCONST_ANGLES(82)
                      eq_angle = EQUILIBRIUM_ANGLES(82)
                  end SELECT IN_RING14
                CASE DEFAULT ANG_PARMJ36
                  write(UNIout,'(a)')'CCC_ANG_PARM> jATOM: Illegal force field atom type!'
                  stop
              end SELECT ANG_PARMJ36
            CASE(22) ANG_PARMK6
              ANG_PARMJ37:SELECT CASE (type_j)
                CASE(22) ANG_PARMJ37 !C(olefinic 4M ring)-C(3M ring)-C(3M ring)
                  force_angle = FCONST_ANGLES(107)
                  eq_angle = EQUILIBRIUM_ANGLES(107)
                CASE DEFAULT ANG_PARMJ37
                  write(UNIout,'(a)')'CCC_ANG_PARM> jATOM: Illegal force field atom type!'
                  stop
              end SELECT ANG_PARMJ37
            CASE(30) ANG_PARMK6
              ANG_PARMJ38:SELECT CASE (type_j)
                CASE(2) ANG_PARMJ38 !C(olefinic 4M ring)-C(olefinic 4M ring)-C(sp2)
                  force_angle = FCONST_ANGLES(113)
                  eq_angle = EQUILIBRIUM_ANGLES(113)
                CASE(20) ANG_PARMJ38 !C(olefinic 4M ring)-C(olefinic 4M ring)-C(4M ring)
                  IN_RING15:SELECT CASE(ANG_IN_RING)
                    CASE(16) IN_RING15  !angle type = 4
                      force_angle = FCONST_ANGLES(117)
                      eq_angle = EQUILIBRIUM_ANGLES(117)
                    CASE DEFAULT IN_RING15  !angle type = 7
                      force_angle = FCONST_ANGLES(125)
                      eq_angle = EQUILIBRIUM_ANGLES(125)
                  end SELECT IN_RING15
                CASE(30) ANG_PARMJ38 !C(olefinic 4M ring)-C(olefinic 4M ring)-C(olefinic 4M ring)
                  force_angle = FCONST_ANGLES(119)
                  eq_angle = EQUILIBRIUM_ANGLES(119)
                CASE DEFAULT ANG_PARMJ38
                  write(UNIout,'(a)')'CCC_ANG_PARM> jATOM: Illegal force field atom type!'
                  stop
              end SELECT ANG_PARMJ38
            CASE(37) ANG_PARMK6
              write(UNIout,'(a)')'CCC_ANG_PARM> Angle type not supported!'
              stop
            CASE DEFAULT ANG_PARMK6
              write(UNIout,'(a)')'CCC_ANG_PARM> kATOM: Illegal force field atom type!'
              stop
          end SELECT ANG_PARMK6
        CASE(37) ANG_PARMI
          ANG_PARMK7:SELECT CASE (type_k)
            CASE(1) ANG_PARMK7
              ANG_PARMJ40:SELECT CASE (type_j)
                CASE(1) ANG_PARMJ40 !C(aromatic)-C(sp3)-C(sp3)
                  force_angle = FCONST_ANGLES(5)
                  eq_angle = EQUILIBRIUM_ANGLES(5)
                CASE(2) ANG_PARMJ40 !C(aromatic)-C(sp3)-C(sp2)
                  force_angle = FCONST_ANGLES(9)
                  eq_angle = EQUILIBRIUM_ANGLES(9)
                CASE(4) ANG_PARMJ40 !C(aromatic)-C(sp3)-C(sp)
                  force_angle = FCONST_ANGLES(12)
                  eq_angle = EQUILIBRIUM_ANGLES(12)
                CASE(20) ANG_PARMJ40 !C(aromatic)-C(sp3)-C(4M ring)
                  force_angle = FCONST_ANGLES(48)
                  eq_angle = EQUILIBRIUM_ANGLES(48)
                CASE(22) ANG_PARMJ40 !C(aromatic)-C(sp3)-C(3M ring)
                  force_angle = FCONST_ANGLES(50)
                  eq_angle = EQUILIBRIUM_ANGLES(50)
                CASE(37) ANG_PARMJ40 !C(aromatic)-C(sp3)-C(aromatic)
                  force_angle = FCONST_ANGLES(15)
                  eq_angle = EQUILIBRIUM_ANGLES(15)
                CASE DEFAULT ANG_PARMJ40
                  write(UNIout,'(a)')'CCC_ANG_PARM> jATOM: Illegal force field atom type!'
                  stop
              end SELECT ANG_PARMJ40
            CASE(2) ANG_PARMK7
              ANG_PARMJ41:SELECT CASE (type_j)
                CASE(1) ANG_PARMJ41 !C(aromatic)-C(sp2)-C(sp3)
                  force_angle = FCONST_ANGLES(20)
                  eq_angle = EQUILIBRIUM_ANGLES(20)
                CASE(2) ANG_PARMJ41 !C(aromatic)-C(sp2)-C(sp2)
! Angle type = 2
                  if(bond_ik.eq.1.and.bond_kj.eq.1) then
                    force_angle = FCONST_ANGLES(24)
                    eq_angle = EQUILIBRIUM_ANGLES(24)
! Angle type = 1
                  else if(bond_ik.eq.1.or.bond_kj.eq.1) then
                    force_angle = FCONST_ANGLES(128)
                    eq_angle = EQUILIBRIUM_ANGLES(128)
                  else
                    write(UNIout,'(a)')'CCC_BOND_PARM> Angle type unsupported.'
                    stop
                  end if
                CASE(4) ANG_PARMJ41 !C(aromatic)-C(sp2)-C(sp)
                  force_angle = FCONST_ANGLES(27)
                  eq_angle = EQUILIBRIUM_ANGLES(27)
                CASE(22) ANG_PARMJ41 !C(aromatic)-C(sp2)-C(3M ring)
                  force_angle = FCONST_ANGLES(62)
                  eq_angle = EQUILIBRIUM_ANGLES(62)
                CASE(30) ANG_PARMJ41 !C(aromatic)-C(sp2)-C(olefinic 4M ring)
                  force_angle = FCONST_ANGLES(63)
                  eq_angle = EQUILIBRIUM_ANGLES(63)
                CASE DEFAULT ANG_PARMJ41
                  write(UNIout,'(a)')'CCC_ANG_PARM> jATOM: Illegal force field atom type!'
                  stop
              end SELECT ANG_PARMJ41
            CASE(4) ANG_PARMK7
              ANG_PARMJ42:SELECT CASE (type_j)
                CASE(4) ANG_PARMJ42 !C(aromatic)-C(sp)-C(sp)
                  force_angle = FCONST_ANGLES(34)
                  eq_angle = EQUILIBRIUM_ANGLES(34)
                CASE DEFAULT ANG_PARMJ42
                  write(UNIout,'(a)')'CCC_ANG_PARM> jATOM: Illegal force field atom type!'
                  stop
              end SELECT ANG_PARMJ42
            CASE(20) ANG_PARMK7
              ANG_PARMJ43:SELECT CASE (type_j)
                CASE(1) ANG_PARMJ43 !C(aromatic)-C(4M ring)-C(sp3)
                  force_angle = FCONST_ANGLES(69)
                  eq_angle = EQUILIBRIUM_ANGLES(69)
                CASE(20) ANG_PARMJ43 !C(aromatic)-C(4M ring)-C(4M ring)
                  IN_RING16:SELECT CASE(ANG_IN_RING)
                    CASE (16) IN_RING16
                      force_angle = FCONST_ANGLES(85)
                      eq_angle = EQUILIBRIUM_ANGLES(85)
                    CASE DEFAULT IN_RING16
                      force_angle = FCONST_ANGLES(84)
                      eq_angle = EQUILIBRIUM_ANGLES(84)
                  end SELECT IN_RING16
                CASE DEFAULT ANG_PARMJ43
                  write(UNIout,'(a)')'CCC_ANG_PARM> jATOM: Illegal force field atom type!'
                  stop
              end SELECT ANG_PARMJ43
            CASE(22) ANG_PARMK7
              ANG_PARMJ44:SELECT CASE (type_j)
                CASE(1) ANG_PARMJ44 !C(aromatic)-C(3M ring)-C(sp3)
                  force_angle = FCONST_ANGLES(91)
                  eq_angle = EQUILIBRIUM_ANGLES(91)
                CASE(22) ANG_PARMJ44 !C(aromatic)-C(3M ring)-C(3M ring)
                  force_angle = FCONST_ANGLES(108)
                  eq_angle = EQUILIBRIUM_ANGLES(108)
                CASE(37) ANG_PARMJ44 !C(aromatic)-C(3M ring)-C(aromatic)
                  IN_RING17:SELECT CASE(ANG_IN_RING)
                    CASE (9) IN_RING17
                      force_angle = FCONST_ANGLES(109)
                      eq_angle = EQUILIBRIUM_ANGLES(109)
                    CASE DEFAULT IN_RING17
                      force_angle = FCONST_ANGLES(109)
                      eq_angle = EQUILIBRIUM_ANGLES(109)
                  end SELECT IN_RING17
                CASE DEFAULT ANG_PARMJ44
                  write(UNIout,'(a)')'CCC_ANG_PARM> jATOM: Illegal force field atom type!'
                  stop
              end SELECT ANG_PARMJ44
            CASE(30) ANG_PARMK7
              write(UNIout,'(a)')'CCC_ANG_PARM> Angle type not supported!'
              stop
            CASE(37) ANG_PARMK7
              ANG_PARMJ46:SELECT CASE (type_j)
                CASE(1) ANG_PARMJ46 !C(aromatic)-C(aromatic)-C(sp3)
                  force_angle = FCONST_ANGLES(35)
                  eq_angle = EQUILIBRIUM_ANGLES(35)
                CASE(2) ANG_PARMJ46 !C(aromatic)-C(aromatic)-C(sp2)
                  force_angle = FCONST_ANGLES(36)
                  eq_angle = EQUILIBRIUM_ANGLES(36)
                CASE(4) ANG_PARMJ46 !C(aromatic)-C(aromatic)-C(sp)
                  force_angle = FCONST_ANGLES(37)
                  eq_angle = EQUILIBRIUM_ANGLES(37)
                CASE(20) ANG_PARMJ46 !C(aromatic)-C(aromatic)-C(4M ring)
                  IN_RING18:SELECT CASE(ANG_IN_RING)
                    CASE (16) IN_RING18
                      force_angle = FCONST_ANGLES(121)
                      eq_angle = EQUILIBRIUM_ANGLES(121)
                    CASE DEFAULT IN_RING18
                      force_angle = FCONST_ANGLES(120)
                      eq_angle = EQUILIBRIUM_ANGLES(120)
                  end SELECT IN_RING18
                CASE(22) ANG_PARMJ46 !C(aromatic)-C(aromatic)-C(3M ring)
                  IN_RING19:SELECT CASE(ANG_IN_RING)
                    CASE (16) IN_RING19
                      force_angle = FCONST_ANGLES(123)
                      eq_angle = EQUILIBRIUM_ANGLES(123)
                    CASE DEFAULT IN_RING19
                      force_angle = FCONST_ANGLES(122)
                      eq_angle = EQUILIBRIUM_ANGLES(122)
                  end SELECT IN_RING19
                CASE(37) ANG_PARMJ46 !C(aromatic)-C(aromatic)-C(aromatic)
! Angle type = 4
                  if(ANG_IN_RING.eq.16) then
                    force_angle = FCONST_ANGLES(130)
                    eq_angle = EQUILIBRIUM_ANGLES(130)
! Angle type = 1
                  else if(ANG_IN_RING.eq.0.and.(bond_ik.eq.1.or.bond_kj.eq.1)) then
                    force_angle = FCONST_ANGLES(129)
                    eq_angle = EQUILIBRIUM_ANGLES(129)
! Angle type = 0
                  else
                    force_angle = FCONST_ANGLES(39)
                    eq_angle = EQUILIBRIUM_ANGLES(39)
                  end if
                CASE DEFAULT ANG_PARMJ46
                  write(UNIout,'(a)')'CCC_ANG_PARM> jATOM: Illegal force field atom type!'
                  stop
              end SELECT ANG_PARMJ46
            CASE DEFAULT ANG_PARMK7
              write(UNIout,'(a)')'CCC_ANG_PARM> kATOM: Illegal force field atom type!'
              stop
          end SELECT ANG_PARMK7
        CASE DEFAULT ANG_PARMI
          write(UNIout,'(a)')'CCC_ANG_PARM> iATOM: Illegal force field atom type!'
          stop
      end SELECT ANG_PARMI

! End of function CCC_ANG_PARM
      return
      END

      subroutine GETTOR_PARMS_HCCH(iTOR, type_j, type_k)
!*******************************************************************************
! Date last modified: 25 August, 1999                              Version 1.0 *
! Author: Michelle Shaw                                                        *
! Description: Get torsional parameters v1, v2, and v3 for a HCCH torsion.     *
!*******************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE tor_params
      USE mm_parameters

      implicit none
!
! Input scalars:
      integer iTOR, type_j, type_k
!
! Begin:
! The torsion parameters v1, v2, and v3 must be collected from the module and placed in the derived
!  type MMtorsion.  This is done by combining the atom "descriptors" in a mathematical
!  statement to obtain a case number, which is then compared against those for the supported torsion angles.
      TOR_PARMJC:SELECT CASE(type_j)
        CASE(1) TOR_PARMJC
          TOR_PARMK1:SELECT CASE(type_k)
            CASE(1) TOR_PARMK1
! H-C(sp3)-C(sp3)-H torsion angle
              MMtorsion(iTOR)%V1 = V1_TORSION(5)
              MMtorsion(iTOR)%V2 = V2_TORSION(5)
              MMtorsion(iTOR)%V3 = V3_TORSION(5)
            CASE(2) TOR_PARMK1
! H-C(sp3)-C(sp2)-H torsion angle
              MMtorsion(iTOR)%V1 = V1_TORSION(14)
              MMtorsion(iTOR)%V2 = V2_TORSION(14)
              MMtorsion(iTOR)%V3 = V3_TORSION(14)
            CASE(20) TOR_PARMK1
! H-C(sp3)-C(4-membered ring)-H torsion angle
              MMtorsion(iTOR)%V1 = V1_TORSION(32)
              MMtorsion(iTOR)%V2 = V2_TORSION(32)
              MMtorsion(iTOR)%V3 = V3_TORSION(32)
            CASE DEFAULT TOR_PARMK1
              write(UNIout,'(a)')'HCCH_TOR_PARM> Torsion not supported.'
          end SELECT TOR_PARMK1
        CASE(2) TOR_PARMJC
          TOR_PARMK2:SELECT CASE(type_k)
            CASE(1) TOR_PARMK2
! H-C(sp2)-C(sp3)-H torsion angle
              MMtorsion(iTOR)%V1 = V1_TORSION(14)
              MMtorsion(iTOR)%V2 = V2_TORSION(14)
              MMtorsion(iTOR)%V3 = V3_TORSION(14)
            CASE(2) TOR_PARMK2
! H-C(sp2)-C(sp2)-H torsion angle
              MMtorsion(iTOR)%V1 = V1_TORSION(23)
              MMtorsion(iTOR)%V2 = V2_TORSION(23)
              MMtorsion(iTOR)%V3 = V3_TORSION(23)
            CASE DEFAULT TOR_PARMK2
              write(UNIout,'(a)')'HCCH_TOR_PARM> Torsion not supported.'
          end SELECT TOR_PARMK2
        CASE(20) TOR_PARMJC
          TOR_PARMK3:SELECT CASE(type_k)
            CASE(1) TOR_PARMK3
! H-C(4-membered ring)-C(sp3)-H torsion angle
              MMtorsion(iTOR)%V1 = V1_TORSION(32)
              MMtorsion(iTOR)%V2 = V2_TORSION(32)
              MMtorsion(iTOR)%V3 = V3_TORSION(32)
            CASE(20) TOR_PARMK3
! H-C(4-membered ring)-C(4M ring)-H torsion angle
              MMtorsion(iTOR)%V1 = V1_TORSION(36)
              MMtorsion(iTOR)%V2 = V2_TORSION(36)
              MMtorsion(iTOR)%V3 = V3_TORSION(36)
            CASE DEFAULT TOR_PARMK3
              write(UNIout,'(a)')'HCCH_TOR_PARM> Torsion not supported.'
          end SELECT TOR_PARMK3
        CASE(37) TOR_PARMJC
! H-C(aromatic)-C(aromatic)-H torsion angle
          MMtorsion(iTOR)%V1 = V1_TORSION(29)
          MMtorsion(iTOR)%V2 = V2_TORSION(29)
          MMtorsion(iTOR)%V3 = V3_TORSION(29)
        CASE DEFAULT TOR_PARMJC
          call GETTOR_PARMS_WILD(iTOR, 5, type_j, type_k, 5)
      end SELECT TOR_PARMJC

! End of routine GETTOR_PARMS_HCCH
      return
      END

      subroutine GETTOR_PARMS_HCCC(iTOR, type_j, type_k, type_l)
!*******************************************************************************
! Date last modified: 25 August, 1999                              Version 1.0 *
! Author: Michelle Shaw                                                        *
! Description: Get torsional parameters v1, v2, and v3 for a HCCC torsion.     *
!*******************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE tor_params
      USE mm_parameters

      implicit none
!
! Input scalars:
      integer iTOR, type_j, type_k, type_l
!
! Begin:
! The torsion parameters v1, v2, and v3 must be collected from the module and placed in the derived
!  type MMtorsion.  This is done by combining the atom "descriptors" in a mathematical statement to
!  obtain a case number, which is then compared against those for the supported torsion angles.
      TOR_PARMJC:SELECT CASE(type_j)
        CASE(1) TOR_PARMJC
          TOR_PARMK1:SELECT CASE(type_k)
            CASE(1) TOR_PARMK1
              TOR_PARML1:SELECT CASE(type_l)
                CASE(1) TOR_PARML1
! H-C(sp3)-C(sp3)-C(sp3) torsion angle
                  MMtorsion(iTOR)%V1 = V1_TORSION(3)
                  MMtorsion(iTOR)%V2 = V2_TORSION(3)
                  MMtorsion(iTOR)%V3 = V3_TORSION(3)
                CASE(2) TOR_PARML1
! H-C(sp3)-C(sp3)-C(sp2) torsion angle
                  MMtorsion(iTOR)%V1 = V1_TORSION(4)
                  MMtorsion(iTOR)%V2 = V2_TORSION(4)
                  MMtorsion(iTOR)%V3 = V3_TORSION(4)
                CASE(37) TOR_PARML1
! H-C(sp3)-C(sp3)-C(aromatic) torsion angle
                  MMtorsion(iTOR)%V1 = V1_TORSION(4)
                  MMtorsion(iTOR)%V2 = V2_TORSION(4)
                  MMtorsion(iTOR)%V3 = V3_TORSION(4)
                CASE DEFAULT TOR_PARML1
                  write(UNIout,'(a)')'HCCC_TOR_PARM> Torsion not supported.'
              end SELECT TOR_PARML1
            CASE(2) TOR_PARMK1
              TOR_PARML2:SELECT CASE(type_l)
                CASE(1) TOR_PARML2
! H-C(sp3)-C(sp2)-C(sp3) torsion angle
                  MMtorsion(iTOR)%V1 = V1_TORSION(12)
                  MMtorsion(iTOR)%V2 = V2_TORSION(12)
                  MMtorsion(iTOR)%V3 = V3_TORSION(12)
                CASE(2) TOR_PARML2
! H-C(sp3)-C(sp2)-C(sp2) torsion angle
                  MMtorsion(iTOR)%V1 = V1_TORSION(13)
                  MMtorsion(iTOR)%V2 = V2_TORSION(13)
                  MMtorsion(iTOR)%V3 = V3_TORSION(13)
                CASE(37) TOR_PARML2
! H-C(sp3)-C(sp2)-C(aromatic) torsion angle
                  MMtorsion(iTOR)%V1 = V1_TORSION(15)
                  MMtorsion(iTOR)%V2 = V2_TORSION(15)
                  MMtorsion(iTOR)%V3 = V3_TORSION(15)
                CASE DEFAULT TOR_PARML2
                  write(UNIout,'(a)')'HCCC_TOR_PARM> Torsion not supported.'
              end SELECT TOR_PARML2
            CASE(20) TOR_PARMK1
              TOR_PARML3:SELECT CASE(type_l)
                CASE(20) TOR_PARML3
! H-C(sp3)-C(4-membered ring)-C(4-membered ring) torsion angle
                  MMtorsion(iTOR)%V1 = V1_TORSION(33)
                  MMtorsion(iTOR)%V2 = V2_TORSION(33)
                  MMtorsion(iTOR)%V3 = V3_TORSION(33)
                CASE DEFAULT TOR_PARML3
                  write(UNIout,'(a)')'HCCC_TOR_PARM> Torsion not supported.'
              end SELECT TOR_PARML3
            CASE(22) TOR_PARMK1
! H-C(sp3)-C(3-membered ring)-* torsion angle
                  MMtorsion(iTOR)%V1 = V1_TORSION(39)
                  MMtorsion(iTOR)%V2 = V2_TORSION(39)
                  MMtorsion(iTOR)%V3 = V3_TORSION(39)
            CASE(37) TOR_PARMK1
              TOR_PARML4:SELECT CASE(type_l)
                CASE(37) TOR_PARML4
! H-C(sp3)-C(aromatic)-C(aromatic) torsion angle
                  MMtorsion(iTOR)%V1 = V1_TORSION(17)
                  MMtorsion(iTOR)%V2 = V2_TORSION(17)
                  MMtorsion(iTOR)%V3 = V3_TORSION(17)
                CASE DEFAULT TOR_PARML4
                  write(UNIout,'(a)')'HCCC_TOR_PARM> Torsion not supported.'
              end SELECT TOR_PARML4
            CASE DEFAULT TOR_PARMK1
              write(UNIout,'(a)')'HCCC_TOR_PARM> Torsion not supported.'
          end SELECT TOR_PARMK1
        CASE(2) TOR_PARMJC
          TOR_PARMK2:SELECT CASE(type_k)
            CASE(1) TOR_PARMK2
              TOR_PARML5:SELECT CASE(type_l)
                CASE(1) TOR_PARML5
! H-C(sp2)-C(sp3)-C(sp3) torsion angle
                  MMtorsion(iTOR)%V1 = V1_TORSION(9)
                  MMtorsion(iTOR)%V2 = V2_TORSION(9)
                  MMtorsion(iTOR)%V3 = V3_TORSION(9)
                CASE(2) TOR_PARML5
! H-C(sp2)-C(sp3)-C(sp2) torsion angle
                  MMtorsion(iTOR)%V1 = V1_TORSION(11)
                  MMtorsion(iTOR)%V2 = V2_TORSION(11)
                  MMtorsion(iTOR)%V3 = V3_TORSION(11)
                CASE DEFAULT TOR_PARML5
                  write(UNIout,'(a)')'HCCC_TOR_PARM> Torsion not supported.'
              end SELECT TOR_PARML5
            CASE(2) TOR_PARMK2
               TOR_PARML6:SELECT CASE(type_l)
                CASE(1) TOR_PARML6
! H-C(sp2)-C(sp2)-C(sp3) torsion angle
                  MMtorsion(iTOR)%V1 = V1_TORSION(20)
                  MMtorsion(iTOR)%V2 = V2_TORSION(20)
                  MMtorsion(iTOR)%V3 = V3_TORSION(20)
                CASE(2) TOR_PARML6
! H-C(sp2)-C(sp2)-C(sp2) torsion angle
                  MMtorsion(iTOR)%V1 = V1_TORSION(22)
                  MMtorsion(iTOR)%V2 = V2_TORSION(22)
                  MMtorsion(iTOR)%V3 = V3_TORSION(22)
                CASE(37) TOR_PARML6
! H-C(sp2)-C(sp2)-C(aromatic) torsion angle
                  MMtorsion(iTOR)%V1 = V1_TORSION(24)
                  MMtorsion(iTOR)%V2 = V2_TORSION(24)
                  MMtorsion(iTOR)%V3 = V3_TORSION(24)
                CASE DEFAULT TOR_PARML6
                  write(UNIout,'(a)')'HCCC_TOR_PARM> Torsion not supported.'
              end SELECT TOR_PARML6
            CASE DEFAULT TOR_PARMK2
              write(UNIout,'(a)')'HCCC_TOR_PARM> Torsion not supported.'
          end SELECT TOR_PARMK2
        CASE(20) TOR_PARMJC
          TOR_PARMK3:SELECT CASE(type_k)
            CASE(20) TOR_PARMK3
              TOR_PARML7:SELECT CASE(type_l)
                CASE(1) TOR_PARML7
! H-C(4-membered ring)-C(4-membered ring)-C(sp3) torsion angle
                  MMtorsion(iTOR)%V1 = V1_TORSION(34)
                  MMtorsion(iTOR)%V2 = V2_TORSION(34)
                  MMtorsion(iTOR)%V3 = V3_TORSION(34)
                CASE(20) TOR_PARML7
! H-C(4-membered ring)-C(4-membered ring)-C(4-membered ring) torsion angle
                  MMtorsion(iTOR)%V1 = V1_TORSION(37)
                  MMtorsion(iTOR)%V2 = V2_TORSION(37)
                  MMtorsion(iTOR)%V3 = V3_TORSION(37)
                CASE DEFAULT TOR_PARML7
                  write(UNIout,'(a)')'HCCC_TOR_PARM> Torsion not supported.'
              end SELECT TOR_PARML7
            CASE DEFAULT TOR_PARMK3
              write(UNIout,'(a)')'HCCC_TOR_PARM> Torsion not supported.'
          end SELECT TOR_PARMK3
        CASE(22) TOR_PARMJC
          call GETTOR_PARMS_WILD(iTOR, 5, type_j, type_k, type_l)
        CASE(37) TOR_PARMJC
          TOR_PARMK4:SELECT CASE(type_k)
            CASE(37) TOR_PARMK4
              TOR_PARML8:SELECT CASE(type_l)
                CASE(1) TOR_PARML8
! H-C(aromatic)-C(aromatic)-C(sp3) torsion angle
                  MMtorsion(iTOR)%V1 = V1_TORSION(25)
                  MMtorsion(iTOR)%V2 = V2_TORSION(25)
                  MMtorsion(iTOR)%V3 = V3_TORSION(25)
                CASE(2) TOR_PARML8
! H-C(aromatic)-C(aromatic)-C(sp2) torsion angle
                  MMtorsion(iTOR)%V1 = V1_TORSION(27)
                  MMtorsion(iTOR)%V2 = V2_TORSION(27)
                  MMtorsion(iTOR)%V3 = V3_TORSION(27)
                CASE(37) TOR_PARML8
! H-C(aromatic)-C(aromatic)-C(aromatic) torsion angle
                  MMtorsion(iTOR)%V1 = V1_TORSION(30)
                  MMtorsion(iTOR)%V2 = V2_TORSION(30)
                  MMtorsion(iTOR)%V3 = V3_TORSION(30)
                CASE DEFAULT TOR_PARML8
                  write(UNIout,'(a)')'HCCC_TOR_PARM> Torsion not supported.'
              end SELECT TOR_PARML8
            CASE DEFAULT TOR_PARMK4
              write(UNIout,'(a)')'HCCC_TOR_PARM> Torsion not supported.'
          end SELECT TOR_PARMK4
        CASE DEFAULT TOR_PARMJC
          call GETTOR_PARMS_WILD(iTOR, 5, type_j, type_k, type_l)
      end SELECT TOR_PARMJC

! End of routine GETTOR_PARMS_HCCC
      return
      END

      subroutine GETTOR_PARMS_CCCH(iTOR, type_i, type_j, type_k)
!*******************************************************************************
! Date last modified: 25 August, 1999                              Version 1.0 *
! Author: Michelle Shaw                                                        *
! Description: Get torsional parameters v1, v2, and v3 for a CCCH torsion.     *
!*******************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE tor_params
      USE mm_parameters

      implicit none
!
! Input scalars:
      integer iTOR, type_i, type_j, type_k
!
! Begin:
! The torsion parameters v1, v2, and v3 must be collected from the module and placed in the derived
!  type MMtorsion.  This is done by combining the atom "descriptors" in a mathematical statement to
!  obtain a case number, which is then compared against those for the supported torsion angles.
      TOR_PARMKC:SELECT CASE(type_k)
        CASE(1) TOR_PARMKC
          TOR_PARMJ1:SELECT CASE(type_j)
            CASE(1) TOR_PARMJ1
              TOR_PARMI1:SELECT CASE(type_i)
                CASE(1) TOR_PARMI1
! C(sp3)-C(sp3)-C(sp3)-H torsion angle
                  MMtorsion(iTOR)%V1 = V1_TORSION(3)
                  MMtorsion(iTOR)%V2 = V2_TORSION(3)
                  MMtorsion(iTOR)%V3 = V3_TORSION(3)
                CASE(2) TOR_PARMI1
! C(sp2)-C(sp3)-C(sp3)-H torsion angle
                  MMtorsion(iTOR)%V1 = V1_TORSION(4)
                  MMtorsion(iTOR)%V2 = V2_TORSION(4)
                  MMtorsion(iTOR)%V3 = V3_TORSION(4)
                CASE(37) TOR_PARMI1
! C(aromatic)-C(sp3)-C(sp3)-H torsion angle
                  MMtorsion(iTOR)%V1 = V1_TORSION(4)
                  MMtorsion(iTOR)%V2 = V2_TORSION(4)
                  MMtorsion(iTOR)%V3 = V3_TORSION(4)
                CASE DEFAULT TOR_PARMI1
                  write(UNIout,'(a)')'CCCH_TOR_PARM> Torsion not supported.'
              end SELECT TOR_PARMI1
            CASE(2) TOR_PARMJ1
              TOR_PARMI2:SELECT CASE(type_i)
                CASE(1) TOR_PARMI2
! C(sp3)-C(sp2)-C(sp3)-H torsion angle
                  MMtorsion(iTOR)%V1 = V1_TORSION(12)
                  MMtorsion(iTOR)%V2 = V2_TORSION(12)
                  MMtorsion(iTOR)%V3 = V3_TORSION(12)
                CASE(2) TOR_PARMI2
! C(sp2)-C(sp2)-C(sp3)-H torsion angle
                  MMtorsion(iTOR)%V1 = V1_TORSION(13)
                  MMtorsion(iTOR)%V2 = V2_TORSION(13)
                  MMtorsion(iTOR)%V3 = V3_TORSION(13)
                CASE(37) TOR_PARMI2
! C(aromatic)-C(sp2)-C(sp3)-H torsion angle
                  MMtorsion(iTOR)%V1 = V1_TORSION(15)
                  MMtorsion(iTOR)%V2 = V2_TORSION(15)
                  MMtorsion(iTOR)%V3 = V3_TORSION(15)
                CASE DEFAULT TOR_PARMI2
                  write(UNIout,'(a)')'CCCH_TOR_PARM> Torsion not supported.'
              end SELECT TOR_PARMI2
            CASE(20) TOR_PARMJ1
              TOR_PARMI3:SELECT CASE(type_i)
                CASE(20) TOR_PARMI3
! C(4M ring)-C(4M ring)-C(sp3)-H torsion angle
                  MMtorsion(iTOR)%V1 = V1_TORSION(33)
                  MMtorsion(iTOR)%V2 = V2_TORSION(33)
                  MMtorsion(iTOR)%V3 = V3_TORSION(33)
                CASE DEFAULT TOR_PARMI3
                  write(UNIout,'(a)')'CCCH_TOR_PARM> Torsion not supported.'
              end SELECT TOR_PARMI3
            CASE(22) TOR_PARMJ1
              call GETTOR_PARMS_WILD(iTOR, type_i, type_j, type_k, 5)
            CASE(37) TOR_PARMJ1
              TOR_PARMI4:SELECT CASE(type_i)
                CASE(37) TOR_PARMI4
! C(aromatic)-C(aromatic)-C(sp3)-H torsion angle
                  MMtorsion(iTOR)%V1 = V1_TORSION(17)
                  MMtorsion(iTOR)%V2 = V2_TORSION(17)
                  MMtorsion(iTOR)%V3 = V3_TORSION(17)
                CASE DEFAULT TOR_PARMI4
                  write(UNIout,'(a)')'HCCC_TOR_PARM> Torsion not supported.'
              end SELECT TOR_PARMI4
            CASE DEFAULT TOR_PARMJ1
              write(UNIout,'(a)')'HCCC_TOR_PARM> Torsion not supported.'
          end SELECT TOR_PARMJ1
        CASE(2) TOR_PARMKC
          TOR_PARMJ2:SELECT CASE(type_j)
            CASE(1) TOR_PARMJ2
              TOR_PARMI5:SELECT CASE(type_i)
                CASE(1) TOR_PARMI5
! C(sp3)-C(sp3)-C(sp2)-H torsion angle
                  MMtorsion(iTOR)%V1 = V1_TORSION(9)
                  MMtorsion(iTOR)%V2 = V2_TORSION(9)
                  MMtorsion(iTOR)%V3 = V3_TORSION(9)
                CASE(2) TOR_PARMI5
! C(sp2)-C(sp3)-C(sp2)-H torsion angle
                  MMtorsion(iTOR)%V1 = V1_TORSION(11)
                  MMtorsion(iTOR)%V2 = V2_TORSION(11)
                  MMtorsion(iTOR)%V3 = V3_TORSION(11)
                CASE DEFAULT TOR_PARMI5
                  write(UNIout,'(a)')'HCCC_TOR_PARM> Torsion not supported.'
              end SELECT TOR_PARMI5
            CASE(2) TOR_PARMJ2
               TOR_PARMI6:SELECT CASE(type_i)
                CASE(1) TOR_PARMI6
! C(sp3)-C(sp2)-C(sp2)-H torsion angle
                  MMtorsion(iTOR)%V1 = V1_TORSION(20)
                  MMtorsion(iTOR)%V2 = V2_TORSION(20)
                  MMtorsion(iTOR)%V3 = V3_TORSION(20)
                CASE(2) TOR_PARMI6
! C(sp2)-C(sp2)-C(sp2)-H torsion angle
                  MMtorsion(iTOR)%V1 = V1_TORSION(22)
                  MMtorsion(iTOR)%V2 = V2_TORSION(22)
                  MMtorsion(iTOR)%V3 = V3_TORSION(22)
                CASE(37) TOR_PARMI6
! C(aromatic)-C(sp2)-C(sp2)-H torsion angle
                  MMtorsion(iTOR)%V1 = V1_TORSION(24)
                  MMtorsion(iTOR)%V2 = V2_TORSION(24)
                  MMtorsion(iTOR)%V3 = V3_TORSION(24)
                CASE DEFAULT TOR_PARMI6
                  write(UNIout,'(a)')'HCCC_TOR_PARM> Torsion not supported.'
              end SELECT TOR_PARMI6
            CASE(22) TOR_PARMJ2
              call GETTOR_PARMS_WILD(iTOR, type_i, type_j, type_k, 5)
            CASE DEFAULT TOR_PARMJ2
              write(UNIout,'(a)')'HCCC_TOR_PARM> Torsion not supported.'
          end SELECT TOR_PARMJ2
        CASE(20) TOR_PARMKC
          TOR_PARMJ3:SELECT CASE(type_j)
            CASE(20) TOR_PARMJ3
              TOR_PARMI7:SELECT CASE(type_i)
                CASE(1) TOR_PARMI7
! C(sp3)-C(4M ring)-C(4M ring)-H torsion angle
                  MMtorsion(iTOR)%V1 = V1_TORSION(34)
                  MMtorsion(iTOR)%V2 = V2_TORSION(34)
                  MMtorsion(iTOR)%V3 = V3_TORSION(34)
                CASE(20) TOR_PARMI7
! C(4M ring)-C(4M ring)-C(4M ring)-H torsion angle
                  MMtorsion(iTOR)%V1 = V1_TORSION(37)
                  MMtorsion(iTOR)%V2 = V2_TORSION(37)
                  MMtorsion(iTOR)%V3 = V3_TORSION(37)
                CASE DEFAULT TOR_PARMI7
                  write(UNIout,'(a)')'HCCC_TOR_PARM> Torsion not supported.'
              end SELECT TOR_PARMI7
            CASE(22) TOR_PARMJ3
              call GETTOR_PARMS_WILD(iTOR, type_i, type_j, type_k, 5)
            CASE DEFAULT TOR_PARMJ3
              write(UNIout,'(a)')'HCCC_TOR_PARM> Torsion not supported.'
          end SELECT TOR_PARMJ3
        CASE(37) TOR_PARMKC
          TOR_PARMJ4:SELECT CASE(type_j)
            CASE(22) TOR_PARMJ4
              call GETTOR_PARMS_WILD(iTOR, type_i, type_j, type_k, 5)
            CASE(37) TOR_PARMJ4
              TOR_PARMI8:SELECT CASE(type_i)
                CASE(1) TOR_PARMI8
! C(sp3)-C(aromatic)-C(aromatic)-H torsion angle
                  MMtorsion(iTOR)%V1 = V1_TORSION(25)
                  MMtorsion(iTOR)%V2 = V2_TORSION(25)
                  MMtorsion(iTOR)%V3 = V3_TORSION(25)
                CASE(2) TOR_PARMI8
! C(sp2)-C(aromatic)-C(aromatic)-H torsion angle
                  MMtorsion(iTOR)%V1 = V1_TORSION(27)
                  MMtorsion(iTOR)%V2 = V2_TORSION(27)
                  MMtorsion(iTOR)%V3 = V3_TORSION(27)
                CASE(37) TOR_PARMI8
! C(aromatic)-C(aromatic)-C(aromatic)-H torsion angle
                  MMtorsion(iTOR)%V1 = V1_TORSION(30)
                  MMtorsion(iTOR)%V2 = V2_TORSION(30)
                  MMtorsion(iTOR)%V3 = V3_TORSION(30)
                CASE DEFAULT TOR_PARMI8
                  write(UNIout,'(a)')'HCCC_TOR_PARM> Torsion not supported.'
              end SELECT TOR_PARMI8
            CASE DEFAULT TOR_PARMJ4
              write(UNIout,'(a)')'HCCC_TOR_PARM> Torsion not supported.'
          end SELECT TOR_PARMJ4
        CASE DEFAULT TOR_PARMKC
          call GETTOR_PARMS_WILD(iTOR, type_i, type_j, type_k, 5)
      end SELECT TOR_PARMKC

! End of routine GETTOR_PARMS_CCCH
      return
      END
      subroutine GETTOR_PARMS_CCCC(iTOR, type_i, type_j, type_k, type_l)
!*******************************************************************************
! Date last modified: 25 August, 1999                              Version 1.0 *
! Author: Michelle Shaw                                                        *
! Description: Get torsional parameters v1, v2, and v3 for a CCCC torsion.     *
!*******************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE tor_params
      USE mm_parameters

      implicit none
!
! Input scalars:
      integer iTOR, type_i, type_j, type_k, type_l
!
! Begin:
! The torsion parameters v1, v2, and v3 must be collected from the module and placed in the derived
!  type MMtorsion.  This is done by combining the atom "descriptors" in a mathematical statement to
!  obtain a case number, which is then compared against those for the supported torsion angles.
      SELECT CASE(type_i)                   ! TOR_PARMI
        CASE(1)
          SELECT CASE(type_j)               ! TOR_PARMJ1
            CASE(1)
              SELECT CASE(type_k)           ! TOR_PARMK1
                CASE(1)
                  SELECT CASE(type_l)       ! TOR_PARML1
                    CASE(1)
! C(sp3)-C(sp3)-C(sp3)-C(sp3) torsion angle
                      MMtorsion(iTOR)%V1 = V1_TORSION(1)
                      MMtorsion(iTOR)%V2 = V2_TORSION(1)
                      MMtorsion(iTOR)%V3 = V3_TORSION(1)
                    CASE(2)
! C(sp3)-C(sp3)-C(sp3)-C(sp2) torsion angle
                      MMtorsion(iTOR)%V1 = V1_TORSION(2)
                      MMtorsion(iTOR)%V2 = V2_TORSION(2)
                      MMtorsion(iTOR)%V3 = V3_TORSION(2)
                    CASE DEFAULT
                      write(UNIout,'(a)')'CCCC_TOR_PARM> Torsion not supported.'
                  end SELECT
                CASE(2)
                  SELECT CASE(type_l)         ! TOR_PARML2
                    CASE(1)
! C(sp3)-C(sp3)-C(sp2)-C(sp3) torsion angle
                      MMtorsion(iTOR)%V1 = V1_TORSION(7)
                      MMtorsion(iTOR)%V2 = V2_TORSION(7)
                      MMtorsion(iTOR)%V3 = V3_TORSION(7)
                    CASE(2)
! C(sp3)-C(sp3)-C(sp2)-C(sp2) torsion angle
                      MMtorsion(iTOR)%V1 = V1_TORSION(8)
                      MMtorsion(iTOR)%V2 = V2_TORSION(8)
                      MMtorsion(iTOR)%V3 = V3_TORSION(8)
                    CASE DEFAULT
                      write(UNIout,'(a)')'CCCC_TOR_PARM> Torsion not supported.'
                  end SELECT
                CASE DEFAULT
                  write(UNIout,'(a)')'CCCC_TOR_PARM> Torsion not supported.'
                  call GETTOR_PARMS_WILD(iTOR, type_i, type_j, type_k, type_l)
              end SELECT
            CASE(2)
              SELECT CASE(type_k)           ! TOR_PARMK2
                CASE(1)
                  SELECT CASE(type_l)        ! TOR_PARML3
                    CASE(1)
! C(sp3)-C(sp2)-C(sp3)-C(sp3) torsion angle
                      MMtorsion(iTOR)%V1 = V1_TORSION(7)
                      MMtorsion(iTOR)%V2 = V2_TORSION(7)
                      MMtorsion(iTOR)%V3 = V3_TORSION(7)
                    CASE DEFAULT
                      write(UNIout,'(a)')'CCCC_TOR_PARM> Torsion not supported.'
                  end SELECT
                CASE(2)
                  SELECT CASE(type_l)        ! TOR_PARML4
                    CASE(1)
! C(sp3)-C(sp2)-C(sp2)-C(sp3) torsion angle
                      MMtorsion(iTOR)%V1 = V1_TORSION(18)
                      MMtorsion(iTOR)%V2 = V2_TORSION(18)
                      MMtorsion(iTOR)%V3 = V3_TORSION(18)
                    CASE(2)
! C(sp3)-C(sp2)-C(sp2)-C(sp2) torsion angle
                      MMtorsion(iTOR)%V1 = V1_TORSION(19)
                      MMtorsion(iTOR)%V2 = V2_TORSION(19)
                      MMtorsion(iTOR)%V3 = V3_TORSION(19)
                    CASE DEFAULT
                      write(UNIout,'(a)')'CCCC_TOR_PARM> Torsion not supported.'
                  end SELECT
                CASE DEFAULT
                  write(UNIout,'(a)')'CCCC_TOR_PARM> Torsion not supported.'
                  call GETTOR_PARMS_WILD(iTOR, type_i, type_j, type_k, type_l)
              end SELECT
            CASE(20)
              SELECT CASE(type_k)            ! TOR_PARMK3
                CASE(20)
                  SELECT CASE(type_l)        ! TOR_PARML5
                    CASE(20)
! C(sp3)-C(4M ring)-C(4M ring)-C(4M ring) torsion angle
                      MMtorsion(iTOR)%V1 = V1_TORSION(35)
                      MMtorsion(iTOR)%V2 = V2_TORSION(35)
                      MMtorsion(iTOR)%V3 = V3_TORSION(35)
                    CASE DEFAULT
                      write(UNIout,'(a)')'CCCC_TOR_PARM> Torsion not supported.'
                  end SELECT
                CASE DEFAULT
                  write(UNIout,'(a)')'CCCC_TOR_PARM> Torsion not supported.'
                  call GETTOR_PARMS_WILD(iTOR, type_i, type_j, type_k, type_l)
              end SELECT
            CASE(37)
              SELECT CASE(type_k)            ! TOR_PARMK4
                CASE(37)
                  SELECT CASE(type_l)        ! TOR_PARML6
                    CASE(37)
! C(sp3)-C(aromatic)-C(aromatic)-C(aromatic) torsion angle
                      MMtorsion(iTOR)%V1 = V1_TORSION(26)
                      MMtorsion(iTOR)%V2 = V2_TORSION(26)
                      MMtorsion(iTOR)%V3 = V3_TORSION(26)
                    CASE DEFAULT
                      write(UNIout,'(a)')'CCCC_TOR_PARM> Torsion not supported.'
                  end SELECT
                CASE DEFAULT
                  write(UNIout,'(a)')'CCCC_TOR_PARM> Torsion not supported.'
                  call GETTOR_PARMS_WILD(iTOR, type_i, type_j, type_k, type_l)
              end SELECT
            CASE DEFAULT
              write(UNIout,'(a)')'CCCC_TOR_PARM> Torsion not supported.'
              call GETTOR_PARMS_WILD(iTOR, type_i, type_j, type_k, type_l)
          end SELECT
        CASE(2)
          SELECT CASE(type_j)            ! TOR_PARMJ2
            CASE(1)
              SELECT CASE(type_k)         ! TOR_PARMK5
                CASE(1)
                  SELECT CASE(type_l)        ! TOR_PARML7
                    CASE(1)
! C(sp2)-C(sp3)-C(sp3)-C(sp3) torsion angle
                      MMtorsion(iTOR)%V1 = V1_TORSION(2)
                      MMtorsion(iTOR)%V2 = V2_TORSION(2)
                      MMtorsion(iTOR)%V3 = V3_TORSION(2)
                    CASE DEFAULT
                      write(UNIout,'(a)')'CCCC_TOR_PARM> Torsion not supported.'
                  end SELECT
                CASE(2)
                  SELECT CASE(type_l)      ! TOR_PARML8
                    CASE(2)
! C(sp2)-C(sp3)-C(sp2)-C(sp2) torsion angle
                      MMtorsion(iTOR)%V1 = V1_TORSION(10)
                      MMtorsion(iTOR)%V2 = V2_TORSION(10)
                      MMtorsion(iTOR)%V3 = V3_TORSION(10)
                    CASE DEFAULT
                      write(UNIout,'(a)')'CCCC_TOR_PARM> Torsion not supported.'
                  end SELECT
                CASE DEFAULT
                  write(UNIout,'(a)')'CCCC_TOR_PARM> Torsion not supported.'
                  call GETTOR_PARMS_WILD(iTOR, type_i, type_j, type_k, type_l)
              end SELECT
            CASE(2)
              SELECT CASE(type_k)         ! TOR_PARMK6
                CASE(1)
                  SELECT CASE(type_l)      ! TOR_PARML9
                    CASE(1)
! C(sp2)-C(sp2)-C(sp3)-C(sp3) torsion angle
                      MMtorsion(iTOR)%V1 = V1_TORSION(8)
                      MMtorsion(iTOR)%V2 = V2_TORSION(8)
                      MMtorsion(iTOR)%V3 = V3_TORSION(8)
                    CASE DEFAULT
                      write(UNIout,'(a)')'CCCC_TOR_PARM> Torsion not supported.'
                  end SELECT
                CASE(2)
                  SELECT CASE(type_l)                ! TOR_PARML10
                    CASE(2)
! C(sp2)-C(sp2)-C(sp2)-C(sp2) torsion angle
                      MMtorsion(iTOR)%V1 = V1_TORSION(21)
                      MMtorsion(iTOR)%V2 = V2_TORSION(21)
                      MMtorsion(iTOR)%V3 = V3_TORSION(21)
                    CASE DEFAULT
                      write(UNIout,'(a)')'CCCC_TOR_PARM> Torsion not supported.'
                  end SELECT
                CASE DEFAULT
                  write(UNIout,'(a)')'CCCC_TOR_PARM> Torsion not supported.'
                  call GETTOR_PARMS_WILD(iTOR, type_i, type_j, type_k, type_l)
              end SELECT
            CASE(37)
              SELECT CASE(type_k)              ! TOR_PARMK7
                CASE(37)
                  SELECT CASE(type_l)        ! TOR_PARML11
                    CASE(37)
! C(sp2)-C(aromatic)-C(aromatic)-C(aromatic) torsion angle, bt_ij and bt_jk are aromatic
                      MMtorsion(iTOR)%V1 = V1_TORSION(28)
                      MMtorsion(iTOR)%V2 = V2_TORSION(28)
                      MMtorsion(iTOR)%V3 = V3_TORSION(28)
                    CASE DEFAULT
                      write(UNIout,'(a)')'CCCC_TOR_PARM> Torsion not supported.'
                  end SELECT
                CASE DEFAULT
                  write(UNIout,'(a)')'CCCC_TOR_PARM> Torsion not supported.'
                  call GETTOR_PARMS_WILD(iTOR, type_i, type_j, type_k, type_l)
              end SELECT
            CASE DEFAULT
              write(UNIout,'(a)')'CCCC_TOR_PARM> Torsion not supported.'
              call GETTOR_PARMS_WILD(iTOR, type_i, type_j, type_k, type_l)
          end SELECT
        CASE(20)
          SELECT CASE(type_j)                ! TOR_PARMJ3
            CASE(20)
              SELECT CASE(type_k)                ! TOR_PARMK8
                CASE(20)
                  SELECT CASE(type_l)      ! TOR_PARML12
                    CASE(1)
! C(4M ring)-C(4M ring)-C(4M ring)-C(sp3) torsion angle
                      MMtorsion(iTOR)%V1 = V1_TORSION(35)
                      MMtorsion(iTOR)%V2 = V2_TORSION(35)
                      MMtorsion(iTOR)%V3 = V3_TORSION(35)
                    CASE(20)
! *-C(4M ring)-C(4M ring)-* torsion angle
                      MMtorsion(iTOR)%V1 = V1_TORSION(49)
                      MMtorsion(iTOR)%V2 = V2_TORSION(49)
                      MMtorsion(iTOR)%V3 = V3_TORSION(49)
                    CASE DEFAULT
                      write(UNIout,'(a)')'CCCC_TOR_PARM> Torsion not supported.'
                  end SELECT
                CASE DEFAULT
                  write(UNIout,'(a)')'CCCC_TOR_PARM> Torsion not supported.'
                  call GETTOR_PARMS_WILD(iTOR, type_i, type_j, type_k, type_l)
              end SELECT
            CASE DEFAULT
              write(UNIout,'(a)')'CCCC_TOR_PARM> Torsion not supported.'
              call GETTOR_PARMS_WILD(iTOR, type_i, type_j, type_k, type_l)
          end SELECT
        CASE(37)
          SELECT CASE(type_j)                 ! TOR_PARMJ4
            CASE(37)
              SELECT CASE(type_k)              ! TOR_PARMK9
                CASE(1)
                  SELECT CASE(type_l)         ! TOR_PARML13
                    CASE(1)
! C(aromatic)-C(aromatic)-C(sp3)-C(sp3) torsion angle
                      MMtorsion(iTOR)%V1 = V1_TORSION(16)
                      MMtorsion(iTOR)%V2 = V2_TORSION(16)
                      MMtorsion(iTOR)%V3 = V3_TORSION(16)
                    CASE DEFAULT
                      write(UNIout,'(a)')'CCCC_TOR_PARM> Torsion not supported.'
                  end SELECT
                CASE(37)
                  SELECT CASE(type_l)    ! TOR_PARML14
                    CASE(1)
! C(aromatic)-C(aromatic)-C(aromatic)-C(sp3) torsion angle
                      MMtorsion(iTOR)%V1 = V1_TORSION(26)
                      MMtorsion(iTOR)%V2 = V2_TORSION(26)
                      MMtorsion(iTOR)%V3 = V3_TORSION(26)
                    CASE(2)
! C(aromatic)-C(aromatic)-C(aromatic)-C(sp2) torsion angle, bt_ij and bt_jk are aromatic
                      MMtorsion(iTOR)%V1 = V1_TORSION(28)
                      MMtorsion(iTOR)%V2 = V2_TORSION(28)
                      MMtorsion(iTOR)%V3 = V3_TORSION(28)
                    CASE(37)
! C(aromatic)-C(aromatic)-C(aromatic)-C(aromatic) torsion angle, bt_ij and bt_jk are aromatic
                      MMtorsion(iTOR)%V1 = V1_TORSION(31)
                      MMtorsion(iTOR)%V2 = V2_TORSION(31)
                      MMtorsion(iTOR)%V3 = V3_TORSION(31)
                    CASE DEFAULT
                      write(UNIout,'(a)')'CCCC_TOR_PARM> Torsion not supported.'
                  end SELECT
                CASE DEFAULT
                  write(UNIout,'(a)')'CCCC_TOR_PARM> Torsion not supported.'
                  call GETTOR_PARMS_WILD(iTOR, type_i, type_j, type_k, type_l)
              end SELECT
            CASE DEFAULT
              write(UNIout,'(a)')'CCCC_TOR_PARM> Torsion not supported.'
              call GETTOR_PARMS_WILD(iTOR, type_i, type_j, type_k, type_l)
          end SELECT
        CASE DEFAULT
          write(UNIout,'(a)')'CCCC_TOR_PARM> Torsion not supported. Defaulting to wild types.'
          call GETTOR_PARMS_WILD(iTOR, type_i, type_j, type_k, type_l)
      end SELECT

! End of routine GETTOR_PARMS_CCCC
      return
      END
      subroutine GETTOR_PARMS_WILD(iTOR, type_i, type_j, type_k, type_l)
!*******************************************************************************
! Date last modified: 25 August, 1999                              Version 1.0 *
! Author: Michelle Shaw                                                        *
! Description: Get torsional parameters v1, v2, and v3 for torsions involving  *
!              wildcards.                                                      *
!*******************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE tor_params
      USE mm_parameters

      implicit none
!
! Input scalars:
      integer iTOR, type_i, type_j, type_k, type_l
!
! Begin:
! The torsions that are not exactly defined are dealt with here.  If all other cases for torsions
!   fail, this routine is called and the case number is compared with the cases defined by the middle
!   bond type and wild cards signifying any atom can be attached.  If the torsion is not found here,
!   then the MM package does not support it.
      TOR_PARMJC:SELECT CASE(type_j)
        CASE(1) TOR_PARMJC
          TOR_PARMK1:SELECT CASE(type_k)
            CASE(20) TOR_PARMK1
! *-C(sp3)-C(4-membered ring)-* torsion angle
              MMtorsion(iTOR)%V1 = V1_TORSION(38)
              MMtorsion(iTOR)%V2 = V2_TORSION(38)
              MMtorsion(iTOR)%V3 = V3_TORSION(38)
            CASE(22) TOR_PARMK1
! *-C(sp3)-C(3-membered ring)-* torsion angle
              MMtorsion(iTOR)%V1 = V1_TORSION(39)
              MMtorsion(iTOR)%V2 = V2_TORSION(39)
              MMtorsion(iTOR)%V3 = V3_TORSION(39)
            CASE DEFAULT TOR_PARMK1
              write(UNIout,'(a)')'GETTOR_PARMS_WILD> Torsion not supported.'
          end SELECT TOR_PARMK1
        CASE(2) TOR_PARMJC
          TOR_PARMK2:SELECT CASE(type_k)
            CASE(20) TOR_PARMK2
! *-C(sp2)-C(4-membered ring)-* torsion angle
              MMtorsion(iTOR)%V1 = V1_TORSION(40)
              MMtorsion(iTOR)%V2 = V2_TORSION(40)
              MMtorsion(iTOR)%V3 = V3_TORSION(40)
            CASE(22) TOR_PARMK2
! *-C(sp2)-C(3-membered ring)-* torsion angle
              MMtorsion(iTOR)%V1 = V1_TORSION(41)
              MMtorsion(iTOR)%V2 = V2_TORSION(41)
              MMtorsion(iTOR)%V3 = V3_TORSION(41)
            CASE DEFAULT TOR_PARMK2
              write(UNIout,'(a)')'GETTOR_PARMS_WILD> Torsion not supported.'
          end SELECT TOR_PARMK2
        CASE(20) TOR_PARMJC
          TOR_PARMK3:SELECT CASE(type_k)
            CASE(1) TOR_PARMK3
! *-C(4-membered ring)-C(sp3)-* torsion angle
              MMtorsion(iTOR)%V1 = V1_TORSION(38)
              MMtorsion(iTOR)%V2 = V2_TORSION(38)
              MMtorsion(iTOR)%V3 = V3_TORSION(38)
            CASE(2) TOR_PARMK3
! *-C(4-membered ring)-C(sp2)-* torsion angle
              MMtorsion(iTOR)%V1 = V1_TORSION(40)
              MMtorsion(iTOR)%V2 = V2_TORSION(40)
              MMtorsion(iTOR)%V3 = V3_TORSION(40)
            CASE(20) TOR_PARMK3
! *-C(4-membered ring)-C(4-membered ring)-* torsion angle
              MMtorsion(iTOR)%V1 = V1_TORSION(42)
              MMtorsion(iTOR)%V2 = V2_TORSION(42)
              MMtorsion(iTOR)%V3 = V3_TORSION(42)
            CASE(22) TOR_PARMK3
! *-C(4-membered ring)-C(3-membered ring)-* torsion angle
              MMtorsion(iTOR)%V1 = V1_TORSION(43)
              MMtorsion(iTOR)%V2 = V2_TORSION(43)
              MMtorsion(iTOR)%V3 = V3_TORSION(43)
            CASE(30) TOR_PARMK3
              TOR_PARML1:SELECT CASE(type_l)
                CASE(20) TOR_PARML1
! *-C(4M ring)-C(olefinic 4M ring)-C(olefinic 4M ring) torsion angle
                  MMtorsion(iTOR)%V1 = V1_TORSION(45)
                  MMtorsion(iTOR)%V2 = V2_TORSION(45)
                  MMtorsion(iTOR)%V3 = V3_TORSION(45)
                CASE DEFAULT TOR_PARML1
! *-C(4-membered ring)-C(4-membered ring, olefinic)-* torsion angle
                  MMtorsion(iTOR)%V1 = V1_TORSION(44)
                  MMtorsion(iTOR)%V2 = V2_TORSION(44)
                  MMtorsion(iTOR)%V3 = V3_TORSION(44)
              end SELECT TOR_PARML1
            CASE DEFAULT TOR_PARMK3
              write(UNIout,'(a)')'GETTOR_PARMS_WILD> Torsion not supported.'
          end SELECT TOR_PARMK3
        CASE(22) TOR_PARMJC
          TOR_PARMK4:SELECT CASE(type_k)
            CASE(1) TOR_PARMK4
! *-C(3-membered ring)-C(sp3)-* torsion angle
              MMtorsion(iTOR)%V1 = V1_TORSION(39)
              MMtorsion(iTOR)%V2 = V2_TORSION(39)
              MMtorsion(iTOR)%V3 = V3_TORSION(39)
            CASE(2) TOR_PARMK4
! *-C(3-membered ring)-C(sp2)-* torsion angle
              MMtorsion(iTOR)%V1 = V1_TORSION(41)
              MMtorsion(iTOR)%V2 = V2_TORSION(41)
              MMtorsion(iTOR)%V3 = V3_TORSION(41)
            CASE(20) TOR_PARMK4
! *-C(3-membered ring)-C(4-membered ring)-* torsion angle
              MMtorsion(iTOR)%V1 = V1_TORSION(43)
              MMtorsion(iTOR)%V2 = V2_TORSION(43)
              MMtorsion(iTOR)%V3 = V3_TORSION(43)
            CASE(22) TOR_PARMK4
! *-C(3-membered ring)-C(3-membered ring)-* torsion angle
              MMtorsion(iTOR)%V1 = V1_TORSION(46)
              MMtorsion(iTOR)%V2 = V2_TORSION(46)
              MMtorsion(iTOR)%V3 = V3_TORSION(46)
            CASE(30) TOR_PARMK4
! *-C(3-membered ring)-C(4-membered ring, olefinic)-* torsion angle
              MMtorsion(iTOR)%V1 = V1_TORSION(47)
              MMtorsion(iTOR)%V2 = V2_TORSION(47)
              MMtorsion(iTOR)%V3 = V3_TORSION(47)
            CASE DEFAULT TOR_PARMK4
              write(UNIout,'(a)')'GETTOR_PARMS_WILD> Torsion not supported.'
          end SELECT TOR_PARMK4
        CASE(30) TOR_PARMJC
          TOR_PARMK5:SELECT CASE(type_k)
            CASE(20) TOR_PARMK5
              TOR_PARMI2:SELECT CASE(type_i)
                CASE(20) TOR_PARMI2
! C(olefinic 4M ring)-C(olefinic 4M ring)-C(4M ring)-* torsion angle
                  MMtorsion(iTOR)%V1 = V1_TORSION(45)
                  MMtorsion(iTOR)%V2 = V2_TORSION(45)
                  MMtorsion(iTOR)%V3 = V3_TORSION(45)
                CASE DEFAULT TOR_PARMI2
! *-C(olefinic 4M ring)-C(4M ring)-* torsion angle
                  MMtorsion(iTOR)%V1 = V1_TORSION(44)
                  MMtorsion(iTOR)%V2 = V2_TORSION(44)
                  MMtorsion(iTOR)%V3 = V3_TORSION(44)
              end SELECT TOR_PARMI2
            CASE(22) TOR_PARMK5
! *-C(4-membered ring, olefinic)-C(3-membered ring)-* torsion angle
              MMtorsion(iTOR)%V1 = V1_TORSION(47)
              MMtorsion(iTOR)%V2 = V2_TORSION(47)
              MMtorsion(iTOR)%V3 = V3_TORSION(47)
            CASE(30) TOR_PARMK5
! *-C(4-membered ring, olefinic)-C(4-membered ring, olefinic)-* torsion angle
              MMtorsion(iTOR)%V1 = V1_TORSION(48)
              MMtorsion(iTOR)%V2 = V2_TORSION(48)
              MMtorsion(iTOR)%V3 = V3_TORSION(48)
            CASE DEFAULT TOR_PARMK5
              write(UNIout,'(a)')'GETTOR_PARMS_WILD> Torsion not supported.'
          end SELECT TOR_PARMK5
        CASE DEFAULT TOR_PARMJC
          write(UNIout,'(a)')'Torsion angle (or atoms) not supported.'
          MMtorsion(iTOR)%V1 = ZERO
          MMtorsion(iTOR)%V2 = ZERO
          MMtorsion(iTOR)%V3 = ZERO
      end SELECT TOR_PARMJC

! End of routine GETTOR_PARMS_WILD
      return
      END
      subroutine GETOOPBEND_HCCH(type_j, & !type for atom j
                                 type_k, & !type for atom k
                                 fc) ! out-of-plane force constant for the case "case_oopbend"
!*******************************************************************************
! Date last modified: 8 September, 1999                            Version 1.0 *
! Author: Michelle Shaw                                                        *
! Description: Obtain out-of-plane bend force constant for a HCCH (or CCHH,    *
!              HHCC) oop bend                                                  *
!*******************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE oopbend_fc

      implicit none
!
! Input scalars:
      integer type_j, type_k
      logical J_IS_AROM

! Output scalar:
      double precision fc
!
! Begin:
      FC_SELECTJ:SELECT CASE (type_j)
        CASE(2) FC_SELECTJ
          FC_SELECTK:SELECT CASE(type_k)
            CASE(2) FC_SELECTK
! *-C(sp2)-C(sp2)-H2 out-of-plane bend, atom k is C(sp2)
              fc = OOPBEND_FORCE_CONST(6)
            CASE(5) FC_SELECTK
! *-C(sp2)-C(sp2)-H2 out-of-plane bend, atom k is H
              fc = OOPBEND_FORCE_CONST(6)
            CASE DEFAULT FC_SELECTK
! *-C(sp2)-*2 out-of-plane bend, atom k is wild (any type)
              fc = OOPBEND_FORCE_CONST(12)
          end SELECT FC_SELECTK
        CASE(30) FC_SELECTJ
! *-C(4-membered ring, olefinic)-*2 out-of-plane bend
          fc = OOPBEND_FORCE_CONST(13)
        CASE(37) FC_SELECTJ
! *-C(aromatic)-*2 out-of-plane bend
          fc = OOPBEND_FORCE_CONST(14)
        CASE DEFAULT FC_SELECTJ
          write(UNIout,'(a)')'GETOOPBEND_HCCH> Out-of-plane bend not supported.'
      end SELECT FC_SELECTJ

! End of routine GETOOPBEND_HCCH
      return
      END

      subroutine GETOOPBEND_HCCC(type_i, & !atom type for atom i
                                 type_j, & !atom type for atom j
                                 type_k, & !atom type for atom k
                                 type_l, & !atom type for atom l
                                 fc) !out-of-plane bend force constant
!*******************************************************************************
! Date last modified: 8 September, 1999                            Version 1.0 *
! Author: Michelle Shaw                                                        *
! Description: Obtain out-of-plane bend force constant for a HCCC (or CCHC,    *
!              CCCH) oop bend                                                  *
!*******************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE oopbend_fc

      implicit none
!
! Input scalars:
      integer type_i, type_j, type_k, type_l

! Output scalar:
      double precision fc
!
! Begin:
      FC_SELECTI:SELECT CASE (type_i)
        CASE(1) FC_SELECTI
          FC_SELECTJ1:SELECT CASE(type_j)
            CASE(2) FC_SELECTJ1
              FC_SELECTK1:SELECT CASE(type_k)
                CASE(2) FC_SELECTK1
                  FC_SELECTL1:SELECT CASE(type_l)
                    CASE(5) FC_SELECTL1
! I is C(sp3), J, K are C(sp2), L is H
                      fc = OOPBEND_FORCE_CONST(3)
                    CASE DEFAULT FC_SELECTL1
                      write(UNIout,'(a)')'GETOOPBEND_HCCC> Out-of-plane bend not supported.'
                  end SELECT FC_SELECTL1
                CASE(5) FC_SELECTK1
                  FC_SELECTL2:SELECT CASE(type_l)
                    CASE(1) FC_SELECTL2
! I is C(sp3), K is H, L is C(sp2)
                      fc = OOPBEND_FORCE_CONST(3)
                    CASE DEFAULT FC_SELECTL2
                  end SELECT FC_SELECTL2
                CASE DEFAULT FC_SELECTK1
                  write(UNIout,'(a)')'GETOOPBEND_HCCC> Out-of-plane bend not supported.'
              end SELECT FC_SELECTK1
            CASE DEFAULT FC_SELECTJ1
              write(UNIout,'(a)')'GETOOPBEND_HCCC> Out-of-plane bend not supported.'
          end SELECT FC_SELECTJ1
        CASE(2) FC_SELECTI
          FC_SELECTJ2:SELECT CASE(type_j)
            CASE(2) FC_SELECTJ2
              FC_SELECTK2:SELECT CASE(type_k)
                CASE(1) FC_SELECTK2
                  FC_SELECTL3:SELECT CASE(type_l)
                    CASE(5) FC_SELECTL3
! I is C(sp2), J is C(sp2), K is C(sp3), L is H
                      fc = OOPBEND_FORCE_CONST(3)
                    CASE DEFAULT FC_SELECTL3
                      write(UNIout,'(a)')'GETOOPBEND_HCCC> Out-of-plane bend not supported.'
                  end SELECT FC_SELECTL3
                CASE(2) FC_SELECTK2
                  FC_SELECTL4:SELECT CASE(type_l)
                    CASE(5) FC_SELECTL4
! I, J, K are C(sp2), L is H
                      fc = OOPBEND_FORCE_CONST(5)
                    CASE DEFAULT FC_SELECTL4
                      write(UNIout,'(a)')'GETOOPBEND_HCCC> Out-of-plane bend not supported.'
                  end SELECT FC_SELECTL4
                CASE(5) FC_SELECTK2
                  FC_SELECTL5:SELECT CASE(type_l)
                    CASE(1) FC_SELECTL5
! I is C(sp2), J is C(sp2), K is H, L is C(sp3)
                      fc = OOPBEND_FORCE_CONST(3)
                    CASE(2) FC_SELECTL5
! I, J, L are C(sp2), K is H
                      fc = OOPBEND_FORCE_CONST(5)
                    CASE(37) FC_SELECTL5
! L is C(aromatic), I, J are C(sp2), K is H
                      fc = OOPBEND_FORCE_CONST(7)
                    CASE DEFAULT FC_SELECTL5
                      write(UNIout,'(a)')'GETOOPBEND_HCCC> Out-of-plane bend not supported.'
                  end SELECT FC_SELECTL5
                CASE(37) FC_SELECTK2
                  FC_SELECTL6:SELECT CASE(type_l)
                    CASE(5) FC_SELECTL6
! I, J are C(sp2), K is C(aromatic), L is H
                      fc = OOPBEND_FORCE_CONST(7)
                    CASE DEFAULT FC_SELECTL6
                      write(UNIout,'(a)')'GETOOPBEND_HCCC> Out-of-plane bend not supported.'
                  end SELECT FC_SELECTL6
                CASE DEFAULT FC_SELECTK2
                  write(UNIout,'(a)')'GETOOPBEND_HCCC> Out-of-plane bend not supported.'
              end SELECT FC_SELECTK2
            CASE DEFAULT FC_SELECTJ2
              write(UNIout,'(a)')'GETOOPBEND_HCCC> Out-of-plane bend not supported.'
          end SELECT FC_SELECTJ2
        CASE(5) FC_SELECTI
          FC_SELECTJ3:SELECT CASE(type_j)
            CASE(2) FC_SELECTJ3
              FC_SELECTK3:SELECT CASE(type_k)
                CASE(1) FC_SELECTK3
                  FC_SELECTL7:SELECT CASE(type_l)
                    CASE(2) FC_SELECTL7
! I is H, J is C(sp2), K is C(sp3), L is C(sp2)
                      fc = OOPBEND_FORCE_CONST(3)
                    CASE DEFAULT FC_SELECTL7
                      write(UNIout,'(a)')'GETOOPBEND_HCCC> Out-of-plane bend not supported.'
                  end SELECT FC_SELECTL7
                CASE(2) FC_SELECTK3
                  FC_SELECTL8:SELECT CASE(type_l)
                    CASE(1) FC_SELECTL8
! I is H, J, K are C(sp2), L is C(sp3)
                      fc = OOPBEND_FORCE_CONST(3)
                    CASE(2) FC_SELECTL8
! J, K, L are C(sp2), I is H
                      fc = OOPBEND_FORCE_CONST(5)
                    CASE(37) FC_SELECTL8
! L is C(aromatic), J, K are C(sp2), I is H
                      fc = OOPBEND_FORCE_CONST(7)
                    CASE DEFAULT FC_SELECTL8
                      write(UNIout,'(a)')'GETOOPBEND_HCCC> Out-of-plane bend not supported.'
                  end SELECT FC_SELECTL8
                CASE DEFAULT FC_SELECTK3
                  write(UNIout,'(a)')'GETOOPBEND_HCCC> Out-of-plane bend not supported.'
              end SELECT FC_SELECTK3
            CASE(30) FC_SELECTJ3
              FC_SELECTK4:SELECT CASE(type_k)
                CASE(20) FC_SELECTK4
                  FC_SELECTL9:SELECT CASE(type_l)
                    CASE(30) FC_SELECTL9
! I is H, J is C(4M ring, olefinic), K is C(4M ring), L is C(4M ring, olefinic)
                      fc = OOPBEND_FORCE_CONST(8)
                    CASE DEFAULT FC_SELECTL9
                      write(UNIout,'(a)')'GETOOPBEND_HCCC> Out-of-plane bend not supported.'
                  end SELECT FC_SELECTL9
                CASE(30) FC_SELECTK4
                  FC_SELECTL10:SELECT CASE(type_l)
                    CASE(20) FC_SELECTL10
! I is H, J is C(4-M ring, olefinic), K is C(4-M ring, olefinic), L is C(4-M ring)
                      fc = OOPBEND_FORCE_CONST(8)
                    CASE DEFAULT FC_SELECTL10
                      write(UNIout,'(a)')'GETOOPBEND_HCCC> Out-of-plane bend not supported.'
                  end SELECT FC_SELECTL10
                CASE DEFAULT FC_SELECTK4
                  write(UNIout,'(a)')'GETOOPBEND_HCCC> Out-of-plane bend not supported.'
              end SELECT FC_SELECTK4
            CASE(37) FC_SELECTJ3
              FC_SELECTK5:SELECT CASE(type_k)
                CASE(37) FC_SELECTK5
                  FC_SELECTL11:SELECT CASE(type_l)
                    CASE(37) FC_SELECTL11
! J, K, L are C(aromatic) and I is H
                      fc = OOPBEND_FORCE_CONST(11)
                    CASE DEFAULT FC_SELECTL11
                      write(UNIout,'(a)')'GETOOPBEND_HCCC> Out-of-plane bend not supported.'
                  end SELECT FC_SELECTL11
                CASE DEFAULT FC_SELECTK5
                  write(UNIout,'(a)')'GETOOPBEND_HCCC> Out-of-plane bend not supported.'
              end SELECT FC_SELECTK5
            CASE DEFAULT FC_SELECTJ3
              write(UNIout,'(a)')'GETOOPBEND_HCCC> Out-of-plane bend not supported.'
          end SELECT FC_SELECTJ3
        CASE(20) FC_SELECTI
          FC_SELECTJ4:SELECT CASE(type_j)
            CASE(30) FC_SELECTJ4
              FC_SELECTK6:SELECT CASE(type_k)
                CASE(5) FC_SELECTK6
                  FC_SELECTL12:SELECT CASE(type_l)
                    CASE(30) FC_SELECTL12
! I is C(4-M ring), J is C(4-M ring, olefinic), K is H, L is C(4-M ring, olefinic)
                      fc = OOPBEND_FORCE_CONST(8)
                    CASE DEFAULT FC_SELECTL12
                      write(UNIout,'(a)')'GETOOPBEND_HCCC> Out-of-plane bend not supported.'
                  end SELECT FC_SELECTL12
                CASE(30) FC_SELECTK6
                  FC_SELECTL13:SELECT CASE(type_l)
                    CASE(5) FC_SELECTL13
! I is C(4-M ring), J is C(4-M ring, olefinic), K is C(4-M ring, olefinic), L is H
                      fc = OOPBEND_FORCE_CONST(8)
                    CASE DEFAULT FC_SELECTL13
                      write(UNIout,'(a)')'GETOOPBEND_HCCC> Out-of-plane bend not supported.'
                  end SELECT FC_SELECTL13
                CASE DEFAULT FC_SELECTK6
                  write(UNIout,'(a)')'GETOOPBEND_HCCC> Out-of-plane bend not supported.'
              end SELECT FC_SELECTK6
            CASE DEFAULT FC_SELECTJ4
              write(UNIout,'(a)')'GETOOPBEND_HCCC> Out-of-plane bend not supported.'
          end SELECT FC_SELECTJ4
        CASE(30) FC_SELECTI
          FC_SELECTJ5:SELECT CASE(type_j)
            CASE(30) FC_SELECTJ5
              FC_SELECTK7:SELECT CASE(type_k)
                CASE(20) FC_SELECTK7
                  FC_SELECTL14:SELECT CASE(type_l)
                    CASE(5) FC_SELECTL14
! I is C(4-M ring, olefinic), J is C(4-M ring, olefinic), K is C(4-M ring), L is H
                      fc = OOPBEND_FORCE_CONST(8)
                    CASE DEFAULT FC_SELECTL14
                      write(UNIout,'(a)')'GETOOPBEND_HCCC> Out-of-plane bend not supported.'
                  end SELECT FC_SELECTL14
                CASE DEFAULT FC_SELECTK7
                  write(UNIout,'(a)')'GETOOPBEND_HCCC> Out-of-plane bend not supported.'
              end SELECT FC_SELECTK7
            CASE DEFAULT FC_SELECTJ5
              write(UNIout,'(a)')'GETOOPBEND_HCCC> Out-of-plane bend not supported.'
          end SELECT FC_SELECTJ5
        CASE(37) FC_SELECTI
          FC_SELECTJ6:SELECT CASE(type_j)
            CASE(2) FC_SELECTJ6
              FC_SELECTK8:SELECT CASE(type_k)
                CASE(2) FC_SELECTK8
                  FC_SELECTL15:SELECT CASE(type_l)
                    CASE(5) FC_SELECTL15
! I is C(aromatic), K is C(sp2), L is H
                      fc = OOPBEND_FORCE_CONST(7)
                    CASE DEFAULT FC_SELECTL15
                      write(UNIout,'(a)')'GETOOPBEND_HCCC> Out-of-plane bend not supported.'
                  end SELECT FC_SELECTL15
                CASE(5) FC_SELECTK8
                  FC_SELECTL16:SELECT CASE(type_l)
                    CASE(2) FC_SELECTL16
! I is C(aromatic), L is C(sp2), K is H
                      fc = OOPBEND_FORCE_CONST(7)
                    CASE DEFAULT FC_SELECTL16
                      write(UNIout,'(a)')'GETOOPBEND_HCCC> Out-of-plane bend not supported.'
                  end SELECT FC_SELECTL16
                CASE DEFAULT FC_SELECTK8
                  write(UNIout,'(a)')'GETOOPBEND_HCCC> Out-of-plane bend not supported.'
              end SELECT FC_SELECTK8
            CASE(37) FC_SELECTJ6
              FC_SELECTK9:SELECT CASE(type_k)
                CASE(37) FC_SELECTK9
                  FC_SELECTL17:SELECT CASE(type_l)
                    CASE(5) FC_SELECTL17
! I, J, K are C(aromatic), and L is H
                      fc = OOPBEND_FORCE_CONST(11)
                    CASE DEFAULT FC_SELECTL17
                      write(UNIout,'(a)')'GETOOPBEND_HCCC> Out-of-plane bend not supported.'
                  end SELECT FC_SELECTL17
                CASE(5) FC_SELECTK9
                  FC_SELECTL18:SELECT CASE(type_l)
                    CASE(37) FC_SELECTL18
! I, J, L are C(aromatic) and K is H
                      fc = OOPBEND_FORCE_CONST(11)
                    CASE DEFAULT FC_SELECTL18
                      write(UNIout,'(a)')'GETOOPBEND_HCCC> Out-of-plane bend not supported.'
                  end SELECT FC_SELECTL18
                CASE DEFAULT FC_SELECTK9
                  write(UNIout,'(a)')'GETOOPBEND_HCCC> Out-of-plane bend not supported.'
              end SELECT FC_SELECTK9
            CASE DEFAULT FC_SELECTJ6
              write(UNIout,'(a)')'GETOOPBEND_HCCC> Out-of-plane bend not supported.'
          end SELECT FC_SELECTJ6
        CASE DEFAULT FC_SELECTI
          write(UNIout,'(a)')'GETOOPBEND_HCCC> Defaulting to wild card types..'
          if(type_j.eq.2) then
! *-C(sp2)-*2 out-of-plane bend
            fc = OOPBEND_FORCE_CONST(12)
          else if(type_j.eq.30) then
! *-(4-membered ring, olefinic)-*2 out-of-plane bend
            fc = OOPBEND_FORCE_CONST(13)
          else if(type_j.eq.37) then
! *-C(aromatic)-*2 out-of-plane bend
            fc = OOPBEND_FORCE_CONST(14)
          else
            write(UNIout,'(a)')'GETOOPBEND_HCCC> Out-of-plane bend not supported.'
          end if
      end SELECT FC_SELECTI

! End of routine GETOOPBEND_HCCC
      return
      END
      subroutine GETOOPBEND_CCCC(type_i, & !atom type for atom i
                                 type_j, & !atom type for atom j
                                 type_k, & !atom type for atom k
                                 type_l, & !atom type for atom l
                                 fc) ! out-of-plane force constant
!*******************************************************************************
! Date last modified: 8 September, 1999                            Version 1.0 *
! Author: Michelle Shaw                                                        *
! Description: Obtain out-of-plane bend force constant for a CCCC oop bend     *
!*******************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE oopbend_fc

      implicit none
!
! Input scalars:
      integer type_i, type_j, type_k, type_l

! Output scalar:
      double precision fc
!
! Begin:
      FC_SELECTI:SELECT CASE (type_i)
        CASE(1) FC_SELECTI
          FC_SELECTJ1:SELECT CASE(type_j)
            CASE(2) FC_SELECTJ1
              FC_SELECTK1:SELECT CASE(type_k)
                CASE(1) FC_SELECTK1
                  FC_SELECTL1:SELECT CASE(type_l)
                    CASE(2) FC_SELECTL1
! I, K are C(sp3), J, L are C(sp2)
                      fc = OOPBEND_FORCE_CONST(1)
                    CASE DEFAULT FC_SELECTL1
                      write(UNIout,'(a)')'GETOOPBEND_CCCC> Out-of-plane bend not supported.'
                  end SELECT FC_SELECTL1
                CASE(2) FC_SELECTK1
                  FC_SELECTL2:SELECT CASE(type_l)
                    CASE(1) FC_SELECTL2
! I, L are C(sp3), J, K are C(sp2)
                      fc = OOPBEND_FORCE_CONST(1)
                    CASE(2) FC_SELECTL2
! I is C(sp3), J, K, L are C(sp2)
                      fc = OOPBEND_FORCE_CONST(2)
                    CASE(37) FC_SELECTL2
! I is C(sp3), J, K are C(sp2), L is C(aromatic)
                      fc = OOPBEND_FORCE_CONST(4)
                    CASE DEFAULT FC_SELECTL2
                  end SELECT FC_SELECTL2
                CASE(37) FC_SELECTK1
                  FC_SELECTL3:SELECT CASE(type_l)
                    CASE(2) FC_SELECTL3
! I is C(sp3), J, L are C(sp2), K is C(aromatic)
                      fc = OOPBEND_FORCE_CONST(4)
                    CASE DEFAULT FC_SELECTL3
                      write(UNIout,'(a)')'GETOOPBEND_CCCC> Out-of-plane bend not supported.'
                  end SELECT FC_SELECTL3
                CASE DEFAULT FC_SELECTK1
                  write(UNIout,'(a)')'GETOOPBEND_CCCC> Out-of-plane bend not supported.'
              end SELECT FC_SELECTK1
            CASE(37) FC_SELECTJ1
              FC_SELECTK2:SELECT CASE(type_k)
                CASE(37) FC_SELECTK2
                  FC_SELECTL4:SELECT CASE(type_l)
                    CASE(37) FC_SELECTL4
! I is C(sp3), J, K, L are C(aromatic)
                      fc = OOPBEND_FORCE_CONST(9)
                    CASE DEFAULT FC_SELECTL4
                      write(UNIout,'(a)')'GETOOPBEND_CCCC> Out-of-plane bend not supported.'
                  end SELECT FC_SELECTL4
                CASE DEFAULT FC_SELECTK2
                  write(UNIout,'(a)')'GETOOPBEND_CCCC> Out-of-plane bend not supported.'
              end SELECT FC_SELECTK2
            CASE DEFAULT FC_SELECTJ1
              write(UNIout,'(a)')'GETOOPBEND_CCCC> Out-of-plane bend not supported.'
          end SELECT FC_SELECTJ1
        CASE(2) FC_SELECTI
          FC_SELECTJ2:SELECT CASE(type_j)
            CASE(2) FC_SELECTJ2
              FC_SELECTK3:SELECT CASE(type_k)
                CASE(1) FC_SELECTK3
                  FC_SELECTL5:SELECT CASE(type_l)
                    CASE(1) FC_SELECTL5
! I, J are C(sp2), K, L are C(sp3)
                      fc = OOPBEND_FORCE_CONST(1)
                    CASE(2) FC_SELECTL5
! I, J, L are C(sp2), K is C(sp3)
                      fc = OOPBEND_FORCE_CONST(2)
                    CASE DEFAULT FC_SELECTL5
                      write(UNIout,'(a)')'GETOOPBEND_CCCC> Out-of-plane bend not supported.'
                  end SELECT FC_SELECTL5
                CASE(2) FC_SELECTK3
                  FC_SELECTL6:SELECT CASE(type_l)
                    CASE(1) FC_SELECTL6
! I, J, K are C(sp2), L is C(sp3)
                      fc = OOPBEND_FORCE_CONST(2)
                    CASE DEFAULT FC_SELECTL6
                      write(UNIout,'(a)')'GETOOPBEND_CCCC> Out-of-plane bend not supported.'
                  end SELECT FC_SELECTL6
                CASE DEFAULT FC_SELECTK3
                  write(UNIout,'(a)')'GETOOPBEND_CCCC> Out-of-plane bend not supported.'
              end SELECT FC_SELECTK3
            CASE(37) FC_SELECTJ2
              FC_SELECTK5:SELECT CASE(type_k)
                CASE(37) FC_SELECTK5
                  FC_SELECTL7:SELECT CASE(type_l)
                    CASE(37) FC_SELECTL7
! I is C(sp2), J, K, L are C(aromatic)
                      fc = OOPBEND_FORCE_CONST(10)
                    CASE DEFAULT FC_SELECTL7
                      write(UNIout,'(a)')'GETOOPBEND_CCCC> Out-of-plane bend not supported.'
                  end SELECT FC_SELECTL7
                CASE DEFAULT FC_SELECTK5
                  write(UNIout,'(a)')'GETOOPBEND_CCCC> Out-of-plane bend not supported.'
              end SELECT FC_SELECTK5
            CASE DEFAULT FC_SELECTJ2
              write(UNIout,'(a)')'GETOOPBEND_CCCC> Out-of-plane bend not supported.'
          end SELECT FC_SELECTJ2
        CASE(37) FC_SELECTI
          FC_SELECTJ3:SELECT CASE(type_j)
            CASE(2) FC_SELECTJ3
              FC_SELECTK6:SELECT CASE(type_k)
                CASE(2) FC_SELECTK6
                  FC_SELECTL8:SELECT CASE(type_l)
                    CASE(1) FC_SELECTL8
! I is C(aromatic), J, K are C(sp2), L is C(sp3)
                      fc = OOPBEND_FORCE_CONST(4)
                    CASE DEFAULT FC_SELECTL8
                      write(UNIout,'(a)')'GETOOPBEND_CCCC> Out-of-plane bend not supported.'
                  end SELECT FC_SELECTL8
                CASE(37) FC_SELECTK6
                  FC_SELECTL9:SELECT CASE(type_l)
                    CASE(37) FC_SELECTL9
! I, K, L are C(aromatic), J is C(sp2)
                      fc = OOPBEND_FORCE_CONST(10)
                    CASE DEFAULT FC_SELECTL9
                      write(UNIout,'(a)')'GETOOPBEND_CCCC> Out-of-plane bend not supported.'
                  end SELECT FC_SELECTL9
                CASE DEFAULT FC_SELECTK6
                  write(UNIout,'(a)')'GETOOPBEND_CCCC> Out-of-plane bend not supported.'
              end SELECT FC_SELECTK6
            CASE(37) FC_SELECTJ3
              FC_SELECTK7:SELECT CASE(type_k)
                CASE(1) FC_SELECTK7
                  FC_SELECTL10:SELECT CASE(type_l)
                    CASE(37) FC_SELECTL10
! I, J, L are C(aromatic), K is C(sp3)
                      fc = OOPBEND_FORCE_CONST(9)
                    CASE DEFAULT FC_SELECTL10
                      write(UNIout,'(a)')'GETOOPBEND_CCCC> Out-of-plane bend not supported.'
                  end SELECT FC_SELECTL10
                CASE(2) FC_SELECTK7
                  FC_SELECTL11:SELECT CASE(type_l)
                    CASE(37) FC_SELECTL11
! I, J, L are C(aromatic), K is C(sp2)
                      fc = OOPBEND_FORCE_CONST(9)
                    CASE DEFAULT FC_SELECTL11
                      write(UNIout,'(a)')'GETOOPBEND_CCCC> Out-of-plane bend not supported.'
                  end SELECT FC_SELECTL11
                CASE(37) FC_SELECTK7
                  FC_SELECTL12:SELECT CASE(type_l)
                    CASE(1) FC_SELECTL12
! I, J, K are C(aromatic), L is C(sp3)
                      fc = OOPBEND_FORCE_CONST(9)
                    CASE(2) FC_SELECTL12
! I, J, K are C(aromatic), L is C(sp2)
                      fc = OOPBEND_FORCE_CONST(10)
                    CASE DEFAULT FC_SELECTL12
                      write(UNIout,'(a)')'GETOOPBEND_CCCC> Out-of-plane bend not supported.'
                  end SELECT FC_SELECTL12
                CASE DEFAULT FC_SELECTK7
                  write(UNIout,'(a)')'GETOOPBEND_CCCC> Out-of-plane bend not supported.'
              end SELECT FC_SELECTK7
            CASE DEFAULT FC_SELECTJ3
              write(UNIout,'(a)')'GETOOPBEND_CCCC> Out-of-plane bend not supported.'
          end SELECT FC_SELECTJ3
        CASE DEFAULT FC_SELECTI
          write(UNIout,'(a)')'GETOOPBEND_CCCC> Defaulting to wild card types..'
          if(type_j.eq.2) then
! *-C(sp2)-*2 out-of-plane bend
            fc = OOPBEND_FORCE_CONST(12)
          else if(type_j.eq.30) then
! *-(4-membered ring, olefinic)-*2 out-of-plane bend
            fc = OOPBEND_FORCE_CONST(13)
          else if(type_j.eq.37) then
! *-C(aromatic)-*2 out-of-plane bend
            fc = OOPBEND_FORCE_CONST(14)
          else
            write(UNIout,'(a)')'GETOOPBEND_CCCC> Out-of-plane bend not supported.'
          end if
      end SELECT FC_SELECTI

! End of routine GETOOPBEND_CCCC
      return
      END
      subroutine GET_STB_FC_HCH(type_k, & !type for atom k
                                stb_fc_ijk, & !force constant for stretch-bend
                                stb_fc_kji) !force constant for stretch-bend
!**********************************************************************
! Date last modified: 19 October, 1999                    Version 1.0 *
! Author: Michelle Shaw                                               *
! Description: Get equilibrium angles and force program_constants for a HCH   *
!              stretch-bend.                                          *
!**********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE strbnd_fc

      implicit none
!
! Input scalars:
      integer type_k
      double precision stb_fc_ijk, stb_fc_kji
!
! Begin:
! This is where the parameters are selected for both equilibrium bond lengths and force program_constants.
      stb_fc_ijk = ZERO
      stb_fc_kji = ZERO

      SELECT CASE (type_k)      ! ANG_PARM
! H-C(sp3)-H angle
        CASE (1)
          stb_fc_ijk = STR_BND_FC_IJK(7)
          stb_fc_kji = STR_BND_FC_KJI(7)
! H-C(sp2)-H angle
        CASE (2)
          stb_fc_ijk = STR_BND_FC_IJK(20)
          stb_fc_kji = STR_BND_FC_KJI(20)
! H-C(4-membered ring)-H angle
        CASE (20)
          stb_fc_ijk = STR_BND_FC_IJK(24)
          stb_fc_kji = STR_BND_FC_KJI(24)
! H-C(3-membered ring)-H angle
        CASE (22)
          stb_fc_ijk = STR_BND_FC_IJK(31)
          stb_fc_kji = STR_BND_FC_KJI(31)
        CASE DEFAULT
! C, so can use *11 wild type (1=row in periodic table)
          write(UNIout,'(a)')'GET_STB_FC_HCH> switching to default force program_constants'
          stb_fc_ijk = STR_BND_FC_IJK(42)
          stb_fc_kji = STR_BND_FC_KJI(42)
      end SELECT

! End of function GET_STB_FC_HCH
      return
      END
      subroutine GET_STB_FC_HCC(type_j, & !atom type for atom j
                                type_k, & !atom type for atom k
                                bond_kj, & !bond type for bond k-j
                                stb_fc_ijk, & !force constant for stretch-bend
                                stb_fc_kji) !force constant for stretch-bend
!**********************************************************************
! Date last modified: 19 October, 1999                    Version 1.0 *
! Author: Michelle Shaw                                               *
! Description: Get equilibrium angles and force program_constants for a HCC   *
!              stretch-bend.                                          *
!**********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE strbnd_fc

      implicit none
!
! Input scalars:
      integer type_j, type_k, bond_kj
      double precision stb_fc_ijk, stb_fc_kji
!
! Begin:
! This is where the parameters are selected for both equilibrium angles and force program_constants.
      stb_fc_ijk = ZERO
      stb_fc_kji = ZERO

      SELECT CASE(type_k)              ! ANG_PARMK
        CASE(1)
          SELECT CASE(type_j)          ! ANG_PARMJ1
            CASE(1) !H-C(sp3)-C(sp3)
              stb_fc_kji = STR_BND_FC_IJK(3)
              stb_fc_ijk = STR_BND_FC_KJI(3)
            CASE(2) !H-C(sp3)-C(sp2)
              stb_fc_kji = STR_BND_FC_IJK(6)
              stb_fc_ijk = STR_BND_FC_KJI(6)
            CASE(20) !H-C(sp3)-C(4M ring)
              stb_fc_ijk = STR_BND_FC_IJK(8)
              stb_fc_kji = STR_BND_FC_KJI(8)
            CASE(22) !H-C(sp3)-C(3M ring)
              stb_fc_ijk = STR_BND_FC_IJK(9)
              stb_fc_kji = STR_BND_FC_KJI(9)
            CASE(30) !H-C(sp3)-C(olefinic 4M ring)
! Wild card type (*1*):
              stb_fc_ijk = STR_BND_FC_IJK(41)
              stb_fc_kji = STR_BND_FC_KJI(41)
            CASE(37) !H-C(sp3)-C(aromatic)
              stb_fc_ijk = STR_BND_FC_IJK(10)
              stb_fc_kji = STR_BND_FC_KJI(10)
            CASE DEFAULT
              write(UNIout,'(a)')'GET_STB_FC_HCC> jATOM: switching to default force program_constants.'
! Wild card type *11 (1=row in periodic table):
              stb_fc_ijk = STR_BND_FC_IJK(42)
              stb_fc_kji = STR_BND_FC_KJI(42)
          end SELECT
        CASE(2)
          SELECT CASE(type_j)            ! ANG_PARMJ2
            CASE(1) !H-C(sp2)-C(sp3)
              stb_fc_kji = STR_BND_FC_IJK(14)
              stb_fc_ijk = STR_BND_FC_KJI(14)
            CASE(2)
              if(bond_kj.eq.3) then
                stb_fc_kji = STR_BND_FC_IJK(17)
                stb_fc_ijk = STR_BND_FC_KJI(17)
              else
                stb_fc_kji = STR_BND_FC_IJK(18)
                stb_fc_ijk = STR_BND_FC_KJI(18)
              end if
            CASE(37)!H-C(sp2)-C(aromatic)
              stb_fc_ijk = STR_BND_FC_IJK(21)
              stb_fc_kji = STR_BND_FC_KJI(21)
            CASE DEFAULT
              write(UNIout,'(a)')'GET_STB_FC_HCC> jATOM: switching to default force program_constants.'
! Wild card type *11 (1=row in periodic table):
              stb_fc_ijk = STR_BND_FC_IJK(42)
              stb_fc_kji = STR_BND_FC_KJI(42)
          end SELECT
        CASE(20)
          SELECT CASE(type_j)            ! ANG_PARMJ4
            CASE(1) !H-C(4M ring)-C(sp3)
              stb_fc_kji = STR_BND_FC_IJK(22)
              stb_fc_ijk = STR_BND_FC_KJI(22)
            CASE(20) !H-C(4M ring)-C(4M ring)
              stb_fc_ijk = STR_BND_FC_IJK(25)
              stb_fc_kji = STR_BND_FC_KJI(25)
            CASE(30) !H-C(4M ring)-C(olefinic 4M ring)
              stb_fc_ijk = STR_BND_FC_IJK(26)
              stb_fc_kji = STR_BND_FC_KJI(26)
            CASE DEFAULT
              write(UNIout,'(a)')'GET_STB_FC_HCC> jATOM: switching to default force program_constants.'
! Wild card type *11 (1=row in periodic table):
              stb_fc_ijk = STR_BND_FC_IJK(42)
              stb_fc_kji = STR_BND_FC_KJI(42)
          end SELECT
        CASE(22)
          SELECT CASE(type_j)             ! ANG_PARMJ5
            CASE(1) !H-C(3M ring)-C(sp3)
              stb_fc_kji = STR_BND_FC_IJK(29)
              stb_fc_ijk = STR_BND_FC_KJI(29)
            CASE(22) !H-C(3M ring)-C(3M ring)
              stb_fc_ijk = STR_BND_FC_IJK(32)
              stb_fc_kji = STR_BND_FC_KJI(32)
            CASE DEFAULT
              write(UNIout,'(a)')'GET_STB_FC_HCC> jATOM: switching to default force program_constants.'
! Wild card type *11 (1=row in periodic table):
              stb_fc_ijk = STR_BND_FC_IJK(42)
              stb_fc_kji = STR_BND_FC_KJI(42)
          end SELECT
        CASE(30)
          SELECT CASE(type_j)              ! ANG_PARMJ6
            CASE(20) !H-C(olefinic 4M ring)-C(4M ring)
              stb_fc_ijk = STR_BND_FC_IJK(34)
              stb_fc_kji = STR_BND_FC_KJI(34)
            CASE(30) !H-C(olefinic 4M ring)-C(olefinic 4M ring)
              stb_fc_ijk = STR_BND_FC_IJK(35)
              stb_fc_kji = STR_BND_FC_KJI(35)
            CASE DEFAULT
              write(UNIout,'(a)')'GET_STB_FC_HCC> jATOM: switching to default force program_constants.'
! Wild card type *11 (1=row in periodic table):
              stb_fc_ijk = STR_BND_FC_IJK(42)
              stb_fc_kji = STR_BND_FC_KJI(42)
          end SELECT
        CASE(37) !only one case: H-C(aromatic)-C(aromatic)
          stb_fc_ijk = STR_BND_FC_IJK(39)
          stb_fc_kji = STR_BND_FC_KJI(39)
        CASE DEFAULT
          write(UNIout,'(a)')'GET_STB_FC_HCC> jATOM: switching to default force program_constants.'
! Wild card type *11 (1=row in periodic table):
          stb_fc_ijk = STR_BND_FC_IJK(42)
          stb_fc_kji = STR_BND_FC_KJI(42)
      end SELECT

! End of subroutine GET_STB_FC_HCC
      return
      END
      subroutine GET_STB_FC_CCH(type_i, & !atom type for atom i
                                type_k, & !atom type for atom k
                                bond_ik, & !bond type for bond i-k
                                stb_fc_ijk, & !force constant for stretch-bend
                                stb_fc_kji) !force constant for stretch-bend
!**********************************************************************
! Date last modified: 19 October, 1999                    Version 1.0 *
! Author: Michelle Shaw                                               *
! Description: Get equilibrium angles and force program_constants for a CCH   *
!              stretch-bend.                                          *
!**********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE strbnd_fc

      implicit none
!
! Input scalars:
      integer type_i, type_k, bond_ik
      double precision stb_fc_ijk, stb_fc_kji
!
! Begin:
! This is where the parameters are selected for both equilibrium angles and force program_constants.
      stb_fc_ijk = ZERO
      stb_fc_kji = ZERO

      SELECT CASE(type_k)                          ! ANG_PARMK
        CASE(1)
          SELECT CASE(type_i)                      ! ANG_PARMI1
            CASE(1) !C(sp3)-C(sp3)-H
              stb_fc_ijk = STR_BND_FC_IJK(3)
              stb_fc_kji = STR_BND_FC_KJI(3)
            CASE(2) !C(sp2)-C(sp3)-H
              stb_fc_ijk = STR_BND_FC_IJK(6)
              stb_fc_kji = STR_BND_FC_KJI(6)
            CASE(20) !C(4M ring)-C(sp3)-H
              stb_fc_kji = STR_BND_FC_IJK(8)
              stb_fc_ijk = STR_BND_FC_KJI(8)
            CASE(22) !C(3M ring)-C(sp3)-H
              stb_fc_kji = STR_BND_FC_IJK(9)
              stb_fc_ijk = STR_BND_FC_KJI(9)
            CASE(30) !C(olefinic 4M ring)-C(sp3)-H
! Wild card type *11 (1=row in periodic table):
              stb_fc_ijk = STR_BND_FC_IJK(42)
              stb_fc_kji = STR_BND_FC_KJI(42)
            CASE(37) !C(aromatic)-C(sp3)-H
              stb_fc_kji = STR_BND_FC_IJK(10)
              stb_fc_ijk = STR_BND_FC_KJI(10)
            CASE DEFAULT
              write(UNIout,'(a)')'GET_STB_FC_CCH> jATOM: switching to default force program_constants.'
! Wild card type *11 (1=row in periodic table):
              stb_fc_ijk = STR_BND_FC_IJK(42)
              stb_fc_kji = STR_BND_FC_KJI(42)
          end SELECT
        CASE(2)
          SELECT CASE(type_i)                       ! ANG_PARMI2
            CASE(1) !C(sp3)-C(sp2)-H
              stb_fc_ijk = STR_BND_FC_IJK(14)
              stb_fc_kji = STR_BND_FC_KJI(14)
            CASE(2) !C(sp2)-C(sp2)-H
              if(bond_ik.eq.3) then
                stb_fc_ijk = STR_BND_FC_IJK(17)
                stb_fc_kji = STR_BND_FC_KJI(17)
              else
                stb_fc_ijk = STR_BND_FC_IJK(18)
                stb_fc_kji = STR_BND_FC_KJI(18)
              end if
            CASE(37) !C(aromatic)-C(sp2)-H
              stb_fc_kji = STR_BND_FC_IJK(21)
              stb_fc_ijk = STR_BND_FC_KJI(21)
            CASE DEFAULT
              write(UNIout,'(a)')'GET_STB_FC_CCH> jATOM: switching to default force program_constants.'
! Wild card type *11 (1=row in periodic table):
              stb_fc_ijk = STR_BND_FC_IJK(42)
              stb_fc_kji = STR_BND_FC_KJI(42)
          end SELECT
        CASE(20)
          SELECT CASE(type_i)                        ! ANG_PARMI4
            CASE(1) !C(sp3)-C(4M ring)-H
              stb_fc_ijk = STR_BND_FC_IJK(22)
              stb_fc_kji = STR_BND_FC_KJI(22)
            CASE(20) !C(4M ring)-C(4M ring)-H
              stb_fc_kji = STR_BND_FC_IJK(25)
              stb_fc_ijk = STR_BND_FC_KJI(25)
            CASE(30) !C(olefinic 4M ring)-C(4M ring)-H
              stb_fc_kji = STR_BND_FC_IJK(26)
              stb_fc_ijk = STR_BND_FC_KJI(26)
            CASE DEFAULT
              write(UNIout,'(a)')'GET_STB_FC_CCH> jATOM: switching to default force program_constants.'
! Wild card type *11 (1=row in periodic table):
              stb_fc_ijk = STR_BND_FC_IJK(42)
              stb_fc_kji = STR_BND_FC_KJI(42)
          end SELECT
        CASE(22)
          SELECT CASE(type_i)                        ! ANG_PARMI5
            CASE(1) !C(sp3)-C(3M ring)-H
              stb_fc_ijk = STR_BND_FC_IJK(29)
              stb_fc_kji = STR_BND_FC_KJI(29)
            CASE(22) !C(3M ring)-C(3M ring)-H
              stb_fc_kji = STR_BND_FC_IJK(32)
              stb_fc_ijk = STR_BND_FC_KJI(32)
            CASE DEFAULT
              write(UNIout,'(a)')'GET_STB_FC_CCH> jATOM: switching to default force program_constants.'
! Wild card type *11 (1=row in periodic table):
              stb_fc_ijk = STR_BND_FC_IJK(42)
              stb_fc_kji = STR_BND_FC_KJI(42)
          end SELECT
        CASE(30)
          SELECT CASE(type_i)                         ! ANG_PARMI6
            CASE(20) !C(4M ring)-C(olefinic 4M ring)-H
              stb_fc_kji = STR_BND_FC_IJK(34)
              stb_fc_ijk = STR_BND_FC_KJI(34)
            CASE(30) !C(olefinic 4M ring)-C(olefinic 4M ring)-H
              stb_fc_kji = STR_BND_FC_IJK(35)
              stb_fc_ijk = STR_BND_FC_KJI(35)
            CASE DEFAULT
              write(UNIout,'(a)')'GET_STB_FC_CCH> jATOM: switching to default force program_constants.'
! Wild card type *11 (1=row in periodic table):
              stb_fc_ijk = STR_BND_FC_IJK(42)
              stb_fc_kji = STR_BND_FC_KJI(42)
          end SELECT
        CASE(37)
          stb_fc_kji = STR_BND_FC_IJK(39)
          stb_fc_ijk = STR_BND_FC_KJI(39)
        CASE DEFAULT
          write(UNIout,'(a)')'GET_STB_FC_CCH> jATOM: switching to default force program_constants.'
! Wild card type *11 (1=row in periodic table):
          stb_fc_ijk = STR_BND_FC_IJK(42)
          stb_fc_kji = STR_BND_FC_KJI(42)
      end SELECT

! End of function GET_STB_FC_CCH
      return
      END
      subroutine GET_STB_FC_CCC(type_i, & !type for atom i
                                type_j, & !type for atom j
                                type_k, & !type for atom k (central atom)
                                bond_ik,&  !type for bond i-k
                                bond_kj, & !type for bond k-j
                                stb_fc_ijk, & !force constant for stretch-bend
                                stb_fc_kji) !force constant for stretch-bend
!**********************************************************************
! Date last modified: 19 October, 1999                    Version 1.0 *
! Author: Michelle Shaw                                               *
! Description: Get equilibrium angles and force program_constants for a CCC   *
!              stretch-bend.                                          *
!**********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE strbnd_fc

      implicit none
!
! Input scalars:
      integer type_i, type_j, type_k, bond_ik, bond_kj
      double precision stb_fc_ijk, stb_fc_kji
!
! Begin:
! This is where the parameters are selected for both equilibrium angles and force program_constants.
      stb_fc_ijk = ZERO
      stb_fc_kji = ZERO

      SELECT CASE (type_i)                                  ! ANG_PARMI
        CASE(1)
          SELECT CASE (type_k)                              ! ANG_PARMK1
            CASE(1)
              SELECT CASE (type_j)                          ! ANG_PARMJ1
                CASE(1) !C(sp3)-C(sp3)-C(sp3)
                  stb_fc_ijk = STR_BND_FC_IJK(1)
                  stb_fc_kji = STR_BND_FC_KJI(1)
                CASE(2) !C(sp3)-C(sp3)-C(sp2)
                  stb_fc_ijk = STR_BND_FC_IJK(2)
                  stb_fc_kji = STR_BND_FC_KJI(2)
                CASE(4) !C(sp3)-C(sp3)-C(sp)
                  stb_fc_ijk = STR_BND_FC_IJK(43)
                  stb_fc_kji = STR_BND_FC_KJI(43)
                CASE(37) !C(sp3)-C(sp3)-C(aromatic)
                  stb_fc_ijk = STR_BND_FC_IJK(4)
                  stb_fc_kji = STR_BND_FC_KJI(4)
                CASE DEFAULT
                  write(UNIout,'(a)')'GET_STB_FC_CCC> jATOM: Switching to defaults.'
! wild card type 111 (1=row in periodic table)
                  stb_fc_ijk = STR_BND_FC_IJK(47)
                  stb_fc_kji = STR_BND_FC_KJI(47)
              end SELECT
            CASE(2)
              SELECT CASE (type_j)                           ! ANG_PARMJ2
                CASE(1) !C(sp3)-C(sp2)-C(sp3)
                  stb_fc_ijk = STR_BND_FC_IJK(11)
                  stb_fc_kji = STR_BND_FC_KJI(11)
                CASE(2) !C(sp3)-C(sp2)-C(sp2)
! Stretch-bend type of 2
                  if(bond_kj.eq.3) then
                    stb_fc_ijk = STR_BND_FC_IJK(12)
                    stb_fc_kji = STR_BND_FC_KJI(12)
! Stretch-bend type of 0
                  else
                    stb_fc_ijk = STR_BND_FC_IJK(13)
                    stb_fc_kji = STR_BND_FC_KJI(13)
                  end if
                CASE(37) !C(sp3)-C(sp2)-C(aromatic)
                  stb_fc_ijk = STR_BND_FC_IJK(15)
                  stb_fc_kji = STR_BND_FC_KJI(15)
                CASE DEFAULT
                  write(UNIout,'(a)')'GET_STB_FC_CCC> jATOM: Switching to defaults.'
! wild card type 111 (1=row in periodic table)
                  stb_fc_ijk = STR_BND_FC_IJK(47)
                  stb_fc_kji = STR_BND_FC_KJI(47)
              end SELECT
            CASE(20)
              SELECT CASE (type_j)                             ! ANG_PARMJ3
                CASE(20) !C(sp3)-C(4M ring)-C(4M ring)
                  stb_fc_ijk = STR_BND_FC_IJK(23)
                  stb_fc_kji = STR_BND_FC_KJI(23)
                CASE DEFAULT
                  write(UNIout,'(a)')'GET_STB_FC_CCC> jATOM: Switching to defaults.'
! wild card type 111 (1=row in periodic table)
                  stb_fc_ijk = STR_BND_FC_IJK(47)
                  stb_fc_kji = STR_BND_FC_KJI(47)
              end SELECT
            CASE(22)
              SELECT CASE (type_j)                              ! ANG_PARMJ4
                CASE(22) !C(sp3)-C(3M ring)-C(3M ring)
                  stb_fc_ijk = STR_BND_FC_IJK(30)
                  stb_fc_kji = STR_BND_FC_KJI(30)
                CASE DEFAULT
                  write(UNIout,'(a)')'GET_STB_FC_CCC> jATOM: Switching to defaults.'
! wild card type 111 (1=row in periodic table)
                  stb_fc_ijk = STR_BND_FC_IJK(47)
                  stb_fc_kji = STR_BND_FC_KJI(47)
              end SELECT
            CASE(37)
              SELECT CASE(type_j)                                ! ANG_PARMJ5
                CASE(37) !C(sp3)-C(aromatic)-C(aromatic)
                  stb_fc_ijk = STR_BND_FC_IJK(37)
                  stb_fc_kji = STR_BND_FC_KJI(37)
                CASE DEFAULT
                  write(UNIout,'(a)')'GET_STB_FC_CCC> jATOM: Switching to defaults.'
! wild card type 111 (1=row in periodic table)
                  stb_fc_ijk = STR_BND_FC_IJK(47)
                  stb_fc_kji = STR_BND_FC_KJI(47)
              end SELECT
            CASE DEFAULT
              write(UNIout,'(a)')'GET_STB_FC_CCC> jATOM: Switching to defaults.'
! wild card type 111 (1=row in periodic table)
              stb_fc_ijk = STR_BND_FC_IJK(47)
              stb_fc_kji = STR_BND_FC_KJI(47)
          end SELECT
        CASE(2)
          SELECT CASE (type_k)                                   ! ANG_PARMK2
            CASE(1)
              SELECT CASE (type_j)                               ! ANG_PARMJ6
                CASE(1) !C(sp2)-C(sp3)-C(sp3)
                  stb_fc_ijk = STR_BND_FC_KJI(2)
                  stb_fc_kji = STR_BND_FC_IJK(2)
                CASE(2) !C(sp2)-C(sp3)-C(sp2)
                  stb_fc_ijk = STR_BND_FC_IJK(5)
                  stb_fc_kji = STR_BND_FC_KJI(5)
                CASE(4) !C(sp2)-C(sp3)-C(sp)
                  stb_fc_ijk = STR_BND_FC_IJK(49)
                  stb_fc_kji = STR_BND_FC_KJI(49)
                CASE DEFAULT
                  write(UNIout,'(a)')'GET_STB_FC_CCC> jATOM: Switching to defaults.'
! wild card type 111 (1=row in periodic table)
                  stb_fc_ijk = STR_BND_FC_IJK(47)
                  stb_fc_kji = STR_BND_FC_KJI(47)
              end SELECT
            CASE(2)
              SELECT CASE (type_j)                               ! ANG_PARMJ7
                CASE(1) !C(sp2)-C(sp2)-C(sp3)
! Stretch-bend type of 2
                  if(bond_ik.eq.3) then
                    stb_fc_ijk = STR_BND_FC_KJI(12)
                    stb_fc_kji = STR_BND_FC_IJK(12)
! Stretch-bend type of 0
                  else
                    stb_fc_ijk = STR_BND_FC_KJI(13)
                    stb_fc_kji = STR_BND_FC_IJK(13)
                  end if
                CASE(2) !C(sp2)-C(sp2)-C(sp2)
                  stb_fc_ijk = STR_BND_FC_KJI(16)
                  stb_fc_kji = STR_BND_FC_IJK(16)
                CASE(37) !C(sp2)-C(sp2)-C(aromatic)
                  stb_fc_ijk = STR_BND_FC_KJI(19)
                  stb_fc_kji = STR_BND_FC_IJK(19)
                CASE DEFAULT
              end SELECT
            CASE(37)
              SELECT CASE(type_j)                                ! ANG_PARMJ8
                CASE(37) !C(sp2)-C(aromatic)-C(aromatic)
                  stb_fc_ijk = STR_BND_FC_IJK(38)
                  stb_fc_kji = STR_BND_FC_KJI(38)
                CASE DEFAULT
              end SELECT
            CASE DEFAULT
              write(UNIout,'(a)')'GET_STB_FC_CCC> jATOM: Switching to defaults.'
! wild card type 111 (1=row in periodic table)
              stb_fc_ijk = STR_BND_FC_IJK(47)
              stb_fc_kji = STR_BND_FC_KJI(47)
          end SELECT
        CASE(20)
          SELECT CASE (type_k)                                        ! ANG_PARMK3
            CASE(20)
              SELECT CASE (type_j)                                    ! ANG_PARMJ9
                CASE(1) !C(4M ring)-C(4M ring)-C(sp3)
                  stb_fc_ijk = STR_BND_FC_KJI(23)
                  stb_fc_kji = STR_BND_FC_IJK(23)
                CASE(20) !C(4M ring)-C(4M ring)-C(4M ring)
                  stb_fc_ijk = STR_BND_FC_IJK(27)
                  stb_fc_kji = STR_BND_FC_KJI(27)
                CASE(30) !C(4M ring)-C(4M ring)-C(olefinic 4M ring)
                  stb_fc_ijk = STR_BND_FC_IJK(28)
                  stb_fc_kji = STR_BND_FC_KJI(28)
                CASE DEFAULT
                  write(UNIout,'(a)')'GET_STB_FC_CCC> jATOM: Switching to defaults.'
! wild card type 111 (1=row in periodic table)
                  stb_fc_ijk = STR_BND_FC_IJK(47)
                  stb_fc_kji = STR_BND_FC_KJI(47)
              end SELECT
            CASE(30)
              SELECT CASE (type_j)                                     ! ANG_PARMJ10
                CASE(30) !C(4M ring)-C(olefinic 4M ring)-C(olefinic 4M ring)
                  stb_fc_ijk = STR_BND_FC_IJK(36)
                  stb_fc_kji = STR_BND_FC_KJI(36)
                CASE DEFAULT
                  write(UNIout,'(a)')'GET_STB_FC_CCC> jATOM: Switching to defaults.'
! wild card type 111 (1=row in periodic table)
                  stb_fc_ijk = STR_BND_FC_IJK(47)
                  stb_fc_kji = STR_BND_FC_KJI(47)
              end SELECT
            CASE DEFAULT
              write(UNIout,'(a)')'GET_STB_FC_CCC> jATOM: Switching to defaults.'
! wild card type 111 (1=row in periodic table)
              stb_fc_ijk = STR_BND_FC_IJK(47)
              stb_fc_kji = STR_BND_FC_KJI(47)
          end SELECT
        CASE(22)
          SELECT CASE (type_k)                                         ! ANG_PARMK4
            CASE(22)
              SELECT CASE (type_j)                                     ! ANG_PARMJ11
                CASE(1) !C(3M ring)-C(3M ring)-C(sp3)
                  stb_fc_ijk = STR_BND_FC_KJI(30)
                  stb_fc_kji = STR_BND_FC_IJK(30)
                CASE(22) !C(3M ring)-C(3M ring)-C(3M ring)
                  stb_fc_ijk = STR_BND_FC_IJK(33)
                  stb_fc_kji = STR_BND_FC_KJI(33)
                CASE DEFAULT
                  write(UNIout,'(a)')'GET_STB_FC_CCC> jATOM: Switching to defaults.'
! wild card type 111 (1=row in periodic table)
                  stb_fc_ijk = STR_BND_FC_IJK(47)
                  stb_fc_kji = STR_BND_FC_KJI(47)
              end SELECT
            CASE DEFAULT
              write(UNIout,'(a)')'GET_STB_FC_CCC> jATOM: Switching to defaults.'
! wild card type 111 (1=row in periodic table)
              stb_fc_ijk = STR_BND_FC_IJK(47)
              stb_fc_kji = STR_BND_FC_KJI(47)
          end SELECT
        CASE(30)
          SELECT CASE (type_k)                                      ! ANG_PARMK5
            CASE(20)
              SELECT CASE (type_j)                                  ! ANG_PARMJ12
                CASE(20) !C(olefinic 4M ring)-C(4M ring)-C(4M ring)
                  stb_fc_ijk = STR_BND_FC_KJI(28)
                  stb_fc_kji = STR_BND_FC_IJK(28)
                CASE DEFAULT
                  write(UNIout,'(a)')'GET_STB_FC_CCC> jATOM: Switching to defaults.'
! wild card type 111 (1=row in periodic table)
                  stb_fc_ijk = STR_BND_FC_IJK(47)
                  stb_fc_kji = STR_BND_FC_KJI(47)
              end SELECT
            CASE(30)
              SELECT CASE (type_j)                                  ! ANG_PARMJ13
                CASE(20) !C(olefinic 4M ring)-C(olefinic 4M ring)-C(4M ring)
                  stb_fc_ijk = STR_BND_FC_KJI(36)
                  stb_fc_kji = STR_BND_FC_IJK(36)
                CASE DEFAULT
                  write(UNIout,'(a)')'GET_STB_FC_CCC> jATOM: Switching to defaults.'
! wild card type 111 (1=row in periodic table)
                  stb_fc_ijk = STR_BND_FC_IJK(47)
                  stb_fc_kji = STR_BND_FC_KJI(47)
              end SELECT
            CASE DEFAULT
              write(UNIout,'(a)')'GET_STB_FC_CCC> jATOM: Switching to defaults.'
! wild card type 111 (1=row in periodic table)
              stb_fc_ijk = STR_BND_FC_IJK(47)
              stb_fc_kji = STR_BND_FC_KJI(47)
          end SELECT
        CASE(37)
          SELECT CASE (type_k)                                      ! ANG_PARMK6
            CASE(1)
              SELECT CASE (type_j)                                  ! ANG_PARMJ14
                CASE(1) !C(aromatic)-C(sp3)-C(sp3)
                  stb_fc_ijk = STR_BND_FC_KJI(4)
                  stb_fc_kji = STR_BND_FC_IJK(4)
                CASE DEFAULT
                  write(UNIout,'(a)')'GET_STB_FC_CCC> jATOM: Switching to defaults.'
! wild card type 111 (1=row in periodic table)
                  stb_fc_ijk = STR_BND_FC_IJK(47)
                  stb_fc_kji = STR_BND_FC_KJI(47)
              end SELECT
            CASE(2)
              SELECT CASE (type_j)                                  ! ANG_PARMJ15
                CASE(1) !C(aromatic)-C(sp2)-C(sp3)
                  stb_fc_ijk = STR_BND_FC_KJI(15)
                  stb_fc_kji = STR_BND_FC_IJK(15)
                CASE(2) !C(aromatic)-C(sp2)-C(sp2)
                  stb_fc_ijk = STR_BND_FC_KJI(19)
                  stb_fc_kji = STR_BND_FC_IJK(19)
                CASE DEFAULT
                  write(UNIout,'(a)')'GET_STB_FC_CCC> jATOM: Switching to defaults.'
! wild card type 111 (1=row in periodic table)
                  stb_fc_ijk = STR_BND_FC_IJK(47)
                  stb_fc_kji = STR_BND_FC_KJI(47)
              end SELECT
            CASE(37)
              SELECT CASE (type_j)                                  ! ANG_PARMJ16
                CASE(1) !C(aromatic)-C(aromatic)-C(sp3)
                  stb_fc_ijk = STR_BND_FC_KJI(37)
                  stb_fc_kji = STR_BND_FC_IJK(37)
                CASE(2) !C(aromatic)-C(aromatic)-C(sp2)
                  stb_fc_ijk = STR_BND_FC_KJI(38)
                  stb_fc_kji = STR_BND_FC_IJK(38)
                CASE(37) !C(aromatic)-C(aromatic)-C(aromatic)
                  stb_fc_ijk = STR_BND_FC_IJK(40)
                  stb_fc_kji = STR_BND_FC_KJI(40)
                CASE DEFAULT
                  write(UNIout,'(a)')'GET_STB_FC_CCC> jATOM: Switching to defaults.'
! wild card type 111 (1=row in periodic table)
                  stb_fc_ijk = STR_BND_FC_IJK(47)
                  stb_fc_kji = STR_BND_FC_KJI(47)
              end SELECT
            CASE DEFAULT
              write(UNIout,'(a)')'GET_STB_FC_CCC> jATOM: Switching to defaults.'
! wild card type 111 (1=row in periodic table)
              stb_fc_ijk = STR_BND_FC_IJK(47)
              stb_fc_kji = STR_BND_FC_KJI(47)
          end SELECT
        CASE DEFAULT
          write(UNIout,'(a)')'GET_STB_FC_CCC> jATOM: Switching to defaults.'
! wild card type 111 (1=row in periodic table)
          stb_fc_ijk = STR_BND_FC_IJK(47)
          stb_fc_kji = STR_BND_FC_KJI(47)
      end SELECT

! End of function GET_STB_FC_CCC
      return
      end
