      module FF_parameters
!**********************************************************************************
! Date last modified: March 22, 2005                                  Version 1.0 *
! Author: R. A. Poirier                                                           *
! Description: Contains object and initialization routines for stretch parameters.*                                                       !**********************************************************************************
! Modules:
      USE program_manager
      USE program_defaults
      USE program_constants
      USE type_molecule
      USE mm_interaction_type_params
      USE nobond_params
      USE GRAPH_objects
      USE atom_type

      implicit none

      type stretch_contributions
        double precision :: k_ij !stretch force constant
        double precision :: R_ij !equilibrium bond length
      end type stretch_contributions

      type (stretch_contributions), dimension(:), allocatable :: MMstretch

CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine GETBOND_PARAMS
!*****************************************************************
! Date last modified: 29 March, 2005                 Version 1.0 *
! Author: R.A. Poirier and M. Shaw                               *
! Description: Get equilibrium bond lengths                      *
!*****************************************************************
! Modules:

      implicit none

! Local scalars:
      integer :: case_bond,IBond,Iatom,Jatom,Ring_size,RsizeI,RsizeJ,type_bond
      integer :: TypeX,TypeY,ZatomI,ZatomJ,Zmax,Zmin
      double precision :: R_ij,K_ij
      logical Ldebug
!
! Local functions:
!
! Begin:
      call PRG_manager ('enter', 'GETBOND_PARAMS', 'UTILITY')
      Ldebug=Local_Debug

! REMEMBER TO CONVERT TO A.U.!!!!!!!!!!!!!!!!!!!!!
! Allocate work arrays:
      if(.not.allocated(MMstretch))then
        allocate(MMstretch(NGBonds))
      end if

! The information about each bond must be collected.
      do IBond = 1,NGBonds
        Iatom = BOND(IBond)%ATOM1
        Jatom = BOND(IBond)%ATOM2
        type_bond = BOND(IBond)%type
        Ring_size=BOND(IBond)%ring_size
        ZatomI=CARTESIANS%Atomic_number(Iatom)
        ZatomJ=CARTESIANS%Atomic_number(Jatom)
        if(ZatomI.ge.ZatomJ)then
          Zmax=ZatomI
          TypeX=vertex(Iatom)%type
          RsizeI=vertex(Iatom)%type
          Zmin=ZatomJ
          TypeY=vertex(Jatom)%type
          RsizeJ=vertex(Jatom)%type
        else
          Zmax=ZatomJ
          TypeX=vertex(Jatom)%type
          RsizeI=vertex(Jatom)%type
          Zmin=ZatomI
          TypeY=vertex(Iatom)%type
          RsizeJ=vertex(Iatom)%type
        end if
        case_bond=110*(Zmin-1)+(Zmax-1)
        write(UNIout,'(a,3i8)')'GETBOND_PARAMS: TypeX,TypeY,type_bond: ',TypeX,TypeY,type_bond

! All values currently defaults to C-C
        SBOND:select case(case_bond)
          case (0) SBOND ! H-H (done)
            MMstretch(IBond)%R_ij=0.746D0
            MMstretch(IBond)%k_ij=0.05168D0
! Li-X bonds
          case (220) SBOND ! Li-H
            MMstretch(IBond)%R_ij=1.508D0
            MMstretch(IBond)%k_ij=4.258D0
          case (222) SBOND ! Li-Li
            MMstretch(IBond)%R_ij=1.508D0
            MMstretch(IBond)%k_ij=4.258D0
! Be-X bonds
          case (330) SBOND ! Be-H
            MMstretch(IBond)%R_ij=1.508D0
            MMstretch(IBond)%k_ij=4.258D0
! B-X bonds
          case (440) SBOND ! B-H
            MMstretch(IBond)%R_ij=1.508D0
            MMstretch(IBond)%k_ij=4.258D0
! C-X bonds
          case (550) SBOND ! C-H
            call MM_CH_BONDS (TypeX, TypeY, RsizeI, R_ij, K_ij)
            MMstretch(IBond)%R_ij=R_ij
            MMstretch(IBond)%k_ij=K_ij
          case (555) SBOND ! C-C
            typeCC:select case(type_bond)
              case (1) typeCC ! Single bond
                call MM_C1C_BONDS (TypeX, TypeY, R_ij, K_ij)
              case (2) typeCC ! Double bond
                call MM_C2C_BONDS (TypeX, TypeY, Ring_size, R_ij, K_ij)
              case (3) typeCC ! Triple bond
                call MM_C3C_BONDS (TypeX, TypeY, R_ij, K_ij)
              case (13,23,14,24,15,25,16,26,66) typeCC ! rings, ..., Aromatic bond
                call MM_CaC_BONDS (TypeX, TypeY, R_ij, K_ij)
            end select typeCC
            MMstretch(IBond)%R_ij=R_ij
            MMstretch(IBond)%k_ij=K_ij
! N-X bonds
          case (660) SBOND ! N-H
            call MM_NH_BONDS (TypeX, TypeY, R_ij, K_ij)
            MMstretch(IBond)%R_ij=R_ij
            MMstretch(IBond)%k_ij=K_ij
          case (665) SBOND ! N-C
            typeNC:select case(type_bond)
              case (1) typeNC ! Single bond
                call MM_N1C_BONDS (TypeX, TypeY, R_ij, K_ij)
              case (2) typeNC ! Double bond
                call MM_N2C_BONDS (TypeX, TypeY, R_ij, K_ij)
              case (3) typeNC ! Triple bond
                call MM_N3C_BONDS (TypeX, TypeY, R_ij, K_ij)
              case (13,23,14,24,15,25,16,26,66) typeNC ! rings, ..., Aromatic bond
!               call MM_NaC_BONDS (TypeX, TypeY, R_ij, K_ij)
            end select typeNC
            MMstretch(IBond)%R_ij=R_ij
            MMstretch(IBond)%k_ij=K_ij
          case (666) SBOND ! N-N
            typeNN:select case(type_bond)
              case (1) typeNN ! Single bond
!               call MM_N1N_BONDS (TypeX, TypeY, R_ij, K_ij)
              case (2) typeNN ! Double bond
!               call MM_N2N_BONDS (TypeX, TypeY, R_ij, K_ij)
              case (3) typeNN ! Triple bond
!               call MM_N3N_BONDS (TypeX, TypeY, R_ij, K_ij)
              case (26) typeNN ! Aromatic bond
!               call MM_NaN_BONDS (TypeX, TypeY, R_ij, K_ij)
            end select typeNN
            MMstretch(IBond)%R_ij=R_ij
            MMstretch(IBond)%k_ij=K_ij
! O-X bonds
          case (770) SBOND ! O-H
            call MM_OH_BONDS (TypeX, TypeY, R_ij, K_ij)
            MMstretch(IBond)%R_ij=R_ij
            MMstretch(IBond)%k_ij=K_ij
          case (775) SBOND ! O-C
            call MM_OC_BONDS (TypeX, TypeY, R_ij, K_ij)
            MMstretch(IBond)%R_ij=R_ij
            MMstretch(IBond)%k_ij=K_ij
          case (776) SBOND ! O-N
!           call MM_ON_BONDS (TypeX, TypeY, R_ij, K_ij)
            MMstretch(IBond)%R_ij=R_ij
            MMstretch(IBond)%k_ij=K_ij
          case (777) SBOND ! O-O (done)
!           call MM_OO_BONDS (TypeX, TypeY, R_ij, K_ij)
            MMstretch(IBond)%R_ij=1.449D0
            MMstretch(IBond)%k_ij=4.088D0
! F-X bonds
          case (880) SBOND ! F-H
            MMstretch(IBond)%R_ij=1.508D0
            MMstretch(IBond)%k_ij=4.258D0
          case (885) SBOND ! F-C(sp3)
            MMstretch(IBond)%R_ij=1.360D0
            MMstretch(IBond)%k_ij=6.011D0
          case (886) SBOND ! F-N
            MMstretch(IBond)%R_ij=1.508D0
            MMstretch(IBond)%k_ij=4.258D0
          case (887) SBOND ! F-O
            MMstretch(IBond)%R_ij=1.508D0
            MMstretch(IBond)%k_ij=4.258D0
          case (888) SBOND ! F-F
            MMstretch(IBond)%R_ij=1.508D0
            MMstretch(IBond)%k_ij=4.258D0
! Si-X bonds
          case (1430) SBOND ! Si-H (done)
            MMstretch(IBond)%R_ij=1.485D0
            MMstretch(IBond)%k_ij=2.254D0
          case (1435) SBOND ! Si-C (SP3)
            MMstretch(IBond)%R_ij=1.830D0
            MMstretch(IBond)%k_ij=2.866D0
! P-X bonds
          case (1540) SBOND ! P-H (done)
            MMstretch(IBond)%R_ij=1.415D0
            MMstretch(IBond)%k_ij=2.959D0
          case (1545) SBOND ! P-C
            MMstretch(IBond)%R_ij=1.508D0
            MMstretch(IBond)%k_ij=4.258D0
          case (1546) SBOND ! P-N
            MMstretch(IBond)%R_ij=1.508D0
            MMstretch(IBond)%k_ij=4.258D0
          case (1547) SBOND ! P-O (done)
            MMstretch(IBond)%R_ij=1.630D0
            MMstretch(IBond)%k_ij=5.243D0
          case (1554) SBOND ! P-P
            MMstretch(IBond)%R_ij=1.630D0
            MMstretch(IBond)%k_ij=5.243D0
! S-X bonds
          case (1650) SBOND ! S-H
            MMstretch(IBond)%R_ij=1.508D0
            MMstretch(IBond)%k_ij=4.258D0
          case (1655) SBOND ! S-C
            MMstretch(IBond)%R_ij=1.508D0
            MMstretch(IBond)%k_ij=4.258D0
          case (1656) SBOND ! S-N
            MMstretch(IBond)%R_ij=1.508D0
            MMstretch(IBond)%k_ij=4.258D0
          case (1657) SBOND ! S-O
            MMstretch(IBond)%R_ij=1.508D0
            MMstretch(IBond)%k_ij=4.258D0
          case (1664) SBOND ! S-P
            MMstretch(IBond)%R_ij=1.508D0
            MMstretch(IBond)%k_ij=4.258D0
          case (1665) SBOND ! S-S
            MMstretch(IBond)%R_ij=1.508D0
            MMstretch(IBond)%k_ij=4.258D0
! Cl-X bonds
          case (1760) SBOND ! Cl-H
            MMstretch(IBond)%R_ij=1.508D0
            MMstretch(IBond)%k_ij=4.258D0
          case (1765) SBOND ! Cl-C(SP3)
            MMstretch(IBond)%R_ij=1.773D0
            MMstretch(IBond)%k_ij=2.974D0
          case (1776) SBOND ! Cl-Cl
            MMstretch(IBond)%R_ij=1.508D0
            MMstretch(IBond)%k_ij=4.258D0
! Default   
          case default SBOND ! Bond is not currently defined:
            write(UNIout,'(a)')'GETBOND_PARAMS> Atom type not supported or is a dummy'
            MMstretch(IBond)%R_ij = ZERO
            MMstretch(IBond)%k_ij = ZERO
        end select SBOND

! Printing for debugging and things that are not accessible via an output statement:
!        if(Ldebug)then
          write(UNIout,'(a,f12.6))'equilibrium bond length: ',MMstretch(IBond)%R_ij
          write(UNIout,'(a,f12.6))'bond force constant: ',MMstretch(IBond)%k_ij
!        end if
      end do ! IBond

! End of routine GETBOND_PARAMS
      call PRG_manager ('exit', 'GETBOND_PARAMS', 'UTILITY')
      return
      end subroutine GETBOND_PARAMS
      subroutine MM_C1C_BONDS (TypeX, TypeY, R_ij, K_ij)
!*****************************************************************************
! Date last modified: March 8, 2005                              Version 1.0 *
! Author: R. A. Poirier                                                      *
! Description: Get equilibrium bond lengths and force program_constants for CC single*
!              bonds                                                         *
!*****************************************************************************
! Modules:

      implicit none

! Local scalars:
      integer, intent(IN) :: TypeX,TypeY
      double precision, intent(OUT) :: R_ij,K_ij
      logical Ldebug
!
! Begin:
      call PRG_manager ('enter', 'MM_C1C_BONDS', 'UTILITY')
      Ldebug=Local_Debug

      StypeX:select case(TypeX)
        case (20) StypeX ! CH3-C
          StypeY: select case (TypeY)
            case (20) StypeY ! CH3-CH3 (done)
              R_ij=1.508D0
              k_ij=4.258D0
            case (35) StypeY ! CH3-CH2,C (done)
              R_ij=1.508D0
              k_ij=4.258D0
            case (45) StypeY ! CH3-CH,C2 (done)
              R_ij=1.508D0
              k_ij=4.258D0
            case (250) StypeY ! CH3-C=CH,C (done)
              R_ij=1.508D0
              k_ij=4.258D0
            case (300) StypeY ! CH3-C=C-C2 (done)
              R_ij=1.482D0
              k_ij=4.539D0
            case (1500) StypeY ! CH3-C-(3)-C (done)
              R_ij=1.459D0
              k_ij=4.707D0
          end select StypeY
        case (35) StypeX ! CH2-C2 (done)
          R_ij=1.508D0
          k_ij=4.258D0
        case (45) StypeX ! CH-C3 (done)
          R_ij=1.508D0
          k_ij=4.258D0
        case (50) StypeX ! C-C4 (done)
          R_ij=1.508D0
          k_ij=4.258D0
        case default StypeX ! Bond is not currently defined:
          write(UNIout,'(a)')'MM_C1C_BONDS> Atom type not supported or is a dummy'
          R_ij = ZERO
          k_ij = ZERO
      end select StypeX

! End of routine MM_C1C_BONDS
      call PRG_manager ('exit', 'MM_C1C_BONDS', 'UTILITY')
      return
      end subroutine MM_C1C_BONDS
      subroutine MM_C2C_BONDS (TypeX, TypeY, Ring_size, R_ij, K_ij)
!*****************************************************************************
! Date last modified: March 8, 2005                              Version 1.0 *
! Author: R. A. Poirier                                                      *
! Description: Get equilibrium bond lengths and force program_constants for CC bonds *
!*****************************************************************************
! Modules:

      implicit none

! Local scalars:
      integer, intent(IN) :: Ring_size,TypeX,TypeY
      double precision, intent(OUT) :: R_ij,K_ij
      logical Ldebug
!
! Begin:
      call PRG_manager ('enter', 'MM_C2C_BONDS', 'UTILITY')
      Ldebug=Local_Debug

      RSIZE:select case(Ring_size)
      case (0) RSIZE
      R0typeX:select case(TypeX)
        case (150) R0typeX ! CH2=C
          R_ij=1.508D0
          k_ij=4.258D0
        case (250) R0typeX ! CH,C=C
          R_ij=1.508D0
          k_ij=4.258D0
        case (300) R0typeX ! C2,C=C
          R_ij=1.508D0
          k_ij=4.258D0
        case default R0typeX ! Bond is not currently defined:
          write(UNIout,'(a)')'MM_C2C_BONDS> Atom type not supported or is a dummy'
          R_ij = ZERO
          k_ij = ZERO
      end select R0typeX
      case (3) RSIZE
      R3typeX:select case(TypeX)
        case (150) R3typeX ! CH2=C
          R_ij=1.508D0
          k_ij=4.258D0
        case (250) R3typeX ! CH,C=C
          R_ij=1.508D0
          k_ij=4.258D0
        case (300) R3typeX ! C2,C=C
          R_ij=1.508D0
          k_ij=4.258D0
        case default R3typeX ! Bond is not currently defined:
          write(UNIout,'(a)')'MM_C2C_BONDS> Atom type not supported or is a dummy'
          R_ij = ZERO
          k_ij = ZERO
      end select R3typeX
      case (4) RSIZE
      R4typeX:select case(TypeX)
        case (150) R4typeX ! CH2=C
          R_ij=1.508D0
          k_ij=4.258D0
        case (250) R4typeX ! CH,C=C
          R_ij=1.508D0
          k_ij=4.258D0
        case (300) R4typeX ! C2,C=C
          R_ij=1.508D0
          k_ij=4.258D0
        case default R4typeX ! Bond is not currently defined:
          write(UNIout,'(a)')'MM_C2C_BONDS> Atom type not supported or is a dummy'
          R_ij = ZERO
          k_ij = ZERO
      end select R4typeX
      case (5) RSIZE
      R5typeX:select case(TypeX)
        case (150) R5typeX ! CH2=C
          R_ij=1.508D0
          k_ij=4.258D0
        case (250) R5typeX ! CH,C=C
          R_ij=1.508D0
          k_ij=4.258D0
        case (300) R5typeX ! C2,C=C
          R_ij=1.508D0
          k_ij=4.258D0
        case default R5typeX ! Bond is not currently defined:
          write(UNIout,'(a)')'MM_C2C_BONDS> Atom type not supported or is a dummy'
          R_ij = ZERO
          k_ij = ZERO
      end select R5typeX
      case (6) RSIZE
      R6typeX:select case(TypeX)
        case (150) R6typeX ! CH2=C
          R_ij=1.508D0
          k_ij=4.258D0
        case (250) R6typeX ! CH,C=C
          R_ij=1.508D0
          k_ij=4.258D0
        case (300) R6typeX ! C2,C=C
          R_ij=1.508D0
          k_ij=4.258D0
        case default R6typeX ! Bond is not currently defined:
          write(UNIout,'(a)')'MM_C2C_BONDS> Atom type not supported or is a dummy'
          R_ij = ZERO
          k_ij = ZERO
      end select R6typeX
      end select RSIZE

! End of routine MM_C2C_BONDS
      call PRG_manager ('exit', 'MM_C2C_BONDS', 'UTILITY')
      return
      end subroutine MM_C2C_BONDS
      subroutine MM_C3C_BONDS (TypeX, TypeY, R_ij, K_ij)
!*****************************************************************************
! Date last modified: March 8, 2005                              Version 1.0 *
! Author: R. A. Poirier                                                      *
! Description: Get equilibrium bond lengths and force program_constants for CC bonds *
!*****************************************************************************
! Modules:

      implicit none

! Local scalars:
      integer, intent(IN) :: TypeX,TypeY
      double precision, intent(OUT) :: R_ij,K_ij
      logical Ldebug
!
! Begin:
      call PRG_manager ('enter', 'MM_C3C_BONDS', 'UTILITY')
      Ldebug=Local_Debug

      StypeX:select case(TypeX)
        case (1000) StypeX ! H-C-(3)-C?
          R_ij=1.508D0
          k_ij=4.258D0
        case (1500) StypeX ! C-C-(3)-C?
          R_ij=1.508D0
          k_ij=4.258D0
        case default StypeX ! Bond is not currently defined:
          write(UNIout,'(a)')'MM_C3C_BONDS> Atom type not supported or is a dummy'
          R_ij = ZERO
          k_ij = ZERO
      end select StypeX

! End of routine MM_C3C_BONDS
      call PRG_manager ('exit', 'MM_C3C_BONDS', 'UTILITY')
      return
      end subroutine MM_C3C_BONDS
      subroutine MM_CaC_BONDS (TypeX, TypeY, R_ij, K_ij)
!*****************************************************************************
! Date last modified: March 8, 2005                              Version 1.0 *
! Author: R. A. Poirier                                                      *
! Description: Get equilibrium bond lengths and force program_constants for CC bonds *
!*****************************************************************************
! Modules:

      implicit none

! Local scalars:
      integer, intent(IN) :: TypeX,TypeY
      double precision, intent(OUT) :: R_ij,K_ij
      logical Ldebug
!
! Begin:
      call PRG_manager ('enter', 'MM_CaC_BONDS', 'UTILITY')
      Ldebug=Local_Debug

      StypeX:select case(TypeX)
        case (150) StypeX ! CH2=C
          R_ij=1.508D0
          k_ij=4.258D0
        case (250) StypeX ! CH,C=C
          R_ij=1.508D0
          k_ij=4.258D0
        case (300) StypeX ! C2,C=C
          R_ij=1.508D0
          k_ij=4.258D0
        case (1000) StypeX ! H-C-(3)-C?
          R_ij=1.508D0
          k_ij=4.258D0
        case (1500) StypeX ! C-C-(3)-C?
          R_ij=1.508D0
          k_ij=4.258D0
        case default StypeX ! Bond is not currently defined:
          write(UNIout,'(a)')'MM_CaC_BONDS> Atom type not supported or is a dummy'
          R_ij = ZERO
          k_ij = ZERO
      end select StypeX

! End of routine MM_CaC_BONDS
      call PRG_manager ('exit', 'MM_CaC_BONDS', 'UTILITY')
      return
      end subroutine MM_CaC_BONDS
      subroutine MM_CH_BONDS (TypeX, TypeY, RsizeI, R_ij, K_ij)
!*****************************************************************************
! Date last modified: March 8, 2005                              Version 1.0 *
! Author: R. A. Poirier                                                      *
! Description: Get equilibrium bond lengths and force program_constants for CH bonds *
!*****************************************************************************
! Modules:

      implicit none

! Local scalars:
      integer, intent(IN) :: RsizeI,TypeX,TypeY
      double precision, intent(OUT) :: R_ij,K_ij
      logical Ldebug
!
! Begin:
      call PRG_manager ('enter', 'MM_CH_BONDS', 'UTILITY')
      Ldebug=Local_Debug

      StypeX:select case(TypeX)
        case (0) StypeX ! C-H4 (done)
          R_ij=1.093D0
          k_ij=4.766D0
        case (20) StypeX ! CH3-C
          StypeY: select case (TypeY)
            case (20) StypeY ! CH3-CH3
              R_ij=1.093D0
              k_ij=4.766D0
            case (35) StypeY ! CH3-CH2,C
              R_ij=1.093D0
              k_ij=4.766D0
            case (45) StypeY ! CH3-CH,C2
              R_ij=1.093D0
              k_ij=4.766D0
            case (250) StypeY ! CH3-C=CH,C
              R_ij=1.093D0
              k_ij=4.766D0
            case (300) StypeY ! CH3-C=C-C2
              R_ij=1.093D0
              k_ij=4.766D0
            case (1500) StypeY ! CH3-C-(3)-C
              R_ij=1.093D0
              k_ij=4.766D0
          end select StypeY
        case (35) StypeX ! CH2-C2
          R_ij=1.093D0
          k_ij=4.766D0
        case (45) StypeX ! CH-C3
          R_ij=1.093D0
          k_ij=4.766D0
        case (150) StypeX ! CH2=C
          R_ij=1.083D0
          k_ij=4.258D0
        case (250) StypeX ! CH,C=C
          R_ij=1.083D0
          k_ij=4.258D0
        case (1000) StypeX ! H-C-(3)-C?
          R_ij=1.065D0
          k_ij=4.258D0
        case default StypeX ! Bond is not currently defined:
          write(UNIout,'(a)')'MM_CH_BONDS> Atom type not supported or is a dummy'
          R_ij = ZERO
          k_ij = ZERO
      end select StypeX

! End of routine MM_CH_BONDS
      call PRG_manager ('exit', 'MM_CH_BONDS', 'UTILITY')
      return
      end subroutine MM_CH_BONDS
      subroutine MM_N1C_BONDS (TypeX, TypeY, R_ij, K_ij)
!******************************************************************************
! Date last modified: March 8, 2005                               Version 1.0 *
! Author: R. A. Poirier                                                       *
! Description: Get equilibrium bond lengths and force program_constants for NC single *
!              bonds                                                          *
!******************************************************************************
! Modules:

      implicit none

! Local scalars:
      integer, intent(IN) :: TypeX,TypeY
      double precision, intent(OUT) :: R_ij,K_ij
      logical Ldebug
!
! Begin:
      call PRG_manager ('enter', 'MM_N1C_BONDS', 'UTILITY')
      Ldebug=Local_Debug

      StypeX:select case(TypeX)
        case (15) StypeX ! N-C,H2
          R_ij=1.508D0
          k_ij=4.258D0
        case (25) StypeX ! N-C2,H
          R_ij=1.508D0
          k_ij=4.258D0
        case (30) StypeX ! N-C3
          R_ij=1.508D0
          k_ij=4.258D0
      end select StypeX

! End of routine MM_N1C_BONDS
      call PRG_manager ('exit', 'MM_N1C_BONDS', 'UTILITY')
      return
      end subroutine MM_N1C_BONDS
      subroutine MM_N2C_BONDS (TypeX, TypeY, R_ij, K_ij)
!******************************************************************************
! Date last modified: March 8, 2005                               Version 1.0 *
! Author: R. A. Poirier                                                       *
! Description: Get equilibrium bond lengths and force program_constants for NC double *
!              bonds                                                          *
!******************************************************************************
! Modules:

      implicit none

! Local scalars:
      integer, intent(IN) :: TypeX,TypeY
      double precision, intent(OUT) :: R_ij,K_ij
      logical Ldebug
!
! Begin:
      call PRG_manager ('enter', 'MM_N2C_BONDS', 'UTILITY')
      Ldebug=Local_Debug

      StypeX:select case(TypeX)
        case (100) StypeX ! N=C,H
          R_ij=1.508D0
          k_ij=4.258D0
        case (150) StypeX ! N=C,C
          R_ij=1.508D0
          k_ij=4.258D0
      end select StypeX

! End of routine MM_N2C_BONDS
      call PRG_manager ('exit', 'MM_N2C_BONDS', 'UTILITY')
      return
      end subroutine MM_N2C_BONDS
      subroutine MM_N3C_BONDS (TypeN, TypeC, R_ij, K_ij)
!******************************************************************************
! Date last modified: March 8, 2005                               Version 1.0 *
! Author: R. A. Poirier                                                       *
! Description: Get equilibrium bond lengths and force program_constants for NC triple *
!              bonds                                                          *
!******************************************************************************
! Modules:

      implicit none

! Local scalars:
      integer, intent(IN) :: TypeN,TypeC
      double precision, intent(OUT) :: R_ij,K_ij
      logical Ldebug
!
! Begin:
      call PRG_manager ('enter', 'MM_N3C_BONDS', 'UTILITY')
      Ldebug=Local_Debug

      StypeN:select case(TypeN)
        case (500) StypeN ! N-(3)-C
          R_ij=1.508D0
          k_ij=4.258D0
      end select StypeN

! End of routine MM_N3C_BONDS
      call PRG_manager ('exit', 'MM_N3C_BONDS', 'UTILITY')
      return
      end subroutine MM_N3C_BONDS
      subroutine MM_OH_BONDS (TypeO, TypeH, R_ij, K_ij)
!*****************************************************************************
! Date last modified: March 8, 2005                              Version 1.0 *
! Author: R. A. Poirier                                                      *
! Description: Get equilibrium bond lengths and force program_constants for OH bonds *
!*****************************************************************************
! Modules:

      implicit none

! Local scalars:
      integer, intent(IN) :: TypeO,TypeH
      double precision, intent(OUT) :: R_ij,K_ij
      logical Ldebug
!
! Begin:
      call PRG_manager ('enter', 'MM_OH_BONDS', 'UTILITY')
      Ldebug=Local_Debug

      StypeO:select case(TypeO)
        case (0) StypeO ! O-H2
          R_ij=1.508D0
          k_ij=4.258D0
        case (10) StypeO ! O-H,C
          R_ij=1.508D0
          k_ij=4.258D0
      end select StypeO

! End of routine MM_OH_BONDS
      call PRG_manager ('exit', 'MM_OH_BONDS', 'UTILITY')
      return
      end subroutine MM_OH_BONDS
      subroutine MM_OC_BONDS (TypeO, TypeC, R_ij, K_ij)
!*****************************************************************************
! Date last modified: March 8, 2005                              Version 1.0 *
! Author: R. A. Poirier                                                      *
! Description: Get equilibrium bond lengths and force program_constants for OC bonds *
!*****************************************************************************
! Modules:

      implicit none

! Local scalars:
      integer, intent(IN) :: TypeO,TypeC
      double precision, intent(OUT) :: R_ij,K_ij
      logical Ldebug
!
! Begin:
      call PRG_manager ('enter', 'MM_OC_BONDS', 'UTILITY')
      Ldebug=Local_Debug

      StypeO:select case(TypeO)
        case (10) StypeO ! O-C,H
          R_ij=1.508D0
          k_ij=4.258D0
        case (15) StypeO ! O-C,C
          R_ij=1.508D0
          k_ij=4.258D0
        case (50) StypeO ! O=C
          R_ij=1.508D0
          k_ij=4.258D0
      end select StypeO

! End of routine MM_OC_BONDS
      call PRG_manager ('exit', 'MM_OC_BONDS', 'UTILITY')
      return
      end subroutine MM_OC_BONDS
      subroutine MM_NH_BONDS (TypeX, TypeY, R_ij, K_ij)
!*****************************************************************************
! Date last modified: March 8, 2005                              Version 1.0 *
! Author: R. A. Poirier                                                      *
! Description: Get equilibrium bond lengths and force program_constants for NH bonds *
!*****************************************************************************
! Modules:

      implicit none

! Local scalars:
      integer, intent(IN) :: TypeX,TypeY
      double precision, intent(OUT) :: R_ij,K_ij
      logical Ldebug
!
! Begin:
      call PRG_manager ('enter', 'MM_NH_BONDS', 'UTILITY')
      Ldebug=Local_Debug

      StypeX:select case(TypeX)
        case (0) StypeX ! N-H3
          R_ij=1.508D0
          k_ij=4.258D0
        case (15) StypeX ! N-H2,C
          R_ij=1.508D0
          k_ij=4.258D0
        case (25) StypeX ! N-H,C2
          R_ij=1.508D0
          k_ij=4.258D0
      end select StypeX

! End of routine MM_NH_BONDS
      call PRG_manager ('exit', 'MM_NH_BONDS', 'UTILITY')
      return
      end subroutine MM_NH_BONDS
      subroutine MM_XX_BONDS (TypeX, TypeY, R_ij, K_ij)
!*****************************************************************************
! Date last modified: March 8, 2005                              Version 1.0 *
! Author: R. A. Poirier                                                      *
! Description: Get equilibrium bond lengths and force program_constants for XX bonds *
!*****************************************************************************
! Modules:

      implicit none

! Local scalars:
      integer, intent(IN) :: TypeX,TypeY
      double precision, intent(OUT) :: R_ij,K_ij
      logical Ldebug
!
! Begin:
      call PRG_manager ('enter', 'MM_XX_BONDS', 'UTILITY')
      Ldebug=Local_Debug

      StypeX:select case(TypeX)
        case (15) StypeX
          R_ij=1.508D0
          k_ij=4.258D0
        case (25) StypeX
          R_ij=1.508D0
          k_ij=4.258D0
        case (30) StypeX
          R_ij=1.508D0
          k_ij=4.258D0
        case (100) StypeX
          R_ij=1.508D0
          k_ij=4.258D0
        case (150) StypeX
          R_ij=1.508D0
          k_ij=4.258D0
        case (500) StypeX
          R_ij=1.508D0
          k_ij=4.258D0
      end select StypeX

! End of routine MM_XX_BONDS
      call PRG_manager ('exit', 'MM_XX_BONDS', 'UTILITY')
      return
      end subroutine MM_XX_BONDS
      end module FF_parameters
