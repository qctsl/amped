      module mm_interaction_type_params
!****************************************************************************
! Date last modified: 2 February, 2000                          Version 1.0 *
! Author: Michelle Shaw                                                     *
! Description: Contains copies of objects stored in allocatable arrays for  *
!              use in MM nonbonded code.  This eliminates repeated calls to *
!              getobj for object retrieval.                                 *
!****************************************************************************
      implicit none

      integer, dimension(:,:), allocatable :: BT            ! work array

      end module mm_interaction_type_params

