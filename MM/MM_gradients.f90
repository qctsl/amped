      subroutine BLD_MM_GRADIENTS
!*****************************************************************
! Date last modified: 1 April, 2000                  Version 1.0 *
! Author: Michelle Shaw                                          *
! Description: Combine gradients for stretches, bends, and       *
!              torsions into an array called PARGRD.             *
!*****************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE mm_gradients
      USE redundant_coordinates

      implicit none
!
! Local scalars:
!
! Begin:
      call PRG_manager ('enter', 'BLD_MM_GRADIENTS', 'UTILITY')
!
! For NOW!
!     call GET_object ('MOL', 'INTERNAL', 'RIC')
      call GET_object ('MM', 'PARAMETER_CONTRIBUTIONS', 'MMFF94')

      if(RIC%NBonds.gt.0)then
        call GRADCLC_STR
      end if
      if(RIC%NAngles.gt.0)then
        call GRADCLC_BND
      end if
      if(RIC%NTorsions.gt.0)then
        call GRADCLC_TOR
      end if
      if(RIC%Noopbends.gt.0)then
        call GRADCLC_OBND
      end if
      if(RIC%Nnonbonded.gt.0)then
        call GRADCLC_VDW
        call GRADCLC_ELEC
      end if

! End of routine BLD_MM_GRADIENTS
      call PRG_manager ('exit', 'BLD_MM_GRADIENTS', 'UTILITY')
      return
      end subroutine BLD_MM_GRADIENTS
      subroutine PRT_MM_GRADIENTS
!*****************************************************************
! Date last modified: 1 April, 2000                  Version 1.0 *
! Author: Michelle Shaw                                          *
! Description: Combine gradients for stretches, bends, and       *
!              torsions into an array called PARGRD.             *
!*****************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE mm_gradients

      implicit none
!
! Local scalars:
      integer Istretch,Ibend,Istr_bend,Itorsion,Ioopbend,IVderW,Ielectrostatic
      integer Nstretchs,Nbends,Nstr_bends,Ntorsions,Noopbends,NVderWs,Nelectrostatic
!
! Begin:
      call PRG_manager ('enter', 'PRT_MM_GRADIENTS', 'UTILITY')
!
! For NOW!
      call GET_object ('MM', 'GRADIENTS', 'MMFF94')

      write(UNIout,'(a)')'MM gradients:'
     
      write(UNIout,'(a)')'MM stretch gradients:'
      Nstretchs=size(G_stretch,1)
      do Istretch=1,Nstretchs
        write(UNIout,'(i6,F14.6)')Istretch,G_stretch(Istretch)
      end do
      write(UNIout,'(a)')'MM bend gradients:'
      Nbends=size(G_bend,1)
      do Ibend=1,Nbends
        write(UNIout,'(i6,F14.6)')Ibend,G_bend(Ibend)
      end do
! Do not exist!!!!
!     write(UNIout,'(a)')'MM stretch-bend gradients:'
!     Nstr_bends=size(G_str_bend,1)
!     do Istr_bend=1,Nstr_bends
!       write(UNIout,'(i6,F14.6)')Istr_bend,G_str_bend(Istr_bend)
!     end do
      write(UNIout,'(a)')'MM torsion gradients:'
      Ntorsions=size(G_torsion,1)
      do Itorsion=1,Ntorsions
        write(UNIout,'(i6,F14.6)')Itorsion,G_torsion(Itorsion)
      end do

      if(RIC%Noopbends.gt.0)then
        write(UNIout,'(a)')'MM oopbend gradients:'
        Noopbends=size(G_oopbend,1)
        do Ioopbend=1,Noopbends
          write(UNIout,'(i6,F14.6)')Ioopbend,G_oopbend(Ioopbend)
        end do
      end if

      write(UNIout,'(a)')'MM Van der Waals gradients:'
      NVderWs=size(G_VderWs,1)
      do IVderW=1,NVderWs
        write(UNIout,'(i6,F14.6)')IVderW,G_VderWs(IVderW)
      end do
      write(UNIout,'(a)')'MM electrostatic gradients:'
      Nelectrostatic=size(G_electrostatic,1)
      do Ielectrostatic=1,Nelectrostatic
        write(UNIout,'(i6,F14.6)')Ielectrostatic,G_electrostatic(Ielectrostatic)
      end do

! End of routine PRT_MM_GRADIENTS
      call PRG_manager ('exit', 'PRT_MM_GRADIENTS', 'UTILITY')
      return
      end subroutine PRT_MM_GRADIENTS
