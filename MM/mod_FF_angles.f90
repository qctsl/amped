      module FF_parameters
!**********************************************************************************
! Date last modified: March 22, 2005                                  Version 1.0 *
! Author: R. A. Poirier                                                           *
! Description: Contains object and initialization routines for stretch parameters.*                                                       !**********************************************************************************
! Modules:
      USE program_manager
      USE program_defaults
      USE program_constants
      USE type_molecule
      USE mm_interaction_type_params
      USE nobond_params
      USE GRAPH_objects
      USE atom_type

      implicit none

      type bend_contributions
        double precision :: k_ijk !bend force constant
        double precision :: theta_ijk !equilibrium angle
      end type bend_contributions

      type (bend_contributions), dimension(:), allocatable :: MMbend

CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine GETANG_PARAMS
!*****************************************************************
! Date last modified: 23 March, 2000                 Version 1.0 *
! Author: R.A. Poirier and Michelle Shaw                         *
! Description: Obtain equilibrium angles.                        *
!*****************************************************************
! Modules:

      implicit none

! Local scalars:
      integer case_angle,Iatom,Jatom,Katom,IBend,TypeJ,TypeK,bond_ik,bond_kj
      integer Ring_size,ZatomI,ZatomJ,ZatomK,Ztemp
      double precision :: theta_ijk,k_ijk
      logical Ldebug
!
! Begin:
      call PRG_manager ('enter', 'GETANG_PARAMS', 'UTILITY')
      Ldebug=Local_Debug
!
! Allocate work arrays:
      if(.not.allocated(MMbend))then
        allocate(MMbend(NGangle))
      end if

!      ANGTYP = .false.

! Next, the equilibrium angle bend must be obtained based on the type of atoms involved in the angle.
      do IBend = 1,NGangle
        Iatom = ANGLE(IBend)%ATOM1
        Katom = ANGLE(IBend)%ATOM2 ! Katom is the central atom
        Jatom = ANGLE(IBend)%ATOM3
        ZatomI = CARTESIANS%Atomic_number(Iatom)
        if(ZatomI.eq.0)cycle
        ZatomJ = CARTESIANS%Atomic_number(Jatom)
        if(ZatomJ.eq.0)cycle
        ZatomK = CARTESIANS%Atomic_number(Katom)
        if(ZatomK.eq.0)cycle
        if(ZatomI.gt.ZatomJ)then ! Want Zj > Zi
          Ztemp=ZatomI
          ZatomI=ZatomJ
          ZatomJ=Ztemp
        end if

        Ring_size=ANGLE(IBend)%ring_size

        TypeK=vertex(Katom)%type ! Get atom type for central atom

! NOTE: 12100=110*110
        case_angle = 12100*(ZatomI-1)+110*(ZatomK-1)+(ZatomJ-1)

        theta_ijk = ZERO
        k_ijk = ZERO
        ANGLEtypeS:select case(case_angle)
          case(550) ANGLEtypeS ! H-C-H
            call HCH_ANG_PARM (TypeK, theta_ijk, k_ijk)
          case(555) ANGLEtypeS ! H-C-C
            call HCC_ANG_PARM (TypeJ, TypeK, theta_ijk, k_ijk)
          case(556) ANGLEtypeS ! H-C-N
            call HCN_ANG_PARM (TypeJ, TypeK, theta_ijk, k_ijk)
          case(557) ANGLEtypeS ! H-C-O
            call HCO_ANG_PARM (TypeJ, TypeK, theta_ijk, k_ijk)
          case(558) ANGLEtypeS ! H-C-F
            call HCF_ANG_PARM (TypeJ, TypeK, theta_ijk, k_ijk)
          case(566) ANGLEtypeS ! H-C-Cl
            call HCCl_ANG_PARM (TypeJ, TypeK, theta_ijk, k_ijk)
          case(660) ANGLEtypeS ! H-N-H
            call HNH_ANG_PARM (TypeK, theta_ijk, k_ijk)
          case(770) ANGLEtypeS ! H-O-H
            call HOH_ANG_PARM (TypeK, theta_ijk, k_ijk)
          case (61055) ANGLEtypeS ! C-C-C
            select case (Ring_size)
              case (0)
                call CCC_ANGLES_R0 (TypeK, theta_ijk, k_ijk)
              case (3)
                call CCC_ANGLES_R3 (TypeK, theta_ijk, k_ijk)
              case (4)
                call CCC_ANGLES_R4 (TypeK, theta_ijk, k_ijk)
              case (5)
                call CCC_ANGLES_R5 (TypeK, theta_ijk, k_ijk)
              case (6)
                call CCC_ANGLES_R6 (TypeK, theta_ijk, k_ijk)
              case (7)
                call CCC_ANGLES_R7 (TypeK, theta_ijk, k_ijk)
              case (8)
                call CCC_ANGLES_R8 (TypeK, theta_ijk, k_ijk)
              end select 
          case (61056) ANGLEtypeS ! C-C-N
            select case (Ring_size)
              case (0)
                call CCN_ANGLES_R0 (TypeK, theta_ijk, k_ijk)
              case (3)
                call CCN_ANGLES_R3 (TypeK, theta_ijk, k_ijk)
              case (4)
                call CCN_ANGLES_R4 (TypeK, theta_ijk, k_ijk)
              case (5)
                call CCN_ANGLES_R5 (TypeK, theta_ijk, k_ijk)
              case (6)
                call CCN_ANGLES_R6 (TypeK, theta_ijk, k_ijk)
              case (7)
                call CCN_ANGLES_R7 (TypeK, theta_ijk, k_ijk)
              case (8)
                call CCN_ANGLES_R8 (TypeK, theta_ijk, k_ijk)
              end select 
          case (61057) ANGLEtypeS ! C-C-O
            select case (Ring_size)
              case (0)
                call CCO_ANGLES_R0 (TypeK, theta_ijk, k_ijk)
              case (3)
                call CCO_ANGLES_R3 (TypeK, theta_ijk, k_ijk)
              case (4)
                call CCO_ANGLES_R4 (TypeK, theta_ijk, k_ijk)
              case (5)
                call CCO_ANGLES_R5 (TypeK, theta_ijk, k_ijk)
              case (6)
                call CCO_ANGLES_R6 (TypeK, theta_ijk, k_ijk)
              case (7)
                call CCO_ANGLES_R7 (TypeK, theta_ijk, k_ijk)
              case (8)
                call CCO_ANGLES_R8 (TypeK, theta_ijk, k_ijk)
              end select 
          case (61058) ANGLEtypeS ! C-C-F
            call CCF_ANGLES (TypeK, theta_ijk, k_ijk)
          case (61066) ANGLEtypeS ! C-C-Cl
            call CCCl_ANGLES (TypeK, theta_ijk, k_ijk)
          case DEFAULT ANGLEtypeS
            write(UNIout,'(a)')'GETANG_PARAMS> ERROR'
            write(UNIout,'(a)')'Atom types not supported or are dummies.'
            call PRG_stop ('Atom types not supported or are dummies')
        end select ANGLEtypeS
        MMbend(IBend)%theta_ijk=theta_ijk
        MMbend(IBend)%k_ijk=k_ijk
!        if(MMbend(IBend)%theta_ijk.ge.(175.0D0*Deg_to_Radian).and.
!    .      MMbend(IBend)%theta_ijk.le.PI_VAL)then
!          ANGTYP(IBend) = .true.
!        end if
! Printing for debugging and things that are not accessible via an output statement:
!        if(Ldebug)then
          write(UNIout,'(a,f12.6)')'equilibrium angle: ',MMbend(IBend)%theta_ijk
          write(UNIout,'(a,f12.6)')'angle force constant: ',MMbend(IBend)%k_ijk
!        end if
      end do

      if(Ldebug)then
        write(UNIout,'(a,i8)')'debug_GETANG_PARAMS > NGangle: ',NGangle
      end if

! End of routine GETANG_PARAMS
      call PRG_manager ('exit', 'GETANG_PARAMS', 'UTILITY')
      return
      end subroutine GETANG_PARAMS
      subroutine CCC_ANGLES_R0 (TypeK, & !type for atom k (central atom)
                               theta_ijk, k_ijk)
!***************************************************************************
! Date last modified: 24 August, 1999                          Version 1.0 *
! Author: R.A. Poirier                                                     *
! Description: Get equilibrium angles and force program_constants for a CCC angle. *
!              Not part of a ring                                          *
!***************************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer :: TypeK
      double precision :: k_ijk,theta_ijk

! Local scalars:
      logical Ldebug
!
! Begin:
! This is where the parameters are selected for both equilibrium angles and force program_constants.
! All values simply defaulted to 1-1-1 case
      StypeK:select case (TypeK)
        case(35) StypeK ! H2-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(36) StypeK ! O=CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(45) StypeK ! H,C-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(50) StypeK ! C2-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(53) StypeK ! H,O-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(58) StypeK ! C,O-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(250) StypeK ! H-CC=C or aromatic
          theta_ijk=109.608
          k_ijk=0.851
        case(300) StypeK ! C-CC=C or aromatic
          theta_ijk=109.608
          k_ijk=0.851
        case(1500) StypeK ! CC-(3)-C
          theta_ijk=109.608
          k_ijk=0.851
      case DEFAULT StypeK
         call PRG_stop ('CCC_ANGLES_R0> iATOM: Illegal force field atom type!')
      end select StypeK

! End of function CCC_ANGLES_R0
      return
      end subroutine CCC_ANGLES_R0
      subroutine CCC_ANGLES_R3 (TypeK, & !type for atom k (central atom)
                               theta_ijk, k_ijk)
!***************************************************************************
! Date last modified: 24 August, 1999                          Version 1.0 *
! Author: R.A. Poirier                                                     *
! Description: Get equilibrium angles and force program_constants for a CCC angle. *
!              Part of a 3-membered ring                                   *
!***************************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer :: TypeK
      double precision :: k_ijk,theta_ijk

! Local scalars:
      logical Ldebug
!
! Begin:
! This is where the parameters are selected for both equilibrium angles and force program_constants.
! All values simply defaulted to 1-1-1 case
      StypeK:select case (TypeK)
        case(35) StypeK ! H2-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(36) StypeK ! O=CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(45) StypeK ! H,C-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(50) StypeK ! C2-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(53) StypeK ! H,O-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(58) StypeK ! C,O-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(250) StypeK ! H-CC=C or aromatic
          theta_ijk=109.608
          k_ijk=0.851
        case(300) StypeK ! C-CC=C or aromatic
          theta_ijk=109.608
          k_ijk=0.851
      case DEFAULT StypeK
         call PRG_stop ('CCC_ANGLES_R3> iATOM: Illegal force field atom type!')
      end select StypeK

! End of function CCC_ANGLES_R3
      return
      end subroutine CCC_ANGLES_R3
      subroutine CCC_ANGLES_R4 (TypeK, & !type for atom k (central atom)
                               theta_ijk, k_ijk)
!***************************************************************************
! Date last modified: 24 August, 1999                          Version 1.0 *
! Author: R.A. Poirier                                                     *
! Description: Get equilibrium angles and force program_constants for a CCC angle. *
!              Part of a 4-membered ring                                   *
!***************************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer :: TypeK
      double precision :: k_ijk,theta_ijk

! Local scalars:
      logical Ldebug
!
! Begin:
! This is where the parameters are selected for both equilibrium angles and force program_constants.
! All values simply defaulted to 1-1-1 case
      StypeK:select case (TypeK)
        case(35) StypeK ! H2-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(36) StypeK ! O=CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(45) StypeK ! H,C-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(50) StypeK ! C2-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(53) StypeK ! H,O-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(58) StypeK ! C,O-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(250) StypeK ! H-CC=C or aromatic
          theta_ijk=109.608
          k_ijk=0.851
        case(300) StypeK ! C-CC=C or aromatic
          theta_ijk=109.608
          k_ijk=0.851
        case(1500) StypeK ! CC-(3)-C
          theta_ijk=109.608
          k_ijk=0.851
      case DEFAULT StypeK
         call PRG_stop ('CCC_ANGLES_R4> iATOM: Illegal force field atom type!')
      end select StypeK

! End of function CCC_ANGLES_R4
      return
      end subroutine CCC_ANGLES_R4
      subroutine CCC_ANGLES_R5 (TypeK, & !type for atom k (central atom)
                               theta_ijk, k_ijk)
!***************************************************************************
! Date last modified: 24 August, 1999                          Version 1.0 *
! Author: R.A. Poirier                                                     *
! Description: Get equilibrium angles and force program_constants for a CCC angle. *
!              Part of a 5-membered ring                                   *
!***************************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer :: TypeK
      double precision :: k_ijk,theta_ijk

! Local scalars:
      logical Ldebug
!
! Begin:
! This is where the parameters are selected for both equilibrium angles and force program_constants.
! All values simply defaulted to 1-1-1 case
      StypeK:select case (TypeK)
        case(35) StypeK ! H2-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(36) StypeK ! O=CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(45) StypeK ! H,C-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(50) StypeK ! C2-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(53) StypeK ! H,O-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(58) StypeK ! C,O-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(250) StypeK ! H-CC=C or aromatic
          theta_ijk=109.608
          k_ijk=0.851
        case(300) StypeK ! C-CC=C or aromatic
          theta_ijk=109.608
          k_ijk=0.851
        case(1500) StypeK ! CC-(3)-C
          theta_ijk=109.608
          k_ijk=0.851
      case DEFAULT StypeK
         call PRG_stop ('CCC_ANGLES_R5> iATOM: Illegal force field atom type!')
      end select StypeK

! End of function CCC_ANGLES_R5
      return
      end subroutine CCC_ANGLES_R5
      subroutine CCC_ANGLES_R6 (TypeK, & !type for atom k (central atom)
                               theta_ijk, k_ijk)
!***************************************************************************
! Date last modified: 24 August, 1999                          Version 1.0 *
! Author: R.A. Poirier                                                     *
! Description: Get equilibrium angles and force program_constants for a CCC angle. *
!              Part of a 6-membered ring                                   *
!***************************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer :: TypeK
      double precision :: k_ijk,theta_ijk

! Local scalars:
      logical Ldebug
!
! Begin:
! This is where the parameters are selected for both equilibrium angles and force program_constants.
! All values simply defaulted to 1-1-1 case
      StypeK:select case (TypeK)
        case(35) StypeK ! H2-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(36) StypeK ! O=CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(45) StypeK ! H,C-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(50) StypeK ! C2-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(53) StypeK ! H,O-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(58) StypeK ! C,O-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(250) StypeK ! H-CC=C or aromatic
          theta_ijk=109.608
          k_ijk=0.851
        case(300) StypeK ! C-CC=C or aromatic
          theta_ijk=109.608
          k_ijk=0.851
        case(1500) StypeK ! CC-(3)-C
          theta_ijk=109.608
          k_ijk=0.851
      case DEFAULT StypeK
         call PRG_stop ('CCC_ANGLES_R6> iATOM: Illegal force field atom type!')
      end select StypeK

! End of function CCC_ANGLES_R6
      return
      end subroutine CCC_ANGLES_R6
      subroutine CCC_ANGLES_R7 (TypeK, & !type for atom k (central atom)
                               theta_ijk, k_ijk)
!***************************************************************************
! Date last modified: 24 August, 1999                          Version 1.0 *
! Author: R.A. Poirier                                                     *
! Description: Get equilibrium angles and force program_constants for a CCC angle. *
!              Part of a 7-membered ring                                   *
!***************************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer :: TypeK
      double precision :: k_ijk,theta_ijk

! Local scalars:
      logical Ldebug
!
! Begin:
! This is where the parameters are selected for both equilibrium angles and force program_constants.
! All values simply defaulted to 1-1-1 case
      StypeK:select case (TypeK)
        case(35) StypeK ! H2-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(36) StypeK ! O=CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(45) StypeK ! H,C-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(50) StypeK ! C2-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(53) StypeK ! H,O-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(58) StypeK ! C,O-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(250) StypeK ! H-CC=C or aromatic
          theta_ijk=109.608
          k_ijk=0.851
        case(300) StypeK ! C-CC=C or aromatic
          theta_ijk=109.608
          k_ijk=0.851
        case(1500) StypeK ! CC-(3)-C
          theta_ijk=109.608
          k_ijk=0.851
      case DEFAULT StypeK
         call PRG_stop ('CCC_ANGLES_R7> iATOM: Illegal force field atom type!')
      end select StypeK

! End of function CCC_ANGLES_R7
      return
      end subroutine CCC_ANGLES_R7
      subroutine CCC_ANGLES_R8 (TypeK, & !type for atom k (central atom)
                               theta_ijk, k_ijk)
!***************************************************************************
! Date last modified: 24 August, 1999                          Version 1.0 *
! Author: R.A. Poirier                                                     *
! Description: Get equilibrium angles and force program_constants for a CCC angle. *
!              Part of a 8-membered ring                                   *
!***************************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer :: TypeK
      double precision :: k_ijk,theta_ijk

! Local scalars:
      logical Ldebug
!
! Begin:
! This is where the parameters are selected for both equilibrium angles and force program_constants.
! All values simply defaulted to 1-1-1 case
      StypeK:select case (TypeK)
        case(35) StypeK ! H2-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(36) StypeK ! O=CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(45) StypeK ! H,C-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(50) StypeK ! C2-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(53) StypeK ! H,O-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(58) StypeK ! C,O-CCC
          theta_ijk=109.608
          k_ijk=0.851
        case(250) StypeK ! H-CC=C or aromatic
          theta_ijk=109.608
          k_ijk=0.851
        case(300) StypeK ! C-CC=C or aromatic
          theta_ijk=109.608
          k_ijk=0.851
        case(1500) StypeK ! CC-(3)-C
          theta_ijk=109.608
          k_ijk=0.851
      case DEFAULT StypeK
         call PRG_stop ('CCC_ANGLES_R8> iATOM: Illegal force field atom type!')
      end select StypeK

! End of function CCC_ANGLES_R8
      return
      end subroutine CCC_ANGLES_R8
      subroutine CCO_ANGLES_R0 (TypeK, & !type for atom k (central atom)
                               theta_ijk, k_ijk)
!***************************************************************************
! Date last modified: 24 August, 1999                          Version 1.0 *
! Author: R.A. Poirier                                                     *
! Description: Get equilibrium angles and force program_constants for a CCO angle. *
!              Not part of a ring                                          *
!***************************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer :: TypeK
      double precision :: k_ijk,theta_ijk

! Local scalars:
      logical Ldebug
!
! Begin:
! This is where the parameters are selected for both equilibrium angles and force program_constants.
! All values simply defaulted to 1-1-1 case
      StypeK:select case (TypeK)
        case(43) StypeK ! H2-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(400) StypeK ! O=CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(53) StypeK ! H,C-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(58) StypeK ! C2-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(59) StypeK ! H,O-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(64) StypeK ! C,O-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(310) StypeK ! H-CC=O or H-OC=C
          theta_ijk=109.608
          k_ijk=0.851
        case(360) StypeK ! C-OC=C or C-CC=O
          theta_ijk=109.608
          k_ijk=0.851
        case(1900) StypeK ! OC-(3)-C
          theta_ijk=109.608
          k_ijk=0.851
      case DEFAULT StypeK
         call PRG_stop ('CCO_ANGLES_R0> iATOM: Illegal force field atom type!')
      end select StypeK

! End of function CCO_ANGLES_R0
      return
      end subroutine CCO_ANGLES_R0
      subroutine CCO_ANGLES_R3 (TypeK, & !type for atom k (central atom)
                               theta_ijk, k_ijk)
!***************************************************************************
! Date last modified: 24 August, 1999                          Version 1.0 *
! Author: R.A. Poirier                                                     *
! Description: Get equilibrium angles and force program_constants for a CCO angle. *
!              Part of a 3-membered ring                                   *
!***************************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer :: TypeK
      double precision :: k_ijk,theta_ijk

! Local scalars:
      logical Ldebug
!
! Begin:
! This is where the parameters are selected for both equilibrium angles and force program_constants.
! All values simply defaulted to 1-1-1 case
      StypeK:select case (TypeK)
        case(43) StypeK ! H2-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(400) StypeK ! O=CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(53) StypeK ! H,C-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(58) StypeK ! C2-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(59) StypeK ! H,O-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(64) StypeK ! C,O-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(310) StypeK ! H-CC=O or H-OC=C
          theta_ijk=109.608
          k_ijk=0.851
        case(360) StypeK ! C-OC=C or C-CC=O
          theta_ijk=109.608
          k_ijk=0.851
        case(1900) StypeK ! OC-(3)-C
          theta_ijk=109.608
          k_ijk=0.851
      case DEFAULT StypeK
         call PRG_stop ('CCO_ANGLES_R3> iATOM: Illegal force field atom type!')
      end select StypeK

! End of function CCO_ANGLES_R3
      return
      end subroutine CCO_ANGLES_R3
      subroutine CCO_ANGLES_R4 (TypeK, & !type for atom k (central atom)
                               theta_ijk, k_ijk)
!***************************************************************************
! Date last modified: 24 August, 1999                          Version 1.0 *
! Author: R.A. Poirier                                                     *
! Description: Get equilibrium angles and force program_constants for a CCO angle. *
!              Part of a 4-membered ring                                   *
!***************************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer :: TypeK
      double precision :: k_ijk,theta_ijk

! Local scalars:
      logical Ldebug
!
! Begin:
! This is where the parameters are selected for both equilibrium angles and force program_constants.
! All values simply defaulted to 1-1-1 case
      StypeK:select case (TypeK)
        case(43) StypeK ! H2-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(400) StypeK ! O=CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(53) StypeK ! H,C-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(58) StypeK ! C2-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(59) StypeK ! H,O-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(64) StypeK ! C,O-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(310) StypeK ! H-CC=O or H-OC=C
          theta_ijk=109.608
          k_ijk=0.851
        case(360) StypeK ! C-OC=C or C-CC=O
          theta_ijk=109.608
          k_ijk=0.851
        case(1900) StypeK ! OC-(3)-C
          theta_ijk=109.608
          k_ijk=0.851
      case DEFAULT StypeK
         call PRG_stop ('CCO_ANGLES_R4> iATOM: Illegal force field atom type!')
      end select StypeK

! End of function CCO_ANGLES_R4
      return
      end subroutine CCO_ANGLES_R4
      subroutine CCO_ANGLES_R5 (TypeK, & !type for atom k (central atom)
                               theta_ijk, k_ijk)
!***************************************************************************
! Date last modified: 24 August, 1999                          Version 1.0 *
! Author: R.A. Poirier                                                     *
! Description: Get equilibrium angles and force program_constants for a CCO angle. *
!              Part of a 5-membered ring                                   *
!***************************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer :: TypeK
      double precision :: k_ijk,theta_ijk

! Local scalars:
      logical Ldebug
!
! Begin:
! This is where the parameters are selected for both equilibrium angles and force program_constants.
! All values simply defaulted to 1-1-1 case
      StypeK:select case (TypeK)
        case(43) StypeK ! H2-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(400) StypeK ! O=CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(53) StypeK ! H,C-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(58) StypeK ! C2-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(59) StypeK ! H,O-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(64) StypeK ! C,O-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(310) StypeK ! H-CC=O or H-OC=C
          theta_ijk=109.608
          k_ijk=0.851
        case(360) StypeK ! C-OC=C or C-CC=O
          theta_ijk=109.608
          k_ijk=0.851
        case(1900) StypeK ! OC-(3)-C
          theta_ijk=109.608
          k_ijk=0.851
      case DEFAULT StypeK
         call PRG_stop ('CCO_ANGLES_R5> iATOM: Illegal force field atom type!')
      end select StypeK

! End of function CCO_ANGLES_R5
      return
      end subroutine CCO_ANGLES_R5
      subroutine CCO_ANGLES_R6 (TypeK, & !type for atom k (central atom)
                               theta_ijk, k_ijk)
!***************************************************************************
! Date last modified: 24 August, 1999                          Version 1.0 *
! Author: R.A. Poirier                                                     *
! Description: Get equilibrium angles and force program_constants for a CCO angle. *
!              Part of a 6-membered ring                                   *
!***************************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer :: TypeK
      double precision :: k_ijk,theta_ijk

! Local scalars:
      logical Ldebug
!
! Begin:
! This is where the parameters are selected for both equilibrium angles and force program_constants.
! All values simply defaulted to 1-1-1 case
      StypeK:select case (TypeK)
        case(43) StypeK ! H2-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(400) StypeK ! O=CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(53) StypeK ! H,C-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(58) StypeK ! C2-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(59) StypeK ! H,O-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(64) StypeK ! C,O-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(310) StypeK ! H-CC=O or H-OC=C
          theta_ijk=109.608
          k_ijk=0.851
        case(360) StypeK ! C-OC=C or C-CC=O
          theta_ijk=109.608
          k_ijk=0.851
        case(1900) StypeK ! OC-(3)-C
          theta_ijk=109.608
          k_ijk=0.851
      case DEFAULT StypeK
         call PRG_stop ('CCO_ANGLES_R6> iATOM: Illegal force field atom type!')
      end select StypeK

! End of function CCO_ANGLES_R6
      return
      end subroutine CCO_ANGLES_R6
      subroutine CCO_ANGLES_R7 (TypeK, & !type for atom k (central atom)
                               theta_ijk, k_ijk)
!***************************************************************************
! Date last modified: 24 August, 1999                          Version 1.0 *
! Author: R.A. Poirier                                                     *
! Description: Get equilibrium angles and force program_constants for a CCO angle. *
!              Part of a 7-membered ring                                   *
!***************************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer :: TypeK
      double precision :: k_ijk,theta_ijk

! Local scalars:
      logical Ldebug
!
! Begin:
! This is where the parameters are selected for both equilibrium angles and force program_constants.
! All values simply defaulted to 1-1-1 case
      StypeK:select case (TypeK)
        case(43) StypeK ! H2-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(400) StypeK ! O=CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(53) StypeK ! H,C-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(58) StypeK ! C2-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(59) StypeK ! H,O-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(64) StypeK ! C,O-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(310) StypeK ! H-CC=O or H-OC=C
          theta_ijk=109.608
          k_ijk=0.851
        case(360) StypeK ! C-OC=C or C-CC=O
          theta_ijk=109.608
          k_ijk=0.851
        case(1900) StypeK ! OC-(3)-C
          theta_ijk=109.608
          k_ijk=0.851
      case DEFAULT StypeK
         call PRG_stop ('CCO_ANGLES_R7> iATOM: Illegal force field atom type!')
      end select StypeK

! End of function CCO_ANGLES_R7
      return
      end subroutine CCO_ANGLES_R7
      subroutine CCO_ANGLES_R8 (TypeK, & !type for atom k (central atom)
                               theta_ijk, k_ijk)
!***************************************************************************
! Date last modified: 24 August, 1999                          Version 1.0 *
! Author: R.A. Poirier                                                     *
! Description: Get equilibrium angles and force program_constants for a CCO angle. *
!              Part of a 8-membered ring                                   *
!***************************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer :: TypeK
      double precision :: k_ijk,theta_ijk

! Local scalars:
      logical Ldebug
!
! Begin:
! This is where the parameters are selected for both equilibrium angles and force program_constants.
! All values simply defaulted to 1-1-1 case
      StypeK:select case (TypeK)
        case(43) StypeK ! H2-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(400) StypeK ! O=CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(53) StypeK ! H,C-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(58) StypeK ! C2-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(59) StypeK ! H,O-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(64) StypeK ! C,O-CCO
          theta_ijk=109.608
          k_ijk=0.851
        case(310) StypeK ! H-CC=O or H-OC=C
          theta_ijk=109.608
          k_ijk=0.851
        case(360) StypeK ! C-OC=C or C-CC=O
          theta_ijk=109.608
          k_ijk=0.851
        case(1900) StypeK ! OC-(3)-C
          theta_ijk=109.608
          k_ijk=0.851
      case DEFAULT StypeK
         call PRG_stop ('CCO_ANGLES_R8> iATOM: Illegal force field atom type!')
      end select StypeK

! End of function CCO_ANGLES_R8
      return
      end subroutine CCO_ANGLES_R8
      end module FF_parameters
