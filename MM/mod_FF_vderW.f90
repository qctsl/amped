      module FF_parameters
!**********************************************************************************
! Date last modified: March 22, 2005                                  Version 1.0 *
! Author: R. A. Poirier                                                           *
! Description: Contains object and initialization routines for stretch parameters.*                                                       !**********************************************************************************
! Modules:
      USE program_manager
      USE program_defaults
      USE program_constants
      USE type_molecule
      USE mm_interaction_type_params
      USE nobond_params
      USE GRAPH_objects
      USE atom_type

      implicit none

      type vanderwaals_contributions
        double precision :: R_IJ, epsilon_IJ
      end type vanderwaals_contributions

      type (vanderwaals_contributions), dimension(:), allocatable :: MMVderW

CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine GETVDW_PARAMS
!*************************************************************************
! Date last modified: 26 March, 2000                         Version 1.0 *
! Author: Michelle Shaw                                                  *
! Description: Calculate the minimum energy separation and well depth    *
!              to be used in the van der Waals energy determination.     *
!*************************************************************************
! formulas are based on MMFF94 force field, see :
!        Halgren, T. A., J. Comp. Chem, vol. 17 (5&6) p. 520 (1996)
!
! Local scalars:
      integer iELEC,Iatom,Jatom
      double precision num,denom,G_I,N_I,G_J,N_J,alpha_I,alpha_J,A_I,A_J,sum_IJ,gamma_IJ,R_II,R_JJ
      logical Ldebug
!
! Begin:
      call PRG_manager ('enter', 'GETVDW_PARAMS', 'UTILITY')
      Ldebug=Local_Debug
!
! Allocate work arrays:
      if(.not.allocated(MMVderW))then
        allocate (MMVderW(max0(1,Nnonbond)))
      end if

! Call subroutine minE_sep to compute the minimum energy separations and calculate the well depth.
! Place both into the object MMVderW for use in the energy expression.
      do iELEC = 1,Nnonbond
        Iatom = pair_IJ(iELEC)%I
        Jatom = pair_IJ(iELEC)%J
        if(pair_IJ(iELEC)%is_vdw.eq.1)then
          PARMI:select case(vertex(Iatom)%type)
            case(1) PARMI
              A_I = A_scalef(1)
              alpha_I = alpha(1)
              G_I = G_scalef(1)
              N_I = N_valE(1)
            case(2) PARMI
              A_I = A_scalef(2)
              G_I = G_scalef(2)
              N_I = N_valE(2)
              alpha_I = alpha(2)
            case(4) PARMI
              A_I = A_scalef(3)
              G_I = G_scalef(3)
              N_I = N_valE(3)
              alpha_I = alpha(3)
            case(5) PARMI
              A_I = A_scalef(4)
              G_I = G_scalef(4)
              N_I = N_valE(4)
              alpha_I = alpha(4)
            case(20) PARMI
              A_I = A_scalef(5)
              G_I = G_scalef(5)
              N_I = N_valE(5)
              alpha_I = alpha(5)
            case(22) PARMI
              A_I = A_scalef(6)
              G_I = G_scalef(6)
              N_I = N_valE(6)
              alpha_I = alpha(6)
            case(30) PARMI
              A_I = A_scalef(7)
              G_I = G_scalef(7)
              N_I = N_valE(7)
              alpha_I = alpha(7)
            case(37) PARMI
              A_I = A_scalef(8)
              G_I = G_scalef(8)
              N_I = N_valE(8)
              alpha_I = alpha(8)
            case DEFAULT PARMI
              write(UNIout,'(a)')'Wrong atom type!'
          end select PARMI
          PARMJ:select case(vertex(Jatom)%type)
            case(1) PARMJ
              A_J = A_scalef(1)
              G_J = G_scalef(1)
              N_J = N_valE(1)
              alpha_J = alpha(1)
            case(2) PARMJ
              A_J = A_scalef(2)
              G_J = G_scalef(2)
              N_J = N_valE(2)
              alpha_J = alpha(2)
            case(4) PARMJ
              A_J = A_scalef(3)
              G_J = G_scalef(3)
              N_J = N_valE(3)
              alpha_J = alpha(3)
            case(5) PARMJ
              A_J = A_scalef(4)
              G_J = G_scalef(4)
              N_J = N_valE(4)
              alpha_J = alpha(4)
            case(20) PARMJ
              A_J = A_scalef(5)
              G_J = G_scalef(5)
              N_J = N_valE(5)
              alpha_J = alpha(5)
            case(22) PARMJ
              A_J = A_scalef(6)
              G_J = G_scalef(6)
              N_J = N_valE(6)
              alpha_J = alpha(6)
            case(30) PARMJ
              A_J = A_scalef(7)
              G_J = G_scalef(7)
              N_J = N_valE(7)
              alpha_J = alpha(7)
            case(37) PARMJ
              A_J = A_scalef(8)
              G_J = G_scalef(8)
              N_J = N_valE(8)
              alpha_J = alpha(8)
            case DEFAULT PARMJ
              write(UNIout,'(a)')'Wrong atom type!'
          end select PARMJ
!          if(Ldebug)then
            write(UNIout,'(a,i8)')'atom type of I: ',vertex(Iatom)%type
            write(UNIout,'(a,4f12.6)')'debug_GETVDW_PARAMS > A_I, N_I, G_I, alpha_I: ',A_I,N_I,G_I,alpha_I
            write(UNIout,'(a,i8)')'atom type of J: ',vertex(Jatom)%type
            write(UNIout,'(a,4f12.6)')'debug_GETVDW_PARAMS > A_J, N_J, G_J, alpha_J: ',A_J,N_J,G_J,alpha_J
!          end if
! R_IJ calculation:
          R_II = A_I*(alpha_I**0.25D0)
          R_JJ = A_J*(alpha_J**0.25D0)
          if(R_II.eq.R_JJ)then
            MMVderW(iELEC)%R_IJ = R_II
          else
            sum_IJ = (R_II + R_JJ)
            gamma_IJ = (R_II - R_JJ)/sum_IJ
            MMVderW(iELEC)%R_IJ = 0.5D0*sum_IJ*(ONE + B_scalef*(ONE-dexp(-beta*(gamma_IJ**2))))
          end if
! epsilon_IJ calculation:
          num = 181.16D0*G_I*G_J*alpha_I*alpha_J
          denom = ( dsqrt(alpha_I/N_I) + dsqrt(alpha_J/N_J))*(MMVderW(iELEC)%R_IJ**6)

          MMVderW(iELEC)%epsilon_IJ = num/denom
        else
          MMVderW(iELEC)%R_IJ = ZERO
          MMVderW(iELEC)%epsilon_IJ = ZERO
        end if
! Printing for debugging and things that are not accessible via an output statement:
         if(Ldebug)then
          write(UNIout,'(a,f12.6)')'minimum energy separation: ',MMVderW(iELEC)%R_IJ
          write(UNIout,'(a,f20.12)')'Well depth: ',MMVderW(iELEC)%epsilon_IJ
         end if
      end do

! End of routine GETVDW_PARAMS
      call PRG_manager ('exit', 'GETVDW_PARAMS', 'UTILITY')
      return
      end subroutine GETVDW_PARAMS
      end module FF_parameters
