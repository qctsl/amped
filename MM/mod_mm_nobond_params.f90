      module nobond_params
!****************************************************************
! Date last modified: 12 December 1999              Version 1.0 *
! Author: Michelle Shaw                                         *
! Description: Nonbonded parameters for Hydrocarbons.           *
!****************************************************************
! Modules:
      USE program_constants

      implicit none

! van der Waals parameters:
      double precision, dimension(8) :: alpha
      double precision, dimension(8) :: A_scalef
      double precision, dimension(8) :: G_scalef
      double precision, dimension(8) :: N_valE
      double precision, parameter :: B_scalef = 0.2D0
      double precision, parameter :: beta = 12.0D0

! scale factors for use with donor-acceptor interactions:
!      double precision, parameter :: DARAD = 0.8D0
!      double precision, parameter :: DAEPS = 0.5D0

! Electrostatic parameters:
      double precision, dimension(36) :: bci_ki ! bond charge increments for k-i interactions
      double precision, dimension(8) :: fchg
      double precision, dimension(8) :: pbci
      double precision, parameter :: scalef_14 = 0.75D0
      double precision, parameter :: delta = 0.05D0  ! angstroms
      double precision, parameter :: dielec_const = 1.0D0
      double precision, parameter :: n = 1.0D0

!                C(sp3)  C(sp2)  C(sp)   H       C(4M)   C(3M)   C(O4M)  C(ar)
      data alpha/1.050,  1.350,  1.300,  0.250,  1.050,  1.100,  1.350,  1.350/

!                   C(sp3)  C(sp2)  C(sp)   H       C(4M)   C(3M)   C(O4M)  C(ar)
      data A_scalef/3.890,  3.890,  3.890,  4.200,  3.890,  3.890,  3.890,  3.890/

!                   C(sp3)  C(sp2)  C(sp)   H       C(4M)   C(3M)   C(O4M)  C(ar)
      data G_scalef/1.282,  1.282,  1.282,  1.209,  1.282,  1.282,  1.282,  1.282/

!                 C(sp3)  C(sp2)  C(sp)   H       C(4M)   C(3M)   C(O4M)  C(ar)
      data N_valE/2.490,  2.490,  2.490,  0.800,  2.490,  2.490,  2.490,  2.490/

!                  C(sp3)   C(sp2)   C(sp)    H        C(4M)    C(3M)   C(O4M)    C(ar)
      data bci_ki/ 0.0000, -0.1382, -0.2000,  0.0000,  0.0000, -0.0950,  0.0000, -0.1435,  & !C(sp3)
                            0.0000, -0.0650,  0.1500,  0.1160,  0.0400, -0.0310,  0.0284,  & !C(sp2)
                                     0.0000,  0.1770,  0.1810,  0.1050,  0.0340,  0.0730,  & !C(sp)
                                              0.0000,  0.0000, -0.1000, -0.1500, -0.1500,  & !H
                                                       0.0000, -0.0760, -0.1380, -0.1080,  & !C(4M)
                                                                0.0000, -0.0710, -0.0320,  & !C(3M)
                                                                         0.0000,  0.0000,  & !C(O4M)
                                                                                  0.0000/ !C(ar)

!                C(sp3) C(sp2)  C(sp)   H       C(4M)   C(3M)   C(O4M)  C(ar)
      data fchg/0.000,  0.000,  0.000,  0.000,  0.000,  0.000,  0.000,  0.000/

!                C(sp3) C(sp2)  C(sp)   H       C(4M)   C(3M)   C(O4M)  C(ar)
      data pbci/0.000, -0.135, -0.200, -0.023, -0.019, -0.095, -0.166, -0.127/

      end module nobond_params
