      subroutine Hessian_MM
!**************************************************************************
! Date last modified: 1 April, 2000                           Version 1.0 *
! Author: Michelle Shaw                                                   *
! Description: Combine second derivatives for stretches, bends, and       *
!              torsions into an object called Hessian_Matrix.             *
!**************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE mm_second_derivatives
      USE mod_type_hessian
      USE OPT_defaults
      USE OPT_objects
      USE matrix_print

      implicit none
!
! Local scalars:
      integer Ioptpr,Iricor,Ninternal,Ntotal
!
! Local arrays:
      double precision, dimension(:,:), allocatable :: Hxyz
      double precision, dimension(:,:), allocatable :: HRIC
!
! Begin:
      call PRG_manager ('enter', 'Hessian_MM', 'UTILITY')

      if(OPT_coord%nonbonded)then
        Ninternal=RIC%NIcoordinates
      else
        Ninternal=RIC%NIcoordinates-RIC%Nnonbonded
      end if

! First generate the Hessian in RIC
      allocate(HRIC(Ninternal,Ninternal))

      call BLD_MM_2ND_DERIVATIVE

      HRIC(1:Ninternal,1:Ninternal)=ZERO
!
      Ntotal=0
      if(RIC%Nbonds.gt.0)then
      HRIC(1:RIC%Nbonds,1:RIC%Nbonds)=MM_2nd_derivative(1:RIC%Nbonds,1:RIC%Nbonds)
      Ntotal=Ntotal+RIC%Nbonds
      end if

      if(RIC%Nangles.gt.0)then
      HRIC(Ntotal+1:Ntotal+RIC%Nangles,Ntotal+1:Ntotal+RIC%Nangles)= &
                          MM_2nd_derivative(Ntotal+1:Ntotal+RIC%Nangles,Ntotal+1:Ntotal+RIC%Nangles)
      Ntotal=Ntotal+RIC%Nangles
      end if

      if(RIC%Ntorsions.gt.0)then
      HRIC(Ntotal+1:Ntotal+RIC%Ntorsions,Ntotal+1:Ntotal+RIC%Ntorsions)= &
                          MM_2nd_derivative(Ntotal+1:Ntotal+RIC%Ntorsions,Ntotal+1:Ntotal+RIC%Ntorsions)
      Ntotal=Ntotal+RIC%Ntorsions
      end if

      if(RIC%Noopbends.gt.0)then
      HRIC(Ntotal+1:Ntotal+RIC%Noopbends,Ntotal+1:Ntotal+RIC%Noopbends)= &
                          MM_2nd_derivative(Ntotal+1:Ntotal+RIC%Noopbends,Ntotal+1:Ntotal+RIC%Noopbends)
      Ntotal=Ntotal+RIC%Noopbends
      end if

      if(RIC%Nnonbonded.gt.0.and.OPT_coord%nonbonded)then
      HRIC(Ntotal+1:Ntotal+RIC%Nnonbonded,Ntotal+1:Ntotal+RIC%Nnonbonded)= &
                          MM_2nd_derivative(Ntotal+1:Ntotal+RIC%Nnonbonded,Ntotal+1:Ntotal+RIC%Nnonbonded)
      Ntotal=Ntotal+RIC%nnonbonded
      end if

      if(Ntotal.ne.Ninternal)then
        write(UNIout,'(a)')'Hessian_MM> ERROR: Ntotal .ne. Ninternal'
        write(UNIout,'(a,i8)')'Hessian_MM> Ntotal: ',Ntotal
        write(UNIout,'(a,i8)')'Hessian_MM> Ninternal: ',Ninternal
        stop 'Hessian_MM> ERROR: Ntotal .ne. Ninternal'
      end if
!
! Check for small diagonal values
      write(UNIout,'(a)')'Small (or negative) diagonal values of Hessian set to 0.05E0'
      do Ioptpr=1,Ninternal
      if(HRIC(Ioptpr,Ioptpr).lt.0.05E0)then
        HRIC(Ioptpr,Ioptpr)=0.05E0
      end if
      end do ! Ioptpr

!      write(UNIout,*)'debug_Hessian_MM > Hessian RIC: '
!      call PRT_matrix (HRIC, Ninternal, Ninternal)

      if(.not.allocated(Hessian_Matrix))then
        allocate(Hessian_Matrix(Noptpr,Noptpr))
      end if

      if(OPT_parameters%name(1:3).eq.'PIC')then ! transform to PIC
        allocate(Hxyz(3*Natoms,3*Natoms))
        call GET_object ('MOL', 'BMATRIX', 'RIC')
        Hxyz=matmul(transpose(Bmatrix_RIC), matmul(HRIC, Bmatrix_RIC))    ! B^t*HRIC*B
!        write(UNIout,*)'debug_Hessian_MM > Hessian XYZ: '
!        call PRT_matrix (Hxyz, 3*Natoms, 3*Natoms)
! Constraints???
        call GET_object ('MOL', 'BMATRIX', 'PIC')
        Hessian_Matrix=matmul(Bmatrix_PIC, matmul(HXYZ, transpose(Bmatrix_PIC)))    ! B*HXYZ*B^t
        deallocate (HXYZ)

      else ! Use RIC
      Ntotal=0
      if(RIC%Nbonds.gt.0.and.OPT_coord%bonds)then
      Hessian_Matrix(1:RIC%Nbonds,1:RIC%Nbonds)=HRIC(1:RIC%Nbonds,1:RIC%Nbonds)
      Ntotal=Ntotal+RIC%Nbonds
      end if

      if(RIC%Nangles.gt.0.and.OPT_coord%angles)then
      Hessian_Matrix(Ntotal+1:Ntotal+RIC%Nangles,Ntotal+1:Ntotal+RIC%Nangles)= &
                HRIC(Ntotal+1:Ntotal+RIC%Nangles,Ntotal+1:Ntotal+RIC%Nangles)
      Ntotal=Ntotal+RIC%Nangles
      end if

      if(RIC%Ntorsions.gt.0.and.OPT_coord%torsions)then
      Hessian_Matrix(Ntotal+1:Ntotal+RIC%Ntorsions,Ntotal+1:Ntotal+RIC%Ntorsions)= &
                HRIC(Ntotal+1:Ntotal+RIC%Ntorsions,Ntotal+1:Ntotal+RIC%Ntorsions)
      Ntotal=Ntotal+RIC%Ntorsions
      end if

      if(RIC%Noopbends.gt.0.and.OPT_coord%oopbends)then
      Hessian_Matrix(Ntotal+1:Ntotal+RIC%Noopbends,Ntotal+1:Ntotal+RIC%Noopbends)= &
                HRIC(Ntotal+1:Ntotal+RIC%Noopbends,Ntotal+1:Ntotal+RIC%Noopbends)
      Ntotal=Ntotal+RIC%Noopbends
      end if

      if(RIC%Nnonbonded.gt.0.and.OPT_coord%nonbonded)then
      Hessian_Matrix(Ntotal+1:Ntotal+RIC%Nnonbonded,Ntotal+1:Ntotal+RIC%Nnonbonded)= &
                HRIC(Ntotal+1:Ntotal+RIC%Nnonbonded,Ntotal+1:Ntotal+RIC%Nnonbonded)
      Ntotal=Ntotal+RIC%nnonbonded
      end if

      if(Ntotal.ne.Noptpr)then
        write(UNIout,'(a)')'Hessian_MM> ERROR: Ntotal .ne. Noptpr'
        write(UNIout,'(a,i8)')'Hessian_MM> Ntotal: ',Ntotal
        write(UNIout,'(a,i8)')'Hessian_MM> Noptpr: ',Noptpr
        stop 'Hessian_MM> ERROR: Ntotal .ne. Noptpr'
      end if

      end if

      deallocate (HRIC)
!
! End of routine Hessian_MM
      call PRG_manager ('exit', 'Hessian_MM', 'UTILITY')
      return
      end subroutine Hessian_MM
