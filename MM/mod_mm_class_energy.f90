      module mm_class_energy
!******************************************************************
! Date last modified: 4 April, 2000                   Version 1.0 *
! Author: Michelle Shaw                                           *
! Description: Stretch object and initialization routine.         *
!******************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE GRAPH_objects
      USE mm_parameters
      USE nobond_params
      USE atom_type

      implicit none

      type type_energy_MM
        double precision :: stretch
        double precision :: bend
        double precision :: torsion
        double precision :: oopbend
        double precision :: VderW
        double precision :: electrostatic
        double precision :: stretch_bend
        double precision :: total
      end type type_energy_MM

      type(type_energy_MM) :: E_MM

      double precision, dimension(:), allocatable :: V_stretch
      double precision, dimension(:), allocatable :: V_bend
      double precision, dimension(:), allocatable :: V_torsion
      double precision, dimension(:), allocatable :: V_oopbend
      double precision, dimension(:), allocatable :: V_VderW
      double precision, dimension(:), allocatable :: V_elect
      double precision, dimension(:), allocatable :: V_str_bend


CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine MMVStretch
!*****************************************************************
! Date last modified: 20 March, 2000                 Version 1.0 *
! Author: Michelle Shaw                                          *
! Description: Calculate individual contributions to the         *
!              stretch portion of the MM energy for each bond    *
!*****************************************************************
      implicit none

! Local scalars:
      integer Istr,Nstretchs
      double precision DIFF_BD,FF,CS,term
!
! Begin:
      call PRG_manager ('enter', 'MMVSTRETCH', 'UTILITY')

! Allocate work arrays:
      Nstretchs=RIC%Nbonds
      if(.not.allocated(V_stretch))then
        allocate (V_stretch(Nstretchs))
      end if

      V_stretch(1:Nstretchs)=ZERO
      E_MM%stretch=ZERO

! Calculation of the individual stretch energy contributions is done here...
! Input and equilibrium bond lengths are input from the set of MM coordinates.
      FF = 143.9325D0/TWO
      CS = -TWO
      term=(7.0D0/12.0D0)*(CS**2)
      do Istr=1,Nstretchs
        DIFF_BD = RIC%bond(iSTR)%value*Bohr_to_Angstrom - MMstretch(iSTR)%R_ij
        V_Stretch(Istr) = FF*MMstretch(iSTR)%k_ij*(DIFF_BD**2)*(ONE+CS*DIFF_BD+term*(DIFF_BD**2))
        V_stretch(Istr)=V_stretch(Istr)*Cal_to_J/hartree_to_kJ
      end do
      E_MM%stretch=sum(V_stretch, dim=1)
!
! End of routine MMVStretch
      call PRG_manager ('exit', 'MMVSTRETCH', 'UTILITY')
      return
      end subroutine MMVStretch
      subroutine MMVBend
!*****************************************************************
! Date last modified: 4 April, 2000                  Version 1.0 *
! Author: Michelle Shaw                                          *
! Description: Calculate individual contributions to the         *
!              bend portion of the MM energy for each bond       *
!*****************************************************************
      implicit none

! Local scalars:
      integer iBND,Nbends
      double precision DIFF,CB,FF
!
! Begin:
      call PRG_manager ('enter', 'MMVBEND', 'UTILITY')

! Allocate work arrays:
      Nbends=RIC%Nangles
      if(.not.allocated(V_bend))then
        allocate (V_bend(Nbends))
      end if

      V_bend(1:Nbends)=ZERO
      E_MM%bend=ZERO

! The energy contribution between triplets of atoms is computed using the function V_Bend
      FF = 0.043844D0/TWO
      CB = -7.0D-03
      do iBND=1,Nbends
        DIFF = (RIC%angle(iBND)%value/Deg_to_Radian) - MMbend(iBND)%theta_ijk
        V_Bend(Ibnd) = FF*MMbend(iBND)%k_ijk*(DIFF**2)*(ONE+CB*DIFF)
        V_Bend(Ibnd)=V_Bend(Ibnd)*Cal_to_J/hartree_to_kJ
      end do
      E_MM%bend=sum(V_Bend, dim=1)

! Printing for debugging and things that are not accessible via an output statement:

! End of routine MMVBend
      call PRG_manager ('exit', 'MMVBEND', 'UTILITY')
      return
      end subroutine MMVBend
      subroutine MMVTorsion
!*****************************************************************
! Date last modified: 4 April, 2000                  Version 1.0 *
! Author: Michelle Shaw                                          *
! Description: Calculate individual contributions to the         *
!              torsion portion of the MM energy for each bond    *
!*****************************************************************
      implicit none

! Local scalars:
      integer iTOR,Ntorsions
!
! Begin:
      call PRG_manager ('enter', 'MMVTORSION', 'UTILITY')

! Allocate work arrays:
      Ntorsions=RIC%Ntorsions
      if(.not.allocated(V_torsion))then
        allocate (V_torsion(Ntorsions))
      end if

      V_torsion(1:Ntorsions)=ZERO
      E_MM%torsion=ZERO

! Calculation of the individual contributions to the torsional energy goes here...
      do iTOR=1,Ntorsions
        V_Torsion(Itor)=0.5D0*(MMtorsion(iTOR)%V1*(ONE + DCOS(RIC%torsion(iTOR)%value)) + &
                MMtorsion(iTOR)%V2*(ONE - DCOS(TWO*RIC%torsion(iTOR)%value)) + &
                MMtorsion(iTOR)%V3*(ONE + DCOS(3.0D0*RIC%torsion(iTOR)%value)))
        V_torsion(Itor)=V_Torsion(Itor)*Cal_to_J/hartree_to_kJ
      end do
      E_MM%torsion=sum(V_torsion, dim=1)

! Printing for debugging and things that are not accessible via an output statement:

! End of routine MMVTorsion
      call PRG_manager ('exit', 'MMVTORSION', 'UTILITY')
      return
      end subroutine MMVTorsion
      subroutine MMVOopbend
!*************************************************************************
! Date last modified: 4 April, 2000                          Version 1.0 *
! Author: Michelle Shaw                                                  *
! Description: Calculate individual contributions to the out-of-plane    *
!              bend portion of the MM energy for each bend.              *
!*************************************************************************
      implicit none

! Local scalars:
      integer iOBND,Noopbends
!
! Begin:
      call PRG_manager ('enter', 'MMVOOPBEND', 'UTILITY')

! Allocate work arrays:
      Noopbends=RIC%Noopbends
      if(.not.allocated(V_oopbend))then
        allocate (V_oopbend(Noopbends))
      end if

      V_oopbend(1:Noopbends)=ZERO
      E_MM%oopbend=ZERO

! Calculation of the individual contributions to the out-of-plane bend energy goes here...
      do iOBND=1,Noopbends
        V_Oopbend(Iobnd)= 0.043844D0*(MMoopbend_k_ijkl(iOBND)/2.0D0)* &
                          (RIC%oopbend(iOBND)%value/Deg_to_Radian)**2
        V_oopbend(iOBND)=V_Oopbend(Iobnd)*Cal_to_J/hartree_to_kJ
      end do
      E_MM%oopbend=sum(V_oopbend, dim=1)

! Printing for debugging and things that are not accessible via an output statement:

! End of routine MMVOopbend
      call PRG_manager ('exit', 'MMVOOPBEND', 'UTILITY')
      return
      end subroutine MMVOopbend
      subroutine MMVVDW
!**************************************************************************
! Date last modified: 4 April, 2000                           Version 1.0 *
! Author: Michelle Shaw                                                   *
! Description: Calculates individual energy contributions due to          *
!              van der Waals interactions and stores them in the object   *
!              V_VderW.                                      *
!**************************************************************************
      implicit none

! Local scalars:
      integer iELEC,NVderWs
      integer iATM, jATM
      double precision mes,e_ij,dist_ij,term1,term2,V_Vanderwaals
!
! Begin:
      call PRG_manager ('enter', 'MMVVDW', 'UTILITY')

!
! Allocate work arrays:
      NVderWs=RIC%Nnonbonded
      if(.not.allocated(V_VderW))then
        allocate (V_VderW(NVderWs))
      end if

! This is where the calculation of the individual van der Waals contributions
!  is done.  The needed parameters are computed and put into modules first,then the
!  energy contributions are calculated.
      V_VderW(1:NVderWs)=ZERO
      E_MM%VderW=ZERO

        do iELEC=1,NVderWs
! van der Waals and Electrostatic interactions:
          if(pair_IJ(iELEC)%is_vdw.eq.1)then
            mes = MMVderW(iELEC)%R_IJ
            e_ij = MMVderW(iELEC)%epsilon_IJ
            dist_ij = RIC%nonbonded(iELEC)%value*Bohr_to_Angstrom
            term1 = 1.07D0*mes/(dist_ij + 0.07D0*mes)
            term2 = 1.12D0*(mes**7)/(dist_ij**7 + 0.12D0*(mes**7))
            V_Vanderwaals = e_ij*(term1**7)*(term2 - 2.0D0)
            V_VderW(iELEC)=V_Vanderwaals*Cal_to_J/hartree_to_kJ
          else
! Neither interaction:
            V_VderW(iELEC)=ZERO
          end if
        end do
        E_MM%VderW=sum(V_VderW, dim=1)

! Printing for debugging and things that are not accessible via an output statement:

! End of routine MMVVDW
      call PRG_manager ('exit', 'MMVVDW', 'UTILITY')
      return
      end subroutine MMVVDW
      subroutine MMVElectronic
!**************************************************************************
! Date last modified: 4 April, 2000                           Version 1.0 *
! Author: Michelle Shaw                                                   *
! Description: Calculates individual energy contributions due to          *
!              van der Waals interactions and stores them in the object   *
!              V_elect.                                *
!**************************************************************************
! Modules:
      USE nobond_params

      implicit none

! Local scalars:
      integer iELEC,Nelectrostatic
      integer iATM, jATM
      double precision V_electrostatic
!
! Begin:
      call PRG_manager ('enter', 'MMVELECTRONIC', 'UTILITY')
!
! Allocate work arrays:
      Nelectrostatic=RIC%Nnonbonded
      if(.not.allocated(V_elect))then
        allocate (V_elect(Nelectrostatic))
      end if

! This is where the calculation of the individual van der Waals contributions
!  is done.  The needed parameters are computed and put into modules first,then the
!  energy contributions are calculated.

      V_elect(1:Nelectrostatic)=ZERO
      E_MM%electrostatic=ZERO

        do iELEC=1,Nelectrostatic
          if(pair_IJ(iELEC)%is_elec.eq.1)then
            V_Electrostatic = 332.0716D0*MMelectrostatic(iELEC)%q_I* &
                              MMelectrostatic(iELEC)%q_J/ &
                        (dielec_const*(RIC%nonbonded(iELEC)%value*Bohr_to_Angstrom + delta)**n)
            V_elect(iELEC)=V_Electrostatic
            if(pair_IJ(iELEC)%is_tors.eq.1)then
              V_elect(iELEC)=V_elect(iELEC)*scalef_14
            end if
            V_elect(iELEC)=V_elect(iELEC)*Cal_to_J/hartree_to_kJ
          else
            V_elect(iELEC)=ZERO
          end if
        end do
        E_MM%electrostatic=sum(V_elect, dim=1)

! Printing for debugging and things that are not accessible via an output statement:

! End of routine MMVElectronic
      call PRG_manager ('exit', 'MMVELECTRONIC', 'UTILITY')
      return
      end subroutine MMVElectronic
      subroutine MMVStrBnd
!*****************************************************************
! Date last modified: 4 April, 2000                  Version 1.0 *
! Author: Michelle Shaw                                          *
! Description: Calculate individual contributions to the         *
!              stretch-bend portion of the MM energy for         *
!              select atoms in the molecule.                     *
!*****************************************************************
      implicit none

! Local scalars:
      integer iBND,Nstr_bends
      integer iSTR, iATM, jATM, kATM
      double precision diff_bond_ij, diff_bond_jk, diff_angle_ijk,V_stretch_Bend
!
! Begin:
      call PRG_manager ('enter', 'MMVSTRBND', 'UTILITY')

! allocate work array if not already allocated:
      Nstr_bends=RIC%Nangles
      if(.not.allocated(V_str_bend))then
        allocate(V_str_bend(Nstr_bends))
      end if

      V_str_bend(1:Nstr_bends)=ZERO
      E_MM%stretch_bend=ZERO

! Calculation of the individual stretch-bend energy contributions is done here...
        do iBND=1,Nstr_bends
          diff_bond_ij = ZERO
          diff_bond_jk = ZERO
          diff_angle_ijk = ZERO

          iATM = ANGLE(iBND)%ATOM1
          jATM = ANGLE(iBND)%ATOM2
          kATM = ANGLE(iBND)%ATOM3
          do iSTR = 1,RIC%Nbonds
          if((BOND(iSTR)%ATOM1.eq.iATM.or.BOND(iSTR)%ATOM2.eq.iATM).and.(BOND(iSTR)%ATOM1.eq.jATM.or. &
           BOND(iSTR)%ATOM2.eq.jATM)) then
          diff_bond_ij = (RIC%bond(iSTR)%value*Bohr_to_Angstrom) - MMstretch(iSTR)%R_ij
          end if
          if((BOND(iSTR)%ATOM1.eq.jATM.or.BOND(iSTR)%ATOM2.eq.jATM).and.(BOND(iSTR)%ATOM1.eq.kATM.or. &
           BOND(iSTR)%ATOM2.eq.kATM)) then
          diff_bond_jk = (RIC%bond(iSTR)%value*Bohr_to_Angstrom) - MMstretch(iSTR)%R_ij
          end if
        end do
        diff_angle_ijk = (RIC%angle(iBND)%value/Deg_to_Radian) - MMbend(iBND)%theta_ijk
        V_Stretch_bend = 2.51210*(MMstr_bend(iBND)%k_ijk*diff_bond_ij + &
                       MMstr_bend(iBND)%k_kji*diff_bond_jk)*diff_angle_ijk
          V_str_bend(iBND)=V_Stretch_bend*Cal_to_J/hartree_to_kJ
        end do
        E_MM%stretch_bend=sum(V_str_bend, dim=1)

! Printing for debugging and things that are not accessible via an output statement:

! End of routine MMVStrBnd
      call PRG_manager ('exit', 'MMVSTRBND', 'UTILITY')
      return
      end subroutine MMVStrBnd

      end module mm_class_energy
