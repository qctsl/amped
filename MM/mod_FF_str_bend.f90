      module FF_parameters
!**********************************************************************************
! Date last modified: March 22, 2005                                  Version 1.0 *
! Author: R. A. Poirier                                                           *
! Description: Contains object and initialization routines for stretch parameters.*                                                       !**********************************************************************************
! Modules:
      USE program_manager
      USE program_defaults
      USE program_constants
      USE type_molecule
      USE mm_interaction_type_params
      USE nobond_params
      USE GRAPH_objects
      USE atom_type

      implicit none

      type stretch_bend_contributions
        double precision :: k_ijk, k_kji !stretch-bend force program_constants
      end type stretch_bend_contributions

      type (stretch_bend_contributions), dimension(:), allocatable :: MMstr_bend

CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine GETSTRBND_PARAMS
!****************************************************************************
! Date last modified: 23 March, 2000                            Version 1.0 *
! Author: Michelle Shaw                                                     *
! Description: Calculates the force program_constants for the coupling of a bond    *
!              stretch to an angle bend and places them in a derived type   *
!              stretch_bend.                                                *
!****************************************************************************
      implicit none

! Local scalars:
      integer Iatom,Jatom,Katom,ZatomI,ZatomJ,ZatomK,TypeI,TypeJ,TypeK
      integer IBend,case_strbnd,bond_ik,bond_kj
      double precision stb_fc_ijk, stb_fc_kji
      logical Ldebug
!
! Begin:
      call PRG_manager ('enter', 'GETSTRBND_PARAMS', 'UTILITY')
      Ldebug=Local_Debug

! Allocate work arrays;
      if(.not.allocated(MMstr_bend))then
        allocate (MMstr_bend(NGAngle))
      end if

! Next, the angle bend force constant must be obtained based on the type of atoms involved
! in the angle.
      do IBend=1,NGAngle
        Iatom = ANGLE(IBend)%ATOM1
        Katom = ANGLE(IBend)%ATOM2 ! Katom is the central atom...
        Jatom = ANGLE(IBend)%ATOM3
        ZatomI = CARTESIANS%Atomic_number(Iatom)
        if(ZatomI.eq.0)cycle
        ZatomJ = CARTESIANS%Atomic_number(Jatom)
        if(ZatomJ.eq.0)cycle
        ZatomK = CARTESIANS%Atomic_number(Katom)
        if(ZatomK.eq.0)cycle

        TypeI = vertex(Iatom)%type
        TypeJ = vertex(Jatom)%type
        TypeK = vertex(Katom)%type

        case_strbnd = 12100*(ZatomI-1)+110*(ZatomK-1)+(ZatomJ-1)

        bond_ik = BT(Iatom, Katom)
        bond_kj = BT(Jatom, Katom)

        ANGLEtypeS:select case (case_strbnd)
          case(550) ANGLEtypeS
            call GET_STB_FC_HCH(TypeK, stb_fc_ijk, stb_fc_kji)
            MMstr_bend(IBend)%k_ijk = stb_fc_ijk
            MMstr_bend(IBend)%k_kji = stb_fc_kji

          case(555) ANGLEtypeS
            call GET_STB_FC_HCC(TypeJ, TypeK, bond_kj, stb_fc_ijk, stb_fc_kji)
            MMstr_bend(IBend)%k_ijk = stb_fc_ijk
            MMstr_bend(IBend)%k_kji = stb_fc_kji

          case (61050) ANGLEtypeS
            call GET_STB_FC_CCH(TypeI, TypeK, bond_ik, stb_fc_ijk, stb_fc_kji)
            MMstr_bend(IBend)%k_ijk = stb_fc_ijk
            MMstr_bend(IBend)%k_kji = stb_fc_kji

          case (61055) ANGLEtypeS
            call GET_STB_FC_CCC(TypeI, TypeJ, TypeK, bond_ik, bond_kj, stb_fc_ijk,stb_fc_kji)
            MMstr_bend(IBend)%k_ijk = stb_fc_ijk
            MMstr_bend(IBend)%k_kji = stb_fc_kji

          case DEFAULT ANGLEtypeS
            write(UNIout,'(a)')'Atom type not supported or is a dummy.'
            MMstr_bend(IBend)%k_ijk = ZERO
            MMstr_bend(IBend)%k_kji = ZERO
        end select ANGLEtypeS

! Printing for debugging and things that are not accessible via an output statement:
        if(Ldebug)then
          write(UNIout,'(a,f12.6)')'force constant for ijk angle: ',MMstr_bend(IBend)%k_ijk
          write(UNIout,'(a,f12.6)')'force constant for kji angle: ',MMstr_bend(IBend)%k_kji
        end if
      end do

      if(Ldebug)then
        write(UNIout,'(a,i8)')'debug_GETSTRBND_PARAMS > NGBonds: ',NGBonds
        write(UNIout,'(a,i8)')'debug_GETSTRBND_PARAMS > NGAngle: ',NGAngle
      end if

! End of routine GETSTRBND_PARAMS
      call PRG_manager ('exit', 'GETSTRBND_PARAMS', 'UTILITY')
      return
      end subroutine GETSTRBND_PARAMS
      end module FF_parameters
