      module mm_gradients
!***********************************************************************
! Date last modified: 30 March, 2000                       Version 1.0 *
! Author: Michelle Shaw                                                *
! Description: Contains objects and generation routines for the MM     *
!              gradients.                                              *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE GRAPH_objects
      USE mm_parameters
      USE atom_type
      USE nobond_params

      implicit none

! Objects:
      double precision, dimension(:), allocatable :: G_stretch
      double precision, dimension(:), allocatable :: G_bend
      double precision, dimension(:), allocatable :: G_torsion
      double precision, dimension(:), allocatable :: G_oopbend
      double precision, dimension(:), allocatable :: G_VderWs
      double precision, dimension(:), allocatable :: G_electrostatic

CONTAINS
      subroutine GRADCLC_STR
!**************************************************************************
! Date last modified: 4 April, 2000                           Version 1.0 *
! Author: Michelle Shaw                                                   *
! Description: Calculates the gradients of the stretch energy.            *
!**************************************************************************
      implicit none

! Local scalars:
      integer iBATM,jBATM,iAATM,jAATM,kAATM,iSTR,iBND,Nstretchs
      double precision cs,term,C,DIFF,diff_ijk,Sgradient
!
! Begin:
      call PRG_manager ('enter', 'GRADCLC_STR', 'UTILITY')

! Allocate work arrays:
      Nstretchs=RIC%NBonds
      if(.not.allocated(G_stretch))then
        allocate (G_stretch(Nstretchs))
      end if

! Initialize object:
      G_stretch = ZERO

! The gradient of the energy with respect to the deformation of a particular bond length
      cs = -TWO
      term = (7.0D0/12.0D0)*(cs**2)

      do iSTR = 1,Nstretchs
        iBATM = RIC%bond(iSTR)%atom1
        jBATM = RIC%bond(iSTR)%atom2
        C = 143.9325D0*MMstretch(iSTR)%k_ij
        DIFF = (RIC%bond(iSTR)%value*Bohr_to_Angstrom) - MMstretch(iSTR)%R_ij
        Sgradient = C*DIFF*(ONE+cs*DIFF+term*(DIFF**2)) + &
                            (C/2.0D0)*(DIFF**2)*(cs+TWO*term*DIFF)
      do iBND = 1,RIC%Nangles
        iAATM = ANGLE(iBND)%ATOM1
        jAATM = ANGLE(iBND)%ATOM2
        kAATM = ANGLE(iBND)%ATOM3

        if((iBATM.eq.iAATM.or.jBATM.eq.iAATM).and.(iBATM.eq.jAATM.or.jBATM.eq.jAATM)) then
          diff_ijk = (RIC%angle(iBND)%value/Deg_to_Radian) - MMbend(iBND)%theta_ijk
          Sgradient = Sgradient + 2.51210D0*MMstr_bend(iBND)%k_ijk*diff_ijk
        end if

        if((iBATM.eq.jAATM.or.jBATM.eq.jAATM).and.(iBATM.eq.kAATM.or.jBATM.eq.kAATM)) then
          diff_ijk = (RIC%angle(iBND)%value/Deg_to_Radian) - MMbend(iBND)%theta_ijk
          Sgradient = Sgradient + 2.51210D0*MMstr_bend(iBND)%k_kji*diff_ijk
        end if
      end do
      G_stretch(iSTR) = Sgradient*(Cal_to_J*Bohr_to_Angstrom/hartree_to_kJ)
      end do

! Printing for debugging and things that are not accessible via an output statement:

! End of routine GRADCLC_STR
      call PRG_manager ('exit', 'GRADCLC_STR', 'UTILITY')
      return
      end subroutine GRADCLC_STR

      subroutine GRADCLC_BND
!**************************************************************************
! Date last modified: 4 April, 2000                           Version 1.0 *
! Author: Michelle Shaw                                                   *
! Description: Calculates the gradients of the bend energy.               *
!**************************************************************************
      implicit none

! Local scalars:
      integer iBND
      integer iATM,jATM,kATM,iSTR,Nbends
      double precision A, DIFF, cb, diff_ik, diff_jk,Bgradient

! Begin:
      call PRG_manager ('enter', 'GRADCLC_BND', 'UTILITY')

! Allocate work arrays:
      Nbends=RIC%Nangles
      if(.not.allocated(G_bend))then
        allocate (G_bend(Nbends))
      end if

! Initialize object:
      G_bend = ZERO

! The gradient of the energy with respect to a change in the input angle will now be calculated...
      cb = -7.0D-03
        do iBND = 1,Nbends

        iATM = ANGLE(iBND)%ATOM1
        kATM = ANGLE(iBND)%ATOM2
        jATM = ANGLE(iBND)%ATOM3

        do iSTR = 1,RIC%Nbonds
          if((BOND(iSTR)%ATOM1.eq.iATM.or.BOND(iSTR)%ATOM2.eq.iATM).and.(BOND(iSTR)%ATOM1.eq.kATM.or. &
             BOND(iSTR)%ATOM2.eq.kATM)) then
            diff_ik = (RIC%bond(iSTR)%value*Bohr_to_Angstrom) - MMstretch(iSTR)%R_ij
          end if
          if((BOND(iSTR)%ATOM1.eq.jATM.or.BOND(iSTR)%ATOM2.eq.jATM).and.(BOND(iSTR)%ATOM1.eq.kATM.or. &
             BOND(iSTR)%ATOM2.eq.kATM)) then
            diff_jk = (RIC%bond(iSTR)%value*Bohr_to_Angstrom) - MMstretch(iSTR)%R_ij
          end if
        end do

        DIFF = (RIC%angle(iBND)%value/Deg_to_Radian) - MMbend(iBND)%theta_ijk
        A = 0.043844D0*MMbend(iBND)%k_ijk
        Bgradient = A*DIFF*(ONE+cb*DIFF)+(A/2.0D0)*DIFF**2*cb + &
        2.51210D0*(MMstr_bend(iBND)%k_ijk*diff_ik + MMstr_bend(iBND)%k_kji*diff_jk)
        G_bend(iBND) = Bgradient*(Cal_to_J/(Deg_to_Radian*hartree_to_kJ))
        end do

! End of routine GRADCLC_BND
      call PRG_manager ('exit', 'GRADCLC_BND', 'UTILITY')
      return
      end subroutine GRADCLC_BND

      subroutine GRADCLC_TOR
!**************************************************************************
! Date last modified: 4 April, 2000                           Version 1.0 *
! Author: Michelle Shaw                                                   *
! Description: Calculates the gradients of the torsion energy.            *
!**************************************************************************
      implicit none

! Local scalars:
      integer iTOR,Ntorsions
      double precision Tgradient
!
! Begin:
      call PRG_manager ('enter', 'GRADCLC_TOR', 'UTILITY')

! Allocate work arrays:
      Ntorsions=RIC%Ntorsions
      if(.not.allocated(G_torsion))then
        allocate (G_torsion(Ntorsions))
      end if

! Initialize objects.
      G_torsion = ZERO

! The gradient of the energy with respect to a change in the input torsion will now be calculated...
        do iTOR = 1,Ntorsions
          Tgradient = -(MMtorsion(iTOR)%V1/TWO)*(DSIN(RIC%torsion(iTOR)%value)) + &
                        MMtorsion(iTOR)%V2*(DSIN(TWO*RIC%torsion(iTOR)%value)) - &
                  1.5D0*MMtorsion(iTOR)%V3*(DSIN(THREE*RIC%torsion(iTOR)%value))
          G_torsion(iTOR) = Tgradient*(Cal_to_J/hartree_to_kJ)
        end do

! Printing for debugging and things that are not accessible via an output statement:

! End of routine GRADCLC_TOR
      call PRG_manager ('exit', 'GRADCLC_TOR', 'UTILITY')
      return
      end subroutine GRADCLC_TOR

      subroutine GRADCLC_OBND
!**************************************************************************
! Date last modified: 4 April, 2000                           Version 1.0 *
! Author: Michelle Shaw                                                   *
! Description: Calculates the gradients of the out-of-plane bend energy.  *
!**************************************************************************
      implicit none

! Local scalars:
      integer iOBND,Noopbends
      double precision Ogradient
!
! Begin:
      call PRG_manager ('enter', 'GRADCLC_OBND', 'UTILITY')

! Allocate work arrays:
      Noopbends=RIC%Noopbends
      if(.not.allocated(G_oopbend))then
        allocate (G_oopbend(Noopbends))
      end if

! Initialize objects.
      G_oopbend = ZERO

! The gradient of the energy with respect to a change in the input out-of-plane bend
        do iOBND = 1,Noopbends
          Ogradient = 0.043844*MMoopbend_k_ijkl(iOBND)*(RIC%oopbend(iOBND)%value/Deg_to_Radian)
          G_oopbend(iOBND) = Ogradient*(Cal_to_J/(Deg_to_Radian*hartree_to_kJ))
        end do

! End of routine GRADCLC_OBND
      call PRG_manager ('exit', 'GRADCLC_OBND', 'UTILITY')
      return
      end subroutine GRADCLC_OBND

      subroutine GRADCLC_VDW
!*******************************************************************
! Date last modified: 4 April, 2000                    Version 1.0 *
! Author: Michelle Shaw                                            *
! Description: Calculate (analytical) gradients of van der Waals   *
!              energies and store them in G_VderWs.   *
!*******************************************************************
      implicit none

! Local scalars:
      integer iELEC
      integer iATM,jATM,NVderWs
      double precision term1,term2,t1_const,t2_const,t1_num,t1_denom,t2_num,t2_denom,dist_ij,Vgradient
!
! Begin:
      call PRG_manager ('enter', 'GRADCLC_VDW', 'UTILITY')

! Allocate work arrays:
      NVderWs=RIC%Nnonbonded
      if(.not.allocated(G_VderWs))then
        allocate (G_VderWs(NVderWs))
      end if

      G_VderWs = ZERO

! Calculate the gradients of the van der Waals energies obtained from Maple
      t1_const = -11.24047033D0
      t2_const = -12.58932677D0
      do iELEC = 1,NVderWs
      if(pair_IJ(iELEC)%is_vdw.eq.1) then
        dist_ij = RIC%nonbonded(iELEC)%value*Bohr_to_Angstrom
        t1_num = (MMVderW(iELEC)%R_IJ**7)*((1.12D0*(MMVderW(iELEC)%R_IJ**7)/ &
                 (dist_ij**7 + 0.12D0*MMVderW(iELEC)%R_IJ**7)) - 2.0D0)* &
                 MMVderW(iELEC)%epsilon_IJ
        t1_denom = (dist_ij + 0.07D0*MMVderW(iELEC)%R_IJ)**8
        term1 = t1_const*t1_num/t1_denom
        t2_num = (MMVderW(iELEC)%R_IJ**14)*(dist_ij**6)*MMVderW(iELEC)%epsilon_IJ
        t2_denom = ((dist_ij + 0.07D0*MMVderW(iELEC)%R_IJ)**7)* &
                   ((dist_ij**7 + 0.12D0*MMVderW(iELEC)%R_IJ**7)**2)
        term2 = t2_const*t2_num/t2_denom
        Vgradient = (term1 + term2)
      else
        Vgradient = ZERO
      end if
          G_VderWs(iELEC) = Vgradient*(Cal_to_J*Bohr_to_Angstrom/hartree_to_kJ)
      end do

! End of routine GRADCLC_VDW
      call PRG_manager ('exit', 'GRADCLC_VDW', 'UTILITY')
      return
      end subroutine GRADCLC_VDW

      subroutine GRADCLC_ELEC
!*******************************************************************
! Date last modified: 4 April, 2000                    Version 1.0 *
! Author: Michelle Shaw                                            *
! Description: Calculate (analytical) gradients of electrostatic   *
!              energies and store them in G_electrostatic. *
!*******************************************************************
      implicit none

! Local scalars:
      integer iELEC
      integer iATM,jATM,Nelectrostatic
      double precision dist_ij,Egradient
!
! Begin:
      call PRG_manager ('enter', 'GRADCLC_ELEC', 'UTILITY')

! Allocate work arrays:
      Nelectrostatic=RIC%Nnonbonded
      if(.not.allocated(G_electrostatic))then
        allocate (G_electrostatic(Nelectrostatic))
      end if

      G_electrostatic = ZERO

! Calculate the gradients of the electrostatic energies obtained from Maple using the energy
        do iELEC = 1,Nelectrostatic
! Calculate one gradient for the van der Waals energy:
      if(pair_IJ(iELEC)%is_elec.eq.1) then
        dist_ij = RIC%nonbonded(iELEC)%value*Bohr_to_Angstrom
        Egradient = -332.0716D0* &
              MMelectrostatic(iELEC)%q_I*MMelectrostatic(iELEC)%q_J*n/ &
              (dielec_const*((dist_ij+delta)**(n+1)))
!     .        ((dielec_const*dist_ij + delta)**n*(dist_ij + delta))    ! June 23, 2003
        if(pair_IJ(iELEC)%is_tors.eq.1)then
          Egradient = Egradient*scalef_14
        end if
      else
        Egradient = ZERO
      end if
      G_electrostatic(iELEC) = Egradient*(Cal_to_J*Bohr_to_Angstrom/hartree_to_kJ)
! Scale gradient if the nonbonded atoms are separated by three bonds:
          if(pair_IJ(iELEC)%is_tors.eq.1)then
            G_electrostatic(iELEC) = G_electrostatic(iELEC)*scalef_14
          end if
        end do

! End of routine GRADCLC_ELEC
      call PRG_manager ('exit', 'GRADCLC_ELEC', 'UTILITY')
      return
      end subroutine GRADCLC_ELEC

      end module mm_gradients
