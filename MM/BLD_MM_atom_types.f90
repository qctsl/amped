      subroutine BLD_MM_ATOM_TYPES
!*****************************************************************
! Date last modified: 26 March, 2000              Version 1.0 *
! Author: Michelle Shaw                                          *
! Description: Determine a type for each atom: initializes       *
!              derived type atype.                               *
!*****************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE mm_interaction_type_params
      USE GRAPH_objects
      USE atom_type

      implicit none
!
! Local scalars:
      integer jSTR,iCTR,jCTR,kCTR,iATM,jATM,iSTR
      logical I_IS_AROM, is_ring
!
! Begin:
      call PRG_manager ('enter', 'BLD_MM_ATOM_TYPES', 'ATOM_TYPES%MMFF94')

      call GET_object ('MOL', 'GRAPH', 'CONVAL')
      call GET_object ('MOL', 'GRAPH', 'BOND_ATOMS')
      call GET_object ('MOL', 'GRAPH', 'EDGE_TO_RING')

! Allocate work arrays:
      if(.not.allocated(TATOM))then
        allocate (TATOM(Natoms))
      end if

      if(.not.allocated(BT))then
        allocate(BT(Natoms, Natoms))          ! NO deallocates!!!!!!!!!!!!!
      end if

! Zero work arrays:
      BT = 0
      is_ring = .false.
      I_IS_AROM = .false.
      iCTR = 0
      jCTR = 0
      kCTR = 0

! Begin by determining from the connectivity matrix if two atoms are connected.
! If they are, then determine the bond type and put it into the matrix.
      do iSTR = 1,NGBONDS
        iATM = BOND(iSTR)%ATOM1
        jATM = BOND(iSTR)%ATOM2
        BT(iATM,jATM) = BOND(iSTR)%Type
        BT(jATM,iATM) = BOND(iSTR)%Type
      end do
! Next, the information about each bond must be collected.  The equilibrium bond distance must be
!  retrieved from the equilibrium bond length module.
      do iATM = 1,Natoms
        TATOM(iATM)%hybridization = CONVAL(iATM)
        TATOM(iATM)%at_num = CARTESIAN(iATM)%atomic_number

        ATMNUM:SELECT CASE(CARTESIAN(iATM)%atomic_number)
          CASE(0) ATMNUM
            TATOM(iATM)%is_arom = .false.
            TATOM(iATM)%ring_size = 0
            TATOM(iATM)%type_mm = 0
          CASE(1) ATMNUM
            TATOM(iATM)%is_arom = .false.
            TATOM(iATM)%ring_size = 0
            TATOM(iATM)%type_mm = 5
          CASE(6) ATMNUM
! Non-ring systems...
            HYBRID:SELECT CASE(CONVAL(iATM))
              CASE(2) HYBRID
                TATOM(iATM)%is_arom = .false.
                TATOM(iATM)%ring_size = 0
                TATOM(iATM)%type_mm = 4
              CASE(3) HYBRID
                TATOM(iATM)%is_arom = .false.
                TATOM(iATM)%ring_size = 0
                TATOM(iATM)%type_mm = 2
              CASE(4) HYBRID
                TATOM(iATM)%is_arom = .false.
                TATOM(iATM)%ring_size = 0
                TATOM(iATM)%type_mm = 1
              CASE DEFAULT HYBRID
                TATOM(iATM)%is_arom = .false.
                TATOM(iATM)%ring_size = 0
                TATOM(iATM)%type_mm = 0
                write(UNIout,*)'Bad carbon type.  Check hybridization.'
            end SELECT HYBRID
            do jSTR = 1,NGBONDS
! Ring systems...
            if((BOND(jSTR)%ATOM1.eq.iATM.or.BOND(jSTR)%ATOM2.eq.iATM).and.edges(jSTR)%ring.gt.0)then
! Aromatic ring...
                if(BT(BOND(jSTR)%ATOM1,BOND(jSTR)%ATOM2).eq.2)then
                  I_IS_AROM = .true.
! 3 or 4-membered ring...
                else if((CONVAL(iATM).eq.4.or.CONVAL(iATM).eq.3).and.BOND(jSTR)%ring_size.eq.3)then
                  iCTR=iCTR+1
                else if(CONVAL(iATM).eq.4.and.BOND(jSTR)%ring_size.eq.4)then
                  jCTR=jCTR+1
                else if(CONVAL(iATM).eq.3.and.BOND(jSTR)%ring_size.eq.4)then
                  kCTR=kCTR+1
                end if
              end if
            end do

            if(iCTR.gt.1)then
              TATOM(iATM)%is_arom = .false.
              TATOM(iATM)%ring_size = 3
              TATOM(iATM)%type_mm = 22
            else if(jCTR.gt.1)then
              TATOM(iATM)%is_arom = .false.
              TATOM(iATM)%ring_size = 4
              TATOM(iATM)%type_mm = 20
            else if(kCTR.gt.1)then
              TATOM(iATM)%is_arom = .false.
              TATOM(iATM)%ring_size = 4
              TATOM(iATM)%type_mm = 30
            end if

            if(I_IS_AROM)TATOM(iATM)%type_mm = 37
! Reset used counters...
            iCTR = 0
            jCTR = 0
            kCTR = 0
            I_IS_AROM = .false.
          CASE DEFAULT ATMNUM
            write(UNIout,*)'Bad atom!  Check atom number.'
        end SELECT ATMNUM

        if(MUN_prtlev.gt.0)then
          write(UNIout,'(a)')'BLD_ATOM_type> atom, at_num, hybridization, ring_size, type_mm, is_arom:'
          write(UNIout,'(5i8,l4)')iATM,TATOM(iATM)%at_num,TATOM(iATM)%hybridization, &
                        TATOM(iATM)%ring_size,TATOM(iATM)%type_mm,TATOM(iATM)%is_arom
        end if
      end do


! End of routine BLD_MM_ATOM_TYPES
      call PRG_manager ('exit', 'BLD_MM_ATOM_TYPES', 'ATOM_TYPES%MMFF94')
      return
      end subroutine BLD_MM_ATOM_TYPES
