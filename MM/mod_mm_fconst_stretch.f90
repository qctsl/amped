      module force_program_constants_stretch

!**************************************************************
! Date last modified: 10 July, 1999            Version 1.0    *
! Author: Michelle Shaw                                       *
! Description: Force program_constants for Carbon and Hydrogen bonds. *
!**************************************************************
! Modules:
      USE program_constants

      implicit none

      double precision, dimension(34) :: FORCE_CONSTANTS_SINGLE_BOND
      double precision, dimension(1) :: FORCE_CONSTANTS_DOUBLE_BOND
      double precision, dimension(12) :: FORCE_CONSTANTS_TRIPLE_BOND
      double precision, dimension(2) :: FORCE_CONSTANTS_AROMATIC_BOND

      double precision :: FC_conv_factor=(Bohr_to_Angstrom)**2/FCCONV !mdyne/angstrom to hartrees/bohr**2

! Data obtained from "Validation of the General Purpose Tripos 5.2 Force Field",
! Clark, M., Cramer III, R. D., and Van Opdenbosch, N., J. Comp. Chem., Vol. 10, No. 8, 982-1012
! (1989) (for H-H and C.1-C.1 single bonds only)
! Remaining data from: J. Comp. Chem., Vol.17, No. 5&6, Supplementary Index (Appendix B).

      DATA FORCE_CONSTANTS_SINGLE_BOND/ &
!     H       C.1      C.2     C.3     C.ar    C.3M    C.4M    C.O4M
      0.05168,                                 5.191,  4.852,  5.176,  & !H
      5.726,  4.864,   5.310,  4.707,  5.445,  5.400,  5.178,  10.227, & !C.1
      5.170,           5.657,  4.539,  5.007,  4.926,  4.593,  8.166,  & !C.2
      4.766,                   4.258,          4.286,  4.650,          & !C.3
      5.306,                   4.957,  5.178,  4.481,  3.740,          & !C.ar
                                               3.969,  4.251,  3.785,  & !C.3M
                                                       3.663,  3.977,  & !C.4M
                                                               9.579/    !C.O4M

      DATA FORCE_CONSTANTS_DOUBLE_BOND/9.505/ !C.2

      DATA FORCE_CONSTANTS_TRIPLE_BOND/ &
     &15.206, 11*0.0D0/ !C.1

!                                         C.1   C.ar
      DATA FORCE_CONSTANTS_AROMATIC_BOND/9.538,5.573/

      end module force_program_constants_stretch
