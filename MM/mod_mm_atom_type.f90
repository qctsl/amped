      module atom_type
!************************************************************************
! Date last modified: 11 February, 2000                     Version 1.0 *
! Author: Michelle Shaw                                                 *
! Description: Determine MMFF94 atom types and store the information    *
!              in a derived type "atom".                                *
!************************************************************************

      implicit none

      type atomtype
        integer :: at_num, hybridization, ring_size, type_mm
        logical :: is_arom
      end type atomtype

      type(atomtype), dimension(:), allocatable :: TATOM
      end module atom_type
