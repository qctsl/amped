      subroutine SGM_default_mesh
!*****************************************************************************************************************
!     Date last modified: September 5, 2014                                                         Version 1.0  *
!     Author: P.L. Warburton                                                                                     *
!     Description: Generates the default mesh for SGM applications                                               *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE SGM_defaults
      USE type_molecule

      implicit none
!
! Local scalars:
      integer :: Iatom, Znum, sumpoints
      logical :: toobig
      
! Begin:
      call PRG_manager ('enter', 'SGM_DEFAULT_MESH', 'UTILITY')

      toobig=.true.

! box generation
      cyclemesh: do     
! initialize box bounds and defaults for mesh 
      xlo=1.0d12
      ylo=1.0d12
      zlo=1.0d12
      xhi=-1.0d12
      yhi=-1.0d12
      zhi=-1.0d12
      xpoints=0
      ypoints=0
      zpoints=0
      maxsurface=0
  
      do Iatom=1,Natoms
!       if (fragindex(Iatom).eq.0) cycle
       Znum=CARTESIAN(Iatom)%Atomic_number
       if (Znum.le.0) cycle
       if (xlo.gt.CARTESIAN(Iatom)%X) xlo=CARTESIAN(Iatom)%X
       if (xhi.lt.CARTESIAN(Iatom)%X) xhi=CARTESIAN(Iatom)%X
       if (ylo.gt.CARTESIAN(Iatom)%Y) ylo=CARTESIAN(Iatom)%Y
       if (yhi.lt.CARTESIAN(Iatom)%Y) yhi=CARTESIAN(Iatom)%Y
       if (zlo.gt.CARTESIAN(Iatom)%Z) zlo=CARTESIAN(Iatom)%Z
       if (zhi.lt.CARTESIAN(Iatom)%Z) zhi=CARTESIAN(Iatom)%Z
      end do
      xlo=xlo-boxmargin
      ylo=ylo-boxmargin
      zlo=zlo-boxmargin
      xhi=xhi+boxmargin
      yhi=yhi+boxmargin
      zhi=zhi+boxmargin
      xptslo=xlo/res
      yptslo=ylo/res
      zptslo=zlo/res
      xptshi=xhi/res
      yptshi=yhi/res
      zptshi=zhi/res
      ixlo=floor(xptslo)
      iylo=floor(yptslo)
      izlo=floor(zptslo)
      ixhi=ceiling(xptshi)
      iyhi=ceiling(yptshi)
      izhi=ceiling(zptshi)
      xlo=dsign((dble(ixlo))*res,xlo)
      ylo=dsign((dble(iylo))*res,ylo)
      zlo=dsign((dble(izlo))*res,zlo)
      xpoints=abs(ixlo)+abs(ixhi)
      ypoints=abs(iylo)+abs(iyhi)
      zpoints=abs(izlo)+abs(izhi)
      xhi=xlo+xpoints*res
      yhi=ylo+ypoints*res
      zhi=zlo+zpoints*res
      sumpoints=xpoints+ypoints+zpoints
! debug print
!     write(UNIout,'(a)') "Box low, high and points"
!     write(UNIout,'(a3,2(F9.4,1x),i5)') "X: ", xlo, xhi, xpoints
!     write(UNIout,'(a3,2(F9.4,1x),i5)') "Y: ", ylo, yhi, ypoints
!     write(UNIout,'(a3,2(F9.4,1x),i5)') "Z: ", zlo, zhi, zpoints
      meshmade=.true.
      maxsurface=2*((xpoints*ypoints)+(ypoints*(zpoints-2))+((xpoints-2)*(zpoints-2)))
!      write(uniout,*) 'Maxsurface is ',maxsurface
      if(sumpoints.le.1000) toobig=.false.
      if(.not.toobig) exit cyclemesh
!      write(UNIout,'(a35,f6.3,a13)') 'Default or requested resolution of ',res,' is too fine.'
!      write(UNIout,*) sumpoints
!      res=dble(xpoints+ypoints+zpoints)/1000.0*res
!      write(UNIout,*) res
!      res=res*1000.0
!      write(UNIout,*) res, ceiling(res), dble(ceiling(res)/1000.)
!      res=dble(ceiling(res)/1000.)
!      write(UNIout,'(a23,f6.3)') 'Resizing resolution to ', res
      end do cyclemesh

      call PRG_manager ('exit', 'SGM_DEFAULT_MESH', 'UTILITY')
     return

      end subroutine SGM_default_mesh

      subroutine SGM_mesh_maker
!*****************************************************************************************************************
!     Date last modified: January 12, 2015                                                          Version 1.0  *
!     Author: P.L. Warburton                                                                                     *
!     Description: Determines the defualt mesh size for SGM applications                                         *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE SGM_defaults
      USE type_molecule

      implicit none
!
! Local scalars:
      double precision :: tempres      
! Begin:
      call PRG_manager ('enter', 'SGM_MESH_MAKER', 'UTILITY')
! for initial mesh check we don't need a robust mesh.  resolution 0.5 should do
! store the resolution for reset after coarse mesh check

       tempres=res
       res=0.5

! determine the boxmargin on the coarse mesh
!       write(UNIout,('(a41,f6.3,a4)')) '*** MESH CHECK ON COARSE MESH RESOLUTION ',res,' ***'
       do
!        write(UNIout,('(a13,f6.3)')) 'Boxmargin is ', boxmargin
        if((.not.meshmade).or.(.not.boxmarginset)) call SGM_default_mesh
         if(meshmade) then
          if(proptype.eq.'DEN') call SGM_checkmesh_density
          if(proptype.eq.'NUC') call SGM_checkmesh_nucpo
          if(proptype.eq.'RAD') call SGM_checkmesh_radial_density
         if(.not.mesherror) then
          boxmarginset=.true.
          meshmade=.false.
          exit
         end if
        end if
       end do  
!       write(UNIout,('(a25,f6.3)')) 'Need a box margin around ',boxmargin-res
       meshcheck2=.true.

! now we go back to a lower boxmargin and determine boxmargin for fine mesh
       boxmarginset=.false.
       boxmargin=boxmargin-res
       res=tempres

! determine the boxmargin on the requested mesh resolution
!       write(UNIout,('(a42,f6.3,a4)')) '*** MESH CHECK ON DESIRED MESH RESOLUTION ',res,' ***'
       do
!        write(UNIout,('(a13,f6.3)')) 'Boxmargin is ', boxmargin
        if((.not.meshmade).or.(.not.boxmarginset)) call SGM_default_mesh
        if(meshmade) then
          if(proptype.eq.'DEN') call SGM_checkmesh_density
          if(proptype.eq.'NUC') call SGM_checkmesh_nucpo
          if(proptype.eq.'RAD') call SGM_checkmesh_radial_density
          if(.not.mesherror) then
            boxmarginset=.true.
            meshmade=.true.
            allocate(property(xpoints,ypoints,zpoints))
            allocate(secondev(xpoints,ypoints,zpoints))
            property(:,:,:)=0.0d0
            secondev(:,:,:)=0.0d0
            meshallocated=.true.
!            write(UNIout,'(a28,a3,a20)') 'Mesh has been generated for ',proptype,' SGM map generation.'
            write(UNIout,('(a13,f6.3)')) 'Boxmargin is ', boxmargin
            exit
          end if
        end if
       end do 


      call PRG_manager ('exit', 'SGM_MESH_MAKER', 'UTILITY')
     return

      end subroutine SGM_mesh_maker
