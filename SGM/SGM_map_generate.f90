      subroutine SGM_map_density
!*****************************************************************************************************************
!     Date last modified: January 12, 2015                                                          Version 1.0  *
!     Author: P.L. Warburton                                                                                     *
!     Description: Generates the shape group map for the electron density of a molecule or frqagment             *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE SGM_defaults
      USE type_molecule
      USE QM_defaults

      implicit none
!
! Local scalars:
      double precision :: tempres
! Local Array	  

! Begin:
      call PRG_manager ('enter', 'SGM_MAP_DENSITY', 'UTILITY')
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)

! Default
      proptype='DEN'
      if (res.eq.0.0) res=0.20
      boundlo=-3.0
      boundhi=0.0
      increment=0.05
      search=3.0
      thick=2.0
      boxmargin=2.5
      meshcheck2=.false.

! make the mesh
      call SGM_mesh_maker

! generate prime numbers for encoding
      call SGM_primes

! generate the electron density on the mesh
      call SGM_mesh_density

! there are 41 reference curvatures 
      do refindex=1,41
! zero the isocontour index each time we change the reference curvature
! to avoid indexing segmentation faults
       contindex=0
       if(refindex.eq.21) then 
         refcurve=0.0d0
       end if
       if(refindex.lt.21) then
         refcurve=-0.25*(dble(refindex-1))
         refcurve=dexp(dlog(10.0d0)*refcurve)
       end if
       if(refindex.gt.21) then
         refcurve=0.25*(dble(refindex-41))
         refcurve=-dexp(dlog(10.0d0)*refcurve)
       end if
!       write(UNIout,'(a27,es12.5)') 'The reference curvature is ',refcurve      
       call SGM_clumping 
      end do

      call SGM_map_print

! these domains were allocated in the mesh generation and primes subroutines
       if(allocated(primes)) deallocate(primes)
       if(allocated(property)) deallocate(property)
       if(allocated(secondev)) deallocate(secondev) 

      call PRG_manager ('exit', 'SGM_MAP_DENSITY', 'UTILITY')
      return
      end subroutine SGM_map_density

      subroutine SGM_map_nuclear_potential
!*****************************************************************************************************************
!     Date last modified: January 13, 2015                                                          Version 1.0  *
!     Author: P.L. Warburton                                                                                     *
!     Description: Generates the shape group map for the charge compressed nuclear potential of a molecule or    *
!                  fragment                                                                                      *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE SGM_defaults
      USE type_molecule

      implicit none
!
! Local scalars:
!
! Begin:
      call PRG_manager ('enter', 'SGM_MAP_NUCLEAR_POTENTIAL', 'UTILITY')
! Default
      proptype='NUC'
      if (res.eq.0.0) res=0.20
      boundlo=0.6
      boundhi=1.0
      increment=0.01
      search=3.0
      thick=2.0
      boxmargin=1.0
      meshcheck2=.false.
      write(UNIout,*) 'Boundlo: ',dexp(dlog(10.0d0)*boundlo)
      write(UNIout,*) 'Boundhi: ',dexp(dlog(10.0d0)*boundhi)


! generate the mesh
!      call SGM_mesh_maker

! generate prime numbers for encoding if it hasn't been done
!      call SGM_primes

! generate the nuclear potential on the mesh
!      call SGM_mesh_nucpo

! there are 41 reference curvatures 
!      do refindex=1,41
! zero the isocontour index each time we change the reference curvature
! to avoid indexing segmentation faults
!       contindex=0
!       if(refindex.eq.21) then 
!         refcurve=0.0d0
!       end if
!       if(refindex.lt.21) then
!         refcurve=-0.25*(dble(refindex-1))
!         refcurve=dexp(dlog(10.0d0)*refcurve)
!       end if
!       if(refindex.gt.21) then
!         refcurve=0.25*(dble(refindex-41))
!         refcurve=-dexp(dlog(10.0d0)*refcurve)
!       end if
!       write(UNIout,'(a27,es12.5)') 'The reference curvature is ',refcurve      
!       call SGM_clumping 
!      end do


!      call SGM_map_print


      write(*,*) 'SGM_map_nuclear_potential is not yet implemented'

! these domains were allocated in the mesh generation and primes subroutines
       if(allocated(primes)) deallocate(primes)
       if(allocated(property)) deallocate(property)
       if(allocated(secondev)) deallocate(secondev)
 
      call PRG_manager ('exit', 'SGM_MAP_NUCLEAR_POTENTIAL', 'UTILITY')
      return
      end subroutine SGM_map_nuclear_potential

      subroutine SGM_map_electrostatic_potential
!*****************************************************************************************************************
!     Date last modified: September 4, 2014                                                         Version 1.0  *
!     Author: P.L. Warburton                                                                                     *
!     Description: Generates the shape group map for the electrostatic potential of a molecule or fragment       *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE SGM_defaults
      USE type_molecule

      implicit none
!
! Local scalars:
!
! Begin:
      call PRG_manager ('enter', 'SGM_MAP_ELECTROSTATIC_POTENTIAL', 'UTILITY')

      write(*,*) 'SGM_map_electrostatic_potential is not yet implemented'

! these domains were allocated in the mesh generation and primes subroutines
       if(allocated(primes)) deallocate(primes)
       if(allocated(property)) deallocate(property)
       if(allocated(secondev)) deallocate(secondev)
 
      call PRG_manager ('exit', 'SGM_MAP_ELECTROSTATIC_POTENTIAL', 'UTILITY')
      return
      end subroutine SGM_map_electrostatic_potential

      subroutine SGM_map_radial_density
!*****************************************************************************************************************
!     Date last modified: January 22, 2015                                                          Version 1.0  *
!     Author: P.L. Warburton                                                                                     *
!     Description: Generates the shape group map for the radial electron density of a molecule or fragment       *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE SGM_defaults
      USE type_molecule
      USE QM_defaults

      implicit none
!
! Local scalars:

! Begin:
      call PRG_manager ('enter', 'SGM_MAP_RADIAL_DENSITY', 'UTILITY')
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)

! Default
      proptype='RAD'
      if (res.eq.0.0) res=0.20
      boundlo=-2.7
      boundhi=0.3
      increment=0.05
      search=3.0
      thick=2.0
      boxmargin=2.5
      meshcheck2=.false.

! make the mesh
      call SGM_mesh_maker

! generate prime numbers for encoding
      call SGM_primes

! generate the electron density on the mesh
      call SGM_mesh_radial_density

! there are 41 reference curvatures 
      do refindex=1,41
! zero the isocontour index each time we change the reference curvature
! to avoid indexing segmentation faults
       contindex=0
       if(refindex.eq.21) then 
         refcurve=0.0d0
       end if
       if(refindex.lt.21) then
         refcurve=-0.20*(dble(refindex-1))
         refcurve=dexp(dlog(10.0d0)*refcurve)*4.0d0
       end if
       if(refindex.gt.21) then
         refcurve=0.20*(dble(refindex-41))
         refcurve=-dexp(dlog(10.0d0)*refcurve)*4.0d0
       end if
!      write(UNIout,'(a27,es12.5)') 'The reference curvature is ',refcurve      
       call SGM_clumping 
      end do

      call SGM_map_print


! these domains were allocated in the mesh generation and primes subroutines
       if(allocated(primes)) deallocate(primes)
       if(allocated(property)) deallocate(property)
       if(allocated(secondev)) deallocate(secondev)

      call PRG_manager ('exit', 'SGM_MAP_RADIAL_DENSITY', 'UTILITY')
      return
      end subroutine SGM_map_radial_density

      subroutine SGM_map_all
!*****************************************************************************************************************
!     Date last modified: September 4, 2014                                                         Version 1.0  *
!     Author: P.L. Warburton                                                                                     *
!     Description: Generates the shape group map for the radial electron density of a molecule or fragment       *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE SGM_defaults
      USE type_molecule

      implicit none
!
! Local scalars:
!
! Begin:
      call PRG_manager ('enter', 'SGM_MAP_ALL', 'UTILITY')


      doall=.true.
      call SGM_map_density
      write(*,*) 'SGM_map_all is not yet implemented'

! these domains were allocated in the mesh generation and primes subroutines
       if(allocated(primes)) deallocate(primes)
       if(allocated(property)) deallocate(property)
       if(allocated(secondev)) deallocate(secondev)

      call PRG_manager ('exit', 'SGM_MAP_ALL', 'UTILITY')
      return
      end subroutine SGM_map_all

      subroutine SGM_map_print
!*****************************************************************************************************************
!     Date last modified: October 16, 2014                                                          Version 1.0  *
!     Author: P.L. Warburton                                                                                     *
!     Description: Prints the shape group map into the MUNgauss output file                                      *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE SGM_defaults
      USE type_molecule

      implicit none
!
! Local scalars:
     integer :: i,j, length1, length2
     character(len=128) :: str
     character(len=2048) :: str2

! 
! Begin:
      call PRG_manager ('enter', 'SGM_MAP_PRINT', 'UTILITY')
      write(UNIout,'(a29,a3,a15,f6.3)') 'Shape group map for property ',proptype,' at resolution ',res
      do i=1,61
      str2=''
        do j=1,41
!          if(dabs(abcode(i,j)).le.2147483647.0d0) then
!           write(str,'(i10)') int(abcode(i,j))
!            else
!           write(str,'(es18.11)') abcode(i,j)
!          end if
         write(str,*) abcode(i,j)
          str=adjustl(str)
          length1=len_trim(str)
          str2=trim(str2)//str(1:length1)//'*'
          length2=len_trim(str2)
        end do
          finalsize=length2
          write(UNIout,'(a)') str2(1:length2)
      end do


      call PRG_manager ('exit', 'SGM_MAP_PRINT', 'UTILITY')
      return
      end subroutine SGM_map_print
      

