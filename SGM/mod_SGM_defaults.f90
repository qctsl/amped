      module SGM_defaults
!*****************************************************************************************************************
!     Date last modified: July 10, 2015                                                             Version 1.0  *
!     Author: P.L. Warburton                                                                                     *
!     Description: Defaults associated with the shape group method code                                          *
!*****************************************************************************************************************
      USE program_parser

      implicit none

      character(len=8) :: SGgrid  ! Use code-generated or user-specified grid
      character(len=8) :: Fragment  ! Full molecule or fragment
      character(len=3) :: proptype ! keep track of property type
      character(len=MAX_length) sgmpathin, sgmpathout, basename
      integer :: xpoints, ypoints, zpoints ! mesh points
      double precision :: res ! mesh resolution
      double precision :: xlo, ylo, zlo, xhi, yhi, zhi ! for mesh generation
      double precision :: boxmargin ! for mesh generation
      double precision :: xptslo, yptslo, zptslo ! for mesh generation
      double precision :: xptshi, yptshi, zptshi ! for mesh generation
      double precision :: boundlo, boundhi, increment, search, thick, refcurve ! used in ab map generation (clumping)  
      double precision :: prop ! used in ab map generation (clumping)  
      double precision :: evhi,evlo,evinc ! used in ab map generation (clumping)
      double precision :: multiplier   
      integer :: ixlo,ixhi,iylo,iyhi,izlo,izhi ! for mesh generation
      integer :: refindex, contindex, maxsurface, pntstrunc, pntskept, jcount  ! used in ab map generation (clumping)
      integer :: incontour, samepiece, diffpiece ! used in ab map generation (clumping)
      integer :: missd1flag, icount, diffhole, samehole, kptemp, tptemp ! used in ab map generation (clumping)
      integer :: tocompare, totcodes ! for SGM map comparisons
      integer :: finalsize ! used in shape group map printing
      logical :: madeprimes ! for prime number encoding
      logical :: meshmade, meshallocated, domainsallocated ! for allocation purposes
      logical :: doall, mesherror, boxmarginset, meshcheck2
! Arrays
      logical :: inMG, alone, style ! SGM map comparison file outputs
      integer :: leninstrpath, lenoutstrpath, sgmcdim
      integer, allocatable, dimension(:) :: fragindex, primes
      double precision, allocatable, dimension(:,:,:) :: property, secondev 
      double precision, allocatable, dimension(:,:) :: comparematrixdecimal
      character(len=7), allocatable, dimension(:,:) :: comparematrixpercent 
      double precision, allocatable, dimension(:,:,:) :: comparedim3decimal
      character(len=7), allocatable, dimension(:,:,:) :: comparedim3percent 
      integer, allocatable, dimension(:,:,:) :: domains
      integer, allocatable, dimension(:) :: holes, truncpiece, keptpiece, pntsinpiece, pntsinhole
      integer, allocatable, dimension(:,:) :: truncate, keep, connect
!      double precision, allocatable, dimension(:,:,:,:) :: d2property 
      integer( selected_int_kind(18)), dimension(61,41)  :: abcode
      integer( selected_int_kind(18)), allocatable, dimension(:,:,:)  :: comparemaps
      character(len=MAX_length) :: mapfile_list(MAX_files)

      interface PRT_MATRIXSGM
        module procedure PRT_matrixSGMDint
        module procedure PRT_matrixSGMPint
      end interface

CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
     subroutine PRT_matrixSGMDint (Array,  & ! Array to be printed
                              Nrow,   & ! Dimension (NROWS)
                              Ncol)  ! Dimension (NCOLUMNS)
!***********************************************************************
!     Date last modified: June 26, 1992                                *
!     Author:                                                          *
!     Description: Matrix print routine.                               *
!***********************************************************************
! Modules:
      USE program_files

      implicit none
!
! Input scalars:
      integer, intent(in) :: Nrow,Ncol
!
! Input array:
      double precision, dimension(:,:), intent(in) :: Array
!
! Local scalars:
      integer Irow,ifLAG,ILOWER,IUPPER,Jcol
!
! Begin:
      call PRG_manager ('enter', 'PRT_matrixSGMDint', 'UTILITY')
      ifLAG=1
      ILOWER=1
 Icol:do
      IUPPER=ILOWER+9
      if(IUPPER.GE.Ncol)then
         IUPPER=Ncol
         ifLAG=0
      end if

      write(UNIout,'(4X,10(6X,I4))')(Jcol,Jcol=ILOWER,IUPPER)
      do Irow=1,Nrow
         write(UNIout,'(1X,I4,2X,10F10.5)')Irow,(Array(Irow,Jcol),Jcol=ILOWER,IUPPER)
      end do ! Irow
      write(UNIout,'(X)')

      if(ifLAG.eq.0)exit Icol
      ILOWER=ILOWER+10
      end do Icol

! End of routine PRT_matrixSGMDint
      call PRG_manager ('exit', 'PRT_matrixSGMDint', 'UTILITY')
      return
      end subroutine PRT_matrixSGMDint

     subroutine PRT_matrixSGMPint (Array,  & ! Array to be printed
                              Nrow,   & ! Dimension (NROWS)
                              Ncol)  ! Dimension (NCOLUMNS)
!***********************************************************************
!     Date last modified: June 26, 1992                                *
!     Author:                                                          *
!     Description: Matrix print routine.                               *
!***********************************************************************
! Modules:
      USE program_files

      implicit none
!
! Input scalars:
      integer, intent(in) :: Nrow,Ncol
!
! Input array:
      character(len=7), dimension(:,:), intent(in) :: Array
!
! Local scalars:
      integer Irow,ifLAG,ILOWER,IUPPER,Jcol
!
! Begin:
      call PRG_manager ('enter', 'PRT_matrixSGMDint', 'UTILITY')

      ifLAG=1
      ILOWER=1
 Icol:do
      IUPPER=ILOWER+9
      if(IUPPER.GE.Ncol)then
         IUPPER=Ncol
         ifLAG=0
      end if

      write(UNIout,'(4X,10(6X,I4))')(Jcol,Jcol=ILOWER,IUPPER)
      do Irow=1,Nrow
         write(UNIout,'(1X,I4,2X,10a10)')Irow,(Array(Irow,Jcol),Jcol=ILOWER,IUPPER)
      end do ! Irow
      write(UNIout,'(X)')

      if(ifLAG.eq.0)exit Icol
      ILOWER=ILOWER+10
      end do Icol

! End of routine PRT_matrixSGMPint
      call PRG_manager ('exit', 'PRT_matrixSGMPint', 'UTILITY')
      return
      end subroutine PRT_matrixSGMPint

      end module SGM_defaults
