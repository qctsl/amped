      subroutine SGM_PRINT_manager (ObjName, Modality)
!********************************************************************************
!     Date last modified: April 24, 2008                           Version 2.0 *
!     Author: R.A. Poirier                                                      *
!     Description: Calls the appropriate routine to "Command" the object nameIN *
!     Command can be "PRINT" or "KILL"                                          *
!     Pwhat can be "OBJECT", "DOCUMENTATION" or "TABLE"                         *
!********************************************************************************
! Modules:
      USE program_files
      USE program_objects

      implicit none
!
! Input scalars:
      character*(*), intent(IN) :: ObjName,Modality
!
! Local scalars:
!
! Begin:
      call PRG_manager ('enter', 'SGM_PRINT_manager', 'UTILITY')

      SelectObject:   select case (ObjName)
      case ('MAP')
      case ('COMPARE')
      case default
        write(UNIout,*)'SGM_PRINT_manager>'
        write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                       '" for Modality "',Modality(1:len_trim(Modality)),'"'
      end select SelectObject
!
! end of function SGM_PRINT_manager
      call PRG_manager ('exit', 'SGM_PRINT_manager', 'UTILITY')
      return
      end subroutine SGM_PRINT_manager
