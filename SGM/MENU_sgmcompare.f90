      subroutine MENU_sgmcompare
!*************************************************************************************************
!     Date last modified:  July 10, 2015                                                         *
!     Author:  P. L. Warburton                                                                   *
!     Description: MENU for reading SGM map file names for SGM map comparison                    *
!*************************************************************************************************
! Modules
      USE program_parser
      USE menu_gets
      USE SGM_defaults

      implicit none
!
! Local scalars:
      integer :: lenName,lenstr
      logical :: done,Lerror
      character(len=MAX_length) File_name,string
!      character(len=MAX_length) :: File_list(MAX_files)

!
! Begin:
      call PRG_manager ('enter', 'MENU_sgmcompare', 'UTILITY')
!
      inMG=.true.
      alone=.false.
      style=.true.
      tocompare=1
      sgmcdim=2
      sgmpathin=''
      sgmpathout=''
      basename='SGMcompare'
      leninstrpath=0
      lenoutstrpath=0
      done=.false.
      do while (.not.done)
      call new_token (' SGMCompare:')
      if     (token(1:4).eq.'HELP')then
      write(UNIout,'(A)') &
      ' Command "SGMCompare":', &
      '   Purpose: Provide list of SGM map files for map comparison objects', &
      '   Syntax SGMCompare                                  ', &
      '   INpath = <string>, i.e., Directory containing all SGM map files.  Default is mgauss executable working directory', &
      '   OUtpath = <string>, i.e., Path for the output file(s) (see REsults).  Default is INpath', &
      '   DImension = <integer>, i.e., Highest dimension of comparison to make.  Currently allows 2 (default) or 3', &
      '   REsults = <string>, i.e., Comparison matrix and legend file(s).  MUngauss output (default), STand-alone CSV, or BOth', &
      '   BAsename = <string>, i.e., Base name for the STand-alone output files.  Defualt is "SGMcompare"', &
      '   STyle = <string>, i.e., Similarity comparison matrix style.  DEcimal (default) or PErcent', &
      '   NAme (file) = <string>, i.e., Shape group map file name (one per file) ', &
      '   end'

!
! INpath
      else if(token(1:2).eq.'IN')then
        call GET_value (string, leninstrpath)
        sgmpathin=string(1:leninstrpath)
        sgmpathout=string(1:leninstrpath)
!

!
! OUtpath
      else if(token(1:2).eq.'OU')then
        call GET_value (string, lenoutstrpath)
        sgmpathout=string(1:lenoutstrpath)
!

!
! REsults
      else if(token(1:2).eq.'RE')then
        call GET_value (string, lenstr)
        if(string(1:2).eq.'ST') then
          inMG=.false.
          alone=.true.
        else if(string(1:2).eq.'BO') then 
          alone=.true.
          inMG=.true.
        else
          inMG=.true.
          alone=.false.
        end if
!
! STyle
      else if(token(1:2).eq.'ST')then
        call GET_value (string, lenstr)
        if(string(1:2).eq.'PE') style=.false.
!

!
! DImension
      else if(token(1:2).eq.'DI')then
        call GET_value (sgmcdim)
!

!
! NAme
      else if(token(1:2).eq.'NA')then
        call GET_value (string, lenstr)
        lenName=leninstrpath+lenstr
        File_name=sgmpathin(1:leninstrpath)//string(1:lenstr)
        File_name=File_name(1:lenName)
        mapfile_list(tocompare)=File_name
        tocompare=tocompare+1
      else
         tocompare=tocompare-1
         call MENU_end (done)
      end if

      end do ! while

! end of routine MENU_sgmcompare
      call PRG_manager ('exit', 'MENU_sgmcompare', 'UTILITY')

      return
      end subroutine MENU_sgmcompare
