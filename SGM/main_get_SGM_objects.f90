      subroutine GET_SGM_object (class, Objname, Modality)
!**************************************************************************
!     Date last modified: Sept 4, 2014                        Version 1.0 *
!     Author: P.L. Warburton                                              *
!     Description: Objects belonging to the class SGM (Shape Group Method)*
!**************************************************************************
! Modules:
      USE program_files
      USE program_objects

      implicit none
!
! Input scalar:
      character*(*) :: class
      character*(*) :: Objname
      character*(*) :: Modality
!
! Local scalars:
      integer :: Object_number
!
! Begin:
      call PRG_manager ('enter', 'GET_SGM_object', 'UTILITY')

      call GET_object_number (OBJ_SGM, NSGMobjects, FirstSGM, Objname, Modality, Object_number)

      if(OBJ_SGM(Object_number)%current)then
        call PRG_manager ('exit', 'GET_SGM_object', 'UTILITY')
        return
      end if

      OBJ_SGM(Object_number)%exist=.true.
      OBJ_SGM(Object_number)%Current=.true.
      Object(ObjNum)%exist=.true.
      Object(ObjNum)%Current=.true.

      select_Object: select case (Objname)
      case ('MAP') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      select case (Modality)
      case ('DENSITY')
       call SGM_map_density
      case ('NUCPO')
       call SGM_map_nuclear_potential
      case ('ESP')
       call SGM_map_electrostatic_potential
      case ('RADIAL')
       call SGM_map_radial_density
      case ('ALL')
       call SGM_map_all
      case default
        write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                       '" for Modality "',Modality(1:len_trim(Modality)),'"'
        stop 'No such object'
      end select

      case ('COMPARE') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      select case (Modality)
      case ('DENSITY')
       call SGM_compare_general
      case ('NUCPO')
       call SGM_compare_general
      case ('ESP')
       call SGM_compare_general
      case ('RADIAL')
       call SGM_compare_general
      case ('ALL')
       call SGM_compare_general
      case default
        write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                       '" for Modality "',Modality(1:len_trim(Modality)),'"'
        stop 'No such object'
      end select

      case default
        write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                       '" for Modality "',Modality(1:len_trim(Modality)),'"'
        stop 'No such object'

      end select select_Object

      call PRG_manager ('exit', 'GET_SGM_object', 'UTILITY')
      return
      end subroutine GET_SGM_object
