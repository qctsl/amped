      subroutine SGM_compare_general
!*****************************************************************************************************************
!     Date last modified: July 24, 2015                                                             Version 1.0  *
!     Author: P.L. Warburton                                                                                     *
!     Description: Compares the shape group maps for the requested property type                                 *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE SGM_defaults
      USE program_objects
      USE QM_defaults


      implicit none
!
! Local scalars:
      integer :: READ_unit, loop
      character(len=MAX_Objlen) :: Modality

! Local Array	  

! Begin:
      call PRG_manager ('enter', 'SGM_COMPARE_GENERAL', 'UTILITY')

      totcodes=2501 ! total number of codes in a map is 61x41 (modern implementation)
      Modality=Object(ObjNum)%modality

! Property type for map comparison
      select case (Modality)
      case ('DENSITY')
       proptype='DEN'
      case ('NUCPO')
       call PRG_stop ('Requested SGM map type not yet implemented.')
      case ('ESP')
       call PRG_stop ('Requested SGM map type not yet implemented.')
      case ('RADIAL')
       proptype='RAD'
      case ('ALL')
       call PRG_stop ('Requested SGM map type not yet implemented.')
      case default
        write(UNIout,*)'Modality "',Modality(1:len_trim(Modality)),'" does not exist'
        stop 'No such object'
      end select

      if (sgmcdim.gt.tocompare) then 
       call PRG_stop ('Requested SGM comparison dimension larger than number of maps to compare.')
      endif

      if(style) then
        allocate(comparematrixdecimal(tocompare,tocompare))
        if(sgmcdim.ge.3) allocate(comparedim3decimal(tocompare,tocompare,tocompare))
      else
        allocate(comparematrixpercent(tocompare,tocompare))
        if(sgmcdim.ge.3) allocate(comparedim3percent(tocompare,tocompare,tocompare))
      end if

      allocate(comparemaps(61,41,tocompare))

      do loop=1,tocompare
         call SGM_map_file_read(loop) 
      end do

      call SGM_compare_matrix_dim2(loop)

      if (style.and.inMG) then
         write(UNIout,'(a38,a3)') 'SGM 2D comparison matrix for property ',proptype
         call PRT_matrixSGMDint(comparematrixdecimal,tocompare,tocompare)
         call SGM_legend_print_internal
      end if
      if ((.not.style).and.inMG) then
         write(UNIout,'(a38,a3)') 'SGM 2D comparison matrix for property ',proptype
         call PRT_matrixSGMPint(comparematrixpercent,tocompare,tocompare)
         call SGM_legend_print_internal
      end if

      if (style.and.inMG.and.(sgmcdim.eq.3)) then
         write(UNIout,'(X)')
         write(UNIout,'(a38,a3)') 'SGM 3D comparison matrix for property ',proptype
         call SGM_compare_matrix_dim3(tocompare)
         call PRT_dim3SGMDint
         call SGM_legend_print_internal
      end if
      if ((.not.style).and.inMG.and.(sgmcdim.eq.3)) then
         write(UNIout,'(X)')
         write(UNIout,'(a38,a3)') 'SGM 3D comparison matrix for property ',proptype
         call SGM_compare_matrix_dim3(tocompare)
         call PRT_dim3SGMPint
         call SGM_legend_print_internal
      end if

      deallocate(comparemaps)

      if(allocated(comparematrixdecimal)) deallocate(comparematrixdecimal)
      if(allocated(comparematrixpercent)) deallocate(comparematrixpercent)
      if(allocated(comparedim3decimal)) deallocate(comparedim3decimal)
      if(allocated(comparedim3percent)) deallocate(comparedim3percent)
      
      call PRG_manager ('exit', 'SGM_COMPARE_GENERAL', 'UTILITY')
      return

      end subroutine SGM_compare_general
      subroutine SGM_map_file_read(loop)
!***********************************************************************
!     Date last modified: July 10, 2015                   Version 1.0  *
!     Authors: P.L. Warburton                                          *
!     Desciption: Read SGM map information from MUNgauss output file   *
!***********************************************************************
! Modules:
      USE SGM_defaults
      USE matrix_print


      implicit none

      integer :: READ_unit, loop, counter, secondloop, nextpos, backpos
      logical :: Lerror, foundit, exists
      character(len=32) :: looking, text
      character(len=2048) :: shapecodes

! Begin:
      call PRG_manager ('enter', 'SGM_MAP_FILE_READ', 'UTILITY')

      looking='Shape group map for property '//proptype
      foundit=.false.
      exists=.true.
      counter=1
      nextpos=1
      backpos=2048

      call GET_unit (mapfile_list(loop), READ_unit, Lerror)
      PRG_file(READ_unit)%status='OLD '
      PRG_file(READ_unit)%name=mapfile_list(loop)
      PRG_file(READ_unit)%form='FORMATTED'
      PRG_file(READ_unit)%type='MUN_OUTPUT'
      call FILE_open (READ_unit)


      firstloop: do ! loop until find SGM map info for desired property
        read(READ_unit,'(a32)') text
        if (index(text,looking).ne.0) foundit=.true.
        if (index(text,'Program terminated normally').ne.0) exists=.false.
        counter=counter+1
        if (foundit) exit
        if (.not.exists) then
         write(UNIout,'(a49,a3,a21)') '**WARNING** This file does not appear to contain ' ,proptype,' SGM map information.'
         exit
        end if
      end do firstloop

      if (.not.exists) then 
       call FILE_release(READ_unit)
       call PRG_stop ('Requested SGM map information does not exist in file')
      endif

      do secondloop=1,61
        read(READ_unit,'(a)') shapecodes
        counter=1
       
       codesread: do ! loop until all 41 shape codes have been found
         nextpos=scan(shapecodes,'*',.false.)
         read(shapecodes(1:nextpos-1),'(i20)') comparemaps(secondloop,counter,loop)
         shapecodes=shapecodes(nextpos+1:backpos)
        counter=counter+1
        if(counter.eq.42) exit
       end do codesread

      end do

      call FILE_release(READ_unit)

      call PRG_manager ('exit', 'SGM_MAP_FILE_READ', 'UTILITY')
      return

      end subroutine SGM_map_file_read 

      subroutine SGM_compare_matrix_dim2(loop)
!**************************************************************************
!     Date last modified: July 14, 2015                   Version 1.0     *
!     Authors: P.L. Warburton                                             *
!     Desciption: Creates SGM similarity comparison matrix for dimension 3*
!**************************************************************************
! Modules:
      USE SGM_defaults
!      USE matrix_print

      implicit none

      integer :: first, second, third, fourth, matches, loop
      double precision :: asdecimal
      character(len=6) :: percent

! Begin:
      call PRG_manager ('enter', 'SGM_COMPARE_MATRIX_dim2', 'UTILITY')

      do first=1,tocompare
        do second=1,tocompare
          matches=0
          do third=1,61
            do fourth=1,41
              if(comparemaps(third,fourth,first).eq.comparemaps(third,fourth,second)) then
                matches=matches+1
              end if
!              write(*,*) third,fourth,comparemaps(third,fourth,first),comparemaps(third,fourth,second),matches
            end do
          end do
          asdecimal=matches/dble(totcodes)
          if(style) then
            comparematrixdecimal(first,second)=asdecimal
!            write(UNIout,'(a8,f7.5)') 'decimal ', comparematrixdecimal(first,second)
          else
            asdecimal=asdecimal*100.
            if(asdecimal.lt.100.) then
             write(percent,'(f6.3)') asdecimal
            else
             write(percent,'(f6.2)') asdecimal
            end if
            comparematrixpercent(first,second)=percent//'%'
!            write(*,*) 'percent ', comparematrixpercent(first,second)
          end if 
         end do
      end do

      call PRG_manager ('exit', 'SGM_COMPARE_MATRIX_dim2', 'UTILITY')
      return

      end subroutine SGM_compare_matrix_dim2 

      subroutine SGM_compare_matrix_dim3(loop)
!**************************************************************************
!     Date last modified: July 24, 2015                   Version 1.0     *
!     Authors: P.L. Warburton                                             *
!     Desciption: Creates SGM similarity comparison matrix for dimension 3*
!**************************************************************************
! Modules:
      USE SGM_defaults
!      USE matrix_print

      implicit none

      integer :: first, second, next, third, fourth, matches, loop
      double precision :: asdecimal
      character(len=6) :: percent

! Begin:
      call PRG_manager ('enter', 'SGM_COMPARE_MATRIX_dim3', 'UTILITY')

      do first=1,tocompare
        do second=1,tocompare
          do next=1,tocompare
            matches=0
            do third=1,61
              do fourth=1,41
                if(comparemaps(third,fourth,first).eq.comparemaps(third,fourth,second)) then
                  if(comparemaps(third,fourth,first).eq.comparemaps(third,fourth,next)) matches=matches+1
                end if
              end do
            end do
            asdecimal=matches/dble(totcodes)
            if(style) then
              comparedim3decimal(first,second,next)=asdecimal
            else
              asdecimal=asdecimal*100.
              if(asdecimal.lt.100.) then
               write(percent,'(f6.3)') asdecimal
              else
               write(percent,'(f6.2)') asdecimal
              end if
              comparedim3percent(first,second,next)=percent//'%'
             end if 
           end do
         end do
      end do

      call PRG_manager ('exit', 'SGM_COMPARE_MATRIX_dim3', 'UTILITY')
      return

      end subroutine SGM_compare_matrix_dim3 


     subroutine PRT_dim3SGMDint 
!***********************************************************************
!     Date last modified: July 24, 2015                                *
!     Author:                                                          *
!     Description: 3 Dim SGM print routine.                            *
!***********************************************************************
! Modules:
      USE program_files
      USE SGM_defaults

      implicit none
!
! Input scalars:
!      integer, intent(in) :: Nrow
!
! Input array:
!      double precision, dimension(:,:,:), intent(in) :: Array

! Local scalars:
      integer mapa,mapb,mapc
!
! Begin:
      call PRG_manager ('enter', 'PRT_dim3SGMDint', 'UTILITY')

      write(UNIout,'(3(1x,a4,1x),a13)')'MapA','MapB','MapC','3D Similarity'

      do mapa=1,tocompare
        do mapb=mapa+1,tocompare
          do mapc=mapb+1,tocompare
          write(UNIout,'((3(1X,I4,1X),F10.5))') mapa,mapb,mapc,comparedim3decimal(mapa,mapb,mapc)
          end do
        end do
      end do
      write(UNIout,'(X)')

! End of routine PRT_dim3SGMDint
      call PRG_manager ('exit', 'PRT_dim3SGMDint', 'UTILITY')
      return
      end subroutine PRT_dim3SGMDint

     subroutine PRT_dim3SGMPint 
!***********************************************************************
!     Date last modified: July 24, 2015                                *
!     Author:                                                          *
!     Description: 3 Dim SGM print routine.                            *
!***********************************************************************
! Modules:
      USE program_files
      USE SGM_defaults

      implicit none
!
! Input scalars:
!      integer, intent(in) :: Nrow
!
! Input array:
!      character(len=7), dimension(:,:,:), intent(in) :: Array

! Local scalars:
      integer mapa,mapb,mapc
!
! Begin:
      call PRG_manager ('enter', 'PRT_dim3SGMPint', 'UTILITY')

      write(UNIout,'((3(1x,a4,1x),a13))')'MapA','MapB','MapC','3D Similarity'

      do mapa=1,tocompare
        do mapb=mapa+1,tocompare
          do mapc=mapb+1,tocompare
          write(UNIout,'((3(1X,I4,1X),3x,a7))') mapa,mapb,mapc,comparedim3percent(mapa,mapb,mapc)
          end do
        end do
      end do
      write(UNIout,'(X)')

! End of routine PRT_dim3SGMDint
      call PRG_manager ('exit', 'PRT_dim3SGMPint', 'UTILITY')
      return
      end subroutine PRT_dim3SGMPint

      subroutine SGM_legend_print_internal
!***********************************************************************
!     Date last modified: July 14, 2015                   Version 1.0  *
!     Authors: P.L. Warburton                                          *
!     Desciption: Print SGM comparison legend information              *
!***********************************************************************
! Modules:
      USE SGM_defaults
      USE program_files

      implicit none

      integer :: loop

      call PRG_manager ('enter', 'SGM_LEGEND_PRINT_INTERNAL', 'UTILITY')

      write(UNIout,'(a)') 'Legend:'

      do loop=1,tocompare
       write(UNIout,'(i4,1x,a)') loop,trim(mapfile_list(loop))    
      end do

      call PRG_manager ('exit', 'SGM_LEGEND_PRINT_INTERNAL', 'UTILITY')
      return

      end subroutine SGM_legend_print_internal
