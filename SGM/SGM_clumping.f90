      subroutine SGM_clumping
!*****************************************************************************************************************
!     Date last modified: October 3, 2014                                                           Version 1.0  *
!     Author: P.L. Warburton                                                                                     *
!     Description: Clumps points of the curvature domains and generates the shape codes                          *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE SGM_defaults
      USE type_molecule

      implicit none
!
! Local scalars:
      integer :: x, y, z, holemult, nholes, iflag
      integer :: i, j, xi, yi, zi, xj, yj, zj, iswitch, jswitch, temp
      integer :: pcstart, pcend, hlstart, hlend, ii, jj
      double precision :: emax, emin, proplog, amiclose
      character*3 :: txtcode
!
! Local Array	
! Begin:
      call PRG_manager ('enter','SGM_clumping', 'UTILITY')
! assuming there cannot be more than 100 convex holes per atom
! if there are issues with segmentation fault here, try increasing holemult first
! points will keep track 
      holemult=100     
!      write(UNIout,*) 'Maxsurface: ',maxsurface

       allocate(domains(xpoints,ypoints,zpoints))
       allocate(holes(Natoms*holemult))
       allocate(truncate(maxsurface,3))
       allocate(keep(maxsurface,3))
       allocate(truncpiece(maxsurface))
       allocate(keptpiece(maxsurface))
       allocate(pntsinpiece(maxsurface))
       allocate(pntsinhole(maxsurface))

 

! domains cannot be negative.  initialize them to -1 for debug purposes
! domain is locally convex (domain=2) or other (domain=0)
      domains(:,:,:)=-1
      do x=1,xpoints
       do y=1,ypoints
        do z=1,zpoints
         if(secondev(x,y,z).lt.refcurve) then
          domains(x,y,z)=2
           else
          domains(x,y,z)=0
         end if
        end do
       end do
      end do 

      isocontours: do 
       if(contindex.eq.61) exit
       proplog=boundlo+dble(contindex)*increment
!       write(*,*) 'proplog: ',proplog 
       prop=dexp(dlog(10.0d0)*proplog)
!       write(*,*) 'prop: ',prop   
       txtcode='XX '
! find the points in the surfaces and in the holes
       txtcode='XX '
       call isosurfaces
       contindex=contindex+1
!       write(UNIout,*) 'Locally convex points ',pntstrunc, 'Other points ',pntskept
!       call flush(6)

! if all points are locally convex, then they are all truncated and there is no surface
! shape code is 0 for no surface
       if(pntskept.eq.0) then
         abcode(contindex,refindex)=0
!         abcode(contindex,refindex)=0.0d0
         txtcode='LC '
!         write(UNIout,'(a3,a15,es10.3,a15,i9)') txtcode,'For isocontour ',prop,' shape code is ',abcode(contindex,refindex)
!         call flush(6)
         diffpiece=0
         diffhole=0
         cycle isocontours
       end if

! if no points are locally convex, then we have an isocontour with no truncation holes
! shape code is 2 for a single surface with no holes
       if(pntstrunc.eq.0) then
         abcode(contindex,refindex)=2
!         abcode(contindex,refindex)=2.0d0
         txtcode='NC '
!         write(UNIout,'(a3,a15,es10.3,a15,i9)') txtcode,'For isocontour ',prop,' shape code is ',abcode(contindex,refindex)
!         call flush(6)
         diffpiece=1
         diffhole=0
         holes(1)=0
         cycle isocontours
       end if

! if we have a significant number of truncated points (65+% of the number of 
! isosurface points) then it is likely more efficient to clump the truncated points
! before the kept points, otherwise we'll deal with the kept points first and count
! the pieces. we might be able to optimize this ratio later for efficiency purposes
!!
       lowkept: if(pntskept.le.(65*pntstrunc/100)) then
         if(pntskept.gt.1) then
           call keptfirst
         else
           keptpiece(1)=1
         end if
         kptemp=pntskept
         tptemp=pntstrunc
         if(pntskept.gt.2) then
           call countpieces
         else
           if(keptpiece(1).eq.keptpiece(2)) then
              diffpiece=1
           else
              diffpiece=2
           end if
         end if
! if we have one or more isosurface pieces but no truncated points then
! each isosurface piece should contain one hole
         if(pntstrunc.eq.0) then
          temp=0
          do i=1,diffpiece
! if a piece contains at least 5% of all kept isosurface points then we consider
! it to be a true piece with one hole.  if not, it's not a true piece and it
! should be ignored (labeled as negative one holes)
           if(pntsinpiece(i).gt.0.05*kptemp) then
            holes(i)=1
            temp=temp+1
           else
            holes(i)=-1
           end if
          end do
! abcode is 2 raised to the power of the number of true pieces
          abcode(contindex,refindex)=2**temp
!          abcode(contindex,refindex)=2.0d0**temp
          txtcode='TP '
!          write(UNIout,'(a3,a15,es10.3,a15,i9)') txtcode,'For isocontour ',prop,' shape code is ',abcode(contindex,refindex)
!          call flush(6)
          cycle isocontours
         end if
       else
! otherwise we'll flag that we have skipped the kept points for now  
         missd1flag=1
       end if lowkept

!!

! if there are truncated points we have gotten here
! if there are a lot of them, it makes sense to only 
! keep track of those that border the isosurface pieces
         call bordertrunc
! we need to clump the truncated points into holes and count them
         call truncfirst
         call countholes
 
! if there's only one isosurface piece or we skipped the kept points
! earlier (missd1flag=1) then the shape code is the nth prime number 
! in our prime number list where n is the number of holes 
!       if(missd1flag.eq.1.or.diffpiece.eq.1) then
       if(diffpiece.eq.1) then
         if(jcount.ne.0) then
           abcode(contindex,refindex)=primes(jcount)
!           abcode(contindex,refindex)=dble(primes(jcount))
           txtcode='TH '
         else
           abcode(contindex,refindex)=0
!           abcode(contindex,refindex)=0.0d0
           txtcode='LC*'
         end if
         diffpiece=1
         holes(1)=jcount
!         write(UNIout,*) 'The number of true holes is ', jcount         
!         write(UNIout,'(a3,a15,es10.3,a15,i9)') txtcode,'For isocontour ',prop,' shape code is ',abcode(contindex,refindex)
!         call flush(6)
         cycle isocontours
       end if

! if there is only one true hole (which implies one or more pieces) then the 
! previous code block counted holes but did not assign a shape code.
! since there are one or more disconnected pieces then we assign a
! shape code of 2 raised to the power of the number of pieces
       if(jcount.eq.1) then
         temp=0
         do i=1,diffpiece
!           write(UNIout,*) 'TP* ',i,pntsinpiece(i)
! we will treat pieces with more than 5% of the kept points points as a real piece
           if(pntsinpiece(i).gt.0.05*kptemp) then
             holes(i)=1
             temp=temp+1
           else
! not a real piece
             holes(i)=-1
           end if
         end do
! if there are no true pieces then this is equivalent to all points being truncated
         if(temp.eq.0) then
           abcode(contindex,refindex)=0
!           abcode(contindex,refindex)=0.0d0
           txtcode='NRP'
         else
           abcode(contindex,refindex)=2**temp
!           abcode(contindex,refindex)=2.0d0**temp
           txtcode='TP*'
         end if
!         write(UNIout,'(a3,a15,es10.3,a15,i9)') txtcode,'For isocontour ',prop,' shape code is ',abcode(contindex,refindex)
!         call flush(6)
         cycle isocontours
       end if       
 
! if we get here there are several true holes.  it's likely that at least one of these 
! holes is the common hole of all disconnected pieces, while others are wholly inside
! one isosurface piece.  we will use a conncection matrix to determine which is which
!       write(UNIout,*) 'True holes ', icount,' diffpiece ',diffpiece,' pntskept ',pntskept, pntstrunc
       allocate(connect(diffpiece,diffhole))
       connect(:,:)=0
       pcstart=1
       pcend=pntsinpiece(1)
       overpieces: do i=1,diffpiece
         hlstart=1
         hlend=pntsinhole(1)
         overholes: do j=1,diffhole
           do ii=pcstart,pcend
             xi=keep(ii,1)
             yi=keep(ii,2)
             zi=keep(ii,3)
             do jj=hlstart,hlend
               xj=truncate(jj,1)
               yj=truncate(jj,2)
               zj=truncate(jj,3)
! if a hole point is within search of a kept point then the hole and piece are connected       
               amiclose=dsqrt((dble(xi)-dble(xj))**2+(dble(yi)-dble(yj))**2+(dble(zi)-dble(zj))**2)
               if(amiclose.lt.search) then
                 connect(i,j)=1
                 hlstart=hlstart+pntsinhole(j)
                 hlend=hlend+pntsinhole(j+1)
!                 write(UNIout,*) 'NEW HL start ',hlstart, hlend,pntsinhole(j),pntsinhole(j+1)
                 cycle overholes
               end if
             end do
           end do
         hlstart=hlstart+pntsinhole(j)
         hlend=hlend+pntsinhole(j+1)
         end do overholes
         pcstart=pcstart+pntsinpiece(i)
         pcend=pcend+pntsinpiece(i+1)
!       write(UNIout,*) 'NEW PC start ',pcstart, pcend,pntsinpiece(i),pntsinpiece(i+1)
       end do overpieces
 
! print the connection matrix
!       write(UNIout,*) 'Connection matrix: '
!       do i=1,diffpiece
!         write(UNIout,*) (connect(i,j), j=1,diffhole)
!       end do       
! now we can count the number of holes each piece has
       do i=1,diffpiece
         do j=1,diffhole
           holes(i)=holes(i)+connect(i,j)
         end do
!        write(UNIout,*) 'Piece ',i,' has ',holes(i),' holes'
       end do
       deallocate(connect)

! now we assign shape code as a multiplicative of the shape code for each piece 
! based on its number of holes
       do i=1,diffpiece
! if a piece has zero holes, it really has one hole (the separate pieces are bound by the same hole)
         if(holes(i).eq.0) holes(i)=1
! if the piece contains more points than 5% of all truncated points (BEFORE border determination!)
! then the number of holes is uncertain 
         if(pntsinpiece(i).gt.0.05*tptemp) holes(i)=-1 
       end do

! now we sort by number of holes in each piece
       sorting: do
         iflag=0
         do i=1,diffpiece-1 
           if(holes(i).lt.holes(i+1)) then
             temp=holes(i)
             holes(i)=holes(i+1)
             holes(i+1)=temp
             iflag=1
           end if
         end do
         if(iflag.eq.0) exit sorting
       end do sorting
! remove all pieces with uncertain holes
       temp=diffpiece
       do i=1,diffpiece
         if(holes(i).eq.-1) temp=temp-1
       end do
! set default shape code to 1 for undetermined shape code assignment
       abcode(contindex,refindex)=1
! if shape code is determinable, do so
       if(temp.eq.0) then
!         abcode(contindex,refindex)=0
         abcode(contindex,refindex)=0.0d0
         txtcode='LC*'
       end if
       do i=1,temp
           if(holes(i).eq.0) then
             abcode(contindex,refindex)=abcode(contindex,refindex)*2
!             abcode(contindex,refindex)=abcode(contindex,refindex)*2.0d0
             txtcode='PH '
           else
             abcode(contindex,refindex)=abcode(contindex,refindex)*primes(holes(i))  
!             abcode(contindex,refindex)=abcode(contindex,refindex)*dble(primes(holes(i)))  
             txtcode='PH '           
           end if
!        write(*,*) i,holes(i),primes(i),abcode(contindex,refindex)
       end do  
  
!       if(txtcode.eq.'XX ') then
!         write(UNIout,'(a18,es10.3,a28)') 'XX For isocontour ',prop,' shape code is not yet coded'
!       else
!         write(UNIout,'(a3,a15,es10.3,a15,i20)') txtcode,'For isocontour ',prop,' shape code is ',abcode(contindex,refindex)        
!       end if
       call flush(6)

       end do isocontours

       deallocate(domains)
       deallocate(holes)
       deallocate(truncate)
       deallocate(keep)
       deallocate(truncpiece)
       deallocate(keptpiece)
       deallocate(pntsinpiece)
       deallocate(pntsinhole)


      call PRG_manager ('exit','SGM_clumping', 'UTILITY') 
      return
      end subroutine SGM_clumping


      subroutine isosurfaces
! modules
      USE SGM_defaults
      USE program_files
! local scalar
      integer :: x, y, z
! begin
      call PRG_manager ('enter','isosurfaces', 'UTILITY')
! isocontour will be those points with property value greater
! than prop that also reside directly next to a point where the
! property value is less than prop. however we must be careful
! about isocontour points right on the edge of the mesh. since
! the mesh check tries to minimize the required mesh, this can
! happen and lead to indexing segmentation fault
!       write(UNIout,*) 'Entered isosurfaces'
!       call flush(6)
       diffpiece=1
       diffhole=1
       missd1flag=0
       missd2flag=0
       pntstrunc=0
       pntskept=0
       tptemp=0
       kptemp=0
       holes(:)=0
       keep(:,:)=0
       truncate(:,:)=0
       truncpiece(:)=0
       keptpiece(:)=0
       pntsinpiece(:)=0
       pntsinhole(:)=0
       emax=-1.0d10
       emin=1.0d10
        do x=1,xpoints
         do y=1,ypoints
          do z=1,zpoints
          if(property(x,y,z).ge.prop) then
           incontour=0
            if(x.eq.1.or.y.eq.1.or.z.eq.1.or.x.eq.xpoints.or.y.eq.ypoints.or.z.eq.zpoints) then
             incontour=1
              cycle
            end if
           if(property(x+1,y,z).lt.prop) incontour=1
           if(property(x-1,y,z).lt.prop) incontour=1
           if(property(x,y+1,z).lt.prop) incontour=1
           if(property(x,y-1,z).lt.prop) incontour=1
           if(property(x,y,z+1).lt.prop) incontour=1
           if(property(x,y,z-1).lt.prop) incontour=1
           if(incontour.eq.1) then
             emin=dmin1(emin,secondev(x,y,z))
             emax=dmax1(emax,secondev(x,y,z))
              if(domains(x,y,z).ne.2) then
                pntskept=pntskept+1
                keep(pntskept,1)=x
                keep(pntskept,2)=y
                keep(pntskept,3)=z
! while indexing the points we assume each point is its own piece of isosurface
! until we clump them together
                keptpiece(pntskept)=pntskept
              else
                pntstrunc=pntstrunc+1
                truncate(pntstrunc,1)=x
                truncate(pntstrunc,2)=y
                truncate(pntstrunc,3)=z
! while checking the points we assume each point is its own piece of isosurface
! until we clump them together
                truncpiece(pntstrunc)=pntstrunc
              end if
           end if
          end if
          end do !x
         end do !y
        end do !z
        kptemp=pntskept
        tptemp=pntstrunc
!        write(UNIout,*) 'Isosurfaces pntskept ',pntskept,' pntstrunc ',pntstrunc
!        write(Uniout,*) 'emax ',emax,' emin ',emin
!       write(UNIout,*) 'Exited isosurfaces'
!       call flush(6)
      call PRG_manager ('exit','isosurfaces', 'UTILITY')
      return
      end subroutine isosurfaces



      subroutine keptfirst
! modules
      USE SGM_defaults
      USE program_files
! local scalar
      integer :: i, j
! begin
      call PRG_manager ('enter','keptfirst', 'UTILITY')
! we'll group the kept points into pieces first
!        write(UNIout,*) 'Entered keptfirst'
!        call flush(6)
        do i=1,pntskept-1
          xi=keep(i,1)
          yi=keep(i,2)
          zi=keep(i,3)
         do j=i+1,pntskept
          xj=keep(j,1)
          yj=keep(j,2)
          zj=keep(j,3)
! as long as any two points of the mesh are less than "search" 
! mesh point units apart we give them the same index implying they belong
! to the same surface piece
          if(keptpiece(i).ne.keptpiece(j)) then
            amiclose=dsqrt((dble(xi)-dble(xj))**2+(dble(yi)-dble(yj))**2+(dble(zi)-dble(zj))**2)
             if(amiclose.lt.search) then
! point j belongs to the same piece as point i so we assign them both to the
! lowest piece index of points i and j
               keptpiece(j)=min0(keptpiece(i),keptpiece(j))
               keptpiece(i)=keptpiece(j)
! if j is not i+1 (the stored point immediately after point i) then the 
! assignment of index values to the intervening points has messed up our
! indexing.  we need to switch points around to get the indexing back on track
! we use temporary index pntskept+1 for this
                if(j.ne.i+1) then
! switch point j to the pntskept+1 index (end of the list)
!                  write(UNIout,*) 'Call switchkept 1', j,pntskept+1
!                  call flush(6)
                 call switchkept(j,pntskept+1)
! all the points between point i+1 and j-1 are moved to the next higher index
! this is all done in reverse order (point j-1 to point i+1) 
!                   write(UNIout,*) 'Call switchkept 2', j-1,i+1
!                   call flush(6)
                   do k=j-1,i+1,-1
                    call switchkept(k,k+1)
                   end do
! the temporary index point now becomes point i+1
!                  write(UNIout,*) 'Call switchkept 3', pntstrunc+1,i+1
!                  call flush(6)
                  call switchkept(pntskept+1,i+1)
                end if
             end if
          end if
         end do
        end do
!       write(UNIout,*) 'Exited keptfirst'
!       call flush(6)
      call PRG_manager ('exit','keptfirst', 'UTILITY')
      return
      end subroutine keptfirst

      subroutine countpieces
! modules
      USE SGM_defaults
      USE program_files
! local scalar
      integer :: i
! begin
      call PRG_manager ('enter','countpieces', 'UTILITY')
! now to count the pieces
!       write(UNIout,*) 'Entered countpieces'
!       call flush(6)
       diffpiece=1
       samepiece=1
       do i=2,pntskept
! adjacent points in the keptpiece list have the same index
! they must belong to the same piece
         if(keptpiece(i).eq.keptpiece(i-1)) then
           samepiece=samepiece+1
! otherwise they are in different pieces
         else 
           pntsinpiece(diffpiece)=samepiece
           diffpiece=diffpiece+1
           samepiece=1
         end if
       end do
       pntsinpiece(diffpiece)=samepiece
!       write(UNIout,*) 'The number of isosurface pieces is ', diffpiece 
!       write(UNIout,*) 'Locally convex points ',pntstrunc, 'Other points ',pntskept
!       call flush(6)
!       write(UNIout,*) 'Exited countpieces'
!       call flush(6)
      call PRG_manager ('exit','countpieces', 'UTILITY')
      return
      end subroutine countpieces



      subroutine truncfirst
! modules
      USE SGM_defaults
      USE program_files
! local scalar
      integer :: i, j, xi, yi, zi, xj, yj, zj, k
      double precision :: amiclose
! Begin
      call PRG_manager ('enter','truncfirst', 'UTILITY')
!       write(UNIout,*) 'Entered truncfirst'
!       write(UNIout,*) 'Pntstrunc: ',pntstrunc
!       call flush(6)
       if(pntstrunc.ne.1) then
         do i=1,pntstrunc-1
           xi=truncate(i,1)
           yi=truncate(i,2)
           zi=truncate(i,3)
          do j=i+1,pntstrunc
           xj=truncate(j,1)
           yj=truncate(j,2)
           zj=truncate(j,3)
! now we clump the points we kept into distinct truncation holes
! as long as any two points of the mesh are less than "search" 
! mesh point units apart we give them the same index implying they belong
! to the same hole
           if(truncpiece(i).ne.truncpiece(j)) then
             amiclose=dsqrt((dble(xi)-dble(xj))**2+(dble(yi)-dble(yj))**2+(dble(zi)-dble(zj))**2)
              if(amiclose.lt.search) then
! point j belongs to the same piece as point i so we assign them both to the
! lowest piece index of points i and j
                truncpiece(j)=min0(truncpiece(i),truncpiece(j))
                truncpiece(i)=truncpiece(j)
! if j is not i+1 (the stored point immediately after point i) then the 
! assignment of index values to the intervening points has messed up our
! indexing.  we need to switch points around to get the indexing back on track
! we use temporary index pntstrunc+1 for this
                 if(j.ne.i+1) then
! switch point j to the pntstrunc+1 index (end of the list)
!                  write(UNIout,*) 'Call switchtrunc 1', j,pntstrunc+1
!                  call flush(6)
                  call switchtrunc(j,pntstrunc+1)
! all the points between point i+1 and j-1 are moved to the next higher index
! this is all done in reverse order (point j-1 to point i+1) 
!                    write(UNIout,*) 'Call switchtrunc 2', j-1,i+1
!                    call flush(6)
                    do k=j-1,i+1,-1
                     call switchtrunc(k,k+1)
                    end do
! the temporary index point now becomes point i+1
!                   write(UNIout,*) 'Call switchtrunc 3', pntstrunc+1,i+1
!                   call flush(6)
                   call switchtrunc(pntstrunc+1,i+1)
                 end if
              end if
           end if
          end do
         end do
       else
         truncpiece(1)=1
       end if
!       write(UNIout,*) 'Exited truncfirst'
!       call flush(6)
      call PRG_manager ('exit','truncfirst', 'UTILITY')
      return
      end subroutine truncfirst


      subroutine countholes
! modules
      USE SGM_defaults
      USE program_files
! local scalar
      integer :: i
! Begin
      call PRG_manager ('enter','countholes', 'UTILITY')
!       write(UNIout,*) 'Entered countholes'
!       write(UNIout,*) 'Pntstrunc ',pntstrunc
!       call flush(6)
       diffhole=1
       samehole=1
       do i=2,pntstrunc
! adjacent points in the truncpiece list have the same index
! they must belong to the same hole
         if(truncpiece(i).eq.truncpiece(i-1)) then
           samehole=samehole+1
! otherwise they are in different holes
         else 
           pntsinhole(diffhole)=samehole
           diffhole=diffhole+1
           samehole=1
         end if
       end do
       pntsinhole(diffhole)=samehole
!       write(UNIout,*) 'The number of holes is ', diffhole 
!       write(UNIout,*) 'Locally convex points ',pntstrunc, 'Other points ',pntskept
!       call flush(6)
!       do i=1,diffhole
!          write(UNIout,*) i,pntsinhole(i)
!          call flush(6)
!       end do


! if we have one or more isosurface pieces but no truncated points then
! each isosurface piece should contain one hole
       jcount=diffhole
!       write(UNIout,*) 'ml ',jcount,pntskept,pntstrunc
       do i=1,diffhole
! if a "hole" contains a number of points less than 5% of all original truncated points then we consider
! it to not be a true hole.  
        if(pntsinhole(i).lt.0.05*tptemp) jcount=jcount-1
       end do
!      write(UNIout,*) 'Exited countholes'
!      write(UNIout,*) 'holes: ',diffhole,' true holes ',jcount
!      call flush(6)
      call PRG_manager ('exit','countholes', 'UTILITY')
      return
      end subroutine countholes



      subroutine switchkept(iswitch,jswitch)
! modules
      USE SGM_defaults
      USE program_files
! local scalar
      integer :: tempx, tempy, tempz, temppiece
! begin
      call PRG_manager ('enter','switchkept', 'UTILITY')
!      write(UNIout,*) 'Entered switchkept'
!      call flush(6)
      if(pntskept.ne.1) then
       tempx=keep(iswitch,1)
       tempy=keep(iswitch,2)
       tempz=keep(iswitch,3)
       temppiece=keptpiece(iswitch)
       keep(iswitch,1)=keep(jswitch,1)
       keep(iswitch,2)=keep(jswitch,2)
       keep(iswitch,3)=keep(jswitch,3)
       keptpiece(jswitch)=temppiece
       keep(jswitch,1)=tempx
       keep(jswitch,2)=tempy
       keep(jswitch,3)=tempz
      end if
!      write(UNIout,*) 'Exited switchkept'
!      call flush(6)
      call PRG_manager ('exit','switchkept', 'UTILITY')
      return
      end subroutine switchkept



      subroutine switchtrunc(iswitch,jswitch)
! modules
      USE SGM_defaults
      USE program_files
! local scalar
      integer :: tempx, tempy, tempz, temppiece
! begin
      call PRG_manager ('enter','switchtrunc', 'UTILITY')
!      write(UNIout,*) 'Entered switchtrunc'
!      call flush(6)
      if(pntstrunc.ne.1) then
       tempx=truncate(iswitch,1)
       tempy=truncate(iswitch,2)
       tempz=truncate(iswitch,3)
       temppiece=truncpiece(iswitch)
       truncate(iswitch,1)=truncate(jswitch,1)
       truncate(iswitch,2)=truncate(jswitch,2)
       truncate(iswitch,3)=truncate(jswitch,3)
       truncpiece(jswitch)=temppiece
       truncate(jswitch,1)=tempx
       truncate(jswitch,2)=tempy
       truncate(jswitch,3)=tempz
      end if
!      write(UNIout,*) 'Exited switchtrunc'
!      call flush(6)
      call PRG_manager ('exit','switchtrunc', 'UTILITY')
      return
      end subroutine switchtrunc



      subroutine bordertrunc
! modules
      USE SGM_defaults
      USE program_files
! local scalar
      integer :: xi, yi, zi, xj, yj, zj
      double precision :: amiclose
! begin
      call PRG_manager ('enter','bordertrunc', 'UTILITY')
! if we have a large number of truncated points (>1500), we don't need to keep track of
! all of them.  we only need to keep track of those that border on isosurface pieces
! if not, we'll keep track of them all
!       write(UNIout,*) 'Entered bordertrunc'
!       call flush(6)
        if(pntstrunc.gt.1500) then
!         write(UNIout,*) 'Finding border points'
         call flush(6)
         icount=0
         outer: do i=1,pntstrunc
           xi=truncate(i,1)
           yi=truncate(i,2)
           zi=truncate(i,3)
           inner: do j=1,pntskept
             xj=keep(j,1)
             yj=keep(j,2)
             zj=keep(j,3)
             amiclose=dsqrt((dble(xi)-dble(xj))**2+(dble(yi)-dble(yj))**2+(dble(zi)-dble(zj))**2)
             if(amiclose.lt.search) then
               icount=icount+1
               truncate(icount,1)=xi
               truncate(icount,2)=yi
               truncate(icount,3)=zi
               truncpiece(icount)=icount
               cycle outer
             end if
           end do inner
         end do outer
         pntstrunc=icount
        else
!         write(UNIout,*) 'Not enough truncated points to risk finding borders'
!         call flush(6)
         do i=1,pntstrunc
          truncate(i,1)=truncate(i,1)
          truncate(i,2)=truncate(i,2)
          truncate(i,3)=truncate(i,3)
         end do
        end if
!        write(UNIout,*) 'Exited bordertrunc'
!        call flush(6)
        call PRG_manager ('exit','bordertrunc', 'UTILITY')
        return
        end subroutine bordertrunc 




