      subroutine MENU_shapegroup
!***********************************************************************
!     Date last modified: October 3, 2014                  Version 1.0 *
!     Author: P.L. Warburton                                           *
!     Description: Create or compare shape group maps                  *
!***********************************************************************
! Modules:
      USE program_parser
      USE program_constants
      USE MENU_gets
      USE QM_defaults
      USE SGM_defaults

      implicit none
!
! Local scalars:
      integer :: length
      logical done

!
! Begin:
      call PRG_manager ('enter', 'MENU_shapegroup', 'UTILITY')
! Defaults:
      done=.false.
      doall=.false.
      res=0.0d0
      multiplier=1.0d0
      Fragment='FULL'
      madeprimes=.false.
      meshmade=.false.
      meshallocated=.false.
      domainsallocated=.false.
      mesherror=.false.
      boxmarginset=.false.

! abcodes cannot be negative.  Here we initialize them to -1 to keep track of any issues
      abcode(:,:)=-1
!

! Menu:
      do while (.not.done)
      call new_token (' SHapegroup:')
!
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      ' Command SHapegroup:', &
      '   Purpose: Overide defaults for shape group map object generation', &
      '   Syntax :', &
      '   MOlecule = <string>, Full molecule or fragment, ', &
      '   MOlecule = FULL, [default]', &
      '   MOlecule = FRagment, Will require fragment menu with partitioning ', &
      '   REsolution = <real>, Override the default mesh resolution', &


      '   end'
!
! MOlecule
      else if(token(1:2).EQ.'MO')then
         call GET_value (Fragment,length)
!
! REsolution
      else if(token(1:2).EQ.'RE')then
         call GET_value (res)
    
      else
         call MENU_end (done)
      end if
!
      end do !(.not.done)


!
! End of routine MENU_shapegroup
      call PRG_manager ('exit', 'MENU_shapegroup', 'UTILITY')
      return
      end
