      subroutine BLD_SGM_objects
!****************************************************************************************************************
!       Date last modified Sept 4, 2014                                                                         *
!       Author: P.L. Warburton                                                                                  *
!       Description:  Builds a complete list of all the objects which can be created in MUNgauss                *
!       When adding a class or object within a class ensure it is done in alphabetical order                    *
!****************************************************************************************************************
! Modules:
      USE program_objects

      implicit none

! Local scalar:
      integer :: Iobject
!
! Begin:
      OBJ_SGM(1:Max_objects)%modality = 'other'
      OBJ_SGM(1:Max_objects)%class = 'SGM'
      OBJ_SGM(1:Max_objects)%depend = .true.
      NSGMobjects = 0
!
! Class Shape Group Method (SGM)
      NSGMobjects = NSGMobjects + 1
      OBJ_SGM(NSGMobjects)%name = 'MAP'
      OBJ_SGM(NSGMobjects)%modality = 'DENSITY'
      OBJ_SGM(NSGMobjects)%routine = 'SGM_MAP_DENSITY'
!
      NSGMobjects = NSGMobjects + 1
      OBJ_SGM(NSGMobjects)%name = 'MAP'
      OBJ_SGM(NSGMobjects)%modality = 'NUCPO'
      OBJ_SGM(NSGMobjects)%routine = 'SGM_MAP_NUCLEAR_POTENTIAL'
!
      NSGMobjects = NSGMobjects + 1
      OBJ_SGM(NSGMobjects)%name = 'MAP'
      OBJ_SGM(NSGMobjects)%modality = 'ESP'
      OBJ_SGM(NSGMobjects)%routine = 'SGM_MAP_ELECTROSTATIC_POTENTIAL'
!
      NSGMobjects = NSGMobjects + 1
      OBJ_SGM(NSGMobjects)%name = 'MAP'
      OBJ_SGM(NSGMobjects)%modality = 'RADIAL'
      OBJ_SGM(NSGMobjects)%routine = 'SGM_MAP_RADIAL_DENSITY'
!
      NSGMobjects = NSGMobjects + 1
      OBJ_SGM(NSGMobjects)%name = 'MAP'
      OBJ_SGM(NSGMobjects)%modality = 'ALL'
      OBJ_SGM(NSGMobjects)%routine = 'SGM_MAP_ALL'
!
      NSGMobjects = NSGMobjects + 1
      OBJ_SGM(NSGMobjects)%name = 'COMPARE'
      OBJ_SGM(NSGMobjects)%modality = 'DENSITY'
      OBJ_SGM(NSGMobjects)%routine = 'SGM_COMPARE_GENERAL'
!
      NSGMobjects = NSGMobjects + 1
      OBJ_SGM(NSGMobjects)%name = 'COMPARE'
      OBJ_SGM(NSGMobjects)%modality = 'NUCPO'
      OBJ_SGM(NSGMobjects)%routine = 'SGM_COMPARE_GENERAL'
!
      NSGMobjects = NSGMobjects + 1
      OBJ_SGM(NSGMobjects)%name = 'COMPARE'
      OBJ_SGM(NSGMobjects)%modality = 'ESP'
      OBJ_SGM(NSGMobjects)%routine = 'SGM_COMPARE_GENERAL'
!
      NSGMobjects = NSGMobjects + 1
      OBJ_SGM(NSGMobjects)%name = 'COMPARE'
      OBJ_SGM(NSGMobjects)%modality = 'RADIAL'
      OBJ_SGM(NSGMobjects)%routine = 'SGM_COMPARE_GENERAL'
!
      NSGMobjects = NSGMobjects + 1
      OBJ_SGM(NSGMobjects)%name = 'COMPARE'
      OBJ_SGM(NSGMobjects)%modality = 'ALL'
      OBJ_SGM(NSGMobjects)%routine = 'SGM_COMPARE_ALL'! Dummy Class

! Dummy Class
      NSGMobjects = NSGMobjects + 1
      OBJ_SGM(NSGMobjects)%name = '?'
      OBJ_SGM(NSGMobjects)%modality = '?'
      OBJ_SGM(NSGMobjects)%routine = '?'
!
      do Iobject=1,NSGMobjects
        OBJ_SGM(Iobject)%exist=.false.
        OBJ_SGM(Iobject)%Current=.false.
      end do

      return
      end subroutine BLD_SGM_objects
