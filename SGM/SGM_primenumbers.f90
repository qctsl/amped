      subroutine SGM_primes
!*****************************************************************************************************************
!     Date last modified: September 30, 2014                                                        Version 1.0  *
!     Author: P.L. Warburton                                                                                     *
!     Description: Generates the prime numbers up to one million for shape group encoding                        *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE SGM_defaults

      implicit none
!
! Local scalars:
      integer :: loop1, loop2, numprimes, theprod
! Local array
      integer, allocatable, dimension(:) :: allnumbers

! Begin:
      call PRG_manager ('enter', 'SGM_PRIMES', 'UTILITY')
! there are 78498 prime numbers from two to one million
! if you encounter errors in shape code generation (segmentation or core dump)
! chances are you may need more prime numbers!
      allocate (allnumbers(1000000))
      allocate (primes(78498))
      allnumbers(:)=0
      allnumbers(1)=1
      primes(:)=0
      numprimes=0
 
      do loop1=2,1000000
       if (allnumbers(loop1).eq.0) then
        do loop2=1,500000
         theprod=loop1*loop2
         if ((theprod).gt.1000000) exit
         allnumbers(theprod)=1 
        end do     
         numprimes=numprimes+1
         primes(numprimes)=loop1
!         write(UNIout,*) 'Prime ',numprimes,' is ',primes(numprimes)
       end if
      end do
      deallocate(allnumbers)
! madeprimes logical is defined in SGM_defaults
      madeprimes=.true.
!      write(UNIout,'(a38,a3,a20)') 'Prime numbers have been generated for ',proptype,' SGM map generation.'

      call PRG_manager ('enter', 'SGM_PRIMES', 'UTILITY')
      return

      end subroutine SGM_primes
