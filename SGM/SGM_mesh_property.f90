      subroutine SGM_mesh_density
!*****************************************************************************************************************
!     Date last modified: October 3, 2014                                                           Version 1.0  *
!     Author: P.L. Warburton                                                                                     *
!     Description: Generates the density and second derivative eigenvalues on a mesh for the SGM                 *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE SGM_defaults
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE type_density

      implicit none
!
! Local scalars:
      integer :: x, y, z
      double precision :: xco,yco,zco,delx,dely,delsq,delz,maxden,minden,maxev,minev 
      double precision :: del2xx,del2yy,del2zz,del2xy,del2xz,del2yz
!
! Local Array	  
      double precision, dimension(3,3) :: Hess, EigVector
      double precision, dimension(3) :: EigValue
      double precision :: TraceAB
      double precision, dimension(:), allocatable :: AOprod
      double precision, dimension(:), allocatable  :: dAOprodX,dAOprodY,dAOprodZ
!
! Begin:
      call PRG_manager ('enter', 'SGM_MESH_DENSITY', 'UTILITY')
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)
      if(.not.allocated(AOprod)) allocate (AOprod(1:MATlen))
      allocate (dAOprodx(1:MATlen))
      allocate (dAOprody(1:MATlen))
      allocate (dAOprodz(1:MATlen))
!     allocate (d2AOprodxx(1:MATlen))
!     allocate (d2AOprodyy(1:MATlen))
!     allocate (d2AOprodzz(1:MATlen))
!     allocate (d2AOprodxy(1:MATlen))
!     allocate (d2AOprodxz(1:MATlen))
!     allocate (d2AOprodyz(1:MATlen))  
!      write(UNIout,'(a)') 'Density arrays allocated'
      maxden=-1.0d12
      minden=1.0d12
      maxev=-1.0d12
      minev=1.0d12

! lowest box corner of generated mesh is mapped to local scalars
      xco=xlo-res
      yco=ylo-res
      zco=zlo-res
! calculate property and second derivative over mesh points
!      write(Uniout,*) 'Xcoord   Ycoord    Zcoord    Density'
      write(UNIout,'(a12,f6.3,a14,3i4)') 'Resolution: ',res,' Mesh points: ',xpoints,ypoints,zpoints
      do x=1,xpoints
       xco=xco+res
       yco=ylo-res
       do y=1,ypoints
        yco=yco+res
        zco=zlo-res
        do z=1,zpoints
         zco=zco+res   
!         write(UNIout,*) 'Point ',x,y,z  
!         write(UNIout,*) 'Coords ',xco,yco,zco

         call AO_products (xco, yco, zco, AOprod, MATlen)
!         write(UNIout,*) 'Called AOprods ',x,y,z
         property(x,y,z)= TraceAB (PM0, AOprod, NBasis)
         maxden=dmax1(maxden,property(x,y,z))
         minden=dmin1(minden,property(x,y,z))
!         if(x.eq.1.or.y.eq.1.or.z.eq.1.or.x.eq.xpoints.or.y.eq.ypoints.or.z.eq.zpoints) then
!          write(UNIout,*) 'Density at ', x,y,z,' is ',property(x,y,z)
!         end if

!         write(UNIout,*) 'density ',xco,yco,zco

         call dAO_products(xco,yco,zco, dAOprodx, dAOprody, dAOprodz, MATlen)
         delx=TraceAB (PM0, dAOprodx, NBasis)
         dely=TraceAB (PM0, dAOprody, NBasis)
         delz=TraceAB (PM0, dAOprodz, NBasis)
         call GET_d2rho_Atom (Xco, Yco, Zco, Hess)
         call MATRIX_diagonalize (Hess, EigVector, EigValue, 2, .true.)
!        call d2AO_products (xco,yco,zco)
!        del2xx=TraceAB (PM0, d2AOprodxx, NBasis)
!        del2yy=TraceAB (PM0, d2AOprodyy, NBasis)
!        del2zz=TraceAB (PM0, d2AOprodzz, NBasis)
!        del2xy=TraceAB (PM0, d2AOprodxy, NBasis)
!        del2xz=TraceAB (PM0, d2AOprodxz, NBasis)
!        del2yz=TraceAB (PM0, d2AOprodyz, NBasis)
!         write(UNIout,*) 'Called d2AOprods ',x,y,z
! the original implementation of the shape group method only requires us 
! to store the second eigenvalue of the curvature matrix
!        Hess(1,1)=del2xx
!        Hess(1,2)=del2xy
!        Hess(1,3)=del2xz
!        Hess(2,1)=Hess(1,2)
!        Hess(2,2)=del2yy
!        Hess(2,3)=del2yz
!        Hess(3,1)=Hess(1,3)
!        Hess(3,2)=Hess(2,3)
!        Hess(3,3)=del2zz
!        call MATRIX_diagonalize (Hess, EigVector, EigValue, 2, .true.)
         secondev(x,y,z)=EigValue(2)
         maxev=dmax1(maxev,secondev(x,y,z))
         minev=dmin1(minev,secondev(x,y,z))
!         write(UNIout,'(a16,f12.9)') 'MG eigenvalue:  ',secondev(x,y,z)

        end do
       end do
      end do
!      write(*,*)'Maxden: ',maxden,' Minden: ',minden
!      write(*,*)'Maxev: ',maxev,' Minev: ',minev
      deallocate (AOprod)
      deallocate (dAOprodx)
      deallocate (dAOprody)
      deallocate (dAOprodz)
!     deallocate (d2AOprodxx)
!     deallocate (d2AOprodyy)
!     deallocate (d2AOprodzz)
!     deallocate (d2AOprodxy)
!     deallocate (d2AOprodxz)
!     deallocate (d2AOprodyz)   

      call PRG_manager ('exit', 'SGM_MESH_DENSITY', 'UTILITY')
      return
      end subroutine SGM_mesh_density

      subroutine SGM_checkmesh_density
!*****************************************************************************************************************
!     Date last modified: October 3, 2014                                                           Version 1.0  *
!     Author: P.L. Warburton                                                                                     *
!     Description: Checks the mesh to see if the lowest property isocontour desired fits completely inside       *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE SGM_defaults
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE type_density

      implicit none
!
! Local scalars:
      integer :: x, y, z
      double precision :: xco,yco,zco, tempprop
      double precision :: TraceAB
      double precision, dimension(:), allocatable :: AOprod
! Begin:
      call PRG_manager ('enter', 'SGM_CHECKMESH_DENSITY', 'UTILITY')
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)

      mesherror=.false.
      meshmade=.true.
      if(.not.allocated(AOprod)) allocate (AOprod(1:MATlen))

      xco=xlo
      do y=1,ypoints
       if(mesherror) exit
        yco=ylo+res*dble(y-1)
       do z=1,zpoints
        if(mesherror) exit
        zco=zlo+res*dble(z-1)
         call AO_products (xco, yco, zco, AOprod, MATlen)
         tempprop=TraceAB (PM0, AOprod, NBasis)
         if(tempprop.gt.(dexp(dlog(10.0d0)*boundlo))) then
!          write(UNIout,*) 'mesh error 1', y,z,' is ',tempprop
          mesherror=.true.
          meshmade=.false.
          exit
         end if
       end do
      end do
      xco=xlo+res*dble(xpoints-1)
      do y=1,ypoints
       if(mesherror) exit
        yco=ylo+res*dble(y-1)
       do z=1,zpoints
        if(mesherror) exit
        zco=zlo+res*dble(z-1)
         call AO_products (xco, yco, zco, AOprod, MATlen)
         tempprop= TraceAB (PM0, AOprod, NBasis)
         if(tempprop.gt.(dexp(dlog(10.0d0)*boundlo))) then
!          write(UNIout,*) 'mesh error ', xpoints,y,z,' is ',tempprop
          mesherror=.true.
          meshmade=.false.
          exit
         end if
       end do
      end do

      yco=ylo
      do x=1,xpoints
       if(mesherror) exit
        xco=xlo+res*dble(x-1)
       do z=1,zpoints
        if(mesherror) exit
        zco=zlo+res*dble(z-1)
         call AO_products (xco, yco, zco, AOprod, MATlen)
         tempprop= TraceAB (PM0, AOprod, NBasis)
         if(tempprop.gt.(dexp(dlog(10.0d0)*boundlo))) then
!          write(UNIout,*) 'mesh error ', x,' 1 ',z,' is ',tempprop
          mesherror=.true.
          meshmade=.false.
          exit
         end if
       end do
      end do
      yco=ylo+res*dble(ypoints-1)
      do x=1,xpoints
       if(mesherror) exit
        xco=xlo+res*dble(x-1)
       do z=1,zpoints
        if(mesherror) exit
        zco=zlo+res*dble(z-1)
         call AO_products (xco, yco, zco, AOprod, MATlen)
         tempprop= TraceAB (PM0, AOprod, NBasis)
         if(tempprop.gt.(dexp(dlog(10.0d0)*boundlo))) then
!          write(UNIout,*) 'mesh error ', x,ypoints,z,' is ',tempprop
          mesherror=.true.
          meshmade=.false.
          exit
         end if
       end do
      end do

      zco=zlo
      do x=1,xpoints
       if(mesherror) exit
        xco=xlo+res*dble(x-1)
       do y=1,ypoints
        if(mesherror) exit
        yco=ylo+res*dble(y-1)
         call AO_products (xco, yco, zco, AOprod, MATlen)
         tempprop= TraceAB (PM0, AOprod, NBasis)
         if(tempprop.gt.(dexp(dlog(10.0d0)*boundlo))) then
!          write(UNIout,*) 'mesh error ', x,y,' 1 is ',tempprop
          mesherror=.true.
          meshmade=.false.
          exit
         end if
       end do
      end do
      zco=zlo+res*dble(zpoints-1)
      do x=1,xpoints
       if(mesherror) exit
        xco=xlo+res*dble(x-1)
       do y=1,ypoints
        if(mesherror) exit
        yco=ylo+res*dble(y-1)
         call AO_products (xco, yco, zco, AOprod, MATlen)
         tempprop= TraceAB (PM0, AOprod, NBasis)
         if(tempprop.gt.(dexp(dlog(10.0d0)*boundlo))) then
!          write(UNIout,*) 'mesh error ', x,y,zpoints,' is ',tempprop
          mesherror=.true.
          meshmade=.false.
          exit
         end if
       end do
      end do

      if(mesherror) boxmargin=boxmargin+res
      deallocate (AOprod)

      call PRG_manager ('exit', 'SGM_CHECKMESH_DENSITY', 'UTILITY')
      return
      end subroutine SGM_checkmesh_density

     subroutine SGM_mesh_nucpo
!*****************************************************************************************************************
!     Date last modified: January 15, 2015                                                          Version 1.0  *
!     Author: P.L. Warburton                                                                                     *
!     Description: Generates the charge compressed nuclear potential and second derivative eigenvalues           *
!                  on a mesh for the SGM                                                                         *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE SGM_defaults
      USE type_molecule

      implicit none
!
! Local scalars:
      integer :: x, y, z, Iatom
      double precision :: xco,yco,zco,potsum,distance,xdist,ydist,zdist
      double precision :: del2xx,del2yy,del2zz,del2xy,del2xz,del2yz,maxnuc,minnuc,maxev,minev
!
! Local Array	  
      double precision, dimension(3,3) :: Hess, EigVector
      double precision, dimension(3) :: EigValue
! Begin:

! this needs much more work
!      write(UNIout,'(a)') 'NUCPO SGM not yet implemented'
!      return
! remove above section for programming purposes  

      call PRG_manager ('enter', 'SGM_MESH_NUCPO', 'UTILITY')

! Nuclear potentials of molecules are generally dominated by the nuclear charge of the heaviest atoms as
! the nuclear potential at a point (in atomic units) is given by the summation of Z/r.  To counter the
! "drowning out" of smaller atoms in determining the isocontours for shape group maps, the nuclear potental
! shape group method is actually based on "compressed nuclear charges".  In this approach, the potential
! will be the summation of (Z**0.25)/r  
      maxnuc=-1.0d12
      minnuc=1.0d12
      maxev=-1.0d12
      minev=1.0d12
! lowest box corner of generated mesh is mapped to local scalars
      xco=xlo-res
      yco=ylo-res
      zco=zlo-res
      potsum=0.0d0
! calculate property and second derivative over mesh points
!      write(Uniout,*) 'Xcoord   Ycoord    Zcoord    Density'
      write(UNIout,'(a12,f6.3,a14,3i4)') 'Resolution: ',res,' Mesh points: ',xpoints,ypoints,zpoints
      do x=1,xpoints
       xco=xco+res
       yco=ylo-res
       do y=1,ypoints
        yco=yco+res
        zco=zlo-res
        do z=1,zpoints
         zco=zco+res   
         write(UNIout,*) 'Point ',x,y,z  
         write(UNIout,*) 'Coords ',xco,yco,zco
         potsum=0.0d0
         del2xx=0.0d0
         del2xy=0.0d0
         del2xz=0.0d0
         del2yy=0.0d0
         del2yz=0.0d0
         del2zz=0.0d0
          do Iatom=1,Natoms
           if(CARTESIAN(Iatom)%Atomic_number.le.0) cycle
           xdist=(xco-CARTESIAN(Iatom)%x)**2
           ydist=(yco-CARTESIAN(Iatom)%y)**2
           zdist=(zco-CARTESIAN(Iatom)%z)**2
           distance=dsqrt(xdist+ydist+zdist)
           if(xdist.lt.1.0D-12) xdist=1.0D-12
           if(ydist.lt.1.0D-12) ydist=1.0D-12
           if(zdist.lt.1.0D-12) zdist=1.0D-12
           if(distance.lt.1.0D-12) distance=1.0D-12
           write(*,*) Iatom,xdist,ydist,zdist,distance
! nuclear potential will be calulated in atomic units
! elementary charge is one
! distances in atomic units
! 1/(4pi epsilon) is one
           potsum=potsum+(dble(CARTESIAN(Iatom)%Atomic_number))/distance
           write(*,*) 'ok',Iatom
           write(*,*) dble(CARTESIAN(Iatom)%Atomic_number),(dble(CARTESIAN(Iatom)%Atomic_number))/distance
           del2xx=del2xx+(dble(CARTESIAN(Iatom)%Atomic_number))*(3.0d0*xdist/distance**5-1.0D0/distance**3)
           del2yy=del2yy+(dble(CARTESIAN(Iatom)%Atomic_number))*(3.0d0*ydist/distance**5-1.0D0/distance**3)
           del2zz=del2zz+(dble(CARTESIAN(Iatom)%Atomic_number))*(3.0d0*zdist/distance**5-1.0D0/distance**3)
           del2xy=del2xy+(dble(CARTESIAN(Iatom)%Atomic_number))*(3.0d0*xdist*ydist/distance**5)
           del2xz=del2xz+(dble(CARTESIAN(Iatom)%Atomic_number))*(3.0d0*xdist*zdist/distance**5)
           del2yz=del2yz+(dble(CARTESIAN(Iatom)%Atomic_number))*(3.0d0*ydist*zdist/distance**5)
          end do
! since nuclear potential will have a significant range (especially for high atomic numbers)
! we will actually store the information in logarithmic form
          property(x,y,z)=potsum
         maxnuc=dmax1(maxnuc,property(x,y,z))
         minnuc=dmin1(minnuc,property(x,y,z))
          write(UNIout,*) 'Property: ',property(x,y,z)
! the original implementation of the shape group method only requires us 
! to store the second eigenvalue of the curvature matrix
         Hess(1,1)=del2xx
         Hess(1,2)=del2xy
         Hess(1,3)=del2xz
         Hess(2,1)=Hess(1,2)
         Hess(2,2)=del2yy
         Hess(2,3)=del2yz
         Hess(3,1)=Hess(1,3)
         Hess(3,2)=Hess(2,3)
         Hess(3,3)=del2zz
         call MATRIX_diagonalize (Hess, EigVector, EigValue, 3, 2, .true.)
         secondev(x,y,z)=EigValue(2)
         maxev=dmax1(maxev,secondev(x,y,z))
         minev=dmin1(minev,secondev(x,y,z))
         write(UNIout,'(a30,f12.9)') 'Nuclear potential eigenvalue: ',secondev(x,y,z)

        end do
       end do
      end do
      write(*,*)'Maxnuc: ',maxnuc,' Minnuc: ',minnuc
      write(*,*)'Maxev: ',maxev,' Minev: ',minev
      call PRG_manager ('exit', 'SGM_MESH_NUCPO', 'UTILITY')
      return
      end subroutine SGM_mesh_nucpo

      subroutine SGM_checkmesh_nucpo
!*****************************************************************************************************************
!     Date last modified: January 14, 2015                                                          Version 1.0  *
!     Author: P.L. Warburton                                                                                     *
!     Description: Checks the mesh to see if the lowest property isocontour desired fits completely inside       *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE SGM_defaults
      USE type_molecule

      implicit none
!
! Local scalars:
      integer :: x, y, z, Iatom
      double precision :: xco,yco,zco, tempprop, xdist, ydist,zdist,distance,potsum
! Begin:
      call PRG_manager ('enter', 'SGM_CHECKMESH_NUCPO', 'UTILITY')

      mesherror=.false.
      meshmade=.true.
      potsum=0.0d0
      tempprop=0.0d0
      xco=xlo
      do y=1,ypoints
       if(mesherror) exit
        yco=ylo+res*dble(y-1)
       do z=1,zpoints
        if(mesherror) exit
        zco=zlo+res*dble(z-1)
          do Iatom=1,Natoms
           if(CARTESIAN(Iatom)%Atomic_number.le.0) cycle
           xdist=(xco-CARTESIAN(Iatom)%x)**2
           ydist=(yco-CARTESIAN(Iatom)%y)**2
           zdist=(zco-CARTESIAN(Iatom)%z)**2
           distance=dsqrt(xdist+ydist+zdist)
           if(xdist.lt.1.0D-12) xdist=1.0D-12
           if(ydist.lt.1.0D-12) ydist=1.0D-12
           if(zdist.lt.1.0D-12) zdist=1.0D-12
           if(distance.lt.1.0D-12) distance=1.0D-12
! nuclear potential will be calulated in atomic units
! elementary charge is one
! distances in atomic units
! 1/(4pi epsilon) is one
           potsum=potsum+(dble(CARTESIAN(Iatom)%Atomic_number))/distance
          end do
          tempprop=potsum

         if(tempprop.gt.(dexp(dlog(10.0d0)*boundlo))) then
          write(UNIout,*) 'mesh error 1', y,z,' is ',tempprop
          mesherror=.true.
          meshmade=.false.
          exit
         end if
       end do
      end do

      potsum=0.0d0
      xco=xlo+res*dble(xpoints-1)
      do y=1,ypoints
       if(mesherror) exit
        yco=ylo+res*dble(y-1)
       do z=1,zpoints
        if(mesherror) exit
        zco=zlo+res*dble(z-1)
          do Iatom=1,Natoms
           if(CARTESIAN(Iatom)%Atomic_number.le.0) cycle
           xdist=(xco-CARTESIAN(Iatom)%x)**2
           ydist=(yco-CARTESIAN(Iatom)%y)**2
           zdist=(zco-CARTESIAN(Iatom)%z)**2
           distance=dsqrt(xdist+ydist+zdist)
           if(xdist.lt.1.0D-12) xdist=1.0D-12
           if(ydist.lt.1.0D-12) ydist=1.0D-12
           if(zdist.lt.1.0D-12) zdist=1.0D-12
           if(distance.lt.1.0D-12) distance=1.0D-12
! nuclear potential will be calulated in atomic units
! elementary charge is one
! distances in atomic units
! 1/(4pi epsilon) is one
           potsum=potsum+(dble(CARTESIAN(Iatom)%Atomic_number))/distance
          end do
          tempprop=potsum
         if(tempprop.gt.(dexp(dlog(10.0d0)*boundlo))) then
          write(UNIout,*) 'mesh error ', xpoints,y,z,' is ',tempprop
          mesherror=.true.
          meshmade=.false.
          exit
         end if
       end do
      end do

      potsum=0.0d0
      yco=ylo
      do x=1,xpoints
       if(mesherror) exit
        xco=xlo+res*dble(x-1)
       do z=1,zpoints
        if(mesherror) exit
        zco=zlo+res*dble(z-1)
          do Iatom=1,Natoms
           if(CARTESIAN(Iatom)%Atomic_number.le.0) cycle
           xdist=(xco-CARTESIAN(Iatom)%x)**2
           ydist=(yco-CARTESIAN(Iatom)%y)**2
           zdist=(zco-CARTESIAN(Iatom)%z)**2
           distance=dsqrt(xdist+ydist+zdist)
           if(xdist.lt.1.0D-12) xdist=1.0D-12
           if(ydist.lt.1.0D-12) ydist=1.0D-12
           if(zdist.lt.1.0D-12) zdist=1.0D-12
           if(distance.lt.1.0D-12) distance=1.0D-12
! nuclear potential will be calulated in atomic units
! elementary charge is one
! distances in atomic units
! 1/(4pi epsilon) is one
           potsum=potsum+(dble(CARTESIAN(Iatom)%Atomic_number))/distance
          end do
          tempprop=potsum
         if(tempprop.gt.(dexp(dlog(10.0d0)*boundlo))) then
          write(UNIout,*) 'mesh error ', x,' 1 ',z,' is ',tempprop
          mesherror=.true.
          meshmade=.false.
          exit
         end if
       end do
      end do

      potsum=0.0d0
      yco=ylo+res*dble(ypoints-1)
      do x=1,xpoints
       if(mesherror) exit
        xco=xlo+res*dble(x-1)
       do z=1,zpoints
      potsum=0.0d0
        if(mesherror) exit
        zco=zlo+res*dble(z-1)
          do Iatom=1,Natoms
           if(CARTESIAN(Iatom)%Atomic_number.le.0) cycle
           xdist=(xco-CARTESIAN(Iatom)%x)**2
           ydist=(yco-CARTESIAN(Iatom)%y)**2
           zdist=(zco-CARTESIAN(Iatom)%z)**2
           distance=dsqrt(xdist+ydist+zdist)
           if(xdist.lt.1.0D-12) xdist=1.0D-12
           if(ydist.lt.1.0D-12) ydist=1.0D-12
           if(zdist.lt.1.0D-12) zdist=1.0D-12
           if(distance.lt.1.0D-12) distance=1.0D-12
! nuclear potential will be calulated in atomic units
! elementary charge is one
! distances in atomic units
! 1/(4pi epsilon) is one
           potsum=potsum+(dble(CARTESIAN(Iatom)%Atomic_number))/distance
          end do
          tempprop=potsum
         if(tempprop.gt.(dexp(dlog(10.0d0)*boundlo))) then
          write(UNIout,*) 'mesh error ', x,ypoints,z,' is ',tempprop
          mesherror=.true.
          meshmade=.false.
          exit
         end if
       end do
      end do

      potsum=0.0d0
      zco=zlo
      do x=1,xpoints
       if(mesherror) exit
        xco=xlo+res*dble(x-1)
       do y=1,ypoints
        if(mesherror) exit
        yco=ylo+res*dble(y-1)
          do Iatom=1,Natoms
           if(CARTESIAN(Iatom)%Atomic_number.le.0) cycle
           xdist=(xco-CARTESIAN(Iatom)%x)**2
           ydist=(yco-CARTESIAN(Iatom)%y)**2
           zdist=(zco-CARTESIAN(Iatom)%z)**2
           distance=dsqrt(xdist+ydist+zdist)
           if(xdist.lt.1.0D-12) xdist=1.0D-12
           if(ydist.lt.1.0D-12) ydist=1.0D-12
           if(zdist.lt.1.0D-12) zdist=1.0D-12
           if(distance.lt.1.0D-12) distance=1.0D-12
! nuclear potential will be calulated in atomic units
! elementary charge is one
! distances in atomic units
! 1/(4pi epsilon) is one
           potsum=potsum+(dble(CARTESIAN(Iatom)%Atomic_number))/distance
          end do
          tempprop=potsum
         if(tempprop.gt.(dexp(dlog(10.0d0)*boundlo))) then
          write(UNIout,*) 'mesh error ', x,y,' 1 is ',tempprop
          mesherror=.true.
          meshmade=.false.
          exit
         end if
       end do
      end do

      potsum=0.0d0
      zco=zlo+res*dble(zpoints-1)
      do x=1,xpoints
       if(mesherror) exit
        xco=xlo+res*dble(x-1)
       do y=1,ypoints
        if(mesherror) exit
        yco=ylo+res*dble(y-1)
          do Iatom=1,Natoms
           if(CARTESIAN(Iatom)%Atomic_number.le.0) cycle
           xdist=(xco-CARTESIAN(Iatom)%x)**2
           ydist=(yco-CARTESIAN(Iatom)%y)**2
           zdist=(zco-CARTESIAN(Iatom)%z)**2
           distance=dsqrt(xdist+ydist+zdist)
           if(xdist.lt.1.0D-12) xdist=1.0D-12
           if(ydist.lt.1.0D-12) ydist=1.0D-12
           if(zdist.lt.1.0D-12) zdist=1.0D-12
           if(distance.lt.1.0D-12) distance=1.0D-12
! nuclear potential will be calulated in atomic units
! elementary charge is one
! distances in atomic units
! 1/(4pi epsilon) is one
           potsum=potsum+(dble(CARTESIAN(Iatom)%Atomic_number))/distance
          end do
          tempprop=potsum
         if(tempprop.gt.(dexp(dlog(10.0d0)*boundlo))) then
          write(UNIout,*) 'mesh error ', x,y,zpoints,' is ',tempprop
          mesherror=.true.
          meshmade=.false.
          exit
         end if
       end do
      end do
!      write(UNIout,*) mesherror
      flush(6)
      if(mesherror) boxmargin=boxmargin+res

      call PRG_manager ('exit', 'SGM_CHECKMESH_NUCPO', 'UTILITY')
      return
      end subroutine SGM_checkmesh_nucpo

      subroutine SGM_mesh_radial_density
!*****************************************************************************************************************
!     Date last modified: January 22, 2015                                                          Version 1.0  *
!     Author: P.L. Warburton                                                                                     *
!     Description: Generates the radial density and second derivative eigenvalues on a mesh for the SGM          *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE SGM_defaults
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE type_density
      USE type_Weights
      USE module_grid_points
      USE type_plotting
      USE program_constants

      implicit none
!
! Local scalars:
      integer :: x,y,z,Ipoint,Iatom
      double precision :: xco,yco,zco,delx,dely,delsq,delz,GET_RADIAL_DENSITY_XYZ
      double precision :: del2xx,del2yy,del2zz,del2xy,del2xz,del2yz,rho
      double precision :: WeightA,Xa,Ya,Za,XVal,YVal,ZVal,Znum
      double precision :: DEL_rhoX,DEL_rhoY,DEL_rhoZ
      double precision :: Del_rho,VAlueXX,VAlueYY,VAlueZZ,VAlueXY,VAlueXZ,VAlueYZ,RvalSq
      double precision :: DEL2_rho_XX,DEL2_rho_YY,DEL2_rho_ZZ,DEL2_rho_XY,DEL2_rho_XZ,DEL2_rho_YZ
      double precision :: BWeight,maxrad,minrad,maxev,minev      
      double precision, allocatable :: XYZ0(:),DEL_W(:),DEL2_W(:,:)
      logical :: bypoint
!
! Local Array	  
      double precision, dimension(3,3) :: Hess, EigVector
      double precision, dimension(3) :: EigValue
      double precision :: TraceAB
      double precision, dimension(:), allocatable :: AOprod
      double precision, dimension(:), allocatable  :: dAOprodX,dAOprodY,dAOprodZ
!
! Begin:
      call PRG_manager ('enter', 'SGM_MESH_RADIAL_DENSITY', 'UTILITY')
      bypoint=.false.
      if(LdW_analytical.and.Ld2W_analytical) bypoint=.true.
!      write(*,*) 'Bypoint: ',bypoint
      maxrad=-1.0d12
      minrad=1.0d12
      maxev=-1.0d12
      minev=1.0d12
      allocate (dAOprodx(1:MATlen))
      allocate (dAOprody(1:MATlen))
      allocate (dAOprodz(1:MATlen))
!     allocate (d2AOprodxx(1:MATlen))
!     allocate (d2AOprodyy(1:MATlen))
!     allocate (d2AOprodzz(1:MATlen))
!     allocate (d2AOprodxy(1:MATlen))
!     allocate (d2AOprodxz(1:MATlen))
!     allocate (d2AOprodyz(1:MATlen))  
      allocate (XYZ0(1:3))
      allocate (DEL_W(1:3))
      allocate (DEL2_W(1:3,1:3))
      XYZ0(:)=ZERO
      DEL_W(:)=ZERO
      DEL2_W(:,:)=ZERO
!      write(UNIout,'(a)') 'Density arrays allocated'
! lowest box corner of generated mesh is mapped to local scalars
      xco=xlo-res
      yco=ylo-res
      zco=zlo-res
! calculate property and second derivative over mesh points
!      write(Uniout,*) 'Xcoord   Ycoord    Zcoord    Density'
      write(UNIout,'(a12,f6.3,a14,3i4)') 'Resolution: ',res,' Mesh points: ',xpoints,ypoints,zpoints
      do x=1,xpoints
       xco=xco+res
       yco=ylo-res
       do y=1,ypoints
        yco=yco+res
        zco=zlo-res
        do z=1,zpoints
         zco=zco+res   
!         write(UNIout,*) 'Point ',x,y,z  
!         write(UNIout,*) 'Coords ',xco,yco,zco

         property(x,y,z)= GET_RADIAL_DENSITY_XYZ (xco,yco,zco)
         maxrad=dmax1(maxrad,property(x,y,z))
         minrad=dmin1(minrad,property(x,y,z))
!         if(x.eq.1.or.y.eq.1.or.z.eq.1.or.x.eq.xpoints.or.y.eq.ypoints.or.z.eq.zpoints) then
!          write(UNIout,*) 'Radial density at ', x,y,z,' is ',property(x,y,z)
!         end if
!          flush(6)

!      do Ipoint=1,NGridPoints
!        write(*,*) 'about to do stuff'
        XVal=xco
        YVal=yco
        ZVal=zco
        if(.not.allocated(AOprod)) allocate (AOprod(1:MATlen))
        call AO_products (Xco, Yco, Zco, AOprod, MATlen)
        rho= TraceAB (PM0, AOprod, NBasis)
!        write(*,*) 'rho: ',rho
        call dAO_products (Xco, Yco, Zco, dAOprodx, dAOprody, dAOprodz, MATlen)
        DEL_rhoX=TraceAB (PM0, dAOprodX, NBasis)
        DEL_rhoY=TraceAB (PM0, dAOprodY, NBasis)
        DEL_rhoZ=TraceAB (PM0, dAOprodZ, NBasis)
!        write(*,*) 'dels: ',DEL_rhoX, DEL_rhoY,DEL_rhoZ
        call GET_d2rho_Atom (Xco, Yco, Zco, Hess)
        DEL2_rho_XX=Hess(1,1)
        DEL2_rho_YY=Hess(2,2)
        DEL2_rho_ZZ=Hess(3,3)
        DEL2_rho_XY=Hess(1,2)
        DEL2_rho_XZ=Hess(1,3)
        DEL2_rho_YZ=Hess(2,3)
!       call d2AO_products (Xco, Yco, Zco)
!       DEL2_rho_XX=TraceAB (PM0, d2AOprodxx, NBasis)
!       DEL2_rho_YY=TraceAB (PM0, d2AOprodyy, NBasis)
!       DEL2_rho_ZZ=TraceAB (PM0, d2AOprodzz, NBasis)
!       DEL2_rho_XY=TraceAB (PM0, d2AOprodxy, NBasis)
!       DEL2_rho_XZ=TraceAB (PM0, d2AOprodxz, NBasis)
!       DEL2_rho_YZ=TraceAB (PM0, d2AOprodyz, NBasis)
!        write(*,*) 'del2s: ',DEL2_rho_XX, DEL2_rho_YY,DEL2_rho_ZZ, DEL2_rho_XY,DEL2_rho_XZ,DEL2_rho_YZ        
!        write(UNIout,'(/a,3f10.6)')'Grid point: ',Xco,Yco,Zco
        flush(6)
        XYZ0(1)=Xco
        XYZ0(2)=Yco
        XYZ0(3)=Zco

! Now compute the contribution to each atom:
       ValueXX=ZERO
       ValueYY=ZERO
       ValueZZ=ZERO
       ValueXY=ZERO
       ValueXZ=ZERO
       ValueYZ=ZERO
!new section
       if(bypoint) call BeckeWdels_allatoms (XVAL, YVAL, ZVAL)
!end new section
       do Iatom=1,Natoms 

         Znum=CARTESIAN(Iatom)%Atomic_number
         if(Znum.le.0)cycle
         Xa=CARTESIAN(Iatom)%X
         Ya=CARTESIAN(Iatom)%Y
         Za=CARTESIAN(Iatom)%Z          
         RvalSq=(XVal-Xa)**2+(YVal-Ya)**2+(ZVal-Za)**2
!         write(*,*) 'LWs: ',LdW_analytical,Ld2W_analytical
!         if(LdW_analytical.and.Ld2W_analytical)then
!           call BeckeWdels (XVAL, YVAL, ZVAL, Iatom, WeightA, DEL_W, DEL2_W)
!         else
         if(.not.bypoint) then
!           write(*,*) 'Evaluating numerically'
           call GET_weight1 (XVal, YVal, ZVal, Iatom, WeightA)
           call GET_dW_NUMERICAL (Ipoint, Iatom, XYZ0, DEL_W)
           call GET_d2W_NUMERICAL (Ipoint, Iatom, XYZ0, DEL2_W)
         else
! using weights stored directly from BeckeWdels_allatoms
          WeightA=BWA(Iatom)
          DEL_W(1)=dBWA(Iatom,1)
          DEL_W(2)=dBWA(Iatom,2)
          DEL_W(3)=dBWA(Iatom,3)
          DEL2_W(1,1)=d2BWA(Iatom,1,1)
          DEL2_W(1,2)=d2BWA(Iatom,1,2)
          DEL2_W(1,3)=d2BWA(Iatom,1,3)
          DEL2_W(2,1)=d2BWA(Iatom,2,1)
          DEL2_W(2,2)=d2BWA(Iatom,2,2)
          DEL2_W(2,3)=d2BWA(Iatom,2,3)
          DEL2_W(3,1)=d2BWA(Iatom,3,1)
          DEL2_W(3,2)=d2BWA(Iatom,3,2)
          DEL2_W(3,3)=d2BWA(Iatom,3,3)

         end if

         ValueXX=ValueXX+(TWO*WeightA+FOUR*(XVal-Xa)*DEL_W(1)+RvalSq*DEL2_W(1,1))*rho &
                        +(FOUR*(XVal-Xa)*WeightA+TWO*RvalSq*DEL_W(1))*DEL_rhoX+RvalSq*WeightA*DEL2_rho_XX
         ValueYY=ValueYY+(TWO*WeightA+FOUR*(YVal-Ya)*DEL_W(2)+RvalSq*DEL2_W(2,2))*rho &
                        +(FOUR*(YVal-Ya)*WeightA+TWO*RvalSq*DEL_W(2))*DEL_rhoY+RvalSq*WeightA*DEL2_rho_YY
         ValueZZ=ValueZZ+(TWO*WeightA+FOUR*(ZVal-Za)*DEL_W(3)+RvalSq*DEL2_W(3,3))*rho &
                        +(FOUR*(ZVal-Za)*WeightA+TWO*RvalSq*DEL_W(3))*DEL_rhoZ+RvalSq*WeightA*DEL2_rho_ZZ
         ValueXY=ValueXY+(TWO*(XVal-Xa)*DEL_W(2)+TWO*(YVal-Ya)*DEL_W(1)+RvalSq*DEL2_W(1,2))*rho &
                        +(TWO*(XVal-Xa)*WeightA+RvalSq*DEL_W(1))*DEL_rhoY &
                        +(TWO*(YVal-Ya)*WeightA+RvalSq*DEL_W(2))*DEL_rhoX+RvalSq*WeightA*DEL2_rho_XY
         ValueXZ=ValueXZ+(TWO*(XVal-Xa)*DEL_W(3)+TWO*(ZVal-Za)*DEL_W(1)+RvalSq*DEL2_W(1,3))*rho &
                        +(TWO*(XVal-Xa)*WeightA+RvalSq*DEL_W(1))*DEL_rhoZ &
                        +(TWO*(ZVal-Za)*WeightA+RvalSq*DEL_W(3))*DEL_rhoX+RvalSq*WeightA*DEL2_rho_XZ
         ValueYZ=ValueYZ+(TWO*(YVal-Ya)*DEL_W(3)+TWO*(ZVal-Za)*DEL_W(2)+RvalSq*DEL2_W(2,3))*rho &
                        +(TWO*(YVal-Ya)*WeightA+RvalSq*DEL_W(2))*DEL_rhoZ &
                        +(TWO*(ZVal-Za)*WeightA+RvalSq*DEL_W(3))*DEL_rhoY+RvalSq*WeightA*DEL2_rho_YZ
       end do !Iatom

! new section 
        if(bypoint) then
         deallocate(BWA)
         deallocate(dBWA)
         deallocate(d2BWA)
        end if

!       write(UNIout,'(a)')'DEL2_rad at grid point: d2XX,d2YY,d2ZZ,d2XY,d2XZ,d2YZ'
!       write(UNIout,Pfmt_d2Rad)ValueXX,ValueYY,ValueZZ,ValueXY,ValueXZ,ValueYZ
        Hess(1,1)=ValueXX
        Hess(1,2)=ValueXY
        Hess(1,3)=ValueXZ
        Hess(2,1)=Hess(1,2)
        Hess(2,2)=ValueYY
        Hess(2,3)=ValueYZ
        Hess(3,1)=Hess(1,3)
        Hess(3,2)=Hess(2,3)
        Hess(3,3)=ValueZZ

        call MATRIX_diagonalize (Hess, EigVector, EigValue, 3, 2, .true.)

         secondev(x,y,z)=EigValue(2)
         maxev=dmax1(maxev,secondev(x,y,z))
         minev=dmin1(minev,secondev(x,y,z))

!         write(UNIout,'(a16,f12.9)') 'MG eigenvalue:  ',secondev(x,y,z)
        end do
       end do
      end do

!      write(*,*)'Maxrad: ',maxrad,' Minrad: ',minrad
!      write(*,*)'Maxev: ',maxev,' Minev: ',minev
      deallocate (dAOprodx)
      deallocate (dAOprody)
      deallocate (dAOprodz)
!     deallocate (d2AOprodxx)
!     deallocate (d2AOprodyy)
!     deallocate (d2AOprodzz)
!     deallocate (d2AOprodxy)
!     deallocate (d2AOprodxz)
!     deallocate (d2AOprodyz) 
      deallocate (XYZ0)
      deallocate (DEL_W)
      deallocate (DEL2_W)  

      call PRG_manager ('exit', 'SGM_MESH_RADIAL_DENSITY', 'UTILITY')
      return
      end subroutine SGM_mesh_radial_density

      subroutine SGM_checkmesh_radial_density
!*****************************************************************************************************************
!     Date last modified: January 22, 2015                                                          Version 1.0  *
!     Author: P.L. Warburton                                                                                     *
!     Description: Checks the mesh to see if the lowest property isocontour desired fits completely inside       *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE SGM_defaults
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE type_density
      USE type_Weights
      USE module_grid_points
      USE type_plotting
      USE program_constants
      implicit none
!
! Local scalars:
      integer :: x, y, z
      double precision :: xco,yco,zco, tempprop,GET_RADIAL_DENSITY_XYZ
! Begin:
      call PRG_manager ('enter', 'SGM_CHECKMESH_RADIAL_DENSITY', 'UTILITY')
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)

      mesherror=.false.
      meshmade=.true.


      xco=xlo
      do y=1,ypoints
       if(mesherror) exit
        yco=ylo+res*dble(y-1)
       do z=1,zpoints
        if(mesherror) exit
        zco=zlo+res*dble(z-1)
         tempprop=GET_RADIAL_DENSITY_XYZ(xco,yco,zco)
         if(tempprop.gt.(dexp(dlog(10.0d0)*boundlo))) then
!          write(UNIout,*) 'mesh error 1', y,z,' is ',tempprop
          mesherror=.true.
          meshmade=.false.
          exit
         end if
       end do
      end do
      xco=xlo+res*dble(xpoints-1)
      do y=1,ypoints
       if(mesherror) exit
        yco=ylo+res*dble(y-1)
       do z=1,zpoints
        if(mesherror) exit
        zco=zlo+res*dble(z-1)
         tempprop=GET_RADIAL_DENSITY_XYZ(xco,yco,zco)
         if(tempprop.gt.(dexp(dlog(10.0d0)*boundlo))) then
!          write(UNIout,*) 'mesh error ', xpoints,y,z,' is ',tempprop
          mesherror=.true.
          meshmade=.false.
          exit
         end if
       end do
      end do

      yco=ylo
      do x=1,xpoints
       if(mesherror) exit
        xco=xlo+res*dble(x-1)
       do z=1,zpoints
        if(mesherror) exit
        zco=zlo+res*dble(z-1)
         tempprop=GET_RADIAL_DENSITY_XYZ(xco,yco,zco)
         if(tempprop.gt.(dexp(dlog(10.0d0)*boundlo))) then
!          write(UNIout,*) 'mesh error ', x,' 1 ',z,' is ',tempprop
          mesherror=.true.
          meshmade=.false.
          exit
         end if
       end do
      end do
      yco=ylo+res*dble(ypoints-1)
      do x=1,xpoints
       if(mesherror) exit
        xco=xlo+res*dble(x-1)
       do z=1,zpoints
        if(mesherror) exit
        zco=zlo+res*dble(z-1)
         tempprop=GET_RADIAL_DENSITY_XYZ(xco,yco,zco)
         if(tempprop.gt.(dexp(dlog(10.0d0)*boundlo))) then
!          write(UNIout,*) 'mesh error ', x,ypoints,z,' is ',tempprop
          mesherror=.true.
          meshmade=.false.
          exit
         end if
       end do
      end do

      zco=zlo
      do x=1,xpoints
       if(mesherror) exit
        xco=xlo+res*dble(x-1)
       do y=1,ypoints
        if(mesherror) exit
        yco=ylo+res*dble(y-1)
         tempprop=GET_RADIAL_DENSITY_XYZ(xco,yco,zco)
         if(tempprop.gt.(dexp(dlog(10.0d0)*boundlo))) then
!          write(UNIout,*) 'mesh error ', x,y,' 1 is ',tempprop
          mesherror=.true.
          meshmade=.false.
          exit
         end if
       end do
      end do
      zco=zlo+res*dble(zpoints-1)
      do x=1,xpoints
       if(mesherror) exit
        xco=xlo+res*dble(x-1)
       do y=1,ypoints
        if(mesherror) exit
        yco=ylo+res*dble(y-1)
         tempprop=GET_RADIAL_DENSITY_XYZ(xco,yco,zco)
         if(tempprop.gt.(dexp(dlog(10.0d0)*boundlo))) then
!          write(UNIout,*) 'mesh error ', x,y,zpoints,' is ',tempprop
          mesherror=.true.
          meshmade=.false.
          exit
         end if
       end do
      end do

      if(mesherror) boxmargin=boxmargin+res


      call PRG_manager ('exit', 'SGM_CHECKMESH_RADIAL_DENSITY', 'UTILITY')
      return
      end subroutine SGM_checkmesh_radial_density

      subroutine SGM_mesh_density_hybrid_test
!*****************************************************************************************************************
!     Date last modified: October 3, 2014                                                           Version 1.0  *
!     Author: P.L. Warburton                                                                                     *
!     Description: Generates the density and second derivative eigenvalues on a mesh for the SGM                 *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE SGM_defaults
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE type_density

      implicit none
!
! Local scalars:
      integer :: x, y, z
      double precision :: xco,yco,zco,delx,dely,delsq,delz
      double precision :: del2xx,del2yy,del2zz,del2xy,del2xz,del2yz
!
! these scalars are for older versions, kept for testing
      double precision :: trlen1, trlen2, trlen3, a, b,ci
      double precision :: v12,v13,v23
      integer :: i,j
!
! Local Array	  
      double precision, dimension(3,3) :: Hess, EigVector
      double precision, dimension(3) :: EigValue
      double precision, dimension(:), allocatable :: AOprod
      double precision, dimension(:), allocatable  :: dAOprodX,dAOprodY,dAOprodZ
!
! these arrays are for older versions, kept for testing
      double precision, dimension (3,3) :: trace
      double precision, dimension(2,2) :: hm
      double precision :: TraceAB

! Begin:

      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)
      allocate (AOprod(1:MATlen))
      allocate (dAOprodx(1:MATlen))
      allocate (dAOprody(1:MATlen))
      allocate (dAOprodz(1:MATlen))
!     allocate (d2AOprodxx(1:MATlen))
!     allocate (d2AOprodyy(1:MATlen))
!     allocate (d2AOprodzz(1:MATlen))
!     allocate (d2AOprodxy(1:MATlen))
!     allocate (d2AOprodxz(1:MATlen))
!     allocate (d2AOprodyz(1:MATlen))  
!      write(UNIout,'(a)') 'Density arrays allocated'
! lowest box corner of generated mesh is mapped to local scalars
      xco=xlo-res
      yco=ylo-res
      zco=zlo-res
! calculate property and second derivative over mesh points
!      write(Uniout,*) 'Xcoord   Ycoord    Zcoord    Density'
      write(UNIout,*) 'Mesh points ',xpoints,ypoints,zpoints
      do x=1,xpoints
       xco=xco+res
       yco=ylo-res
       do y=1,ypoints
        yco=yco+res
        zco=zlo-res
        do z=1,zpoints
         zco=zco+res   
!         write(UNIout,*) 'Point ',x,y,z  
!         write(UNIout,*) 'Coords ',xco,yco,zco

         call AO_products (xco, yco, zco, AOprod, MATlen)
!         write(UNIout,*) 'Called AOprods ',x,y,z
         property(x,y,z)= TraceAB (PM0, AOprod, NBasis)
!         if(x.eq.1.or.y.eq.1.or.z.eq.1.or.x.eq.xpoints.or.y.eq.ypoints.or.z.eq.zpoints) then
!          write(UNIout,*) 'Density at ', x,y,z,' is ',property(x,y,z)
!         end if

!         write(UNIout,*) 'density ',xco,yco,zco

! this is a mixed version that uses analytical derivatives of the property
! but also uses the original means to get the eigenvalues
! it's kept here for possible testing and debug purposes
         call dAO_products(xco,yco,zco, dAOprodx, dAOprody, dAOprodz, MATlen)
         delx=TraceAB (PM0, dAOprodx, NBasis)
         dely=TraceAB (PM0, dAOprody, NBasis)
         delz=TraceAB (PM0, dAOprodz, NBasis)
        call GET_d2rho_Atom (Xco, Yco, Zco, Hess)
         del2xx=Hess(1,1)
         del2yy=Hess(2,2)
         del2zz=Hess(3,3)
         del2xy=Hess(1,2)
         del2xz=Hess(1,3)
         del2yz=Hess(2,3)
!        call d2AO_products (xco,yco,zco)
!        del2xx=TraceAB (PM0, d2AOprodxx, NBasis)
!        del2yy=TraceAB (PM0, d2AOprodyy, NBasis)
!        del2zz=TraceAB (PM0, d2AOprodzz, NBasis)
!        del2xy=TraceAB (PM0, d2AOprodxy, NBasis)
!        del2xz=TraceAB (PM0, d2AOprodxz, NBasis)
!        del2yz=TraceAB (PM0, d2AOprodyz, NBasis)

          if((dabs(delx)+dabs(dely)+dabs(delz)).gt.1.0d-14) then
            trace(:,:)=0.0d0
            if((dabs(delx)+dabs(dely)).lt.1.0d-14) then
              trace(1,1)=1.0d0
              trace(2,2)=1.0d0         
              trace(3,3)=dsign(1.0d0,delz)
          else 
             if((dabs(delx)+dabs(delz)).lt.1.0d-14) then
              trace(1,1)=1.0d0
              trace(3,2)=1.0d0         
              trace(2,3)=dsign(1.0d0,dely)
          else 
             if((dabs(dely)+dabs(delz)).lt.1.0d-14) then
              trace(2,1)=1.0d0
              trace(3,2)=1.0d0         
              trace(1,3)=dsign(1.0d0,delx)
          else
            delsq=delx**2+dely**2+delz**2
            v13=delx/delsq
            trace(1,1)=1.0d0-v13*delx
            trace(2,1)=-v13*dely
            trace(3,1)=-v13*delz
              if((dabs(trace(1,1))+dabs(delx)).lt.1.0d-14) then
                trace(1,2)=1.0d0
              else 
                if((dabs(trace(2,1))+dabs(dely)).lt.1.0d-14) then
                trace(2,2)=1.0d0
              else 
                if((dabs(trace(3,1))+dabs(delz)).lt.1.0d-14) then
                trace(3,2)=1.0d0
              else
                v23=dely/delsq
                v12=trace(2,1)/(trace(1,1)**2+trace(2,1)**2+trace(3,1)**2)
                trace(1,2)=-v12*trace(1,1)-v23*delx
                trace(2,2)=-v12*trace(2,1)-v23*dely+1.0d0
                trace(3,2)=-v12*trace(3,1)-v23*delz
              end if
              end if
              end if
           trlen1=dsqrt((trace(1,1)**2+trace(2,1)**2+trace(3,1)**2))
           trlen2=dsqrt((trace(1,2)**2+trace(2,2)**2+trace(3,2)**2))
           trlen3=dsqrt(delx**2+dely**2+delz**2)
           trace(1,1)=trace(1,1)/trlen1
           trace(2,1)=trace(2,1)/trlen1
           trace(3,1)=trace(3,1)/trlen1
           trace(1,2)=trace(1,2)/trlen2
           trace(2,2)=trace(2,2)/trlen2
           trace(3,2)=trace(3,2)/trlen2
           trace(1,3)=delx/trlen3
           trace(2,3)=dely/trlen3
           trace(3,3)=delz/trlen3
         end if
         end if
         end if
!         write(UNIout,*) 'Analytical:'
!         write(UNIout,'(a6,3f9.6)') 'dels: ', delx,dely,delz
!
         do i=1,3
         do j=1,3
           if(dabs(trace(i,j)).lt.1.0d-14) trace(i,j)=0.0d0
         end do
         write(UNIout,'(3f9.6)') trace(i,1),trace(i,2),trace(i,3)
         end do
!
!
! hessian here
!        write(UNIout,*) 'Hessian:'
        hm(1,1)=(trace(1,1)*((del2xx*trace(1,1))+(del2xy*trace(2,1)) &
                 +(del2xz*trace(3,1))))+(trace(2,1)*((del2xy*trace(1,1)) &
                 +(del2yy*trace(2,1))+(del2yz*trace(3,1))))+(trace(3,1) &
                 *((del2xz*trace(1,1))+(del2yz*trace(2,1))+(del2zz*trace(3,1))))
        
        hm(2,2)=(trace(1,2)*((del2yz*trace(1,2))+(del2xy*trace(2,2)) &
                 +(del2xz*trace(3,2))))+(trace(2,2)*((del2xy*trace(1,2)) &
                 +(del2yy*trace(2,2))+(del2yz*trace(3,2))))+(trace(3,2) &
                 *((del2xz*trace(1,2))+(del2yz*trace(2,2))+(del2zz*trace(3,2))))
        
        hm(2,1)=(trace(1,2)*((del2xx*trace(1,1))+(del2xy*trace(2,1)) &
                +(del2xz*trace(3,1))))+(trace(2,2)*((del2xy*trace(1,1)) &
                +(del2yy*trace(2,1))+(del2yz*trace(3,1))))+(trace(3,2) &
                *((del2xz*trace(1,1))+(del2yz*trace(2,1))+(del2zz*trace(3,1))))
        
        hm(1,2)=hm(2,1)
!
!       write(UNIout,'(2f9.6)') hm(1,1),hm(1,2)
!       write(UNIout,'(2f9.6)') hm(1,1),hm(1,2)
!
!  calc eigenvalues
!
        do i=1,2
        do j=1,2
        if(dabs(hm(i,j)).lt.1.d-14)hm(i,j)=0.d0
        enddo
        enddo
        a=hm(1,1)+hm(2,2)
        b=(hm(1,1)*hm(2,2))-(hm(2,1)**2)
        ci= a**2-4.d0*b
        if(dabs(ci).lt.1.d-14)ci=0.d0
        secondev(x,y,z)=(a+dsqrt(ci))/2.
!
!        write(UNIout,'(a16,f12.9)') 'Mix eigenvalue: ',secondev(x,y,z)
!
!
!
        end if

        end do
       end do
      end do

      deallocate (AOprod)
      deallocate (dAOprodx)
      deallocate (dAOprody)
      deallocate (dAOprodz)
!     deallocate (d2AOprodxx)
!     deallocate (d2AOprodyy)
!     deallocate (d2AOprodzz)
!     deallocate (d2AOprodxy)
!     deallocate (d2AOprodxz)
!     deallocate (d2AOprodyz)   

      return
      end subroutine SGM_mesh_density_hybrid_test

     subroutine SGM_mesh_density_numerical_test
!*****************************************************************************************************************
!     Date last modified: October 3, 2014                                                           Version 1.0  *
!     Author: P.L. Warburton                                                                                     *
!     Description: Generates the density and second derivative eigenvalues on a mesh for the SGM                 *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE SGM_defaults
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE type_density

      implicit none
!
! Local scalars:
      integer :: x, y, z
      double precision :: xco,yco,zco,delx,dely,delsq,delz
      double precision :: del2xx,del2yy,del2zz,del2xy,del2xz,del2yz
!
! these scalars are for older versions, kept for testing
      double precision :: trlen1, trlen2, trlen3, a, b,ci
      double precision :: v12,v13,v23
      integer :: i,j
!
! Local Array	  
      double precision, dimension(3,3) :: Hess, EigVector
      double precision, dimension(3) :: EigValue
      double precision, dimension(:), allocatable :: AOprod
!
! these arrays are for older versions, kept for testing
      double precision, dimension (3,3) :: trace
      double precision, dimension(2,2) :: hm
      double precision :: TraceAB

! Begin:
      write(UNIout,*) 'Entered numerical density'
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)
      allocate (AOprod(1:MATlen))
!      write(UNIout,'(a)') 'Density arrays allocated'
! lowest box corner of generated mesh is mapped to local scalars
      xco=xlo-res
      yco=ylo-res
      zco=zlo-res
! calculate property and second derivative over mesh points
!      write(Uniout,*) 'Xcoord   Ycoord    Zcoord    Density'
!      write(UNIout,*) 'Mesh points ',xpoints,ypoints,zpoints
!      call flush(6)
      do x=1,xpoints
       xco=xco+res
       yco=ylo-res
       do y=1,ypoints
        yco=yco+res
        zco=zlo-res
        do z=1,zpoints
         zco=zco+res  
         call AO_products (xco, yco, zco, AOprod, MATlen)
!         write(UNIout,*) 'Called AOprods ',x,y,z
         property(x,y,z)= TraceAB (PM0, AOprod, NBasis)

         end do
         end do
         end do


! lowest box corner of generated mesh is mapped to local scalars
      xco=xlo-res
      yco=ylo-res
      zco=zlo-res
! calculate property and second derivative over mesh points
!      write(Uniout,*) 'Xcoord   Ycoord    Zcoord    Density'
!      write(UNIout,*) 'Mesh points ',xpoints,ypoints,zpoints
!      call flush(6)
      do x=1,xpoints
       xco=xco+res
       yco=ylo-res
       do y=1,ypoints
        yco=yco+res
        zco=zlo-res
        do z=1,zpoints
         zco=zco+res  

! this is the original numerical approach to finding curvature
! it's being kept for future testing and debug purposes
         if(x.lt.3.or.y.lt.3.or.z.lt.3.or.x.gt.xpoints-2.or.y.gt.ypoints-2.or.z.gt.zpoints-2) then
          cycle
         else
!         write(UNIout,*) 'Calculating dels ',x,y,z
!         call flush(6)
         delx=(property(x+1,y,z)-property(x-1,y,z))/(2*res)
         dely=(property(x,y+1,z)-property(x,y-1,z))/(2*res)
         delz=(property(x,y,z+1)-property(x,y,z-1))/(2*res)
         del2xx=(property(x+2,y,z)+property(x-2,y,z)-2*property(x,y,z))/(4*res**2)
         del2yy=(property(x,y+2,z)+property(x,y-2,z)-2*property(x,y,z))/(4*res**2)
         del2zz=(property(x,y,z+2)+property(x,y,z-2)-2*property(x,y,z))/(4*res**2)
         del2xy=(property(x+1,y+1,z)+property(x-1,y-1,z)-property(x-1,y+1,z)-property(x+1,y-1,z))/(4*res**2)
         del2yz=(property(x,y+1,z+1)+property(x,y-1,z-1)-property(x,y+1,z-1)-property(x,y-1,z+1))/(4*res**2)
         del2xz=(property(x+1,y,z+1)+property(x-1,y,z-1)-property(x+1,y,z-1)-property(x-1,y,z+1))/(4*res**2)
         delsq=0.0d0 
!         write(UNIout,*) delx,dely,delz,del2xx,del2yy,del2zz,del2xy,del2yz,del2xz 
         if((dabs(delx)+dabs(dely)+dabs(delz)).gt.1.0d-14) then
            trace(:,:)=0.0d0
            if((dabs(delx)+dabs(dely)).lt.1.0d-14) then
              trace(1,1)=1.0d0
              trace(2,2)=1.0d0         
              trace(3,3)=dsign(1.0d0,delz)
          else 
             if((dabs(delx)+dabs(delz)).lt.1.0d-14) then
              trace(1,1)=1.0d0
              trace(3,2)=1.0d0         
              trace(2,3)=dsign(1.0d0,dely)
          else 
             if((dabs(dely)+dabs(delz)).lt.1.0d-14) then
              trace(2,1)=1.0d0
              trace(3,2)=1.0d0         
              trace(1,3)=dsign(1.0d0,delx)
          else
            delsq=delx**2+dely**2+delz**2
            v13=delx/delsq
            trace(1,1)=1.0d0-v13*delx
            trace(2,1)=-v13*dely
            trace(3,1)=-v13*delz
              if((dabs(trace(1,1))+dabs(delx)).lt.1.0d-14) then
                trace(1,2)=1.0d0
              else 
                if((dabs(trace(2,1))+dabs(dely)).lt.1.0d-14) then
                trace(2,2)=1.0d0
              else 
                if((dabs(trace(3,1))+dabs(delz)).lt.1.0d-14) then
                trace(3,2)=1.0d0
              else
                v23=dely/delsq
                v12=trace(2,1)/(trace(1,1)**2+trace(2,1)**2+trace(3,1)**2)
                trace(1,2)=-v12*trace(1,1)-v23*delx
                trace(2,2)=-v12*trace(2,1)-v23*dely+1.0d0
                trace(3,2)=-v12*trace(3,1)-v23*delz
              end if
              end if
              end if
           trlen1=dsqrt((trace(1,1)**2+trace(2,1)**2+trace(3,1)**2))
           trlen2=dsqrt((trace(1,2)**2+trace(2,2)**2+trace(3,2)**2))
           trlen3=dsqrt(delx**2+dely**2+delz**2)
           trace(1,1)=trace(1,1)/trlen1
           trace(2,1)=trace(2,1)/trlen1
           trace(3,1)=trace(3,1)/trlen1
           trace(1,2)=trace(1,2)/trlen2
           trace(2,2)=trace(2,2)/trlen2
           trace(3,2)=trace(3,2)/trlen2
           trace(1,3)=delx/trlen3
           trace(2,3)=dely/trlen3
           trace(3,3)=delz/trlen3
         end if
         end if
         end if
!         write(UNIout,*) 'Numerical:'
!         write(UNIout,'(a6,3f9.6)') 'dels: ', delx,dely,delz
!      call flush(6)
         do i=1,3
         do j=1,3
           if(dabs(trace(i,j)).lt.1.0d-14) trace(i,j)=0.0d0
         end do
!         write(UNIout,'(3f9.6)') trace(i,1),trace(i,2),trace(i,3)
         end do
! original means of getting hessian here
!        write(UNIout,*) 'Hessian:'
        hm(1,1)=(trace(1,1)*((del2xx*trace(1,1))+(del2xy*trace(2,1)) &
                 +(del2xz*trace(3,1))))+(trace(2,1)*((del2xy*trace(1,1)) &
                 +(del2yy*trace(2,1))+(del2yz*trace(3,1))))+(trace(3,1) &
                 *((del2xz*trace(1,1))+(del2yz*trace(2,1))+(del2zz*trace(3,1))))        
        hm(2,2)=(trace(1,2)*((del2yz*trace(1,2))+(del2xy*trace(2,2)) &
                 +(del2xz*trace(3,2))))+(trace(2,2)*((del2xy*trace(1,2)) &
                 +(del2yy*trace(2,2))+(del2yz*trace(3,2))))+(trace(3,2) &
                 *((del2xz*trace(1,2))+(del2yz*trace(2,2))+(del2zz*trace(3,2))))
        hm(2,1)=(trace(1,2)*((del2xx*trace(1,1))+(del2xy*trace(2,1)) &
                +(del2xz*trace(3,1))))+(trace(2,2)*((del2xy*trace(1,1)) &
                +(del2yy*trace(2,1))+(del2yz*trace(3,1))))+(trace(3,2) &
                *((del2xz*trace(1,1))+(del2yz*trace(2,1))+(del2zz*trace(3,1))))
        hm(1,2)=hm(2,1)
!
!       write(UNIout,'(2f9.6)') hm(1,1),hm(1,2)
!       write(UNIout,'(2f9.6)') hm(1,1),hm(1,2)
!  original means to calculate eigenvalues of tangent plane to the surface normal
!
        do i=1,2
        do j=1,2
        if(dabs(hm(i,j)).lt.1.d-14)hm(i,j)=0.d0
        enddo
        enddo
        a=hm(1,1)+hm(2,2)
        b=(hm(1,1)*hm(2,2))-(hm(2,1)**2)
        ci= a**2-4.d0*b
        if(dabs(ci).lt.1.d-14)ci=0.d0
        secondev(x,y,z)=(a+dsqrt(ci))/2.

!        write(UNIout,'(a16,f12.9)') 'Num eigenvalue: ',secondev(x,y,z)
        end if
        end if

        end do
       end do
      end do
      return
      end subroutine SGM_mesh_density_numerical_test
 
