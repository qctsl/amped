      subroutine BLD_QTAIM_objects
!*************************************************************************************************
!       Date last modified July 22, 2015                                                         *
!       Author:                                                                                  *
!       Description:                                                                             * 
!*************************************************************************************************
! Modules:
      USE program_objects

      implicit none

! Local scalar:
      integer :: Iobject
!
! Begin:
      OBJ_QTAIM(1:Max_objects)%modality = 'other'
      OBJ_QTAIM(1:Max_objects)%class = 'QTAIM'
      OBJ_QTAIM(1:Max_objects)%depend = .true.
      NQTAIMobjects = 0
!
! Class Atoms and Bonds in Molecules (QTAIM)
      NQTAIMobjects = NQTAIMobjects + 1
      OBJ_QTAIM(NQTAIMobjects)%name = 'QTAIM'
      OBJ_QTAIM(NQTAIMobjects)%modality = 'BASIN'
      OBJ_QTAIM(NQTAIMobjects)%routine = 'QTAIM_basin_setup'

      NQTAIMobjects = NQTAIMobjects + 1
      OBJ_QTAIM(NQTAIMobjects)%name = 'QTAIM'
      OBJ_QTAIM(NQTAIMobjects)%modality = 'BASIN_DEV'
      OBJ_QTAIM(NQTAIMobjects)%routine = 'QTAIM_basin_setup'



! Dummy Class
      NQTAIMobjects = NQTAIMobjects + 1
      OBJ_QTAIM(NQTAIMobjects)%name = '?'
      OBJ_QTAIM(NQTAIMobjects)%modality = '?'
      OBJ_QTAIM(NQTAIMobjects)%routine = '?'
!
      do Iobject=1,NQTAIMobjects
        OBJ_QTAIM(Iobject)%exist=.false.
        OBJ_QTAIM(Iobject)%Current=.false.
      end do

      return
      end subroutine BLD_QTAIM_objects
