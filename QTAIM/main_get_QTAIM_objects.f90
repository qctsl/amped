      subroutine GET_QTAIM_object (class, Objname, Modality)
!**************************************************************************
!     Date last modified: July 22, 2015                                   *
!     Author: I. Awad                                                     *
!     Description: Objects belonging to the class QTAIM                    *
!**************************************************************************
! Modules:
      USE program_files
      USE program_objects

      implicit none
!
! Input scalar:
      character*(*) :: class
      character*(*) :: Objname
      character*(*) :: Modality
!
! Local scalars:
      integer :: Object_number
!
! Begin:
      call PRG_manager ('enter', 'GET_QTAIM_object', 'UTILITY')

      call GET_object_number (OBJ_QTAIM, NQTAIMobjects, FirstQTAIM, Objname, Modality, Object_number)

      if(OBJ_QTAIM(Object_number)%current)then
        call PRG_manager ('exit', 'GET_QTAIM_object', 'UTILITY')
        return
      end if

      OBJ_QTAIM(Object_number)%exist=.true.
      OBJ_QTAIM(Object_number)%Current=.true.
      Object(ObjNum)%exist=.true.
      Object(ObjNum)%Current=.true.

      select_Object: select case (Objname)
      case ('QTAIM') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      select case (Modality)
      case('BASIN')
       call QTAIM_basin_setup
      case('BASIN_DEV')
       call QTAIM_basin_setup
      case default
       write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                      '" for Modality "',Modality(1:len_trim(Modality)),'"'
       stop 'No such object'
      end select

!     case ('COMPARE') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!     select case (Modality)
!     case default
!       write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
!                      '" for Modality "',Modality(1:len_trim(Modality)),'"'
!       stop 'No such object'
!     end select

      case default
        write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                       '" for Modality "',Modality(1:len_trim(Modality)),'"'
        stop 'No such object'

      end select select_Object

      call PRG_manager ('exit', 'GET_QTAIM_object', 'UTILITY')
      return
      end subroutine GET_QTAIM_object
