      subroutine MENU_QTAIM
!***********************************************************************
!     Date last modified: August 12, 2015                  Version 1.0 *
!     Author: P.L. Warburton                                           *
!     Description: QTAIM menu                                          *
!***********************************************************************
! Modules:
      USE program_parser
      USE program_constants
      USE MENU_gets
      USE QM_defaults
      USE SGM_defaults
      USE QTAIM_defaults

      implicit none
!
! Local scalars:
      integer :: length
      logical done

!
! Begin:
      call PRG_manager ('enter', 'MENU_QTAIM', 'UTILITY')
! Defaults:
      done=.false.
      doall=.false.
      res=0.00
      tol=0.00
      madeprimes=.false.
      meshmade=.false.
      meshallocated=.false.
      domainsallocated=.false.
      mesherror=.false.
      boxmarginset=.false.
      qplot=.false.
      reflevel=1
      verbose=1
      stage=2
! Menu:
      do while (.not.done)
      call new_token (' QTaim:')
!
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      ' Command QTaim:', &
      '   Purpose: Request QTAIM analysis and properties', &
      '   Syntax :', &
      '   REsolution = <real>, Override the default mesh resolution of 0.20 bohr', &
      '   TOlerance = <real>, Override the default tolerance of 95.0%', &
      '   PLots, Supply a menu for plotting basin outputs', &
      '   LEvel = <integer> , Determines level of point refinement (between 1 and 3). 1 is default', &
      '   STage = <integer> , Determines number of refinement stages (between 1 and 4). 2 is default', &
      '   VErbose = <integer> , Determines level of output (between 1 and 4). 1 is default', &
      '                         1 indicates minimal output while 4 will output everything (very large file)', &

      '   end'
!
! REsolution
      else if(token(1:2).EQ.'RE')then
         call GET_value (res)
    
! TOlerance
      else if(token(1:2).EQ.'TO')then
         call GET_value (tol)

! PLots
      else if(token(1:2).EQ.'PL')then
         qplot=.true.
         call MENU_QTAIM_PLOTS

! LEvel
      else if(token(1:2).EQ.'LE')then
         call GET_value (reflevel)
         if(reflevel.lt.1) reflevel=1
         if(reflevel.gt.3) reflevel=3
! VErbose
      else if(token(1:2).EQ.'VE')then
         call GET_value (verbose)
         if(verbose.lt.1) verbose=1
         if(verbose.gt.4) verbose=4
! STage
      else if(token(1:2).EQ.'ST')then
         call GET_value (stage)
         if(stage.lt.1) stage=1
         if(stage.gt.4) stage=4


      else
         call MENU_end (done)
      end if
!
      end do !(.not.done)


!
! End of routine MENU_QTAIM
      call PRG_manager ('exit', 'MENU_QTAIM', 'UTILITY')
      return
      end

      subroutine MENU_QTAIM_PLOTS
!***********************************************************************
!     Date last modified: August 12, 2015                  Version 1.0 *
!     Author: P.L. Warburton                                           *
!     Description: QTAIM plots menu                                    *
!***********************************************************************
! Modules:
      USE program_parser
      USE program_constants
      USE MENU_gets
      USE QM_defaults
      USE SGM_defaults
      USE QTAIM_defaults
      USE type_plotting

      implicit none
!
! Local scalars:
      integer :: length,lenstr
      logical done

!
! Begin:
      call PRG_manager ('enter', 'MENU_QTAIM_PLOTS', 'UTILITY')
! Defaults:
     qpden=.false.
     qprad=.false.
     qpx=.false.
     qpy=.false.
     qpz=.false. 

! Menu:
      do while (.not.done)
      call new_token (' QPlot:')
!
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      ' Command PLot:', &
      '   Purpose: Request a 2D QTAIM basin plot based on a single variable held', &
      '            at a constant value.  Plot file will include basin information', &
      '            and can also include density or radial density values', &
      '   Syntax :', &
      '   DEnsity = <logical>, Plot file includes column for density', &
      '   RAdial = <logical>, Plot fileincludes column for radial density', &
      '   X = <real>, 2D plot will be for mesh points nearest indicated fixed X value', &
      '   Y = <real>, 2D plot will be for mesh points nearest indicated fixed Y value', &
      '   Z = <real>, 2D plot will be for mesh points nearest indicated fixed Z value', &
      '   Indicating more than one of X, Y, Z will lead to error termination', &
      '   FIlename = < character > Change Mathematica input file to PlotFilename.',&

      '   end'
!
! DEnsity
      else if(token(1:2).EQ.'DE')then
         qpden=.true.
    
! RAdial
      else if(token(1:2).EQ.'RA')then
         qprad=.true.

! X
      else if(token(1:1).EQ.'X')then
         call GET_value(qxplot)
         qpx=.true.
         if(qpx.and.qpy) call QTAIM_PLOT_ERROR
         if(qpx.and.qpz) call QTAIM_PLOT_ERROR
! Y
      else if(token(1:1).EQ.'Y')then
         call GET_value(qyplot)
         qpy=.true.
         if(qpx.and.qpy) call QTAIM_PLOT_ERROR
         if(qpy.and.qpz) call QTAIM_PLOT_ERROR
! Z
      else if(token(1:1).EQ.'Z')then
         call GET_value(qzplot)
         qpz=.true.
         if(qpx.and.qpz) call QTAIM_PLOT_ERROR
         if(qpy.and.qpz) call QTAIM_PLOT_ERROR
! PLotfilename
      else if(token(1:2).eq.'FI')then
        call GET_value (PlotFilename,lenstr)
        LPlotNamed =.True.


      else
         call MENU_end (done)
      end if
!
      end do !(.not.done)


!
! End of routine MENU_QTAIM_PLOTS
      call PRG_manager ('exit', 'MENU_QTAIM_PLOTS', 'UTILITY')
      return
      end

      subroutine QTAIM_PLOT_ERROR
!***********************************************************************
!     Date last modified: August 12, 2015                  Version 1.0 *
!     Author: P.L. Warburton                                           *
!     Description: QTAIM plots error handling                          *
!***********************************************************************
! Modules:
      USE QTAIM_defaults

      implicit none
! Local scalars:
      character(58) stop_message
!
! Begin:
      call PRG_manager ('enter', 'QTAIM_PLOT_ERROR', 'UTILITY')
      stop_message='Defined more than one fixed variable for QTAIM basin plots'
      call PRG_stop(stop_message)

!
! End of routine QTAIM_PLOT_ERROR
      call PRG_manager ('exit', 'QTAIM_PLOT_ERROR', 'UTILITY')
      return
      end
