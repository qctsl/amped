      subroutine QTAIM_basin_setup
!*****************************************************************************************************************
!     Date last modified: October 26, 2015                                                          Version 1.0  *
!     Author: P.L. Warburton                                                                                     *
!     Description: Handles changes in variables for different QTAIM basin objects                                *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE SGM_defaults
      USE program_objects
      USE QM_defaults
      USE QM_objects
      USE type_density
      USE QTAIM_defaults

      implicit none
!
! Local scalars:
      character(len=MAX_Objlen) :: Modality 
  
! Local Array	  

! Begin:
      call PRG_manager ('enter', 'QTAIM_BASIN_SETUP', 'UTILITY')

! Default
      proptype='DEN'
      if (res.eq.0.0) res=0.20
      if (tol.eq.0.0) tol=95.0
      nNNA=0
      nSP=0
! refinement resolution is one third of the mesh resolution [3 x 3 mesh for each refined point]
      refres=res/3.0
! For plotting of basins and volumes, only need mesh to include isocontors to 1E-3 a.u.
! For mesh integrated properties mesh needs to include isocontours to 1E-6 a.u.
      Modality=Object(ObjNum)%modality
      select case (Modality)
      case ('BASIN_DEV')
       boundlo=-3.0
       boxmargin=2.5
       pathlist=100
       call QTAIM_BASIN_DEV
!      case ('BASIN')
!       boundlo=-6.0
!       boxmargin=4.5
!       pathlist=400
!       call QTAIM_BASIN
      case default
        write(UNIout,*)'Modality "',Modality(1:len_trim(Modality)),'" does not exist'
        stop 'No such object'
      end select


      call PRG_manager ('exit', 'QTAIM_BASIN_SETUP', 'UTILITY')
      return
      end subroutine QTAIM_basin_setup
