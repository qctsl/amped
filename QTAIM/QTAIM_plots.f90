      subroutine QTAIM_basin_plot
!*****************************************************************************************************************
!     Date last modified: August 12, 2015                                                           Version 1.0  *
!     Author: P.L. Warburton                                                                                     *
!     Description: Generates plots of QTAIM basin data with opions for density and radial density as well        *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE SGM_defaults
      USE type_molecule
      USE QTAIM_defaults
      USE type_plotting
      USE module_grid_points

      implicit none
!
! Local scalars:
! Local Array	  

! Begin:
      call PRG_manager ('enter', 'QTAIM_BASIN_PLOT', 'UTILITY')
       PlotFileID=' '
       if((.not.qpden).and.(.not.qprad)) write(PlotFileID,'(a)')'BASIN' 
       if(qpden.and.(.not.qprad)) write(PlotFileID,'(a)')'BASIN_DEN' 
       if((.not.qpden).and.qprad) write(PlotFileID,'(a)')'BASIN_RAD' 
       if(qpden.and.qprad) write(PlotFileID,'(a)')'BASIN_DEN_RAD' 
       if(qpx) NGridPoints=zpoints*ypoints
       if(qpy) NGridPoints=xpoints*zpoints
       if(qpz) NGridPoints=xpoints*ypoints
       call BLD_plot_file_BASIN (PlotFileID, NGridPoints, Plotfile_unit)

!       write(*,*) Plotfilename,Plotfile_unit
!       write(PlotFunction,'(a)')'Radial Density'
!       call PRT_GRID_MOL (Rden, NGridPoints, fmt_r2rho)
!       close (unit=Plotfile_unit)

      call PRG_manager ('exit', 'QTAIM_BASIN_PLOT', 'UTILITY')
      return
      end subroutine QTAIM_basin_plot

      subroutine BLD_plot_file_BASIN (FileID, NGridpts, File_unit)
!**********************************************************************
!     Date last modified: October 10, 2013                            *
!     Authors: R.A. Poirier                                           *
!     Description: Create a filename for plotting data and get a unit.*
!**********************************************************************
! Modules:
      USE program_files
      USE type_molecule
      USE module_grid_points
      USE QM_defaults
      USE type_plotting

      implicit none
!
! Input/Output variables
      integer, intent(IN) :: NGridpts
      integer, intent(OUT) :: File_unit
      character*(*) :: FileID
!
! Local scalars:
      integer :: lenstr
      logical :: Lerror
      character(len=32):: Ctemp,CNGridPts
!
! Begin:
      call PRG_manager ('enter','BLD_plot_file_BASIN', 'UTILITY')

      if(.not.LPlotNamed)then
        write(Ctemp,'(a,i8)')'_',NGridpts
        call RemoveBlanks (Ctemp, CNGridPts, lenstr)
        PlotFilename='plot_'//trim(FileID)//CNGridPts(1:lenstr)//'_'//trim(MOLECULE%FORMULA)//'.dat'
      end if

      call GET_unit (PlotFilename, File_unit, Lerror)
      PRG_file(File_unit)%status='REPLACE '
      PRG_file(File_unit)%name=PlotFilename
      PRG_file(File_unit)%form='FORMATTED'
      PRG_file(File_unit)%type=FileID
      call FILE_open (File_unit)

      write(File_unit,'(2a)')'Results calculated at: ',trim(Level_of_Theory)//'/'//trim(Basis_set_name)
!
! End of routine BLD_plot_file_MOL
      call PRG_manager ('exit', 'BLD_plot_file_BASIN', 'UTILITY')
      return
      end subroutine BLD_plot_file_BASIN
