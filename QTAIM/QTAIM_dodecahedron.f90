      subroutine dodecahedron
!*****************************************************************************************************************
!     Date last modified: August 12, 2015                                                           Version 1.0  *
!     Author: P.L. Warburton                                                                                     *
!     Description: Determines the QTAIM basins for a molecule based on J. Phys.: Condens. Matter 21 084204 (2009)*
!                  This procedure uses analytical density gradients instead of the numerical gradients           *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE SGM_defaults
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE type_density
      USE QTAIM_defaults

      implicit none
!
! Local scalars:
      integer :: Iatom,xloop,yloop,zloop,maxbasins
      double precision :: rstep,maxden,minden,r,phi,XApt,YApt,ZApt  
      double precision :: x,y,z,den,ratio,length,invphi
      double precision :: TraceAB
      logical :: sphere  
! Local Array	  
      double precision, dimension(:), allocatable :: AOprod
!
! Begin:
      call PRG_manager ('enter', 'dodecahedron', 'UTILITY')

! the golden ratio (phi) is useful in this subroutine
      phi=(1.0d0+dsqrt(5.0d0))/2.0d0
      invphi=1.0d0/phi
! this is the tolerance of min/max density above which it is considered spherical
! assuming there will be no more than 1 non-nuclear attractor per atom
      allocate(radii(2*Natoms))
      radii(:)=0.0d0
      maxbasins=2*Natoms
      allocate(ptsinbasin(maxbasins))
      ptsinbasin(:)=0.0d0

      write(UNIout,'(a20)') 'QTAIM basin analysis'
      write(UNIout,'(a20)') '===================='
       
      write(UNIout,'(a24,f4.1,a34)') 'Based on a tolerance of ',tol,'% the radius around each attractor'
      write(UNIout,'(a63)') 'where it is assumed all points within are part of the basin is:'

! based on knowledge that radial density of core shells is effectively spherical
! we will assume that up until some radius r all points near a nucleus belong to
! the basin of that nucleus.  As long as the radial density is reasonably the same
! at all points at radius r (based on a sampling) then the steept ascent gradient path
! should lead to that nucleus and all points within that radius are part of that basin
      do Iatom=1,Natoms
        if(CARTESIAN(Iatom)%Atomic_number.le.0) cycle
        nSP=nSP+1
        r=0.0d0
        rstep=0.005
        sphere=.true.
        XApt=CARTESIAN(Iatom)%X
        YApt=CARTESIAN(Iatom)%Y
        ZApt=CARTESIAN(Iatom)%Z
! start keeping track of stationary points
         call DEL2_DENSITY_QTAIM(XApt,YApt,ZApt)
         sprank(nSP)=ran
         spsig(nSP)=sig
         spx(nSP)=XApt
         spy(nSP)=YApt
         spz(nSP)=ZApt
         spcode='NA  '
         if(verbose.ge.2) then
          write(*,*) 'Stationary point ',nSP
          write(UNIout,'(a32,3(f10.6,1x))') 'There is a nuclear attractor at ',spx(nSP),spy(nSP),spz(nSP)
          write(UNIout,'(a24,i1,1x,i2,1x,a3)') 'Rank, sig, and code are ',sprank(nSP),spsig(nSP),spcode(nSP)
         end if

! we will model the sphere around the nucleus by a dodecahedron with 20 vertices
! we will follow the cartesian axes of the mesh to define unit directions             
        theloop: do
          if(.not.sphere) exit
          r=r+rstep
!          write(*,'(a3,f12.6)') 'r: ',r        
          maxden=-1.0d12
          minden=1.0d12
! there are 8 points of the dodecahedron that have coordinates (+/-r,+/-r,+/-r)
          do xloop=-1,1,2
            do yloop=-1,1,2
              do zloop=-1,1,2
                x=XApt+r*dble(xloop)
                y=YApt+r*dble(yloop)
                z=ZApt+r*dble(zloop)
!                length=dsqrt((r*dble(xloop))**2+(r*dble(yloop))**2+(r*dble(zloop))**2)
!                write(*,'(a7,4(f12.6,1x))') 'x,y,z: ',x,y,z,length
                call AO_products (x,y,z, AOprod, MATlen)
                den= TraceAB (PM0, AOprod, NBasis)
                maxden=dmax1(maxden,den)
                minden=dmin1(minden,den)
                ratio=minden/maxden*100.
!                write(*,'(a8,2(es12.5,1x),f7.3,a1)') 'Radial: ',minden,maxden,ratio,'%' 
              end do
            end do 
          end do  
! there are 3x4=12 points with one coordinate of zero, 1 of +/- phi and one with +/- (1/phi)             
          do yloop=-1,1,2
            do zloop=-1,1,2
                x=XApt
                y=YApt+r*dble(yloop)*invphi
                z=ZApt+r*dble(zloop)*phi
!                length=dsqrt((dble(yloop)*r*invphi)**2+(dble(zloop)*r*phi)**2)
!                write(*,'(a7,4(f12.6,1x))') 'x,y,z: ',x,y,z,length
                call AO_products (x, y, z, AOprod, MATlen)
                den= TraceAB (PM0, AOprod, NBasis)
                maxden=dmax1(maxden,den)
                minden=dmin1(minden,den)
                ratio=minden/maxden*100.
!                write(*,'(a8,2(es12.5,1x),f7.3,a1)') 'Radial: ',minden,maxden,ratio,'%' 
            end do 
          end do  
          do xloop=-1,1,2
            do yloop=-1,1,2
                z=ZApt
                x=XApt+r*dble(xloop)*invphi
                y=YApt+r*dble(yloop)*phi
!                length=dsqrt((dble(xloop)*r*invphi)**2+(dble(yloop)*r*phi)**2)
!                write(*,'(a7,4(f12.6,1x))') 'x,y,z: ',x,y,z,length
                call AO_products (x, y, z, AOprod, MATlen)
                den= TraceAB (PM0, AOprod, NBasis)
                maxden=dmax1(maxden,den)
                minden=dmin1(minden,den)
                ratio=minden/maxden*100.
!                write(*,'(a8,2(es12.5,1x),f7.3,a1)') 'Radial: ',minden,maxden,ratio,'%' 
            end do 
          end do  
          do xloop=-1,1,2
            do zloop=-1,1,2
                y=YApt
                x=XApt+r*dble(xloop)*phi
                z=ZApt+r*dble(zloop)*invphi
!                length=dsqrt((dble(zloop)*r*invphi)**2+(dble(xloop)*r*phi)**2)
!                write(*,'(a7,4(f12.6,1x))') 'x,y,z: ',x,y,z,length
                call AO_products (x, y, z, AOprod, MATlen)
                den= TraceAB (PM0, AOprod, NBasis)
                maxden=dmax1(maxden,den)
                minden=dmin1(minden,den)
                ratio=minden/maxden*100.
!                write(*,'(a8,2(es12.5,1x),f7.3,a1)') 'Radial: ',minden,maxden,ratio,'%' 
            end do 
          end do  
     
          if(ratio.lt.tol) then
            sphere=.false. 
            radii(Iatom)=dsqrt(3.0*(r-rstep)**2)
! if radius is less than half the grid resolution we set the radius as the distance from the center
! of a mesh point to the any corner of that mesh point, namely sqrt(3*(res/2)**2).  This avoids floating
! point exceptions caused by nuclear position grid points not being correctly assigned to the basin in
! some situations
            if(radii(Iatom).lt.dsqrt(3.0*(res/2.0)**2)) radii(Iatom)=dsqrt(3.0*(res/2.0)**2)
           if(verbose.le.4) write(UNIout,'(f6.3,a15,i3)') radii(Iatom), ' bohr for atom ',Iatom
          end if      
                  
        end do theloop

      end do ! Iatom

      call PRG_manager ('exit', 'dodecahedron', 'UTILITY')
      return
      end subroutine dodecahedron

      subroutine dodecahedron_NNA(xin,yin,zin,Iatom)
!*****************************************************************************************************************
!     Date last modified: August 12, 2015                                                           Version 1.0  *
!     Author: P.L. Warburton                                                                                     *
!     Description: Determines the QTAIM basins for a molecule based on J. Phys.: Condens. Matter 21 084204 (2009)*
!                  This procedure uses analytical density gradients instead of the numerical gradients           *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE SGM_defaults
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE type_density
      USE QTAIM_defaults

      implicit none
!
! Local scalars:
      integer :: Iatom,xloop,yloop,zloop,maxbasins,xl,yl,zl,check,atpoint
      double precision :: rstep,maxden,minden,r,phi,XApt,YApt,ZApt,xin,yin,zin  
      double precision :: x,y,z,den,ratio,length,invphi,xco,yco,zco
      double precision :: TraceAB
      logical :: sphere  
! Local Array	  
      double precision, dimension(:), allocatable :: AOprod
!
! Begin:
      call PRG_manager ('enter', 'dodecahedron_NNA', 'UTILITY')

! the golden ratio (phi) is useful in this subroutine
      phi=(1.0d0+dsqrt(5.0d0))/2.0d0
      invphi=1.0d0/phi

! initialize
        r=0.0d0
        rstep=0.005
        sphere=.true.
        check=0
        if(verbose.eq.4) write(UNIout,'(a4,3(f10.6,1x))') 'Xin ',xin,yin,zin
        XApt=xin
        YApt=yin
        ZApt=zin


! we will model the sphere around the nucleus by a dodecahedron with 20 vertices
! we will follow the cartesian axes of the mesh to define unit directions             
        theloop: do
          if(.not.sphere) exit
          r=r+rstep
!          write(*,'(a3,f12.6)') 'r: ',r        
          maxden=-1.0d12
          minden=1.0d12
! there are 8 points of the dodecahedron that have coordinates (+/-r,+/-r,+/-r)
          do xloop=-1,1,2
            do yloop=-1,1,2
              do zloop=-1,1,2
                x=XApt+r*dble(xloop)
                y=YApt+r*dble(yloop)
                z=ZApt+r*dble(zloop)
!                length=dsqrt((r*dble(xloop))**2+(r*dble(yloop))**2+(r*dble(zloop))**2)
!                write(*,'(a7,4(f12.6,1x))') 'x,y,z: ',x,y,z,length
                call AO_products (x, y, z, AOprod, MATlen)
                den= TraceAB (PM0, AOprod, NBasis)
                maxden=dmax1(maxden,den)
                minden=dmin1(minden,den)
                ratio=minden/maxden*100.
!                write(*,'(a8,2(es12.5,1x),f7.3,a1)') 'Radial: ',minden,maxden,ratio,'%' 
              end do
            end do 
          end do  
! there are 3x4=12 points with one coordinate of zero, 1 of +/- phi and one with +/- (1/phi)             
          do yloop=-1,1,2
            do zloop=-1,1,2
                x=XApt
                y=YApt+r*dble(yloop)*invphi
                z=ZApt+r*dble(zloop)*phi
!                length=dsqrt((dble(yloop)*r*invphi)**2+(dble(zloop)*r*phi)**2)
!                write(*,'(a7,4(f12.6,1x))') 'x,y,z: ',x,y,z,length
                call AO_products (x, y, z, AOprod, MATlen)
                den= TraceAB (PM0, AOprod, NBasis)
                maxden=dmax1(maxden,den)
                minden=dmin1(minden,den)
                ratio=minden/maxden*100.
!                write(*,'(a8,2(es12.5,1x),f7.3,a1)') 'Radial: ',minden,maxden,ratio,'%' 
            end do 
          end do  
          do xloop=-1,1,2
            do yloop=-1,1,2
                z=ZApt
                x=XApt+r*dble(xloop)*invphi
                y=YApt+r*dble(yloop)*phi
!                length=dsqrt((dble(xloop)*r*invphi)**2+(dble(yloop)*r*phi)**2)
!                write(*,'(a7,4(f12.6,1x))') 'x,y,z: ',x,y,z,length
                call AO_products (x, y, z, AOprod, MATlen)
                den= TraceAB (PM0, AOprod, NBasis)
                maxden=dmax1(maxden,den)
                minden=dmin1(minden,den)
                ratio=minden/maxden*100.
!                write(*,'(a8,2(es12.5,1x),f7.3,a1)') 'Radial: ',minden,maxden,ratio,'%' 
            end do 
          end do  
          do xloop=-1,1,2
            do zloop=-1,1,2
                y=YApt
                x=XApt+r*dble(xloop)*phi
                z=ZApt+r*dble(zloop)*invphi
!                length=dsqrt((dble(zloop)*r*invphi)**2+(dble(xloop)*r*phi)**2)
!                write(*,'(a7,4(f12.6,1x))') 'x,y,z: ',x,y,z,length
                call AO_products (x, y, z, AOprod, MATlen)
                den= TraceAB (PM0, AOprod, NBasis)
                maxden=dmax1(maxden,den)
                minden=dmin1(minden,den)
                ratio=minden/maxden*100.
!                write(*,'(a8,2(es12.5,1x),f7.3,a1)') 'Radial: ',minden,maxden,ratio,'%' 
            end do 
          end do  
     
          if(ratio.lt.tol) then
            sphere=.false. 
            radii(Iatom)=dsqrt(3.0*(r-rstep)**2)
! if radius is less than half the grid resolution we set the radius as the distance from the center
! of a mesh point to the any corner of that mesh point, namely sqrt(3*(res/2)**2).  This avoids floating
! point exceptions caused by nuclear position grid points not being correctly assigned to the basin in
! some situations
             if(radii(Iatom).lt.dsqrt(3.0*(res/2.0)**2)) radii(Iatom)=dsqrt(3.0*(res/2.0)**2)
           if(verbose.le.4) write(UNIout,'(f6.3,a15,i3)') radii(Iatom), ' bohr for NNA  ',Iatom
          end if      
                  
        end do theloop
! now we assign mesh points within radius to the basin
      xco=xlo-res
      yco=ylo-res
      zco=zlo-res
      do xl=1,xpoints
       xco=xco+res
       yco=ylo-res
       do yl=1,ypoints
        yco=yco+res
        zco=zlo-res
        do zl=1,zpoints
         atpoint=zl+zpoints*(yl-1)+zpoints*ypoints*(xl-1)
         zco=zco+res   
          if(basin(atpoint).eq.0) then
           R=dsqrt((xco-xin)**2+(yco-yin)**2+(zco-zin)**2)
           if(verbose.eq.4) write(UNIout,'(a6,3(f10.6,1x))')'point ',xco,yco,zco
           if(verbose.eq.4) write(UNIout,'(a2,f10.6)')'R ',R
            if(R.le.radii(Iatom)) then
             basin(atpoint)=Iatom
             if(R.le.dsqrt(3.0*(res/2.0)**2)) then
               ptcode(atpoint)='NNA'
             else
               ptcode(atpoint)='ANN'
             end if
             assigned=assigned+1.0
             notass=notass-1.0
             ptsinbasin(Iatom)=ptsinbasin(Iatom)+1.0d0
              if(verbose.ge.3) then
               write(UNIout,'(a6,3(i5),a22,i3)') 'Point ',xl,yl,zl,' is assigned to basin ',Iatom
               write(*,*) 'Ptcode: ',ptcode(atpoint)
               flush(6)
              end if
            end if
           end if
        end do
       end do
      end do


      call PRG_manager ('exit', 'dodecahedron_NNA', 'UTILITY')
      return
      end subroutine dodecahedron_NNA
