      subroutine QTAIM_SP_point(x,y,z)
!*****************************************************************************************************************
!     Date last modified: September 15, 2015                                                        Version 1.0  *
!     Author: P.L. Warburton                                                                                     *
!     Description: Determines the QTAIM basins for a molecule based on J. Phys.: Condens. Matter 21 084204 (2009)*
!                  This procedure uses analytical density gradients instead of the numerical gradients           *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE SGM_defaults
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE QTAIM_defaults 

      implicit none
!
! Local scalars:
      integer :: x,y,z,ohmy,atpoint,lx,ly,lz
      double precision :: xco,yco,zco,xloc,yloc,zloc,oldlen,newlen,tx,ty,tz,con,lendiff,mult
      double precision :: TraceAB
      logical :: fNNA,fSP
      double precision, dimension(:), allocatable  :: dAOprodX,dAOprodY,dAOprodZ
!
! Begin:
      call PRG_manager ('enter', 'QTAIM_SP_point', 'UTILITY')

      atpoint=z+zpoints*(y-1)+zpoints*ypoints*(x-1)
      xco=xlo+(x-1)*res
      yco=ylo+(y-1)*res
      zco=zlo+(z-1)*res
      
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! may as well try and find stationary points while we're at it
      mult=res/2.0
      ohmy=1
      oldlen=1.0d12
      newlen=-1.0d12
      fNNa=.false.
      fSP=.false.
      xloc=xco
      yloc=yco
      zloc=zco
! loop SP
      SP: do
       if(fNNA.or.fSP) exit
        if(allocated(dAOprodx)) deallocate(dAOprodx)
        allocate(dAOprodx(1:MATlen))
        if(allocated(dAOprody)) deallocate(dAOprody)
        allocate(dAOprody(1:MATlen))
        if(allocated(dAOprodz)) deallocate(dAOprodz)
        allocate(dAOprodz(1:MATlen))      
!       call AO_products (xco,yco,zco, AOprod, MATlen)  ! NOT used (RAP)
        call dAO_products(xco,yco,zco, dAOprodX, dAOprodY, dAOprodZ, MATlen)
        tx=TraceAB (PM0, dAOprodx, NBasis)
        ty=TraceAB (PM0, dAOprody, NBasis)
        tz=TraceAB (PM0, dAOprodz, NBasis)
! calculate the constant c
! need to avoid divide by zero errors
            con=1.0d30
            if(tx.ne.0.0) then
             con=dmin1(con,res/dabs(tx))    
            end if
            if(tx.ne.0.0) then
             con=dmin1(con,res/dabs(tx))    
            end if
            if(tx.ne.0.0) then
             con=dmin1(con,res/dabs(tx))    
            end if
        newlen=dsqrt(tx**2+ty**2+tz**2)
         if(verbose.ge.3) then
          write(UNIout,*) 'Now we are looking at point ',x,y,z
          write(UNIout,*) 'Point ',xloc,yloc,zloc
          write(UNIout,'(a6,3(es10.3,1x))') 'dels: ',tx,ty,tz
          write(UNIout,'(a6,es10.3)') 'mult: ',mult
          write(UNIout,'(a8,1(es10.3,1x))') 'length: ',newlen
          write(UNIout,'(a9,1(es10.3,1x))') 'lendiff: ',lendiff
          write(UNIout,'(a6,i3)') 'ohmy: ',ohmy 
          flush(6)  
         end if
        if(newlen.lt.1.0e-6) then
! now we've found a stationary point, what kind is it?
         call DEL2_DENSITY_QTAIM(xloc,yloc,zloc)
         if(ran.ne.3) exit SP
         if(ran.eq.3.and.sig.eq.-3) then 
          fNNA=.true.
           else
          fSP=.true.
         end if
         exit SP
        end if
        lendiff=oldlen-newlen

        oldlen=newlen
        xloc=xloc-mult*tx/con
        yloc=yloc-mult*ty/con
        zloc=zloc-mult*tz/con
! mesh point the new point would be in
        lx=nint((xloc-xlo)/res)+1              
        ly=nint((yloc-ylo)/res)+1              
        lz=nint((zloc-zlo)/res)+1
        if(verbose.ge.3) then
          write(UNIout,*) 'Been taken to ',lx,ly,lz
        end if

! if the new point is outside the mesh we are falling to a (0,0) INF point
! we don't care
        if(lx.lt.1.or.lx.gt.xpoints) exit sp
        if(ly.lt.1.or.ly.gt.ypoints) exit sp
        if(lz.lt.1.or.lz.gt.zpoints) exit sp
      
        ohmy=ohmy+1
        if(ohmy.gt.100) then
         exit SP
        end if

        if(verbose.ge.4) write(UNIout,'(a8,3(f10.6,1x))') 'now at: ',xco,yco,zco
        if((mod(ohmy,10).eq.0).or.(lendiff.lt.0.0)) then
         mult=mult/2.0
        end if
        end do SP

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      call PRG_manager ('exit', 'QTAIM_SP_point', 'UTILITY')
      return
      end subroutine QTAIM_SP_point

     subroutine DEL2_DENSITY_QTAIM(xco,yco,zco)
!***********************************************************************************
!     Date last modified: August 15, 2012                                          *
!     Authors: R.A. Poirier                                                        *
!     Description:                                                                 *
!                Calculate DEL2 charge density on a grid point (mesh)              *
!***********************************************************************************
! Modules:
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE module_grid_points
      USE module_type_basin
      USE type_molecule
      USE QTAIM_defaults
      USE type_density

      implicit none
!
! Local scalars:
      double precision :: xx,xy,xz,yy,yz,zz,xco,yco,zco,dx,dy,dz
      double precision :: TraceAB
  
! Local Array	  
      double precision, dimension(3,3) :: Hess, EigVector
      double precision, dimension(3) :: EigValue
      double precision, dimension(:), allocatable  :: dAOprodX,dAOprodY,dAOprodZ
!
! Begin:
      call PRG_manager ('enter','DEL2_DENSITY_QTAIM', 'UTILITY')

      if(allocated(dAOprodx)) deallocate(dAOprodx)
      allocate(dAOprodx(1:MATlen))
      if(allocated(dAOprody)) deallocate(dAOprody)
      allocate(dAOprody(1:MATlen))
      if(allocated(dAOprodz)) deallocate(dAOprodz)
      allocate(dAOprodz(1:MATlen))      
!     if(allocated(d2AOprodxx)) deallocate(d2AOprodxx)
!     allocate (d2AOprodxx(1:MATlen))
!     if(allocated(d2AOprodyy)) deallocate(d2AOprodyy)
!     allocate (d2AOprodyy(1:MATlen))
!     if(allocated(d2AOprodzz)) deallocate(d2AOprodzz)
!     allocate (d2AOprodzz(1:MATlen))
!     if(allocated(d2AOprodxy)) deallocate(d2AOprodxy)
!     allocate (d2AOprodxy(1:MATlen))
!     if(allocated(d2AOprodxz)) deallocate(d2AOprodxz)
!     allocate (d2AOprodxz(1:MATlen))
!     if(allocated(d2AOprodyz)) deallocate(d2AOprodyz)
!     allocate (d2AOprodyz(1:MATlen))   

      call dAO_products (xco,yco,zco, dAOprodX, dAOprodY, dAOprodZ, MATlen)
      dx=TraceAB (PM0, dAOprodx, NBasis)
      dy=TraceAB (PM0, dAOprody, NBasis)
      dz=TraceAB (PM0, dAOprodz, NBasis)
      call GET_d2rho_Atom (Xco, Yco, Zco, Hess)
!     call d2AO_products (xco,yco,zco)
!     xx=TraceAB (PM0, d2AOprodxx, NBasis)
!     yy=TraceAB (PM0, d2AOprodyy, NBasis)
!     zz=TraceAB (PM0, d2AOprodzz, NBasis)
!     xy=TraceAB (PM0, d2AOprodxy, NBasis)
!     xz=TraceAB (PM0, d2AOprodxz, NBasis)
!     yz=TraceAB (PM0, d2AOprodyz, NBasis)

!     Hess(1,1)=xx
!     Hess(1,2)=xy
!     Hess(1,3)=xz
!     Hess(2,1)=Hess(1,2)
!     Hess(2,2)=yy
!     Hess(2,3)=yz
!     Hess(3,1)=Hess(1,3)
!     Hess(3,2)=Hess(2,3)
!     Hess(3,3)=zz
      call MATRIX_diagonalize (Hess, EigVector, EigValue, 2, .true.)

      if(verbose.ge.2) then
       write(UNIout,'(a20,3(es10.3,1x))') 'The gradients are   ',dx,dy,dz
       write(UNIout,'(a20,3(es10.3,1x))') 'The eigenvalues are ',EigValue(1),EigValue(2),EigValue(3)
      end if
      call CP_classification_QTAIM (EigValue, CPRS)
      if (verbose.ge.2) write(UNIout,'(2a)')'Rank and Signature of the stationary point is: ',CPRS

! End of routine DEL2_DENSITY_QTAIM
      call PRG_manager ('exit', 'DEL2_DENSITY_QTAIM', 'UTILITY')
      return
      end subroutine DEL2_DENSITY_QTAIM

      subroutine CP_classification_QTAIM (EigValues, CPRS)
!********************************************************************
!     Date last modified: August 15, 2012                           *
!     Authors: R.A. Poirier                                         *
!     Description: Calculate the charge density at point.           *
!********************************************************************
! Modules:
      USE program_constants
      USE QTAIM_defaults
      implicit none
!
! Input array:
      double precision :: EigValues(3)
!
! Output scalar:
      character(*) :: CPRS ! (R,S)
!
! Local scalars:
      integer :: Ieig,Rank,Signature
      double precision :: cutoff
      parameter (cutoff=1.0E-5)
!
! Begin:
      call PRG_manager ('enter','CP_classification_QTAIM', 'UTILITY')
!
      ran=0
      sig=0
      do Ieig=1,3
        if(dabs(EigValues(Ieig)).ge.cutoff)then
          Ran=Ran+1
        else
          EigValues(Ieig)=ZERO
        end if
        if(EigValues(Ieig).lt.ZERO)Sig=Sig-1
        if(EigValues(Ieig).gt.ZERO)Sig=Sig+1
      end do ! Ieig
      write(CPRS,'(a,i1,a,i2,a)')'(',Ran,',',Sig,')'
!
! End of routine CP_classification
      call PRG_manager ('exit', 'CP_classification_QTAIM', 'UTILITY')
      return
      end subroutine CP_classification_QTAIM

      subroutine FIND_SP_QTAIM (xco,yco,zco,last,pm,fNNA,fSP,getout)
!*****************************************************************************
!     Date last modified: Spetember 29, 2015                                 *
!     Authors: P.L Warburton                                                 *
!     Description: Based on path oscillation, there may be a stationary point*
!*****************************************************************************
! Modules:
      USE program_files
      USE SGM_defaults
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE QTAIM_defaults 

      implicit none

!
! Local scalars:
      double precision :: tx,ty,tz,newlen,oldlen,lendiff,xco,yco,zco,mult,pm
      double precision :: TraceAB
      integer :: ohmy,last
      logical :: fNNA,fSP,getout
      double precision, dimension(:), allocatable  :: dAOprodX,dAOprodY,dAOprodZ
!
! Begin:
      call PRG_manager ('enter','FIND_SP_QTAIM', 'UTILITY')
!
      mult=res/4.0 
      ohmy=1
      oldlen=1.0d12
      newlen=-1.0d12
      fNNa=.false.
      fSP=.false.

! loop findSP
      findSP: do
       if(fNNA.or.fSP) exit
!       call AO_products (xco, yco, zco, AOprod, MATlen)  ! NOT used (RAP)
        call dAO_products(xco,yco,zco, dAOprodX, dAOprodY, dAOprodZ, MATlen)
        tx=TraceAB (PM0, dAOprodx, NBasis)
        ty=TraceAB (PM0, dAOprody, NBasis)
        tz=TraceAB (PM0, dAOprodz, NBasis)
        newlen=dsqrt(tx**2+ty**2+tz**2)
        lendiff=oldlen-newlen
         if(verbose.ge.3) then
          write(UNIout,'(a6,3(es10.3,1x))') 'dels: ',tx,ty,tz
          write(UNIout,'(a6,es10.3)') 'mult: ',mult
          write(UNIout,'(a8,1(es10.3,1x))') 'length: ',newlen
          write(UNIout,'(a6,i3)') 'ohmy: ',ohmy 
          write(UNIout,*)
          flush(6)  
         end if

        if(newlen.lt.1.0e-6) then
 ! now we've found a stationary point, what kind is it?
         call DEL2_DENSITY_QTAIM(xco,yco,zco)
         if(ran.eq.3.and.sig.eq.-3) then 
          fNNA=.true.
           else
          fSP=.true.
         end if
         exit findSP
        end if
        oldlen=newlen
        xco=xco+pm*mult*tx/newlen
        yco=yco+pm*mult*ty/newlen
        zco=zco+pm*mult*tz/newlen
        ohmy=ohmy+1
! if steepst ascent is not having success, the oscillation may be due to a different type
! of stationary point.  reset to the original mesh point and switch to steepest descent to find SP
        if(ohmy.eq.100) then
         if(verbose.ge.3) write(UNIout,'(a36)') 'Switching to steepest descent search'
         xco=xlo+(listx(last)-1)*res
         yco=ylo+(listy(last)-1)*res
         zco=zlo+(listz(last)-1)*res   
         mult=res/4.0 
         pm=-1.0
        end if
! if steepest ascent and descent searches have no success, the point is unassigned
! refinement should hopefully deal with this point later
        if (ohmy.gt.200) then
         getout=.true.
         exit
        end if
        if(verbose.ge.3) write(UNIout,'(a8,3(f10.6,1x))') 'now at: ',xco,yco,zco
        if((mod(ohmy,10).eq.0).or.(lendiff.lt.0.0)) then
         mult=mult/2.0
        end if
        end do findSP

      call PRG_manager ('exit', 'FIND_SP_QTAIM', 'UTILITY')
      return
      end subroutine FIND_SP_QTAIM

      subroutine FOUND_NNA_QTAIM (xco,yco,zco)
!********************************************************************
!     Date last modified: Spetember 29, 2015                        *
!     Authors: P.L Warburton                                        *
!     Description:Found a non-nuclear attractor                     *
!********************************************************************
! Modules:
      USE program_files
      USE SGM_defaults
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE QTAIM_defaults 

      implicit none

!
! Local scalars:
      double precision :: xco,yco,zco
      integer :: lx,ly,lz,atpoint
!
! Begin:
      call PRG_manager ('enter','FOUND_NNA_QTAIM', 'UTILITY')
!
      nSP=nSP+1
      lx=nint((xco-xlo)/res)+1              
      ly=nint((yco-ylo)/res)+1              
      lz=nint((zco-zlo)/res)+1
      atpoint=lz+zpoints*(ly-1)+zpoints*ypoints*(lx-1)
      sprank(nSP)=ran
      spsig(nSP)=sig
      spx(nSP)=xco
      spy(nSP)=yco
      spz(nSP)=zco
      spcode(nSP)='NNA '
        if(verbose.ge.3) then
         write(UNIout,'(a36,3(f10.6,1x))') 'There is a non-nuclear attractor at ' &
              ,spx(nSP),spy(nSP),spz(nSP)
         write(UNIout,'(a24,i1,1x,i2,1x,a4)') 'Rank, sig, and code are ' &
              ,sprank(nSP),spsig(nSP),spcode(nSP)
        end if
        if(verbose.ge.2) then
         write(*,*) 'There is a NNA located in/near mesh point ',lx,ly,lz  
         write(UNIout,'(1x,a23,3(f16.8))') 'The actual location is ',xco,yco,zco
        end if  
        nNNA=nNNA+1 
        basin(atpoint)=nextbasin
        NNAx(nNNA)=xco
        NNAy(nNNA)=yco
        NNAz(nNNA)=zco
        ptcode(atpoint)='NNA'
        assigned=assigned+1.0
        notass=notass-1.0
        ptsinbasin(basin(atpoint))=ptsinbasin(basin(atpoint))+1.0d0
! now we use a modified dodecahedron procedure to assign other points to the NNA basin
        call dodecahedron_NNA(xco,yco,zco,nextbasin)
        nextbasin=nextbasin+1
      call PRG_manager ('exit', 'FOUND_NNA_QTAIM', 'UTILITY')
      return
      end subroutine FOUND_NNA_QTAIM

      subroutine FOUND_SP_QTAIM (xco,yco,zco,again)
!********************************************************************
!     Date last modified: Spetember 29, 2015                        *
!     Authors: P.L Warburton                                        *
!     Description: Found a QTAIM stationary point                   *
!********************************************************************
! Modules:
      USE program_files
      USE SGM_defaults
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE QTAIM_defaults 

      implicit none

!
! Local scalars:
      double precision :: xco,yco,zco,spdist
      integer :: lx,ly,lz,chksp,atpoint
      logical :: again
!
! Begin:
      call PRG_manager ('enter','FOUND_SP_QTAIM', 'UTILITY')
!
! need to check if we found this stationary point before.  it's quite likely we will find
! a stationary point many times and we want to avoid duplication
      again=.false.
      do chksp=1,nSP
       spdist=dsqrt((xco-spx(chksp))**2+(yco-spy(chksp))**2+(zco-spz(chksp))**2)
        if(spdist.le.5.0d-4) then
         again=.true.
         call PRG_manager ('exit', 'FOUND_SP_QTAIM', 'UTILITY')
         return
        end if
      end do
      nSP=nSP+1
      lx=nint((xco-xlo)/res)+1              
      ly=nint((yco-ylo)/res)+1              
      lz=nint((zco-zlo)/res)+1
      atpoint=lz+zpoints*(ly-1)+zpoints*ypoints*(lx-1)
      sprank(nSP)=ran
      spsig(nSP)=sig
      spx(nSP)=xco
      spy(nSP)=yco
      spz(nSP)=zco
      if(ran.eq.0) spcode(nSP)='INF '
      if((ran.lt.3).and.(ran.gt.0)) spcode(nSP)='UNST'
      if(ran.eq.3) then
       if(sig.eq.-1) spcode(nSP)='BCP '
       if(sig.eq.1)  spcode(nSP)='RCP '
       if(sig.eq.3)  spcode(nSP)='CCP '
      end if         
      if(verbose.ge.2) then
       write(UNIout,'(a31,3(f10.6,1x))') 'There is a stationary point at ' &
             ,spx(nSP),spy(nSP),spz(nSP)
       write(UNIout,'(a24,i1,1x,i2,1x,a4)') 'Rank, sig, and code are ' &
             ,sprank(nSP),spsig(nSP),spcode(nSP)
      end if
      if(verbose.ge.2) then
       write(*,*) 'There is a SP located in/near mesh point ',lx,ly,lz  
       write(UNIout,'(1x,a23,3(f16.8))') 'The actual location is ',xco,yco,zco
      end if  
      ptcode(atpoint)='NOT'
      call PRG_manager ('exit', 'FOUND_SP_QTAIM', 'UTILITY')
      return

      end subroutine FOUND_SP_QTAIM

