      subroutine QTAIM_mesh_maker
!*****************************************************************************************************************
!     Date last modified: January 12, 2015                                                          Version 1.0  *
!     Author: P.L. Warburton                                                                                     *
!     Description: Determines the defualt mesh size for SGM applications                                         *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE SGM_defaults
      USE type_molecule
      USE QTAIM_defaults

      implicit none
!
! Local scalars:
      double precision :: tempres      
! Begin:
      call PRG_manager ('enter', 'QTAIM_MESH_MAKER', 'UTILITY')
! for initial mesh check we don't need a robust mesh.  resolution 0.5 should do
! store the resolution for reset after coarse mesh check

       tempres=res
       res=0.5

! determine the boxmargin on the coarse mesh
       if(verbose.eq.4) write(UNIout,('(a41,f6.3,a4)')) '*** MESH CHECK ON COARSE MESH RESOLUTION ',res,' ***'
       do
        if(verbose.eq.4) write(UNIout,('(a13,f6.3)')) 'Boxmargin is ', boxmargin
        if((.not.meshmade).or.(.not.boxmarginset)) call SGM_default_mesh
         if(meshmade) then
          if(proptype.eq.'DEN') call SGM_checkmesh_density

         if(.not.mesherror) then
          boxmarginset=.true.
          meshmade=.false.
          exit
         end if
        end if
       end do  
       if(verbose.eq.4) write(UNIout,('(a25,f6.3)')) 'Need a box margin around ',boxmargin-res
       meshcheck2=.true.

! now we go back to a lower boxmargin and determine boxmargin for fine mesh
       boxmarginset=.false.
       boxmargin=boxmargin-res
       res=tempres

! determine the boxmargin on the requested mesh resolution
       if(verbose.eq.4) then
        write(UNIout,('(a42,f6.3,a4)')) '*** MESH CHECK ON DESIRED MESH RESOLUTION ',res,' ***'
       end if
       do
        if(verbose.eq.4) write(UNIout,('(a13,f6.3)')) 'Boxmargin is ', boxmargin
        if((.not.meshmade).or.(.not.boxmarginset)) call SGM_default_mesh
        if(meshmade) then
          if(proptype.eq.'DEN') call SGM_checkmesh_density
          if(.not.mesherror) then
            if(verbose.ge.2) then
             write(UNIout,'(a34)') 'Mesh has been generated for QTAIM.'
             write(UNIout,('(a13,f6.3)')) 'Boxmargin is ', boxmargin
             write(UNIout,('(a15,3(i6,1x))')) 'Box dimensions ',xpoints,ypoints,zpoints
             write(UNIout,('(a15,3(f12.6,1x))')) 'Low box corner ',xlo,ylo,zlo
            end if

            boxmarginset=.true.
            meshmade=.true.
            meshpoints=xpoints*ypoints*zpoints
            allocate(property(xpoints,ypoints,zpoints))
            property(:,:,:)=0.0d0
            allocate(qgradx(meshpoints))
            qgradx(:)=0.0d0
            allocate(qgrady(meshpoints))
            qgrady(:)=0.0d0            
            allocate(qgradz(meshpoints))
            qgradz(:)=0.0d0            
            allocate(basin(meshpoints))
            basin(:)=0
            allocate(Irefine(meshpoints))
            Irefine(:)=0
            ptsref=0
            allocate(ptcode(meshpoints))
            ptcode(:)='NOT'
!            allocate(switch(meshpoints))
!            switch(:)=0
            notass=dble(meshpoints)
            meshallocated=.true.
           exit
          end if
        end if
       end do 

      call PRG_manager ('exit', 'QTAIM_MESH_MAKER', 'UTILITY')
     return

     end subroutine QTAIM_mesh_maker

      subroutine QTAIM_mesh_density
!*****************************************************************************************************************
!     Date last modified: September 15, 2015                                                        Version 1.0  *
!     Author: P.L. Warburton                                                                                     *
!     Description: Generates the density and second derivative eigenvalues on a mesh for the QTAIM basin analysis*
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE SGM_defaults
      USE type_molecule
      USE QM_defaults
      USE QM_objects
!     USE type_density
      USE QTAIM_defaults 

      implicit none
!
! Local scalars:
      integer :: x,y,z,Iatom,Znum,atpoint,loop,ohmy,lx,ly,lz
      double precision :: xco,yco,zco,XApt,YApt,ZApt,R,mult,oldlen,newlen,tx,ty,tz,con,lendiff
      double precision :: xloc,yloc,zloc
      double precision :: TraceAB
      logical :: fNNA,fSP 
      double precision, dimension(:), allocatable :: AOprod
      double precision, dimension(:), allocatable  :: dAOprodX,dAOprodY,dAOprodZ
!
! Local Array	  
 

! Begin:
      call PRG_manager ('enter', 'QTAIM_MESH_DENSITY', 'UTILITY')
      if(.not.allocated(AOprod)) allocate (AOprod(1:MATlen))
      if(.not.allocated(dAOprodx)) allocate (dAOprodx(1:MATlen))
      if(.not.allocated(dAOprody)) allocate (dAOprody(1:MATlen))
      if(.not.allocated(dAOprodz)) allocate (dAOprodz(1:MATlen))
      assigned=0.0
      reserved=1
! lowest box corner of generated mesh is mapped to local scalars
      xco=xlo-res
      yco=ylo-res
      zco=zlo-res
! calculate property and second derivative over mesh points
      if(verbose.ge.4) then
        write(Uniout,*) 'Xcoord   Ycoord    Zcoord    Density'
        write(UNIout,'(a12,f6.3,a14,3i4)') 'Resolution: ',res,' Mesh points: ',xpoints,ypoints,zpoints
      end if
      do x=1,xpoints
       xco=xco+res
       yco=ylo-res
       do y=1,ypoints
        yco=yco+res
        zco=zlo-res
        do z=1,zpoints
         zco=zco+res  
         atpoint=z+zpoints*(y-1)+zpoints*ypoints*(x-1)
!         write(*,*) 'Atpoint ',atpoint 
         if(verbose.ge.4)  write(UNIout,*) 'Point ',x,y,z  
         if(verbose.ge.4) write(UNIout,*) 'Coords ',xco,yco,zco
         do Iatom=1,Natoms
           Znum=CARTESIAN(Iatom)%Atomic_number
            if(Znum.le.0)cycle ! Skip dummy atoms
            if((x.eq.1).and.(y.eq.1).and.(z.eq.1)) reserved=reserved+1
            XApt=CARTESIAN(Iatom)%X
            YApt=CARTESIAN(Iatom)%Y
            ZApt=CARTESIAN(Iatom)%Z
            R=dsqrt((xco-XApt)**2+(yco-YApt)**2+(zco-ZApt)**2)
           if(R.le.radii(Iatom)) then
            basin(atpoint)=Iatom
            if(R.le.dsqrt(3.0*(res/2.0)**2)) then
              ptcode(atpoint)='NA '
            else
              ptcode(atpoint)='PRE'
            end if
            assigned=assigned+1.0
            notass=notass-1.0
            ptsinbasin(Iatom)=ptsinbasin(Iatom)+1.0d0
             if(verbose.ge.3) then
              write(UNIout,'(a6,3(i5),a22,i3)') 'Point ',x,y,z,' is assigned to basin ',Iatom
              write(*,*) 'Ptcode: ',ptcode(atpoint)
             endif
           end if
          end do

! now all the nuclear attractors are assigned basin numbers we will assign the reserved basin number

         call AO_products (xco,yco,zco, AOprod, MATlen)
         property(x,y,z)= TraceAB (PM0, AOprod, NBasis)

         call dAO_products(xco,yco,zco, dAOprodX, dAOprodY, dAOprodZ, MATlen)
         qgradx(atpoint)=TraceAB (PM0, dAOprodx, NBasis)
         qgrady(atpoint)=TraceAB (PM0, dAOprody, NBasis)
         qgradz(atpoint)=TraceAB (PM0, dAOprodz, NBasis)
                           
         if(verbose.ge.4) then
          write(*,*) 'atpoint ',atpoint
          write(*,*) 'grads: ',qgradx(atpoint),qgrady(atpoint),qgradz(atpoint)
         end if
        end do
       end do
      end do

      deallocate (AOprod)
      deallocate (dAOprodx)
      deallocate (dAOprody)
      deallocate (dAOprodz)
      if(verbose.ge.2) then
       write(*,*) 'Basin ',reserved,' is reserved for points on the zero flux surface'
       write(UNIout,'(a31,i8,a8,i8)') 'Points pre-assigned to basins: ',nint(assigned),' out of ',meshpoints
       do loop=1,Natoms
        write(*,*) 'Points preassigned to basin ',loop,' = ',ptsinbasin(loop)
       end do
       flush(6)
      end if

      call PRG_manager ('exit', 'QTAIM_MESH_DENSITY', 'UTILITY')
      return
      end subroutine QTAIM_mesh_density

