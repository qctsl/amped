      subroutine QTAIM_PRINT_manager (ObjName, Modality)
!********************************************************************************
!     Date last modified: April 24, 2008                           Version 2.0 *
!     Author: R.A. Poirier                                                      *
!     Description: Calls the appropriate routine to "Command" the object nameIN *
!     Command can be "PRINT" or "KILL"                                          *
!     Pwhat can be "OBJECT", "DOCUMENTATION" or "TABLE"                         *
!********************************************************************************
! Modules:
      USE program_files
      USE program_objects

      implicit none
!
! Input scalars:
      character*(*), intent(IN) :: ObjName,Modality
!
! Local scalars:
!
! Begin:
      call PRG_manager ('enter', 'QTAIM_PRINT_manager', 'UTILITY')

      SelectObject:   select case (ObjName)
      case ('QTAIM')
      case default
        write(UNIout,*)'QTAIM_PRINT_manager>'
        write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                       '" for Modality "',Modality(1:len_trim(Modality)),'"'
      end select SelectObject
!
! end of function QTAIM_PRINT_manager
      call PRG_manager ('exit', 'QTAIM_PRINT_manager', 'UTILITY')
      return
      end subroutine QTAIM_PRINT_manager
