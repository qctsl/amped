      subroutine NEED_REFINE_QTAIM 
!********************************************************************
!     Date last modified: Spetember 29, 2015                        *
!     Authors: P.L Warburton                                        *
!     Description: Determine the basin points to be refined         *
!********************************************************************
! Modules:
      USE program_files
      USE SGM_defaults
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE type_density
      USE QTAIM_defaults 

      implicit none

!
! Local scalars:
!      double precision :: xco,yco,zco,spdist
      integer :: x,y,z,lx,ly,lz,loop,atpoint,tempat,px,py,pz
      logical :: need
!
! Begin:
      call PRG_manager ('enter','NEED_REFINE_QTAIM', 'UTILITY')
      do x=1,xpoints
       do y=1,ypoints
        do z=1,zpoints
         atpoint=z+zpoints*(y-1)+zpoints*ypoints*(x-1)
         if(Irefine(atpoint).ne.0) then
! we'll remove the points already identified for refinement from the basin points totals
          ptsinbasin(basin(atpoint))=ptsinbasin(basin(atpoint))-1.0
          cycle
         end if
         need=.false.

! if the point has neighbors assigned to a different basin, then it should be refined  
! points on mesh corners and edges have less neighbors, so we cycle to avoid out of bounds
         do lx=(x-1),(x+1),1
          if((lx.lt.1).or.(lx.gt.xpoints)) cycle
          if(need) cycle
           do ly=(y-1),(y+1),1
            if((ly.lt.1).or.(ly.gt.ypoints)) cycle
            if(need) cycle
             do lz=(z-1),(z+1),1
              if((lz.lt.1).or.(lz.gt.zpoints)) cycle
              if(need) cycle
              tempat=lz+zpoints*(ly-1)+zpoints*ypoints*(lx-1)
              if(basin(tempat).ne.basin(atpoint)) need=.true.
             end do !lz
            end do !ly         
           end do !lx
!          write(*,*) 'Point ',x,y,z,'refinement set to ',need
          if(need) then
           Irefine(atpoint)=1
           ptsref=ptsref+1
! we'll also remove the points to be refined from the basin points totals
           ptsinbasin(basin(atpoint))=ptsinbasin(basin(atpoint))-1.0
         end if
        end do
       end do
      end do

      write(UNIout,'(a1)')
      write(UNIout,'(a7,i10,a8,i10,a20)') 'Of the ',meshpoints,' points ',ptsref,' need to be refined.'
      write(UNIout,'(a1)')
      write(UNIout,'(a49)') 'Points now assigned to basins before refinement: '
      do loop=1,nextbasin-1
       write(UNIout,'(1x,a16,i5,a3,i8)') 'Points in basin ',loop,' = ',nint(ptsinbasin(loop))
      end do
       write(UNIout,'(1x,a24,i8)') 'Points unassigned     = ',nint(notass)
       write(UNIout,'(1x,a24,i8)') 'Points to refine      = ',ptsref
       write(UNIout,*)
! if desired list of all points flagged for future refinement
      if(verbose.ge.3) then
       write(UNIout,*)
       write(UNIout,'(a44)') 'Full list of points for further refinement: ' 
       do loop=1,meshpoints
        if(Irefine(loop).gt.0) then
         pz=mod(loop,zpoints)
         if(pz.eq.0) pz=zpoints
         px=floor(dble(((loop-1)/(ypoints*zpoints))+1))
         py=1+((loop-ypoints*zpoints*(px-1)-pz)/zpoints)
         write(UNIout,'(1x,a6,i10,a6,3(i4,1x))') 'Point ',loop,' is at ',px,py,pz
        end if
       end do
      end if
      
      call PRG_manager ('exit', 'NEED_REFINE_QTAIM', 'UTILITY')
      return

      end subroutine NEED_REFINE_QTAIM

