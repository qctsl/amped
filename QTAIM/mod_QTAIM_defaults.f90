      module QTAIM_defaults
!***********************************************************************
!     Date last modified: August 12, 2015                  Version 1.0 *
!     Author: P.L. Warburton                                           *
!     Description: Defaults for the QTAIM approach                     *
!***********************************************************************
      USE program_parser

      implicit none
      integer :: reflevel,verbose,stage,nNNA,nSP,pathlist,ran,sig,meshpoints,reserved
      integer :: nextbasin,ptsref
      double precision :: tol,qxplot,qyplot,qzplot,notass,assigned,refres
!      double precision ::   
      logical :: qplot,qpden,qprad,qpx,qpy,qpz


! Arrays
      integer, allocatable, dimension(:) :: sprank,spsig,basin,Irefine,listx,listy,listz 
      integer, allocatable, dimension(:) :: basins
      double precision, allocatable, dimension(:) :: spx,spy,spz,qgradx,qgrady,qgradz
      double precision, allocatable, dimension(:) :: ptsinbasin,NNAx,NNAy,NNAz
      double precision, allocatable, dimension(:,:) :: refden, refdelx, refdely, refdelz
      double precision, allocatable, dimension(:) :: radii
      integer, allocatable, dimension(:) :: refine
      logical, allocatable, dimension(:) :: torefine
      double precision, allocatable, dimension(:,:) :: dels
      double precision, allocatable, dimension(:,:) :: tempdel
      integer, allocatable, dimension(:) :: midprop
      integer, allocatable, dimension(:) :: tightprop
      double precision, allocatable, dimension(:) :: basinvol
!      integer, allocatable, dimension(:,:,:) ::   
!      integer, allocatable, dimension(:,:) ::   
!      double precision, allocatable, dimension(:,:,:) ::   
!      logical, allocatable, dimension(:) :: 
!      character*3, allocatable, dimension(:,:,:) :: 
      character*3, allocatable, dimension(:) :: spcode,ptcode
!      character*4, allocatable, dimension(:) :: 


      end module QTAIM_defaults
