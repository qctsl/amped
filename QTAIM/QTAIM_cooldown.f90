      subroutine QTAIM_cooldown
!*****************************************************************************************************************
!     Date last modified: October 26, 2015                                                          Version 1.0  *
!     Author: P.L. Warburton                                                                                     *
!     Description: Deallocates arrays used in QTAIM jobs                                                         *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE QTAIM_defaults

      implicit none
      call PRG_manager ('exit', 'QTAIM_COOLDOWN', 'UTILITY')
      if(allocated(qgradz)) deallocate(qgradz)
      if(allocated(qgrady)) deallocate(qgrady)
      if(allocated(qgradx)) deallocate(qgradx)
      if(allocated(Irefine)) deallocate(Irefine)
      if(allocated(basin)) deallocate(basin)
      if(allocated(ptcode)) deallocate(ptcode)
      if(allocated(spx)) deallocate(spx)
      if(allocated(spy)) deallocate(spy)
      if(allocated(spz)) deallocate(spz)
      if(allocated(sprank)) deallocate(sprank)
      if(allocated(spsig)) deallocate(spsig)
      if(allocated(spcode)) deallocate(spcode)
      if(allocated(listx)) deallocate(listx)
      if(allocated(listy)) deallocate(listy)
      if(allocated(listz)) deallocate(listz)
      if(allocated(NNAx)) deallocate(NNAx)
      if(allocated(NNAy)) deallocate(NNAy)
      if(allocated(NNAz)) deallocate(NNAz)
      if(allocated(refdelz)) deallocate(refdelz)
      if(allocated(refdely)) deallocate(refdely)
      if(allocated(refdelx)) deallocate(refdelx)
      if(allocated(refden)) deallocate(refden)
      call PRG_manager ('exit', 'QTAIM_COOLDOWN', 'UTILITY')
      return
      end subroutine QTAIM_COOLDOWN
