      subroutine QTAIM_basin_dev
!*****************************************************************************************************************
!     Date last modified: August 12, 2015                                                           Version 1.0  *
!     Author: P.L. Warburton                                                                                     *
!     Description: Determines the QTAIM basins for a molecule based on J. Phys.: Condens. Matter 21 084204 (2009)*
!                  This procedure uses analytical density gradients instead of the numerical gradients           *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE SGM_defaults
      USE type_molecule
      USE QM_defaults
      USE QM_objects
!     USE type_density
      USE QTAIM_defaults

      implicit none
! Local scalars:
      integer :: Iatom,xloop,yloop,zloop
      double precision :: rstep,maxden,minden,r,phi,XApt,YApt,ZApt  
      double precision :: x,y,z,den,ratio,length,invphi
      double precision :: TraceAB
      logical :: sphere  
! Local Array	  
      double precision, dimension(:), allocatable :: AOprod
!
! Begin:
      call PRG_manager ('enter', 'QTAIM_BASIN_DEV', 'UTILITY')
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)
      allocate(AOprod(1:MATLEN))

! as we assign basins we will find some but NOT NECESSARILY ALL stationary points
! we will store info on locations, rank, signature and type of stationary points
! we will assume there are no more than 4*Natoms stationary points. increase this
! value if you run out of allocated memory for these arrays

      allocate(spx(Natoms*4))
      allocate(spy(Natoms*4))
      allocate(spz(Natoms*4))
      allocate(sprank(Natoms*4))
      allocate(spsig(Natoms*4))
      allocate(spcode(Natoms*4))

      spx(:)=-99999999.0
      spy(:)=-99999999.0
      spz(:)=-99999999.0
      sprank(:)=-99
      spsig(:)=-99
      spcode(:)='NOT'

! preassign basin points based on dodecahedral point sampling around nuclei
      call dodecahedron

! make the mesh, calculate density and gradient and preassign basins
      call QTAIM_mesh_maker
      call QTAIM_mesh_density
! Need to store points along path from point of interest to a preassigned basin
! point.  Assume that there are at most 'pathlist' such points.  This value is set 
! in QTAIM_basin_setup based on basin properties that are requested.  Any out-of array bounds
! errors mean this may need to be increased
      allocate(listx(pathlist))
      allocate(listy(pathlist))
      allocate(listz(pathlist))
! in the case of NNA we need to be able to add new basin IDs. the first of these will be Natoms+1
! we want to keep track of the Cartesian positions of NNAs, these are NNAx,NNAy,NNAz  
! also, we will assume the number of NNAs will be at most equal to the number of atoms
      nextbasin=Natoms+1
      allocate(NNAx(Natoms))
      allocate(NNAy(Natoms))
      allocate(NNAz(Natoms))
      NNAx(:)=0.0d0
      NNAy(:)=0.0d0
      NNAz(:)=0.0d0
! roughly assign basins
      call QTAIM_basin_assign  
! while we have identified points for refinement, there could be more.  let's find those
      call NEED_REFINE_QTAIM
! the mesh points needing refinement are close to zero-flux surfaces, we will divide each 
! of these points into a 3x3 box. we need the density and gradients at these points
       allocate(refden(ptsref,27))
       allocate(refdelx(ptsref,27))
       allocate(refdely(ptsref,27))
       allocate(refdelz(ptsref,27))
       refden(:,:)=0.0d0
       refdelx(:,:)=0.0d0
       refdely(:,:)=0.0d0
       refdelz(:,:)=0.0d0
!       call QTAIM_refine_density
!      call QTAIM_basin_refine

      call QTAIM_cooldown

      call PRG_manager ('exit', 'QTAIM_BASIN_DEV', 'UTILITY')
      return
      end subroutine QTAIM_basin_dev

      subroutine QTAIM_basin_assign
!*****************************************************************************************************************
!     Date last modified: September 15, 2015                                                        Version 1.0  *
!     Author: P.L. Warburton                                                                                     *
!     Description: Determines the QTAIM basins for a molecule based on J. Phys.: Condens. Matter 21 084204 (2009)*
!                  This procedure uses analytical density gradients instead of the numerical gradients           *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE SGM_defaults
      USE type_molecule
      USE QM_defaults
      USE QTAIM_defaults

      implicit none
!
! Local scalars:
      double precision :: tempres
! Local Array	  

! Begin:
      call PRG_manager ('enter', 'QTAIM_basin_assign', 'UTILITY')
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)

! Default
      proptype='DEN'
      if (res.eq.0.0) res=0.20
      boundlo=-5.0
      boxmargin=2.5
!      meshcheck2=.false.

! make the mesh using the SGM subroutine for mesh creation
      call SGM_mesh_maker
      meshpoints=xpoints*ypoints*zpoints
      allocate(basins(meshpoints))
      basins(:)=0
      allocate(refine(meshpoints))
      refine(:)=99
      allocate(torefine(meshpoints))
      torefine(:)=.false.
      allocate(dels(meshpoints,3))
      dels(:,:)=0.0d0
      allocate(tempdel(343,3))
      tempdel(:,:)=0.0d0
      allocate(midprop(27))
      midprop(:)=0.0d0
      allocate(tightprop(343))
      tightprop(:)=0.0d0
! assuming a maximum number of NNAs of 10 the number of basins should
! be at most number of atoms 10
      allocate(basinvol(Natoms+10))
      basinvol(:)=0.0d0

! generate the electron density on the mesh 
      call QTAIM_mesh_density

      
! these domains were allocated in the mesh generation subroutine
       if(allocated(property)) deallocate(property)


      call PRG_manager ('exit', 'QTAIM_basin_assign', 'UTILITY')
      return
      end subroutine QTAIM_basin_assign


      subroutine QTAIM_basin_point(x,y,z)
!*****************************************************************************************************************
!     Date last modified: September 15, 2015                                                        Version 1.0  *
!     Author: P.L. Warburton                                                                                     *
!     Description: Determines the QTAIM basins for a molecule based on J. Phys.: Condens. Matter 21 084204 (2009)*
!                  This procedure uses analytical density gradients instead of the numerical gradients           *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE SGM_defaults
      USE type_molecule
      USE QM_defaults
      USE QM_objects
!     USE type_density
      USE QTAIM_defaults 

      implicit none
!
! Local scalars:
      integer :: x,y,z,atpoint,inpath,tempx,tempy,tempz,repeat,check,last,tempat,tb,px,py,pz
      integer :: gridx,gridy,gridz,corrx,corry,corrz,btempx,btempy,btempz,btempat,loop
      double precision :: xco,yco,zco,pm,con,rgradx,rgrady,rgradz,rgridx,rgridy,rgridz
      double precision :: delrx,delry,delrz
!      double precision :: spdist
      logical :: terminus,getout,isit,again,toomany,fNNA,fSP
!     double precision, dimension(:), allocatable :: AOprod

! Begin:
      call PRG_manager ('enter', 'QTAIM_basin_point', 'UTILITY')

         atpoint=z+zpoints*(y-1)+zpoints*ypoints*(x-1)
         if(verbose.ge.3) then
           write(*,*) 'Point ',x,y,z
           write(*,*) 'Atpoint ',atpoint
           flush(6)
         end if
! point is not already assigned
          inpath=1  
          terminus=.false. 
          delrx=0.0d0
          delry=0.0d0
          delrz=0.0d0
          listx(:)=0
          listy(:)=0
          listz(:)=0
          tempx=x
          tempy=y
          tempz=z
          isit=.false.
          again=.false.
! loop follow
           follow: do
            getout=.false.
            if(terminus) exit
            if(inpath.eq.1) then
             listx(inpath)=x
             listy(inpath)=y
             listz(inpath)=z
             tempat=z+zpoints*(y-1)+zpoints*ypoints*(x-1)
            end if
            inpath=inpath+1

! if inpath is equal to pathlist, we likely have a NNA or other stationary point 
! present or mesh sufficiently fine so that pathlist needs to be increased
            if((inpath-1).eq.pathlist) then

! check the inpath points for evidence of oscillation between points possibly indicating a 
! charge density stationary point exists
! with the correction step in the algorithm, it is possible a point can be visted on the
! path twice before the path "moves on".  Here we check the last point of the path and count 
! how many times it shows up.  If the count reaches 3 there is oscillation of points in the path
! We will then determine the actual posiion of the stationary point and what type it is
! if it is a non-nuclear attractor we will assign it to a new basin. if it's something else
! we will keep track of what kind of stationary point it is but assign no points on the path
! with the exception of the first, which will be assigned in the refinement stages  
            toomany=.false.
            repeat=1
            check=pathlist-1
!            last=check
! loop oscillate
            oscilate: do
            if(toomany) exit

             if(verbose.ge.3) then
              write(*,*) 'SP: ', pathlist, check, repeat
              write(*,*) 'Term: ',listx(pathlist),listy(pathlist),listz(pathlist)
              write(*,*) 'In: ',listx(check),listy(check),listz(check)
              write(*,*) 'Last: ',last
             end if
             if(listx(pathlist).eq.listx(check)) then
              if(listy(pathlist).eq.listy(check)) then
               if(listz(pathlist).eq.listz(check)) then
                 repeat=repeat+1
                 last=check
               end if
              end if
             end if
             if(repeat.eq.3) then
              toomany=.true.
              inpath=last

! find the actual location of the SP
              call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)
! NOT used (RAP)
!             if(allocated(AOprod)) deallocate(AOprod)
!             if(allocated(dAOprodx)) deallocate(dAOprodx)
!             if(allocated(dAOprody)) deallocate(dAOprody)
!             if(allocated(dAOprodz)) deallocate(dAOprodz)
!             allocate (AOprod(1:MATlen))
!             allocate (dAOprodx(1:MATlen))
!             allocate (dAOprody(1:MATlen))
!             allocate (dAOprodz(1:MATlen))
              xco=xlo+(listx(last)-1)*res
              yco=ylo+(listy(last)-1)*res
              zco=zlo+(listz(last)-1)*res   
              if(verbose.ge.2)  write(UNIout,'(a18,3(f10.6,1x))') 'SP search around: ',xco,yco,zco
              flush(6)
              fNNa=.false.
              fSP=.false.
              pm=1.0

              call FIND_SP_QTAIM (xco,yco,zco,last,pm,fNNA,fSP,getout)
              if(getout) then
               if(basin(atpoint).ne.0) notass=notass+1
               basin(atpoint)=0
               ptcode(atpoint)='NOT'
               isit=.false.
               exit follow 
              end if

! identify the mesh point that contains the NNA
               if(fNNA) then
                call FOUND_NNA_QTAIM (xco,yco,zco)
                exit follow
               end if ! fNNA

! if the mesh point contains a stationary point we want to keep track of it, but we cannot
! assign the points of the path to a basin based on what's happened here. we will deal with
! the starting point and ending point in the refinement stages
               if(fSP) then
! need to check if we found this stationary point before.  it's quite likely we will find
! a stationary point many times and we want to avoid duplication
                call FOUND_SP_QTAIM (xco,yco,zco,again)
                if((verbose.ge.2).and.again) then
                 write(UNIout,'(a26,3(f10.6,1x),a9)') 'Found stationary point at ',xco,yco,zco,' already!'
                 flush(6)
                end if
! found the true end of the path
                exit follow
               end if ! fSP  

             end if ! repeat was equal to 3
             check=check-1
             if(check.eq.0) toomany=.true.
            end do oscilate
         
           end if ! (inpath-1 was equal to pathlen)

!            write(*,*) 'Inpath during loop: ',inpath
! calculate the constant c
! need to avoid divide by zero errors
            con=1.0d30
            if(qgradx(atpoint).ne.0.0) then
             con=dmin1(con,res/dabs(qgradx(tempat)))    
            end if
            if(qgrady(atpoint).ne.0.0) then
             con=dmin1(con,res/dabs(qgrady(tempat)))    
            end if
            if(qgradz(atpoint).ne.0.0) then
             con=dmin1(con,res/dabs(qgradz(tempat)))    
            end if
! generate the rgrad vector
            rgradx=con*qgradx(tempat)             
            rgrady=con*qgrady(tempat)             
            rgradz=con*qgradz(tempat)             
            if(verbose.ge.4)  write(*,*)'rgrad',rgradx,rgrady,rgradz

! generate the rgrid vector
            gridx=tempx+nint(rgradx/res)
            gridy=tempy+nint(rgrady/res)
            gridz=tempz+nint(rgradz/res)
            if(verbose.ge.4)  write(*,*)'grid',gridx,gridy,gridz

            rgridx=(dble(gridx)-dble(tempx))*res
            rgridy=(dble(gridy)-dble(tempy))*res
            rgridz=(dble(gridz)-dble(tempz))*res
            if(verbose.ge.4)  write(*,*)'rgrid',rgridx,rgridy,rgridz
            
! generate the delr vector
            delrx=delrx-rgridx+rgradx
            delry=delry-rgridy+rgrady
            delrz=delrz-rgridz+rgradz
            if(verbose.ge.4)  write(*,*)'delr',delrx,delry,delrz
! determine if correction step is required
            corrx=nint(delrx/res)
            corry=nint(delry/res)
            corrz=nint(delrz/res)
            if(verbose.ge.4)  write(*,*)'corr',corrx,corry,corrz

! find next point on path and adjust delr if corrections are applied
            tempx=gridx+corrx
            tempy=gridy+corry
            tempz=gridz+corrz         
            tempat=tempz+zpoints*(tempy-1)+zpoints*ypoints*(tempx-1)
            delrx=delrx-dble(corrx)*res
            delry=delry-dble(corry)*res
            delrz=delrz-dble(corrz)*res  
            if(verbose.ge.4)  then       
             write(*,*) 'new x,y,z: ',tempx,tempy,tempz
             write(*,*) 'new atpoint ',tempat
             write(*,*)'delr',delrx,delry,delrz
             write(*,*) 'basin:', basin(tempat)
             write(*,*)
            end if
            listx(inpath)=tempx
            listy(inpath)=tempy
            listz(inpath)=tempz
! reached a charge density maximum (NA or NNA)
            if((ptcode(tempat).eq.'NA ').or.(ptcode(tempat).eq.'NNA').or.(ptcode(tempat).eq.'ANN') &
             .or.(ptcode(tempat).eq.'PRE')) then
             if(verbose.ge.3)  then
              write(*,*)'terminus',qgradx(tempat),qgrady(tempat),qgradz(tempat)
             end if
             terminus=.true.
             isit=.true.
             exit follow
            end if
           end do follow

           if(verbose.ge.3)  then  
            write(*,*) 'Origin:   ',x,y,z
            write(*,*) 'Terminus: ',tempx,tempy,tempz
            write(*,*) 'Inpath:   ',inpath
           end if
           tb=basin(tempat)
 
! assign all previously unassigned points of the path to the basin of the terminus point
! we'll also keep track of points on the path previously assigned to a different basin
! while we won't change the assignment of these, we will mark these for later refinement
! since it's likely they are near a zero flux surface.
         if(isit) then
          do loop=1,inpath
           btempx=listx(loop)
           btempy=listy(loop)
           btempz=listz(loop)
           btempat=btempz+zpoints*(btempy-1)+zpoints*ypoints*(btempx-1)
            if(verbose.ge.3)  write(*,*) 'InPoint:   ',listx(loop),listy(loop),listz(loop),basin(btempat)        
            if(basin(btempat).eq.0) then
             assigned=assigned+1.0
             notass=notass-1.0
             basin(btempat)=tb
             ptcode(btempat)='ASS' 
             ptsinbasin(tb)=ptsinbasin(tb)+1.0d0
             Irefine(btempat)=1
             ptsref=ptsref+1
            end if  
            if(verbose.ge.3)  then
             write(*,*) 'OutPoint:   ',listx(loop),listy(loop),listz(loop),basin(btempat)        
             write(*,*)
            end if
          end do
         end if
      if(verbose.ge.1) then
       write(UNIout,*)
       write(UNIout,'(a50)') 'Further assignment of all points to basins gives: ' 
       if(nint(assigned+notass).ne.meshpoints) then
        write(UNIout,'(a31,i8,a8,i8)') 'Points now assigned to basins: ',nint(assigned),' out of ',meshpoints
        write(UNIout,'(a31,i8,a8,i8)') 'Points not assigned to basins: ',nint(notass),' out of ',meshpoints
        call PRG_stop('Count error between mesh points and assigned points')
       end if
       do loop=1,nextbasin-1
        write(UNIout,'(1x,a16,i5,a3,i10)') 'Points in basin ',loop,' = ',nint(ptsinbasin(loop))
       end do
        write(UNIout,'(1x,a24,i10)') 'Points unassigned     = ',nint(notass)
      end if
 

         if(verbose.ge.3)  write(*,*) 'Assigned:     ',assigned
         if(verbose.ge.3)  write(*,*) 'Not assigned: ',notass
         if(verbose.ge.3)  write(*,*) 'Will reassess: ',ptsref
         tb=basin(atpoint)
         if(verbose.ge.3) then  
          do loop=1,(nextbasin-1)
           write(*,*) 'points in basin ',loop,' are ',ptsinbasin(loop)
          end do
          write(*,*)
          end if 

      call PRG_manager ('exit', 'QTAIM_basin_point', 'UTILITY')
      return
      end subroutine QTAIM_basin_point
