#!/usr/bin/env python
"""
generate_test_report.py

Tool to parse XUNIT xml-files (e.g. created by nosetests --with-xunit) and print test-results in a nice table.
Generation of report as HTML file will be added later


Created on 2013-04-02

@author: "Oliver Stueker (ostueker)"

@organization:  Research Group of R.A. Poirier
                Department of Chemistry
                Memorial University of Newfoundland
                St. John's, NL
                CANADA
"""

import os
import re
import codecs
import argparse
from xml.etree import ElementTree
from datetime import timedelta, datetime

# global vars:
verbose = 0

class TestCase:
    """Data-structure  holding information about a MUNgauss test case

    the TestCase() constructor takes 2 arguments:
        name : name of the test (string)
        time : time (in sec) it took to execute the test (float)

    The following attributes can be set directly:
        status: (string) is either:
            OK     test passed (output is as expected)
            Failed test failed (output differs or MUNgauss terminated with error)
            Error  other error (e.g. no sample output file to compare to)

        error:   (string) a more detailed error-message
        diffFile:(string) if output differs: name of the diff-file, otherwise None
    """
    def __init__( self, name="", time=0.0 ):
        """TestCase() constructor

        Takes 2 arguments:
            name : name of the test (string)
            time : time (in sec) it took to execute the test (float)
        """
        self.name = name
        self.time = time
        self.status = None
        self.error = None
        self.diffFile = None
        self.lisFile = None

    def __repr__( self ):
        args = {'name': self.name,
                'time': self.time,
                'status': self.status,
                'msg': self.error,
                }
        return "%(name)-40s %(time)8.3f %(status)-6s %(msg)-50s" % args

def readXunitFile( fileName="" ):
    "parses a Xunit formatted XML file and returns a list of TestCase objects"

    tests = []  # list if TestCase objects

    try:
        document = ElementTree.parse( fileName )  # parse XUnit file
    except ElementTree.ParseError as e:
        print "Warning: " + fileName + ": " + str( e.msg )
        return []

    try:
        document.find( 'testsuite' )
    except ElementTree.ParseError as e:
        print "Warning: " + fileName + ": " + str( e.msg )
        return []

    # some Regular expressions
    testname_re = re.compile( r".+/([^/]+/[^/]+)',\)" )
    captured_stdout_re = re.compile( r"-+ >> begin captured stdout << -+\n*(.+[^\n])\n+-+ >> end captured stdout << -+", re.DOTALL )
    no_input_file_re = re.compile( r"No such file or directory: '.+\.dat'", re.DOTALL )

    # extract Data from ElementTree (all <testcase ... > tags)
    for testcase in document.findall( 'testcase' ):
        # get test's name:
        testName = testname_re.match( testcase.attrib['name'] )
        if testName:
            testName = testName.group( 1 )
        else:
            testName = testcase.attrib['name']

        # get test's runtime
        test = TestCase( name=testName, time=float( testcase.attrib['time'] ) )

        # if there was an error, <testcase ... > has <error ..> as child
        children = testcase.getchildren()
        if len( children ) > 0:
            # determine status:
            if 'TestFailed' in children[0].attrib['type']:
                test.status = 'Failed'
            elif 'TestError' in children[0].attrib['type']:
                test.status = 'Error'
            elif 'IOError' in children[0].attrib['type']:
                test.status = 'Error'
            elif 'SkipTest' in children[0].attrib['type']:
                test.status = 'Skipped'
            else:
                test.status = children[0].attrib['type']

            # determine error-message:
            if 'output differs from reference-data' in children[0].attrib['message']:
                # Output differs from template
                test.error = 'DIFF: output differs from reference-data'
            elif 'no reference file' in children[0].attrib['message']:
                # have input file but no reference data
                test.error = 'OUT: no reference file'
            elif no_input_file_re.search( children[0].attrib['message'] ):
                # no input file
                test.error = 'DAT: no input file'
            if captured_stdout_re.search( children[0].attrib['message'] ):
                if test.error:
                    test.error += "\n"
                else:
                    test.error = ''
                test.error += captured_stdout_re.search( children[0].attrib['message'] ).group( 1 )
            if not test.error:
                test.error = children[0].attrib['message']

            if os.path.isfile( test.name + ".lis" ):
                test.lisFile = test.name + ".lis"
            if os.path.isfile( test.name + ".diff" ):
                test.diffFile = test.name + ".diff"

        else:  # no error, all OK
            test.status = "OK"
            test.error = None
        # append to test
        tests.append( test )
    return tests

def testStats( testcases={} ):
    """takes a dict of TestCase objects and generates statistics of errors, passed, failed, skipped tests and runtime.
    """
    stats = {
        "nOK":0,
        "nFailed":0,
        "nError":0,
        "nSkipped": 0,
        "nTotal": 0,
        "totalTime": 0.0,
        }
    for testName in testcases.keys():
        test = testcases[ testName ]
        if test.status == "OK":
            stats['nOK'] += 1
        elif test.status == "Failed":
            stats['nFailed'] += 1
        elif test.status == "Error":
            stats['nError'] += 1
        elif test.status == "Skipped":
            stats['nSkipped'] += 1
        stats['nTotal'] += 1
        stats['totalTime'] += test.time
    time = datetime( 1, 1, 1 ) + timedelta( seconds=stats["totalTime"] )
    stats['timeString'] = "%dd %dh %dm %ds" % ( time.day - 1, time.hour, time.minute, time.second )
    return stats

def genHtmlReport( testcases=[], title="MUNgauss tests", subtitle=None, filename=None , stats=None ):
    """takes a list of TestCase objects and generates a HTML report.
    If no filename is set, the report is returned as a String."""

    if stats == None:
        stats = testStats( testcases )

    htmldata = []
    htmldata.append( u'<!doctype html>' )
    htmldata.append( u'<html>\n<head>' )
    htmldata.append( u'  <meta content="text/html; charset=UTF-8" http-equiv="content-type">' )
    htmldata.append( u'  <title>%s</title>' % title )
    htmldata.append( u'  <style type="text/css">' )
    htmldata.append( u'''    /* Style for report of MUNgauss unit-tests */
    table                  { font-size: small;}
    /* format every second row with grey background */
    tr:nth-of-type(2n)     { background-color: #eee; }
    /* column 2 (timing) align right */
    td:nth-of-type(2)      { text-align: right; }
    /* column 3 (status) align  center */
    td:nth-of-type(3)      { text-align: center; }
    td.red                 { color: red; }
    td.green               { color: green; }
    td.orange              { color: orange; }
    .hidden                { display: none; }
    input.toggle           { float: right; }''' )
    htmldata.append( u'  </style>' )
    htmldata.append( u'</head>' )
    htmldata.append( u'<body>' )
    htmldata.append( u'<h1>%s</h1>' % title )
    if subtitle:
        htmldata.append( u'<h3>%s</h3>' % subtitle )
    htmldata.append( u'<div id="forButton"></div>' )
    htmldata.append( u'Found %(nTotal)i tests: %(nOK)i passed, %(nFailed)i failed, %(nError)i Errors, %(nSkipped)i skipped.<br />\n' % stats )
    htmldata.append( u'Runtime: %(timeString)s.<br />' % stats )
    htmldata.append( u'<table>' )
    htmldata.append( u'  <thead>' )
    htmldata.append( u'    <th>Name</th>' )
    htmldata.append( u'    <th>timing</th>' )
    htmldata.append( u'    <th>Status</th>' )
    htmldata.append( u'    <th>Message</th>' )
    htmldata.append( u'    <th>Diff</th>' )
    htmldata.append( u'    <th>output file</th>' )
    htmldata.append( u'  </thead>' )

    testcase = TestCase()
#    for testcase in testcases:
    for testName in sorted( testcases ):
        testcase = tests[testName]

        args = {'name':     testcase.name,
                'timing':   testcase.time,
                'status':   testcase.status,
                'message':  testcase.error.replace( u"\n", u"<br />" ) if testcase.error else u"&nbsp;",
                'diff':     testcase.diffFile,
                'lis':      testcase.lisFile,
                }

        if testcase.status == "OK":
            args['class'] = u"green"
        elif testcase.status == "Skipped":
            args['class'] = u"orange"
        else:
            args['class'] = u"red"

        htmldata.append( u'  <tr>' )
        htmldata.append( u'    <td>%(name)s</td>' % args )
        htmldata.append( u'    <td>%(timing)5.3f</td>' % args )
        htmldata.append( u'    <td class="%(class)s">%(status)s</td>' % args )
        htmldata.append( u'    <td>%(message)s</td>' % args )
        if testcase.diffFile:
            htmldata.append( u'    <td><a href="%(diff)s">diff</a> <a href="%(diff)s.html">html</a></td>' % args )
        else:
            htmldata.append( u'    <td>&nbsp;</td>' )
        if testcase.lisFile:
            htmldata.append( u'    <td><a href="%(lis)s">LIS file</a></td>' % args )
        else:
            htmldata.append( u'    <td>&nbsp;</td>' )
        htmldata.append( u'  </tr>' )


    htmldata.append( u'</table>' )
    htmldata.append( u'<script src="http://code.jquery.com/jquery-1.9.1.min.js"  ></script>' )
    htmldata.append( u'<script src="http://code.jquery.com/jquery-migrate-1.1.1.min.js" ></script>' )
    htmldata.append( u'''<script>
    var showPassed=true;
    var showSkipped=true;
    var showFailed=true;
    if( window.location.search.substring(1).search("hid") > -1) {
        toggleShowPassed();
        toggleShowSkipped();
    }
    if( window.location.search.substring(1).search("p=h") > -1) {
        $(".green").parent().addClass("hidden");
        showPassed = false;
    }
    if( window.location.search.substring(1).search("s=h") > -1) {
        $(".orange").parent().addClass("hidden");
        showSkipped = false;
    }
    if( window.location.search.substring(1).search("f=h") > -1) {
        $(".red").parent().addClass("hidden");
        showFailed = false;
    }
    function toggleShowPassed(){
        if (showPassed){
            $(".green").parent().addClass("hidden");
            showPassed = false;
        }
        else {
            $(".green").parent().removeClass("hidden");
            showPassed = true;
        }
    };
    function toggleShowSkipped(){
        if (showSkipped){
            $(".orange").parent().addClass("hidden");
            showSkipped = false;
        }
        else {
            $(".orange").parent().removeClass("hidden");
            showSkipped = true;
        }
    };
    function toggleShowFailed(){
        if (showFailed){
            $(".red").parent().addClass("hidden");
            showFailed = false;
        }
        else {
            $(".red").parent().removeClass("hidden");
            showFailed = true;
        }
    };

    $("div#forButton").append(
        '<input type="button" value="hide/show passed" class="toggle" id="togglePassed" onclick="toggleShowPassed()"/>'
    );
    $("div#forButton").append(
        '<input type="button" value="hide/show skipped" class="toggle" id="toggleSkipped" onclick="toggleShowSkipped()"/>'
    );
    $("div#forButton").append(
        '<input type="button" value="hide/show failed" class="toggle" id="toggleFailed" onclick="toggleShowFailed()"/>'
    );
    </script>''' )
    htmldata.append( u'</body>' )
    htmldata.append( u'</html>' )
    if filename == None:
        return u"\n".join( htmldata )

if __name__ == "__main__":
    __description = """Parse XUNIT xml-files (created by nosetests --with-xunit)\nand print test-results in a nice table."""
    __epilog = """
    If a second set of test results is supplied with the --set2 option,
    no HTML report will be created, instead only tests whose status
    differs between the sets are printed to STDOUT (with -v all tests
    with their status in both sets will be printed).

Note: When using the --set2 option the name(s) of the primary test
      results can't be given directly after the name(s) of the secondary
      test result.
    * `./%(prog)s     set1/*.xml  -2 set2/*.xml`   works
    * `./%(prog)s  -2 set2/*.xml  -v set1/*.xml`   works
    * `./%(prog)s  -2 set2/*.xml  -- set1/*.xml`   works
    * `./%(prog)s  -2 set2/*.xml     set1/*.xml`   fails!!
      (set1/*.xml will be considered as argument of --set2)
    """
    parser = argparse.ArgumentParser( description=__description, epilog=__epilog,
                                      formatter_class=argparse.RawDescriptionHelpFormatter, )
    parser.add_argument( 'xmlfiles',
                         nargs="+",
                         default=[],
                         metavar='XUNIT.xml',
                         help="Test results in xunit format"
                       )
    parser.add_argument( '-t', '--title',
                         dest='title',
                         type=str,
                         help="Title for the report"
                       )
    parser.add_argument( '-s', '--subtitle',
                         dest='subtitle',
                         type=str,
                         default=None,
                         help="Subtitle for the report",
                       )
    parser.add_argument( '-H', '--html',
                         dest='html_filename',
                         type=str,
                         metavar="FILE",
                         default='report.html',
                         help="Filename for HTML report. use '0', 'no', 'False' or 'None' to skip."
                       )
    parser.add_argument( '-2', '--set2',
                         dest='xmlfiles2',
                         type=str,
                         nargs='*',
#                        action='append',
                         metavar='XUNIT.xml',
                         default=None,
                         help="Reference test results in xunit format"
                      )
    parser.add_argument( '-v', '--verbose',
                         dest='verbose',
                         action='count',
                         default=verbose,
                         help="Be loud and noisy!"
                      )
    parser.add_argument( '--test',
                         dest='test_mode',
                         action='store_true',
                         default=False,
                         help=argparse.SUPPRESS
                      )
    args = parser.parse_args()

    if args.html_filename in ['0', 'no', 'False', 'None']:
        args.html_filename = None

    verbose = args.verbose

    if args.test_mode:
        os.chdir( os.environ['HOME'] + '/mungauss' )
        rep_title = "MUNgauss Tests on BRASDOR on 2013-04-04"
        rep_subtitle = None
        xmlfiles = ['mgauss_test_BRASDOR_2013-04-04/nosetests_mgauss_test_1.xml',
                    'mgauss_test_BRASDOR_2013-04-04/nosetests_mgauss_test_2.xml',
                    'mgauss_test_BRASDOR_2013-04-04/nosetests_mgauss_test_3.xml',
                    'mgauss_test_BRASDOR_2013-04-04/nosetests_mgauss_test_4.xml']
        xmlfiles2 = ['mgauss_test_retrievium-one_2013-04-04/nosetests_mgauss_test_1.xml',
                    'mgauss_test_retrievium-one_2013-04-04/nosetests_mgauss_test_2.xml',
                    'mgauss_test_retrievium-one_2013-04-04/nosetests_mgauss_test_3.xml',
                    'mgauss_test_retrievium-one_2013-04-04/nosetests_mgauss_test_4.xml']
        rep_title = "MUNgauss Tests BRASDOR vs. retrievium-one"

    else:
        rep_title = args.title
        rep_subtitle = args.subtitle
        xmlfiles = args.xmlfiles
        if args.xmlfiles2 == None:
            xmlfiles2 = []
        else:
            xmlfiles2 = args.xmlfiles2

    # read Xunit files
    tests = {}
    for xmlfile in xmlfiles:  # loop over xml-files
        # tests.extend(readXunitFile(xmlfile))
        for testcase in readXunitFile( xmlfile ):
            if testcase.name in tests.keys():
                print "DUPLICATE TEST: %s" % testcase.name
            tests[testcase.name] = testcase

    stats = testStats( tests )

    if len( xmlfiles2 ) > 0:
        # 2 data-sets: compare
        tests2 = {}
        for xmlfile in xmlfiles2:  # loop over xml-files
            for testcase in readXunitFile( xmlfile ):
                tests2[testcase.name] = testcase

        stats2 = testStats( tests2 )
        testNames = set( tests.keys() ) | set( tests2.keys() )
        testNames = list( testNames )
        testNames.sort()

        for name in testNames:
            if tests.has_key( name ) and tests2.has_key( name ):
                if ( not tests[name].status == tests2[name].status ) or verbose > 0 :
                    print "%-40s %-8s %-8s" % ( name, tests[name].status, tests2[name].status )

        print "\nTest Set 1:"
        time = datetime( 1, 1, 1 ) + timedelta( seconds=stats["totalTime"] )
        stats['timeString'] = "%dd %dh %dm %ds" % ( time.day - 1, time.hour, time.minute, time.second )
        print "Found %(nTotal)i tests: %(nOK)i passed, %(nFailed)i failed, %(nError)i Errors, %(nSkipped)i skipped." % stats
        print "Runtime: %(timeString)s." % stats

        print "\nTest Set 2:"
        time = datetime( 1, 1, 1 ) + timedelta( seconds=stats2["totalTime"] )
        stats2['timeString'] = "%dd %dh %dm %ds" % ( time.day - 1, time.hour, time.minute, time.second )
        print "Found %(nTotal)i tests: %(nOK)i passed, %(nFailed)i failed, %(nError)i Errors, %(nSkipped)i skipped." % stats2
        print "Runtime: %(timeString)s." % stats2


    else:
        # simple report (1 data-set)
        if verbose > 0:
            for test in sorted( tests ):
                print test
        time = datetime( 1, 1, 1 ) + timedelta( seconds=stats["totalTime"] )
        stats['timeString'] = "%dd %dh %dm %ds" % ( time.day - 1, time.hour, time.minute, time.second )
        print "Found %(nTotal)i tests: %(nOK)i passed, %(nFailed)i failed, %(nError)i Errors, %(nSkipped)i skipped." % stats
        print "Runtime: %(timeString)s." % stats

        if args.html_filename:
            if verbose > 1:
                print "Writing HTML report to: %s" % args.html_filename
            htmlreport = genHtmlReport( tests, title=rep_title, subtitle=rep_subtitle, stats=stats )
            with codecs.open( args.html_filename, 'w', 'utf-8' ) as fh:
                fh.write( htmlreport )

