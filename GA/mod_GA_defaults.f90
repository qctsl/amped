      module GA_defaults
!*****************************************************************************************************************
!     Date last modified: January 18, 2001                                                          Version 1.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Names of allowed basis sets.                                                                  *
!*****************************************************************************************************************
      implicit none
!
! Genetic algorithm parameter data
      integer :: popsize,maxgen,bits_per_var,toursize,PRT_gen_inc
      integer :: NNiches ! Total number of niches created
      double precision :: crossprob,mutprob,paramscale,valid,tourprob
      double precision :: tolerance
      double precision :: GA_epsilon
      double precision :: MAX_mutation
      double precision :: GA_convergence ! Required convergence on ||g|| before switching to another OPT method
      character(len=16) :: Gene_format
      character(len=128) point_type,select,encode,bin_rep,newpop_method
      character(len=8) :: GA_mode   ! GA can be used to LOCATE or OPTIMIZE a sationary point

      double precision, dimension(:), allocatable :: domain

      end module GA_defaults
