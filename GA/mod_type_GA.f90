      module type_GA
!***************************************************************************
!     Date last modified: August 4, 2003                       Version 1.0 *
!     Author: R. A. Poirier and A. Sivret                                  *
!     Description:                                                         *
!***************************************************************************
! Modules:

      implicit none
!
      integer :: Ngenes           ! Number of genes per chromosome
      integer :: Nindividuals     ! Number of individuals in population
      integer :: Neig_wanted      ! Number of negative eigenvalues wanted (i.e., one for Saddle point)
      integer :: Gene_length      ! Number of characters representing a gene
      integer :: generation       ! Number of current generation
      integer :: Best_indv        ! Best individual in the current population (oldpop)

      double precision :: kt      ! Needed for the step function in GAf90
      double precision :: kt_2
      double precision :: lower_bound
      double precision :: upper_bound
      double precision :: up_low_bound

      character(len=32) :: Chromosome_format
      character(len=14) :: IN_OUT_BOUND
      logical flag                ! Test to make sure each offspring goes through at least one mutation or crossover

      type type_individual
        double precision, dimension(:), pointer :: chromosome => null()
        double precision :: fitness
        double precision :: step_function
        double precision :: func_value
        double precision :: global
        double precision :: gradient_length
      end type

      type type_individual_best
        integer :: generation
        integer :: Neigval
        double precision, dimension(:), pointer :: chromosome => null()
        double precision :: fitness
        double precision :: gradient_length
        double precision :: func_value
      end type

      type (type_individual_best) :: best
      type (type_individual_best) :: temp

      type (type_individual), dimension(:), allocatable :: oldpop
      type (type_individual), dimension(:), allocatable :: offspring
      type (type_individual), dimension(:), allocatable :: newpop
!
!      type (type_individual), dimension(:,:), allocatable :: array
!      type (dimension), dimension(:), allocatable :: array2

      double precision, dimension(:), allocatable :: save_parset

      end module type_GA
