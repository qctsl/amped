#ifndef SELECT_H
#define SELECT_H

/* select a single parent using roulette-wheel selection */
int roulette_select();

/* select a single parent using tournament selection */
int tour_select();

#endif
