#ifndef GA_MEMORY_H
#define GA_MEMORY_H

/* controls allocation of all memory required for the populations */
void alloc_memory();

/* allocate memory for num individuals in a population */
Individual* alloc_pop(int num);

/* allocate an array of num unsigned integers */
unsigned int* alloc_uintarr(int num);

/* allocate an array of num doubles */
double* alloc_doublearr(int num);

/* allocate an n x m array of doubles */
double** alloc_2Ddoublearr(int n, int m);

/* deallocate all memory allocated */
void free_memory();

#endif
