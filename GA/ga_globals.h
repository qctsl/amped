#ifndef GLOBALS_H
#define GLOBALS_H
#include "mun_ga_data_structs.h"

/*
Assumptions:
1. all vars are represented by no more than 32 bits, and the value
   of each variable is less than 10.0.
2. currently assuming bits_per_var = 24.
   (a) 24 data bits.
   (b) Sign is stored in bit 25.
3. conversion factors:
   (a) parameter representation: rep=(unsigned)abs(real_param * conversion 
       factor) where conversion factor currently = 1.0E6.
	 (b) note that sign bit must be copied to bit position 25.
			 if(real_param < 0){ rep = rep | (1<<24)}
	 (c) tmp = (rep & 0xffffff) 
			 if(rep & (1<<24)){ tmp *= -1}
			 real_param = ((double)tmp)/1.0E6
*/

struct prng *g;     /* required for pseudo-random number generator */

int generation;     /* generation counter */
int popsize;        /* number of individuals in population */
int maxgen;         /* maximum number of generations */
int toursize;       /* number of individuals to include in a tournament */
int numvar;         /* number of variables in the problem */
int chromlength;    /* number of bits per chromosome */
int bits_per_var;   /* number of bits used per integer (variable) */
int gen_inc;        /* increment for writing xy_gen data files */

char fitness_func;  /* character used to determine which fitness 
                     * function to call */
char generate_func; /* character used to determine which population 
                     * generation function to call */
char select_method; /* character used to determine which selection 
                     * method to call */
char encode_method; /* character used to determine which encode method 
                     *	to call */
char bin_rep;       /* character used to determine which binary 
										 * representation to use */ 
char select_newpop; /* character used to determine which method to use for
                     * setting the new population for next generation */

char *pop_file;    /* file from which to read population if generate_func=f */

Individual *oldpop; /* population for current generation */
Individual *newpop; /* population for next generation */
Individual *offspring;  /* offspring from crossover and mutation */
Individual best;    /* most fit individual */

float crossprob;    /* probability of crossover */
float mutprob;      /* probability of crossover */

double paramscale;  /* maximum perturbation from initial geometry when
										 * constructing initial population */
double valid;       /* max amt that an offspring can be from initial geometry */
double avg;         /* average fitness value */
double max,min;     /* maximum and minimum fitness values */
double epsilon;     /* parameter in fitness function */
double tourprob;    /* probability of selecting best individual in tournament
                     * selection */
double *domains;      /* upper and lower bounds for interval in interval 
										 * encoding */
double tol;         /* tolerance for the negativity of hessian eigenvalues */

FILE *outfile;      /* pointer for stats file */
#ifdef SAVEHIST
FILE *history;      /* pointer for history file */
#endif

/* Function pointers... */
Fn fitnessFn;       /* pointer to fitness function */
selectFn selection; /* pointer to parent selection function */
encodeFn to_int;    /* pointer to encoding scheme function */
decodeFn to_float;  /* pointer to decoding scheme function */
generateFn initpop; /* pointer to function to generate initial population */
choose_newpopFn choose_newpop; /* pointer to method for choosing newpop func */


#endif
