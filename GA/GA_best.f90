      subroutine GA_find_best (currentpop)
!*****************************************************************************************************************
!     Date last modified: August 4, 2003                                                            Version 1.0  *
!     Author: A. Sivret and R.A. Poirier                                                                         *
!     Description:  Find the individual with the highest fitness value and copy it to the best individual        *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE OPT_objects
      USE GA_defaults
      USE type_GA

      implicit none
!
! Local scalars:
      integer i
      double precision tmpfit
      logical found_best
!
! Derived type:
      type (type_individual) :: currentpop(Nindividuals)
! Begin:
      call PRG_manager ('enter', 'GA_find_best', 'UTILITY')
!
      found_best=.false.           ! initialization
      tmpfit=ZERO ! smallest fitness
! Scan all of the individuals in current pop and keep the index of the best indv. in Best_indv
      do i=1,Nindividuals
        if(currentpop(i)%fitness.gt.tmpfit)then
          found_best=.true.
          Best_indv=i
          tmpfit=currentpop(Best_indv)%fitness
        end if
      end do

! If a better individual is found, store it in best
      if(found_best)then
        best%chromosome(1:Ngenes)=currentpop(Best_indv)%chromosome(1:Ngenes)
        best%fitness=currentpop(Best_indv)%fitness
        best%generation=generation
        best%gradient_length=currentpop(Best_indv)%gradient_length
        best%func_value=currentpop(Best_indv)%func_value
      else
        write(UNIout,'()')'ERROR> GA_find_best: failed contact the programmers'
        stop 'ERROR> GA_find_best: failed'
      end if

      write(UNIout,'(a,i6)')'Best individual in current population: ',Best_indv
      write(UNIout,'(a,1PE14.4)')'currentpop(Best_indv)%gradient_length : ', currentpop(Best_indv)%gradient_length
      write(UNIout,'(a,1PE14.4)')'currentpop(Best_indv)%func_value : ', currentpop(Best_indv)%func_value
      write(UNIout,'(a)')'best%chromosome(1:Ngenes): '
      write(UNIout,FMT=Chromosome_format)best%chromosome(1:Ngenes)
      write(UNIout,'(a,1PE14.4)')'best%fitness: ',best%fitness
      write(UNIout,'(a,1PE14.4)')'best%gradient_length: ',best%gradient_length
      write(UNIout,'(a)')' '
      call flush(6)

! End of routine GA_find_best
      call PRG_manager ('exit', 'GA_find_best', 'UTILITY')
      return
      end subroutine GA_find_best
      subroutine GA_choose_newpop
!*****************************************************************************************************************
!     Date last modified: May 30, 2003                                                              Version 1.0  *
!     Author: A. Sivret and R.A. Poirier                                                                         *
!     Description:  Set the method to choose the individuals for a new population                                *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE OPT_objects
      USE GA_defaults
      USE type_GA

      implicit none
!
! Local scalars:

! Begin:
      call PRG_manager ('enter', 'GA_choose_newpop', 'UTILITY')
!
! Choose the new population, choice is between all offspring or above average
      if(NEWpop_method(1:1) .EQ. 'O')then
        call GA_all_offspring
      else if(NEWpop_method(1:1) .EQ. 'A')then
        call GA_above_average
      else
        write(UNIout,'(a)')'ERROR> GA_choose_newpop: Incorrect value in choose_newpop'
        stop 'ERROR> GA_choose_newpop: Incorrect value in choose_newpop'
      end if

! End of routine GA_choose_newpop
      call PRG_manager ('exit', 'GA_choose_newpop', 'UTILITY')
      return
!
      contains
      subroutine GA_all_offspring
!*****************************************************************************************************************
!     Date last modified: May 30, 2003                                                              Version 1.0  *
!     Author: A. Sivret and R.A. Poirier                                                                         *
!     Description:  Copy all of the offsprings to the new population                                             *
!*****************************************************************************************************************
      implicit none
!
! Local scalars:
      integer Iindividual

! Begin:
! Copy all of the offsprings directly into newpop without any changes
      do Iindividual=1,Nindividuals
        newpop(Iindividual)%chromosome(1:Ngenes)=offspring(Iindividual)%chromosome(1:Ngenes)
        newpop(Iindividual)%fitness=offspring(Iindividual)%fitness
        newpop(Iindividual)%func_value=offspring(Iindividual)%func_value
        newpop(Iindividual)%gradient_length=offspring(Iindividual)%gradient_length
      end do

      return
      end subroutine GA_all_offspring

      subroutine GA_above_average
!*****************************************************************************************************************
!     Date last modified: June 19, 2003                                                             Version 1.0  *
!     Author: A. Sivret and R.A. Poirier                                                                         *
!     Description:  Generate the new population by randomly choosing parents and offsprings with greater         *
!                   than average fitness                                                                         *
!*****************************************************************************************************************
      implicit none
!
! Local scalars:
      integer Iindividual,Index,Ibest,i,OldIndv
      integer numold,numnew,pool_size
      double precision target,totalfit,avg
      double precision tmpbest_fit
      double precision tmp_mutprob      ! variable to save the real value of mutprob

! Local work arrays:
      integer, dimension(2*Nindividuals) :: pool_of_indv  ! array of above average indv. from oldpop and offspring
      integer, dimension(Nindividuals) :: tmpbest
!
! Begin:
      numold = 0         ! Temporary index for the pool of indv. that came from oldpop
      numnew = 0         ! Temporary index for the pool of indv. that came from offspring
      totalfit=ZERO      ! sum of fitness from offspring for calculating average fitness
      avg = ZERO         ! average fitness in offspring.

! Sum up the fitness of each individual from offspring
      do Iindividual=1,Nindividuals
        totalfit=totalfit+offspring(Iindividual)%fitness
      end do
!      print*,'totalfit=',totalfit
      avg=totalfit/Nindividuals

! Find out who has a greater than average fitness in oldpop and store it in pool of indv.
      do Iindividual=1,Nindividuals
        if(oldpop(Iindividual)%fitness .GT. avg) then
          numold = numold + 1
          pool_of_indv(numold)=Iindividual
        end if
      end do

! Find out who has a greater than average fitness in offspring and store it in pool of indv.
      do Iindividual=1,Nindividuals
        if(offspring(Iindividual)%fitness .GT. avg) then
          numnew=numnew+1
          pool_of_indv(numold+numnew)=Iindividual
        end if
      end do

      pool_size = numold + numnew
      print*,'numold = ' , numold
      print*,'numnew = ' , numnew
!      print*,'pool_size = ' , pool_size
!      call flush(UNIout)
! Store the best individual in the first indv. of newpop
      newpop(1)%chromosome(1:Ngenes)=best%chromosome(1:Ngenes)
      newpop(1)%fitness=best%fitness
      newpop(1)%func_value=best%func_value
      newpop(1)%gradient_length=best%gradient_length

!      print*,'pool before : ' , pool_of_indv

! if the pool size is greater than the number needed in newpop, then fill up newpop with individuals randomly
! chosen from pool of indv.  An individual can only be chosen once to be in newpop.
      if(pool_size .GE. Nindividuals) then
        call random_number(target)
        Index=int(dble(pool_size)*target+ONE)
        do Iindividual=2,Nindividuals
          do while (pool_of_indv(Index).EQ.-1)  ! to make sure each indv. can only be chosen once
            call random_number(target)
            Index=int(dble(pool_size)*target+ONE)
          end do
          OldIndv=pool_of_indv(Index)
          if (Index.le.numold) then             ! individual is from oldpop
            newpop(Iindividual)%chromosome(1:Ngenes)=oldpop(OldIndv)%chromosome(1:Ngenes)
            newpop(Iindividual)%fitness=oldpop(OldIndv)%fitness
            newpop(Iindividual)%func_value=oldpop(OldIndv)%func_value
            newpop(Iindividual)%gradient_length=oldpop(OldIndv)%gradient_length
            pool_of_indv(Index)=-1
          else                                  ! individual is from offspring
            newpop(Iindividual)%chromosome(1:Ngenes)=offspring(OldIndv)%chromosome(1:Ngenes)
            newpop(Iindividual)%fitness=offspring(OldIndv)%fitness
            newpop(Iindividual)%func_value=offspring(OldIndv)%func_value
            newpop(Iindividual)%gradient_length=offspring(OldIndv)%gradient_length
            pool_of_indv(Index)=-1
          end if
        end do ! Iindividual
!        print*,'pool_of_indv : ', pool_of_indv

! if the pool size is too small, fill up newpop from all of the individuals from pool of indv. and then randomly
! select individuals and perform a mutation to fill up the remainder of newpop
      else
        tmp_mutprob=mutprob   ! save value of mutprob
        mutprob=1.0D0         ! change mutprob to 1 to make sure that each mutations will occur
        do Iindividual=2,numold
          OldIndv=pool_of_indv(Iindividual)
          newpop(Iindividual)%chromosome(1:Ngenes)=oldpop(OldIndv)%chromosome(1:Ngenes)
          newpop(Iindividual)%fitness=oldpop(OldIndv)%fitness
          newpop(Iindividual)%func_value=oldpop(OldIndv)%func_value
          newpop(Iindividual)%gradient_length=oldpop(OldIndv)%gradient_length
        end do
        do Iindividual=numold+1,pool_size
          OldIndv=pool_of_indv(Iindividual)
          newpop(Iindividual)%chromosome(1:Ngenes)=offspring(OldIndv)%chromosome(1:Ngenes)
          newpop(Iindividual)%fitness=offspring(OldIndv)%fitness
          newpop(Iindividual)%func_value=offspring(OldIndv)%func_value
          newpop(Iindividual)%gradient_length=offspring(OldIndv)%gradient_length
        end do
        do Iindividual=pool_size+1,Nindividuals
          call random_number(target)
          Index=int(dble(pool_size)*target+ONE)
          OldIndv=pool_of_indv(Index)
!          print*,'Index = ' ,Index
!          print*,'pool_of_indv(Index) = ' ,pool_of_indv(Index)
!          call flush(6)
          if(Index.le.numold) then
            call GA_mutate (oldpop(OldIndv))
            call GA_fitness (oldpop, OldIndv)
!            print*,'Define Iindiv from olpop= ',Iindividual
!          call flush(6)
            newpop(Iindividual)%chromosome(1:Ngenes)=oldpop(OldIndv)%chromosome(1:Ngenes)
            newpop(Iindividual)%fitness=oldpop(OldIndv)%fitness
            newpop(Iindividual)%func_value=oldpop(OldIndv)%func_value
            newpop(Iindividual)%gradient_length=oldpop(OldIndv)%gradient_length
          else
            call GA_mutate (offspring(OldIndv))
            call GA_fitness (offspring, OldIndv)
!            print*,'Define Iindiv from offspring= ',Iindividual
!          call flush(6)
            newpop(Iindividual)%chromosome(1:Ngenes)=offspring(OldIndv)%chromosome(1:Ngenes)
            newpop(Iindividual)%fitness=offspring(OldIndv)%fitness
            newpop(Iindividual)%func_value=offspring(OldIndv)%func_value
            newpop(Iindividual)%gradient_length=offspring(OldIndv)%gradient_length
          end if
        end do
!            print*,'end do '
!          call flush(6)
        mutprob=tmp_mutprob     ! give back to mutprob its original value
!        print*,'mutprob = ' , mutprob
      end if

      return
      end subroutine GA_above_average

      end subroutine GA_choose_newpop

