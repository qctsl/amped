#ifndef GA_H
#define GA_H

/* main function that controls the genetic algorithm */
void ga_(double parset[], double pargrd[], int* noptpr, double* grdlth, double* func_value);

#endif
