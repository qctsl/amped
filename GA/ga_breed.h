#ifndef BREED_H
#define BREED_H

/* control the reproduction...call functions to select parents, perform
 * crossover and mutation, and ensure that the offspring are valid (if
 * multiplicative encoding is used) */
void breed(double* initial);

/* perform mutation on offspring */
void mutate(Chrome *offspring);

/* perform crossover on parent1 and parent2, forming offspring1 and 2 */
void crossover(Chrome *parent1, Chrome *parent2,Chrome *offspring1, Chrome *offspring2);

/* examine individuals in current_pop for higher fitness than best.fitness 
 * updating the best individual if a better fitness is found */
void find_best(Individual *current_pop);

/* ensure that each variable in test is within `valid' of initial variables, 
 * if not, set extraneous variable to small random perturbation of initial */
void validate(Individual* test, double* initial);

#endif
