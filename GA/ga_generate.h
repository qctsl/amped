#ifndef GENERATE_H
#define GENERATE_H

/* generate the initial population using small perturbations of parset */
void generate_population(double *parset);

/* create initial population from an input file */
void file_start(double *initial);

/* create newpop using random selection from those individuals in oldpop
 * and offspring whose fitness values are above average as well as the
 * best individual from the previous generation */
void above_average();

/* create newpop using all of the offspring created */
void all_offspring();

#endif
