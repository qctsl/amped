#include <stdio.h>
#include <stdlib.h>
#include "prng.h"
#include "mun_ga_data_structs.h"
#include "mun_ga_encode.h"
#include "mun_ga_breed.h"
#include "mun_ga_utils.h"
#include "mun_ga_generate.h"

extern double paramscale;
extern struct prng *g;
extern int popsize;
extern Individual *oldpop;
extern Individual *newpop;
extern Individual *offspring;
extern Individual best;
extern double avg;
extern int numvar;
extern int bits_per_var;
extern char encode_method;
extern char* pop_file;
extern double *domains;
#ifdef SAVEHIST
extern FILE *history;
#endif

void generate_population(double *parset){
	int i,j;
	double perturbation;
	double *temp;
	
	temp = (double*)calloc(numvar,sizeof(double));

/* copy parset to first individual in oldpop */
	encode(parset,oldpop[0].chrom);

/* initialize all popsize individuals */
/* do things differently depending on interval or multiplicative encoding...*/
	if (encode_method=='m') { /* do it the multiplicative way...*/
		for(i=1;i<popsize;i++){
			for(j=0;j<numvar;j++){
				/* get random number to scale initial parameters to create new individual 
				 * restrict random number between -paramscale and paramscale */
				perturbation=paramscale*(2*prng_get_next(g)-1.0);
				temp[j] = parset[j]+perturbation;
			}
			/* encode individual created by random perturbations */
			encode(temp,oldpop[i].chrom); 
		}
	} 
	else { /* do it the range-encoding way... */
		for (i=1;i<popsize;i++) {
			for (j=0;j<numvar;j++) {
/*				do { * keep going until we get a suitable perturbation...*
					perturbation=paramscale*(2*prng_get_next(g)-1.0);
 				} while (parset[j]+perturbation<domains[2*j]
 					|| parset[j]+perturbation>domains[2*j+1]);
 			temp[j]=parset[j]+perturbation;
*/
    /* try just using a random number and scale it to within the interval */
				oldpop[i].chrom[j]=(unsigned)(prng_get_next(g)*((1<<bits_per_var)-1));
			}
/* don't need to encode the individual now since an integer was generated
  instead of a floating point number which previously had to be mapped
  into the correct interval */
/*			* encode the indivdual...*
 			encode(temp,oldpop[i].chrom);
*/
		}
	}

	free(temp);
	return;
}

void file_start(double *initial){
	/* population is to be read in from file */
	int i,j;
	double *temp;
	FILE *init_file;

	temp = (double*)calloc(numvar,sizeof(double));

	printf("opening file %s\n",pop_file);
	fflush(NULL);

	init_file = fopen(pop_file,"r");
	if(init_file==NULL){
		fprintf(stderr,"Could not open population input file");
		exit(1);
	}

	for(i=0;i<popsize;i++){
		for(j=0;j<numvar;j++){
			fscanf(init_file,"%lf",&(temp[j]));
			printf("%f ",temp[j]);
		}
		encode(temp,oldpop[i].chrom); 
	}

	fclose(init_file);
	return;
}

void above_average(){
	int i;
	int numold=0;  /* number of individuals from oldpop with fitness greater than avg */
	int numnew=0;  /* number of individuals from offspring with fitness greater than avg */
	int *subpop;   /* indices of individuals chosen from oldpop and offspring */
	int r;
	int subpopsize;

	subpop = (int*)calloc(2*popsize,sizeof(int));

	/* create pool of individuals from oldpop (previous generation) and offspring 
	 * (newly created individuals) consisting of those individuals whose fitness
	 * values are greater than the average from the previous generation */
#ifdef SAVEHIST
	fprintf(history,"average fitness = %f\n",avg);
#endif
	for(i=0;i<popsize;i++){
		if(oldpop[i].fitness > avg){
			subpop[numold] = i;
			numold++;
#ifdef SAVEHIST
			fprintf(history,"oldpop %d placed in subpop\n",i);
#endif
		}
	}
	for(i=0;i<popsize;i++){
		if(offspring[i].fitness > avg){
			subpop[numold+numnew] = i;
			numnew++;
#ifdef SAVEHIST
			fprintf(history,"offspring %d placed in subpop\n",i);
#endif
		}
	}
	subpopsize=numold+numnew;
	/* subpop complete... */
	/* Add best individual from previous generation to new population */
	copy_ind(&(best),&(newpop[0]));

	/* if numold+numnew > popsize, then just pick popsize individuals from 
	 * the subpopulation...
	 */
	if (subpopsize>=popsize) {
		/* Choose random individuals from
		* subpop to occur in the next generation.
		*/
		for(i=1;i<popsize;i++){  /* start at 1 since best individual is in 0 */
			r = (int)(prng_get_next_int(g) % subpopsize);
			if(r < numold){  /* chosen individual came from previous generation */
				copy_ind(&(oldpop[subpop[r]]),&(newpop[i]));
#ifdef SAVEHIST
				fprintf(history,"individual %d chosen from oldpop\n",subpop[r]);
#endif
			}
			else { /* chosen individual came from offsprint */
				copy_ind(&(offspring[subpop[r]]),&(newpop[i]));
#ifdef SAVEHIST
				fprintf(history,"individual %d chosen from offspring\n",subpop[r]);
#endif
			}
		}
	}
	else {
		/* subpopsize too small to fill up a new population, so just
		 * copy over all of them to new pop, and then choose randomly
		 * from oldpop to fill up the remainder.
		 */
		for (i=1;i<numold;i++) {
			copy_ind(&(oldpop[subpop[i]]),&(newpop[i]));
		}
		for (i=numold;i<numold+numnew;i++) {
			copy_ind(&(offspring[subpop[i]]),&(newpop[i]));
		}
		/* ok, fill up the rest */
		for (i=0;i<popsize-subpopsize;i++) {
			r=(int)(prng_get_next_int(g)%subpopsize);
			if(r<numold){
				mutate(oldpop[subpop[r]].chrom);
				copy_ind(&(oldpop[subpop[r]]),&(newpop[i+subpopsize]));
			}
			else{
				mutate(offspring[subpop[r]].chrom);
				copy_ind(&(offspring[subpop[r]]),&(newpop[i+subpopsize]));
			}
		}
	}

	free(subpop);			
	return;
}

void all_offspring(){  /* fill newpop with offspring created */
	int i;
	for(i=0;i<popsize;i++){
		copy_ind(&(offspring[i]),&(newpop[i]));
	}
#ifdef SAVEHIST
	fprintf(history,"all offspring copied into newpop\n");
#endif
	return;
}
