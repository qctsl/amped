      subroutine GA_C
!*****************************************************************************************************************
!     Date last modified: May 20, 2003                                                              Version 1.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Computes OBJECT_NAME for the following MODALITY.                                              *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE OPT_defaults
      USE OPT_objects

      implicit none
!
! Begin:
      call PRG_manager ('enter', 'GA_C', 'UTILITY')
!
      write(UNIout,'(a)')'Genetic-Algorithm optimization (C Version: Sept. 20, 2000)'
      call OPT_initialize
      call ga (PARSET, PARGRD, Noptpr, GRDLTH, OPT_function%value)
      write(UNIout,'(a)') 'C genetic algorithm terminated normally'
      call OPT_terminate
!
! End of routine GA_C
      call PRG_manager ('exit', 'GA_C', 'UTILITY')
      return
      end subroutine GA_C
      subroutine get_func_value (param,   & ! Input Parameter set
                                 local_fn,  & ! output function value
                                 n)      ! Noptpr
!*******************************************************
!     Date last modified:  May 18, 2000               *
!     Author:  Sharene Bungay                          *
!     Description:  Get energy of new geometry 'param' *
!                   called by C code mun_ga.c          *
!*******************************************************
! Modules:
      USE program_objects
      USE OPT_defaults
      USE OPT_objects

      implicit none

! Input scalars:
      integer n
      double precision local_fn

! Input arrays:
      double precision param(n);

      write(6,*) 'param= ',param(1:Noptpr)

! Set PARSET to geometry 'param' received
      parset(1:Noptpr)=param(1:NOPTPR)

! Clear stack (does not remove PARSET)
      call OPT_TO_COOR
      call KILL_objects (.true.)

      call get_object ('OPT:FUNCTION')

      local_fn=OPT_function%value
      n = Noptpr

      return
      end
      subroutine get_grad(grad,    & ! Calculated gradient
                           n) ! Number of optimizable parameters
!**************************************************************
!     Date last modified:  Feb. 12, 2000                      *
!     Author:  Sharene Bungay                                 *
!     Description:  Get gradient of new geometry 'param'      *
!                   which was set by previous call to         *
!                   get_func_value.  Called by mun_ga.c       *
!**************************************************************
! Modules:
      use OPT_defaults
      USE OPT_objects

      implicit none

! Input scalar:
      integer n
!
! Input arrays:
      double precision grad(n);

! Build gradient object
      call get_object ('OPT:GRADIENTS')
! Copy gradient values to grad for return
      grad(1:Noptpr)=pargrd(1:NOPTPR)

      return
      end
      subroutine get_hess (n,   & ! Number of optimizable parameters
                           tol, & ! Tolerance for the negativity of eigenvalues
                           numneg)  ! Number of negative eigenvalues
!*************************************************************
!     Date last modified:  Feb. 12, 2000                     *
!     Author:  Sharene Bungay                                *
!     Description:  Get hessian of new geometry 'param'      *
!                   set by previous call to get_func_value;  *
!                   the gradient was computed by call        *
!                   to get_grad.  Called by mun_ga.c         *
!*************************************************************
! Modules:
      USE OPT_defaults
      USE OPT_objects
      USE mod_type_hessian

      implicit none

! Input scalars:
      integer n
      double precision tol
!
! Output scalars:
      integer numneg
!
! Local scalars:
      integer i
!
! Local arrays:
      double precision, dimension(:), allocatable :: eigval
      double precision, dimension(:,:), allocatable :: eigvec

      allocate (eigval(noptpr), eigvec(noptpr,noptpr))

! Force re-calculation of the Hessian
      call get_object ('OPT:HESSIAN%'//Hessian_method(1:len_trim(Hessian_method)))

! Copy hessian values to 'hessian' for return

      call diag_hessian (Hessian_matrix, noptpr, eigval, eigvec, .false.)
!
      do i=1,noptpr
        write(6,*)'eigval(',i,') = ',eigval(i)
      end do

      numneg = 0

      do i=1,noptpr
        if(eigval(i).lt.(tol)) then
          numneg = numneg + 1
        end if
      end do

      write(6,*)'number of negative eigenvalues=',numneg

      return
      end
