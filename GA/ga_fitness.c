#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "mun_ga_data_structs.h"
#include "mun_ga_encode.h"
#include "mun_ga_fitness.h"

/* Fortran functions called from C...*/
extern get_func_value_();  /* set PARSET and get energy of these parameters */
extern get_grad_();  /* get gradient of parameters set in get_func_value */
extern get_hess_();  /* get number of negative hessian eigenvalues */

extern int generation;
extern int popsize;
extern Individual *oldpop;
extern Individual *offspring;
extern Individual best;
extern double avg;
extern double max;
extern double min;
extern double tol;
extern double epsilon;
extern int numvar;
extern char select_method;
extern Fn fitnessFn;
extern FILE *outfile;
#ifdef SAVEHIST
extern FILE *history;
#endif

void init_fitness(){  /* set fitness of initial population */
	int i;
	int numneg;
	double *temp;

	temp = (double*)calloc(numvar,sizeof(double));

	for(i=0;i<popsize;i++){
		/* decode the current individual and store in temp, to determine energy,
		 * gradient, and hessian, leaving individual unchanged 
		 */
		decode(oldpop[i].chrom,temp);

		/* get energy, gradient and number of negative eigenvalues
		 * (of the Hessian) of current individual */
		get_func_value_(temp,&oldpop[i].func_value,&numvar);
		get_grad_(oldpop[i].grad,&numvar);
		get_hess_(&numvar,&tol,&numneg);

		/* evaluate fitness of individual just initialized */
		fitnessFn(&(oldpop[i]),numneg);
	}

	/* just set the best to the first one here so that we can
	 * do a meaningful comparison...*/
	for (i=0;i<numvar;i++) {
		best.chrom[i] = oldpop[0].chrom[i];
		best.grad[i] = oldpop[0].grad[i];
#ifdef SAVEHIST
		fprintf(history,"best.grad[%d]=%f, oldpop[0].grad[%d]=%f\n",i,best.grad[i],i,oldpop[0].grad[i]);
#endif
	}
	best.fitness = oldpop[0].fitness;
	best.normfit = oldpop[0].normfit;
	best.generation = generation;
	best.grdlth = oldpop[0].grdlth;
#ifdef SAVEHIST
	fprintf(history,"best fit initialized\n");
#endif

	free(temp);
	return;
}

void ts_evaluate(Individual *ind,int numneg){
	double gradnorm;

	/* find gradient norm of current individual */
	gradnorm = calc_norm(ind->grad,numvar);
	ind->grdlth = gradnorm;

	ind->negeig = numneg;

	/* ideal situation is a hessian with only one negative eigenvalue
	 * and a very small (~0) gradient.  Add epsilon to prevent
	 * division by zero.
	 */
	ind->fitness = 1.0/((gradnorm+epsilon)*(numneg-1+epsilon));

	return;
}

void min_evaluate(Individual *ind,int numneg){
	double gradnorm;

	/* find gradient norm of current individual */
	gradnorm = calc_norm(ind->grad,numvar);
	ind->grdlth = gradnorm;

	ind->negeig = numneg;

	/* ideal situation is a hessian with zero negative eigenvalues
	 * and a very small (~0) gradient.  Add epsilon to prevent
	 * division by zero.
	 */
	ind->fitness = 1.0/((gradnorm+epsilon)*(numneg+epsilon));

	return;
}

/* calculate the L2-norm of a vector of dimension n */
double calc_norm(double *vec, int n){
	int i;
	double temp=0.0;
	double norm=0.0;
	
	for(i=0;i<n;i++){
		temp += vec[i]*vec[i];
	}
	norm = sqrt(temp/numvar);

	return norm;
}

void offspring_fit(){
	int i,numneg;
	double *temp;

	temp = (double*)calloc(numvar,sizeof(double));

	for(i=0;i<popsize;i++){
		/* decode current offspring to calculate gradient, etc. 
		 * store "real" (decoded) values in temp */
		decode(offspring[i].chrom,temp);

		/* get energy, gradient and Hessian of current offspring */
		get_func_value_(temp,&(offspring[i].func_value),&numvar);
		get_grad_(offspring[i].grad,&numvar);
		get_hess_(&numvar,&tol,&numneg);

		/* calculate fitness of current offspring */
		fitnessFn(&(offspring[i]),numneg);
	}

	free(temp);
	return;
}

/* Normalize steps through all the individuals in a population,
 * calculates the max, min, and avg fitness values, stores these in 
 * min,max,avg global variables, and then scales all the fitness
 * values to lie within [0,1], which are then stored in 
 * normfit.
 */
void normalize(Individual *pop){
	int i;

	double curfit=0;
	double totalfit = 0.0;

	/* set max and min to the fitness value of the first individual, just
	 * for comparison purposes */
	max = pop[0].fitness;
	min = pop[0].fitness;

	for(i=0;i<popsize;i++){
		curfit=pop[i].fitness;
		totalfit += curfit;
		if(curfit > max) max = curfit;
		if(curfit < min) min = curfit;
	}

	/* now we know max and min fitness values...
	 * scale everything to lie within [0,1]
	 * this is only needed by roulette wheel selection...
	 */
	if(select_method=='r'){
		for(i=0;i<popsize;i++){
	 	 	if((max-min)!=0){  /* ensure no division by zero */
		  	printf("normalizing fitness\n");
				fflush(NULL);
				pop[i].normfit = (pop[i].fitness - min) / (max - min);
			}
			else{
		  	pop[i].normfit = 1.0;
			}
		}
	}
	
	/* calculate the average fitness */
	avg=totalfit/popsize;

	return;
}
