#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "mun_ga_data_structs.h"
#include "mun_ga_params.h"
#include "mun_ga_encode.h"

extern int numvar;
extern int bits_per_var;
extern char bin_rep;
extern double *domains;
extern encodeFn to_int;
extern decodeFn to_float;
#ifdef SAVEHIST
extern FILE *history;
#endif

void encode(double *data, Chrome* ind){
  to_int(data,ind); /* change real number data to integer data */
	if(bin_rep=='g'){
		gray_encode(ind);  /* change from standard binary to gray encoding */
	}

	return;
}

void mult_encode(double *real, unsigned *int_rep){
	int index;
	unsigned temp;

	for(index=0;index<numvar;index++){
		temp = (unsigned)(fabs(real[index])*SHIFTAMT);
		/* if the value is negative, set the bits_per_var bit to 1 */
		if(real[index]<0){
			temp = temp | (1<<bits_per_var);
		}
		int_rep[index] = temp;
	}

	return;
}

void interval_encode(double *real, unsigned *int_rep){
	int i;

	for(i=0;i<numvar;i++){
	/* add 0.5 to temp to ensure rounding up for the integer representation */
		int_rep[i] = (unsigned)((real[i]-domains[2*i])*((1<<bits_per_var)-1)/(domains[2*i+1]-domains[2*i])+0.5);
	}
	return;
}

void gray_encode(Chrome *bin){
	int i,j;
	unsigned mask;
	unsigned *gray;

	gray = (unsigned*)calloc(numvar,sizeof(unsigned));

	for(i=0;i<numvar;i++){
		mask = (1<<(bits_per_var-1));
		gray[i] = bin[i] & mask;
		for(j=0;j<bits_per_var;j++){
			if(((bin[i] & mask)>>1) == (bin[i] & (mask>>1))){
				mask = mask >> 1;  /* leave gray bit as 0 */
			}
			else{
				mask = mask >> 1;
				gray[i] |= mask;  /* set gray bit to 1 */
			}
		}
		/* set the sign bit */
		if (bin[i]&(1<<bits_per_var)) gray[i]=gray[i]|(1<<bits_per_var);
	}

	/* copy the contents of gray to the individual passed in */
	for(i=0;i<numvar;i++){
		bin[i] = gray[i];
	}

	free(gray);
	return;
}

void decode(Chrome *ind, double *decoded){
	unsigned *bin; 
	bin = (unsigned*)calloc(numvar,sizeof(unsigned));
	if(bin_rep=='g'){
		gray_decode(bin,ind);  /* change from standard binary to gray encoding */
  	to_float(bin,decoded); /* change real number data to integer data */
	}
	else {
  	to_float(ind,decoded); /* change real number data to integer data */
	}

	free(bin);
	return;
}

void mult_decode(unsigned *ind, double *decoded){
	int index;
	double temp;
	
	for(index=0;index<numvar;index++){	
		temp = (double)(ind[index] & 0xffffff);
		/* restore negative sign if present (in bits_per_var position) */
		if(ind[index] & (1<<bits_per_var)){
			temp *= -1;
		}
		temp = temp/(double)(SHIFTAMT);
		decoded[index] = temp;
	}

	return;
}

void interval_decode(unsigned *int_rep, double *real){
	int i;
	double interval;
	const unsigned maxint=(1<<bits_per_var)-1;


	for(i=0;i<numvar;i++){
		interval=(double)(domains[2*i+1]-domains[2*i])/(double)maxint;
		real[i] = domains[2*i]+int_rep[i]*interval;
	}

	return;
}

void gray_decode(unsigned *bin, unsigned *gray){
	int i, j;
	unsigned mask;

	for(i=0;i<numvar;i++){
		mask = (1<<(bits_per_var-1));
		bin[i] = gray[i] & mask;
		for(j=0;j<bits_per_var;j++){
			if(((bin[i] & mask)>>1)==(gray[i] & (mask>>1))){
				mask = mask >>1;  /* leave bin bit as 0 */
			}
			else{
				mask = mask >> 1;
				bin[i] |= mask;  /* set bin bit to 1 */
			}
		}
		/* do the sign bit thingy */
		if (gray[i]&(1<<bits_per_var)) bin[i]|=(1<<bits_per_var);
	}
	return;
}
