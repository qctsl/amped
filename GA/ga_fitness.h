#ifndef FITNESS_H
#define FITNESS_H

/* compute the fitness values of the initial population */
void init_fitness(); 

/* the fitness function used for minima, calculates the fitness of ind and
 * sets negeig in the ind structure to numneg */
void min_evaluate(Individual *ind,int numneg);

/* the fitness function used for transition states, calculates the fitness 
 * of ind and sets negeig in the ind structure to numneg */
void ts_evaluate(Individual *ind,int numneg);

/* calculate the l2 norm of a vector of dimension n */
double calc_norm(double *vec, int n);

/* compute the fitness values of the offspring created */
void offspring_fit();

/* scale pop fitness values to between 0 and 1 if roulette wheel selection 
 * compute max, min, and avg fitness of pop */
void normalize(Individual *pop);

#endif
