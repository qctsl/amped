#include <stdio.h>
#include <stdlib.h>
#include "mun_ga_data_structs.h"
#include "mun_ga_memory.h"

extern int popsize;
extern Individual *oldpop;
extern Individual *newpop;
extern Individual *offspring;
extern Individual best;
extern int numvar;

void alloc_memory(){
	/* allocate memory for oldpop, newpop, and offspring */
	printf("allocating populations...\n");
	oldpop = alloc_pop(popsize);
	newpop = alloc_pop(popsize);
	offspring = alloc_pop(popsize);

	/* allocate memory for most fit individual */
	printf("allocating best individual...\n");
	best.chrom = alloc_uintarr(numvar);
	best.grad = alloc_doublearr(numvar);

	return;
}

Individual* alloc_pop(int num){
	int i;
	Individual *temp;

	/* allocate memory for num individuals in pop */
	if((temp=(Individual*)calloc(num,sizeof(Individual)))==NULL){
		fprintf(stderr,"not enough memory for pop");
	}

	for(i=0;i<num;i++){
		/* allocate memory for chromosome of each individual in pop */
		temp[i].chrom = alloc_uintarr(numvar);
		/* allocate memory for gradient of each individual in pop */
		temp[i].grad = alloc_doublearr(numvar);
	}
	printf("populations allocated\n");

	return temp;
}

unsigned int* alloc_uintarr(int num){
	unsigned int *arr;
	/* allocate memory for an unsigned int array with num entries */
	if((arr=(unsigned int*)calloc(num,sizeof(unsigned int)))==NULL){
		fprintf(stderr,"not enough memory for array unsigned ints");
	}

	return arr;
}

double* alloc_doublearr(int num){
	double *arr;
	/* allocate memory for a double array with num entries */
	if((arr=(double*)calloc(num,sizeof(double)))==NULL){
		fprintf(stderr,"not enough memory for array of doubles");
	}

	return arr;
}

double** alloc_2Ddoublearr(int n, int m){
	int i;
	double **arr;
	/* allocate memory for a two dimensional (n x m) array
	 * first allocate n pointers to arrays of doubles */
	if((arr=(double**)calloc(n,sizeof(double*)))==NULL){
		fprintf(stderr,"not enough memory for 2D array of doubles");
	}
	/* allocate m entries for each of the n pointers allocated above */
	for(i=0;i<n;i++){
		arr[i] = alloc_doublearr(m);
	}

	return arr;
}


void free_memory(){
	int i;

	for(i=0;i<popsize;i++){
		free(oldpop[i].chrom);
		free(newpop[i].chrom);
		free(offspring[i].chrom);
		free(oldpop[i].grad);
		free(newpop[i].grad);
		free(offspring[i].grad);
	}

	free(oldpop);
	free(newpop);
	free(offspring);
	free(best.chrom);
	free(best.grad);

	return;
}
