#include <stdio.h>
#include <stdlib.h>
#include "mun_ga_data_structs.h"
#include "mun_ga_encode.h"
#include "mun_ga_IO.h"

extern int numvar;
extern int popsize;
extern int chromlength;
extern int bits_per_var;
extern int generation;
extern double avg;
extern double max;
extern double min;
extern FILE *history;
extern FILE *outfile;
extern Individual *oldpop;
extern Individual best;

void print_stats(){
	int i;
	double *temp;
	temp = (double*)calloc(numvar,sizeof(double));

	decode(best.chrom,temp);

	fprintf(outfile,"%d ",generation);
	fprintf(outfile,"%f ",avg);
	fprintf(outfile,"%f ",max);
	fprintf(outfile,"%f ",min);
	fprintf(outfile,"%f ",best.fitness);
	for(i=0;i<numvar;i++){
		fprintf(outfile,"%f ",temp[i]);
	}
	fprintf(outfile,"%g ",best.grdlth);
	fprintf(outfile,"\n");
	free(temp);
}

void print_ind(Individual ind){
 	int j;
	double *temp;

	temp = (double*)calloc(numvar,sizeof(double));

	decode(ind.chrom,temp);
#ifdef SAVEHIST
	for(j=0;j<numvar;j++){
		fprintf(history,"%f ",temp[j]);
	}
	fprintf(history,"\n\tgradient = ");
	for(j=0;j<numvar;j++){
		fprintf(history,"%14.10g ",ind.grad[j]);
	}
	fprintf(history,"\n\tfunction value = %f\n",ind.func_value);
	fprintf(history,"\tfitness value = %f\n",ind.fitness);
	fprintf(history,"\tnormalized fitness = %f\n",ind.normfit);
	fprintf(history,"\tgradient length = %14.10g\n",ind.grdlth);
	fprintf(history,"\tnegative eigenvalues = %d\n",ind.negeig);
#endif
	
	free(temp);
	return;
}

void print_chrom(FILE *fp,Chrome *chrom,int chromlength){
	int i,j;
	int *chromstring;
	unsigned int mask=1;
	unsigned int temp;

	chromstring = (int*)calloc(chromlength,sizeof(int));

	for(i=0;i<numvar;i++){
		temp = chrom[i];
		for(j=0;j<bits_per_var;j++){
			if((temp & mask) ==1){
				chromstring[i*bits_per_var + j] = 1;
			}
			else{
				chromstring[i*bits_per_var + j] = 0;
			}
			temp = temp >> 1;
		}
	}
	for(i=0;i<numvar;i++){
		for(j=bits_per_var-1;j>=0;j--){
			fprintf(fp,"%d",chromstring[i*bits_per_var+j]);
		}
		fprintf(fp," ");
	}
	free(chromstring);
	return;
}

void print_params(FILE* fp){
	int i,j;
	double *decoded;

	decoded = (double*)calloc(numvar,sizeof(double));

	for(i=0;i<popsize;i++){
		decode(oldpop[i].chrom,decoded);
		for(j=0;j<numvar;j++){
			fprintf(fp,"%f ",decoded[j]);
		}
		fprintf(fp,"\n");
	}

	free(decoded);
	return;
}

