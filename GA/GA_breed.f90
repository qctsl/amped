      subroutine GA_breed
!*****************************************************************************************************************
!     Date last modified: July 7, 2003                                                              Version 1.0  *
!     Author: A. Sivret and R.A. Poirier                                                                         *
!     Description: Individuals are selected by Tournament or Roulette-wheel and offsprings are generated from    *
!                  the selected individuals (parents) by performing crossovers and/or mutations                  *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE OPT_objects
      USE GA_defaults
      USE type_GA

      implicit none
!
! Local scalars:
      integer i,Iindividual,winner,Individual1,Individual2
      integer tour_best,tour_value
      integer index
      integer, dimension(toursize) :: parent
      double precision sum,target,totalfit
      double precision, dimension(Ngenes) :: Perturbation
      double precision cross_best
!
! Begin:
      call PRG_manager ('enter', 'GA_breed', 'UTILITY')
!
      index=1
      cross_best=0.1*Nindividuals   ! best individual will breed 10 % of total population

! Breed all the offsprings, two at a time
      do while(index .LT. Nindividuals)
        if(select(1:1) .EQ. 'R')then
          Individual1=roulette()
          Individual2=roulette()
          do while(Individual2.EQ.Individual1)
            Individual2=roulette()
          end do
        else if(select(1:1) .EQ. 'T')then
          Individual1=tournament()
          Individual2=tournament()
          do while(Individual2.EQ.Individual1)
            Individual2=tournament()
          end do
        else
          write(UNIout,*)'ERROR> GA_breed: No selection method specified'
          stop 'No selection method specified'
        end if

!Both parents selected, and ready for crossover
        flag=.false. ! true if crossover or mutation has taken place

        if(cross_best.GE.index)then  ! best individual must be a parent for 10 % of the population of offsprings
          Individual1=Best_indv ! 5/2/4 RAP
        end if

        call GA_crossover (Individual1, Individual2, index)

        call GA_mutate (offspring(index))
        call GA_mutate (offspring(index+1))

        if(flag)then ! if the offsprings did not go through any crossovers or mutations, they are not valid and must be replaced
          index=index+2
        end if

      end do
!
! End of routine GA_breed
      call PRG_manager ('exit', 'GA_breed', 'UTILITY')
      return

      contains

      integer function tournament()
!*****************************************************************************************************************
!     Date last modified: May 27, 2003                                                              Version 1.0  *
!     Author: A. Sivret and R.A. Poirier                                                                         *
!     Description: Individuals are selected by Tournament                                                        *
!*****************************************************************************************************************
!
! Begin:

! Randomly select the individuals from oldpop to participate in the tournament
        do i=1,toursize
          call random_number (target)
          parent(i)=int(dble(Nindividuals)*target+ONE)
        end do

        tour_best=parent(1)    ! We assume parent 1 is the best

! Selection of the best indv. in the tournament
        do i=2,toursize
          if(oldpop(parent(i))%fitness .GT. oldpop(tour_best)%fitness)then
            tour_best=parent(i)
          end if
        end do

        call random_number (target)

! If the tourprob is greater than target (random number between 0 and 1), than tour_best is kept
        if(tourprob .GT. target)then
          tournament=tour_best
        else ! one of the participants is randomly chosen ( could be the tour_best )
          call random_number (target)
          tournament=parent(int(dble(toursize)*target+ONE))
        end if

        return

      end function tournament

      integer function roulette()
!*****************************************************************************************************************
!     Date last modified: May 27, 2003                                                              Version 1.0  *
!     Author: A. Sivret and R.A. Poirier                                                                         *
!     Description: Individuals are selected by Roulette-wheel                                                    *
!*****************************************************************************************************************

! Begin:
        totalfit=ZERO

! Calculate the total fitness of oldpop
        do Iindividual=1,Nindividuals
          totalfit = totalfit + oldpop(Iindividual)%fitness
        end do

        call random_number (target)
        sum=ZERO

! Add up the fraction of the total fitness for each individuals until we reach the target
        fitness_sum: do Iindividual=1,Nindividuals
          sum = sum + oldpop(Iindividual)%fitness/totalfit
          if(sum .GT. target) exit fitness_sum
        end do fitness_sum

        if(Iindividual.GT.Nindividuals)then
          call random_number (target)
          Iindividual=int(dble(Nindividuals)*target+ONE)
        end if
!
        roulette=Iindividual
        return

      end function roulette
      end subroutine GA_breed

      subroutine GA_crossover (Individual1, Individual2, index)
!*****************************************************************************************************************
!     Date last modified: May 27, 2003                                                              Version 1.0  *
!     Author: A. Sivret and R.A. Poirier                                                                         *
!     Description: Create two offspring by crossover of chromosomes                                              *
!*****************************************************************************************************************
! Modules:
      USE program_constants
      USE OPT_objects
      USE GA_defaults
      USE type_GA

      implicit none
!
! Input scalars:
      integer Individual1,Individual2,index
!
! Local scalars:
      integer i,crosspoint
      double precision target,target2
!
! Local work arrays:
      character(len=Gene_length*Ngenes) parent1
      character(len=Gene_length*Ngenes) parent2
      character(len=Gene_length*Ngenes) Coffspring1
      character(len=Gene_length*Ngenes) Coffspring2
!
! Begin:
      call random_number(target)
      crosspoint=target*Gene_length*Ngenes+1 !find the crosspoint

      call random_number(target2)

      write(parent1,FMT=Chromosome_format)oldpop(Individual1)%chromosome(1:Ngenes) ! write the chromosome(double precision numbers)
      write(parent2,FMT=Chromosome_format)oldpop(Individual2)%chromosome(1:Ngenes)
!
! The part of the chromosome after the crosspoint between the two offsprings will be swapped if the
! crossprob is greater than target 2
      do i=1,Gene_length*Ngenes
        if(crossprob.GT.target2)then
          flag=.true.
          if(i .LT. crosspoint)then
            Coffspring1(i:i) = parent1(i:i)
            Coffspring2(i:i) = parent2(i:i)
          else
            Coffspring1(i:i) = parent2(i:i)
            Coffspring2(i:i) = parent1(i:i)
          end if
        else
          Coffspring1(i:i) = parent1(i:i)  ! If the crossprob is less than target2, then nothing will be swapped
          Coffspring2(i:i) = parent2(i:i)
        end if
      end do

      read(Coffspring1,FMT=Chromosome_format)offspring(index)%chromosome(1:Ngenes)  ! read the character string Coffspring in the of
      read(Coffspring2,FMT=Chromosome_format)offspring(index+1)%chromosome(1:Ngenes)

      return
      end subroutine GA_crossover

      subroutine GA_mutate (Individual)
!*****************************************************************************************************************
!     Date last modified: May 30, 2003                                                              Version 1.0  *
!     Author: A. Sivret and R.A. Poirier                                                                         *
!     Description: Create a small mutation on the genes of a chromosome                                          *
!*****************************************************************************************************************
! Modules:
      USE program_constants
      USE OPT_objects
      USE GA_defaults
      USE type_GA

      implicit none
!
! Local scalars:
      integer i
      double precision target
!
! Derived type:
      type (type_individual) :: Individual
!
! Local work array:
      double precision, dimension(Ngenes) :: Perturbation
!
!Begin:

      Perturbation(1:Ngenes)=0

! Add a random positive or negative small perturbation to a gene
      call random_number (perturbation)
      call GA_pos_neg (perturbation, Ngenes)

! Control mutation size
      perturbation(1:Ngenes)=perturbation(1:Ngenes)*MAX_mutation

      do i=1,Ngenes
        call random_number(target)
        if(mutprob .GT. target)then
          flag=.true.
!          write(6,*)'mutation: perturbation= ',perturbation
          Individual%chromosome(i)=Individual%chromosome(i)+perturbation(i)
        end if
      end do

      return
      end subroutine GA_mutate
