#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "mun_ga_data_structs.h"
#include "mun_ga_select.h"
#include "mun_ga_encode.h"
#include "mun_ga_fitness.h"
#include "mun_ga_generate.h"
#include "mun_ga_setup.h"

extern double paramscale;
extern double valid;
extern int popsize;
extern int maxgen;
extern int toursize;
extern int gen_inc;
extern int numvar;
extern double tourprob;
extern double tol;
extern double epsilon;
extern float crossprob;
extern float mutprob;
extern int bits_per_var;
extern char fitness_func;
extern char select_method;
extern char encode_method;
extern char select_newpop;
extern char generate_func;
extern char bin_rep;
extern char* pop_file;
extern double *domains;
extern Fn fitnessFn;
extern selectFn selection;
extern encodeFn to_int;
extern decodeFn to_float;
extern generateFn initpop;
extern choose_newpopFn choose_newpop;
extern FILE *outfile;
#ifdef SAVEHIST
extern FILE *history;
#endif

/* set pointer to correct fitness function, based on the value of the
 * fitness_func input parameter
 */
void set_fitness(){
	switch(fitness_func){
		case 'm':
			fitnessFn = min_evaluate;
#ifdef SAVEHIST
			fprintf(history,"Fitness function for minima chosen\n");
#endif
			break;
		case 's':
			fitnessFn = ts_evaluate;
#ifdef SAVEHIST
			fprintf(history,"Fitness function for transition states chosen\n");
#endif
			break;
		default:
			fprintf(stderr,"Invalid fitness function\n");
			break;
	}

	return;
}

/* set pointer to function to generate the initial population */
void set_generate(){
	switch(generate_func){
		case 'd':
			initpop = generate_population;
#ifdef SAVEHIST
			fprintf(history,"The default population generation method chosen\n");
#endif
			break;
		case 'f':
			initpop = file_start;
#ifdef SAVEHIST
			fprintf(history,"The initial population will be read in from a file\n");
#endif
			break;
	}
	return;
}

/* set pointer to correct selection method, based on the value of the
 * select_method input parameter
 */
void set_select(){
	switch(select_method){
		case 'r':
			selection = roulette_select;
#ifdef SAVEHIST
			fprintf(history,"Roulette wheel selection chosen\n");
#endif
			break;
		case 't':
			selection = tour_select;
#ifdef SAVEHIST
			fprintf(history,"Tournament selection chosen\n");
#endif
			break;
		default:
			fprintf(stderr,"Invalid selection method\n");
			break;
	}

	return;
}

/* set pointer to correct encode method, based on the value of the
 * encode_method input parameter
 */
void set_encode(){
  	switch(encode_method){
		case 'm':
			to_int = mult_encode;
			to_float = mult_decode;
#ifdef SAVEHIST
			fprintf(history,"Multiplication encoding chosen\n");
#endif
			break;
		case 'i':
			to_int = interval_encode;
			to_float = interval_decode;
#ifdef SAVEHIST
			fprintf(history,"Interval encoding chosen\n");
#endif
			break;
		default:
			fprintf(stderr,"Invalid encoding method\n");
			break;
	}

	return;
}

/* set pointer to correct method for setting newpop, based on the value of the
 * select_newpop input parameter
 */
void set_choose_newpop(){
	switch(select_newpop){
		case 'o':
			choose_newpop = all_offspring;
#ifdef SAVEHIST
			fprintf(history,"All_offspring for newpop chosen\n");
#endif
			break;
		case 'a':
			choose_newpop = above_average;
#ifdef SAVEHIST
			fprintf(history,"Random selection from above average individuals chosen for newpop\n");
#endif
			break;
		default:
			fprintf(stderr,"Invalid newpop replacement method chosen\n");
			break;
	}

	return;
}

void set_ga_params_(char *pt, int *size, int *max, double *pc, double *pm,
                    int *bpv, double *ps, double *val, char *sel, char *enc,
                    double *intervals, char *br, char *np, double *tolerance,
                    double *eps, int *gi, int *num){
  /* set global variables to values passed in from Fortran */
	int i;
	int n;

	n = *num;  /* should be noptpr...just use local variable here, noptpr
	            * will be passed in later when ga_ is called.
	            */
/*	printf("local noptpr = %d\n",n); */
	fflush(NULL);
	fitness_func = tolower(pt[0]);
/*	printf("fitness_func = %c\n",fitness_func); */
	fflush(NULL);
	popsize = *size;
/*	printf("popsize = %d\n",popsize); */
	fflush(NULL);
	maxgen = *max;
/*	printf("maxgen = %d\n",maxgen); */
	fflush(NULL);
	crossprob = *pc;
/*	printf("crossprob = %f\n",crossprob); */
	fflush(NULL);
	mutprob = *pm;
/*	printf("mutprob = %f\n",mutprob); */
	fflush(NULL);
	bits_per_var = *bpv;
/*	printf("bits_per_var = %d\n",bits_per_var); */
	fflush(NULL);
	paramscale = *ps;
/*	printf("paramscale = %f\n",paramscale); */
	fflush(NULL);
	valid = *val;
/*	printf("valid = %f\n",valid); */
	fflush(NULL);
	select_method = tolower(sel[0]);
/*	printf("select_method = %c\n",select_method); */
	fflush(NULL);
	encode_method = tolower(enc[0]);
/*	printf("encode_method = %c\n",encode_method); */
	fflush(NULL);
	bin_rep = tolower(br[0]);
/*	printf("bin_rep = %c\n",bin_rep); */
	fflush(NULL);
	select_newpop = tolower(np[0]);
/*	printf("select_newpop = %c\n",select_newpop); */
	fflush(NULL);
	tol = *tolerance;
/*	printf("tol = %f\n",tol); */
	fflush(NULL);
	epsilon = *eps;
/*	printf("epsilon = %f\n",epsilon); */
	fflush(NULL);
	gen_inc = *gi;
/*	printf("gen_inc = %d\n",gen_inc); */
	fflush(NULL);

	if(encode_method=='i'){
			printf("allocating domains array\n");
			fflush(NULL);
			domains = (double*)calloc(2*n,sizeof(double));
			/* set global values for domain i, used in interval encoding,
			 * to values passed in from Fortran */
			for(i=0;i<n;i++){
				domains[2*i] = intervals[2*i];
				domains[2*i+1] = intervals[2*i+1];
				/* print for debugging */
				fprintf(stdout,"(domains[%d],domains[%d]) = ( %f, %f)\n",
					2*i,2*i+1,domains[2*i],domains[2*i+1]);
			}
	}
/*	printf("returning from set_ga_params\n"); */
	fflush(NULL);
	return;
}

void set_ga_tour_(int *ts, double *tp){
	/* set variables used in tournament selection to values passed
	 * in from Fortran */
	toursize = *ts;
/*	printf("toursize = %d\n",toursize); */
	fflush(NULL);
	tourprob = *tp;
/*	printf("tourprob = %f\n",tourprob); */
	fflush(NULL);

	return;
}

void file_format(){
	FILE *fp;
	fp = stdout;
	fprintf(fp,"Input file format is as follows:");
	fprintf(fp,"\n");
	fprintf(fp,"Variable       Type        Description\n");
	fprintf(fp,"--------       ----        -----------\n");
	fprintf(fp,"#               N/A        a comment\n");
	fprintf(fp,"point_type     char        type of point sought\n");
	fprintf(fp,"                            'm' -> minimum\n");
	fprintf(fp,"                            's' -> first order saddle\n");
	fprintf(fp,"popsize         int        number of individuals\n");
	fprintf(fp,"maxgen          int        maximum number of generations\n");
	fprintf(fp,"crossprob      float       probability of crossover\n");
	fprintf(fp,"mutprob        float       probability of mutation\n");
	fprintf(fp,"bits_per_var    int        number of bits used per variable\n");
	fprintf(fp,"paramscale     float       max perturbation of initial pop\n");
	fprintf(fp,"                           from initial geometry\n");
	fprintf(fp,"valid          float       max perturbation of offspring\n");
	fprintf(fp,"                           from initial geometry\n");
	fprintf(fp,"select_method  char        which selection method to use\n");
	fprintf(fp,"                            'r' -> roulette wheel\n");
	fprintf(fp,"                            't' -> tournament\n");
	fprintf(fp,"--if tournament selection, read in integer toursize and\n");
	fprintf(fp,"  double tourprob (probability of choosing best individual)\n");
	fprintf(fp,"encode_method  char (double) (double)   which encode method to use\n");
	fprintf(fp,"                                         'm' -> multiplication encoding\n");
	fprintf(fp,"                                         'i' -> interval encoding\n");
	fprintf(fp,"---if interval encoding, read in double lower and upper bounds\n");
	fprintf(fp,"bin_rep        char        which binary representation to use\n");
	fprintf(fp,"                            's' -> standard binary\n");
	fprintf(fp,"                            'g' -> gray encoding\n");
	fprintf(fp,"choose_newpop  char        which newpop replacement to use\n");
	fprintf(fp,"                            'o' -> all offspring\n");
	fprintf(fp,"                            'a' -> random choice of above average individuals\n");
	fprintf(fp,"tol           double       tolerance for the negativity of\n");
	fprintf(fp,"                           hessian eigenvalues\n");
	fprintf(fp,"epsilon       double       parameter in fitness function\n");
	fprintf(fp,"gen_inc        int         increment for writing data files\n");

	return;
}

void corrupt_file(char *filename, int line, char *str){
	fprintf(stderr, "Corrupt input file\n");
	fprintf(stderr, "Error in %s on line %d\n",filename,line);
	fprintf(stderr, "%s\n",str);
	file_format();
	exit(1);
}

void read_parameters(char *filename){
	char buf[512];
	char *pt;
	FILE *infile;
	int i=0;
	int line = 0;
	int gotgen,gotpt,gotpop,gotmax,gotcp,gotmp,gotbpi,gotps,gotval,gotsel,goten,gotbin,gotnew,gottol,goteps,gotinc;
	enum{INIT,POINT_TYPE,POPSIZE,MAXGEN,CROSSPROB,MUTPROB,BITS_PER_INT,PARAMSCALE,VALID,SELECT,ENCODE,BIN_REP,NEWPOP,TOLERANCE,EPS,GENINC}; 
	int item;
	infile = fopen(filename,"r");
	if(infile==NULL){
		fprintf(stderr,"FILE %s not found\n",filename);
		exit(1);
	}

	gotgen = gotpt = gotpop = gotmax = gotcp = gotmp = gotbpi = gotps = gotval = gotsel = goten = gotbin = gotnew = gottol = goteps = gotinc = 0;

	fprintf(stdout,"******Initializing data******\n");

	while(fgets(buf, sizeof(buf), infile)!=NULL){
		if(buf[0]=='#'){
			continue;
/*			line++; this line is not reachable*/     
		}
		else{
			pt = strtok(buf," ");
			if (strcmp(pt,"INIT")==0) item=INIT;
			else if(strcmp(pt,"POINT_TYPE")==0) item=POINT_TYPE;
			else if(strcmp(pt,"POPSIZE")==0) item=POPSIZE;
			else if(strcmp(pt,"MAXGEN")==0) item=MAXGEN;
			else if(strcmp(pt,"CROSSPROB")==0) item=CROSSPROB;
			else if(strcmp(pt,"MUTPROB")==0) item=MUTPROB;
			else if(strcmp(pt,"BITS_PER_INT")==0) item=BITS_PER_INT;
			else if(strcmp(pt,"PARAMSCALE")==0) item=PARAMSCALE;
			else if(strcmp(pt,"VALID")==0) item=VALID;
			else if(strcmp(pt,"SELECT")==0) item=SELECT;
			else if(strcmp(pt,"ENCODE")==0) item=ENCODE;
			else if(strcmp(pt,"BIN_REP")==0) item=BIN_REP;
			else if(strcmp(pt,"NEWPOP")==0) item=NEWPOP;
			else if(strcmp(pt,"TOLERANCE")==0) item=TOLERANCE;
			else if(strcmp(pt,"EPS")==0) item=EPS;
			else if(strcmp(pt,"GENINC")==0) item=GENINC;
			else{
				corrupt_file(filename,line,pt);
			}

			switch(item){
				case INIT:
					pt = strtok(NULL," ");
					if(pt==NULL){
						corrupt_file(filename,line,pt);
					}
					generate_func = pt[0];
					printf("gotgen=%c\n",generate_func);
					if(generate_func=='f'){  /* read in filename as well */
						pt = strtok(NULL," ");
						if(pt==NULL){
							corrupt_file(filename,line,pt);
						}
						pop_file = (char*)calloc(strlen(pt),sizeof(char));
						strcpy(pop_file,pt);
						pop_file[strlen(pop_file)-1]='\0';
					}
					line++;
					gotgen=1;
					break;
				case POINT_TYPE:
					pt = strtok(NULL," ");
					if(pt==NULL){
						corrupt_file(filename,line,pt);
					}
					fitness_func = pt[0];
					printf("gotpt=%c\n",fitness_func);
					line++;
					gotpt=1;
					break;
				case POPSIZE:
					pt = strtok(NULL," ");
					if(pt==NULL){
						corrupt_file(filename,line,pt);
					}
					popsize = atoi(pt);
					if(popsize%2 != 0){
						fprintf(stdout,"Must be an even number of individuals...adding one\n");
						popsize++;
					}
					printf("gotpop=%d\n",popsize);
					line++;
					gotpop=1;
					break;
				case MAXGEN:
					pt = strtok(NULL," ");
					if(pt==NULL){
						corrupt_file(filename,line,pt);
					}
					maxgen = atoi(pt);
					printf("gotmax=%d\n",maxgen);
					line++;
					gotmax=1;
					break;
				case CROSSPROB:
					pt = strtok(NULL," ");
					if(pt==NULL){
						corrupt_file(filename,line,pt);
					}
					crossprob = atof(pt);
					printf("gotcp=%f\n",crossprob);
					line++;
					gotcp=1;
					break;
				case MUTPROB:
					pt = strtok(NULL," ");
					if(pt==NULL){
						corrupt_file(filename,line,pt);
					}
					mutprob = atof(pt);
					printf("gotmp=%f\n",mutprob);
					line++;
					gotmp=1;
					break;
				case BITS_PER_INT:
					pt = strtok(NULL," ");
					if(pt==NULL){
						corrupt_file(filename,line,pt);
					}
					bits_per_var = atoi(pt);
					printf("gotbpi=%d\n",bits_per_var);
					line++;
					gotbpi=1;
					break;
				case PARAMSCALE:
					pt = strtok(NULL," ");
					if(pt==NULL){
					  	corrupt_file(filename,line,pt);
					}
					paramscale = atof(pt);
					printf("gotps=%f\n",paramscale);
					line++;
					gotps=1;
					break;
				case VALID:
					pt = strtok(NULL," ");
					if(pt==NULL){
					  	corrupt_file(filename,line,pt);
					}
					valid = atof(pt);
					printf("gotval=%f\n",valid);
					line++;
					gotval=1;
					break;
				case SELECT:
					pt = strtok(NULL," ");
					if(pt==NULL){
						corrupt_file(filename,line,pt);
					}
					select_method = pt[0];
					printf("gotsel=%c\n",select_method);
					gotsel=1;
					if(select_method=='t'){
						/* need to read in tournament size and probability */
						pt = strtok(NULL," ");
						if(pt==NULL){
							corrupt_file(filename,line,pt);
						}
						toursize = atoi(pt);
						printf("toursize = %d\n",toursize);

						pt = strtok(NULL," ");
						if(pt==NULL){
							corrupt_file(filename,line,pt);
						}
						tourprob = atof(pt);
						printf("tourprob = %f\n",tourprob);
					}
					line++;
					break;
				case ENCODE:
					pt = strtok(NULL," ");
					if(pt==NULL){
						corrupt_file(filename,line,pt);
					}
					encode_method = pt[0];
					printf("goten=%c\n",encode_method);
					goten=1;
					if(encode_method=='i'){
						/* need to read in upper and lower bounds */
						domains = (double*)calloc(2*numvar,sizeof(double));
						for(i=0;i<numvar;i++){
							printf("domains loop i=%d\n",i);  
							pt = strtok(NULL," ");
							if(pt==NULL){
								corrupt_file(filename,line,pt);
							}
							domains[2*i] = atof(pt);
							printf("lower bound = %f\n",domains[2*i]);
	
							pt = strtok(NULL," ");
							if(pt==NULL){
								corrupt_file(filename,line,pt);
							}
							domains[2*i+1] = atof(pt);
							printf("upper bound = %f\n",domains[2*i+1]);
						}
					}
					line++;
					break;
				case BIN_REP:
					pt = strtok(NULL," ");
					if(pt==NULL){
						corrupt_file(filename,line,pt);
					}
					bin_rep = pt[0];
					printf("gotbin=%c\n",bin_rep);
					line++;
					gotbin=1;
					break;
				case NEWPOP:
					pt = strtok(NULL," ");
					if(pt==NULL){
						corrupt_file(filename,line,pt);
					}
					select_newpop = pt[0];
					printf("gotnew=%c\n",select_newpop);
					line++;
					gotnew=1;
					break;
				case TOLERANCE:
					pt = strtok(NULL," ");
					if(pt==NULL){
						corrupt_file(filename,line,pt);
					}
					tol = atof(pt);
					printf("gottol=%f\n",tol);
					line++;
					gottol=1;
					break;
				case EPS:
					pt = strtok(NULL," ");
					if(pt==NULL){
						corrupt_file(filename,line,pt);
					}
					epsilon = atof(pt);
					printf("goteps=%f\n",epsilon);
					line++;
					goteps=1;
					break;
				case GENINC:
					pt = strtok(NULL," ");
					if(pt==NULL){
						corrupt_file(filename,line,pt);
					}
					gen_inc = atoi(pt);
					printf("gotinc=%d\n",gen_inc);
					line++;
					gotinc=1;
					break;
				default:
					corrupt_file(filename,line,pt);
					break;
			}
		}
	}
	if(gotgen*gotpt*gotpop*gotmax*gotcp*gotmp*gotbpi*gotps*gotval*gotsel*goten*gotbin*gotnew*gottol*goteps*gotinc==0){
		fprintf(stderr,"Some variable was not initialized\n");
		file_format();
		exit(0);
	}

	return;
}
