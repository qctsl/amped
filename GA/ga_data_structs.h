#ifndef DATA_STRUCTS_H
#define DATA_STRUCTS_H

typedef unsigned int Chrome;

typedef struct individual{
	Chrome *chrom;   /* chromosome bit string */
	double fitness;  /* fitness value */
	double normfit;  /* the normalized fitness value */
	double *grad;    /* gradient */
	double func_value;     /* function value */
	double grdlth;   /* gradient norm */
	int negeig;      /* number of negative eigenvalues in the hessian */
	int generation;  /* used only for "best fit" individual */
} Individual;

/* Function pointer definitions... */
typedef void (*Fn)(Individual *,int); /* a pointer to the fitness func */
typedef int (*selectFn)();        /* a pointer to the selection method */
typedef void (*encodeFn)(double*,unsigned*);/* a pointer to the encode method */
typedef void (*decodeFn)(unsigned*,double*);/* a pointer to the decode method */
typedef void (*generateFn)(double*);/* a pointer to the generate method */
typedef void (*choose_newpopFn)(); /* a pointer to function which sets newpop */

#endif
