#ifndef ENCODE_H
#define ENCODE_H

/* calls appropriate encoding functions depending on to_int pointer 
 * as well as gray_encode if bin_rep=`g' */
void encode(double *data, Chrome* ind);

/* encodes real data into int_rep using multiplicative encoding */
void mult_encode(double *real, unsigned *int_rep);

/* encodes real data into int_rep using interval (or range) encoding */
void interval_encode(double *real, unsigned *int_rep);

/* converts standard binary encoded bin to gray encoding */
void gray_encode(Chrome *bin);

/* calls appropriate decoding functions depending on to_float pointer 
 * as well as gray_decode if bin_rep=`g' */
void decode(Chrome *ind, double *decoded);

/* decodes ind data into decoded using multiplicative decoding */
void mult_decode(unsigned *ind, double *decoded);

/* decodes int_rep data into real using interval (or range) encoding */
void interval_decode(unsigned *int_rep, double *real);

/* converts gray encoded variables to standard binary encoding */
void gray_decode(unsigned *bin, unsigned *gray);

#endif
