#ifndef SETUP_H
#define SETUP_H

/* set pointer to correct population generation function, based on 
 * the value of the generate_func input parameter
 */
void set_generate();

/* set pointer to correct fitness function, based on the value of the
 * fitness_func input parameter
 */
void set_fitness();

/* set pointer to correct selection method, based on the value of the
 * select_method input parameter
 */
void set_select();

/* set pointer to correct encode method, based on the value of the
 * encode_method input parameter
 */
void set_encode();

/* set pointer to correct method for choosing population for next 
 * generation, based on the value of the select_newpop input parameter
 */
void set_choose_newpop();

void set_ga_params_(char*,int*,int*,double*,double*,int*,double*,double*,char*,char*,double*,char*,char*,double*,double*,int*,int*);

void set_ga_tour_(int*,double*);

/* prints the assumed format of the gadat input file */
void file_format();

/* stops execution if the input file has an incorrect format, and 
 * prints the file format to stdout 
 */
void corrupt_file(char *filename, int line, char *str);

/* parses the input file `filename' setting appropriate global variables */
void read_parameters(char *filename);

#endif
