#include <stdio.h>
#include <stdlib.h>
#include "prng.h"
#include "mun_ga_data_structs.h"
#include "mun_ga_select.h"

extern struct prng *g;
extern int popsize;
extern Individual *oldpop;
extern int toursize;
extern double tourprob;

int roulette_select(){  /* perform roulette wheel selection */
	int i;
	double r;
	double sum;
	double totalfit=0.0;

	/* sum up the total fitness...*/
	for(i=0;i<popsize;i++){
		totalfit += oldpop[i].normfit;
	}
	
	r = prng_get_next(g);
	sum = 0;
	if(totalfit != 0){
		for(i=0;i<popsize;i++){
			sum += oldpop[i].normfit / totalfit;
			if(sum > r) break;
		}
	}
	else{  /* just pick a random parent if totalfit=0 */
		i = (int)(prng_get_next_int(g) % popsize);
	}
	return i;
}

int tour_select(){
	int i;
	int *parent;
	int tour_best;  /* index of best individual in current tournament */
	int winner;
	double r;

	parent = (int*)calloc(toursize,sizeof(int));

	for(i=0;i<toursize;i++){
		parent[i] = (int)(prng_get_next_int(g) % popsize);
	}

	/* find best individual in current tournament */
	tour_best = parent[0];
	for(i=1;i<toursize;i++){
		if(oldpop[parent[i]].fitness > oldpop[tour_best].fitness){
			tour_best = parent[i];
		}
	}

	/* get random number for comparison to tourprob */
	r = prng_get_next(g);

	if(tourprob > r){  /* choose the best individual in the tournament */
		winner = tour_best;
	}
	else{ /* choose random individual in the tournament,could still be the best */
		winner = parent[(int)(prng_get_next_int(g) % toursize)];
	}

	return winner;
}
