#ifndef PARAMS_H
#define PARAMS_H

/* number used for multiplicative encoding and decoding */
#define SHIFTAMT (1.0E6)  

/* number of bits in an unsigned integer */
#define UNSIGNEDSIZE (8*sizeof(unsigned int))

#endif
