#ifndef GAIO_H
#define GAIO_H

/* print generation #, avg, max, min, and best fitness values to stats file */
void print_stats();

/* print components of the ind structure to history file */
void print_ind(Individual ind);

/* print chrom bitwise to file given by fp */
void print_chrom(FILE *fp,Chrome *chrom,int chromlength);

/* print decoded variable values of each individual in oldpop */
void print_params(FILE *fp);

#endif
