#ifndef UTILS_H
#define UTILS_H

/* flip a biased coin, heads chosen with a probability `prob', returning
 * 0 or 1 reflecting heads or tails.
 */
int flip(double prob);

/* Copy contents of ind_source structure to ind_dest structure */
void copy_ind(Individual *ind_source, Individual *ind_dest);

#endif
