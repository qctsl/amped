#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "prng.h"
#include "mun_ga_params.h"
#include "mun_ga_data_structs.h"
#include "mun_ga_utils.h"
#include "mun_ga_encode.h"
#include "mun_ga_breed.h"

extern struct prng *g;
extern double valid;
extern int generation;
extern int popsize;
extern double paramscale;
extern Individual *oldpop;
extern Individual *offspring;
extern Individual best;
extern float crossprob;
extern float mutprob;
extern int numvar;
extern int chromlength;
extern int bits_per_var;
extern selectFn selection;
extern char encode_method;
#ifdef SAVEHIST
extern FILE* history;
#endif


void breed(double* initial){
	int i=0;
	int parent1, parent2;
	
	for(i=0;i<popsize-1;i+=2){
		/* choose two parents for mating, selection method was set by
		 * previous call to set_select() */
		parent1 = selection();
		parent2 = selection();

#ifdef SAVEHIST
		fprintf(history,"Parent1 selected = %d, normfit = %f\n",parent1,oldpop[parent1].normfit);
		fprintf(history,"Parent2 selected = %d, normfit = %f\n",parent2,oldpop[parent2].normfit);
#endif

		/* perform crossover on parents selected, forming 2 offspring */
		crossover(oldpop[parent1].chrom,oldpop[parent2].chrom,offspring[i].chrom,offspring[i+1].chrom);

		/* mutate first offspring produced */
#ifdef SAVEHIST
		fprintf(history,"Mutating individual %d...",i);
#endif
		mutate(offspring[i].chrom);

		/* mutate second offspring produced */
#ifdef SAVEHIST
		fprintf(history,"Mutating individual %d...",i+1);
#endif
		mutate(offspring[i+1].chrom);

	}

	/* confirm that each offspring created is a valid geometry, if not
	 * set any invalid geometries to a small random perturbation of the
	 * initial geometry */
	if(encode_method=='m'){
		validate(offspring,initial);
	}

	return;
}


void mutate(Chrome *c){
	int i,j;
	unsigned mask=0;
	unsigned temp = 1;  /* temp remains unchanged */

#ifdef SAVEHIST
	fprintf(history,"Flipping bits ");
#endif 
	for(i=0;i<numvar;i++){
		mask = 0;
		for(j=0;j<bits_per_var;j++){
			if(flip(mutprob)){
				mask = mask | (temp<<j);
#ifdef SAVEHIST
				fprintf(history,"%d ",j);
#endif
			}
		}
		c[i] = c[i]^mask;
	}
#ifdef SAVEHIST
	fprintf(history,"\n");
#endif

	return;
}

void crossover(Chrome *parent1, Chrome *parent2, 
               Chrome *offspring1, Chrome *offspring2){
	int i,j;
	unsigned mask;
	unsigned temp=1;
	int crosspoint;

	/* determine whether or not to perform crossover */
	if(flip(crossprob)){
		crosspoint = (int)(prng_get_next_int(g) % chromlength);
#ifdef SAVEHIST
		fprintf(history,"Crosspoint = %d\n",crosspoint);
#endif
		for(i=0;i<numvar;i++){
			if(crosspoint >= ((i+1)*UNSIGNEDSIZE)){
				/* crosspoint not reached yet, so swap these ints */
				offspring1[i] = parent1[i];
				offspring2[i] = parent2[i];
			}
			else if((crosspoint<((i+1)*UNSIGNEDSIZE))&&(crosspoint>(i*UNSIGNEDSIZE))){
				mask = 1;
				for(j=0;j<(crosspoint+1-i*UNSIGNEDSIZE);j++){
					mask = mask <<1;
					mask = mask | temp;
				}
				offspring1[i] = (parent1[i] & mask) | (parent2[i] & (~mask));
				offspring2[i] = (parent1[i] & (~mask)) | (parent2[i] & mask);
			}
			else{
				offspring1[i] = parent2[i];
				offspring2[i] = parent1[i];
			}
		}
	}
	else{  /* do not crossover, just copy parents to offspring */
#ifdef SAVEHIST
		fprintf(history,"Crossover not performed, parents copied to offspring\n");
#endif
		for(i=0;i<numvar;i++){
			offspring1[i] = parent1[i];
			offspring2[i] = parent2[i];
		}
	}
}

/* Find best takes a population, and searches for the individual
 * with the best fitness... this individual and its statistics are
 * then stored in the Individual called "best".
 */
void find_best(Individual *current_pop){
	int i;
	int bfi=-1; /* index of individual with best fit */
	double tmpfit;

	tmpfit=best.fitness;

	for(i=0;i<popsize;i++){
		if(current_pop[i].fitness > tmpfit) {
			bfi=i;
			tmpfit=current_pop[bfi].fitness;
		}
	}
	if (bfi==-1) {
		/* no better fitness was found in this population! */
		return;
	}

#ifdef SAVEHIST
	fprintf(history,"Better fit found!!!\n");
	fprintf(history,"Old best fitness was : %f\n",best.fitness);
	fprintf(history,"New best fitness is : %f\n",tmpfit);
#endif

	/* update best individual structure */
	copy_ind(&(current_pop[bfi]),&best);
	best.generation = generation;
#ifdef SAVEHIST
	fprintf(history,"returning from find_best\n");
#endif

	return;
}

void validate(Individual* test, double* initial){
	int i=0,j=0;
	int changed=0; /* set if individual contained invalid parameters */
	double perturbation;
	double *temp;

	temp = (double*)calloc(numvar,sizeof(double));

	for(i=0;i<popsize;i++){
		decode(test[i].chrom,temp);
		for(j=0;j<numvar;j++){
			if(fabs(temp[j]-initial[j])>valid){ /* current parameter of individual is invalid */
				perturbation=paramscale*(2.0*prng_get_next(g)-1.0);
#ifdef SAVEHIST
				fprintf(history,"offspring %d has invalid parameters,\n",i);
				fprintf(history,"\tresetting parameter %d, ",j);
				fprintf(history,"old value = %f,",temp[j]);
#endif
				temp[j] = initial[j] + perturbation;
#ifdef SAVEHIST
				fprintf(history,"new value = %f\n",temp[j]);
#endif
				changed=1;
			}
		}
		/* re-encode individuals that were invalid, and had parameters changed */
		if(changed){
			encode(temp,test[i].chrom);
			changed=0;
		}
	}
		
	free(temp);
	return;
}

