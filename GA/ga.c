#include<stdio.h>
#include<stdlib.h>
#include<sys/time.h>
#include "prng.h"
#include "mun_ga_data_structs.h"
#include "mun_ga_globals.h"
#include "mun_ga_breed.h"
#include "mun_ga_encode.h"
#include "mun_ga_fitness.h"
#include "mun_ga_IO.h"
#include "mun_ga_memory.h"
#include "mun_ga_generate.h"
#include "mun_ga_params.h"
#include "mun_ga_select.h"
#include "mun_ga_setup.h"
#include "mun_ga_utils.h"
#include "mun_ga.h"


void ga_(double parset[], double pargrd[], int *noptpr, double *grdlth_ptr, double *func_value_ptr){
	
	int i;
	/*int seed_read;*/
	char *xy_file;         /* filename for parameter data */
	Individual *temp_pop;
	double grdlth, func_value;
	double *temp;
	double *initial;       /* initial variable values input in parset */
	FILE *xy_data;         /* file used for individual variable data */
	/*FILE *seed;            * file containing a single seed */
	FILE *run_info;        /* file containing seed information */
	struct timeval tv;     /* structure used for gettimeofday */
	struct timezone tz;    /* structure used for gettimeofday */

	numvar = *noptpr;  /* set global variable numvar to number of parameters */
	grdlth = *grdlth_ptr;
	func_value = *func_value_ptr;

	printf("number of parameters = %d\n",numvar);
	fflush(NULL);

	chromlength = numvar * UNSIGNEDSIZE;  /* number of bits in chromosome */

	/*if((temp = (double*)calloc(numvar,sizeof(double)))==NULL){*/
	if((temp = (double*)malloc(numvar*sizeof(double)))==NULL){
		fprintf(stderr,"not enough memory for temp\n");
		exit(EXIT_FAILURE);
	}
	initial = (double*)calloc(numvar,sizeof(double));
	xy_file = (char*)calloc(12,sizeof(char));

	/* copy parset input variables values to initial array */
	for(i=0;i<numvar;i++){
	  	initial[i] = parset[i];
	}

	run_info = fopen("run_info","a");
	if(run_info==NULL){
		fprintf(stderr,"FILE run_info cannot be opened\n");
		exit(1);
	}

	/*
	seed = fopen("seed","r");
	if(seed==NULL){
		fprintf(stderr,"FILE seed cannot be opened\n");
		exit(1);
	}
	fscanf(seed,"%d",&seed_read);
	*/

	/* initialize pseudo-random number generator */
	g = prng_new("eicg(2147483647,111,1,0)");

	/* seed random number generator with time of day */
	gettimeofday(&tv,&tz);
	if(prng_can_seed(g)) prng_seed(g,tv.tv_sec);
	fprintf(run_info,"%d\n",tv.tv_sec);

	/*
	if(prng_can_seed(g)) prng_seed(g,seed_read);
	fprintf(run_info,"%d\n",seed_read);
	*/

	/* open output files */
	outfile = fopen("stats","w");
	if(outfile==NULL){
		fprintf(stderr,"FILE stats cannot be opened\n");
		exit(1);
	}
#ifdef SAVEHIST
	history = fopen("history","w");
	if(history==NULL){
		fprintf(stderr,"FILE history cannot be opened\n");
		exit(1);
	}
#endif

	/* set required data... */ 
	/*read_parameters("gadat");  * read parameter values from input file */
	/*printf("parameters read\n");*/
	/*fflush(NULL);*/
	generate_func = 'd';
	set_generate();/* set population generation function to be used */
	set_fitness(); /* set fitness function to be used */
	set_select();  /* set selection method using value of select_method 
                   * set in read_parameters() */
	set_encode();  /* set encode method using value of encode_method
						 * set in read_parameters() */
	set_choose_newpop();

#ifdef SAVEHIST
	/* print initial information to history file */
	fprintf(history,"numvar=%d\n",numvar);
	fprintf(history,"param set received: ");
	for(i=0;i<numvar;i++){
		fprintf(history,"%f ",initial[i]);
	}
	fprintf(history,"\ngradient values: ");
	for(i=0;i<numvar;i++){
		fprintf(history,"%f ",pargrd[i]);
	}
#endif

	alloc_memory();    /* allocate required memory */
	printf("memory allocated...\n");
	fflush(NULL);

	initpop(initial); /* generate an initial population */
	printf("Population of %d individuals has been initialized\n",popsize);
	fflush(NULL);

	/* create filename for parameter data */
	sprintf(xy_file,"xy_gen_0");
	xy_data = fopen(xy_file,"w");
	if(xy_data==NULL){
		fprintf(stderr,"FILE %s cannot be opened\n",xy_file);
		exit(1);
	}

	/* print individual variable information for the initial population */
	print_params(xy_data);
	fclose(xy_data);

#ifdef SAVEHIST
	/* Set fitness of generated individuals */
	fprintf(history,"initializing fitness values...\n");
#endif
	init_fitness();

#ifdef SAVEHIST
	/* normalize the fitness values of the initial population. */
	fprintf(history,"normalizing fitness values...\n");
#endif
	normalize(oldpop); 

	/* determine best individual in generated population */
	find_best(oldpop);

	/* update stats file */
	print_stats();
	fflush(NULL);

#ifdef SAVEHIST
	/* print initial population info to history file */
	fprintf(history,"Initial population:\n");
	fprintf(history,"-------------------\n");
	for(i=0;i<popsize;i++){
		fprintf(history,"oldpop %d = ",i);
		print_ind(oldpop[i]);
	}
	fprintf(history,"best individual in initial population: ");
	print_ind(best);
#endif

	/* MAIN LOOP OF CODE... */ 
		/* start at generation 1 since generation 0 is just a random sample */
	for(generation=1;generation<=maxgen;generation++){
		if(generation%gen_inc==0){
			/* create filename for parameter data */
			sprintf(xy_file,"xy_gen_%d",generation);
			xy_data = fopen(xy_file,"w");
			if(xy_data==NULL){
				fprintf(stderr,"FILE %s cannot be opened\n",xy_file);
				exit(1);
			}
			/* print parameter information every few generations for a scatter plot */
			print_params(xy_data);
			fclose(xy_data);
		}

#ifdef SAVEHIST
		fprintf(history,"\nGeneration %d\n",generation);
		fprintf(history,"-------------\n");

		/* generate offspring by performing selection, crossover, and mutation */
		fprintf(history,"breeding population...\n");
#endif
		breed(initial);

#ifdef SAVEHIST
		/* calculate the fitness of the offspring produced */
		fprintf(history,"calculating offspring fitness values...\n");
#endif
		offspring_fit();

#ifdef SAVEHIST
		/* normalize the fitness values of the offspring population. */
		fprintf(history,"normalizing offspring fitness values...\n");
#endif
		normalize(offspring);  /* normalize all fitness values */

#ifdef SAVEHIST
		/* print offspring information to history file */
		fprintf(history,"Offspring Created:\n");
		fprintf(history,"-----------------\n");
		for(i=0;i<popsize;i++){
			fprintf(history,"offspring %d = ",i);
			print_ind(offspring[i]);
		}

		/* choose individuals from oldpop and offspring for next generation */
		fprintf(history,"choosing individuals for next generation...\n");
#endif
		choose_newpop();

		/* normalize and calculate stats for members of the new population */
		normalize(newpop);

#ifdef SAVEHIST
		/* print newpop information to history file */
		for(i=0;i<popsize;i++){
			fprintf(history,"newpop %d = ",i);
			print_ind(newpop[i]);
		}
#endif

		/* find the best individual in the newly created population */
		find_best(newpop);

		/* update stats file */
		print_stats();
		fflush(NULL);

		if(best.grdlth < 5.0E-5){
		/* structure converged to a satisfactory gradient length */
			printf("Structure converged! gradient length=%g\n",best.grdlth);
			break;
		}

#ifdef SAVEHIST
		fprintf(history,"Best individual thus far: ");
		print_ind(best);
		fprintf(history,"\n");
#endif

		/* advance generation */
		temp_pop = oldpop;
		oldpop = newpop;
		newpop = temp_pop;
	} /* END OF MAIN LOOP */

#ifdef SAVEHIST
	fprintf(history,"\n");

	/* print summary of optimum information to history file */
	fprintf(history,"GA complete...performed %d generations\n",generation);
	fprintf(history,"*****Best Individual*****\n");
	fprintf(history,"--Chromosome string: ");
	print_chrom(history,best.chrom,chromlength);
#endif

	/* decode variables of best individual for printing */
	decode(best.chrom,temp);

#ifdef SAVEHIST
	fprintf(history,"\n --Parameters : ");
	for(i=0;i<numvar;i++){
		fprintf(history,"%f ",temp[i]);
	}
	fprintf(history,"\n --Gradient : ");
	for(i=0;i<numvar;i++){
		fprintf(history,"%14.10g ",best.grad[i]);
	}
	fprintf(history,"\n");
	fprintf(history,"--Fitness : %f\n", best.fitness);
	fprintf(history,"--Gradient Length : %14.10g\n", best.grdlth);
	fprintf(history,"--Negative eigenvalues = %d\n",best.negeig);
	fprintf(history,"--Generation : %d\n", best.generation);
#endif

  /*
	 * Print out the best individuals stats to the stats file... 
	 */

	fprintf(outfile,"# Best individual's stats:\n");
	fprintf(outfile,"# Gen Fitness GrdLth ");
	for (i=0;i<numvar;i++) {
		fprintf(outfile,"g[%d] ",i);
	}
	fprintf(outfile,"NegEig");
	for (i=0;i<numvar;i++) {
		fprintf(outfile," x[%d]",i);
	}
	fprintf(outfile,"\n");
	fprintf(outfile,"# %d %14.10g %14.10g",
			best.generation, best.fitness, best.grdlth);

	for(i=0;i<numvar;i++) {
		fprintf(outfile," %14.10g",best.grad[i]);
	}
	fprintf(outfile," %d",best.negeig);
	decode(best.chrom,temp);
	for(i=0;i<numvar;i++) {
		fprintf(outfile," %14.10g",temp[i]);
	}
	fprintf(outfile,"\n");

	fflush(NULL);

	/* reset the random number generator */
	prng_reset(g);
	/* deallocate the random number generator object */
	prng_free(g);

	/* flush and close any files that are open... */
#ifdef SAVEHIST
	fclose(history);
#endif
	fclose(outfile);
	fclose(run_info);

	/* free all memory allocated */
	free(temp);
	free_memory();

	return;
}
