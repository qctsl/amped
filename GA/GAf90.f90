      subroutine GA_f90
!*****************************************************************************************************************
!     Date last modified: August 4, 2003                                                            Version 1.0  *
!     Author: A. Sivret and R.A. Poirier                                                                         *
!     Description: Subroutine to create different niches of population.                                          *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE OPT_defaults
      USE OPT_objects
      USE GA_defaults
      USE type_GA

      implicit none

      integer Igene,Iniche,ipos,unit
      integer Iindividual
      integer ,dimension(Nniches) :: niche_unit
      logical :: Lerror, Lexist
      logical :: Eig_search
      character(len=16), dimension(Nniches) :: GAfile_name
      character(len=32) format
      character(len=128) string
!!$OMP THREADPRIVATE(PARSET,PARGRD,Lacceptable)
!
! Begin:
      call PRG_manager ('enter', 'GA_f90', 'UTILITY')
!
! Initialization:
      call GET_object ('OPT', 'PARAMETERS', OPT_parameters%name)
      Ngenes=Noptpr                      ! Number of genes for each chromosomes
      Nindividuals=PopSize               ! Number of individuals in the population
      kt=0.005 ! Used by GA_step_func
! Find out if the program will look for a saddle point, a minimum or anything
      Eig_search=.true.
      kt_2=2.0D0
      if(POINT_type(1:1).eq.'S')then
        Neig_wanted=1
      else if(POINT_type(1:3).eq.'MIN')then
        Neig_wanted=0
        kt_2=-kt_2
      else if(POINT_type(1:3).eq.'MAX')then
        Neig_wanted=Ngenes
      else
        Eig_search=.false.
      end if
!
      Lacceptable=.true.
      GAfile_name='NICHE'

      allocate (oldpop(Nindividuals))
      allocate (offspring(Nindividuals))
      allocate (newpop(Nindividuals))
!
! Array allocated to save the first individual for each niche
      allocate (save_parset(1:Ngenes))
!
! Allocate chromosomes for each individual
      do Iindividual=1,Nindividuals
        allocate (oldpop(Iindividual)%chromosome(Ngenes))
        allocate (offspring(Iindividual)%chromosome(Ngenes))
        allocate (newpop(Iindividual)%chromosome(Ngenes))
      end do ! Iindividual
!
! Allocate the best individual
      allocate (best%chromosome(Ngenes))
!
! Determine Chromosome_format
      write(Chromosome_format,'(a,I8,2a)')"(",Ngenes,Gene_format(1:len_trim(Gene_format)),")"
!
! Determine a new format for writing a chromosome and his fitness to files
      ipos=index(Chromosome_format, ')')
      format(1:)=Chromosome_format
      format(ipos:)=',1PE14.4)'
!
! Determine Gene_length
      write(string,FMT=Chromosome_format)parset(1)
      Gene_length=len_trim(string)
! NOTE: Gene_length*Ngenes cannot exceed some limit (about 128,000,000)
! Determine up_low_bound
      call GET_object ('OPT', 'FUNCTION', OPT_function%name)
      if(POINT_type(1:3).eq.'MIN')then
        up_low_bound=OPT_function%value-dabs(OPT_function%value)*0.1D0 ! FOr now???????
      else
        up_low_bound=OPT_function%value
      end if
!
! Write GA parameters used
      write(UNIout,'(/a/)')'**Genetic-Algorithm optimization (Fortran Version: July 16, 2003)**'
      write(UNIout,'(a,a6,a,a14,2a)')'Point type= ',Point_type,' NEWPOP method= ',NEWPOP_method, &
                                  ' GA mode= ',GA_mode
      write(UNIout,'(5(a,i4))')'Nniches= ',Nniches,' MAX generations= ',MAXgen,' Individuals= ',Nindividuals, &
                               ' Neig_wanted= ',Neig_wanted,' Gene_length= ',Gene_length
      write(UNIout,'(3(a,F10.4))')'CROSSprob= ',CROSSprob,' MUTprob= ',MUTprob,' TOURprob= ',TOURprob
      write(UNIout,'(a,i4,a,F10.4)')'TOURsize= ',TOURsize,' MAX_mutation= ',MAX_mutation
      write(UNIout,'(2(a,F10.4))')'PARAMscale',PARAMscale,' GA_convergence= ',GA_convergence
      write(UNIout,'(3(a,1PE11.4))')'Tolerance',Tolerance,' GA_epsilon= ',GA_epsilon,' VALID= ',VALID
      write(UNIout,'(2(a,F10.4))')'lower bound= ',lower_bound,' upper bound= ',upper_bound
      write(UNIout,'(a,F12.6,2(a,F10.4))')'up_low_bound= ',up_low_bound,' kt= ',kt,' kt_2= ',kt_2
      write(6,'(a,a32)')'Chromosome format =',Chromosome_format
      write(6,'(a,a32)')'Chromosome file format =',format

      write(UNIout,'(a)')'Domains for each gene: High and Low'
      do Igene=1,Ngenes
        write(UNIout,'(2(F12.6,2x))')GA_domain(Igene)%high,GA_domain(Igene)%low
      end do
!
! Find one individual with the correct number of negative eigenvalues
! Generate population from that individual ( parset )
      oldpop(1)%chromosome(1:Ngenes)=parset(1:Ngenes)     ! start with same individual for each Niche
      if(Eig_search)then
        call GA_search
      end if
! Save parset for future niches
      save_parset(1:Ngenes)=parset(1:Ngenes)
! Main loop for the creation of niches, executes the genetic algorithm on each iteration
      NICHE_LOOP : do Iniche=1,Nniches
      print*,'       '
      print*,'**********************   NICHE # ',Iniche,'   **********************'
      print*,'      '

! 30/01/04 initialize the first individual and set to best
      oldpop(1)%chromosome(1:Ngenes)=save_parset(1:Ngenes)     ! start with same individual for each Niche
        call GA_main    ! executes all of the genetic algorithm

! Create the file name for the current niche
        if(Iniche .LT. 10) then
          write(GAfile_name(Iniche),'(a,i1)')'NICHE',Iniche
        else if (Iniche .LT. 100) then
          write(GAfile_name(Iniche),'(a,I2)')'NICHE',Iniche
        else if (Iniche .LT. 1000) then
          write(GAfile_name(Iniche),'(a,I3)')'NICHE',Iniche
        else
          write(UNIout,'(a)')'ERROR> GA_f90: Number of niches is incorrect'
          stop 'ERROR> GA_f90: Number of niches is incorrect'
        end if
!
! Creation of new files to hold newpop for each niche
        call GET_unit (GAfile_name(Iniche), unit, Lerror)
        niche_unit(Iniche)=unit
      write(UNIout,*)'Niche connected to unit ',unit
      PRG_file(unit)%unit=unit
      PRG_file(unit)%status='NEW '
      PRG_file(unit)%action='WRITE '
      PRG_file(unit)%type='NICHE '
      PRG_file(unit)%form='FORMATTED '
      PRG_file(unit)%name=GAfile_name(Iniche)
      call FILE_open (unit)
      PRG_file(unit)%status='OLD '
!
! Writes newpop (last generation) for each niches into files
        write(niche_unit(Iniche),FMT='(a)')'Genes and  fitness for last generation'
        do Iindividual=1,Nindividuals
          write(niche_unit(Iniche),FMT=format)NEWPop(Iindividual)%chromosome(1:Ngenes), &
                                newpop(Iindividual)%fitness
        end do ! Iindividual
!
!        ParamScale=ParamScale * 4

      end do NICHE_LOOP

!     do Iindividual=1,Nindividuals
!       deallocate (oldpop(Iindividual)%chromosome)
!       deallocate (offspring(Iindividual)%chromosome)
!       deallocate (newpop(Iindividual)%chromosome)
!     end do ! Iindividual

      deallocate (oldpop)
      deallocate (offspring)
      deallocate (newpop)
!
! Now delete all the NICHE files
      NICHE_FILES : do Iniche=1,Nniches
        call FILE_delete(GAfile_name(Iniche), Lerror)
      end do NICHE_FILES

! End of routine GA_f90
      call PRG_manager ('exit', 'GA_f90', 'UTILITY')
      return
      contains

      subroutine GA_main
!*****************************************************************************************************************
!     Date last modified: July 28, 2003                                                             Version 1.0  *
!     Author: A. Sivret and R.A. Poirier                                                                         *
!     Description: Fortran 90 version of Genetic Algorithm.                                                      *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE OPT_defaults

      implicit none
!
! Local scalars:
      integer I,Iindividual
!
! Begin:
      call PRG_manager ('enter', 'GA_main', 'UTILITY')
!

      MUN_prtlev=MUN_prtlev-1
!
! Generate the initial population
      generation=1
      call GA_generate
!
! Now compute the fitness for each individual:
      write(UNIout,'(a)')'Individual  fitness   gradient length   function value'
!!$OMP parallel do private(Iindividual, parset, pargrd) shared(oldpop, Nindividuals)
!!$OMP& firstprivate(Lacceptable)
      do Iindividual=1,Nindividuals
        call GA_fitness (oldpop, Iindividual)
      end do
!        call GA_fitness_all (oldpop)

      call PRT_GA_fitness (oldpop)
!
! Find the best individual in oldpop
      call GA_find_best(oldpop)

! The Main Loop of Code for the Genetic Algorithm
      GA_MAIN_LOOP : do generation=2,maxgen
        write(6,*)'*****Generation*****: ',generation
        if(MOD(generation,PRT_gen_inc).EQ.0)then  ! print?
          write(UNIout,'(a,i6,a,1PE14.4)')'Best fitness generation ',best%generation, &
                                          ' : best%fitness ', best%fitness
        end if

        call GA_breed

        do Iindividual=1,Nindividuals
          call GA_fitness (offspring, Iindividual)
        end do

        call GA_choose_newpop

! Print new generation
        write(UNIout,'(a)')'Individual  fitness   gradient length   function value'
        call PRT_GA_fitness (newpop)
        call GA_find_best (newpop)

! If gradient_length < OPTACC, than the GA has converged and can be stopped
        if(best%gradient_length.lt.OPTACC)then
          write(UNIout,'(a)')'GA_f90 has converged within OPTACC (grdlth)'
! Print final results:
        write(UNIout,'(a,i5)')'Results (BEST) at Generation ',Generation
        parset(1:Ngenes)=best%chromosome(1:Ngenes)
        write(UNIout,'(a/1X,6(I6,1PE16.7))')'Final parameters: ',(I,parset(I),I=1,Ngenes)
        write(UNIout,'(a,1PE16.7)')'Final Gradient length = ',best%gradient_length
        write(UNIout,'(a,F17.9/)')'Final OPT_function%value = ',best%func_value
        call OPT_final_results
!
        exit GA_MAIN_LOOP
        else if(best%gradient_length .LT.GA_convergence)then
          write(UNIout,'(a)')'GA_f90 has converged within GA_convergence - switching method '
          OPT_method='VA'
          parset(1:Ngenes)=best%chromosome(1:Ngenes)
          call optclc
          exit GA_MAIN_LOOP
        end if
!
        oldpop=newpop !  oldpop becomes newpop

      end do GA_MAIN_LOOP

! End the Main Loop of Code for the Genetic Algorithm
!      write(UNIout,'(a)')'ERROR> GA_f90: did not converge to desire accuracy'
!
! End of routine GA_main
      call PRG_manager ('exit', 'GA_main', 'UTILITY')
      return
      end subroutine GA_main
      end subroutine GA_f90

      subroutine GA_generate
!*****************************************************************************************************************
!     Date last modified: May 30, 2003                                                              Version 1.0  *
!     Author: A. Sivret and R.A. Poirier                                                                         *
!     Description: Generate initial population from first individual.                                            *
!*****************************************************************************************************************
! Modules:
      USE program_constants
      USE OPT_objects
      USE GA_defaults
      USE type_GA

      implicit none
!
! Local scalars:
      integer Iindividual,Jgene,Ioptpr
!
! Local work array:
      double precision, dimension(:), allocatable :: Perturbation
!
! Begin:
      call PRG_manager ('enter', 'GA_generate', 'UTILITY')

! Find out what the encode method is ( multiplicative or range-encoding)
      allocate (Perturbation(1:Ngenes))
      Perturbation(1:Ngenes)=ZERO

! Add a random number to each gene of the initial individual to create a population
! The number will have a range between -ParamScale and +ParamScale
! The population generated will be in the oldpop array and each individual will have a fitness
      call random_seed
      if(encode(1:1).EQ.'M')then
        do Iindividual=2,Nindividuals
          call random_number (Perturbation) ! find random number between 0 and 1
          call GA_pos_neg (perturbation, Ngenes)
          oldpop(Iindividual)%chromosome(1:Ngenes)=oldpop(1)%chromosome(1:Ngenes)+ &
                                                   ParamScale*Perturbation(1:Ngenes)
        end do ! Individual
      else
        write(6,*)'INTERVAL encoding not available yet'
        stop 'INTERVAL encoding not available yet'
      end if

      deallocate (Perturbation)
!
! End of routine GA_generate
      call PRG_manager ('exit', 'GA_generate', 'UTILITY')
      return
      end subroutine GA_generate
      subroutine GA_fitness (curpop, Iindividual)
!*****************************************************************************************************************
!     Date last modified: June 19, 2003                                                             Version 1.0  *
!     Author: A. Sivret and R.A. Poirier                                                                         *
!     Description: Generate fitness value of individuals                                                         *
!*****************************************************************************************************************
! Modules:
      USE program_constants
      USE OPT_defaults
      USE OPT_objects
      USE GA_defaults
      USE type_GA

      implicit none
!
! Input scalars:
      integer, intent(IN) :: Iindividual
!
! Local scalars:
      integer :: Jgene,Ioptpr,Neigenval
      double precision :: fitness,fit_grdlen
      double precision :: global,EXParg
!
      type (type_individual) :: curpop(Nindividuals)
!
! Local function:
      double precision OPT_gradient_length
!
! Begin:
      call PRG_manager ('enter', 'GA_fitness', 'UTILITY')
!
        call GA_step_func (curpop, Iindividual)
        fitness=ZERO
        global=ZERO
        fit_grdlen=10.0D0
! For a step function < 0.01, then the indv. is considered out of the domain and the fitness value is very low
      if(curpop(Iindividual)%step_function.LT.0.01)then
        IN_OUT_bound='OUT of DOMAIN'
        curpop(Iindividual)%fitness = fitness
        curpop(Iindividual)%global=global
        curpop(Iindividual)%func_value=ZERO
        curpop(Iindividual)%gradient_length=10.0D0

! If the indv. is in the domain, the fitness value has almost no changes
      else

        IN_OUT_bound='IN the DOMAIN'
        parset(1:Ngenes)=curpop(Iindividual)%chromosome(1:Ngenes)

        call OPT_TO_COOR
        if(Lacceptable)then
          call GET_object ('OPT', 'FUNCTION', OPT_function%name)
          call GET_object ('OPT', 'GRADIENTS', OPT_parameters%name)
!
          fit_grdlen=OPT_gradient_length (PARGRD, Ngenes) ! Calculate the gradient length
!
          fitness=ONE/(fit_grdlen+GA_epsilon)
!         print*,'fitness(just gradient): ', fitness
!         print*,'up_low_bound: ', up_low_bound
!       call flush(6)
!       write(6,*)'OPT_function%value: ',OPT_function%value
!       call flush(6)
          EXParg=(OPT_function%value-up_low_bound)/kt_2
          if(EXParg.lt.500.0D0)then  ! need a better fix
          global=dexp(EXParg)
          else
          global=1000.0D0
          end if
!         print*,'global : ', global
!       call flush(6)
          fitness=fitness * global
!         print*,'fitness(with global): ', fitness
!       call flush(6)

          fitness=fitness*curpop(Iindividual)%step_function
!         print*,'fitness(with step f): ', fitness
!       call flush(6)
          curpop(Iindividual)%fitness=fitness
          curpop(Iindividual)%global=global
          curpop(Iindividual)%func_value=OPT_function%value
          curpop(Iindividual)%gradient_length=fit_grdlen
        else ! Geometry unacceptable
          curpop(Iindividual)%fitness =fitness
          curpop(Iindividual)%global=global
          curpop(Iindividual)%func_value=ZERO
          curpop(Iindividual)%gradient_length=fit_grdlen
        end if
      end if
      print*,'GA_fitness> Iindividual,pargrd: ',Iindividual,pargrd
!
! End of routine GA_fitness
      call PRG_manager ('exit', 'GA_fitness', 'UTILITY')
      return
      end subroutine GA_fitness
      subroutine GA_superfit(best_ind)
!*****************************************************************************************************************
!     Date last modified: June 6, 2003                                                              Version 1.0  *
!     Author: A. Sivret                                                                                          *
!     Description:  Calculate the second derivative wich gives out the number of negetive eigenvalues and a      *
!     higher fitness value                                                                                       *
!*****************************************************************************************************************
! Modules:
      USE program_constants
      USE program_objects
      USE OPT_objects
      USE type_GA

      implicit none

! Input derived type:
      type (type_individual_best) :: best_ind

! Local scalars:
       integer Neigenval

! Begin:
      parset(1:Ngenes)=best_ind%chromosome(1:Ngenes)  ! give parset the value of the newly created indv.
!
! Clear stack
      call OPT_TO_COOR
      call KILL_objects (.true.)

      call GA_get_Neigenval (Neigenval)    ! call to calculate the hessian and number of neg. eigenvalues
      best_ind%Neigval=Neigenval
!
      return
      end subroutine GA_superfit
      subroutine GA_get_Neigenval (Neigenval)
!*****************************************************************************************************************
!     Date last modified: May 20, 2003                                                              Version 1.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: calculate the hessian and the number of negetive eigenvalues                                  *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE OPT_defaults
      USE OPT_objects
      USE GA_defaults
      USE mod_type_hessian

      implicit none

! Output scalars:
      integer Neigenval
!
! Local scalars:
      integer i
!
! Local arrays:
      double precision, dimension(:), allocatable :: eigval
      double precision, dimension(:,:), allocatable :: eigvec

      allocate (eigval(noptpr), eigvec(noptpr,noptpr))

      call GET_object ('OPT', 'HESSIAN', Hessian_method(1:len_trim(Hessian_method)))
!
      call diag_hessian (Hessian_matrix, noptpr, eigval, eigvec, .false.)
!
      Neigenval = 0

      do i=1,Noptpr
        if(eigval(i).lt.(Tolerance)) then
          Neigenval = Neigenval + 1
        end if
      end do

!      write(UNIout,*)'GA_get_Neigenval> Number of negative eigenvalues=',Neigenval

      deallocate (eigval, eigvec)

      return
      end
      subroutine GA_pos_neg (Perturbation,Ngenes)
!*****************************************************************************************************************
!     Date last modified: May 30, 2003                                                              Version 1.0  *
!     Author: A. Sivret                                                                                          *
!     Description: Functions takes a positive real number and randomly assigns a positive or negative value      *
!*****************************************************************************************************************
! Modules:
      USE program_constants

      implicit none

! Input Scalars:
      integer, intent(IN) :: Ngenes

!
! Output array:
      double precision Perturbation(Ngenes)

! Local Scalars:
      double precision target
      integer Igene,r
!
! Begin:
      do Igene=1,Ngenes
      call random_number(target)
      r=int(TWO*target)           ! generate a random integer between 0 and 1
      if(r.NE.0)then
        Perturbation(Igene)=-Perturbation(Igene)  ! half the time will be negative
      end if
      end do
!
      return
      end subroutine GA_pos_neg
      subroutine GA_manage
!*****************************************************************************************************************
!     Date last modified: June 19, 2003                                                             Version 1.0  *
!     Author: A. Sivret                                                                                          *
!     Description:  Subroutine searches a population until it finds an individual with the correct number of     *
!     negative eigenvalues                                                                                       *
!*****************************************************************************************************************
! Modules:
      USE program_constants
      USE OPT_objects
      USE type_GA

      implicit none
!
! Begin:
      call PRG_manager ('enter', 'GA_manage', 'UTILITY')
!
      best%chromosome(1:Ngenes)=oldpop(1)%chromosome(1:Ngenes)
!      best%fitness=oldpop(Iindividual)%fitness
      call GA_superfit (best)                            ! Find the number of negetive eigenvalues
      if (best%Neigval .EQ. Neig_wanted) then
        print*,'Found it!'
      else
        print*,'Not yet!'
      end if

      parset(1:Ngenes)=best%chromosome(1:Ngenes)

! End of routine GA_manage
      call PRG_manager ('exit', 'GA_manage', 'UTILITY')
      return
      end subroutine GA_manage
      subroutine GA_search
!*****************************************************************************************************************
!     Date last modified: February 4, 2004                                                          Version 1.1  *
!     Author: A. Sivret and R.A. Poirier                                                                         *
!     Description:  Generate one individual until someone with the correct number of negetive eigenvalues is     *
!     created, and he must also be inside the specified domain                                                   *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE OPT_objects
      USE GA_defaults
      USE type_GA

      implicit none
!
! Local scalars:
      integer Niteration,MAX_iterations
      double precision temp_var
! Local work array:
      double precision, dimension(:), allocatable :: Perturbation
!
! Begin:
      call PRG_manager ('enter', 'GA_search', 'UTILITY')

      MUN_prtlev=MUN_prtlev-1 ! prevent printing of hessian evaluation
      allocate (Perturbation(1:Ngenes))
      Perturbation(1:Ngenes)=ZERO

      MAX_iterations = 2500
      Niteration = 1

      m_i : do while (Niteration .LE. MAX_iterations)
        print*,'Search Iteration : ' , Niteration

        call GA_step_func(oldpop,1)                  ! make sure the new indv. is in the domain
        if(oldpop(1)%step_function.GT.0.1)then
          call GA_manage                   ! Find number of neg. eigenvalues the indv. has, this will determine
          if(best%Neigval.EQ.Neig_wanted)then ! if we are searching for a saddle point,max or min
!            ParamScale=ParamScale/4.00
!            if((POINT_type(1:3).eq.'MAX').or.(POINT_type(1:3).eq.'MIN'))then
!              call GA_fitness(oldpop,1)
!              best%fitness=oldpop(1)%fitness
!              best%func_value=OPT_function%value
!              temp_var=1*best%func_value
!              up_low_bound=best%func_value+temp_var
!            end if
            exit m_i
          end if ! best%Neigval.EQ.Neig_wanted
        end if ! oldpop(1)%step_function.GT.0.1
! Moved here so that the first individual is tested 3/2/4 RP
        call random_number (Perturbation) ! find random number between 0 and 1
        call GA_pos_neg (perturbation, Ngenes)
        oldpop(1)%chromosome(1:Ngenes)=parset(1:Ngenes)+ParamScale*Perturbation(1:Ngenes) ! create a new indiv.
        Niteration=Niteration+1
      end do m_i

      parset(1:Ngenes)=oldpop(1)%chromosome(1:Ngenes)  ! Save this individual to parset
      deallocate (Perturbation)

      if(best%Neigval.ne.Neig_wanted)then
        write(UNIout,'(a)')'ERROR> GA_f90: Could not find correct number of neg. eigenvalues'
        stop 'ERROR> GA_f90: Could not find correct number of neg. eigenvalues'
      end if
!
! End of routine GA_search
      call PRG_manager ('exit', 'GA_search', 'UTILITY')
      return
      end subroutine GA_search
      subroutine GA_step_func (curpop, Iindividual)
!*****************************************************************************************************************
!     Date last modified: June 19, 2003                                                             Version 1.0  *
!     Author: A. Sivret                                                                                          *
!     Description: Function that calculates if an individual is in the specified domain                          *
!*****************************************************************************************************************
! Modules:
      USE program_constants
      USE OPT_objects
      USE type_GA

      implicit none
!
! Input scalars:
      integer, intent(IN) :: Iindividual
      double precision X_high,X_low,exparg

! Input derived type
      type (type_individual) :: curpop(Nindividuals)

! Local scalars
      integer :: Igene
      double precision :: f_x    ! the result of the step function , f(x)

! Begin:
      call PRG_manager ('enter', 'GA_step_func', 'UTILITY')
!
      curpop(Iindividual)%step_function=1.0D0

      do Igene=1,Ngenes
        x_high=GA_domain(Igene)%high
        x_low=GA_domain(Igene)%low
        exparg=(curpop(Iindividual)%chromosome(Igene)-x_low)*(curpop(Iindividual)%chromosome(Igene)-x_high)/kt

        if(exparg.gt.45.0D0)then
          f_x=ZERO
        else if(exparg.lt.-45.0D0)then
          f_x=ONE
        else
          f_x=ONE/(dexp(exparg)+ONE)
        end if

        curpop(Iindividual)%step_function=curpop(Iindividual)%step_function*f_x
!        write(6,*)'f_x : ', f_x, '    step_function :  ' , curpop(Iindividual)%step_function
      end do

!
! End of routine GA_step_func
      call PRG_manager ('exit', 'GA_step_func', 'UTILITY')
      return
      end subroutine GA_step_func
      subroutine PRT_GA_fitness (curpop)
!*****************************************************************************************************************
!     Date last modified: June 19, 2003                                                             Version 1.0  *
!     Author: A. Sivret                                                                                          *
!     Description: Function that calculates if an individual is in the specified domain                          *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE type_GA

      implicit none
!
! Local scalars:
      integer :: Iindividual
!
! Input array:
      type (type_individual) :: curpop(Nindividuals)
!
      do Iindividual=1,Nindividuals
        write(UNIout,'(i8,1PE14.4,2(E14.4,4x))') &
            Iindividual,curpop(Iindividual)%fitness,curpop(Iindividual)%gradient_length, &
            curpop(Iindividual)%func_value
        call flush (6)
      end do ! Iindividual
!
! End of routine PRT_GA_fitness
      call PRG_manager ('exit', 'PRT_GA_fitness', 'UTILITY')
      return
      end subroutine PRT_GA_fitness
      subroutine GA_fitness_all (curpop, Iindividual)
!*****************************************************************************************************************
!     Date last modified: June 19, 2003                                                             Version 1.0  *
!     Author: A. Sivret and R.A. Poirier                                                                         *
!     Description: Generate fitness value of individuals                                                         *
!*****************************************************************************************************************
! Modules:
      USE program_constants
      USE OPT_objects
      USE GA_defaults
      USE type_GA

      implicit none
!
! Input scalars:
      integer :: Iindividual
      logical :: Lacceptable ! Must be passed from distance matrix!!!!!
!
! Local scalars:
      integer Jgene,Ioptpr,Neigenval
      double precision fitness,fit_grdlen
      double precision global
!
      type (type_individual) :: curpop(Nindividuals)
!
! Local function:
      double precision OPT_gradient_length
!
! Begin:
      call PRG_manager ('enter', 'GA_fitness_all', 'UTILITY')
!
!!$OMP parallel do private(Iindividual, parset, pargrd, fitness, global, fit_grdlen, IN_OUT_bound)
!!$OMP& firstprivate(Lacceptable)
!!$OMP& private(OPT_function%value)
!!$OMP& shared(curpop, Nindividuals, Ngenes)
!      do Iindividual=1,Nindividuals
!      allocate (parset(1:Ngenes), pargrd(1:Ngenes))
!      parset(1:Ngenes)=ZERO
!      pargrd(1:Ngenes)=ZERO
      print*,'Iindividual,parset after do: ',Iindividual,parset
      call flush(6)
        call GA_step_func (curpop, Iindividual)
        fitness=ZERO
        global=ZERO
        fit_grdlen=10.0D0
! For a step function < 0.01, then the indv. is considered out of the domain and the fitness value is very low
      if(curpop(Iindividual)%step_function.LT.0.01)then
        IN_OUT_bound='OUT of DOMAIN'
        curpop(Iindividual)%fitness = fitness
        curpop(Iindividual)%global=global
        curpop(Iindividual)%func_value=ZERO
        curpop(Iindividual)%gradient_length=10.0D0

! If the indv. is in the domain, the fitness value has almost no changes

      else

        IN_OUT_bound='IN the DOMAIN'
      print*,'Iindividual,parset: ',Iindividual,parset
      call flush(6)
        parset(1:Ngenes)=curpop(Iindividual)%chromosome(1:Ngenes)

        call OPT_TO_COOR
        if(Lacceptable)then
          call GET_object ('OPT', 'FUNCTION', OPT_function%name)
          call GET_object ('OPT', 'GRADIENTS', OPT_parameters%name)
      print*,'GA_fitness_all> Iindividual,pargrd: ',Iindividual,pargrd
      call flush(6)
!
          fit_grdlen=OPT_gradient_length (PARGRD, Ngenes) ! Calculate the gradient length
!
          fitness=ONE/(fit_grdlen+GA_epsilon)
          print*,'fitness(just gradient): ', fitness
          print*,'up_low_bound: ', up_low_bound
          global=dexp((OPT_function%value - up_low_bound)/kt_2)
          fitness=fitness * global
          print*,'global : ', global
          print*,'fitness(with global): ', fitness

          fitness=fitness*curpop(Iindividual)%step_function
          print*,'fitness(with step f): ', fitness
          curpop(Iindividual)%fitness=fitness
          curpop(Iindividual)%global=global
          curpop(Iindividual)%func_value=OPT_function%value
          curpop(Iindividual)%gradient_length=fit_grdlen
        else ! Geometry unacceptable
          curpop(Iindividual)%fitness =fitness
          curpop(Iindividual)%global=global
          curpop(Iindividual)%func_value=ZERO
          curpop(Iindividual)%gradient_length=fit_grdlen
        end if
      end if
!      deallocate (parset, pargrd)
!      end do
!
! End of routine GA_fitness_all
      call PRG_manager ('exit', 'GA_fitness_all', 'UTILITY')
      return
      end subroutine GA_fitness_all
