#include <stdio.h>
#include <stdlib.h>
#include "prng.h"
#include "mun_ga_data_structs.h"
#include "mun_ga_utils.h"

extern struct prng *g;
extern int numvar;
#ifdef SAVEHIST
extern FILE* history;
#endif

int flip(double prob){
	double rand; 

	/* generate a random number between 0 and 1 */
	rand = prng_get_next(g);

	if(rand<=prob){
		return 1;
	}
	else{
		return 0;
	}
}

/* Copy contents of ind_source to ind_dest */
void copy_ind(Individual *ind_source, Individual *ind_dest){
 	int j;
	for(j=0;j<numvar;j++){
		ind_dest->chrom[j] = ind_source->chrom[j];
		ind_dest->grad[j] = ind_source->grad[j];
#ifdef SAVEHIST
//		fprintf(history,"ind_dest->grad[%d]=%f\n",j,ind_dest->grad[j]);
#endif
	}
	ind_dest->fitness = ind_source->fitness;
	ind_dest->normfit = ind_source->normfit;
	ind_dest->negeig = ind_source->negeig;
	ind_dest->grdlth = ind_source->grdlth;
	ind_dest->func_value = ind_source->func_value;

	return;
}
