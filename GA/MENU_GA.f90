      subroutine SET_DEFAULTS_GA
!********************************************************************
!     Description:  Genetic algorithm parameter defaults            *
!     Date last modified:  July 13, 2000
!     Author:  S. Bungay                                            *
!********************************************************************
! Modules:
      USE GA_defaults
!
! Local scalars:
      integer nlist
!
      POINT_type = 'SADDLE'  ! or MIN, MAX, require hessian, ALL/ANY (does not use hessian)
      POPSIZE = 100          ! individuals
      MAXGEN = 100           ! generations
      CROSSPROB = 0.75       ! crossover probability
      MUTPROB = 0.05
      BITS_PER_VAR = 24
      PARAMSCALE = 0.04      ! Maximum change allowed for each gene when generating the initial population
      MAX_mutation = 0.5     ! Maximum change allowed for each gene
      GA_convergence=0.001   ! Required convergence on gradient length before switching to another OPT method
      VALID = 0.08
      SELECT = 'TOURNAMENT'
      TOURSIZE = 6
      TOURPROB = 0.75
      ENCODE = 'MULTIPLICATIVE'
      BIN_REP = 'GRAY'
      NEWPOP_method = 'ABOVE_average'
      NNiches=10
      TOLERANCE = -0.001
      GA_epsilon = 0.000001
      PRT_gen_inc = 20           ! Generation increment
      Gene_format(1:)='F8.4 '
      GA_mode='LOCATE'

      allocate (domain(500)) ! For now!!!!
      domain(1:500)=ZERO
      nlist=0

!     call set_ga_params (point_type(1:1), popsize, maxgen, crossprob, mutprob, bits_per_var, paramscale, &
!                         valid, select(1:1), encode(1:1), domain, bin_rep(1:1), newpop_method(1:1), &
!                         tolerance, GA_epsilon, PRT_gen_inc, nlist)
!     call set_ga_tour(toursize, tourprob)

      deallocate (domain)
      return
      end subroutine SET_DEFAULTS_GA
      subroutine MENU_GA
!***********************************************************************
!     Date last modified:  July 13, 2000                               *
!     Author: S. Bungay                                                *
!     Description: Set genetic algorithm parameters.                   *
!***********************************************************************
! Modules:
      USE program_constants
      USE program_parser
      USE menu_gets
      USE GA_defaults

      implicit none

! Local scalars:
      logical done
      integer length,nlist
!
! Begin:
      call PRG_manager ('enter', 'MENU_GA', 'UTILITY')

      allocate (domain(500)) ! For now!!!!
!
! Defaults:
      done=.false.

!Menu:
      do while (.not.done)
         call new_token('GA(GENETIC_ALGORITHM):')

         if(token(1:4).eq.'HELP')then
            write(UNIout,'(a)') &
      ' Command GA:', &
      '   Purpose: Perform a genetic algorithm optimization ', &
      '   Syntax :', &
      '     BITS_per_var =  <integer> ', &
      '     BIN_rep = <string>,  BINary or GRAY (default) ', &
      '     CROSSprob = <double precision>, Crossover probability ', &
      '     DOMain = <double precision list> ', &
      '     A1= <double precision>, B1 = <double precision>,...,Ak = <double precision>', &
      '     Bk = <double precision> (k = number of variables) ', &
      '         --[A1,B1]...[Ak,Bk] are required only if ENCode = INTerval', &
      '     ENCode = <string>, INTerval or MULTiplicative (default)', &
      '     EPSilon = <double precision> ', &
      '     GA_convergence = <double precision> Convergence on gradient length', &
      '     GENe_format = <string> , i.e., F10.6', &
      '     MAXGeneration = <integer> Maximum number of generations allowed', &
      '     MAXMutation = <real> Maximum change allowed for each gene', &
      '     MUTationProb = <double precision> ', &
      '     NEWpop_method = <string> OFFspring or ABOVE_average (default)', &
      '     NNIche = <integer>, Total number of niches created', &
      '     PARamscale = <double precision> must be in Angstrom', &
      '     POINTtype = <string>, MINimum, ANY or ALL, SADdle (default)', &
      '     POPsize = <integer>, Total number of individuals in the population, i.e., 100 ', &
      '     PRT_gen_inc = <integer>, Generation increment for which results are printed', &
      '     SELect = <string> i.e., ROUlette-wheel or TOURnament (default)', &
      '         --TSIZE and TPROB are required only if SELect = TOUR ', &
      '     TOLerance = <double precision> ', &
      '     TPROB = <double precision> Tournament probability', &
      '     TSIZE = <integer> Number of individuals in the tournament', &
      '     VALid  = <double precision> must be in Angstrom', &
      '     RUN command: Will cause execution', &
      '     end'
! BIN_rep
         else if(token(1:3).eq.'BIN')then
            call GET_value (bin_rep,length)
! BITS_per_var
         else if(token(1:4).eq.'BITS')then
            call GET_value (BITS_PER_var)
! CROSSprob
         else if(token(1:5).eq.'CROSS')then
            call GET_value (CROSSPROB)
! DOMains
         else if(token(1:3).eq.'DOM')then
            call GET_value (DOMAIN, nlist)   ! nlist should be Noptpr*2 (to be checked later)
            encode='INT'
! ENCode
         else if(token(1:3).eq.'ENC')then
            call GET_value (encode, length)
! EPSilon
         else if(token(1:3).eq.'EPS')then
            call GET_value (GA_epsilon)
! GAConvergence
         else if(token(1:3).eq.'GAC')then
            call GET_value (GA_convergence)
! GENe_format
         else if(token(1:3).eq.'GEN')then
            call GET_value (GENE_format, length)
! MAXGeneration
         else if(token(1:4).eq.'MAXG')then
            call GET_value (MAXGEN)
! MAXMutation
         else if(token(1:4).eq.'MAXM')then
            call GET_value (MAX_mutation)
! MUTprob
         else if(token(1:3).eq.'MUT')then
            call GET_value (MUTPROB)
! NEWpop_method
         else if(token(1:3).eq.'NEW')then
           call GET_value (NEWpop_method, length)
           if(NEWpop_method(1:1).eq.'O')then
             NEWpop_method(1:)='OFFSPRINGS'
           else if(NEWpop_method(1:1).eq.'A')then
             NEWpop_method(1:)='ABOVE_average'
           else
             write(UNIout,'(a)')'NEW population breeding method not available: ',NEWpop_method(1:14)
             stop 'NEW population breeding method not available'
           end if
! NNIche
         else if(token(1:3).eq.'NNI')then
            call GET_value (NNiches)
! PARamscale
         else if(token(1:3).eq.'PAR')then
            call GET_value (PARAMSCALE)
! POINT_type
         else if(token(1:5).eq.'POINT')then
            call GET_value (point_type, length)
! POPsize
         else if(token(1:3).eq.'POP')then
            call GET_value (POPSIZE)
! PRT_gen_inc
         else if(token(1:3).eq.'PRT')then
            call GET_value (PRT_gen_inc)
! SELect
         else if(token(1:3).eq.'SEL')then
            call GET_value (select, length)
! TOLerance
         else if(token(1:3).eq.'TOL')then
            call GET_value (TOLERANCE)
! TOURProb
         else if(token(1:5).eq.'TOURP')then
               call GET_value (TOURPROB)
! TOURSize
         else if(token(1:5).eq.'TOURS')then
               call GET_value (TOURSIZE)
! VALid
         else if(token(1:3).eq.'VAL')then
            call GET_value (VALID)
         else
            call MENU_end(done)
         end if
      end do

! Send parameter values to C code
!     call set_ga_params (point_type(1:1), popsize, maxgen, crossprob, mutprob, bits_per_var, &
!                         paramscale, valid, select(1:1), encode(1:1), domain, bin_rep(1:1), &
!                         NEWpop_method(1:1), tolerance, GA_epsilon, PRT_gen_inc, nlist/2)
      if(select(1:4).eq.'TOUR')then
!        call set_ga_tour (toursize, tourprob)
      end if

      deallocate (domain)
!
      call PRG_manager ('exit', 'MENU_GA', 'UTILITY')
      return
      end subroutine MENU_GA
