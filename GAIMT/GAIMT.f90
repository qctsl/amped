      subroutine GAIMT
!*******************************************************************************************
!     Date last modified: May 07, 2013                                        Version 1    *
!     Authors: Ahmad and R.A. Poirier                                                      *
!     Description: Solution of the GAIMT equations for spin-restricted open shell systems. *
!                                                                                          *
!     For printing in the code, you can use the following subroutines:                     *
!      1) PRT_Gaimt_S (Nbasis_G, Abasis, Bbasis, MATlen_G, MATlen_A, MATlen_B)                 *
!      2) PRT_Gaimt_H (Nbasis_G, Abasis, Bbasis, MATlen_G, MATlen_A, MATlen_B)                 * 
!      3) PRT_Gaimt_P (Nbasis_G, Abasis, Bbasis, MATlen_G, MATlen_A, MATlen_B)                 *
!      4) PRT_Gaimt_G (Nbasis_G, Abasis, Bbasis, MATlen_G, MATlen_A, MATlen_B)                 *
!      5) PRT_Gaimt_F (Nbasis_G, Abasis, Bbasis, MATlen_G, MATlen_A, MATlen_B)                 *
!      6) PRT_Gaimt_CAO (Nbasis_G, Abasis, Bbasis, MATlen_G, MATlen_A, MATlen_B)               *
!      7) PRT_Gaimt_Vne (Nbasis_G, Abasis, Bbasis, MATlen_G, MATlen_A, MATlen_B)               *
!      8) PRT_E_COMP  ! kinetic, potential(Vne), E_Vee(J+K), total energy, and virial.     *
!     These subroutines are in the module; QM/mod_gaimt_objects.f90.                       *
!*******************************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_objects                                                   
      USE INT_objects                                                   
      USE type_molecule
      USE type_basis_set                                            
      USE GRAPH_objects
      USE ixtrapolate                                                 
      USE GAIMT_objects  ! For declaring and allocating gaimt objects.
      USE matrix_print
!
      implicit none
!
! Local scalars:                                             
      integer :: ERR, Ibasis, Jbasis, Abasis, Bbasis
      integer :: MATlen_A, MATlen_B
      integer :: MATlen_G, Nbasis_G
! Local function:
      double precision :: ETRACE
!
! Begin:
      call PRG_manager ('enter', 'GAIMT', 'CMO%GAIMT')
!
! Obtain Nbasis_G and MATlen_G: 
      Nbasis_G=BASIS%Nbasis              ! Nbasis_G: total No of basis functions on a molecule. 
      MATlen_G=Nbasis*(Nbasis+1)/2       ! MATlen_G: No of elements in an UPT(NxN) matrix.
! Calculate number of basis functions on atoms A and B:
      Abasis=0                         ! Abasis: No of basis functions on atom A.   
      Bbasis=0                         ! Bbasis: No of basis functions on atom B.    
      do Iatom=1,molecule%Natoms
        do Ibasis=1,Nbasis_G
          if((Basis%AOtoAtom(Ibasis).eq.Iatom))then
            if(Iatom.eq.1)Abasis=Abasis+1
            if(Iatom.eq.2)Bbasis=Bbasis+1
          end if
        end do ! Ibasis
      end do ! Iatom
! Determine MATlen_A & MATlen_B:
      MATlen_A=Abasis*(Abasis+1)/2     ! MATlen_A: No of elements in an UPT(AxA) matrix.
      MATlen_B=Bbasis*(Bbasis+1)/2     ! MATlen_B: No of elements in an UPT(BxB) matrix.
!
! Allocate objects:
      call ALOCT_OBJS (Nbasis_G, Abasis, Bbasis, MATlen_G, MATlen_A, MATlen_B)
! Initialize GAIMT & get the guess:
      call INI_GAIMT
      if(.not.allocated(GuessCAO_A).or..not.allocated(GuessCAO_B))then
        allocate(GuessCAO_A(Nbasis_G,Nbasis_G), GuessCAO_B(Nbasis_G,Nbasis_G))
        allocate(CAO_Ai(Nbasis_G,Nbasis_G), CAO_Bi(Nbasis_G,Nbasis_G))
        call GET_IniGuess (GuessCAO_A, GuessCAO_B, Nbasis_G)
      end if ! not.allocated
      call GAIM_Guess
! Initial density matrices; P_A_alpha, P_A_beta, P_B_alpha, and P_B_beta.
      call GET_DenM 
      write(6,*)"GAIM's Electronic and Total Energies:"
!
! Start SCF cycling.    _______________________________________________________________
      converged=.FALSE.
      do while (.not.converged)  
! Form G matrices:
      call BLD_GgaimM (G_A_alpha, G_A_beta, G_B_alpha, G_B_beta, MATlen_G, Nbasis_G)
! Form F matrices, F_A_alpha, F_B_alpha, F_A_beta, and F_B_beta:
      call BLD_F
! Calculate the energy:
      call CAL_Energy
! Form F matrices (FgaimA & FgaimB) for atom A and atom B:
      call BLD_FgaimM
! Diagonalization of the FgaimA and FgaimB martices:
      CAO_Ai=CAO_A
      CAO_Bi=CAO_B
      TA=ZERO
      call MATRIX_diagonalize (FgaimM_A, TA, Eigval_A, 2, .true.)
      CAO_A=matmul(CAO_A, TA)
      TB=ZERO
      call MATRIX_diagonalize (FgaimM_B, TB, Eigval_B, 2, .true.)
      CAO_B=matmul(CAO_B, TB)
! Sort CAO_A and CAO_B:
      call SRT_CAO (CAO_Ai, CAO_A, Nbasis_G)
      call SRT_CAO (CAO_Bi, CAO_B, Nbasis_G)
! Form alpha and beta density matrices:
      call GET_DenM 
! Check for convergence and exit the loop:
      call CHK_Convergence
! End the SCF loop:
      end do ! WHILE __________________________________________________________________
! Save the guess:
      GuessCAO_A=CAO_A
      GuessCAO_B=CAO_B
! Deallocate GAIMT objects:
      call DEALOCT_OBJS 
! End of routine GAIMT
      call PRG_manager ('exit', 'GAIMT', 'CMO%GAIMT')
      return
      CONTAINS   !  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine INI_GAIMT
!***********************************************************************
!     Date last modified: May 09, 2013                                 *
!     Author: Ahmad and R.A. Poirier                                   *
!     Description: Initialize all GAIMT parameters.                    *
!***********************************************************************
! Modules:
      implicit none
!
! Local function:
      double precision GET_Enuclear
      character (len=8) :: States(4)
      data States/'Singlet','Doublet','Triplet','Quartet'/
! Local scalars:
       integer :: Ibasis, JbasisA, JbasisB
!
! Begin:
      call PRG_manager ('enter', 'INI_GAIMT', 'UTILITY')
!
! Intialize the energies
      ENERGY=>ENERGY_UHF
      ENERGY%nuclear=GET_Enuclear()
      Vnn_Gaim=ENERGY%nuclear
      ENERGY%HF=ZERO
      ENERGY_GAIMT=ZERO
      ENERGY_GAIMT_total=ZERO
!
! Intialize the G and F matrices: 
      G_A_alpha(1:MATlen_G)=ZERO
      G_A_beta(1:MATlen_G)=ZERO
      G_B_alpha(1:MATlen_G)=ZERO
      G_B_beta(1:MATlen_G)=ZERO
      F_A_alpha(1:MATlen_G)=ZERO
      F_A_beta(1:MATlen_G)=ZERO
      F_B_alpha(1:MATlen_G)=ZERO
      F_B_beta(1:MATlen_G)=ZERO
!
      Eigval_A(1:Nbasis_G)=ZERO
      Eigval_B(1:Nbasis_G)=ZERO
!
      OCC_A_alpha=ZERO
      OCC_A_beta=ZERO
      OCC_B_alpha=ZERO
      OCC_B_beta=ZERO
!
      PM0=>P_B  ! pointer
      P_A(1:MATlen_G)=ZERO
      P_B(1:MATlen_G)=ZERO
!
      E1=ZERO
      SCFCON=TEN
      Jcycle=0
   
!
      if(MUN_prtlev.GT.0)then
        write(UNIout,'(3a,F17.9,a)')'GAIMT Open Shell (',States(Multiplicity),') SCF, Nuclear Repulsion Energy: ', &
                                         ENERGY_UHF%nuclear,' Hartrees'
        write(UNIout,'(a,1PE12.4)')'Convergence on Density Matrix Required is ',SCFACC
        write(UNIout,'(a,6x,a)')'Cycle    Electronic Energy',' Total Energy    Convergence   Extrapolation'
      end if
!
! End of routine INI_GAIMT
      call PRG_manager ('exit', 'INI_GAIMT', 'UTILITY')
      return
      end subroutine INI_GAIMT
      subroutine GAIM_Guess
!************************************************************************
!     Date last modified: Sep 23, 2013                                  *
!     Author: Ahmad and R. A. Poirier                                   *
!     Description: Bluid the initial guess of GAIMT                     *
!************************************************************************
! Modules:
      implicit none
!
! Local scalars:
      integer :: Iatom,Jatom,Znum_X,Ielem
! Local array:
      double precision :: Vmol_A(MATlen_G), Vmol_B(MATlen_G)
!
! Begin:
! First get the one-electron integral for the molecule
      call GET_object ('INT', '1EINT', 'AO')
      call GET_object ('INT', '2EINT', 'RAW')
!
! Save H_A and H_B:
      Hmol=HCORE
      Vmol=VINT
      Tmol=TINT
      Smol=OVRLAP
      H_A=HCORE
      H_B=HCORE
!
      call KILL_object ('INT', '1EINT', 'AO')
! Get components for A---* and *---B:
      do Iatom=1,Natoms
          Znum_X=CARTESIAN(Iatom)%atomic_number
          call GET_IATOM(Znum_X)
          call GET_OCC_IATOM(Znum_X, Iatom)
        if(Natoms.eq.1)then                    ! Calculating for atoms
          call GET_object ('INT', '1EINT', 'AO')
          call GET_object ('QM', 'CMO', Wavefunction)
          if(Wavefunction.eq.'UHF')CAO_A=CMOA%coeff
          if(Wavefunction.eq.'RHF')CAO_A=CMO%coeff
          CAO_B=ZERO
        else
        do Jatom=1,Natoms                      ! Calculating for molecules
          Znum_X=CARTESIAN(Iatom)%atomic_number
          call GET_IATOM(Znum_X)
          call GET_OCC_IATOM(Znum_X, Iatom)
          if(Jatom.eq.Iatom)cycle
          CARTESIAN(Jatom)%atomic_number=-CARTESIAN(Jatom)%atomic_number
        end do! Jatom
          select case (Iatom)
            case (1)
              CAO_A=GuessCAO_A
            case (2)
              CAO_B=GuessCAO_B
            case default
              write(6,*)'ATOM IS NOT AVAILABLE'
            stop
          end select
        do Jatom=1,Natoms
          if(Jatom.eq.Iatom)cycle
          CARTESIAN(Jatom)%atomic_number=-CARTESIAN(Jatom)%atomic_number
        end do ! Jatom
      end if   ! Natoms.eq.1
! End the loop over atoms
      end do! Iatom
!
      call GET_object ('INT', '1EINT', 'AO')
      call UPT_to_SQS (OVRLAP, MATlen_G, SQS, Nbasis_G)
      call SCHMDT (CAO_A, SQS, Nbasis_G)
      call SCHMDT (CAO_B, SQS, Nbasis_G)
! End of routine GAIM_Guess
      return
      end subroutine GAIM_Guess
      subroutine GET_IniGuess (GuessCAO_X, &
                               GuessCAO_Y, &
                               Nbasis_G)
!************************************************************************
!     Date last modified: Sep 23, 2013                                  *
!     Author: Ahmad and R. A. Poirier                                   *
!     Description: Bluid the initial guess of GAIMT                     *
!************************************************************************
! Modules:
      USE type_molecule
      USE ixtrapolate                                                 

      implicit none
!
! Local scalars:
      integer :: Iatom,Jatom,Znum_X
! Input scalars:
      integer :: Nbasis_G
! Output array:
      double precision :: GuessCAO_X(Nbasis_G,Nbasis_G), GuessCAO_Y(Nbasis_G,Nbasis_G)
! Local array:
      double precision :: Eigval(Nbasis_G)
!
! Get combonents for A---* and *---B: 
      do Iatom=1,Natoms
          Znum_X=CARTESIAN(Iatom)%atomic_number
          call GET_IATOM(Znum_X)
          call GET_OCC_IATOM(Znum_X, Iatom)
        do Jatom=1,Natoms                      ! Calculating for molecules
          if(Jatom.eq.Iatom)cycle
          CARTESIAN(Jatom)%atomic_number=-CARTESIAN(Jatom)%atomic_number
        end do! Jatom
        call GET_object ('INT', '1EINT', 'AO')
        call GET_object ('QM', 'CMO', Wavefunction)
        select case (Iatom)
          case (1)
           if(Wavefunction.eq.'UHF')then
              GuessCAO_X=CMOA%coeff
            else
              GuessCAO_X=CMO%coeff
           end if ! Wavefunction.eq.'UHF'
          case (2)
           if(Wavefunction.eq.'UHF')then
             GuessCAO_Y=CMOA%coeff
            else
             GuessCAO_Y=CMO%coeff
           end if ! Wavefunction.eq.'UHF'
          case default
            write(6,*)'ATOM IS NOT AVAILABLE'
          stop
        end select
        do Jatom=1,Natoms
         if(Jatom.eq.Iatom)cycle
          CARTESIAN(Jatom)%atomic_number=-CARTESIAN(Jatom)%atomic_number
        end do! Jatom
        call KILL_object ('INT', '1EINT', 'AO')
        call KILL_object ('QM', 'CMO', Wavefunction)
! End the loop over atoms
      end do! Iatom
      
      call ORDR_CAO (GuessCAO_X, 1, Abasis, Nbasis_G)
      call ORDR_CAO (GuessCAO_Y, 2, Bbasis, Nbasis_G)
      !HSQS=GuessCAO_Y
      !GuessCAO_Y(1:Nbasis_G,7)=HSQS(1:Nbasis_G,10)
      !GuessCAO_Y(1:Nbasis_G,10)=HSQS(1:Nbasis_G,7)
      call GET_object ('INT', '1EINT', 'AO')
      call UPT_to_SQS (OVRLAP, MATlen_G, SQS, Nbasis_G)
      call SCHMDT (GuessCAO_X, SQS, Nbasis_G)
      call SCHMDT (GuessCAO_Y, SQS, Nbasis_G)
      call KILL_object ('INT', '1EINT', 'AO')
!      write(6,*)'GuessCAO_X'
!      call PRT_matrix (GuessCAO_X, Nbasis_G, Nbasis_G)
!      write(6,*)'GuessCAO_Y'
!      call PRT_matrix (GuessCAO_Y, Nbasis_G, Nbasis_G)
!
! End of routine GET_IniGuess
      return
      end subroutine GET_IniGuess
      subroutine GET_IATOM (Znum_X)
!***********************************************************************
!     Date last modified: Aug 13, 2013                                 *
!     Author: Ahmad                                                    *
!     Description: Obtain number of occupied and doubly occupied       *
!                  atomic orbitals on atoms A and B.                   *
!***********************************************************************
! Modules:
      implicit none
!
! Input scalars:
      integer :: Znum_X
!
! Begin:
      call PRG_manager ('enter', 'GET_IATOM','UTILITY')
!
      select case (Znum_X)
      case (1)
        Wavefunction='UHF'
        Multiplicity=2
        Molecule%charge=0
        Ncore=0
        Nopen=1
      case (2)
        Wavefunction='UHF'
        Multiplicity=1
        Molecule%charge=0
        Ncore=1
        Nopen=0
      case (3)
        Wavefunction='UHF'
        Multiplicity=2
        Molecule%charge=0
        Ncore=1
        Nopen=1
      case (4)
        Wavefunction='RHF'
        Multiplicity=1
        Molecule%charge=0
        Ncore=2
        Nopen=0
      case (5)
        Wavefunction='UHF'
        Multiplicity=2
        Molecule%charge=0
        Ncore=2
        Nopen=3
      case (6)
        Wavefunction='UHF'
        Multiplicity=3
        Molecule%charge=0
        Ncore=2
        Nopen=3
      case (7)
        Wavefunction='UHF'
        Multiplicity=4
        Molecule%charge=0
        Ncore=2
        Nopen=3
      case (8)
        Wavefunction='UHF'
        Multiplicity=3
        Molecule%charge=0
        Ncore=2
        Nopen=3
      case (9)
        Wavefunction='UHF'
        Multiplicity=2
        Molecule%charge=0
        Ncore=2
        Nopen=3
      case (10)
        Wavefunction='RHF'
        Multiplicity=1
        Molecule%charge=0
        Ncore=5
        Nopen=0
      case (11)
        Wavefunction='UHF'
        Multiplicity=2
        Molecule%charge=0
        Ncore=5
        Nopen=1
      case (12)
        Wavefunction='UHF'
        Multiplicity=1
        Molecule%charge=0
        Ncore=6
        Nopen=0
      case (13)
        Wavefunction='UHF'
        Multiplicity=2
        Molecule%charge=0
        Ncore=6
        Nopen=3
      case (14)
        Wavefunction='UHF'
        Multiplicity=3
        Molecule%charge=0
        Ncore=6
        Nopen=3
      case (15)
        Wavefunction='UHF'
        Multiplicity=4
        Molecule%charge=0
        Ncore=6
        Nopen=3
      case (16)
        Wavefunction='UHF'
        Multiplicity=3
        Molecule%charge=0
        Ncore=6
        Nopen=3
      case (17)
        Wavefunction='UHF'
        Multiplicity=2
        Molecule%charge=0
        Ncore=6
        Nopen=3
      case (18)
        Wavefunction='UHF'
        Multiplicity=1
        Molecule%charge=0
        Ncore=9
        Nopen=0
      case default
        write(6,*)'ELEMENT IS NOT AVAILABLE'
        stop
      end select
!
! End of routine GET_IATOM
      call PRG_manager ('exit', 'GET_IATOM', 'UTILITY')
      return
      end subroutine GET_IATOM
      subroutine GET_OCC_IATOM(Znum_X, Iatom_X)
!****************************************************************************
!     Date last modified: Sep 19, 2013                                      *
!     Author: Ahmad                                                         *
!     Description: Define Number of AOs and occupancies of Atoms A & B      *
!****************************************************************************
! Modules:
      implicit none
! Local scalars:
      integer :: AO_K,Znum_X,Iatom_X
!
! Begin:
      call PRG_manager ('enter', 'GET_OCC_IATOM','UTILITY')
!
! Get occupied AOs and occupancies of atom A:
      if(Iatom_X.eq.1)then
        Nelectrons_A=Znum_X-Molecule%charge
        Nunpairele_A=Multiplicity-1
        NDoccAO_A=(Nelectrons_A-Nunpairele_A)/2
        NoccAO_A=NDoccAO_A+Nunpairele_A
        if(NDoccAO_A.gt.0)then
          do AO_K=1,NDoccAO_A
            OCC_A_alpha(AO_K)=ONE
            OCC_A_beta(AO_K)=ONE
          end do ! AO_K
        end if ! NDoccAO_A.gt.0
        if(NDoccAO_A.ne.NoccAO_A)then
          do AO_K=NDoccAO_A+1,NoccAO_A
            OCC_A_alpha(AO_K)=ONE
          end do ! AO_K
        end if ! NDoccAO_A.ne.NoccAO_A
      end if ! Iatom_X.eq.1
!
! Get occupied AOs and occupancies of atom B:
      if(Iatom_X.eq.2)then
        Nelectrons_B=Znum_X-Molecule%charge
        Nunpairele_B=Multiplicity-1
        NDoccAO_B=(Nelectrons_B-Nunpairele_B)/2
        NoccAO_B=NDoccAO_B+Nunpairele_B
      
        if(NDoccAO_B.gt.0)then
          do AO_K=1,NDoccAO_B
            OCC_B_alpha(AO_K)=ONE
            OCC_B_beta(AO_K)=ONE
          end do ! AO_K
        end if ! NDoccAO_B.gt.0
        if(NDoccAO_B.ne.NoccAO_B)then
          do AO_K=NDoccAO_B+1,NoccAO_B
            OCC_B_beta(AO_K)=ONE
          end do ! AO_K
        end if ! NDoccAO_B.ne.NoccAO_B
      end if ! Iatom_X.eq.2
!
! End of routine GET_OCC_IATOM
      call PRG_manager ('exit', 'GET_OCC_IATOM', 'UTILITY')
      return
      end subroutine GET_OCC_IATOM
      subroutine GET_DenM 
!*************************************************************************
!     Date last modified: Mar 28, 2014                                   *
!     Author: Ahmad                                                      *
!     Description: Bluild density matrices of GAIMT.                     *
!*************************************************************************
      implicit none
! Local scalar:
      integer :: Ielem
! Begin:
! Build the density matrices:
      call BLD_PAO (P_A_alpha, CAO_A, OCC_A_alpha, NoccAO_A, MATlen_G, Nbasis_G)
      call BLD_PAO (P_A_beta,  CAO_A, OCC_A_beta,  NoccAO_A, MATlen_G, Nbasis_G)
      call BLD_PAO (P_B_alpha, CAO_B, OCC_B_alpha, NoccAO_B, MATlen_G, Nbasis_G)
      call BLD_PAO (P_B_beta,  CAO_B, OCC_B_beta,  NoccAO_B, MATlen_G, Nbasis_G)
      P_A=P_A_alpha+P_A_beta
      P_B=P_B_alpha+P_B_beta
!
! End of routine GET_DenM
      return
      end subroutine GET_DenM
      subroutine BLD_F 
!****************************************************************************
!     Date last modified: Mar 28, 2014                                      *
!     Author: Ahmad                                                         *
!     Description: Bluild F matrices of GAIMT.                              *
!****************************************************************************
      implicit none
!
      F_A_alpha=Hmol+G_A_alpha
      F_A_beta=Hmol+G_A_beta
      F_B_alpha=Hmol+G_B_alpha
      F_B_beta=Hmol+G_B_beta
!
! End of routine BLD_F
      return
      end subroutine BLD_F
      subroutine BLD_FgaimM
!****************************************************************************
!     Date last modified: June 18, 2013                                     *
!     Author: Ahmad                                                         *
!     Description: Build FgaimM_A and FgaimM_B  matrices over AO basis,     *
!                  FAAa and FAAb are the alpha and beta F matrices for      *
!                  atom A, and FBBa and FBBb are the alpha and beta F       *
!                  matrices for atom B. CAO_A and CAO_B are the coefficient *   
!                  matrices for atoms A and B, respectivel.                 *
!                  FGaimM matrix has the form:-                             *
!                                                                           *
!                                  closed     open    virtual               *
!                       closed     F_cc       F_b     F_ab                  *
!                       open       F_b        F_oo    F_a                   *    
!                       virtual    F_ab       F_a     F_vv                  *
!    where:                                                                 *
!    For atom A:-                                                           *
!             F_a=F_alpha,    F_b=F_beta,    F_cc=(3F_beta-F_alpha)/2       *
!             F_ab=F_oo=(F_alpha+F_beta)/2,  F_vv=(3F_alpha-F_beta)/2       *
!                                                                           *
!    For atom A:-                                                           *
!             F_a=F_beta,    F_b=F_alpha,    F_cc=(3F_alpha-F_beta)/2       *
!             F_ab=F_oo=(F_alpha+F_beta)/2,  F_vv=(3F_beta-F_alpha)/2       *
!****************************************************************************
! Modules:
      implicit none
! Local arrays:
       double precision :: FX_sq(Nbasis_G,Nbasis_G)
!Local scalar:
       integer :: AO_I,St_open_A,End_open_A,St_vir_A
       integer :: AO_J,St_open_B,End_open_B,St_vir_B  
! Begin:
      call UPT_to_SQS (F_A_alpha, MATlen_G, FX_sq, Nbasis_G)
      FAAa=matmul(transpose(CAO_A), matmul(FX_sq, CAO_A))
 
      call UPT_to_SQS (F_A_beta, MATlen_G, FX_sq, Nbasis_G)
      FAAb=matmul(transpose(CAO_A), matmul(FX_sq, CAO_A))
      call UPT_to_SQS (F_B_alpha, MATlen_G, FX_sq, Nbasis_G)
      FBBa=matmul(transpose(CAO_B), matmul(FX_sq, CAO_B))
      call UPT_to_SQS (F_B_beta, MATlen_G, FX_sq, Nbasis_G)
      FBBb=matmul(transpose(CAO_B), matmul(FX_sq, CAO_B))
!
      St_open_A=NDoccAO_A+1
      End_open_A=NoccAO_A
      St_vir_A=End_open_A+1
!
      St_open_B=NDoccAO_B+1
      End_open_B=NoccAO_B 
      St_vir_B=End_open_B+1
!
      FgaimM_A=ZERO
      FgaimM_B=ZERO
!
! Closed-Closed:
      if(NDoccAO_A.ne.0)then
        do AO_I=1,NDoccAO_A
          do AO_J=1,NDoccAO_A
            FgaimM_A(AO_I,AO_J)=(1.50d0*FAAb(AO_I,AO_J)-0.50d0*FAAa(AO_I,AO_J))
          end do ! AO_
        end do ! AO_J
      end if
      if(NDoccAO_B.ne.0)then
        do AO_I=1,NDoccAO_B
          do AO_J=1,NDoccAO_B
             FgaimM_B(AO_I,AO_J)=(1.50d0*FBBa(AO_I,AO_J)-0.50d0*FBBb(AO_I,AO_J))
          end do ! AO_I
        end do ! AO_J
      end if
!
! Open_Open:
      if(NDoccAO_A.ne.NoccAO_A)then
        do AO_I=St_open_A,End_open_A
          do AO_J=St_open_A,End_open_A
            FgaimM_A(AO_I,AO_J)=PT5*(FAAa(AO_I,AO_J)+FAAb(AO_I,AO_J))
          end do ! AO_I
        end do ! AO_J
      end if
      if(NDoccAO_B.ne.NoccAO_B)then
        do AO_I=St_open_B,End_open_B
          do AO_J=St_open_B,End_open_B
             FgaimM_B(AO_I,AO_J)=PT5*(FBBb(AO_I,AO_J)+FBBa(AO_I,AO_J))
          end do ! AO_I
        end do ! AO_J
      end if
!
! Virtual-Virtual:
      if(NoccAO_A.ne.Nbasis_G)then
      do AO_I=St_vir_A,Nbasis_G
        do AO_J=St_vir_A,Nbasis_G
           FgaimM_A(AO_I,AO_J)=(1.50d0*FAAa(AO_I,AO_J)-0.50d0*FAAb(AO_I,AO_J))
        end do ! AO_I
      end do ! AO_J
      end if
      if(NoccAO_B.ne.Nbasis_G)then
      do AO_I=St_vir_B,Nbasis_G
        do AO_J=St_vir_B,Nbasis_G
           FgaimM_B(AO_I,AO_J)=(1.50d0*FBBb(AO_I,AO_J)-0.50d0*FBBa(AO_I,AO_J))
        end do ! AO_I
      end do ! AO_J
      end if
!
! Open-Virtual & Virtual-Open:
      if(NDoccAO_A.ne.NoccAO_A)then
        do AO_I=St_open_A,End_open_A
          do AO_J=St_vir_A,Nbasis_G
             FgaimM_A(AO_I,AO_J)=FAAa(AO_I,AO_J)
             FgaimM_A(AO_J,AO_I)=FgaimM_A(AO_I,AO_J)
          end do ! AO_I
        end do ! AO_J
      end if
      if(NDoccAO_B.ne.NoccAO_B)then
        do AO_I=St_open_B,End_open_B
          do AO_J=St_vir_B,Nbasis_G
             FgaimM_B(AO_I,AO_J)=FBBb(AO_I,AO_J)
             FgaimM_B(AO_J,AO_I)=FgaimM_B(AO_I,AO_J)
          end do ! AO_I
        end do ! AO_J
      end if
!
! Closed-Open & Open-closed:
      if((NDoccAO_A.ne.0).and.(NDoccAO_A.ne.NoccAO_A))then
        do AO_I=1,NDoccAO_A
          do AO_J=St_open_A,End_open_A
             FgaimM_A(AO_I,AO_J)=FAAb(AO_I,AO_J)
             FgaimM_A(AO_J,AO_I)=FgaimM_A(AO_I,AO_J)
          end do ! AO_I
        end do ! AO_J
      end if
      if((NDoccAO_B.ne.0).and.(NDoccAO_B.ne.NoccAO_B))then
        do AO_I=1,NDoccAO_B
          do AO_J=St_open_B,End_open_B
             FgaimM_B(AO_I,AO_J)=FBBa(AO_I,AO_J)
             FgaimM_B(AO_J,AO_I)=FgaimM_B(AO_I,AO_J)
          end do ! AO_I
        end do ! AO_J
      end if
!
! Closed-Virtual & Virtual-closed:
      if(NDoccAO_A.ne.0)then
        do AO_I=1,NDoccAO_A
          do AO_J=St_vir_A,Nbasis_G
             FgaimM_A(AO_I,AO_J)=PT5*(FAAa(AO_I,AO_J)+FAAb(AO_I,AO_J))
             FgaimM_A(AO_J,AO_I)=PT5*(FAAa(AO_J,AO_I)+FAAb(AO_J,AO_I))
          end do ! AO_I
        end do ! AO_J
      end if
      if(NDoccAO_B.ne.0)then
        do AO_I=1,NDoccAO_B
          do AO_J=St_vir_B,Nbasis_G
             FgaimM_B(AO_I,AO_J)=PT5*(FBBb(AO_I,AO_J)+FBBa(AO_I,AO_J))
             FgaimM_B(AO_J,AO_I)=PT5*(FBBb(AO_J,AO_I)+FBBa(AO_J,AO_I))
          end do ! AO_I
        end do ! AO_J
      end if
!
! End of routine BLD_FgaimM
      return
      end subroutine BLD_FgaimM
      subroutine CAL_Energy
!*************************************************************************
!     Date last modified: Mar 28, 2014                                   *
!     Author: Ahmad                                                      *
!     Description: Bluild F matrices of GAIMT.                           *
!*************************************************************************
      implicit none
!
! Local function:
      double precision :: ETRACE       
!
! Compute the energy:
      ENERGY_GAIMT=ZERO
      ENERGY_GAIMT=ETRACE (Hmol, F_A_alpha, P_A_alpha, Nbasis_G, MATlen_G)
      ENERGY_GAIMT=ENERGY_GAIMT+ETRACE (Hmol, F_A_beta, P_A_beta, Nbasis_G, MATlen_G)
      ENERGY_GAIMT=ENERGY_GAIMT+ETRACE (Hmol, F_B_alpha, P_B_alpha, Nbasis_G, MATlen_G)
      ENERGY_GAIMT=ENERGY_GAIMT+ETRACE (Hmol, F_B_beta, P_B_beta, Nbasis_G, MATlen_G)
      ENERGY_GAIMT=ENERGY_GAIMT*PT5
      ENERGY_GAIMT_total=ENERGY_GAIMT+Vnn_Gaim
      E2=ENERGY_GAIMT
!
      Jcycle=Jcycle+1
      if(.not.Lextrapolated)EPREV=ENERGY_GAIMT
! Print iteration results.
      if(MUN_prtlev.GT.0)then
        if(Lextrapolated)then
          write(UNIout,1000)Jcycle,ENERGY_GAIMT,ENERGY_GAIMT_total,LABEL
 1000 FORMAT(1X,I4,1X,2F20.9,20x,a8)
        else
          write(UNIout,1001)'Cycle ',Jcycle,ENERGY_GAIMT,ENERGY_GAIMT_total!,SCFCON
 1001 FORMAT(1X,a,I4,1X,2F20.9,1PE14.5)
        end if
      end if
!
! End of routine CAL_Energy
      return
      end subroutine CAL_Energy
      subroutine CAL_E_Comp
!*************************************************************************
!     Date last modified: Mar 28, 2014                                   *
!     Author: Ahmad                                                      *
!     Description: Compute energy components.                            *
!*************************************************************************
      implicit none
!
! Local functions:
      double precision :: TraceAB
!
! Compute the energy components:
      E_HAa=TraceAB (Hmol, P_A_alpha, Nbasis_G)
      E_HAb=TraceAB (Hmol, P_A_beta,  Nbasis_G)
      E_HBa=TraceAB (Hmol, P_B_alpha, Nbasis_G)
      E_HBb=TraceAB (Hmol, P_B_beta,  Nbasis_G)
! Compute kinetic and attraction energies:
      E_TA=TraceAB (Tmol, P_A, Nbasis_G)
      E_VA=TraceAB (Vmol, P_A, Nbasis_G)
      E_TB=TraceAB (Tmol, P_B, Nbasis_G)
      E_VB=TraceAB (Vmol, P_B, Nbasis_G)
! Print energy components
      call PRT_E_Comp
!
! End of routine CAL_E_Comp
      return
      end subroutine CAL_E_Comp
      subroutine CAL_No_of_Electrons
!*************************************************************************
!     Date last modified: April 01, 2014                                 *
!     Author: Ahmad                                                      *
!     Description: Bluild F matrices of GAIMT.                           *
!*************************************************************************
      implicit none
!Local scalars:
      double precision :: PS_Aa, PS_Ab, PS_Ba, PS_Bb
!
! Local functions:
      double precision :: TraceAB
!
! Compute No of electrons = PS:
      PS_Aa=TraceAB (P_A_alpha, Smol, Nbasis_G)
      PS_Ab=TraceAB (P_A_beta, Smol, Nbasis_G)
      PS_Ba=TraceAB (P_B_alpha, Smol, Nbasis_G)
      PS_Bb=TraceAB (P_B_beta, Smol, Nbasis_G)
!     write(6,*)
!     write(6,'(a,f12.6)')'PS_Aa =',PS_Aa
!     write(6,'(a,f12.6)')'PS_Ab =',PS_Ab
!     write(6,'(a,f12.6)')'PS_Ba =',PS_Ba
!     write(6,'(a,f12.6)')'PS_Bb =',PS_Bb
!     write(6,*)
!
! End of routine CAL_No_of_Electrons
      return
      end subroutine CAL_No_of_Electrons
      subroutine CHK_Convergence
!*************************************************************************
!     Date last modified: Mar 28, 2014                                   *
!     Author: Ahmad                                                      *
!     Description: Bluild F matrices of GAIMT.                           *
!*************************************************************************
      implicit none
!
! Begin:
! Calculate dE; the convergence criterion:
      dE=E1-E2
      E1=E2
!
! Has convergence been met?  If so, exit.
      if(dabs(dE).lt.1.0D-4)then 
        converged=.true.
        write(6,*)
        write(UNIout,1010)ENERGY_GAIMT_total
        write(6,*)"              --------------------------------"
        write(6,*)
!
! Compute and print energy components; Kinetic, Vne, Vee, and virial.
!      call CAL_No_of_Electrons
!      call CAL_E_Comp
!
! Have we exceeded the maximum allowed number of cycles? If so, exit.
      else if(Jcycle.GT.SCFITN)then ! Maximum iteration count exceeded without convergence.
        converged=.true.
        write(6,'(A,I4,1X,A)')'ERROR> GAIMT: SCF DID NOT CONVERGE AFTER',SCFITN,' ITERATIONS'
        stop 'GAIMT: DID NOT CONVERGE'
      end if
 1010 FORMAT('At Termination Total_Energy is',F17.6,'  Hartrees')
!
! End of routine CHK_Convergence
      return
      end subroutine CHK_Convergence
      end subroutine GAIMT                                   ! End GAIMT 
      subroutine BLD_PAO (PAO, &
                          C_AO, &
                          OCC_AO, &
                          NoccAO, &
                          MATlen_G, &
                          Nbasis_G)
!***********************************************************************
!     Date last modified: May 09, 2013                                 *
!     Author: Ahmad                                                    *
!     Description: Build the density matrices.                         *
!***********************************************************************
! Modules:
      USE program_constants
      implicit none
!
! Input scalars:
      integer :: N_AO,MATlen_G,Nbasis_G,NoccAO
      double precision :: OCC_AO(Nbasis_G),PAO(MATlen_G),C_AO(Nbasis_G,Nbasis_G)
! Local scalars:
      integer :: K_AO,Ibasis,Jbasis,IJ
      double precision :: ToTal
!
! Begin:
      call PRG_manager ('enter', 'BLD_PAO','UTILITY')
!
! Form the density matrix.
      IJ=0
      N_AO=NoccAO 
      do Ibasis=1,Nbasis_G
        do Jbasis=1,Ibasis
          IJ=IJ+1
          ToTal=ZERO
          do K_AO=1,N_AO
            ToTal=ToTal+C_AO(Ibasis,K_AO)*C_AO(Jbasis,K_AO)*OCC_AO(K_AO)
          end do
          PAO(IJ)=ToTal
        end do ! Jbasis
      end do ! Ibasis
!
! End of routine BLD_PAO
      call PRG_manager ('exit', 'BLD_PAO', 'UTILITY')
      return
      end subroutine BLD_PAO
      subroutine BLD_GgaimM (G_X_alpha, &
                             G_X_beta, &
                             G_Y_alpha, &
                             G_Y_beta, &
                             MATlen_G, &
                             Nbasis_G)
!***********************************************************************
!     Date last modified: May 11, 2013                                 *
!     Author: Ahmad                                                    *
!     Description: MUN Version.                                        *
!                  Form the two-electron part of the F matrices:-      *
!                  G_A_alpha,  G_A_beta, G_B_alpha, and G_B_beta       *
!***********************************************************************
! Modules:
      USE program_constants
      USE type_basis_set
      USE matrix_print
      USE GAIMT_objects                                       
      implicit none
!
! Input scalar:
      integer, intent(IN) :: MATlen_G, Nbasis_G 
!
! Local scalar:
      integer :: Ibasis,Jbasis,Ielem
!
! Local functions:
      double precision :: TraceAB
!
! Local arrays:
      double precision :: J_AA_alpha(MATlen_G), J_AA_beta(MATlen_G)
      double precision :: J_BB_alpha(MATlen_G), J_BB_beta(MATlen_G)
      double precision :: J_AB_alpha(MATlen_G), J_AB_beta(MATlen_G)
      double precision :: J_BA_alpha(MATlen_G), J_BA_beta(MATlen_G)
      double precision :: K_AA_alpha(MATlen_G), K_AA_beta(MATlen_G)
      double precision :: K_BB_alpha(MATlen_G), K_BB_beta(MATlen_G)
      double precision :: K_AB_alpha(MATlen_G), K_AB_beta(MATlen_G)
      double precision :: K_BA_alpha(MATlen_G), K_BA_beta(MATlen_G)
      double precision :: G_AA_alpha(MATlen_G), G_AA_beta(MATlen_G)
      double precision :: G_BB_alpha(MATlen_G), G_BB_beta(MATlen_G)
      double precision :: G_AB_alpha(MATlen_G), G_AB_beta(MATlen_G)
      double precision :: G_BA_alpha(MATlen_G), G_BA_beta(MATlen_G)
      double precision :: PA_a(MATlen_G), PA_b(MATlen_G)
      double precision :: PB_a(MATlen_G), PB_b(MATlen_G)
!
! Output arrays:
      double precision :: G_X_alpha(MATlen_G), G_X_beta(MATlen_G)
      double precision :: G_Y_alpha(MATlen_G), G_Y_beta(MATlen_G)
!
! Begin:
      call PRG_manager ('enter', 'BLD_GgaimM', 'UTILITY')
! Form the two-ectron part of the F matrices: 
      call BLD_GM (J_AA_alpha, J_AA_beta, K_AA_alpha, K_AA_beta, P_A_alpha, P_A_beta, MATlen_G, .false.)
      call BLD_GM (J_BB_alpha, J_BB_beta, K_BB_alpha, K_BB_beta, P_B_alpha, P_B_beta, MATlen_G, .false.)
      call BLD_GM (J_AB_alpha, J_AB_beta, K_AB_alpha, K_AB_beta, P_B_alpha, P_B_beta, MATlen_G, .false.)
      call BLD_GM (J_BA_alpha, J_BA_beta, K_BA_alpha, K_BA_beta, P_A_alpha, P_A_beta, MATlen_G, .false.)
!
      G_AA_alpha=J_AA_alpha-K_AA_alpha
      G_BB_alpha=J_BB_alpha-K_BB_alpha
      G_AA_beta=J_AA_beta-K_AA_beta
      G_BB_beta=J_BB_beta-K_BB_beta
      G_AB_alpha=J_AB_alpha-K_AB_alpha
      G_BA_alpha=J_BA_alpha-K_BA_alpha
      G_AB_beta=J_AB_beta-K_AB_beta
      G_BA_beta=J_BA_beta-K_BA_beta
      E_GAAa=TraceAB (G_AA_alpha, P_A_alpha, Nbasis_G)
      E_GAAb=TraceAB (G_AA_beta,  P_A_beta,  Nbasis_G)
      E_GBBa=TraceAB (G_BB_alpha, P_B_alpha, Nbasis_G)
      E_GBBb=TraceAB (G_BB_beta,  P_B_beta,  Nbasis_G)
      E_GABa=TraceAB (G_AB_alpha, P_A_alpha, Nbasis_G)
      E_GABb=TraceAB (G_AB_beta,  P_A_beta,  Nbasis_G)
      E_GBAa=TraceAB (G_BA_alpha, P_B_alpha, Nbasis_G)
      E_GBAb=TraceAB (G_BA_beta,  P_B_beta,  Nbasis_G)
!
      G_X_alpha=G_AA_alpha+G_AB_alpha
      G_X_beta=G_AA_beta+G_AB_beta
      G_Y_alpha=G_BB_alpha+G_BA_alpha
      G_Y_beta=G_BB_beta+G_BA_beta
! End of routine BLD_GgaimM
      call PRG_manager ('exit', 'BLD_GgaimM', 'UTILITY')
      return
      end subroutine BLD_GgaimM
      subroutine BLD_GM (J_X_alpha, &
                         J_X_beta, &
                         K_X_alpha, &
                         K_X_beta, &
                         P_X_alpha, &
                         P_X_beta, &
                         MATlen_G, &
                         lexchange)
!***********************************************************************
!     Date last modified: May 11, 2013                                 *
!     Author: Ahmad                                                    *
!     Description:  MUN Version.                                       *
!     Form the two-electron part of the Fock matrix, given the         *
!     density matrix and any or all two-electron integrals.            *
!***********************************************************************
! Modules:
      USE program_constants
      USE QM_objects
      USE INT_objects

      implicit none
!
! Input scalar:
     integer, intent(IN) :: MATlen_G
!
! Input arrays:
      double precision P_X_alpha(MATlen_G),P_X_beta(MATlen_G)
      double precision P_X0_alpha(MATlen_G),P_X0_beta(MATlen_G)
!
! Output arrays:
      double precision J_X_alpha(MATlen_G),J_X_beta(MATlen_G)
      double precision K_X_alpha(MATlen_G),K_X_beta(MATlen_G)
!
! Local scalars:
      integer BUFSIZ,I,LENGTH
      logical EOF, lexchange
!
! Begin:
      call PRG_manager ('enter', 'BLD_GM', 'UTILITY')
!
! Initialize DGM.
      J_X_alpha(1:MATlen_G)=ZERO
      J_X_beta(1:MATlen_G)=ZERO
      K_X_alpha(1:MATlen_G)=ZERO
      K_X_beta(1:MATlen_G)=ZERO
      PM0=P_X_alpha+P_X_beta
      P_X0_alpha=P_X_alpha
      P_X0_beta=P_X_beta
      if(lexchange)then        ! to calculate the second term in G matrix of GAIMT.
        P_X_alpha=-P_X_alpha!zero
        P_X_beta=-P_X_beta!zero
      end if ! lexchange
!
! Process all INCORE integrals
      if(IJKLCN.gt.0)call BLD_ijkl_GAIMT (IJKL_INCORE, IJKLCN)
      if(IIKLCN.gt.0)call BLD_iikl_GAIMT (IIKL_INCORE, IIKLCN)
      if(IJKJCN.gt.0)call BLD_ijkj_GAIMT (IJKJ_INCORE, IJKJCN)
      if(IJJLCN.gt.0)call BLD_ijjl_GAIMT (IJJL_INCORE, IJJLCN)
      if(IIKKCN.gt.0)call BLD_iikk_GAIMT (IIKK_INCORE, IIKKCN)
      if(IJJJCN.gt.0)call BLD_ijjj_GAIMT (IJJJ_INCORE, IJJJCN)
      if(IIILCN.gt.0)call BLD_iiil_GAIMT (IIIL_INCORE, IIILCN)
      if(IIIICN.gt.0)call BLD_iiii_GAIMT (IIII_INCORE, IIIICN)
!
      if(ANY_ONDISK)then
      if(LIJKL_ONDISK)then
      EOF=.FALSE.
      rewind IJKLUI
      call READ_ijkl (IJKL_BUFFER, IJKLLN, IJKLUI, LENGTH, EOF)
      do while (.not.EOF)
        call BLD_ijkl_GAIMT (IJKL_BUFFER, LENGTH)
        call READ_ijkl (IJKL_BUFFER, IJKLLN, IJKLUI, LENGTH, EOF)
      end do
      end if ! ONDISK
!
      if(LIIKL_ONDISK)then
      EOF=.FALSE.
      rewind IIKLUI
      call READ_iikl (IIKL_BUFFER, IIKLLN, IIKLUI, LENGTH, EOF)
      do while (.not.EOF)
         call BLD_iikl_GAIMT (IIKL_BUFFER, LENGTH)
         call READ_iikl (IIKL_BUFFER, IIKLLN, IIKLUI, LENGTH, EOF)
      end do
      end if ! ONDISK
!
      if(LIJKJ_ONDISK)then
      EOF=.FALSE.
      rewind IJKJUI
      call READ_ijkj (IJKJ_BUFFER, IJKJLN, IJKJUI, LENGTH, EOF)
      do while (.not.EOF)
        call BLD_ijkj_GAIMT (IJKJ_BUFFER, LENGTH)
        call READ_ijkj (IJKJ_BUFFER, IJKJLN, IJKJUI, LENGTH, EOF)
      end do
      end if ! ONDISK
!
      if(LIJJL_ONDISK)then
      EOF=.FALSE.
      rewind IJJLUI
      call READ_ijjl (IJJL_BUFFER, IJJLLN, IJJLUI, LENGTH, EOF)
      do while (.not.EOF)
        call BLD_ijjl_GAIMT (IJJL_BUFFER, LENGTH)
        call READ_ijjl (IJJL_BUFFER, IJJLLN, IJJLUI, LENGTH, EOF)
      end do
      end if ! ONDISK
      end if ! ANY_ONDISK
!
! End of routine BLD_GM
      call PRG_manager ('exit', 'BLD_GM', 'UTILITY')
      return
!
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine BLD_ijkl_GAIMT (IJKL_IN, &
                                 LENGTH)
!***********************************************************************
!     Date last modified: May 11, 2013                                 *
!     Author: Ahmad and R.A. Poirier                                   *
!     Description:  MUN Version.                                       *
!     Add IJKL contributions to Matrix G=P*(IJKL+IKJL+ILKJ).           *
!***********************************************************************
! Modules:
      implicit none
!
! Input scalars:
      integer LENGTH
!
! Input arrays:
      type (ints_2e_ijkl) IJKL_IN(LENGTH)
!
! Local scalars:
      integer I,Iao,I1,I2,Jao,JS,JT,Kao,L,Lao,MAX
      double precision GT,G1,G2,G3
!
! Begin:
      call PRG_manager ('enter', 'BLD_ijkl_GAIMT', 'UTILITY')
!
      if(LENGTH.LE.0)stop 'BLD_ijkl_GAIMT, BUFFER LENGTH = 0'
!
      do I=1,LENGTH
      Iao=IJKL_IN(I)%I
      Jao=IJKL_IN(I)%J
      Kao=IJKL_IN(I)%K
      Lao=IJKL_IN(I)%L
      G1=IJKL_IN(I)%IJKL
      G2=IJKL_IN(I)%IKJL
      G3=IJKL_IN(I)%ILJK
!
      JS=Iao*(Iao-1)/2+Jao
      JT=Kao*(Kao-1)/2+Lao
      GT=G2+G3
      J_X_alpha(JS)=J_X_alpha(JS)+TWO*PM0(JT)*G1
      J_X_beta(JS)=J_X_beta(JS)+TWO*PM0(JT)*G1
      J_X_alpha(JT)=J_X_alpha(JT)+TWO*PM0(JS)*G1
      J_X_beta(JT)=J_X_beta(JT)+TWO*PM0(JS)*G1
      K_X_alpha(JS)=K_X_alpha(JS)+P_X_alpha(JT)*GT
      K_X_beta(JS)=K_X_beta(JS)+P_X_beta(JT)*GT
      K_X_alpha(JT)=K_X_alpha(JT)+P_X_alpha(JS)*GT
      K_X_beta(JT)=K_X_beta(JT)+P_X_beta(JS)*GT
      JS=Iao*(Iao-1)/2+Kao
      JT=Jao*(Jao-1)/2+Lao
      GT=G1+G3
      J_X_alpha(JS)=J_X_alpha(JS)+TWO*PM0(JT)*G2
      J_X_beta(JS)=J_X_beta(JS)+TWO*PM0(JT)*G2
      J_X_alpha(JT)=J_X_alpha(JT)+TWO*PM0(JS)*G2
      J_X_beta(JT)=J_X_beta(JT)+TWO*PM0(JS)*G2
      K_X_alpha(JS)=K_X_alpha(JS)+P_X_alpha(JT)*GT
      K_X_beta(JS)=K_X_beta(JS)+P_X_beta(JT)*GT
      K_X_alpha(JT)=K_X_alpha(JT)+P_X_alpha(JS)*GT
      K_X_beta(JT)=K_X_beta(JT)+P_X_beta(JS)*GT
      JS=Iao*(Iao-1)/2+Lao
      JT=Jao*(Jao-1)/2+Kao
      GT=G1+G2
      J_X_alpha(JS)=J_X_alpha(JS)+TWO*PM0(JT)*G3
      J_X_beta(JS)=J_X_beta(JS)+TWO*PM0(JT)*G3
      J_X_alpha(JT)=J_X_alpha(JT)+TWO*PM0(JS)*G3
      J_X_beta(JT)=J_X_beta(JT)+TWO*PM0(JS)*G3
      K_X_alpha(JS)=K_X_alpha(JS)+P_X_alpha(JT)*GT
      K_X_beta(JS)=K_X_beta(JS)+P_X_beta(JT)*GT
      K_X_alpha(JT)=K_X_alpha(JT)+P_X_alpha(JS)*GT
      K_X_beta(JT)=K_X_beta(JT)+P_X_beta(JS)*GT
      end do
!
! End of routine BLD_ijkl_GAIMT
      call PRG_manager ('exit', 'BLD_ijkl_GAIMT', 'UTILITY')
      return
      end subroutine BLD_ijkl_GAIMT
      subroutine BLD_ijkj_GAIMT (IJKJ_IN, &
                                 LENGTH)
!***********************************************************************
!     Date last modified: May 11, 2013                                 *
!     Author: Ahmad and R.A. Poirier                                   *
!     Description:  MUN Version.                                       *
!     Add IJKL contributions to Matrix G=P*(IJKL+IKJL).                *
!***********************************************************************
! Modules:
      implicit none
!
! Input scalars:
      integer LENGTH
!
! Input arrays:
      type (ints_2e_ijkj) IJKJ_IN(LENGTH)
!
! Local scalars:
      integer I,Iao,I1,I2,Jao,Kao,L,JS,JT,MAX
      double precision GT,G1,G2
!
! Begin:
      call PRG_manager ('enter', 'BLD_ijkj_GAIMT', 'UTILITY')
!
        if(LENGTH.LE.0)stop 'BLD_ijkj_GAIMT, IJKJ LENGTH = 0'
!
        do I=1,LENGTH
        Iao=IJKJ_IN(I)%I
        Jao=IJKJ_IN(I)%J
        Kao=IJKJ_IN(I)%K
        G1=IJKJ_IN(I)%IJKJ
        G2=IJKJ_IN(I)%IKJJ
!
        JS=Iao*(Iao-1)/2+Jao
        JT=Kao*(Kao-1)/2+Jao
        GT=G1+G2
        J_X_alpha(JS)=J_X_alpha(JS)+TWO*PM0(JT)*G1
        J_X_beta(JS)=J_X_beta(JS)+TWO*PM0(JT)*G1
        J_X_alpha(JT)=J_X_alpha(JT)+TWO*PM0(JS)*G1
        J_X_beta(JT)=J_X_beta(JT)+TWO*PM0(JS)*G1
        K_X_alpha(JS)=K_X_alpha(JS)+P_X_alpha(JT)*GT
        K_X_beta(JS)=K_X_beta(JS)+P_X_beta(JT)*GT
        K_X_alpha(JT)=K_X_alpha(JT)+P_X_alpha(JS)*GT
        K_X_beta(JT)=K_X_beta(JT)+P_X_beta(JS)*GT
        JS=Iao*(Iao-1)/2+Kao
        JT=Jao*(Jao+1)/2
        J_X_alpha(JS)=J_X_alpha(JS)+PM0(JT)*G2
        J_X_beta(JS)=J_X_beta(JS)+PM0(JT)*G2
        J_X_alpha(JT)=J_X_alpha(JT)+TWO*PM0(JS)*G2
        J_X_beta(JT)=J_X_beta(JT)+TWO*PM0(JS)*G2
        K_X_alpha(JS)=K_X_alpha(JS)+P_X_alpha(JT)*G1
        K_X_beta(JS)=K_X_beta(JS)+P_X_beta(JT)*G1
        K_X_alpha(JT)=K_X_alpha(JT)+TWO*P_X_alpha(JS)*G1
        K_X_beta(JT)=K_X_beta(JT)+TWO*P_X_beta(JS)*G1
        end do
!
! End of routine BLD_ijkj_GAIMT
      call PRG_manager ('exit', 'BLD_ijkj_GAIMT', 'UTILITY')
      return
      end subroutine BLD_ijkj_GAIMT
      subroutine BLD_iikk_GAIMT (IIKK_IN, &
                                 LENGTH)
!***********************************************************************
!     Date last modified: May 11, 2013                                 *
!     Author: Ahmad and R.A. Poirier                                   *
!     Description:  MUN Version.                                       *
!     Add IJKL contributions to Matrix G=P*(IJKL+IKJL).                *
!***********************************************************************
! Modules:
      implicit none
!
! Input scalars:
      integer LENGTH
!
! Input arrays:
      type (ints_2e_iikk) IIKK_IN(LENGTH)
!
! Local scalars:
      integer I,Iao,I1,I2,Jao,Kao,L,JS,JT,MAX
      double precision GT,G1,G2
!
! Begin:
      call PRG_manager ('enter', 'BLD_iikk_GAIMT', 'UTILITY')
!
        if(LENGTH.LT.0)stop 'BLD_iikk_GAIMT, IIKK LENGTH = 0'
!
        do I=1,LENGTH
        Iao=IIKK_IN(I)%I
        Jao=IIKK_IN(I)%K
        G1=IIKK_IN(I)%iikk
        G2=IIKK_IN(I)%ikik
        JS=Iao*(Iao+1)/2
        JT=Jao*(Jao+1)/2
        J_X_alpha(JS)=J_X_alpha(JS)+PM0(JT)*G1
        J_X_beta(JS)=J_X_beta(JS)+PM0(JT)*G1
        J_X_alpha(JT)=J_X_alpha(JT)+PM0(JS)*G1
        J_X_beta(JT)=J_X_beta(JT)+PM0(JS)*G1
        K_X_alpha(JS)=K_X_alpha(JS)+P_X_alpha(JT)*G2
        K_X_beta(JS)=K_X_beta(JS)+P_X_beta(JT)*G2
        K_X_alpha(JT)=K_X_alpha(JT)+P_X_alpha(JS)*G2
        K_X_beta(JT)=K_X_beta(JT)+P_X_beta(JS)*G2
        JS=Iao*(Iao-1)/2+Jao
        J_X_alpha(JS)=J_X_alpha(JS)+(PM0(JS)+P_X0_beta(JS))*G2
        J_X_beta(JS)=J_X_beta(JS)+(PM0(JS)+P_X0_alpha(JS))*G2
        K_X_alpha(JS)=K_X_alpha(JS)+P_X_alpha(JS)*G1
        K_X_beta(JS)=K_X_beta(JS)+P_X_beta(JS)*G1
        end do
!
! End of routine BLD_iikk_GAIMT
      call PRG_manager ('exit', 'BLD_iikk_GAIMT', 'UTILITY')
      return
      end subroutine BLD_iikk_GAIMT
      subroutine BLD_ijjl_GAIMT (IJJL_IN, &
                                 LENGTH)
!***********************************************************************
!     Date last modified: May 11, 2013                                 *
!     Author: Ahmad and R.A. Poirier                                   *
!     Description:  MUN Version.                                       *
!     Add IJKL contributions to Matrix G=P*(IJKL+IKJL).                *
!***********************************************************************
! Modules:
      implicit none
!
! Input scalars:
      integer LENGTH
!
! Input arrays:
      type (ints_2e_ijjl) IJJL_IN(LENGTH)
!
! Local scalars:
      integer I,Iao,I1,I2,Jao,Kao,L,JS,JT,MAX
      double precision GT,G1,G2
!
! Begin:
      call PRG_manager ('enter', 'BLD_ijjl_GAIMT', 'UTILITY')
!
        if(LENGTH.LE.0)stop 'BLD_ijjl_GAIMT, IJJL LENGTH = 0'
!
        do I=1,LENGTH
        Iao=IJJL_IN(I)%I
        Jao=IJJL_IN(I)%J
        Kao=IJJL_IN(I)%L
        G1=IJJL_IN(I)%ijjl
        G2=IJJL_IN(I)%iljj
!
        JS=Iao*(Iao-1)/2+Jao
        JT=Jao*(Jao-1)/2+Kao
        GT=G1+G2
        J_X_alpha(JS)=J_X_alpha(JS)+TWO*PM0(JT)*G1
        J_X_beta(JS)=J_X_beta(JS)+TWO*PM0(JT)*G1
        J_X_alpha(JT)=J_X_alpha(JT)+TWO*PM0(JS)*G1
        J_X_beta(JT)=J_X_beta(JT)+TWO*PM0(JS)*G1
        K_X_alpha(JS)=K_X_alpha(JS)+P_X_alpha(JT)*GT
        K_X_beta(JS)=K_X_beta(JS)+P_X_beta(JT)*GT
        K_X_alpha(JT)=K_X_alpha(JT)+P_X_alpha(JS)*GT
        K_X_beta(JT)=K_X_beta(JT)+P_X_beta(JS)*GT
        JS=Iao*(Iao-1)/2+Kao
        JT=Jao*(Jao+1)/2
        J_X_alpha(JS)=J_X_alpha(JS)+PM0(JT)*G2
        J_X_beta(JS)=J_X_beta(JS)+PM0(JT)*G2
        J_X_alpha(JT)=J_X_alpha(JT)+TWO*PM0(JS)*G2
        J_X_beta(JT)=J_X_beta(JT)+TWO*PM0(JS)*G2
        K_X_alpha(JS)=K_X_alpha(JS)+P_X_alpha(JT)*G1
        K_X_beta(JS)=K_X_beta(JS)+P_X_beta(JT)*G1
        K_X_alpha(JT)=K_X_alpha(JT)+TWO*P_X_alpha(JS)*G1
        K_X_beta(JT)=K_X_beta(JT)+TWO*P_X_beta(JS)*G1
        end do
!
! End of routine BLD_ijjl_GAIMT
      call PRG_manager ('exit', 'BLD_ijjl_GAIMT', 'UTILITY')
      return
      end subroutine BLD_ijjl_GAIMT
      subroutine BLD_iikl_GAIMT (IIKL_IN, &
                                 LENGTH)
!***********************************************************************
!     Date last modified: May 11, 2013                                 *
!     Author: Ahmad and R.A. Poirier                                   *
!     Description:  MUN Version.                                       *
!     Add IJKL contributions to Matrix G=P*(IJKL+IKJL).                *
!***********************************************************************
! Modules:
      implicit none
!
! Input scalars:
      integer LENGTH
!
! Input arrays:
      type (ints_2e_iikl) IIKL_IN(LENGTH)
!
! Local scalars:
      integer I,Iao,I1,I2,Jao,Kao,L,JS,JT,MAX
      double precision GT,G1,G2
!
! Begin:
      call PRG_manager ('enter', 'BLD_iikl_GAIMT', 'UTILITY')
!
        if(LENGTH.LE.0)stop 'BLD_iikl_GAIMT, IIKL LENGTH = 0'
!
        do I=1,LENGTH
        Iao=IIKL_IN(I)%I
        Jao=IIKL_IN(I)%K
        Kao=IIKL_IN(I)%L
        G1=IIKL_IN(I)%iikl
        G2=IIKL_IN(I)%ikil
!
        JS=Iao*(Iao+1)/2
        JT=Jao*(Jao-1)/2+Kao
        J_X_alpha(JS)=J_X_alpha(JS)+TWO*PM0(JT)*G1
        J_X_beta(JS)=J_X_beta(JS)+TWO*PM0(JT)*G1
        J_X_alpha(JT)=J_X_alpha(JT)+PM0(JS)*G1
        J_X_beta(JT)=J_X_beta(JT)+PM0(JS)*G1
        K_X_alpha(JS)=K_X_alpha(JS)+TWO*P_X_alpha(JT)*G2
        K_X_beta(JS)=K_X_beta(JS)+TWO*P_X_beta(JT)*G2
        K_X_alpha(JT)=K_X_alpha(JT)+P_X_alpha(JS)*G2
        K_X_beta(JT)=K_X_beta(JT)+P_X_beta(JS)*G2
        JS=Iao*(Iao-1)/2+Jao
        JT=Iao*(Iao-1)/2+Kao
        GT=G1+G2
        J_X_alpha(JS)=J_X_alpha(JS)+TWO*PM0(JT)*G2
        J_X_beta(JS)=J_X_beta(JS)+TWO*PM0(JT)*G2
        J_X_alpha(JT)=J_X_alpha(JT)+TWO*PM0(JS)*G2
        J_X_beta(JT)=J_X_beta(JT)+TWO*PM0(JS)*G2
        K_X_alpha(JS)=K_X_alpha(JS)+P_X_alpha(JT)*GT
        K_X_beta(JS)=K_X_beta(JS)+P_X_beta(JT)*GT
        K_X_alpha(JT)=K_X_alpha(JT)+P_X_alpha(JS)*GT
        K_X_beta(JT)=K_X_beta(JT)+P_X_beta(JS)*GT
        end do
!
! End of routine BLD_iikl_GAIMT
      call PRG_manager ('exit', 'BLD_iikl_GAIMT', 'UTILITY')
      return
      end subroutine BLD_iikl_GAIMT
      subroutine BLD_ijjj_GAIMT (IJJJ_IN, &
                                 LENGTH)
!***********************************************************************
!     Date last modified: May 11, 2013                                 *
!     Author: Ahmad and R.A. Poirier                                   *
!     Description:  MUN Version.                                       *
!     Add IJKL contributions to Matrix G=P*(IJKL+IKJL).                *
!***********************************************************************
! Modules:
      implicit none
!
! Input scalars:
      integer LENGTH
!
! Input arrays:
      type (ints_2e_ijjj) IJJJ_IN(LENGTH)
!
! Local scalars:
      integer I,Iao,I1,I2,Jao,L,JS,JT,MAX
      double precision G1
!
! Begin:
      call PRG_manager ('enter', 'BLD_ijjj_GAIMT', 'UTILITY')
!
      if(LENGTH.LE.0)stop 'BLD_ijjj_GAIMT, IJJJ BUFFER LENGTH = 0'
!
      do I=1,LENGTH
        Iao=IJJJ_IN(I)%I
        Jao=IJJJ_IN(I)%J
        G1=IJJJ_IN(I)%ijjj
        JS=Iao*(Iao-1)/2+Jao
        JT=Jao*(Jao+1)/2
        J_X_alpha(JS)=J_X_alpha(JS)+P_X0_beta(JT)*G1
        J_X_beta(JS)=J_X_beta(JS)+P_X0_alpha(JT)*G1
        J_X_alpha(JT)=J_X_alpha(JT)+TWO*P_X0_beta(JS)*G1
        J_X_beta(JT)=J_X_beta(JT)+TWO*P_X0_alpha(JS)*G1
      end do
!
! End of routine BLD_ijjj_GAIMT
      call PRG_manager ('exit', 'BLD_ijjj_GAIMT', 'UTILITY')
      return
      end subroutine BLD_ijjj_GAIMT
      subroutine BLD_iiil_GAIMT (IIIL_IN, &
                                 LENGTH)
!***********************************************************************
!     Date last modified: May 11, 2013                                 *
!     Author: Ahmad and R.A. Poirier                                   *
!     Description:  MUN Version.                                       *
!     Add IJKL contributions to Matrix G=P*(IJKL+IKJL).                *
!***********************************************************************
! Modules:
      implicit none
!
! Input scalars:
      integer LENGTH
!
! Input arrays:
      type (ints_2e_iiil) IIIL_IN(LENGTH)
!
! Local scalars:
      integer I,Iao,I1,I2,Jao,L,JS,JT,MAX
      double precision G1
!
! Begin:
      call PRG_manager ('enter', 'BLD_iiil_GAIMT', 'UTILITY')
!
      if(LENGTH.LE.0)stop 'BLD_iiil_GAIMT, IIIL BUFFER LENGTH = 0'
!
      do I=1,LENGTH
      Iao=IIIL_IN(I)%I
      Jao=IIIL_IN(I)%L
      G1=IIIL_IN(I)%iiil
      JS=Iao*(Iao+1)/2
      JT=Iao*(Iao-1)/2+Jao
      J_X_alpha(JS)=J_X_alpha(JS)+TWO*P_X0_beta(JT)*G1
      J_X_beta(JS)=J_X_beta(JS)+TWO*P_X0_alpha(JT)*G1
      J_X_alpha(JT)=J_X_alpha(JT)+P_X0_beta(JS)*G1
      J_X_beta(JT)=J_X_beta(JT)+P_X0_alpha(JS)*G1
      end do
!
! End of routine BLD_iiil_GAIMT
      call PRG_manager ('exit', 'BLD_iiil_GAIMT', 'UTILITY')
      return
      end subroutine BLD_iiil_GAIMT
      subroutine BLD_iiii_GAIMT (IIII_IN, &
                                 LENGTH)
!***********************************************************************
!     Date last modified: May 11, 2013                                 *
!     Author: Ahmad and R.A. Poirier                                   *
!     Description:  MUN Version.                                       *
!     Add IIII contributions to Matrix G=P*(IIII+IKJL).                *
!***********************************************************************
! Modules:
      implicit none
!
! Input scalars:
      integer LENGTH
!
! Input arrays:
      type (ints_2e_iiii) IIII_IN(LENGTH)
!
! Local scalars:
      integer I,Iao,I1,I2,Jao,L,JS,JT,MAX
      double precision G1
!
! Begin:
      call PRG_manager ('enter', 'BLD_iiii_GAIMT', 'UTILITY')
!
      if(LENGTH.LE.0)stop 'BLD_iiii_GAIMT, IIII BUFFER LENGTH = 0'
!
      do I=1,LENGTH
      Iao=IIII_IN(I)%I
      G1=IIII_IN(I)%iiii
      JS=Iao*(Iao+1)/2
      J_X_alpha(JS)=J_X_alpha(JS)+P_X0_beta(JS)*G1
      J_X_beta(JS)=J_X_beta(JS)+P_X0_alpha(JS)*G1
      end do
!
! End of routine BLD_iiii_GAIMT
      call PRG_manager ('exit', 'BLD_iiii_GAIMT', 'UTILITY')
      return
      end subroutine BLD_iiii_GAIMT
      end subroutine BLD_GM
      subroutine ORDR_CAO (CAOx, Xatom, Xbasis, Nbasis_G)
!*************************************************************************
!     Date last modified: Aug 30, 2013                                   *
!     Author: Ahmad                                                      *
!     Description:                                                       *
!                                                                        *
!*************************************************************************
! Modules:
      USE program_constants
      USE type_basis_set
      USE matrix_print
      implicit none
! Input scalars:
       Integer :: Xatom, Xbasis, Nbasis_G
! Local scalars: 
       integer :: Ibasis, AO_K, K, L
       double precision :: Sum1, Sum2, Stotal, Frac
! Local array:
       integer :: A(Xbasis), B(Nbasis_G-Xbasis)
       double precision :: CX(Nbasis_G,Nbasis_G)
! Input/Output array:
       double precision :: CAOx(Nbasis_G,Nbasis_G)
!
! Begin:
      K=0
      L=0
      do AO_K=1,Nbasis_G
        Sum1=ZERO
        Sum2=ZERO
        Stotal=ZERO
        do Ibasis=1,Nbasis_G
          Stotal=Stotal+CAOx(Ibasis,AO_K)**2
          if(Basis%AOtoAtom(Ibasis).eq.Xatom)then
            Sum1=Sum1+CAOx(Ibasis,AO_K)**2
          else
            Sum2=Sum2+CAOx(Ibasis,AO_K)**2
          end if
        end do ! Ibasis
        if(Stotal.eq.ZERO)then
          write(6,*)'ERROR> CAO equals zero'
          stop 'ERROR> CAO equals zero'
        end if
        Frac=Sum1/Stotal
        if(Frac.eq.PT5)then
          write(6,*)'ERROR> CAO has equal contribution from both A & B'
          stop 'ERROR> CAO has equal contribution from both A & B'
        end if
        if(Frac.gt.PT5)then
          K=K+1
          A(K)=AO_K
        else
          L=L+1
          B(L)=AO_K
        end if
      end do ! AO_K
      if(K.ne.Xbasis)then
        write(6,*)'ERROR> K is not equal Xbasis'
        stop 'ERROR> K is not equal Xbasis'
      end if
      CX=CAOx
      CAOx=ZERO
      do AO_K=1,Xbasis
         CAOx(1:Nbasis_G,AO_K)=CX(1:Nbasis_G,A(AO_K))
      end do ! AO_K
      L=0
      do AO_K=Xbasis+1,Nbasis_G
         L=L+1
         CAOx(1:Nbasis_G,AO_K)=CX(1:Nbasis_G,B(L))
      end do ! AO_K
!
! End of routine Ordr_CAO
      return
      end subroutine ORDR_CAO
      subroutine SRT_CAO (CAOi, CAOx, Nbasis_G)
!*************************************************************************
!     Date last modified: July 17, 2014                                  *
!     Author: Ahmad                                                      *
!     Description:                                                       *
!*************************************************************************
! Modules:
      USE program_constants
      USE type_basis_set                                            
      USE matrix_print
      implicit none
! Input scalars:
      Integer :: Nbasis_G
! Input array:
      double precision :: CAOi(Nbasis_G,Nbasis_G),CAO1(Nbasis_G,Nbasis_G)
! Input/Output array:
      double precision :: CAOx(Nbasis_G,Nbasis_G)
! Local scalars:
      integer :: Ibasis,AOx,AOi,AOr,NAOre,K,K1,L,NAOre1,SumAOs,SumNo
      double precision :: Diff
! Local array:
      double precision :: TwM(Nbasis_G,Nbasis_G)
      double precision :: STORE(Nbasis_G,Nbasis_G)
      integer :: MiLox_i(Nbasis_G), MiLoi_x(Nbasis_G), FitOrdr(Nbasis_G)
      integer, dimension(:), allocatable :: MiLox_i1, FitOrdr1
      integer, dimension(:), allocatable :: MiLox_i2, FitOrdr2
      integer, dimension(:), allocatable :: MiLox_i3, FitOrdr3
      integer, dimension(:), allocatable :: MiLoi_x1
      integer, dimension(:), allocatable :: MiLoi_x2
      integer, dimension(:), allocatable :: MiLoi_x3
      integer, dimension(:), allocatable :: AOiRemain, AOxRemain
      integer, dimension(:), allocatable :: AOiRemain1, AOxRemain1
      integer, dimension(:), allocatable :: AOiRemain2, AOxRemain2
      integer, dimension(:), allocatable :: AOiRemain3, AOxRemain3
      double precision, dimension(:,:), allocatable :: ReStor, ReStor1, ReStor2
!
! Begin:
      allocate(AOxRemain(Nbasis_G),AOiRemain(Nbasis_G))
      TwM=CAOx
      !write(6,*)'CAOi'
      !call PRT_matrix (CAOi, Nbasis_G, Nbasis_G)
      !write(6,*)'TwM'
      !call PRT_matrix (TwM, Nbasis_G, Nbasis_G)
      
      do AOi=1,Nbasis_G
        do AOx=1,Nbasis_G
          Diff=ZERO
          do Ibasis=1,Nbasis_G
           Diff=Diff+dabs(dabs(TwM(Ibasis,AOi))-dabs(CAOi(Ibasis,AOx)))
          end do ! Ibasis
          STORE(AOi,AOx)=Diff
        end do ! AOx
      end do ! AOi
!        write(6,*)'STORE'
!        call PRT_matrix (STORE, Nbasis_G, Nbasis_G)
      do AOx=1,Nbasis_G
        MiLox_i(AOx)=Minloc(STORE(AOx,1:Nbasis_G), dim=1)
        MiLoi_x(AOx)=Minloc(STORE(1:Nbasis_G,AOx), dim=1)
      end do ! AOx
      !write(6,*)'MiLox_i   =',MiLox_i
      !write(6,*)'MiLoi_x   =',MiLoi_x
      do AOx=1,Nbasis_G
        if(MiLoi_x(MiLox_i(AOx)).eq.AOx)then
          FitOrdr(AOx)=MiLox_i(AOx)
         else
           FitOrdr(AOx)=0
        end if ! MiLox_i(AOx).eq.MiLoi_x(MiLox_i(AOx)) 
      end do ! AOx
      
      NAOre=0
      AOxRemain=0
      AOiRemain=0
      do AOx=1,Nbasis_G
        if(FitOrdr(AOx).ne.0)cycle
        NAOre=NAOre+1
        AOxRemain(NAOre)=AOx
      end do ! AOx
      K=0
      do AOi=1,Nbasis_G
        if(any(FitOrdr.eq.AOi))cycle
        K=K+1
        AOiRemain(K)=AOi
      end do ! AOx
      !write(6,*)'AOxRemain =',AOxRemain
      !write(6,*)'AOiRemain =',AOiRemain
      if(NAOre.eq.1)then
        FitOrdr(AOxRemain(1))=AOiRemain(1)
      elseif(NAOre.ge.2)then
        allocate(ReStor(NAOre,NAOre),MiLox_i1(NAOre))
        allocate(MiLoi_x1(NAOre))
        allocate(AOxRemain1(NAOre),AOiRemain1(NAOre),FitOrdr1(NAOre))
        do AOi=1,NAOre
          do AOx=1,NAOre
            ReStor(AOx,AOi)=STORE(AOxRemain(AOx),AOiRemain(AOi))
          end do
        end do
!       write(6,*)'ReStor'
!       call PRT_matrix (ReStor, NAOre, NAOre)
!
      do AOx=1,NAOre 
        MiLox_i1(AOx)=Minloc(ReStor(AOx,1:NAOre), dim=1)
        MiLoi_x1(AOx)=Minloc(ReStor(1:NAOre,AOx), dim=1)
      end do ! AOx 
      do AOx=1,NAOre
        if(MiLoi_x1(MiLox_i1(AOx)).eq.AOx)then
          FitOrdr1(AOx)=MiLox_i1(AOx)
         else
           FitOrdr1(AOx)=0
        end if ! MiLox_i(AOx).eq.MiLoi_x(MiLox_i(AOx)) 
      end do ! AOx
        NAOre1=0
        AOxRemain1=0                          
        AOiRemain1=0                          
        do AOx=1,NAOre                             
          if(FitOrdr1(AOx).ne.0)cycle        
          NAOre1=NAOre1+1                               
          AOxRemain1(NAOre1)=AOx                    
        end do ! AOx                           
        L=0                               
        do AOx=1,NAOre                         
          if(any(FitOrdr1.eq.AOx))cycle  
          L=L+1                         
          AOiRemain1(L)=AOx              
        end do ! AOx                       
       ! write(6,*)'AOxRemain1 =',AOxRemain1
       ! write(6,*)'AOiRemain1 =',AOiRemain1
       ! write(6,*)'FitOrdr1   =',FitOrdr1
        if(NAOre1.eq.1)then
          FitOrdr1(AOxRemain1(1))=AOiRemain1(1)
        !  write(6,*)'FitOrdr1 12 =',FitOrdr
         else if (NAOre1.ge.2)then
           allocate(ReStor1(NAOre1,NAOre1),MiLox_i2(NAOre1))
           allocate(MiLoi_x2(NAOre1))
           allocate(AOxRemain2(NAOre1),AOiRemain2(NAOre1),FitOrdr2(NAOre1))
           do AOi=1,NAOre1
             do AOx=1,NAOre1
               ReStor1(AOx,AOi)=STORE(AOxRemain1(AOx),AOiRemain1(AOi))
             end do
           end do
!          write(6,*)'ReStor'
!          call PRT_matrix (ReStor, NAOre1, NAOre1)
!
           do AOx=1,NAOre1
             MiLox_i2(AOx)=Minloc(ReStor1(AOx,1:NAOre1), dim=1)
             MiLoi_x2(AOx)=Minloc(ReStor1(1:NAOre1,AOx), dim=1)
           end do ! AOx 
           do AOx=1,NAOre1
             if(MiLoi_x2(MiLox_i2(AOx)).eq.AOx)then
               FitOrdr2(AOx)=MiLox_i2(AOx)
              else
                FitOrdr2(AOx)=0
             end if ! MiLox_i(AOx).eq.MiLoi_x(MiLox_i(AOx)) 
           end do ! AOx
           K=0
           AOxRemain2=0
           AOiRemain2=0
           K=0
           do AOx=1,NAOre1
             if(FitOrdr2(AOx).ne.0)cycle
             K=K+1
             AOxRemain2(K)=AOx
           end do ! AOx                           
           L=0
           do AOx=1,NAOre1
             if(any(FitOrdr2.eq.AOx))cycle
             L=L+1
             AOiRemain2(L)=AOx
           end do ! AOx                       
          ! write(6,*)'AOxRemain2 =',AOxRemain2
          ! write(6,*)'AOiRemain2 =',AOiRemain2
          ! write(6,*)'FitOrdr1 2 =',FitOrdr2
           if(K.eq.1)then
             FitOrdr2(AOxRemain2(1))=AOiRemain2(1)
            else if(K.ge.2)then
           allocate(ReStor2(K,K),MiLox_i3(K))
           allocate(MiLoi_x3(K))
           allocate(AOxRemain3(K),AOiRemain3(K),FitOrdr3(K))
           do AOi=1,K
             do AOx=1,K
               ReStor2(AOx,AOi)=STORE(AOxRemain2(AOx),AOiRemain2(AOi))
             end do
           end do
!          write(6,*)'ReStor'
!          call PRT_matrix (ReStor, NAOre1, NAOre1)
!
           do AOx=1,K
             MiLox_i3(AOx)=Minloc(ReStor2(AOx,1:K), dim=1)
             MiLoi_x3(AOx)=Minloc(ReStor2(1:K,AOx), dim=1)
           end do ! AOx 
           do AOx=1,K
             if(MiLoi_x3(MiLox_i3(AOx)).eq.AOx)then
               FitOrdr3(AOx)=MiLox_i3(AOx)
              else
                FitOrdr3(AOx)=0
             end if ! MiLox_i(AOx).eq.MiLoi_x(MiLox_i(AOx)) 
           end do ! AOx
           K1=0
           AOxRemain3=0
           AOiRemain3=0
           do AOx=1,K
             if(FitOrdr3(AOx).ne.0)cycle
             K1=K1+1
             AOxRemain3(K1)=AOx
           end do ! AOx                           
           L=0
           do AOx=1,K
             if(any(FitOrdr3.eq.AOx))cycle
             L=L+1
             AOiRemain3(L)=AOx
           end do ! AOx                       
           !write(6,*)'AOxRemain2 =',AOxRemain2
           !write(6,*)'AOiRemain2 =',AOiRemain2
           !write(6,*)'FitOrdr1 2 =',FitOrdr2
           if(K1.eq.1)then
             FitOrdr3(AOxRemain3(1))=AOiRemain3(1)
            else if(K1.eq.2)then
               FitOrdr3(AOxRemain3(1))=AOiRemain3(2)
               FitOrdr3(AOxRemain3(2))=AOiRemain3(1)
            else if(K1.eq.3)then
               FitOrdr3(AOxRemain3(1))=AOiRemain3(2)
               FitOrdr3(AOxRemain3(2))=AOiRemain3(3)
               FitOrdr3(AOxRemain3(3))=AOiRemain3(1)
            else if(K1.eq.4)then
               FitOrdr3(AOxRemain3(1))=AOiRemain3(2)
               FitOrdr3(AOxRemain3(2))=AOiRemain3(1)
               FitOrdr3(AOxRemain3(3))=AOiRemain3(4)
               FitOrdr3(AOxRemain3(4))=AOiRemain3(3)
            else if(K1.eq.5)then
               FitOrdr3(AOxRemain3(1))=AOiRemain3(2)
               FitOrdr3(AOxRemain3(2))=AOiRemain3(1)
               FitOrdr3(AOxRemain3(3))=AOiRemain3(4)
               FitOrdr3(AOxRemain3(4))=AOiRemain3(3)
               FitOrdr3(AOxRemain3(5))=AOiRemain3(5)
            else if(K1.ge.6)then
               write(6,*)'Unable to re-order the orbitals'
               stop 'Unable to re-order the orbitals'
           end if ! K.eq.1
           do AOx=1,K
             FitOrdr2(AOxRemain2(AOx))=AOiRemain2(FitOrdr3(AOx))
           end do ! AOx=1,K
        end if  ! K.eq.1
           do AOx=1,NAOre1
             FitOrdr1(AOxRemain1(AOx))=AOiRemain1(FitOrdr2(AOx))
           end do ! AOx=1,NAOre1
        end if  ! NAOre1.eq.1
        do AOx=1,NAOre
          FitOrdr(AOxRemain(AOx))=AOiRemain(FitOrdr1(AOx))
        end do ! AOx=1,NAOre
      end if  ! NAOre.eq.1
      SumAOs=0
      do AOx=1,Nbasis_G
         SumAOs=SumAOs+FitOrdr(AOx)
      end do ! AOx
      SuMNo=Nbasis_G*(Nbasis_G+1)/2
      if(SumAOs.ne.SumNo)stop 'ERROR: AOs are not ordered correctly'
      TwM=CAOx
      do AOx=1,Nbasis_G
        CAOx(1:Nbasis_G,FitOrdr(AOx))=TwM(1:Nbasis_G,AOx)
      end do  !AOx=1,Nbasis_G
!      write(6,*)'CAOx*'
!      call PRT_matrix (CAOx, Nbasis_G, Nbasis_G)
!
! End of routine SRT_CAO
      return
      end subroutine SRT_CAO

