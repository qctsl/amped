      MODULE GAIMT_objects
!      VA_sq=VA_sq+Ssq_AA!*(DBLE(Nelectrons_A))!+two*abs(G_AxA)!/(sqrt(DBLE(Nelectrons_B+Nelectrons_A)))
!      VB_sq=VB_sq+Ssq_BB!*(DBLE(Nelectrons_B))!+Two*abs(G_BxB)!/(sqrt(DBLE(Nelectrons_B+Nelectrons_A)))
!*************************************************************************
!     Date last modified: Mar 27, 2014                                   *
!     Authors: Ahmad Alrawashdeh                                         *
!     Description: GAIMT objects                                         *
!*************************************************************************
! MODULES
      USE matrix_print
      USE program_constants
!
      implicit none                                             
!                                                                                                                         
! Local scalars:                                                                                                                       
      integer :: Jcycle,St
      integer :: I,J,IERROR,Iatom
      integer :: NoccAO_A,NoccAO_B,NDoccAO_A,NDoccAO_B,Nopen_A,Nopen_B
      integer :: Nelectrons_A,Nunpairele_A,Nelectrons_B,Nunpairele_B
      double precision :: SCFCON,ENERGY_GAIMT,ENERGY_GAIMT_total,Vnn_Gaim
      double precision :: EPREV,T,TEN,E_TA,E_VA,E_TB,E_VB,E_A,E_B,E_GAIM,Sab,Sba
      double precision :: EA,E_HAa,E_HBa,E_GAa,E_GBa,E_HAb,E_HBb,E_GAb,E_GBb
      double precision :: E_JAAa,E_JBBa,E_JAAb,E_JBBb,E_JABa,E_JBAa,E_JABb,E_JBAb
      double precision :: E_KAAa,E_KBBa,E_KAAb,E_KBBb,E_KABa,E_KBAa,E_KABb,E_KBAb
      double precision :: E_GAAa,E_GAAb,E_GABa,E_GABb
      double precision :: E_GBBa,E_GBBb,E_GBAa,E_GBAb
      double precision :: dE,E1,E2,PS_Aa,PS_Ab,PS_Ba,PS_Bb,E_VexAa,E_VexBa
      double precision :: E_VexAb,E_VexBb,E_TexA,E_TexB
!
! Work arrays: MATlen dimension:
      double precision, dimension(:), allocatable :: H_A
      double precision, dimension(:), allocatable :: H_B
      double precision, dimension(:), allocatable :: H_AB
      double precision, dimension(:), allocatable :: Hmol

      double precision, dimension(:), allocatable :: Tint_A
      double precision, dimension(:), allocatable :: Tint_B
      double precision, dimension(:), allocatable :: Tmol

      double precision, dimension(:), allocatable :: Vint_A
      double precision, dimension(:), allocatable :: Vint_B
      double precision, dimension(:), allocatable :: Vmol

      double precision, dimension(:), allocatable :: G_A_alpha
      double precision, dimension(:), allocatable :: G_A_beta
      double precision, dimension(:), allocatable :: G_B_alpha
      double precision, dimension(:), allocatable :: G_B_beta

      double precision, dimension(:), allocatable :: K_AB
      double precision, dimension(:), allocatable :: K_BA

      double precision, dimension(:), allocatable :: F_A_alpha
      double precision, dimension(:), allocatable :: F_A_beta
      double precision, dimension(:), allocatable :: F_B_alpha
      double precision, dimension(:), allocatable :: F_B_beta

      double precision, dimension(:), allocatable :: OCC_A_alpha
      double precision, dimension(:), allocatable :: OCC_A_beta
      double precision, dimension(:), allocatable :: OCC_B_alpha
      double precision, dimension(:), allocatable :: OCC_B_beta

      double precision, dimension(:), allocatable :: Smol

      !double precision, dimension(:), allocatable :: P_A_alpha
      double precision, dimension(:), allocatable :: P_A_beta
      double precision, dimension(:), allocatable :: P_B_alpha
      double precision, dimension(:), allocatable :: P_B_beta
      double precision, dimension(:), allocatable :: P_Aex
      double precision, dimension(:), allocatable :: P_Bex
      double precision, dimension(:), allocatable, save, target :: P_A_alpha
      double precision, dimension(:), allocatable, save, target :: P_A
      double precision, dimension(:), allocatable, save, target :: P_B
!
! Work arrays: NxN dimension:
      double precision, dimension(:,:), allocatable :: S_BA
      double precision, dimension(:,:), allocatable :: S_AB
      double precision, dimension(:,:), allocatable :: SMhalf
      double precision, dimension(:,:), allocatable :: SPhalf
      double precision, dimension(:,:), allocatable :: S_Smol

      double precision, dimension(:,:), allocatable :: T_A
      double precision, dimension(:,:), allocatable :: T_B

      double precision, dimension(:,:), allocatable :: VBA
      double precision, dimension(:,:), allocatable :: VAB
      double precision, dimension(:,:), allocatable :: V_Aa
      double precision, dimension(:,:), allocatable :: V_Ab
      double precision, dimension(:,:), allocatable :: V_Ba
      double precision, dimension(:,:), allocatable :: V_Bb

      double precision, dimension(:,:), allocatable :: CAO_A
      double precision, dimension(:,:), allocatable :: CAO_B
      double precision, dimension(:,:), allocatable :: C_AB
      double precision, dimension(:,:), allocatable :: C_BA
      double precision, dimension(:,:), allocatable :: TempCB
      double precision, dimension(:,:), allocatable, save :: GuessCAO_A
      double precision, dimension(:,:), allocatable, save :: GuessCAO_B
      double precision, dimension(:,:), allocatable, save :: CAO_Ai
      double precision, dimension(:,:), allocatable, save :: CAO_Bi
      double precision, dimension(:,:), allocatable, save :: C_ABi
      double precision, dimension(:,:), allocatable, save :: C_BAi

      double precision, dimension(:,:), allocatable :: FgaimA
      double precision, dimension(:,:), allocatable :: FgaimB
      double precision, dimension(:,:), allocatable :: FgaimM
      double precision, dimension(:,:), allocatable :: FgaimM_A
      double precision, dimension(:,:), allocatable :: FgaimM_B

      double precision, dimension(:,:), allocatable :: G_ABa
      double precision, dimension(:,:), allocatable :: G_ABb
      double precision, dimension(:,:), allocatable :: G_BAa
      double precision, dimension(:,:), allocatable :: G_BAb

      double precision, dimension(:,:), allocatable :: FAAa
      double precision, dimension(:,:), allocatable :: FAAb
      double precision, dimension(:,:), allocatable :: FBBa
      double precision, dimension(:,:), allocatable :: FBBb

      double precision, dimension(:,:), allocatable :: SQS
      double precision, dimension(:,:), allocatable :: HSQS
!
! Work arrays: MATlen_A and MATlen_B dimensions:
      double precision, dimension(:), allocatable :: H_AxA
      double precision, dimension(:), allocatable :: H_BxB
      double precision, dimension(:,:), allocatable :: H_AxB
      double precision, dimension(:,:), allocatable :: H_BxA

      double precision, dimension(:), allocatable :: T_AA
      double precision, dimension(:), allocatable :: T_BB

      double precision, dimension(:), allocatable :: V_AAa
      double precision, dimension(:), allocatable :: V_AAb
      double precision, dimension(:), allocatable :: V_BBa
      double precision, dimension(:), allocatable :: V_BBb

      double precision, dimension(:), allocatable :: P_AxA
      double precision, dimension(:), allocatable :: P_BxB
      double precision, dimension(:), allocatable :: P_AxAa
      double precision, dimension(:), allocatable :: P_AxAb
      double precision, dimension(:), allocatable :: P_BxBa
      double precision, dimension(:), allocatable :: P_BxBb

      double precision, dimension(:), allocatable :: G_AxA_alpha
      double precision, dimension(:), allocatable :: G_BxB_alpha
      double precision, dimension(:), allocatable :: G_AxA_beta
      double precision, dimension(:), allocatable :: G_BxB_beta

      double precision, dimension(:,:), allocatable :: G_AxB_alpha
      double precision, dimension(:,:), allocatable :: G_BxA_alpha
      double precision, dimension(:,:), allocatable :: G_AxB_beta
      double precision, dimension(:,:), allocatable :: G_BxA_beta

      double precision, dimension(:), allocatable :: F_AxA_alpha
      double precision, dimension(:), allocatable :: F_AxA_beta
      double precision, dimension(:), allocatable :: F_BxB_alpha
      double precision, dimension(:), allocatable :: F_BxB_beta

      double precision, dimension(:), allocatable :: OCC_AxA_alpha
      double precision, dimension(:), allocatable :: OCC_BxB_alpha
      double precision, dimension(:), allocatable :: OCC_AxA_beta
      double precision, dimension(:), allocatable :: OCC_BxB_beta

      double precision, dimension(:), allocatable :: Eigval_AxA
      double precision, dimension(:), allocatable :: Eigval_BxB
      double precision, dimension(:), allocatable :: Eigval_A
      double precision, dimension(:), allocatable :: Eigval_B
!
! Work arrays: AxA and BxB dimensions:
      double precision, dimension(:,:), allocatable :: CAO_AxA
      double precision, dimension(:,:), allocatable :: CAO_BxB
      double precision, dimension(:,:), allocatable :: C_AxB
      double precision, dimension(:,:), allocatable :: C_BxA

      double precision, dimension(:,:), allocatable :: HSQ_AxA
      double precision, dimension(:,:), allocatable :: HSQ_BxB

      double precision, dimension(:,:), allocatable :: S_AxA
      double precision, dimension(:,:), allocatable :: S_AxB
      double precision, dimension(:,:), allocatable :: S_BxA
      double precision, dimension(:,:), allocatable :: S_BxB
      double precision, dimension(:,:), allocatable :: SMhalfa
      double precision, dimension(:,:), allocatable :: SMhalfb

      double precision, dimension(:,:), allocatable :: T_AxB
      double precision, dimension(:,:), allocatable :: T_BxA

      double precision, dimension(:,:), allocatable :: V_AxB
      double precision, dimension(:,:), allocatable :: V_BxA

      double precision, dimension(:,:), allocatable :: TAxA
      double precision, dimension(:,:), allocatable :: TBxB
      double precision, dimension(:,:), allocatable :: TA
      double precision, dimension(:,:), allocatable :: TB
!
      double precision, dimension(:), allocatable :: S_AAa 
      double precision, dimension(:), allocatable :: S_AAb 
      double precision, dimension(:), allocatable :: S_BBa
      double precision, dimension(:), allocatable :: S_BBb
      double precision, dimension(:), allocatable :: HS_AAa 
      double precision, dimension(:), allocatable :: HS_AAb 
      double precision, dimension(:), allocatable :: HS_BBa
      double precision, dimension(:), allocatable :: HS_BBb
      double precision, dimension(:), allocatable :: GS_AAa
      double precision, dimension(:), allocatable :: GS_AAb
      double precision, dimension(:), allocatable :: GS_BBa
      double precision, dimension(:), allocatable :: GS_BBb
!
! End of MODULE QM_objects
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine ALOCT_OBJS (Nbasis, &
                             Abasis, &
                             Bbasis, &
                             MATlen, &
                             MATlen_A, &
                             MATlen_B)
!*************************************************************************************
!     Date last modified: Mar 27, 2014                                               *
!     Author: Ahmad Alrawashdeh                                                      *
!     Description: allocate them matrices of the GAIMT code.                         *
!*************************************************************************************
      implicit none
!
       integer :: Nbasis,Abasis,Bbasis
       integer :: MATlen,MATlen_A,MATlen_B
      !
! Objects:
      allocate (CAO_A(Nbasis,Nbasis), &
                CAO_B(Nbasis,Nbasis), &
                C_AB(Nbasis,Nbasis), &
                C_BA(Nbasis,Nbasis), &
                C_AxB(Abasis,Bbasis), &
                C_BxA(Bbasis,Abasis), &
                CAO_AxA(Abasis,Abasis), &
                CAO_BxB(Bbasis,Bbasis))
      allocate (TempCB(Nbasis,Nbasis), &
                HSQS(Nbasis,Nbasis), &
                SQS(Nbasis,Nbasis), &
                HSQ_AxA(Abasis,Abasis), &
                HSQ_BxB(Bbasis,Bbasis), &
                SMhalf(Nbasis,Nbasis), &
                SPhalf(Nbasis,Nbasis), &
                SMhalfa(Abasis,Abasis), &
                SMhalfb(Bbasis,Bbasis), &
                S_BA(Nbasis,Nbasis), &
                S_AB(Nbasis,Nbasis), &
                S_Smol(Nbasis,Nbasis), &
                S_AxA(Abasis,Abasis), &
                S_AxB(Abasis,Bbasis), &
                S_BxA(Bbasis,Abasis), &
                S_BxB(Bbasis,Bbasis), &
                H_AxB(Abasis,Bbasis), &
                H_BxA(Bbasis,Abasis), &
                G_AxB_alpha(Abasis,Bbasis), &
                G_BxA_alpha(Bbasis,Abasis), &
                G_AxB_beta(Abasis,Bbasis), &
                G_BxA_beta(Bbasis,Abasis), &
                T_A(Nbasis,Nbasis), &
                T_B(Nbasis,Nbasis), &
                T_AxB(Abasis,Bbasis), &
                T_BxA(Bbasis,Abasis))
      allocate (VBA(Nbasis,Nbasis), &
                VAB(Nbasis,Nbasis))
      allocate (V_Aa(Nbasis,Nbasis))
      allocate (V_Ab(Nbasis,Nbasis))
      allocate (V_Ba(Nbasis,Nbasis))
      allocate (V_Bb(Nbasis,Nbasis))
      allocate (V_AxB(Abasis,Bbasis))
      allocate (V_BxA(Bbasis,Abasis))
      allocate (TA(Nbasis,Nbasis), &
                TB(Nbasis,Nbasis), &
                TAxA(Abasis,Abasis), &
                TBxB(Bbasis,Bbasis))
      allocate (P_A_alpha(1:MATlen), &
                P_A_beta(1:MATlen), &
                P_B_alpha(1:MATlen), &
                P_B_beta(1:MATlen), &
                P_A(1:MATlen), &
                P_B(1:MATlen), &
                P_Aex(1:MATlen), &
                P_Bex(1:MATlen), &
                P_AxA(1:MATlen_A), &
                P_BxB(1:MATlen_B), &
                P_AxAa(1:MATlen_A), &
                P_AxAb(1:MATlen_A), &
                P_BxBa(1:MATlen_B), &
                P_BxBb(1:MATlen_B), &
                G_AxA_alpha(1:MATlen_A), &
                G_BxB_alpha(1:MATlen_B), &
                G_AxA_beta(1:MATlen_A), &
                G_BxB_beta(1:MATlen_B), &
                H_A(1:MATlen), &
                H_B(1:MATlen), &
                T_AA(1:MATlen), &
                T_BB(1:MATlen), &
                V_AAa(1:MATlen), &
                V_AAb(1:MATlen), &
                V_BBa(1:MATlen), &
                V_BBb(1:MATlen), &
                H_AB(1:MATlen), &
                H_AxA(1:MATlen_A), &
                H_BxB(1:MATlen_B), &
                Hmol(1:MATlen), &
                Tmol(1:MATlen), &
                Vmol(1:MATlen), &
                Smol(1:MATlen), &
                Tint_A(1:MATlen), &
                Tint_B(1:MATlen), &
                Vint_A(1:MATlen), &
                Vint_B(1:MATlen))
      allocate (FgaimM(Nbasis,Nbasis), &
                FgaimM_A(Nbasis,Nbasis), &
                FgaimM_B(Nbasis,Nbasis), &
                G_ABa(Nbasis,Nbasis), &
                G_ABb(Nbasis,Nbasis), &
                G_BAa(Nbasis,Nbasis), &
                G_BAb(Nbasis,Nbasis), &
                FAAa(Nbasis,Nbasis), &
                FAAb(Nbasis,Nbasis), &
                FBBa(Nbasis,Nbasis), &
                FBBb(Nbasis,Nbasis), &
                FgaimA(Abasis,Abasis), &
                FgaimB(Bbasis,Bbasis))
      allocate (Eigval_AxA(1:Abasis), &
                Eigval_BxB(1:Bbasis), &
                Eigval_A(Nbasis), &
                Eigval_B(Nbasis), &
                OCC_AxA_alpha(1:Abasis), &
                OCC_AxA_beta(1:Abasis), &
                OCC_BxB_alpha(1:Bbasis), &
                OCC_BxB_beta(1:Bbasis), &
                OCC_A_alpha(1:Nbasis), &
                OCC_A_beta(1:Nbasis), &
                OCC_B_alpha(1:Nbasis), &
                OCC_B_beta(1:Nbasis))
      allocate (F_A_alpha(1:MATlen), &
                F_B_alpha(1:MATlen), &
                F_A_beta(1:MATlen), &
                F_B_beta(1:MATlen), &
                K_AB(1:MATlen), &
                K_BA(1:MATlen), &
                S_AAa(1:MATlen), &
                S_AAb(1:MATlen), &
                S_BBa(1:MATlen), &
                S_BBb(1:MATlen), &
                HS_AAa(1:MATlen), &
                HS_AAb(1:MATlen), &
                HS_BBa(1:MATlen), &
                HS_BBb(1:MATlen), &
                GS_AAa(1:MATlen), &
                GS_AAb(1:MATlen), &
                GS_BBa(1:MATlen), &
                GS_BBb(1:MATlen), &
                G_A_beta(1:MATlen), &
                G_B_beta(1:MATlen), &
                G_A_alpha(1:MATlen), &
                G_B_alpha(1:MATlen))
      allocate (F_AxA_alpha(1:MATlen_A), &
                F_AxA_beta(1:MATlen_A), &
                F_BxB_alpha(1:MATlen_B), &
                F_BxB_beta(1:MATlen_B))
!
! End of routine ALOCT_OBJS
      return
      end subroutine ALOCT_OBJS
      subroutine DEALOCT_OBJS
!*************************************************************************************
!     Date last modified: Mar 27, 2014                                               *
!     Author: Ahmad Alrawashdeh                                                      *
!     Description: allocate them matrices of the GAIMT code.                         *
!*************************************************************************************
      implicit none
!
! Objects:
      deallocate (CAO_A, CAO_B, CAO_AxA, CAO_BxB, TempCB)
      deallocate (C_AB, C_BA, C_AxB, C_BxA)
      deallocate (HSQS, SQS, HSQ_AxA, HSQ_BxB, S_BA, S_AB, S_AxA, S_AxB, &
                  S_BxA, S_BxB, SMhalf, SPhalf, SMhalfa, SMhalfb, S_Smol)
      deallocate (T_A, T_B, T_AxB, T_BxA, T_AA, T_BB, TA, TB)
      deallocate (VBA, VAB, V_AxB, V_BxA, V_AAa, V_BBa, V_AAb, V_BBb)
      deallocate (TAxA, TBxB, V_Aa, V_Ba, V_Ab, V_Bb)
      deallocate (P_A_alpha, P_A_beta, P_B_alpha, P_B_beta, &
                  P_A, P_B, P_AxAa, P_BxBa, P_AxAb, &
                  P_BxBb, P_AxA, P_BxB, P_Aex, P_Bex)
      deallocate (H_A, H_B, H_AB, H_AxA, H_BxB, Hmol)
      deallocate (H_AxB, H_BxA, G_AxB_alpha, G_AxB_beta, G_BxA_alpha, G_BxA_beta)
      deallocate (Tmol, Vmol, Smol, Tint_A, Tint_B, Vint_A, Vint_B)
      deallocate (FgaimM, FgaimM_A, FgaimM_B, FgaimA, FgaimB)
      deallocate (Eigval_AxA, Eigval_BxB, Eigval_A, Eigval_B)
      deallocate (OCC_A_alpha, OCC_B_beta, OCC_A_beta, OCC_B_alpha)
      deallocate (OCC_AxA_alpha, OCC_BxB_beta, OCC_AxA_beta, OCC_BxB_alpha)
      deallocate (F_A_alpha, F_A_beta, F_B_alpha, F_B_beta, F_AxA_alpha, &
                  F_AxA_beta, F_BxB_alpha, F_BxB_beta)
      deallocate (G_A_alpha, G_A_beta, G_B_alpha, G_B_beta)
      deallocate (HS_AAa, HS_BBa, HS_AAb, HS_BBb, GS_AAa, GS_AAb, GS_BBa, GS_BBb)
      deallocate (FAAa, FBBa, FAAb, FBBb)
      deallocate (S_AAa, S_BBa, S_AAb, S_BBb)
      deallocate (K_AB, K_BA, G_ABa, G_ABb, G_BAa, G_BAb)
      deallocate (G_AxA_alpha, G_BxB_alpha, G_AxA_beta, G_BxB_beta)
!
! End of routine DEALOCT_OBJS
      return
      end subroutine DEALOCT_OBJS  
      subroutine PRT_Gaimt_S (Nbasis, Abasis, Bbasis, &
                              MATlen, MATlen_A, MATlen_B)
!*************************************************************************
!     Date last modified: Mar 28, 2014                                   *
!     Author: Ahmad                                                      *
!     Description: Print the overlap matrices of GAIMT.                  *
!*************************************************************************
      implicit none
!
! Local scalars:
      integer :: Nbasis, Abasis, Bbasis
      integer :: MATlen, MATlen_A, MATlen_B
!
      write(6,*)'Smol: '
      call PRT_matrix(SQS, Nbasis, Nbasis)
      write(6,*)'S_AxA: '
      call PRT_matrix(S_AxA, Abasis, Abasis)
      write(6,*)'S_AxB: '
      call PRT_matrix(S_AxB, Abasis, Bbasis)
      write(6,*)'S_BxA: '
      call PRT_matrix(S_BxA, Bbasis, Abasis)
      write(6,*)'S_BxB: '
      call PRT_matrix(S_BxB, Bbasis, Bbasis)
!
! End of routine PRT_Gaimt_S
      return
      end subroutine PRT_Gaimt_S
      subroutine PRT_Gaimt_H (Nbasis, Abasis, Bbasis, &
                              MATlen, MATlen_A, MATlen_B)
!*************************************************************************
!     Date last modified: Mar 28, 2014                                   *
!     Author: Ahmad                                                      *
!     Description: Print the H matrices of GAIMT.                        *
!*************************************************************************
      implicit none
!
! Local scalars:
      integer :: Nbasis, Abasis, Bbasis
      integer :: MATlen, MATlen_A, MATlen_B
!
      write(6,*)'Hmol: '
      call PRT_matrix (Hmol, MATlen, Nbasis)
      write(6,*)'H_A: '
      call PRT_matrix (H_A, MATlen, Nbasis)
      write(6,*)'H_B: '
      call PRT_matrix (H_B, MATlen, Nbasis)
      write(6,*)'H_AxA: '
      call PRT_matrix (H_AxA, MATlen_A, Abasis)
      write(6,*)'H_BxB: '
      call PRT_matrix (H_BxB, MATlen_B, Bbasis)
!
! End of routine PRT_Gaimt_H
      return
      end subroutine PRT_Gaimt_H
      subroutine PRT_Gaimt_CAO (Nbasis, Abasis, Bbasis, &
                                MATlen, MATlen_A, MATlen_B)
!*************************************************************************
!     Date last modified: Mar 28, 2014                                   *
!     Author: Ahmad                                                      *
!     Description: Print the coefficient matrices of GAIMT.              *
!*************************************************************************
      implicit none
!
! Local scalars:
      integer :: Nbasis, Abasis, Bbasis
      integer :: MATlen, MATlen_A, MATlen_B
!
      write(6,*)'COA_A='
      call PRT_matrix (CAO_A, Nbasis, Nbasis)
      write(6,*)'COA_B='
      call PRT_matrix (CAO_B, Nbasis, Nbasis)
      write(6,*)'COA_AxA='
      call PRT_matrix (CAO_AxA, Abasis, Abasis)
      write(6,*)'COA_BxB='
      call PRT_matrix (CAO_BxB, Bbasis, Bbasis)
!
! End of routine PRT_Gaimt_CAO
      return
      end subroutine PRT_Gaimt_CAO
      subroutine PRT_Gaimt_P (Nbasis, Abasis, Bbasis, &
                              MATlen, MATlen_A, MATlen_B)
!*************************************************************************
!     Date last modified: Mar 28, 2014                                   *
!     Author: Ahmad                                                      *
!     Description: Print the density matrices of GAIMT.                  *
!*************************************************************************
      implicit none
!
! Local scalars:
      integer :: Nbasis, Abasis, Bbasis
      integer :: MATlen, MATlen_A, MATlen_B
!
      write(6,*)'P_A_alpha= '
      call PRT_matrix (P_A_alpha, MATlen, Nbasis)
      write(6,*)'P_A_beta= '
      call PRT_matrix (P_A_beta, MATlen, Nbasis)
      write(6,*)'P_B_alpha= '
      call PRT_matrix (P_B_alpha, MATlen, Nbasis)
      write(6,*)'P_B_beta= '
      call PRT_matrix (P_B_beta, MATlen, Nbasis)

      write(6,*)'P_A ='
      call PRT_matrix (P_A, MATlen, Nbasis)
      write(6,*)'P_B ='
      call PRT_matrix (P_B, MATlen, Nbasis)
      write(6,*)'P_AxA ='
      call PRT_matrix (P_AxA, MATlen_A, Abasis)
      write(6,*)'P_BxB ='
      call PRT_matrix (P_BxB, MATlen_B, Bbasis)
!
! End of routine PRT_Gaimt_P
      return
      end subroutine PRT_Gaimt_P
      subroutine PRT_Gaimt_Vne (Nbasis, Abasis, Bbasis, &
                                MATlen, MATlen_A, MATlen_B)
!*************************************************************************
!     Date last modified: Mar 28, 2014                                   *
!     Author: Ahmad                                                      *
!     Description: Print the Vne matrices of GAIMT.                      *
!*************************************************************************
      implicit none
!
! Local scalars:
      integer :: Nbasis, Abasis, Bbasis
      integer :: MATlen, MATlen_A, MATlen_B
!
      write(6,*)'Vint_A ='
      call PRT_matrix (Vint_A, MATlen, Nbasis)
      write(6,*)'VBA ='
      call PRT_matrix (VBA, Nbasis, Nbasis)
      write(6,*)'VAB ='
      call PRT_matrix (VAB, Nbasis, Nbasis)
      write(6,*)'V_BxA ='
      call PRT_matrix (V_BxA, Bbasis, Abasis)
      write(6,*)'V_AxB ='
      call PRT_matrix (V_AxB, Abasis, Bbasis)
      write(6,*)'V_AA ='
      call PRT_matrix (V_AA, MATlen, Nbasis)
      write(6,*)'V_BB ='
      call PRT_matrix (V_BB, MATlen, Nbasis)
!
! End of routine PRT_Gaimt_Vne
      return
      end subroutine PRT_Gaimt_Vne
      subroutine PRT_Gaimt_G (Nbasis, Abasis, Bbasis, &
                              MATlen, MATlen_A, MATlen_B)
!*************************************************************************
!     Date last modified: Mar 28, 2014                                   *
!     Author: Ahmad                                                      *
!     Description: Print the G matrices of GAIMT.                        *
!*************************************************************************
      implicit none
!
! Local scalars:
      integer :: Nbasis, Abasis, Bbasis
      integer :: MATlen, MATlen_A, MATlen_B
!
      write(6,*)'G_A_alpha: '
      call PRT_matrix (G_A_alpha, MATlen, Nbasis)
      write(6,*)'G_A_beta: '
      call PRT_matrix (G_A_beta, MATlen, Nbasis)
      write(6,*)'G_B_alpha: '
      call PRT_matrix (G_B_alpha, MATlen, Nbasis)
      write(6,*)'G_B_beta: '
      call PRT_matrix (G_B_beta, MATlen, Nbasis)
!
! End of routine PRT_Gaimt_G
      return
      end subroutine PRT_Gaimt_G
      subroutine PRT_Gaimt_F (Nbasis, Abasis, Bbasis, &
                              MATlen, MATlen_A, MATlen_B)
!*************************************************************************
!     Date last modified: Mar 28, 2014                                   *
!     Author: Ahmad                                                      *
!     Description: Print the F matrices of GAIMT.                        *
!*************************************************************************
      implicit none
!
! Local scalars:
      integer :: Nbasis, Abasis, Bbasis
      integer :: MATlen, MATlen_A, MATlen_B
!
      write(6,*)'F_A_alpha: '
      call PRT_matrix (F_A_alpha, MATlen, Nbasis)
      write(6,*)'F_B_beta: '
      call PRT_matrix (F_B_beta, MATlen, Nbasis)
      write(6,*)'F_A_beta: '
      call PRT_matrix (F_A_beta, MATlen, Nbasis)
      write(6,*)'F_B_alpha: '
      call PRT_matrix (F_B_alpha, MATlen, Nbasis)
!
! End of routine PRT_Gaimt_F
      return
      end subroutine PRT_Gaimt_F
      subroutine PRT_E_COMP
!*************************************************************************
!     Date last modified: June 18, 2013                                  *
!     Author: Ahmad                                                      *
!     Description: Printing the energy components                        *
!*************************************************************************
      implicit none

! Local scalars:
      double precision :: Kin, Vne, H, Vex, Tex, G, E_T, Vir
!
      Kin=E_TA+E_TB
      Vne=E_VA+E_VB
      H=E_HAa+E_HAb+E_HBa+E_HBb
      G=PT5*(E_GAAa+E_GAAb+E_GBBa+E_GBBb+E_GABa+E_GABb+E_GBAa+E_GBAb)
      E_T=H+G+Vnn_Gaim
      Vir=-(Vne+G+Vnn_Gaim)/(E_TA+E_TB)

!      Vex=(E_VexAa+E_VexBa+E_VexAb+E_VexBb)/1.0d0
!      Tex=(E_TexA+E_TexB)!/TWO
!      G=PT5*(E_JAAa+E_JAAb+E_JBBa+E_JBBb+E_JABa+E_JABb+E_JBAa+E_JBAb &
!            -E_KAAa-E_KAAb-E_KBBa-E_KBBb+E_KABa+E_KABb+E_KBAa+E_KBAb)
!
      write(6,*)'Energy components: '
      write(6,'(a,1f12.6)')' Kinetic       :  ',Kin
      write(6,'(a,1f12.6)')' Potential     :  ',Vne
      write(6,'(a,1f12.6)')' Kin+Potential :  ',H
!      write(6,'(a,1f12.6)')' Vex           :  ',Vex
!      write(6,'(a,1f12.6)')' Tex           :  ',Tex
      write(6,'(a,1f12.6)')' E_Vee (J+K)   :  ',G
!      write(6,'(a,1f12.6)')' Sab           :  ',Sab
!      write(6,'(a,1f12.6)')' Sba           :  ',Sba
      write(6,'(a,1f12.6)')' Nuclear       :  ',Vnn_Gaim
      write(6,'(a,1f12.6)')' Total energy  :  ',E_T
      write(6,'(a,1f12.6)')' Virial        :  ',Vir
!
! Print energy components:
      write(6,*)' '
      write(6,'(a,1f12.6)')' E_TA : ',E_TA
      write(6,'(a,1f12.6)')' E_VA : ',E_VA
      write(6,'(a,1f12.6)')' E_TB : ',E_TB
      write(6,'(a,1f12.6)')' E_VB : ',E_VB
      write(6,*)' '
      write(6,'(a,1f12.6)')' E_HAa: ',E_HAa
      write(6,'(a,1f12.6)')' E_HAb: ',E_HAb
      write(6,'(a,1f12.6)')' E_HBa: ',E_HBa
      write(6,'(a,1f12.6)')' E_HBb: ',E_HBb
      write(6,*)' '
      write(6,'(a,1f12.6)')' E_GAAa:',E_GAAa
      write(6,'(a,1f12.6)')' E_GAAb:',E_GAAb
      write(6,'(a,1f12.6)')' E_GBBa:',E_GBBa
      write(6,'(a,1f12.6)')' E_GBBb:',E_GBBb
      write(6,*)' '
      write(6,'(a,1f12.6)')' E_GABa:',E_GABa
      write(6,'(a,1f12.6)')' E_GABb:',E_GABb
      write(6,'(a,1f12.6)')' E_GBAa:',E_GBAa
      write(6,'(a,1f12.6)')' E_GBAb:',E_GBAb
      write(6,*)' '
!      write(6,'(a,1f12.6)')' E_JAAa: ',E_JAAa
!      write(6,'(a,1f12.6)')' E_JAAb: ',E_JAAb
!      write(6,'(a,1f12.6)')' E_JBBa: ',E_JBBa
!      write(6,'(a,1f12.6)')' E_JBBb: ',E_JBBb
!      write(6,*)' '
!      write(6,'(a,1f12.6)')' E_JABa: ',E_JABa
!      write(6,'(a,1f12.6)')' E_JABb: ',E_JABb
!      write(6,'(a,1f12.6)')' E_JBAa: ',E_JBAa
!      write(6,'(a,1f12.6)')' E_JBAb: ',E_JBAb
!      write(6,*)' '
!      write(6,'(a,1f12.6)')' E_KAAa: ',E_KAAa
!      write(6,'(a,1f12.6)')' E_KAAb: ',E_KAAb
!      write(6,'(a,1f12.6)')' E_KBBa: ',E_KBBa
!      write(6,'(a,1f12.6)')' E_KBBb: ',E_KBBb
!      write(6,*)' '
!      write(6,'(a,1f12.6)')' E_KABa: ',E_KABa
!      write(6,'(a,1f12.6)')' E_KABb: ',E_KABb
!      write(6,'(a,1f12.6)')' E_KBAa: ',E_KBAa
!      write(6,'(a,1f12.6)')' E_KBAb: ',E_KBAb
!      write(6,*)' '
!      write(6,'(a,1f12.6)')' E_VexA:',E_VexAa+E_VexAb
!      write(6,'(a,1f12.6)')' E_VexB:',E_VexBa+E_VexBb
!      write(6,'(a,1f12.6)')' E_TexA:',E_TexA
!      write(6,'(a,1f12.6)')' E_TexB:',E_TexB
      
!
! End of routine PRT_E_COMP
      return
      end subroutine PRT_E_COMP
      end module GAIMT_objects
      subroutine BLD_JandK_GAIM (Jcoul, Kexch, PT, Pa, MATlen_G, Nbasis_G)
!*****************************************************************************
!     Date last modified: May 22, 2013                                       *
!     Author: Ahmad and R.A. Poirier                                         *
!     Description: Form the two-electron part of the Fock matrix, given the  *
!     density matrix and any or all two-electron integrals.                  *
!*****************************************************************************
! Modules:
      USE program_constants
      USE matrix_print
      USE QM_defaults
      USE QM_objects
      USE INT_objects

      implicit none
!
! Input scalars:
      integer :: MATlen_G,Nbasis_G
!
! Input arrays:
      double precision :: PT(MATlen_G),Pa(MATlen_G)
      double precision :: Jcoul(MATlen_G),Kexch(MATlen_G)
!
! Local scalars:
      integer :: I,LENGTH
      logical :: EOF
!
! Begin:
      call PRG_manager ('enter', 'BLD_JandK_GAIM', 'UTILITY')
!
      write(6,*)'PT '
      call PRT_matrix (PT, MATlen_G, Nbasis_G)
      write(6,*)'Pa: '
      call PRT_matrix (Pa, MATlen_G, Nbasis_G)
!
! Initialize Jcoul/Kexch.
      Jcoul(1:MATlen_G)=ZERO
      Kexch(1:MATlen_G)=ZERO
!
      IF(LIJKL_ONDISK)then
        EOF=.FALSE.
        rewind IJKLUI
        call READ_ijkl (IJKL_BUFFER, IJKLLN, IJKLUI, LENGTH, EOF)
        do while (.not.EOF)
          call BLD_JKM_ijkl (IJKL_BUFFER, LENGTH)
          call READ_ijkl (IJKL_BUFFER, IJKLLN, IJKLUI, LENGTH, EOF)
        end do
      end if ! ONDISK
!
      IF(LIIKL_ONDISK)then
        EOF=.FALSE.
        rewind IIKLUI
        call READ_iikl (IIKL_BUFFER, IIKLLN, IIKLUI, LENGTH, EOF)
        do while (.not.EOF)
          call BLD_JKM_iikl (IIKL_BUFFER, LENGTH)
          call READ_iikl (IIKL_BUFFER, IIKLLN, IIKLUI, LENGTH, EOF)
        end do
      end if ! ONDISK
!
      IF(LIJKJ_ONDISK)then
        EOF=.FALSE.
        rewind IJKJUI
        call READ_ijkj (IJKJ_BUFFER, IJKJLN, IJKJUI, LENGTH, EOF)
        do while (.not.EOF)
          call BLD_JKM_ijkj (IJKJ_BUFFER, LENGTH)
          call READ_ijkj (IJKJ_BUFFER, IJKJLN, IJKJUI, LENGTH, EOF)
        end do
      end if ! ONDISK
!
      IF(LIJJL_ONDISK)then
        EOF=.FALSE.
        rewind IJJLUI
        call READ_ijjl (IJJL_BUFFER, IJJLLN, IJJLUI, LENGTH, EOF)
        do while (.not.EOF)
          call BLD_JKM_ijjl (IJJL_BUFFER, LENGTH)
          call READ_ijjl (IJJL_BUFFER, IJJLLN, IJJLUI, LENGTH, EOF)
        end do
      end if ! ONDISK

! Now add contributions from INCORE arrays
      call BLD_JKM_ijkl (IJKL_INCORE, IJKLCN)
      call BLD_JKM_iikl (IIKL_INCORE, IIKLCN)
      call BLD_JKM_ijkj (IJKJ_INCORE, IJKJCN)
      call BLD_JKM_ijjl (IJJL_INCORE, IJJLCN)
!
      if(Lall_ONDISK)call READ_iiii
      call BLD_JKM_iikk (IIKK_INCORE, IIKKCN)
      call BLD_JKM_ijjj (IJJJ_INCORE, IJJJCN)
      call BLD_JKM_iiil (IIIL_INCORE, IIILCN)
      call BLD_JKM_iiii (IIII_INCORE, IIIICN)
!
! end of routine JcoulKMBLD
      call PRG_manager ('exit', 'BLD_JandK_GAIM', 'UTILITY')
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine BLD_JKM_ijkl (IJKL_IN, &
                               LENGTH)
!***********************************************************************
!     Date last modified: July 07, 1992                                *
!     Author: F. Colonna and R.A. Poirier                              *
!     Description:  MUN Version.                                       *
!     Add IJKL contributions to Matrix G=P*(IJKL+IKJL+ILKJ).           *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer, intent(IN) :: LENGTH
!
! Input arrays:
      type (INTS_2e_ijkl) IJKL_IN(LENGTH)
!
! Local scalars:
      integer I,JS,JT,Iao,Jao,Kao,Lao
      double precision A1,A2,A3,G1,G2,G3
!
! Begin:
      call PRG_manager ('enter', 'BLD_JKM_ijkl', 'UTILITY')
!
      IF(LENGTH.GT.0)then
      do I=1,LENGTH
      Iao=IJKL_IN(I)%I
      Jao=IJKL_IN(I)%J
      Kao=IJKL_IN(I)%K
      Lao=IJKL_IN(I)%L
      A1=IJKL_IN(I)%IJKL
      A2=IJKL_IN(I)%IKJL
      A3=IJKL_IN(I)%ILJK
      G1=(A2+A3)
      G2=(A1+A3)
      G3=(A1+A2)
!
      JS=Iao*(Iao-1)/2+Jao
      JT=Kao*(Kao-1)/2+Lao
      Jcoul(JS)=Jcoul(JS)+PT(JT)*A1
      Jcoul(JT)=Jcoul(JT)+PT(JS)*A1
      Kexch(JS)=Kexch(JS)+Pa(JT)*G1
      Kexch(JT)=Kexch(JT)+Pa(JS)*G1
      JS=Iao*(Iao-1)/2+Kao
      JT=Jao*(Jao-1)/2+Lao
      Jcoul(JS)=Jcoul(JS)+PT(JT)*A2
      Jcoul(JT)=Jcoul(JT)+PT(JS)*A2
      Kexch(JS)=Kexch(JS)+Pa(JT)*G2
      Kexch(JT)=Kexch(JT)+Pa(JS)*G2
      JS=Iao*(Iao-1)/2+Lao
      JT=Jao*(Jao-1)/2+Kao
      Jcoul(JS)=Jcoul(JS)+PT(JT)*A3
      Jcoul(JT)=Jcoul(JT)+PT(JS)*A3
      Kexch(JS)=Kexch(JS)+Pa(JT)*G3
      Kexch(JT)=Kexch(JT)+Pa(JS)*G3
      end do
      end if
!
! End of routine BLD_JKM_ijkl
      call PRG_manager ('exit', 'BLD_JKM_ijkl', 'UTILITY')
      return
      end subroutine BLD_JKM_ijkl
      subroutine BLD_JKM_iiii (IIII_IN, &
                               LENGTH)
!***********************************************************************
!     Date last modified: December 16, 1997                            *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version.                                       *
!     Add IJKL contributions to Matrix G=P*(IJKL+IKJL).                *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer, intent(IN) :: LENGTH
!
! Input arrays:
      type (ints_2e_iiii) IIII_IN(LENGTH)
!
! Local scalars:
      integer I,JS,JT,Iao
      double precision G1
!
! Begin:
      call PRG_manager ('enter', 'BLD_JKM_iiii', 'UTILITY')
!
      IF(LENGTH.GT.0)then
        do I=1,LENGTH
        Iao=IIII_IN(I)%I
        G1=IIII_IN(I)%iiii
        JS=Iao*(Iao+1)/2
        Jcoul(JS)=Jcoul(JS)+PT(JS)*G1*PT5
        Kexch(JS)=Kexch(JS)+Pa(JS)*G1
        end do
      end if
!
! End of routine BLD_JKM_iiii
      call PRG_manager ('exit', 'BLD_JKM_iiii', 'UTILITY')
      return
      end subroutine BLD_JKM_iiii
      subroutine BLD_JKM_ijkj (IJKJ_IN, &
                               LENGTH)
!***********************************************************************
!     Date last modified: December 16, 1997                            *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version.                                       *
!     Add IJKJ contributions to Matrix G=P*(IJKJ+IKJJ).                *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer, intent(IN) :: LENGTH
!
! Input arrays:
      double precision PT(MATlen_G), Pa(MATlen_G)
      type (ints_2e_ijkj) IJKJ_IN(LENGTH)
!
! Local scalars:
      integer I,L,JS,JT,Iao,Jao,Kao
      double precision A1,A2,G1,G2
!
! Begin:
      call PRG_manager ('enter', 'BLD_JKM_ijkj', 'UTILITY')
!
      if(LENGTH.gt.0)then
      do I=1,LENGTH
        Iao=IJKJ_IN(I)%I
        Jao=IJKJ_IN(I)%J
        Kao=IJKJ_IN(I)%K
        A1=IJKJ_IN(I)%IJKJ
        A2=IJKJ_IN(I)%IKJJ
!
        JS=Iao*(Iao-1)/2+Jao
        JT=Kao*(Kao-1)/2+Jao
        Jcoul(JS)=Jcoul(JS)+PT(JT)*A1
        Jcoul(JT)=Jcoul(JT)+PT(JS)*A1
        Kexch(JS)=Kexch(JS)+Pa(JT)*(A1+A2)
        Kexch(JT)=Kexch(JT)+Pa(JS)*(A1+A2)
        JS=Jao*(Jao+1)/2
        JT=Iao*(Iao-1)/2+Kao
        Jcoul(JS)=Jcoul(JS)+PT(JT)*A2
        Jcoul(JT)=Jcoul(JT)+PT(JS)*A2*PT5
        Kexch(JS)=Kexch(JS)+Pa(JT)*A1*TWO
        Kexch(JT)=Kexch(JT)+Pa(JS)*A1
      end do
      end if ! LENGTH.gt.0
!
! End of routine BLD_JKM_ijkj
      call PRG_manager ('exit', 'BLD_JKM_ijkj', 'UTILITY')
      return
      end subroutine BLD_JKM_ijkj
      subroutine BLD_JKM_iikl (IIKL_IN, &
                               LENGTH)
!***********************************************************************
!     Date last modified: December 16, 1997                            *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version.                                       *
!     Add IIKL contributions to Matrix G=P*(IIKL+IKJJ).                *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer, intent(IN) :: LENGTH
!
! Input arrays:
      double precision PT(MATlen_G), Pa(MATlen_G)
      type (ints_2e_iikl) IIKL_IN(LENGTH)
!
! Local scalars:
      integer I,JS,JT,Iao,Jao,Kao
      double precision A1,A2
!
! Begin:
      call PRG_manager ('enter', 'BLD_JKM_iikl', 'UTILITY')
!
      if(LENGTH.gt.0)then
      do I=1,LENGTH
        Iao=IIKL_IN(I)%I
        Jao=IIKL_IN(I)%K
        Kao=IIKL_IN(I)%L
        A1=IIKL_IN(I)%iikl
        A2=IIKL_IN(I)%ikil
!
        JS=Iao*(Iao+1)/2
        JT=Jao*(Jao-1)/2+Kao
        Jcoul(JS)=Jcoul(JS)+PT(JT)*A1
        Jcoul(JT)=Jcoul(JT)+PT(JS)*A1*PT5
        Kexch(JS)=Kexch(JS)+Pa(JT)*TWO*A2
        Kexch(JT)=Kexch(JT)+Pa(JS)*A2
        JS=Iao*(Iao-1)/2+Jao
        JT=Iao*(Iao-1)/2+Kao
        Jcoul(JS)=Jcoul(JS)+PT(JT)*A2
        Jcoul(JT)=Jcoul(JT)+PT(JS)*A2
        Kexch(JS)=Kexch(JS)+Pa(JT)*(A1+A2)
        Kexch(JT)=Kexch(JT)+Pa(JS)*(A1+A2)
      end do
      end if ! LENGTH.gt.0
!
! End of routine BLD_JKM_iikl
      call PRG_manager ('exit', 'BLD_JKM_iikl', 'UTILITY')
      return
      end subroutine BLD_JKM_iikl
      subroutine BLD_JKM_iikk (IIKK_IN, &
                               LENGTH)
!***********************************************************************
!     Date last modified: December 16, 1997                            *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version.                                       *
!     Add IIKK contributions to Matrix G=P*(IIKK+IKIK).                *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer, intent(IN) :: LENGTH
!
! Input arrays:
      double precision PT(MATlen_G), Pa(MATlen_G)
      type (ints_2e_iikk) IIKK_IN(LENGTH)
!
! Local scalars:
      integer I,JS,JT,Iao,Jao
      double precision A1,A2
!
! Begin:
      call PRG_manager ('enter', 'BLD_JKM_iikk', 'UTILITY')
!
      if(LENGTH.gt.0)then
      do I=1,LENGTH
        Iao=IIKK_IN(I)%I
        Jao=IIKK_IN(I)%K
        A1=IIKK_IN(I)%iikk
        A2=IIKK_IN(I)%ikik

        JS=Iao*(Iao+1)/2
        JT=Jao*(Jao+1)/2
        Jcoul(JS)=Jcoul(JS)+PT(JT)*A1*PT5
        Jcoul(JT)=Jcoul(JT)+PT(JS)*A1*PT5
        Kexch(JS)=Kexch(JS)+Pa(JT)*A2
        Kexch(JT)=Kexch(JT)+Pa(JS)*A2
        JS=Iao*(Iao-1)/2+Jao
        Jcoul(JS)=Jcoul(JS)+PT(JS)*A2
        Kexch(JS)=Kexch(JS)+Pa(JS)*(A1+A2)
      end do
      end if ! LENGTH.gt.0
!
! End of routine BLD_JKM_iikk
      call PRG_manager ('exit', 'BLD_JKM_iikk', 'UTILITY')
      return
      end subroutine BLD_JKM_iikk
      subroutine BLD_JKM_ijjj (IJJJ_IN, &
                               LENGTH)
!***********************************************************************
!     Date last modified: December 16, 1997                            *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version.                                       *
!     Add IJKL contributions to Matrix G=P*(IJKL+IKJL).                *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer, intent(IN) :: LENGTH
!
! Input arrays:
      double precision PT(MATlen_G), Pa(MATlen_G)
      type (ints_2e_ijjj) IJJJ_IN(LENGTH)
!
! Local scalars:
      integer I,JS,JT,Iao,Jao
      double precision G1
!
! Begin:
      call PRG_manager ('enter', 'BLD_JKM_ijjj', 'UTILITY')
!
      IF(LENGTH.GT.0)then
        do I=1,LENGTH
        Iao=IJJJ_IN(I)%I
        Jao=IJJJ_IN(I)%J
        G1=IJJJ_IN(I)%ijjj
        JS=Iao*(Iao-1)/2+Jao
        JT=Jao*(Jao+1)/2
        Jcoul(JS)=Jcoul(JS)+PT(JT)*G1*PT5
        Jcoul(JT)=Jcoul(JT)+PT(JS)*G1
        Kexch(JS)=Kexch(JS)+Pa(JT)*G1
        Kexch(JT)=Kexch(JT)+Pa(JS)*G1*TWO
        end do
      end if
!
! End of routine BLD_JKM_ijjj
      call PRG_manager ('exit', 'BLD_JKM_ijjj', 'UTILITY')
      return
      end subroutine BLD_JKM_ijjj
      subroutine BLD_JKM_iiil (IIIL_IN, &
                               LENGTH)
!***********************************************************************
!     Date last modified: December 16, 1997                            *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version.                                       *
!     Add IJKL contributions to Matrix G=P*(IJKL+IKJL).                *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer, intent(IN) :: LENGTH
!
! Input arrays:
      double precision PT(MATlen_G), Pa(MATlen_G)
      type (ints_2e_iiil) IIIL_IN(LENGTH)
!
! Local scalars:
      integer I,JS,JT,Iao,Jao
      double precision G1
!
! Begin:
      call PRG_manager ('enter', 'BLD_JKM_iiil', 'UTILITY')
!
      IF(LENGTH.GT.0)then
        do I=1,LENGTH
        Iao=IIIL_IN(I)%I
        Jao=IIIL_IN(I)%L
        G1=IIIL_IN(I)%iiil
        JS=Iao*(Iao+1)/2
        JT=Iao*(Iao-1)/2+Jao
        Jcoul(JS)=Jcoul(JS)+PT(JT)*G1
        Jcoul(JT)=Jcoul(JT)+PT(JS)*G1*PT5
        Kexch(JS)=Kexch(JS)+Pa(JT)*G1*TWO
        Kexch(JT)=Kexch(JT)+Pa(JS)*G1
        end do
      end if
!
! End of routine BLD_JKM_iiil
      call PRG_manager ('exit', 'BLD_JKM_iiil', 'UTILITY')
      return
      end subroutine BLD_JKM_iiil
      subroutine BLD_JKM_ijjl (IJJL_IN, &
                               LENGTH)
!***********************************************************************
!     Date last modified: December 16, 1997                            *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version.                                       *
!     Add IJJL contributions to Matrix G=P*(IJJL+ILJJ).                *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer, intent(IN) :: LENGTH
!
! Input arrays:
      double precision PT(MATlen_G), Pa(MATlen_G)
      type (ints_2e_ijjl) IJJL_IN(LENGTH)
!
! Local scalars:
      integer I,JS,JT,Iao,Jao,Kao
      double precision A1,A2
!
! Begin:
      call PRG_manager ('enter', 'BLD_JKM_ijjl', 'UTILITY')
!
      if(LENGTH.gt.0)then
      do I=1,LENGTH
        Iao=IJJL_IN(I)%I
        Jao=IJJL_IN(I)%J
        Kao=IJJL_IN(I)%L
        A1=IJJL_IN(I)%ijjl
        A2=IJJL_IN(I)%iljj
!
        JS=Iao*(Iao-1)/2+Jao
        JT=Jao*(Jao-1)/2+Kao
        Jcoul(JS)=Jcoul(JS)+PT(JT)*A1
        Jcoul(JT)=Jcoul(JT)+PT(JS)*A1
        Kexch(JS)=Kexch(JS)+Pa(JT)*(A1+A2)
        Kexch(JT)=Kexch(JT)+Pa(JS)*(A1+A2)
        JS=Jao*(Jao+1)/2
        JT=Iao*(Iao-1)/2+Kao
        Jcoul(JS)=Jcoul(JS)+PT(JT)*A2
        Jcoul(JT)=Jcoul(JT)+PT(JS)*A2*PT5
        Kexch(JS)=Kexch(JS)+Pa(JT)*A1*TWO
        Kexch(JT)=Kexch(JT)+Pa(JS)*A1
      end do
      end if ! LENGTH.gt.0
!
! End of routine BLD_JKM_ijjl
      call PRG_manager ('exit', 'BLD_JKM_ijjl', 'UTILITY')
      return
      end subroutine BLD_JKM_ijjl
      end subroutine BLD_JandK_GAIM
