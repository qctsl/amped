      subroutine electronic_dipole (Iatom, Ifound)
!***********************************************************************
!     Date last modified April 25th ,2005                              *
!     Author: Aisha                                                    *
!     Description:                                                     *
!***********************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE N_integration
      USE NI_Dipole

      implicit none

! Local scalars:
      integer :: IApoint,Z_num
      integer :: Iatom,Ifound
      type(dipole_moment), dimension(:), allocatable :: point_dipole

! Begin: 

      allocate(point_dipole(NApts_atom))

      do IApoint=1,NApts_atom
        point_dipole(IApoint)%X=charge(IApoint)*(grid_points(IApoint)%X)* &
        Bweights(IApoint)*grid_points(IApoint)%w
        point_dipole(IApoint)%Y=charge(IApoint)*(grid_points(IApoint)%Y)* &
        Bweights(IApoint)*grid_points(IApoint)%w
        point_dipole(IApoint)%Z=charge(IApoint)*(grid_points(IApoint)%Z)* &
        Bweights(IApoint)*grid_points(IApoint)%w
      end do

      do IApoint=1,NApts_atom
        elec_dipole(Iatom)%X=elec_dipole(Iatom)%X+point_dipole(IApoint)%X
        elec_dipole(Iatom)%Y=elec_dipole(Iatom)%Y+point_dipole(IApoint)%Y
        elec_dipole(Iatom)%Z=elec_dipole(Iatom)%Z+point_dipole(IApoint)%Z
      end do !IApoint

      elec_dipole(Iatom)%X=-FourPi*elec_dipole(Iatom)%X 
      elec_dipole(Iatom)%Y=-FourPi*elec_dipole(Iatom)%Y
      elec_dipole(Iatom)%Z=-FourPi*elec_dipole(Iatom)%Z

      deallocate(point_dipole)
      return
      end subroutine electronic_dipole
      subroutine total_dipole (Iatom, Ifound) 
!******************************************************************
!   Date last modified: Oct 11th, 2001                            *
!   Author: Aisha                                                 *
!   Description:                                                  *
!******************************************************************
! Modules:
      USE N_integration
      USE NI_Dipole
      USE type_molecule

      implicit none

! Local scalars:  
      integer :: Iatom,Ifound

! Begin:
      call electronic_dipole (Iatom, Ifound)

      elec_dipole_tot%X=elec_dipole_tot%X+elec_dipole(Iatom)%X
      elec_dipole_tot%Y=elec_dipole_tot%Y+elec_dipole(Iatom)%Y
      elec_dipole_tot%Z=elec_dipole_tot%Z+elec_dipole(Iatom)%Z

      nuclear_dipole%X=nuclear_dipole%X+CARTESIAN(Iatom)%Atomic_number*CARTESIAN(Iatom)%X
      nuclear_dipole%Y=nuclear_dipole%Y+CARTESIAN(Iatom)%Atomic_number*CARTESIAN(Iatom)%Y
      nuclear_dipole%Z=nuclear_dipole%Z+CARTESIAN(Iatom)%Atomic_number*CARTESIAN(Iatom)%Z
      
      return
      end subroutine total_dipole  
      subroutine PRT_NI_dipole
!***********************************************************************
!     Date last modified December 2013                                 *
!     Author: R.A. Poirier                                             *
!     Description: Print the dipole moment results                     *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE NI_dipole

      implicit none

! Local scalars:  

! Begin:
      elec_dipole_tot%total =dsqrt((elec_dipole_tot%X)**2 + (elec_dipole_tot%Y)**2 + (elec_dipole_tot%Z)**2)
      net_dipole%X   = elec_dipole_tot%X+nuclear_dipole%X
      net_dipole%Y   = elec_dipole_tot%Y+nuclear_dipole%Y
      net_dipole%Z   = elec_dipole_tot%Z+nuclear_dipole%Z
      net_dipole%total = dsqrt((net_dipole%X)**2+(net_dipole%Y)**2+(net_dipole%Z)**2)
      error_dipole%X = net_dipole%X - Dipole%X
      error_dipole%Y = net_dipole%Y - Dipole%Y
      error_dipole%Z = net_dipole%Z - Dipole%Z
      error_dipole%total = net_dipole%total - Dipole%total
      write(UNIout,'(a,f14.6,f16.6,14x,1PE21.3)')'Dipole     ',net_dipole%total,Dipole%total,error_dipole%total

      return
      end subroutine PRT_NI_dipole
      subroutine PRT_NI_dipole_AIM
!***********************************************************************
!     Date last modified December 2013                                 *
!     Author: R.A. Poirier                                             *
!     Description: Print the dipole moment results                     *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE NI_dipole

      implicit none

! Local scalars:  
      integer :: Iatom
!
! Begin:
      write(UNIout,'(a)')'Electronic dipole moment by atom:'
      do Iatom=1,Natoms
        write(UNIout,'(i8,3f14.8)')Iatom,elec_dipole(Iatom)%x,elec_dipole(Iatom)%y,elec_dipole(Iatom)%z
      end do

      deallocate(elec_dipole)

      return
      end subroutine PRT_NI_dipole_AIM
      subroutine INI_NI_Dipole
!************************************************************************************
!     Date last modified December 10, 2013                                          *
!     Author: R.A. Poirier                                                          *
!     Description: Initialization for the dipole moment (first)                     *
!************************************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE NI_dipole

      implicit none

! Input scalars:
      allocate(elec_dipole(Natoms))
      elec_dipole(1:Natoms)%X=ZERO
      elec_dipole(1:Natoms)%Y=ZERO
      elec_dipole(1:Natoms)%Z=ZERO
      elec_dipole_tot%X=ZERO
      elec_dipole_tot%Y=ZERO
      elec_dipole_tot%Z=ZERO
      nuclear_dipole%X=ZERO
      nuclear_dipole%Y=ZERO
      nuclear_dipole%Z=ZERO

      return
      end subroutine INI_NI_Dipole
