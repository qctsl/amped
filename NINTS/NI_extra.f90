      subroutine NI_extra
!*********************************************************************************************
!     Date last modified: October 24, 2024                                                   *
!     Author: JWH                                                                            *
!     Desciption: Calculate the number of electron pairs by integrating the 3D extracule.    *
!*********************************************************************************************
! Modules:
      USE program_constants
      USE program_timing
      USE program_files
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE module_grid_points
      USE INT_objects

      implicit none
!
! Local scalars:
      integer :: Iatom,IApoint,IAIM,Znum,Ifound
      double precision :: Xpt,Ypt,Zpt
      double precision :: extaaatom,extHFaaatom
      double precision :: extaagridA, extHFaagridA
      double precision :: extbbatom,extHFbbatom
      double precision :: extbbgridA, extHFbbgridA
      double precision :: extabatom,extHFabatom
      double precision :: extabgridA, extHFabgridA
      double precision :: extbaatom,extHFbaatom
      double precision :: extbagridA, extHFbagridA
      double precision :: t0, t1, t2, time
!
! Local arrays:
      double precision, dimension(:), allocatable :: WeightsA
!
! Begin:
      call PRG_manager ('enter', 'NI_extra', 'EXTRA%NI')
!
      call GET_object ('QM', '2RDM', 'AO')
!
! Get grid and weights
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
      write(UNIout,'(/2a)')'Radial grid used for numerical integration: ',RADIAL_grid
      call BLD_BeckeW_XI
      write(UNIout,'(2a)')'Weights used for numerical integration:      BECKE'
!
      call CPU_time(t0)
      call CPU_time(t1)      
!
! same-spin
      allocate(extHFaa_atomic(Natoms),extaa_atomic(Natoms))
      allocate(extHFbb_atomic(Natoms),extbb_atomic(Natoms))
      extaa_total=ZERO
      extaa_atomic=ZERO
      extHFaa_total=ZERO
      extHFaa_atomic=ZERO
      extbb_total=ZERO
      extbb_atomic=ZERO
      extHFbb_total=ZERO
      extHFbb_atomic=ZERO
!
      write(UNIout,'(/a)')'    Number of electron pairs via extracule integration (same-spin) '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '                            Atomic contributions             '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '                   Hartree-Fock                 Correlated          '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '  Atom       alpha-alpha    beta-beta    alpha-alpha    beta-beta   '
      write(UNIout,'(a)') '--------------------------------------------------------------------'

      if(NAIMprint.eq.0)then ! If no atoms specified, do them all!
        NAIMprint=Natoms
        do Iatom=1,Natoms
          AIMprint(Iatom)=Iatom
        end do ! Iatom
      end if

      do IAIM=1,NAIMprint
!     do Iatom=1,Natoms
        Iatom=AIMprint(IAIM)
        Znum=CARTESIAN(Iatom)%atomic_number
        if(Znum.le.0)cycle
        Ifound=GRID_loc(Znum)
        NApts_atom=NApoints_atom(Ifound)

        allocate(grid_points(1:NApts_atom))
        allocate(WeightsA(NApts_atom))
!
! Moving grid points on cartesian coordinate grid
        do IApoint=1,NApts_atom
          grid_points(IApoint)%X=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          grid_points(IApoint)%Y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          grid_points(IApoint)%Z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
        end do ! IApoint

        call GET_weights (grid_points, NApts_atom, Iatom, WeightsA)

        extaaatom=ZERO
        extHFaaatom=ZERO
        extbbatom=ZERO
        extHFbbatom=ZERO
!

        do IApoint=1,NApts_atom
      !    if(WeightsA(IApoint).le.1.0D-6)cycle ! Makes no difference
          Xpt=grid_points(IApoint)%X
          Ypt=grid_points(IApoint)%Y
          Zpt=grid_points(IApoint)%Z
!
          call GET_extra_ss_point(Xpt, Ypt, Zpt, extaagridA, extHFaagridA, extbbgridA, extHFbbgridA)
!
          extaaatom = extaaatom + extaagridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
          extHFaaatom = extHFaaatom + extHFaagridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
          extbbatom = extbbatom + extbbgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
          extHFbbatom = extHFbbatom + extHFbbgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
        end do ! IApoint

        extaa_atomic(Iatom) = extaa_atomic(Iatom) + extaaatom
        extHFaa_atomic(Iatom) = extHFaa_atomic(Iatom) + extHFaaatom
!
        extbb_atomic(Iatom) = extbb_atomic(Iatom) + extbbatom
        extHFbb_atomic(Iatom) = extHFbb_atomic(Iatom) + extHFbbatom
!
        deallocate(grid_points)
        deallocate(WeightsA)

      end do ! Iatom

!
! Now multiply by 4pi and get total exchange
      do IAIM=1,NAIMprint
        Iatom=AIMprint(IAIM)
!     do Iatom=1,Natoms
      Znum=CARTESIAN(Iatom)%atomic_number
      if(Znum.le.0)cycle
!
      extaa_atomic(Iatom) = FourPi*extaa_atomic(Iatom)
      extHFaa_atomic(Iatom) = FourPi*extHFaa_atomic(Iatom)
!
      extbb_atomic(Iatom) = FourPi*extbb_atomic(Iatom)
      extHFbb_atomic(Iatom) = FourPi*extHFbb_atomic(Iatom)
!
      write(UNIout,'(i8,1x,4f14.8)')Iatom,extHFaa_atomic(Iatom),extHFbb_atomic(Iatom),&
                                          extaa_atomic(Iatom),extbb_atomic(Iatom)
!
      extaa_total = extaa_total + extaa_atomic(Iatom)
      extHFaa_total = extHFaa_total + extHFaa_atomic(Iatom)
!
      extbb_total = extbb_total + extbb_atomic(Iatom)
      extHFbb_total = extHFbb_total + extHFbb_atomic(Iatom)
!
      end do ! Iatom
!
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      if(NAIMprint.eq.Natoms)write(UNIout,'(a,1x,4f14.8)')'   Total',extHFaa_total,extHFbb_total,&
                                                                     extaa_total,extbb_total
!
      deallocate(extaa_atomic,extHFaa_atomic)
      deallocate(extbb_atomic,extHFbb_atomic)
!
! opposite-spin
      allocate(extHFab_atomic(Natoms),extab_atomic(Natoms))
      allocate(extHFba_atomic(Natoms),extba_atomic(Natoms))
      extab_total=ZERO
      extab_atomic=ZERO
      extHFab_total=ZERO
      extHFab_atomic=ZERO
      extba_total=ZERO
      extba_atomic=ZERO
      extHFba_total=ZERO
      extHFba_atomic=ZERO
!
      write(UNIout,'(/a)')' Number of electron pairs via extracule integration (opposite-spin) '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '                            Atomic contributions             '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '                   Hartree-Fock                 Correlated          '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '  Atom       alpha-beta    beta-alpha    alpha-beta    beta-alpha   '
      write(UNIout,'(a)') '--------------------------------------------------------------------'

      if(NAIMprint.eq.0)then ! If no atoms specified, do them all!
        NAIMprint=Natoms
        do Iatom=1,Natoms
          AIMprint(Iatom)=Iatom
        end do ! Iatom
      end if

      do IAIM=1,NAIMprint
!     do Iatom=1,Natoms
        Iatom=AIMprint(IAIM)
        Znum=CARTESIAN(Iatom)%atomic_number
        if(Znum.le.0)cycle
        Ifound=GRID_loc(Znum)
        NApts_atom=NApoints_atom(Ifound)

        allocate(grid_points(1:NApts_atom))
        allocate(WeightsA(NApts_atom))
!
! Moving grid points on cartesian coordinate grid
        do IApoint=1,NApts_atom
          grid_points(IApoint)%X=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          grid_points(IApoint)%Y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          grid_points(IApoint)%Z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
        end do ! IApoint

        call GET_weights (grid_points, NApts_atom, Iatom, WeightsA)

        extabatom=ZERO
        extHFabatom=ZERO
        extbaatom=ZERO
        extHFbaatom=ZERO
!

        do IApoint=1,NApts_atom
      !    if(WeightsA(IApoint).le.1.0D-6)cycle ! Makes no difference
          Xpt=grid_points(IApoint)%X
          Ypt=grid_points(IApoint)%Y
          Zpt=grid_points(IApoint)%Z
!
          call GET_extra_os_point(Xpt, Ypt, Zpt, extabgridA, extHFabgridA, extbagridA, extHFbagridA)
!
          extabatom = extabatom + extabgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
          extHFabatom = extHFabatom + extHFabgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
          extbaatom = extbaatom + extbagridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
          extHFbaatom = extHFbaatom + extHFbagridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
        end do ! IApoint

        extab_atomic(Iatom) = extab_atomic(Iatom) + extabatom
        extHFab_atomic(Iatom) = extHFab_atomic(Iatom) + extHFabatom
!
        extba_atomic(Iatom) = extba_atomic(Iatom) + extbaatom
        extHFba_atomic(Iatom) = extHFba_atomic(Iatom) + extHFbaatom
!
        deallocate(grid_points)
        deallocate(WeightsA)

      end do ! Iatom

!
! Now multiply by 4pi and get total exchange
      do IAIM=1,NAIMprint
        Iatom=AIMprint(IAIM)
!     do Iatom=1,Natoms
      Znum=CARTESIAN(Iatom)%atomic_number
      if(Znum.le.0)cycle
!
      extab_atomic(Iatom) = FourPi*extab_atomic(Iatom)
      extHFab_atomic(Iatom) = FourPi*extHFab_atomic(Iatom)
!
      extba_atomic(Iatom) = FourPi*extba_atomic(Iatom)
      extHFba_atomic(Iatom) = FourPi*extHFba_atomic(Iatom)
!
      write(UNIout,'(i8,1x,4f14.8)')Iatom,extHFab_atomic(Iatom),extHFba_atomic(Iatom),&
                                          extab_atomic(Iatom),extba_atomic(Iatom)
!
      extab_total = extab_total + extab_atomic(Iatom)
      extHFab_total = extHFab_total + extHFab_atomic(Iatom)
!
      extba_total = extba_total + extba_atomic(Iatom)
      extHFba_total = extHFba_total + extHFba_atomic(Iatom)
!
      end do ! Iatom
!
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      if(NAIMprint.eq.Natoms)write(UNIout,'(a,1x,4f14.8)')'   Total',extHFab_total,extHFba_total,&
                                                                     extab_total,extba_total
!
      deallocate(extab_atomic,extHFab_atomic)
      deallocate(extba_atomic,extHFba_atomic)
!
      call CPU_time(t2)
      time = t2 - t1
!
! End of routine NI_extra
      call PRG_manager ('exit', 'NI_extra', 'extra%NI')
      end subroutine NI_extra
