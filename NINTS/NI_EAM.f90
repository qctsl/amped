      subroutine NI_EAM
!*********************************************************************************************
!     Date last modified: April 24, 2019                                                     *
!     Author: JWH                                                                            *
!     Desciption: Calculate the equi- and antimomentum via integration of their densities.   *
!*********************************************************************************************
! Modules:
      USE program_constants
      USE program_timing
      USE program_files
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE module_grid_points

      implicit none
!
! Local scalars:
      integer :: Iatom,IApoint,IAIM,Znum,Ifound
      double precision :: Xpt,Ypt,Zpt
      double precision :: EMaaatom,EMHFaaatom
      double precision :: AMaaatom,AMHFaaatom
      double precision :: EMaagridA, EMHFaagridA
      double precision :: AMaagridA, AMHFaagridA
      double precision :: EMbbatom,EMHFbbatom
      double precision :: AMbbatom,AMHFbbatom
      double precision :: EMbbgridA, EMHFbbgridA
      double precision :: AMbbgridA, AMHFbbgridA
      double precision :: EMabatom,EMHFabatom
      double precision :: AMabatom,AMHFabatom
      double precision :: EMabgridA, EMHFabgridA
      double precision :: AMabgridA, AMHFabgridA
      double precision :: EMbaatom,EMHFbaatom
      double precision :: AMbaatom,AMHFbaatom
      double precision :: EMbagridA, EMHFbagridA
      double precision :: AMbagridA, AMHFbagridA
      double precision :: t0, t1, t2, time
!
! Local arrays:
      double precision, dimension(:), allocatable :: WeightsA
!
! Begin:
      call PRG_manager ('enter', 'NI_EAM', 'EAM%NI')
!
      call GET_object ('QM', '2RDM', 'AO')
!
! Get grid and weights
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
      write(UNIout,'(/2a)')'Radial grid used for numerical integration: ',RADIAL_grid
      call BLD_BeckeW_XI
      write(UNIout,'(2a)')'Weights used for numerical integration:      BECKE'
!
      call CPU_time(t0)
      call CPU_time(t1)      
!
! same-spin
      allocate(EMHFaa_atomic(Natoms),EMaa_atomic(Natoms))
      allocate(AMHFaa_atomic(Natoms),AMaa_atomic(Natoms))
      allocate(EMHFbb_atomic(Natoms),EMbb_atomic(Natoms))
      allocate(AMHFbb_atomic(Natoms),AMbb_atomic(Natoms))
      EMaa_total=ZERO
      EMaa_atomic=ZERO
      EMHFaa_total=ZERO
      EMHFaa_atomic=ZERO
      AMaa_total=ZERO
      AMaa_atomic=ZERO
      AMHFaa_total=ZERO
      AMHFaa_atomic=ZERO
      EMbb_total=ZERO
      EMbb_atomic=ZERO
      EMHFbb_total=ZERO
      EMHFbb_atomic=ZERO
      AMbb_total=ZERO
      AMbb_atomic=ZERO
      AMHFbb_total=ZERO
      AMHFbb_atomic=ZERO
!
      if(NAIMprint.eq.0)then ! If no atoms specified, do them all!
        NAIMprint=Natoms
        do Iatom=1,Natoms
          AIMprint(Iatom)=Iatom
        end do ! Iatom
      end if

      do IAIM=1,NAIMprint
!     do Iatom=1,Natoms
        Iatom=AIMprint(IAIM)
        Znum=CARTESIAN(Iatom)%atomic_number
        if(Znum.le.0)cycle
        Ifound=GRID_loc(Znum)
        NApts_atom=NApoints_atom(Ifound)

        allocate(grid_points(1:NApts_atom))
        allocate(WeightsA(NApts_atom))
!
! Moving grid points on cartesian coordinate grid
        do IApoint=1,NApts_atom
          grid_points(IApoint)%X=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          grid_points(IApoint)%Y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          grid_points(IApoint)%Z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
        end do ! IApoint

        call GET_weights (grid_points, NApts_atom, Iatom, WeightsA)

        EMaaatom=ZERO
        EMHFaaatom=ZERO
        AMaaatom=ZERO
        AMHFaaatom=ZERO
        EMbbatom=ZERO
        EMHFbbatom=ZERO
        AMbbatom=ZERO
        AMHFbbatom=ZERO
!
        do IApoint=1,NApts_atom
          !if(WeightsA(IApoint).le.1.0D-6)cycle ! Makes no difference
          Xpt=grid_points(IApoint)%X
          Ypt=grid_points(IApoint)%Y
          Zpt=grid_points(IApoint)%Z
!
          call GET_EAMDss_point (Xpt, Ypt, Zpt, EMaagridA, EMHFaagridA, AMaagridA, AMHFaagridA,&
                               EMbbgridA, EMHFbbgridA, AMbbgridA, AMHFbbgridA)
!
          EMaaatom = EMaaatom + EMaagridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
          EMHFaaatom = EMHFaaatom + EMHFaagridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
          EMbbatom = EMbbatom + EMbbgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
          EMHFbbatom = EMHFbbatom + EMHFbbgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
          AMaaatom = AMaaatom + AMaagridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
          AMHFaaatom = AMHFaaatom + AMHFaagridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
          AMbbatom = AMbbatom + AMbbgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
          AMHFbbatom = AMHFbbatom + AMHFbbgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
        end do ! IApoint

        EMaa_atomic(Iatom) = EMaa_atomic(Iatom) + EMaaatom
        EMHFaa_atomic(Iatom) = EMHFaa_atomic(Iatom) + EMHFaaatom
!
        EMbb_atomic(Iatom) = EMbb_atomic(Iatom) + EMbbatom
        EMHFbb_atomic(Iatom) = EMHFbb_atomic(Iatom) + EMHFbbatom
!
        AMaa_atomic(Iatom) = AMaa_atomic(Iatom) + AMaaatom
        AMHFaa_atomic(Iatom) = AMHFaa_atomic(Iatom) + AMHFaaatom
!
        AMbb_atomic(Iatom) = AMbb_atomic(Iatom) + AMbbatom
        AMHFbb_atomic(Iatom) = AMHFbb_atomic(Iatom) + AMHFbbatom
!
        deallocate(grid_points)
        deallocate(WeightsA)

      end do ! Iatom
!
      write(UNIout,'(/a)')'           Equimomentum via numerical integration (same-spin) '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '                            Atomic contributions             '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '                   Hartree-Fock                 Correlated          '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '  Atom       alpha-alpha    beta-beta    alpha-alpha    beta-beta   '
      write(UNIout,'(a)') '--------------------------------------------------------------------'

! Now multiply by 4pi and get total exchange
      do IAIM=1,NAIMprint
        Iatom=AIMprint(IAIM)
!     do Iatom=1,Natoms
      Znum=CARTESIAN(Iatom)%atomic_number
      if(Znum.le.0)cycle
!
      EMaa_atomic(Iatom) = FourPi*EMaa_atomic(Iatom)
      EMHFaa_atomic(Iatom) = FourPi*EMHFaa_atomic(Iatom)
!
      EMbb_atomic(Iatom) = FourPi*EMbb_atomic(Iatom)
      EMHFbb_atomic(Iatom) = FourPi*EMHFbb_atomic(Iatom)
!
      write(UNIout,'(i8,1x,4f14.8)')Iatom,EMHFaa_atomic(Iatom),EMHFbb_atomic(Iatom),&
      EMaa_atomic(Iatom),EMbb_atomic(Iatom)
!
      EMaa_total = EMaa_total + EMaa_atomic(Iatom)
      EMHFaa_total = EMHFaa_total + EMHFaa_atomic(Iatom)
!
      EMbb_total = EMbb_total + EMbb_atomic(Iatom)
      EMHFbb_total = EMHFbb_total + EMHFbb_atomic(Iatom)
!
      end do ! Iatom
!
      write(UNIout,'(a)') '------------------------------------------------------------------'
      if(NAIMprint.eq.Natoms)write(UNIout,'(a,1x,4f14.8)')'   Total',EMHFaa_total,EMHFbb_total,&
      EMaa_total,EMbb_total
!
      deallocate(EMaa_atomic,EMHFaa_atomic)
      deallocate(EMbb_atomic,EMHFbb_atomic)
!
      write(UNIout,'(/a)')'           Antimomentum via numerical integration (same-spin) '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '                            Atomic contributions             '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '                   Hartree-Fock                 Correlated          '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '  Atom       alpha-alpha    beta-beta    alpha-alpha    beta-beta   '
      write(UNIout,'(a)') '--------------------------------------------------------------------'

! Now multiply by 4pi and get total exchange
      do IAIM=1,NAIMprint
        Iatom=AIMprint(IAIM)
!     do Iatom=1,Natoms
      Znum=CARTESIAN(Iatom)%atomic_number
      if(Znum.le.0)cycle
!
      AMaa_atomic(Iatom) = FourPi*AMaa_atomic(Iatom)
      AMHFaa_atomic(Iatom) = FourPi*AMHFaa_atomic(Iatom)
!
      AMbb_atomic(Iatom) = FourPi*AMbb_atomic(Iatom)
      AMHFbb_atomic(Iatom) = FourPi*AMHFbb_atomic(Iatom)
!
      write(UNIout,'(i8,1x,4f14.8)')Iatom,AMHFaa_atomic(Iatom),AMHFbb_atomic(Iatom),&
      AMaa_atomic(Iatom),AMbb_atomic(Iatom)
!
      AMaa_total = AMaa_total + AMaa_atomic(Iatom)
      AMHFaa_total = AMHFaa_total + AMHFaa_atomic(Iatom)
!
      AMbb_total = AMbb_total + AMbb_atomic(Iatom)
      AMHFbb_total = AMHFbb_total + AMHFbb_atomic(Iatom)
!
      end do ! Iatom
!
      write(UNIout,'(a)') '------------------------------------------------------------------'
      if(NAIMprint.eq.Natoms)write(UNIout,'(a,1x,4f14.8)')'   Total',AMHFaa_total,AMHFbb_total,&
      AMaa_total,AMbb_total
!
      deallocate(AMaa_atomic,AMHFaa_atomic)
      deallocate(AMbb_atomic,AMHFbb_atomic)
!
! opposite-spin
      allocate(EMHFab_atomic(Natoms),EMab_atomic(Natoms))
      allocate(AMHFab_atomic(Natoms),AMab_atomic(Natoms))
      allocate(EMHFba_atomic(Natoms),EMba_atomic(Natoms))
      allocate(AMHFba_atomic(Natoms),AMba_atomic(Natoms))
      EMab_total=ZERO
      EMab_atomic=ZERO
      EMHFab_total=ZERO
      EMHFab_atomic=ZERO
      AMab_total=ZERO
      AMab_atomic=ZERO
      AMHFab_total=ZERO
      AMHFab_atomic=ZERO
      EMba_total=ZERO
      EMba_atomic=ZERO
      EMHFba_total=ZERO
      EMHFba_atomic=ZERO
      AMba_total=ZERO
      AMba_atomic=ZERO
      AMHFba_total=ZERO
      AMHFba_atomic=ZERO
!
      if(NAIMprint.eq.0)then ! If no atoms specified, do them all!
        NAIMprint=Natoms
        do Iatom=1,Natoms
          AIMprint(Iatom)=Iatom
        end do ! Iatom
      end if

      do IAIM=1,NAIMprint
!     do Iatom=1,Natoms
        Iatom=AIMprint(IAIM)
        Znum=CARTESIAN(Iatom)%atomic_number
        if(Znum.le.0)cycle
        Ifound=GRID_loc(Znum)
        NApts_atom=NApoints_atom(Ifound)

        allocate(grid_points(1:NApts_atom))
        allocate(WeightsA(NApts_atom))
!
! Moving grid points on cartesian coordinate grid
        do IApoint=1,NApts_atom
          grid_points(IApoint)%X=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          grid_points(IApoint)%Y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          grid_points(IApoint)%Z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
        end do ! IApoint

        call GET_weights (grid_points, NApts_atom, Iatom, WeightsA)

        EMabatom=ZERO
        EMHFabatom=ZERO
        AMabatom=ZERO
        AMHFabatom=ZERO
        EMbaatom=ZERO
        EMHFbaatom=ZERO
        AMbaatom=ZERO
        AMHFbaatom=ZERO
!
        do IApoint=1,NApts_atom
          !if(WeightsA(IApoint).le.1.0D-6)cycle ! Makes no difference
          Xpt=grid_points(IApoint)%X
          Ypt=grid_points(IApoint)%Y
          Zpt=grid_points(IApoint)%Z
!
          call GET_EAMDos_point (Xpt, Ypt, Zpt, EMabgridA, EMHFabgridA, AMabgridA, AMHFabgridA,&
                                 EMbagridA, EMHFbagridA, AMbagridA, AMHFbagridA)
!
          EMabatom = EMabatom + EMabgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
          EMHFabatom = EMHFabatom + EMHFabgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
          EMbaatom = EMbaatom + EMbagridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
          EMHFbaatom = EMHFbaatom + EMHFbagridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
          AMabatom = AMabatom + AMabgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
          AMHFabatom = AMHFabatom + AMHFabgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
          AMbaatom = AMbaatom + AMbagridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
          AMHFbaatom = AMHFbaatom + AMHFbagridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
        end do ! IApoint

        EMab_atomic(Iatom) = EMab_atomic(Iatom) + EMabatom
        EMHFab_atomic(Iatom) = EMHFab_atomic(Iatom) + EMHFabatom
!
        EMba_atomic(Iatom) = EMba_atomic(Iatom) + EMbaatom
        EMHFba_atomic(Iatom) = EMHFba_atomic(Iatom) + EMHFbaatom
!
        AMab_atomic(Iatom) = AMab_atomic(Iatom) + AMabatom
        AMHFab_atomic(Iatom) = AMHFab_atomic(Iatom) + AMHFabatom
!
        AMba_atomic(Iatom) = AMba_atomic(Iatom) + AMbaatom
        AMHFba_atomic(Iatom) = AMHFba_atomic(Iatom) + AMHFbaatom
!
        deallocate(grid_points)
        deallocate(WeightsA)

      end do ! Iatom
!
      write(UNIout,'(/a)')'          Equimomentum via numerical integration (opposite-spin) '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '                            Atomic contributions             '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '                   Hartree-Fock                 Correlated          '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '  Atom       alpha-beta    beta-alpha    alpha-beta    beta-alpha   '
      write(UNIout,'(a)') '--------------------------------------------------------------------'

! Now multiply by 4pi and get total exchange
      do IAIM=1,NAIMprint
        Iatom=AIMprint(IAIM)
!     do Iatom=1,Natoms
      Znum=CARTESIAN(Iatom)%atomic_number
      if(Znum.le.0)cycle
!
      EMab_atomic(Iatom) = FourPi*EMab_atomic(Iatom)
      EMHFab_atomic(Iatom) = FourPi*EMHFab_atomic(Iatom)
!
      EMba_atomic(Iatom) = FourPi*EMba_atomic(Iatom)
      EMHFba_atomic(Iatom) = FourPi*EMHFba_atomic(Iatom)
!
      write(UNIout,'(i8,1x,4f14.8)')Iatom,EMHFab_atomic(Iatom),EMHFba_atomic(Iatom),&
      EMab_atomic(Iatom),EMba_atomic(Iatom)
!
      EMab_total = EMab_total + EMab_atomic(Iatom)
      EMHFab_total = EMHFab_total + EMHFab_atomic(Iatom)
!
      EMba_total = EMba_total + EMba_atomic(Iatom)
      EMHFba_total = EMHFba_total + EMHFba_atomic(Iatom)
!
      end do ! Iatom
!
      write(UNIout,'(a)') '------------------------------------------------------------------'
      if(NAIMprint.eq.Natoms)write(UNIout,'(a,1x,4f14.8)')'   Total',EMHFab_total,EMHFba_total,&
      EMab_total,EMba_total
!
      deallocate(EMab_atomic,EMHFab_atomic)
      deallocate(EMba_atomic,EMHFba_atomic)
!
      write(UNIout,'(/a)')'          Antimomentum via numerical integration (opposite-spin) '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '                            Atomic contributions             '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '                   Hartree-Fock                 Correlated          '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '  Atom       alpha-beta    beta-alpha    alpha-beta    beta-alpha   '
      write(UNIout,'(a)') '--------------------------------------------------------------------'

! Now multiply by 4pi and get total exchange
      do IAIM=1,NAIMprint
        Iatom=AIMprint(IAIM)
!     do Iatom=1,Natoms
      Znum=CARTESIAN(Iatom)%atomic_number
      if(Znum.le.0)cycle
!
      AMab_atomic(Iatom) = FourPi*AMab_atomic(Iatom)
      AMHFab_atomic(Iatom) = FourPi*AMHFab_atomic(Iatom)
!
      AMba_atomic(Iatom) = FourPi*AMba_atomic(Iatom)
      AMHFba_atomic(Iatom) = FourPi*AMHFba_atomic(Iatom)
!
      write(UNIout,'(i8,1x,4f14.8)')Iatom,AMHFab_atomic(Iatom),AMHFba_atomic(Iatom),&
      AMab_atomic(Iatom),AMba_atomic(Iatom)
!
      AMab_total = AMab_total + AMab_atomic(Iatom)
      AMHFab_total = AMHFab_total + AMHFab_atomic(Iatom)
!
      AMba_total = AMba_total + AMba_atomic(Iatom)
      AMHFba_total = AMHFba_total + AMHFba_atomic(Iatom)
!
      end do ! Iatom
!
      write(UNIout,'(a)') '------------------------------------------------------------------'
      if(NAIMprint.eq.Natoms)write(UNIout,'(a,1x,4f14.8)')'   Total',AMHFab_total,AMHFba_total,&
      AMab_total,AMba_total
!
      deallocate(AMab_atomic,AMHFab_atomic)
      deallocate(AMba_atomic,AMHFba_atomic)
!
! sanity check
      call GET_object ('QM','EAM','ANA')
!
      call CPU_time(t2)
      time = t2 - t1
!
! End of routine NI_EAM
      call PRG_manager ('exit', 'NI_EAM', 'EAM%NI')
      end subroutine NI_EAM
