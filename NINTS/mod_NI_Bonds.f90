      module type_NI_Nelec_AB
!************************************************************************************
!     Date last modified December 13, 2013                                          *
!     Author: R.A. Poirier                                                          *
!     Description: Arrays and scalars for the number of electrons by atom/bond.     *
!************************************************************************************
! Modules:

      implicit none

! Output scalars:
      double precision :: N_elec_tot  ! Charge density

      double precision, dimension(:,:), allocatable :: N_elec_AB ! Number of electrons associated with AB bond
      double precision, dimension(:,:), allocatable :: N_elec_A_AB ! Number of electrons contributed by atom A to AB bond
      double precision, dimension(3) :: Bond_origin ! The X,Y,Z coordinates (origin) for the bond
      integer,  dimension(2) :: Bond_atoms  ! The atoms defining the bond

      end module type_NI_Nelec_AB
