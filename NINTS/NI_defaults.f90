      subroutine SET_defaults_NIM
!*******************************************************************************************************
!     Date last modified: July 22, 2014                                                   Version 2.0  *
!     Author: R.A. Poirier                                                                             *
!     Description: Set defaults for Numerical Integration Methods.                                     *
!*******************************************************************************************************
! Modules:
      USE NI_defaults

      implicit none
!
! Begin:
! Numerical Integration
      NI_Atomic=.false.  ! To print the atomic values of the property
      NI_property='NUMELECTRONS '
      NRPoints_Gill=20
      NAPoints_Gill(1)=6
      NAPoints_Gill(2)=86
      NAPoints_Gill(3)=194
      SSFA=0.65D0
      Sfactor=16.0D0
      LROT_inv=.false.  ! Grid points are not transformed (not rotationally invariant)
      LDM_QTAIM_type='DOUBLE '
!
! End of routine SET_defaults_NIM
      return
      end subroutine SET_defaults_NIM
