      subroutine NI_Tp_spin
!*********************************************************************************************
!     Date last modified: October 7, 2022                                                    *
!     Author: JWH                                                                            *
!     Desciption: Calculate the positive definite kinetic energy for alpha and beta          *
!                 electrons via numerical integration.                                       *
!*********************************************************************************************
! Modules:
      USE program_constants
      USE program_timing
      USE program_files
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE module_grid_points
      USE INT_objects

      implicit none
!
! Local scalars:
      integer :: Iatom,IApoint,IAIM,Znum,Ifound
      double precision :: Xpt,Ypt,Zpt
      double precision :: Tpaatom,TpHFaatom
      double precision :: TpagridA,TpHFagridA
      double precision :: Tpbatom,TpHFbatom
      double precision :: TpbgridA,TpHFbgridA
      double precision :: t0, t1, t2, time
!
! Local arrays:
      double precision, dimension(:), allocatable :: WeightsA
!
! Begin:
      call PRG_manager ('enter', 'NI_Tp_spin', 'TPOS%NI')
!
      call GET_object ('QM', 'SPIN_DENSITY_1MATRIX', 'AO')
!
! Get grid and weights
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
      write(UNIout,'(/2a)')'Radial grid used for numerical integration: ',RADIAL_grid
      call BLD_BeckeW_XI
      write(UNIout,'(2a)')'Weights used for numerical integration:      BECKE'
!
      call CPU_time(t0)
      call CPU_time(t1)      
!
! same-spin
      allocate(TpHFa_atomic(Natoms),Tpa_atomic(Natoms))
      allocate(TpHFb_atomic(Natoms),Tpb_atomic(Natoms))
      Tpa_total=ZERO
      Tpa_atomic=ZERO
      TpHFa_total=ZERO
      TpHFa_atomic=ZERO
      Tpb_total=ZERO
      Tpb_atomic=ZERO
      TpHFb_total=ZERO
      TpHFb_atomic=ZERO
!
      write(UNIout,'(/a)')'      Positive definite kinetic energy via numerical integration    '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '                            Atomic contributions             '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '                   Hartree-Fock                 Correlated          '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '  Atom           alpha         beta          alpha         beta     '
      write(UNIout,'(a)') '--------------------------------------------------------------------'

      if(NAIMprint.eq.0)then ! If no atoms specified, do them all!
        NAIMprint=Natoms
        do Iatom=1,Natoms
          AIMprint(Iatom)=Iatom
        end do ! Iatom
      end if

      if(allocated(grid_points))then
        deallocate(grid_points)
      end if 
!
      do IAIM=1,NAIMprint
!     do Iatom=1,Natoms
        Iatom=AIMprint(IAIM)
        Znum=CARTESIAN(Iatom)%atomic_number
        if(Znum.le.0)cycle
        Ifound=GRID_loc(Znum)
        NApts_atom=NApoints_atom(Ifound)

        allocate(grid_points(1:NApts_atom))
        allocate(WeightsA(NApts_atom))
!
! Moving grid points on cartesian coordinate grid
        do IApoint=1,NApts_atom
          grid_points(IApoint)%X=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          grid_points(IApoint)%Y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          grid_points(IApoint)%Z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
        end do ! IApoint

        call GET_weights (grid_points, NApts_atom, Iatom, WeightsA)

        Tpaatom=ZERO
        TpHFaatom=ZERO
        Tpbatom=ZERO
        TpHFbatom=ZERO
!

        do IApoint=1,NApts_atom
      !    if(WeightsA(IApoint).le.1.0D-6)cycle ! Makes no difference
          Xpt=grid_points(IApoint)%X
          Ypt=grid_points(IApoint)%Y
          Zpt=grid_points(IApoint)%Z
!
          call GET_Tp_spin_point (Xpt, Ypt, Zpt, TpagridA, TpHFagridA, TpbgridA, TpHFbgridA)
!
          Tpaatom = Tpaatom + TpagridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
          TpHFaatom = TpHFaatom + TpHFagridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
          Tpbatom = Tpbatom + TpbgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
          TpHFbatom = TpHFbatom + TpHFbgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
        end do ! IApoint

        Tpa_atomic(Iatom) = Tpa_atomic(Iatom) + Tpaatom
        TpHFa_atomic(Iatom) = TpHFa_atomic(Iatom) + TpHFaatom
!
        Tpb_atomic(Iatom) = Tpb_atomic(Iatom) + Tpbatom
        TpHFb_atomic(Iatom) = TpHFb_atomic(Iatom) + TpHFbatom
!
        deallocate(grid_points)
        deallocate(WeightsA)

      end do ! Iatom

!
! Now multiply by 4pi and get total exchange
      do IAIM=1,NAIMprint
        Iatom=AIMprint(IAIM)
!     do Iatom=1,Natoms
      Znum=CARTESIAN(Iatom)%atomic_number
      if(Znum.le.0)cycle
!
      Tpa_atomic(Iatom) = FourPi*Tpa_atomic(Iatom)
      TpHFa_atomic(Iatom) = FourPi*TpHFa_atomic(Iatom)
!
      Tpb_atomic(Iatom) = FourPi*Tpb_atomic(Iatom)
      TpHFb_atomic(Iatom) = FourPi*TpHFb_atomic(Iatom)
!
      write(UNIout,'(i8,1x,4f14.8)')Iatom,TpHFa_atomic(Iatom),TpHFb_atomic(Iatom),&
                                          Tpa_atomic(Iatom),Tpb_atomic(Iatom)
!
      Tpa_total = Tpa_total + Tpa_atomic(Iatom)
      TpHFa_total = TpHFa_total + TpHFa_atomic(Iatom)
!
      Tpb_total = Tpb_total + Tpb_atomic(Iatom)
      TpHFb_total = TpHFb_total + TpHFb_atomic(Iatom)
!
      end do ! Iatom
!
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      if(NAIMprint.eq.Natoms)write(UNIout,'(a,1x,4f14.8)')'   Total',TpHFa_total,TpHFb_total,&
                                                                     Tpa_total,Tpb_total
!
      deallocate(Tpa_atomic,TpHFa_atomic)
      deallocate(Tpb_atomic,TpHFb_atomic)
!
      call CPU_time(t2)
      time = t2 - t1
!
! End of routine NI_Tp_spin
      call PRG_manager ('exit', 'NI_Tp_spin', 'TPOS%NI')
      end subroutine NI_Tp_spin
      subroutine NI_Ts_spin
!*********************************************************************************************
!     Date last modified: October 7, 2022                                                    *
!     Author: JWH                                                                            *
!     Desciption: Calculate the positive definite kinetic energy for alpha and beta          *
!                 electrons via numerical integration.                                       *
!*********************************************************************************************
! Modules:
      USE program_constants
      USE program_timing
      USE program_files
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE module_grid_points
      USE INT_objects

      implicit none
!
! Local scalars:
      integer :: Iatom,IApoint,IAIM,Znum,Ifound
      double precision :: Xpt,Ypt,Zpt
      double precision :: Tsaatom,TsHFaatom
      double precision :: TsagridA,TsHFagridA
      double precision :: Tsbatom,TsHFbatom
      double precision :: TsbgridA,TsHFbgridA
      double precision :: t0, t1, t2, time
!
! Local arrays:
      double precision, dimension(:), allocatable :: WeightsA
!
! Begin:
      call PRG_manager ('enter', 'NI_Ts_spin', 'TSCH%NI')
!
      call GET_object ('QM', 'SPIN_DENSITY_1MATRIX', 'AO')
!
! Get grid and weights
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
      write(UNIout,'(/2a)')'Radial grid used for numerical integration: ',RADIAL_grid
      call BLD_BeckeW_XI
      write(UNIout,'(2a)')'Weights used for numerical integration:      BECKE'
!
      call CPU_time(t0)
      call CPU_time(t1)      
!
! same-spin
      allocate(TsHFa_atomic(Natoms),Tsa_atomic(Natoms))
      allocate(TsHFb_atomic(Natoms),Tsb_atomic(Natoms))
      Tsa_total=ZERO
      Tsa_atomic=ZERO
      TsHFa_total=ZERO
      TsHFa_atomic=ZERO
      Tsb_total=ZERO
      Tsb_atomic=ZERO
      TsHFb_total=ZERO
      TsHFb_atomic=ZERO
!
      write(UNIout,'(/a)')'         Schrodinger kinetic energy via numerical integration    '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '                            Atomic contributions             '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '                   Hartree-Fock                 Correlated          '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '  Atom           alpha         beta          alpha         beta     '
      write(UNIout,'(a)') '--------------------------------------------------------------------'

      if(NAIMprint.eq.0)then ! If no atoms specified, do them all!
        NAIMprint=Natoms
        do Iatom=1,Natoms
          AIMprint(Iatom)=Iatom
        end do ! Iatom
      end if

      do IAIM=1,NAIMprint
!     do Iatom=1,Natoms
        Iatom=AIMprint(IAIM)
        Znum=CARTESIAN(Iatom)%atomic_number
        if(Znum.le.0)cycle
        Ifound=GRID_loc(Znum)
        NApts_atom=NApoints_atom(Ifound)

        allocate(grid_points(1:NApts_atom))
        allocate(WeightsA(NApts_atom))
!
! Moving grid points on cartesian coordinate grid
        do IApoint=1,NApts_atom
          grid_points(IApoint)%X=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          grid_points(IApoint)%Y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          grid_points(IApoint)%Z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
        end do ! IApoint

        call GET_weights (grid_points, NApts_atom, Iatom, WeightsA)

        Tsaatom=ZERO
        TsHFaatom=ZERO
        Tsbatom=ZERO
        TsHFbatom=ZERO
!

        do IApoint=1,NApts_atom
      !    if(WeightsA(IApoint).le.1.0D-6)cycle ! Makes no difference
          Xpt=grid_points(IApoint)%X
          Ypt=grid_points(IApoint)%Y
          Zpt=grid_points(IApoint)%Z
!
          call GET_Ts_spin_point (Xpt, Ypt, Zpt, TsagridA, TsHFagridA, TsbgridA, TsHFbgridA)
!
          Tsaatom = Tsaatom + TsagridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
          TsHFaatom = TsHFaatom + TsHFagridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
          Tsbatom = Tsbatom + TsbgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
          TsHFbatom = TsHFbatom + TsHFbgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
        end do ! IApoint

        Tsa_atomic(Iatom) = Tsa_atomic(Iatom) + Tsaatom
        TsHFa_atomic(Iatom) = TsHFa_atomic(Iatom) + TsHFaatom
!
        Tsb_atomic(Iatom) = Tsb_atomic(Iatom) + Tsbatom
        TsHFb_atomic(Iatom) = TsHFb_atomic(Iatom) + TsHFbatom
!
        deallocate(grid_points)
        deallocate(WeightsA)

      end do ! Iatom

!
! Now multiply by 4pi and get total exchange
      do IAIM=1,NAIMprint
        Iatom=AIMprint(IAIM)
!     do Iatom=1,Natoms
      Znum=CARTESIAN(Iatom)%atomic_number
      if(Znum.le.0)cycle
!
      Tsa_atomic(Iatom) = FourPi*Tsa_atomic(Iatom)
      TsHFa_atomic(Iatom) = FourPi*TsHFa_atomic(Iatom)
!
      Tsb_atomic(Iatom) = FourPi*Tsb_atomic(Iatom)
      TsHFb_atomic(Iatom) = FourPi*TsHFb_atomic(Iatom)
!
      write(UNIout,'(i8,1x,4f14.8)')Iatom,TsHFa_atomic(Iatom),TsHFb_atomic(Iatom),&
                                          Tsa_atomic(Iatom),Tsb_atomic(Iatom)
!
      Tsa_total = Tsa_total + Tsa_atomic(Iatom)
      TsHFa_total = TsHFa_total + TsHFa_atomic(Iatom)
!
      Tsb_total = Tsb_total + Tsb_atomic(Iatom)
      TsHFb_total = TsHFb_total + TsHFb_atomic(Iatom)
!
      end do ! Iatom
!
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      if(NAIMprint.eq.Natoms)write(UNIout,'(a,1x,4f14.8)')'   Total',TsHFa_total,TsHFb_total,&
                                                                     Tsa_total,Tsb_total
!
      deallocate(Tsa_atomic,TsHFa_atomic)
      deallocate(Tsb_atomic,TsHFb_atomic)
!
      call CPU_time(t2)
      time = t2 - t1
!
! End of routine NI_Ts_spin
      call PRG_manager ('exit', 'NI_Ts_spin', 'TSCH%NI')
      end subroutine NI_Ts_spin
