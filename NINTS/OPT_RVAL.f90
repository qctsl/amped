      subroutine RVAL_TO_OPT
!***********************************************************************
!     Date last modified: January 27, 1998                Version 2.0  *
!     Author: R.A. Poirier                                             *
!     Description: Set Cartesian coordinate parameters and             *
!                  gradients for optimization.                         *
!***********************************************************************
! Modules:
      USE type_elements
      USE OPT_objects
      USE OPT_DOC
      USE MultiExp_parameters
      USE module_grid_points

      implicit none
!
! Local scalars:
      integer Ioptpr,IZnum
!
! Begin:
      call PRG_manager ('enter', 'RVAL_TO_OPT', 'UTILITY')
!
      call Numerical_Integration ! to define R_value, ...

! First determine Noptpr
      if(.not.allocated(PARSET))then
      Noptpr=0
      if(ZVALUE.eq.0)then ! Do all atoms
        do IZnum=1,Nelements
          if(LZfound(IZnum))then
            Noptpr=Noptpr+1
          end if
        end do
      else
        Noptpr=1
      end if
!
        allocate (PARSET(Noptpr))
        allocate (PAR_type(Noptpr))
!
      Ioptpr=0
      if(ZVALUE.eq.0)then ! Do all atoms
        do IZnum=1,Nelements
          if(LZfound(IZnum))then
            Ioptpr=Ioptpr+1
            PARset(Ioptpr)=R_value(IZnum)
            PAR_type(Ioptpr)=.true.
          end if
        end do
      else
        if(RVALUE.le.0.0d0)RVALUE=R_value(ZVALUE)
        R_value(ZVALUE)=RVALUE
        PARset(1)=RVALUE
        PAR_type(1)=.true.
      end if

      else ! take values from PARSET (MENU)
      Ioptpr=0
        do IZnum=1,Nelements
          if(LZfound(IZnum))then
            Ioptpr=Ioptpr+1
            R_value(IZnum)=PARset(Ioptpr)
          end if
        end do

      end if

      call Numerical_Integration ! Update function value
!
! End of routine RVAL_TO_OPT
      call PRG_manager ('exit', 'RVAL_TO_OPT', 'UTILITY')
      return
      end
      subroutine OPT_TO_RVAL
!***********************************************************************
!     Date last modified: January 28, 1998                 Version 2.0 *
!     Author: R. A. Poirier                                            *
!     Description: Update the Cartesian coordinates.                   *
!***********************************************************************
! Modules:
      USE type_elements
      USE OPT_objects
      USE OPT_DOC
      USE MultiExp_parameters
      USE module_grid_points

      implicit none
!
! Local scalars:
      integer Ioptpr,IZnum
!
! Begin:
      call PRG_manager ('enter', 'OPT_TO_RVAL', 'UTILITY')
!
      Ioptpr=0
      if(ZVALUE.eq.0)then ! Do all atoms
        do IZnum=1,Nelements
          if(LZfound(IZnum))then
            Ioptpr=Ioptpr+1
            R_value(IZnum)=PARset(Ioptpr)
          end if
        end do
      else
        R_value(ZVALUE)=PARset(1)
      end if
!
! End of routine OPT_TO_RVAL
      call PRG_manager ('exit', 'OPT_TO_RVAL', 'UTILITY')
      return
      end
      subroutine SET_RVAL_STEPS
!***********************************************************************
!     Date last modified: June ,1997                    Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description:                               *
!                          *
!***********************************************************************
! Modules:
      USE mod_type_hessian
      USE OPT_objects

      implicit none
!
! Local scalars:
      integer Ioptpr
!
! Begin:
      call PRG_manager ('enter', 'SET_RVAL_STEPS', 'UTILITY')
!
      do Ioptpr=1,Noptpr
        DSTEP(Ioptpr)=0.01D0
      end do
!
! End of routine SET_RVAL_STEPS
      call PRG_manager ('exit', 'SET_RVAL_STEPS', 'UTILITY')
      return
      end
