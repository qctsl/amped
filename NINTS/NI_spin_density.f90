      subroutine NI_spin_density
!*********************************************************************************************
!     Date last modified: April 24, 2019                                                     *
!     Author: JWH                                                                            *
!     Desciption: Calculate the number of alpha and beta electrons via integration           *
!                 of the spin density.                                                       *
!*********************************************************************************************
! Modules:
      USE program_constants
      USE program_timing
      USE program_files
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE module_grid_points
      USE INT_objects

      implicit none
!
! Local scalars:
      integer :: Iatom,IApoint,IAIM,Znum,Ifound
      double precision :: Xpt,Ypt,Zpt
      double precision :: rhoaatom,rhoHFaatom
      double precision :: rhoagridA,rhoHFagridA
      double precision :: rhobatom,rhoHFbatom
      double precision :: rhobgridA,rhoHFbgridA
      double precision :: t0, t1, t2, time
!
! Local arrays:
      double precision, dimension(:), allocatable :: WeightsA
!
! Begin:
      call PRG_manager ('enter', 'NI_spin_density', 'SPIN_DENSITY%NI')
!
      call GET_object ('QM', 'SPIN_DENSITY_1MATRIX', 'AO')
!
! Get grid and weights
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
      write(UNIout,'(/2a)')'Radial grid used for numerical integration: ',RADIAL_grid
      call BLD_BeckeW_XI
      write(UNIout,'(2a)')'Weights used for numerical integration:      BECKE'
!
      call CPU_time(t0)
      call CPU_time(t1)      
!
! same-spin
      allocate(rhoHFa_atomic(Natoms),rhoa_atomic(Natoms))
      allocate(rhoHFb_atomic(Natoms),rhob_atomic(Natoms))
      rhoa_total=ZERO
      rhoa_atomic=ZERO
      rhoHFa_total=ZERO
      rhoHFa_atomic=ZERO
      rhob_total=ZERO
      rhob_atomic=ZERO
      rhoHFb_total=ZERO
      rhoHFb_atomic=ZERO
!
      write(UNIout,'(/a)')'               Number of electrons via numerical integration       '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '                            Atomic contributions             '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '                   Hartree-Fock                 Correlated          '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '  Atom           alpha         beta          alpha         beta     '
      write(UNIout,'(a)') '--------------------------------------------------------------------'

      if(NAIMprint.eq.0)then ! If no atoms specified, do them all!
        NAIMprint=Natoms
        do Iatom=1,Natoms
          AIMprint(Iatom)=Iatom
        end do ! Iatom
      end if
!
      if(allocated(grid_points))then
        deallocate(grid_points)
      end if 
!
      do IAIM=1,NAIMprint
!     do Iatom=1,Natoms
        Iatom=AIMprint(IAIM)
        Znum=CARTESIAN(Iatom)%atomic_number
        if(Znum.le.0)cycle
        Ifound=GRID_loc(Znum)
        NApts_atom=NApoints_atom(Ifound)

        allocate(grid_points(1:NApts_atom))
        allocate(WeightsA(NApts_atom))
!
! Moving grid points on cartesian coordinate grid
        do IApoint=1,NApts_atom
          grid_points(IApoint)%X=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          grid_points(IApoint)%Y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          grid_points(IApoint)%Z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
        end do ! IApoint

        call GET_weights (grid_points, NApts_atom, Iatom, WeightsA)

        rhoaatom=ZERO
        rhoHFaatom=ZERO
        rhobatom=ZERO
        rhoHFbatom=ZERO
!

        do IApoint=1,NApts_atom
      !    if(WeightsA(IApoint).le.1.0D-6)cycle ! Makes no difference
          Xpt=grid_points(IApoint)%X
          Ypt=grid_points(IApoint)%Y
          Zpt=grid_points(IApoint)%Z
!
          call GET_spin_den_point (Xpt, Ypt, Zpt, rhoagridA, rhoHFagridA, rhobgridA, rhoHFbgridA)
!
          rhoaatom = rhoaatom + rhoagridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
          rhoHFaatom = rhoHFaatom + rhoHFagridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
          rhobatom = rhobatom + rhobgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
          rhoHFbatom = rhoHFbatom + rhoHFbgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
        end do ! IApoint

        rhoa_atomic(Iatom) = rhoa_atomic(Iatom) + rhoaatom
        rhoHFa_atomic(Iatom) = rhoHFa_atomic(Iatom) + rhoHFaatom
!
        rhob_atomic(Iatom) = rhob_atomic(Iatom) + rhobatom
        rhoHFb_atomic(Iatom) = rhoHFb_atomic(Iatom) + rhoHFbatom
!
        deallocate(grid_points)
        deallocate(WeightsA)

      end do ! Iatom

!
! Now multiply by 4pi and get total exchange
      do IAIM=1,NAIMprint
        Iatom=AIMprint(IAIM)
!     do Iatom=1,Natoms
      Znum=CARTESIAN(Iatom)%atomic_number
      if(Znum.le.0)cycle
!
      rhoa_atomic(Iatom) = FourPi*rhoa_atomic(Iatom)
      rhoHFa_atomic(Iatom) = FourPi*rhoHFa_atomic(Iatom)
!
      rhob_atomic(Iatom) = FourPi*rhob_atomic(Iatom)
      rhoHFb_atomic(Iatom) = FourPi*rhoHFb_atomic(Iatom)
!
      write(UNIout,'(i8,1x,4f14.8)')Iatom,rhoHFa_atomic(Iatom),rhoHFb_atomic(Iatom),&
                                          rhoa_atomic(Iatom),rhob_atomic(Iatom)
!
      rhoa_total = rhoa_total + rhoa_atomic(Iatom)
      rhoHFa_total = rhoHFa_total + rhoHFa_atomic(Iatom)
!
      rhob_total = rhob_total + rhob_atomic(Iatom)
      rhoHFb_total = rhoHFb_total + rhoHFb_atomic(Iatom)
!
      end do ! Iatom
!
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      if(NAIMprint.eq.Natoms)write(UNIout,'(a,1x,4f14.8)')'   Total',rhoHFa_total,rhoHFb_total,&
                                                                     rhoa_total,rhob_total
!
      deallocate(rhoa_atomic,rhoHFa_atomic)
      deallocate(rhob_atomic,rhoHFb_atomic)
!
      call CPU_time(t2)
      time = t2 - t1
!
! End of routine NI_spin_density
      call PRG_manager ('exit', 'NI_spin_density', 'SPIN_DENSITY%NI')
      end subroutine NI_spin_density
