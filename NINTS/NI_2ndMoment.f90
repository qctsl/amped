      subroutine electronic_2ndMoment (Iatom, Ifound)
!***********************************************************************
!     Date last modified December 2013                                 *
!     Author: R.A. Poirier                                             *
!     Description: Compute the second moment (size and shape) for AIM  *
!***********************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE N_integration
      USE NI_Dipole
      USE NI_Second_Moment

      implicit none

! Local scalars:
      integer :: IApoint
      integer :: Iatom,Ifound
      type(dipole_moment), dimension(:), allocatable :: point_dipole
      type(Moment_2nd), dimension(:), allocatable :: point_Moment

! Begin: 

      allocate(point_dipole(NApts_atom))
      allocate(point_Moment(NApts_atom))
!
! Dipole
      do IApoint=1,NApts_atom
        point_dipole(IApoint)%X=charge(IApoint)*(grid_points(IApoint)%X-CARTESIAN(Iatom)%X)* &
        Bweights(IApoint)*grid_points(IApoint)%w
        point_dipole(IApoint)%Y=charge(IApoint)*(grid_points(IApoint)%Y-CARTESIAN(Iatom)%Y)* &
        Bweights(IApoint)*grid_points(IApoint)%w
        point_dipole(IApoint)%Z=charge(IApoint)*(grid_points(IApoint)%Z-CARTESIAN(Iatom)%Z)* &
        Bweights(IApoint)*grid_points(IApoint)%w
      end do

      do IApoint=1,NApts_atom
        elec_dipole(Iatom)%X=elec_dipole(Iatom)%X+point_dipole(IApoint)%X
        elec_dipole(Iatom)%Y=elec_dipole(Iatom)%Y+point_dipole(IApoint)%Y
        elec_dipole(Iatom)%Z=elec_dipole(Iatom)%Z+point_dipole(IApoint)%Z
      end do !IApoint

      elec_dipole(Iatom)%X=-FourPi*elec_dipole(Iatom)%X
      elec_dipole(Iatom)%Y=-FourPi*elec_dipole(Iatom)%Y
      elec_dipole(Iatom)%Z=-FourPi*elec_dipole(Iatom)%Z

      deallocate(point_dipole)


      do IApoint=1,NApts_atom
        point_Moment(IApoint)%XX=charge(IApoint)* &
        (grid_points(IApoint)%X-CARTESIAN(Iatom)%X)*(grid_points(IApoint)%X-CARTESIAN(Iatom)%X)* &
!       grid_points(IApoint)%X*grid_points(IApoint)%X* &
        Bweights(IApoint)*grid_points(IApoint)%w
        point_Moment(IApoint)%YY=charge(IApoint)* &
        (grid_points(IApoint)%Y-CARTESIAN(Iatom)%Y)*(grid_points(IApoint)%Y-CARTESIAN(Iatom)%Y)* &
!       grid_points(IApoint)%Y*grid_points(IApoint)%Y* &
        Bweights(IApoint)*grid_points(IApoint)%w
        point_Moment(IApoint)%ZZ=charge(IApoint)* &
        (grid_points(IApoint)%Z-CARTESIAN(Iatom)%Z)*(grid_points(IApoint)%Z-CARTESIAN(Iatom)%Z)* &
!       grid_points(IApoint)%Z*grid_points(IApoint)%Z* &
        Bweights(IApoint)*grid_points(IApoint)%w
        point_Moment(IApoint)%XY=charge(IApoint)* &
        (grid_points(IApoint)%X-CARTESIAN(Iatom)%X)*(grid_points(IApoint)%Y-CARTESIAN(Iatom)%Y)* &
!       grid_points(IApoint)%X*grid_points(IApoint)%Y* &
        Bweights(IApoint)*grid_points(IApoint)%w
        point_Moment(IApoint)%XZ=charge(IApoint)* &
        (grid_points(IApoint)%X-CARTESIAN(Iatom)%X)*(grid_points(IApoint)%Z-CARTESIAN(Iatom)%Z)* &
!       grid_points(IApoint)%X*grid_points(IApoint)%Z* &
        Bweights(IApoint)*grid_points(IApoint)%w
        point_Moment(IApoint)%YZ=charge(IApoint)* &
        (grid_points(IApoint)%Y-CARTESIAN(Iatom)%Y)*(grid_points(IApoint)%Z-CARTESIAN(Iatom)%Z)* &
!       grid_points(IApoint)%Y*grid_points(IApoint)%Z* &
        Bweights(IApoint)*grid_points(IApoint)%w
      end do

      do IApoint=1,NApts_atom
        A2ndMom(Iatom)%XX=A2ndMom(Iatom)%XX+point_Moment(IApoint)%XX
        A2ndMom(Iatom)%YY=A2ndMom(Iatom)%YY+point_Moment(IApoint)%YY
        A2ndMom(Iatom)%ZZ=A2ndMom(Iatom)%ZZ+point_Moment(IApoint)%ZZ
        A2ndMom(Iatom)%XY=A2ndMom(Iatom)%XY+point_Moment(IApoint)%XY
        A2ndMom(Iatom)%XZ=A2ndMom(Iatom)%XZ+point_Moment(IApoint)%XZ
        A2ndMom(Iatom)%YZ=A2ndMom(Iatom)%YZ+point_Moment(IApoint)%YZ
      end do !IApoint

      A2ndMom(Iatom)%XX=-FourPi*A2ndMom(Iatom)%XX
      A2ndMom(Iatom)%YY=-FourPi*A2ndMom(Iatom)%YY
      A2ndMom(Iatom)%ZZ=-FourPi*A2ndMom(Iatom)%ZZ
      A2ndMom(Iatom)%XY=-FourPi*A2ndMom(Iatom)%XY
      A2ndMom(Iatom)%XZ=-FourPi*A2ndMom(Iatom)%XZ
      A2ndMom(Iatom)%YZ=-FourPi*A2ndMom(Iatom)%YZ

      deallocate(point_Moment)
      return
      end subroutine electronic_2ndMoment
      subroutine total_2ndMoment (Iatom, Ifound)
!***********************************************************************
!     Date last modified December 2013                                 *
!     Author: R.A. Poirier                                             *
!     Description: Compute the second moment (size and shape) for AIM  *
!                  Summing over atoms                                  *
!***********************************************************************
! Modules:
      USE N_integration
      USE NI_Second_Moment

      implicit none

! Local scalars:  
      integer :: Iatom,Ifound

! Begin:
       call electronic_2ndMoment (Iatom, Ifound)

      SecondXX=SecondXX+A2ndMom(Iatom)%XX
      SecondYY=SecondYY+A2ndMom(Iatom)%YY
      SecondZZ=SecondZZ+A2ndMom(Iatom)%ZZ
      SecondXY=SecondXY+A2ndMom(Iatom)%XY
      SecondXZ=SecondXZ+A2ndMom(Iatom)%XZ
      SecondYZ=SecondYZ+A2ndMom(Iatom)%YZ

      return
      end Subroutine total_2ndMoment
      subroutine PRT_NI_2ndMoment_AIM
!***********************************************************************
!     Date last modified December 2013                                 *
!     Author: R.A. Poirier                                             *
!     Description: Print the second moment (size and shape) for AIM    *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE NI_Nelectrons
      USE NI_Dipole
      USE NI_Second_Moment

      implicit none

! Local scalars:  
      integer :: Iatom

! Begin:
      TVolume=ZERO
      write(UNIout,'(a)')'Second Moment tensor (bohr^2) of AIM:'
      write(UNIout,'(a8,6a16)')'Atom','XX','YY','ZZ','XY','XZ','YZ'
      do Iatom=1,Natoms
      if(CARTESIAN(Iatom)%Atomic_number.le.0)cycle
!     write(UNIout,'(a,i8)')'For atom: ',Iatom
      write(UNIout,'(i8,6f16.6)')Iatom,A2ndMom(Iatom)%XX,A2ndMom(Iatom)%YY, &
                              A2ndMom(Iatom)%ZZ,A2ndMom(Iatom)%XY, &
                              A2ndMom(Iatom)%XZ,A2ndMom(Iatom)%YZ
      end do ! Iatom

      write(UNIout,'(a)')'Size/Shape (bohr) and Volume ((bohr^3) of AIM: '
      write(UNIout,'(a8,6a16)')'Atom','X','Y','Z','Volume'
      do Iatom=1,Natoms
      if(CARTESIAN(Iatom)%Atomic_number.le.0)cycle
!     write(UNIout,'(a,i8)')'For atom: ',Iatom
      Hess(1,1)=-A2ndMom(Iatom)%XX !-elec_dipole(Iatom)%x**2/num_electrons(Iatom)
      Hess(1,2)=-A2ndMom(Iatom)%XY !-elec_dipole(Iatom)%x*elec_dipole(Iatom)%y/num_electrons(Iatom)
      Hess(1,3)=-A2ndMom(Iatom)%XZ !-elec_dipole(Iatom)%x*elec_dipole(Iatom)%z/num_electrons(Iatom)
      Hess(2,1)=Hess(1,2)
      Hess(2,2)=-A2ndMom(Iatom)%YY !-elec_dipole(Iatom)%y**2/num_electrons(Iatom)
      Hess(2,3)=-A2ndMom(Iatom)%YZ !-elec_dipole(Iatom)%y*elec_dipole(Iatom)%z/num_electrons(Iatom)
      Hess(3,1)=Hess(1,3)
      Hess(3,2)=Hess(2,3)
      Hess(3,3)=-A2ndMom(Iatom)%ZZ !-elec_dipole(Iatom)%z**2/num_electrons(Iatom)

      call MATRIX_diagonalize (Hess, EigVector, EigValue, 3, 2, .false.)
      SizeX=dsqrt(dabs(EigValue(1)))
      SizeY=dsqrt(dabs(EigValue(2)))
      SizeZ=dsqrt(dabs(EigValue(3)))
      AVolume=(FOUR*PI_val/THREE)*(SizeX*SizeY*SizeZ)
      write(UNIout,'(i8,4f16.6)')Iatom,SizeX,SizeY,SizeZ,AVolume
!     write(UNIout,'(a,f16.6)')'Volume(bohr^3): ',AVolume
      TVolume=TVolume+AVolume
      end do ! Iatom
      write(UNIout,'(a,f16.6)')'Total Volume(bohr^3): ',TVolume

      call PRT_NI_dipole_AIM

      deallocate(A2ndMom)

      return
      end Subroutine PRT_NI_2ndMoment_AIM
      subroutine INI_NI_2ndMoment
!************************************************************************************
!     Date last modified December 10, 2013                                          *
!     Author: R.A. Poirier                                                          *
!     Description: Initialization for the second moment                             *
!************************************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE NI_defaults
      USE NI_Second_Moment

      implicit none

! Input scalars:
      NI_Atomic=.true.
      allocate(A2ndMom(Natoms))
      A2ndMom(1:Natoms)%XX=ZERO
      A2ndMom(1:Natoms)%YY=ZERO
      A2ndMom(1:Natoms)%ZZ=ZERO
      A2ndMom(1:Natoms)%XY=ZERO
      A2ndMom(1:Natoms)%XZ=ZERO
      A2ndMom(1:Natoms)%YZ=ZERO
      SecondXX=ZERO
      SecondYY=ZERO
      SecondZZ=ZERO
      SecondXY=ZERO
      SecondXZ=ZERO
      SecondYZ=ZERO

      return
      end subroutine INI_NI_2ndMoment
