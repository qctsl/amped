      module NI_defaults
!*****************************************************************************************************************
!     Date last modified: October 30, 2008                                                          Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Defaults associated with the numerical integration code                                       *
!*****************************************************************************************************************
      implicit none

      integer :: NRPoints_Gill
      integer :: NAPoints_Gill(3)

      logical :: NI_Atomic     !  if true, print the atomic values of the property        
      logical :: LROT_inv      !  if true, transforms grid points (rotationally invariant)

      double precision :: SSFa ! value of parameter a for SSF partitioning scheme
      double precision :: Sfactor ! The exponent used in the step function S(nu_ij) 1/(exp(a*nu_ij)+1)
      double precision :: RUpperLimit, RLowerLimit ! Upper and lower radii limits to integrate and determine # electrons in a shell 
      double precision :: WCutoff
      parameter (WCutoff=1.0D-05)


      character(len=16) :: NI_property  ! number of electrons, potential energy, dipole, coulomb energy
      character (len=8) :: LDM_QTAIM_type ! SINGLE or DOUBLE integral
      character (len=16) :: LDM_type ! ABIM or QTAIM

      end module NI_defaults
