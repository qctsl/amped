      module NI_Dipole
!**************************************************************************************
!     Date last modified: Sept 26th ,2001                                             *
!     Author: Aisha                                                                   *
!     Desciption:                                                                     *
!**************************************************************************************
! Modules:
      USE QM_objects

      implicit none

! Output scalars:
      type(dipole_moment) :: elec_dipole_tot ! Electronic component of the dipole moment
      type(dipole_moment) :: nuclear_dipole  ! Nuclear component of the dipole moment
      type(dipole_moment) :: net_dipole

      type(dipole_moment),dimension(:),allocatable :: elec_dipole ! electronic dipole for all AIM
      type(dipole_moment) :: error_dipole    !  Error in dipole moment
      type(dipole_moment) :: accuracy_dipole    !  Accuracy in dipole moment


      end module NI_Dipole
