      module NI_Second_Moment
!***********************************************************************************
!     Date last modified December 2013                                             *
!     Author: R.A. Poirier                                                         *
!     Description: Arraysi/scalars for the second moment (size and shape) for AIM  *
!***********************************************************************************
! Modules:

      implicit none

      type Moment_2nd
        double precision :: XX
        double precision :: YY
        double precision :: ZZ
        double precision :: XY
        double precision :: XZ
        double precision :: YZ
        double precision :: total
      end type Moment_2nd

!
! Atomic second moment (AIM)
      type(Moment_2nd), dimension(:), allocatable :: A2ndMom  ! electronic dipole for all atoms in the molecule
!
! Total second moment
      double precision :: SecondXX,SecondYY,SecondZZ,SecondXY,SecondXZ,SecondYZ
      double precision :: SizeX,SizeY,SizeZ,AVolume,TVolume

      double precision, dimension(3,3) :: Hess,EigVector
      double precision, dimension(3) :: EigValue

      end module NI_Second_Moment
