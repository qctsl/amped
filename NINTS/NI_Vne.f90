      subroutine potential_energy (Iatom, Ifound)
!************************************************************************************
!     Date last modified Sept 26th ,2001                                            *
!     Author: Aisha                                                                 *
!     Description:                                                                  *
!************************************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE N_integration
      USE NI_potential

      implicit none

! Input scalars:
      integer :: Iatom,Ifound
!
! Local scalars:
      integer :: IApoint,JZ_num
      integer :: Jatom
      double precision :: temp
      double precision, dimension(:), allocatable :: Pcharge

! Begin: 
      allocate (Pcharge(NApts_atom))

      Pcharge(1:NApts_atom) = ZERO
      do IApoint = 1,NApts_atom
        do Jatom = 1,Natoms
          JZ_num = CARTESIAN(Jatom)%Atomic_number
          temp = JZ_num*charge(IApoint)/    &
          dsqrt((grid_points(IApoint)%X-CARTESIAN(Jatom)%X)**2 + &
                (grid_points(IApoint)%Y-CARTESIAN(Jatom)%Y)**2 + &
                (grid_points(IApoint)%Z-CARTESIAN(Jatom)%Z)**2)
          Pcharge(IApoint) = Pcharge(IApoint) + temp
        end do ! Jatom
      end do ! IApoint

      do IApoint = 1,NApts_atom
        Pcharge(IApoint) = Pcharge(IApoint)*Bweights(IApoint)*grid_points(IApoint)%w
      end do

      call NI_Atomic_Property (Iatom, Ifound, pot_energy, Natoms, Pcharge)

      pot_energy_tot = pot_energy_tot + pot_energy(Iatom)

      deallocate(Pcharge)

      return
      end subroutine potential_energy      
      subroutine PRT_NI_potential
!***********************************************************************
!     Date last modified December 2013                                 *
!     Author: R.A. Poirier                                             *
!     Description: Print the potential energy results                  *
!***********************************************************************
! Modules:
      USE program_files
      USE QM_objects
      USE NI_potential

      implicit none

! Local scalars:  

! Begin:
      pot_energy_tot = -pot_energy_tot
      error_pot = pot_energy_tot - E_V_RHF
      error_pot = error_pot *1D6
      accuracy_pot = - DLOG10(DABS((pot_energy_tot/E_V_RHF)-1))
      write(UNIout,'(a,f14.6,f16.6,f14.3,1PE21.3,a)')'Potential  ',pot_energy_tot,E_V_RHF,accuracy_pot,error_pot,' mhartrees'

      return
      end subroutine PRT_NI_potential
      subroutine PRT_NI_potential_AIM
!***********************************************************************
!     Date last modified December 2013                                 *
!     Author: R.A. Poirier                                             *
!     Description: Print the potential energy results                  *
!***********************************************************************
! Modules:
      USE program_files
      USE type_molecule
      USE NI_potential

      implicit none

! Local scalars:  
      integer :: Iatom

! Begin:
      write(UNIout,'(a)')'Potential energy by atom:'
      do Iatom=1,Natoms
        write(UNIout,'(i8,f14.8)')Iatom,pot_energy(Iatom)
      end do

      deallocate(pot_energy)

      return
      end subroutine PRT_NI_potential_AIM
      subroutine INI_NI_Vne
!************************************************************************************
!     Date last modified December 10, 2013                                          *
!     Author: R.A. Poirier                                                          *
!     Description: Initialization for the potential energy                          *
!************************************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE NI_potential

      implicit none

! Input scalars:
      allocate(pot_energy(Natoms))
      pot_energy_tot=ZERO
      pot_energy(1:Natoms)=ZERO

      return
      end subroutine INI_NI_Vne
