      subroutine number_electrons (Iatom, Ifound)
!************************************************************************************
!     Date last modified Sept 26th ,2001                                            *
!     Author: Aisha                                                                 *
!     Description:                                                                  *
!************************************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE N_integration
      USE NI_Nelectrons

      implicit none

! Input scalars:
      integer :: Iatom,Ifound
!
! Local scalars:
      integer :: IApoint
      double precision, dimension(:), allocatable :: Pcharge

! Begin: 
      allocate (Pcharge(NApts_atom))
      do IApoint = 1,NApts_atom
        Pcharge(IApoint) = charge(IApoint)*Bweights(IApoint)*grid_points(IApoint)%w
      end do

      call NI_Atomic_Property (Iatom, Ifound, num_electrons, Natoms, Pcharge)
 
      num_electrons_tot = num_electrons_tot + num_electrons(Iatom)

      deallocate(Pcharge)
      
      return
      end subroutine number_electrons
      subroutine PRT_NI_Nelectrons
!***********************************************************************
!     Date last modified December 2013                                 *
!     Author: R.A. Poirier                                             *
!     Description: Print the number of electrons                       *
!***********************************************************************
! Modules:
      USE program_files
      USE type_molecule
      USE NI_Nelectrons

      implicit none

! Local scalars:  
      double precision :: Z_tot
      double precision :: accuracy_ele  ! Accuracy in num _ele
!
! Begin:
      Z_tot=dble(Nelectrons)
      error_ele = num_electrons_tot - Z_tot
      accuracy_ele = - DLOG10(DABS((num_electrons_tot/Z_tot)-1))
      write(UNIout,'(a,f14.8,f10.1,f20.3,1PE20.2)')'N electrons',num_electrons_tot,Z_tot,accuracy_ele,error_ele

      return
      end subroutine PRT_NI_Nelectrons
      subroutine PRT_NI_Nelectrons_AIM
!***********************************************************************
!     Date last modified December 2013                                 *
!     Author: R.A. Poirier                                             *
!     Description: Print the number of electrons                       *
!***********************************************************************
! Modules:
      USE program_files
      USE type_molecule
      USE NI_Nelectrons

      implicit none

! Local scalars:  
      integer :: Iatom
!
! Begin:
      write(UNIout,'(a)')'Number of electrons by atom:'
      do Iatom=1,Natoms
        write(UNIout,'(i8,f14.8)')Iatom,num_electrons(Iatom)
      end do
!
! Now deallocate:
      deallocate(num_electrons)

      return
      end subroutine PRT_NI_Nelectrons_AIM
      subroutine INI_NI_Nelectrons
!************************************************************************************
!     Date last modified December 10, 2013                                          *
!     Author: R.A. Poirier                                                          *
!     Description: Intialization for the number of electrons                        *
!************************************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE NI_Nelectrons

      implicit none

! Input scalars:

      if(allocated(num_electrons))deallocate(num_electrons)
      allocate(num_electrons(Natoms))
      num_electrons_tot=ZERO
      num_electrons(1:Natoms)=ZERO

      return
      end subroutine INI_NI_Nelectrons

