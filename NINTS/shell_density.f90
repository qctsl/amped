      subroutine Numerical_Integration_shell
!**************************************************************************************
!     Date last modified: February 8th ,2013                                          *
!     Author: Jessica Besaw                                                           *
!     Desciption: Calculate the electron density in a shell volume between            *
!		 		  RLowerLimit and RUpperLimit and                                     *
!**************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE QM_defaults
      USE QM_objects
      USE NI_defaults
      USE N_integration
      USE type_molecule
      USE type_Weights

      implicit none

! Local scalars:
      integer :: Iatom,IApoint,Ifound,NApoints_tot,Z_num,Npts_kept
      double precision :: radius,NElec_shell
      double precision, dimension(:), allocatable :: Pcharge
!
! Begin:
      call GET_object ('QM', 'CMO', Wavefunction)
      call GET_object ('GRID', 'RADIAL', 'BIG')

      write(UNIout,'(2a)')'Radial grid used for numerical integration: ',RADIAL_grid
!     write(UNIout,'(2a)')'Weights used for numerical integration:     ',DEN_partitioning
!     if(DEN_partitioning(1:5).eq.'BECKE')call BLD_BeckeW_XI

      if(Natoms.gt.1)then
        stop 'Numerical_Integration_shell only available for single atoms!!'
      end if
      do Iatom=1,Natoms
        Z_num=CARTESIAN(Iatom)%Atomic_number
        if(Z_num.le.0)cycle
        Ifound=GRID_loc(Z_num)
        NApts_atom=NApoints_atom(Ifound)
        NApoints_tot=NApoints_tot+NApts_atom
        allocate(grid_points(1:NApts_atom))
        Npts_kept=0

! Integrate between RLowerLimit And RUpperLimit as defined in Input
        write(UNIout,'(a,f20.8)')'RLowerLimit: ',RLowerLimit
        write(UNIout,'(a,f20.8)')'RUpperLimit: ',RUpperLimit

        do IApoint=1,NApts_atom
          radius=dsqrt((Egridpts(Ifound,IApoint)%X)**2 &
                 & +(Egridpts(Ifound,IApoint)%Y)**2 &
                 & +(Egridpts(Ifound,IApoint)%Z)**2)
          if(radius.lt.RLowerLimit)cycle 
          if(radius.ge.RUpperLimit)cycle
          Npts_kept=Npts_kept+1
          grid_points(Npts_kept)%X=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          grid_points(Npts_kept)%Y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          grid_points(Npts_kept)%Z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
          grid_points(Npts_kept)%w=Egridpts(Ifound,IApoint)%w
        end do ! IApoint

        write(UNIout,'(a,i8)')'Npts_kept: ',Npts_kept
        write(UNIout,'(a,i8)')'NApts_atom: ',NApts_atom

        allocate(charge(Npts_kept))
        call GET_density (grid_points, Npts_kept, charge)

! Now compute the Becke weights for all the grid points for Iatom
!       allocate(Bweights(Npts_kept))
!       if (Natoms.EQ.1) then	
!         Bweights=1.0D0
!       else
!         call Beckew (grid_points, Npts_kept, Iatom, Bweights)   
!       end if 

        allocate (Pcharge(Npts_kept))
        NElec_shell=0.0D0
        do IApoint = 1,Npts_kept
          Pcharge(IApoint)=charge(IApoint)*grid_points(IApoint)%w
!         Pcharge(IApoint)=charge(IApoint)*Bweights(IApoint)*grid_points(IApoint)%w
          NElec_shell=NElec_shell+Pcharge(IApoint)
        end do ! IApoint

        NElec_shell=FourPi*NElec_shell
 
        write(UNIout,'(a,i8,a,f20.8)') &
              'The number of electrons for atom ', Iatom, ' is ', NElec_shell 
   
!       deallocate(Bweights)
        deallocate(charge)
        deallocate(grid_points)

      end do ! Iatom

      end subroutine Numerical_Integration_shell
