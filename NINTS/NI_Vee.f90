      subroutine coulomb_energy (Iatom,Ifound)
!************************************************************************************
!     Date last modified April 25th ,2005                                            *
!     Author: Aisha                                                                 *
!     Description: Double numerical                                                 *
!************************************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE N_integration
      USE NI_defaults
      USE NI_Coulomb
      USE type_Weights

      implicit none

! Local scalars:
      integer :: IApoint,JApoint,RIend,AIend,Iregion,RIbegn,KRpoint,IZ_num
      integer :: AIbegn,Nrp_region,Nap_sphere,Iatom,Ifound
      integer :: Jatom,Jfound,JZ_num,JNApts_atom,RJend,AJend,Jregion,RJbegn,AJbegn
      double precision :: rij,temp,sum_coul
      double precision, dimension(:), allocatable :: IPoint_coulomb,JPoint_coulomb
      double precision, dimension(:), allocatable :: Jcharge,JBweights
      type(type_grid_points),dimension(:),allocatable :: J_grid_points

! Begin: 
      allocate(IPoint_coulomb(NApts_atom))

      do IApoint = 1,NApts_atom
        IPoint_coulomb(IApoint) = charge(IApoint)*Bweights(IApoint)*grid_points(IApoint)%w
      end do

      do Jatom=Iatom,Natoms
        JZ_num = CARTESIAN(Jatom)%Atomic_number
        if(JZ_num.le.0)cycle
        Jfound=GRID_loc(JZ_num)
          if(Iatom.eq.Jatom)then
            JNApts_atom = NApts_atom
            allocate(J_grid_points(JNApts_atom),JPoint_coulomb(JNApts_atom))
            allocate(Jcharge(JNApts_atom),JBweights(JNApts_atom))
            J_grid_points = grid_points
            JPoint_coulomb = IPoint_coulomb
          else
            JNApts_atom=NApoints_atom(Jfound)
            allocate(J_grid_points(1:JNApts_atom))
            do JApoint=1,JNApts_atom
              J_grid_points(JApoint)%X=Egridpts(Jfound,JApoint)%X+CARTESIAN(Jatom)%X
              J_grid_points(JApoint)%Y=Egridpts(Jfound,JApoint)%Y+CARTESIAN(Jatom)%Y
              J_grid_points(JApoint)%Z=Egridpts(Jfound,JApoint)%Z+CARTESIAN(Jatom)%Z
              J_grid_points(JApoint)%w=Egridpts(Jfound,JApoint)%w
            end do
            allocate(Jcharge(JNApts_atom),JPoint_coulomb(JNApts_atom),JBweights(JNApts_atom))
            call GET_density (J_grid_points, JNApts_atom, Jcharge)
            call GET_weights (J_grid_points, JNApts_atom, Jatom, JBweights)  

            do JApoint = 1,JNApts_atom
              JPoint_coulomb(JApoint) = Jcharge(JApoint)*JBweights(JApoint)*J_grid_points(JApoint)%w
            end do
          end if ! Iatom.eq.Jatom
 
          do IApoint =1, NApts_atom
            do JApoint = 1, JNApts_atom
              if(IApoint.eq.JApoint .and. Iatom.eq.Jatom) cycle
              rij = DSQRT((grid_points(IApoint)%X - J_grid_points(JApoint)%X)**2+ &
                          (grid_points(IApoint)%Y - J_grid_points(JApoint)%Y)**2+ &
                          (grid_points(IApoint)%Z - J_grid_points(JApoint)%Z)**2)
              temp = IPoint_coulomb(IApoint)*JPoint_coulomb(JApoint)/rij
              coul_energy(Iatom)=coul_energy(Iatom)+PT5*temp
              if(Iatom.ne.Jatom)then
              coul_energy(Jatom)=coul_energy(Jatom)+PT5*temp
              end if
            end do !JApoint
          end do !IApoint
          deallocate(J_grid_points)
          deallocate(Jcharge,JPoint_coulomb,JBweights)
      end do ! Jatom

      coul_energy(Iatom)=FourPi*FourPi*coul_energy(Iatom)
      coul_energy_tot = coul_energy_tot+coul_energy(Iatom)

      deallocate(IPoint_coulomb)

      return
      end subroutine coulomb_energy
      subroutine coulomb_energy_DFT (Iatom, Ifound)
!************************************************************************************
!     Date last modified April 25th ,2005                                            *
!     Author: Aisha                                                                 *
!     Description:                                                                  *
!************************************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE N_integration
      USE NI_defaults
      USE NI_Coulomb
      USE QM_objects

      implicit none

      integer :: Iatom,Ifound
! Local scalars:
      integer :: IApoint
      double precision, dimension(:), allocatable :: Vpotl
      double precision, dimension(:), allocatable :: Pcharge

! Begin: 
      allocate(Vpotl(NApts_atom))
      allocate (Pcharge(NApts_atom))

      call I1E_potl (Vpotl, PM0, MATlen, NApts_atom)

      do IApoint = 1,NApts_atom
        Pcharge(IApoint) = Vpotl(IApoint)*charge(IApoint)*Bweights(IApoint)*grid_points(IApoint)%w
      end do

      call NI_Atomic_Property (Iatom, Ifound, coul_energy_DFT, Natoms, Pcharge)

      coul_energy_DFT(Iatom)=PT5*coul_energy_DFT(Iatom)
      coul_energy_DFT_tot=coul_energy_DFT_tot+coul_energy_DFT(Iatom)

      deallocate(Pcharge)
      return
      end subroutine coulomb_energy_DFT
      subroutine I1E_potl (Vpotl, PM0, MATlen, NgridPts)
!***********************************************************************
!     Date last modified: February 9, 2006                 Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!***********************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE type_basis_set
      USE INT_objects
      USE module_grid_points

      implicit none

      double precision, dimension(MATlen) :: PM0
!
! Local scalars:
      integer :: MATlen
      integer :: NgridPts
!
! Local scalars:
      integer :: Ishell,Jshell,Ifrst,Jfrst,Ilast,Jlast
      integer :: Istart,Jstart,Iend,Jend
      integer :: Iatmshl,Jatmshl
      integer :: LAMAX,LBMAX
      integer :: Iatom,Jatom
      integer :: Irange,Jrange
      integer :: Igauss,Jgauss
      integer :: Iaos,Jaos
      integer :: IGBGN,JGBGN,IGend,JGend
      integer :: LPMAX,I,IA,IJX,IJY,IJZ,INTC,IX,IXP,IY,IYP,IZ,IZERO,IZP,J,JX,JY,JZ,LIM1DS,NZERO
      integer :: LENTQ
      integer :: IApoint
      double precision :: XA,XB,XC,YA,YB,YC,ZA,ZB,ZC,RABSQ
      double precision, dimension (NApts_atom) :: Vpotl
      double precision ::TRACLO

      double precision ABX,ABY,ABZ,ARABSQ,ARG,AS,ASXA,ASYA,ASZA,BS,COEF,CUT1,EP,EPI,EPIO2,PCX,PCY,PCZ,PEXP, &
                       PX,PY,PZ,RPCSQ,TWOASQ,TWOP,TWOPI,TWOPT2,XAP,XBP,XIIM1, &
                       XK,YAP,YBP,YIIM1,ZAP,ZBP,ZCONST,ZIIM1,ZT,ZTEMP
      integer :: INDIX(20),INDIY(20),INDIZ(20),INDJX(20),INDJY(20),INDJZ(20)
      double precision :: TP(7),WP(7)
      double precision A(45),CA(20),CB(20),CCX(192),CCY(192),CCZ(192),EEPB(100),EEPV(100), &
                       SX(32),SY(32),SZ(32), &
                       S1C(6),TWOCX(9),TWOCY(9),TWOCZ(9),XIP(80),YIP(80),ZIP(80)

      parameter (TWOPI=TWO*PI_VAL,CUT1=-75.0D0)
      DATA INDJX/1,2,1,1,3,1,1,2,2,1,4,1,1,2,3,3,2,1,1,2/,INDJY/1,1,2,1,1,3,1,2,1,2,1,4,1,3,2,1,1,2,3,2/, &
           INDJZ/1,1,1,2,1,1,3,1,2,2,1,1,4,1,1,2,3,3,2,2/
! Begin:
      call PRG_manager ('enter', 'I1E_potl', 'UTILITY')
!
      MATlen=Basis%nbasis*(Basis%nbasis+1)/2
!
      allocate (V12dr2(MAtlen))

      call RYSSET
!
! Fill INDIX.
      do I=1,20
        INDIX(I)=4*(INDJX(I)-1)
        INDIY(I)=4*(INDJY(I)-1)
        INDIZ(I)=4*(INDJZ(I)-1)
      end do ! I
!
! Loop over shells.
      Vpotl(1:NgridPts) = 0.0D0
      do IApoint = 1,NgridPts
! Loop over Ishell.
      do Ishell=1,Basis%Nshells
      LAMAX=Basis%shell(Ishell)%Xtype+1
      Istart=Basis%shell(Ishell)%Xstart
      Iend=Basis%shell(Ishell)%XEND
      Irange=Iend-Istart+1
      IGBGN=Basis%shell(Ishell)%EXPBGN
      IGEND=Basis%shell(Ishell)%EXPEND
      Ifrst=Basis%shell(Ishell)%frstSHL
!
! Loop over Jshell.
      do Jshell=1,Ishell
      LBMAX=Basis%shell(Jshell)%Xtype+1
      Jstart=Basis%shell(Jshell)%Xstart
      Jend=Basis%shell(Jshell)%XEND
      Jrange=Jend-Jstart+1
      JGBGN=Basis%shell(Jshell)%EXPBGN
      JGEND=Basis%shell(Jshell)%EXPEND
      Jfrst=Basis%shell(Jshell)%frstSHL
!
      Ilast= Basis%shell(Ishell)%lastSHL
      Jlast= Basis%shell(Jshell)%lastSHL
!
! Loop over Iatmshl
      do Iatmshl=Ifrst,Ilast
      XA=CARTESIAN(Basis%atmshl(Iatmshl)%ATMLST)%X
      YA=CARTESIAN(Basis%atmshl(Iatmshl)%ATMLST)%Y
      ZA=CARTESIAN(Basis%atmshl(Iatmshl)%ATMLST)%Z
      Iaos=Basis%atmshl(Iatmshl)%frstAO-1
!
! Loop over Jatmshl
      if(Ishell.EQ.Jshell)Jlast=Iatmshl
      do Jatmshl=Jfrst,Jlast
      XB=CARTESIAN(Basis%atmshl(Jatmshl)%ATMLST)%X
      YB=CARTESIAN(Basis%atmshl(Jatmshl)%ATMLST)%Y
      ZB=CARTESIAN(Basis%atmshl(Jatmshl)%ATMLST)%Z
      Jaos=Basis%atmshl(Jatmshl)%frstAO-1
!
      LPMAX=LAMAX+LBMAX-1
      LIM1DS=(LPMAX+3)/2
      LENTQ=Irange*Jrange
      NZERO=(LAMAX+LBMAX-2)/2+1
      ABX=XB-XA
      ABY=YB-YA
      ABZ=ZB-ZA
      RABSQ=ABX*ABX+ABY*ABY+ABZ*ABZ
!
      EEPV(1:LENTQ)=ZERO
!
! Loop over primitive Gaussians.
      do Igauss=IGBGN,IGEND
      AS=Basis%gaussian(Igauss)%exp
      TWOASQ=TWO*AS*AS
      ASXA=AS*XA
      ASYA=AS*YA
      ASZA=AS*ZA
      ARABSQ=AS*RABSQ
      call FILLCC (LAMAX, Basis%gaussian(Igauss)%CONTRC, CA)

      do Jgauss=JGBGN,JGEND
      BS=Basis%gaussian(Jgauss)%exp
      call FILLCC (LBMAX, Basis%gaussian(Jgauss)%CONTRC, CB)
      EP=AS+BS
      EPI=ONE/EP
      EPIO2=PT5*EPI
      TWOP=EP+EP
      ARG=-BS*ARABSQ*EPI
      PEXP=ZERO
      if(ARG.GT.CUT1)PEXP=DEXP(ARG)
      ZTEMP=TWOPI*EPI*PEXP
      PX=(ASXA+BS*XB)*EPI
      PY=(ASYA+BS*YB)*EPI
      PZ=(ASZA+BS*ZB)*EPI
      XAP=PX-XA
      XBP=PX-XB
      YAP=PY-YA
      YBP=PY-YB
      ZAP=PZ-ZA
      ZBP=PZ-ZB
      call GETCC1 (CCX, XAP, XBP, LAMAX+2, LBMAX+2)
      call GETCC1 (CCY, YAP, YBP, LAMAX+2, LBMAX+2)
      call GETCC1 (CCZ, ZAP, ZBP, LAMAX+2, LBMAX+2)
!
! Zero accumulation area.
      EEPB(1:LENTQ)=ZERO
!
! Calculate the potential at grid point
      XC=grid_points(IApoint)%X
      YC=grid_points(IApoint)%Y
      ZC=grid_points(IApoint)%Z

      ZT=ZTEMP
      PCX=XC-PX
      PCY=YC-PY
      PCZ=ZC-PZ
      RPCSQ=PCX*PCX+PCY*PCY+PCZ*PCZ
      ARG=EP*RPCSQ
      call RPOLX (NZERO, ARG, TP, WP)
      call GETA1 (A, EPIO2, 0, LPMAX)
!
! Loop over zeroes of Rys polynomial.
      do IZERO=1,NZERO
      TWOPT2=TWOP*TP(IZERO)
      ZCONST=ZT*WP(IZERO)
      call GET2C (TWOCX, PCX, ONE, A, TWOPT2, 0, LPMAX)
      call GET2C (TWOCY, PCY, ONE, A, TWOPT2, 0, LPMAX)
      call GET2C (TWOCZ, PCZ, ZCONST, A, TWOPT2, 0, LPMAX)
      call GET3C (XIP, 0, TWOCX, CCX, LAMAX, LBMAX)
      call GET3C (YIP, 0, TWOCY, CCY, LAMAX, LBMAX)
      call GET3C (ZIP, 0, TWOCZ, CCZ, LAMAX, LBMAX)
!
! Loop over atomic orbitals.
      INTC=0
      do I=Istart,Iend
      IX=INDIX(I)
      IY=INDIY(I)
      IZ=INDIZ(I)
!
      do J=Jstart,Jend
      JX=INDJX(J)
      JY=INDJY(J)
      JZ=INDJZ(J)
      INTC=INTC+1
      EEPB(INTC)=EEPB(INTC)+XIP(IX+JX)*YIP(IY+JY)*ZIP(IZ+JZ)
      end do ! J
      end do ! I
! End of AO loop.
!
      end do ! IZERO
! End of loop over Rys zeroes.
!
! Apply the contraction coefficients.
! The potential integrals, is in EEPB
      INTC=0
      do I=Istart,Iend
      do J=Jstart,Jend
      INTC=INTC+1
      COEF=CA(I)*CB(J)
      EEPV(INTC)=EEPV(INTC)+EEPB(INTC)*COEF          ! new
!     EEPV(INTC)=EEPV(INTC)-EEPB(INTC)*COEF          ! new
      end do ! J
      end do ! I
!
      end do ! Jgauss
      end do ! Igauss
! End of loop over Gaussians.
!
! FILMAT takes the integrals in EEPV and stores them in their proper places in V12dr2.
      call FILMAT (EEPV, V12dr2, MATlen, Iend, Jend, &
                         Iatmshl, Jatmshl, Irange, Jrange, Iaos, Jaos)
!
      end do ! Jatmshl
      end do ! Iatmshl
      end do ! Jshell
      end do ! Ishell
!
      Vpotl(IApoint)= TRACLO (V12dr2, PM0, Basis%Nbasis, MATlen)

      end do ! IApoint

! End of loop over shells.
      deallocate (V12dr2)
!
! End of routine I1E_potl
      call PRG_manager ('exit', 'I1E_potl', 'UTILITY')
      return
      end subroutine I1E_potl
      subroutine PRT_NI_Coulomb
!***********************************************************************
!     Date last modified December 2013                                 *
!     Author: R.A. Poirier                                             *
!     Description: Print the dipole moment results                     *
!***********************************************************************
! Modules:
      USE program_files
      USE QM_objects
      USE NI_defaults
      USE N_integration
      USE NI_potential
      USE NI_Coulomb

      implicit none

! Local scalars:  
      double precision :: HFsum,NIsum

! Begin:
      select case(NI_property)
        case('COULOMBENERGY')
          error_coul = coul_energy_tot - E_COUL_RHF
          accuracy_coul = - DLOG10(DABS((coul_energy_tot/E_COUL_RHF)-1))
          error_coul = error_coul *1D6
          write(UNIout,'(a,f14.6,f16.6,f14.3,1PE21.3,a)') &
          'Coulomb    ',coul_energy_tot,E_COUL_RHF,accuracy_coul,error_coul,' mhartrees'

        case('COUL_DFT')
          coul_energy_DFT_tot = coul_energy_DFT_tot
          error_coul_DFT = coul_energy_DFT_tot - E_COUL_RHF
          accuracy_coul_DFT = - DLOG10(DABS((coul_energy_DFT_tot/E_COUL_RHF)-1))
          error_coul_DFT = error_coul_DFT*1D6
          write(UNIout,'(a,f14.6,f16.6,f14.3,1PE21.3,a)') &
          'Coulomb DFT',coul_energy_DFT_tot,E_COUL_RHF,accuracy_coul_DFT,error_coul_DFT,' mhartrees'

        case('ALL')
          coul_energy_DFT_tot = coul_energy_DFT_tot
          error_coul_DFT = coul_energy_DFT_tot - E_COUL_RHF
          accuracy_coul_DFT = - DLOG10(DABS((coul_energy_DFT_tot/E_COUL_RHF)-1))
          error_coul_DFT = error_coul_DFT*1D6
          write(UNIout,'(a,f14.6,f16.6,f14.3,1PE21.3,a)') &
          'Coulomb DFT',coul_energy_DFT_tot,E_COUL_RHF,accuracy_coul_DFT,error_coul_DFT,' mhartrees'

          NIsum = pot_energy_tot  + coul_energy_DFT_tot
          HFsum = E_V_RHF + E_COUL_RHF
          error_VTOT = (HFsum - NIsum)*1.0D06
          write(UNIout,'(a,4x,f14.6,f16.6,14x,1PE21.3,a)') &
          'VNE+VEE',NIsum,HFsum,error_VTOT,' mhartrees'
!         write(UNIout,'(a,f14.6,f16.6,14x,1PE21.3/)')'Vne+Coulomb',NIsum,HFsum,error_VTOT
! NN old
          error_coul = coul_energy_tot - E_COUL_RHF
          accuracy_coul = - DLOG10(DABS((coul_energy_tot/E_COUL_RHF)-1))
          error_coul = error_coul *1D6
          write(UNIout,'(a,f14.6,f16.6,f14.3,1PE21.3,a)') &
          'Coulomb OLD',coul_energy_tot,E_COUL_RHF,accuracy_coul,error_coul,' mhartrees'
! NN new
          error_coul = coul_energy_NN_tot - E_COUL_RHF
          accuracy_coul = - DLOG10(DABS((coul_energy_NN_tot/E_COUL_RHF)-1))
          error_coul = error_coul *1D6
          write(UNIout,'(a,f14.6,f16.6,f14.3,1PE21.3,a)') &
          'Coulomb NN ',coul_energy_NN_tot,E_COUL_RHF,accuracy_coul,error_coul,' mhartrees'
      end select

      return
      end subroutine PRT_NI_Coulomb
      subroutine PRT_NI_Coulomb_AIM
!***********************************************************************
!     Date last modified December 2013                                 *
!     Author: R.A. Poirier                                             *
!     Description: Print the dipole moment results                     *
!***********************************************************************
! Modules:
      USE program_files
      USE type_molecule
      USE NI_defaults
      USE N_integration
      USE NI_Coulomb

      implicit none

! Local scalars:  
      integer :: Iatom

! Begin:
      select case(NI_property)
        case('COULOMBENERGY')
          write(UNIout,'(a)')'Coulomb energy by atom:'
          do Iatom=1,Natoms
            write(UNIout,'(i8,f14.8)')Iatom,coul_energy(Iatom)
          end do
          deallocate(coul_energy)

        case('COUL_DFT')
          write(UNIout,'(a)')'Coulomb (Num/Anal) energy by atom:'
          do Iatom=1,Natoms
            write(UNIout,'(i8,f14.8)')Iatom,coul_energy_DFT(Iatom)
          end do
          write(UNIout,'(a8,f14.8)')'Total: ',coul_energy_DFT_tot
          deallocate(coul_energy_DFT)

        case('ALL')
          write(UNIout,'(a)')'Coulomb energy by atom:'
          write(UNIout,'(a)')'Atom         Num/Anal      Num/Num       Num/Num'
          do Iatom=1,Natoms
            write(UNIout,'(i8,3f14.8)')Iatom,coul_energy_DFT(Iatom),coul_energy(Iatom),coul_energy_NN(Iatom)
          end do
          write(UNIout,'(a8,3f14.8)')'Total: ',coul_energy_DFT_tot,coul_energy_tot,coul_energy_NN_tot
          deallocate(coul_energy_DFT)

      end select

      return
      end subroutine PRT_NI_Coulomb_AIM
      subroutine INI_NI_Vee
!************************************************************************************
!     Date last modified December 10, 2013                                          *
!     Author: R.A. Poirier                                                          *
!     Description: Initialization for the Coulomb (full numerical)                  *
!************************************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE NI_Coulomb

      implicit none

! Input scalars:
      allocate(coul_energy(Natoms))
      coul_energy_tot=ZERO
      coul_energy(1:Natoms)=ZERO

      return
      end subroutine INI_NI_Vee
      subroutine INI_NI_Vee_NN
!************************************************************************************
!     Date last modified December 10, 2013                                          *
!     Author: R.A. Poirier                                                          *
!     Description: Initialization for the Coulomb (full numerical)                  *
!************************************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE NI_Coulomb

      implicit none

! Input scalars:
      allocate(coul_energy_NN(Natoms))
      coul_energy_NN_tot=ZERO
      coul_energy_NN(1:Natoms)=ZERO

      return
      end subroutine INI_NI_Vee_NN
      subroutine INI_NI_Vee_DFT
!************************************************************************************
!     Date last modified December 10, 2013                                          *
!     Author: R.A. Poirier                                                          *
!     Description: Initialization for the Coulomb (partly numerical)                *
!************************************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE NI_Coulomb

      implicit none

! Input scalars:
      allocate(coul_energy_DFT(Natoms))
      coul_energy_DFT_tot=ZERO
      coul_energy_DFT(1:Natoms)=ZERO

      return
      end subroutine INI_NI_Vee_DFT
      subroutine coulomb_energy_NN (Iatom,Ifound)
!************************************************************************************
!     Date last modified April 25th ,2005                                            *
!     Author: Aisha                                                                 *
!     Description: Double numerical                                                 *
!************************************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE N_integration
      USE NI_defaults
      USE NI_Coulomb
      USE type_Weights

      implicit none

! Local scalars:
      integer :: Iatom,Ifound
      integer :: IApoint,JApoint,IZ_num
      integer :: Jatom,Jfound,JZ_num,JNApts_atom
      double precision :: rij,temp,sum_coul
      double precision, dimension(:), allocatable :: CoulombA,CoulombB
      double precision, dimension(:), allocatable :: Jcharge,JBweights
      type(type_grid_points),dimension(:),allocatable :: J_grid_points

! Begin: 
      allocate(CoulombA(NApts_atom))

! rho(ri)dri
      do IApoint=1,NApts_atom
        CoulombA(IApoint)=charge(IApoint)*Bweights(IApoint)*grid_points(IApoint)%w
      end do

      do Jatom=Iatom,Natoms
        JZ_num=CARTESIAN(Jatom)%Atomic_number
        if(JZ_num.le.0)cycle
        Jfound=GRID_loc(JZ_num)
          if(Iatom.eq.Jatom)then
            JNApts_atom = NApts_atom
            allocate(J_grid_points(JNApts_atom),CoulombB(JNApts_atom))
            allocate(Jcharge(JNApts_atom),JBweights(JNApts_atom))
            J_grid_points=grid_points
            CoulombB=CoulombA
          else
            JNApts_atom=NApoints_atom(Jfound)
            allocate(J_grid_points(1:JNApts_atom))
            do JApoint=1,JNApts_atom
              J_grid_points(JApoint)%X=Egridpts(Jfound,JApoint)%X+CARTESIAN(Jatom)%X
              J_grid_points(JApoint)%Y=Egridpts(Jfound,JApoint)%Y+CARTESIAN(Jatom)%Y
              J_grid_points(JApoint)%Z=Egridpts(Jfound,JApoint)%Z+CARTESIAN(Jatom)%Z
              J_grid_points(JApoint)%w=Egridpts(Jfound,JApoint)%w
            end do
            allocate(Jcharge(JNApts_atom),CoulombB(JNApts_atom),JBweights(JNApts_atom))
            call GET_density (J_grid_points, JNApts_atom, Jcharge)
            call GET_weights (J_grid_points, JNApts_atom, Jatom, JBweights)  

            do JApoint = 1,JNApts_atom
              CoulombB(JApoint) = Jcharge(JApoint)*JBweights(JApoint)*J_grid_points(JApoint)%w
            end do
          end if ! Iatom.eq.Jatom
 
          do IApoint=1,NApts_atom
            do JApoint=1,JNApts_atom
              if(IApoint.eq.JApoint.and.Iatom.eq.Jatom)cycle
              rij = DSQRT((grid_points(IApoint)%X-J_grid_points(JApoint)%X)**2+ &
                          (grid_points(IApoint)%Y-J_grid_points(JApoint)%Y)**2+ &
                          (grid_points(IApoint)%Z-J_grid_points(JApoint)%Z)**2)
              temp = CoulombA(IApoint)*CoulombB(JApoint)/rij
              coul_energy_NN(Iatom)=coul_energy_NN(Iatom)+PT5*temp
              if(Iatom.ne.Jatom)then
              coul_energy_NN(Jatom)=coul_energy_NN(Jatom)+PT5*temp
              end if
            end do !JApoint
          end do !IApoint
          deallocate(J_grid_points)
          deallocate(Jcharge,CoulombB,JBweights)
      end do ! Jatom

      coul_energy_NN(Iatom)=FourPi*FourPi*coul_energy_NN(Iatom)
      coul_energy_NN_tot = coul_energy_NN_tot+coul_energy_NN(Iatom)

      deallocate(CoulombA)

      return
      end subroutine coulomb_energy_NN
      subroutine I1E_ERpot_val (rx, ry, rz)
!***********************************************************************
!     Date last modified: August 14, 2018                  Version 1.0 *
!     Author: JWH                                                      *
!     Description: Calculate AO product contribution to ERI potential  *
!***********************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE type_basis_set
      USE INT_objects
      USE module_grid_points
      USE NI_Coulomb

      implicit none
!
! Input scalars:
      double precision :: rx,ry,rz

! Local scalars:
      integer :: MATlen
!
! Local scalars:
      integer :: Ishell,Jshell,Ifrst,Jfrst,Ilast,Jlast
      integer :: Istart,Jstart,Iend,Jend
      integer :: Iatmshl,Jatmshl
      integer :: LAMAX,LBMAX
      integer :: Iatom,Jatom
      integer :: Irange,Jrange
      integer :: Igauss,Jgauss
      integer :: Iaos,Jaos
      integer :: IGBGN,JGBGN,IGend,JGend
      integer :: LPMAX,I,IA,IJX,IJY,IJZ,INTC,IX,IXP,IY,IYP,IZ,IZERO,IZP,J,JX,JY,JZ,LIM1DS,NZERO
      integer :: LENTQ
      integer :: IApoint
      double precision :: XA,XB,XC,YA,YB,YC,ZA,ZB,ZC,RABSQ
      double precision :: TRACLO
      double precision ABX,ABY,ABZ,ARABSQ,ARG,AS,ASXA,ASYA,ASZA,BS,COEF,CUT1,EP,EPI,EPIO2,PCX,PCY,PCZ,PEXP, &
                       PX,PY,PZ,RPCSQ,TWOASQ,TWOP,TWOPI,TWOPT2,XAP,XBP,XIIM1, &
                       XK,YAP,YBP,YIIM1,ZAP,ZBP,ZCONST,ZIIM1,ZT,ZTEMP
      integer :: INDIX(20),INDIY(20),INDIZ(20),INDJX(20),INDJY(20),INDJZ(20)
      double precision :: TP(7),WP(7)
      double precision A(45),CA(20),CB(20),CCX(192),CCY(192),CCZ(192),EEPB(100),EEPV(100), &
                       SX(32),SY(32),SZ(32), &
                       S1C(6),TWOCX(9),TWOCY(9),TWOCZ(9),XIP(80),YIP(80),ZIP(80)

      parameter (TWOPI=TWO*PI_VAL,CUT1=-75.0D0)
      DATA INDJX/1,2,1,1,3,1,1,2,2,1,4,1,1,2,3,3,2,1,1,2/,INDJY/1,1,2,1,1,3,1,2,1,2,1,4,1,3,2,1,1,2,3,2/, &
           INDJZ/1,1,1,2,1,1,3,1,2,2,1,1,4,1,1,2,3,3,2,2/
!
! Begin:
      call PRG_manager ('enter', 'I1E_ERpot_val', 'UTILITY')
!
      MATlen=Basis%nbasis*(Basis%nbasis+1)/2
!
      call RYSSET
!
! Fill INDIX.
      do I=1,20
        INDIX(I)=4*(INDJX(I)-1)
        INDIY(I)=4*(INDJY(I)-1)
        INDIZ(I)=4*(INDJZ(I)-1)
      end do ! I
!
! Loop over shells.
! Loop over Ishell.
!
      do Ishell=1,Basis%Nshells
      LAMAX=Basis%shell(Ishell)%Xtype+1
      Istart=Basis%shell(Ishell)%Xstart
      Iend=Basis%shell(Ishell)%XEND
      Irange=Iend-Istart+1
      IGBGN=Basis%shell(Ishell)%EXPBGN
      IGEND=Basis%shell(Ishell)%EXPEND
      Ifrst=Basis%shell(Ishell)%frstSHL
!
! Loop over Jshell.
      do Jshell=1,Ishell
      LBMAX=Basis%shell(Jshell)%Xtype+1
      Jstart=Basis%shell(Jshell)%Xstart
      Jend=Basis%shell(Jshell)%XEND
      Jrange=Jend-Jstart+1
      JGBGN=Basis%shell(Jshell)%EXPBGN
      JGEND=Basis%shell(Jshell)%EXPEND
      Jfrst=Basis%shell(Jshell)%frstSHL
!
      Ilast= Basis%shell(Ishell)%lastSHL
      Jlast= Basis%shell(Jshell)%lastSHL
!
! Loop over Iatmshl
      do Iatmshl=Ifrst,Ilast
      XA=CARTESIAN(Basis%atmshl(Iatmshl)%ATMLST)%X
      YA=CARTESIAN(Basis%atmshl(Iatmshl)%ATMLST)%Y
      ZA=CARTESIAN(Basis%atmshl(Iatmshl)%ATMLST)%Z
      Iaos=Basis%atmshl(Iatmshl)%frstAO-1
!
! Loop over Jatmshl
      if(Ishell.EQ.Jshell)Jlast=Iatmshl
      do Jatmshl=Jfrst,Jlast
      XB=CARTESIAN(Basis%atmshl(Jatmshl)%ATMLST)%X
      YB=CARTESIAN(Basis%atmshl(Jatmshl)%ATMLST)%Y
      ZB=CARTESIAN(Basis%atmshl(Jatmshl)%ATMLST)%Z
      Jaos=Basis%atmshl(Jatmshl)%frstAO-1
!
      LPMAX=LAMAX+LBMAX-1
      LIM1DS=(LPMAX+3)/2
      LENTQ=Irange*Jrange
      NZERO=(LAMAX+LBMAX-2)/2+1
      ABX=XB-XA
      ABY=YB-YA
      ABZ=ZB-ZA
      RABSQ=ABX*ABX+ABY*ABY+ABZ*ABZ
!
      EEPV(1:LENTQ)=ZERO
!
! Loop over primitive Gaussians.
      do Igauss=IGBGN,IGEND
      AS=Basis%gaussian(Igauss)%exp
      TWOASQ=TWO*AS*AS
      ASXA=AS*XA
      ASYA=AS*YA
      ASZA=AS*ZA
      ARABSQ=AS*RABSQ
      call FILLCC (LAMAX, Basis%gaussian(Igauss)%CONTRC, CA)

      do Jgauss=JGBGN,JGEND
      BS=Basis%gaussian(Jgauss)%exp
      call FILLCC (LBMAX, Basis%gaussian(Jgauss)%CONTRC, CB)
      EP=AS+BS
      EPI=ONE/EP
      EPIO2=PT5*EPI
      TWOP=EP+EP
      ARG=-BS*ARABSQ*EPI
      PEXP=ZERO
      if(ARG.GT.CUT1)PEXP=DEXP(ARG)
      ZTEMP=TWOPI*EPI*PEXP
      PX=(ASXA+BS*XB)*EPI
      PY=(ASYA+BS*YB)*EPI
      PZ=(ASZA+BS*ZB)*EPI
      XAP=PX-XA
      XBP=PX-XB
      YAP=PY-YA
      YBP=PY-YB
      ZAP=PZ-ZA
      ZBP=PZ-ZB
      call GETCC1 (CCX, XAP, XBP, LAMAX+2, LBMAX+2)
      call GETCC1 (CCY, YAP, YBP, LAMAX+2, LBMAX+2)
      call GETCC1 (CCZ, ZAP, ZBP, LAMAX+2, LBMAX+2)
!
! Zero accumulation area.
      EEPB(1:LENTQ)=ZERO
!
! Calculate the potential at grid point
      XC=rx
      YC=ry
      ZC=rz

      ZT=ZTEMP
      PCX=XC-PX
      PCY=YC-PY
      PCZ=ZC-PZ
      RPCSQ=PCX*PCX+PCY*PCY+PCZ*PCZ
      ARG=EP*RPCSQ
      call RPOLX (NZERO, ARG, TP, WP)
      call GETA1 (A, EPIO2, 0, LPMAX)
!
! Loop over zeroes of Rys polynomial.
      do IZERO=1,NZERO
      TWOPT2=TWOP*TP(IZERO)
      ZCONST=ZT*WP(IZERO)
      call GET2C (TWOCX, PCX, ONE, A, TWOPT2, 0, LPMAX)
      call GET2C (TWOCY, PCY, ONE, A, TWOPT2, 0, LPMAX)
      call GET2C (TWOCZ, PCZ, ZCONST, A, TWOPT2, 0, LPMAX)
      call GET3C (XIP, 0, TWOCX, CCX, LAMAX, LBMAX)
      call GET3C (YIP, 0, TWOCY, CCY, LAMAX, LBMAX)
      call GET3C (ZIP, 0, TWOCZ, CCZ, LAMAX, LBMAX)
!
! Loop over atomic orbitals.
      INTC=0
      do I=Istart,Iend
      IX=INDIX(I)
      IY=INDIY(I)
      IZ=INDIZ(I)
!
      do J=Jstart,Jend
      JX=INDJX(J)
      JY=INDJY(J)
      JZ=INDJZ(J)
      INTC=INTC+1
      EEPB(INTC)=EEPB(INTC)+XIP(IX+JX)*YIP(IY+JY)*ZIP(IZ+JZ)
      end do ! J
      end do ! I
! End of AO loop.
!
      end do ! IZERO
! End of loop over Rys zeroes.
!
! Apply the contraction coefficients.
! The potential integrals, is in EEPB
      INTC=0
      do I=Istart,Iend
      do J=Jstart,Jend
      INTC=INTC+1
      COEF=CA(I)*CB(J)
      EEPV(INTC)=EEPV(INTC)+EEPB(INTC)*COEF          ! new
      end do ! J
      end do ! I
!
      end do ! Jgauss
      end do ! Igauss
! End of loop over Gaussians.
!
! FILMAT takes the integrals in EEPV and stores them in their proper places in ERpot
      call FILMAT (EEPV, ERpot, MATlen, Iend, Jend, &
                         Iatmshl, Jatmshl, Irange, Jrange, Iaos, Jaos)
!
      end do ! Jatmshl
      end do ! Iatmshl
      end do ! Jshell
      end do ! Ishell
!
! End of routine I1E_ERpot_val
      call PRG_manager ('exit', 'I1E_ERpot_val', 'UTILITY')
      return
      end subroutine I1E_ERpot_val
      subroutine NI_Vee
!*********************************************************************************************
!     Date last modified: August 10, 2022                                                    *
!     Author: JWH                                                                            *
!     Desciption: Calculate the electron repulsion energy via integration of the potential.  *
!*********************************************************************************************
! Modules:
      USE program_constants
      USE program_timing
      USE program_files
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE module_grid_points
      USE INT_objects

      implicit none
!
! Local scalars:
      integer :: Iatom,IApoint,IAIM,Znum,Ifound
      double precision :: Xpt,Ypt,Zpt
      double precision :: Veeaaatom,VeeHFaaatom
      double precision :: Veeaa_total,VeeHFaa_total
      double precision :: VeeaagridA, VeeHFaagridA
      double precision :: Veebbatom,VeeHFbbatom
      double precision :: Veebb_total,VeeHFbb_total
      double precision :: VeebbgridA, VeeHFbbgridA
      double precision :: Veeabatom,VeeHFabatom
      double precision :: Veeab_total,VeeHFab_total
      double precision :: VeeabgridA, VeeHFabgridA
      double precision :: Veebaatom,VeeHFbaatom
      double precision :: Veeba_total,VeeHFba_total
      double precision :: VeebagridA, VeeHFbagridA
      double precision, allocatable, dimension(:) :: Veeaa_atomic, Veebb_atomic
      double precision, allocatable, dimension(:) :: VeeHFaa_atomic, VeeHFbb_atomic
      double precision, allocatable, dimension(:) :: Veeab_atomic, Veeba_atomic
      double precision, allocatable, dimension(:) :: VeeHFab_atomic, VeeHFba_atomic
      double precision :: t0, t1, t2, time
!
! Local arrays:
      double precision, dimension(:), allocatable :: WeightsA
!
! Begin:
      call PRG_manager ('enter', 'NI_Vee', 'VEE%NI')
!
      call GET_object ('QM', '2RDM', 'AO')
!
! Get grid and weights
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
      write(UNIout,'(/2a)')'Radial grid used for numerical integration: ',RADIAL_grid
      call BLD_BeckeW_XI
      write(UNIout,'(2a)')'Weights used for numerical integration:      BECKE'
!
      call CPU_time(t0)
      call CPU_time(t1)      
!
! same-spin
      allocate(VeeHFaa_atomic(Natoms),Veeaa_atomic(Natoms))
      allocate(VeeHFbb_atomic(Natoms),Veebb_atomic(Natoms))
      Veeaa_total=ZERO
      Veeaa_atomic=ZERO
      VeeHFaa_total=ZERO
      VeeHFaa_atomic=ZERO
      Veebb_total=ZERO
      Veebb_atomic=ZERO
      VeeHFbb_total=ZERO
      VeeHFbb_atomic=ZERO
!
      write(UNIout,'(/a)')'   Electron repulsion energy via numerical integration (same-spin)  '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '                            Atomic contributions             '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '                   Hartree-Fock                 Correlated          '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '  Atom       alpha-alpha    beta-beta    alpha-alpha    beta-beta   '
      write(UNIout,'(a)') '--------------------------------------------------------------------'

      if(NAIMprint.eq.0)then ! If no atoms specified, do them all!
        NAIMprint=Natoms
        do Iatom=1,Natoms
          AIMprint(Iatom)=Iatom
        end do ! Iatom
      end if

      if(allocated(grid_points))then
        deallocate(grid_points)
      end if 
!
      do IAIM=1,NAIMprint
!     do Iatom=1,Natoms
        Iatom=AIMprint(IAIM)
        Znum=CARTESIAN(Iatom)%atomic_number
        if(Znum.le.0)cycle
        Ifound=GRID_loc(Znum)
        NApts_atom=NApoints_atom(Ifound)

        allocate(grid_points(1:NApts_atom))
        allocate(WeightsA(NApts_atom))
!
! Moving grid points on cartesian coordinate grid
        do IApoint=1,NApts_atom
          grid_points(IApoint)%X=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          grid_points(IApoint)%Y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          grid_points(IApoint)%Z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
        end do ! IApoint

        call GET_weights (grid_points, NApts_atom, Iatom, WeightsA)

        Veeaaatom=ZERO
        VeeHFaaatom=ZERO
        Veebbatom=ZERO
        VeeHFbbatom=ZERO
!

        do IApoint=1,NApts_atom
      !    if(WeightsA(IApoint).le.1.0D-6)cycle ! Makes no difference
          Xpt=grid_points(IApoint)%X
          Ypt=grid_points(IApoint)%Y
          Zpt=grid_points(IApoint)%Z
!
          call GET_ERpotss_point (Xpt, Ypt, Zpt, VeeaagridA, VeeHFaagridA, VeebbgridA, VeeHFbbgridA)
!
          Veeaaatom = Veeaaatom + VeeaagridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
          VeeHFaaatom = VeeHFaaatom + VeeHFaagridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
          Veebbatom = Veebbatom + VeebbgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
          VeeHFbbatom = VeeHFbbatom + VeeHFbbgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
        end do ! IApoint

        Veeaa_atomic(Iatom) = Veeaa_atomic(Iatom) + Veeaaatom
        VeeHFaa_atomic(Iatom) = VeeHFaa_atomic(Iatom) + VeeHFaaatom
!
        Veebb_atomic(Iatom) = Veebb_atomic(Iatom) + Veebbatom
        VeeHFbb_atomic(Iatom) = VeeHFbb_atomic(Iatom) + VeeHFbbatom
!
        deallocate(grid_points)
        deallocate(WeightsA)

      end do ! Iatom

!
! Now multiply by 4pi and get total
      do IAIM=1,NAIMprint
        Iatom=AIMprint(IAIM)
!     do Iatom=1,Natoms
      Znum=CARTESIAN(Iatom)%atomic_number
      if(Znum.le.0)cycle
!
      Veeaa_atomic(Iatom) = FourPi*Veeaa_atomic(Iatom)
      VeeHFaa_atomic(Iatom) = FourPi*VeeHFaa_atomic(Iatom)
!
      Veebb_atomic(Iatom) = FourPi*Veebb_atomic(Iatom)
      VeeHFbb_atomic(Iatom) = FourPi*VeeHFbb_atomic(Iatom)
!
      write(UNIout,'(i8,1x,4f14.8)')Iatom,VeeHFaa_atomic(Iatom),VeeHFbb_atomic(Iatom),&
                                          Veeaa_atomic(Iatom),Veebb_atomic(Iatom)
!
      Veeaa_total = Veeaa_total + Veeaa_atomic(Iatom)
      VeeHFaa_total = VeeHFaa_total + VeeHFaa_atomic(Iatom)
!
      Veebb_total = Veebb_total + Veebb_atomic(Iatom)
      VeeHFbb_total = VeeHFbb_total + VeeHFbb_atomic(Iatom)
!
      end do ! Iatom
!
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      if(NAIMprint.eq.Natoms)write(UNIout,'(a,1x,4f14.8)')'   Total',VeeHFaa_total,VeeHFbb_total,&
                                                                     Veeaa_total,Veebb_total
!
      deallocate(Veeaa_atomic,VeeHFaa_atomic)
      deallocate(Veebb_atomic,VeeHFbb_atomic)
!
! opposite-spin
      allocate(VeeHFab_atomic(Natoms),Veeab_atomic(Natoms))
      allocate(VeeHFba_atomic(Natoms),Veeba_atomic(Natoms))
      Veeab_total=ZERO
      Veeab_atomic=ZERO
      VeeHFab_total=ZERO
      VeeHFab_atomic=ZERO
      Veeba_total=ZERO
      Veeba_atomic=ZERO
      VeeHFba_total=ZERO
      VeeHFba_atomic=ZERO
!
      write(UNIout,'(/a)')' Electron repulsion energy via numerical integration (opposite-spin) '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '                            Atomic contributions             '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '                   Hartree-Fock                 Correlated          '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '  Atom       alpha-beta    beta-alpha    alpha-beta    beta-alpha   '
      write(UNIout,'(a)') '--------------------------------------------------------------------'

      if(NAIMprint.eq.0)then ! If no atoms specified, do them all!
        NAIMprint=Natoms
        do Iatom=1,Natoms
          AIMprint(Iatom)=Iatom
        end do ! Iatom
      end if

      do IAIM=1,NAIMprint
!     do Iatom=1,Natoms
        Iatom=AIMprint(IAIM)
        Znum=CARTESIAN(Iatom)%atomic_number
        if(Znum.le.0)cycle
        Ifound=GRID_loc(Znum)
        NApts_atom=NApoints_atom(Ifound)

        allocate(grid_points(1:NApts_atom))
        allocate(WeightsA(NApts_atom))
!
! Moving grid points on cartesian coordinate grid
        do IApoint=1,NApts_atom
          grid_points(IApoint)%X=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          grid_points(IApoint)%Y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          grid_points(IApoint)%Z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
        end do ! IApoint

        call GET_weights (grid_points, NApts_atom, Iatom, WeightsA)

        Veeabatom=ZERO
        VeeHFabatom=ZERO
        Veebaatom=ZERO
        VeeHFbaatom=ZERO
!

        do IApoint=1,NApts_atom
      !    if(WeightsA(IApoint).le.1.0D-6)cycle ! Makes no difference
          Xpt=grid_points(IApoint)%X
          Ypt=grid_points(IApoint)%Y
          Zpt=grid_points(IApoint)%Z
!
          call GET_ERpotos_point (Xpt, Ypt, Zpt, VeeabgridA, VeeHFabgridA, VeebagridA, VeeHFbagridA)
!
          Veeabatom = Veeabatom + VeeabgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
          VeeHFabatom = VeeHFabatom + VeeHFabgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
          Veebaatom = Veebaatom + VeebagridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
          VeeHFbaatom = VeeHFbaatom + VeeHFbagridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
        end do ! IApoint

        Veeab_atomic(Iatom) = Veeab_atomic(Iatom) + Veeabatom
        VeeHFab_atomic(Iatom) = VeeHFab_atomic(Iatom) + VeeHFabatom
!
        Veeba_atomic(Iatom) = Veeba_atomic(Iatom) + Veebaatom
        VeeHFba_atomic(Iatom) = VeeHFba_atomic(Iatom) + VeeHFbaatom
!
        deallocate(grid_points)
        deallocate(WeightsA)

      end do ! Iatom

!
! Now multiply by 4pi and get total
      do IAIM=1,NAIMprint
        Iatom=AIMprint(IAIM)
!     do Iatom=1,Natoms
      Znum=CARTESIAN(Iatom)%atomic_number
      if(Znum.le.0)cycle
!
      Veeab_atomic(Iatom) = FourPi*Veeab_atomic(Iatom)
      VeeHFab_atomic(Iatom) = FourPi*VeeHFab_atomic(Iatom)
!
      Veeba_atomic(Iatom) = FourPi*Veeba_atomic(Iatom)
      VeeHFba_atomic(Iatom) = FourPi*VeeHFba_atomic(Iatom)
!
      write(UNIout,'(i8,1x,4f14.8)')Iatom,VeeHFab_atomic(Iatom),VeeHFba_atomic(Iatom),&
                                          Veeab_atomic(Iatom),Veeba_atomic(Iatom)
!
      Veeab_total = Veeab_total + Veeab_atomic(Iatom)
      VeeHFab_total = VeeHFab_total + VeeHFab_atomic(Iatom)
!
      Veeba_total = Veeba_total + Veeba_atomic(Iatom)
      VeeHFba_total = VeeHFba_total + VeeHFba_atomic(Iatom)
!
      end do ! Iatom
!
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      if(NAIMprint.eq.Natoms)write(UNIout,'(a,1x,4f14.8)')'   Total',VeeHFab_total,VeeHFba_total,&
                                                                     Veeab_total,Veeba_total
!
      deallocate(Veeab_atomic,VeeHFab_atomic)
      deallocate(Veeba_atomic,VeeHFba_atomic)
!
! Could add this, but already exists just in a different form
! Get analytical value just for sanity check
!      call GET_object ('QM', 'Vee', 'ANA')
!
      call CPU_time(t2)
      time = t2 - t1
!
! End of routine NI_Vee
      call PRG_manager ('exit', 'NI_Vee', 'VEE%NI')
      end subroutine NI_Vee
      subroutine NI_Jcoul
!*********************************************************************************************
!     Date last modified: September 6, 2023                                                  *
!     Author: JWH                                                                            *
!     Desciption: Calculate the RHF/ROHF Coulomb energy via integration of the potential.    *
!*********************************************************************************************
! Modules:
      USE program_constants
      USE program_timing
      USE program_files
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE module_grid_points
      USE INT_objects

      implicit none
!
! Local scalars:
      integer :: Iatom,IApoint,IAIM,Znum,Ifound
      double precision :: Xpt,Ypt,Zpt
      double precision :: Jcoul, Jcoul_total, Jcoul_gridA, Jcoul_atom
      double precision, allocatable, dimension(:) :: Jcoul_atomic
      double precision :: t0, t1, t2, time
!
! Local arrays:
      double precision, dimension(:), allocatable :: WeightsA
!
! Begin:
      call PRG_manager ('enter', 'NI_Jcoul', 'JCOUL%NI')
!
      call GET_object ('QM', 'CMO', Wavefunction)
!
! Get grid and weights
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
      write(UNIout,'(/2a)')'Radial grid used for numerical integration: ',RADIAL_grid
      call BLD_BeckeW_XI
      write(UNIout,'(2a)')'Weights used for numerical integration:      BECKE'
!
      call CPU_time(t0)
      call CPU_time(t1)      
!
! same-spin
      allocate(Jcoul_atomic(Natoms))
      Jcoul_total=ZERO
      Jcoul_atomic=ZERO
!
      write(UNIout,'(/a)')' Coulomb repulsion energy via numerical integration   '
      write(UNIout,'(a)') '-------------------------------------------'
      write(UNIout,'(a)') '          Atomic contributions             '
      write(UNIout,'(a)') '-------------------------------------------'
      write(UNIout,'(a)') '    Atom                  Jcoul            '
      write(UNIout,'(a)') '-------------------------------------------'

      if(NAIMprint.eq.0)then ! If no atoms specified, do them all!
        NAIMprint=Natoms
        do Iatom=1,Natoms
          AIMprint(Iatom)=Iatom
        end do ! Iatom
      end if

      if(allocated(grid_points))then
        deallocate(grid_points)
      end if 
!
      do IAIM=1,NAIMprint
!     do Iatom=1,Natoms
        Iatom=AIMprint(IAIM)
        Znum=CARTESIAN(Iatom)%atomic_number
        if(Znum.le.0)cycle
        Ifound=GRID_loc(Znum)
        NApts_atom=NApoints_atom(Ifound)

        allocate(grid_points(1:NApts_atom))
        allocate(WeightsA(NApts_atom))
!
! Moving grid points on cartesian coordinate grid
        do IApoint=1,NApts_atom
          grid_points(IApoint)%X=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          grid_points(IApoint)%Y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          grid_points(IApoint)%Z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
        end do ! IApoint

        call GET_weights (grid_points, NApts_atom, Iatom, WeightsA)

        Jcoul_atom=ZERO
!

        do IApoint=1,NApts_atom
      !    if(WeightsA(IApoint).le.1.0D-6)cycle ! Makes no difference
          Xpt=grid_points(IApoint)%X
          Ypt=grid_points(IApoint)%Y
          Zpt=grid_points(IApoint)%Z
!
          call GET_coul_point (Xpt, Ypt, Zpt, Jcoul_gridA)
!
          Jcoul_atom = Jcoul_atom + Jcoul_gridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
        end do ! IApoint

        Jcoul_atomic(Iatom) = Jcoul_atomic(Iatom) + Jcoul_atom
!
        deallocate(grid_points)
        deallocate(WeightsA)

      end do ! Iatom

!
! Now multiply by 4pi and get total
      do IAIM=1,NAIMprint
        Iatom=AIMprint(IAIM)
!     do Iatom=1,Natoms
      Znum=CARTESIAN(Iatom)%atomic_number
      if(Znum.le.0)cycle
!
      Jcoul_atomic(Iatom) = FourPi*Jcoul_atomic(Iatom)
!
      write(UNIout,'(i8,12x,f14.8)')Iatom,Jcoul_atomic(Iatom)
!
      Jcoul_total = Jcoul_total + Jcoul_atomic(Iatom)
!
      end do ! Iatom
!
      write(UNIout,'(a)') '-------------------------------------------'
      if(NAIMprint.eq.Natoms)write(UNIout,'(a,12x,f14.8)')'   Total',Jcoul_total
!
      deallocate(Jcoul_atomic)
!
! Could add this, but already exists just in a different form
! Get analytical value just for sanity check
!      call GET_object ('QM', 'Jcoul', 'ANA')
!
      call CPU_time(t2)
      time = t2 - t1
!
! End of routine NI_Jcoul
      call PRG_manager ('exit', 'NI_Jcoul', 'JCOUL%NI')
      end subroutine NI_Jcoul
      subroutine NI_Kexch
!*********************************************************************************************
!     Date last modified: September 6, 2023                                                  *
!     Author: JWH                                                                            *
!     Desciption: Calculate the RHF/ROHF exchange energy via integration of the potential.   *
!*********************************************************************************************
! Modules:
      USE program_constants
      USE program_timing
      USE program_files
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE module_grid_points
      USE INT_objects

      implicit none
!
! Local scalars:
      integer :: Iatom,IApoint,IAIM,Znum,Ifound
      double precision :: Xpt,Ypt,Zpt
      double precision :: Kexch, Kexch_total, Kexch_gridA, Kexch_atom
      double precision, allocatable, dimension(:) :: Kexch_atomic
      double precision :: t0, t1, t2, time
!
! Local arrays:
      double precision, dimension(:), allocatable :: WeightsA
!
! Begin:
      call PRG_manager ('enter', 'NI_Kexch', 'KEXCH%NI')
!
      call GET_object ('QM', 'CMO', Wavefunction)
!
! Get grid and weights
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
      write(UNIout,'(/2a)')'Radial grid used for numerical integration: ',RADIAL_grid
      call BLD_BeckeW_XI
      write(UNIout,'(2a)')'Weights used for numerical integration:      BECKE'
!
      call CPU_time(t0)
      call CPU_time(t1)      
!
! same-spin
      allocate(Kexch_atomic(Natoms))
      Kexch_total=ZERO
      Kexch_atomic=ZERO
!
      write(UNIout,'(/a)')' Exchange energy via numerical integration   '
      write(UNIout,'(a)') '-------------------------------------------'
      write(UNIout,'(a)') '          Atomic contributions             '
      write(UNIout,'(a)') '-------------------------------------------'
      write(UNIout,'(a)') '    Atom                  Kexch            '
      write(UNIout,'(a)') '-------------------------------------------'

      if(NAIMprint.eq.0)then ! If no atoms specified, do them all!
        NAIMprint=Natoms
        do Iatom=1,Natoms
          AIMprint(Iatom)=Iatom
        end do ! Iatom
      end if

      if(allocated(grid_points))then
        deallocate(grid_points)
      end if 
!
      do IAIM=1,NAIMprint
!     do Iatom=1,Natoms
        Iatom=AIMprint(IAIM)
        Znum=CARTESIAN(Iatom)%atomic_number
        if(Znum.le.0)cycle
        Ifound=GRID_loc(Znum)
        NApts_atom=NApoints_atom(Ifound)

        allocate(grid_points(1:NApts_atom))
        allocate(WeightsA(NApts_atom))
!
! Moving grid points on cartesian coordinate grid
        do IApoint=1,NApts_atom
          grid_points(IApoint)%X=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          grid_points(IApoint)%Y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          grid_points(IApoint)%Z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
        end do ! IApoint

        call GET_weights (grid_points, NApts_atom, Iatom, WeightsA)

        Kexch_atom=ZERO
!

        do IApoint=1,NApts_atom
      !    if(WeightsA(IApoint).le.1.0D-6)cycle ! Makes no difference
          Xpt=grid_points(IApoint)%X
          Ypt=grid_points(IApoint)%Y
          Zpt=grid_points(IApoint)%Z
!
          call GET_exch_point (Xpt, Ypt, Zpt, Kexch_gridA)
!
          Kexch_atom = Kexch_atom + Kexch_gridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
        end do ! IApoint

        Kexch_atomic(Iatom) = Kexch_atomic(Iatom) + Kexch_atom
!
        deallocate(grid_points)
        deallocate(WeightsA)

      end do ! Iatom

!
! Now multiply by 4pi and get total
      do IAIM=1,NAIMprint
        Iatom=AIMprint(IAIM)
!     do Iatom=1,Natoms
      Znum=CARTESIAN(Iatom)%atomic_number
      if(Znum.le.0)cycle
!
      Kexch_atomic(Iatom) = FourPi*Kexch_atomic(Iatom)
!
      write(UNIout,'(i8,12x,f14.8)')Iatom,Kexch_atomic(Iatom)
!
      Kexch_total = Kexch_total + Kexch_atomic(Iatom)
!
      end do ! Iatom
!
      write(UNIout,'(a)') '-------------------------------------------'
      if(NAIMprint.eq.Natoms)write(UNIout,'(a,12x,f14.8)')'   Total',Kexch_total
!
      deallocate(Kexch_atomic)
!
! Could add this, but already exists just in a different form
! Get analytical value just for sanity check
!      call GET_object ('QM', 'Kexch', 'ANA')
!
      call CPU_time(t2)
      time = t2 - t1
!
! End of routine NI_Kexch
      call PRG_manager ('exit', 'NI_Kexch', 'KEXCH%NI')
      end subroutine NI_Kexch
