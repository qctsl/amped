      module N_integration
!**************************************************************************************
!     Date last modified: Sept 26th ,2001                                             *
!     Author: Aisha                                                                   *
!     Desciption:                                                                     *
!**************************************************************************************
! Modules:
      USE module_grid_points

      implicit none

! Local scalars:
      integer :: NRpoints ! Total number of radial points
      integer :: num_regions ! Number of regions specified by each NI method
      logical :: invariant,nuc_rot!,SSF

! Local arrays:
!     double precision, dimension (:,:), allocatable :: Radial_points ! Radial points for each atom 
!     double precision, dimension (:,:), allocatable :: Radial_weights ! Radial weights for each atom 
      integer, dimension(:,:), allocatable :: NAp_grid ! Number of angular points in each sphere for an atom type
      integer, dimension(:,:), allocatable :: NRpoints_region ! Number of radial points in each region for an atom type
      double precision, dimension (100) :: Rpoints ! ! Temporary array
      type(type_grid_points), dimension(:), allocatable :: Apoints_temp! Temporary arrays 
      double precision, dimension(:), allocatable :: Bweights ! Becke weights for each atom in the molecule
      double precision, dimension(:), allocatable :: charge ! Contains charge for each atom in the molecule
!
! SG1 data
!     The atomic radii of the atoms from H to- Ar

! Radii are from a paper by: Peter M. W. Gill, Benny G. Johnson
! and John A. Pople, Chemical Physics letters, Vol. 209, no. 5,6, page 506, 1993.
      double precision, dimension (30), parameter :: Radii = &
              (/1.0000,0.5882, &
                3.0769,2.0513,1.5385,1.2308,1.0256,0.8791,0.7692,0.6838, &
                4.0909,3.1579,2.5714,2.1687,1.8750,1.6514,1.4754,1.3333, &
                4.40,3.60,3.20,2.80,2.70,2.80,2.80,2.80,2.70,2.70,2.70,2.70/) ! Estimated to 2R_BS

      end module N_integration
