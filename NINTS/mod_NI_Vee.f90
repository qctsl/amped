      module NI_Coulomb
!**************************************************************************************
!     Date last modified: Sept 26th ,2001                                             *
!     Author: Aisha                                                                   *
!     Desciption:                                                                     *
!**************************************************************************************
! Modules:

      implicit none

! Output scalars:
      double precision :: coul_energy_tot   ! Coulomb energy double numerical
      double precision :: coul_energy_NN_tot   ! Coulomb energy double numerical
      double precision :: coul_energy_DFT_tot    ! Coulomb energy analytical/numerical
      double precision :: error_coul ! Error in Coulomb energies 
      double precision :: error_coul_DFT  ! Error in Coulomb energies DFT

! Local arrays:
      double precision, dimension(:), allocatable :: coul_energy ! Coulomb energies for the molecule
      double precision, dimension(:), allocatable :: coul_energy_NN ! Coulomb energies for the molecule
      double precision, dimension(:), allocatable :: coul_energy_DFT ! Coulomb energies for the molecule
      double precision :: accuracy_coul  ! Accuracy in Coulomb energies
      double precision :: accuracy_coul_DFT  ! Accuracy in Coulomb energies DFT

      end module NI_Coulomb
