      subroutine NI_MB
!*********************************************************************************************
!     Date last modified: April 24, 2019                                                     *
!     Author: JWH                                                                            *
!     Desciption: Calculate the momentum-balance via integration of its density.             *
!*********************************************************************************************
! Modules:
      USE program_constants
      USE program_timing
      USE program_files
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE module_grid_points
      USE INT_objects

      implicit none
!
! Local scalars:
      integer :: Iatom,IApoint,IAIM,Znum,Ifound
      double precision :: Xpt,Ypt,Zpt
      double precision :: MBaaatom,MBHFaaatom
      double precision :: MBaagridA, MBHFaagridA
      double precision :: MBbbatom,MBHFbbatom
      double precision :: MBbbgridA, MBHFbbgridA
      double precision :: MBabatom,MBHFabatom
      double precision :: MBabgridA, MBHFabgridA
      double precision :: MBbaatom,MBHFbaatom
      double precision :: MBbagridA, MBHFbagridA
      double precision :: t0, t1, t2, time
!
! Local arrays:
      double precision, dimension(:), allocatable :: WeightsA
!
! Begin:
      call PRG_manager ('enter', 'NI_MB', 'MB%NI')
!
      call GET_object ('QM', '2RDM', 'AO')
!
! Create 2rdms designed for fast calculation of MBD
       call BLD_TRDM_MB
!
! Get grid and weights
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
      write(UNIout,'(/2a)')'Radial grid used for numerical integration: ',RADIAL_grid
      call BLD_BeckeW_XI
      write(UNIout,'(2a)')'Weights used for numerical integration:      BECKE'
!
      call CPU_time(t0)
      call CPU_time(t1)      
!
! same-spin
      allocate(MBHFaa_atomic(Natoms),MBaa_atomic(Natoms))
      allocate(MBHFbb_atomic(Natoms),MBbb_atomic(Natoms))
      MBaa_total=ZERO
      MBaa_atomic=ZERO
      MBHFaa_total=ZERO
      MBHFaa_atomic=ZERO
      MBbb_total=ZERO
      MBbb_atomic=ZERO
      MBHFbb_total=ZERO
      MBHFbb_atomic=ZERO
!
      write(UNIout,'(/a)')'            Momentum-balance via numerical integration (same-spin) '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '                            Atomic contributions             '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '                   Hartree-Fock                 Correlated          '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '  Atom       alpha-alpha    beta-beta    alpha-alpha    beta-beta   '
      write(UNIout,'(a)') '--------------------------------------------------------------------'

      if(NAIMprint.eq.0)then ! If no atoms specified, do them all!
        NAIMprint=Natoms
        do Iatom=1,Natoms
          AIMprint(Iatom)=Iatom
        end do ! Iatom
      end if

      do IAIM=1,NAIMprint
!     do Iatom=1,Natoms
        Iatom=AIMprint(IAIM)
        Znum=CARTESIAN(Iatom)%atomic_number
        if(Znum.le.0)cycle
        Ifound=GRID_loc(Znum)
        NApts_atom=NApoints_atom(Ifound)

        allocate(grid_points(1:NApts_atom))
        allocate(WeightsA(NApts_atom))
!
! Moving grid points on cartesian coordinate grid
        do IApoint=1,NApts_atom
          grid_points(IApoint)%X=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          grid_points(IApoint)%Y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          grid_points(IApoint)%Z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
        end do ! IApoint

        call GET_weights (grid_points, NApts_atom, Iatom, WeightsA)

        MBaaatom=ZERO
        MBHFaaatom=ZERO
        MBbbatom=ZERO
        MBHFbbatom=ZERO
!

        do IApoint=1,NApts_atom
      !    if(WeightsA(IApoint).le.1.0D-6)cycle ! Makes no difference
          Xpt=grid_points(IApoint)%X
          Ypt=grid_points(IApoint)%Y
          Zpt=grid_points(IApoint)%Z
!
          call GET_MBDss_point (Xpt, Ypt, Zpt, MBaagridA, MBHFaagridA, MBbbgridA, MBHFbbgridA)
!
          MBaaatom = MBaaatom + MBaagridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
          MBHFaaatom = MBHFaaatom + MBHFaagridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
          MBbbatom = MBbbatom + MBbbgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
          MBHFbbatom = MBHFbbatom + MBHFbbgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
        end do ! IApoint

        MBaa_atomic(Iatom) = MBaa_atomic(Iatom) + MBaaatom
        MBHFaa_atomic(Iatom) = MBHFaa_atomic(Iatom) + MBHFaaatom
!
        MBbb_atomic(Iatom) = MBbb_atomic(Iatom) + MBbbatom
        MBHFbb_atomic(Iatom) = MBHFbb_atomic(Iatom) + MBHFbbatom
!
        deallocate(grid_points)
        deallocate(WeightsA)

      end do ! Iatom

!
! Now multiply by 4pi and get total exchange
      do IAIM=1,NAIMprint
        Iatom=AIMprint(IAIM)
!     do Iatom=1,Natoms
      Znum=CARTESIAN(Iatom)%atomic_number
      if(Znum.le.0)cycle
!
      MBaa_atomic(Iatom) = FourPi*MBaa_atomic(Iatom)
      MBHFaa_atomic(Iatom) = FourPi*MBHFaa_atomic(Iatom)
!
      MBbb_atomic(Iatom) = FourPi*MBbb_atomic(Iatom)
      MBHFbb_atomic(Iatom) = FourPi*MBHFbb_atomic(Iatom)
!
      write(UNIout,'(i8,1x,4f14.8)')Iatom,MBHFaa_atomic(Iatom),MBHFbb_atomic(Iatom),&
                                          MBaa_atomic(Iatom),MBbb_atomic(Iatom)
!
      MBaa_total = MBaa_total + MBaa_atomic(Iatom)
      MBHFaa_total = MBHFaa_total + MBHFaa_atomic(Iatom)
!
      MBbb_total = MBbb_total + MBbb_atomic(Iatom)
      MBHFbb_total = MBHFbb_total + MBHFbb_atomic(Iatom)
!
      end do ! Iatom
!
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      if(NAIMprint.eq.Natoms)write(UNIout,'(a,1x,4f14.8)')'   Total',MBHFaa_total,MBHFbb_total,&
                                                                     MBaa_total,MBbb_total
!
      deallocate(MBaa_atomic,MBHFaa_atomic)
      deallocate(MBbb_atomic,MBHFbb_atomic)
!
! opposite-spin
      allocate(MBHFab_atomic(Natoms),MBab_atomic(Natoms))
      allocate(MBHFba_atomic(Natoms),MBba_atomic(Natoms))
      MBab_total=ZERO
      MBab_atomic=ZERO
      MBHFab_total=ZERO
      MBHFab_atomic=ZERO
      MBba_total=ZERO
      MBba_atomic=ZERO
      MBHFba_total=ZERO
      MBHFba_atomic=ZERO
!
      write(UNIout,'(/a)')'         Momentum-balance via numerical integration (opposite-spin) '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '                            Atomic contributions             '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '                   Hartree-Fock                 Correlated          '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '  Atom       alpha-beta    beta-alpha    alpha-beta    beta-alpha   '
      write(UNIout,'(a)') '--------------------------------------------------------------------'

      if(NAIMprint.eq.0)then ! If no atoms specified, do them all!
        NAIMprint=Natoms
        do Iatom=1,Natoms
          AIMprint(Iatom)=Iatom
        end do ! Iatom
      end if

      do IAIM=1,NAIMprint
!     do Iatom=1,Natoms
        Iatom=AIMprint(IAIM)
        Znum=CARTESIAN(Iatom)%atomic_number
        if(Znum.le.0)cycle
        Ifound=GRID_loc(Znum)
        NApts_atom=NApoints_atom(Ifound)

        allocate(grid_points(1:NApts_atom))
        allocate(WeightsA(NApts_atom))
!
! Moving grid points on cartesian coordinate grid
        do IApoint=1,NApts_atom
          grid_points(IApoint)%X=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          grid_points(IApoint)%Y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          grid_points(IApoint)%Z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
        end do ! IApoint

        call GET_weights (grid_points, NApts_atom, Iatom, WeightsA)

        MBabatom=ZERO
        MBHFabatom=ZERO
        MBbaatom=ZERO
        MBHFbaatom=ZERO
!

        do IApoint=1,NApts_atom
      !    if(WeightsA(IApoint).le.1.0D-6)cycle ! Makes no difference
          Xpt=grid_points(IApoint)%X
          Ypt=grid_points(IApoint)%Y
          Zpt=grid_points(IApoint)%Z
!
          call GET_MBDos_point (Xpt, Ypt, Zpt, MBabgridA, MBHFabgridA, MBbagridA, MBHFbagridA)
!
          MBabatom = MBabatom + MBabgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
          MBHFabatom = MBHFabatom + MBHFabgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
          MBbaatom = MBbaatom + MBbagridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
          MBHFbaatom = MBHFbaatom + MBHFbagridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
        end do ! IApoint

        MBab_atomic(Iatom) = MBab_atomic(Iatom) + MBabatom
        MBHFab_atomic(Iatom) = MBHFab_atomic(Iatom) + MBHFabatom
!
        MBba_atomic(Iatom) = MBba_atomic(Iatom) + MBbaatom
        MBHFba_atomic(Iatom) = MBHFba_atomic(Iatom) + MBHFbaatom
!
        deallocate(grid_points)
        deallocate(WeightsA)

      end do ! Iatom

!
! Now multiply by 4pi and get total exchange
      do IAIM=1,NAIMprint
        Iatom=AIMprint(IAIM)
!     do Iatom=1,Natoms
      Znum=CARTESIAN(Iatom)%atomic_number
      if(Znum.le.0)cycle
!
      MBab_atomic(Iatom) = FourPi*MBab_atomic(Iatom)
      MBHFab_atomic(Iatom) = FourPi*MBHFab_atomic(Iatom)
!
      MBba_atomic(Iatom) = FourPi*MBba_atomic(Iatom)
      MBHFba_atomic(Iatom) = FourPi*MBHFba_atomic(Iatom)
!
      write(UNIout,'(i8,1x,4f14.8)')Iatom,MBHFab_atomic(Iatom),MBHFba_atomic(Iatom),&
                                          MBab_atomic(Iatom),MBba_atomic(Iatom)
!
      MBab_total = MBab_total + MBab_atomic(Iatom)
      MBHFab_total = MBHFab_total + MBHFab_atomic(Iatom)
!
      MBba_total = MBba_total + MBba_atomic(Iatom)
      MBHFba_total = MBHFba_total + MBHFba_atomic(Iatom)
!
      end do ! Iatom
!
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      if(NAIMprint.eq.Natoms)write(UNIout,'(a,1x,4f14.8)')'   Total',MBHFab_total,MBHFba_total,&
                                                                     MBab_total,MBba_total
!
      deallocate(MBab_atomic,MBHFab_atomic)
      deallocate(MBba_atomic,MBHFba_atomic)
!
! Get analytical value just for sanity check
      call GET_object ('QM', 'MB', 'ANA')
!
      call CPU_time(t2)
      time = t2 - t1
!
! End of routine NI_MBD
      call PRG_manager ('exit', 'NI_MB', 'MB%NI')
      end subroutine NI_MB
      subroutine BLD_TRDM_MB
!*********************************************************************************************
!     Date last modified: April 24, 2019                                                     *
!     Author: JWH                                                                            *
!     Desciption: Build a 2-RDM for the specific purpose of calculating the MB density.      *
!*********************************************************************************************
! Modules:
      USE program_constants
      USE program_timing
      USE program_files
      USE QM_defaults
      USE QM_objects

      implicit none
!
! Local scalars:
      integer :: iAO, jAO, kAO, lAO, ijkl_index, ijlk_index
!
! Begin:
      call PRG_manager ('enter', 'BLD_TRDM_MB', 'UTILITY')
!
      if(allocated(TRDM_MBaa))then
        deallocate(TRDM_MBaa,TRDM_HF_MBaa)
      end if
      if(allocated(TRDM_MBbb))then
        deallocate(TRDM_MBbb,TRDM_HF_MBbb)
      end if
      if(allocated(TRDM_MBab))then
        deallocate(TRDM_MBab,TRDM_HF_MBab)
      end if
      if(allocated(TRDM_MBba))then
        deallocate(TRDM_MBba,TRDM_HF_MBba)
      end if
      allocate(TRDM_MBaa(Nbasis**4),TRDM_HF_MBaa(Nbasis**4))
      allocate(TRDM_MBbb(Nbasis**4),TRDM_HF_MBbb(Nbasis**4))
      allocate(TRDM_MBab(Nbasis**4),TRDM_HF_MBab(Nbasis**4))
      allocate(TRDM_MBba(Nbasis**4),TRDM_HF_MBba(Nbasis**4))
      ijkl_index = 0
      do iAO = 1, Nbasis
        do jAO = 1, Nbasis
          do kAO = 1, Nbasis
            do lAO = 1, Nbasis
              ijkl_index = ijkl_index + 1
              ijlk_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(lAO-1) + kAO
!
              TRDM_MBaa(ijkl_index) = TRDM_AOaa(ijkl_index) - TRDM_AOaa(ijlk_index)
              TRDM_HF_MBaa(ijkl_index) = TRDM_HF_AOaa(ijkl_index) - TRDM_HF_AOaa(ijlk_index)
!
              TRDM_MBbb(ijkl_index) = TRDM_AObb(ijkl_index) - TRDM_AObb(ijlk_index)
              TRDM_HF_MBbb(ijkl_index) = TRDM_HF_AObb(ijkl_index) - TRDM_HF_AObb(ijlk_index)
!
              TRDM_MBab(ijkl_index) = TRDM_AOab(ijkl_index) - TRDM_AOab(ijlk_index)
              TRDM_HF_MBab(ijkl_index) = TRDM_HF_AOab(ijkl_index) - TRDM_HF_AOab(ijlk_index)
!
              TRDM_MBba(ijkl_index) = TRDM_AOba(ijkl_index) - TRDM_AOba(ijlk_index)
              TRDM_HF_MBba(ijkl_index) = TRDM_HF_AOba(ijkl_index) - TRDM_HF_AOba(ijlk_index)
!
            end do 
          end do 
        end do 
      end do 
!
! End of routine NI_MBD
      call PRG_manager ('exit', 'BLD_TRDM_MB', 'UTILITY')
      end subroutine BLD_TRDM_MB
