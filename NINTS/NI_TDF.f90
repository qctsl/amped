      subroutine NI_TDF_Vee_OS
!*********************************************************************************************
!     Date last modified: October 19, 2021                                                   *
!     Author: JWH                                                                            *
!     Desciption: Integrate the two-electron density functional to get the contribution to   *
!                 the correlation energy between opposite-spin electrons. (e-e repulsion)    *
!*********************************************************************************************
! Modules:
      USE program_constants
      USE program_timing
      USE program_files
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE module_grid_points
      USE INT_objects

      implicit none
!
! Local scalars:
      integer :: Iatom,IApoint,IAIM,Znum,Ifound
      double precision :: t0, t1, t2, time
      double precision :: tdf_total, rf, tdf, tdfatom
!
! Local arrays:
      double precision, dimension(:), allocatable :: WeightsA
      double precision, dimension(:), allocatable :: tdf_atomic
      double precision :: Rv(3)
!
! Begin:
      call PRG_manager ('enter', 'NI_TDF_Vee_OS', '2EDFVEEOS%NI')
!
! Get density matrices
      call GET_object ('QM','SPIN_DENSITY_1MATRIX', 'AO')
!
      call GET_object ('CFT', 'R2RDM', 'DELTANO')
!
! Get grid and weights
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
      write(UNIout,'(/2a)')'Radial grid used for numerical integration: ',RADIAL_grid
      call BLD_BeckeW_XI
      write(UNIout,'(2a)')'Weights used for numerical integration:      BECKE'
!
      call CPU_time(t0)
      call CPU_time(t1)      
!
! opposite-spin
      allocate(tdf_atomic(Natoms))
      tdf_total  = ZERO
      tdf_atomic = ZERO
!
      write(UNIout,'(a,F5.3)') 'q_TDF= ',q_TDF
!
      write(UNIout,'(/a)')'    Two-electron density functional integration      '
      write(UNIout,'(a)') '          (electron-electron repulsion)              '
      write(UNIout,'(a)') '-----------------------------------------------------'
      write(UNIout,'(a)') '              Atomic contributions    '
      write(UNIout,'(a)') '-----------------------------------------------------'
      write(UNIout,'(a)') '       Atom                           tdf      '
      write(UNIout,'(a)') '-----------------------------------------------------'
!
      if(NAIMprint.eq.0)then ! If no atoms specified, do them all!
        NAIMprint=Natoms
        do Iatom=1,Natoms
          AIMprint(Iatom)=Iatom
        end do ! Iatom
      end if

      do IAIM=1,NAIMprint
!     do Iatom=1,Natoms
        Iatom=AIMprint(IAIM)
        Znum=CARTESIAN(Iatom)%atomic_number
        if(Znum.le.0)cycle
        Ifound=GRID_loc(Znum)
        NApts_atom=NApoints_atom(Ifound)

        allocate(grid_points(1:NApts_atom))
        allocate(WeightsA(NApts_atom))
!
! Moving grid points on cartesian coordinate grid
        do IApoint=1,NApts_atom
          grid_points(IApoint)%X=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          grid_points(IApoint)%Y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          grid_points(IApoint)%Z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
        end do ! IApoint

        call GET_weights (grid_points, NApts_atom, Iatom, WeightsA)

        tdfatom = ZERO
!
        do IApoint=1,NApts_atom
      !    if(WeightsA(IApoint).le.1.0D-6)cycle ! Makes no difference
! Extracule coordinates
          Rv(1) = grid_points(IApoint)%X
          Rv(2) = grid_points(IApoint)%Y
          Rv(3) = grid_points(IApoint)%Z
!
! Get the value of the renormalization function (Phi)          
          call GET_RF_OS_point(Rv, Nnatorb, rf)
!
! Get value of the two-electron density functional
          call GET_TDF_Vee_OS_point(Rv, rf, Nnatorb, tdf)
!
          tdfatom = tdfatom + tdf*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
        end do ! IApoint
!
        tdf_atomic(Iatom) = tdf_atomic(Iatom) + tdfatom
!
        deallocate(grid_points)
        deallocate(WeightsA)
!
      end do ! Iatom
!
! Now multiply by 4pi and get total exchange
      do IAIM=1,NAIMprint
        Iatom=AIMprint(IAIM)
!     do Iatom=1,Natoms
      Znum=CARTESIAN(Iatom)%atomic_number
      if(Znum.le.0)cycle
!
      tdf_atomic(Iatom) = FourPi*tdf_atomic(Iatom)
!
      write(UNIout,'(4x,i6,23x,f14.8)')Iatom,tdf_atomic(Iatom)
!
      tdf_total = tdf_total + tdf_atomic(Iatom)
!
      end do ! Iatom
!
      write(UNIout,'(a)') '-----------------------------------------------------'
      if(NAIMprint.eq.Natoms)write(UNIout,'(a,19x,f14.8)')'     Total    ',tdf_total
!
      deallocate(tdf_atomic)
!
      call CPU_time(t2)
      time = t2 - t1
!
! End of routine NI_2RDM
      call PRG_manager ('exit', 'NI_TDF_Vee_OS', '2EDFVEEOS%NI')
      end subroutine NI_TDF_Vee_OS
      subroutine NI_OSEC
!*********************************************************************************************
!     Date last modified: October 19, 2021                                                   *
!     Author: JWH                                                                            *
!     Desciption: Integrate the OSEC functional to get the contribution to                   *
!                 the correlation energy between opposite-spin electrons.                    *
!*********************************************************************************************
! Modules:
      USE program_constants
      USE program_timing
      USE program_files
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE module_grid_points
      USE INT_objects

      implicit none
!
! Local scalars:
      integer :: Iatom,IApoint,IAIM,Znum,Ifound
      double precision :: t0, t1, t2, time
      double precision :: tdf_total, tdf, tdfatom
!
! Local arrays:
      double precision, dimension(:), allocatable :: WeightsA
      double precision, dimension(:), allocatable :: tdf_atomic
      double precision :: Rv(3)
!
! Begin:
      call PRG_manager ('enter', 'NI_OSEC', 'OSEC%NI')
!
! Get density matrices
      call GET_object ('QM','SPIN_DENSITY_1MATRIX', 'AO')
!
      call GET_object ('CFT', 'R2RDM', 'DELTANO')
!
! Get grid and weights
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
      write(UNIout,'(/2a)')'Radial grid used for numerical integration: ',RADIAL_grid
      call BLD_BeckeW_XI
      write(UNIout,'(2a)')'Weights used for numerical integration:      BECKE'
!
      call CPU_time(t0)
      call CPU_time(t1)      
!
! opposite-spin
      allocate(tdf_atomic(Natoms))
      tdf_total  = ZERO
      tdf_atomic = ZERO
      write(UNIout,'(a,F5.3)') 'q_TDF= ',q_TDF
!
      write(UNIout,'(/a)')'          OSEC functional integration      '
      write(UNIout,'(a)') '-----------------------------------------------------'
      write(UNIout,'(a)') '              Atomic contributions    '
      write(UNIout,'(a)') '-----------------------------------------------------'
      write(UNIout,'(a)') '       Atom                           tdf      '
      write(UNIout,'(a)') '-----------------------------------------------------'
!
      if(NAIMprint.eq.0)then ! If no atoms specified, do them all!
        NAIMprint=Natoms
        do Iatom=1,Natoms
          AIMprint(Iatom)=Iatom
        end do ! Iatom
      end if

      do IAIM=1,NAIMprint
!     do Iatom=1,Natoms
        Iatom=AIMprint(IAIM)
        Znum=CARTESIAN(Iatom)%atomic_number
        if(Znum.le.0)cycle
        Ifound=GRID_loc(Znum)
        NApts_atom=NApoints_atom(Ifound)

        allocate(grid_points(1:NApts_atom))
        allocate(WeightsA(NApts_atom))
!
! Moving grid points on cartesian coordinate grid
        do IApoint=1,NApts_atom
          grid_points(IApoint)%X=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          grid_points(IApoint)%Y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          grid_points(IApoint)%Z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
        end do ! IApoint

        call GET_weights (grid_points, NApts_atom, Iatom, WeightsA)

        tdfatom = ZERO
!
        do IApoint=1,NApts_atom
      !    if(WeightsA(IApoint).le.1.0D-6)cycle ! Makes no difference
! Extracule coordinates
          Rv(1) = grid_points(IApoint)%X
          Rv(2) = grid_points(IApoint)%Y
          Rv(3) = grid_points(IApoint)%Z
!
! Get value of the two-electron density functional
          call GET_TDFapp_Vee_OS_point(Rv, Nnatorb, tdf)
!
          tdfatom = tdfatom + tdf*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
        end do ! IApoint
!
        tdf_atomic(Iatom) = tdf_atomic(Iatom) + tdfatom
!
        deallocate(grid_points)
        deallocate(WeightsA)
!
      end do ! Iatom
!
! Now multiply by 4pi and get total exchange
      do IAIM=1,NAIMprint
        Iatom=AIMprint(IAIM)
!     do Iatom=1,Natoms
      Znum=CARTESIAN(Iatom)%atomic_number
      if(Znum.le.0)cycle
!
      tdf_atomic(Iatom) = FourPi*tdf_atomic(Iatom)
!
      write(UNIout,'(4x,i6,23x,f14.8)')Iatom,tdf_atomic(Iatom)
!
      tdf_total = tdf_total + tdf_atomic(Iatom)
!
      end do ! Iatom
!
      write(UNIout,'(a)') '-----------------------------------------------------'
      if(NAIMprint.eq.Natoms)write(UNIout,'(a,19x,f14.8)')'     Total    ',tdf_total
!
      deallocate(tdf_atomic)
!
      call CPU_time(t2)
      time = t2 - t1
!
! End of routine NI_2RDM
      call PRG_manager ('exit', 'NI_OSEC', 'OSEC%NI')
      end subroutine NI_OSEC
      subroutine NI_TDF_T_OS
!*********************************************************************************************
!     Date last modified: December 15, 2021                                                  *
!     Author: JWH                                                                            *
!     Desciption: Integrate the two-electron density functional to get the contribution to   *
!                 the correlation energy between opposite-spin electrons. (kinetic)          *
!*********************************************************************************************
! Modules:
      USE program_constants
      USE program_timing
      USE program_files
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE module_grid_points
      USE INT_objects

      implicit none
!
! Local scalars:
      integer :: Iatom,IApoint,IAIM,Znum,Ifound
      double precision :: t0, t1, t2, time
      double precision :: tdf_total, rf, tdf, tdfatom
      double precision :: rfgrad(3), rflap
!
! Local arrays:
      double precision, dimension(:), allocatable :: WeightsA
      double precision, dimension(:), allocatable :: tdf_atomic
      double precision :: Rv(3)
!
! Begin:
      call PRG_manager ('enter', 'NI_TDF_T_OS', '2EDFTOS%NI')
!
! Get density matrices
      call GET_object ('QM','SPIN_DENSITY_1MATRIX', 'AO')
!
      call GET_object ('CFT', 'R2RDM', 'DELTANO')
!
! Get grid and weights
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
      write(UNIout,'(/2a)')'Radial grid used for numerical integration: ',RADIAL_grid
      call BLD_BeckeW_XI
      write(UNIout,'(2a)')'Weights used for numerical integration:      BECKE'
!
      call CPU_time(t0)
      call CPU_time(t1)      
!
! opposite-spin
      allocate(tdf_atomic(Natoms))
      tdf_total  = ZERO
      tdf_atomic = ZERO
!
      write(UNIout,'(a,F5.3)') 'q_TDF= ',q_TDF
!
      write(UNIout,'(/a)')'    Two-electron density functional integration      '
      write(UNIout,'(a)') '                   (kinetic)                         '
      write(UNIout,'(a)') '-----------------------------------------------------'
      write(UNIout,'(a)') '              Atomic contributions    '
      write(UNIout,'(a)') '-----------------------------------------------------'
      write(UNIout,'(a)') '       Atom                           tdf      '
      write(UNIout,'(a)') '-----------------------------------------------------'
!
      if(NAIMprint.eq.0)then ! If no atoms specified, do them all!
        NAIMprint=Natoms
        do Iatom=1,Natoms
          AIMprint(Iatom)=Iatom
        end do ! Iatom
      end if

      do IAIM=1,NAIMprint
!     do Iatom=1,Natoms
        Iatom=AIMprint(IAIM)
        Znum=CARTESIAN(Iatom)%atomic_number
        if(Znum.le.0)cycle
        Ifound=GRID_loc(Znum)
        NApts_atom=NApoints_atom(Ifound)

        allocate(grid_points(1:NApts_atom))
        allocate(WeightsA(NApts_atom))
!
! Moving grid points on cartesian coordinate grid
        do IApoint=1,NApts_atom
          grid_points(IApoint)%X=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          grid_points(IApoint)%Y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          grid_points(IApoint)%Z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
        end do ! IApoint

        call GET_weights (grid_points, NApts_atom, Iatom, WeightsA)

        tdfatom = ZERO
!
        do IApoint=1,NApts_atom
      !    if(WeightsA(IApoint).le.1.0D-6)cycle ! Makes no difference
! Extracule coordinates
          Rv(1) = grid_points(IApoint)%X
          Rv(2) = grid_points(IApoint)%Y
          Rv(3) = grid_points(IApoint)%Z
!
! Get the value of the renormalization function (Phi), it's gradient and laplacian
          !call GET_RF_VGL_OS_point(Rv, Nnatorb, rf, rfgrad, rflap)
          rf = zero
          rfgrad = zero
          rflap = zero
!
! Get value of the two-electron density kinetic energy functional
          call GET_TDF_T_OS_point(Rv, rf, rfgrad, rflap, Nnatorb, tdf)
! Use 1-rdm version for testing
!          call GET_1eDF_T_point(Rv, Nnatorb, tdf)
!
          tdfatom = tdfatom + tdf*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
        end do ! IApoint
!
        tdf_atomic(Iatom) = tdf_atomic(Iatom) + tdfatom
!
        deallocate(grid_points)
        deallocate(WeightsA)
!
      end do ! Iatom
!
! Now multiply by 4pi and get total exchange
      do IAIM=1,NAIMprint
        Iatom=AIMprint(IAIM)
!     do Iatom=1,Natoms
      Znum=CARTESIAN(Iatom)%atomic_number
      if(Znum.le.0)cycle
!
      tdf_atomic(Iatom) = FourPi*tdf_atomic(Iatom)
!
      write(UNIout,'(4x,i6,23x,f14.8)')Iatom,tdf_atomic(Iatom)
!
      tdf_total = tdf_total + tdf_atomic(Iatom)
!
      end do ! Iatom
!
      write(UNIout,'(a)') '-----------------------------------------------------'
      if(NAIMprint.eq.Natoms)write(UNIout,'(a,19x,f14.8)')'     Total    ',tdf_total
!
      deallocate(tdf_atomic)
!
      call CPU_time(t2)
      time = t2 - t1
!
! End of routine NI_2RDM
      call PRG_manager ('exit', 'NI_TDF_T_OS', '2EDFTOS%NI')
      end subroutine NI_TDF_T_OS
