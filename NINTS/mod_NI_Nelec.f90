      module NI_Nelectrons
!**************************************************************************************
!     Date last modified: Sept 26th ,2001                                             *
!     Author: Aisha                                                                   *
!     Desciption:                                                                     *
!**************************************************************************************
! Modules:

      implicit none

! Output scalars:
      double precision :: num_electrons_tot  ! Charge density
      double precision :: error_ele ! Error in charge density

      double precision, dimension(:), allocatable :: num_electrons ! Total number of electrons
      end module NI_Nelectrons
