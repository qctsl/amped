     subroutine Numerical_Integration
!**************************************************************************************
!     Date last modified: Sept 26th ,2001                                             *
!     Author: Aisha                                                                   *
!     Desciption:                                                                     *
!**************************************************************************************
! Modules:
      USE program_files
      USE QM_defaults
      USE NI_defaults
      USE N_integration
      USE type_molecule
      USE type_Weights

      implicit none

! Local scalars:
      integer :: Iatom,IApoint,Ifound,NApoints_tot,Nbasis,MATlen,Z_num
      double precision :: t0,t1,t2,time
!
! Begin:
      call CPU_time(t0)
      call CPU_time(t1)      

      call GET_object ('QM', 'ENERGY_COMPONENTS', Wavefunction)
      call GET_object ('QM', 'DIPOLE_MOMENT', Wavefunction)
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)

      write(UNIout,'(2a)')'Radial grid used for numerical integration: ',RADIAL_grid
      write(UNIout,'(2a)')'Weights used for numerical integration:     ',DEN_partitioning
      call flush(6)
      if(DEN_partitioning(1:5).eq.'BECKE')then
        select case(XI_type)
          case(0)
          write(UNIout,'(a)')'XI has been set to the identity matrix '
          case(1)
          write(UNIout,'(a)')'The Bragg-Slater radius of H has been set to 0.35'
          case(2)
          write(UNIout,'(a)')'The Bragg-Slater radius of H has been set to 0.25'
        end select
      end if
      write(UNIout,'(50("*"))')
      call flush(6)

      call INI_NI_data

      do Iatom=1,Natoms
        Z_num = CARTESIAN(Iatom)%Atomic_number
        if(Z_num.le.0)cycle
        Ifound=GRID_loc(Z_num)
        NApts_atom=NApoints_atom(Ifound)
        NApoints_tot = NApoints_tot + NApts_atom
        allocate(grid_points(1:NApts_atom))
        invariant = .false.
        if(invariant) then
          call  Rotation_invariance (NApts_atom)
        end if !invariant

        do IApoint=1,NApts_atom
          grid_points(IApoint)%X=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          grid_points(IApoint)%Y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          grid_points(IApoint)%Z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
          grid_points(IApoint)%w=Egridpts(Ifound,IApoint)%w
        end do ! IApoint

        allocate(charge(NApts_atom))
        if(NI_property(1:2).ne.'EX'.and.NI_property(1:2).ne.'RA')then
        call GET_density (grid_points, NApts_atom, charge)
        end if

! Now compute the Becke weights for all the grid points for Iatom
        allocate(Bweights(NApts_atom))
        call GET_weights (grid_points, NApts_atom, Iatom, Bweights)

        call method_specification (Iatom, Ifound)

        deallocate(Bweights)
        deallocate(charge)
        deallocate(grid_points)

      end do ! Iatom

      call PRT_NI_results
      if(NI_Atomic)call PRT_NI_Atomic
      if(MUN_prtlev.gt.0)write(UNIout,'(a,i8/)')'Total number of grid points: ',NApoints_tot

      call CPU_time(t2)
      time = t2 - t1

      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine INI_NI_data
!************************************************************************************
!     Date last modified July 27, 2010                                              *
!     Author: R.A. Poirier                                                          *
!     Description: Initial the data for numerical integration                       *
!************************************************************************************
! Modules:

      implicit none

! Input scalars:
!
! Begin:
      NApoints_tot = 0

      select case(NI_property)
        case('NUMELECTRONS')
          call INI_NI_Nelectrons
        case ('EXCHANGE')
          call INI_NI_Exchange
        case('POTENTIALENERGY')
          call INI_NI_Vne
        case('COULOMBENERGY')
          call INI_NI_Vee
        case('COUL_DFT')
          call INI_NI_Vee_DFT
          call INI_NI_Vee_NN
        case('DIPOLEMO')
          call INI_NI_dipole
        case('SECONDMO') ! Need AIM number of electons and dipole
          call INI_NI_Nelectrons
          call INI_NI_dipole
          call INI_NI_2ndMoment
        case('BONDS')
          call INI_NI_Nelec_AB
        case('RADIAL')
          call INI_NI_Radial
        case('ALL') ! Except 2nd moment
          call INI_NI_Nelectrons
          call INI_NI_Exchange
          call INI_NI_Vne
          call INI_NI_Vee_DFT
          call INI_NI_Vee
          call INI_NI_Vee_NN
          call INI_NI_dipole
        case default
          write(UNIout,'(2a)') 'NI_property = ' , NI_property
          write(UNIout,'(a)')'ERROR> INI_NI_data: illegal property'
          stop 'ERROR> INI_NI_data: illegal property'
      end select

      return
      end subroutine INI_NI_data
! END CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine Numerical_Integration
      subroutine method_specification (Iatom, Ifound)
!************************************************************************************
!     Date last modified Jan. 18th ,2005                                            *
!     Author: Aisha ElSherbiny and R. Poirier                                       *
!     Description: Call the appropriate routine                                     *
!************************************************************************************
! Modules:
      USE program_files
      USE NI_defaults
      USE module_grid_points

      implicit none

! Input scalars:
      integer :: Iatom,Ifound

!Begin:
        select case (NI_property)

        case('NUMELECTRONS')
          call number_electrons (Iatom, Ifound)

        case ('EXCHANGE')
          call GET_NI_exchange (Iatom, Ifound)

        case('POTENTIALENERGY')
          call potential_energy (Iatom,Ifound)

        case('DIPOLEMO')
        call total_dipole (Iatom, Ifound)

        case('RADIAL')
        call radial_atomic (Iatom, Ifound)

        case('SECONDMO')
        call number_electrons (Iatom, Ifound)
        call total_2ndMoment (Iatom, Ifound)

        case('BONDS')
        call NI_Nelec_AB (Iatom, Ifound)

        case('COULOMBENERGY')
            call coulomb_energy (Iatom,Ifound)

        case('COULOMBNN')
            call coulomb_energy_NN (Iatom,Ifound)

        case('COUL_DFT')
          call coulomb_energy_DFT (Iatom,Ifound)
          call coulomb_energy_NN (Iatom, Ifound)

        case('ALL')
!       else ! Except 2nd moment
          call number_electrons(Iatom,Ifound)
          call potential_energy(Iatom,Ifound)
          call total_dipole (Iatom, Ifound)              
          call coulomb_energy_DFT (Iatom, Ifound)
          call coulomb_energy_NN (Iatom, Ifound)
          call coulomb_energy (Iatom, Ifound)

        case default
          write(UNIout,'(2a)') 'NI_property = ' , NI_property
          write(UNIout,'(a)')'ERROR> Numerical integration: illegal property'
          stop 'ERROR> Numerical integration: illegal property'
        end select

      return 
      end subroutine method_specification
      subroutine NI_Atomic_Property (Iatom, Ifound, Property_val, Natoms, Pcharge)
!************************************************************************************
!     Date last modified Sept 26th ,2001                                            *
!     Author: Aisha                                                                 *
!     Description:                                                                  *
!************************************************************************************
! Modules:
      USE program_constants
      USE module_grid_points

      implicit none

! Input scalars:
      integer :: Iatom,Ifound,Natoms
      double precision :: Pcharge(NApts_atom)
!
! Local scalars:
      integer :: IApoint
      double precision :: Property_val(Natoms)

! Begin: 
      do IApoint = 1,NApts_atom
        Property_val(Iatom)=Property_val(Iatom)+Pcharge(IApoint)
      end do ! IApoint

! Multiply by 4pi
      Property_val(Iatom)=FourPi*Property_val(Iatom)

      return
      end subroutine NI_Atomic_Property
      subroutine PRT_NI_results
!***********************************************************************
!     Date last modified: Nov. 29, 2007                                *
!     Author: R.A. Poirier                                             *
!     Description: Print all the results                               *
!***********************************************************************
! Modules:
      USE program_files
      USE NI_defaults

      implicit none

! Local scalars:

      if(NI_property(1:2).ne.'EX'.and.NI_property(1:2).ne.'SE'.and.NI_property(1:2).ne.'RA')then
        write(UNIout,'(/a)')'Summary of numerical integration results:'
        write(UNIout,'(86("-"))')
        write(UNIout,'(a)')'Property      NIntegration     Actual           Accuracy           Error'
        write(UNIout,'(86("-"))')
      end if

      select case(NI_property)
        case('NUMELECTRONS')
          call PRT_NI_Nelectrons

        case ('EXCHANGE')
          call PRT_NI_exchange

        case('POTENTIALENERGY')
          call PRT_NI_potential

        case('DIPOLEMO')
          call PRT_NI_Dipole

        case('COULOMBENERGY','COUL_DFT')
          call PRT_NI_Coulomb

        case('BONDS')
          call PRT_NI_Nelec_AB
          call PRT_NI_Nelec_AB_AIM

        case ('RADIAL')
          call PRT_NI_radial_AIM

        case('ALL')
          call PRT_NI_Nelectrons
          call PRT_NI_potential
          call PRT_NI_dipole
          call PRT_NI_Coulomb
      end select

      if(NI_property(1:2).ne.'EX')then
        write(UNIout,'(86("-"))')
      end if

      return
      end subroutine PRT_NI_results
      subroutine PRT_NI_atomic
!***********************************************************************
!     Date last modified: Nov. 29, 2007                                *
!     Author: R.A. Poirier                                             *
!     Description: Print all the results for each atom.                *
!***********************************************************************
! Modules:
      USE program_files
      USE NI_defaults

      implicit none
!
! Begin:
      select case(NI_property)
        case('NUMELECTRONS')
          call PRT_NI_Nelectrons_AIM

        case('SECONDMO')
          call PRT_NI_2ndMoment_AIM

        case ('EXCHANGE')
          write(UNIout,'(a)')'Exchange not available by atom:'

        case('POTENTIALENERGY')
          call PRT_NI_potential_AIM

        case('DIPOLEMO')
          call PRT_NI_dipole_AIM

        case('COULOMBENERGY','COUL_DFT')
          call PRT_NI_Coulomb_AIM

        case('ALL')
          call PRT_NI_Nelectrons_AIM
          call PRT_NI_potential_AIM
          call PRT_NI_dipole_AIM
          call PRT_NI_Coulomb_AIM
!         call PRT_NI_2ndMoment_AIM ! Causes a segmentation fault
      end select

      return
      end subroutine PRT_NI_atomic
