      subroutine NI_Nelec_AB (Iatom, Ifound)
!************************************************************************************
!     Date last modified December 13, 2013                                          *
!     Author: R.A. Poirier                                                          *
!     Description: Initialization for the dipole moment (first)                     *
!************************************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE N_integration
      USE type_NI_Nelec_AB

      implicit none

! Input scalars:
      integer :: Iatom,Ifound
!
! Local scalars:
      integer :: IApoint,Jatom
      double precision :: WAB
      double precision, dimension(:), allocatable :: Pcharge
      double precision, dimension(:), allocatable :: BweightsA

! Begin: 
      allocate (Pcharge(NApts_atom))
      allocate(BweightsA(NApts_atom))
!
! Save the weights for Iatom
      BweightsA=Bweights
      do Jatom=1,Natoms
        if(CARTESIAN(Jatom)%Atomic_number.le.0)cycle
        if(Iatom.eq.Jatom)then
        do IApoint=1,NApts_atom
          Pcharge(IApoint)=charge(IApoint)*BweightsA(IApoint)*grid_points(IApoint)%w
        end do
        else   ! A-B
        deallocate(Bweights)
        allocate(Bweights(NApts_atom))
        call GET_weights (grid_points, NApts_atom, Jatom, Bweights)
        do IApoint=1,NApts_atom
          WAB=dsqrt(Bweights(IApoint)*BweightsA(IApoint))
          Pcharge(IApoint)=WAB*charge(IApoint)*grid_points(IApoint)%w
        end do
        end if

        do IApoint=1,NApts_atom
          N_elec_AB(Iatom,Jatom)=N_elec_AB(Iatom,Jatom)+Pcharge(IApoint)
          if(Iatom.ne.Jatom)N_elec_A_AB(Iatom,Jatom)=N_elec_A_AB(Iatom,Jatom)+BweightsA(IApoint)*Pcharge(IApoint)
        end do ! IApoint

          N_elec_AB(Iatom,Jatom)=FourPi*N_elec_AB(Iatom,Jatom)
          N_elec_A_AB(Iatom,Jatom)=FourPi*N_elec_A_AB(Iatom,Jatom)
      end do ! Jatom

      N_elec_tot = N_elec_tot + N_elec_AB(Iatom,Iatom)

      deallocate(Pcharge)
      deallocate(BweightsA)
      
      return
      end subroutine NI_Nelec_AB
      subroutine PRT_NI_Nelec_AB
!***********************************************************************
!     Date last modified December 2013                                 *
!     Author: R.A. Poirier                                             *
!     Description: Print the number of electrons                       *
!***********************************************************************
! Modules:
      USE program_files
      USE type_molecule
      USE type_NI_Nelec_AB

      implicit none

! Local scalars:  
      double precision :: Z_tot
      double precision :: accuracy_ele  ! Accuracy in num _ele
      double precision :: error_ele  ! Error in num _ele
!
! Begin:
      Z_tot=dble(Nelectrons)
      error_ele = N_elec_tot - Z_tot
      accuracy_ele = - DLOG10(DABS((N_elec_tot/Z_tot)-1))
      write(UNIout,'(a,f14.8,f10.1,f20.3,1PE20.2)')'N electrons',N_elec_tot,Z_tot,accuracy_ele,error_ele

      return
      end subroutine PRT_NI_Nelec_AB
      subroutine PRT_NI_Nelec_AB_AIM
!***********************************************************************
!     Date last modified December 2013                                 *
!     Author: R.A. Poirier                                             *
!     Description: Print the number of electrons                       *
!***********************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE matrix_print
      USE type_molecule
      USE type_NI_Nelec_AB

      implicit none

! Local scalars:  
      integer :: Iatom,Jatom
      double precision :: sumAB
!
! Begin:
      write(UNIout,'(a)')'Number of electrons associated with each bond:'
      call PRT_matrix (N_elec_AB, Natoms, Natoms)
      write(UNIout,'(a)')'Number of electrons contributed by each atom to each bond:'
      call PRT_matrix (N_elec_A_AB, Natoms, Natoms)
      write(UNIout,'(a)')'Number of electrons associated with each bond (averaged):'
      write(UNIout,'(a)')' AtomA    AtomB    Number of electrons:'
      do Iatom=1,Natoms
      do Jatom=1,Iatom
        if(Iatom.eq.Jatom)cycle
        sumAB=(N_elec_AB(Iatom,Jatom)+N_elec_AB(Jatom,Iatom))/TWO
      write(UNIout,'(i5,3x,i5,5x,F12.6)')Iatom,Jatom,sumAB
      end do
      end do
!
      return
      end subroutine PRT_NI_Nelec_AB_AIM
      subroutine INI_NI_Nelec_AB
!************************************************************************************
!     Date last modified December 10, 2013                                          *
!     Author: R.A. Poirier                                                          *
!     Description: Intialization for the number of electrons                        *
!************************************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE type_NI_Nelec_AB

      implicit none

! Input scalars:

      allocate(N_elec_AB(Natoms,Natoms))
      allocate(N_elec_A_AB(Natoms,Natoms))
      N_elec_tot=ZERO
      N_elec_AB(1:Natoms,1:Natoms)=ZERO
      N_elec_A_AB(1:Natoms,1:Natoms)=ZERO

      return
      end subroutine INI_NI_Nelec_AB
      subroutine NI_BONDS
!**************************************************************************************
!     Date last modified: March 2, 2015                                               *
!     Author: R.A. Poirier                                                            *
!     Desciption:                                                                     *
!**************************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE NI_defaults
      USE N_integration
      USE type_molecule
      USE type_NI_Nelec_AB
      USE type_Weights

      implicit none

! Local scalars:
      integer :: Aatom,Batom,IApoint,IfoundA,IfoundB,NApts_A,NApts_B,NApoints_tot,Tpoints,Z_num
      double precision :: t0,t1,t2,time
      double precision :: Nelec_AB,Nelec_AB_A,Nelec_AB_B,Nelec_tot,Bond_r,Bond_r2
      double precision :: DipoleX_AB,DipoleY_AB,DipoleZ_AB
      double precision, dimension(:), allocatable :: BweightsA,BweightsB
      type(type_grid_points),dimension(:),allocatable :: grid_A,grid_B
      double precision :: SizeX,SizeY,SizeZ,AVolume
      double precision :: A2ndMomXX,A2ndMomYY,A2ndMomZZ,A2ndMomXY,A2ndMomXZ,A2ndMomYZ
!
! Begin:
      call PRG_manager ('enter', 'NI_BONDS', 'DENSITY_BOND%NUMERICAL')
      call CPU_time(t0)
      call CPU_time(t1)      

      call GET_object ('QM', 'CMO', Wavefunction)
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)

      write(UNIout,'(2a)')'Radial grid used for numerical integration: ',RADIAL_grid
      write(UNIout,'(2a)')'Weights used for numerical integration:     ',DEN_partitioning
      if(DEN_partitioning(1:5).eq.'BECKE')then
        select case(XI_type)
          case(0)
          write(UNIout,'(a)')'XI has been set to the identity matrix '
          case(1)
          write(UNIout,'(a)')'The Bragg-Slater radius of H has been set to 0.35'
          case(2)
          write(UNIout,'(a)')'The Bragg-Slater radius of H has been set to 0.25'
        end select
      end if
      write(UNIout,'(50("*"))')

      NApoints_tot=0

      Aatom=Bond_atoms(1)
      Batom=Bond_atoms(2)
      if(Aatom.le.0.or.Batom.le.0)then
        write(UNIout,'(a,2i6)')'ERROR NI_BONDS> Illegal atoms:',Aatom,Batom
        stop 'ERROR NI_BONDS> Illegal atoms'
      end if
      Nelec_AB=ZERO
      Nelec_AB_A=ZERO
      Nelec_AB_B=ZERO
      Bond_r=ZERO
      Bond_r2=ZERO
      DipoleX_AB=ZERO
      DipoleY_AB=ZERO
      DipoleZ_AB=ZERO
      A2ndMomXX=ZERO
      A2ndMomYY=ZERO
      A2ndMomZZ=ZERO
      A2ndMomXY=ZERO
      A2ndMomXZ=ZERO
      A2ndMomYZ=ZERO
      Z_num=CARTESIAN(Aatom)%Atomic_number
      if(Z_num.le.0)then
        write(UNIout,'(a,i6)')'ERROR NI_BONDS> Illegal atom:',Z_num
        stop 'ERROR NI_BONDS> Illegal atom'
      end if
      IfoundA=GRID_loc(Z_num)
      NApts_A=NApoints_atom(IfoundA)
      NApoints_tot=NApoints_tot+NApts_A
      allocate(grid_A(1:NApts_A))
      Z_num=CARTESIAN(Batom)%Atomic_number
      if(Z_num.le.0)then
        write(UNIout,'(a,i6)')'ERROR NI_BONDS> Illegal atom:',Z_num
        stop 'ERROR NI_BONDS> Illegal atom'
      end if
      IfoundB=GRID_loc(Z_num)
      NApts_B=NApoints_atom(IfoundB)
      NApoints_tot=NApoints_tot+NApts_B
      allocate(grid_B(1:NApts_B))

      allocate(grid_points(1:NApoints_tot))
      Tpoints=0
      do IApoint=1,NApts_A
        Tpoints=Tpoints+1
        grid_points(Tpoints)%X=Egridpts(IfoundA,IApoint)%X+CARTESIAN(Aatom)%X
        grid_points(Tpoints)%Y=Egridpts(IfoundA,IApoint)%Y+CARTESIAN(Aatom)%Y
        grid_points(Tpoints)%Z=Egridpts(IfoundA,IApoint)%Z+CARTESIAN(Aatom)%Z
        grid_points(Tpoints)%w=Egridpts(IfoundA,IApoint)%w
      end do ! IApoint
      do IApoint=1,NApts_B
        Tpoints=Tpoints+1
        grid_points(Tpoints)%X=Egridpts(IfoundB,IApoint)%X+CARTESIAN(Batom)%X
        grid_points(Tpoints)%Y=Egridpts(IfoundB,IApoint)%Y+CARTESIAN(Batom)%Y
        grid_points(Tpoints)%Z=Egridpts(IfoundB,IApoint)%Z+CARTESIAN(Batom)%Z
        grid_points(Tpoints)%w=Egridpts(IfoundB,IApoint)%w
      end do ! IApoint

      allocate(charge(NApoints_tot))
      call GET_density (grid_points, NApoints_tot, charge)

! Now compute the Becke weights for all the grid points for Aatom
      allocate(BweightsA(NApoints_tot))
      call GET_weights (grid_points, NApoints_tot, Aatom, BweightsA)
      allocate(BweightsB(NApoints_tot))
      call GET_weights (grid_points, NApoints_tot, Batom, BweightsB)

      write(UNIout,'(a,i6,a,i6)')'Properties associated with bond (averaged): Aatom= ',Aatom,' Batom= ',Batom
      write(UNIout,'(a2,a,a2,a)')trim(CARTESIAN(Aatom)%element),'-',trim(CARTESIAN(Batom)%element),' Bond'
      call NI_BOND_AB (Aatom, Batom)

      write(UNIout,'(a,f12.6,a)')'Number of electrons: ',Nelec_AB,' =int sqrt(WA(r)*WB(r))rho(r)dr = int rho^AB(r)dr'
      write(UNIout,'(a,9x,f12.6,a)')'From Aatom: ',Nelec_AB_A,' =int WA(r)rho^AB(r)dr'
      write(UNIout,'(a,9x,f12.6,a)')'From Batom: ',Nelec_AB_B,' =int WB(r)rho^AB(r)dr'
      Nelec_tot=Nelec_AB_A+Nelec_AB_B
      write(UNIout,'(a,f12.6)')'Sum:                 ',Nelec_tot
      write(UNIout,'(a,3f12.6)')'Properties calculated at origin: ',Bond_origin 
      write(UNIout,'(a,11x,F12.6)')'  <r>   : ',Bond_r
      write(UNIout,'(a,11x,F12.6)')'  <r2>  : ',Bond_r2
      write(UNIout,'(a,11x,F12.6)')'Dipole X: ',DipoleX_AB
      write(UNIout,'(a,11x,F12.6)')'Dipole Y: ',DipoleY_AB
      write(UNIout,'(a,11x,F12.6)')'Dipole Z: ',DipoleZ_AB
      write(UNIout,'(a,10x,f16.6)')'SizeX: ',SizeX
      write(UNIout,'(a,10x,f16.6)')'SizeY: ',SizeY
      write(UNIout,'(a,10x,f16.6)')'SizeZ: ',SizeZ
      write(UNIout,'(a,9x,f16.6)')'Volume: ',AVolume

      if(MUN_prtlev.gt.0)write(UNIout,'(a,i8/)')'Total number of grid points: ',NApoints_tot

      deallocate(charge)
      deallocate(grid_points)
      deallocate(grid_A)
      deallocate(grid_B)

      call CPU_time(t2)
      time = t2 - t1

      call PRG_manager ('exit', 'NI_BONDS', 'DENSITY_BOND%NUMERICAL')
      return
CONTAINS ! -------------------------------------------------------------------------
      subroutine NI_BOND_AB (Aatom, Batom)
!************************************************************************************
!     Date last modified December 13, 2013                                          *
!     Author: R.A. Poirier                                                          *
!     Description: Initialization for the dipole moment (first)                     *
!************************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE matrix_print
      USE type_molecule
      USE N_integration
      USE type_NI_Nelec_AB

      implicit none

! Input scalars:
      integer :: Aatom,Batom
!
! Local scalars:
      integer :: IApoint
      double precision :: WAB
      double precision, dimension(:), allocatable :: PchargeAB
      double precision :: XApt,YApt,ZApt,Rval,Rval2
      double precision :: Hess(3,3),EigVector(3,3),Eigvalue(3)

! Begin: 
      allocate (PchargeAB(NApoints_tot))
! Save the weights for Aatom
      do IApoint=1,NApoints_tot
        WAB=dsqrt(BweightsA(IApoint)*BweightsB(IApoint))
        PchargeAB(IApoint)=WAB*charge(IApoint)*grid_points(IApoint)%w
        Nelec_AB_A=Nelec_AB_A+BweightsA(IApoint)*PchargeAB(IApoint)
        Nelec_AB_B=Nelec_AB_B+BweightsB(IApoint)*PchargeAB(IApoint)
        Nelec_AB=Nelec_AB+PchargeAB(IApoint)
        XApt=(grid_points(IApoint)%X-Bond_origin(1))
        YApt=(grid_points(IApoint)%Y-Bond_origin(2))
        ZApt=(grid_points(IApoint)%Z-Bond_origin(3))
        DipoleX_AB=DipoleX_AB+XApt*PchargeAB(IApoint)
        DipoleY_AB=DipoleY_AB+YApt*PchargeAB(IApoint)
        DipoleZ_AB=DipoleZ_AB+ZApt*PchargeAB(IApoint)
        A2ndMomXX=A2ndMomXX+XApt*XApt*PchargeAB(IApoint)
        A2ndMomYY=A2ndMomYY+YApt*YApt*PchargeAB(IApoint)
        A2ndMomZZ=A2ndMomZZ+ZApt*ZApt*PchargeAB(IApoint)
        A2ndMomXY=A2ndMomXY+XApt*YApt*PchargeAB(IApoint)
        A2ndMomXZ=A2ndMomXZ+XApt*ZApt*PchargeAB(IApoint)
        A2ndMomYZ=A2ndMomYZ+YApt*ZApt*PchargeAB(IApoint)

        Rval2=XApt**2+YApt**2+ZApt**2
        Rval=dsqrt(Rval2)
        Bond_r=Bond_r+Rval*PchargeAB(IApoint)
        Bond_r2=Bond_r2+Rval2*PchargeAB(IApoint)
      end do ! IApoint

      DipoleX_AB=-FourPi*DipoleX_AB
      DipoleY_AB=-FourPi*DipoleY_AB
      DipoleZ_AB=-FourPi*DipoleZ_AB
      A2ndMomXX=-FourPi*A2ndMomXX
      A2ndMomYY=-FourPi*A2ndMomYY
      A2ndMomZZ=-FourPi*A2ndMomZZ
      A2ndMomXY=-FourPi*A2ndMomXY
      A2ndMomXZ=-FourPi*A2ndMomXZ
      A2ndMomYZ=-FourPi*A2ndMomYZ
      Bond_r=FourPi*Bond_r
      Bond_r2=FourPi*Bond_r2
      Nelec_AB=FourPi*Nelec_AB
      Nelec_AB_A=FourPi*Nelec_AB_A
      Nelec_AB_B=FourPi*Nelec_AB_B

      Hess(1,1)=-A2ndMomXX
      Hess(1,2)=-A2ndMomXY
      Hess(1,3)=-A2ndMomXZ
      Hess(2,1)=Hess(1,2)
      Hess(2,2)=-A2ndMomYY
      Hess(2,3)=-A2ndMomYZ
      Hess(3,1)=Hess(1,3)
      Hess(3,2)=Hess(2,3)
      Hess(3,3)=-A2ndMomZZ

      write(UNIout,'(a)')'Second Moment Tensor:'
      call PRT_matrix(Hess, 3, 3)
      call MATRIX_diagonalize (Hess, EigVector, EigValue, 3, 2, .false.)
      SizeX=dsqrt(dabs(EigValue(1)))
      SizeY=dsqrt(dabs(EigValue(2)))
      SizeZ=dsqrt(dabs(EigValue(3)))
      AVolume=(FOUR*PI_val/THREE)*(SizeX*SizeY*SizeZ)

      deallocate(PchargeAB)
      deallocate(BweightsA)

      return
      end subroutine NI_BOND_AB
! END CONTAINS ---------------------------------------------------------------------
      end subroutine NI_BONDS
      subroutine PRT_NI_BONDS
!***********************************************************************
!     Date last modified December 2013                                 *
!     Author: R.A. Poirier                                             *
!     Description: Print the number of electrons                       *
!***********************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE matrix_print
      USE type_molecule
      USE type_NI_Nelec_AB

      implicit none

! Local scalars:  
      integer :: Iatom,Jatom
      double precision :: sumAB
!
! Begin:
      write(UNIout,'(a)')'Number of electrons associated with each bond:'
      call PRT_matrix (N_elec_AB, Natoms, Natoms)
      write(UNIout,'(a)')'Number of electrons contributed by each atom to each bond:'
      call PRT_matrix (N_elec_A_AB, Natoms, Natoms)
      write(UNIout,'(a)')'Number of electrons associated with each bond (averaged):'
      write(UNIout,'(a)')' AtomA    AtomB    Number of electrons:'
      do Iatom=1,Natoms
      do Jatom=1,Iatom
        if(Iatom.eq.Jatom)cycle
        sumAB=(N_elec_AB(Iatom,Jatom)+N_elec_AB(Jatom,Iatom))/TWO
      write(UNIout,'(i5,3x,i5,5x,F12.6)')Iatom,Jatom,sumAB
      end do
      end do
!
! Now deallocate:
      deallocate(N_elec_AB)
      deallocate(N_elec_A_AB)

      return
      end subroutine PRT_NI_BONDS
