      module NI_radial
!**************************************************************************************
!     Date last modified: Sept 26th ,2001                                             *
!     Author: R. Poirier                                                              *
!     Desciption:                                                                     *
!**************************************************************************************
! Modules:

      implicit none

! Output scalars:
      double precision, allocatable :: radial_r(:,:) ! radial <r> for all AIM (Natoms by NMO)
      double precision, allocatable :: radial_r2(:,:) ! radial <r> for all AIM (Natoms by NMO)
      double precision, allocatable :: radial_Atomic_r(:) ! radial <r> for all AIM (Natoms)
      double precision, allocatable :: radial_Atomic_r2(:) ! radial <r> for all AIM (Natoms)
      double precision, allocatable :: radial_r_total(:) ! Total  <r> 
      double precision, allocatable :: radial_r2_total(:) ! Total  <r> 

      end module NI_radial
