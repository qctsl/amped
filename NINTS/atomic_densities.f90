      subroutine GET_density (Angular_points, NApts_tot, charge)
!******************************************************************
!   Date last modified: Oct 3rd, 2001                             *
!   Author: Aisha                                                 *
!   Description: Get the electron density at Angular_points       *
!******************************************************************
! Modules:
      USE QM_defaults
      USE QM_objects
      USE type_basis_set
      USE module_grid_points

      implicit none
!
! Input scalars:
      integer   :: NApts_tot
      type(type_grid_points),dimension(NApts_tot) :: Angular_points
      double precision, dimension(NApts_tot) :: charge
!
! Local scalars:
      integer   :: IApoint
      double precision Traclo
      double precision, dimension(:), allocatable :: AOprod

!begin:

      allocate (AOprod(1:MATlen))

      charge(1:NApts_tot) = 0.0D0
      do IApoint = 1,NApts_tot
      call AO_products (Angular_points(IApoint)%X, Angular_points(IApoint)%Y, Angular_points(IApoint)%Z, AOprod, MATlen)
      charge(IApoint) = traclo (PM0, AOprod, NBasis, MATlen )
      end do

      deallocate (AOprod)
!
      end subroutine GET_density
      subroutine GET_density_ab (Angular_points, NApts_tot, charge, AMO, BMO)
!******************************************************************
!   Date last modified: Oct 3rd, 2001                             *
!   Author: Aisha                                                 *
!   Description: Get |Psi_A(r)*Psi_B(r)| for all Angular_points   *
!******************************************************************
! Modules:
      USE program_constants
      USE QM_defaults
      USE QM_objects
      USE type_basis_set
      USE module_grid_points

      implicit none
!
! Input scalars:
      integer :: AMO,BMO
      integer   :: NApts_tot
      type(type_grid_points), dimension(NApts_tot) :: Angular_points
      double precision, dimension(NApts_tot) :: charge
!
! Local scalars:
      integer   :: IApoint
      integer   :: Ibasis,Jbasis,IJ
      double precision :: sum
      double precision, dimension(:), allocatable :: AOprod

!begin:
      allocate (AOprod(1:MATlen))

      charge(1:NApts_tot) = 0.0D0

      do IApoint=1,NApts_tot
      call AO_products (Angular_points(IApoint)%X, Angular_points(IApoint)%Y, Angular_points(IApoint)%Z, AOprod, MATlen)

      sum=ZERO
      do Ibasis=1,Nbasis
      do Jbasis=1,Nbasis
        IJ=Ibasis*(Ibasis-1)/2+Jbasis
        if(Ibasis.lt.Jbasis)IJ=Jbasis*(Jbasis-1)/2+Ibasis
        sum=sum+CMO%coeff(Ibasis,AMO)*CMO%coeff(Jbasis,BMO)*AOprod(IJ)
      end do ! Jbasis
      end do ! Ibasis

! sum is now the value of MOA*MOB at each grid point
!     charge(IApoint)=sum
      charge(IApoint)=dabs(sum)
!     charge(IApoint)=sum*sum

      end do ! IApoint

      deallocate (AOprod)
!
      end subroutine GET_density_ab
