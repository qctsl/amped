      subroutine Numerical_Integration_shell_energy(ENERGY_PROPERTY)
!*******************************************************************************
!     Date last modified: March 5th ,2019                                   *
!     Author: Ibrahim Awad                                                     *
!     Desciption: Calculate the energy density in a shell volume between       *
!		  RLowerLimit and RUpperLimit and                              *
!*******************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE QM_defaults
      USE QM_objects
      USE NI_defaults
      USE N_integration
      USE type_molecule
      USE type_Weights

      implicit none

! Local scalars:
      integer :: Iatom,Ipoint,Ifound,Z_num,Npts_kept,lenstr
      double precision :: radius,Nshell,EdenP,KgridA, KHFgridA, JgridA, JHFgridA,Vne_grid
      double precision :: XApt, YApt, ZApt, Tx, Ty, Tz, GET_density_at_xyz
      double precision :: TraceAB
      double precision, dimension(:), allocatable :: Vpotl
      double precision, dimension(:), allocatable :: V12dr2
      character(len=32) :: ENERGY_PROPERTY
      character(len=4)  :: ENERGY_PROPERTY_SHORT
      character(:), allocatable :: ENRSTR, ENRSTR2
!
! Begin:
        call GET_object ('QM', 'CMO', Wavefunction)
        call GET_object ('GRID', 'RADIAL', 'BIG')

        write(UNIout,'(2a)')'Radial grid used for numerical integration: ',RADIAL_grid

        if(Natoms.gt.1)then
          stop 'Numerical_Integration_shell_energy only available for single atoms!!'
        end if
        
        Iatom=1
        
        Z_num=CARTESIAN(Iatom)%Atomic_number
        if(Z_num.le.0)then
          stop 'Numerical_Integration_shell_energy only available for real atoms!!'
        end if   
        
        ! Integrate between RLowerLimit And RUpperLimit as defined in Input
        write(UNIout,'(/a,f20.8)', advance='no')'RLowerLimit: ',RLowerLimit
        write(UNIout,'(a,f20.8)')',  RUpperLimit: ',RUpperLimit
        
        if(RUpperLimit.le.RLowerLimit)then
          stop 'RUpperLimit is less or equal RLowerLimit !!'
        end if   

        Ifound=GRID_loc(Z_num)
        NApts_atom=NApoints_atom(Ifound)
        allocate(grid_points(1:NApts_atom))

        Npts_kept=0
        
        ENERGY_PROPERTY_SHORT=ENERGY_PROPERTY(1:4)
        
        allocate(vpotl(1:NApts_atom))
        allocate(V12dr2(1:MATlen))

        do Ipoint=1,NApts_atom
          radius=dsqrt((Egridpts(Ifound,Ipoint)%X)**2 &
                    & +(Egridpts(Ifound,Ipoint)%Y)**2 &
                    & +(Egridpts(Ifound,Ipoint)%Z)**2)
          if(radius.lt.RLowerLimit)cycle 
          if(radius.ge.RUpperLimit)cycle
          Npts_kept=Npts_kept+1
          grid_points(Npts_kept)%X=Egridpts(Ifound,Ipoint)%X+CARTESIAN(Iatom)%X
          grid_points(Npts_kept)%Y=Egridpts(Ifound,Ipoint)%Y+CARTESIAN(Iatom)%Y
          grid_points(Npts_kept)%Z=Egridpts(Ifound,Ipoint)%Z+CARTESIAN(Iatom)%Z
          grid_points(Npts_kept)%w=Egridpts(Ifound,Ipoint)%w
        end do ! IApoint

        write(UNIout,'(a,i8)', advance='no')'Npts_kept: ',Npts_kept
        write(UNIout,'(a,i8)')', NApts_atom: ',NApts_atom

        Nshell=0.0D0
        
        do Ipoint = 1,Npts_kept
           XApt=grid_points(Ipoint)%x
           YApt=grid_points(Ipoint)%y
           ZApt=grid_points(Ipoint)%z
           SELECT CASE (ENERGY_PROPERTY_SHORT)
             CASE ("SCHK")
               KineticMethod='SCH'
               ENRSTR='Sch-kinetic energy'           
!               call GET_Kinetic_GRID (XApt, YApt, ZApt, Tx, Ty, Tz)
               EdenP=PT5*(Tx+Ty+Tz)*grid_points(Ipoint)%w 
             CASE ("PLSK")
               KineticMethod='PLS'
               ENRSTR='PLS-kinetic energy'           
!               call GET_Kinetic_GRID (XApt, YApt, ZApt, Tx, Ty, Tz)
               EdenP=PT5*(Tx+Ty+Tz)*grid_points(Ipoint)%w          
             CASE ("DENS")
               ENRSTR='number of electrons'
               EdenP=GET_density_at_xyz (XApt, YApt, ZApt)*grid_points(Ipoint)%w
             CASE ("EXCH")
               ENRSTR='exchange energy'
               call GET_Exchange_MO_point (XApt, YApt, ZApt, KgridA, KHFgridA)
               EdenP=KgridA*grid_points(Ipoint)%w
             CASE ("MOCO")
               ENRSTR='MO Coulomb energy'
               call GET_COULOMB_MO_point (XApt, YApt, ZApt, JgridA, JHFgridA)
               EdenP=JgridA*grid_points(Ipoint)%w
             CASE ("COUL")
               ENRSTR='Coulomb energy'
               CALL I1E_V12dr2 (V12dr2,MATlen,XApt,YApt,ZApt)                            
               Vpotl(Ipoint)=-TraceAB (PM0, V12dr2, NBasis)
               EdenP=0.50d0*Vpotl(Ipoint)*GET_density_at_xyz(XApt,YApt,ZApt)*grid_points(Ipoint)%w
             CASE ("VNE_")
               ENRSTR='VNE energy'
               call GET_Vne_GRID (XApt, YApt, ZApt, Vne_grid)
               EdenP=Vne_grid*grid_points(Ipoint)%w
             CASE DEFAULT
             write(*,*) ENERGY_PROPERTY_SHORT
               stop 'ERROR NINT_SHELL> Illegal property'
           END SELECT    
           Nshell=Nshell+EdenP
        end do ! IApoint

        Nshell=FourPi*Nshell
 
        ENRSTR2=trim(ENRSTR)
        lenstr=len(ENRSTR2)
        write(UNIout,'(a4,a,a,f14.8)') &
              'The ', ENRSTR2(1:lenstr), ' between lower and upper limits is ', Nshell 
              
        deallocate(grid_points,Vpotl,V12dr2)

      end subroutine Numerical_Integration_shell_energy
