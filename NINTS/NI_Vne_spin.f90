      subroutine NI_Vne_spin
!*********************************************************************************************
!     Date last modified: October 7, 2022                                                    *
!     Author: JWH                                                                            *
!     Desciption: Calculate the electron-nuclear attraction energy for alpha and beta        *
!                 electrons via numerical integration.                                       *
!*********************************************************************************************
! Modules:
      USE program_constants
      USE program_timing
      USE program_files
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE module_grid_points
      USE INT_objects

      implicit none
!
! Local scalars:
      integer :: Iatom,IApoint,IAIM,Znum,Ifound
      double precision :: Xpt,Ypt,Zpt
      double precision :: Vneaatom,VneHFaatom
      double precision :: VneagridA,VneHFagridA
      double precision :: Vnebatom,VneHFbatom
      double precision :: VnebgridA,VneHFbgridA
      double precision :: t0, t1, t2, time
!
! Local arrays:
      double precision, dimension(:), allocatable :: WeightsA
!
! Begin:
      call PRG_manager ('enter', 'NI_Vne_spin', 'VNE%NI')
!
      call GET_object ('QM', 'SPIN_DENSITY_1MATRIX', 'AO')
!
! Get grid and weights
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
      write(UNIout,'(/2a)')'Radial grid used for numerical integration: ',RADIAL_grid
      call BLD_BeckeW_XI
      write(UNIout,'(2a)')'Weights used for numerical integration:      BECKE'
!
      call CPU_time(t0)
      call CPU_time(t1)      
!
! same-spin
      allocate(VneHFa_atomic(Natoms),Vnea_atomic(Natoms))
      allocate(VneHFb_atomic(Natoms),Vneb_atomic(Natoms))
      Vnea_total=ZERO
      Vnea_atomic=ZERO
      VneHFa_total=ZERO
      VneHFa_atomic=ZERO
      Vneb_total=ZERO
      Vneb_atomic=ZERO
      VneHFb_total=ZERO
      VneHFb_atomic=ZERO
!
      write(UNIout,'(/a)')'      Electron-nuclear potential energy via numerical integration    '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '                            Atomic contributions             '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '                   Hartree-Fock                 Correlated          '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '  Atom           alpha         beta          alpha         beta     '
      write(UNIout,'(a)') '--------------------------------------------------------------------'

      if(NAIMprint.eq.0)then ! If no atoms specified, do them all!
        NAIMprint=Natoms
        do Iatom=1,Natoms
          AIMprint(Iatom)=Iatom
        end do ! Iatom
      end if

      if(allocated(grid_points))then
        deallocate(grid_points)
      end if 
!
      do IAIM=1,NAIMprint
!     do Iatom=1,Natoms
        Iatom=AIMprint(IAIM)
        Znum=CARTESIAN(Iatom)%atomic_number
        if(Znum.le.0)cycle
        Ifound=GRID_loc(Znum)
        NApts_atom=NApoints_atom(Ifound)

        allocate(grid_points(1:NApts_atom))
        allocate(WeightsA(NApts_atom))
!
! Moving grid points on cartesian coordinate grid
        do IApoint=1,NApts_atom
          grid_points(IApoint)%X=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          grid_points(IApoint)%Y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          grid_points(IApoint)%Z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
        end do ! IApoint

        call GET_weights (grid_points, NApts_atom, Iatom, WeightsA)

        Vneaatom=ZERO
        VneHFaatom=ZERO
        Vnebatom=ZERO
        VneHFbatom=ZERO
!

        do IApoint=1,NApts_atom
      !    if(WeightsA(IApoint).le.1.0D-6)cycle ! Makes no difference
          Xpt=grid_points(IApoint)%X
          Ypt=grid_points(IApoint)%Y
          Zpt=grid_points(IApoint)%Z
!
          call GET_Vne_spin_point (Xpt, Ypt, Zpt, VneagridA, VneHFagridA, VnebgridA, VneHFbgridA)
!
          Vneaatom = Vneaatom + VneagridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
          VneHFaatom = VneHFaatom + VneHFagridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
          Vnebatom = Vnebatom + VnebgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
          VneHFbatom = VneHFbatom + VneHFbgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
        end do ! IApoint

        Vnea_atomic(Iatom) = Vnea_atomic(Iatom) + Vneaatom
        VneHFa_atomic(Iatom) = VneHFa_atomic(Iatom) + VneHFaatom
!
        Vneb_atomic(Iatom) = Vneb_atomic(Iatom) + Vnebatom
        VneHFb_atomic(Iatom) = VneHFb_atomic(Iatom) + VneHFbatom
!
        deallocate(grid_points)
        deallocate(WeightsA)

      end do ! Iatom

!
! Now multiply by 4pi and get total exchange
      do IAIM=1,NAIMprint
        Iatom=AIMprint(IAIM)
!     do Iatom=1,Natoms
      Znum=CARTESIAN(Iatom)%atomic_number
      if(Znum.le.0)cycle
!
      Vnea_atomic(Iatom) = FourPi*Vnea_atomic(Iatom)
      VneHFa_atomic(Iatom) = FourPi*VneHFa_atomic(Iatom)
!
      Vneb_atomic(Iatom) = FourPi*Vneb_atomic(Iatom)
      VneHFb_atomic(Iatom) = FourPi*VneHFb_atomic(Iatom)
!
      write(UNIout,'(i8,1x,4f14.8)')Iatom,VneHFa_atomic(Iatom),VneHFb_atomic(Iatom),&
                                          Vnea_atomic(Iatom),Vneb_atomic(Iatom)
!
      Vnea_total = Vnea_total + Vnea_atomic(Iatom)
      VneHFa_total = VneHFa_total + VneHFa_atomic(Iatom)
!
      Vneb_total = Vneb_total + Vneb_atomic(Iatom)
      VneHFb_total = VneHFb_total + VneHFb_atomic(Iatom)
!
      end do ! Iatom
!
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      if(NAIMprint.eq.Natoms)write(UNIout,'(a,1x,4f14.8)')'   Total',VneHFa_total,VneHFb_total,&
                                                                     Vnea_total,Vneb_total
!
      deallocate(Vnea_atomic,VneHFa_atomic)
      deallocate(Vneb_atomic,VneHFb_atomic)
!
      call CPU_time(t2)
      time = t2 - t1
!
! End of routine NI_Vne_spin
      call PRG_manager ('exit', 'NI_Vne_spin', 'VNE%NI')
      end subroutine NI_Vne_spin
