      module NI_potential
!**************************************************************************************
!     Date last modified: Dec. 6, 2013                                                *
!     Author: R. Poirier                                                              *
!     Desciption: Potential energy values                                             *
!**************************************************************************************
! Modules:

      implicit none

! Output scalars:
      double precision :: pot_energy_tot     ! Potential energy
      double precision :: error_pot
      double precision :: error_VTOT   ! Error in total, potential energy

! Local scalars:
      double precision, dimension(:), allocatable :: pot_energy    ! potential energies for the molecule
      double precision :: accuracy_pot  !Accuracy in potential (Vne)

      end module NI_potential
