      subroutine NI_BOND_ELECTRONS
!**************************************************************************************
!     Date last modified: March 2, 2015                                               *
!     Author: R.A. Poirier                                                            *
!     Desciption:                                                                     *
!**************************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE NI_defaults
      USE N_integration
      USE type_molecule
      USE type_NI_Nelec_AB
      USE type_Weights

      implicit none

! Local scalars:
      integer :: Aatom,Batom,IApoint,IfoundA,IfoundB,NApts_A,NApts_B,NApoints_tot,Tpoints,Z_num
      double precision :: Nelec_AB,Nelec_AB_A,Nelec_AB_B,Nelec_tot
      double precision, dimension(:), allocatable :: BweightsA,BweightsB
      type(type_grid_points),dimension(:),allocatable :: grid_A,grid_B
!
! Begin:
      call PRG_manager ('enter', 'NI_BOND_ELECTRONS', 'BOND_ELECTRONS%ABIM')

      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)

      write(UNIout,'(2a)')'Radial grid used for numerical integration: ',RADIAL_grid
      write(UNIout,'(2a)')'Weights used for numerical integration:     ',DEN_partitioning
      if(DEN_partitioning(1:5).eq.'BECKE')call BLD_BeckeW_XI 

!     call INI_NI_Nelec_AB
      NApoints_tot=0

      Aatom=Bond_atoms(1)
      Batom=Bond_atoms(2)
      if(Aatom.le.0.or.Batom.le.0)then
        write(UNIout,'(a,2i6)')'ERROR NI_BOND_ELECTRONS> Illegal atoms:',Aatom,Batom
        stop 'ERROR NI_BOND_ELECTRONS> Illegal atoms'
      end if
      Nelec_AB=ZERO
      Nelec_AB_A=ZERO
      Nelec_AB_B=ZERO
      Z_num=CARTESIAN(Aatom)%Atomic_number
      if(Z_num.le.0)then
        write(UNIout,'(a,i6)')'ERROR NI_BOND_ELECTRONS> Illegal atom:',Z_num
        stop 'ERROR NI_BOND_ELECTRONS> Illegal atom'
      end if
      IfoundA=GRID_loc(Z_num)
      NApts_A=NApoints_atom(IfoundA)
      NApoints_tot=NApoints_tot+NApts_A
      allocate(grid_A(1:NApts_A))
      Z_num=CARTESIAN(Batom)%Atomic_number
      if(Z_num.le.0)then
        write(UNIout,'(a,i6)')'ERROR NI_BOND_ELECTRONS> Illegal atom:',Z_num
        stop 'ERROR NI_BOND_ELECTRONS> Illegal atom'
      end if
      IfoundB=GRID_loc(Z_num)
      NApts_B=NApoints_atom(IfoundB)
      NApoints_tot=NApoints_tot+NApts_B
      allocate(grid_B(1:NApts_B))

      allocate(grid_points(1:NApoints_tot))
      Tpoints=0
      do IApoint=1,NApts_A
        Tpoints=Tpoints+1
        grid_points(Tpoints)%X=Egridpts(IfoundA,IApoint)%X+CARTESIAN(Aatom)%X
        grid_points(Tpoints)%Y=Egridpts(IfoundA,IApoint)%Y+CARTESIAN(Aatom)%Y
        grid_points(Tpoints)%Z=Egridpts(IfoundA,IApoint)%Z+CARTESIAN(Aatom)%Z
        grid_points(Tpoints)%w=Egridpts(IfoundA,IApoint)%w
      end do ! IApoint
      do IApoint=1,NApts_B
        Tpoints=Tpoints+1
        grid_points(Tpoints)%X=Egridpts(IfoundB,IApoint)%X+CARTESIAN(Batom)%X
        grid_points(Tpoints)%Y=Egridpts(IfoundB,IApoint)%Y+CARTESIAN(Batom)%Y
        grid_points(Tpoints)%Z=Egridpts(IfoundB,IApoint)%Z+CARTESIAN(Batom)%Z
        grid_points(Tpoints)%w=Egridpts(IfoundB,IApoint)%w
      end do ! IApoint

      allocate(charge(NApoints_tot))
      call GET_density (grid_points, NApoints_tot, charge)

! Now compute the Becke weights for all the grid points for Aatom
      allocate(BweightsA(NApoints_tot))
      call GET_weights (grid_points, NApoints_tot, Aatom, BweightsA)
      allocate(BweightsB(NApoints_tot))
      call GET_weights (grid_points, NApoints_tot, Batom, BweightsB)

      write(UNIout,'(a,i6,a,i6)')'Properties associated with bond (averaged): Aatom= ',Aatom,' Batom= ',Batom
      write(UNIout,'(a2,a,a2,a)')trim(CARTESIAN(Aatom)%element),'-',trim(CARTESIAN(Batom)%element),' Bond'
      call NI_BOND_ELECTRONS_AB (Aatom, Batom)

      write(UNIout,'(a,f12.6,a)')'Number of electrons: ',Nelec_AB,' =int sqrt(WA(r)*WB(r))rho(r)dr = int rho^AB(r)dr'
      write(UNIout,'(a,9x,f12.6,a)')'From Aatom: ',Nelec_AB_A,' =int WA(r)rho^AB(r)dr'
      write(UNIout,'(a,9x,f12.6,a)')'From Batom: ',Nelec_AB_B,' =int WB(r)rho^AB(r)dr'
      Nelec_tot=Nelec_AB_A+Nelec_AB_B
      write(UNIout,'(a,f12.6)')'Sum:                 ',Nelec_tot

      if(MUN_prtlev.gt.0)write(UNIout,'(a,i8/)')'Total number of grid points: ',NApoints_tot

      deallocate(charge)
      deallocate(grid_points)
      deallocate(grid_A)
      deallocate(grid_B)

      call PRG_manager ('exit', 'NI_BOND_ELECTRONS', 'BOND_ELECTRONS%ABIM')
      return
CONTAINS ! -------------------------------------------------------------------------
      subroutine NI_BOND_ELECTRONS_AB (Aatom, Batom)
!************************************************************************************
!     Date last modified December 13, 2013                                          *
!     Author: R.A. Poirier                                                          *
!     Description: Initialization for the dipole moment (first)                     *
!************************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE matrix_print
      USE type_molecule
      USE N_integration
      USE type_NI_Nelec_AB

      implicit none

! Input scalars:
      integer :: Aatom,Batom
!
! Local scalars:
      integer :: IApoint
      double precision :: WAB
      double precision, dimension(:), allocatable :: PchargeAB

! Begin: 
      allocate (PchargeAB(NApoints_tot))
! Save the weights for Aatom
      do IApoint=1,NApoints_tot
        WAB=dsqrt(BweightsA(IApoint)*BweightsB(IApoint))
        PchargeAB(IApoint)=WAB*charge(IApoint)*grid_points(IApoint)%w
        Nelec_AB_A=Nelec_AB_A+BweightsA(IApoint)*PchargeAB(IApoint)
        Nelec_AB_B=Nelec_AB_B+BweightsB(IApoint)*PchargeAB(IApoint)
        Nelec_AB=Nelec_AB+PchargeAB(IApoint)
      end do ! IApoint

      Nelec_AB=FourPi*Nelec_AB
      Nelec_AB_A=FourPi*Nelec_AB_A
      Nelec_AB_B=FourPi*Nelec_AB_B

      deallocate(PchargeAB)
      deallocate(BweightsA)

      return
      end subroutine NI_BOND_ELECTRONS_AB
! END CONTAINS ---------------------------------------------------------------------
      end subroutine NI_BOND_ELECTRONS
      subroutine NI_ELECTRONS_ALL_BONDS
!**************************************************************************************
!     Date last modified: March 2, 2015                                               *
!     Author: R.A. Poirier                                                            *
!     Desciption:                                                                     *
!**************************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE NI_defaults
      USE N_integration
      USE type_molecule
      USE type_NI_Nelec_AB
      USE type_Weights

      implicit none

! Local scalars:
      integer :: Iatom,Jatom,IApoint,Ifound,Z_num
!
! Begin:
      call PRG_manager ('enter', 'NI_ELECTRONS_ALL_BONDS', 'ALL_BONDS_ELECTRONS%ABIM')

      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)

      write(UNIout,'(2a)')'Radial grid used for numerical integration: ',RADIAL_grid
      write(UNIout,'(2a)')'Weights used for numerical integration:     ',DEN_partitioning
      if(DEN_partitioning(1:5).eq.'BECKE')call BLD_BeckeW_XI 

      call INI_NI_Nelec_AB
      do Iatom=1,Natoms
        Z_num = CARTESIAN(Iatom)%Atomic_number
        if(Z_num.le.0)cycle
        Ifound=GRID_loc(Z_num)
        NApts_atom=NApoints_atom(Ifound)
        allocate(grid_points(1:NApts_atom))

        do IApoint=1,NApts_atom
          grid_points(IApoint)%X=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          grid_points(IApoint)%Y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          grid_points(IApoint)%Z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
          grid_points(IApoint)%w=Egridpts(Ifound,IApoint)%w
        end do ! IApoint

        allocate(charge(NApts_atom))

! Now compute the Becke weights for all the grid points for Iatom
        allocate(Bweights(NApts_atom))
        call GET_weights (grid_points, NApts_atom, Iatom, Bweights)
        call GET_density (grid_points, NApts_atom, charge)

        call NI_Nelec_AB (Iatom, Ifound)

        deallocate(Bweights)
        deallocate(charge)
        deallocate(grid_points)
      end do ! Iatom

      do Iatom=1,Natoms
        Z_num = CARTESIAN(Iatom)%Atomic_number
        if(Z_num.le.0)cycle
      do Jatom=1,Iatom
        Z_num = CARTESIAN(Jatom)%Atomic_number
        if(Z_num.le.0)cycle
        if(Iatom.eq.Jatom)cycle
        N_elec_AB(Iatom,Jatom)=(N_elec_AB(Iatom,Jatom)+N_elec_AB(Jatom,Iatom))/TWO
        N_elec_AB(Jatom,Iatom)=N_elec_AB(Iatom,Jatom)
      end do ! Jatom
      end do ! Iatom
        
      call PRT_NI_Nelec_AB_AIM

      call PRG_manager ('exit', 'NI_ELECTRONS_ALL_BONDS', 'ALL_BONDS_ELECTRONS%ABIM')
      return
      end subroutine NI_ELECTRONS_ALL_BONDS
