      subroutine NI_BONDS_ENERGY(ENERGY_PROPERTY)
!*******************************************************************************
!     Date last modified: Feb. 24, 2019                                        *
!     Author:  Ibrahim Awad                                                    *
!     Desciption:                                                              *
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE NI_defaults
      USE N_integration
      USE type_molecule
      USE type_NI_Nelec_AB
      USE type_Weights
!      USE module_grid_points

      implicit none

! Local scalars:
      integer :: Aatom,Batom,Z_numA,Z_numB
      integer :: IApoint,IfoundA,IfoundB,NApts_A,NApts_B,NApoints_tot,Tpoints
      double precision :: P_AB
      double precision, dimension(:), allocatable :: BweightsA,BweightsB,WAB
      type(type_grid_points),dimension(:),allocatable :: grid_A,grid_B
      character(len=16) :: ENERGY_PROPERTY, ENRSTR
      character(len=4)  :: ENERGY_PROPERTY_SHORT
      
!
! Begin:

      call GET_object ('QM', 'CMO', Wavefunction)
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
      write(UNIout,'(2a)')'Radial grid used for numerical integration: ',RADIAL_grid

      if(Natoms.eq.2.and.NAIMprint.eq.0)then
        write(UNIout,'(a)')'WARNING PLOT_BONDS> No AIMprint selected'
        write(UNIout,'(a/)')'WARNING PLOT_BONDS> Aatom=1, Batom=2 are set by default'
        AIMprint(1)=1
        AIMprint(2)=2
        NAIMprint=2
      else if(NAIMprint.ne.2)then
        write(UNIout,'(a,i2)')'ERROR PLOT_BONDS> NAIMprint should be 2, NAIMprint=',NAIMprint
        stop 'ERROR PLOT_BONDS> Illegal NAIMprint'
      end if
      Aatom=AIMprint(1)
      Batom=AIMprint(2)
      if(Aatom.le.0.or.Batom.le.0)then
        write(UNIout,'(a,2i6)')'ERROR PLOT_BONDS> Illegal atoms:',Aatom,Batom
        stop 'ERROR PLOT_BONDS> Illegal atoms'
      end if
      Z_numA=CARTESIAN(Aatom)%Atomic_number
      if(Z_numA.le.0)then
        write(UNIout,'(a,i3)')'ERROR PLOT_BONDS> Illegal atom:',Z_numA
        stop 'ERROR PLOT_BONDS> Illegal atom'
      end if
      Z_numB=CARTESIAN(Batom)%Atomic_number
      if(Z_numB.le.0)then
        write(UNIout,'(a,i3)')'ERROR PLOT_BONDS> Illegal atom:',Z_numB
        stop 'ERROR PLOT_BONDS> Illegal atom'
      end if
      
      write(UNIout,'(a,i3,a,i3,4x)',advance='no')'Properties associated with bond: Aatom= ',Aatom,' Batom= ',Batom
      write(UNIout,'(a2,a,a2,a)')trim(CARTESIAN(Aatom)%element),'-',trim(CARTESIAN(Batom)%element),' Bond'
      if(DEN_partitioning(1:5).eq.'BECKE')call BLD_BeckeW_XI ! Only needed if using Becke weights

      IfoundA=GRID_loc(Z_numA)
      NApts_A=NApoints_atom(IfoundA)    

      IfoundB=GRID_loc(Z_numB)
      NApts_B=NApoints_atom(IfoundB)
      
      NApoints_tot=NApts_B+NApts_A
      write(UNIout,'(a,i8/)')'NGridPoints: ', NApoints_tot
         
      allocate(grid_A(1:NApts_A))
      allocate(grid_B(1:NApts_B))
      allocate(grid_points(1:NApoints_tot))
      
      do IApoint=1,NApts_A
        grid_A(IApoint)%X=Egridpts(IfoundA,IApoint)%X+CARTESIAN(Aatom)%X
        grid_A(IApoint)%Y=Egridpts(IfoundA,IApoint)%Y+CARTESIAN(Aatom)%Y
        grid_A(IApoint)%Z=Egridpts(IfoundA,IApoint)%Z+CARTESIAN(Aatom)%Z
        grid_A(IApoint)%w=Egridpts(IfoundA,IApoint)%w
      end do ! IApoint
      do IApoint=1,NApts_B
        grid_B(IApoint)%X=Egridpts(IfoundB,IApoint)%X+CARTESIAN(Batom)%X
        grid_B(IApoint)%Y=Egridpts(IfoundB,IApoint)%Y+CARTESIAN(Batom)%Y
        grid_B(IApoint)%Z=Egridpts(IfoundB,IApoint)%Z+CARTESIAN(Batom)%Z
        grid_B(IApoint)%w=Egridpts(IfoundB,IApoint)%w
      end do ! IApoint
           
      ! Now compute the Becke weights for all the grid points for Aatom
      grid_points(1:NApts_A)=grid_A(1:NApts_A)
      grid_points(NApts_A+1:NApoints_tot)=grid_B(1:NApts_B)
      
      allocate(BweightsA(NApoints_tot))
      call GET_weights (grid_points, NApoints_tot, Aatom, BweightsA)
      allocate(BweightsB(NApoints_tot))
      call GET_weights (grid_points, NApoints_tot, Batom, BweightsB)
      
      allocate(WAB(NApoints_tot))
      do IApoint=1,NApoints_tot
         WAB(IApoint)=dsqrt(BweightsA(IApoint)*BweightsB(IApoint))
      end do
       
      ENERGY_PROPERTY_SHORT=ENERGY_PROPERTY(1:4)

      SELECT CASE (ENERGY_PROPERTY_SHORT)
          CASE ("SCHK")
              ENRSTR='SCH_KINETIC'! compute kinetic energy in the AB bond        
              KineticMethod='SCH' ! 'SCH' or 'PLS'
              call BONDS_KINETIC_ENERGY 
          CASE ("PLSK")
              ENRSTR='PLS_KINETIC' ! compute kinetic energy in the AB bond
              KineticMethod='PLS'  ! 'SCH' or 'PLS'
              call BONDS_KINETIC_ENERGY             
          CASE ("DENS")
              ENRSTR='DENSITY'     ! compute number of electron in the AB bond      
              call NI_BONDS_ELEC
          CASE ("EXCH")
              ENRSTR='EXCHANGE'    ! compute exchange energy in the AB bond
              call BONDS_EXCHANGE_ENERGY
          CASE ("MOCO")
              ENRSTR='MO_COULOMB'  ! compute MO coulomb energy in the AB bond
              call BONDS_MO_COULOMB_ENERGY
          CASE ("COUL")
              ENRSTR='COULOMB'     ! compute coulomb energy in the AB bond
              call BONDS_COULOMB_ENERGY
          CASE ("VNE_")
              ENRSTR='VNE'
              call BONDS_VNE_ENERGY
          CASE DEFAULT
            stop 'ERROR ENERGY_BONDS> Illegal property'
       END SELECT    

      deallocate(grid_points)
      deallocate(grid_A)
      deallocate(grid_B)
      deallocate(WAB,BweightsA,BweightsB)

      return
      contains
      subroutine NI_BONDS_ELEC
!*******************************************************************************
!     Date last modified: Feb. 24, 2019                                        *
!     Author:  Ibrahim Awad                                                    *
!     Desciption:                                                              *
!*******************************************************************************
! Modules:

!      USE type_NI_Nelec_AB

      implicit none

! Local scalars:
       double precision :: Nelec_AB,Nelec_AB_A,Nelec_AB_B,Nelec_tot
!
! Begin:
      
      Nelec_AB=ZERO
      Nelec_AB_A=ZERO
      Nelec_AB_B=ZERO

      allocate(charge(NApoints_tot))
      call GET_density (grid_points, NApoints_tot, charge)    
      
     do IApoint=1,NApoints_tot
!        WAB=dsqrt(BweightsA(IApoint)*BweightsB(IApoint))
        P_AB=WAB(IApoint)*charge(IApoint)*grid_points(IApoint)%w
        Nelec_AB_A=Nelec_AB_A+BweightsA(IApoint)*P_AB
        Nelec_AB_B=Nelec_AB_B+BweightsB(IApoint)*P_AB
        Nelec_AB=Nelec_AB+P_AB
      end do ! IApoint

      Nelec_AB=FourPi*Nelec_AB
      Nelec_AB_A=FourPi*Nelec_AB_A
      Nelec_AB_B=FourPi*Nelec_AB_B    

      write(UNIout,'(a,f12.6,a)')'Electrons in AB-bond:',Nelec_AB,' = int sqrt(WA(r)*WB(r))rho(r)dr = int rho^AB(r)dr'
      write(UNIout,'(a,9x,f12.6,a)')'From Aatom: ',Nelec_AB_A,' = int WA(r)rho^AB(r)dr'
      write(UNIout,'(a,9x,f12.6,a)')'From Batom: ',Nelec_AB_B,' = int WB(r)rho^AB(r)dr'
      Nelec_tot=Nelec_AB_A+Nelec_AB_B
      write(UNIout,'(a,f12.6/)')'Sum:                 ',Nelec_tot

      deallocate(charge)
      
      end subroutine NI_BONDS_ELEC
      subroutine BONDS_MO_COULOMB_ENERGY
!*******************************************************************************
!     Date last modified: Feb. 24, 2019                                        *
!     Author:  Ibrahim Awad                                                    *
!     Desciption:                                                              *
!*******************************************************************************
! Modules:

!      USE type_NI_Nelec_AB

      implicit none

! Local scalars:
      double precision :: J_AB,J_AB_A,J_AB_B,J_tot,JgridA, JHFgridA
      double precision :: XApt,YApt,ZApt  
!
! Begin:
    
      J_AB=ZERO
      J_AB_A=ZERO
      J_AB_B=ZERO
      P_AB=ZERO
       
      do IApoint=1,NApoints_tot            
         XApt=grid_points(IApoint)%x
         YApt=grid_points(IApoint)%y
         ZApt=grid_points(IApoint)%z

         call GET_COULOMB_MO_point (XApt, YApt, ZApt, JgridA, JHFgridA)        
         P_AB=WAB(IApoint)*JgridA*grid_points(IApoint)%w                 
         J_AB_A=J_AB_A+BweightsA(IApoint)*P_AB
         J_AB_B=J_AB_B+BweightsB(IApoint)*P_AB        
         J_AB=J_AB+P_AB        
      end do ! IApoint

      J_AB=FourPi*J_AB
      J_AB_A=FourPi*J_AB_A
      J_AB_B=FourPi*J_AB_B    

      write(UNIout,'(a,f12.6,a)')'MO COULOMB in AB-bond:',J_AB,' = int sqrt(WA(r)*WB(r))J(r)dr = int J^AB(r)dr'
      write(UNIout,'(a,9x,f12.6,a)')'From Aatom: ',J_AB_A,' = int WA(r)J^AB(r)dr'
      write(UNIout,'(a,9x,f12.6,a)')'From Batom: ',J_AB_B,' = int WB(r)J^AB(r)dr'
      J_tot=J_AB_A+J_AB_B
      write(UNIout,'(a,f12.6/)')'Sum:                 ',J_tot
      
      end subroutine BONDS_MO_COULOMB_ENERGY
      subroutine BONDS_COULOMB_ENERGY
!*******************************************************************************
!     Date last modified: Feb. 24, 2019                                        *
!     Author:  Ibrahim Awad                                                    *
!     Desciption:                                                              *
!*******************************************************************************
! Modules:

!      USE type_NI_Nelec_AB

      implicit none

! Local scalars:
      double precision :: J_AB,J_AB_A,J_AB_B,J_tot,JgridA, JHFgridA
      double precision :: XApt,YApt,ZApt
      double precision :: TraceAB
      double precision, dimension(:), allocatable :: Vpotl 
      double precision, dimension(:), allocatable :: V12dr2
      double precision :: GET_density_at_xyz
!
! Begin:
    
      J_AB=ZERO
      J_AB_A=ZERO
      J_AB_B=ZERO
      P_AB=ZERO

      allocate(V12dr2(1:MATlen))
      allocate(vpotl(1:NApoints_tot))
       
      do IApoint=1,NApoints_tot            
         XApt=grid_points(IApoint)%x
         YApt=grid_points(IApoint)%y
         ZApt=grid_points(IApoint)%z

         CALL I1E_V12dr2 (V12dr2,MATlen,XApt,YApt,ZApt)                            
         Vpotl(IApoint)=-TraceAB (PM0, V12dr2, NBasis)
         P_AB=0.50d0*WAB(IApoint)*Vpotl(IApoint)*GET_density_at_xyz(XApt,YApt,ZApt)*grid_points(IApoint)%w   
                  
         J_AB_A=J_AB_A+BweightsA(IApoint)*P_AB
         J_AB_B=J_AB_B+BweightsB(IApoint)*P_AB        
         J_AB=J_AB+P_AB        
      end do ! IApoint

      J_AB=FourPi*J_AB
      J_AB_A=FourPi*J_AB_A
      J_AB_B=FourPi*J_AB_B    

      write(UNIout,'(a,f12.6,a)')'COULOMB in AB-bond:',J_AB,' = int sqrt(WA(r)*WB(r))J(r)dr = int J^AB(r)dr'
      write(UNIout,'(a,9x,f12.6,a)')'From Aatom: ',J_AB_A,' = int WA(r)J^AB(r)dr'
      write(UNIout,'(a,9x,f12.6,a)')'From Batom: ',J_AB_B,' = int WB(r)J^AB(r)dr'
      J_tot=J_AB_A+J_AB_B
      write(UNIout,'(a,f12.6/)')'Sum:                 ',J_tot
      deallocate(Vpotl,V12dr2)
      
      end subroutine BONDS_COULOMB_ENERGY

      subroutine BONDS_EXCHANGE_ENERGY
!*******************************************************************************
!     Date last modified: Feb. 24, 2019                                        *
!     Author:  Ibrahim Awad                                                    *
!     Desciption:                                                              *
!*******************************************************************************
! Modules:

!      USE type_NI_Nelec_AB

      implicit none

! Local scalars:
      double precision :: K_AB,K_AB_A,K_AB_B,K_tot,KgridA, KHFgridA
      double precision :: XApt,YApt,ZApt  
!
! Begin:
    
      K_AB=ZERO
      K_AB_A=ZERO
      K_AB_B=ZERO
      P_AB=ZERO
       
      do IApoint=1,NApoints_tot           
         XApt=grid_points(IApoint)%x
         YApt=grid_points(IApoint)%y
         ZApt=grid_points(IApoint)%z

         call GET_Exchange_MO_point (XApt, YApt, ZApt, KgridA, KHFgridA)        
         P_AB=WAB(IApoint)*KgridA*grid_points(IApoint)%w                 
         K_AB_A=K_AB_A+BweightsA(IApoint)*P_AB
         K_AB_B=K_AB_B+BweightsB(IApoint)*P_AB        
         K_AB=K_AB+P_AB        
      end do ! IApoint

      K_AB=FourPi*K_AB
      K_AB_A=FourPi*K_AB_A
      K_AB_B=FourPi*K_AB_B    

      write(UNIout,'(a,f12.6,a)')'EXCHANGE in AB-bond:  ',K_AB,' = int sqrt(WA(r)*WB(r))K(r)dr = int K^AB(r)dr'
      write(UNIout,'(a,9x,f12.6,a)')'From Aatom:  ',K_AB_A,' = int WA(r)K^AB(r)dr'
      write(UNIout,'(a,9x,f12.6,a)')'From Batom:  ',K_AB_B,' = int WB(r)K^AB(r)dr'
      K_tot=K_AB_A+K_AB_B
      write(UNIout,'(a,f12.6/)')'Sum:                  ',K_tot
      
      end subroutine BONDS_EXCHANGE_ENERGY
      subroutine BONDS_KINETIC_ENERGY
!*******************************************************************************
!     Date last modified: Feb. 24, 2019                                        *
!     Author:  Ibrahim Awad                                                    *
!     Desciption:                                                              *
!*******************************************************************************
! Modules:

!      USE type_NI_Nelec_AB

      implicit none

! Local scalars:
      double precision :: T_AB,T_AB_A,T_AB_B,T_tot
      double precision :: Tx,Ty,Tz,XApt,YApt,ZApt  
!
! Begin:
    
      T_AB=ZERO
      T_AB_A=ZERO
      T_AB_B=ZERO
      P_AB=ZERO
       
      do IApoint=1,NApoints_tot     
         XApt=grid_points(IApoint)%x
         YApt=grid_points(IApoint)%y
         ZApt=grid_points(IApoint)%z

!         call GET_Kinetic_GRID (XApt, YApt, ZApt, Tx, Ty, Tz)
         P_AB=WAB(IApoint)*PT5*(Tx+Ty+Tz)*grid_points(IApoint)%w                
         T_AB_A=T_AB_A+BweightsA(IApoint)*P_AB
         T_AB_B=T_AB_B+BweightsB(IApoint)*P_AB
         T_AB=T_AB+P_AB
      end do ! IApoint

      T_AB=FourPi*T_AB
      T_AB_A=FourPi*T_AB_A
      T_AB_B=FourPi*T_AB_B    

      write(UNIout,'(3a,f12.6,a)')'KINETIC ',KineticMethod,' in AB-bond:  ',T_AB,' = int sqrt(WA(r)*WB(r))T(r)dr = int T^AB(r)dr'
      write(UNIout,'(a,9x,f12.6,a)')'From Aatom: ',T_AB_A,' = int WA(r)T^AB(r)dr'
      write(UNIout,'(a,9x,f12.6,a)')'From Batom: ',T_AB_B,' = int WB(r)T^AB(r)dr'
      T_tot=T_AB_A+T_AB_B
      write(UNIout,'(a,f12.6/)')'Sum:                 ',T_tot
      
      end subroutine BONDS_KINETIC_ENERGY


      subroutine BONDS_VNE_ENERGY
!*******************************************************************************
!     Date last modified: Feb. 24, 2019                                        *
!     Author:  Ibrahim Awad                                                    *
!     Desciption:                                                              *
!*******************************************************************************
! Modules:

!      USE type_NI_Nelec_AB

      implicit none

! Local scalars:
      double precision :: VNE_AB,VNE_AB_A,VNE_AB_B,VNE_tot,VNEgridA
      double precision :: XApt,YApt,ZApt  
!
! Begin:
    
      VNE_AB=ZERO
      VNE_AB_A=ZERO
      VNE_AB_B=ZERO
      P_AB=ZERO
       
      do IApoint=1,NApoints_tot           
         XApt=grid_points(IApoint)%x
         YApt=grid_points(IApoint)%y
         ZApt=grid_points(IApoint)%z

         call GET_Vne_GRID (XApt, YApt, ZApt, VNEgridA)        
         P_AB=WAB(IApoint)*VNEgridA*grid_points(IApoint)%w                 
         VNE_AB_A=VNE_AB_A+BweightsA(IApoint)*P_AB
         VNE_AB_B=VNE_AB_B+BweightsB(IApoint)*P_AB        
         VNE_AB=VNE_AB+P_AB        
      end do ! IApoint

      VNE_AB=FourPi*VNE_AB
      VNE_AB_A=FourPi*VNE_AB_A
      VNE_AB_B=FourPi*VNE_AB_B    

      write(UNIout,'(a,f12.6,a)')'VNE in AB-bond:      ',VNE_AB,' = int sqrt(WA(r)*WB(r))VNE(r)dr = int VNE^AB(r)dr'
      write(UNIout,'(a,9x,f12.6,a)')'From Aatom: ',VNE_AB_A,' = int WA(r)VNE^AB(r)dr'
      write(UNIout,'(a,9x,f12.6,a)')'From Batom: ',VNE_AB_B,' = int WB(r)VNE^AB(r)dr'
      VNE_tot=VNE_AB_A+VNE_AB_B
      write(UNIout,'(a,f12.6/)')'Sum:                 ',VNE_tot
      
      end subroutine BONDS_VNE_ENERGY

      end subroutine NI_BONDS_ENERGY

