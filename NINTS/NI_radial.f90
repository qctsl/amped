      subroutine radial_atomic (Iatom, Ifound)
!***********************************************************************
!     Date last modified April 25th ,2005                              *
!     Author: R. Poirier                                               *
!     Description:                                                     *
!***********************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE QM_objects
      USE N_integration
      USE NI_radial

      implicit none

! Local scalars:
      integer :: IApoint
      integer :: Iatom,Ifound
      integer :: AMO
      double precision :: XApt2,YApt2,ZApt2,Rval,Rval2
      double precision, dimension(:), allocatable :: radial_r_point
      double precision, dimension(:), allocatable :: radial_r2_point
!
! Begin: 

      allocate(radial_r_point(NApts_atom))
      allocate(radial_r2_point(NApts_atom))

! Begin: 
      do AMO=1,CMO%NoccMO          ! CMO%NoccMO ! Occupied MO only
! Get |phi_a(r)|^2
      call GET_density_ab (grid_points, NApts_atom, charge, AMO, AMO)

      do IApoint=1,NApts_atom
        XApt2=(grid_points(IApoint)%X-CARTESIAN(Iatom)%X)**2
        YApt2=(grid_points(IApoint)%Y-CARTESIAN(Iatom)%Y)**2
        ZApt2=(grid_points(IApoint)%Z-CARTESIAN(Iatom)%Z)**2
        Rval2=XApt2+YApt2+ZApt2
        Rval=dsqrt(Rval2)
!    write(6,*)XApt2,YApt2,ZApt2,Rval,charge(IApoint)
        radial_r_point(IApoint)=charge(IApoint)*Rval*Bweights(IApoint)*grid_points(IApoint)%w
        radial_r2_point(IApoint)=charge(IApoint)*Rval2*Bweights(IApoint)*grid_points(IApoint)%w
      end do

      do IApoint=1,NApts_atom
        radial_r(Iatom,AMO)=radial_r(Iatom,AMO)+radial_r_point(IApoint)
        radial_r2(Iatom,AMO)=radial_r2(Iatom,AMO)+radial_r2_point(IApoint)
      end do !IApoint
! Only for closed shell (occupancy=2)
      radial_r(Iatom,AMO)=CMOG%occupancy(AMO)*FourPi*radial_r(Iatom,AMO) 
      radial_r_total(Iatom)=radial_r_total(Iatom)+radial_r(Iatom,AMO) 
      radial_r2(Iatom,AMO)=CMOG%occupancy(AMO)*FourPi*radial_r2(Iatom,AMO) 
      radial_r2_total(Iatom)=radial_r2_total(Iatom)+radial_r2(Iatom,AMO) 
      end do ! AMO

      deallocate(radial_r_point)
      deallocate(radial_r2_point)
      return
      end subroutine radial_atomic
      subroutine PRT_NI_radial_AIM
!***********************************************************************
!     Date last modified December 2013                                 *
!     Author: R.A. Poirier                                             *
!     Description: Print the dipole moment results                     *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE QM_objects
      USE NI_radial

      implicit none

! Local scalars:  
      integer :: Iatom,IMO
      integer :: Ilower,Iupper
      logical :: Ldone
!
! Begin:
      write(UNIout,'(a)')'Radial by atom and MO:  <r> (bohr)'
      write(UNIout,'(86("-"))')
      do Iatom=1,Natoms
      if(CARTESIAN(Iatom)%Atomic_number.le.0)cycle
      Ilower=1
      Ldone=.false.
      do
      Iupper=Ilower+6
      if(Iupper.ge.CMO%NoccMO)then
        Iupper=CMO%NoccMO
        Ldone=.true.
      end if
      if(Ilower.eq.1)then
        write(UNIout,'(a,7(i8,2x))')'   Atom,MO:',(IMO, IMO=Ilower,Iupper) 
        write(UNIout,'(i6,6x,10f10.5)')Iatom,(radial_r(Iatom,IMO),IMO=Ilower,Iupper)
      else
        write(UNIout,'(11x,7(i8,2x))')(IMO, IMO=Ilower,Iupper) 
        write(UNIout,'(12x,10f10.5)')(radial_r(Iatom,IMO),IMO=Ilower,Iupper)
      end if
      if(Ldone)then
        write(UNIout,'(a,f10.5)')'Total <r>:  ',radial_r_total(Iatom)
        exit
      end if
      Ilower=Ilower+7
      end do
      end do ! Iatom
      write(UNIout,'(86("-"))')

      write(UNIout,'(a)')'Radial by atom and MO:  <r^2> (bohr^2)'
      write(UNIout,'(86("-"))')
      do Iatom=1,Natoms
      if(CARTESIAN(Iatom)%Atomic_number.le.0)cycle
      Ilower=1
      Ldone=.false.
      do
      Iupper=Ilower+6
      if(Iupper.ge.CMO%NoccMO)then
        Iupper=CMO%NoccMO
        Ldone=.true.
      end if
      if(Ilower.eq.1)then
        write(UNIout,'(a,7(i8,2x))')'   Atom,MO:',(IMO, IMO=Ilower,Iupper)
        write(UNIout,'(i6,6x,10f10.5)')Iatom,(radial_r2(Iatom,IMO),IMO=Ilower,Iupper)
      else
        write(UNIout,'(11x,7(i8,2x))')(IMO, IMO=Ilower,Iupper)
        write(UNIout,'(12x,10f10.5)')(radial_r2(Iatom,IMO),IMO=Ilower,Iupper)
      end if
      if(Ldone)then
        write(UNIout,'(a,f10.5)')'Total <r^2>: ',radial_r2_total(Iatom)
        exit
      end if
      Ilower=Ilower+7
      end do
      end do ! Iatom

      deallocate(radial_r)
      deallocate(radial_r2)

      return
      end subroutine PRT_NI_radial_AIM
      subroutine INI_NI_Radial
!************************************************************************************
!     Date last modified December 10, 2013                                          *
!     Author: R.A. Poirier                                                          *
!     Description: Initialization for the dipole moment (first)                     *
!************************************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE QM_objects
      USE NI_radial

      implicit none

! Input scalars:
      allocate(radial_r(Natoms,CMO%NoccMO))
      allocate(radial_r2(Natoms,CMO%NoccMO))
      allocate(radial_r_total(Natoms))
      allocate(radial_r2_total(Natoms))
      radial_r(1:Natoms,1:CMO%NoccMO)=ZERO
      radial_r2(1:Natoms,1:CMO%NoccMO)=ZERO
      radial_r_total(1:Natoms)=ZERO
      radial_r2_total(1:Natoms)=ZERO

      return
      end subroutine INI_NI_Radial
