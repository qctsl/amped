      subroutine MENU_Numerical
!***********************************************************************
!     Date last modified: February 27, 2015                            *
!     Author: R.A. Poirier                                             *
!     Description: Set up Numerical Integration                        *
!***********************************************************************
! Modules:
      USE program_parser
      USE program_constants
      USE MENU_gets
      USE QM_defaults
      USE NI_defaults

      implicit none
!
! Local scalars:
      integer :: counter,length,Rexp,nlist
      double precision Bound
      logical done
      character(len=MAX_string) string
!
! Begin:
      call PRG_manager ('enter', 'MENU_Numerical', 'UTILITY')
      done=.false.
!
! Initialize counter for the number of Numericals in Numerical_LIST:
      counter = 0

! Menu:
      do while (.not.done)
      call new_token (' Numerical:')
!
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      ' Command Numerical:', &
      '   Purpose: Calculate One-Electron Properties', &
      '   Syntax :', &
      '   ATomic = <logical>, true to print the atomic values', &
      '   BOND = <BOND>, Command to define bonds', &
      '   BOund_density = <real>, boundary value of density', &
      '   BOund_density = 0.050 {default}', &
      '   Grid  <command>, Defining the grid used', &
      '   LDM_type  <string>, Use DOuble or SIngle integral for the defining LDM', &
      '   PArtitioning <command>, Defining the weighting scheme', &
      '   PRoperty = <string>, One electron Property', &
      '   PRoperty = ELectrons or NUmelec, {default}', &
      '   PRoperty = DIpole_moment, ', &
      '   PRoperty = FIrst_moment, ', &
      '   PRoperty = SEcond_moment, ', &
      '   PRoperty = BOnds, ', &
      '   PRoperty = POtential, {example}', &
      '   PRoperty = RAdial, {example}', &
      '   PRoperty = ALL, {example}', &
      '   RADial  <command>, Defining the radial grid used', &
      '   RExp = <integer>, power of r operator', &
      '   RExp = 1, {default, dipole moment}', &
      '   RUpperLimit = <real>  Upper limit of shell integration', &
      '   RLowerLimit = <real>  Lower limit of shell integration', &
      '   end'
!
! Atomic
      else if(token(1:2).eq.'AT')then
         call GET_value (NI_Atomic)
!
! BOND
      else if(token(1:3).EQ.'BON')then
         call MENU_BOND
!
! BOund_density
      else if(token(1:2).EQ.'BO')then
         call GET_value (Bound)
!
! GRid
      else if(token(1:2).EQ.'GR')then
         call MENU_grids
!
! LDM_type
      else if(token(1:3).EQ.'LDM')then
         call GET_value (LDM_QTAIM_type, length)
         if(LDM_QTAIM_type(1:2).eq.'SI')then
           LDM_QTAIM_type(1:)='SINGLE'
         else if(LDM_QTAIM_type(1:2).eq.'DO')then
           LDM_QTAIM_type(1:)='DOUBLE'
         else
           write(UNIout,'(2a)')'Incorrect LDM type should be SIngle or DOuble: ',LDM_QTAIM_type
           stop 'Integration PROPERTY is not available'
         end if
!
! PArtitioningScheme
      else if(token(1:2).EQ.'PA')then
         call MENU_partitioning

! NI_properties
      else if(token(1:2).eq.'PR')then
         call GET_value (NI_property, length)
         if(NI_property(1:2).eq.'EL')then
            NI_property = 'NUMELECTRONS'
         else if(NI_property(1:2).eq.'NU')then
            NI_property = 'NUMELECTRONS'
         else if(NI_property(1:2).eq.'PO')then
            NI_property = 'POTENTIALENERGY'
         else if(NI_property(1:2).eq.'EX')then
            NI_property = 'EXCHANGE'
         else if(NI_property(1:2).eq.'DI')then
            NI_property = 'DIPOLEMO'
         else if(NI_property(1:2).eq.'CO')then
            NI_property = 'COULOMBENERGY'
         else if(NI_property(1:2).eq.'CD')then
            NI_property = 'COUL_DFT'   
         else if(NI_property(1:2).eq.'SE')then
            NI_property = 'SECONDMO'
         else if(NI_property(1:2).eq.'BO')then
            NI_property = 'BONDS'
         else if(NI_property(1:2).eq.'RA')then
            NI_property = 'RADIAL'
         else if(NI_property(1:2).eq.'AL')then
            NI_property = 'ALL'   
         else
           write(UNIout,'(a)')'Integration PROPERTY is not available: ',NI_PROPERTY(1:16)
           stop 'Integration PROPERTY is not available'
         end if
!
! PRoperty
      else if(token(1:2).EQ.'PR')then
         counter = counter + 1
         call GET_value (string, length)

! RADial
      else if(token(1:3).EQ.'RAD')then
         call MENU_RADIAL

! RExp
      else if(token(1:2).EQ.'RE')then
         counter = counter + 1
         call GET_value (Rexp)

! RUpperLimit
      else if(token(1:2).EQ.'RU')then
         call GET_value (RUpperLimit)

! RLowerLimit
      else if(token(1:2).EQ.'RL')then
         call GET_value (RLowerLimit)

      else
         call MENU_end (done)
      end if
!
      end do !(.not.done)

!     call Numerical_Integration
!
! End of routine MENU_Numerical
      call PRG_manager ('exit', 'MENU_Numerical', 'UTILITY')
      return
      end
      subroutine MENU_BOND
!***********************************************************************
!     Date last modified: February 27, 2015                            *
!     Author: R.A. Poirier                                             *
!     Description: Set up Numerical Integration                        *
!***********************************************************************
! Modules:
      USE program_parser
      USE program_constants
      USE type_molecule
      USE MENU_gets
      USE QM_defaults
      USE NI_defaults
      USE type_NI_Nelec_AB

      implicit none
!
! Local scalars:
      integer :: Iatom,Jatom,lenlist,Nbbyte,Nlist
      logical :: done
      parameter (nbbyte=4) ! must be the same as atmlst
!
! Local array:
      character*4 atmlst(2)
!
! Begin:
      call PRG_manager ('enter', 'MENU_BOND', 'UTILITY')
      done=.false.
      do while (.not.done)
      call new_token (' BOND:')
!
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      ' Command Numerical:', &
      '   Purpose: Get properties of bonds', &
      '   Syntax :', &
      '   AToms = ( 1 2 ) or ( C1 C2 )Defines the bond {no default!}', &
      '   ORigin = ( 0.0 0.0 0.0 ) {no default!}', &
      '   end'
!
! AToms
       else if(token(1:2).EQ.'AT')then
        nlist=2
        atmlst(1:2)='    '
        call GET_Clist (atmlst, nbbyte, 2, nlist)
        write(UNIout,'(3a)')' Atoms set to: ',atmlst
        Bond_atoms(1)=0
        Bond_atoms(2)=0
!
      do Iatom=1,nlist
      if(CHK_string(atmlst(Iatom),'+-0123456789'))then
        call CNV_StoI (atmlst(Iatom), Bond_atoms(Iatom))
      else
        do Jatom=1,Natoms
        if(CARTESIAN(Jatom)%element(1:len_trim(CARTESIAN(Jatom)%element)).eq.atmlst(Iatom)(1:len_trim(atmlst(Iatom))))then
          if(CARTESIAN(Jatom)%Atomic_number.le.0)then
            write(UNIout,'(2a)')'ERROR MENU_BOND> Illegal atom:',atmlst(Iatom)
            stop 'ERROR MENU_BOND> Illegal atom'
          end if
          Bond_atoms(Iatom)=Jatom
        end if
        end do ! Jatom
      end if
      if(Bond_atoms(Iatom).le.0.or.Bond_atoms(Iatom).gt.Natoms)then
        write(UNIout,'(2a)')'ERROR MENU_BOND> Illegal atom:',atmlst(Iatom)
        stop 'ERROR MENU_BOND> Illegal atom'
      end if
      end do ! Iatom
      write(UNIout,'(a,2i6)')' Bond between atoms: ',Bond_atoms

!
! ORigin
       else if(token(1:2).EQ.'OR')then
        call GET_value (Bond_origin, lenlist)
        write(UNIout,'(a,3F15.6)')' Origin set to: ',Bond_origin

      else
         call MENU_end (done)
      end if


      end do !(.not.done)
!
! End of routine MENU_BOND
      call PRG_manager ('exit', 'MENU_BOND', 'UTILITY')
      return
      end

