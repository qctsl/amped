      subroutine GET_NI_exchange (Iatom, Ifound)
!************************************************************************************
!     Date last modified Sept 26th ,2001                                            *
!     Author: Aisha                                                                 *
!     Description:                                                                  *
!************************************************************************************
! Modules:
      USE program_constants
      USE QM_objects
      USE N_integration
      USE NI_exchange

      implicit none

! Input scalars:
      integer :: Iatom,Ifound

! Input arrays:

! Local scalars:
      integer :: IApoint
      integer :: AMO,BMO,AB
      double precision :: sum

! Begin: 
      do AMO=1,Nbasis          ! CMO%NoccMO ! Occupied MO only
      do BMO=1,AMO
      AB=AMO*(AMO-1)/2+BMO

! |phi_a(r)*phi_b(r)| MOVE here!
      call GET_density_ab (grid_points, NApts_atom, charge, AMO, BMO)

      sum=ZERO
      do IApoint=1,NApts_atom
        sum=sum+charge(IApoint)*Bweights(IApoint)*grid_points(IApoint)%w
      end do

      Rho_ab(AB)=Rho_ab(AB)+FourPi*sum
 
      end do ! BMO
      end do ! AMO

      return
      end subroutine GET_NI_exchange 
      subroutine PRT_NI_exchange
!***********************************************************************
!     Date last modified December 2013                                 *
!     Author: R.A. Poirier                                             *
!     Description: Print the exchange (MO coincidence)                 *
!***********************************************************************
! Modules:
      USE program_files
      USE QM_objects
      USE matrix_print
      USE NI_exchange

      implicit none

! Local scalars:  

! Begin:
      write(UNIout,'(a)')'Rho_ab(|(phi_a*phi_b)|): Measure of spatial coincidence'
      call PRT_matrix (Rho_ab, MATlen, Nbasis) !CMO%NoccMO)

      deallocate (Rho_ab)

      return
      end subroutine PRT_NI_exchange
      subroutine INI_NI_Exchange
!************************************************************************************
!     Date last modified December 10, 2013                                          *
!     Author: R.A. Poirier                                                          *
!     Description: Initialization for the exchange (Spacial overlap)                *
!************************************************************************************
! Modules:
      USE program_constants
      USE NI_exchange
      USE QM_objects

      implicit none

! Input scalars:
!
! Begin:
      allocate (Rho_ab(1:MATlen))
      Rho_ab=ZERO

      return
      end subroutine INI_NI_Exchange
