      subroutine NI_RAM
!*********************************************************************************************
!     Date last modified: April 24, 2019                                                     *
!     Author: JWH                                                                            *
!     Desciption: Calculate the momentum-balance via integration of its density.             *
!*********************************************************************************************
! Modules:
      USE program_constants
      USE program_timing
      USE program_files
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE module_grid_points
      USE INT_objects

      implicit none
!
! Local scalars:
      integer :: Iatom,IApoint,IAIM,Znum,Ifound
      double precision :: Xpt,Ypt,Zpt
      double precision :: RAMaaatom,RAMHFaaatom
      double precision :: RAMaagridA, RAMHFaagridA
      double precision :: RAMbbatom,RAMHFbbatom
      double precision :: RAMbbgridA, RAMHFbbgridA
      double precision :: RAMabatom,RAMHFabatom
      double precision :: RAMabgridA, RAMHFabgridA
      double precision :: RAMbaatom,RAMHFbaatom
      double precision :: RAMbagridA, RAMHFbagridA
      double precision :: t0, t1, t2, time
!
! Local arrays:
      double precision, dimension(:), allocatable :: WeightsA
!
! Begin:
      call PRG_manager ('enter', 'NI_RAM', 'RAM%NI')
!
      call GET_object ('QM', '2RDM', 'AO')
!
! Get grid and weights
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
      write(UNIout,'(/2a)')'Radial grid used for numerical integration: ',RADIAL_grid
      call BLD_BeckeW_XI
      write(UNIout,'(2a)')'Weights used for numerical integration:      BECKE'
!
      call CPU_time(t0)
      call CPU_time(t1)      
!
! same-spin
      allocate(RAMHFaa_atomic(Natoms),RAMaa_atomic(Natoms))
      allocate(RAMHFbb_atomic(Natoms),RAMbb_atomic(Natoms))
      RAMaa_total=ZERO
      RAMaa_atomic=ZERO
      RAMHFaa_total=ZERO
      RAMHFaa_atomic=ZERO
      RAMbb_total=ZERO
      RAMbb_atomic=ZERO
      RAMHFbb_total=ZERO
      RAMHFbb_atomic=ZERO
!
      write(UNIout,'(/a)')'    Relative angular momentum via numerical integration (same-spin) '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '                            Atomic contributions             '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '                   Hartree-Fock                 Correlated          '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '  Atom       alpha-alpha    beta-beta    alpha-alpha    beta-beta   '
      write(UNIout,'(a)') '--------------------------------------------------------------------'

      if(NAIMprint.eq.0)then ! If no atoms specified, do them all!
        NAIMprint=Natoms
        do Iatom=1,Natoms
          AIMprint(Iatom)=Iatom
        end do ! Iatom
      end if
!
      if(allocated(grid_points))then
        deallocate(grid_points)
      end if
!
      do IAIM=1,NAIMprint
!     do Iatom=1,Natoms
        Iatom=AIMprint(IAIM)
        Znum=CARTESIAN(Iatom)%atomic_number
        if(Znum.le.0)cycle
        Ifound=GRID_loc(Znum)
        NApts_atom=NApoints_atom(Ifound)

        allocate(grid_points(1:NApts_atom))
        allocate(WeightsA(NApts_atom))
!
! Moving grid points on cartesian coordinate grid
        do IApoint=1,NApts_atom
          grid_points(IApoint)%X=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          grid_points(IApoint)%Y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          grid_points(IApoint)%Z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
        end do ! IApoint

        call GET_weights (grid_points, NApts_atom, Iatom, WeightsA)

        RAMaaatom=ZERO
        RAMHFaaatom=ZERO
        RAMbbatom=ZERO
        RAMHFbbatom=ZERO
!

        do IApoint=1,NApts_atom
      !    if(WeightsA(IApoint).le.1.0D-6)cycle ! Makes no difference
          Xpt=grid_points(IApoint)%X
          Ypt=grid_points(IApoint)%Y
          Zpt=grid_points(IApoint)%Z
!
          call GET_RAMDss_point (Xpt, Ypt, Zpt, RAMaagridA, RAMHFaagridA, RAMbbgridA, RAMHFbbgridA)
!
          RAMaaatom = RAMaaatom + RAMaagridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
          RAMHFaaatom = RAMHFaaatom + RAMHFaagridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
          RAMbbatom = RAMbbatom + RAMbbgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
          RAMHFbbatom = RAMHFbbatom + RAMHFbbgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
        end do ! IApoint

        RAMaa_atomic(Iatom) = RAMaa_atomic(Iatom) + RAMaaatom
        RAMHFaa_atomic(Iatom) = RAMHFaa_atomic(Iatom) + RAMHFaaatom
!
        RAMbb_atomic(Iatom) = RAMbb_atomic(Iatom) + RAMbbatom
        RAMHFbb_atomic(Iatom) = RAMHFbb_atomic(Iatom) + RAMHFbbatom
!
        deallocate(grid_points)
        deallocate(WeightsA)

      end do ! Iatom

!
! Now multiply by 4pi and get total exchange
      do IAIM=1,NAIMprint
        Iatom=AIMprint(IAIM)
!     do Iatom=1,Natoms
      Znum=CARTESIAN(Iatom)%atomic_number
      if(Znum.le.0)cycle
!
      RAMaa_atomic(Iatom) = FourPi*RAMaa_atomic(Iatom)
      RAMHFaa_atomic(Iatom) = FourPi*RAMHFaa_atomic(Iatom)
!
      RAMbb_atomic(Iatom) = FourPi*RAMbb_atomic(Iatom)
      RAMHFbb_atomic(Iatom) = FourPi*RAMHFbb_atomic(Iatom)
!
      write(UNIout,'(i8,1x,4f14.8)')Iatom,RAMHFaa_atomic(Iatom),RAMHFbb_atomic(Iatom),&
                                          RAMaa_atomic(Iatom),RAMbb_atomic(Iatom)
!
      RAMaa_total = RAMaa_total + RAMaa_atomic(Iatom)
      RAMHFaa_total = RAMHFaa_total + RAMHFaa_atomic(Iatom)
!
      RAMbb_total = RAMbb_total + RAMbb_atomic(Iatom)
      RAMHFbb_total = RAMHFbb_total + RAMHFbb_atomic(Iatom)
!
      end do ! Iatom
!
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      if(NAIMprint.eq.Natoms)write(UNIout,'(a,1x,4f14.8)')'   Total',RAMHFaa_total,RAMHFbb_total,&
                                                                     RAMaa_total,RAMbb_total
!
      deallocate(RAMaa_atomic,RAMHFaa_atomic)
      deallocate(RAMbb_atomic,RAMHFbb_atomic)
!
! opposite-spin
      allocate(RAMHFab_atomic(Natoms),RAMab_atomic(Natoms))
      allocate(RAMHFba_atomic(Natoms),RAMba_atomic(Natoms))
      RAMab_total=ZERO
      RAMab_atomic=ZERO
      RAMHFab_total=ZERO
      RAMHFab_atomic=ZERO
      RAMba_total=ZERO
      RAMba_atomic=ZERO
      RAMHFba_total=ZERO
      RAMHFba_atomic=ZERO
!
      write(UNIout,'(/a)')' Relative angular momentum via numerical integration (opposite-spin) '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '                            Atomic contributions             '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '                   Hartree-Fock                 Correlated          '
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      write(UNIout,'(a)') '  Atom       alpha-beta    beta-alpha    alpha-beta    beta-alpha   '
      write(UNIout,'(a)') '--------------------------------------------------------------------'

      if(NAIMprint.eq.0)then ! If no atoms specified, do them all!
        NAIMprint=Natoms
        do Iatom=1,Natoms
          AIMprint(Iatom)=Iatom
        end do ! Iatom
      end if

      do IAIM=1,NAIMprint
!     do Iatom=1,Natoms
        Iatom=AIMprint(IAIM)
        Znum=CARTESIAN(Iatom)%atomic_number
        if(Znum.le.0)cycle
        Ifound=GRID_loc(Znum)
        NApts_atom=NApoints_atom(Ifound)

        allocate(grid_points(1:NApts_atom))
        allocate(WeightsA(NApts_atom))
!
! Moving grid points on cartesian coordinate grid
        do IApoint=1,NApts_atom
          grid_points(IApoint)%X=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          grid_points(IApoint)%Y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          grid_points(IApoint)%Z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
        end do ! IApoint

        call GET_weights (grid_points, NApts_atom, Iatom, WeightsA)

        RAMabatom=ZERO
        RAMHFabatom=ZERO
        RAMbaatom=ZERO
        RAMHFbaatom=ZERO
!

        do IApoint=1,NApts_atom
      !    if(WeightsA(IApoint).le.1.0D-6)cycle ! Makes no difference
          Xpt=grid_points(IApoint)%X
          Ypt=grid_points(IApoint)%Y
          Zpt=grid_points(IApoint)%Z
!
          call GET_RAMDos_point (Xpt, Ypt, Zpt, RAMabgridA, RAMHFabgridA, RAMbagridA, RAMHFbagridA)
!
          RAMabatom = RAMabatom + RAMabgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
          RAMHFabatom = RAMHFabatom + RAMHFabgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
          RAMbaatom = RAMbaatom + RAMbagridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
          RAMHFbaatom = RAMHFbaatom + RAMHFbagridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
        end do ! IApoint

        RAMab_atomic(Iatom) = RAMab_atomic(Iatom) + RAMabatom
        RAMHFab_atomic(Iatom) = RAMHFab_atomic(Iatom) + RAMHFabatom
!
        RAMba_atomic(Iatom) = RAMba_atomic(Iatom) + RAMbaatom
        RAMHFba_atomic(Iatom) = RAMHFba_atomic(Iatom) + RAMHFbaatom
!
        deallocate(grid_points)
        deallocate(WeightsA)

      end do ! Iatom

!
! Now multiply by 4pi and get total exchange
      do IAIM=1,NAIMprint
        Iatom=AIMprint(IAIM)
!     do Iatom=1,Natoms
      Znum=CARTESIAN(Iatom)%atomic_number
      if(Znum.le.0)cycle
!
      RAMab_atomic(Iatom) = FourPi*RAMab_atomic(Iatom)
      RAMHFab_atomic(Iatom) = FourPi*RAMHFab_atomic(Iatom)
!
      RAMba_atomic(Iatom) = FourPi*RAMba_atomic(Iatom)
      RAMHFba_atomic(Iatom) = FourPi*RAMHFba_atomic(Iatom)
!
      write(UNIout,'(i8,1x,4f14.8)')Iatom,RAMHFab_atomic(Iatom),RAMHFba_atomic(Iatom),&
                                          RAMab_atomic(Iatom),RAMba_atomic(Iatom)
!
      RAMab_total = RAMab_total + RAMab_atomic(Iatom)
      RAMHFab_total = RAMHFab_total + RAMHFab_atomic(Iatom)
!
      RAMba_total = RAMba_total + RAMba_atomic(Iatom)
      RAMHFba_total = RAMHFba_total + RAMHFba_atomic(Iatom)
!
      end do ! Iatom
!
      write(UNIout,'(a)') '--------------------------------------------------------------------'
      if(NAIMprint.eq.Natoms)write(UNIout,'(a,1x,4f14.8)')'   Total',RAMHFab_total,RAMHFba_total,&
                                                                     RAMab_total,RAMba_total
!
      deallocate(RAMab_atomic,RAMHFab_atomic)
      deallocate(RAMba_atomic,RAMHFba_atomic)
!
! Get analytical value just for sanity check
      call GET_object ('QM', 'RAM', 'ANA')
!
      call CPU_time(t2)
      time = t2 - t1
!
! End of routine NI_RAMD
      call PRG_manager ('exit', 'NI_RAM', 'RAM%NI')
      end subroutine NI_RAM
