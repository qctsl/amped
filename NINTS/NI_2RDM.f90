      subroutine NI_2RDM
!*********************************************************************************************
!     Date last modified: May 29, 2021                                                       *
!     Author: JWH                                                                            *
!     Desciption: Integrate the two-electron density to get the one-electron density at a    *
!                 test point.                                                                *
!*********************************************************************************************
! Modules:
      USE program_constants
      USE program_timing
      USE program_files
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE module_grid_points
      USE INT_objects

      implicit none
!
! Local scalars:
      integer :: Iatom,IApoint,IAIM,Znum,Ifound
      double precision :: Xpt,Ypt,Zpt,Na,Na2,Nb,Nb2,rhoa,rhoHFa,rhob,rhoHFb
      double precision :: TRDMaaatom, TRDMaagridA, TRDMaa_total
      double precision :: TRDMbbatom, TRDMbbgridA, TRDMbb_total
      double precision :: TRDMabatom, TRDMabgridA, TRDMab_total
      double precision :: TRDMbaatom, TRDMbagridA, TRDMba_total
      double precision, allocatable, dimension(:) :: TRDMaa_atomic, TRDMbb_atomic
      double precision, allocatable, dimension(:) :: TRDMab_atomic, TRDMba_atomic
      double precision :: t0, t1, t2, time
      integer :: iAO, aMO
!
! Local arrays:
      double precision, dimension(:), allocatable :: WeightsA
!
! Begin:
      call PRG_manager ('enter', 'NI_2RDM', '2RDM%NI')
!
! Get density matrix
      call GET_object ('QM','SPIN_DENSITY_1MATRIX', 'AO')
!
      call GET_object ('QM', '2RDM', 'MO')
!
! Get grid and weights
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
      write(UNIout,'(/2a)')'Radial grid used for numerical integration: ',RADIAL_grid
      call BLD_BeckeW_XI
      write(UNIout,'(2a)')'Weights used for numerical integration:      BECKE'
!
      call CPU_time(t0)
      call CPU_time(t1)      
!
! same-spin
      allocate(TRDMaa_atomic(Natoms))
      allocate(TRDMbb_atomic(Natoms))
      TRDMaa_total=ZERO
      TRDMaa_atomic=ZERO
      TRDMbb_total=ZERO
      TRDMbb_atomic=ZERO
!
      write(UNIout,'(/a)')'    Numerical Integration of two-electron density    '
      write(UNIout,'(a)') '-----------------------------------------------------'
      write(UNIout,'(a)') '               Atomic contributions             '
      write(UNIout,'(a)') '-----------------------------------------------------'
      write(UNIout,'(a)') '  Atom         alpha-alpha    beta-beta      '
      write(UNIout,'(a)') '-----------------------------------------------------'

      if(NAIMprint.eq.0)then ! If no atoms specified, do them all!
        NAIMprint=Natoms
        do Iatom=1,Natoms
          AIMprint(Iatom)=Iatom
        end do ! Iatom
      end if

      if(allocated(grid_points))then
        deallocate(grid_points)
      end if 
!
      do IAIM=1,NAIMprint
!     do Iatom=1,Natoms
        Iatom=AIMprint(IAIM)
        Znum=CARTESIAN(Iatom)%atomic_number
        if(Znum.le.0)cycle
        Ifound=GRID_loc(Znum)
        NApts_atom=NApoints_atom(Ifound)

        if(allocated(grid_points)) deallocate(grid_points)
        allocate(grid_points(1:NApts_atom))
        allocate(WeightsA(NApts_atom))
!
! Moving grid points on cartesian coordinate grid
        do IApoint=1,NApts_atom
          grid_points(IApoint)%X=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          grid_points(IApoint)%Y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          grid_points(IApoint)%Z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
        end do ! IApoint
!
        call GET_weights (grid_points, NApts_atom, Iatom, WeightsA)
!
        TRDMaaatom=ZERO
        TRDMbbatom=ZERO
!
        do IApoint=1,NApts_atom
          Xpt=grid_points(IApoint)%X
          Ypt=grid_points(IApoint)%Y
          Zpt=grid_points(IApoint)%Z
!
          call GET_2RDM_point (Xpt, Ypt, Zpt, test_pt, TRDM_MOaa, CMO%coeff, TRDMaagridA)
          TRDMaaatom = TRDMaaatom + TRDMaagridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
          call GET_2RDM_point (Xpt, Ypt, Zpt, test_pt, TRDM_MObb, CMO%coeff, TRDMbbgridA)
          TRDMbbatom = TRDMbbatom + TRDMbbgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
        end do ! IApoint
!
        TRDMaa_atomic(Iatom) = TRDMaa_atomic(Iatom) + TRDMaaatom
        TRDMbb_atomic(Iatom) = TRDMbb_atomic(Iatom) + TRDMbbatom
!
        deallocate(grid_points)
        deallocate(WeightsA)

      end do ! Iatom

!
! Now multiply by 4pi and get total exchange
      do IAIM=1,NAIMprint
        Iatom=AIMprint(IAIM)
!     do Iatom=1,Natoms
      Znum=CARTESIAN(Iatom)%atomic_number
      if(Znum.le.0)cycle
!
      TRDMaa_atomic(Iatom) = FourPi*TRDMaa_atomic(Iatom)
      TRDMbb_atomic(Iatom) = FourPi*TRDMbb_atomic(Iatom)
!
      write(UNIout,'(i6,5x,2f14.8)')Iatom,TRDMaa_atomic(Iatom),TRDMbb_atomic(Iatom)
!
      TRDMaa_total = TRDMaa_total + TRDMaa_atomic(Iatom)
      TRDMbb_total = TRDMbb_total + TRDMbb_atomic(Iatom)
!
      end do ! Iatom
!
      write(UNIout,'(a)') '-----------------------------------------------------'
      if(NAIMprint.eq.Natoms)write(UNIout,'(a,1x,2f14.8)')' Total    ',TRDMaa_total,TRDMbb_total
!
      deallocate(TRDMaa_atomic)
      deallocate(TRDMbb_atomic)
!
! opposite-spin
      allocate(TRDMab_atomic(Natoms))
      allocate(TRDMba_atomic(Natoms))
      TRDMab_total=ZERO
      TRDMab_atomic=ZERO
      TRDMba_total=ZERO
      TRDMba_atomic=ZERO
!
      write(UNIout,'(/a)')'    Numerical Integration of two-electron density    '
      write(UNIout,'(a)') '-----------------------------------------------------'
      write(UNIout,'(a)') '               Atomic contributions             '
      write(UNIout,'(a)') '-----------------------------------------------------'
      write(UNIout,'(a)') '  Atom         alpha-beta     beta-alpha     '
      write(UNIout,'(a)') '-----------------------------------------------------'
!
      if(NAIMprint.eq.0)then ! If no atoms specified, do them all!
        NAIMprint=Natoms
        do Iatom=1,Natoms
          AIMprint(Iatom)=Iatom
        end do ! Iatom
      end if

      do IAIM=1,NAIMprint
!     do Iatom=1,Natoms
        Iatom=AIMprint(IAIM)
        Znum=CARTESIAN(Iatom)%atomic_number
        if(Znum.le.0)cycle
        Ifound=GRID_loc(Znum)
        NApts_atom=NApoints_atom(Ifound)

        allocate(grid_points(1:NApts_atom))
        allocate(WeightsA(NApts_atom))
!
! Moving grid points on cartesian coordinate grid
        do IApoint=1,NApts_atom
          grid_points(IApoint)%X=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          grid_points(IApoint)%Y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          grid_points(IApoint)%Z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
        end do ! IApoint

        call GET_weights (grid_points, NApts_atom, Iatom, WeightsA)

        TRDMabatom=ZERO
        TRDMbaatom=ZERO
!

        do IApoint=1,NApts_atom
      !    if(WeightsA(IApoint).le.1.0D-6)cycle ! Makes no difference
          Xpt=grid_points(IApoint)%X
          Ypt=grid_points(IApoint)%Y
          Zpt=grid_points(IApoint)%Z
!
          call GET_2RDM_point (Xpt, Ypt, Zpt, test_pt, TRDM_MOab, CMO%coeff, TRDMabgridA)
          TRDMabatom = TRDMabatom + TRDMabgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
          call GET_2RDM_point (Xpt, Ypt, Zpt, test_pt, TRDM_MOba, CMO%coeff, TRDMbagridA)
          TRDMbaatom = TRDMbaatom + TRDMbagridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
        end do ! IApoint
!
        TRDMab_atomic(Iatom) = TRDMab_atomic(Iatom) + TRDMabatom
        TRDMba_atomic(Iatom) = TRDMba_atomic(Iatom) + TRDMbaatom
!
        deallocate(grid_points)
        deallocate(WeightsA)

      end do ! Iatom

!
! Now multiply by 4pi and get total exchange
      do IAIM=1,NAIMprint
        Iatom=AIMprint(IAIM)
!     do Iatom=1,Natoms
      Znum=CARTESIAN(Iatom)%atomic_number
      if(Znum.le.0)cycle
!
      TRDMab_atomic(Iatom) = FourPi*TRDMab_atomic(Iatom)
      TRDMba_atomic(Iatom) = FourPi*TRDMba_atomic(Iatom)
!
      write(UNIout,'(i6,5x,2f14.8)')Iatom,TRDMab_atomic(Iatom),TRDMba_atomic(Iatom)
!
      TRDMab_total = TRDMab_total + TRDMab_atomic(Iatom)
      TRDMba_total = TRDMba_total + TRDMba_atomic(Iatom)
!
      end do ! Iatom
!
      write(UNIout,'(a)') '-----------------------------------------------------'
      if(NAIMprint.eq.Natoms)write(UNIout,'(a,1x,2f14.8)')' Total    ',TRDMab_total,TRDMba_total
!
      deallocate(TRDMab_atomic)
      deallocate(TRDMba_atomic)
!
! Get analytical value just for sanity check
! Density at a point
      call GET_spin_den_point (test_pt(1), test_pt(2), test_pt(3),&
                               rhoa, rhoHFa, rhob, rhoHFb)
!
      NAelectrons = (Nelectrons + Multiplicity - 1)/2
      NBelectrons = Nelectrons - NAelectrons
!
! Modify electron count if excluding cores
      if(.not.LRDMcore)then
        Ncore = NFrozen_cores()
        NAelectrons = NAelectrons - Ncore
        NBelectrons = NBelectrons - Ncore
      end if 
!
      Na2 = dble(NAelectrons - 1)/2.0D0
      Nb = dble(NBelectrons)/2.0D0
      Nb2 = dble(NBelectrons - 1)/2.0D0
      Na = dble(NAelectrons)/2.0D0
!
      write(UNIout,'(/a)')'    alpha one-electron density at the test point     '
      write(UNIout,'(a)') '       (multiplied by electron-pair factor)          '
      write(UNIout,'(a)') '-----------------------------------------------------'
      write(UNIout,'(a)') '          (Na - 1)/2                   Nb/2          '
      write(UNIout,'(a)') '-----------------------------------------------------'
      write(UNIout,'(7x,f14.8,10x,f14.8)')Na2*rhoa, Nb*rhoa
      write(UNIout,'(a)') '-----------------------------------------------------'
!
      write(UNIout,'(/a)')'    beta one-electron density at the test point     '
      write(UNIout,'(a)') '       (multiplied by electron-pair factor)          '
      write(UNIout,'(a)') '-----------------------------------------------------'
      write(UNIout,'(a)') '          (Nb - 1)/2                   Na/2          '
      write(UNIout,'(a)') '-----------------------------------------------------'
      write(UNIout,'(7x,f14.8,10x,f14.8)')Nb2*rhob, Na*rhob
      write(UNIout,'(a)') '-----------------------------------------------------'
!
!
      call CPU_time(t2)
      time = t2 - t1
!
! End of routine NI_2RDM
      call PRG_manager ('exit', 'NI_2RDM', '2RDM%NI')
      end subroutine NI_2RDM
