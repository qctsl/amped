      subroutine CFT_CISD
!***********************************************************************
!     Date last modified: May 7, 2024                                  *
!     Author: I. Press and J.W. Hollett                                *
!     Description: Determine the CISD eigenfunctions and eigenvalues.  *
!***********************************************************************
! Modules:
      USE program_files
      USE CFT_objects
      USE QM_objects
      USE INT_objects
      USE type_molecule
      USE program_constants

      implicit none
!
! Local scalars:
      integer :: aNO, pNO, qNO, Na, Nb, Nall, Naconf, Nbconf, Iaconf, Ibconf
      double precision :: Vnn, E_II, onap, onbp, onbq
!      
! Local arrays:
      integer, allocatable, dimension(:) :: NO_all
      integer, allocatable, dimension(:,:) :: occa, occb
! 
! Functions:
      double precision :: GET_Enuclear
      integer :: bico
!
! Begin:
!
      call PRG_manager('enter','CFT_CISD','UTILITY')
!
! Get RHF wavefunction
      call GET_object ('CFT', 'NO', 'RHF')
      NO = CMO%coeff
!
! Get necessary integrals of NOs (natural orbitals [more general than molecular orbitals])
! OEIs
      call GET_object ('INT', '1EINT', 'AO')
      if(allocated(h_NO))then
        deallocate(h_NO)
      end if
      allocate(h_NO(Nbasis,Nbasis))
      call I1E_transform (Hcore, NO, h_NO, Nbasis, Nbasis, MATlen)
!
! Create list for all orbitals
      allocate(NO_all(Nbasis))
      do aNO = 1, Nbasis
        NO_all(aNO) = aNO
      end do
      Nall = Nbasis
!
      if(allocated(eriNO))then
        deallocate(eriNO)
      end if
      allocate(eriNO(Nbasis**4))
!
      call CFT_gen_TEItrans(NO_all,Nall,NO_all,Nall,NO_all,Nall,NO_all,Nall,&
                             NO,Nbasis,eriAO,eriNO)
!
      if(allocated(J_NO))then
       deallocate(J_NO)
      end if
      if(allocated(K_NO))then
        deallocate(K_NO)
      end if
      allocate(J_NO(Nbasis,Nbasis),K_NO(Nbasis,Nbasis))
!
      call CFT_gen_BLD_J(NO_all,Nall,NO_all,Nall,eriNO,J_NO,Nbasis)
      call CFT_gen_BLD_K(NO_all,Nall,NO_all,Nall,eriNO,K_NO,Nbasis)
!
! Nuclear repulsion energy
      Vnn = GET_Enuclear()
!
! Numbers of electrons (to start, it will be one of each)
      Na = NAelectrons
      Nb = NBelectrons
!
! Number of possible occupancy configurations
      Naconf = bico(Nbasis,Na)
      Nbconf = bico(Nbasis,Nb)
!
! Note: the total number of determinants (configurations) will be Naconf*Nbconf
!
! Occupancies (first index -> configuration, second index -> orbital)
      allocate(occa(Naconf,Nbasis), occb(Nbconf,Nbasis))
!
! Set all configurations to that of the ground state
      ! occupied
      occa(1:Naconf,1:Na) = 1
      occb(1:Nbconf,1:Nb) = 1
      ! vacant  
      occa(1:Naconf,Na+1:Nbasis) = 0
      occb(1:Nbconf,Nb+1:Nbasis) = 0
!
! Now, just as a demonstration we could calculate the energy of a config (or diagonal of the CI matrix)
! Let's choose Iaconf = 1 and Ibconf = 1
!
      Iaconf = 1
      Ibconf = 1
!
      E_II = 0.0D0
      do pNO = 1, Nbasis
        onap = dble(occa(Iaconf,pNO))
        onbp = dble(occb(Ibconf,pNO))
        E_II = E_II + (onap + onbp)*h_NO(pNO,pNO) ! one-electron energy
        do qNO = 1, pNO ! no double-counting
          onbq = dble(occb(Ibconf,qNO))
          E_II = E_II + (onap*onbp)*J_NO(pNO,qNO) ! two-electron energy
        end do ! qNO
      end do ! pNO
      E_II = E_II + Vnn
!
      write(6,*)'E_II = ', E_II
!
! End of routine CFT_CISD
      call PRG_manager('exit','CFT_CISD','UTILITY')
      end subroutine CFT_CISD
