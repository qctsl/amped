      subroutine MENU_CFT
!***********************************************************************
!     Date last modified: March 29, 2016                               *
!     Author: JWH                                                      *
!     Description: Set up CFT calculation.                             *
!***********************************************************************
!Modules:
      USE CFT_objects
      USE QM_objects
      USE program_parser
      USE MENU_gets
      USE CCSD_objects ! needs to be moved, with CC stuff set in its own menu

      implicit none
!
! Local scalars:
      logical done,lrun
      integer cnvlen,lenstr,nbbyte,nlist
      integer, parameter :: MAX_string=128
      character(len=MAX_string) string
      character(len=MAX_string) command
      integer, dimension(:), allocatable :: NOpair, pair_sign
      integer :: INO, RNO
!
! Begin:
      call PRG_manager ('enter', 'MENU_CFT', 'UTILITY')
!
      allocate(NOpair(2),pair_sign(3),Qinput(Nbasis,Nbasis),sigin(Nbasis,Nbasis))
!
! Defaults:
      done=.false.
      lrun=.false.
      LQin=.false.
      Lsigin=.false.
      cnvlen=9
      Qinput = 0
      call SET_defaults_CFT
!
! Menu:
      do while (.not.done)
      call new_token (command)
!
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      ' Command CFT:', &
      '   Purpose: Determine NOs and ONs ', &
      '   Syntax :', &
      '     NOfile     = <string>  : filename for guess NO coefficients', &
      '     Deltafile  = <string>  : filename for guess Delta values', &
      '     optmethod  = <string>  : method used for 2-RDM optimization: simul or alter ',&
      '     dynamic    = <string>  : functional used for short-range dynamic correlation',&
      '     NOguess    = <string>  : guess natural orbitals',&
      '     Deltaguess = <string>  : guess deltas',&
      '     pair       = <string list>: ( 1 8 ) occupied NO 1 and virtual NO 8',&
      '     sigma      = <string list>: ( 1 8 ) occupied NO 1 and virtual NO 8',&
      '     nudge      = <string>  : option to nudge optimization off saddle points: true or false',&
      '     varTDF    = <string>  : option to determine universal correlation variationally: true or false',&
      '     Nupts      = <integer> : number of u grid pts (in one direction) for universal correlation', &
      '     uscale     = <real*8>  : scaling parameter for u grid for universal correlation',&
      '     RUN command: Will cause execution', &
      '   end'
!
! NOfile
      else if(token(1:3).EQ.'NOF')then
        call GET_value (string, lenstr)
        NOfile = string
!
! optmethod
      else if(token(1:3).EQ.'OPT')then
        call GET_value (string, lenstr)
        optmethod = string
!
! saddle point nudge
      else if(token(1:3).EQ.'NUD')then
        call GET_value (string, lenstr)
        if(string.eq.'FALSE')then 
          Lnudge = .false. ! true by default
        end if
!
! variational TDF energy
      else if(token(1:3).EQ.'VAR')then
        call GET_value (string, lenstr)
        if(string.eq.'TRUE')then 
          LTDFvar = .true. ! false by default
        end if
!
! CF parameters
      else if(token(1:3).EQ.'DYN')then
        call GET_value (string, lenstr)
        dynamic = string
        !If CC then set LCCsing
        if(dynamic.eq.'CCSD')then
          LCCsing = .true.
        else if (dynamic.eq.'CCD')then
          LCCsing = .false.
        end if
!
! Dynamic functional parameters
!
      else if(token(1:3).EQ.'USC')then
        call GET_value (u_scale)
!
      else if(token(1:2).EQ.'NU')then
        call GET_value (N_u_pts)
!
! Input statically correlated orbital pairs
!
      else if(token(1:4).EQ.'PAIR')then
        nlist = 2
        nbbyte = 4
        LQin = .true.
        call GET_Ilist (NOpair, nlist)
        !write(6,*) 'NOpair(1): ',NOpair(1)
        !write(6,*) 'NOpair(2): ',NOpair(2)
        write(UNIout,'(a,i3,x,i3)') 'Input NO pair: ',NOpair(1), NOpair(2)
        Qinput(NOpair(1),NOpair(2)) = 1
!
! Alter the sign of pair interaction
!
      else if(token(1:3).EQ.'SIG')then
        nlist = 3
        Lsigin = .true.
        call GET_Ilist (pair_sign, nlist)
        !write(6,*) 'pair_sign(1): ',pair_sign(1)
        !write(6,*) 'pair_sign(2): ',pair_sign(2)
        !write(6,*) 'pair_sign(3): ',pair_sign(3)
        write(UNIout,'(a,i3,x,i3,a,i3)') 'Input sigma(',pair_sign(1), pair_sign(2),')= ',pair_sign(3)
        sigin(pair_sign(1),pair_sign(2)) = pair_sign(3)
!
! Guess NOs and Deltas
      else if(token(1:3).EQ.'NOG')then
        call GET_value (string, lenstr)
        NO_guess = string
!
      else if(token(1:3).EQ.'DEL')then
        call GET_value (string, lenstr)
        Delta_guess = string
!
      else if(token(1:3).EQ.'RUN')then
        lrun=.true.
      else
        call MENU_end (done)
      end if
 
      end do !(.not.done)
!
      if(lrun)then
        call GET_object ('CFT', 'ENERGY', 'DELTANO')
      end if
!
! End of routine MENU_CFT
      call PRG_manager ('exit', 'MENU_CFT', 'UTILITY')
      return
      end
