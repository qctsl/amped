      MODULE CFT_objects
!**************************************************************************************************
!     Date last modified: March 29, 2016                                                          *
!     Author: JWH                                                                                 *
!     Description: CFT objects                                                                   *
!**************************************************************************************************
      implicit none
!
! CFT input
      character(len=128) :: NOfile, Deltafile ! input data
      character(len=128) :: NO_guess, Delta_guess ! read or from RHF, UHF, MOM,...
      character(len=128) :: optmethod
      integer, dimension(:,:), allocatable :: Qinput, sigin
      logical :: LQin, Lsigin
!
! CFT coefficients, occupancies, vacancies
      double precision, dimension(:,:), allocatable :: P_NO, Delta, eta, zeta, xi, sigma
      double precision, dimension(:,:), allocatable :: Pcl_NO, Pop_NO
      double precision, dimension(:), allocatable :: PFeigval
!      double precision, dimension(:), allocatable :: on, vn
      double precision, dimension(:,:), allocatable :: kappa
      integer, dimension(:,:), allocatable :: stat_pair, occ_stat, vac_stat, Qstat, coup_pair, vacshr
      integer, dimension(:), allocatable :: coupNO
      integer, dimension(:), allocatable :: Nstat_occ, Nstat_vac, NO_act, occ_froz, vac_froz, Nvacshr
      integer, dimension(:), allocatable :: NO_occ, NO_vac
      integer, dimension(:), allocatable :: occ_act, vac_act, occ_val, vac_val, NO_closed, NO_open
      integer :: Nstat, Nocc, Nvac, Nact, Nclosed, Nopen, Nocc_froz, Nvac_froz
      integer :: Nocc_val, Nvac_val, NcoreNO, Nocc_act, Nvac_act, Ncoup
!
! damping for post-HF like dynamic correlation energy
      double precision, dimension(:,:,:,:), allocatable :: doovv
      double precision, dimension(:,:,:), allocatable :: doov, dovv
      double precision, dimension(:,:), allocatable :: doo, dvv, dov, pairw
      double precision, dimension(:), allocatable :: dSo, dSv
!
! Orbital pair potential 
      double precision, dimension(:,:), allocatable :: pair_pot
!
! CF parameters
      double precision :: epsstat
      character(len=128) :: dynamic ! name of dynamic correlation functional
      logical :: LTDFvar ! variational or post-DeltaNO TDF evaluation
!
! Energy components
      double precision :: E_CFT, E_CFT_HF, E_CFT_stat, E_CFT_pair, E_CFT_dyn
      double precision :: E_CFT_T, E_CFT_VNe, E_CFT_Vee
!
! Optimization scalars
      double precision :: damp, epsON, epslambda, epsE, max_grad_length, max_lambda
      integer :: max_RDMstep, max_NOstep, max_ONstep, max_NOONstep, NOONstep, ONstep
      logical :: Ldiagstall, LHSCon, Lnudge
      double precision :: SCFconv
      integer :: maxSCF
!
! Integrals over NOs
      double precision, allocatable, dimension(:,:) :: H_NO, T_NO, V_NO, FockNO
      double precision, allocatable, dimension(:,:) :: Gcl_AO, Gop_AO, Goc_AO
      double precision, allocatable, dimension(:,:) :: Gcl_NO, Gop_NO, Goc_NO
      double precision, allocatable, dimension(:,:) :: lambda, lambdap, J_NO, K_NO, M_NO
      double precision, allocatable, dimension(:) :: eriNO, mbNO
!
! Optimization arrays
      double precision, allocatable, dimension(:) :: theta, grad_theta, grad_Delta, kapthe
      double precision, allocatable, dimension(:,:) :: hessian, Acoup, NOinit
      double precision, allocatable, dimension(:,:,:) :: dxi, dzeta, deta
      double precision, allocatable, dimension(:,:,:) :: dkappa, donon
      double precision, allocatable, dimension(:,:,:,:) :: d2xi, d2zeta
      double precision, allocatable, dimension(:,:,:,:) :: d2kappa
      double precision, allocatable, dimension(:,:,:) :: dR2RDMaa, dR2RDMbb
      double precision, allocatable, dimension(:,:,:) :: dR2RDMJab, dR2RDMLab
      double precision, allocatable, dimension(:,:,:) :: dR2RDMJba, dR2RDMLba
      double precision, allocatable, dimension(:,:,:) :: dR2RDMJos, dR2RDMLos
      double precision, allocatable, dimension(:,:,:,:) :: d2R2RDMaa, d2R2RDMbb
      double precision, allocatable, dimension(:,:,:,:) :: d2R2RDMJab, d2R2RDMLab
      double precision, allocatable, dimension(:,:,:,:) :: d2R2RDMJba, d2R2RDMLba
      double precision, allocatable, dimension(:,:,:,:) :: d2R2RDMJos, d2R2RDMLos
      double precision, allocatable, dimension(:,:,:,:) :: chessJ, chessL
      double precision, allocatable, dimension(:,:,:) :: chessrho
      double precision, allocatable, dimension(:,:) :: chessJ4i, chessK4i
      double precision, allocatable, dimension(:) :: tdf_grad
      double precision, allocatable, dimension(:,:) :: tdf_hess
!
! EPUM
      double precision, allocatable, dimension(:,:) :: Ahess
      integer, allocatable, dimension(:,:) :: orbpr, orbpr_co, orbpr_cv, orbpr_ca, orbpr_ov
      integer, allocatable, dimension(:,:) :: orbpr_oa, orbpr_va, orbpr_aa
      double precision, allocatable, dimension(:) :: wgrad, tdf_wgrad, orbopen
      integer :: Npair, Npair_co, Npair_cv, Npair_ca, Npair_ov, Npair_oa, Npair_va, Npair_aa
!
! Numerical integration objects (dynamic correlation)
      double precision, allocatable, dimension(:,:) :: ERpotNO, sr0OE, LRSR
      double precision, allocatable, dimension(:,:) :: dECSdD_atom, d2ECSdD2_atom
      double precision, allocatable, dimension(:) :: E_CS_atom, dECSdD, d2ECSdD2
      double precision, allocatable, dimension(:,:) :: dEOSECdD_atom, d2EOSECdD2_atom
      double precision, allocatable, dimension(:) :: E_OSEC_atom, dEOSECdD, d2EOSECdD2
      double precision, allocatable, dimension(:) :: Vee_OSEC_atom, VNe_OSEC_atom
      double precision, allocatable, dimension(:,:) :: dEFHCdD_atom, d2EFHCdD2_atom
      double precision, allocatable, dimension(:,:) :: lambda_OSEC, lambdap_OSEC
      double precision, allocatable, dimension(:,:) :: lambda_FHC, lambdap_FHC
      double precision, allocatable, dimension(:) :: E_FHCaa_atom, E_FHCbb_atom, dEFHCdD, d2EFHCdD2
      double precision, allocatable, dimension(:) :: E_OSEC_norm_atom, rhoNO, ontopw1D
      double precision, allocatable, dimension(:,:) :: ontopw_atom, ontopw
      double precision, allocatable, dimension(:) :: E_AC_atom
      double precision :: E_CS, E_OSEC, E_FHCaa, E_FHCbb, E_OSEC_norm, E_AC
      double precision :: Vee_OSEC, VNe_OSEC
      double precision :: aOS, qOS, aFHC, qFHC, cDC, qCS, Vfac, aAC
      double precision :: dAC, pAC
      double precision :: E_TDF, E_ODF, u_scale
      integer :: N_u_pts
      double precision, parameter :: F1_2 = 0.5D0
      double precision, parameter :: F3_2 = 1.5D0
      double precision, parameter :: F3_4 = 0.75D0
      double precision, parameter :: F1_4 = 1.0D0/4.0D0
      double precision, parameter :: F1_5 = 1.0D0/5.0D0
      double precision, parameter :: F1_6 = 1.0D0/6.0D0
      double precision, parameter :: F1_7 = 1.0D0/7.0D0
      double precision, parameter :: F1_8 = 1.0D0/8.0D0
      double precision, parameter :: F5_6 = 5.0D0/6.0D0
      double precision, parameter :: F2_9 = 2.0D0/9.0D0
      double precision, parameter :: F11_6 = 11.0D0/6.0D0
      double precision, parameter :: F2_3 = 2.0D0/3.0D0
      double precision, parameter :: F4_3 = 4.0D0/3.0D0
      double precision, parameter :: F5_3 = 5.0D0/3.0D0
      double precision, parameter :: F8_3 = 8.0D0/3.0D0
      double precision, parameter :: SIX =  6.0D0
      double precision, parameter :: TEN = 10.0D0
      double precision, parameter :: N12 = 12.0D0
      double precision, parameter :: N18 = 18.0D0
      double precision, parameter :: N20 = 20.0D0
      double precision, parameter :: N24 = 24.0D0
      double precision, parameter :: TOL1 = 1.0D-10
      double precision, parameter :: TOL2 = 1.0D-16
!
      end MODULE CFT_objects
