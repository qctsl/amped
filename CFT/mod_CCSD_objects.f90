      MODULE CCSD_objects
!**************************************************************************************************
!     Date last modified: October 25, 2018                                                        *
!     Author: JWH                                                                                 *
!     Description: CCSD objects                                                                   *
!**************************************************************************************************
      implicit none
!
      double precision                                  :: E_CCSD, maxCCSDres
      double precision, allocatable, dimension(:)       :: onso, vnso
      double precision, allocatable, dimension(:,:)     :: t1, r1, foo, fov, fvv, d1eso, d1Xso
      double precision, allocatable, dimension(:,:)     :: d1bkooso, d1bkvvso
      double precision, allocatable, dimension(:,:,:,:) :: t2, r2, eri
      double precision, allocatable, dimension(:,:,:,:) :: oooo, ooov, oovv, ovov, ovvo, ovvv, vvvv, d2so
!
      integer :: maxCCSDiter, Nso_occ, Nso_vac
      integer, allocatable, dimension(:) :: so_occ, so_vac
!
      logical :: LCCsing
!
        
      end MODULE CCSD_objects
