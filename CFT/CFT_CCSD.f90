      subroutine CFT_CCSD_energy
!*****************************************************************
!     Date last modified: September 18, 2018                     *
!     Author: JWH                                                *
!     Description: Calculate coupled-cluster singles and doubles *
!                  energy for the statically correlated Delta-NO * 
!                  reference.                                    *
!*****************************************************************
!Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE CFT_objects
      USE QM_objects
      USE CCSD_objects

      implicit none
!
! Local scalars: 
!
      integer :: CCSDiter, max_diis, n_diis, Nt1, Nt2, maxr1pos(2), maxr2pos(4)
      double precision :: Resmax
!
! Local arrays:
!
      double precision, allocatable, dimension(:,:) :: ttot_diis, rtot_diis
!
! Begin:
      call PRG_manager ('enter', 'CFT_CCSD_energy', 'ENERGY%CCSD')
!
! Get orbtials and occupancies
      call GET_object('CFT','ENERGY','DELTANO')
!
! Create lists of occupied and vacant orbitals
      call CFT_BLD_occvac_list
!
! Convert lists to spin orbitals 
      call CFT_BLD_so_lists
!
! Calculate orbital vacancy
      call CFT_BLD_vacancy
!
! Calculate occupations and vacancies of spin orbitals 
      call CFT_BLD_so_occvac
!
! Get necessary Fock matrix elements and ERIs
      call CFT_BLD_CCSD_OTEI
!
! Calculate damping factors for TEIs, which correct for double-counting and avoid degeneracy poles
      call CFT_BLD_so_damp
!
      write(UNIout,'(/a)')'              CCSD correlation energy'
      write(UNIout,'(a)')'----------------------------------------------------------'
      write(UNIout,'(a)')' iteration              E_c                  max residual '
      write(UNIout,'(a)')'----------------------------------------------------------'
!
      allocate(t1(Nso_occ,Nso_vac),t2(Nso_occ,Nso_occ,Nso_vac,Nso_vac))
      allocate(r1(Nso_occ,Nso_vac),r2(Nso_occ,Nso_occ,Nso_vac,Nso_vac))
!
! Set-up DIIS
      max_diis = 10
      Nt1 = Nso_occ*Nso_vac
      Nt2 = Nt1**2
      allocate(ttot_diis(Nt1+Nt2,max_diis),rtot_diis(Nt1+Nt2,max_diis))
      n_diis = 0
      ttot_diis = ZERO
      rtot_diis = ZERO
!
! Begin CCSD iterations
      Resmax = 66.0D0 
      CCSDiter = 0
      t1 = ZERO 
      t2 = ZERO 
!
      do while((dabs(Resmax).gt.maxCCSDres).and.(CCSDiter.le.maxCCSDiter))
!
! Perform DIIS extrapolation
        if(CCSDiter.ge.1)then ! unlike SCF we have no guess error vector, so do an iteration first
          n_diis = min(n_diis+1,max_diis)
          ! Interpolate t1 and t2
          call CFT_CCSD_DIIS(Nt1,Nt2,n_diis,rtot_diis,ttot_diis,r1,r2,t1,t2) 
        end if 
!
! Calculate residual
!
        call CFT_BLD_r1r2_CCSD
!
! Update amplitudes
!
        call CFT_t1t2_CCSD_step
!
! Calculate the energy
!
        call CFT_E_CCSD
! 
! Get maximum residual
        if(maxval(dabs(r1)).gt.maxval(dabs(r2)))then
          maxr1pos = maxloc(dabs(r1))
          Resmax = r1(maxr1pos(1),maxr1pos(2))
        else
          maxr2pos = maxloc(dabs(r2))
          Resmax = r2(maxr2pos(1),maxr2pos(2),maxr2pos(3),maxr2pos(4))
        end if
!
        CCSDiter = CCSDiter + 1
!
        write(UNIout,'(3X,I3,9X,F16.10,10X,F16.10)')CCSDiter,E_CCSD,Resmax
!
      end do ! CCSD iterations
!
! Print CCSD energy components
! 
!      call CFT_E_CCSD_comp
!
! end routine CFT_CCSD_energy
      call PRG_manager ('exit', 'CFT_CCSD_energy', 'ENERGY%CCSD')
      end subroutine CFT_CCSD_energy 
      subroutine CFT_BLD_CCSD_OTEI
!***********************************************************************
!     Date last modified: October 22, 2018                             *
!     Author: JWH                                                      *
!     Description: Build Fock matrix and two-electron integrals over   *
!                  NOs.                                                *
!***********************************************************************
! Modules:
      USE CFT_objects
      USE CCSD_objects
      USE QM_objects
      USE INT_objects
      USE type_molecule
      USE program_constants
      USE program_files

      implicit none
!
! Local scalars:
      integer :: a, b, c, d
      integer :: Nall, abcd_index
! Local arrays:
      integer, dimension(:), allocatable :: NO_all
!
! Begin:
!
      call PRG_manager('enter','CFT_BLD_CCSD_OTEI','UTILITY')
!
      if(allocated(FockNO))then
        deallocate(FockNO)
      end if
      allocate(FockNO(Nbasis,Nbasis))
!
      FockNO = ZERO
!
!
! Let's just get all TEIs for now and worry about making smaller transformation later
      Nall = Nbasis
      if(allocated(NO_all))then
        deallocate(NO_all)
      end if
      allocate(NO_all(Nall))
!
      do a = 1, Nall
        NO_all(a) = a
      end do
!
! Get the necessary integrals
      ! Transform to NO basis
      call CFT_gen_TEItrans(NO_all,Nall,NO_all,Nall,NO_all,Nall,NO_all,Nall,&
                             NO,Nbasis,eriAO,eriNO)
!
      if(allocated(eri))then
        deallocate(eri)
      end if
      allocate(eri(Nbasis,Nbasis,Nbasis,Nbasis))
!
! Copy ERIs to 4D array
      abcd_index = 0
      do a = 1, Nbasis
        do b = 1, Nbasis
          do c = 1, Nbasis
            do d = 1, Nbasis
              abcd_index = abcd_index + 1
              eri(a,b,c,d) = eriNO(abcd_index)
            end do ! d
          end do ! c
        end do ! b
      end do ! a
!
! Need G for Fock matrix
      call CFT_BLD_Gmatrix
! Transform to NO basis
      call CFT_2D_transform(Gcl_AO, NO, Gcl_NO, Nbasis, Nbasis)
      call CFT_2D_transform(Gop_AO, NO, Gop_NO, Nbasis, Nbasis)
      call CFT_2D_transform(Goc_AO, NO, Goc_NO, Nbasis, Nbasis)
!
! and h
      if(allocated(H_NO))then
        deallocate(H_NO)
      end if
      allocate(H_NO(Nbasis,Nbasis))
      call I1E_transform (Hcore, NO, H_NO, Nbasis, Nbasis, MATlen)
!
      call CFT_BLD_red_lambda 
!
      FockNO = lambda
!
! Now create antisymmetrized integrals over spin-orbitals
! Better to do this once at the beginning
!
      allocate(oooo(Nso_occ,Nso_occ,Nso_occ,Nso_occ),ooov(Nso_occ,Nso_occ,Nso_occ,Nso_vac))
      allocate(oovv(Nso_occ,Nso_occ,Nso_vac,Nso_vac),ovov(Nso_occ,Nso_vac,Nso_occ,Nso_vac))
      allocate(ovvo(Nso_occ,Nso_vac,Nso_vac,Nso_occ),ovvv(Nso_occ,Nso_vac,Nso_vac,Nso_vac))
      allocate(vvvv(Nso_vac,Nso_vac,Nso_vac,Nso_vac))
!
      call CFT_CCSD_BLD_aseri(so_occ,Nso_occ,so_occ,Nso_occ,so_occ,Nso_occ,so_occ,Nso_occ,oooo)
      call CFT_CCSD_BLD_aseri(so_occ,Nso_occ,so_occ,Nso_occ,so_occ,Nso_occ,so_vac,Nso_vac,ooov)
      call CFT_CCSD_BLD_aseri(so_occ,Nso_occ,so_occ,Nso_occ,so_vac,Nso_vac,so_vac,Nso_vac,oovv)
      call CFT_CCSD_BLD_aseri(so_occ,Nso_occ,so_vac,Nso_vac,so_occ,Nso_occ,so_vac,Nso_vac,ovov)
      call CFT_CCSD_BLD_aseri(so_occ,Nso_occ,so_vac,Nso_vac,so_vac,Nso_vac,so_occ,Nso_occ,ovvo)
      call CFT_CCSD_BLD_aseri(so_occ,Nso_occ,so_vac,Nso_vac,so_vac,Nso_vac,so_vac,Nso_vac,ovvv)
      call CFT_CCSD_BLD_aseri(so_vac,Nso_vac,so_vac,Nso_vac,so_vac,Nso_vac,so_vac,Nso_vac,vvvv)
!
      allocate(foo(Nso_occ,Nso_occ),fov(Nso_occ,Nso_vac),fvv(Nso_vac,Nso_vac))
!
      call CFT_CCSD_BLD_fso(so_occ,Nso_occ,so_occ,Nso_occ,foo)
      call CFT_CCSD_BLD_fso(so_occ,Nso_occ,so_vac,Nso_vac,fov)
      call CFT_CCSD_BLD_fso(so_vac,Nso_vac,so_vac,Nso_vac,fvv)
!
! end routine CFT_BLD_CCSD_OTEI
      call PRG_manager ('exit', 'CFT_BLD_CCSD_OTEI', 'UTILITY')
      end subroutine CFT_BLD_CCSD_OTEI
      subroutine CFT_BLD_r1r2_CCSD
!******************************************************************
!     Date last modified: October 22, 2018                        *
!     Author: JWH                                                 *
!     Description: Calculate residuals of t1 and t2 equations for *
!                  non-canonical CCSD.                            *
!******************************************************************
!Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE CFT_objects
      USE CCSD_objects
      USE QM_objects
      USE INT_objects
!
      implicit none
!
! Local scalars: 
!
      integer :: a, b, e, f, i, j, m, n
      integer :: Nv2, Nov, No2
      double precision :: tol
!
! Local arrays:
!
      double precision, allocatable, dimension(:,:,:,:) :: taut, tau, cWvvvv, cWoooo, cWovvo, x4, y4, z4
      double precision, allocatable, dimension(:,:,:,:) :: t2cn, ttcn, r2cn, tcn
      double precision, allocatable, dimension(:,:) :: cFvv, cFoo, cFov, mFoo, mFvv, x2, y2
!
! Begin:
      call PRG_manager ('enter', 'CFT_BLD_r1r2_CCSD', 'UTILITY')
!
      tol = 1.0d-16
!
      No2 = Nso_occ**2
      Nv2 = Nso_vac**2
      Nov = Nso_occ*Nso_vac
!
      allocate(taut(Nso_occ,Nso_occ,Nso_vac,Nso_vac),tau(Nso_occ,Nso_occ,Nso_vac,Nso_vac))
      allocate(cWvvvv(Nso_vac,Nso_vac,Nso_vac,Nso_vac),cWoooo(Nso_occ,Nso_occ,Nso_occ,Nso_occ))
      allocate(cWovvo(Nso_occ,Nso_vac,Nso_vac,Nso_occ))
      allocate(cFvv(Nso_vac,Nso_vac),cFoo(Nso_occ,Nso_occ),cFov(Nso_occ,Nso_vac))
      allocate(mFvv(Nso_vac,Nso_vac),mFoo(Nso_occ,Nso_occ))
!
! Equations from Stanton, JCP 1991, 94, 4334
! Now modified according to White and Chan, JCTC 2018, ASAP (FT-CCSD)
!
! Define intermediates
!
! tau and taut
!
      do i = 1, Nso_occ
        do j = 1, Nso_occ
          do a = 1, Nso_vac
            do b = 1, Nso_vac
!
              taut(i,j,a,b)  = t2(i,j,a,b) + F1_2*(t1(i,a)*t1(j,b) - t1(i,b)*t1(j,a))
              tau(i,j,a,b)   = t2(i,j,a,b) + t1(i,a)*t1(j,b) - t1(i,b)*t1(j,a)
!
            end do ! bvac
          end do ! avac
        end do ! jocc
      end do ! iocc
!
! For t1 equation
!
! cF (curly F's)
!
! Fae (vv)
!
      do a = 1, Nso_vac
        cFvv(a,1:Nso_vac) = vnso(a)*fvv(a,1:Nso_vac)
      end do ! a
!
      allocate(x2(Nso_vac,Nso_vac))
!
      call dgemm('t','n',Nso_vac,Nso_vac,Nso_occ,-F1_2,t1,Nso_occ,fov,Nso_occ,ZERO,x2,Nso_vac)
      cFvv = cFvv + x2
!
! Reshape for contraction
      allocate(x4(Nso_occ,Nso_vac,Nso_vac,Nso_vac))
      do f = 1, Nso_vac
        do a = 1, Nso_vac
!
          x4(1:Nso_occ,f,a,1:Nso_vac) = vnso(a)*ovvv(1:Nso_occ,a,f,1:Nso_vac)
! 
        end do
      end do
!
      call dgemm('t','n',Nv2,1,Nov,ONE,x4,Nov,t1,Nov,ZERO,x2,Nv2)
      cFvv = cFvv + x2
!      
      deallocate(x2,x4)
      allocate(x4(Nso_vac,Nso_vac,Nso_vac,Nso_vac))
!
      call dgemm('t','n',Nv2,Nv2,No2,-F1_2,taut,No2,oovv,No2,ZERO,x4,Nv2)
      do e = 1, Nso_vac
        do a = 1, Nso_vac
          do f = 1, Nso_vac
            cFvv(a,e) = cFvv(a,e) + x4(a,f,e,f)
          end do 
        end do 
      end do! 
!
      deallocate(x4)
!
! Fmi (oo)
!
      do i = 1, Nso_occ
        !cFoo(1:Nso_occ,i) = onso(i)*foo(1:Nso_occ,i)
        ! For Delta NO lambda
        cFoo(1:Nso_occ,i) = onso(i)*foo(i,1:Nso_occ)
      end do ! i
!
      allocate(x2(Nso_occ,Nso_occ))
!
      call dgemm('n','t',Nso_occ,Nso_occ,Nso_vac,F1_2,fov,Nso_occ,t1,Nso_occ,ZERO,x2,Nso_occ)
      cFoo = cFoo + x2
!
! Reshape for contraction
      allocate(x4(Nso_occ,Nso_occ,Nso_occ,Nso_vac))
      do n = 1, Nso_occ
        do i = 1, Nso_occ
!
          x4(1:Nso_occ,i,n,1:Nso_vac) = onso(i)*ooov(1:Nso_occ,n,i,1:Nso_vac)
! 
        end do
      end do
!
      call dgemm('n','n',No2,1,Nov,ONE,x4,No2,t1,Nov,ZERO,x2,No2)
      cFoo = cFoo + x2
!
      deallocate(x2,x4) 
!
      allocate(x4(Nso_occ,Nso_occ,Nso_occ,Nso_occ))
!
      call dgemm('n','t',No2,No2,Nv2,F1_2,oovv,No2,taut,No2,ZERO,x4,No2)
      do i = 1, Nso_occ
        do m = 1, Nso_occ
          do n = 1, Nso_occ
            cFoo(m,i) = cFoo(m,i) + x4(m,n,i,n)
          end do 
        end do 
      end do
!
      deallocate(x4)
!
! Fme (ov)
!
      cFov = fov
!
! Reshape for contraction
      allocate(x4(Nso_occ,Nso_vac,Nso_occ,Nso_vac))
        do e = 1, Nso_vac
          do n = 1, Nso_occ
!
              x4(1:Nso_occ,e,n,1:Nso_vac) = oovv(1:Nso_occ,n,e,1:Nso_vac)
! 
        end do
      end do
!
      allocate(x2(Nso_occ,Nso_vac))
!
      call dgemm('n','n',Nov,1,Nov,ONE,x4,Nov,t1,Nov,ZERO,x2,Nov)
      cFov = cFov + x2
!
      deallocate(x4,x2)
!
! For t2 equation
!
! cW (curly W's)
!
! Wmnij (oooo)
!
      do j = 1, Nso_occ
        do i = 1, Nso_occ
!
! damping
          cWoooo(1:Nso_occ,1:Nso_occ,i,j) = onso(i)*onso(j)*oooo(1:Nso_occ,1:Nso_occ,i,j)
!          cWoooo(1:Nso_occ,1:Nso_occ,i,j) = onso(i)*onso(j)*oooo(1:Nso_occ,1:Nso_occ,i,j)*doo(i,j)
!
        end do ! i
      end do ! j
!
      allocate(y2(Nso_occ,Nso_vac))
      allocate(x2(Nso_occ,Nso_occ))
      do n = 1, Nso_occ
        do m = 1, Nso_occ
          do i = 1, Nso_occ
            y2(i,1:Nso_vac) = onso(i)*ooov(m,n,i,1:Nso_vac)
          end do
          call dgemm('n','t',Nso_occ,Nso_occ,Nso_vac,ONE,y2,Nso_occ,t1,Nso_occ,ZERO,x2,Nso_occ)
          cWoooo(m,n,1:Nso_occ,1:Nso_occ) = cWoooo(m,n,1:Nso_occ,1:Nso_occ) + x2 - transpose(x2)
        end do
      end do
      deallocate(x2,y2)
!
      allocate(x4(Nso_occ,Nso_occ,Nso_occ,Nso_occ)) 
!
      call dgemm('n','t',No2,No2,Nv2,F1_4,oovv,No2,tau,No2,ZERO,x4,No2)
      cWoooo = cWoooo + x4
!
      deallocate(x4)
!
! Wabef (vvvv)
!
      do b = 1, Nso_vac
        do a = 1, Nso_vac
!
! damping
          cWvvvv(a,b,1:Nso_vac,1:Nso_vac) = vnso(a)*vnso(b)*vvvv(a,b,1:Nso_vac,1:Nso_vac)
!          cWvvvv(a,b,1:Nso_vac,1:Nso_vac) = vnso(a)*vnso(b)*vvvv(a,b,1:Nso_vac,1:Nso_vac)&
!                                             *dvv(a,b) 
!
        end do ! a 
      end do ! b 
!
      allocate(y2(Nso_vac,Nso_occ))
      allocate(x2(Nso_vac,Nso_vac))
      do e = 1, Nso_vac
        do f = 1, Nso_vac
          do a = 1, Nso_vac
            y2(a,1:Nso_occ) = vnso(a)*ovvv(1:Nso_occ,a,f,e)
          end do 
          call dgemm('n','n',Nso_vac,Nso_vac,Nso_occ,ONE,y2,Nso_vac,t1,Nso_occ,ZERO,x2,Nso_vac)
          cWvvvv(1:Nso_vac,1:Nso_vac,e,f) = cWvvvv(1:Nso_vac,1:Nso_vac,e,f) - x2 + transpose(x2)
        end do
      end do
      deallocate(x2,y2)
!
      allocate(x4(Nso_vac,Nso_vac,Nso_vac,Nso_vac)) 
!
      call dgemm('t','n',Nv2,Nv2,No2,F1_4,tau,No2,oovv,No2,ZERO,x4,Nv2)
      cWvvvv = cWvvvv + x4
!
      deallocate(x4)
!
! Wmbej (ovvo)
!
      do j = 1, Nso_occ
        do b = 1, Nso_vac
!
! damping
          cWovvo(1:Nso_occ,b,1:Nso_vac,j) = vnso(b)*onso(j)*ovvo(1:Nso_occ,b,1:Nso_vac,j)
!          cWovvo(1:Nso_occ,b,1:Nso_vac,j) = vnso(b)*onso(j)*ovvo(1:Nso_occ,b,1:Nso_vac,j)*dov(j,b)
!
        end do ! b 
      end do ! j
!
      allocate(y2(Nso_occ,Nso_occ))
      allocate(x2(Nso_occ,Nso_vac))
      do e = 1, Nso_vac
        do j = 1, Nso_occ
          y2 = onso(j)*ooov(1:Nso_occ,1:Nso_occ,j,e)
          call dgemm('t','n',Nso_occ,Nso_vac,Nso_occ,ONE,y2,Nso_occ,t1,Nso_occ,ZERO,x2,Nso_occ)
          cWovvo(1:Nso_occ,1:Nso_vac,e,j) = cWovvo(1:Nso_occ,1:Nso_vac,e,j) - x2
        end do
      end do
      deallocate(x2,y2)
!
      allocate(y2(Nso_vac,Nso_vac))
      allocate(x2(Nso_vac,Nso_occ))
      do b = 1, Nso_vac
        do m = 1, Nso_occ
          y2 = vnso(b)*ovvv(m,b,1:Nso_vac,1:Nso_vac)
          call dgemm('n','t',Nso_vac,Nso_occ,Nso_vac,ONE,y2,Nso_vac,t1,Nso_occ,ZERO,x2,Nso_vac)
          cWovvo(m,b,1:Nso_vac,1:Nso_occ) = cWovvo(m,b,1:Nso_vac,1:Nso_occ) + x2
        end do
      end do
      deallocate(x2,y2)
!
      allocate(x4(Nso_occ,Nso_vac,Nso_vac,Nso_occ))
      allocate(y4(Nso_occ,Nso_vac,Nso_vac,Nso_occ))
      allocate(tcn(Nso_vac,Nso_occ,Nso_vac,Nso_occ))
!
! reshape arrays for multiplication
      do f = 1, Nso_vac
        do e = 1, Nso_vac
          do n = 1, Nso_occ
            do m = 1, Nso_occ
              y4(m,e,f,n) = oovv(m,n,e,f)
              tcn(f,n,e,m) = F1_2*t2(m,n,f,e) + t1(m,f)*t1(n,e)
            end do ! m
          end do ! n
        end do ! e
      end do ! f
!
      call dgemm('n','n',Nov,Nov,Nov,ONE,y4,Nov,tcn,Nov,ZERO,x4,Nov)
!
      do j = 1, Nso_occ
        do b = 1, Nso_vac
          do a = 1, Nso_vac
            do i = 1, Nso_occ
              cWovvo(i,a,b,j) = cWovvo(i,a,b,j) - x4(i,b,a,j)
            end do ! i
          end do ! j
        end do ! b
      end do ! a
!
      deallocate(x4,y4,tcn)
!
! New t2 intermediates
!
! mFbe (vv)
      mFvv = cFvv
      allocate(x2(Nso_vac,Nso_vac))
      call dgemm('t','n',Nso_vac,Nso_vac,Nso_occ,F1_2,t1,Nso_occ,cFov,Nso_occ,ZERO,x2,Nso_vac)
      mFvv = mFvv - x2
      deallocate(x2)
!
! mFmj (oo)
      mFoo = cFoo
      allocate(x2(Nso_occ,Nso_occ))
      call dgemm('n','t',Nso_occ,Nso_occ,Nso_vac,F1_2,cFov,Nso_occ,t1,Nso_occ,ZERO,x2,Nso_occ)
      mFoo = mFoo + x2
      deallocate(x2)
!
! t1 equation
!
      r1 = ZERO
!
      if(LCCsing)then
!
      do a = 1, Nso_vac
        do i = 1, Nso_occ 
        ! damping
          r1(i,a) = onso(i)*vnso(a)*fov(i,a)
          !r1(i,a) = onso(i)*vnso(a)*fov(i,a)*dov(i)
          !r1(i,a) = onso(i)*vnso(a)*fov(i,a)*dSo(i)*dSv(a)*dSov(i,a)
        end do ! i
      end do ! a
!
      allocate(x2(Nso_occ,Nso_vac))
      call dgemm('n','t',Nso_occ,Nso_vac,Nso_vac,ONE,t1,Nso_occ,cFvv,Nso_vac,ZERO,x2,Nso_occ)
      r1 = r1 + x2
      call dgemm('t','n',Nso_occ,Nso_vac,Nso_occ,ONE,cFoo,Nso_occ,t1,Nso_occ,ZERO,x2,Nso_occ)
      r1 = r1 - x2
      deallocate(x2)
!
      allocate(tcn(Nso_occ,Nso_vac,Nso_occ,Nso_vac))
      allocate(x2(Nso_occ,Nso_vac))
      do e = 1, Nso_vac
        do a = 1, Nso_vac
          do m = 1, Nso_occ
            do i = 1, Nso_occ
              tcn(i,a,m,e) = t2(i,m,a,e)
            end do
          end do
        end do
      end do
!      
      call dgemm('n','n',Nov,1,Nov,ONE,tcn,Nov,cFov,Nov,ZERO,x2,Nov)
      r1 = r1 + x2
      deallocate(tcn,x2)
!
      allocate(x4(Nso_occ,Nso_vac,Nso_occ,Nso_vac))
      allocate(x2(Nso_occ,Nso_vac))
      do f = 1, Nso_vac
        do i = 1, Nso_occ
          do a = 1, Nso_vac
            do n = 1, Nso_occ
            ! damping
              x4(i,a,n,f) = onso(i)*vnso(a)*ovov(n,a,i,f)
              !x4(i,a,n,f) = onso(i)*vnso(a)*ovov(n,a,i,f)*dov(i,a)
            end do
          end do
        end do
      end do
!      
      call dgemm('n','n',Nov,1,Nov,ONE,x4,Nov,t1,Nov,ZERO,x2,Nov)
      r1 = r1 - x2
      deallocate(x4,x2)
!
      allocate(x4(Nso_vac,Nso_vac,Nso_occ,Nso_vac))
      call dgemm('t','n',Nv2,Nov,No2,F1_2,t2,No2,ooov,No2,ZERO,x4,Nv2)
      do i = 1, Nso_occ
        do a = 1, Nso_vac
          do e = 1, Nso_vac
            r1(i,a) = r1(i,a) - onso(i)*x4(a,e,i,e)
          end do 
        end do 
      end do 
      deallocate(x4)
!
      allocate(x4(Nso_occ,Nso_occ,Nso_occ,Nso_vac))
      call dgemm('n','t',No2,Nov,Nv2,F1_2,t2,No2,ovvv,Nov,ZERO,x4,No2)
      do i = 1, Nso_occ
        do a = 1, Nso_vac
          do m = 1, Nso_occ
            r1(i,a) = r1(i,a) - vnso(a)*x4(i,m,m,a)
          end do 
        end do
      end do
      deallocate(x4)
!
! Maybe there is a smarter way to do this, but r(p,p) needs to be zero
      do i = 1, Nso_occ
        do a = 1, Nso_vac
          if(so_occ(i).eq.so_vac(a))then ! same spin orbital (check CCSD again for H2)
          !if((so_occ(i)+1)/2.eq.(so_vac(a)+1)/2)then ! spatial orbital
            r1(i,a) = ZERO
          end if
        end do ! a
      end do ! i
!
! Zero residues corresponding to excitations from active orbitals
      do i = 1, Nso_occ
        r1(i,1:Nso_vac) = dSo(i)*r1(i,1:Nso_vac)
      end do
! Zero residues corresponding to excitations to active orbitals
!      do a = 1, Nso_vac
!        r1(1:Nso_occ,a) = dSv(a)*r1(1:Nso_occ,a)
!      end do
!
      end if ! LCCsing
!
! t2 equation
!
      do b = 1, Nso_vac
        do a = 1, Nso_vac
          do j = 1, Nso_occ
            do i = 1, Nso_occ
            ! damping
              r2(i,j,a,b) = onso(i)*onso(j)*vnso(a)*vnso(b)*oovv(i,j,a,b)*doovv(i,j,a,b)
            end do ! i
          end do ! j
        end do ! b
      end do ! a
!
      allocate(y2(Nso_vac,Nso_vac))
      allocate(x2(Nso_vac,Nso_vac))
      do j = 1, Nso_occ
        do i = 1, Nso_occ
          y2 = t2(i,j,1:Nso_vac,1:Nso_vac)
          call dgemm('n','t',Nso_vac,Nso_vac,Nso_vac,ONE,y2,Nso_vac,mFvv,Nso_vac,ZERO,x2,Nso_vac)
          r2(i,j,1:Nso_vac,1:Nso_vac) = r2(i,j,1:Nso_vac,1:Nso_vac) + x2 - transpose(x2)
        end do
      end do
      deallocate(x2,y2)
!
      allocate(y2(Nso_occ,Nso_occ))
      allocate(x2(Nso_occ,Nso_occ))
      do b = 1, Nso_vac
        do a = 1, Nso_vac
          y2 = t2(1:Nso_occ,1:Nso_occ,a,b)
          call dgemm('n','n',Nso_occ,Nso_occ,Nso_occ,ONE,y2,Nso_occ,mFoo,Nso_occ,ZERO,x2,Nso_occ)
          r2(1:Nso_occ,1:Nso_occ,a,b) = r2(1:Nso_occ,1:Nso_occ,a,b) - x2 + transpose(x2)
        end do
      end do
      deallocate(x2,y2)
!
      allocate(y2(Nso_occ,Nso_vac))
      allocate(x2(Nso_vac,Nso_vac))
      do j = 1, Nso_occ
        do i = 1, Nso_occ
          do b = 1, Nso_vac
          ! damping
            y2(1:Nso_occ,b) = onso(i)*onso(j)*vnso(b)*ooov(i,j,1:Nso_occ,b)
            !y2(1:Nso_occ,b) = onso(i)*onso(j)*vnso(b)*ooov(i,j,1:Nso_occ,b)&
            !                 *doov(i,j,b)
          end do ! b
          call dgemm('t','n',Nso_vac,Nso_vac,Nso_occ,ONE,t1,Nso_occ,y2,Nso_occ,ZERO,x2,Nso_vac)
          r2(i,j,1:Nso_vac,1:Nso_vac) = r2(i,j,1:Nso_vac,1:Nso_vac) - x2 + transpose(x2)
        end do
      end do
      deallocate(x2,y2)
!
      allocate(y2(Nso_occ,Nso_vac))
      allocate(x2(Nso_occ,Nso_occ))
      do a = 1, Nso_vac
        do b = 1, Nso_vac
          do j = 1, Nso_occ
          ! damping
            y2(j,1:Nso_vac) = onso(j)*vnso(a)*vnso(b)*ovvv(j,1:Nso_vac,b,a)
            !y2(j,1:Nso_vac) = onso(j)*vnso(a)*vnso(b)*ovvv(j,1:Nso_vac,b,a)&
            !                  *dovv(j,a,b)
          end do ! j
          call dgemm('n','t',Nso_occ,Nso_occ,Nso_vac,ONE,t1,Nso_occ,y2,Nso_occ,ZERO,x2,Nso_occ)
          r2(1:Nso_occ,1:Nso_occ,a,b) = r2(1:Nso_occ,1:Nso_occ,a,b) + x2 - transpose(x2)
        end do
      end do
      deallocate(x2,y2)
!
      allocate(x4(Nso_occ,Nso_occ,Nso_vac,Nso_vac))
      call dgemm('t','n',No2,Nv2,No2,F1_2,cWoooo,No2,tau,No2,ZERO,x4,No2)
      r2 = r2 + x4
      deallocate(x4)
!
! Used to be slowest step
      allocate(x4(Nso_occ,Nso_occ,Nso_vac,Nso_vac))
      call dgemm('n','t',No2,Nv2,Nv2,F1_2,tau,No2,cWvvvv,Nv2,ZERO,x4,No2)
      r2 = r2 + x4
      deallocate(x4)
!
! Need alternate order (chemist notation) for the following contractions
      allocate(x4(Nso_occ,Nso_vac,Nso_occ,Nso_vac))
      allocate(y4(Nso_occ,Nso_vac,Nso_occ,Nso_vac))
      allocate(z4(Nso_occ,Nso_vac,Nso_occ,Nso_vac))
      allocate(t2cn(Nso_occ,Nso_vac,Nso_occ,Nso_vac))
      allocate(ttcn(Nso_occ,Nso_vac,Nso_occ,Nso_vac))
      allocate(r2cn(Nso_occ,Nso_vac,Nso_occ,Nso_vac))
!
      do j = 1, Nso_occ
        do e = 1, Nso_vac
          do b = 1, Nso_vac
            do m = 1, Nso_occ
              y4(m,e,j,b) = cWovvo(m,b,e,j)
              ! damping
              z4(m,e,j,b) = onso(j)*vnso(b)*ovvo(m,b,e,j)
              !z4(m,e,j,b) = onso(j)*vnso(b)*ovvo(m,b,e,j)*dov(j,b)
              t2cn(m,b,j,e) = t2(m,j,b,e)
              ttcn(m,b,j,e) = -t1(m,e)*t1(j,b)
            end do ! m
          end do ! b
        end do ! e
      end do ! j
!
      call dgemm('n','n',Nov,Nov,Nov,ONE,t2cn,Nov,y4,Nov,ZERO,r2cn,Nov)
      call dgemm('n','n',Nov,Nov,Nov,ONE,ttcn,Nov,z4,Nov,ZERO,x4,Nov)
      r2cn = r2cn + x4
!
      do b = 1, Nso_vac
        do a = 1, Nso_vac
          do j = 1, Nso_occ
            do i = 1, Nso_occ
              r2(i,j,a,b) = r2(i,j,a,b) + r2cn(i,a,j,b)&
                                        - r2cn(i,b,j,a)&
                                        - r2cn(j,a,i,b)&
                                        + r2cn(j,b,i,a)
            end do ! i
          end do ! j
        end do ! b
      end do ! a
!
! Zero excitations to same spin orbital
!
      do i = 1, Nso_occ
        do a = 1, Nso_vac
          if(so_occ(i).eq.so_vac(a))then ! same spin orbital
          !if((so_occ(i)+1)/2.eq.(so_vac(a)+1)/2)then ! same spatial orbital
            r2(i,1:Nso_occ,a,1:Nso_vac) = ZERO
            r2(1:Nso_occ,i,1:Nso_vac,a) = ZERO
            r2(1:Nso_occ,i,a,1:Nso_vac) = ZERO
            r2(i,1:Nso_occ,1:Nso_vac,a) = ZERO
          end if
        end do ! a
      end do ! i
      do i = 1, Nso_occ
        r2(i,i,1:Nso_vac,1:Nso_vac) = ZERO
      end do ! i
      do a = 1, Nso_vac
        r2(1:Nso_occ,1:Nso_occ,a,a) = ZERO
      end do ! i
!
! Zero residuals for excitations that have been removed by damping
!
      do b = 1, Nso_vac
        do a = 1, Nso_vac
          do j = 1, Nso_occ
            do i = 1, Nso_occ
              if(onso(i)*onso(j)*vnso(a)*vnso(b)*doovv(i,j,a,b).lt.tol)then
                r2(i,j,a,b) = ZERO 
              end if
            end do ! i
          end do ! j
        end do ! b
      end do ! a
!
! end routine CFT_BLD_r1r2_CCSD
      call PRG_manager ('exit', 'CFT_BLD_r1r2_CCSD', 'UTILITY')
      end subroutine CFT_BLD_r1r2_CCSD
      subroutine CFT_t1t2_CCSD_step
!******************************************************************
!     Date last modified: October 25, 2018                        *
!     Author: JWH                                                 *
!     Description: Update the t1 and t2 amplitudes.               *
!******************************************************************
!Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE CFT_objects
      USE CCSD_objects
      USE QM_objects
      USE INT_objects

      implicit none
!
! Local scalars: 
!
      integer :: a, b, i, j
!
! Local arrays:
!
! Begin:
      call PRG_manager ('enter', 'CFT_t1t2_CCSD_step', 'UTILITY')
!
      do i = 1, Nso_occ
        do a = 1, Nso_vac
          if(so_occ(i).eq.so_vac(a))cycle ! same spin orbital
!          if((so_occ(i)+1)/2.eq.(so_vac(a)+1)/2)cycle ! same spatial orbital
!
          if(LCCsing)then 
!          if(mod(i,2).eq.mod(a,2))then ! conserve spin
            t1(i,a) = t1(i,a) + r1(i,a)/(onso(i)*foo(i,i) - vnso(a)*fvv(a,a))
!          end if
          endif ! LCCsing
!
          do j = 1, Nso_occ
!            if(i.eq.j)cycle
            if(so_occ(j).eq.so_vac(a))cycle ! same spin orbital
            !if((so_occ(j)+1)/2.eq.(so_vac(a)+1)/2)cycle ! same spatial orbital
            do b = 1, Nso_vac
!              if(a.eq.b)cycle
              if(so_occ(j).eq.so_vac(b))cycle ! same spin orbital
              if(so_occ(i).eq.so_vac(b))cycle ! same spin orbital
              !if((so_occ(i)+1)/2.eq.(so_vac(b)+1)/2)cycle ! same spatial orbital
              !if((so_occ(j)+1)/2.eq.(so_vac(b)+1)/2)cycle ! same spatial orbital
!
              t2(i,j,a,b) = t2(i,j,a,b)&
              + r2(i,j,a,b)/(onso(i)*foo(i,i) + onso(j)*foo(j,j) - vnso(a)*fvv(a,a) - vnso(b)*fvv(b,b))
!
            end do ! bvac
          end do ! avac
        end do ! jocc
      end do ! iocc
!
! end routine CFT_t1t2_CCSD_step
      call PRG_manager ('exit', 'CFT_t1t2_CCSD_step', 'UTILITY')
      end subroutine CFT_t1t2_CCSD_step
      subroutine CFT_E_CCSD
!******************************************************************
!     Date last modified: October 25, 2018                        *
!     Author: JWH                                                 *
!     Description: Calculate CCSD energy.                         *
!******************************************************************
!Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE CFT_objects
      USE CCSD_objects
      USE QM_objects
      USE INT_objects

      implicit none
!
! Local scalars: 
!
      integer :: a, b, i, j
!
! Local arrays:
!
!
! Begin:
      call PRG_manager ('enter', 'CFT_E_CCSD', 'UTILITY')
!
      E_CCSD = ZERO
      do i = 1, Nso_occ
        do a = 1, Nso_vac
!
          E_CCSD = E_CCSD + t1(i,a)*fov(i,a)
!
          do j = 1, Nso_occ
            do b = 1, Nso_vac
!
              E_CCSD = E_CCSD + F1_2*(F1_2*t2(i,j,a,b) + t1(i,a)*t1(j,b))*oovv(i,j,a,b)
!
            end do ! bvac
          end do ! avac
        end do ! jocc
      end do ! iocc
!
! end routine CFT_E_CCSD
      call PRG_manager ('exit', 'CFT_E_CCSD', 'UTILITY')
      end subroutine CFT_E_CCSD
      subroutine CFT_BLD_so_lists
!********************************************************************** 
!     Date last modified: October 25, 2018                            *
!     Author: JWH                                                     *
!     Description: Create lists of occupied and vacant spin orbitals.*
!**********************************************************************
!Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE CFT_objects
      USE QM_objects
      USE INT_objects
      USE CCSD_objects

      implicit none
!
! Local scalars: 
!
      integer :: iocc, avac
!
!
! Local arrays:
!
!
! Begin:
      call PRG_manager ('enter', 'CFT_BLD_so_lists', 'UTILITY')
!
! CC is computed over spin-orbitals (easier implementation)
! Closed-shell for now
      Nso_occ = 2*Nocc
      Nso_vac = 2*Nvac
!
      allocate(so_occ(Nso_occ),so_vac(Nso_vac))
!
      do iocc = 1, Nocc
        so_occ(2*iocc - 1) = 2*no_occ(iocc) - 1
        so_occ(2*iocc)     = 2*no_occ(iocc)
      end do ! iocc
      do avac = 1, Nvac
        so_vac(2*avac - 1) = 2*no_vac(avac) - 1
        so_vac(2*avac)     = 2*no_vac(avac)
      end do ! avac
!
!      write(6,*)' occupied spin orbitals '
!      do iocc = 1, Nso_occ
!        write(6,*)so_occ(iocc)
!      end do 
!      write(6,*)' vacant spin orbitals '
!      do avac = 1, Nso_vac
!        write(6,*)so_vac(avac)
!      end do 
!
! end routine CFT_BLD_so_lists
      call PRG_manager ('exit', 'CFT_BLD_so_lists', 'UTILITY')
      end subroutine CFT_BLD_so_lists
      subroutine CFT_BLD_so_damp
!********************************************************************** 
!     Date last modified: November 30, 2018                           *
!     Author: JWH                                                     *
!     Description: Calculate damping factors for integrals over spin- *
!                  orbitals.                                          *
!**********************************************************************
!Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE CFT_objects
      USE QM_objects
      USE INT_objects
      USE CCSD_objects

      implicit none
!
! Local scalars: 
!
      integer :: i, j, a, b
      integer :: pact, p, pocc, pvac, po, pv, poa, pob, pva, pvb
      integer :: q, qocc, qvac, qo, qv, qoa, qob, qva, qvb
      integer :: io, iv, ioa, iob, iva, ivb, iocc
      integer :: jo, jv, joa, job, jva, jvb, jocc
!
      double precision :: foo_opp, foo_par, fov_opp, fov_par, kappat
      double precision :: oopq, vvpq, xisum, zetacon
!
! Local arrays:
      integer, allocatable, dimension(:) :: p2occ, p2vac, oact, vact
      double precision, allocatable, dimension(:,:,:,:) :: doovvI
      double precision, allocatable, dimension(:,:) :: alphaoo,alphavv
      double precision, allocatable, dimension(:,:) :: betaoo,betavv,betaov
!
! Begin:
      call PRG_manager ('enter', 'CFT_BLD_so_damp', 'UTILITY')
!
      allocate(alphaoo(Nso_occ,Nso_occ),alphavv(Nso_vac,Nso_vac))
      allocate(betaoo(Nso_occ,Nso_occ),betavv(Nso_vac,Nso_vac),betaov(Nso_occ,Nso_vac))
      allocate(doovv(Nso_occ,Nso_occ,Nso_vac,Nso_vac))
      allocate(doovvI(Nso_occ,Nso_occ,Nso_vac,Nso_vac))
      allocate(dSo(Nso_occ),dSv(Nso_vac))
!
      alphaoo = one
      alphavv = one
      betaoo  = one
      betavv  = one
      betaov  = one
      doovv = one
      dSo   = one
      dSv   = one
!
! Set up index translator (occ and vac index from absolute index [spatial])
!
      allocate(p2occ(Nbasis),p2vac(Nbasis))
      do i = 1, Nocc
        p2occ(NO_occ(i)) = i
      end do
      do a = 1, Nvac
        p2vac(NO_vac(a)) = a
      end do
!
! Get pair correction component of damping factors (alpha)
!
      do pocc = 1, Nocc_act
        p = occ_act(pocc)
!
        ! occ and vac spatial indices
        po = p2occ(p)
        pv = p2vac(p)
        poa = 2*po-1
        pob = poa + 1
        pva = 2*pv-1
        pvb = pva + 1
!
        alphaoo(poa,pob) = one/on(p)
        alphaoo(pob,poa) = one/on(p)
        alphavv(pva,pvb) = one/vn(p)
        alphavv(pvb,pva) = one/vn(p)
!
        do qocc = 1, Nocc_act
          q = occ_act(qocc)
          if(p.eq.q)cycle
!
          ! occ and vac spatial indices
          qo = p2occ(q)
          qv = p2vac(q)
          qoa = 2*qo-1
          qob = qoa + 1
          qva = 2*qv-1
          qvb = qva + 1
!
          ! no Delta, both are occ
          oopq = (on(p)*on(q) + eta(p,q))/(on(p)*on(q))
          vvpq = (vn(p)*vn(q) + eta(p,q))/(vn(p)*vn(q))
!
          alphaoo(poa,qoa) = oopq
          alphaoo(pob,qob) = oopq
          alphaoo(poa,qob) = oopq
          alphaoo(pob,qoa) = oopq
!
          alphavv(pva,qva) = vvpq
          alphavv(pvb,qvb) = vvpq
          alphavv(pva,qvb) = vvpq
          alphavv(pvb,qva) = vvpq
!
        end do ! qocc 
!
        do qvac = 1, Nvac_act
          q = vac_act(qvac)
!
          ! occ and vac spatial indices
          qo = p2occ(q)
          qv = p2vac(q)
          qoa = 2*qo-1
          qob = qoa + 1
          qva = 2*qv-1
          qvb = qva + 1
!
          ! no eta, occ and vac 
          oopq = (on(p) + Delta(p,q))*(on(q) - Delta(p,q))/(on(p)*on(q))
          vvpq = (vn(p) - Delta(p,q))*(vn(q) + Delta(p,q))/(vn(p)*vn(q))
!
          alphaoo(poa,qoa) = oopq
          alphaoo(pob,qob) = oopq
          alphaoo(poa,qob) = oopq
          alphaoo(pob,qoa) = oopq
!
          alphaoo(qoa,poa) = oopq
          alphaoo(qob,pob) = oopq
          alphaoo(qoa,pob) = oopq
          alphaoo(qob,poa) = oopq
!
          alphavv(pva,qva) = vvpq
          alphavv(pvb,qvb) = vvpq
          alphavv(pva,qvb) = vvpq
          alphavv(pvb,qva) = vvpq
!
          alphavv(qva,pva) = vvpq
          alphavv(qvb,pvb) = vvpq
          alphavv(qva,pvb) = vvpq
          alphavv(qvb,pva) = vvpq
!
        end do ! qvac
!
      end do ! pocc
!
      do pvac = 1, Nvac_act
        p = vac_act(pvac)
!
        ! occ and vac spatial indices
        po = p2occ(p)
        pv = p2vac(p)
        poa = 2*po-1
        pob = poa + 1
        pva = 2*pv-1
        pvb = pva + 1
!
        alphaoo(poa,pob) = one/on(p)
        alphaoo(pob,poa) = one/on(p)
        alphavv(pva,pvb) = one/vn(p)
        alphavv(pvb,pva) = one/vn(p)
!
        do qvac = 1, Nvac_act
          q = vac_act(qvac)
          if(p.eq.q)cycle
!
          ! occ and vac spatial indices
          qo = p2occ(q)
          qv = p2vac(q)
          qoa = 2*qo-1
          qob = qoa + 1
          qva = 2*qv-1
          qvb = qva + 1
!
          ! no Delta, both are vac
          oopq = (on(p)*on(q) + eta(p,q))/(on(p)*on(q))
          vvpq = (vn(p)*vn(q) + eta(p,q))/(vn(p)*vn(q))
!
          alphaoo(poa,qoa) = oopq
          alphaoo(pob,qob) = oopq
          alphaoo(poa,qob) = oopq
          alphaoo(pob,qoa) = oopq
!
          alphaoo(pva,qva) = vvpq
          alphaoo(pvb,qvb) = vvpq
          alphaoo(pva,qvb) = vvpq
          alphaoo(pvb,qva) = vvpq
!
        end do ! qvac
!
      end do ! pvac
!
! Calculate stat and HSC contribution (beta)
!
      do iocc = 1, Nocc_act
        i = occ_act(iocc)
!
        ! occ and vac spatial indices
        io = p2occ(i)
        iv = p2vac(i)
        ioa = 2*io-1
        iob = ioa + 1
        iva = 2*iv-1
        ivb = iva + 1
!
! Calculate i zeta contribution 
        zetacon = zero
        do pvac = 1, Nvac_act
          p = vac_act(pvac)
          do qvac = 1, Nvac_act
            q = vac_act(qvac)
            if(p.eq.q)cycle
            zetacon = zetacon + dsqrt(Delta(i,p)*Delta(i,q))
          end do ! q
        end do ! p
!
! Calculate xi contribution
        xisum = zero
        do pvac = 1, Nstat_occ(i)
          p = occ_stat(i,pvac)
!
          xisum = xisum + dsqrt(on(i)*Delta(i,p))
        end do ! pvac
        xisum = two*xisum
!
        foo_opp = one + zetacon - xisum
        fov_opp = one - zetacon + xisum
        fov_par = foo_opp
!
! oo
        betaoo(ioa,iob) = foo_opp
        betaoo(iob,ioa) = foo_opp
! vv
        betavv(iva,ivb) = foo_opp
        betavv(ivb,iva) = foo_opp
! ov
! No spin-flipping to same spatial (if alpha is occupied, then so is beta)
        betaov(ioa,ivb) = zero
        betaov(iob,iva) = zero
!
        do pvac = 1, Nstat_occ(i)
          p = occ_stat(i,pvac)
          ! occ and vac spatial indices
          po = p2occ(p)
          pv = p2vac(p)
          poa = 2*po-1
          pob = poa + 1
          pva = 2*pv-1
          pvb = pva + 1
!
! oo
          betaoo(ioa,pob) = betaoo(ioa,pob)*foo_opp
          betaoo(iob,poa) = betaoo(iob,poa)*foo_opp
          betaoo(poa,iob) = betaoo(poa,iob)*foo_opp
          betaoo(pob,ioa) = betaoo(pob,ioa)*foo_opp
!
          betaoo(poa,pob) = betaoo(poa,pob)*foo_opp
          betaoo(pob,poa) = betaoo(pob,poa)*foo_opp
!
! vv
          betavv(iva,pvb) = betavv(iva,pvb)*foo_opp
          betavv(ivb,pva) = betavv(ivb,pva)*foo_opp
          betavv(pva,ivb) = betavv(pva,ivb)*foo_opp
          betavv(pvb,iva) = betavv(pvb,iva)*foo_opp
!
          betavv(pva,pvb) = betavv(pva,pvb)*foo_opp
          betavv(pvb,pva) = betavv(pvb,pva)*foo_opp
!
! ov
          betaov(ioa,pvb) = betaov(ioa,pvb)*fov_opp
          betaov(iob,pva) = betaov(iob,pva)*fov_opp
          betaov(poa,ivb) = betaov(poa,ivb)*fov_opp
          betaov(pob,iva) = betaov(pob,iva)*fov_opp
!
          betaov(ioa,pva) = betaov(ioa,pva)*fov_par
          betaov(iob,pvb) = betaov(iob,pvb)*fov_par
          betaov(poa,iva) = betaov(poa,iva)*fov_par
          betaov(pob,ivb) = betaov(pob,ivb)*fov_par
!
! No spin-flipping to same spatial (if alpha is occupied, then so is beta)
          betaov(poa,pvb) = zero
          betaov(pob,pva) = zero
!
        end do ! pvac
!
! Second occupied (j)
!
        do jocc = 1, iocc-1
          j = occ_act(jocc)
!
          ! occ and vac spatial indices
          jo = p2occ(j)
          jv = p2vac(j)
          joa = 2*jo-1
          job = joa + 1
          jva = 2*jv-1
          jvb = jva + 1
!
! Calculate kappat_ij
          kappat = zero
          do pvac = 1, Nstat_occ(i)
            p = occ_stat(i,pvac)
            do qvac = 1, Nstat_occ(j)
              q = occ_stat(j,qvac)
              if(p.eq.q)cycle
              kappat = kappat + dsqrt(on(i)*Delta(i,p)*on(j)*Delta(j,q)) 
            end do ! qvac
          end do ! pvac
!
          foo_opp = one + zeta(i,j) - four*kappat
          foo_par = one + four*kappat
!
          fov_opp = one - zeta(i,j) + four*kappat
          fov_par = one - four*kappat
!
! oo
          betaoo(ioa,job) = foo_opp
          betaoo(iob,joa) = foo_opp
          betaoo(joa,iob) = foo_opp
          betaoo(job,ioa) = foo_opp
!
          betaoo(ioa,joa) = foo_par
          betaoo(iob,job) = foo_par
          betaoo(joa,ioa) = foo_par
          betaoo(job,iob) = foo_par
!
! vv
          betavv(iva,jvb) = foo_opp
          betavv(ivb,jva) = foo_opp
          betavv(jva,ivb) = foo_opp
          betavv(jvb,iva) = foo_opp
!
          betavv(iva,jva) = foo_par
          betavv(ivb,jvb) = foo_par
          betavv(jva,iva) = foo_par
          betavv(jvb,ivb) = foo_par
!
! ov
          betaov(ioa,jvb) = fov_opp
          betaov(iob,jva) = fov_opp
          betaov(joa,ivb) = fov_opp
          betaov(job,iva) = fov_opp
!
          betaov(ioa,jva) = fov_par
          betaov(iob,jvb) = fov_par
          betaov(joa,iva) = fov_par
          betaov(job,ivb) = fov_par
!
          do pvac = 1, Nstat_occ(i)
            p = occ_stat(i,pvac)
            ! occ and vac spatial indices
            po = p2occ(p)
            pv = p2vac(p)
            poa = 2*po-1
            pob = poa + 1
            pva = 2*pv-1
            pvb = pva + 1
! oo
            betaoo(poa,job) = betaoo(poa,job)*foo_opp
            betaoo(pob,joa) = betaoo(pob,joa)*foo_opp
            betaoo(joa,pob) = betaoo(joa,pob)*foo_opp
            betaoo(job,poa) = betaoo(job,poa)*foo_opp
!
            betaoo(poa,joa) = betaoo(poa,joa)*foo_par
            betaoo(pob,job) = betaoo(pob,job)*foo_par
            betaoo(joa,poa) = betaoo(joa,poa)*foo_par
            betaoo(job,pob) = betaoo(job,pob)*foo_par
!
! vv
            betavv(pva,jvb) = betavv(pva,jvb)*foo_opp
            betavv(pvb,jva) = betavv(pvb,jva)*foo_opp
            betavv(jva,pvb) = betavv(jva,pvb)*foo_opp
            betavv(jvb,pva) = betavv(jvb,pva)*foo_opp
!
            betavv(pva,jva) = betavv(pva,jva)*foo_par
            betavv(pvb,jvb) = betavv(pvb,jvb)*foo_par
            betavv(jva,pva) = betavv(jva,pva)*foo_par
            betavv(jvb,pvb) = betavv(jvb,pvb)*foo_par
!
! ov
            betaov(poa,jvb) = betaov(poa,jvb)*fov_opp
            betaov(pob,jva) = betaov(pob,jva)*fov_opp
            betaov(joa,pvb) = betaov(joa,pvb)*fov_opp
            betaov(job,pva) = betaov(job,pva)*fov_opp
!
            betaov(poa,jva) = betaov(poa,jva)*fov_par
            betaov(pob,jvb) = betaov(pob,jvb)*fov_par
            betaov(joa,pva) = betaov(joa,pva)*fov_par
            betaov(job,pvb) = betaov(job,pvb)*fov_par
!
          end do ! pvac
!
          do qvac = 1, Nstat_occ(j)
            q = occ_stat(j,qvac)
            ! occ and vac spatial indices
            qo = p2occ(q)
            qv = p2vac(q)
            qoa = 2*qo-1
            qob = qoa + 1
            qva = 2*qv-1
            qvb = qva + 1
! oo
            betaoo(qoa,iob) = betaoo(qoa,iob)*foo_opp
            betaoo(qob,ioa) = betaoo(qob,ioa)*foo_opp
            betaoo(ioa,qob) = betaoo(ioa,qob)*foo_opp
            betaoo(iob,qoa) = betaoo(iob,qoa)*foo_opp
!
            betaoo(qoa,ioa) = betaoo(qoa,ioa)*foo_par
            betaoo(qob,iob) = betaoo(qob,iob)*foo_par
            betaoo(ioa,qoa) = betaoo(ioa,qoa)*foo_par
            betaoo(iob,qob) = betaoo(iob,qob)*foo_par
!
! vv
            betavv(qva,ivb) = betavv(qva,ivb)*foo_opp
            betavv(qvb,iva) = betavv(qvb,iva)*foo_opp
            betavv(iva,qvb) = betavv(iva,qvb)*foo_opp
            betavv(ivb,qva) = betavv(ivb,qva)*foo_opp
!
            betavv(qva,iva) = betavv(qva,iva)*foo_par
            betavv(qvb,ivb) = betavv(qvb,ivb)*foo_par
            betavv(iva,qva) = betavv(iva,qva)*foo_par
            betavv(ivb,qvb) = betavv(ivb,qvb)*foo_par
!
! ov
            betaov(qoa,ivb) = betaov(qoa,ivb)*fov_opp
            betaov(qob,iva) = betaov(qob,iva)*fov_opp
            betaov(ioa,qvb) = betaov(ioa,qvb)*fov_opp
            betaov(iob,qva) = betaov(iob,qva)*fov_opp
!
            betaov(qoa,iva) = betaov(qoa,iva)*fov_par
            betaov(qob,ivb) = betaov(qob,ivb)*fov_par
            betaov(ioa,qva) = betaov(ioa,qva)*fov_par
            betaov(iob,qvb) = betaov(iob,qvb)*fov_par
!
          end do ! qvac
!
          do pvac = 1, Nstat_occ(i)
            p = occ_stat(i,pvac)
            po = p2occ(p)
            pv = p2vac(p)
            poa = 2*po-1
            pob = poa + 1
            pva = 2*pv-1
            pvb = pva + 1
            do qvac = 1, Nstat_occ(j)
              q = occ_stat(j,qvac)
              if(p.eq.q)cycle
              qo = p2occ(q)
              qv = p2vac(q)
              qoa = 2*qo-1
              qob = qoa + 1
              qva = 2*qv-1
              qvb = qva + 1
!
! oo
              betaoo(qoa,pob) = betaoo(qoa,pob)*foo_opp
              betaoo(qob,poa) = betaoo(qob,poa)*foo_opp
              betaoo(poa,qob) = betaoo(poa,qob)*foo_opp
              betaoo(pob,qoa) = betaoo(pob,qoa)*foo_opp
!
              betaoo(qoa,poa) = betaoo(qoa,poa)*foo_par
              betaoo(qob,pob) = betaoo(qob,pob)*foo_par
              betaoo(poa,qoa) = betaoo(poa,qoa)*foo_par
              betaoo(pob,qob) = betaoo(pob,qob)*foo_par
!
! vv
              betavv(qva,pvb) = betavv(qva,pvb)*foo_opp
              betavv(qvb,pva) = betavv(qvb,pva)*foo_opp
              betavv(pva,qvb) = betavv(pva,qvb)*foo_opp
              betavv(pvb,qva) = betavv(pvb,qva)*foo_opp
!
              betavv(qva,pva) = betavv(qva,pva)*foo_par
              betavv(qvb,pvb) = betavv(qvb,pvb)*foo_par
              betavv(pva,qva) = betavv(pva,qva)*foo_par
              betavv(pvb,qvb) = betavv(pvb,qvb)*foo_par
!
! ov
              betaov(qoa,pvb) = betaov(qoa,pvb)*fov_opp
              betaov(qob,pva) = betaov(qob,pva)*fov_opp
              betaov(poa,qvb) = betaov(poa,qvb)*fov_opp
              betaov(pob,qva) = betaov(pob,qva)*fov_opp
!
              betaov(qoa,pva) = betaov(qoa,pva)*fov_par
              betaov(qob,pvb) = betaov(qob,pvb)*fov_par
              betaov(poa,qva) = betaov(poa,qva)*fov_par
              betaov(pob,qvb) = betaov(pob,qvb)*fov_par
!
            end do ! qvac
          end do ! pvac
!
        end do ! jocc
!
      end do ! iocc
!
! Zero all orbital interactions that should be taken care of by Delta NO
! Assign 1 for active, 0 for frozen
!
      allocate(oact(Nso_occ),vact(Nso_vac))
!
      oact = 0
      vact = 0
! Set 1's
      do pact = 1, Nact
        p = NO_act(pact)
!
        ! occ and vac spatial indices
        po = p2occ(p)
        pv = p2vac(p)
        ! spin-orbital indices
        poa = 2*po-1
        pob = poa + 1
        pva = 2*pv-1
        pvb = pva + 1
!
        oact(poa) = 1
        oact(pob) = 1
        vact(pva) = 1
        vact(pvb) = 1
!
! Use these for zeroing single excitations from the active
        dSo(poa) = ZERO
        dSo(pob) = ZERO
        dSv(pva) = ZERO
        dSv(pvb) = ZERO
!
      end do ! Istat
!
      do i = 1, Nso_occ
        do j = 1, Nso_occ
          do a = 1, Nso_vac
            do b = 1, Nso_vac
!
              doovvI(i,j,a,b) = dble(oact(i)*oact(j)*vact(a)*vact(b))
!
            end do ! b
          end do ! a
        end do ! j
      end do ! i
!
! Zero terms with all active orbitals (these interactions should be in Delta NO)
      doovvI = ONE - doovvI
      doovv = doovv*doovvI
!
! Build doovv
      do i = 1, Nso_occ
        do j = 1, Nso_occ
          do a = 1, Nso_vac
            do b = 1, Nso_vac
!
              doovv(i,j,a,b) = doovv(i,j,a,b)*alphaoo(i,j)*alphavv(a,b)&
                              *betaoo(i,j)*betavv(a,b)*betaov(i,a)*betaov(j,b)*betaov(i,b)*betaov(j,a)
!
            end do ! b
          end do ! a
        end do ! j
      end do ! i
!
! Turn damping off
! factors used in 2nd order terms
! turned off in CCSD code too!
!      doo = ONE
!      dvv = ONE
!      dov = ONE
!      doov = ONE
!      dovv = ONE
! factor used in 1st order term
!      doovv = ONE
!
! end routine CFT_BLD_so_damp
      call PRG_manager ('exit', 'CFT_BLD_so_damp', 'UTILITY')
      end subroutine CFT_BLD_so_damp
      subroutine CFT_BLD_so_occvac
!********************************************************************** 
!     Date last modified: November 21, 2018                           *
!     Author: JWH                                                     *
!     Description: Create occupation and vacancy numbers for the      *
!                  spin orbitals.                                     *
!**********************************************************************
!Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE CFT_objects
      USE QM_objects
      USE INT_objects
      USE CCSD_objects

      implicit none
!
! Local scalars: 
!
      integer :: i, a, ip, ap
!
! Local arrays:
!
!
! Begin:
      call PRG_manager ('enter', 'CFT_BLD_so_occvac', 'UTILITY')
!
! CC is computed over spin-orbitals (easier implementation)
! Closed-shell for now
      Nso_occ  = 2*Nocc
      Nso_vac = 2*Nvac
!
      allocate(onso(Nso_occ),vnso(Nso_vac))
!
      do i = 1, Nocc
        ip = NO_occ(i)
        onso(2*i - 1) = on(ip)
        onso(2*i)     = on(ip)
      end do ! i
!
      do a = 1, Nvac
        ap = NO_vac(a)
        vnso(2*a - 1) = vn(ap)
        vnso(2*a)     = vn(ap)
      end do ! a
!
!      write(6,*)'Occupied spin-orbital occupancies'
!      do i = 1, Nso_occ
!        write(6,'(I4,X,F12.8)')i,onso(i)
!      end do 
!      write(6,*)'Virtual spin-orbital vacancies'
!      do a = 1, Nso_vac
!        write(6,'(I4,X,F12.8)')a,vnso(a)
!      end do 
!
! end routine CFT_BLD_so_occvac
      call PRG_manager ('exit', 'CFT_BLD_so_occvac', 'UTILITY')
      end subroutine CFT_BLD_so_occvac
      subroutine CFT_CCSD_DIIS(Nt1,Nt2,n_diis,rtot_diis,ttot_diis,r1,r2,t1,t2)
!********************************************************************** 
!     Date last modified: October 25, 2018                            *
!     Author: JWH                                                     *
!     Description: Perform DIIS on t1 and t2 together.                *
!**********************************************************************
!Modules:

      implicit none
!
! Input scalars: 
      integer :: Nt1, Nt2, n_diis
!
! Input arrays:
      double precision :: rtot_diis(Nt1+Nt2,n_diis), ttot_diis(Nt1+Nt2,n_diis)
      double precision :: r1(Nt1), r2(Nt2), t1(Nt1), t2(Nt2)
!
! Local scalars:
      integer :: Nttot
!
! Local arrays:
      double precision, allocatable, dimension(:) :: ttot, rtot
!
! Begin:
      call PRG_manager ('enter', 'CFT_CCSD_diis', 'UTILITY')
!
      Nttot = Nt1 + Nt2
!
      allocate(ttot(Nttot),rtot(Nttot))
!
! join vectors
      ttot(1:Nt1)       = t1(1:Nt1)
      ttot(Nt1+1:Nttot) = t2(1:Nt2)
      rtot(1:Nt1)       = r1(1:Nt1)
      rtot(Nt1+1:Nttot) = r2(1:Nt2)
!
      call DIIS_extrapolation(Nttot,Nttot,n_diis,rtot_diis,ttot_diis,rtot,ttot) 
!
! Split the vectors back up
      t1(1:Nt1) = ttot(1:Nt1)
      t2(1:Nt2) = ttot(Nt1+1:Nttot)
      r1(1:Nt1) = rtot(1:Nt1)
      r2(1:Nt2) = rtot(Nt1+1:Nttot)
!
! end routine CFT_CCSD_diis
      call PRG_manager ('exit', 'CFT_CCSD_diis', 'UTILITY')
      end subroutine CFT_CCSD_diis
      subroutine CFT_E_CCSD_comp
!******************************************************************
!     Date last modified: November 27, 2018                       *
!     Author: JWH                                                 *
!     Description: Calculate the components of the MP2 energy.    *
!******************************************************************
!Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE CFT_objects
      USE CCSD_objects
      USE QM_objects
      USE INT_objects

      implicit none
!
! Local scalars:
!
      integer :: a, b, i, j, ip, ap, k
      double precision :: Esing, Edoub, Efock, E1e, E2e
!
! Local functions:
      double precision :: hso, aseri
!
! Local arrays:
!
! Begin:
      call PRG_manager ('enter', 'CFT_E_CCSD_comp', 'UTILITY')
!
      Efock = ZERO
      Esing = ZERO
      Edoub = ZERO
      do i = 1, Nso_occ
        do a = 1, Nso_vac
!
          Efock = Efock + t1(i,a)*fov(i,a)
!
          do j = 1, Nso_occ
            do b = 1, Nso_vac
!
              Esing = Esing + F1_2*t1(i,a)*t1(j,b)*oovv(i,j,a,b)
              Edoub = Edoub + F1_2*F1_2*t2(i,j,a,b)*oovv(i,j,a,b)
!
            end do ! bvac
          end do ! avac
        end do ! jocc
      end do ! iocc
!
      write(UNIout,'(/a)')'  Decomposed CCSD correlation energy'
      write(UNIout,'(a)') '--------------------------------------'
      write(UNIout,'(a)') '    component              energy     '
      write(UNIout,'(a)') '--------------------------------------'
      write(UNIout,'(a,F16.10)')'      fock           ',Efock
      write(UNIout,'(a,F16.10)')'     singles         ',Esing
      write(UNIout,'(a,F16.10)')'     doubles         ',Edoub
      write(UNIout,'(a,F16.10)')'      total          ',Edoub+Efock+Esing
!
! Can also decompose into 1 and 2-electron contributions
! This ain't right, you actually need to solve the CCSD repsonse equations
! energy is not variational
      E1e = ZERO
      E2e = ZERO
!
      do i = 1, Nso_occ
        do a = 1, Nso_vac
          ip = so_occ(i)
          ap = so_vac(a)
!
          ! one-electron part of fock matrix element
          E1e = E1e + t1(i,a)*hso(ip,ap)
!
          do k = 1, 2*Nbasis
            ! two-electron part of fock matrix element
            E2e = E2e + t1(i,a)*on((k+1)/2)*aseri(ip,k,ap,k)
          end do
!
          do j = 1, Nso_occ
            do b = 1, Nso_vac
!
              E2e = E2e + F1_2*(t1(i,a)*t1(j,b) + F1_2*t2(i,j,a,b))*oovv(i,j,a,b)
!
            end do ! bvac
          end do ! avac
        end do ! jocc
      end do ! iocc
!
      write(UNIout,'(/a)')'  Decomposed CCSD correlation energy'
      write(UNIout,'(a)') '--------------------------------------'
      write(UNIout,'(a)') '    component              energy     '
      write(UNIout,'(a)') '--------------------------------------'
      write(UNIout,'(a,F16.10)')'    one-electron     ',E1e
      write(UNIout,'(a,F16.10)')'    two-electron     ',E2e
      write(UNIout,'(a,F16.10)')'      total          ',E1e+E2e
!
! end routine CFT_E_MP2_comp
      call PRG_manager ('exit', 'CFT_E_CCSD_comp', 'UTILITY')
      end subroutine CFT_E_CCSD_comp
      subroutine CFT_CCSD_BLD_aseri(alist,Na,blist,Nb,clist,Nc,dlist,Nd,eri)
!********************************************************************** 
!     Date last modified: October 31, 2018                            *
!     Author: JWH                                                     *
!     Description: Create different classes of antisymmetrized        *
!                  two-electron integrals.                            *
!**********************************************************************
!Modules:

      implicit none
!
! Input scalars: 
      integer :: Na, Nb, Nc, Nd
!
! Input arrays:
      integer :: alist(Na), blist(Nb), clist(Nc), dlist(Nd)
!
! Output arrays:
      double precision :: eri(Na,Nb,Nc,Nd)
!
! Local scalars:
      integer :: a, b, c, d
      integer :: ap, bp, cp, dp
!
! Local functions:
      double precision :: aseri
!
! Begin:
      call PRG_manager ('enter', 'CFT_CCSD_BLD_aseri', 'UTILITY')
!
      do d = 1, Nd
        dp = dlist(d)
        do c = 1, Nc
          cp = clist(c)
          do b = 1, Nb
            bp = blist(b)
            do a = 1, Na
              ap = alist(a)
!
              eri(a,b,c,d) = aseri(ap,bp,cp,dp)
!
            end do
          end do
        end do
      end do
!
! end routine CFT_CCSD_BLD_aseri
      call PRG_manager ('exit', 'CFT_CCSD_BLD_aseri', 'UTILITY')
      end subroutine CFT_CCSD_BLD_aseri
      subroutine CFT_CCSD_BLD_fso(alist,Na,blist,Nb,f)
!********************************************************************** 
!     Date last modified: October 31, 2018                            *
!     Author: JWH                                                     *
!     Description: Create different classes of Fock matrices over spin*
!                  orbitals.                                          *
!**********************************************************************
!Modules:

      implicit none
!
! Input scalars: 
      integer :: Na, Nb
!
! Input arrays:
      integer :: alist(Na), blist(Nb)
!
! Output arrays:
      double precision :: f(Na,Nb)
!
! Local scalars:
      integer :: a, b
      integer :: ap, bp
!
! Local functions:
      double precision :: fso
!
! Begin:
      call PRG_manager ('enter', 'CFT_CCSD_BLD_fso', 'UTILITY')
!
      do b = 1, Nb
        bp = blist(b)
        do a = 1, Na
          ap = alist(a)
!
          f(a,b) = fso(ap,bp)
!
        end do
      end do
!
! end routine CFT_CCSD_BLD_fso
      call PRG_manager ('exit', 'CFT_CCSD_BLD_fso', 'UTILITY')
      end subroutine CFT_CCSD_BLD_fso
      subroutine CFT_get_timing
!********************************************************************** 
!     Date last modified: October 26, 2018                            *
!     Author: JWH                                                     *
!     Description: Get elapsed calculation time and print it.         *
!**********************************************************************
!Modules:
      USE program_files
      USE program_constants
      USE program_timing

      implicit none
!
! Local scalars: 
!
      double precision :: time_elapsed
!
! Begin:
      call PRG_manager ('enter', 'CFT_get_timing', 'UTILITY')
!
      call PRG_CPU_time
      call CNV_cpu (lstcpu, hcpu, mcpu, scpu, ccpu, secpu)
      call PRG_date
      call PRG_time
!
      Time_elapsed = Time_current-Time_begin
      if(Time_elapsed.lt.0.0D0)then ! It's a new day
        Time_elapsed=Time_elapsed+86400
      end if
!
      call CNV_cpu (Time_elapsed, hcpu, mcpu, scpu, ccpu, setim)
!
      write(UNIout,'(/2a)')'Cpu     time:',secpu
      write(UNIout,'(2a/)')'Elapsed time:',setim
!
! end routine CFT_get_timing
      call PRG_manager ('exit', 'CFT_get_timing', 'UTILITY')
      end subroutine CFT_get_timing
! Function to create anti-symmetrized eri over spin-orbitals from eris over spatial orbitals
      function aseri(i,j,a,b)

      USE CCSD_objects

      implicit none 

      double precision :: aseri
      integer :: i,j,a,b
      integer :: is,js,as,bs
      integer :: ias,jbs,ibs,jas
      integer :: ip,jp,ap,bp
!
! Assuming closed-shell (i.e. simple mapping between spatial and spin-orbital indices)
!
! Determine the spin (1 = alpha, 0 = beta)
      is = mod(i,2)
      js = mod(j,2)
      as = mod(a,2)
      bs = mod(b,2)
!
      ias = 1 - abs(is - as)
      jbs = 1 - abs(js - bs)
      ibs = 1 - abs(is - bs)
      jas = 1 - abs(js - as)
!
      ip = (i+1)/2
      jp = (j+1)/2
      ap = (a+1)/2
      bp = (b+1)/2
!
      aseri = ias*jbs*eri(ip,jp,ap,bp) - ibs*jas*eri(ip,jp,bp,ap)
!
      end function aseri
      ! Function to create Fock matrix element over spin-orbitals from Fock matrix over spatial orbitals
      function fso(i,a)

      USE CFT_objects

      implicit none

      double precision :: fso
      integer :: i,a
      integer :: is,as
      integer :: ias
      integer :: ip,ap
!
! Assuming closed-shell (i.e. simple mapping between spatial and spin-orbital indices)
!
! Determine the spin (1 = alpha, 0 = beta)
      is = mod(i,2)
      as = mod(a,2)
!
      ias = 1 - abs(is - as)
!
      ip = (i+1)/2
      ap = (a+1)/2
!
      fso = ias*FockNO(ip,ap)
!
      end function fso
      ! Function to create one-electron matrix element over spin-orbitals integrals over spatial orbitals
      function hso(i,a)

      USE CFT_objects

      implicit none

      double precision :: hso
      integer :: i,a
      integer :: is,as
      integer :: ias
      integer :: ip,ap
!
! Assuming closed-shell (i.e. simple mapping between spatial and spin-orbital indices)
!
! Determine the spin (1 = alpha, 0 = beta)
      is = mod(i,2)
      as = mod(a,2)
!
      ias = 1 - abs(is - as)
!
      ip = (i+1)/2
      ap = (a+1)/2
!
      hso = ias*H_NO(ip,ap)
!
      end function hso
      subroutine CFT_BLD_red_lambda
!***********************************************************************
!     Date last modified: July 5, 2019                                 *
!     Author: JWH                                                      *
!     Description: Construct a reduced lambda matrix. Remove a factor  *
!                  of 2n_c.                                            *
!***********************************************************************
! Modules:
      USE CFT_objects
      USE QM_objects
      USE INT_objects
      USE type_molecule
      USE program_constants
      USE program_files

      implicit none
!
! Local scalars:
      integer :: cNO, dNO, iNO, rNO, Iact, Cact, Iclosed, Iopen
      integer :: Cocc, Cvac, Rvac, Jocc, Svac
      integer :: NO_all(Nbasis), aNO, Nall, Istat, jNO, sNO
      integer :: cidi_index, ciid_index
!
! Local functions: 
      integer :: TEIindex
!
! Begin:
!
      call PRG_manager('enter','CFT_BLD_lambda','UTILITY')
!
      if(allocated(lambda))then
        deallocate(lambda)
      end if
      allocate(lambda(Nbasis,Nbasis))
!
      lambda = ZERO
!
! Create list for all orbitals
      do aNO = 1, Nbasis
        NO_all(aNO) = aNO
      end do
      Nall = Nbasis
!
! Get the necessary integrals
      ! Transform to NO basis
      call CFT_gen_TEItrans(NO_act,Nact,NO_act,Nact,NO_all,Nall,NO_all,Nall,&
                             NO,Nbasis,eriAO,eriNO)
!
! Need G for Fock matrix parts
      call CFT_BLD_Gmatrix
! Transform to NO basis
      call CFT_2D_transform(Gcl_AO, NO, Gcl_NO, Nbasis, Nbasis)
      call CFT_2D_transform(Gop_AO, NO, Gop_NO, Nbasis, Nbasis)
      call CFT_2D_transform(Goc_AO, NO, Goc_NO, Nbasis, Nbasis)
!
! and h
      if(allocated(H_NO))then
        deallocate(H_NO)
      end if
      allocate(H_NO(Nbasis,Nbasis))
      call I1E_transform (Hcore, NO, H_NO, Nbasis, Nbasis, MATlen)
!
! HF-like part
      do Iclosed = 1, Nclosed ! closed-shell
        cNO = NO_closed(Iclosed)
        do dNO = 1, Nbasis
          lambda(cNO,dNO) = h_NO(cNO,dNO) + Gcl_NO(cNO,dNO) + Goc_NO(cNO,dNO)
        end do ! dNO
      end do ! cNO
!
! pair correction
!
      do Cact = 1, Nact
        cNO = NO_act(Cact)
        do dNO = 1, Nbasis
!
          do Iact = 1, Nact
            iNO = NO_act(Iact)
            cidi_index = TEIindex(Nbasis,cNO,iNO,dNO,iNO)
            ciid_index = TEIindex(Nbasis,cNO,iNO,iNO,dNO)
!
            lambda(cNO,dNO) = lambda(cNO,dNO)&
                            + eta(cNO,iNO)*(2.0D0*eriNO(cidi_index) - eriNO(ciid_index))/ON(cNO)
!
          end do ! Iact
!
        end do ! dNO
      end do ! Cact
!
! End of routine CFT_BLD_red_lambda
      call PRG_manager('exit','CFT_BLD_red_lambda','UTILITY')
      end subroutine CFT_BLD_red_lambda
