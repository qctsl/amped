      subroutine BLD_CFT_objects
!**********************************************************************************************
!       Date last modified: April 13, 2016                                                    *
!       Author: JWH                                                                           *
!       Description:  Builds list of CFT objects.                                            *
!**********************************************************************************************
! Modules:
      USE program_objects

      implicit none

! Local scalar:
      integer :: Iobject
!
! Begin:
      OBJ_CFT(1:Max_objects)%modality = 'other'
      OBJ_CFT(1:Max_objects)%class = 'CFT'
      OBJ_CFT(1:Max_objects)%depend = .true.
      NCFTobjects = 0
!
! Class CFT
! Modality=RHF
      NCFTobjects = NCFTobjects + 1
      OBJ_CFT(NCFTobjects)%name = 'NO'
      OBJ_CFT(NCFTobjects)%modality = 'RHF'
      OBJ_CFT(NCFTobjects)%routine = 'CFT_RHF_SCF'
!
! Class CFT
! Modality=DeltaNO
      NCFTobjects = NCFTobjects + 1
      OBJ_CFT(NCFTobjects)%name = 'ENERGY'
      OBJ_CFT(NCFTobjects)%modality = 'DELTANO'
      OBJ_CFT(NCFTobjects)%routine = 'CFT_calc'
!
      NCFTobjects = NCFTobjects + 1
      OBJ_CFT(NCFTobjects)%name = 'NO' ! natural orbitals
      OBJ_CFT(NCFTobjects)%modality = 'DELTANO'
      OBJ_CFT(NCFTobjects)%routine = 'CFT_calc'
!
      NCFTobjects = NCFTobjects + 1
      OBJ_CFT(NCFTobjects)%name = 'ON' ! occupation numbers
      OBJ_CFT(NCFTobjects)%modality = 'DELTANO'
      OBJ_CFT(NCFTobjects)%routine = 'CFT_calc'
!
      NCFTobjects = NCFTobjects + 1
      OBJ_CFT(NCFTobjects)%name = 'R2RDM' ! reduced (2-index) 2RDM
      OBJ_CFT(NCFTobjects)%modality = 'DELTANO'
      OBJ_CFT(NCFTobjects)%routine = 'CFT_BLD_R2RDM'
!
      NCFTobjects = NCFTobjects + 1
      OBJ_CFT(NCFTobjects)%name = '2RDM' ! reduced (2-index) 2RDM
      OBJ_CFT(NCFTobjects)%modality = 'DELTANO'
      OBJ_CFT(NCFTobjects)%routine = 'CFT_calc'
!
! Modality=OS (Obara-Saika)
      NCFTobjects = NCFTobjects + 1
      OBJ_CFT(NCFTobjects)%name = '2EINT'
      OBJ_CFT(NCFTobjects)%modality = 'OS'
      OBJ_CFT(NCFTobjects)%routine = 'BLD_2eint'
!
      NCFTobjects = NCFTobjects + 1
      OBJ_CFT(NCFTobjects)%name = '2EINTLR'
      OBJ_CFT(NCFTobjects)%modality = 'OS'
      OBJ_CFT(NCFTobjects)%routine = 'BLD_2eintlr'
!
! Modality=CS
      NCFTobjects = NCFTobjects + 1
      OBJ_CFT(NCFTobjects)%name = 'dynamic_Ec'
      OBJ_CFT(NCFTobjects)%modality = 'CS'
      OBJ_CFT(NCFTobjects)%routine = 'CFT_CS_energy'
!
      NCFTobjects = NCFTobjects + 1
      OBJ_CFT(NCFTobjects)%name = 'dEdyndD'
      OBJ_CFT(NCFTobjects)%modality = 'CS'
      OBJ_CFT(NCFTobjects)%routine = 'CFT_CS_Delta_gradient'
!
      NCFTobjects = NCFTobjects + 1
      OBJ_CFT(NCFTobjects)%name = 'd2EdyndD2'
      OBJ_CFT(NCFTobjects)%modality = 'CS'
      OBJ_CFT(NCFTobjects)%routine = 'CFT_CS_Delta_hessian'
!
! Modality=OSEC
      NCFTobjects = NCFTobjects + 1
      OBJ_CFT(NCFTobjects)%name = 'dynamic_Ec'
      OBJ_CFT(NCFTobjects)%modality = 'OSEC'
      OBJ_CFT(NCFTobjects)%routine = 'CFT_OSEC_energy'
!
      NCFTobjects = NCFTobjects + 1
      OBJ_CFT(NCFTobjects)%name = 'lambda_dyn'
      OBJ_CFT(NCFTobjects)%modality = 'OSEC'
      OBJ_CFT(NCFTobjects)%routine = 'CFT_OSEC_lambda'
!
      NCFTobjects = NCFTobjects + 1
      OBJ_CFT(NCFTobjects)%name = 'lambdap_dyn'
      OBJ_CFT(NCFTobjects)%modality = 'OSEC'
      OBJ_CFT(NCFTobjects)%routine = 'CFT_OSEC_lambdap'
!
      NCFTobjects = NCFTobjects + 1
      OBJ_CFT(NCFTobjects)%name = 'dynamic_norm'
      OBJ_CFT(NCFTobjects)%modality = 'OSEC'
      OBJ_CFT(NCFTobjects)%routine = 'CFT_OSEC_norm'
!
      NCFTobjects = NCFTobjects + 1
      OBJ_CFT(NCFTobjects)%name = 'dEdyndD'
      OBJ_CFT(NCFTobjects)%modality = 'OSEC'
      OBJ_CFT(NCFTobjects)%routine = 'CFT_OSEC_Delta_gradient'
!
      NCFTobjects = NCFTobjects + 1
      OBJ_CFT(NCFTobjects)%name = 'd2EdyndD2'
      OBJ_CFT(NCFTobjects)%modality = 'OSEC'
      OBJ_CFT(NCFTobjects)%routine = 'CFT_OSEC_Delta_hessian'
!
      NCFTobjects = NCFTobjects + 1
      OBJ_CFT(NCFTobjects)%name = 'ontop_weight'
      OBJ_CFT(NCFTobjects)%modality = 'OSEC'
      OBJ_CFT(NCFTobjects)%routine = 'CFT_ontop_weight'
!
! Modality=FHC
      NCFTobjects = NCFTobjects + 1
      OBJ_CFT(NCFTobjects)%name = 'dynamic_Ec'
      OBJ_CFT(NCFTobjects)%modality = 'FHC'
      OBJ_CFT(NCFTobjects)%routine = 'CFT_FHC_energy'
!
      NCFTobjects = NCFTobjects + 1
      OBJ_CFT(NCFTobjects)%name = 'lambda_dyn'
      OBJ_CFT(NCFTobjects)%modality = 'FHC'
      OBJ_CFT(NCFTobjects)%routine = 'CFT_FHC_lambda'
!
      NCFTobjects = NCFTobjects + 1
      OBJ_CFT(NCFTobjects)%name = 'lambdap_dyn'
      OBJ_CFT(NCFTobjects)%modality = 'FHC'
      OBJ_CFT(NCFTobjects)%routine = 'CFT_FHC_lambdap'
!
      NCFTobjects = NCFTobjects + 1
      OBJ_CFT(NCFTobjects)%name = 'dEdyndD'
      OBJ_CFT(NCFTobjects)%modality = 'FHC'
      OBJ_CFT(NCFTobjects)%routine = 'CFT_FHC_Delta_gradient'
!
      NCFTobjects = NCFTobjects + 1
      OBJ_CFT(NCFTobjects)%name = 'd2EdyndD2'
      OBJ_CFT(NCFTobjects)%modality = 'FHC'
      OBJ_CFT(NCFTobjects)%routine = 'CFT_FHC_Delta_hessian'
!
! Modality=TDF
      NCFTobjects = NCFTobjects + 1
      OBJ_CFT(NCFTobjects)%name = 'dynamic_Ec'
      OBJ_CFT(NCFTobjects)%modality = 'TDF'
      OBJ_CFT(NCFTobjects)%routine = 'CFT_TDF_energy'
!
! Modality=CCSD
      NCFTobjects = NCFTobjects + 1
      OBJ_CFT(NCFTobjects)%name = 'ENERGY'
      OBJ_CFT(NCFTobjects)%modality = 'CCSD'
      OBJ_CFT(NCFTobjects)%routine = 'CFT_CCSD_energy'
!
      NCFTobjects = NCFTobjects + 1
      OBJ_CFT(NCFTobjects)%name = '2RDM'
      OBJ_CFT(NCFTobjects)%modality = 'CCSD'
      OBJ_CFT(NCFTobjects)%routine = 'CFT_CCSD_2RDM'
!
! Modality=MP2
      NCFTobjects = NCFTobjects + 1
      OBJ_CFT(NCFTobjects)%name = 'ENERGY'
      OBJ_CFT(NCFTobjects)%modality = 'MP2'
      OBJ_CFT(NCFTobjects)%routine = 'CFT_MP2_energy'
!
      NCFTobjects = NCFTobjects + 1
      OBJ_CFT(NCFTobjects)%name = '2RDM'
      OBJ_CFT(NCFTobjects)%modality = 'MP2'
      OBJ_CFT(NCFTobjects)%routine = 'CFT_MP2_2RDM'
!
      do Iobject=1,NCFTobjects
        OBJ_CFT(Iobject)%exist=.false.
        OBJ_CFT(Iobject)%Current=.false.
      end do

      return
      end subroutine BLD_CFT_objects
