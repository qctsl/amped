      subroutine CFT_calc
!*****************************************************************
!     Date last modified: April 6, 2020                          *
!     Author: J.W. Hollett                                       *
!     Description:  Determine the natural orbitals, their        *
!                   occupancies, and the Delta NO energy.        *
!*****************************************************************
! Modules:
      USE program_files
      USE CFT_objects
      USE QM_objects
      USE type_molecule
      USE iso_c_binding

      implicit none
!
! Local scalars: 
      double precision :: max_deltaON
      integer :: aNO, iAO, iNO, rNO, Istat

! Local arrays:
      double precision, allocatable, dimension(:) :: ONprev
!
! Local functions:
      double precision :: GET_Enuclear
!
! Begin:
      call PRG_manager ('enter', 'CFT_calc', 'UTILITY')
!
! Put in defaults later
      LHSCon = .true.
      !LHSCon = .false.
!
      write(UNIout,'(/a)')' '
      write(UNIout,'(a)')'**************************************************'
      write(UNIout,'(a)')'*          Cumulant Functional Theory            *'
      write(UNIout,'(a)')'*                 version 4.1                    *'
      write(UNIout,'(a)')'*                  15/04/2020                    *'
      write(UNIout,'(a)')'*           (JCP, 2017, 145, 084106)             *'
      write(UNIout,'(a)')'*           (JCP, 2020, 152, 014101)             *'
      write(UNIout,'(a/)')'**************************************************'
!
! Print cumulant functional parameters
! 
!      write(UNIout,'(a)')'Static cumulant functional parameters'
!      write(UNIout,'(a)')'-------------------------------------'
!
      select case(dynamic)
      case('CS')
        write(UNIout,'(/a)')'Colle-Salvetti functional used for dynamic correlation'
        write(UNIout,'(a)')'---------------'
        write(UNIout,'(a)')'CS parameters'
        write(UNIout,'(a)')'---------------'
        cDC = 0.35D0
        write(UNIout,'(a,F6.3)')' cDC = ',cDC
      case('OF')
        write(UNIout,'(/a)')'OSEC functional used for opposite-spin dynamic correlation'
        write(UNIout,'(a)')'---------------'
        write(UNIout,'(a)')'OSEC parameters'
        write(UNIout,'(a)')'---------------'
        write(UNIout,'(a,F6.3)')' qOS = ',qOS
        cDC = 0.40D0
        write(UNIout,'(a,F6.3)')' cDC = ',cDC
!
        write(UNIout,'(/a)')'FHC functional used for parallel-spin dynamic correlation'
        write(UNIout,'(a)')'---------------'
        write(UNIout,'(a)')'FHC parameters'
        write(UNIout,'(a)')'---------------'
        write(UNIout,'(a,F6.3)')' qFHC = ',qFHC
!
      case('ODF')
        write(UNIout,'(/a)')'On-top density functional used for short-range correlation'
        write(UNIout,'(a)')'---------------'
        write(UNIout,'(a)')'ODF parameter'
        write(UNIout,'(a)')'---------------'
        write(UNIout,'(a,F6.3)')' q_TDF = ', q_TDF
!
      case('TDF')
        write(UNIout,'(/a)')'Two-electron density functional used for short-range correlation'
        write(UNIout,'(a)')'---------------'
        write(UNIout,'(a)')'TDF parameter'
        write(UNIout,'(a)')'---------------'
        write(UNIout,'(a,F6.3)')' q_TDF = ', q_TDF
!
      case('MP2')
        write(UNIout,'(/a)')'2nd-order perturbation theory used for dynamic correlation'
      case('CCD')
        write(UNIout,'(/a)')'Coupled-cluster doubles used for dynamic correlation'
      case('CCSD')
        write(UNIout,'(/a)')'Coupled-cluster singles and doubles used for dynamic correlation'
      case('NONE')
        write(UNIout,'(/a)')'No dynamic correlation functional'
!
       case('CISD')
         write(UNIout,'(/a)')'Bailing out of CFT calculation and just calculating the CISD wave function'
         call CFT_CISD
         return
!
      end select
!
! Get guess NOs
      call CFT_guess_NO
!
! Initialize occupancies
      call CFT_guess_Delta ! allocate Delta
!
! Determine active space
      call CFT_BLD_NO_pairs
!
! Determine coupled pairs (share vacants) 
      call CFT_BLD_NO_coupled
!
! Get guess ONs
      call CFT_Delta_to_ON
!
! Calculate integral coefficients (eta, zeta, xi, and kappa)
      call CFT_BLD_ezxk
!
! Calculate initial CFT energy
!
      MUN_prtlev = 0
      if((dynamic.eq.'TDF').and.(.not.LTDFvar))then
        dynamic = 'NONE'
        call CFT_energy
        dynamic = 'TDF'
      else if(dynamic.eq.'ODF')then
        dynamic = 'NONE'
        call CFT_energy
        dynamic = 'ODF'
      else
        call CFT_energy
      end if
      write(UNIout,'(/a,F20.12)')'CFT energy (guess ONs and NOs): ',E_CFT+GET_Enuclear()
!
! Optimize ONs for guess orbitals
!
! Not using dynamic gradient/hessian at the moment
! Optimize without dynamic correlation first
!      if(dynamic.eq.'CS')then
!        dynamic = 'NONE'
!        call CFT_ON_opt
!        dynamic = 'CS'
!      else if(dynamic.eq.'OF')then
!        dynamic = 'NONE'
!        call CFT_ON_opt
!        dynamic = 'OF'
!      end if
!
      select case(optmethod)
!
      case('SIMUL')
! Simultaneous optimization of total 2-RDM (Deltas and orbitals)
      !call CFT_2RDM_deriv_test
      if((dynamic.eq.'TDF').and.(.not.LTDFvar))then
        dynamic = 'NONE'
        call CFT_2RDM_opt
        dynamic = 'TDF'
      else if(dynamic.eq.'ODF')then
        dynamic = 'NONE'
        call CFT_2RDM_opt
        dynamic = 'ODF'
      else
        call CFT_2RDM_opt
      end if
!
      case('ALTER')
!      write(6,*)' ON optimization turned off'
      call CFT_ON_opt
!
      call CFT_energy
!
      write(UNIout,'(/a,F20.12)')'CFT energy (guess orbitals): ',E_CFT
!
! 
!      if(Nstat.eq.0)then
! 1-RDM optimization turned off
!        write(UNIout,'(/a/)')' 1-RDM optimization turned off '
!        max_NOONstep = 0
!      else 
        write(UNIout,'(/a)')'-----------------------------'
        write(UNIout,'(a)')'    Optimization of 2-RDM    '
        write(UNIout,'(a)')'-----------------------------'
!      end if
!
! ******************** 2-RDM optimization ******************
! Iterative diagonalization (or rotation) of Fock(-like) matrix, ON optimization step, repeat
!
      max_lambda = 1.0D0
      max_deltaON = 1.0D0
      NOONstep = 0 
      Ldiagstall = .false.
!
      allocate(ONprev(Nbasis),PFeigval(Nbasis))
!
      do while(((max_lambda.gt.epslambda).or.(max_deltaON.gt.epsON)).and.&
               (NOONstep.lt.max_NOONstep))
        write(UNIout,'(/a,I3)')'NO and ON step ',NOONstep+1
!
! This stuff is for old diagonalization method
! Set off-diagonal damping factor
        damp = 1.0D-2
        if(max_lambda.lt.damp)then
         damp = 1.0D-5
          if(max_lambda.gt.1.0D-4) damp = 1.0D-4
          if(max_lambda.gt.1.0D-3) damp = 1.0D-3 
          if(Ldiagstall) damp = damp*1.0D1
        end if
        if(Ldiagstall)then
          Ldiagstall = .false.
          write(UNIout,'(/a)')'Diagonalization stalled, resetting zeta'
        end if
!
! Optimize NOs
!
! Diagonalization, annoyingly finds the lowest energy pairing most of the time
!          call CFT_NO_opt_diag ! iterative diagonalization
!
        call CFT_NO_opt_EPUM ! Newton-Raphson with exponential parameterization of unitary matrix
!
! Update NO active space, and then integrals
        call CFT_BLD_NO_pairs
        call CFT_BLD_energyOTEI
!
! Optimize ONs for new NOs
        ONprev = ON
! Perform optimization without dynamic functional first
!        if(dynamic.eq.'CS')then
!          dynamic = 'NONE'
!          call CFT_ON_opt
!          dynamic = 'CS'
!        else if(dynamic.eq.'OF')then
!          dynamic = 'NONE'
!          call CFT_ON_opt
!          dynamic = 'OF'
!        end if
!
        call CFT_ON_opt
!
        max_deltaON = maxval(dabs(ONprev-ON))
!
        call CFT_BLD_lambda
!
! Check the hermiticity of lambda
        max_lambda = maxval(dabs(lambda-transpose(lambda)))
!
        write(UNIout,'(a)')'----------------------------------------------------'
        write(UNIout,'(a)')'            1-RDM convergence diagnostics           '
        write(UNIout,'(a)')'----------------------------------------------------'
        write(UNIout,'(a,F14.10)')'maximum occupancy change        : ',max_deltaON
        write(UNIout,'(a,F14.10)')'maximum pseudo-Fock off-diagonal: ',max_lambda
!
        NOONstep = NOONstep + 1
!
      end do ! 1-RDM optimization
!
      end select ! optmethod
!
! Post DeltaNO dynamic correlation
      select case(dynamic)
!
! Get Colle-Salvetti energy
      case('CS') 
        call GET_object('CFT','dynamic_Ec','CS')
! Get OSEC+FHC energy
      case('OF') 
        call GET_object('CFT','dynamic_Ec','OSEC')
        call GET_object('CFT','dynamic_Ec','FHC')
! Get MP2 energy
      case('MP2')
        call GET_object('CFT','ENERGY','MP2')
! Get CCSD energy
      case('CCD','CCSD')
        call GET_object('CFT','ENERGY','CCSD')
      case default

      end select
!
      MUN_prtlev = 1
      call CFT_energy
!
      write(UNIout,'(/a)')'   Final occupation numbers   '
      write(UNIout,'(a)')'-------------------------------'
      write(UNIout,'(a)')'       a            2 n_a      '
      write(UNIout,'(a)')'-------------------------------'
      do aNO = 1, NBelectrons
        write(UNIout,'(2X,I6,10X,F10.8)')aNO,2.0D0*ON(aNO)
      end do ! INO
      do aNO = NBelectrons+1, NAelectrons
        write(UNIout,'(2X,I6,10X,F10.8)')aNO,ON(aNO)
      end do ! INO
      if(Nstat.gt.0)then
        do aNO = NAelectrons+1, vac_act(Nvac_act)
          write(UNIout,'(2X,I6,10X,F10.8)')aNO,2.0D0*ON(aNO)
        end do ! INO
      end if
!
      write(UNIout,'(/a)')'        Final Deltas   '
      write(UNIout,'(a)')'-------------------------------'
      write(UNIout,'(a)')'    p     q       Delta_pq     '
      write(UNIout,'(a)')'-------------------------------'
      if(Nstat.gt.0)then
        do Istat = 1, Nstat
          write(UNIout,'(2X,I3,3X,I3,7X,F10.8)')stat_pair(Istat,1),stat_pair(Istat,2)&
                                                ,Delta(stat_pair(Istat,1),stat_pair(Istat,2))
        end do ! Istat
      end if
!
! Calculate and print the momentum balance
      call CFT_MB_calc
!
!      call CFT_pair_pot
!
!      if(Nstat.gt.0)then
!        call CFT_Delta_hessian
!      end if
!
! Write final NO coefficients
      open(UNIT=33, FILE=NOfile, STATUS='Replace')
      do aNO = 1, Nbasis
        do iAO = 1, Nbasis
          write(33,*) NO(iAO,aNO)
        end do ! iAO
      end do ! aNO
      close(33)
!
      write(UNIout,'(/a,a/)')' Final orbitals written to: ',NOfile
!
! Write final Deltas
      open(UNIT=33, FILE=Deltafile, STATUS='Replace')
      write(33,'(I4)') Nstat
      do Istat = 1, Nstat
        iNO = stat_pair(Istat,1)
        rNO = stat_pair(Istat,2)
        write(33,'(I4,X,I4,X,F18.10)') iNO,rNO,Delta(INO,rNO)
      end do ! Istat
      close(33)
!
      write(UNIout,'(/a,a/)')' Final Deltas written to: ',Deltafile
!
! Output orbitals in GAMESS format for pasting into output file for visualization
      call CFT_NO_GAMESS
!
! Let's copy over CMO%coeff with NO just so the NOs can be used more easily in other applications
! (e.g. 2-RDM calculations)
      CMO%coeff = NO
!      
! Set number of actually occupied orbitals
      if(Nvac_act.gt.0)then
        Nnatorb = vac_act(Nvac_act)
      else
        Nnatorb = NAelectrons
      end if
!
!      call CFT_BLD_2RDM
!
! end subroutine CFT_calc
      call PRG_manager ('exit', 'CFT_calc', 'UTILITY')
!
      end subroutine CFT_calc
      subroutine CFT_energy
!*****************************************************************
!     Date last modified: April 13, 2018                         *
!     Author: JWH                                                *
!     Description: Calculate CFT energy.                         *
!*****************************************************************
!Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE CFT_objects
      USE QM_objects
      USE CCSD_objects
      USE MP2_objects

      implicit none
!
! Local scalars: 
      integer :: iAO, jAO, aNO, bNO, Aact, Bact
      integer :: Iclosed, Iopen
      double precision :: VNN
!
! Local arrays: 
      double precision, allocatable, dimension(:,:) :: c
!
! Local functions:
      double precision :: GET_Enuclear
!
! Begin:
      call PRG_manager ('enter', 'CFT_energy', 'UTILITY')
!
      VNN = GET_Enuclear()
!
! Get integrals required for energy calculation
      call CFT_BLD_energyOTEI
!
! Hartree-Fock (-like) energy
!
! 1e energy
      E_CFT_T = 0.0D0
      E_CFT_VNe = 0.0D0
      do Iclosed = 1, Nclosed ! doubly occupied
        aNO = NO_closed(Iclosed)
        E_CFT_T = E_CFT_T + 2.0D0*ON(aNO)*T_NO(aNO,aNO)
        E_CFT_VNe = E_CFT_VNe + 2.0D0*ON(aNO)*V_NO(aNO,aNO)
      end do 
      do Iopen = 1, Nopen ! singly occupied
        aNO = NO_open(Iopen)
        E_CFT_T = E_CFT_T + ON(aNO)*T_NO(aNO,aNO)
        E_CFT_VNe = E_CFT_VNe + ON(aNO)*V_NO(aNO,aNO)
      end do 
!
      E_CFT_HF = E_CFT_T + E_CFT_VNe
!
! 2e energy (density matrix was created in CFT_BLD_Gmatrix in CFT_BLD_energyOTEI)
      E_CFT_Vee = 0.0D0
      do iAO = 1, Nbasis
        do jAO = 1, Nbasis
          E_CFT_Vee = E_CFT_Vee + (0.5D0*Pcl_NO(iAO,jAO) + Pop_NO(iAO,jAO))*Gcl_AO(iAO,jAO)&
                                  + 0.5D0*Pop_NO(iAO,jAO)*Gop_AO(iAO,jAO)
        end do ! jAO
      end do ! iAO
!
      E_CFT_HF = E_CFT_HF + E_CFT_Vee
!
! pair correction
      E_CFT_pair = 0.0D0
!
      do aNO = 1, Nbasis
        do bNO = 1, Nbasis
          E_CFT_pair = E_CFT_pair + eta(aNO,bNO)*(2.0D0*J_NO(aNO,bNO) - K_NO(aNO,bNO))
        end do ! bNO
      end do ! aNO
!
! static correlation
      E_CFT_stat = 0.0D0
!
      c = zeta - xi 
!
      do aNO = 1, Nbasis
        do bNO = 1, Nbasis
          E_CFT_stat = E_CFT_stat + c(aNO,bNO)*K_NO(aNO,bNO)
        end do ! bNO
      end do ! aNO
!
! high-spin correction
      if(LHSCon)then
!
      do aNO = 1, Nbasis
        do bNO = 1, Nbasis
!
          E_CFT_stat = E_CFT_stat - kappa(aNO,bNO)*K_NO(aNO,bNO)
!
        end do ! aNO
      end do ! bNO
!
      end if ! HSC
!
! dynamic cumulant energy
!
      select case(dynamic)
        case('CS')
!
          E_CFT_dyn = E_CS
!
        case('OF')
!
          E_CFT_dyn = E_OSEC + E_FHCaa + E_FHCbb
!
        case('MP2')
!
          E_CFT_dyn = E_MP2
!
        case('CCD','CCSD')
!
          E_CFT_dyn = E_CCSD
!
        case('TDF')
          !call GET_object('CFT','dynamic_Ec','TDF')
          E_TDF = 0.0D0
          call CFT_TDF_energy
          E_CFT_dyn = E_TDF
!
        case('ODF')
          E_ODF = 0.0D0
          call CFT_ODF_energy
          E_CFT_dyn = E_ODF
!
        case default
!
          E_CFT_dyn = 0.0D0
!
      end select
!
! Sum CFT energy
      E_CFT = E_CFT_HF + E_CFT_pair + E_CFT_stat + E_CFT_dyn
!
      if(MUN_prtlev.gt.0)then
      write(UNIout,'(/a)')'------------------------------------------------------------------------'
      write(UNIout,'(a)')'                             CFT energy components        '
      write(UNIout,'(a)')'------------------------------------------------------------------------'
      write(UNIout,'(/a,5X,F16.10)')'                                 E_T (kinetic) =',E_CFT_T
      write(UNIout,'(a,5X,F16.10)')'                      E_Vne (electron-nucleus) =',E_CFT_VNe
      write(UNIout,'(a,5X,F16.10)')'                       E_Vnn (nucleus-nucleus) =',VNN
      write(UNIout,'(a,5X,F16.10)')'                                          E_1e =',E_CFT_T + E_CFT_VNe
      write(UNIout,'(a,5X,F16.10)')'                                          E_2e =',E_CFT - E_CFT_T &
                                                                         &- E_CFT_VNe
      write(UNIout,'(a,5X,F16.10)')'                        Pair correction energy =',E_CFT_pair
      write(UNIout,'(a,5X,F16.10)')'                        Static cumulant energy =',E_CFT_stat
      if(dynamic.eq.'OF')then
        write(UNIout,'(a,5X,F16.10)')'     Dynamic cumulant energy (alpha-beta cusp) =',E_OSEC
        write(UNIout,'(a,5X,F16.10)')'         Dynamic cumulant energy (alpha-alpha) =',E_FHCaa
        write(UNIout,'(a,5X,F16.10)')'           Dynamic cumulant energy (beta-beta) =',E_FHCbb
      else
        write(UNIout,'(a,5X,F16.10)')'                       Dynamic cumulant energy =',E_CFT_dyn
      end if
      write(UNIout,'(a,5X,F16.10)')  '                              Total CFT energy =',E_CFT+VNN
      write(UNIout,'(a/)')'-------------------------------------------------------------------------'
!
      end if
!
! end routine CFT_energy
      call PRG_manager ('exit', 'CFT_energy', 'UTILITY')
      end subroutine CFT_energy 
      subroutine CFT_Delta_to_ON
!***********************************************************************
!     Date last modified: April 6, 2020                                *
!     Author: JWH                                                      *
!     Description: Calculate ONs from Deltas.                          *
!***********************************************************************
! Modules:
      USE CFT_objects
      USE QM_objects
      USE type_molecule
      USE program_files

      implicit none
!
! Local scalars:
      integer :: Istat, iNO, rNO
      double precision :: Delta_scale, Delta_scale_new
      logical :: Lscale
!
! Begin:
!
      call PRG_manager('enter','CFT_Delta_to_ON','UTILITY')
!
      Lscale = .false.
      Delta_scale = 1.0D0
!
      if(allocated(ON))then
        deallocate(ON)
      end if
      allocate(ON(Nbasis))
!
! Determine number of alpha and beta electrons
!
      NAelectrons = (Nelectrons + Multiplicity - 1)/2
      NBelectrons = Nelectrons - NAelectrons
!
! set RHF occupancies
      ON(1:NAelectrons) = 1.0D0
      ON(NAelectrons+1:Nbasis) = 0.0D0
!
      do Istat = 1, Nstat
        iNO = stat_pair(Istat,1)
        rNO = stat_pair(Istat,2)
        ON(iNO) = ON(iNO) - Delta(iNO,rNO)
        ON(rNO) = ON(rNO) + Delta(iNO,rNO)
      end do ! Istat
!
! It is possible during optimization to have n's > 1 or < 0
      do iNO = 1, Nbasis
        if(ON(iNO).lt.0.0D0)then
          Delta_scale_new = (1.0D0-1.0D-10)/(1.0D0 - ON(iNO))
          Lscale = .true.
          if(Delta_scale_new.lt.Delta_scale) Delta_scale = Delta_scale_new
        end if
        if(ON(iNO).gt.1.0D0)then
          Delta_scale_new = (1.0D0-1.0D-10)/ON(iNO)
          Lscale = .true.
          if(Delta_scale_new.lt.Delta_scale) Delta_scale = Delta_scale_new
        end if
      end do ! iNO
!
      if(Lscale)then
        write(UNIout,'(a,F14.10)')'ON bounds violated, scaling Deltas by: ', Delta_scale
        Delta = Delta_scale*Delta
!
        ON(1:NAelectrons) = 1.0D0
        ON(NAelectrons+1:Nbasis) = 0.0D0
!
        do Istat = 1, Nstat
          iNO = stat_pair(Istat,1)
          rNO = stat_pair(Istat,2)
          ON(iNO) = ON(iNO) - Delta(iNO,rNO)
          ON(rNO) = ON(rNO) + Delta(iNO,rNO)
        end do ! Istat
!
      end if ! scale Deltas and get new ONs
!
! End of routine CFT_Delta_to_ON
      call PRG_manager('exit','CFT_Delta_to_ON','UTILITY')
      end subroutine CFT_Delta_to_ON
      subroutine CFT_Delta_to_theta
!***********************************************************************
!     Date last modified: March 31, 2016                               *
!     Author: JWH                                                      *
!     Description: Creates theta vector for optimization of occupation *
!                  number Deltas.                                      * 
!***********************************************************************
! Modules:
      USE CFT_objects

      implicit none
!
! Local scalars:
      integer :: Istat, iNO, rNO
!
! Begin:
!
      call PRG_manager('enter','CFT_Delta_to_theta','UTILITY')
!
      if(allocated(theta))then
        deallocate(theta)
      end if
      allocate(theta(Nstat))
!
! Optimization parameters are the thetas associated with each delta
      do Istat = 1, Nstat
        iNO = stat_pair(Istat,1)
        rNO = stat_pair(Istat,2)
        ! modify amplitude
        theta(Istat) =&
        !dacos(dsqrt(Delta(iNO,rNO)/min(ON(iNO) + Delta(iNO,rNO),1.0D0 - ON(rNO) + Delta(iNO,rNO))))
        !dacos(dsqrt(Delta(iNO,rNO)))
        dacos(dsqrt(2.0D0*Delta(iNO,rNO)))
      end do ! Istat
!
! End of routine CFT_Delta_to_theta
      call PRG_manager('exit','CFT_Delta_to_theta','UTILITY')
      end subroutine CFT_Delta_to_theta
      subroutine CFT_theta_to_Delta
!***********************************************************************
!     Date last modified: March 31, 2016                               *
!     Author: JWH                                                      *
!     Description: Updates Deltas from theta vector.                   *
!***********************************************************************
! Modules:
      USE CFT_objects

      implicit none
!
! Local:
      integer :: iNO, rNO, Istat
!
! Begin:
!
      call PRG_manager('enter','CFT_theta_to_Delta','UTILITY')
!
      do Istat = 1, Nstat
        iNO = stat_pair(Istat,1)
        rNO = stat_pair(Istat,2)
        ! modify amplitude
        Delta(iNO,rNO) = &
        !min(ON(iNO) + Delta(iNO,rNO), 1.0D0 - ON(rNO) + Delta(iNO,rNO))*dcos(theta(Istat))**2.0D0
        !dcos(theta(Istat))**2.0D0
        0.5D0*dcos(theta(Istat))**2.0D0
      end do ! aNO
!
!        write(6,*)' Deltas'
!        do Istat = 1, Nstat
!          iNO = stat_pair(Istat,1)
!          rNO = stat_pair(Istat,2)
!          write(6,'(I4,X,I4,X,F20.10)')iNO, rNO, Delta(iNO,rNO)
!        end do
!        call flush(6)
!
! End of routine CFT_theta_to_Delta
      call PRG_manager('exit','CFT_theta_to_Deta','UTILITY')
      end subroutine CFT_theta_to_Delta
      subroutine CFT_Delta_gradient
!***********************************************************************
!     Date last modified: July 31, 2020                                *
!     Author: J.W. Hollett                                             *
!     Description: Calculate the gradient of the energy wrt Deltas.    *
!***********************************************************************
! Modules:
      USE program_constants
      USE CFT_objects
      USE QM_objects

      implicit none
!
! Local scalars:
      integer :: kNO, tNO, iNO, rNO, Istat
      integer :: pNO, Pocc, qNO, Qvac
      integer :: Pact, Qact, Qocc
!
! Begin:
!
      call PRG_manager('enter','CFT_Delta_gradient','UTILITY')
!
! Get derivatives of energy with respect to each occupation number
!
      if(allocated(grad_Delta))then
        deallocate(grad_Delta)
      end if
      allocate(grad_Delta(Nstat))
!
! HF-like (1e plus 0-1RDM)
      do Istat = 1, Nstat
        kNO = stat_pair(Istat,1)
        tNO = stat_pair(Istat,2)
!
        grad_Delta(Istat) = 2.0D0*(h_NO(tNO,tNO) + Gcl_NO(tNO,tNO) + Goc_NO(tNO,tNO)&
                                  -h_NO(kNO,kNO) - Gcl_NO(kNO,kNO) - Goc_NO(kNO,kNO))
!
      end do ! Istat
!
! pair correction (eta)
!
      call CFT_BLD_deta
!
      do Istat = 1, Nstat
!
        do Pact = 1, Nact
          pNO = NO_act(Pact)
          do Qact = 1, Nact
            qNO = NO_act(Qact)
!
            grad_Delta(Istat) = grad_Delta(Istat)&
                              + deta(pNO,qNO,Istat)*(2.0D0*J_NO(pNO,qNO) - K_NO(pNO,qNO))
!
          end do ! Qact
        end do ! Pact
!
      end do ! Istat
!
! intrapair correlation (xi)
!
      call CFT_BLD_dxi
!
      do Istat = 1, Nstat
!
        do Pocc = 1, Nocc_act
          pNO = occ_act(Pocc)
          do Qvac = 1, Nvac_act
            qNO = vac_act(Qvac)
!
            grad_Delta(Istat) = grad_Delta(Istat) - 2.0D0*dxi(pNO,qNO,Istat)*K_NO(pNO,qNO)
!
          end do ! Qvac
        end do ! Pvac
!
      end do ! Istat 
!
! intra (and inter) pair correlation correction (zeta)
!
      call CFT_BLD_dzeta
!
      do Istat = 1, Nstat
!
        do Pact = 1, Nact
          pNO = NO_act(Pact)
          do Qact = 1, Nact
            qNO = NO_act(Qact)
!
            grad_Delta(Istat) = grad_Delta(Istat) + dzeta(pNO,qNO,Istat)*K_NO(pNO,qNO)
!
          end do ! Qvac
        end do ! Pvac
!
      end do ! Istat 
!
! high-spin correction (kappa)
      if(LHSCon)then
!
      call CFT_BLD_dkappa
!
      do Istat = 1, Nstat
!
        do Pocc = 1, Nocc
          pNO = NO_occ(Pocc)
          do Qocc = 1, Nocc
            qNO = NO_occ(Qocc)
!
            grad_Delta(Istat) = grad_Delta(Istat)&
                              - dkappa(pNO,qNO,Istat)*K_NO(pNO,qNO)
!
          end do ! Qocc
        end do ! Pocc
!
      end do ! Istat
!
      end if ! HSC
!
! dynamic cumulant part
!      select case(dynamic)
! For Nonvariational dynamic cumulant
      select case('NONE')
!
        case('CS')
!
! Get derivative of CS wrt Delta
          call KILL_object ('CFT', 'dEdyndD', 'CS')      
          call GET_object ('CFT', 'dEdyndD', 'CS')
!
!          write(6,*)'CS contribution to gradient'
          do Istat = 1, Nstat
            grad_Delta(Istat) = grad_Delta(Istat) + dECSdD(Istat)
!            write(6,'(I4,F24.12)')Istat,dECSdD(Istat)
          end do ! Istat
!
        case('OF')
!
! Get derivative of OSEC wrt Delta
          call KILL_object ('CFT', 'dEdyndD', 'OSEC')      
          call GET_object ('CFT', 'dEdyndD', 'OSEC')
!
! Get derivative of FHC wrt Delta
          call KILL_object ('CFT', 'dEdyndD', 'FHC')      
          call GET_object ('CFT', 'dEdyndD', 'FHC')
!
!          write(6,*)'OSEC contribution to gradient'
!          do Istat = 1, Nstat
!            write(6,'(I4,F24.12)')Istat,dEOSECdD(Istat)
!          end do ! Istat
!          write(6,*)'FHC contribution to gradient'
!          do Istat = 1, Nstat
!            write(6,'(I4,F24.12)')Istat,dEFHCdD(Istat)
!          end do ! Istat
          do Istat = 1, Nstat
            grad_Delta(Istat) = grad_Delta(Istat) + dEOSECdD(Istat) + dEFHCdD(Istat)
          end do ! Istat
!
        case default
!
      end select ! dynamic
!
!      write(6,*)'Gradient (Delta)'
!      do Istat = 1, Nstat
!        write(6,'(I4,2X,f24.10)')Istat,grad_Delta(Istat)
!      end do 
!
! Calculate gradient numerically
!      Delta_step = 0.00000001D0
!      call CFT_energy
!      E_0 = E_CFT
!      write(6,*)'Gradient (Delta) numerical'
!      do Istat = 1, Nstat
!        iNO = stat_pair(Istat,1)
!        rNO = stat_pair(Istat,2)
!
!        Delta(iNO,rNO) = Delta(iNO,rNO) + Delta_step
!        call CFT_Delta_to_ON
!        call CFT_BLD_ezxk
!        call CFT_energy
!
!        write(6,'(I4,2X,f24.10)')Istat,(E_CFT - E_0)/Delta_step
!
!        Delta(iNO,rNO) = Delta(iNO,rNO) - Delta_step
!        call CFT_Delta_to_ON
!        call CFT_BLD_ezxk
!
!      end do ! Istat
!      call flush(6)
!
! Use chain rule to get derivatives with respect to theta
!
      if(allocated(grad_theta))then
        deallocate(grad_theta)
      end if
      allocate(grad_theta(Nstat))
!
      do Istat = 1, Nstat
          iNO = stat_pair(Istat,1)
          rNO = stat_pair(Istat,2)
          ! amplitude modification
          !grad_theta(Istat) = -grad_Delta(Istat)*dsin(2.0D0*theta(Istat))&
          !                    *min(ON(iNO) + Delta(iNO,rNO), 1.0D0 - ON(rNO) + Delta(iNO,rNO))
          !grad_theta(Istat) = -grad_Delta(Istat)*dsin(2.0D0*theta(Istat))
          grad_theta(Istat) = -0.5D0*grad_Delta(Istat)*dsin(2.0D0*theta(Istat))
      end do ! cNO
!
!      write(6,*)'Gradient (theta)'
!      do Istat = 1, Nstat
!        write(6,'(I4,2X,f14.10)')Istat,grad_theta(Istat)
!      end do 
!
! End of routine CFT_Delta_gradient
      call PRG_manager('exit','CFT_Delta_gradient','UTILITY')
      end subroutine CFT_Delta_gradient
      subroutine CFT_BLD_deta
!***********************************************************************
!     Date last modified: July 31, 2020                                *
!     Author: J.W. Hollett                                             *
!     Description: Calculate the gradient of the eta coefficient.      *
!***********************************************************************
! Modules:
      USE program_constants
      USE CFT_objects
      USE QM_objects

      implicit none
!
! Local scalars:
      integer :: kNO, tNO, rNO, Istat, sNO
      integer :: pNO, qNO
      integer :: Jstat, Kstat
!
! Local functions:
      double precision :: Kronecker_delta
!
! Begin:
!
      call PRG_manager('enter','CFT_BLD_deta','UTILITY')
!
      if(allocated(deta))then
        deallocate(deta)
      end if
      allocate(deta(Nbasis,Nbasis,Nstat))
      deta = 0.0D0
!
      do Istat = 1, Nstat
        kNO = stat_pair(Istat,1)
        tNO = stat_pair(Istat,2)
!
        deta(kNO,kNO,Istat) = -1.0D0 + 2.0D0*ON(kNO)
        deta(tNO,tNO,Istat) =  1.0D0 - 2.0D0*ON(tNO)
!
        do Jstat = 1, Nstat
          pNO = stat_pair(Jstat,1)
          qNO = stat_pair(Jstat,2)
!
          deta(pNO,qNO,Istat) = deta(pNO,qNO,Istat) + Kronecker_delta(pNO,kNO)*Kronecker_delta(qNO,tNO)&
                                                     *(ON(qNO) - ON(pNO) - Delta(pNO,qNO))&
                                                + Delta(pNO,qNO)*(Kronecker_delta(qNO,tNO)&
                                                                + Kronecker_delta(pNO,kNO)& 
                                       - Kronecker_delta(pNO,kNO)*Kronecker_delta(qNO,tNO))
          deta(qNO,pNO,Istat) = deta(pNO,qNO,Istat)
!
          do Kstat = 1, Nstat
            if(Jstat.eq.Kstat)cycle
          
            rNO = stat_pair(Kstat,1)
            sNO = stat_pair(Kstat,2)
!
            if(pNO.eq.rNO)then 
              deta(qNO,sNO,Istat) = deta(qNO,sNO,Istat)&
                                  - Kronecker_delta(pNO,kNO)*Kronecker_delta(qNO,tNO)*Delta(rNO,sNO)&
                                  - Kronecker_delta(rNO,kNO)*Kronecker_delta(sNO,tNO)*Delta(pNO,qNO)
            end if 
!
            if(qNO.eq.sNO)then
              deta(pNO,rNO,Istat) = deta(pNO,rNO,Istat)&
                                  - Kronecker_delta(pNO,kNO)*Kronecker_delta(qNO,tNO)*Delta(rNO,sNO)&
                                  - Kronecker_delta(rNO,kNO)*Kronecker_delta(sNO,tNO)*Delta(pNO,qNO)
            end if 
!
          end do ! Kstat
        end do ! Jstat
!
      end do ! Istat 
!
! End of routine CFT_BLD_deta
      call PRG_manager('exit','CFT_BLD_deta','UTILITY')
      end subroutine CFT_BLD_deta
      subroutine CFT_BLD_dzeta
!***********************************************************************
!     Date last modified: July 31, 2020                                *
!     Author: J.W. Hollett                                             *
!     Description: Calculate the gradient of the zeta coefficient.     *
!***********************************************************************
! Modules:
      USE program_constants
      USE CFT_objects
      USE QM_objects

      implicit none
!
! Local scalars:
      integer :: aNO, kNO, tNO, rNO, Istat, Icoup
      integer :: Rvac, bNO, pNO, Pvac, qNO, Qvac
!
! Local arrays:
      integer, allocatable, dimension(:) :: NO2cp
!
! Local functions:
      double precision :: Kronecker_delta
!
! Begin:
!
      call PRG_manager('enter','CFT_BLD_dzeta','UTILITY')
!
      allocate(NO2cp(Nbasis)) 
      NO2cp = 0
      do Icoup = 1, Ncoup
        aNO = coup_pair(Icoup,1)
        bNO = coup_pair(Icoup,2)
        ! set back translator
        NO2cp(aNO) = Icoup
        NO2cp(bNO) = Icoup
      end do ! Icoup 
!
      if(allocated(dzeta))then
        deallocate(dzeta)
      end if
      allocate(dzeta(Nbasis,Nbasis,Nstat))
      dzeta = zero
!
      do Istat = 1, Nstat
        kNO = stat_pair(Istat,1)
        tNO = stat_pair(Istat,2)
!
        if(coupNO(kNO).eq.0)then ! k not coupled
!
          do Pvac = 1, Nstat_occ(kNO)
            pNO = occ_stat(kNO,Pvac)
            do Qvac = 1, Nstat_occ(kNO)
              qNO = occ_stat(kNO,Qvac)
              if(pNO.eq.qNO)cycle
!
              dzeta(pNO,qNO,Istat) = dzeta(pNO,qNO,Istat)&
                                   + 0.5D0*(Kronecker_delta(pNO,tNO)/Delta(kNO,pNO)&
                                          + Kronecker_delta(qNO,tNO)/Delta(kNO,qNO))& 
                                          *dsqrt(Delta(kNO,pNO)*Delta(kNO,qNO))
              dzeta(pNO,qNO,Istat) = sigma(kNO,pNO)*sigma(kNO,qNO)*dzeta(pNO,qNO,Istat)
!
            end do ! Qvac            
          end do ! Pvac            
!
        else ! k coupled
! let k=a 
          bNO = coupNO(kNO)
          Icoup = NO2cp(kNO)
!
! zeta_pq
          do Pvac = 1, Nstat_occ(kNO)
            pNO = occ_stat(kNO,Pvac) 
            do Qvac = 1, Nstat_occ(kNO)
              qNO = occ_stat(kNO,Qvac) 
              if(pNO.eq.qNO)cycle
!
              dzeta(pNO,qNO,Istat) = dzeta(pNO,qNO,Istat)&
                                   + 0.5D0*(Kronecker_delta(pNO,tNO)/Delta(kNO,pNO)&
                                          + Kronecker_delta(qNO,tNO)/Delta(kNO,qNO))&
                                          *dsqrt(Delta(kNO,pNO)*Delta(kNO,qNO)&
                                                *(ON(bNO) + Delta(bNO,pNO))*(ON(bNO) + Delta(bNO,qNO)))
!
            end do ! Qvac
          end do ! Pvac
!
          do Pvac = 1, Nstat_occ(bNO)
            pNO = occ_stat(bNO,Pvac) 
            do Qvac = 1, Nstat_occ(bNO)
              qNO = occ_stat(bNO,Qvac) 
              if(pNO.eq.qNO)cycle
!
              dzeta(pNO,qNO,Istat) = dzeta(pNO,qNO,Istat)&
                                   + 0.5D0*((-1.0D0 + Kronecker_delta(pNO,tNO))&
                                           /(ON(kNO) + Delta(kNO,pNO))&
                                          + (-1.0D0 + Kronecker_delta(qNO,tNO))&
                                           /(ON(kNO) + Delta(kNO,qNO)))&
                                          *dsqrt(Delta(bNO,pNO)*Delta(bNO,qNO)&
                                                *(ON(kNO) + Delta(kNO,pNO))*(ON(kNO) + Delta(kNO,qNO)))
!
            end do ! Qvac
          end do ! Pvac
!
          do Pvac = 1, Nvac_act
            pNO = vac_act(Pvac)
            do Qvac = 1, Nvac_act
              qNO = vac_act(Qvac)
              if(pNO.eq.qNO)cycle
              do Rvac = 1, Nvac_act
                  rNO = vac_act(Rvac)
                  if(rNO.eq.pNO)cycle
                  if(rNO.eq.qNO)cycle
!
                  dzeta(pNO,qNO,Istat) = dzeta(pNO,qNO,Istat)&
                                       + 0.5D0*((Kronecker_delta(pNO,tNO)*Delta(bNO,rNO)&
                                               + Kronecker_delta(rNO,tNO)*Delta(bNO,pNO))&
                                       /(Delta(kNO,pNO)*Delta(bNO,rNO) + Delta(kNO,rNO)*Delta(bNO,pNO))&
                                              + (Kronecker_delta(qNO,tNO)*Delta(bNO,rNO)&
                                               + Kronecker_delta(rNO,tNO)*Delta(bNO,qNO))&
                                       /(Delta(kNO,qNO)*Delta(bNO,rNO) + Delta(kNO,rNO)*Delta(bNO,qNO)))&
                                 *dsqrt((Delta(kNO,pNO)*Delta(bNO,rNO) + Delta(kNO,rNO)*Delta(bNO,pNO))&
                                       *(Delta(kNO,qNO)*Delta(bNO,rNO) + Delta(kNO,rNO)*Delta(bNO,qNO)))
!
              end do ! Rvac
            end do ! Qvac
          end do ! Pvac
!
! zeta_ab
!
          do Pvac = 1, Nvacshr(Icoup)
            pNO = vacshr(Icoup,Pvac) 
!
            dzeta(kNO,bNO,Istat) = dzeta(kNO,bNO,Istat)&
                                 + 0.5D0*(Kronecker_delta(pNO,tNO)/Delta(kNO,pNO)&
                                        + (-1.0D0 + Kronecker_delta(pNO,tNO))&
                                         /(ON(kNO) + Delta(kNO,pNO)))&
                                  *dsqrt(Delta(kNO,pNO)*Delta(bNO,pNO)&
                                        *(ON(kNO) + Delta(kNO,pNO))*(ON(bNO) + Delta(bNO,pNO)))
            dzeta(bNO,kNO,Istat) = dzeta(kNO,bNO,Istat)
!
          end do ! Pvac 
!
        end if ! k coupled or not
!
      end do ! Istat 
!
! End of routine CFT_BLD_dzeta
      call PRG_manager('exit','CFT_BLD_dzeta','UTILITY')
      end subroutine CFT_BLD_dzeta
      subroutine CFT_BLD_dxi
!***********************************************************************
!     Date last modified: July 31, 2020                                *
!     Author: J.W. Hollett                                             *
!     Description: Calculate the gradient of the xi coefficient.       *
!***********************************************************************
! Modules:
      USE program_constants
      USE CFT_objects
      USE QM_objects

      implicit none
!
! Local scalars:
      integer :: aNO, kNO, tNO, Istat, Icoup
      integer :: bNO, pNO, Pocc, Pvac, qNO, Qvac
!
! Local arrays:
      integer, allocatable, dimension(:) :: NO2cp
!
! Local functions:
      double precision :: Kronecker_delta
!
! Begin:
!
      call PRG_manager('enter','CFT_BLD_dxi','UTILITY')
!
! static correlation
!
      allocate(NO2cp(Nbasis)) 
      NO2cp = 0
      do Icoup = 1, Ncoup
        aNO = coup_pair(Icoup,1)
        bNO = coup_pair(Icoup,2)
        ! set back translator
        NO2cp(aNO) = Icoup
        NO2cp(bNO) = Icoup
      end do ! Icoup 
!
! Determine derivative of xi's for use with both terms (static correlation and HSC)
!
      if(allocated(dxi))then
        deallocate(dxi)
      end if
      allocate(dxi(Nbasis,Nbasis,Nstat))
      dxi = zero
!
      do Istat = 1, Nstat
        kNO = stat_pair(Istat,1)
        tNO = stat_pair(Istat,2)
!
        if(coupNO(kNO).eq.0)then ! 
!
          do Qvac = 1, Nstat_occ(kNO)
            qNO = occ_stat(kNO,Qvac)
!
            dxi(kNO,qNO,Istat) = 0.5D0*(Kronecker_delta(qNO,tNO)/Delta(kNO,qNO)&
                                - 1.0D0/ON(kNO))*xi(kNO,qNO)
!
          end do ! Qvac
!
        else ! k coupled
!
! Delta then appears in two xi's xi_ap and xi_bp
!
! let k=a 
          bNO = coupNO(kNO)
!
! xi_ap
!
          do Pvac = 1, Nstat_occ(kNO)
            pNO = occ_stat(kNO,Pvac)
!
            dxi(kNO,pNO,Istat) = 0.5D0*((-ON(bNO) - Delta(bNO,tNO))/(ON(kNO)*ON(bNO) + eta(kNO,bNO))&
                               + Kronecker_delta(pNO,tNO)/Delta(kNO,pNO))&
                                *dsqrt((ON(kNO)*ON(bNO) + eta(kNO,bNO))&
                                      *Delta(kNO,pNO)*(ON(bNO) + Delta(bNO,pNO)))
          end do ! Pvac 
!          
          do Pvac = 1, Nvac_act
            pNO = vac_act(Pvac)
            do Qvac = 1, Nstat_occ(bNO)
              qNO = occ_stat(bNO,Qvac)
              if(qNO.eq.pNO)cycle
!
              dxi(kNO,pNO,Istat) = dxi(kNO,pNO,Istat)&
                                 + 0.5D0*((Kronecker_delta(qNO,tNO)*Delta(bNO,pNO)&
                                         + Kronecker_delta(pNO,tNO)*Delta(bNO,qNO))& 
                                   /(Delta(kNO,qNO)*Delta(bNO,pNO) + Delta(kNO,pNO)*Delta(bNO,qNO))& 
                                   + (-1.0D0 + Kronecker_delta(qNO,tNO))/(ON(kNO) + Delta(kNO,qNO)))&
                                 *dsqrt((Delta(kNO,qNO)*Delta(bNO,pNO) + Delta(kNO,pNO)*Delta(bNO,qNO))&
                                        *Delta(bNO,qNO)*(ON(kNO) + Delta(kNO,qNO)))
!
            end do ! Qvac
          end do ! Pvac 
!          
! xi_bp
!
          do Pvac = 1, Nstat_occ(bNO)
            pNO = occ_stat(bNO,Pvac)
!
            dxi(bNO,pNO,Istat) = 0.5D0*((-ON(bNO) - Delta(bNO,tNO))/(ON(kNO)*ON(bNO) + eta(kNO,bNO))&
                               + (-1.0D0 + Kronecker_delta(pNO,tNO))/(ON(kNO) + Delta(kNO,pNO)))&
                                *dsqrt((ON(kNO)*ON(bNO) + eta(kNO,bNO))*Delta(bNO,pNO)&
                                      *(ON(kNO) + Delta(kNO,pNO)))
          end do ! Pvac 
!
          do Pvac = 1, Nvac_act
            pNO = vac_act(Pvac)
            do Qvac = 1, Nstat_occ(kNO)
              qNO = occ_stat(kNO,Qvac)
              if(qNO.eq.pNO)cycle
!
              dxi(bNO,pNO,Istat) = dxi(bNO,pNO,Istat)&
                                 + 0.5D0*((Kronecker_delta(qNO,tNO)*Delta(bNO,pNO)&
                                         + Kronecker_delta(pNO,tNO)*Delta(bNO,qNO))&
                                   /(Delta(kNO,qNO)*Delta(bNO,pNO) + Delta(kNO,pNO)*Delta(bNO,qNO))&
                                 + Kronecker_delta(qNO,tNO)/Delta(kNO,qNO))&
                                  *dsqrt((Delta(kNO,qNO)*Delta(bNO,pNO) + Delta(kNO,pNO)*Delta(bNO,qNO))&
                                        *Delta(kNO,qNO)*(ON(bNO) + Delta(bNO,qNO)))
!
            end do ! Qvac 
          end do ! Pvac 
!
        end if ! k coupled or not
!
      end do ! Istat 
!
! symmetrize for use with kappa
      do Pocc = 1, Nocc_act
        pNO = occ_act(Pocc)
        do Qvac = 1, Nvac_act
          qNO = vac_act(Qvac)
!
! May not need this, see use below for kappa
          dxi(qNO,pNO,1:Nstat) = dxi(pNO,qNO,1:Nstat)
!
        end do ! Qvac
      end do ! Pocc
!
! End of routine CFT_BLD_dxi
      call PRG_manager('exit','CFT_BLD_dxi','UTILITY')
      end subroutine CFT_BLD_dxi
      subroutine CFT_BLD_dkappa
!***********************************************************************
!     Date last modified: July 31, 2020                                *
!     Author: J.W. Hollett                                             *
!     Description: Calculate the gradient of the kappa coefficient.    *
!***********************************************************************
! Modules:
      USE program_constants
      USE CFT_objects
      USE QM_objects

      implicit none
!
! Local scalars:
      integer :: aNO, kNO, tNO, rNO, Istat, sNO, Icoup
      integer :: bNO, pNO, qNO, Iopen
      integer :: Jstat, Kstat
      double precision :: dkap, kap
! Local arrays:
      integer, allocatable, dimension(:) :: NO2cp
!
! Local functions:
      double precision :: Kronecker_delta
!
!
! Begin:
!
      call PRG_manager('enter','CFT_BLD_dkappa','UTILITY')
!
      allocate(NO2cp(Nbasis)) 
      NO2cp = 0
      do Icoup = 1, Ncoup
        aNO = coup_pair(Icoup,1)
        bNO = coup_pair(Icoup,2)
        ! set back translator
        NO2cp(aNO) = Icoup
        NO2cp(bNO) = Icoup
      end do ! Icoup 
!
! high-spin correction
!
      if(allocated(dkappa))then
        deallocate(dkappa)
      end if
      allocate(dkappa(Nbasis,Nbasis,Nstat))
      dkappa = 0.0D0
!
      do Istat = 1, Nstat
        kNO = stat_pair(Istat,1)
        tNO = stat_pair(Istat,2)
!
        do Jstat = 1, Nstat
          pNO = stat_pair(Jstat,1)
          rNO = stat_pair(Jstat,2)
!
          do Iopen = 1, Nopen ! open-shell
            aNO = NO_open(Iopen)
!
            !dkap = 0.353553D0*ON(aNO)*dxi(pNO,rNO,Istat)
            dkap = 0.5D0*on(aNO)*Kronecker_delta(pNO,kNO)*Kronecker_delta(rNO,tNO)
!
            dkappa(aNO,pNO,Istat) = dkap
            dkappa(pNO,aNO,Istat) = dkap
            dkappa(aNO,rNO,Istat) = dkap
            dkappa(rNO,aNO,Istat) = dkap
!
          end do ! Iopen
        end do ! Jstat
!
        do Jstat = 2, Nstat
          pNO = stat_pair(Jstat,1)
          rNO = stat_pair(Jstat,2)
          do Kstat = 1, Jstat-1
            qNO = stat_pair(Kstat,1)
            sNO = stat_pair(Kstat,2)
            if(qNO.eq.pNO)cycle
            if(rNO.eq.sNO)cycle
!
! xi*xi - good for square H4, violates PQG
! D*D   - satisfies PQ, violates G
!
            dkap = dxi(pNO,rNO,Istat)*xi(qNO,sNO) + xi(pNO,rNO)*dxi(qNO,sNO,Istat)
            !kap  = 4.0D0*Delta(pNO,rNO)*Delta(qNO,sNO)
            !dkap = (Kronecker_delta(pNO,kNO)*Kronecker_delta(rNO,tNO)*Delta(qNO,sNO)&
            !      + Kronecker_delta(qNO,kNO)*Kronecker_delta(sNO,tNO)*Delta(pNO,rNO))
!
            dkappa(pNO,qNO,Istat) = dkappa(pNO,qNO,Istat) + dkap
            !dkappa(pNO,qNO,Istat) = dkappa(pNO,qNO,Istat) + on(pNO)*on(qNO)*dkap&
            !                      + ((Kronecker_delta(pNO,tNO) - Kronecker_delta(pNO,kNO))*on(qNO)&
            !                      +  (Kronecker_delta(qNO,tNO) - Kronecker_delta(qNO,kNO))*on(pNO))*kap
            dkappa(qNO,pNO,Istat) = dkappa(pNO,qNO,Istat)
!
            dkappa(pNO,sNO,Istat) = dkappa(pNO,sNO,Istat) + dkap
            !dkappa(pNO,sNO,Istat) = dkappa(pNO,sNO,Istat) + on(pNO)*on(sNO)*dkap&
            !                      + ((Kronecker_delta(pNO,tNO) - Kronecker_delta(pNO,kNO))*on(sNO)&
            !                      +  (Kronecker_delta(sNO,tNO) - Kronecker_delta(sNO,kNO))*on(pNO))*kap
            dkappa(sNO,pNO,Istat) = dkappa(pNO,sNO,Istat)
!
            dkappa(rNO,qNO,Istat) = dkappa(rNO,qNO,Istat) + dkap
            !dkappa(rNO,qNO,Istat) = dkappa(rNO,qNO,Istat) + on(rNO)*on(qNO)*dkap&
            !                      + ((Kronecker_delta(rNO,tNO) - Kronecker_delta(rNO,kNO))*on(qNO)&
            !                      +  (Kronecker_delta(qNO,tNO) - Kronecker_delta(qNO,kNO))*on(rNO))*kap
            dkappa(qNO,rNO,Istat) = dkappa(rNO,qNO,Istat)
!
            dkappa(rNO,sNO,Istat) = dkappa(rNO,sNO,Istat) + dkap
            !dkappa(rNO,sNO,Istat) = dkappa(rNO,sNO,Istat) + on(rNO)*on(sNO)*dkap&
            !                      + ((Kronecker_delta(rNO,tNO) - Kronecker_delta(rNO,kNO))*on(sNO)&
            !                      +  (Kronecker_delta(sNO,tNO) - Kronecker_delta(sNO,kNO))*on(rNO))*kap
            dkappa(sNO,rNO,Istat) = dkappa(rNO,sNO,Istat)
!
          end do ! Kstat
        end do ! Jstat
      end do ! Istat
!
! End of routine CFT_BLD_dkappa
      call PRG_manager('exit','CFT_BLD_dkappa','UTILITY')
      end subroutine CFT_BLD_dkappa
      subroutine CFT_Delta_hessian
!***********************************************************************
!     Date last modified: July 31, 2020                                *
!     Author: J.W. Hollett                                             *
!     Description: Calculates Delta hessian of the energy.             *
!***********************************************************************
! Modules:
      USE program_constants
      USE CFT_objects
      USE QM_objects

      implicit none
!
! Local scalars:
      integer :: Istat, Jstat, kNO, tNO, lNO, uNO, iNO, rNO, sNO
      integer :: Rvac, IJ_index, jNO, aNO, bNO, Icoup, pNO, Pocc, Pvac, qNO, Qvac, Qocc
      integer :: Kstat, Lstat, Pact, Qact, Iopen
! Local arrays:
      integer, allocatable, dimension(:) :: NO2cp
      double precision, allocatable, dimension(:,:) :: hessian_Delta, nneta, xsq1, zsq3
      double precision, allocatable, dimension(:,:,:) :: DDpDD, xsq2, dnneta, xip1, zetap3
      double precision, allocatable, dimension(:,:,:,:) :: d2nneta, dDDpDD, xip2, zetap1
      double precision, allocatable, dimension(:,:,:,:) :: zsq1, zsq2
      double precision, allocatable, dimension(:,:,:,:,:) :: d2DDpDD, zetap2
      double precision :: d2kap, d2nn, dkapkt, dkaplu, dnnkt, dnnlu, kap
!
! Local functions:
      double precision :: Kronecker_delta
!
! Begin:
!
      call PRG_manager('enter','CFT_Delta_hessian','UTILITY')
!
      if(allocated(hessian))then
        deallocate(hessian)
      end if
      allocate(hessian_Delta(Nstat,Nstat))
      allocate(hessian(Nstat,Nstat))
!
      hessian_Delta = 0.0D0
      hessian = 0.0D0
!
! 0-1RDM and pair correction component
!
      do Istat = 1, Nstat
        kNO = stat_pair(Istat,1)
        tNO = stat_pair(Istat,2)
!
        do Jstat = 1, Nstat
          if(Istat.eq.Jstat)cycle
          lNO = stat_pair(Jstat,1)
          uNO = stat_pair(Jstat,2)
!
          if((kNO.ne.lNO).and.(tNO.ne.uNO))then
!
            hessian_Delta(Istat,Jstat) = hessian_Delta(Istat,Jstat)&
            + 4.0D0*(J_NO(kNO,lNO) + J_NO(tNO,uNO) - J_NO(kNO,uNO) - J_NO(lNO,tNO))&
            - 2.0D0*(K_NO(kNO,lNO) + K_NO(tNO,uNO) - K_NO(kNO,uNO) - K_NO(lNO,tNO))
!
          end if
!
        end do ! Jstat
      end do ! Istat
!
! static correlation terms
! 
! calculate some intermediates
      ! can make these arrays smaller later
      allocate(NO2cp(Nbasis))
      allocate(nneta(Nbasis,Nbasis),DDpDD(Ncoup,Nbasis,Nbasis))
      allocate(xsq1(Nbasis,Nbasis),xsq2(Nbasis,Nbasis,Nbasis))
      nneta = 0.0D0
      DDpDD = 0.0D0
      xsq1 = 0.0D0
      xsq2 = 0.0D0
!
      do Icoup = 1, Ncoup
        aNO = coup_pair(Icoup,1)
        bNO = coup_pair(Icoup,2)
        ! set back translator
        NO2cp(aNO) = Icoup
        NO2cp(bNO) = Icoup
!
        nneta(aNO,bNO) = ON(aNO)*ON(bNO) + eta(aNO,bNO)
        nneta(bNO,aNO) = nneta(aNO,bNO)
!
        do Pvac = 1, Nvac_act
          pNO = vac_act(Pvac)
          do Qvac = 1, Nvac_act
            qNO = vac_act(Qvac)
            if(pNO.eq.qNO)cycle
!
            DDpDD(Icoup,qNO,pNO) = Delta(aNO,pNO)*Delta(bNO,qNO) + Delta(aNO,qNO)*Delta(bNO,pNO)
!
          end do ! Qvac
        end do ! Pvac
!
      end do ! Icoup
!
      do Icoup = 1, Ncoup
        aNO = coup_pair(Icoup,1)
        bNO = coup_pair(Icoup,2)
!        
        do Pvac = 1, Nvac_act
          pNO = vac_act(Pvac)
!
          xsq1(aNO,pNO) = dsqrt(nneta(aNO,bNO)*Delta(aNO,pNO)*(ON(bNO) + Delta(bNO,pNO)))
          xsq1(bNO,pNO) = dsqrt(nneta(aNO,bNO)*Delta(bNO,pNO)*(ON(aNO) + Delta(aNO,pNO)))
!
          do Qvac = 1, Nvac_act
            qNO = vac_act(Qvac)
!
            xsq2(aNO,pNO,qNO) = dsqrt(DDpDD(Icoup,pNO,qNO)*Delta(bNO,qNO)*(ON(aNO) + Delta(aNO,qNO)))
            xsq2(bNO,pNO,qNO) = dsqrt(DDpDD(Icoup,pNO,qNO)*Delta(aNO,qNO)*(ON(bNO) + Delta(bNO,qNO)))
!
          end do ! Qvac 
        end do ! Pvac 
!
      end do ! Icoup
!
      allocate(dnneta(Nbasis,Nbasis,Nstat))
      allocate(d2nneta(Nbasis,Nbasis,Nstat,Nstat))
      dnneta = 0.0D0
      d2nneta = 0.0D0
!
      do Istat = 1, Nstat
        kNO = stat_pair(Istat,1)
        tNO = stat_pair(Istat,2)
!
        bNO = coupNO(kNO)
        if(bNO.ne.0)then ! k coupled
!
          dnneta(kNO,bNO,Istat) = -ON(bNO) - Delta(bNO,tNO)
          dnneta(bNO,kNO,Istat) = dnneta(kNO,bNO,Istat)
!
        end if
!
        do Jstat = 1, Nstat
          lNO = stat_pair(Jstat,1)
          uNO = stat_pair(Jstat,2)
!
          if(lNO.eq.bNO)then ! k and l coupled to each other
!
            d2nneta(kNO,bNO,Istat,Jstat) = 1.0D0 - Kronecker_delta(tNO,uNO)
            d2nneta(bNO,kNO,Istat,Jstat) = d2nneta(kNO,bNO,Istat,Jstat)
!
          end if
!
        end do  ! Jstat
      end do  ! Istat
!
      allocate(dDDpDD(Ncoup,Nbasis,Nbasis,Nstat),d2DDpDD(Ncoup,Nbasis,Nbasis,Nstat,Nstat))
      dDDpDD = 0.0D0
      d2DDpDD = 0.0D0
!
      do Istat = 1, Nstat
        kNO = stat_pair(Istat,1)
        tNO = stat_pair(Istat,2)
!
        if(coupNO(kNO).ne.0)then ! k coupled
          Icoup = NO2cp(kNO)
          aNO = coup_pair(Icoup,1)
          bNO = coup_pair(Icoup,2)
!
          do Pvac = 1, Nvac_act
            pNO = vac_act(Pvac)
            do Qvac = 1, Nvac_act
              qNO = vac_act(Qvac)
              if(pNO.eq.qNO)cycle
!
              dDDpDD(Icoup,pNO,qNO,Istat) = &
                                    Kronecker_delta(aNO,kNO)*Kronecker_delta(qNO,tNO)*Delta(bNO,pNO)&
                                  + Kronecker_delta(bNO,kNO)*Kronecker_delta(pNO,tNO)*Delta(aNO,qNO)&
                                  + Kronecker_delta(aNO,kNO)*Kronecker_delta(pNO,tNO)*Delta(bNO,qNO)&
                                  + Kronecker_delta(bNO,kNO)*Kronecker_delta(qNO,tNO)*Delta(aNO,pNO)
!
            end do ! Qvac
          end do ! Pvac
!
        end if
!
        do Jstat = 1, Nstat
          lNO = stat_pair(Jstat,1)
          uNO = stat_pair(Jstat,2)
!
          if(coupNO(lNO).eq.kNO)then ! coupled to each other
!
            do Pvac = 1, Nvac_act
              pNO = vac_act(Pvac)
              do Qvac = 1, Nvac_act
                qNO = vac_act(Qvac)
                if(pNO.eq.qNO)cycle
!
                d2DDpDD(Icoup,pNO,qNO,Istat,Jstat) = &
                                                   Kronecker_delta(aNO,kNO)*Kronecker_delta(qNO,tNO)&
                                                  *Kronecker_delta(bNO,lNO)*Kronecker_delta(pNO,uNO)&
                                                 + Kronecker_delta(bNO,kNO)*Kronecker_delta(pNO,tNO)&
                                                  *Kronecker_delta(aNO,lNO)*Kronecker_delta(qNO,uNO)&
                                                 + Kronecker_delta(aNO,kNO)*Kronecker_delta(pNO,tNO)&
                                                  *Kronecker_delta(bNO,lNO)*Kronecker_delta(qNO,uNO)&
                                                 + Kronecker_delta(bNO,kNO)*Kronecker_delta(qNO,tNO)&
                                                  *Kronecker_delta(aNO,lNO)*Kronecker_delta(pNO,uNO)
!
              end do ! Qvac
            end do ! Pvac
!
          end if ! l and k coupled
!
        end do  ! Jstat
      end do  ! Istat
!
! xi specific intermediates
      allocate(xip1(Nbasis,Nbasis,Nstat),xip2(Nbasis,Nbasis,Nbasis,Nstat))
      xip1 = 0.0D0
      xip2 = 0.0D0
!
      do Istat = 1, Nstat
        kNO = stat_pair(Istat,1)
        tNO = stat_pair(Istat,2)
!
        if(coupNO(kNO).eq.0)then ! k not coupled
!
          do Qvac = 1, Nstat_occ(kNO)
            qNO = occ_stat(kNO,Qvac)
!
            xip1(kNO,qNO,Istat) = 0.5D0*(-1.0D0/ON(kNO) + Kronecker_delta(qNO,tNO)/Delta(kNO,qNO))
!       
          end do ! Qvac
!
        else ! k is coupled  
!
          bNO = coupNO(kNO)
          Icoup = NO2cp(kNO)
!
          do Pvac = 1, Nstat_occ(kNO)
            pNO = occ_stat(kNO,Pvac)
!
! first factorizable term
!
            xip1(kNO,pNO,Istat) = 0.5D0*(dnneta(kNO,bNO,Istat)/nneta(kNO,bNO)&
                                + Kronecker_delta(pNO,tNO)/Delta(kNO,pNO))
!
          end do ! Pvac
!
          do Pvac = 1, Nstat_occ(bNO)
            pNO = occ_stat(bNO,Pvac)
!
! first factorizable term
!
            xip1(bNO,pNO,Istat) = 0.5D0*(dnneta(bNO,kNO,Istat)/nneta(bNO,kNO)&
                                + (-1.0D0 + Kronecker_delta(pNO,tNO))/(ON(kNO) + Delta(kNO,pNO)))
!
          end do ! Pvac
!
          do Pvac = 1, Nvac_act
            pNO = vac_act(Pvac)
            do Qvac = 1, Nstat_occ(bNO)
              qNO = occ_stat(bNO,Qvac)
              if(pNO.eq.qNO)cycle 
!
! second factorizable term
!
              xip2(kNO,pNO,qNO,Istat) = 0.5D0*(dDDpDD(Icoup,pNO,qNO,Istat)/DDpDD(Icoup,pNO,qNO)&
                                      + (-1.0D0 + Kronecker_delta(qNO,tNO))&
                                       /(ON(kNO) + Delta(kNO,qNO)))
!
            end do ! Qvac
          end do ! Pvac
!
          do Pvac = 1, Nvac_act
            pNO = vac_act(Pvac)
            do Qvac = 1, Nstat_occ(kNO)
              qNO = occ_stat(kNO,Qvac)
              if(pNO.eq.qNO)cycle 
!
! second factorizable term
!
              xip2(bNO,pNO,qNO,Istat) = 0.5D0*(dDDpDD(Icoup,pNO,qNO,Istat)/DDpDD(Icoup,pNO,qNO)&
                                      + Kronecker_delta(qNO,tNO)/Delta(kNO,qNO))
!
            end do ! Qvac
          end do ! Pvac
!
        end if ! k coupled or not
!
      end do ! Istat
!
      if(.not.allocated(d2xi))then
        allocate(d2xi(Nbasis,Nbasis,Nstat,Nstat))
      end if 
      d2xi = zero
!
      do Istat = 1, Nstat
        kNO = stat_pair(Istat,1)
        tNO = stat_pair(Istat,2)
!
        do Jstat = 1, Nstat
          lNO = stat_pair(Jstat,1)
          uNO = stat_pair(Jstat,2)
!
          if((coupNO(kNO).eq.0).and.(coupNO(lNO).eq.0))then ! k and l not coupled
!
            if(kNO.eq.lNO)then
! xi_pq
              do Qvac = 1, Nstat_occ(kNO)
                qNO = occ_stat(kNO,Qvac)
!
                d2xi(kNO,qNO,Istat,Jstat) = (0.5D0*(-1.0D0/ON(kNO)**2&
                                            - Kronecker_delta(qNO,tNO)*Kronecker_delta(tNO,uNO)&
                                             /Delta(kNO,qNO)**2)&
                                          + xip1(kNO,qNO,Istat)*xip1(kNO,qNO,Jstat))*xi(kNO,qNO)
!
              end do ! Qvac
!
            end if ! k eq l
!
          else if(NO2cp(kNO).eq.NO2cp(lNO))then ! k and l coupled to each other or equal
!
            ! Let k = a
            Icoup = NO2cp(kNO)
            bNO = coupNO(kNO)
!
            do Pvac = 1, Nstat_occ(kNO)
              pNO = occ_stat(kNO,Pvac)
!
! xi_ap
!
              d2xi(kNO,pNO,Istat,Jstat) = d2xi(kNO,pNO,Istat,Jstat)&
                                      + (0.5D0*(d2nneta(kNO,bNO,Istat,Jstat)/nneta(kNO,bNO)&
                                      - dnneta(kNO,bNO,Istat)*dnneta(kNO,bNO,Jstat)/nneta(kNO,bNO)**2&
                                      - Kronecker_delta(kNO,lNO)&
                                   *Kronecker_delta(pNO,tNO)*Kronecker_delta(pNO,uNO)/Delta(kNO,pNO)**2)&
                                  + xip1(kNO,pNO,Istat)*xip1(kNO,pNO,Jstat))*xsq1(kNO,pNO)
!
            end do ! Pvac
!
            do Pvac = 1, Nstat_occ(bNO)
              pNO = occ_stat(bNO,Pvac)
!
! xi_bp
!
              d2xi(bNO,pNO,Istat,Jstat) = d2xi(bNO,pNO,Istat,Jstat)&
                                      + (0.5D0*(d2nneta(bNO,kNO,Istat,Jstat)/nneta(bNO,kNO)&
                                      - dnneta(bNO,kNO,Istat)*dnneta(bNO,kNO,Jstat)/nneta(bNO,kNO)**2&
                                      - Kronecker_delta(kNO,lNO)& 
                                *(-1.0D0 + Kronecker_delta(pNO,tNO))*(-1.0D0 + Kronecker_delta(pNO,uNO))&
                                /(ON(kNO) + Delta(kNO,pNO))**2)&
                                  + xip1(bNO,pNO,Istat)*xip1(bNO,pNO,Jstat))*xsq1(bNO,pNO)
!
            end do ! Pvac
!
            do Pvac = 1, Nvac_act
              pNO = vac_act(Pvac)
              do Qvac = 1, Nstat_occ(bNO)
                qNO = occ_stat(bNO,Qvac)
                if(qNO.eq.pNO)cycle
!
! xi_ap
                d2xi(kNO,pNO,Istat,Jstat) = d2xi(kNO,pNO,Istat,Jstat)&
                + (0.5D0*(d2DDpDD(Icoup,pNO,qNO,Istat,Jstat)/DDpDD(Icoup,pNO,qNO)&
                  - dDDpDD(Icoup,pNO,qNO,Istat)*dDDpDD(Icoup,pNO,qNO,Jstat)/DDpDD(Icoup,pNO,qNO)**2&
                  - Kronecker_delta(kNO,lNO)&
                   *(-1.0D0 + Kronecker_delta(qNO,tNO))*(-1.0D0 + Kronecker_delta(qNO,uNO))&
                   /(ON(kNO) + Delta(kNO,qNO))**2)&
                  + xip2(kNO,pNO,qNO,Istat)*xip2(kNO,pNO,qNO,Jstat))*xsq2(kNO,pNO,qNO)
!
              end do ! Qvac
            end do ! Pvac
!
            do Pvac = 1, Nvac_act
              pNO = vac_act(Pvac)
              do Qvac = 1, Nstat_occ(kNO)
                qNO = occ_stat(kNO,Qvac)
                if(qNO.eq.pNO)cycle
!
! xi_bp
                d2xi(bNO,pNO,Istat,Jstat) = d2xi(bNO,pNO,Istat,Jstat)&
                + (0.5D0*(d2DDpDD(Icoup,pNO,qNO,Istat,Jstat)/DDpDD(Icoup,pNO,qNO)&
                  - dDDpDD(Icoup,pNO,qNO,Istat)*dDDpDD(Icoup,pNO,qNO,Jstat)/DDpDD(Icoup,pNO,qNO)**2&
                  - Kronecker_delta(kNO,lNO)&
                   *Kronecker_delta(qNO,tNO)*Kronecker_delta(qNO,uNO)/Delta(kNO,qNO)**2)&
                  + xip2(bNO,pNO,qNO,Istat)*xip2(bNO,pNO,qNO,Jstat))*xsq2(bNO,pNO,qNO)
!
              end do ! Qvac
            end do ! Pvac
!
          end if ! k and l coupled or not
!
        end do ! Jstat
      end do ! Istat
!
! Add contribution of xi terms to hessian
!
      do Istat = 1, Nstat
        do Jstat = 1, Nstat
          do Pocc = 1, Nocc_act
            pNO = occ_act(Pocc)
            do Qvac = 1, Nvac_act
              qNO = vac_act(Qvac)
!
              hessian_Delta(Istat,Jstat) = hessian_Delta(Istat,Jstat)&
                                         - 2.0D0*d2xi(pNO,qNO,Istat,Jstat)*K_NO(pNO,qNO)
!
! symmetrize for use in TDF
              d2xi(qNO,pNO,Istat,Jstat) = d2xi(pNO,qNO,Istat,Jstat) 
!
            end do ! Qvac
          end do ! Pocc
!
        end do ! Jstat
      end do ! Istat
!
      deallocate(xsq1,xsq2,xip1,xip2,nneta,dnneta,d2nneta)
!
! zeta specific intermediates
      allocate(zsq1(Nbasis,Nbasis,Nbasis,Nbasis),zsq2(Ncoup,Nbasis,Nbasis,Nbasis),zsq3(Ncoup,Nbasis))
      zsq1 = 0.0D0
      zsq2 = 0.0D0
      zsq3 = 0.0D0
!
      do Icoup = 1, Ncoup
        aNO = coup_pair(Icoup,1)
        bNO = coup_pair(Icoup,2)
!
        do Pvac = 1, Nvac_act
          pNO = vac_act(Pvac)
          do Qvac = 1, Nvac_act
            qNO = vac_act(Qvac)
            if(qNO.eq.pNO)cycle
!
            zsq1(aNO,bNO,pNO,qNO) = dsqrt(Delta(aNO,pNO)*Delta(aNO,qNO)&
                                  *(ON(bNO) + Delta(bNO,pNO))*(ON(bNO) + Delta(bNO,qNO)))
!
            zsq1(bNO,aNO,pNO,qNO) = dsqrt(Delta(bNO,pNO)*Delta(bNO,qNO)&
                                  *(ON(aNO) + Delta(aNO,pNO))*(ON(aNO) + Delta(aNO,qNO)))
!
          end do ! Qvac 
        end do ! Pvac
!
        do Pvac = 1, Nvac_act
          pNO = vac_act(Pvac)
          do Qvac = 1, Nvac_act
            qNO = vac_act(Qvac)
            if(qNO.eq.pNO)cycle
            do Rvac = 1, Nvac_act
              rNO = vac_act(Rvac)
              if(rNO.eq.pNO)cycle
              if(rNO.eq.qNO)cycle
!
              zsq2(Icoup,pNO,qNO,rNO) = dsqrt(DDpDD(Icoup,pNO,rNO)*DDpDD(Icoup,qNO,rNO))
!
            end do ! Rvac 
          end do ! Qvac 
        end do ! Pvac 
!
        do Pvac = 1, Nvac_act
          pNO = vac_act(Pvac)
!
          zsq3(Icoup,pNO) = dsqrt(Delta(aNO,pNO)*Delta(bNO,pNO)&
                                *(ON(aNO) + Delta(aNO,pNO))*(ON(bNO) + Delta(bNO,pNO)))
!
        end do ! Pvac
!
      end do ! Icoup
!
      allocate(zetap1(Nbasis,Nbasis,Nbasis,Nstat),zetap2(Ncoup,Nbasis,Nbasis,Nbasis,Nstat))
      allocate(zetap3(Ncoup,Nbasis,Nstat))
      zetap1 = 0.0D0
      zetap2 = 0.0D0
      zetap3 = 0.0D0
!
      do Istat = 1, Nstat
        kNO = stat_pair(Istat,1)
        tNO = stat_pair(Istat,2)
!
        if(coupNO(kNO).eq.0)then ! k not coupled
!
          do Pvac = 1, Nstat_occ(kNO)
            pNO = occ_stat(kNO,Pvac)
            do Qvac = 1, Nstat_occ(kNO)
              qNO = occ_stat(kNO,Qvac)
              if(qNO.eq.pNO)cycle 
!
              zetap1(kNO,pNO,qNO,Istat) = 0.5D0*(Kronecker_delta(pNO,tNO)/Delta(kNO,pNO)&
                                               + Kronecker_delta(qNO,tNO)/Delta(kNO,qNO))
!       
            end do ! Qvac
          end do ! Pvac
!
        else ! k is coupled  
!
          bNO = coupNO(kNO)
          Icoup = NO2cp(kNO)
!
          do Pvac = 1, Nstat_occ(kNO)
            pNO = occ_stat(kNO,Pvac)
            do Qvac = 1, Nstat_occ(kNO)
              qNO = occ_stat(kNO,Qvac)
              if(qNO.eq.pNO)cycle 
!
              zetap1(kNO,pNO,qNO,Istat) = 0.5D0*(Kronecker_delta(pNO,tNO)/Delta(kNO,pNO)&
                                               + Kronecker_delta(qNO,tNO)/Delta(kNO,qNO))
!
            end do ! Qvac
          end do ! Pvac
!
          do Pvac = 1, Nstat_occ(bNO)
            pNO = occ_stat(bNO,Pvac)
            do Qvac = 1, Nstat_occ(bNO)
              qNO = occ_stat(bNO,Qvac)
              if(qNO.eq.pNO)cycle 
!
              zetap1(bNO,pNO,qNO,Istat) = 0.5D0*((-1.0D0 + Kronecker_delta(pNO,tNO))&
                                                /(ON(kNO) + Delta(kNO,pNO))&
                                              + (-1.0D0 + Kronecker_delta(qNO,tNO))&
                                                /(ON(kNO) + Delta(kNO,qNO)))
!
            end do ! Qvac
          end do ! Pvac
!
          do Pvac = 1, Nvac_act
            pNO = vac_act(Pvac)
            do Qvac = 1, Nvac_act
              qNO = vac_act(Qvac)
              if(qNO.eq.pNO)cycle
              do Rvac = 1, Nvac_act
                rNO = vac_act(Rvac)
                if(rNO.eq.pNO)cycle
                if(rNO.eq.qNO)cycle
! 
               zetap2(Icoup,pNO,qNO,rNO,Istat) = 0.5D0*(dDDpDD(Icoup,pNO,rNO,Istat)/DDpDD(Icoup,pNO,rNO)&
                                               + dDDpDD(Icoup,qNO,rNO,Istat)/DDpDD(Icoup,qNO,rNO))
!
              end do ! Rvac
            end do ! Qvac
          end do ! Pvac

          do Pvac = 1, Nstat_occ(kNO)
            pNO = occ_stat(kNO,Pvac)
!
            zetap3(Icoup,pNO,Istat) = 0.5D0*(Kronecker_delta(pNO,tNO)/Delta(kNO,pNO)&
                                    + (-1.0D0 + Kronecker_delta(pNO,tNO))/(ON(kNO) + Delta(kNO,pNO)))
!
          end do ! Pvac
!          
        end if ! k coupled or not
!        
      end do ! Istat
!
      if(.not.allocated(d2zeta))then
        allocate(d2zeta(Nbasis,Nbasis,Nstat,Nstat))
      end if
      d2zeta = zero
!
! zeta_pq
!
      do Istat = 1, Nstat
        kNO = stat_pair(Istat,1)
        tNO = stat_pair(Istat,2)
!
        do Jstat = 1, Nstat
          lNO = stat_pair(Jstat,1)
          uNO = stat_pair(Jstat,2)
!
          if((coupNO(kNO).eq.0).and.coupNO(lNO).eq.0)then ! k and l not coupled
!
            if(kNO.eq.lNO)then
!
              do Pvac = 1, Nstat_occ(kNO)
                pNO = occ_stat(kNO,Pvac)
                do Qvac = 1, Nstat_occ(kNO)
                  qNO = occ_stat(kNO,Qvac)
                  if(qNO.eq.pNO)cycle
!
              d2zeta(pNO,qNO,Istat,Jstat) = (0.5D0*(-Kronecker_delta(pNO,tNO)*Kronecker_delta(pNO,uNO)&
                                                   /Delta(kNO,pNO)**2& 
                                                   - Kronecker_delta(qNO,tNO)*Kronecker_delta(qNO,uNO)&
                                                   /Delta(kNO,qNO)**2)&
                                            + zetap1(kNO,pNO,qNO,Istat)*zetap1(kNO,pNO,qNO,Jstat))&
                                            *dsqrt(Delta(kNO,pNO)*Delta(kNO,qNO))
              d2zeta(pNO,qNO,Istat,Jstat) = sigma(kNO,pNO)*sigma(kNO,qNO)*d2zeta(pNO,qNO,Istat,Jstat)
!
                end do ! Qvac
              end do ! Pvac
!
            end if 
!
          else if(NO2cp(kNO).eq.NO2cp(lNO))then ! k and l coupled to each other or equal
!
            ! Let k = a
            Icoup = NO2cp(kNO)
            bNO = coupNO(kNO)
!
            do Pvac = 1, Nstat_occ(kNO)
              pNO = occ_stat(kNO,Pvac)
              do Qvac = 1, Nstat_occ(kNO)
                qNO = occ_stat(kNO,Qvac)
                if(qNO.eq.pNO)cycle
!
                d2zeta(pNO,qNO,Istat,Jstat) = d2zeta(pNO,qNO,Istat,Jstat)&
                + (0.5D0*Kronecker_delta(kNO,lNO)*(-Kronecker_delta(pNO,tNO)*Kronecker_delta(pNO,uNO)&
                                                     /Delta(kNO,pNO)**2&
                                                    - Kronecker_delta(qNO,tNO)*Kronecker_delta(qNO,uNO)&
                                                     /Delta(kNO,qNO)**2)& 
                                             + zetap1(kNO,pNO,qNO,Istat)*zetap1(kNO,pNO,qNO,Jstat))&
                                             *zsq1(kNO,bNO,pNO,qNO)
!
              end do ! Qvac
            end do ! Pvac
!
            do Pvac = 1, Nstat_occ(bNO)
              pNO = occ_stat(bNO,Pvac)
              do Qvac = 1, Nstat_occ(bNO)
                qNO = occ_stat(bNO,Qvac)
                if(qNO.eq.pNO)cycle
!
                d2zeta(pNO,qNO,Istat,Jstat) = d2zeta(pNO,qNO,Istat,Jstat)&
                + (0.5D0*Kronecker_delta(kNO,lNO)*(-(-1.0D0 + Kronecker_delta(pNO,tNO))&
                                                      *(-1.0D0 + Kronecker_delta(pNO,uNO))&
                                                     /(ON(kNO) + Delta(kNO,pNO))**2&
                                                      -(-1.0D0 + Kronecker_delta(qNO,tNO))&
                                                      *(-1.0D0 + Kronecker_delta(qNO,uNO))&
                                                     /(ON(kNO) + Delta(kNO,qNO))**2)&
                                             + zetap1(bNO,pNO,qNO,Istat)*zetap1(bNO,pNO,qNO,Jstat))&
                                             *zsq1(bNO,kNO,pNO,qNO)
!
              end do ! Qvac
            end do ! Pvac
!
            do Pvac = 1, Nvac_act
              pNO = vac_act(Pvac)
              do Qvac = 1, Nvac_act
                qNO = vac_act(Qvac)
                if(qNO.eq.pNO)cycle
                do Rvac = 1, Nvac_act
                  rNO = vac_act(Rvac)
                  if(rNO.eq.pNO)cycle
                  if(rNO.eq.qNO)cycle
!
                  d2zeta(pNO,qNO,Istat,Jstat) = d2zeta(pNO,qNO,Istat,Jstat)&
                  + (0.5D0*(d2DDpDD(Icoup,pNO,rNO,Istat,Jstat)/DDpDD(Icoup,pNO,rNO)&
                  - dDDpDD(Icoup,pNO,rNO,Istat)*dDDpDD(Icoup,pNO,rNO,Jstat)/DDpDD(Icoup,pNO,rNO)**2&
                          + d2DDpDD(Icoup,qNO,rNO,Istat,Jstat)/DDpDD(Icoup,qNO,rNO)&
                  - dDDpDD(Icoup,qNO,rNO,Istat)*dDDpDD(Icoup,qNO,rNO,Jstat)/DDpDD(Icoup,qNO,rNO)**2)&
                  + zetap2(Icoup,pNO,qNO,rNO,Istat)*zetap2(Icoup,pNO,qNO,rNO,Jstat))&
                  *zsq2(Icoup,pNO,qNO,rNO)
!
                end do ! Rvac
              end do ! Qvac
            end do ! Pvac
!
! zeta_ab
            do Pvac = 1, Nvacshr(Icoup)
              pNO = vacshr(Icoup,Pvac)
!
              d2zeta(kNO,bNO,Istat,Jstat) = d2zeta(kNO,bNO,Istat,Jstat)&
                                          + (0.5D0*Kronecker_delta(kNO,lNO)&
                                                  *(-Kronecker_delta(pNO,tNO)*Kronecker_delta(pNO,uNO)&
                                                    /Delta(kNO,pNO)**2&
                                                    - (-1.0D0 + Kronecker_delta(pNO,tNO))&
                                                     *(-1.0D0 + Kronecker_delta(pNO,uNO))&
                                                    /(ON(kNO) + Delta(kNO,pNO))**2)&
                                          + zetap3(Icoup,pNO,Istat)*zetap3(Icoup,pNO,Jstat))&
                                          *zsq3(Icoup,pNO)
!
            end do ! Pvac
!
            d2zeta(bNO,kNO,Istat,Jstat) = d2zeta(kNO,bNO,Istat,Jstat)
!
          end if ! k and l coupled
!
        end do ! Jstat
      end do ! Istat
!
! Add zeta contribution to hessian
!
      do Istat = 1, Nstat 
        do Jstat = 1, Nstat 
!
          do Pvac = 1, Nvac_act
            pNO = vac_act(Pvac)
            do Qvac = 1, Nvac_act
              qNO = vac_act(Qvac)
!
              hessian_Delta(Istat,Jstat) = hessian_Delta(Istat,Jstat)&
                                         + d2zeta(pNO,qNO,Istat,Jstat)*K_NO(pNO,qNO)
!
! symmetrize for use in TDF
              d2zeta(qNO,pNO,Istat,Jstat) = d2zeta(pNO,qNO,Istat,Jstat) 
!
            end do ! Qvac
          end do ! Pvac
!
          do Icoup = 1, Ncoup
            aNO = coup_pair(Icoup,1)
            bNO = coup_pair(Icoup,2)
!
            hessian_Delta(Istat,Jstat) = hessian_Delta(Istat,Jstat)&
                                       + 2.0D0*d2zeta(aNO,bNO,Istat,Jstat)*K_NO(aNO,bNO)
!
          end do ! Icoup
!
        end do ! Jstat 
      end do ! Jstat 
!
! high-spin correction
!
      if(.not.allocated(d2kappa))then
        allocate(d2kappa(Nbasis,Nbasis,Nstat,Nstat))
      end if
      d2kappa = zero
!
! kappa
!
      do Istat = 1, Nstat
        kNO = stat_pair(Istat,1)
        tNO = stat_pair(Istat,2)
        do Jstat = 1, Nstat
          lNO = stat_pair(Jstat,1)
          uNO = stat_pair(Jstat,2)
!
          do Kstat = 1, Nstat
            pNO = stat_pair(Kstat,1)
            rNO = stat_pair(Kstat,2)
!
            do Iopen = 1, Nopen ! open-shell
              aNO = NO_open(Iopen)
!
              d2kap = 0.353553D0*ON(aNO)*d2xi(pNO,rNO,Istat,Jstat)
              !d2kap = 0.0D0
!
              d2kappa(aNO,pNO,Istat,Jstat) = d2kap
              d2kappa(pNO,aNO,Istat,Jstat) = d2kap
              d2kappa(aNO,rNO,Istat,Jstat) = d2kap
              d2kappa(rNO,aNO,Istat,Jstat) = d2kap
!
            end do ! Iopen
          end do ! Kstat
!
          do Kstat = 2, Nstat
            pNO = stat_pair(Kstat,1)
            rNO = stat_pair(Kstat,2)
            do Lstat = 1, Kstat-1
              qNO = stat_pair(Lstat,1)
              sNO = stat_pair(Lstat,2)
              if(qNO.eq.pNO)cycle
              if(rNO.eq.sNO)cycle
!
! xi*xi - good for square H4, violates PQG
! D*D   - satisfies PQ, violates G
!
              d2kap = d2xi(pNO,rNO,Istat,Jstat)*xi(qNO,sNO)&
                    + dxi(pNO,rNO,Istat)*dxi(qNO,sNO,Jstat)&
                    + dxi(pNO,rNO,Jstat)*dxi(qNO,sNO,Istat)&
                    + xi(pNO,rNO)*d2xi(qNO,sNO,Istat,Jstat)
              !kap    = 4.0D0*Delta(pNO,rNO)*Delta(qNO,sNO)
              !dkapkt = 4.0D0*(Kronecker_delta(pNO,kNO)*Kronecker_delta(rNO,tNO)*Delta(qNO,sNO)&
              !              + Kronecker_delta(qNO,kNO)*Kronecker_delta(sNO,tNO)*Delta(pNO,rNO))
              !dkaplu = 4.0D0*(Kronecker_delta(pNO,lNO)*Kronecker_delta(rNO,uNO)*Delta(qNO,sNO)&
              !              + Kronecker_delta(qNO,lNO)*Kronecker_delta(sNO,uNO)*Delta(pNO,rNO))
              !d2kap  = Kronecker_delta(pNO,kNO)*Kronecker_delta(rNO,tNO)&
              !        *Kronecker_delta(qNO,lNO)*Kronecker_delta(sNO,uNO)&
              !       + Kronecker_delta(qNO,kNO)*Kronecker_delta(sNO,tNO)&
              !        *Kronecker_delta(pNO,lNO)*Kronecker_delta(rNO,uNO) 
              !d2kap = 4.0D0*d2kap
!
              d2kappa(pNO,qNO,Istat,Jstat) = d2kappa(pNO,qNO,Istat,Jstat) + d2kap
              !dnnkt = (Kronecker_delta(pNO,tNO) - Kronecker_delta(pNO,kNO))*on(qNO)&
              !      + (Kronecker_delta(qNO,tNO) - Kronecker_delta(qNO,kNO))*on(pNO)
              !dnnlu = (Kronecker_delta(pNO,uNO) - Kronecker_delta(pNO,lNO))*on(qNO)&
              !      + (Kronecker_delta(qNO,uNO) - Kronecker_delta(qNO,lNO))*on(pNO)
              !d2nn  = (Kronecker_delta(pNO,tNO) - Kronecker_delta(pNO,kNO))&
              !       *(Kronecker_delta(qNO,uNO) - Kronecker_delta(qNO,lNO))& 
              !      + (Kronecker_delta(qNO,tNO) - Kronecker_delta(qNO,kNO))&
              !       *(Kronecker_delta(pNO,uNO) - Kronecker_delta(pNO,lNO))
              !d2kappa(pNO,qNO,Istat,Jstat) = d2kappa(pNO,qNO,Istat,Jstat)&
              !                             + d2nn*kap + on(pNO)*on(qNO)*d2kap&
              !                             + dnnkt*dkaplu + dnnlu*dkapkt
              d2kappa(qNO,pNO,Istat,Jstat) = d2kappa(pNO,qNO,Istat,Jstat)
!
              d2kappa(pNO,sNO,Istat,Jstat) = d2kappa(pNO,sNO,Istat,Jstat) + d2kap
              !dnnkt = (Kronecker_delta(pNO,tNO) - Kronecker_delta(pNO,kNO))*on(sNO)&
              !      + (Kronecker_delta(sNO,tNO) - Kronecker_delta(sNO,kNO))*on(pNO)
              !dnnlu = (Kronecker_delta(pNO,uNO) - Kronecker_delta(pNO,lNO))*on(sNO)&
              !      + (Kronecker_delta(sNO,uNO) - Kronecker_delta(sNO,lNO))*on(pNO)
              !d2nn  = (Kronecker_delta(pNO,tNO) - Kronecker_delta(pNO,kNO))&
              !       *(Kronecker_delta(sNO,uNO) - Kronecker_delta(sNO,lNO))& 
              !      + (Kronecker_delta(sNO,tNO) - Kronecker_delta(sNO,kNO))&
              !       *(Kronecker_delta(pNO,uNO) - Kronecker_delta(pNO,lNO))
              !d2kappa(pNO,sNO,Istat,Jstat) = d2kappa(pNO,sNO,Istat,Jstat)&
              !                             + d2nn*kap + on(pNO)*on(sNO)*d2kap&
              !                             + dnnkt*dkaplu + dnnlu*dkapkt
              d2kappa(sNO,pNO,Istat,Jstat) = d2kappa(pNO,sNO,Istat,Jstat)
!
              d2kappa(rNO,qNO,Istat,Jstat) = d2kappa(rNO,qNO,Istat,Jstat) + d2kap
              !dnnkt = (Kronecker_delta(rNO,tNO) - Kronecker_delta(rNO,kNO))*on(qNO)&
              !      + (Kronecker_delta(qNO,tNO) - Kronecker_delta(qNO,kNO))*on(rNO)
              !dnnlu = (Kronecker_delta(rNO,uNO) - Kronecker_delta(rNO,lNO))*on(qNO)&
              !      + (Kronecker_delta(qNO,uNO) - Kronecker_delta(qNO,lNO))*on(rNO)
              !d2nn  = (Kronecker_delta(rNO,tNO) - Kronecker_delta(rNO,kNO))&
              !       *(Kronecker_delta(qNO,uNO) - Kronecker_delta(qNO,lNO))& 
              !      + (Kronecker_delta(qNO,tNO) - Kronecker_delta(qNO,kNO))&
              !       *(Kronecker_delta(rNO,uNO) - Kronecker_delta(rNO,lNO))
              !d2kappa(rNO,qNO,Istat,Jstat) = d2kappa(rNO,qNO,Istat,Jstat)&
              !                             + d2nn*kap + on(rNO)*on(qNO)*d2kap&
              !                             + dnnkt*dkaplu + dnnlu*dkapkt
              d2kappa(qNO,rNO,Istat,Jstat) = d2kappa(rNO,qNO,Istat,Jstat)
!
              d2kappa(rNO,sNO,Istat,Jstat) = d2kappa(rNO,sNO,Istat,Jstat) + d2kap
              !dnnkt = (Kronecker_delta(rNO,tNO) - Kronecker_delta(rNO,kNO))*on(sNO)&
              !      + (Kronecker_delta(sNO,tNO) - Kronecker_delta(sNO,kNO))*on(rNO)
              !dnnlu = (Kronecker_delta(rNO,uNO) - Kronecker_delta(rNO,lNO))*on(sNO)&
              !      + (Kronecker_delta(sNO,uNO) - Kronecker_delta(sNO,lNO))*on(rNO)
              !d2nn  = (Kronecker_delta(rNO,tNO) - Kronecker_delta(rNO,kNO))&
              !       *(Kronecker_delta(sNO,uNO) - Kronecker_delta(sNO,lNO))& 
              !      + (Kronecker_delta(sNO,tNO) - Kronecker_delta(sNO,kNO))&
              !       *(Kronecker_delta(rNO,uNO) - Kronecker_delta(rNO,lNO))
              !d2kappa(rNO,sNO,Istat,Jstat) = d2kappa(rNO,sNO,Istat,Jstat)&
              !                             + d2nn*kap + on(rNO)*on(sNO)*d2kap&
              !                             + dnnkt*dkaplu + dnnlu*dkapkt
              d2kappa(sNO,rNO,Istat,Jstat) = d2kappa(rNO,sNO,Istat,Jstat)
!
            end do ! Lstat
          end do ! Kstat
!
        end do ! Jstat
      end do ! Istat
!
! add contribution to hessian
      if(LHSCon)then
!
      do Istat = 1, Nstat
        do Jstat = 1, Nstat
!
          do Pocc = 1, Nocc
            pNO = NO_occ(Pocc)
            do Qocc = 1, Nocc
              qNO = NO_occ(Qocc)
!          
              hessian_Delta(Istat,Jstat) = hessian_Delta(Istat,Jstat)&
                                         - d2kappa(pNO,qNO,Istat,Jstat)*K_NO(pNO,qNO)
!
            end do ! Qocc
          end do ! Pocc
!
        end do ! Jstat
      end do ! Istat
!
      end if ! HSC
!
!
! Add dynamic contribution
!
!      select case(dynamic)
! For nonvariational 
      select case('NONE')
!
        case('CS')
!
! Get 2nd derivative of CS wrt Delta
          call KILL_object ('CFT', 'd2EdyndD2', 'CS')      
          call GET_object ('CFT', 'd2EdyndD2', 'CS')
!
!          write(6,*)'CS contribution to hessian'
          do Istat = 1, Nstat
            do Jstat = 1, Nstat
              IJ_index = Nstat*(Istat - 1) + Jstat
              hessian_Delta(Istat,Jstat) =  hessian_Delta(Istat,Jstat) + d2ECSdD2(IJ_index)
!      write(6,'(I4,X,I4,X,F30.15)') Istat,Jstat, d2ECSdD2(IJ_index)*dsin(2.0D0*theta(Istat))*dsin(2.0D0*theta(Jstat))
            end do ! Jstat
          end do ! Istat
!
        case('OF')
!
! Get 2nd derivative of OSEC wrt Delta
          call KILL_object ('CFT', 'd2EdyndD2', 'OSEC')      
          call GET_object ('CFT', 'd2EdyndD2', 'OSEC')
!
! Get 2nd derivative of FHC wrt Delta
          call KILL_object ('CFT', 'd2EdyndD2', 'FHC')      
          call GET_object ('CFT', 'd2EdyndD2', 'FHC')
!
!          write(6,*)'OSEC contribution to hessian'
!          do Istat = 1, Nstat
!            do Jstat = 1, Nstat
!              IJ_index = Nstat*(Istat - 1) + Jstat
!      write(6,'(I4,X,I4,X,F30.15)') Istat,Jstat, d2EOSECdD2(IJ_index)
!            end do
!          end do
!
          do Istat = 1, Nstat
            do Jstat = 1, Nstat
              IJ_index = Nstat*(Istat - 1) + Jstat
              hessian_Delta(Istat,Jstat) =  hessian_Delta(Istat,Jstat) + d2EOSECdD2(IJ_index)&
                                                                       + d2EFHCdD2(IJ_index)
            end do ! Jstat
          end do ! Istat
!
        case default
! 
      end select
! 
! theta hessian
!
! Multiply by theta derivative factor
!
      do Istat = 1, Nstat
        iNO = stat_pair(Istat,1)
        rNO = stat_pair(Istat,2)
        do Jstat = 1, Nstat
          jNO = stat_pair(Jstat,1)
          sNO = stat_pair(Jstat,2)
!          
! here we are ignoring the dependence of cos^2 amplitude on other thetas
          !hessian(Istat,Jstat) = hessian_Delta(Istat,Jstat)&
          !                       *dsin(2.0D0*theta(Istat))*dsin(2.0D0*theta(Jstat))&
          !                       *min(ON(iNO) + Delta(iNO,rNO), 1.0D0 - ON(rNO) + Delta(iNO,rNO))&
          !                       *min(ON(jNO) + Delta(jNO,sNO), 1.0D0 - ON(sNO) + Delta(jNO,sNO))
          !hessian(Istat,Jstat) = hessian_Delta(Istat,Jstat)&
          !                       *dsin(2.0D0*theta(Istat))*dsin(2.0D0*theta(Jstat))
          hessian(Istat,Jstat) = hessian_Delta(Istat,Jstat)&
                                 *0.25D0*dsin(2.0D0*theta(Istat))*dsin(2.0D0*theta(Jstat))
!
        end do ! Jstat
      end do ! Istat
!
! Add gradient times second derivative wrt theta term to diagonal
!
      do Istat = 1, Nstat
        !hessian(Istat,Istat) = hessian(Istat,Istat) - 2.0D0*grad_Delta(Istat)*dcos(2.0D0*theta(Istat))&
        !                                *min(ON(iNO) + Delta(iNO,rNO), 1.0D0 - ON(rNO) + Delta(iNO,rNO))
        !hessian(Istat,Istat) = hessian(Istat,Istat) - 2.0D0*grad_Delta(Istat)*dcos(2.0D0*theta(Istat))
        hessian(Istat,Istat) = hessian(Istat,Istat) - grad_Delta(Istat)*dcos(2.0D0*theta(Istat))
      end do ! Istat
!
!      write(6,'(/a)')'                 Delta hessian                        '
!      write(6,'(a)')'-------------------------------------------------------'
!      write(6,'(a)')'       a         b                        h_ab         '
!      write(6,'(a)')'-------------------------------------------------------'
!      do Istat = 1, Nstat
!       do Jstat = 1, Nstat
!          !if(dabs(hessian_Delta(Istat,Jstat)-hessian_Delta(Jstat,Istat)).gt.1.0D-10)then
!           write(6,'(5x,I3,5x,I3,4x,F24.10)')Istat,Jstat,hessian_Delta(Istat,Jstat)
!          !end if
!        end do ! Jstat
!      end do ! Istat
!      write(6,'(/a)')'                 Delta hessian (numerical)            '
!      write(6,'(a)')'-------------------------------------------------------'
!      write(6,'(a)')'       a         b                        h_ab         '
!      write(6,'(a)')'-------------------------------------------------------'
!      Delta_step = 0.00000001D0
!      allocate(grad_Delta0(Nstat))
!      grad_Delta0 = grad_Delta
!     do Istat = 1, Nstat
!        do Jstat = 1, Nstat
!          iNO = stat_pair(Jstat,1)
!          rNO = stat_pair(Jstat,2)
!
!          Delta(iNO,rNO) = Delta(iNO,rNO) + Delta_step
!         call CFT_Delta_to_ON
!          call CFT_BLD_ezxk
!          call CFT_energy
!          call CFT_Delta_gradient
!          !if(dabs(hessian_Delta(Istat,Jstat)-hessian_Delta(Jstat,Istat)).gt.1.0D-10)then
!           write(6,'(5x,I3,5x,I3,4x,F24.10)')Istat,Jstat,(grad_Delta(Istat) - grad_Delta0(Istat))&
!                                                          /Delta_step 
!         !end if
!
!          Delta(iNO,rNO) = Delta(iNO,rNO) - Delta_step
!          call CFT_Delta_to_ON
!          call CFT_BLD_ezxk
!          call CFT_energy
!
!        end do ! Jstat
!      end do ! Istat
!      call flush(6)
!
!      write(6,'(/a)')'                 theta hessian                        '
!      write(6,'(a)')'-------------------------------------------------------'
!      write(6,'(a)')'       a         b                        h_ab         '
!      write(6,'(a)')'-------------------------------------------------------'
!      do Istat = 1, Nstat
!        do Jstat = 1, Istat
!          if(dabs(hessian(Istat,Jstat)-hessian(Jstat,Istat)).gt.1.0D-10)then
!            write(6,'(5x,I3,5x,I3,4x,F24.10)')Istat,Jstat,hessian(Istat,Jstat)
!          end if
!        end do ! Jstat
!      end do ! Istat
!
! Diagonalize
!      call dsyev('V', 'U', Nstat, QNO, Nstat, eigval, work, 3*Nstat-1, kNO)
!
!      write(UNIout,'(/a)')'      theta hessian eigenvalues        '
!      write(UNIout,'(a)')'----------------------------------------'
!      write(UNIout,'(a)')'       a                    h_a         '
!      write(UNIout,'(a)')'----------------------------------------'
!      do cNO = 1, Nbasis
!        write(6,'(5x,I3,4x,F24.10)')cNO,eigval(cNO)
!      end do ! cNO
!
! End of routine CFT_Delta_hessian
      call PRG_manager('exit','CFT_Delta_hessian','UTILITY')
      end subroutine CFT_Delta_hessian
      subroutine CFT_BLD_FockNO
!***********************************************************************
!     Date last modified: April 4, 2016                                *
!     Author: JWH                                                      *
!     Description: Construct the CFT Fock matrix analogue for          *
!                  determining the NOs.                                *
!***********************************************************************
! Modules:
      USE QM_objects
      USE CFT_objects
      USE type_molecule

      implicit none
!
! Local scalars:
      integer :: cNO, dNO
!
! Begin:
!
      call PRG_manager('enter','CFT_BLD_FockNO','UTILITY')
!
      if(allocated(FockNO))then
        deallocate(FockNO)
      end if
      allocate(FockNO(Nbasis,Nbasis))
!
      FockNO = 0.0D0
      do cNO = 2, Nbasis
        do dNO = 1, cNO-1
          FockNO(cNO,dNO) = lambda(dNO,cNO) - lambda(cNO,dNO)
          FockNO(dNO,cNO) = FockNO(cNO,dNO)
        end do ! dNO
      end do ! cNO
!
! End of routine CFT_BLD_FockNO
      call PRG_manager('exit','CFT_BLD_FockNO','UTILITY')
      end subroutine CFT_BLD_FockNO
      subroutine CFT_BLD_lambda
!***********************************************************************
!     Date last modified: April 4, 2019                                *
!     Author: JWH                                                      *
!     Description: Construct the CFT lambda matrix as defined by       *
!                  Piris and Ugalde.                                   *
!***********************************************************************
! Modules:
      USE CFT_objects
      USE QM_objects
      USE INT_objects
      USE type_molecule
      USE program_constants
      USE program_files

      implicit none
!
! Local scalars:
      integer :: cNO, dNO, iNO, Cact, Iclosed, Iopen
      integer :: Iact, Cocc, Iocc
      integer :: NO_all(Nbasis), aNO, Nall
      integer :: cidi_index, ciid_index
!
! Local functions: 
      integer :: TEIindex
!
! Begin:
!
      call PRG_manager('enter','CFT_BLD_lambda','UTILITY')
!
      if(allocated(lambda))then
        deallocate(lambda)
      end if
      allocate(lambda(Nbasis,Nbasis))
!
      lambda = zero
!
! Create list for all orbitals
      do aNO = 1, Nbasis
        NO_all(aNO) = aNO
      end do
      Nall = Nbasis
!
! Create list for all occupied (including partially) orbitals
      call CFT_BLD_occvac_list
!
! Get the necessary integrals
      ! Transform to NO basis
      call CFT_gen_TEItrans(NO_occ,Nocc,NO_all,Nall,NO_all,Nall,NO_all,Nall,&
                            NO,Nbasis,eriAO,eriNO)
!
! Need G for Fock matrix parts
      call CFT_BLD_Gmatrix
! Transform to NO basis
      call CFT_2D_transform(Gcl_AO, NO, Gcl_NO, Nbasis, Nbasis)
      call CFT_2D_transform(Gop_AO, NO, Gop_NO, Nbasis, Nbasis)
      call CFT_2D_transform(Goc_AO, NO, Goc_NO, Nbasis, Nbasis)
!
! and h
      if(allocated(h_NO))then
        deallocate(h_NO)
      end if
      allocate(h_NO(Nbasis,Nbasis))
      call I1E_transform (Hcore, NO, h_NO, Nbasis, Nbasis, MATlen)
!
! HF-like part
      do Iclosed = 1, Nclosed ! closed-shell
        cNO = NO_closed(Iclosed)
        do dNO = 1, Nbasis
          lambda(cNO,dNO) = 2.0D0*ON(cNO)*(h_NO(cNO,dNO) + Gcl_NO(cNO,dNO) + Goc_NO(cNO,dNO))
        end do ! dNO
      end do ! cNO
      do Iopen = 1, Nopen ! open-shell
        cNO = NO_open(Iopen)
        do dNO = 1, Nbasis
          lambda(cNO,dNO) = ON(cNO)*(h_NO(cNO,dNO) + Gcl_NO(cNO,dNO) + Gop_NO(cNO,dNO))
        end do ! dNO
      end do ! cNO
!
! pair correction and static terms
!
      do Cact = 1, Nact
        cNO = NO_act(Cact)
        do dNO = 1, Nbasis
!
          do Iact = 1, Nact
            iNO = NO_act(Iact)
            cidi_index = TEIindex(Nbasis,cNO,iNO,dNO,iNO)
            ciid_index = TEIindex(Nbasis,cNO,iNO,iNO,dNO)
!
            lambda(cNO,dNO) = lambda(cNO,dNO)&
                            + 2.0D0*eta(cNO,iNO)*(2.0D0*eriNO(cidi_index) - eriNO(ciid_index))&
                            + 2.0D0*(zeta(cNO,iNO) - xi(cNO,iNO))*eriNO(ciid_index)
!
          end do ! Iact
!
        end do ! dNO
      end do ! Cact
!
! high-spin correction
      if(LHSCon)then
!
      do Cocc = 1, Nocc
        cNO = NO_occ(Cocc)
        do dNO = 1, Nbasis
!
          do Iocc = 1, Nocc
            iNO = NO_occ(Iocc)
!
            ciid_index = TEIindex(Nbasis,cNO,iNO,iNO,dNO)
!
            lambda(cNO,dNO) = lambda(cNO,dNO)&
                            - 2.0D0*kappa(cNO,iNO)*eriNO(ciid_index)
          end do ! Iocc
!
        end do ! dNO
      end do ! Cocc
!
      end if ! HSC
!
! dynamic part
!      select case(dynamic)
! Nonvariational CS
      select case('NONE')
!
        case('CS')
!
        case('OF')
          call KILL_object ('CFT', 'lambda_dyn', 'OSEC')      
          call GET_object ('CFT', 'lambda_dyn', 'OSEC')
!
          lambda = lambda + lambda_OSEC
!
          call KILL_object ('CFT', 'lambda_dyn', 'FHC')      
          call GET_object ('CFT', 'lambda_dyn', 'FHC')
!
          lambda = lambda + lambda_FHC
!
        case default
!
      end select ! dynamic
!
! Print 
!
!      write(UNIout,'(/a)')'   Diagonal lambda values      '
!      write(UNIout,'(a)')'-------------------------------'
!      write(UNIout,'(a)')'       a         lambda_aa     '
!      write(UNIout,'(a)')'-------------------------------'
! 
!      do aNO = 1, Nbasis
!        if(ON(aNO).ge.5.0D-09)then
!          write(UNIout,'(2X,I6,6X,F14.8)')aNO,lambda(aNO,aNO)/(TWO*ON(aNO))
!        end if
!      end do
!
!
! End of routine CFT_BLD_lambda
      call PRG_manager('exit','CFT_BLD_lambda','UTILITY')
      end subroutine CFT_BLD_lambda
      subroutine order_of_elements(N,array,order)
!***********************************************************************
!     Date last modified: June 20, 2014                                *
!     Author: J.W. Hollett                                             *
!     Description: Get the order of elements of a 1D array of length N.* 
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer, intent(IN) :: N
! Input arrays:
      double precision, intent(IN) :: array(N)
! Output arrays:
      integer, intent(OUT) :: order(N)
! Local scalars:
      integer :: I,J
! Local arrays:
      double precision :: work(N)
!
! Begin:
!
      call PRG_manager('enter','order_of_elements','UTILITY')
!
      work = array
!
      do I = 1, N
        J = minloc(work,1)
        order(I) = J
        work(J) = maxval(work) + 1.0D0
      end do ! I
!
! End of routine order_of_elements
      call PRG_manager('exit','order_of_elements','UTILITY')
      end subroutine order_of_elements
      subroutine CFT_ON_opt
!***********************************************************************
!     Date last modified: January 20, 2017                             *
!     Author: JWH                                                      *
!     Description: Optimize the ONs.                                   * 
!***********************************************************************
! Modules:
      USE program_files
      USE CFT_objects
      USE type_molecule
      USE QM_objects
      USE program_timing

      implicit none
!
! Local scalars:
      integer :: IER, aNO, Istat, max_diis, n_diis
      double precision :: max_stepsize
      double precision :: grad_length, grad_length_prev, Time_elapsed, difftol
      logical :: Lsteep
! Local arrays:
      double precision, allocatable, dimension(:,:) :: X, QNO, grad_diis, theta_diis
      double precision, allocatable, dimension(:) :: eigval, p, work, theta_diff, theta0
!
! Begin:
!
      call PRG_manager('enter','CFT_ON_opt','UTILITY')
!
      max_stepsize = 1.5707963267948966D0 ! pi/2, cos^2theta repeats after pi/2
      difftol = 1.0D-8
!
      write(UNIout,'(/a)')'       ON optimization'
      write(UNIout,'(a)')'----------------------------------'
!
      if(Nstat.eq.0)then
        write(UNIout, '(/a)')'   No statically correlated orbital pairs, therefore ONs are optimized.  '
        ! Reset occupation numbers
        call CFT_Delta_to_ON
        return
      end if
!
      allocate(QNO(Nstat,Nstat),eigval(Nstat),p(Nstat))
      allocate(work(3*Nstat-1),X(Nstat,Nstat),theta_diff(Nstat),theta0(Nstat))
!
! Get thetas
      call CFT_Delta_to_theta
!
! Set-up DIIS
      max_diis = 10
      allocate(grad_diis(Nstat,max_diis),theta_diis(Nstat,max_diis))
      n_diis = 0
      grad_diis = 0.0D0
      theta_diis = 0.0D0
!
! Optimize ONs
      ONstep = 0
      grad_length = 1.0D0
      Lsteep = .false.
      write(UNIout,'(/a)')'   ON step         |grad|        '
      write(UNIout,'(a)')'----------------------------------'
!
      do while((grad_length.gt.max_grad_length).and.(ONstep.lt.max_ONstep))
!
! Need gradient and hessian, with respect to thetas
        if(ONstep.eq.0)then
          call CFT_Delta_gradient
        end if ! It is calculated to check convergence below
        call CFT_Delta_hessian
!
! Zero off-diagonals of hessian
!        do Istat = 1, Nstat
!          do Jstat = 1, Nstat
!           if(Istat.eq.Jstat)cycle
!            hessian(Istat,Jstat) = 0.0D0
!          end do ! Jstat
!        end do ! Istat
!
        QNO = hessian
!
! Invert hessian
        call dsyev('V', 'U', Nstat, QNO, Nstat, eigval, work, 3*Nstat-1, IER)
!
!        write(6,*)' Hessian eigenvalues'
!        do Istat = 1, Nstat
!          write(6,'(I4,X,F20.10)')Istat, eigval(Istat)
!        end do
!
! Take absolute value of hessian eigenvalues (ensure descent step)
        X = 0.0D0
        do Istat = 1, Nstat
          X(Istat,Istat) = 1.0D0/dabs(eigval(Istat))
          !X(Istat,Istat) = 1.0D0/eigval(Istat)
        end do ! Istat
!
        hessian = matmul(QNO,matmul(X,transpose(QNO)))
!
! Calculate step
        if(Lsteep)then
          p = -grad_theta
          Lsteep = .false.
        else
          p = -matmul(hessian,grad_theta)
        end if
!
! Reduce step size if outside the trust-region
!        pnorm = dsqrt(dot_product(p(1:Nstat),p(1:Nstat)))/dble(Nstat)
!        pnorm = maxval(dabs(p(1:Nstat)))
!        if(pnorm.gt.max_stepsize)then
!          p = max_stepsize/pnorm*p
!        end if
!
! Take full step
        theta(1:Nstat) = theta(1:Nstat) + p(1:Nstat)
!
! Perform DIIS extrapolation
          n_diis = min(n_diis+1,max_diis)
          ! interpolate theta
          call DIIS_extrapolation(Nstat,Nstat,n_diis,grad_diis,theta_diis,grad_theta,theta)
!
!        write(6,*)' p '
!        do Istat = 1, Nstat
!          write(6,'(I4,X,F20.10)')Istat, p(Istat)
!        end do
!
!        write(6,*)' thetas'
!        do Istat = 1, Nstat
!          i = stat_pair(Istat,1)
!          r = stat_pair(Istat,2)
!          write(6,'(I4,X,I4,X,F20.10)')i, r, theta(Istat)
!        end do
!
        ONstep = ONstep + 1
!
! Update Deltas and ONs
        call CFT_theta_to_Delta
        call CFT_Delta_to_ON
        call CFT_Delta_to_theta ! May of needed to scale Deltas
        call CFT_BLD_ezxk
!
! Also need to update G
        call CFT_BLD_Gmatrix
        call CFT_2D_transform(Gcl_AO, NO, Gcl_NO, Nbasis, Nbasis)
        call CFT_2D_transform(Gop_AO, NO, Gop_NO, Nbasis, Nbasis)
        call CFT_2D_transform(Goc_AO, NO, Goc_NO, Nbasis, Nbasis)
!
! Check gradient of the Lagrangian
        grad_length_prev = grad_length ! save previous mu_rmsd
        call CFT_Delta_gradient
        grad_length = dsqrt(dot_product(grad_theta,grad_theta))
!
        write(UNIout,'(3x,I4,7x,F14.10)')ONstep,grad_length
!
      end do ! ON opt
!
      if(ONstep.eq.max_ONstep)then
        write(UNIout,'(a,I3,a)')'Maximum number of steps reached. ',ONstep,' steps taken.'
      else
        write(UNIout,'(a,I3,a)')'ONs optimized. ',ONstep,' steps taken'
      end if
!
! Print natural orbital occupation numbers
      write(UNIout,'(/a)')'         orbital occupancies           '
      write(UNIout,'(a)')'---------------------------------------'
      write(UNIout,'(a)')'       a             alpha + beta      '
      write(UNIout,'(a)')'---------------------------------------'
      do aNO = 1, NBelectrons
        write(UNIout,'(2X,I6,10X,F14.11)')aNO,2.0D0*ON(aNO)
      end do ! aNO
      do aNO = NBelectrons+1, NAelectrons
        write(UNIout,'(2X,I6,10X,F14.11)')aNO,ON(aNO)
      end do ! aNO
      do aNO = NAelectrons+1, vac_act(Nvac_act)
        write(UNIout,'(2X,I6,10X,F14.11)')aNO,2.0D0*ON(aNO)
      end do ! aNO
!
! Get timing
      call PRG_CPU_time
      call CNV_cpu (lstcpu, hcpu, mcpu, scpu, ccpu, secpu)
      call PRG_date
      call PRG_time
!
      Time_elapsed = Time_current-Time_begin
      if(Time_elapsed.lt.0.0D0)then ! It's a new day
        Time_elapsed=Time_elapsed+86400
      end if
!
      call CNV_cpu (Time_elapsed, hcpu, mcpu, scpu, ccpu, setim)
!
      write(UNIout,'(/2a)')'Cpu     time:',secpu
      write(UNIout,'(2a/)')'Elapsed time:',setim
!
! End of routine CFT_ON_opt
      call PRG_manager('exit','CFT_ON_opt','UTILITY')
      end subroutine CFT_ON_opt
      subroutine CFT_BLD_NO_pairs
!***********************************************************************
!     Date last modified: April 6, 2020                                *
!     Author: J.W. Hollett                                             *
!     Description: Determine static orbital pairs.                     *
!                  (Pairs only determined manually right now.)         * 
!***********************************************************************
! Modules:
      USE CFT_objects
      USE program_files
      USE type_molecule
      USE QM_objects
      USE INT_objects
!
      implicit none
!
! Local scalars:
      integer :: aNO, bNO, rNO, Istat
      integer :: Aocc, Avac, Bvac, Aact, Bact
      double precision :: K_ABmax, Dtol, Ktol
      integer :: Npaired, Iclosed, Iopen
! Local arrays:
      integer, allocatable, dimension(:) :: occ_count, vac_count, temp_vac, temp_occ, temp_act
      integer, allocatable, dimension(:,:) :: Qval
      double precision, allocatable, dimension(:,:) :: K_NOtemp

!
! Begin:
      call PRG_manager ('enter', 'CFT_BLD_NO_pairs', 'UTILITY')
!
      Dtol = 5.0D-09
!      Ktol = 1.0D-08
!
! Make list of closed and open shell orbitals
      if(allocated(NO_open))then
        deallocate(NO_open,NO_closed)
      end if
      Nclosed = NBelectrons+Nbasis-NAelectrons
      Nopen = Nbasis-Nclosed
      allocate(NO_open(Nopen),NO_closed(Nclosed))
!
! Determine number of alpha and beta electrons
!
      NAelectrons = (Nelectrons + Multiplicity - 1)/2
      NBelectrons = Nelectrons - NAelectrons
!
! Identify closed and open NOs
      Iclosed = 0
      do aNO = 1,NBelectrons
        Iclosed = Iclosed + 1
        NO_closed(Iclosed) = aNO
      end do
      do aNO = NAelectrons+1,Nbasis
        Iclosed = Iclosed + 1
        NO_closed(Iclosed) = aNO
      end do
      Iopen = 0 
      do aNO = NBelectrons+1,NAelectrons
        Iopen = Iopen + 1
        NO_open(Iopen) = aNO
      end do
!
! No longer use energy criteria, just take valence and (HOMO+valence)
!      call CFT_NO_valence
!
! Calculate pair potential for each active occupied with the active vacants
!      call CFT_pair_pot
!
! Calculate the coincidence of NOs, explore this later
!      call GET_NI_exchange
!
! Next, get exchange (actually time-inversion and exchange) between these orbitals
!      call GET_object ('CFT', '2EINT', 'OS')
!      ! Transform to NO basis
!      if(allocated(eriNO))then
!        deallocate(eriNO)
!      end if
!      allocate(eriNO(Nbasis**4))
!
!      call CFT_gen_TEItrans(occ_val,Nocc_val,vac_val,Nvac_val,vac_val,Nvac_val,occ_val,Nocc_val,&
!                             NO,Nbasis,eriAO,eriNO)
!
!      if(allocated(K_NO))then
!        deallocate(K_NO)
!      end if
!      allocate(K_NO(Nbasis,Nbasis))
!      call CFT_gen_BLD_K(occ_val,Nocc_val,vac_val,Nvac_val,eriNO,K_NO,Nbasis)
!
!
! Get momentum-balance between orbitals
      call GET_object ('INT', 'EAMINT', 'AO')
      ! Calculate momentum balance from equi and antimomentum
      if(allocated(mbAO))then
        deallocate(mbAO)
      end if
      allocate(mbAO(Nbasis**4))
      ! incorporate a negative sign
      mbAO = amAO - emAO
      ! Transform to NO basis
      if(allocated(mbNO))then
        deallocate(mbNO)
      end if
      allocate(mbNO(Nbasis**4))
!
! Get the MB between the occupied and first Nocc vacant
!      
      if(allocated(NO_act))then
        deallocate(NO_act)
      end if
      !NcoreNO = NFrozen_cores() can't use this right now because it requires mod_QM_defaults
      ! which creates clash with Nopen
      NcoreNO = 0
      Nocc_val = NBelectrons - NcoreNO
      Nact = min(Nbasis,NAelectrons+3*Nocc_val) - NcoreNO
      allocate(NO_act(Nact))
      do Aact = 1, Nact
        NO_act(Aact) = NcoreNO + Aact
      end do 
!
      call CFT_gen_TEItrans(NO_act,Nact,NO_act,Nact,NO_act,Nact,NO_act,Nact,&
                             NO,Nbasis,mbAO,mbNO)
      deallocate(mbAO)
!
      if(allocated(M_NO))then
        deallocate(M_NO)
      end if
      allocate(M_NO(Nbasis,Nbasis))
      call CFT_gen_BLD_M(NO_act,Nact,NO_act,Nact,mbNO,M_NO,Nbasis)
!
! Print momentum-balance between active orbitals
! will use this later for NO pairing
      write(UNIout,'(a)')'----------------------------------------------------'
      write(UNIout,'(a)')'          Interorbital momentum-balance            '
      write(UNIout,'(a/)')'----------------------------------------------------'
      do Aact = 1, Nact
        aNO = NO_act(Aact)
        do Bact = 1, Nact
          bNO = NO_act(Bact)
!
          write(UNIout,'(2X,I4,4X,I4,4X,F16.10)')aNO,bNO,M_NO(aNO,bNO)
!
        end do ! Bact
      end do ! Aact
      write(UNIout,'(a/)')' '
!
! Use max exchange to pair orbitals
!
!      allocate(K_NOtemp(Nbasis,Nbasis),Qval(Nbasis,Nbasis))
!      if(Nocc_val.gt.0)then
!        K_NOtemp = K_NO
!        Qval = Qstat
!        Qstat = 0
!        Npaired = 0
! Need to go through K_NOtemp and zero any values that do not correspond to valence orbitals
!        do Aocc = 1, Nocc_val
!          aNO = occ_val(Aocc)
!          do Bvac = 1, Nvac_val
!            bNO = vac_val(Bvac)
!            if(Qval(aNO,bNO).lt.1)then
!              K_NOtemp(aNO,bNO) = 0.0D0
!              K_NOtemp(bNO,aNO) = 0.0D0
!            end if 
!          end do 
!        end do 
! Now pair orbitals
!        do while(Npaired.lt.Nocc_val)
!          K_ABmax = maxval(K_NOtemp(occ_val(1):occ_val(Nocc_val),vac_val(1):vac_val(Nvac_val)))
!          if(K_ABmax.lt.Ktol)exit
!          do Aocc = 1, Nocc_val
!            aNO = occ_val(Aocc)
!            if(sum(Qval(aNO,1:Nbasis)).lt.1)cycle
!            do Bvac = 1, Nvac_val
!              bNO = vac_val(Bvac)
!              if(Qval(aNO,bNO).lt.1)cycle
!
!              if(dabs(K_NO(aNO,bNO)-K_ABmax).lt.Ktol)then ! found maximum K_AB
!              if(dabs(K_NOtemp(aNO,bNO)-K_ABmax).lt.Ktol)then ! found maximum K_AB, 1 pair per
!                Qstat(aNO,bNO) = 1
!                K_NOtemp(1:Nbasis,bNO) = 0.0D0 ! These orbitals will not form any other pairs
!                K_NOtemp(aNO,1:Nbasis) = 0.0D0
!              end if
!
!            end do ! bNO
!          end do ! aNO
!
!          Npaired = 0
!          do Aocc = 1, Nocc_val ! occupied
!            aNO = occ_val(Aocc)
!            if(sum(Qstat(aNO,1:Nbasis)).gt.0)then
!              Npaired = Npaired + 1
!            end if
!          end do
!
!        end do
!
!      end if ! Nocc_val
!
      if(Delta_guess.ne.'READ')then
        if(allocated(Qstat))then
          deallocate(Qstat)
        end if
        allocate(Qstat(Nbasis,Nbasis))
        Qstat = 0
      end if
!
! Check for input orbital pairs
!
      if(LQin)then
!
! Just for now, will have to revise this once automated selection is decided upon
!
        do aNO = 1, Nbasis
          do bNO = 1, Nbasis
            if(Qinput(aNO,bNO).eq.1)then
              Qstat(aNO,bNO) = 1
            end if
          end do ! bNO vac
        end do ! aNO occ
!
      end if ! input pairs
!
      Nstat = sum(Qstat(1:Nbasis,1:Nbasis))
!
! Make a list of static pairs
      if(Nstat.gt.0)then
        if(allocated(stat_pair))then
          deallocate(stat_pair)
        end if
        allocate(stat_pair(Nstat,2))
        Istat = 0
        do aNO = 1, Nbasis
          do bNO = 1, Nbasis
            if(Qstat(aNO,bNO).eq.1)then
              Istat = Istat + 1
              stat_pair(Istat,1) = aNO
              stat_pair(Istat,2) = bNO
              if(Delta(aNO,bNO).lt.Dtol) Delta(aNO,bNO) = Dtol ! Initial Delta should not be zero
            else
              ! Zero any Deltas that no longer correspond to a static pair
              Delta(aNO,bNO) = 0.0D0
            end if
          end do ! bNO
        end do ! aNO
!
      else ! Nstat .eq. 0
        ! Zero any Deltas that no longer correspond to a static pair
        Delta = 0.0D0
      end if !
!
! Set sign of pair interaction, sigma
      if(allocated(sigma))then
        deallocate(sigma)
      end if
      allocate(sigma(Nbasis,Nbasis))
!
      sigma = 1.0D0 ! set all interactions to the usual unpairing
!
      if(Lsigin)then
!
        do aNO = 1, Nbasis
          do bNO = 1, Nbasis
            if(sigin(aNO,bNO).eq.-1)then
              sigma(aNO,bNO) = -1.0D0
            end if
          end do ! bNO
        end do ! aNO
!
      end if ! input sigma
!
! Print statically correlated orbital pairs
      write(UNIout,'(a)')'----------------------------------------------------'
      write(UNIout,'(a)')'      Statically correlated NO pairs and (sigma)           '
      write(UNIout,'(a/)')'----------------------------------------------------'
      if(Nstat.gt.0)then
      do Istat = 1, Nstat
        write(UNIout,'(16X,I4,4X,I4,8x,a,F4.1,a)')stat_pair(Istat,1),stat_pair(Istat,2),'(',&
                                                sigma(stat_pair(Istat,1),stat_pair(Istat,2)),')'
      end do
      else
        write(UNIout,'(a)')'        No statically correlated NO pairs!'
      end if
!
! Create lists of active vacant and occupied orbitals
      allocate(temp_vac(Nbasis))
      allocate(temp_occ(Nbasis))
      allocate(temp_act(Nbasis))
      temp_vac = 0
      temp_occ = 0
      temp_act = 0
      do Istat = 1, Nstat
        aNO = stat_pair(Istat,1)
        bNO = stat_pair(Istat,2)
!
        temp_occ(aNO) = 1
        temp_vac(bNO) = 1
        temp_act(aNO) = 1
        temp_act(bNO) = 1
!
      end do ! Istat
!
! Create a list of active occupied orbitals
      Nocc_act = sum(temp_occ)
      !write(6,*)'Nocc= ', Nocc
      if(allocated(occ_act))then
        deallocate(occ_act)
      end if
      allocate(occ_act(Nocc_act))
      Aocc = 0
      do aNO = 1, Nbasis
        if(temp_occ(aNO).eq.1)then
          Aocc = Aocc + 1
          occ_act(Aocc) = aNO
        end if
      end do
!
!      write(6,*)'Active occupied orbitals'
!      do Aocc = 1, Nocc_act
!        aNO = occ_act(Aocc)
!        write(6,'(I4,X,I4)') Aocc, aNO
!      end do
!
! Create a list of vacant orbitals
      Nvac_act = sum(temp_vac)
      !write(6,*)'Nvac= ', Nvac
      if(allocated(vac_act))then
        deallocate(vac_act)
      end if
      allocate(vac_act(Nvac_act))
      Avac = 0
      do aNO = 1, Nbasis
        if(temp_vac(aNO).eq.1)then
          Avac = Avac + 1
          vac_act(Avac) = aNO
        end if
      end do
!
!      write(6,*)'Active vacant orbitals'
!      do Avac = 1, Nvac_act
!        aNO = vac_act(Avac)
!        write(6,'(I4,X,I4)') Avac, aNO
!      end do
!
! Create a list of active orbitals
      Nact = sum(temp_act)
      !write(6,*)'Nact= ', Nact
      if(allocated(NO_act))then
        deallocate(NO_act)
      end if
      allocate(NO_act(Nact))
      Aact = 0
      do aNO = 1, Nbasis
        if(temp_act(aNO).eq.1)then
          Aact = Aact + 1
          NO_act(Aact) = aNO
        end if
      end do
!
!      write(6,*)'Active orbitals'
!      do Aact = 1, Nact
!        aNO = NO_act(Aact)
!        write(6,'(I4,X,I4)') Aact, aNO
!      end do
!
! Create list of inactive(frozen) vacant
      Nvac_froz = Nbasis - NAelectrons - Nvac_act
      if(allocated(vac_froz))then
        deallocate(vac_froz)
      end if
      allocate(vac_froz(Nvac_froz))
      Avac = 0
      do aNO = NAelectrons+1, Nbasis
        if(temp_act(aNO).eq.0)then
          Avac = Avac + 1
          vac_froz(Avac) = aNO
        end if
      end do ! aNO
!
!      write(6,*)'Frozen vacant orbitals'
!      do Avac = 1, Nvac_froz
!        aNO = vac_froz(Avac)
!        write(6,'(I4,X,I4)') Avac, aNO
!      end do
!
! Create list of inactive(frozen) occupied
      Nocc_froz = NBelectrons - Nocc_act
      if(allocated(occ_froz))then
        deallocate(occ_froz)
      end if
      allocate(occ_froz(Nocc_froz))
      Aocc = 0
      do aNO = 1, NBelectrons
        if(temp_act(aNO).eq.0)then
          Aocc = Aocc + 1
          occ_froz(Aocc) = aNO
        end if
      end do ! aNO
!
!      write(6,*)'Frozen occupied orbitals'
!      do Aocc = 1, Nocc_froz
!        aNO = occ_froz(Aocc)
!        write(6,'(I4,X,I4)') Aocc, aNO
!      end do
!
! Create a list of static pair orbitals for each orbital
! 
      if(allocated(Nstat_occ))then
        deallocate(Nstat_occ,Nstat_vac,occ_stat,vac_stat)
      end if
      allocate(Nstat_occ(Nbasis),Nstat_vac(Nbasis))
      allocate(occ_stat(Nbasis,Nvac_act),vac_stat(Nbasis,Nocc_act))
      allocate(occ_count(Nbasis),vac_count(Nbasis))
      Nstat_occ = 0
      Nstat_vac = 0
      occ_count = 0
      vac_count = 0
      occ_stat = 0
      vac_stat = 0
!
      do Istat = 1, Nstat
        aNO = stat_pair(Istat,1)
        rNO = stat_pair(Istat,2)
!
        Nstat_occ(aNO) = Nstat_occ(aNO) + 1
        Nstat_vac(rNO) = Nstat_vac(rNO) + 1
!
        occ_count(aNO) = occ_count(aNO) + 1
        vac_count(rNO) = vac_count(rNO) + 1
!
        occ_stat(aNO,occ_count(aNO)) = rNO
        vac_stat(rNO,vac_count(rNO)) = aNO
!
      end do ! Istat
!
! Set number of actually occupied orbitals
      if(Nvac_act.gt.0)then
        Nnatorb = vac_act(Nvac_act)
      else
        Nnatorb = NAelectrons
      end if
!
!      write(6,'(a)')'Active occupied and their vacant pairs'
!      do aNO = 1, Nelectrons/2
!        if(Nstat_occ(aNO).gt.0)then
!          write(6,'(a,I4,a)')'Orbital ', aNO, ' is statically correlated to:'
!        end if
!        do Avac = 1, Nstat_occ(aNO)
!          write(6,'(I4)') occ_stat(aNO,Avac)
!        end do
!      end do 
!
!      write(6,'(a)')'Active vacant and their occupied pairs'
!      do aNO = Nelectrons/2+1, Nbasis
!        if(Nstat_vac(aNO).gt.0)then
!          write(6,'(a,I4,a)')'Orbital ', aNO, ' is statically correlated to:'
!        end if
!        do Aocc = 1, Nstat_vac(aNO)
!          write(6,'(I4)') vac_stat(aNO,Aocc)
!        end do
!      end do 
!
! End of routine CFT_BLD_NO_pairs
      call PRG_manager ('exit', 'CFT_BLD_NO_pairs', 'UTILITY')
      end subroutine CFT_BLD_NO_pairs
      subroutine CFT_NO_valence
!***********************************************************************
!     Date last modified: April 6, 2020                                *
!     Author: JWH                                                      *
!     Description: Determine valence space (occupied and vacant),      *
!                  used for default NO pairing.                        * 
!***********************************************************************
! Modules:
      USE program_constants
      USE CFT_objects
      USE type_molecule
      USE QM_objects
      USE QM_defaults
!
      implicit none
!
! Local scalars:
      integer :: aNO, bNO, Aact, Aocc, Avac
      integer :: Iocc_val, Ivac_val
!
! Begin:
      call PRG_manager ('enter', 'CFT_NO_neardeg', 'UTILITY')
!
      if(Delta_guess.ne.'READ')then
      if(allocated(Qstat))then
        deallocate(Qstat)
      end if
      allocate(Qstat(Nbasis,Nbasis))
      Qstat = 0
      end if
!
      if(Delta_guess.ne.'READ')then
!
! Do not use energy cutoff anymore
!
! Turn off static for now
!      LNstat=.true.
!      Nstat=0
!
! Determine number of active pairs
! Let's let all vacant be possible partners for occupied
!
        ! From valence orbitals
        NcoreNO = NFrozen_cores()
        Nocc_val = NBelectrons - NcoreNO
        Nvac_val = 2*Nocc_val
        Qstat =  ZERO
        Qstat(NcoreNO+1:NBelectrons,NAelectrons+1:NAelectrons+Nvac_val) = ONE
        !Qstat(NcoreNO+1:NBelectrons,NAelectrons+1:Nbasis) = ONE
!
      end if ! Q not read
!
! Count valence occupied and vacant
      Nocc_val = 0
      do aNO = 1, NBelectrons ! HF doubly occupied
        if(sum(Qstat(aNO,1:Nbasis)).gt.0)then
          Nocc_val = Nocc_val + 1
        end if
      end do
      Nvac_val = 0
      do bNO = NAelectrons+1, Nbasis ! HF vacual
        if(sum(Qstat(1:Nbasis,bNO)).gt.0)then
          Nvac_val = Nvac_val + 1
        end if
      end do
! 
! Create list of valence occupied and vacual orbitals
      if(allocated(occ_val))then
        deallocate(occ_val,vac_val)
      end if
      allocate(occ_val(Nocc_val),vac_val(Nvac_val))
      Iocc_val = 0
      do aNO = 1, NBelectrons ! HF doubly occupied
        if(sum(Qstat(aNO,1:Nbasis)).gt.0)then
          Iocc_val = Iocc_val + 1
          occ_val(Iocc_val) = aNO
        end if
      end do
      Ivac_val = 0
      do bNO = NAelectrons+1, Nbasis ! HF vacant
        if(sum(Qstat(1:Nbasis,bNO)).gt.0)then
          Ivac_val = Ivac_val + 1
          vac_val(Ivac_val) = bNO
        end if
      end do
!
! Combine lists to get a temporary list of active orbitals
!
      Nact = Nocc_val + Nvac_val
      if(allocated(NO_act))then
        deallocate(NO_act)
      end if
      allocate(NO_act(Nact))
      Aact = 0 
      do Aocc = 1, Nocc_val
        Aact = Aact + 1
        NO_act(Aact) = occ_val(Aocc)
      end do ! Aocc
      do Avac = 1, Nvac_val
        Aact = Aact + 1
        NO_act(Aact) = vac_val(Avac)
      end do ! Iocc
!
      call PRG_manager ('exit', 'CFT_NO_valence', 'UTILITY')
!
! End of routine CFT_NO_valence
      end subroutine CFT_NO_valence
      subroutine CFT_BLD_NO_coupled
!***********************************************************************
!     Date last modified: July 15, 2020                                *
!     Author: J.W. Hollett                                             *
!     Description: Determine which pairs of occupied share vacant.     *
!***********************************************************************
! Modules:
      USE CFT_objects
      USE program_files
      USE type_molecule
      USE QM_objects
      USE INT_objects
!
      implicit none
!
! Local scalars:
      integer :: Istat, Jstat, Icoup, aNO, bNO, Nshr, rNO, sNO, Rvac, Svac, Ishr
      logical :: Ldup
!
! Local arrays:
      integer, allocatable, dimension(:) :: coupled
      integer, allocatable, dimension(:,:) :: temp_coup_pair, temp2_coup_pair
!
! Begin:
      call PRG_manager ('enter', 'CFT_BLD_NO_coupled', 'UTILITY')
!
      if(allocated(coupNO))then
        deallocate(coupNO)
      end if
      allocate(coupled(Nbasis))
      allocate(coupNO(Nbasis))
      allocate(temp_coup_pair(Nbasis,2))
      allocate(temp2_coup_pair(Nbasis,2))
!
      Ncoup = 0
      coupled = 0
      coupNO = 0
      do Istat = 2, Nstat
        do Jstat = 1, Istat-1
          if(stat_pair(Istat,1).ne.stat_pair(Jstat,1))then ! different occupied
            if(stat_pair(Istat,2).eq.stat_pair(Jstat,2))then ! same vacant
              ! Check to see if it is a new couple
              Ldup = .false.
              do Icoup = 1, Ncoup
                if(((temp_coup_pair(Icoup,1).eq.stat_pair(Istat,1)).and.&
                    (temp_coup_pair(Icoup,2).eq.stat_pair(Jstat,1))).or.&
                   ((temp_coup_pair(Icoup,1).eq.stat_pair(Jstat,1)).and.&
                    (temp_coup_pair(Icoup,2).eq.stat_pair(Istat,1))))then
                  Ldup = .true. 
                end if
              end do ! Icoup 
              !
              if(Ldup)cycle 
              !
              Ncoup = Ncoup + 1  
              !
              coupled(stat_pair(Istat,1)) = coupled(stat_pair(Istat,1)) + 1
              coupled(stat_pair(Jstat,1)) = coupled(stat_pair(Jstat,1)) + 1
              coupNO(stat_pair(Istat,1)) = stat_pair(Jstat,1)
              coupNO(stat_pair(Jstat,1)) = stat_pair(Istat,1)
              temp_coup_pair(Ncoup,1) = stat_pair(Istat,1)
              temp_coup_pair(Ncoup,2) = stat_pair(Jstat,1)
!
            end if ! shared vacant
          end if ! occupied not equal
        end do ! Jstat
      end do ! Istat
!
      if(allocated(coup_pair))then
        deallocate(coup_pair)
      end if
      allocate(coup_pair(Ncoup,2))
      coup_pair(1:Ncoup,1:2) = temp_coup_pair(1:Ncoup,1:2)
!
! Check to make sure there are only couples (no triples, etc.)
      if(maxval(coupled).gt.1)then
              write(6,*)'More than two occupied are sharing a vacant'
              write(6,*)'Current DeltaNO implementation only supports two occupied sharing a vacant'
              write(6,*)'This will cause trouble!'
      end if
!
      write(UNIout,'(a)')'----------------------------------------------------'
      write(UNIout,'(a)')'              Coupled occupied NOs                  '
      write(UNIout,'(a/)')'----------------------------------------------------'
      if(Ncoup.gt.0)then
        do Icoup = 1, Ncoup
          write(UNIout,'(18X,I4,4X,I4)')coup_pair(Icoup,1),coup_pair(Icoup,2)
        end do
      else
        write(UNIout,'(a)')'  No occupied paired with the same vacant!'
      end if
!
! Get a list of shared vacants for each couple
!
      if(allocated(vacshr))then
        deallocate(vacshr,Nvacshr)
      end if
      allocate(vacshr(Ncoup,Nvac_act),Nvacshr(Ncoup))
      vacshr = 0
      Nvacshr = 0
!
      do Icoup = 1, Ncoup
        Nshr = 0
!
        aNO = coup_pair(Icoup,1)
        bNO = coup_pair(Icoup,2)
!
        do Rvac = 1, Nstat_occ(aNO)
          rNO = occ_stat(aNO,Rvac)
!
          do Svac = 1, Nstat_occ(bNO)
            sNO = occ_stat(bNO,Svac)
!
            if(rNO.eq.sNO)then ! shared
              Nshr = Nshr + 1
              vacshr(Icoup,Nshr) = rNO
            end if
!
          end do ! Svac
        end do ! Rvac
!
        Nvacshr(Icoup) = Nshr
!
      end do ! Icoup
!
      write(UNIout,'(a)')'----------------------------------------------------'
      write(UNIout,'(a)')'              Shared vacant NOs                      '
      write(UNIout,'(a/)')'----------------------------------------------------'
      if(Ncoup.gt.0)then
        do Icoup = 1, Ncoup
          write(UNIout,'(a,I4,4X,I4)')'occupied couple: ',coup_pair(Icoup,1),coup_pair(Icoup,2)
          write(UNIout,'(a)')'shared vacant:'
          do Ishr = 1, Nvacshr(Icoup)
            write(UNIout,'(10X,I4)')vacshr(Icoup,Ishr)
          end do 
        end do
      else
        write(UNIout,'(a)')'  No occupied paired with the same vacant!'
      end if
!
      call PRG_manager ('exit', 'CFT_BLD_NO_coupled', 'UTILITY')
      end subroutine CFT_BLD_NO_coupled
      subroutine CFT_gen_TEItrans(Alist,NA,Blist,NB,Clist,NC,Dlist,ND,Coeff,Nbasis,intAO,intNO)
!***********************************************************************
!     Date last modified: March 20, 2016                               *
!     Author: JWH                                                      *
!     Description: Transform two-electron integrals in four steps.     *
!                  Generalized to any mix of orbital spaces.           *
!                  In physicist notation: <ij|kl>                      * 
!***********************************************************************
! Modules:
      USE program_files
      USE program_timing

      implicit none
!
! Input scalars:
      integer, intent(IN) :: Nbasis, NA, NB, NC, ND
! Input arrays:
      double precision, intent(IN) :: intAO(Nbasis**4)
      double precision, intent(IN) :: Coeff(Nbasis,Nbasis)
      integer, intent(IN) :: Alist(NA), Blist(NB), Clist(NC), Dlist(ND)
! Output arrays:
      double precision, intent(OUT) :: intNO(Nbasis**4) ! alter size when its an issue
!
! Local scalars:
      integer :: aNO, bNO, cNO, dNO, Aiter, Biter, Citer, Diter
      integer :: iAO, jAO, kAO, lAO, a_index, abcd_index, abcl_index, abkl_index, ajkl_index, akjl_index
      integer :: b_index, c_index, ijkl_index, jkl_index, kjl_index, kl_index
      double precision :: Cia, Cjb, Ckc, Time_elapsed
!
! Local arrays:
      double precision, dimension(:) :: int_temp(Nbasis**4)
!
! Begin:
      call PRG_manager ('enter', 'CFT_gen_TEItrans', 'UTILITY')
!
!      write(UNIout,'(/a)')'Begin TEI transformation'
! Get timing
!      call PRG_CPU_time
!      call CNV_cpu (lstcpu, hcpu, mcpu, scpu, ccpu, secpu)
!      call PRG_date
!      call PRG_time
!
!      Time_elapsed = Time_current-Time_begin
!      if(Time_elapsed.lt.0.0D0)then ! It's a new day
!        Time_elapsed=Time_elapsed+86400
!      end if
!
!      call CNV_cpu (Time_elapsed, hcpu, mcpu, scpu, ccpu, setim)
!
!      write(UNIout,'(/2a)')'Cpu     time:',secpu
!      write(UNIout,'(2a/)')'Elapsed time:',setim
!
! First step [aj|kl] = \sum_{i} C_{ia} [ij|kl]
!
      int_temp = 0.0D0
      do Aiter = 1, NA
        aNO = Alist(Aiter)
        a_index = Nbasis**3*(aNO-1)
        ijkl_index = 0
        do iAO = 1, Nbasis
          Cia = Coeff(iAO,ANO)
          jkl_index = 0
          do jAO = 1, Nbasis
            do kAO = 1, Nbasis
              do lAO = 1, Nbasis
                ijkl_index = ijkl_index + 1
                jkl_index = jkl_index + 1
                ajkl_index = a_index + jkl_index
                int_temp(ajkl_index) = int_temp(ajkl_index) + Cia*intAO(ijkl_index)
              end do ! lAO
            end do ! kAO
          end do ! jAO
        end do ! iAO
      end do ! ANO
!
! Second step <ab|kl> = \sum_{j} C_{jb} [ak|jl]  (notice the switch in notation)
!
      intNO = 0.0D0
      do Biter = 1, NB
        bNO = Blist(Biter)
        b_index = Nbasis**2*(bNO-1)
        do Aiter = 1, NA
          aNO = Alist(Aiter)
          a_index = Nbasis**3*(aNO-1)
          kjl_index = 0
          do kAO = 1, Nbasis
            do jAO = 1, Nbasis
              Cjb = Coeff(jAO,bNO)
              do lAO = 1, Nbasis
                kjl_index = kjl_index + 1
                akjl_index = a_index + kjl_index
                abkl_index = a_index + b_index + Nbasis*(kAO-1) + lAO
                intNO(abkl_index) = intNO(abkl_index) + Cjb*int_temp(akjl_index)
              end do ! lAO
            end do ! jAO
          end do ! kAO
        end do ! ANO
      end do ! BNO
!
! Third step <ab|cl> = \sum_{k} C_{kc} <ab|kl>
!
      int_temp = 0.0D0
      do Citer = 1,NC
        cNO = Clist(Citer)
        c_index = Nbasis*(cNO-1)
        do Aiter = 1, NA
          aNO = Alist(Aiter)
          a_index = Nbasis**3*(aNO-1)
          do Biter = 1,NB
            bNO = Blist(Biter)
            b_index = Nbasis**2*(bNO-1)
            kl_index = 0
            do kAO = 1, Nbasis
              Ckc = Coeff(kAO,cNO)
              do lAO = 1, Nbasis
                kl_index = kl_index + 1
                abkl_index = a_index + b_index + kl_index
                abcl_index = a_index + b_index + c_index + lAO
                int_temp(abcl_index) = int_temp(abcl_index) + Ckc*intNO(abkl_index)
              end do ! lAO
            end do ! kAO
          end do ! BNO
        end do ! ANO
      end do ! CNO
!
! Fourth step <ab|cd> = \sum_{l} C_{ld} <ab|cl>
!
      intNO = 0.0D0
      do Diter = 1,ND
        dNO = Dlist(Diter)
        do Aiter = 1, NA
          aNO = Alist(Aiter)
          a_index = Nbasis**3*(aNO-1)
          do Biter = 1,NB
            bNO = Blist(Biter)
            b_index = Nbasis**2*(bNO-1)
            do Citer = 1,NC
              cNO = Clist(Citer)
              c_index = Nbasis*(cNO-1)
              abcd_index = a_index + b_index + c_index + dNO
              do lAO = 1, Nbasis
                abcl_index = a_index + b_index + c_index + lAO
                intNO(abcd_index) = intNO(abcd_index) + Coeff(lAO,dNO)*int_temp(abcl_index)
              end do ! lAO
            end do ! kAO
          end do ! BNO
        end do ! ANO
      end do ! CNO
!
!      write(UNIout,'(/a)')'TEI transformation complete'
! Get timing
!      call PRG_CPU_time
!      call CNV_cpu (lstcpu, hcpu, mcpu, scpu, ccpu, secpu)
!      call PRG_date
!      call PRG_time
!
!      Time_elapsed = Time_current-Time_begin
!      if(Time_elapsed.lt.0.0D0)then ! It's a new day
!        Time_elapsed=Time_elapsed+86400
!      end if
!
!      call CNV_cpu (Time_elapsed, hcpu, mcpu, scpu, ccpu, setim)
!
!      write(UNIout,'(/2a)')'Cpu     time:',secpu
!      write(UNIout,'(2a/)')'Elapsed time:',setim
!
! End of routine CFT_gen_TEItrans
      call PRG_manager ('exit', 'CFT_gen_TEItrans', 'UTILITY')
      end subroutine CFT_gen_TEItrans
      subroutine CFT_gen_BLD_M(Alist,NA,Blist,NB,intNO,M,Nbasis)
!***********************************************************************
!     Date last modified: July 9, 2019                                 *
!     Author: JWH                                                      *
!     Description: Store momentum-balance integrals in 2D.             *
!***********************************************************************
! Modules:
!
      implicit none
!
! Input scalars:
      integer, intent(IN) :: Nbasis, NA, NB
!
! Input arrays:
      double precision, intent(IN) :: intNO(Nbasis**4)
      integer, intent(IN) :: Alist(NA), Blist(NB)
!
! Output arrays:
      double precision, intent(OUT) :: M(Nbasis,Nbasis)
!
! Local scalars:
      integer :: aNO, bNO, Aiter, Biter, aabb_index
! 
! Local functions:
      integer :: TEIindex
!
! Begin:
      call PRG_manager ('enter', 'CFT_gen_BLD_M', 'UTILITY')
!
      M = 0.0D0
      do Aiter = 1, NA
        aNO = Alist(Aiter)
        do Biter = 1, NB
          bNO = Blist(Biter)
          aabb_index = TEIindex(Nbasis,aNO,aNO,bNO,bNO)
!
          M(aNO,bNO) = intNO(aabb_index)
          M(bNO,aNO) = M(aNO,bNO)
!
        end do ! aNO
      end do ! bNO
!
! End of routine CFT_gen_BLD_M
      call PRG_manager ('exit', 'CFT_gen_BLD_M', 'UTILITY')
      end subroutine CFT_gen_BLD_M
      subroutine CFT_gen_BLD_K(Alist,NA,Blist,NB,intNO,K,Nbasis)
!***********************************************************************
!     Date last modified: March 30, 2016                               *
!     Author: JWH                                                      *
!     Description: Store exchange integrals in 2D.                     *
!***********************************************************************
! Modules:
!
      implicit none
!
! Input scalars:
      integer, intent(IN) :: Nbasis, NA, NB
!
! Input arrays:
      double precision, intent(IN) :: intNO(Nbasis**4)
      integer, intent(IN) :: Alist(NA), Blist(NB)
!
! Output arrays:
      double precision, intent(OUT) :: K(Nbasis,Nbasis)
!
! Local scalars:
      integer :: aNO, bNO, Aiter, Biter, abba_index
! 
! Local functions:
      integer :: TEIindex
!
! Begin:
      call PRG_manager ('enter', 'CFT_gen_BLD_K', 'UTILITY')
!
      K = 0.0D0
      do Aiter = 1, NA
        aNO = Alist(Aiter)
        do Biter = 1, NB
          bNO = Blist(Biter)
          abba_index = TEIindex(Nbasis,aNO,bNO,bNO,aNO)
!
          K(aNO,bNO) = intNO(abba_index)
          K(bNO,aNO) = K(aNO,bNO)
!
        end do ! aNO
      end do ! bNO
!
! End of routine CFT_gen_BLD_K
      call PRG_manager ('exit', 'CFT_gen_BLD_K', 'UTILITY')
      end subroutine CFT_gen_BLD_K
      subroutine CFT_gen_BLD_J(Alist,NA,Blist,NB,intNO,J,Nbasis)
!***********************************************************************
!     Date last modified: March 30, 2016                               *
!     Author: JWH                                                      *
!     Description: Store Coulomb integrals in 2D.                      *
!***********************************************************************
! Modules:
!
      implicit none
!
! Input scalars:
      integer, intent(IN) :: Nbasis, NA, NB
!
! Input arrays:
      double precision, intent(IN) :: intNO(Nbasis**4)
      integer, intent(IN) :: Alist(NA), Blist(NB)
!
! Output arrays:
      double precision, intent(OUT) :: J(Nbasis,Nbasis)
!
! Local scalars:
      integer :: aNO, bNO, Aiter, Biter, abab_index
!
! Local functions:
      integer :: TEIindex
!
! Begin:
      call PRG_manager ('enter', 'CFT_gen_BLD_J', 'UTILITY')
!
      do Aiter = 1, NA
        aNO = Alist(Aiter)
        do Biter = 1, NB
          bNO = Blist(Biter)
          abab_index = TEIindex(Nbasis,aNO,bNO,aNO,bNO)
!
          J(aNO,bNO) = intNO(abab_index)
          J(bNO,aNO) = J(aNO,bNO)
!
        end do ! aNO
      end do ! bNO
!
! End of routine CFT_gen_BLD_J
      call PRG_manager ('exit', 'CFT_gen_BLD_J', 'UTILITY')
      end subroutine CFT_gen_BLD_J
      subroutine CFT_BLD_ezxk
!***********************************************************************
!     Date last modified: July 16, 2020                                *
!     Author: JWH                                                      *
!     Description: Calculate etas, zetas, xis and kappas from Deltas   *
!                  and ONs.                                            *
!***********************************************************************
! Modules:
      USE CFT_objects
      USE type_basis_set
      USE INT_objects
      USE QM_objects
      USE program_constants

      implicit none
!
! Local scalars:
      integer :: rNO, sNO
      integer :: aNO, bNO, Aocc
      integer :: Icoup, Iopen
      integer :: Istat, Jstat, Pact, pNO, qNO
      integer :: Pvac, Qvac, Rocc, Rvac
      double precision :: kap
!
! Begin:
!
      call PRG_manager('enter','CFT_BLD_eta_zeta','UTILITY')
!
      if(allocated(eta))then
        deallocate(eta,zeta,xi,kappa)
      end if
      allocate(eta(Nbasis,Nbasis))
      allocate(zeta(Nbasis,Nbasis))
      allocate(xi(Nbasis,Nbasis))
      allocate(kappa(Nbasis,Nbasis))
      eta   = 0.0D0
      zeta  = 0.0D0
      xi    = 0.0D0
      kappa = 0.0D0
!
! Pair correction term, eta
! (not influenced by coupling)
!
      do Pact = 1, Nact
        pNO = NO_act(Pact)
!
        eta(pNO,pNO) = ON(pNO)*(1.0D0 - ON(pNO))
!
      end do ! Pact
!
      do Istat = 1, Nstat
        pNO = stat_pair(Istat,1)
        qNO = stat_pair(Istat,2)
!
        eta(pNO,qNO) = eta(pNO,qNO) + Delta(pNO,qNO)*(ON(qNO) - ON(pNO) - Delta(pNO,qNO))
        eta(qNO,pNO) = eta(qNO,pNO) + Delta(pNO,qNO)*(ON(qNO) - ON(pNO) - Delta(pNO,qNO))
!
        do Jstat = 1, Nstat
          if(Istat.eq.Jstat)cycle
          
          rNO = stat_pair(Jstat,1)
          sNO = stat_pair(Jstat,2)
!
          if(pNO.eq.rNO)then 
            eta(qNO,sNO) = eta(qNO,sNO) - Delta(pNO,qNO)*Delta(rNO,sNO)
          end if 
!
          if(qNO.eq.sNO)then
            eta(pNO,rNO) = eta(pNO,rNO) - Delta(pNO,qNO)*Delta(rNO,sNO)
          end if 
!
        end do ! Jstat
      end do ! Istat
!
! xi (appears as tau in JCP 2020)
!
      do Aocc = 1, Nocc_act
        aNO = occ_act(Aocc)
!
        if(coupNO(aNO).eq.0)then ! a not coupled
          do Pvac = 1, Nstat_occ(aNO)
            pNO = occ_stat(aNO,Pvac)
!
            xi(aNO,pNO) = sigma(aNO,pNO)*dsqrt(ON(aNO)*Delta(aNO,pNO))
            xi(pNO,aNO) = xi(aNO,pNO)
!
          end do ! Pvac
        else ! a is coupled
          bNO = coupNO(aNO)
!          do Pvac = 1, Nstat_occ(aNO)
!            pNO = occ_stat(aNO,Pvac)
          do Pvac = 1, Nvac_act
            pNO = vac_act(Pvac)
!
            xi(aNO,pNO) = &
            dsqrt((ON(aNO)*ON(bNO) + eta(aNO,bNO))*Delta(aNO,pNO)*(ON(bNO) + Delta(bNO,pNO)))

            do Qvac = 1, Nstat_occ(bNO)
              qNO = occ_stat(bNO,Qvac)
              if(qNO.eq.pNO)cycle
!
              xi(aNO,pNO) = xi(aNO,pNO)&
              + dsqrt((Delta(aNO,qNO)*Delta(bNO,pNO) + Delta(aNO,pNO)*Delta(bNO,qNO))&
                     *Delta(bNO,qNO)*(ON(aNO) + Delta(aNO,qNO)))
!
            end do ! Qvac
!
            xi(pNO,aNO) = xi(aNO,pNO)
!
          end do ! Pvac
!
        end if ! a coupled or not
!
      end do ! Aocc
!
!
! zeta
!
! vacant-vacant
!
      do Rocc = 1, Nocc_act
        rNO = occ_act(Rocc)
!
        do Pvac = 1, Nstat_occ(rNO)
          pNO = occ_stat(rNO,Pvac)
          do Qvac = 1, Nstat_occ(rNO)
            qNO = occ_stat(rNO,Qvac)
            if(pNO.eq.qNO)cycle 
!
            if(coupNO(rNO).eq.0)then ! r not coupled
!
              zeta(pNO,qNO) = zeta(pNO,qNO) + dsqrt(Delta(rNO,pNO)*Delta(rNO,qNO))&
                                             *sigma(rNO,pNO)*sigma(rNO,qNO)
!
            else ! coupled
!
              aNO = rNO
              bNO = coupNO(rNO)
!
              zeta(pNO,qNO) = zeta(pNO,qNO)&
           + dsqrt(Delta(aNO,pNO)*Delta(aNO,qNO)*(ON(bNO) + Delta(bNO,pNO))*(ON(bNO) + Delta(bNO,qNO)))
!
            end if ! coupled or not
!
          end do ! Qvac
        end do ! Pvac
      end do ! Rocc
!
      do Icoup = 1, Ncoup
        aNO = coup_pair(Icoup,1)
        bNO = coup_pair(Icoup,2)
!
        do Rvac = 1, Nvac_act
          rNO = vac_act(Rvac)
!
          do Pvac = 1, Nvac_act
            pNO = vac_act(Pvac)
            if(pNO.eq.rNO)cycle
            do Qvac = 1, Nvac_act
              qNO = vac_act(Qvac)
              if(qNO.eq.rNO)cycle
              if(qNO.eq.pNO)cycle
!
              zeta(pNO,qNO) = zeta(pNO,qNO)&
              + dsqrt((Delta(aNO,pNO)*Delta(bNO,rNO) + Delta(aNO,rNO)*Delta(bNO,pNO))&
                     *(Delta(aNO,qNO)*Delta(bNO,rNO) + Delta(aNO,rNO)*Delta(bNO,qNO)))
!
            end do ! Qvac 
          end do ! Pvac 
!
        end do ! Rvac
!
      end do ! Icoup
!
! occupied-occupied
!
      do Icoup = 1, Ncoup
        aNO = coup_pair(Icoup,1)
        bNO = coup_pair(Icoup,2)
!
        do Pvac = 1, Nvac_act
          pNO = vac_act(Pvac)
!
          zeta(aNO,bNO) = zeta(aNO,bNO)&
          + dsqrt(Delta(aNO,pNO)*Delta(bNO,pNO)*(ON(aNO) + Delta(aNO,pNO))*(ON(bNO) + Delta(bNO,pNO)))
          zeta(bNO,aNO) = zeta(bNO,aNO)&
          + dsqrt(Delta(aNO,pNO)*Delta(bNO,pNO)*(ON(aNO) + Delta(aNO,pNO))*(ON(bNO) + Delta(bNO,pNO)))
!
        end do ! Pvac
!
      end do ! Icoup 
!
! high-spin correction
! kappa
!
      if(LHSCon)then
!
      do Istat = 1, Nstat
        pNO = stat_pair(Istat,1)
        rNO = stat_pair(Istat,2)
!
        do Iopen = 1, Nopen ! open-shell
          aNO = NO_open(Iopen)
!
          !kap = 0.353553D0*ON(aNO)*xi(pNO,rNO)
          kap = 0.5D0*on(aNO)*Delta(pNO,rNO)
!
          kappa(aNO,pNO) = kap
          kappa(pNO,aNO) = kap
          kappa(aNO,rNO) = kap
          kappa(rNO,aNO) = kap
!
        end do ! Iopen
      end do ! Istat
!
      do Istat = 2, Nstat
        pNO = stat_pair(Istat,1)
        rNO = stat_pair(Istat,2)
!
        do Jstat = 1, Istat-1
          qNO = stat_pair(Jstat,1)
          sNO = stat_pair(Jstat,2)
          if(qNO.eq.pNO)cycle
          if(rNO.eq.sNO)cycle
!
! xi*xi - good for square H4, violates PQG
! D*D   - satisfies PQ, violates G
!
          kap = xi(pNO,rNO)*xi(qNO,sNO)
          !kap = Delta(pNO,rNO)*Delta(qNO,sNO)
!
          kappa(pNO,qNO) = kappa(pNO,qNO) + kap
          !kappa(pNO,qNO) = kappa(pNO,qNO) + on(pNO)*on(qNO)*kap
          kappa(qNO,pNO) = kappa(pNO,qNO)
!
          kappa(pNO,sNO) = kappa(pNO,sNO) + kap
          !kappa(pNO,sNO) = kappa(pNO,sNO) + on(pNO)*on(sNO)*kap
          kappa(sNO,pNO) = kappa(pNO,sNO)
!
          kappa(rNO,qNO) = kappa(rNO,qNO) + kap
          !kappa(rNO,qNO) = kappa(rNO,qNO) + on(rNO)*on(qNO)*kap
          kappa(qNO,rNO) = kappa(rNO,qNO)
!
          kappa(rNO,sNO) = kappa(rNO,sNO) + kap
          !kappa(rNO,sNO) = kappa(rNO,sNO) + on(rNO)*on(sNO)*kap
          kappa(sNO,rNO) = kappa(rNO,sNO)
!
        end do ! Jstat
      end do ! Istat
!
      end if
!
! End of routine CFT_BLD_ezxk
      call PRG_manager('exit','CFT_BLD_ezxk','UTILITY')
      end subroutine CFT_BLD_ezxk
      subroutine CFT_guess_NO
!***********************************************************************
!     Date last modified: January 20, 2017                             *
!     Author: JWH                                                      *
!     Description: Obtain guess NOs.                                   *
!***********************************************************************
! Modules:
      USE CFT_objects
      USE program_files
      USE QM_objects
      USE type_molecule
      USE INT_objects

      implicit none
!
! Local scalars:
      integer :: aNO, iAO, IERROR
!
! Local arrays:
      double precision, allocatable, dimension(:,:) :: S, S_NO
!
! Begin:
!
      call PRG_manager('enter','CFT_guess_NO','UTILITY')
!
      if(allocated(NO)) deallocate(NO)
      allocate(NO(Nbasis,Nbasis))
!
      if(NO_guess.eq.'READ')then
!
! Write data file names
        write(UNIout,'(/a,1x,a)')'NO input file name: ',NOfile
!
! Read NO coefficients
        NO = 0.0D0
        open(33, FILE=NOfile)
        do aNO = 1, Nbasis
          do iAO = 1, Nbasis
            read(33,*) NO(iAO,aNO)
          end do ! iAO
        end do ! aNO
        close(33)
!
! Re-orthonormalize guess NOs
        call GET_object ('INT', '1EINT', 'AO')
!
        allocate(S(Nbasis,Nbasis),S_NO(Nbasis,Nbasis))
        ! convert overlap to 2D matrix
        call UPT_to_SQS (OVRLAP, MATlen, S, Nbasis)
!
!        ! Transform overlap to NO basis
!        S_NO = matmul(transpose(NO),matmul(S,NO))
!
!        write(6,*)'Test re-orthonormalization (before)'
!        do aNO = 2, Nbasis
!          do iAO = 1, aNO
!            write(6,*)aNO,iAO,S_NO(aNO,iAO)
!          end do ! iAO
!        end do ! aNO
!
! orthonormalize
!
        call CFT_modGSortho_NO(NO,S,Nbasis)
!
!        ! Transform overlap to NO basis
!        S_NO = matmul(transpose(NO),matmul(S,NO))
!
!        write(6,*)'Test re-orthonormalization (after)'
!        do aNO = 2, Nbasis
!          do iAO = 1, aNO
!            write(6,*)aNO,iAO,S_NO(aNO,iAO)
!          end do ! iAO
!        end do ! aNO
!
! Create run name to avoid problems with files later
        call BLD_FileID
        I2I_BasicFileID(1:)=Basic_FileID(1:len_trim(Basic_FileID))
        RUN_name(1:)=I2I_BasicFileID(1:len_trim(I2I_BasicFileID))
!
! Determine number of alpha and beta electrons
!
        NAelectrons = (Nelectrons + Multiplicity - 1)/2
        NBelectrons = Nelectrons - NAelectrons
!
! Will not be defined otherwise
!        MATlen = Nbasis*(Nbasis+1)/2
!        write(6,*)'MATlen = ',MATlen 
! Set-up CMO in case we want to save NOs to it later for use somewhere else in MUNgauss
        if(.not.associated(CMO_RHF%coeff))then
          allocate(CMO_RHF%coeff(Nbasis,Nbasis), CMO_RHF%EIGVAL(Nbasis), CMO_RHF%occupancy(Nbasis), STAT=IERROR)
          if(IERROR.ne.0)then
            stop ' SCF_RHF> allocate failure: CMO...'
          end if
        else
          deallocate (CMO_RHF%coeff, CMO_RHF%EIGVAL, CMO_RHF%occupancy, STAT=IERROR)
          if(IERROR.ne.0)then
            stop ' SCF_RHF> deallocate failure: CMO...'
          end if
          allocate (CMO_RHF%coeff(Nbasis,Nbasis), CMO_RHF%EIGVAL(Nbasis), CMO_RHF%occupancy(Nbasis), STAT=IERROR)
          if(IERROR.ne.0)then
            stop ' SCF_RHF> Re-allocate failure: CMO...'
          end if
        end if
!
        CMO => CMO_RHF
!
      else ! use ROHF
!
        if(Multiplicity.eq.1)then
          write(UNIout,'(/a/)')'RHF MOs used as guess NOs'
!          call GET_object ('QM', 'CMO', 'RHF')
!          NO = CMO%coeff
! Use CFT code
          call GET_object ('CFT', 'NO', 'RHF')
          NO = CMO%coeff
          !call GET_object ('QM', 'LMO', 'BOYS')
          !NO = LMO%coeff
        else
!          write(UNIout,'(/a/)')'ROHF MOs used as guess NOs'
!          call GET_object ('QM', 'CMO', 'ROHF')
!          NO = CMO_ROHF%coeff
! Use CFT code
          write(UNIout,'(/a/)')'RHF MOs used as guess NOs'
          call GET_object ('CFT', 'NO', 'RHF')
          NO = CMO%coeff
        end if
!        
      end if
!
! End of routine CFT_guess_NO
      call PRG_manager('exit','CFT_guess_NO','UTILITY')
      end subroutine CFT_guess_NO
      subroutine CFT_guess_Delta
!***********************************************************************
!     Date last modified: January 20, 2017                             *
!     Author: JWH                                                      *
!     Description: Obtain guess Deltas.                                *
!***********************************************************************
! Modules:
      USE CFT_objects
      USE program_files
      USE QM_objects

      implicit none
!
! Local scalars:
      integer :: iNO, rNO, Istat
!
! Begin:
!
      call PRG_manager('enter','CFT_guess_Delta','UTILITY')
!
      if(allocated(Delta)) deallocate(Delta)
      allocate(Delta(Nbasis,Nbasis))
!
      if(Delta_guess.eq.'READ')then
!
! Write data file names
        write(UNIout,'(/a,1x,a)')'Delta input file name: ',Deltafile
!
        allocate(Qstat(Nbasis,Nbasis))
        Qstat = 0
! Read Deltas
        write(6,'(/a)')'Guess Deltas read from file'
        open(UNIT=33, FILE=Deltafile)
        read(33,'(I4)') Nstat
!        write(6,'(I4)') Nstat
        allocate(stat_pair(Nstat,2))
        do Istat = 1, Nstat
          read(33,'(I4,X,I4,X)',advance='NO') iNO, rNO
!          write(6,'(I4,X,I4,X)')iNO,rNO
          read(33,'(F18.10)') Delta(iNO,rNO)
!          write(6,'(F18.10)') Delta(iNO,rNO)
          Qstat(iNO,rNO) = 1
          stat_pair(Istat,1) = iNO
          stat_pair(Istat,2) = rNO
        end do ! Istat
        close(33)
!
      else
        ! Corresponds to RHF/ROHF occupancies
        Delta = 0.0D0
      end if
! 
! End of routine CFT_guess_Delta
      call PRG_manager('exit','CFT_guess_Delta','UTILITY')
      end subroutine CFT_guess_Delta
      subroutine CFT_BLD_Gmatrix
!***********************************************************************
!     Date last modified: January 20, 2017                             *
!     Author: JWH                                                      *
!     Description: Build G matrix using CFT density matrix.            *
!***********************************************************************
! Modules:
      USE CFT_objects
      USE QM_objects
      USE type_molecule
      USE INT_objects

      implicit none
!
! Local scalars:
      integer :: iAO, jAO, kAO, lAO, ijkl_index, ilkj_index
!
! Begin:
!
      call PRG_manager('enter','CFT_BLD_Gmatrix','UTILITY')
!
      if(allocated(Gcl_AO))then
        deallocate(Gcl_AO)
        deallocate(Gop_AO)
        deallocate(Goc_AO)
      end if
      allocate(Gcl_AO(Nbasis,Nbasis))
      allocate(Gop_AO(Nbasis,Nbasis))
      allocate(Goc_AO(Nbasis,Nbasis))
!
! Build density matrices (closed, open and total)
      call CFT_BLD_Pmatrix
!
! Get two-electron integrals (from Obara-Saika code)
      call GET_object ('CFT', '2EINT', 'OS')
!
! Contract ERIs onto G
!
      Gcl_AO = 0.0D0
      Gop_AO = 0.0D0
      Goc_AO = 0.0D0
      ijkl_index = 0
      do iAO = 1, Nbasis
        do jAO = 1, Nbasis
          do kAO = 1, Nbasis
            do lAO = 1, Nbasis
              ijkl_index = ijkl_index + 1
              !ilkj_index = TEIindex(Nbasis,iAO,lAO,kAO,jAO)
              ! Don't use function inside 4-index loop, slows down the code too much
              ilkj_index = Nbasis**3*(iAO-1) + Nbasis**2*(lAO-1) + Nbasis*(kAO-1) + jAO
!
              Gcl_AO(iAO,jAO) = Gcl_AO(iAO,jAO)&
                        + Pcl_NO(lAO,kAO)*(eriAO(ijkl_index) - 0.5D0*eriAO(ilkj_index))
!
              Gop_AO(iAO,jAO) = Gop_AO(iAO,jAO)&
                              + Pop_NO(lAO,kAO)*(eriAO(ijkl_index) - eriAO(ilkj_index))
!
              Goc_AO(iAO,jAO) = Goc_AO(iAO,jAO)&
                        + Pop_NO(lAO,kAO)*(eriAO(ijkl_index) - 0.5D0*eriAO(ilkj_index))
!
            end do ! lAO
          end do ! kAO
        end do ! jAO
      end do ! iAO
!
! End of routine CFT_BLD_Gmatrix
      call PRG_manager('exit','CFT_BLD_Gmatrix','UTILITY')
      end subroutine CFT_BLD_Gmatrix
      subroutine CFT_BLD_Pmatrix
!***********************************************************************
!     Date last modified: January 23, 2017                             *
!     Author: JWH                                                      *
!     Description: Build CFT density matrix.                           *
!***********************************************************************
! Modules:
      USE CFT_objects
      USE QM_objects
      USE type_molecule
      USE program_constants

      implicit none
!
! Local arrays:
      double precision, allocatable, dimension(:) :: ONtemp
!
! Begin:
!
      call PRG_manager('enter','CFT_BLD_Pmatrix','UTILITY')
!
      if(allocated(P_NO))then
        deallocate(Pcl_NO,P_NO)
        deallocate(Pop_NO)
      end if
      allocate(Pcl_NO(Nbasis,Nbasis),P_NO(Nbasis,Nbasis))
      allocate(Pop_NO(Nbasis,Nbasis))
      allocate(ONtemp(Nbasis))
!
! Get density matrix (CF occupation numbers go from 0 to 1, multiply by 2 for closed shell)
      ONtemp = ON
      ONtemp(NBelectrons+1:NAelectrons) = 0.0D0 ! Zero open-shell orbitals
!
      if(allocated(PM0_ROHF))then
        deallocate (PM0_ROHF)
      end if
      allocate(PM0_ROHF(MATlen))
      PM0_ROHF = ZERO
      PM0 => PM0_ROHF
!
      call DENBLD(NO,2.0D0*ONtemp,PM0,Nbasis,MATlen,Nbasis)
! Make it a square matrix
      call UPT_to_SQS(PM0,MATlen,Pcl_NO,Nbasis)
!
      ONtemp = ON
      ONtemp(1:NBelectrons) = 0.0D0 ! Zero closed-shell orbitals
      ONtemp(NAelectrons+1:Nbasis) = 0.0D0 ! Zero closed-shell orbitals
      call DENBLD(NO,ONtemp,PM0,Nbasis,MATlen,Nbasis)
! Make it a square matrix
      call UPT_to_SQS(PM0,MATlen,Pop_NO,Nbasis)
!
      ONtemp(1:NBelectrons) = 2.0D0*ON(1:NBelectrons) ! closed-shell
      ONtemp(NBelectrons+1:NAelectrons) = ON(NBelectrons+1:NAelectrons) ! open-shell
      ONtemp(NAelectrons+1:Nbasis) = 2.0D0*ON(NAelectrons+1:Nbasis) !  closed-shell
!
      call DENBLD(NO,ONtemp,PM0,Nbasis,MATlen,Nbasis)
! No need to square matricize the total P, we just use it for the one-electron density, PM0
!
! Build alpha and beta matrices for uses elsewhere
      if(.not.allocated(PM0_alpha))then
        allocate(PM0_alpha(MATlen),PM0_beta(MATlen))
      end if
      call DENBLD(NO,ON,PM0_alpha,Nbasis,MATlen,Nbasis)
      ONtemp = ON
      ONtemp(NBelectrons+1:NAelectrons) = 0.0D0
      call DENBLD(NO,ONtemp,PM0_beta,Nbasis,MATlen,Nbasis)
!
! End of routine CFT_BLD_Pmatrix
      call PRG_manager('exit','CFT_BLD_Pmatrix','UTILITY')
      end subroutine CFT_BLD_Pmatrix
      subroutine CFT_2D_transform(A,Coeff,B,Nbasis1,Nbasis2)
!***********************************************************************
!     Date last modified: April 4, 2016                                *
!     Author: JWH                                                      *
!     Description: Transform a 2D matrix to another basis.             *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer :: Nbasis1, Nbasis2
! Input arrays:
      double precision, intent(IN) :: A(Nbasis1,Nbasis1), Coeff(Nbasis1,Nbasis2)
! Output arrays:
      double precision, intent(OUT) :: B(Nbasis2,Nbasis2)
!
! Local array:
      double precision :: temp(Nbasis2,Nbasis1)
!
! Begin:
!
      call PRG_manager('enter','CFT_2D_transform','UTILITY')
!
      temp(1:Nbasis2,1:Nbasis1) = matmul(transpose(Coeff(1:Nbasis1,1:Nbasis2)),A(1:Nbasis1,1:Nbasis1))
      B(1:Nbasis2,1:Nbasis2) = matmul(temp(1:Nbasis2,1:Nbasis1),Coeff(1:Nbasis1,1:Nbasis2))
!
! End of routine CFT_2D_transform
      call PRG_manager('exit','CFT_2D_transform','UTILITY')
      end subroutine CFT_2D_transform
      subroutine CFT_BLD_energyOTEI
!***********************************************************************
!     Date last modified: January 20, 2017                             *
!     Author: JWH                                                      *
!     Description: Get 1e and 2e integrals required for energy and     *
!                  ON optimization.                                    *
!***********************************************************************
! Modules:
      USE CFT_objects
      USE QM_objects
      USE INT_objects

      implicit none
!
! Local scalars:
      integer :: NO_all(Nbasis), aNO, Nall
!
! Begin:
!
      call PRG_manager('enter','CFT_BLD_energyOTEI','UTILITY')
!
! OEIs
      call GET_object ('INT', '1EINT', 'AO')
      if(allocated(T_NO))then
        deallocate(T_NO,V_NO,h_NO)
      end if
      allocate(V_NO(Nbasis,Nbasis),T_NO(Nbasis,Nbasis),h_NO(Nbasis,Nbasis))
      call I1E_transform (Vint, NO, V_NO, Nbasis, Nbasis, MATlen)
      call I1E_transform (Tint, NO, T_NO, Nbasis, Nbasis, MATlen)
      call I1E_transform (Hcore, NO, h_NO, Nbasis, Nbasis, MATlen)
!
! TEIs
      call CFT_BLD_Gmatrix
!
      if(allocated(Gcl_NO))then
        deallocate(Gcl_NO,Gop_NO,Goc_NO)
      end if
      allocate(Gcl_NO(Nbasis,Nbasis),Gop_NO(Nbasis,Nbasis),Goc_NO(Nbasis,Nbasis))
      call CFT_2D_transform(Gcl_AO, NO, Gcl_NO, Nbasis, Nbasis)
      call CFT_2D_transform(Gop_AO, NO, Gop_NO, Nbasis, Nbasis)
      call CFT_2D_transform(Goc_AO, NO, Goc_NO, Nbasis, Nbasis)
!
      if(allocated(eriNO))then
        deallocate(eriNO)
      end if
      allocate(eriNO(Nbasis**4))
!
      call CFT_BLD_occvac_list
!
! Create list for all orbitals
      do aNO = 1, Nbasis
        NO_all(aNO) = aNO
      end do
      Nall = Nbasis
!
      call CFT_gen_TEItrans(NO_occ,Nocc,NO_all,Nall,NO_all,Nall,NO_all,Nall,&
                             NO,Nbasis,eriAO,eriNO)
!
! CFT energy wipes out integrals needed for optimization (once algo is settled we can modify)
!      call CFT_gen_TEItrans(NO_occ,Nocc,NO_occ,Nocc,NO_occ,Nocc,NO_occ,Nocc,&
!                             NO,Nbasis,eriAO,eriNO)
      !call CFT_gen_TEItrans(NO_act,Nact,NO_act,Nact,NO_act,Nact,NO_act,Nact,&
      !                       NO,Nbasis,eriAO,eriNO)
!
      if(allocated(J_NO))then
       deallocate(J_NO)
      end if
      if(allocated(K_NO))then
        deallocate(K_NO)
      end if
      allocate(J_NO(Nbasis,Nbasis),K_NO(Nbasis,Nbasis))
!
! HSC now includes open shell electrons
      call CFT_gen_BLD_J(NO_occ,Nocc,NO_occ,Nocc,eriNO,J_NO,Nbasis)
      call CFT_gen_BLD_K(NO_occ,Nocc,NO_occ,Nocc,eriNO,K_NO,Nbasis)
!
! End of routine CFT_BLD_energyOTEI
      call PRG_manager('exit','CFT_BLD_energyOTEI','UTILITY')
      end subroutine CFT_BLD_energyOTEI
      subroutine CFT_NO_opt_diag
!***********************************************************************
!     Date last modified: April 4, 2016                                *
!     Author: JWH                                                      *
!     Description: Optimize the NOs via iterative diagonalization of   *
!                  the pseudo-Fock matrix.                             *
!***********************************************************************
! Modules:
      USE CFT_objects
      USE QM_objects
      USE program_files
      USE program_timing
      USE program_constants

      implicit none
!
! Local scalars:
      integer :: NOstep, aNO
      double precision :: Eprev, Time_elapsed, VNN
!
! Local arrays:
      double precision, allocatable, dimension(:,:) :: QNO
!
! Local functions: 
      double precision :: GET_Enuclear
!
! Begin:
!
      call PRG_manager('enter','CFT_NO_opt_diag','UTILITY')
!
      VNN = GET_Enuclear()
!
      allocate(QNO(Nbasis,Nbasis))
!
! NO's by iterative diagonalization
! Dynamic functional seems to effect SCF wildly (just use E_HF and E_stat for now)
!
! Set intial values of loop conditions
      NOstep = 0
      Eprev = E_CFT_HF + E_CFT_stat + E_CFT_pair + VNN + 2.0D0*epsE
!
      write(UNIout,'(a)')'------------------------------------------------------------------'
      write(UNIout,'(a)')'              Diagonalization of Pseudo-Fock matrix               '
      write(UNIout,'(a)')'------------------------------------------------------------------'
      write(UNIout,'(a)')' NO step        E_HF + E_stat          Delta E            damp    '
      write(UNIout,'(a)')'------------------------------------------------------------------'
!
! Build orthonormality Lagrange multiplier matrix
        call CFT_BLD_lambda
        max_lambda = maxval(dabs(lambda-transpose(lambda)))
!
!        do while((dabs(Eprev-E_CFT).gt.epsE).and.(NOstep.lt.max_NOstep))
      do while((dabs(Eprev-(E_CFT_HF+E_CFT_stat+E_CFT_pair+VNN)).gt.epsE).and.(NOstep.lt.max_NOstep))
!      do while((max_lambda.gt.epslambda).and.(NOstep.lt.max_NOstep))
!
!       Eprev = E_CFT
        Eprev = E_CFT_HF+E_CFT_stat+E_CFT_pair+VNN
!
! On first step, diagonalize Hermitized lambda matrix for initial NOs and Fock diagonals
!
        if((NOstep.eq.0).and.(NOONstep.eq.0))then
          QNO = 0.5D0*(lambda + transpose(lambda))
          if(allocated(FockNO))then
            deallocate(FockNO)
          end if
          allocate(FockNO(Nbasis,Nbasis))
          FockNO = QNO
        else
          call CFT_BLD_FockNO ! Just builds off-diagonals
!
          QNO = FockNO*damp/maxval(dabs(FockNO)) ! damp diagonals
!
          ! Previous diagonals used for new diagonals (See Piris and Ugalde, J Comp Chem)
          do aNO = 1, Nbasis
            QNO(aNO,aNO) = PFeigval(aNO)
          end do ! aNO
!
        end if !  i>1
!
! Diagonalize
        call diagonalize_matrix(Nbasis, QNO, PFeigval)
!
        NO = matmul(NO, QNO)
!
! Save some time, this ignores contribution of Edyn to convergence of diagonalization
        if(dynamic.eq.'CS')then
          dynamic = 'NONE'
          call CFT_energy
          dynamic = 'CS'
        else if(dynamic.eq.'OF')then
          dynamic = 'NONE'
          call CFT_energy
          dynamic = 'OF'
        else
          call CFT_energy
        end if
!
        NOstep = NOstep + 1
!
        call CFT_BLD_lambda
        call CFT_BLD_FockNO ! Just builds off-diagonals
        max_lambda = maxval(dabs(FockNO))
!
!          write(UNIout,'(X,I4,2X,F20.12,2X,F20.12,4X,F10.8)')NOstep,E_CFT,E_CFT-Eprev,zeta
        write(UNIout,'(X,I4,2X,F20.12,2X,F20.12,6X,F10.8)')&
        &NOstep,E_CFT_HF+E_CFT_stat+E_CFT_pair+VNN,E_CFT_HF+E_CFT_stat+E_CFT_pair+VNN-Eprev,damp
!
! Reduce damping factor
        if(mod(NOstep,10).eq.0)then
          damp = damp/10.0D0
        end if
!
      end do ! NO iterative diagonalization
!
      write(UNIout,'(/a)')'Iterative diagonalization complete.'
      write(UNIout,'(I3,a)')NOstep,' steps taken.'
!
      if(NOstep.eq.1)then
        Ldiagstall = .true.
      end if
!
! Get timing
      call PRG_CPU_time
      call CNV_cpu (lstcpu, hcpu, mcpu, scpu, ccpu, secpu)
      call PRG_date
      call PRG_time
!
      Time_elapsed = Time_current-Time_begin
      if(Time_elapsed.lt.0.0D0)then ! It's a new day
        Time_elapsed=Time_elapsed+86400
      end if
!
      call CNV_cpu (Time_elapsed, hcpu, mcpu, scpu, ccpu, setim)
!
      write(UNIout,'(/2a)')'Cpu     time:',secpu
      write(UNIout,'(2a/)')'Elapsed time:',setim
!
! End of routine CFT_NO_opt_diag
      call PRG_manager('exit','CFT_NO_opt_diag','UTILITY')
      end subroutine CFT_NO_opt_diag
      subroutine CFT_pair_pot
!***********************************************************************
!     Date last modified: April 25, 2017                               *
!     Author: J.W. Hollett                                             *
!     Description: Calculate the chemical potential for each active    *
!                  occupied NO with each active vacant.                *
!***********************************************************************
! Modules:
      USE program_files
      USE CFT_objects
      USE QM_objects
      USE type_molecule
      USE program_constants

      implicit none
!
! Local scalars:
      integer :: iNO, rNO, Iocc, Rvac
      double precision :: Dir
      double precision, allocatable, dimension(:,:) :: pair_pot_occ
!
! Begin:
!
      call PRG_manager('enter','CFT_pair_pot','UTILITY')
!
! Need some energy integrals
! Temporarily set active orbitals to those that are near-degenerate (larger)
      if(allocated(NO_act))then
        deallocate(NO_act)
      end if
      allocate(NO_act(Nocc_val+Nvac_val))
!
      Nact = Nocc_val + Nvac_val
      NO_act(1:Nocc_val) = occ_val(1:Nocc_val)
      NO_act(Nocc_val+1:Nact) = vac_val(1:Nvac_val)
      call CFT_BLD_energyOTEI
!
! Get derivatives of energy with respect to each occupation number
!
      if(allocated(pair_pot))then
        deallocate(pair_pot)
      end if
      allocate(pair_pot(Nbasis,Nbasis),pair_pot_occ(Nbasis,Nbasis))
!
      pair_pot = ZERO
!
      do Rvac = 1, Nvac_val
        rNO = vac_val(Rvac)
!
        pair_pot(1:Nbasis,rNO) = TWO*(h_NO(rNO,rNO) + Gcl_NO(rNO,rNO) + Goc_NO(rNO,rNO))&
                               + (ONE - TWO*ON(rNO))*J_NO(rNO,rNO)
!
        do Iocc = 1, Nocc_val
          iNO = occ_val(Iocc)
!
          if(Delta(iNO,rNO).lt.5.0D-09)then
            Dir = 5.0D-9
          else
            Dir = Delta(iNO,rNO)
          end if
!
          pair_pot(iNO,rNO) = pair_pot(iNO,rNO)&
                            - ON(iNO)*(FOUR*J_NO(iNO,rNO) - TWO*K_NO(iNO,rNO))&
                            - dsqrt(ON(iNO)/Dir)*K_NO(iNO,rNO)
!
        end do ! Iocc
!
      end do ! Rvac
!
! Pair potential of occupied orbital (for convergence checking)
!
      do Iocc = 1, Nocc_val
        iNO = occ_val(Iocc)
!
        pair_pot_occ(iNO,1:Nbasis) = TWO*(h_NO(iNO,iNO) + Gcl_NO(iNO,iNO) + Goc_NO(iNO,iNO))&
                                   + (ONE - TWO*ON(iNO))*J_NO(iNO,iNO)
!
        do Rvac = 1, Nvac_val
          rNO = vac_val(Rvac)
!
          if(Delta(iNO,rNO).lt.5.0D-09)then
            Dir = 5.0D-9
          else
            Dir = Delta(iNO,rNO)
          end if
!
          pair_pot_occ(iNO,rNO) = pair_pot_occ(iNO,rNO)&
                                - Dir*(FOUR*J_NO(iNO,rNO) - TWO*K_NO(iNO,rNO))&
                                - dsqrt(Dir/ON(iNO))*K_NO(iNO,rNO)
!
        end do ! Rvac
!
      end do ! Iocc
!
! dynamic cumulant part
      select case(dynamic)
!
        case('OF')
!
        case('NONE')
!
      end select ! dynamic
!
! Print 
!
      write(UNIout,'(/a)')'         Pair potentials for each active occupied       '
      write(UNIout,'(a)')'-------------------------------------------------------------'
      write(UNIout,'(a)')'           pair                                          '
      write(UNIout,'(a)')'-------------------------------------------------------------'
      write(UNIout,'(a)')'       i          r            mu_r           mu_i       '
      write(UNIout,'(a)')'-------------------------------------------------------------'
 ! 
      do Iocc = 1, Nocc_val
        iNO = occ_val(Iocc)
        do Rvac = 1, Nvac_val
          rNO = vac_val(Rvac)
          write(UNIout,'(2X,I6,5X,I6,6X,F14.8,4X,F14.8)')iNO,rNO,pair_pot(iNO,rNO),pair_pot_occ(iNO,rNO)
        end do
      end do
      write(UNIout,'(a)')'                '
!
! End of routine CFT_pair_pot
      call PRG_manager('exit','CFT_pair_pot','UTILITY')
      end subroutine CFT_pair_pot
      subroutine CFT_RHF_SCF
!***********************************************************************
!     Date last modified: July 16, 2019                                *
!     Author: J.W. Hollett                                             *
!     Description: Determine the RHF orbtials.                         *
!                  Or, determine NOs when Deltas are all zero.         *
!***********************************************************************
! Modules:
      USE program_files
      USE CFT_objects
      USE QM_objects
      USE INT_objects
      USE type_molecule
      USE program_constants

      implicit none
!
! Local scalars:
      double precision :: conv, VNN
      integer :: n_diis, i_SCF, max_diis, IERROR
!      
! Local arrays:
      double precision, allocatable, dimension(:,:) :: error, error_diis, F_diis, hAO, X, S
! 
! Functions:
      double precision :: trace_matrix, GET_Enuclear
!
! Begin:
!
      call PRG_manager('enter','CFT_RHF_SCF','NO%RHF')
!
      max_diis = 10
!
! initialize arrays
      allocate(error(Nbasis,Nbasis),error_diis(Nbasis**2,max_diis),F_diis(Nbasis**2,max_diis))
      allocate(S(Nbasis,Nbasis),X(Nbasis,Nbasis),hAO(Nbasis,Nbasis))
!
      if(allocated(FockNO))then
        deallocate(FockNO) 
      end if 
      allocate(FockNO(Nbasis,Nbasis))
!
!
      if(allocated(PFeigval))then
        deallocate(PFeigval) 
      end if 
      allocate(PFeigval(Nbasis))
!
      VNN = GET_Enuclear()
!
! Get one-electron integrals 
      call GET_object ('INT', '1EINT', 'AO')
! Get Orthogonalization matrix X = S^{-1/2}
      ! convert overlap to 2D matrix
      call UPT_to_SQS (OVRLAP, MATlen, S, Nbasis)
!
      ! Lowdin orthogonalization by default
      call orthogonalization_matrix(1, Nbasis, S, X)
!
! Use Hcore as a guess
      call UPT_to_SQS (Hcore, MATlen, hAO, Nbasis)
      FockNO = hAO 
      NO = matmul(transpose(X),matmul(FockNO,X))
      call diagonalize_matrix(Nbasis,NO,PFeigval)
      NO = matmul(X,NO)
!
! Determine number of alpha and beta electrons
!
      NAelectrons = (Nelectrons + Multiplicity - 1)/2
      NBelectrons = Nelectrons - NAelectrons
!
! Set occupancies (closed shell)
      allocate(ON(Nbasis)) 
      ON(1:NAelectrons) = ONE 
      ON(NAelectrons+1:Nbasis) = ZERO
!  
! Now build P and G matrices
      call CFT_BLD_Gmatrix
!
! Set-up DIIS
      n_diis = 0
      F_diis = ZERO
      error_diis = ZERO
      conv = 66.0D0
      i_SCF = 0
!
      write(UNIout,'(a)')'---------------------------------------------------'
      write(UNIout,'(a)')'               RHF SCF for guess NOs              '
      write(UNIout,'(a)')'---------------------------------------------------'
      write(UNIout,'(a)')' iteration         E_RHF            DIIS error'
      write(UNIout,'(a)')'---------------------------------------------------'
!
! Begin SCF
      do while ((conv.gt.SCFconv).and.(i_SCF.le.maxSCF))
!
        i_SCF = i_SCF + 1
!
! Build Fock matrix
        FockNO = hAO + Gcl_AO
!
! Check convergence
        error = matmul(FockNO,matmul(Pcl_NO,S)) - matmul(matmul(S,Pcl_NO),FockNO)
        conv = maxval(dabs(error))
!
! DIIS
        n_diis = min(n_diis+1,max_diis)
        call DIIS_extrapolation(Nbasis**2,Nbasis**2,n_diis,error_diis,F_diis,error,FockNO)
!
! Get new coefficients
        NO = matmul(transpose(X),matmul(FockNO,X))
        call diagonalize_matrix(Nbasis,NO,PFeigval)
        NO = matmul(X,NO)
!
! Now build P and G matrices
        call CFT_BLD_Gmatrix
!
! Calculate energy
        E_CFT_HF = trace_matrix(Nbasis,matmul(Pcl_NO,hAO))&
                 + F1_2*trace_matrix(Nbasis,matmul(Pcl_NO,Gcl_AO))
!
! Print
        write(UNIout,'(X,I4,4X,F20.12,5X,F12.8)')&
        &i_SCF,E_CFT_HF+VNN,conv
!
      end do
!
      write(UNIout,'(a)')'---------------------------------------------------'
      write(UNIout,'(a,F20.12)')'RHF energy: ',E_CFT_HF+VNN
      write(UNIout,'(a)')''
!
! Set-up CMO in case we want to save NOs to it later for use somewhere else in MUNgauss
      if(.not.associated(CMO_RHF%coeff))then
        allocate(CMO_RHF%coeff(Nbasis,Nbasis), CMO_RHF%EIGVAL(Nbasis), CMO_RHF%occupancy(Nbasis), STAT=IERROR)
        if(IERROR.ne.0)then
          stop ' SCF_RHF> allocate failure: CMO...'
        end if
      else
        deallocate (CMO_RHF%coeff, CMO_RHF%EIGVAL, CMO_RHF%occupancy, STAT=IERROR)
        if(IERROR.ne.0)then
          stop ' SCF_RHF> deallocate failure: CMO...'
        end if
        allocate (CMO_RHF%coeff(Nbasis,Nbasis), CMO_RHF%EIGVAL(Nbasis), CMO_RHF%occupancy(Nbasis), STAT=IERROR)
        if(IERROR.ne.0)then
          stop ' SCF_RHF> Re-allocate failure: CMO...'
        end if
      end if
!
      CMO => CMO_RHF
!
      deallocate(PFeigval)
!
      CMO%coeff = NO
!
! End of routine CFT_RHF_SCF
      call PRG_manager('exit','CFT_RHF_SCF','NO%RHF')
      end subroutine CFT_RHF_SCF
      subroutine CFT_NO_opt_EPUM
!***********************************************************************
!     Date last modified: February 13, 2020                            *
!     Author: JWH                                                      *
!     Description: Optimize the NOs using exponential parameterization *
!                  of a unitary matrix and Newton-Raphson steps.       *
!***********************************************************************
! Modules:
      USE CFT_objects
      USE QM_objects
      USE program_files
      USE program_timing
      USE program_constants

      implicit none
!
! Local scalars:
      integer :: NOstep, cNO, dNO, Ipair, n_diis, max_diis
      double precision :: Time_elapsed, grad_length, rcond, Eprev, VNN
      double precision :: NOtol
!
! Local arrays:
      double precision, allocatable, dimension(:,:) :: U, Kskew, kap_diis, wgrad_diis
      double precision, allocatable, dimension(:) :: kap, kapn
!
! Local functions:
      double precision :: GET_Enuclear
!
! Begin:
!
      call PRG_manager('enter','CFT_NO_opt_EPUM','UTILITY')
!
! Set this in CFT defaults later 
      NOtol = 1.0D-06
!
! Get orbital pair lists
      call CFT_BLD_EPUM_orbpr
!
      allocate(kap(Npair),kapn(Npair))
      allocate(U(Nbasis,Nbasis))
      allocate(Kskew(Nbasis,Nbasis))
      kap = zero
!
! Set intial values of loop conditions
      NOstep = 0
      VNN = GET_Enuclear()
! Calculate the gradient
      call CFT_BLD_EPUM_w
! Get gradient length
      grad_length = dot_product(wgrad,wgrad)
      grad_length = dsqrt(grad_length) 
!
      write(UNIout,'(a)')'------------------------------------------------------------------'
      write(UNIout,'(a)')'              orbital optimization via EPUM                       '
      write(UNIout,'(a)')'------------------------------------------------------------------'
      write(UNIout,'(a)')' NO step        E_DeltaNO              Delta E        grad length '
      write(UNIout,'(a)')'------------------------------------------------------------------'
!
! Set up DIIS
!
      max_diis = 15
      allocate(kap_diis(Npair,max_diis),wgrad_diis(Npair,max_diis))
      n_diis = 0
      kap_diis = zero
      wgrad_diis = zero
!
      NOinit = NO
!
      do while((grad_length.gt.NOtol).and.(NOstep.lt.max_NOstep))
!
        Eprev = E_CFT_HF + E_CFT_stat + E_CFT_pair + VNN
!
! Calculate the Hessian
        call CFT_BLD_EPUM_A
!
! Calculate NR step
!
        ! Solve w = -A k for k
        call linear_solve(Npair,Ahess,wgrad,kapn,rcond)
!
        kap = kap - kapn
!
! Perform DIIS extrapolation
        n_diis = min(n_diis+1,max_diis)
        call DIIS_extrapolation(Npair,Npair,n_diis,wgrad_diis,kap_diis,wgrad,kap)
!
! Construct U
        ! First build K, which is a skew matrix comprised of kappa
        Kskew = zero
        do Ipair= 1, Npair
          cNO = orbpr(Ipair,1)
          dNO = orbpr(Ipair,2)
          Kskew(cNO,dNO) =  kap(Ipair)
          Kskew(dNO,cNO) = -kap(Ipair)
        end do ! Ipair
!
! U = e^K, use first three terms of expansion
        ! zeroth term
        U = zero
        do cNO = 1, Nbasis
          U(cNO,cNO) = one
        end do
!
      ! first term
        U = U + Kskew
!
      ! second term
        U = U + 0.5D0*matmul(Kskew,Kskew)
!
! Orthonormalize U
        call CFT_EPUM_modGSortho(U,Nbasis)
!
! Take step
        NO = matmul(NOinit,U)
!
        NOstep = NOstep + 1
!
! Save some time, this ignores contribution of Edyn to convergence of diagonalization
        if(dynamic.eq.'CS')then
          dynamic = 'NONE'
          call CFT_energy
          dynamic = 'CS'
        else if(dynamic.eq.'OF')then
          dynamic = 'NONE'
          call CFT_energy
          dynamic = 'OF'
        else
          call CFT_energy
        end if
!
! Calculate the gradient
        call CFT_BLD_EPUM_w
! Get gradient length
        grad_length = dot_product(wgrad,wgrad)
        grad_length = dsqrt(grad_length) 
!
        write(UNIout,'(X,I4,2X,F20.12,2X,F20.12,6X,F10.8)')&
        &NOstep,E_CFT_HF+E_CFT_stat+E_CFT_pair+VNN,E_CFT_HF+E_CFT_stat+E_CFT_pair+VNN-Eprev,grad_length
!
      end do ! EPUM optimization of NOs
!
      write(UNIout,'(/a)')'EPUM optimization complete.'
      write(UNIout,'(I3,a)')NOstep,' steps taken.'
!
! Get timing
      call PRG_CPU_time
      call CNV_cpu (lstcpu, hcpu, mcpu, scpu, ccpu, secpu)
      call PRG_date
      call PRG_time
!
      Time_elapsed = Time_current-Time_begin
      if(Time_elapsed.lt.0.0D0)then ! It's a new day
        Time_elapsed=Time_elapsed+86400
      end if
!
      call CNV_cpu (Time_elapsed, hcpu, mcpu, scpu, ccpu, setim)
!
      write(UNIout,'(/2a)')'Cpu     time:',secpu
      write(UNIout,'(2a/)')'Elapsed time:',setim
!
! End of routine CFT_NO_opt_EPUM
      call PRG_manager('exit','CFT_NO_opt_EPUM','NO%RHF')
      end subroutine CFT_NO_opt_EPUM
      subroutine CFT_BLD_EPUM_orbpr
!***********************************************************************
!     Date last modified: July 18, 2019                                *
!     Author: J.W. Hollett                                             *
!     Description: Create lists of orbital pairs of each type.         *
!***********************************************************************
! Modules:
      USE program_constants
      USE QM_objects
      USE CFT_objects
      USE type_molecule

      implicit none
!
! Local scalars:
      integer :: cNO, dNO, Ipair, Iact, Iocc, Ivac, Jact, Jvac, Nstart
!
! Begin:
!
      call PRG_manager('enter','CFT_BLD_EPUM_orbpr','UTILITY')
!
! Define a function to identify open-shell orbitals
      if(allocated(orbopen))then
        deallocate(orbopen)
      end if
      allocate(orbopen(Nbasis))
      orbopen = 0.0D0
      orbopen(NBelectrons+1:NAelectrons) = 1.0D0
!
! closed-open
      Npair_co = Nocc_froz*(NAelectrons-NBelectrons)
      if(allocated(orbpr_co))then
        deallocate(orbpr_co)
      end if
      allocate(orbpr_co(Npair_co,2))
!
      Ipair = 0
      do Iocc = 1, Nocc_froz
        cNO = occ_froz(Iocc)
        do dNO = NBelectrons+1, NAelectrons
          Ipair = Ipair + 1
          orbpr_co(Ipair,1) = cNO
          orbpr_co(Ipair,2) = dNO
        end do
      end do
!
! Print
!      write(6,*)'closed-open pairs'
!      do Ipair = 1, Npair_co 
!        write(6,'(2X,I4,2X,I4)') orbpr_co(Ipair,1), orbpr_co(Ipair,2)
!      end do
!
! closed-vacant
      Npair_cv = Nocc_froz*Nvac_froz
      if(allocated(orbpr_cv))then
        deallocate(orbpr_cv)
      end if
      allocate(orbpr_cv(Npair_cv,2))
!
      Ipair = 0
      do Iocc = 1, Nocc_froz
        cNO = occ_froz(Iocc)
        do Jvac = 1, Nvac_froz
          dNO = vac_froz(Jvac)
          Ipair = Ipair + 1
          orbpr_cv(Ipair,1) = cNO
          orbpr_cv(Ipair,2) = dNO
        end do
      end do
!
! Print
!      write(6,*)'closed-vacant pairs'
!      do Ipair = 1, Npair_cv 
!        write(6,'(2X,I4,2X,I4)') orbpr_cv(Ipair,1), orbpr_cv(Ipair,2)
!      end do
!
! closed-active
      Npair_ca = Nocc_froz*Nact
      if(allocated(orbpr_ca))then
        deallocate(orbpr_ca)
      end if
      allocate(orbpr_ca(Npair_ca,2))
!
      Ipair = 0
      do Iocc = 1, Nocc_froz
        cNO = occ_froz(Iocc)
        do Jact = 1, Nact
          dNO = NO_act(Jact)
          Ipair = Ipair + 1
          orbpr_ca(Ipair,1) = cNO
          orbpr_ca(Ipair,2) = dNO
        end do
      end do
!
! Print
!      write(6,*)'closed-active pairs'
!      do Ipair = 1, Npair_ca 
!        write(6,'(2X,I4,2X,I4)') orbpr_ca(Ipair,1), orbpr_ca(Ipair,2)
!      end do
!
! open-vacant
      Npair_ov = (NAelectrons-NBelectrons)*Nvac_froz
      if(allocated(orbpr_ov))then
        deallocate(orbpr_ov)
      end if
      allocate(orbpr_ov(Npair_ov,2))
!
      Ipair = 0
      do cNO = NBelectrons+1, NAelectrons
        do Jvac = 1, Nvac_froz
          dNO = vac_froz(Jvac)
          Ipair = Ipair + 1
          orbpr_ov(Ipair,1) = cNO
          orbpr_ov(Ipair,2) = dNO
        end do
      end do
!
! Print
!      write(6,*)'open-vacant pairs'
!      do Ipair = 1, Npair_ov 
!        write(6,'(2X,I4,2X,I4)') orbpr_ov(Ipair,1), orbpr_ov(Ipair,2)
!      end do
!
! open-active
      Npair_oa = (NAelectrons-NBelectrons)*Nact
      if(allocated(orbpr_oa))then
        deallocate(orbpr_oa)
      end if
      allocate(orbpr_oa(Npair_oa,2))
!
      Ipair = 0
      do cNO = NBelectrons+1, NAelectrons
        do Jact = 1, Nact
          dNO = NO_act(Jact)
          Ipair = Ipair + 1
          orbpr_oa(Ipair,1) = cNO
          orbpr_oa(Ipair,2) = dNO
        end do
      end do
!
! Print
!      write(6,*)'open-active pairs'
!      do Ipair = 1, Npair_oa 
!        write(6,'(2X,I4,2X,I4)') orbpr_oa(Ipair,1), orbpr_oa(Ipair,2)
!      end do
!
! vacant-active
      Npair_va = Nvac_froz*Nact
      if(allocated(orbpr_va))then
        deallocate(orbpr_va)
      end if
      allocate(orbpr_va(Npair_va,2))
!
      Ipair = 0
      do Ivac = 1, Nvac_froz
        cNO = vac_froz(Ivac)
        do Jact = 1, Nact
          dNO = NO_act(Jact)
          Ipair = Ipair + 1
          orbpr_va(Ipair,1) = cNO
          orbpr_va(Ipair,2) = dNO
        end do
      end do
!
! Print
!      write(6,*)'vacant-active pairs'
!      do Ipair = 1, Npair_va 
!        write(6,'(2X,I4,2X,I4)') orbpr_va(Ipair,1), orbpr_va(Ipair,2)
!      end do
!
! active-active
      Npair_aa = Nact*(Nact-1)/2
      if(allocated(orbpr_aa))then
        deallocate(orbpr_aa)
      end if
      allocate(orbpr_aa(Npair_aa,2))
!
      Ipair = 0
      do Iact = 2, Nact
        cNO = NO_act(Iact)
        do Jact = 1, Iact-1
          dNO = NO_act(Jact)
          Ipair = Ipair + 1
          orbpr_aa(Ipair,1) = cNO
          orbpr_aa(Ipair,2) = dNO
        end do
      end do
!
! Print
!      write(6,*)'active-active pairs'
!      do Ipair = 1, Npair_aa 
!        write(6,'(2X,I4,2X,I4)') orbpr_aa(Ipair,1), orbpr_aa(Ipair,2)
!      end do
!
! Combine into single list
      Npair = Npair_co + Npair_cv + Npair_ca + Npair_ov + Npair_oa + Npair_va + Npair_aa
      if(allocated(orbpr))then
        deallocate(orbpr)
      end if
      allocate(orbpr(Npair,2))
!
      Nstart = 0
      orbpr(1:Npair_co,1:2) = orbpr_co(1:Npair_co,1:2)
!
      Nstart = Nstart + Npair_co
      orbpr(Nstart+1:Nstart+Npair_cv,1:2) = orbpr_cv(1:Npair_cv,1:2)
!
      Nstart = Nstart + Npair_cv
      orbpr(Nstart+1:Nstart+Npair_ca,1:2) = orbpr_ca(1:Npair_ca,1:2)
!
      Nstart = Nstart + Npair_ca
      orbpr(Nstart+1:Nstart+Npair_ov,1:2) = orbpr_ov(1:Npair_ov,1:2)
!
      Nstart = Nstart + Npair_ov
      orbpr(Nstart+1:Nstart+Npair_oa,1:2) = orbpr_oa(1:Npair_oa,1:2)
!
      Nstart = Nstart + Npair_oa
      orbpr(Nstart+1:Nstart+Npair_va,1:2) = orbpr_va(1:Npair_va,1:2)
!
      Nstart = Nstart + Npair_va
      orbpr(Nstart+1:Nstart+Npair_aa,1:2) = orbpr_aa(1:Npair_aa,1:2)
!
! Print
!      write(6,*)'Full pair list'
!      do Ipair = 1, Npair
!        write(6,'(2X,I4,2X,I4)') orbpr(Ipair,1), orbpr(Ipair,2)
!      end do
!
! End of routine CFT_BLD_EPUM_orbpr
      call PRG_manager('exit','CFT_BLD_EPUM_orbpr','UTILITY')
      end subroutine CFT_BLD_EPUM_orbpr
      subroutine CFT_BLD_EPUM_w
!***********************************************************************
!     Date last modified: July 17, 2019                                *
!     Author: J.W. Hollett                                             *
!     Description: Determine the gradient of the energy with respect   *
!                  to elements of the kappa matrix.                    *
!***********************************************************************
! Modules:
      USE program_constants
      USE QM_objects
      USE CFT_objects
      USE type_molecule

      implicit none
!
! Local scalars:
      integer :: cNO, dNO, Ipair
!
! Begin:
!
      call PRG_manager('enter','CFT_BLD_EPUM_w','UTILITY')
!
      call CFT_BLD_lambda
!
      if(allocated(wgrad))then
        deallocate(wgrad)
      end if
      allocate(wgrad(Npair))
!
      do Ipair = 1, Npair
        cNO = orbpr(Ipair,1)
        dNO = orbpr(Ipair,2)
        wgrad(Ipair) = TWO*(lambda(dNO,cNO) - lambda(cNO,dNO))
      end do ! Ipair
!
!      write(6,*)'w vector for EPUM'
!      do Ipair = 1, Npair
!        cNO = orbpr(Ipair,1)
!        dNO = orbpr(Ipair,2)
!        write(6,'(I3,X,I3,X,F13.10)')cNO,dNO,wgrad(Ipair)
!      end do
!
! End of routine CFT_BLD_EPUM_w
      call PRG_manager('exit','CFT_BLD_EPUM_w','UTILITY')
      end subroutine CFT_BLD_EPUM_w
      subroutine CFT_BLD_EPUM_A
!***********************************************************************
!     Date last modified: July 16, 2019                                *
!     Author: J.W. Hollett                                             *
!     Description: Determine the hessian of the energy with respect    *
!                  to elements of the kappa matrix.                    *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE QM_objects
      USE CFT_objects
      USE type_molecule

      implicit none
!
! Local scalars:
      integer :: pNO, qNO, rNO, sNO, Ipair, Jpair
      integer :: prqs_index, prsq_index, nJK
      integer :: qspr_index, qsrp_index
      integer :: psrq_index, qrsp_index
      double precision :: na_p, na_q, na_r, na_s
      double precision :: nb_p, nb_q, nb_r, nb_s
!
! Local arrays:
      double precision, allocatable, dimension(:) :: ONa, ONb
      double precision, allocatable, dimension(:,:) :: c
!
! Local functions:
      double precision :: Kronecker_delta
      integer :: TEIindex
!
! Begin:
!
      call PRG_manager('enter','CFT_BLD_EPUM_A','UTILITY')
!
      if(allocated(Ahess))then
        deallocate(Ahess)
      end if
      allocate(Ahess(Npair,Npair))
      Ahess = zero 
!
! Gradient code already built H and Gs
!
! set alpha and beta occupancies
      allocate(ONa(Nbasis),ONb(Nbasis))
      ONa = ON
      ONb = ON
      ! open orbitals 
      ONb(NBelectrons+1:NAelectrons) = zero
!
! one-electron part
!
      do Ipair = 1, Npair
        pNO = orbpr(Ipair,1)
        qNO = orbpr(Ipair,2)
        na_p = ONa(pNO)
        nb_p = ONb(pNO)
        na_q = ONa(qNO)
        nb_q = ONb(qNO)
        do Jpair = 1, Npair
          rNO = orbpr(Jpair,1)
          sNO = orbpr(Jpair,2)
          na_r = ONa(rNO)
          nb_r = ONb(rNO)
          na_s = ONa(sNO)
          nb_s = ONb(sNO)
!
            Ahess(Ipair,Jpair) = Ahess(Ipair,Jpair)&
      + Kronecker_delta(pNO,rNO)*(na_p + nb_p + na_r + nb_r - na_q - nb_q - na_s - nb_s)*h_NO(qNO,sNO)&
      + Kronecker_delta(qNO,sNO)*(na_q + nb_q + na_s + nb_s - na_p - nb_p - na_r - nb_r)*h_NO(pNO,rNO)&
      + Kronecker_delta(pNO,sNO)*(na_q + nb_q + na_r + nb_r - na_p - nb_p - na_s - nb_s)*h_NO(qNO,rNO)&
      + Kronecker_delta(qNO,rNO)*(na_p + nb_p + na_s + nb_s - na_q - nb_q - na_r - nb_r)*h_NO(pNO,sNO)
!
        end do ! Jpair
      end do ! Ipair
!
! two-electron part
! Do all pairs at once
!
! HF part first
!
      do Ipair = 1, Npair
        pNO = orbpr(Ipair,1)
        qNO = orbpr(Ipair,2)
        na_p = ONa(pNO)
        nb_p = ONb(pNO)
        na_q = ONa(qNO)
        nb_q = ONb(qNO)
        do Jpair = 1, Npair
          rNO = orbpr(Jpair,1)
          sNO = orbpr(Jpair,2)
          na_r = ONa(rNO)
          nb_r = ONb(rNO)
          na_s = ONa(sNO)
          nb_s = ONb(sNO)
!
          prqs_index = TEIindex(Nbasis,pNO,rNO,qNO,sNO)
          prsq_index = TEIindex(Nbasis,pNO,rNO,sNO,qNO)
          qspr_index = TEIindex(Nbasis,qNO,sNO,pNO,rNO)
          qsrp_index = TEIindex(Nbasis,qNO,sNO,rNO,pNO)
!
          psrq_index = TEIindex(Nbasis,pNO,sNO,rNO,qNO)
          qrsp_index = TEIindex(Nbasis,qNO,rNO,sNO,pNO)
!
          Ahess(Ipair,Jpair) = Ahess(Ipair,Jpair)&
      + Kronecker_delta(pNO,rNO)*((na_p + nb_p + na_r + nb_r - na_q - nb_q - na_s - nb_s)&
                                  *Gcl_NO(qNO,sNO)&
                                + (na_p + na_r - na_q - na_s)*Gop_NO(qNO,sNO)&
                                + (nb_p + nb_r - nb_q - nb_s)*(TWO*Goc_NO(qNO,sNO) - Gop_NO(qNO,sNO)))&
      + Kronecker_delta(qNO,sNO)*((na_q + nb_q + na_s + nb_s - na_p - nb_p - na_r - nb_r)&
                                  *Gcl_NO(pNO,rNO)&
                                + (na_q + na_s - na_p - na_r)*Gop_NO(pNO,rNO)&
                                + (nb_q + nb_s - nb_p - nb_r)*(TWO*Goc_NO(pNO,rNO) - Gop_NO(pNO,rNO)))&
      + Kronecker_delta(pNO,sNO)*((na_q + nb_q + na_r + nb_r - na_p - nb_p - na_s - nb_s)&
                                  *Gcl_NO(qNO,rNO)&
                                + (na_q + na_r - na_p - na_s)*Gop_NO(qNO,rNO)&
                                + (nb_q + nb_r - nb_p - nb_s)*(TWO*Goc_NO(qNO,rNO) - Gop_NO(qNO,rNO)))&
      + Kronecker_delta(qNO,rNO)*((na_p + nb_p + na_s + nb_s - na_q - nb_q - na_r - nb_r)&
                                  *Gcl_NO(pNO,sNO)&
                                + (na_p + na_s - na_q - na_r)*Gop_NO(pNO,sNO)&
                                + (nb_p + nb_s - nb_q - nb_r)*(TWO*Goc_NO(pNO,sNO) - Gop_NO(pNO,sNO)))&
!
!      + FOUR*((na_p - na_q)*(na_r - na_s) + (nb_p - nb_q)*(nb_r - nb_s))&
!                                 *(eriNO(prqs_index) - eriNO(prsq_index))&
!      + FOUR*((nb_p - nb_q)*(na_r - na_s) + (na_p - na_q)*(nb_r - nb_s))*eriNO(prqs_index)
! Modify to enable partial integral transformation
!      + FOUR*(na_p*na_r - na_p*na_s + nb_p*nb_r - nb_p*nb_s)*eriNO(prqs_index)&
!      + FOUR*(-na_q*na_r + na_q*na_s - nb_q*nb_r + nb_q*nb_s)*eriNO(qspr_index)&
!      - FOUR*(na_p*na_r - na_p*na_s + nb_p*nb_r - nb_p*nb_s)*eriNO(prsq_index)&
!      - FOUR*(-na_q*na_r + na_q*na_s - nb_q*nb_r + nb_q*nb_s)*eriNO(qsrp_index)&
!      + FOUR*(nb_p*na_r - nb_p*na_s + na_p*nb_r - na_p*nb_s)*eriNO(prqs_index)&
!      + FOUR*(-nb_q*na_r + nb_q*na_s - na_q*nb_r + na_q*nb_s)*eriNO(qspr_index)
!
      + TWO*(na_p*na_r - na_p*na_s + nb_p*nb_r - nb_p*nb_s)*(eriNO(prqs_index) - eriNO(prsq_index)&
                                                           + eriNO(prqs_index) - eriNO(psrq_index))&
      + TWO*(na_q*na_s - na_q*na_r + nb_q*nb_s - nb_q*nb_r)*(eriNO(qspr_index) - eriNO(qsrp_index)&
                                                           + eriNO(qspr_index) - eriNO(qrsp_index))&
      + FOUR*(na_p*nb_r - na_p*nb_s + nb_p*na_r - nb_p*na_s)*eriNO(prqs_index)&
      + FOUR*(na_q*nb_s - na_q*nb_r + nb_q*na_s - nb_q*na_r)*eriNO(qspr_index)
!
        end do ! Jpair 
      end do ! Ipair 
!
! Cumulant part
      allocate(c(Nbasis,Nbasis))
!
! pair correction
!
      nJK = 1 ! 2J - K
      call CFT_BLD_2ndderiv(eta,Nbasis,nJK)
!
! intra- and interorbital static correlation
!
      c = zeta - xi
      nJK = 2 ! K
      call CFT_BLD_2ndderiv(c,Nbasis,nJK)
!
! high-spin correction
      if(LHSCon)then
!
      nJK = 2 ! K
      call CFT_BLD_2ndderiv(-kappa,Nbasis,nJK)
!
      end if ! HSC
!
! End of routine CFT_BLD_EPUM_A
      call PRG_manager('exit','CFT_BLD_EPUM_A','UTILITY')
      end subroutine CFT_BLD_EPUM_A
      subroutine CFT_BLD_2ndderiv(c,Nbasis,nJK)
!***********************************************************************
!     Date last modified: February 7, 2020                             *
!     Author: J.W. Hollett                                             *
!     Description: Calculate contribution to the hessian using the     *
!                  general expression for a two-electron component.    *
!***********************************************************************
! Modules:
      USE program_constants
      USE CFT_objects

      implicit none
!
! Input scalars:
      integer, intent(IN) :: Nbasis, nJK
!
! Input arrays:
      double precision, intent(IN) :: c(Nbasis,Nbasis)
!
! Local scalars:
      integer :: pNO, qNO, rNO, sNO, Ipair, Jpair, aNO, Aact, Aocc
      integer :: prqs_index, aqar_index, apas_index, apar_index, aqas_index 
      integer :: prsq_index, aqra_index, apsa_index, apra_index, aqsa_index 
      integer :: qspr_index, qsrp_index
      integer :: psrq_index, qrsp_index
!
! Local functions:
      double precision :: Kronecker_delta
      integer :: TEIindex
!
! Begin:
!
      call PRG_manager('enter','CFT_BLD_2ndderiv','UTILITY')
!
      select case(nJK)
!
      case(1) ! 2Jab - Kab
!
      do Ipair = 1, Npair
        pNO = orbpr(Ipair,1)
        qNO = orbpr(Ipair,2)
        do Jpair = 1, Npair
          rNO = orbpr(Jpair,1)
          sNO = orbpr(Jpair,2)
!
          do Aact = 1, Nact
            aNO = NO_act(Aact)
!
            aqar_index = TEIindex(Nbasis,aNO,qNO,aNO,rNO)
            apas_index = TEIindex(Nbasis,aNO,pNO,aNO,sNO)
            apar_index = TEIindex(Nbasis,aNO,pNO,aNO,rNO)
            aqas_index = TEIindex(Nbasis,aNO,qNO,aNO,sNO)
!
            aqra_index = TEIindex(Nbasis,aNO,qNO,rNO,aNO)
            apsa_index = TEIindex(Nbasis,aNO,pNO,sNO,aNO)
            apra_index = TEIindex(Nbasis,aNO,pNO,rNO,aNO)
            aqsa_index = TEIindex(Nbasis,aNO,qNO,sNO,aNO)
!          
            Ahess(Ipair,Jpair) = Ahess(Ipair,Jpair)&
            + two*Kronecker_delta(pNO,rNO)*(c(aNO,pNO) + c(aNO,rNO) - c(aNO,qNO) - c(aNO,sNO))&
                                          *(two*eriNO(aqas_index) - eriNO(aqsa_index))&
            + two*Kronecker_delta(qNO,sNO)*(c(aNO,qNO) + c(aNO,sNO) - c(aNO,pNO) - c(aNO,rNO))&
                                          *(two*eriNO(apar_index) - eriNO(apra_index))&
            + two*Kronecker_delta(pNO,sNO)*(c(aNO,qNO) + c(aNO,rNO) - c(aNO,pNO) - c(aNO,sNO))&
                                          *(two*eriNO(aqar_index) - eriNO(aqra_index))&
            + two*Kronecker_delta(qNO,rNO)*(c(aNO,pNO) + c(aNO,sNO) - c(aNO,qNO) - c(aNO,rNO))&
                                          *(two*eriNO(apas_index) - eriNO(apsa_index))
          end do ! Aact
!
          prqs_index = TEIindex(Nbasis,pNO,rNO,qNO,sNO)
          prsq_index = TEIindex(Nbasis,pNO,rNO,sNO,qNO)
          qspr_index = TEIindex(Nbasis,qNO,sNO,pNO,rNO)
          qsrp_index = TEIindex(Nbasis,qNO,sNO,rNO,pNO)
          psrq_index = TEIindex(Nbasis,pNO,sNO,rNO,qNO)
          qrsp_index = TEIindex(Nbasis,qNO,rNO,sNO,pNO)
!
          Ahess(Ipair,Jpair) = Ahess(Ipair,Jpair)&
                             !+ 8.0D0*(c(pNO,rNO) + c(qNO,sNO) - c(pNO,sNO) - c(qNO,rNO))&
                             !       *(two*eriNO(prqs_index) - eriNO(prsq_index))
                             !+ 8.0D0*(c(pNO,rNO) - c(pNO,sNO))&
                             !       *(two*eriNO(prqs_index) - eriNO(prsq_index))&
                             !+ 8.0D0*(c(qNO,sNO) - c(qNO,rNO))&
                             !       *(two*eriNO(qspr_index) - eriNO(qsrp_index))
                             + 4.0D0*(c(pNO,rNO) - c(pNO,sNO))&
                                    *(two*eriNO(prqs_index) - eriNO(prsq_index))&
                             + 4.0D0*(c(qNO,sNO) - c(qNO,rNO))&
                                    *(two*eriNO(qspr_index) - eriNO(qsrp_index))&
                             + 4.0D0*(c(pNO,rNO) - c(pNO,sNO))&
                                    *(two*eriNO(prqs_index) - eriNO(psrq_index))&
                             + 4.0D0*(c(qNO,sNO) - c(qNO,rNO))&
                                    *(two*eriNO(qspr_index) - eriNO(qrsp_index))
        end do ! Jpair
      end do ! Ipair
!
      case(2) ! Kab
!
      do Ipair = 1, Npair
        pNO = orbpr(Ipair,1)
        qNO = orbpr(Ipair,2)
        do Jpair = 1, Npair
          rNO = orbpr(Jpair,1)
          sNO = orbpr(Jpair,2)
!
          do Aocc = 1, Nocc
            aNO = NO_occ(Aocc)
!
            aqra_index = TEIindex(Nbasis,aNO,qNO,rNO,aNO)
            apsa_index = TEIindex(Nbasis,aNO,pNO,sNO,aNO)
            apra_index = TEIindex(Nbasis,aNO,pNO,rNO,aNO)
            aqsa_index = TEIindex(Nbasis,aNO,qNO,sNO,aNO)
!          
            Ahess(Ipair,Jpair) = Ahess(Ipair,Jpair)&
            + two*Kronecker_delta(pNO,rNO)*(c(aNO,pNO) + c(aNO,rNO) - c(aNO,qNO) - c(aNO,sNO))&
                                          *eriNO(aqsa_index)&
            + two*Kronecker_delta(qNO,sNO)*(c(aNO,qNO) + c(aNO,sNO) - c(aNO,pNO) - c(aNO,rNO))&
                                          *eriNO(apra_index)&
            + two*Kronecker_delta(pNO,sNO)*(c(aNO,qNO) + c(aNO,rNO) - c(aNO,pNO) - c(aNO,sNO))&
                                          *eriNO(aqra_index)&
            + two*Kronecker_delta(qNO,rNO)*(c(aNO,pNO) + c(aNO,sNO) - c(aNO,qNO) - c(aNO,rNO))&
                                          *eriNO(apsa_index)
          end do ! Aact
!
          prsq_index = TEIindex(Nbasis,pNO,rNO,sNO,qNO)
          qsrp_index = TEIindex(Nbasis,qNO,sNO,rNO,pNO)
          psrq_index = TEIindex(Nbasis,pNO,sNO,rNO,qNO)
          qrsp_index = TEIindex(Nbasis,qNO,rNO,sNO,pNO)
!
          Ahess(Ipair,Jpair) = Ahess(Ipair,Jpair)&
                             !+ 8.0D0*(c(pNO,rNO) + c(qNO,sNO) - c(pNO,sNO) - c(qNO,rNO))&
                             !       *eriNO(prsq_index)
                             !+ 8.0D0*(c(pNO,rNO) - c(pNO,sNO))&
                             !       *eriNO(prsq_index)&
                             !+ 8.0D0*(c(qNO,sNO) - c(qNO,rNO))&
                             !       *eriNO(qsrp_index)
                             + 4.0D0*(c(pNO,rNO) - c(pNO,sNO))&
                                    *eriNO(prsq_index)&
                             + 4.0D0*(c(qNO,sNO) - c(qNO,rNO))&
                                    *eriNO(qsrp_index)&
                             + 4.0D0*(c(pNO,rNO) - c(pNO,sNO))&
                                    *eriNO(psrq_index)&
                             + 4.0D0*(c(qNO,sNO) - c(qNO,rNO))&
                                    *eriNO(qrsp_index)
        end do ! Jpair
      end do ! Ipair
!
      end select ! nJK
!
! End of routine CFT_BLD_2ndderiv
      call PRG_manager('exit','CFT_BLD_2ndderiv','UTILITY')
      end subroutine CFT_BLD_2ndderiv
      subroutine CFT_BLD_2ndderiv_4ind(G,Nbasis,nJK)
!***********************************************************************
!     Date last modified: August 18, 2020                              *
!     Author: J.W. Hollett                                             *
!     Description: Calculate contribution to the hessian using the     *
!                  general expression for a two-electron component.    *
!***********************************************************************
! Modules:
      USE program_constants
      USE CFT_objects

      implicit none
!
! Input scalars:
      integer, intent(IN) :: Nbasis, nJK
!
! Input arrays:
      double precision, intent(IN) :: G(Nbasis,Nbasis,Nbasis,Nbasis)
!
! Local scalars:
      integer :: pNO, qNO, rNO, sNO, Ipair, Jpair, aNO, Aact, bNO, Bact, cNO, Cact
      integer :: qsab_index, psab_index, qrab_index, prab_index, qasb_index, pasb_index
      integer :: qarb_index, parb_index, qabs_index, pabs_index, qabr_index, pabr_index
      integer :: qabc_index, sabc_index, rabc_index, pabc_index
!
! Local functions:
      double precision :: Kronecker_delta
      integer :: TEIindex
!
! Begin:
!
      call PRG_manager('enter','CFT_BLD_2ndderiv','UTILITY')
!
      select case(nJK)
!
! Need to use some TEI symmetry so that only active orbitals appear in first position
!
      case(1) ! <ab|cd>
!
      do Ipair = 1, Npair
        pNO = orbpr(Ipair,1)
        qNO = orbpr(Ipair,2)
        do Jpair = 1, Npair
          rNO = orbpr(Jpair,1)
          sNO = orbpr(Jpair,2)
!
          do Aact = 1, Nact
            aNO = NO_act(Aact)
            do Bact = 1, Nact
              bNO = NO_act(Bact)
!
              qsab_index = TEIindex(Nbasis,qNO,sNO,aNO,bNO)
              psab_index = TEIindex(Nbasis,pNO,sNO,aNO,bNO)
              qrab_index = TEIindex(Nbasis,qNO,rNO,aNO,bNO)
              prab_index = TEIindex(Nbasis,pNO,rNO,aNO,bNO)
              qasb_index = TEIindex(Nbasis,qNO,aNO,sNO,bNO)
              pasb_index = TEIindex(Nbasis,pNO,aNO,sNO,bNO)
              qarb_index = TEIindex(Nbasis,qNO,aNO,rNO,bNO)
              parb_index = TEIindex(Nbasis,pNO,aNO,rNO,bNO)
              qabs_index = TEIindex(Nbasis,qNO,aNO,bNO,sNO)
              pabs_index = TEIindex(Nbasis,pNO,aNO,bNO,sNO)
              qabr_index = TEIindex(Nbasis,qNO,aNO,bNO,rNO)
              pabr_index = TEIindex(Nbasis,pNO,aNO,bNO,rNO)
!          
              Ahess(Ipair,Jpair) = Ahess(Ipair,Jpair)&
              + 4.0D0*(G(pNO,rNO,aNO,bNO)*eriNO(qsab_index) - G(qNO,rNO,aNO,bNO)*eriNO(psab_index)&
                     - G(pNO,sNO,aNO,bNO)*eriNO(qrab_index) + G(qNO,sNO,aNO,bNO)*eriNO(prab_index)& 
                     + G(pNO,aNO,rNO,bNO)*eriNO(qasb_index) - G(qNO,aNO,rNO,bNO)*eriNO(pasb_index)&
                     - G(pNO,aNO,sNO,bNO)*eriNO(qarb_index) + G(qNO,aNO,sNO,bNO)*eriNO(parb_index)&
                     + G(pNO,aNO,bNO,rNO)*eriNO(qabs_index) - G(qNO,aNO,bNO,rNO)*eriNO(pabs_index)&
                     - G(pNO,aNO,bNO,sNO)*eriNO(qabr_index) + G(qNO,aNO,bNO,sNO)*eriNO(pabr_index))
!
              do Cact = 1, Nact
                cNO = NO_act(Cact)
!
                qabc_index = TEIindex(Nbasis,qNO,aNO,bNO,cNO)
                pabc_index = TEIindex(Nbasis,pNO,aNO,bNO,cNO)
                rabc_index = TEIindex(Nbasis,rNO,aNO,bNO,cNO)
                sabc_index = TEIindex(Nbasis,sNO,aNO,bNO,cNO)
!
                Ahess(Ipair,Jpair) = Ahess(Ipair,Jpair)&
                + 2.0D0*(&
             (Kronecker_delta(pNO,sNO)*G(rNO,aNO,bNO,cNO) - Kronecker_delta(pNO,rNO)*G(sNO,aNO,bNO,cNO))&
            *eriNO(qabc_index)&
           + (Kronecker_delta(qNO,rNO)*G(pNO,aNO,bNO,cNO) - Kronecker_delta(pNO,rNO)*G(qNO,aNO,bNO,cNO))&
            *eriNO(sabc_index)&
           + (Kronecker_delta(pNO,sNO)*G(qNO,aNO,bNO,cNO) - Kronecker_delta(qNO,sNO)*G(pNO,aNO,bNO,cNO))&
            *eriNO(rabc_index)&
           + (Kronecker_delta(qNO,rNO)*G(sNO,aNO,bNO,cNO) - Kronecker_delta(qNO,sNO)*G(rNO,aNO,bNO,cNO))&
            *eriNO(pabc_index))
!
              end do ! Cact
            end do ! Bact
          end do ! Aact
!
        end do ! Jpair
      end do ! Ipair
!
      end select ! nJK
!
! End of routine CFT_BLD_2ndderiv_4ind
      call PRG_manager('exit','CFT_BLD_2ndderiv_4ind','UTILITY')
      end subroutine CFT_BLD_2ndderiv_4ind
      subroutine CFT_EPUM_modGSortho(U,N)
!***********************************************************************
!     Date last modified: February 14, 2020                            *
!     Author: JWH                                                      *
!     Description: Ensure that U is unitary by applying modified Gram- *
!                  Schmidt procedure.                                  *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer, intent(in) :: N
!
! Input/Output arrays:
      double precision :: U(N,N)
!
! Local scalars:
      integer :: i,j
!
! Local arrays:
      double precision ::  ip(N,N), C(N,N)

! Begin:
!
      call PRG_manager('enter','CFT_EPUM_modGSortho','UTILITY')
!
! Get UtU
      ip = matmul(transpose(U),U)
!
! Orthonormalize
      C = U 
      do i = 1, N
        C(1:N,i) = C(1:N,i)/dsqrt(ip(i,i))
        do j = i+1, N
          ! remove projection of Ui onto Uj
          C(1:N,j) = C(1:N,j) - ip(i,j)*C(1:N,i)/dsqrt(ip(i,i))
        end do ! jNO
        ! update inner product (overlap)
        ip = matmul(transpose(C),C)
      end do ! iNO
!
      U = C
!
! End of routine CFT_EPUM_modGSortho
      call PRG_manager('exit','CFT_EPUM_modGSortho','UTILITY')
      end subroutine CFT_EPUM_modGSortho
      subroutine CFT_modGSortho_NO(U,S,N)
!***********************************************************************
!     Date last modified: October 13, 2020                             *
!     Author: JWH                                                      *
!     Description: Ensure that guess orbitals are orthonormal by       *
!                  by applying a modified Gram-Schmidt procedure.      *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer, intent(in) :: N
      double precision, intent(in) :: S(N,N)
!
! Input/Output arrays:
      double precision :: U(N,N)
!
! Local scalars:
      integer :: i,j
!
! Local arrays:
      double precision ::  C(N,N), ip(N,N)

! Begin:
!
      call PRG_manager('enter','CFT_modGSortho_NO','UTILITY')
!
      ip = matmul(transpose(U),matmul(S,U))
!
! Orthonormalize
      C = U 
      do i = 1, N
        C(1:N,i) = C(1:N,i)/dsqrt(ip(i,i))
        do j = i+1, N
          ! remove projection of Ui onto Uj
          C(1:N,j) = C(1:N,j) - ip(i,j)*C(1:N,i)/dsqrt(ip(i,i))
        end do ! jNO
        ! update inner product (overlap)
        ip = matmul(transpose(C),matmul(S,C))
      end do ! iNO
!
      U = C
!
! End of routine CFT_modGSortho_NO
      call PRG_manager('exit','CFT_modGSortho_NO','UTILITY')
      end subroutine CFT_modGSortho_NO
      subroutine CFT_2RDM_opt
!***********************************************************************
!     Date last modified: August 27, 2020                              *
!     Author: JWH                                                      *
!     Description: Optimize both the Deltas and orbitals using a       *
!                  Newton-Raphson algorithm.                           *
!***********************************************************************
! Modules:
      USE CFT_objects
      USE QM_objects
      USE program_files
      USE program_timing
      USE program_constants

      implicit none
!
! Local scalars:
      integer :: RDMstep, n_diis, max_diis, i, j
      double precision :: Time_elapsed, grad_length, rcond, Eprev, VNN, rho_lb, rho_ub
      double precision :: RDMtol, inc_fac, low_fac, Ediis, gpush
      double precision :: trad, trad_max, rho, E1, m1, eps, plength, pthresh
      logical :: trad_done
!
! Local arrays:
      double precision, allocatable, dimension(:,:) :: gradtot_diis, kapthe_diis
      double precision, allocatable, dimension(:,:) :: hesstot, Q, X, Ident, modhess, R
      double precision, allocatable, dimension(:) :: pkapthe, gradtot, eigval, work, kapthe0, qkapthe
      double precision, allocatable, dimension(:) :: kapthe1, pkapthe0, gprime, pprime
!
! Local functions:
      double precision :: GET_Enuclear
!
! Begin:
!
      call PRG_manager('enter','CFT_2RDM_opt','UTILITY')
!
! Set this in CFT defaults later 
      RDMtol   = 1.0D-06
      pthresh  = 1.0D-4
      trad_max = 2.0D0
!      trad_max = 0.1D0
      trad     = 0.05D0
!      trad     = 0.01D0
      rho_lb   = 0.25D0
      rho_ub   = 0.75D0
      inc_fac  = 2.0D0
      low_fac  = 0.5D0
!
! saddle nudge (for getting off saddle points)
      gpush = 0.0D-04
      if(Lnudge)then
        gpush    = 1.0D-04
      end if 
!
! Get orbital pair lists
      call CFT_BLD_EPUM_orbpr
!
      allocate(Q(Npair+Nstat,Npair+Nstat),eigval(Npair+Nstat))
      allocate(work(3*(Npair+Nstat)-1),X(Npair+Nstat,Npair+Nstat))
!
      if(allocated(kapthe)) deallocate(kapthe) 
      allocate(kapthe(Npair+Nstat),pkapthe(Npair+Nstat),pkapthe0(Npair+Nstat))
      allocate(kapthe0(Npair+Nstat),qkapthe(Npair+Nstat),kapthe1(Npair+Nstat))
      allocate(gradtot(Npair+Nstat)) 
      allocate(hesstot(Npair+Nstat,Npair+Nstat)) 
      allocate(tdf_hess(Npair+Nstat,Npair+Nstat),tdf_grad(Npair+Nstat)) 
      allocate(modhess(Npair+Nstat,Npair+Nstat)) 
      allocate(R(Npair+Nstat,Npair+Nstat)) 
      allocate(gprime(Nstat+Npair))
      allocate(pprime(Nstat+Npair))
      kapthe = zero
      tdf_hess = zero
      tdf_grad = zero
!
! Get thetas
      call CFT_Delta_to_theta
      kapthe(Npair+1:Npair+Nstat) = theta(1:Nstat)
!
! Initialize identity
      allocate(Ident(Npair+Nstat,Npair+Nstat)) 
      Ident = zero
      do I = 1, Npair+Nstat
        Ident(I,I) = 1.0D0
      end do ! I
!
! Set intial values of loop conditions
      RDMstep = 0
      VNN = GET_Enuclear()
!
! Calculate the orbital gradient
      call CFT_BLD_EPUM_w
! Calculate the Delta/theta gradient
      call CFT_Delta_gradient
!
! Calculate hessian too (need to initialize things for TDF)
! 
! Calculate the orbital hessian
      call CFT_BLD_EPUM_A
!
! Calculate the theta (Delta) hessian
      call CFT_Delta_hessian
!
! Calculate the Delta-orbital coupling
      call CFT_BLD_coupling_hessian
!
!
      if(dynamic(1:4).eq.'TDF')then
        call CFT_TDF_hess
      end if
!
! Combine
      gradtot(1:Npair) = wgrad(1:Npair)
      gradtot(Npair+1:Npair+Nstat) = grad_theta(1:Nstat)
      gradtot = gradtot + tdf_grad
!
! Get gradient length
      grad_length = dsqrt(dot_product(gradtot,gradtot))/dble(Nstat+Npair)
!
      write(UNIout,'(a)')'---------------------------------------------------------------------------------------'
      write(UNIout,'(a)')'                                  Optimization of 2-RDM                      '
      write(UNIout,'(a)')'---------------------------------------------------------------------------------------'
      write(UNIout,'(a)')'   step           E_DeltaNO              Delta E             grad length   trust radius'
      write(UNIout,'(a)')'---------------------------------------------------------------------------------------'
!
! Set up DIIS
!
      max_diis = 10
      allocate(kapthe_diis(Npair+Nstat,max_diis),gradtot_diis(Npair+Nstat,max_diis))
      n_diis = 0
      kapthe_diis = zero
      gradtot_diis = zero
!
      NOinit = NO
!
!      do while((grad_length.gt.RDMtol).and.(RDMstep.lt.max_RDMstep))
      do while((grad_length.gt.RDMtol).and.(RDMstep.lt.max_RDMstep).and.(trad.gt.1.0D-09))
!
        Eprev = E_CFT + VNN
!
! Calculate the orbital hessian
        call CFT_BLD_EPUM_A
!
! Calculate the theta (Delta) hessian
        call CFT_Delta_hessian
!
! Calculate the Delta-orbital coupling
        call CFT_BLD_coupling_hessian
!
! Combine hessians
        ! orbital-orbital
        hesstot(1:Npair,1:Npair) = Ahess(1:Npair,1:Npair)
        ! theta-theta
        hesstot(Npair+1:Npair+Nstat,Npair+1:Npair+Nstat)& 
        = hessian(1:Nstat,1:Nstat)
        ! orbital-theta
        hesstot(1:Npair,Npair+1:Npair+Nstat) = Acoup(1:Npair,1:Nstat)
        ! theta-orbital
        hesstot(Npair+1:Npair+Nstat,1:Npair) = transpose(Acoup(1:Npair,1:Nstat))
        ! TDF contribution
        hesstot = hesstot + tdf_hess
!
        Q = hesstot
        call diagonalize_matrix(Npair+Nstat, Q, eigval)
!
! Analyze hessian
!        write(6,*)'first two hessian eigenvalues: ',eigval(1),eigval(2)
        if(eigval(1).lt.0.0D0)then
!
! Perturb gradient to ensure escape of any saddle points
!
          ! transform gradient to normal coordinates
          gprime = matmul(transpose(Q),gradtot)
          ! augment gradient in normal coordinates
          do i = 1, Npair+Nstat
            if((eigval(i).lt.0.0D0).and.(dabs(gprime(i)).lt.gpush))then
              gprime(i) = dsign(gpush,gprime(i))
              !write(6,*)'pushing on a normal mode'
            end if
          end do
          ! transform back to regular coordinates
          gradtot = matmul(Q,gprime)
!
        end if
!
! Trust region method
!
        trad_done = .false.
        do while(.not.trad_done.and.(trad.gt.1.0D-09))
!
          if(minval(eigval).lt.0.0D0)then
            eps = -2.0D0*minval(eigval)
          else ! already positive definite
            eps = 0.0D0
          end if
!
! Modify the hessian
          modhess = hesstot + eps*Ident
! take a step
          call linear_solve(Npair+Nstat,modhess,-gradtot,pkapthe,rcond)
          plength = dsqrt(dot_product(pkapthe,pkapthe))
!
          if((plength-trad)/trad.gt.pthresh)then
!
            call solve_TR_subproblem(Npair+Nstat,hesstot,gradtot,trad,pthresh,eps,m1,pkapthe)
            plength = dsqrt(dot_product(pkapthe,pkapthe))
!
          end if ! actually need to solve subproblem
!
          kapthe0 = kapthe
          kapthe = kapthe + pkapthe
!
! Evaluate actual and estimated change in energy
          call CFT_kapthe_update
          call CFT_energy
          ! actual
          E1 = E_CFT + VNN
!
          ! estimate from quadratic model
          m1 = Eprev + dot_product(pkapthe,gradtot) + 0.5D0*dot_product(pkapthe,matmul(hesstot,pkapthe))
!
! Evaluate objective function
          rho = (Eprev - E1)/(Eprev - m1)
! 
! Determine how to proceed
!
          ! trust radius modifications
          if(rho.lt.rho_lb)then
            trad = low_fac*trad
          else
            if((rho.gt.rho_ub).and.(dabs(plength - trad)/trad.lt.pthresh))then
              trad = min(inc_fac*trad,trad_max)
            end if
          end if
!
          if(rho.ge.rho_lb)then ! perform a step 
            trad_done = .true.
          else
            !write(6,*)'no step made, kapthe reset'
            kapthe = kapthe0
          end if
!
          if(trad.lt.1.0D-9)then
!
             write(UNIout,*)' ****** Trust-region method failed ******'
             write(UNIout,*)' trust radius = ', trad
             write(UNIout,*)' Not sure why this happens (yet), check your gradient it might be OK'
!
          end if 
!
        end do ! trust-region method
!
! Use DIIS
!        n_diis = min(n_diis+1,max_diis)
!        call DIIS_extrapolation(Npair+Nstat,Npair+Nstat,n_diis,gradtot_diis,kapthe_diis,gradtot,kapthe)
!
! Get energy 
        call CFT_energy
!
! Calculate the gradient
        call CFT_BLD_EPUM_w
        call CFT_Delta_gradient
!
        if(dynamic.eq.'TDF')then
          call CFT_TDF_hess
        end if
!
! Combine
        gradtot(1:Npair) = wgrad(1:Npair)
        gradtot(Npair+1:Npair+Nstat) = grad_theta(1:Nstat)
        gradtot = gradtot + tdf_grad
!
! Get gradient length
        grad_length = dsqrt(dot_product(gradtot,gradtot))/dble(Nstat+Npair)
        !write(6,*)'theta grad = ',dsqrt(dot_product(grad_theta,grad_theta))/dble(Nstat+Npair)
        !write(6,*)'orb grad   = ',dsqrt(dot_product(wgrad,wgrad))/dble(Nstat+Npair)
!
        RDMstep = RDMstep + 1
!
        E1 = E_CFT + VNN
!
        write(UNIout,'(X,I4,4X,F20.12,2X,F20.12,4X,F16.8,4X,F10.6)')&
        &RDMstep,E1,E1-Eprev,grad_length,trad
        call flush(UNIout)
!
      end do ! combined 2RDM optimization
!
      write(UNIout,'(/a)')'2RDM optimization complete.'
      write(UNIout,'(I3,a)')RDMstep,' steps taken.'
!
! Check hessian eigenvalues
!
! Calculate the orbital hessian
      call CFT_BLD_EPUM_A
!
! Calculate the theta (Delta) hessian
      call CFT_Delta_hessian
!
! Calculate the Delta-orbital coupling
      call CFT_BLD_coupling_hessian
!
! Combine hessians
      ! orbital-orbital
      hesstot(1:Npair,1:Npair) = Ahess(1:Npair,1:Npair)
      ! theta-theta
      hesstot(Npair+1:Npair+Nstat,Npair+1:Npair+Nstat)& 
      = hessian(1:Nstat,1:Nstat)
      ! orbital-theta
      hesstot(1:Npair,Npair+1:Npair+Nstat) = Acoup(1:Npair,1:Nstat)
      ! theta-orbital
      hesstot(Npair+1:Npair+Nstat,1:Npair) = transpose(Acoup(1:Npair,1:Nstat))
!
      ! TDF contribution
      if(dynamic.eq.'TDF')then
        call CFT_TDF_hess
        hesstot = hesstot + tdf_hess
      end if
!
      call diagonalize_matrix(Npair+Nstat, hesstot, eigval)
!
      if(eigval(1).lt.0.0D0)then
        write(UNIout,'(a)')'Beware! There are negative hessian eigenvalues:'
        do i = 1, Npair+Nstat
          if(eigval(i).lt.0.0D0)then
            write(UNIout,'(F16.10)')eigval(i)
          end if
        end do 
      end if
!
      deallocate(tdf_hess,tdf_grad)
!
! Get timing
      call PRG_CPU_time
      call CNV_cpu (lstcpu, hcpu, mcpu, scpu, ccpu, secpu)
      call PRG_date
      call PRG_time
!
      Time_elapsed = Time_current-Time_begin
      if(Time_elapsed.lt.0.0D0)then ! It's a new day
        Time_elapsed=Time_elapsed+86400
      end if
!
      call CNV_cpu (Time_elapsed, hcpu, mcpu, scpu, ccpu, setim)
!
      write(UNIout,'(/2a)')'Cpu     time:',secpu
      write(UNIout,'(2a/)')'Elapsed time:',setim
!
! End of routine CFT_2RDM_opt
      call PRG_manager('exit','CFT_2RDM_opt','UTILITY')
      end subroutine CFT_2RDM_opt
      subroutine CFT_2RDM_deriv_test
!***********************************************************************
!     Date last modified: April 27, 2022                               *
!     Author: JWH                                                      *
!     Description: Test gradient and hessian numerically.              *
!***********************************************************************
! Modules:
      USE CFT_objects
      USE QM_objects
      USE program_files
      USE program_timing
      USE program_constants

      implicit none
!
! Local scalars:
      integer :: I, J
      double precision :: E0, kapthe_step
!
! Local arrays:
      double precision, allocatable, dimension(:,:) :: hesstot, numhess
      double precision, allocatable, dimension(:) :: gradtot, grad0
      double precision, allocatable, dimension(:) :: numgrad
!
! Local functions:
      double precision :: GET_Enuclear
!
! Begin:
!
      call PRG_manager('enter','CFT_2RDM_deriv_test','UTILITY')
!
! Get orbital pair lists
      call CFT_BLD_EPUM_orbpr
!
      if(allocated(kapthe)) deallocate(kapthe) 
      allocate(kapthe(Npair+Nstat))
      allocate(gradtot(Npair+Nstat),grad0(Npair+Nstat)) 
      allocate(hesstot(Npair+Nstat,Npair+Nstat)) 
      allocate(tdf_hess(Npair+Nstat,Npair+Nstat),tdf_grad(Npair+Nstat)) 
      kapthe = zero
      tdf_hess = zero
      tdf_grad = zero
!
      CMO%coeff = NO
      NOinit = NO
!
! Get thetas
      call CFT_Delta_to_theta
      kapthe(Npair+1:Npair+Nstat) = theta(1:Nstat)
!
! Calculate the orbital gradient
      call CFT_BLD_EPUM_w
! Calculate the Delta/theta gradient
      call CFT_Delta_gradient
!
! Calculate the orbital hessian
      call CFT_BLD_EPUM_A
!
! Calculate the theta (Delta) hessian
      call CFT_Delta_hessian
!
! Calculate the Delta-orbital coupling
      call CFT_BLD_coupling_hessian
!
! Calculate contribution from 2e-density functional
      if(dynamic(1:4).eq.'TDF')then
        call CFT_TDF_hess
      end if
!
! Combine
      gradtot(1:Npair) = wgrad(1:Npair)
      gradtot(Npair+1:Npair+Nstat) = grad_theta(1:Nstat)
      gradtot = gradtot + tdf_grad
!
! Now calculate numerically
      call CFT_energy
      E0 = E_CFT
!
      allocate(numgrad(Npair+Nstat))
      numgrad = zero
      kapthe_step = 0.00000001D0
      do I = 1, Npair+Nstat
        kapthe(I) = kapthe(I) + kapthe_step
        call CFT_kapthe_update
        call CFT_energy
        numgrad(I) = (E_CFT - E0)/kapthe_step
        kapthe(I) = kapthe(I) - kapthe_step
        call CFT_kapthe_update
      end do ! Ipair
!
! Print comparison
      write(6,'(/a)')'        Comparison of numerical and analytical gradient     '
      write(6,'(a)')'------------------------------------------------------------'
      write(6,'(a)')'       I               analytical           numerical     '
      write(6,'(a)')'------------------------------------------------------------'
      do I = 1, Npair+Nstat
        write(6,'(5x,I3,7x,F20.10,1x,F20.10)')I,gradtot(I),numgrad(I)
      end do ! I
!
! Combine hessians
      ! orbital-orbital
      hesstot(1:Npair,1:Npair) = Ahess(1:Npair,1:Npair)
      ! theta-theta
      hesstot(Npair+1:Npair+Nstat,Npair+1:Npair+Nstat)& 
      = hessian(1:Nstat,1:Nstat)
      ! orbital-theta
      hesstot(1:Npair,Npair+1:Npair+Nstat) = Acoup(1:Npair,1:Nstat)
      ! theta-orbital
      hesstot(Npair+1:Npair+Nstat,1:Npair) = transpose(Acoup(1:Npair,1:Nstat))
      ! TDF contribution
      hesstot = hesstot + tdf_hess
!
! Now calculate numerically
      grad0 = gradtot
!
      allocate(numhess(Npair+Nstat,Npair+Nstat))
      numhess = zero
      kapthe_step = 0.00000001D0
      do I = 1, Npair+Nstat
        do J = 1, Npair+Nstat
          kapthe(J) = kapthe(J) + kapthe_step
          call CFT_kapthe_update
          call CFT_energy
! Calculate the orbital gradient
          call CFT_BLD_EPUM_w
! Calculate the Delta/theta gradient
          call CFT_Delta_gradient
! Calculate contribution from 2e-density functional
          if(dynamic(1:4).eq.'TDF')then
            call CFT_TDF_hess
          end if
! Combine
          gradtot(1:Npair) = wgrad(1:Npair)
          gradtot(Npair+1:Npair+Nstat) = grad_theta(1:Nstat)
          gradtot = gradtot + tdf_grad
!
          numhess(I,J) = (gradtot(I) - grad0(I))/kapthe_step
          kapthe(J) = kapthe(J) - kapthe_step
          call CFT_kapthe_update
        end do ! J
      end do ! I
!
! Print comparison
      write(6,'(/a)')'        Comparison of numerical and analytical hessian      '
      write(6,'(a)')'-----------------------------------------------------------------'
      write(6,'(a)')'       I       J               analytical           numerical     '
      write(6,'(a)')'-----------------------------------------------------------------'
      do I = 1, Npair+Nstat
        do J = 1, Npair+Nstat
          write(6,'(5x,I3,5x,I3,7X,F20.10,1x,F20.10)')I,J,hesstot(I,J),numhess(I,J)
        end do ! J
      end do ! I
!
      deallocate(tdf_hess,tdf_grad)
!
! End of routine CFT_2RDM_deriv_test
      call PRG_manager('exit','CFT_2RDM_deriv_test','UTILITY')
      end subroutine CFT_2RDM_deriv_test
      subroutine CFT_BLD_coupling_hessian
!***********************************************************************
!     Date last modified: August 28, 2020                              *
!     Author: JWH                                                      *
!     Description: Construct the elements of the 2RDM hessian that     *
!                  couple the orbital and Delta optimization.          *
!***********************************************************************
! Modules:
      USE CFT_objects
      USE QM_objects
      USE INT_objects
      USE type_molecule
      USE program_constants
      USE program_files

      implicit none
!
! Local scalars:
      integer :: cNO, dNO, iNO, Cact, Iclosed, Iopen, Ipair, kNO, tNO, Istat
      integer :: Iact, Cocc, Iocc
      integer :: cidi_index, ciid_index, ctdt_index, cttd_index, ckdk_index, ckkd_index
!
! Local arrays:
      double precision, allocatable, dimension(:,:,:) :: dlambda
!
! Local functions: 
      integer :: TEIindex
      double precision :: Kronecker_delta
!
! Begin:
!
      call PRG_manager('enter','CFT_BLD_coupling_hessian','UTILITY')
!
      allocate(dlambda(Nbasis,Nbasis,Nstat))
!
      dlambda = zero
!
! Transforms have all been done for the other parts of the hessian
!
      do Istat = 1, Nstat
        kNO = stat_pair(Istat,1)
        tNO = stat_pair(Istat,2)
!
! HF-like part
        do Iclosed = 1, Nclosed ! closed-shell
          cNO = NO_closed(Iclosed)
          do dNO = 1, Nbasis
            ctdt_index = TEIindex(Nbasis,cNO,tNO,dNO,tNO)
            cttd_index = TEIindex(Nbasis,cNO,tNO,tNO,dNO)
            ckdk_index = TEIindex(Nbasis,cNO,kNO,dNO,kNO)
            ckkd_index = TEIindex(Nbasis,cNO,kNO,kNO,dNO)
!
            dlambda(cNO,dNO,Istat) = 2.0D0*(Kronecker_delta(cNO,tNO) - Kronecker_delta(cNO,kNO))&
                                          *(h_NO(cNO,dNO) + Gcl_NO(cNO,dNO) + Goc_NO(cNO,dNO))&
                                   + 2.0D0*on(cNO)*(2.0D0*(eriNO(ctdt_index) - eriNO(ckdk_index))&
                                                        - (eriNO(cttd_index) - eriNO(ckkd_index)))
!
          end do ! dNO
        end do ! cNO
        do Iopen = 1, Nopen ! open-shell
          cNO = NO_open(Iopen)
          do dNO = 1, Nbasis
            ctdt_index = TEIindex(Nbasis,cNO,tNO,dNO,tNO)
            cttd_index = TEIindex(Nbasis,cNO,tNO,tNO,dNO)
            ckdk_index = TEIindex(Nbasis,cNO,kNO,dNO,kNO)
            ckkd_index = TEIindex(Nbasis,cNO,kNO,kNO,dNO)
!
            dlambda(cNO,dNO,Istat) = (Kronecker_delta(cNO,tNO) - Kronecker_delta(cNO,kNO))&
                                    *(h_NO(cNO,dNO) + Gcl_NO(cNO,dNO) + Gop_NO(cNO,dNO))&
                                   + on(cNO)*(2.0D0*(eriNO(ctdt_index) - eriNO(ckdk_index))&
                                                  - (eriNO(cttd_index) - eriNO(ckkd_index)))
!
          end do ! dNO
        end do ! cNO
!
! pair correction and static terms
!
        do Cact = 1, Nact
          cNO = NO_act(Cact)
          do dNO = 1, Nbasis
!
            do Iact = 1, Nact
              iNO = NO_act(Iact)
              cidi_index = TEIindex(Nbasis,cNO,iNO,dNO,iNO)
              ciid_index = TEIindex(Nbasis,cNO,iNO,iNO,dNO)
!
              dlambda(cNO,dNO,Istat) = dlambda(cNO,dNO,Istat)&
              + 2.0D0*deta(cNO,iNO,Istat)*(2.0D0*eriNO(cidi_index) - eriNO(ciid_index))&
              + 2.0D0*(dzeta(cNO,iNO,Istat) - dxi(cNO,iNO,Istat))*eriNO(ciid_index)
!
            end do ! Iact
!
          end do ! dNO
        end do ! Cact
!
! high-spin correction
        if(LHSCon)then
!
        do Cocc = 1, Nocc
          cNO = NO_occ(Cocc)
          do dNO = 1, Nbasis
!
            do Iocc = 1, Nocc
              iNO = NO_occ(Iocc)
!
              ciid_index = TEIindex(Nbasis,cNO,iNO,iNO,dNO)
!
              dlambda(cNO,dNO,Istat) = dlambda(cNO,dNO,Istat)&
                                     - 2.0D0*dkappa(cNO,iNO,Istat)*eriNO(ciid_index)
!
            end do ! Iact
!
          end do ! dNO
        end do ! Cact
!
        end if ! HSC
!
      end do ! Istat
!
! Calculate contibrution to the hessian
!
      if(allocated(Acoup))then
        deallocate(Acoup)
      end if
      allocate(Acoup(Npair,Nstat))
!
! don't forget theta and chain rule
      do Istat = 1, Nstat
        do Ipair = 1, Npair
          cNO = orbpr(Ipair,1)
          dNO = orbpr(Ipair,2)
          Acoup(Ipair,Istat) = TWO*(dlambda(dNO,cNO,Istat) - dlambda(cNO,dNO,Istat))
          !Acoup(Ipair,Istat) = -Acoup(Ipair,Istat)*dsin(2.0D0*theta(Istat))
          Acoup(Ipair,Istat) = -0.5D0*Acoup(Ipair,Istat)*dsin(2.0D0*theta(Istat))
        end do ! Ipair
      end do ! Istat
!
! End of routine CFT_BLD_coupling_hessian
      call PRG_manager('exit','CFT_BLD_coupling_hessian','UTILITY')
      end subroutine CFT_BLD_coupling_hessian
      subroutine CFT_kapthe_update
!***********************************************************************
!     Date last modified: August 28, 2020                              *
!     Author: JWH                                                      *
!     Description: Update orbitals and occupancies after a 2-RDM       *
!                  optimization step.                                  *
!***********************************************************************
! Modules:
      USE CFT_objects
      USE QM_objects
      USE INT_objects
      USE type_molecule
      USE program_constants
      USE program_files

      implicit none
!
! Local scalars:
      integer :: cNO, dNO, Ipair 
!
! Local arrays:
      double precision, allocatable, dimension(:,:) :: Kskew, U
!
! Begin:
!
      call PRG_manager('enter','CFT_kapthe_update','UTILITY')
!
! EPUM objects
      allocate(U(Nbasis,Nbasis))
      allocate(Kskew(Nbasis,Nbasis))
!
! Orbital part first
!
! Construct U
        ! First build K, which is a skew matrix comprised of kappa
        Kskew = zero
        do Ipair= 1, Npair
          cNO = orbpr(Ipair,1)
          dNO = orbpr(Ipair,2)
          Kskew(cNO,dNO) =  kapthe(Ipair)
          Kskew(dNO,cNO) = -kapthe(Ipair)
        end do ! Ipair
!
! U = e^K, use first three terms of expansion
        ! zeroth term
        U = zero
        do cNO = 1, Nbasis
          U(cNO,cNO) = one
        end do
!
      ! first term
        U = U + Kskew
!
      ! second term
        U = U + 0.5D0*matmul(Kskew,Kskew)
!
! Orthonormalize U
        call CFT_EPUM_modGSortho(U,Nbasis)
!
! Transform orbitals
        NO = matmul(NOinit,U)
        ! keep CMO current
        CMO%coeff = NO
!
! Now Delta part
!
        theta(1:Nstat) = kapthe(Npair+1:Npair+Nstat)
!
! Update Deltas and ONs
        call CFT_theta_to_Delta
        call CFT_Delta_to_ON
        call CFT_Delta_to_theta ! May of needed to scale Deltas
        call CFT_BLD_ezxk
!
! End of routine CFT_kapthe_update
      call PRG_manager('exit','CFT_kapthe_update','UTILITY')
      end subroutine CFT_kapthe_update
      subroutine CFT_BLD_2RDM
!*****************************************************************
!     Date last modified: September 25, 2020                     *
!     Author: JWH                                                *
!     Description: Construct the 2RDM for analysis               *
!*****************************************************************
!Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE CFT_objects
      USE QM_objects
      USE INT_objects

      implicit none
!
! Local scalars: 
      integer :: aNO, bNO, Aact, Bact, Aclosed, Bclosed, Aopen, Bopen
      double precision :: E2e, Ne, negsum, eigsum
!
! Local arrays: 
      double precision, allocatable, dimension(:) :: eigval
      double precision, allocatable, dimension(:,:) :: S, S_NO, Gsup
      double precision, allocatable, dimension(:,:,:,:) :: Qaa, Qbb, Qab, Qba
      double precision, allocatable, dimension(:,:,:,:) :: Gaa, Gbb, Gab, Gba, Gaabb, Gbbaa
!
! Begin:
      call PRG_manager ('enter', 'CFT_BLD_2RDM', 'UTILITY')
!
! Construct 2-RDMs (spin-resolved)
!
      if(allocated(CFT2RDMaa))then
        deallocate(CFT2RDMaa,CFT2RDMbb,CFT2RDMab,CFT2RDMba) 
      end if
      allocate(CFT2RDMaa(Nbasis,Nbasis,Nbasis,Nbasis))
      allocate(CFT2RDMbb(Nbasis,Nbasis,Nbasis,Nbasis))
      allocate(CFT2RDMab(Nbasis,Nbasis,Nbasis,Nbasis))
      allocate(CFT2RDMba(Nbasis,Nbasis,Nbasis,Nbasis))
!
      CFT2RDMaa = zero
      CFT2RDMbb = zero
      CFT2RDMab = zero
      CFT2RDMba = zero
!
! Delta NO 2-RDM
!
      do Aclosed = 1, Nclosed
        aNO = NO_closed(Aclosed)
        do Bclosed = 1, Nclosed
          bNO = NO_closed(Bclosed)
! Coulomb
          if(aNO.ne.bNO)then
            CFT2RDMaa(aNO,bNO,aNO,bNO) = 0.5D0*(ON(aNO)*ON(bNO) + eta(aNO,bNO))
            CFT2RDMbb(aNO,bNO,aNO,bNO) = 0.5D0*(ON(aNO)*ON(bNO) + eta(aNO,bNO))
          end if
          CFT2RDMab(aNO,bNO,aNO,bNO) = 0.5D0*(ON(aNO)*ON(bNO) + eta(aNO,bNO))
          CFT2RDMba(aNO,bNO,aNO,bNO) = 0.5D0*(ON(aNO)*ON(bNO) + eta(aNO,bNO))
! Exchange
          if(aNO.ne.bNO)then
            CFT2RDMaa(aNO,bNO,bNO,aNO) = -0.5D0*(ON(aNO)*ON(bNO) + eta(aNO,bNO))
            CFT2RDMbb(aNO,bNO,bNO,aNO) = -0.5D0*(ON(aNO)*ON(bNO) + eta(aNO,bNO))
            CFT2RDMab(aNO,bNO,bNO,aNO) = -0.5D0*kappa(aNO,bNO)
            CFT2RDMba(aNO,bNO,bNO,aNO) = -0.5D0*kappa(aNO,bNO)
          end if
! Time-inversion exchange
          if(aNO.ne.bNO)then
            CFT2RDMab(aNO,aNO,bNO,bNO) = 0.5D0*(zeta(aNO,bNO) - xi(aNO,bNO))
            CFT2RDMba(aNO,aNO,bNO,bNO) = 0.5D0*(zeta(aNO,bNO) - xi(aNO,bNO))
          end if
!
        end do 
      end do 
!
      do Aopen = 1, Nopen
        aNO = NO_open(Aopen)
        do Bclosed = 1, Nclosed
          bNO = NO_closed(Bclosed)
! Coulomb
          CFT2RDMaa(aNO,bNO,aNO,bNO) = 0.5D0*ON(aNO)*ON(bNO)
          CFT2RDMaa(bNO,aNO,bNO,aNO) = 0.5D0*ON(aNO)*ON(bNO)
!
          CFT2RDMab(aNO,bNO,aNO,bNO) = 0.25D0*ON(aNO)*ON(bNO)
          CFT2RDMab(bNO,aNO,bNO,aNO) = 0.25D0*ON(aNO)*ON(bNO)
          CFT2RDMba(aNO,bNO,aNO,bNO) = 0.25D0*ON(aNO)*ON(bNO)
          CFT2RDMba(bNO,aNO,bNO,aNO) = 0.25D0*ON(aNO)*ON(bNO)
! Exchange
          CFT2RDMaa(aNO,bNO,bNO,aNO) = -0.5D0*ON(aNO)*ON(bNO)
          CFT2RDMaa(bNO,aNO,aNO,bNO) = -0.5D0*ON(aNO)*ON(bNO)
!
          CFT2RDMab(aNO,bNO,bNO,aNO) = -0.5D0*kappa(aNO,bNO)
          CFT2RDMab(bNO,aNO,aNO,bNO) = -0.5D0*kappa(aNO,bNO)
          CFT2RDMba(aNO,bNO,bNO,aNO) = -0.5D0*kappa(aNO,bNO)
          CFT2RDMba(bNO,aNO,aNO,bNO) = -0.5D0*kappa(aNO,bNO)
!
        end do 
      end do 
!
      do Aopen = 1, Nopen
        aNO = NO_open(Aopen)
        do Bopen = 1, Nopen
          bNO = NO_open(Bopen)
          if(aNO.eq.bNO)cycle
! Coulomb
         CFT2RDMaa(aNO,bNO,aNO,bNO) =  0.5D0*ON(aNO)*ON(bNO)
! Exchange
         CFT2RDMaa(aNO,bNO,bNO,aNO) = -0.5D0*ON(aNO)*ON(bNO)
!
        end do 
      end do 
!
! Print 2-RDMs       
! 
! alpha-alpha
!      write(6,*)' alpha-alpha 2-RDM (active orbitals)'
!      write(6,*)' Coulomb terms '
!      do Aact = 1, Nact
!        aNO = NO_act(Aact)
!        do Bact = 1, Nact
!          bNO = NO_act(Bact)
!
!          write(6,*)aNO,bNO,aNO,bNO,CFT2RDMaa(aNO,bNO,aNO,bNO)
!
!        end do ! bNO
!      end do ! aNO
!
!      write(6,*)' Exchange terms '
!      do Aact = 1, Nact
!        aNO = NO_act(Aact)
!        do Bact = 1, Nact
!          bNO = NO_act(Bact)
!
!          write(6,*)aNO,bNO,bNO,aNO,CFT2RDMaa(aNO,bNO,bNO,aNO)
!
!        end do ! bNO
!      end do ! aNO
!
!      write(6,*)' alpha-beta 2-RDM (active orbitals)'
!      write(6,*)' Coulomb terms '
!      do Aact = 1, Nact
!        aNO = NO_act(Aact)
!        do Bact = 1, Nact
!          bNO = NO_act(Bact)
!
!          write(6,*)aNO,bNO,aNO,bNO,CFT2RDMab(aNO,bNO,aNO,bNO)
!
!        end do ! bNO
!      end do ! aNO
!
!      write(6,*)' Exchange terms '
!      do Aact = 1, Nact
!        aNO = NO_act(Aact)
!        do Bact = 1, Nact
!          bNO = NO_act(Bact)
!
!          write(6,*)aNO,bNO,bNO,aNO,CFT2RDMab(aNO,bNO,bNO,aNO)
!
!        end do ! bNO
!      end do ! aNO
!
!      write(6,*)' Time-inversion exchange terms '
!      do Aact = 1, Nact
!        aNO = NO_act(Aact)
!        do Bact = 1, Nact
!          bNO = NO_act(Bact)
!
!          write(6,*)aNO,aNO,bNO,bNO,CFT2RDMab(aNO,aNO,bNO,bNO)
!
!        end do ! bNO
!      end do ! aNO
!
! Calculate two-electron energy to verify
      E2e = zero
! 
      do aNO = 1, Nbasis
        do bNO = 1, Nbasis
!
          E2e = E2e + (CFT2RDMaa(aNO,bNO,aNO,bNO) + CFT2RDMbb(aNO,bNO,aNO,bNO)&
                    +  CFT2RDMab(aNO,bNO,aNO,bNO) + CFT2RDMba(aNO,bNO,aNO,bNO))*J_NO(aNO,bNO)&
                    + (CFT2RDMaa(aNO,bNO,bNO,aNO) + CFT2RDMbb(aNO,bNO,bNO,aNO))*K_NO(aNO,bNO)
!
          if(aNO.eq.bNO)cycle
!
          E2e = E2e + (CFT2RDMab(aNO,aNO,bNO,bNO) + CFT2RDMba(aNO,aNO,bNO,bNO))*K_NO(aNO,bNO)
!
          E2e = E2e + (CFT2RDMab(aNO,bNO,bNO,aNO) + CFT2RDMba(bNO,aNO,aNO,bNO))*K_NO(aNO,bNO)
!
        end do ! bNO 
      end do ! aNO 
!
      write(UNIout,'(/a)')'2-RDM sanity check'
      write(UNIout,'(a,5X,F16.10)')' E_2e =',E2e
!
! Check orthonormalization
!      call GET_object ('INT', '1EINT', 'AO')
!
!      allocate(S(Nbasis,Nbasis),S_NO(Nbasis,Nbasis))
!      ! convert overlap to 2D matrix
!      call UPT_to_SQS (OVRLAP, MATlen, S, Nbasis)
!
!      ! Transform overlap to NO basis
!      S_NO = matmul(transpose(NO),matmul(S,NO))
!
!      write(6,*)'NO overlap'
!      do aNO = 1, Nbasis
!        do bNO = 1, Nbasis
!          write(6,*)aNO,bNO,S_NO(aNO,bNO)
!        end do ! bNO
!      end do ! aNO
!
!      Ne = zero
!      do aNO = 1, Nbasis
!        Ne = Ne + 2.0D0*ON(aNO)*S_NO(aNO,aNO)
!      end do
!      write(6,*)'Ne = ',Ne
!
! Move to separate subroutine later
      allocate(Qaa(Nbasis,Nbasis,Nbasis,Nbasis),Qbb(Nbasis,Nbasis,Nbasis,Nbasis))
      allocate(Qab(Nbasis,Nbasis,Nbasis,Nbasis),Qba(Nbasis,Nbasis,Nbasis,Nbasis))
      allocate(Gaa(Nbasis,Nbasis,Nbasis,Nbasis),Gbb(Nbasis,Nbasis,Nbasis,Nbasis))
      allocate(Gab(Nbasis,Nbasis,Nbasis,Nbasis),Gba(Nbasis,Nbasis,Nbasis,Nbasis))
      allocate(Gaabb(Nbasis,Nbasis,Nbasis,Nbasis),Gbbaa(Nbasis,Nbasis,Nbasis,Nbasis))
!
      Qaa = zero
      Qbb = zero
      Qab = zero
      Qba = zero
      Gaa = zero
      Gbb = zero
      Gab = zero
      Gba = zero
      Gaabb = zero
      Gbbaa = zero
!
! hole-hole (Q) and particle-hole (G) matrices
!
      do aNO = 1, Nbasis
        do bNO = 1, Nbasis
!
! hole-hole
          Qab(aNO,bNO,aNO,bNO) = CFT2RDMab(aNO,bNO,aNO,bNO) + 0.5D0*(one - on(aNO) - on(bNO))
          Qba(aNO,bNO,aNO,bNO) = CFT2RDMba(aNO,bNO,aNO,bNO) + 0.5D0*(one - on(aNO) - on(bNO))
!
! particle-hole
          Gab(aNO,bNO,aNO,bNO) =  0.5D0*on(bNO) - CFT2RDMab(aNO,bNO,aNO,bNO)
          Gba(aNO,bNO,aNO,bNO) =  0.5D0*on(bNO) - CFT2RDMba(aNO,bNO,aNO,bNO)
!
          if(aNO.ne.bNO)then
!            
! hole-hole
            Qaa(aNO,bNO,aNO,bNO) = CFT2RDMaa(aNO,bNO,aNO,bNO) + 0.5D0*(one - on(aNO) - on(bNO))
            Qbb(aNO,bNO,aNO,bNO) = CFT2RDMbb(aNO,bNO,aNO,bNO) + 0.5D0*(one - on(aNO) - on(bNO))
!
            Qaa(aNO,bNO,bNO,aNO) = CFT2RDMaa(aNO,bNO,bNO,aNO) + 0.5D0*(on(aNO) + on(bNO) - one)
            Qbb(aNO,bNO,bNO,aNO) = CFT2RDMbb(aNO,bNO,bNO,aNO) + 0.5D0*(on(aNO) + on(bNO) - one)
!
! not sure if this should be here, K term is zero for alpha-beta (unless HSC)
            !Qab(aNO,bNO,bNO,aNO) = CFT2RDMab(aNO,bNO,bNO,aNO) + 0.5D0*(on(aNO) + on(bNO) - one)
            !Qba(aNO,bNO,bNO,aNO) = CFT2RDMba(aNO,bNO,bNO,aNO) + 0.5D0*(on(aNO) + on(bNO) - one)
            Qab(aNO,bNO,bNO,aNO) = CFT2RDMab(aNO,bNO,bNO,aNO)
            Qba(aNO,bNO,bNO,aNO) = CFT2RDMba(aNO,bNO,bNO,aNO)
!
            Qab(aNO,aNO,bNO,bNO) = CFT2RDMab(aNO,aNO,bNO,bNO)
            Qba(aNO,aNO,bNO,bNO) = CFT2RDMba(aNO,aNO,bNO,bNO)
!
! particle-hole
            Gaa(aNO,bNO,aNO,bNO) = 0.5D0*on(bNO) - CFT2RDMaa(aNO,bNO,aNO,bNO)
            Gbb(aNO,bNO,aNO,bNO) = 0.5D0*on(bNO) - CFT2RDMbb(aNO,bNO,aNO,bNO)
!
            Gab(aNO,bNO,aNO,bNO) = 0.5D0*on(bNO) - CFT2RDMab(aNO,bNO,aNO,bNO)
            Gba(aNO,bNO,aNO,bNO) = 0.5D0*on(bNO) - CFT2RDMba(aNO,bNO,aNO,bNO)
!
            Gab(aNO,aNO,bNO,bNO) = -CFT2RDMab(bNO,aNO,aNO,bNO)
            Gba(aNO,aNO,bNO,bNO) = -CFT2RDMba(bNO,aNO,aNO,bNO)
!
            Gab(aNO,bNO,bNO,aNO) = -CFT2RDMab(bNO,bNO,aNO,aNO)
            Gba(aNO,bNO,bNO,aNO) = -CFT2RDMba(bNO,bNO,aNO,aNO)
!
            Gaabb(aNO,bNO,bNO,aNO) = CFT2RDMab(bNO,bNO,aNO,aNO)
            Gbbaa(aNO,bNO,bNO,aNO) = CFT2RDMba(bNO,bNO,aNO,aNO)
!
            Gaabb(aNO,bNO,aNO,bNO) = CFT2RDMab(bNO,aNO,aNO,bNO)
            Gbbaa(aNO,bNO,aNO,bNO) = CFT2RDMba(bNO,aNO,aNO,bNO)
!
          end if
!
!          if(dabs(Qab(aNO,bNO,aNO,bNO)).gt.1.0D-12)then
!            write(6,*)'Qab ',aNO,bNO,aNO,bNO, Qab(aNO,bNO,aNO,bNO)
!            write(6,*)'Pab ',aNO,bNO,aNO,bNO, CFT2RDMab(aNO,bNO,aNO,bNO)
!          end if
!          if(dabs(Qab(aNO,bNO,bNO,aNO)).gt.1.0D-12)then
!            write(6,*)'Qab ',aNO,bNO,bNO,aNO, Qab(aNO,bNO,bNO,aNO)
!            write(6,*)'Qab ',aNO,bNO,aNO,bNO, Qab(aNO,bNO,aNO,bNO)
!          end if
!
        end do ! bNO 
      end do ! aNO 
!
! Diagonalize each spin component and get eigenvalues
      allocate(eigval(Nbasis**2))
!
      call diagonalize_matrix(Nbasis**2, CFT2RDMaa, eigval)
      write(6,*)'negative CFT2RDMaa eigenvalues'
      negsum = 0.0D0
      do aNO = 1, Nbasis**2
        if(eigval(aNO).lt.0.0D0)then
          if(eigval(aNO).lt.-1.0D-12)then
            write(6,*)eigval(aNO)
            negsum = negsum + eigval(aNO)
          end if
        end if
      end do ! aNO
      eigsum = sum(eigval)
      write(6,*)'sum = ',eigsum
      !write(6,*)'negP_aa = ',negsum/eigsum
      write(6,*)'negP_aa = ',negsum
!
      call diagonalize_matrix(Nbasis**2, CFT2RDMbb, eigval)
      write(6,*)'negative CFT2RDMbb eigenvalues'
      negsum = 0.0D0
      do aNO = 1, Nbasis**2
        if(eigval(aNO).lt.0.0D0)then
          if(eigval(aNO).lt.-1.0D-12)then
            write(6,*)eigval(aNO)
            negsum = negsum + eigval(aNO)
          end if
        end if
      end do ! aNO
      eigsum = sum(eigval)
      write(6,*)'sum = ',eigsum
      !write(6,*)'negP_bb = ',negsum/eigsum
      write(6,*)'negP_bb = ',negsum
!
      call diagonalize_matrix(Nbasis**2, CFT2RDMab, eigval)
      write(6,*)'negative CFT2RDMab eigenvalues'
      negsum = 0.0D0
      do aNO = 1, Nbasis**2
        if(eigval(aNO).lt.0.0D0)then
          if(eigval(aNO).lt.-1.0D-12)then
            write(6,*)eigval(aNO)
            negsum = negsum + eigval(aNO)
          end if
        end if
      end do ! aNO
      eigsum = sum(eigval)
      write(6,*)'sum = ',eigsum
      !write(6,*)'negP_ab = ',negsum/eigsum
      write(6,*)'negP_ab = ',negsum
!
      call diagonalize_matrix(Nbasis**2, CFT2RDMba, eigval)
      write(6,*)'negative CFT2RDMba eigenvalues'
      negsum = 0.0D0
      do aNO = 1, Nbasis**2
        if(eigval(aNO).lt.0.0D0)then
          if(eigval(aNO).lt.-1.0D-12)then
            write(6,*)eigval(aNO)
            negsum = negsum + eigval(aNO)
          end if
        end if
      end do ! aNO
      eigsum = sum(eigval)
      write(6,*)'sum = ',eigsum
      !write(6,*)'negP_ba = ',negsum/eigsum
      write(6,*)'negP_ba = ',negsum
!
! Diagonalize each spin component and get eigenvalues
!
      call diagonalize_matrix(Nbasis**2, Qaa, eigval)
      write(6,*)'negative Qaa eigenvalues'
      negsum = 0.0D0
      do aNO = 1, Nbasis**2
        if(eigval(aNO).lt.0.0D0)then
          if(eigval(aNO).lt.-1.0D-12)then
            write(6,*)eigval(aNO)
            negsum = negsum + eigval(aNO)
          end if
        end if
      end do ! aNO
      eigsum = sum(eigval)
      write(6,*)'sum = ',eigsum
      !write(6,*)'negQ_aa = ',negsum/eigsum
      write(6,*)'negQ_aa = ',negsum
!
      call diagonalize_matrix(Nbasis**2, Qbb, eigval)
      write(6,*)'negative Qbb eigenvalues'
      negsum = 0.0D0
      do aNO = 1, Nbasis**2
        if(eigval(aNO).lt.0.0D0)then
          if(eigval(aNO).lt.-1.0D-12)then
            write(6,*)eigval(aNO)
            negsum = negsum + eigval(aNO)
          end if
        end if
      end do ! aNO
      eigsum = sum(eigval)
      write(6,*)'sum = ',eigsum
      !write(6,*)'negQ_bb = ',negsum/eigsum
      write(6,*)'negQ_bb = ',negsum
!
      call diagonalize_matrix(Nbasis**2, Qab, eigval)
      write(6,*)'negative Qab eigenvalues'
      negsum = 0.0D0
      do aNO = 1, Nbasis**2
        if(eigval(aNO).lt.0.0D0)then
          if(eigval(aNO).lt.-1.0D-12)then
            write(6,*)eigval(aNO)
            negsum = negsum + eigval(aNO)
          end if
        end if
      end do ! aNO
      eigsum = sum(eigval)
      write(6,*)'sum = ',eigsum
      !write(6,*)'negQ_ab = ',negsum/eigsum
      write(6,*)'negQ_ab = ',negsum
!
      call diagonalize_matrix(Nbasis**2, Qba, eigval)
      write(6,*)'negative Qba eigenvalues'
      negsum = 0.0D0
      do aNO = 1, Nbasis**2
        if(eigval(aNO).lt.0.0D0)then
          if(eigval(aNO).lt.-1.0D-12)then
            write(6,*)eigval(aNO)
            negsum = negsum + eigval(aNO)
          end if
        end if
      end do ! aNO
      eigsum = sum(eigval)
      write(6,*)'sum = ',eigsum
      !write(6,*)'negQ_ba = ',negsum/eigsum
      write(6,*)'negQ_ba = ',negsum
!
! Diagonalize each spin component and get eigenvalues
!
! Construct "super" matrix because Gaa and Gbb blocks are coupled via Gaabb blocks
!
      allocate(Gsup(2*Nbasis**2,2*Nbasis**2))
!
      call CFT_BLD_Gsup(Gaa, Gbb, Gaabb, Gbbaa, Gsup)
!
      deallocate(eigval)
      allocate(eigval(2*Nbasis**2))
      call diagonalize_matrix(2*Nbasis**2, Gsup, eigval)
      write(6,*)'negative Gsup eigenvalues'
      negsum = 0.0D0
      do aNO = 1, 2*Nbasis**2
        if(eigval(aNO).lt.0.0D0)then
          if(eigval(aNO).lt.-1.0D-12)then
            write(6,*)eigval(aNO)
            negsum = negsum + eigval(aNO)
          end if
        end if
      end do ! aNO
      eigsum = sum(eigval)
      write(6,*)'sum = ',eigsum
      !write(6,*)'negG_aa = ',negsum/eigsum
      write(6,*)'negG_sup = ',negsum
!
      deallocate(eigval)
      allocate(eigval(Nbasis**2))
      call diagonalize_matrix(Nbasis**2, Gab, eigval)
      write(6,*)'negative Gab eigenvalues'
      negsum = 0.0D0
      do aNO = 1, Nbasis**2
        if(eigval(aNO).lt.0.0D0)then
          if(eigval(aNO).lt.-1.0D-12)then
            write(6,*)eigval(aNO)
            negsum = negsum + eigval(aNO)
          end if
        end if
      end do ! aNO
      eigsum = sum(eigval)
      write(6,*)'sum = ',eigsum
      !write(6,*)'negG_ab = ',negsum/eigsum
      write(6,*)'negG_ab = ',negsum
!
      call diagonalize_matrix(Nbasis**2, Gba, eigval)
      write(6,*)'negative Gba eigenvalues'
      negsum = 0.0D0
      do aNO = 1, Nbasis**2
        if(eigval(aNO).lt.0.0D0)then
          if(eigval(aNO).lt.-1.0D-12)then
            write(6,*)eigval(aNO)
            negsum = negsum + eigval(aNO)
          end if
        end if
      end do ! aNO
      eigsum = sum(eigval)
      write(6,*)'sum = ',eigsum
      !write(6,*)'negG_ba = ',negsum/eigsum
      write(6,*)'negG_ba = ',negsum
!
      call PRG_manager ('exit', 'CFT_BLD_2RDM', 'UTILITY')
      end subroutine CFT_BLD_2RDM
      subroutine CFT_BLD_Gsup(Gaa, Gbb, Gaabb, Gbbaa, Gsup)
!*****************************************************************
!     Date last modified: January 21, 2022                       *
!     Author: JWH                                                *
!     Description: Build the alpha-alpha-beta-beta particle-     *
!                  matrix.                                       *
!*****************************************************************
!Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE CFT_objects
      USE QM_objects

      implicit none
!
! Input arrays:
      double precision :: Gaa(Nbasis**2,Nbasis**2), Gbb(Nbasis**2,Nbasis**2)
      double precision :: Gaabb(Nbasis**2,Nbasis**2), Gbbaa(Nbasis**2,Nbasis**2)
! 
! Output array:
      double precision :: Gsup(2*Nbasis**2,2*Nbasis**2)
!
! Begin:
      call PRG_manager ('enter', 'CFT_BLD_Gsup', 'UTILITY')
!
      Gsup(1:Nbasis**2,1:Nbasis**2) = Gaa(1:Nbasis**2,1:Nbasis**2)
      Gsup(Nbasis**2+1:2*Nbasis**2,Nbasis**2+1:2*Nbasis**2) = Gbb(1:Nbasis**2,1:Nbasis**2)
      Gsup(1:Nbasis**2,Nbasis**2+1:2*Nbasis**2) = Gaabb(1:Nbasis**2,1:Nbasis**2)
      Gsup(Nbasis**2+1:2*Nbasis**2,1:Nbasis**2) = Gbbaa(1:Nbasis**2,1:Nbasis**2)
!
      call PRG_manager ('exit', 'CFT_BLD_Gsup', 'UTILITY')
      end subroutine CFT_BLD_Gsup
      subroutine CFT_BLD_R2RDM
!*****************************************************************
!     Date last modified: June 10, 2021                          *
!     Author: JWH                                                *
!     Description: Construct the on-top density reduced 2RDM for *
!                  use in correlation functional.                *
!*****************************************************************
!Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE CFT_objects
      USE QM_objects
      USE INT_objects

      implicit none
!
! Local scalars: 
      integer :: aNO, bNO, Aclosed, Aopen, Bclosed, Bopen
      double precision :: E2e
!
! Begin:
      call PRG_manager ('enter', 'CFT_BLD_R2RDM', 'UTILITY')
!
! Get CFT 2RDM
! Hmmm...can't do this if using inside CFT calc 
!      call GET_object('CFT','2RDM','DELTANO')
!
! Construct 2-RDMs (spin-resolved)
!
      if(allocated(R2RDMaa))then
        deallocate(R2RDMaa,R2RDMbb,R2RDMJab,R2RDMLab,R2RDMJba,R2RDMLba) 
      end if
      allocate(R2RDMaa(Nbasis,Nbasis))
      allocate(R2RDMbb(Nbasis,Nbasis))
      allocate(R2RDMJab(Nbasis,Nbasis))
      allocate(R2RDMLab(Nbasis,Nbasis))
      allocate(R2RDMJba(Nbasis,Nbasis))
      allocate(R2RDMLba(Nbasis,Nbasis))
!
      R2RDMaa =  zero ! multiplies J-K
      R2RDMbb =  zero ! multiplies J-K
      R2RDMJab = zero ! multiplies J
      R2RDMLab = zero ! multiplies L (or K for HSC)
      R2RDMJba = zero ! multiplies J
      R2RDMLba = zero ! multiplies L (or K for HSC)
!
! 2-RDM is modified here so that HSC correction affects parallel-spin and FHC can be applied
!
      do Aclosed = 1, Nclosed
        aNO = NO_closed(Aclosed)
        do Bclosed = 1, Nclosed
          bNO = NO_closed(Bclosed)
! Coulomb
         if(aNO.ne.bNO)then
           R2RDMaa(aNO,bNO) = 0.5D0*(ON(aNO)*ON(bNO) + eta(aNO,bNO) + kappa(aNO,bNO))
           R2RDMbb(aNO,bNO) = 0.5D0*(ON(aNO)*ON(bNO) + eta(aNO,bNO) + kappa(aNO,bNO))
         end if
         R2RDMJab(aNO,bNO) = 0.5D0*(ON(aNO)*ON(bNO) + eta(aNO,bNO) - kappa(aNO,bNO))
         R2RDMJba(aNO,bNO) = 0.5D0*(ON(aNO)*ON(bNO) + eta(aNO,bNO) - kappa(aNO,bNO))
! Time-inversion exchange
         if(aNO.ne.bNO)then
           R2RDMLab(aNO,bNO) = 0.5D0*(zeta(aNO,bNO) - xi(aNO,bNO))
           R2RDMLba(aNO,bNO) = 0.5D0*(zeta(aNO,bNO) - xi(aNO,bNO))
         end if
!
        end do 
      end do 
!
      do Aopen = 1, Nopen
        aNO = NO_open(Aopen)
        do Bclosed = 1, Nclosed
          bNO = NO_closed(Bclosed)
! Coulomb
         if(aNO.ne.bNO)then
           R2RDMaa(aNO,bNO) = 0.5D0*(ON(aNO)*ON(bNO) + eta(aNO,bNO)) + kappa(aNO,bNO)
           R2RDMaa(bNO,aNO) = 0.5D0*(ON(aNO)*ON(bNO) + eta(aNO,bNO)) + kappa(aNO,bNO)
         end if
         R2RDMJab(aNO,bNO) = 0.5D0*(ON(aNO)*ON(bNO) + eta(aNO,bNO)) - kappa(aNO,bNO)
         R2RDMJba(bNO,aNO) = 0.5D0*(ON(aNO)*ON(bNO) + eta(aNO,bNO)) - kappa(aNO,bNO)
!
        end do 
      end do 
!
      do Aopen = 1, Nopen
        aNO = NO_open(Aopen)
        do Bopen = 1, Nopen
          bNO = NO_open(Bopen)
! Coulomb
         if(aNO.ne.bNO)then
           R2RDMaa(aNO,bNO) = 0.5D0*ON(aNO)*ON(bNO)
         end if
!
        end do 
      end do 
!
!      write(6,*)' Another sanity check '
!      E2e = 0.0D0
!      do aNO = 1, Nbasis
!        do bNO = 1, Nbasis
!          E2e = E2e + (R2RDMaa(aNO,bNO) + R2RDMbb(aNO,bNO))*(J_NO(aNO,bNO) - K_NO(aNO,bNO))
!          E2e = E2e + (R2RDMJab(aNO,bNO) + R2RDMJba(aNO,bNO))*J_NO(aNO,bNO)
!          E2e = E2e + (R2RDMLab(aNO,bNO) + R2RDMLba(aNO,bNO))*K_NO(aNO,bNO)
!        end do ! bNO
!      end do ! aNO
!      write(6,*) 'E2e = ',E2e
!
      call PRG_manager ('exit', 'CFT_BLD_R2RDM', 'UTILITY')
      end subroutine CFT_BLD_R2RDM
      subroutine CFT_BLD_dR2RDM
!*****************************************************************
!     Date last modified: April 7, 2022                          *
!     Author: JWH                                                *
!     Description: Construct the Delta derivative of the reduced *
!                  two-index 2RDM.                               *
!*****************************************************************
!Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE CFT_objects
      USE QM_objects
      USE INT_objects

      implicit none
!
! Local scalars: 
      integer :: aNO, bNO, Aclosed, Aopen, Bclosed, Bopen, Istat, kNO, tNO
      double precision :: E2e
!
! Begin:
      call PRG_manager ('enter', 'CFT_BLD_dR2RDM', 'UTILITY')
!
! Get CFT 2RDM
! Keep this ??
!      call GET_object('CFT','2RDM','DELTANO')
!
! Construct 2-RDMs (spin-resolved)
!
      if(allocated(dR2RDMaa))then
        deallocate(dR2RDMaa,dR2RDMbb,dR2RDMJab,dR2RDMLab,dR2RDMJba,dR2RDMLba,donon) 
      end if
      allocate(dR2RDMaa(Nbasis,Nbasis,Nstat))
      allocate(dR2RDMbb(Nbasis,Nbasis,Nstat))
      allocate(dR2RDMJab(Nbasis,Nbasis,Nstat))
      allocate(dR2RDMLab(Nbasis,Nbasis,Nstat))
      allocate(dR2RDMJba(Nbasis,Nbasis,Nstat))
      allocate(dR2RDMLba(Nbasis,Nbasis,Nstat))
      allocate(donon(Nbasis,Nbasis,Nstat))
!
      dR2RDMaa = zero  ! multiplies J-K
      dR2RDMbb = zero  ! multiplies J-K
      dR2RDMJab = zero ! multiplies J
      dR2RDMLab = zero ! multiplies L (or K for HSC)
      dR2RDMJba = zero ! multiplies J
      dR2RDMLba = zero ! multiplies L (or K for HSC)
!
! create derivative of on*on
      donon = zero
      do Istat = 1, Nstat
        kNO = stat_pair(Istat,1)
        tNO = stat_pair(Istat,2)
!
        do aNO = 1, Nbasis
          donon(aNO,kNO,Istat) = donon(aNO,kNO,Istat) - on(aNO)
          donon(aNO,tNO,Istat) = donon(aNO,tNO,Istat) + on(aNO)
          donon(kNO,aNO,Istat) = donon(kNO,aNO,Istat) - on(aNO)
          donon(tNO,aNO,Istat) = donon(tNO,aNO,Istat) + on(aNO)
        end do ! aNO
!
      end do ! Istat
!
! 2-RDM is modified here so that HSC correction affects parallel-spin and FHC can be applied
      do Istat = 1, Nstat
!
        do Aclosed = 1, Nclosed
          aNO = NO_closed(Aclosed)
          do Bclosed = 1, Nclosed
            bNO = NO_closed(Bclosed)
! Coulomb
           if(aNO.ne.bNO)then
             dR2RDMaa(aNO,bNO,Istat) = 0.5D0*(donon(aNO,bNO,Istat) + deta(aNO,bNO,Istat)&
                                            + dkappa(aNO,bNO,Istat))
             dR2RDMbb(aNO,bNO,Istat) = 0.5D0*(donon(aNO,bNO,Istat) + deta(aNO,bNO,Istat)&
                                            + dkappa(aNO,bNO,Istat))
           end if
           dR2RDMJab(aNO,bNO,Istat) = 0.5D0*(donon(aNO,bNO,Istat) + deta(aNO,bNO,Istat)&
                                           - dkappa(aNO,bNO,Istat))
           dR2RDMJba(aNO,bNO,Istat) = 0.5D0*(donon(aNO,bNO,Istat) + deta(aNO,bNO,Istat)&
                                           - dkappa(aNO,bNO,Istat))
! Time-inversion exchange
           if(aNO.ne.bNO)then
             dR2RDMLab(aNO,bNO,Istat) = 0.5D0*(dzeta(aNO,bNO,Istat) - dxi(aNO,bNO,Istat))
             dR2RDMLba(aNO,bNO,Istat) = 0.5D0*(dzeta(aNO,bNO,Istat) - dxi(aNO,bNO,Istat))
           end if
!
          end do 
        end do 
!
        do Aopen = 1, Nopen
          aNO = NO_open(Aopen)
          do Bclosed = 1, Nclosed
            bNO = NO_closed(Bclosed)
! Coulomb
           if(aNO.ne.bNO)then
             dR2RDMaa(aNO,bNO,Istat) = 0.5D0*(donon(aNO,bNO,Istat) + deta(aNO,bNO,Istat))&
                                     + dkappa(aNO,bNO,Istat)
             dR2RDMaa(bNO,aNO,Istat) = 0.5D0*(donon(aNO,bNO,Istat) + deta(aNO,bNO,Istat))&
                                     + dkappa(aNO,bNO,Istat)
           end if
           dR2RDMJab(aNO,bNO,Istat) = 0.5D0*(donon(aNO,bNO,Istat) + deta(aNO,bNO,Istat))&
                                    - dkappa(aNO,bNO,Istat)
           dR2RDMJab(bNO,aNO,Istat) = 0.5D0*(donon(aNO,bNO,Istat) + deta(aNO,bNO,Istat))&
                                    - dkappa(aNO,bNO,Istat)
!
          end do 
        end do 
!
        do Aopen = 1, Nopen
          aNO = NO_open(Aopen)
          do Bopen = 1, Nopen
            bNO = NO_open(Bopen)
! Coulomb
            if(aNO.ne.bNO)then
              dR2RDMaa(aNO,bNO,Istat) = 0.5D0*donon(aNO,bNO,Istat)
            end if
!
          end do 
        end do 
!
      end do ! Istat 
!
      call PRG_manager ('exit', 'CFT_BLD_dR2RDM', 'UTILITY')
      end subroutine CFT_BLD_dR2RDM
      subroutine CFT_BLD_d2R2RDM
!*****************************************************************
!     Date last modified: April 18, 2022                         *
!     Author: JWH                                                *
!     Description: Construct the Delta hessian of the reduced    *
!                  two-index 2RDM.                               *
!*****************************************************************
!Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE CFT_objects
      USE QM_objects
      USE INT_objects

      implicit none
!
! Local scalars: 
      integer :: aNO, bNO, Aclosed, Aopen, Bclosed, Bopen, Istat, Jstat, kNO, tNO, lNO, uNO
      double precision :: E2e
!
! Local arrays:
      double precision, allocatable, dimension(:,:,:,:) :: d2ononeta
!
! Begin:
      call PRG_manager ('enter', 'CFT_BLD_d2R2RDM', 'UTILITY')
!
! Get CFT 2RDM
! Keep this ??
!      call GET_object('CFT','2RDM','DELTANO')
!
! Construct 2-RDMs (spin-resolved)
!
      if(allocated(d2R2RDMaa))then
        deallocate(d2R2RDMaa,d2R2RDMbb,d2R2RDMJab,d2R2RDMLab,d2R2RDMJba,d2R2RDMLba) 
      end if
      allocate(d2R2RDMaa(Nbasis,Nbasis,Nstat,Nstat))
      allocate(d2R2RDMbb(Nbasis,Nbasis,Nstat,Nstat))
      allocate(d2R2RDMJab(Nbasis,Nbasis,Nstat,Nstat))
      allocate(d2R2RDMLab(Nbasis,Nbasis,Nstat,Nstat))
      allocate(d2R2RDMJba(Nbasis,Nbasis,Nstat,Nstat))
      allocate(d2R2RDMLba(Nbasis,Nbasis,Nstat,Nstat))
!
      allocate(d2ononeta(Nbasis,Nbasis,Nstat,Nstat))
!
      d2R2RDMaa = zero  ! multiplies J-K
      d2R2RDMbb = zero  ! multiplies J-K
      d2R2RDMJab = zero ! multiplies J
      d2R2RDMLab = zero ! multiplies L (or K for HSC)
      d2R2RDMJba = zero ! multiplies J
      d2R2RDMLba = zero ! multiplies L (or K for HSC)
!
! create derivative of on*on
      d2ononeta = zero
      do Istat = 1, Nstat
        kNO = stat_pair(Istat,1)
        tNO = stat_pair(Istat,2)
!
        do Jstat = 1, Nstat
          if(Istat.eq.Jstat)cycle
          lNO = stat_pair(Jstat,1)
          uNO = stat_pair(Jstat,2)
!
          if((kNO.ne.lNO).and.(tNO.ne.uNO))then
            d2ononeta(tNO,uNO,Istat,Jstat) =  one
            d2ononeta(uNO,tNO,Istat,Jstat) =  one
            d2ononeta(tNO,lNO,Istat,Jstat) = -one
            d2ononeta(lNO,tNO,Istat,Jstat) = -one
            d2ononeta(kNO,uNO,Istat,Jstat) = -one
            d2ononeta(uNO,kNO,Istat,Jstat) = -one
            d2ononeta(kNO,lNO,Istat,Jstat) =  one
            d2ononeta(lNO,kNO,Istat,Jstat) =  one
          end if
!
        end do ! Jstat
      end do ! Istat
!
! 2-RDM is modified here so that HSC correction affects parallel-spin and FHC can be applied
      do Istat = 1, Nstat
        do Jstat = 1, Istat
!
          do Aclosed = 1, Nclosed
            aNO = NO_closed(Aclosed)
            do Bclosed = 1, Nclosed
              bNO = NO_closed(Bclosed)
! Coulomb
             if(aNO.ne.bNO)then
               d2R2RDMaa(aNO,bNO,Istat,Jstat) = 0.5D0*(d2ononeta(aNO,bNO,Istat,Jstat)&
                                                     + d2kappa(aNO,bNO,Istat,Jstat))
               d2R2RDMaa(aNO,bNO,Jstat,Istat) = d2R2RDMaa(aNO,bNO,Istat,Jstat)
!
               d2R2RDMbb(aNO,bNO,Istat,Jstat) = 0.5D0*(d2ononeta(aNO,bNO,Istat,Jstat)&
                                                     + d2kappa(aNO,bNO,Istat,Jstat))
               d2R2RDMbb(aNO,bNO,Jstat,Istat) = d2R2RDMbb(aNO,bNO,Istat,Jstat)
             end if
             d2R2RDMJab(aNO,bNO,Istat,Jstat) = 0.5D0*(d2ononeta(aNO,bNO,Istat,Jstat)&
                                                    - d2kappa(aNO,bNO,Istat,Jstat))
             d2R2RDMJab(aNO,bNO,Jstat,Istat) = d2R2RDMJab(aNO,bNO,Istat,Jstat)
!
             d2R2RDMJba(aNO,bNO,Istat,Jstat) = 0.5D0*(d2ononeta(aNO,bNO,Istat,Jstat)&
                                                    - d2kappa(aNO,bNO,Istat,Jstat))
             d2R2RDMJba(aNO,bNO,Jstat,Istat) = d2R2RDMJba(aNO,bNO,Istat,Jstat)
! Time-inversion exchange
             if(aNO.ne.bNO)then
               ! ommitting xi gives much better hessian values
               !debug
               d2R2RDMLab(aNO,bNO,Istat,Jstat) = 0.5D0*(d2zeta(aNO,bNO,Istat,Jstat))!&
                                                      !- d2xi(aNO,bNO,Istat,Jstat))
               d2R2RDMLab(aNO,bNO,Jstat,Istat) = d2R2RDMLab(aNO,bNO,Istat,Jstat)
!
               !debug
               d2R2RDMLba(aNO,bNO,Istat,Jstat) = 0.5D0*(d2zeta(aNO,bNO,Istat,Jstat))!&
                                                      !- d2xi(aNO,bNO,Istat,Jstat))
               d2R2RDMLba(aNO,bNO,Jstat,Istat) = d2R2RDMLba(aNO,bNO,Istat,Jstat)
             end if
!
            end do 
          end do 
!
          do Aopen = 1, Nopen
            aNO = NO_open(Aopen)
            do Bclosed = 1, Nclosed
              bNO = NO_closed(Bclosed)
! Coulomb
             if(aNO.ne.bNO)then
               d2R2RDMaa(aNO,bNO,Istat,Jstat) = 0.5D0*d2ononeta(aNO,bNO,Istat,Jstat)&
                                              + d2kappa(aNO,bNO,Istat,Jstat)
               d2R2RDMaa(aNO,bNO,Jstat,Istat) = d2R2RDMaa(aNO,bNO,Istat,Jstat)
!
               d2R2RDMaa(bNO,aNO,Istat,Jstat) = 0.5D0*d2ononeta(bNO,aNO,Istat,Jstat)&
                                              + d2kappa(bNO,aNO,Istat,Jstat)
               d2R2RDMaa(bNO,aNO,Jstat,Istat) = d2R2RDMaa(bNO,aNO,Istat,Jstat)
             end if
             d2R2RDMJab(aNO,bNO,Istat,Jstat) = 0.5D0*d2ononeta(aNO,bNO,Istat,Jstat)&
                                             - d2kappa(aNO,bNO,Istat,Jstat)
             d2R2RDMJab(aNO,bNO,Jstat,Istat) = d2R2RDMJab(aNO,bNO,Istat,Jstat)
!
             d2R2RDMJab(bNO,aNO,Istat,Jstat) = 0.5D0*d2ononeta(bNO,aNO,Istat,Jstat)&
                                             - d2kappa(bNO,aNO,Istat,Jstat)
             d2R2RDMJab(bNO,aNO,Jstat,Istat) = d2R2RDMJab(bNO,aNO,Istat,Jstat)
!
            end do 
          end do 
!
          do Aopen = 1, Nopen
            aNO = NO_open(Aopen)
            do Bopen = 1, Nopen
              bNO = NO_open(Bopen)
! Coulomb
              if(aNO.ne.bNO)then
                d2R2RDMaa(aNO,bNO,Istat,Jstat) = 0.5D0*d2ononeta(aNO,bNO,Istat,Jstat)
                d2R2RDMaa(aNO,bNO,Jstat,Istat) = d2R2RDMaa(aNO,bNO,Istat,Jstat)
              end if
!
            end do 
          end do 
!
        end do ! Jstat 
      end do ! Istat 
!
      call PRG_manager ('exit', 'CFT_BLD_d2R2RDM', 'UTILITY')
      end subroutine CFT_BLD_d2R2RDM
      subroutine CFT_BLD_hesscoeff
!*****************************************************************
!     Date last modified: April 20, 2022                         *
!     Author: JWH                                                *
!     Description: Construct the EPUM hessian coefficients for   *
!                  repetitive use in numerical integration.      *
!*****************************************************************
!Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE CFT_objects
      USE QM_objects
      USE INT_objects

      implicit none
!
! Local scalars: 
      integer :: aNO, bNO, Aclosed, Aopen, Bclosed, Bopen, Istat, kNO, tNO, Ipair, Jpair
      integer :: pNO, qNO, rNO, sNO
      double precision :: E2e, na_p, nb_p, na_q, nb_q, na_r, nb_r, na_s, nb_s
!
! Local arrays:
      double precision, allocatable, dimension(:) :: ONa, ONb
!
! Local functions: 
      double precision :: Kronecker_delta
!
! Begin:
      call PRG_manager ('enter', 'CFT_BLD_hesscoeff', 'UTILITY')
!
! Get CFT 2RDM
! Keep this ??
!      call GET_object('CFT','2RDM','DELTANO')
!
! set alpha and beta occupancies
      allocate(ONa(Nbasis),ONb(Nbasis))
      ONa = ON
      ONb = ON
      ! open orbitals
      ONb(NBelectrons+1:NAelectrons) = zero
!
! rho coefficients
!
      if(allocated(chessrho))then
        deallocate(chessrho) 
      end if
      allocate(chessrho(4,Npair,Npair))
!
      do Ipair = 1, Npair
        pNO = orbpr(Ipair,1)
        qNO = orbpr(Ipair,2)
        na_p = ONa(pNO)
        nb_p = ONb(pNO)
        na_q = ONa(qNO)
        nb_q = ONb(qNO)
!
        do Jpair = 1, Npair
          rNO = orbpr(Jpair,1)
          sNO = orbpr(Jpair,2)
          na_r = ONa(rNO)
          nb_r = ONb(rNO)
          na_s = ONa(sNO)
          nb_s = ONb(sNO)
!
          chessrho(1,Ipair,Jpair) = Kronecker_delta(pNO,rNO)&
                                    *(na_p + nb_p + na_r + nb_r - na_q - nb_q - na_s - nb_s)
          chessrho(2,Ipair,Jpair) = Kronecker_delta(qNO,sNO)&
                                    *(na_q + nb_q + na_s + nb_s - na_p - nb_p - na_r - nb_r)
          chessrho(3,Ipair,Jpair) = Kronecker_delta(pNO,sNO)&
                                    *(na_q + nb_q + na_r + nb_r - na_p - nb_p - na_s - nb_s)
          chessrho(4,Ipair,Jpair) = Kronecker_delta(qNO,rNO)&
                                    *(na_p + nb_p + na_s + nb_s - na_q - nb_q - na_r - nb_r)
        end do ! Jpair
      end do ! Ipair
!
! R2RDM coefficients
!
      if(allocated(chessJ))then
        deallocate(chessJ,chessL,chessJ4i,chessK4i) 
      end if
      allocate(chessJ(4,Nbasis,Npair,Npair))
      allocate(chessL(4,Nbasis,Npair,Npair))
      allocate(chessJ4i(Npair,Npair))
      allocate(chessK4i(Npair,Npair))
!
      chessJ   = zero
      chessL   = zero
      chessJ4i = zero
      chessK4i = zero
!
      do Ipair = 1, Npair
        pNO = orbpr(Ipair,1)
        qNO = orbpr(Ipair,2)
        do Jpair = 1, Npair
          rNO = orbpr(Jpair,1)
          sNO = orbpr(Jpair,2)
!
          do aNO = 1, Nnatorb
!
            chessJ(1,aNO,Ipair,Jpair) = two*Kronecker_delta(pNO,rNO)&
            *(R2RDMJos(pNO,aNO) + R2RDMJos(rNO,aNO) - R2RDMJos(qNO,aNO) - R2RDMJos(sNO,aNO))
            chessJ(2,aNO,Ipair,Jpair) = two*Kronecker_delta(qNO,sNO)&
            *(R2RDMJos(qNO,aNO) + R2RDMJos(sNO,aNO) - R2RDMJos(pNO,aNO) - R2RDMJos(rNO,aNO))
            chessJ(3,aNO,Ipair,Jpair) = two*Kronecker_delta(pNO,sNO)&
            *(R2RDMJos(qNO,aNO) + R2RDMJos(rNO,aNO) - R2RDMJos(pNO,aNO) - R2RDMJos(sNO,aNO))
            chessJ(4,aNO,Ipair,Jpair) = two*Kronecker_delta(qNO,rNO)&
            *(R2RDMJos(pNO,aNO) + R2RDMJos(sNO,aNO) - R2RDMJos(qNO,aNO) - R2RDMJos(rNO,aNO))
!
            chessL(1,aNO,Ipair,Jpair) = two*Kronecker_delta(pNO,rNO)&
            *(R2RDMLos(pNO,aNO) + R2RDMLos(rNO,aNO) - R2RDMLos(qNO,aNO) - R2RDMLos(sNO,aNO))
            chessL(2,aNO,Ipair,Jpair) = two*Kronecker_delta(qNO,sNO)&
            *(R2RDMLos(qNO,aNO) + R2RDMLos(sNO,aNO) - R2RDMLos(pNO,aNO) - R2RDMLos(rNO,aNO))
            chessL(3,aNO,Ipair,Jpair) = two*Kronecker_delta(pNO,sNO)&
            *(R2RDMLos(qNO,aNO) + R2RDMLos(rNO,aNO) - R2RDMLos(pNO,aNO) - R2RDMLos(sNO,aNO))
            chessL(4,aNO,Ipair,Jpair) = two*Kronecker_delta(qNO,rNO)&
            *(R2RDMLos(pNO,aNO) + R2RDMLos(sNO,aNO) - R2RDMLos(qNO,aNO) - R2RDMLos(rNO,aNO))
!
          end do ! aNO 
!
          chessJ4i(Ipair,Jpair) = 8.0D0*(R2RDMJos(pNO,rNO) + R2RDMJos(qNO,sNO)&
                                       - R2RDMJos(pNO,sNO) - R2RDMJos(qNO,rNO))
          chessK4i(Ipair,Jpair) = 4.0D0*(R2RDMLos(pNO,rNO) + R2RDMLos(qNO,sNO)&
                                       - R2RDMLos(pNO,sNO) - R2RDMLos(qNO,rNO))
!
        end do ! Jpair
      end do ! Ipair
!
      call PRG_manager ('exit', 'CFT_BLD_hesscoeff', 'UTILITY')
      end subroutine CFT_BLD_hesscoeff
      subroutine CFT_NO_GAMESS
!*****************************************************************
!     Date last modified: October 9, 2020                        *
!     Author: JWH                                                *
!     Description: Output NOs to file for copying into a GAMESS  *
!                  output file for visualization.                *
!*****************************************************************
!Modules:
      USE program_files
      USE program_constants
      USE CFT_objects
      USE QM_objects

      implicit none
!
! Local scalars: 
      integer :: aNO, iAO
      character(len=128) :: gamess_orb
!
! Local arrays: 
      double precision, allocatable, dimension(:,:) :: NO_gamess
!
! Begin:
      call PRG_manager ('enter', 'CFT_NO_GAMESS', 'UTILITY')
!
      gamess_orb = 'gamess_orb.dat'
!
      allocate(NO_gamess(Nbasis,Nbasis))
!
      call reorder_AO_for_MO_reverse(NO,NO_gamess)
!
      open(UNIT=33, FILE=gamess_orb, STATUS='Replace')
      write(33,'(I4)')Nbasis
      do aNO = 1, Nbasis
        do iAO = 1, Nbasis
          write(33,'(F12.6)') NO_gamess(iAO,aNO)
        end do ! iAO
      end do ! aNO
      close(33)
!
      write(UNIout,'(/a,a/)')' GAMESS formatted orbitals written to: ',gamess_orb
!
      call PRG_manager ('exit', 'CFT_NO_GAMESS', 'UTILITY')
      end subroutine CFT_NO_GAMESS
      subroutine CFT_MB_calc
!*****************************************************************
!     Date last modified: July 14, 2021                          *
!     Author: JWH                                                *
!     Description: Calculate the CFT momentum-balance.           *
!*****************************************************************
!Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE CFT_objects
      USE QM_objects
      USE INT_objects

      implicit none
!
! Local scalars: 
      integer :: aNO, bNO, Aact, Bact, Aclosed, Bclosed, Aopen, Bopen, Nall
      integer :: aabb_index, abab_index, abba_index 
      double precision :: mb_aa, mb_bb, mb_ab, mb_ba
!
! Local arrays: 
      integer, allocatable, dimension(:) :: NO_all
!
! Functions:
      integer :: TEIindex 
!
! Begin:
      call PRG_manager ('enter', 'CFT_MB_calc', 'UTILITY')
!
! Get integrals
      call GET_object ('INT', 'EAMINT', 'AO')
      ! Calculate momentum balance from equi and antimomentum
      if(allocated(mbAO))then
        deallocate(mbAO)
      end if
      allocate(mbAO(Nbasis**4))
      ! incorporate a negative sign
      mbAO = emAO - amAO
      ! Transform to NO basis
      if(allocated(mbNO))then
        deallocate(mbNO)
      end if
      allocate(mbNO(Nbasis**4))
!
! Create list for all orbitals
      allocate(NO_all(Nbasis))
      do aNO = 1, Nbasis
        NO_all(aNO) = aNO
      end do
      Nall = Nbasis
!
      call CFT_gen_TEItrans(NO_all,Nall,NO_all,Nall,NO_all,Nall,NO_all,Nall,&
                             NO,Nbasis,mbAO,mbNO)
      deallocate(mbAO)
!
      mb_aa = zero
      mb_bb = zero
      mb_ab = zero
      mb_ba = zero
!
! Delta NO 2-RDM
!
      do Aclosed = 1, Nclosed
        aNO = NO_closed(Aclosed)
        do Bclosed = 1, Nclosed
          bNO = NO_closed(Bclosed)
          abab_index = TEIindex(Nbasis,aNO,bNO,aNO,bNO)
          abba_index = TEIindex(Nbasis,aNO,bNO,bNO,aNO)
          aabb_index = TEIindex(Nbasis,aNO,aNO,bNO,bNO)
! Coulomb
          if(aNO.ne.bNO)then
            mb_aa = mb_aa + 0.5D0*(ON(aNO)*ON(bNO) + eta(aNO,bNO))*mbNO(abab_index)
            mb_bb = mb_bb + 0.5D0*(ON(aNO)*ON(bNO) + eta(aNO,bNO))*mbNO(abab_index)
          end if
          mb_ab = mb_ab + 0.5D0*(ON(aNO)*ON(bNO) + eta(aNO,bNO))*mbNO(abab_index)
          mb_ba = mb_ba + 0.5D0*(ON(aNO)*ON(bNO) + eta(aNO,bNO))*mbNO(abab_index)
! Exchange
          if(aNO.ne.bNO)then
            mb_aa = mb_aa - 0.5D0*(ON(aNO)*ON(bNO) + eta(aNO,bNO))*mbNO(abba_index)
            mb_bb = mb_bb - 0.5D0*(ON(aNO)*ON(bNO) + eta(aNO,bNO))*mbNO(abba_index)
!
            mb_ab = mb_ab - 0.5D0*kappa(aNO,bNO)*mbNO(abba_index)
            mb_ba = mb_ba - 0.5D0*kappa(aNO,bNO)*mbNO(abba_index)
          end if
! Time-inversion exchange
          if(aNO.ne.bNO)then
            mb_ab = mb_ab + 0.5D0*(zeta(aNO,bNO) - xi(aNO,bNO))*mbNO(aabb_index)
            mb_ba = mb_ba + 0.5D0*(zeta(aNO,bNO) - xi(aNO,bNO))*mbNO(aabb_index)
          end if
!
        end do 
      end do 
!
      do Aopen = 1, Nopen
        aNO = NO_open(Aopen)
        do Bclosed = 1, Nclosed
          bNO = NO_closed(Bclosed)
!
          abab_index = TEIindex(Nbasis,aNO,bNO,aNO,bNO)
          abba_index = TEIindex(Nbasis,aNO,bNO,bNO,aNO)
! Coulomb
          mb_aa = mb_aa + (ON(aNO)*ON(bNO))*mbNO(abab_index)
!
          mb_ab = mb_ab + 0.5D0*(ON(aNO)*ON(bNO))*mbNO(abab_index)
          mb_ba = mb_ba + 0.5D0*(ON(aNO)*ON(bNO))*mbNO(abab_index)
! Exchange
          mb_aa = mb_aa - (ON(aNO)*ON(bNO))*mbNO(abba_index)
!
          mb_ab = mb_ab - kappa(aNO,bNO)*mbNO(abba_index)
          mb_ba = mb_ba - kappa(aNO,bNO)*mbNO(abba_index)
!
        end do 
      end do 
!
      do Aopen = 1, Nopen
        aNO = NO_open(Aopen)
        do Bopen = 1, Nopen
          bNO = NO_open(Bopen)
!
          abab_index = TEIindex(Nbasis,aNO,bNO,aNO,bNO)
          abba_index = TEIindex(Nbasis,aNO,bNO,bNO,aNO)
! Coulomb
          if(aNO.ne.bNO)then
            mb_aa = mb_aa + 0.5D0*(ON(aNO)*ON(bNO))*mbNO(abab_index)
          end if
! Exchange
          if(aNO.ne.bNO)then
            mb_aa = mb_aa - 0.5D0*(ON(aNO)*ON(bNO))*mbNO(abba_index)
          end if
!
        end do 
      end do 
!
      write(UNIout,'(/a)')'               Momentum-balance analytically  '
      write(UNIout,'(a)') '----------------------------------------------------------------------'
      write(UNIout,'(a)') '              same-spin                       opposite-spin           '
      write(UNIout,'(a)') '----------------------------------------------------------------------'
      write(UNIout,'(a)') '    alpha-alpha       beta-beta         alpha-beta        beta-alpha  '
      write(UNIout,'(a)') '----------------------------------------------------------------------'
      write(UNIout,'(4(f14.8,4x))')mb_aa,mb_bb,mb_ab,mb_ba
!
      call PRG_manager ('exit', 'CFT_MB_calc', 'UTILITY')
      end subroutine CFT_MB_calc
