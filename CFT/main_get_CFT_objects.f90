      subroutine GET_CFT_object (class, Objname, Modality)
!**************************************************************************
!     Date last modified: April 6, 2016                                   *
!     Author: JWH                                                         *
!     Description: Objects belonging to the class CFT.                   *
!**************************************************************************
! Modules:
      USE program_files
      USE program_objects
      USE CCSD_objects

      implicit none
!
! Input scalars:
      character*(*) :: class
      character*(*) :: Objname
      character*(*) :: Modality
!
! Local scalars:
      integer :: Object_number
!
! Begin:
      call PRG_manager ('enter', 'GET_CFT_object', 'UTILITY')

      call GET_object_number (OBJ_CFT, NCFTobjects, FirstCFT, Objname, Modality, Object_number)

      if(OBJ_CFT(Object_number)%current)then
        call PRG_manager ('exit', 'GET_CFT_object', 'UTILITY')
        return
      end if

      OBJ_CFT(Object_number)%exist=.true.
      OBJ_CFT(Object_number)%Current=.true.
      Object(ObjNum)%exist=.true.
      Object(ObjNum)%Current=.true.

      select_Object: select case (Objname)

      case ('ENERGY') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
        case ('DELTANO')
          call CFT_calc
        case ('CCSD')
          LCCsing = .true.
          call CFT_CCSD_energy
        case ('CCD')
          LCCsing = .false.
          call CFT_CCSD_energy
        case ('MP2')
          call CFT_MP2_energy
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select

      case ('R2RDM') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
        case ('DELTANO')
          call CFT_BLD_R2RDM
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select

      case ('2RDM') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
        case ('DELTANO')
          call CFT_calc
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select

      case ('NO') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
        case ('DELTANO')
          call CFT_calc
        case ('RHF')
          call CFT_RHF_SCF
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select

      case ('ON') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
        case ('DELTANO')
          call CFT_calc
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select

      case ('dynamic_Ec') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
        case ('CS')
          call CFT_CS_energy
        case ('OSEC')
          call CFT_OSEC_energy
        case ('FHC')
          call CFT_FHC_energy
        case ('TDF')
          call CFT_TDF_energy
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select

!      case ('dynamic_Veec') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!        select case (Modality)
!        case ('OSEC')
!          call CFT_OSEC_Vee_energy
!        case default
!          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
!                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
!          stop 'No such object'
!        end select
!
!      case ('dynamic_VNec') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!        select case (Modality)
!        case ('OSEC')
!          call CFT_OSEC_VNe_energy
!        case default
!          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
!                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
!          stop 'No such object'
!        end select

      case default ! Object
        write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                       '" for Modality "',Modality(1:len_trim(Modality)),'"'
        stop 'No such object'

      case ('dynamic_norm') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
        case ('OSEC')
          call CFT_OSEC_norm
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select

      case ('2EINTLR') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
        case ('OS')
          call BLD_lr2eint
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select

      case ('2EINT') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
        case ('OS')
          call BLD_2eint
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select

      end select select_Object

      call PRG_manager ('exit', 'GET_CFT_object', 'UTILITY')
      return
      end subroutine GET_CFT_object
