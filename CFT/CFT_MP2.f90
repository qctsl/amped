      subroutine CFT_MP2_energy
!*****************************************************************
!     Date last modified: September 18, 2018                     *
!     Author: JWH                                                *
!     Description: Calculate Moller-Plesst correlation           *
!                  energy for the statically correlated Delta-NO * 
!                  reference.                                    *
!*****************************************************************
!Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE CFT_objects
      USE QM_objects
      USE MP2_objects
      USE CCSD_objects

      implicit none
!
! Local scalars: 
!
      integer :: MP2iter, max_diis, n_diis, Nt2, maxr2pos(4)
      double precision :: Resmax
!
! Local arrays:
!
      double precision, allocatable, dimension(:,:) :: t2_diis, r2_diis
!
!
! Begin:
      call PRG_manager ('enter', 'CFT_MP2_energy', 'ENERGY%MP2')
!
! Get orbtials and occupancies
      call GET_object('CFT','ENERGY','DELTANO')
!
! Create lists of occupied and vacant orbitals
      call CFT_BLD_occvac_list
!
! Convert lists to spin orbitals 
      call CFT_BLD_so_lists
!
! Calculate orbital vacancy
      call CFT_BLD_vacancy
!
! Calculate occupations and vacancies of spin orbitals 
      call CFT_BLD_so_occvac
!
! Convert damping factors to spin orbitals 
      call CFT_BLD_so_damp
!
! Get necessary Fock matrix elements and ERIs
      call CFT_BLD_CCSD_OTEI
!
      write(UNIout,'(/a)')'              MP2 correlation energy'
      write(UNIout,'(a)')'----------------------------------------------------------'
      write(UNIout,'(a)')' iteration              E_c                  max residual '
      write(UNIout,'(a)')'----------------------------------------------------------'
!
      allocate(t2(Nso_occ,Nso_occ,Nso_vac,Nso_vac))
      allocate(r2(Nso_occ,Nso_occ,Nso_vac,Nso_vac))
!
! Set-up DIIS
      max_diis = 10
      Nt2 = (Nso_occ*Nso_vac)**2
      allocate(t2_diis(Nt2,max_diis),r2_diis(Nt2,max_diis))
      n_diis = 0
      t2_diis = ZERO
      r2_diis = ZERO
!
! Begin MP2 iterations
      Resmax = 66.0D0 
      MP2iter = 0
      t2 = ZERO 
! 
      do while((dabs(Resmax).gt.maxMP2res).and.(MP2iter.le.maxMP2iter))
!
! Perform DIIS extrapolation
        if(MP2iter.ge.1)then ! unlike SCF we have no guess error vector, so do an iteration first
          n_diis = min(n_diis+1,max_diis)
          ! Interpolate t2
          call DIIS_extrapolation(Nt2,Nt2,n_diis,r2_diis,t2_diis,r2,t2)
        end if 
!
! Calculate residual
!
        call CFT_BLD_r2_MP2
!
! Update amplitudes
!
        call CFT_t2_MP2_step
!
! Calculate the energy
!
        call CFT_E_MP2
! 
! Get maximum residual
!
        maxr2pos = maxloc(dabs(r2))
        Resmax = r2(maxr2pos(1),maxr2pos(2),maxr2pos(3),maxr2pos(4))
!
        MP2iter = MP2iter + 1
!
        write(UNIout,'(3X,I3,9X,F16.10,10X,F16.10)')MP2iter,E_MP2,Resmax
      !  write(UNIout,'(3X,I3,9X,F16.10,10X,F16.10,3X,F16.10)')MP2iter,E_MP2,Resmax&
      !                ,r2(maxr2pos(1),maxr2pos(2),maxr2pos(3),maxr2pos(4))
      !  write(UNIout,'(a,4I3)')'max r2: ',maxr2pos(1),maxr2pos(2),maxr2pos(3),maxr2pos(4)
      !  write(UNIout,'(a,F16.10)')'doovv: ',doovv(maxr2pos(1),maxr2pos(2),maxr2pos(3),maxr2pos(4))
!
      end do ! MP2 iterations
!
!      call CFT_E_MP2_comp
!
! end routine CFT_MP2_energy
      call PRG_manager ('exit', 'CFT_MP2_energy', 'energy%MP2')
      end subroutine CFT_MP2_energy 
      subroutine CFT_BLD_r2_MP2
!******************************************************************
!     Date last modified: October 22, 2018                        *
!     Author: JWH                                                 *
!     Description: Calculate residual of t2 equation for          *
!                  non-canonical MP2.                             *
!******************************************************************
!Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE CFT_objects
      USE MP2_objects
      USE QM_objects
      USE INT_objects
      USE CCSD_objects
!
      implicit none
!
! Local scalars: 
!
      integer :: a, b, c, i, j, k
      double precision :: tol
!
! Local arrays:
!
! Begin:
      call PRG_manager ('enter', 'CFT_BLD_r2_MP2', 'UTILITY')
!
      tol = 1.0d-16
!
! Equations based on FT-CCSD rules from White and Chan, JCTC 2018, ASAP
!
! Zero matrix elements (integrals) that correspond to an orbital being o and v simultaneously
! put this in separate subroutine if it solves the problem
!
!      do i = 1, Nso_occ
!        do a = 1, Nso_vac
!          if(so_occ(i).eq.so_vac(a))then ! spin orbital
          !if((so_occ(i)+1)/2.eq.(so_vac(a)+1)/2)then ! spatial orbital
          ! Maybe this should be based on spatial orbital not spin orbital?
          ! Actually, spatial orbital would be Delta NO specific, so we leave it to damping
!           fov(i,a) = ZERO
            ! oovv
!            oovv(i,1:Nso_occ,a,1:Nso_vac) = ZERO
!            oovv(i,1:Nso_occ,1:Nso_vac,a) = ZERO
!            oovv(1:Nso_occ,i,a,1:Nso_vac) = ZERO
!            oovv(1:Nso_occ,i,1:Nso_vac,a) = ZERO
!          end if
!        end do
!      end do
!
! Calculate residual
!
      r2 = ZERO
!
      do i = 1, Nso_occ
        do j = 1, Nso_occ
          do a = 1, Nso_vac
            do b = 1, Nso_vac
!
            if(doovv(i,j,a,b)*onso(i)*onso(j)*vnso(a)*vnso(b).lt.tol)cycle
!
              r2(i,j,a,b) = doovv(i,j,a,b)*onso(i)*onso(j)*vnso(a)*vnso(b)*oovv(i,j,a,b)
!
              do c = 1, Nso_vac
                r2(i,j,a,b) = r2(i,j,a,b) + vnso(b)*t2(i,j,a,c)*fvv(b,c)&
                                          + vnso(a)*t2(i,j,c,b)*fvv(a,c)
              end do ! c
!
              do k = 1, Nso_occ
                r2(i,j,a,b) = r2(i,j,a,b) - onso(j)*t2(i,k,a,b)*foo(j,k)&
                                          - onso(i)*t2(k,j,a,b)*foo(i,k)
              end do ! c
!
            end do ! bvac
          end do ! avac
        end do ! jocc
      end do ! iocc
!
! Zero excitations to same spin orbital
!
      do i = 1, Nso_occ
        do a = 1, Nso_vac
          if(so_occ(i).eq.so_vac(a))then ! same spin orbital
          !if((so_occ(i)+1)/2.eq.(so_vac(a)+1)/2)then ! same spatial orbital
            r2(i,1:Nso_occ,a,1:Nso_vac) = ZERO
            r2(1:Nso_occ,i,1:Nso_vac,a) = ZERO
            r2(1:Nso_occ,i,a,1:Nso_vac) = ZERO
            r2(i,1:Nso_occ,1:Nso_vac,a) = ZERO
          end if
        end do ! a
      end do ! i
! Added from CCSD
      do i = 1, Nso_occ
        r2(i,i,1:Nso_vac,1:Nso_vac) = ZERO
      end do ! i
      do a = 1, Nso_vac
        r2(1:Nso_occ,1:Nso_occ,a,a) = ZERO
      end do ! i
!
! end routine CFT_BLD_r2_MP2
      call PRG_manager ('exit', 'CFT_BLD_r2_MP2', 'UTILITY')
      end subroutine CFT_BLD_r2_MP2
      subroutine CFT_t2_MP2_step
!******************************************************************
!     Date last modified: October 25, 2018                        *
!     Author: JWH                                                 *
!     Description: Update the t2 amplitudes.                      *
!******************************************************************
!Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE CFT_objects
      USE MP2_objects
      USE QM_objects
      USE INT_objects
      USE CCSD_objects

      implicit none
!
! Local scalars: 
!
      integer :: a, b, i, j
!
! Local arrays:
!
! Begin:
      call PRG_manager ('enter', 'CFT_t2_MP2_step', 'UTILITY')
!
      do i = 1, Nso_occ
        do a = 1, Nso_vac
          if(so_occ(i).eq.so_vac(a))cycle ! same spin orbital
!
          do j = 1, Nso_occ
!            if(i.eq.j)cycle
            if(so_occ(j).eq.so_vac(a))cycle ! same spin orbital
            do b = 1, Nso_vac
!              if(a.eq.b)cycle
              if(so_occ(j).eq.so_vac(b))cycle ! same spin orbital
              if(so_occ(i).eq.so_vac(b))cycle ! same spin orbital
!
              t2(i,j,a,b) = t2(i,j,a,b)&
              + r2(i,j,a,b)/(onso(i)*foo(i,i) + onso(j)*foo(j,j) - vnso(a)*fvv(a,a) - vnso(b)*fvv(b,b))
!
            end do ! bvac
          end do ! avac
        end do ! jocc
      end do ! iocc
!
! end routine CFT_t2_MP2_step
      call PRG_manager ('exit', 'CFT_t2_MP2_step', 'UTILITY')
      end subroutine CFT_t2_MP2_step
      subroutine CFT_E_MP2
!******************************************************************
!     Date last modified: October 25, 2018                        *
!     Author: JWH                                                 *
!     Description: Calculate MP2 energy.                          *
!******************************************************************
!Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE CFT_objects
      USE MP2_objects
      USE QM_objects
      USE INT_objects
      USE CCSD_objects

      implicit none
!
! Local scalars: 
!
      integer :: a, b, i, j
!
! Local arrays:
!
!
! Begin:
      call PRG_manager ('enter', 'CFT_E_MP2', 'UTILITY')
!
      E_MP2 = ZERO
      do i = 1, Nso_occ
        do a = 1, Nso_vac
          do j = 1, Nso_occ
            do b = 1, Nso_vac
!
              E_MP2 = E_MP2 + F1_4*t2(i,j,a,b)*oovv(i,j,a,b)
!
            end do ! bvac
          end do ! avac
        end do ! jocc
      end do ! iocc
!
! end routine CFT_E_MP2
      call PRG_manager ('exit', 'CFT_E_MP2', 'UTILITY')
      end subroutine CFT_E_MP2
      subroutine CFT_E_MP2_comp
!******************************************************************
!     Date last modified: October 29, 2018                        *
!     Author: JWH                                                 *
!     Description: Calculate the components of the MP2 energy.    *
!******************************************************************
!Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE CFT_objects
      USE MP2_objects
      USE QM_objects
      USE INT_objects
      USE CCSD_objects

      implicit none
!
! Local scalars:
!
      integer :: a, b, i, j, Nso_act
      double precision :: Eoovv, Eooav, Eooaa, Eaovv, Eaoav, Eaova, Eaavv, Eaoaa, Eaaav, Eaaaa
!
! Local arrays:
!
! Begin:
      call PRG_manager ('enter', 'CFT_E_MP2_comp', 'UTILITY')
!
      Nso_act = 2*Nact
!
! oovv
      Eoovv = ZERO
!
      do b = Nso_act+1, Nso_vac   ! vacant
        do a = Nso_act+1, Nso_vac ! vacant
          do j = 1, Nso_occ-Nso_act ! occupied
            do i = 1, Nso_occ-Nso_act ! occupied
!
              Eoovv = Eoovv + F1_4*t2(i,j,a,b)*oovv(i,j,a,b)
!
            end do ! i
          end do ! j
        end do ! a
      end do ! b
!
! ooav
      Eooav = ZERO
!
      do b = Nso_act+1, Nso_vac   ! vacant
        do a = 1, Nso_act ! active 
          do j = 1, Nso_occ-Nso_act ! occupied
            do i = 1, Nso_occ-Nso_act ! occupied
!
              Eooav = Eooav + F1_4*t2(i,j,a,b)*oovv(i,j,a,b)
              Eooav = Eooav + F1_4*t2(j,i,b,a)*oovv(j,i,b,a)
!
            end do ! i
          end do ! j
        end do ! a
      end do ! b
!
! ooaa
      Eooaa = ZERO
!
      do b = 1, Nso_act ! active 
        do a = 1, Nso_act ! active 
          do j = 1, Nso_occ-Nso_act ! occupied
            do i = 1, Nso_occ-Nso_act ! occupied
!
              Eooaa = Eooaa + F1_4*t2(i,j,a,b)*oovv(i,j,a,b)
!
            end do ! i
          end do ! j
        end do ! a
      end do ! b
!
! aovv
      Eaovv = ZERO
!
      do b = Nso_act+1, Nso_vac   ! vacant
        do a = Nso_act+1, Nso_vac ! vacant
          do j = 1, Nso_occ-Nso_act ! occupied
            do i = Nso_occ-Nso_act+1, Nso_occ ! active 
!
              Eaovv = Eaovv + F1_4*t2(i,j,a,b)*oovv(i,j,a,b)
              Eaovv = Eaovv + F1_4*t2(j,i,b,a)*oovv(j,i,b,a)
!
            end do ! i
          end do ! j
        end do ! a
      end do ! b
!
! aoav
      Eaoav = ZERO
!
      do b = Nso_act+1, Nso_vac   ! vacant
        do a = 1, Nso_act ! active 
          do j = 1, Nso_occ-Nso_act ! occupied
            do i = Nso_occ-Nso_act+1, Nso_occ ! active 
!
              Eaoav = Eaoav + F1_4*t2(i,j,a,b)*oovv(i,j,a,b)
              Eaoav = Eaoav + F1_4*t2(j,i,b,a)*oovv(j,i,b,a)
!
            end do ! i
          end do ! j
        end do ! a
      end do ! b
!
! aova
      Eaova = ZERO
!
      do b = 1, Nso_act ! active 
        do a = Nso_act+1, Nso_vac   ! vacant
          do j = 1, Nso_occ-Nso_act ! occupied
            do i = Nso_occ-Nso_act+1, Nso_occ ! active 
!
              Eaova = Eaova + F1_4*t2(i,j,a,b)*oovv(i,j,a,b)
              Eaova = Eaova + F1_4*t2(j,i,b,a)*oovv(j,i,b,a)
!
            end do ! i
          end do ! j
        end do ! a
      end do ! b
!
! aavv
      Eaavv = ZERO
!
      do b = Nso_act+1, Nso_vac   ! vacant
        do a = Nso_act+1, Nso_vac   ! vacant
          do j = Nso_occ-Nso_act+1, Nso_occ ! active 
            do i = Nso_occ-Nso_act+1, Nso_occ ! active 
!
              Eaavv = Eaavv + F1_4*t2(i,j,a,b)*oovv(i,j,a,b)
!
            end do ! i
          end do ! j
        end do ! a
      end do ! b
!
! aoaa
      Eaoaa = ZERO
!
      do b = 1, Nso_act ! active 
        do a = 1, Nso_act ! active 
          do j = 1, Nso_occ-Nso_act ! occupied
            do i = Nso_occ-Nso_act+1, Nso_occ ! active 
!
              Eaoaa = Eaoaa + F1_4*t2(i,j,a,b)*oovv(i,j,a,b)
              Eaoaa = Eaoaa + F1_4*t2(j,i,b,a)*oovv(j,i,b,a)
!
            end do ! i
          end do ! j
        end do ! a
      end do ! b
!
! aaav
      Eaaav = ZERO
!
      do b = Nso_act+1, Nso_vac   ! vacant
        do a = 1, Nso_act ! active 
          do j = Nso_occ-Nso_act+1, Nso_occ ! active 
            do i = Nso_occ-Nso_act+1, Nso_occ ! active 
!
              Eaaav = Eaaav + F1_4*t2(i,j,a,b)*oovv(i,j,a,b)
              Eaaav = Eaaav + F1_4*t2(j,i,b,a)*oovv(j,i,b,a)
!
            end do ! i
          end do ! j
        end do ! a
      end do ! b
!
! aaaa
      Eaaaa = ZERO
!
      do b = 1, Nso_act ! active 
        do a = 1, Nso_act ! active 
          do j = Nso_occ-Nso_act+1, Nso_occ ! active 
            do i = Nso_occ-Nso_act+1, Nso_occ ! active 
!
              Eaaaa = Eaaaa + F1_4*t2(i,j,a,b)*oovv(i,j,a,b)
!
            end do ! i
          end do ! j
        end do ! a
      end do ! b
!
      write(6,*) 'Decomposed MP2 energy'
      write(6,'(a,F16.10)')'Eoovv = ',Eoovv
      write(6,'(a,F16.10)')'Eooav = ',Eooav
      write(6,'(a,F16.10)')'Eaovv = ',Eaovv
      write(6,'(a,F16.10)')'Eooaa = ',Eooaa
      write(6,'(a,F16.10)')'Eaoav = ',Eaoav
      write(6,'(a,F16.10)')'Eaova = ',Eaova
      write(6,'(a,F16.10)')'Eaavv = ',Eaavv
      write(6,'(a,F16.10)')'Eaoaa = ',Eaoaa
      write(6,'(a,F16.10)')'Eaaav = ',Eaaav
      write(6,'(a,F16.10)')'Eaaaa = ',Eaaaa
      write(6,'(a,F16.10)')'Etot  = ',Eoovv+Eooav+Eooaa+Eaovv+Eaoav+Eaova+Eaavv+Eaoaa+Eaaav+Eaaaa
!
! end routine CFT_E_MP2_comp
      call PRG_manager ('exit', 'CFT_E_MP2_comp', 'UTILITY')
      end subroutine CFT_E_MP2_comp
      subroutine CFT_BLD_occvac_list
!***********************************************************************
!     Date last modified: October 19, 2018                             *
!     Author: JWH                                                      *
!     Description: Create lists of "occupied" and "vacant" orbitals   *
!                  for post-HF-like calculations.                      *
!***********************************************************************
! Modules:
      USE CFT_objects
      USE QM_objects
      USE type_molecule
      USE program_constants
      USE program_files

      implicit none
!
! Local scalars:
      integer :: INO, Iocc, Ivac
! Local arrays:
!
! Begin:
!
      call PRG_manager('enter','CFT_BLD_occvac_list','UTILITY')
!
      Nocc = NAelectrons + Nvac_act
      Nvac = Nbasis - NAelectrons + Nocc_act
!
      if(.not.allocated(NO_occ))then 
        allocate(NO_occ(Nocc))
      end if
      if(.not.allocated(NO_vac))then 
        allocate(NO_vac(Nvac))
      end if
!
! Occupied
      Iocc = 0
      do INO = 1, NAelectrons
        Iocc = Iocc + 1
        NO_occ(Iocc) = INO
      end do ! INO
      do Ivac = 1, Nvac_act
        Iocc = Iocc + 1
        NO_occ(Iocc) = vac_act(Ivac)
      end do
! Virtual
      Ivac = 0
      do Iocc = 1, Nocc_act
        Ivac = Ivac + 1
        NO_vac(Ivac) = occ_act(Iocc) 
      end do
      do INO = NAelectrons + 1, Nbasis
        Ivac = Ivac + 1
        NO_vac(Ivac) = INO 
      end do
!
! end routine CFT_BLD_occvac_list
      call PRG_manager ('exit', 'CFT_BLD_occvac_list', 'UTILITY')
      end subroutine CFT_BLD_occvac_list
      subroutine CFT_BLD_vacancy
!***********************************************************************
!     Date last modified: November 6, 2018                             *
!     Author: JWH                                                      *
!     Description: Calculate the orbital vacancies (holes).            *
!***********************************************************************
! Modules:
      USE CFT_objects
      USE QM_objects
      USE program_constants

      implicit none
!
! Local scalars:
!
! Local arrays:
!
! Begin:
!
      call PRG_manager('enter','CFT_BLD_vacancy','UTILITY')
!
      if(allocated(vn))then
        deallocate(vn)
      end if
      allocate(vn(Nbasis)) 
!
      vn = ONE - on
!
! end routine CFT_BLD_vacancy
      call PRG_manager ('exit', 'CFT_BLD_vacancy', 'UTILITY')
      end subroutine CFT_BLD_vacancy
