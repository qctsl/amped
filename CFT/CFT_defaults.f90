      subroutine SET_defaults_CFT
!**********************************************************************************************
!     Date last modified: March 29, 2016                                         Version 1.0  *
!     Author: J.W. Hollett                                                                    *
!     Description: Set CFT defaults.                                                          *
!**********************************************************************************************
! Modules:
      USE CFT_objects
      USE QM_objects
      USE CCSD_objects
      USE MP2_objects
!
! Begin:
!
! Defaults:
      Nstat = 0
      dynamic = 'OF'
      NO_guess = 'RHF'
      NOfile = 'NO.dat'
      Delta_guess = 'RHF'
      Deltafile = 'Delta.dat'
      epsON = 1.0D-05
      epslambda = 1.0D-05
      epsE = 1.0D-08
      max_NOstep = 100
      max_ONstep = 100
      max_NOONstep = 200
      max_RDMstep = 800
      ONtol = 1.0D-08
      max_grad_length = 1.0D-05
      optmethod = 'SIMUL'
      Lnudge = .true.
      LTDFvar = .false.
      N_u_pts = 6
      u_scale = 2.0D0
!
      maxSCF = 100
      SCFconv = 1.0D-6
!
      qCS = 2.29D0    ! rho^(1/3) - Colle and Salvetti
      qOS = 2.54D0   ! rho^(1/3) - best fit (IE and EA) and high Z limit
      aOS = 1.0D0   ! singlet cusp condition
      E_CS = 0.0D0
      E_OSEC = 0.0D0
!
!      cDC = 0.417377D0 ! Split gaussian model
!      cDC = 0.38D0 ! Split gaussian model
!      cDC = 0.235D0 ! CS value from H2
!
      qFHC = 2.2D0 ! L^1/8 - best fit (IE and EA) (not anymore)
!      qFHC = 2.65D0 ! L^1/8 - high Z limit
      aFHC = 0.5D0  ! triplet cusp condition
      Vfac = 1.0D0  ! virial factor (1 = no virial, 0.5 = virial)
      E_FHCaa = 0.0D0
      E_FHCbb = 0.0D0
!
!      aAC = 0.3D0   ! angular correlation strength (full u range)
      aAC = 0.406D0   ! angular correlation strength  ( 0 < u < 2R)
      dAC = 0.8D0   ! angular correlation decay    
      pAC = 0.667D0 ! cusp blending partition
!
      maxCCSDiter = 100
      maxCCSDres = 1D-07
      E_CCSD = 0.0D0
!
      maxMP2iter = 200
      maxMP2res = 1D-07
      E_MP2 = 0.0D0
!
      return
      end subroutine SET_defaults_CFT
