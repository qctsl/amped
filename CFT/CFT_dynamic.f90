      subroutine CFT_CS_energy
!*********************************************************************************************
!     Date last modified: April 12, 2016                                                     *
!     Author: JWH                                                                            *
!     Desciption: Compute the Colle-Salvetti correlation energy.                             *
!*********************************************************************************************
! Modules:
      USE program_constants
      USE program_timing
      USE program_files
      USE type_molecule
      USE QM_objects
      USE CFT_objects
      USE module_grid_points

      implicit none
!
! Input scalars:
      integer :: Iatom,Ifound,NApoints_tot,Znum,IApoint
!
! Local scalars:
      double precision :: t0,t1,t2,time
!
! Begin:
      call PRG_manager ('enter', 'CFT_CS_energy', 'dynamic_Ec%CS')
!
! Not sure what to get there (CFT energy?)
!      call GET_object ('QM', 'CMO', Wavefunction)
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
!
      call CPU_time(t0)
      call CPU_time(t1)      

      call BLD_BeckeW_XI
      call INI_CS_energy
!
      call CFT_BLD_R2RDM
!
      NApoints_tot = 0
      do Iatom=1,Natoms
        Znum=CARTESIAN(Iatom)%Atomic_number
        if(Znum.le.0)cycle
        Ifound=GRID_loc(Znum)
        NApts_atom=NApoints_atom(Ifound)
!
        NApoints_tot = NApoints_tot + NApts_atom
!
! Generate a temporary array of grid points
        allocate(grid_points(NApts_atom))
        do IApoint=1,NApts_atom
          grid_points(IApoint)%X=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          grid_points(IApoint)%Y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          grid_points(IApoint)%Z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
          grid_points(IApoint)%w=Egridpts(Ifound,IApoint)%w
        end do ! IApoint
!
! Calculate the atomic contribution to E_CS
        call CS_energy (Iatom, Ifound)
!
        deallocate(grid_points)
!
      end do ! Iatom
!
      if(MUN_prtlev.gt.0)write(UNIout,'(a,i8/)')'Total number of grid points: ',NApoints_tot
!
      write(UNIout,'(a,f20.12)')'Colle-Salvetti energy   = ',E_CS


      call CPU_time(t2)
      time = t2 - t1
!
! End of routine CFT_CS_energy
      call PRG_manager ('exit', 'CFT_CS_energy', 'dynamic_Ec%CS')
      end subroutine CFT_CS_energy
      subroutine CS_energy (Iatom, Ifound)
!************************************************************************************
!     Date last modified: April 12, 2016                                            *
!     Author: JWH                                                                   *
!     Description: Calculate the Colle-Salvetti corrleation energy, from an         *
!                  uncorrelated on-top 2-RDM. (Phys.Rev.B. 1988, 37, 785)           *
!                                             (Theo. Chim. Acta. 1975, 37, 329)     *
!************************************************************************************
! Modules:
      USE program_constants
      USE N_integration
      USE type_molecule
      USE type_density 
      USE module_grid_points
      USE NI_defaults
      USE type_basis_set
      USE QM_objects
      USE CFT_objects

      implicit none

! Input scalars:
      integer :: Iatom,Ifound
!
! Local scalars:
      integer :: IApoint, Aocc, Bocc, Iocc, Rvac, Svac
      integer :: SNO, ANO, BNO, INO, RNO, Istat
      double precision :: BWeight,XApt,YApt,ZApt,WApt, rho, sumE_CS_r, Lap, pi, sig2
      double precision :: a,b,c,d, Gii, G0ii
      double precision :: phi2_a, phi2_b, G, E_CS_r, phi2_i, phi2_r, phi2_s
      double precision :: Cab, Eab, Rv(3)
      double precision :: d2phi_a, d2phi_b, phi_a, phi_b, dphi2_a, dphi2_b
      double precision :: dphix_a, dphiy_a, dphiz_a, dphix_b, dphiy_b, dphiz_b
!
! Local arrays:
      double precision, allocatable, dimension(:) :: G0ow, Gow
      double precision, allocatable, dimension(:) :: phi, d2phi
      double precision, allocatable, dimension(:,:) :: dphi
!
! Begin: 
!
      allocate(G0ow(Nbasis),Gow(Nbasis))
      allocate(phi(Nbasis),dphi(Nbasis,3),d2phi(Nbasis))
!
      a = 0.049D0
      b = 0.132D0
      c = 0.2533D0
      d = 0.349D0
      pi = PI_VAL
!
      sumE_CS_r = ZERO
      do IApoint=1,NApts_atom
          XApt=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          YApt=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          ZApt=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
          WApt=Egridpts(Ifound,IApoint)%w
          ! Get Becke weights
          call BeckeW1 (XApt, YApt, ZApt, Iatom, Bweight)
          ! Get density
          call GET_rho_Atom (XApt, YApt, ZApt, rho)
!
        if(rho.lt.TOL2)then
!
          E_CS_r = ZERO
!
!        else if(rho.lt.TOL1)then
        ! This assumes that for very small rho, 2RDM is approx 1/4 rho^2 (really an upper bound)
        ! Need to work out this for the new formulation, but this may be OK
!        
!          E_CS_r = Bweight*WApt&
!                  *(-a)*rho**(F4_3)/(rho**(F1_3)+d)
!
        else
!
        ! Get orbital values and derivatives
        Rv(1) = XApt
        Rv(2) = YApt
        Rv(3) = ZApt
        call     GET_MO_point(Rv, Nbasis, NO, phi)
        call GET_MOgrad_point(Rv, Nbasis, NO, dphi)
        call  GET_MOlap_point(Rv, Nbasis, NO, d2phi)
!
        ! Calculate "on-top" 2-RDM at IApoint (uncorrelated and correlated components)
        G = ZERO   ! total on-top density
        Gow = ZERO ! orbital-wise on-top density
!
        do Aocc = 1, Nocc
          aNO = NO_occ(Aocc)
          phi2_a = phi(aNO)**2
!
          do Bocc = 1, Nocc
            bNO = NO_occ(Bocc)
            phi2_b = phi(bNO)**2
!
            G = G + (R2RDMJab(aNO,bNO) + R2RDMJba(aNO,bNO) + R2RDMLab(aNO,bNO) + R2RDMLba(aNO,bNO))&
                   *phi2_a*phi2_b 
!
          end do ! Bocc
        end do ! Aocc
!
! Orbital-wise (ow) alpha-beta on-top density
        Gow = ZERO
!
        do Iocc = 1, Nocc_act
          iNO = occ_act(Iocc)
          phi2_i = phi(iNO)**2
!
          Gow(iNO) = Gow(iNO) + ON(iNO)*phi2_i**2
!
          do Rvac = 1, Nstat_occ(iNO)
            rNO = occ_stat(iNO,Rvac)
            phi2_r = phi(rNO)**2
!
            Gow(iNO) = Gow(iNO) + Delta(iNO,rNO)*phi2_r**2
!
          end do ! Rvac
        end do ! Iocc
!
        G0ow = Gow
!
        do Iocc = 1, Nocc_act
          iNO = occ_act(Iocc)
          do Rvac = 1, Nstat_occ(iNO)
            rNO = occ_stat(iNO,Rvac)
            phi2_r = phi(rNO)**2
!
            do Svac = 1, Nstat_occ(iNO)
              if(Rvac.eq.Svac)cycle
              sNO = occ_stat(iNO,Svac)
              phi2_s = phi(sNO)**2
!
              Gow(iNO) = Gow(iNO) + dsqrt(Delta(iNO,rNO)*Delta(iNO,sNO))*phi2_r*phi2_s
!
            end do ! Svac
          end do ! Rvac
        end do ! Iocc
!
        do Istat = 1, Nstat
          iNO = stat_pair(Istat,1)
          rNO = stat_pair(Istat,2)
          phi2_i = phi(iNO)**2
          phi2_r = phi(rNO)**2
!          
          Gow(iNO) = Gow(iNO) - two*xi(iNO,rNO)*phi2_i*phi2_r
!
        end do ! Istat
!
        ! Calculate Laplacian of "on-top" 2-RDM at IApoint
!
        Lap = ZERO
!
        do Aocc = 1, Nocc
          aNO = NO_occ(Aocc)
          phi_a = phi(aNO)
          dphix_a = dphi(aNO,1)
          dphiy_a = dphi(aNO,2)
          dphiz_a = dphi(aNO,3)
          d2phi_a = d2phi(aNO)
          dphi2_a = dphix_a**2 + dphiy_a**2 + dphiz_a**2
!
          do Bocc = 1, Nocc
            bNO = NO_occ(Bocc)
            phi_b = phi(bNO)
            dphix_b = dphi(bNO,1)
            dphiy_b = dphi(bNO,2)
            dphiz_b = dphi(bNO,3)
            d2phi_b = d2phi(bNO)
            dphi2_b = dphix_b**2 + dphiy_b**2 + dphiz_b**2
!
            Cab = F1_2*(phi_a**2*dphi2_b + phi_b**2*dphi2_a + phi_a*phi_b**2*d2phi_a&
                         + phi_b*phi_a**2*d2phi_b&
                         - FOUR*phi_a*phi_b*(dphix_a*dphix_b + dphiy_a*dphiy_b + dphiz_a*dphiz_b))
!
            Eab = F1_2*(phi_a**2*phi_b*d2phi_b + phi_b**2*phi_a*d2phi_a&
                         - phi_a**2*dphi2_b - phi_b**2*dphi2_a)
!
            Lap = Lap + (R2RDMaa(aNO,bNO) + R2RDMbb(aNO,bNO))*(Cab - Eab)&
                      + (R2RDMJab(aNO,bNO) + R2RDMJba(aNO,bNO))*Cab&
                      + (R2RDMLab(aNO,bNO) + R2RDMLba(aNO,bNO))*Eab
!
          end do ! Bocc
        end do ! Aocc
!
! Correlation-length (for DC)
        sig2 = (qCS*rho**F1_3)**2
!
! Colle-Salvetti (see open-shell paper and LYP paper)
!
        E_CS_r = -FOUR*a*G/rho*(ONE + b*rho**(-F8_3)*Lap*dexp(-c*rho**(-F1_3)))&
                  /(ONE + d*rho**(-F1_3))
!
! double-counting correction
        do Iocc = 1, Nocc_act
          iNO = occ_act(Iocc)
!
          G0ii = G0ow(iNO)
          Gii = Gow(iNO)
!
          E_CS_r = E_CS_r + two*pi*cDC*Gii/G0ii*(G0ii - Gii)/sig2
!
        end do ! Iocc
!
        E_CS_r = Bweight*WApt*E_CS_r
!
        end if ! rho < TOL
!
! Add integration point to atomic contribution
        sumE_CS_r = sumE_CS_r + E_CS_r
!
      end do ! IApoint
!
      E_CS_atom(Iatom) = E_CS_atom(Iatom) + FourPi*sumE_CS_r
      E_CS = E_CS + E_CS_atom(Iatom)
!
      end subroutine CS_energy
      subroutine INI_CS_energy
!************************************************************************************
!     Date last modified: April 16, 2016                                            *
!     Author: JWH                                                                   *
!     Description: Initialization for CS energy.                                    *
!************************************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE CFT_objects

      implicit none

! Begin:
      if(allocated(E_CS_atom))then
        deallocate(E_CS_atom)
      end if
      allocate(E_CS_atom(Natoms))
      E_CS_atom = ZERO 
      E_CS = ZERO

      end subroutine INI_CS_energy
      subroutine GET_ERpot_NO (rx, ry, rz)
!*********************************************************************
!   Date last modified: August 15, 2018                              *
!   Author: JWH                                                      *
!   Description: Calculate the electron-repulsion potential for each *
!                NO pair at the given point.                         * 
!*********************************************************************
! Modules:
      USE QM_objects
      USE INT_objects
      USE CFT_objects

      implicit none
!
! Input scalars:
      double precision :: rx, ry, rz
!
! Begin:
!
        allocate(ERpot(MATlen))
!
        call I1E_ERpot_val (rx, ry, rz)
        call I1E_transform (ERpot, NO, ERpotNO, Nbasis, Nbasis, MATlen)
!
        deallocate(ERpot)
!
      end subroutine GET_ERpot_NO
      subroutine CFT_OSEC_energy
!*********************************************************************************************
!     Date last modified: May 14, 2016                                                       *
!     Author: JWH                                                                            *
!     Desciption: Compute the opposite-spin exponential cusp correlation energy.             *
!*********************************************************************************************
! Modules:
      USE program_constants
      USE program_timing
      USE program_files
      USE N_integration
      USE type_molecule
      USE QM_objects
      USE CFT_objects

      implicit none
!
! Input scalars:
      integer :: Iatom,Ifound,NApoints_tot,Znum,IApoint
!
! Local scalars:
      double precision :: t0,t1,t2,time
!
! Begin:
      call PRG_manager ('enter', 'CFT_OSEC_energy', 'dynamic_Ec%OSEC')
!
! Not sure what to get there (CFT energy?)
!      call GET_object ('QM', 'CMO', Wavefunction)
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
!
      call CPU_time(t0)
      call CPU_time(t1)      

      call BLD_BeckeW_XI
      call INI_OSEC_energy
!
      call CFT_BLD_R2RDM
!
      NApoints_tot = 0
      do Iatom=1,Natoms
        Znum=CARTESIAN(Iatom)%Atomic_number
        if(Znum.le.0)cycle
        Ifound=GRID_loc(Znum)
        NApts_atom=NApoints_atom(Ifound)
!
        NApoints_tot = NApoints_tot + NApts_atom
!
! Generate a temporary array of grid points
        allocate(grid_points(NApts_atom))
        do IApoint=1,NApts_atom
          grid_points(IApoint)%X=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          grid_points(IApoint)%Y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          grid_points(IApoint)%Z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
          grid_points(IApoint)%w=Egridpts(Ifound,IApoint)%w
        end do ! IApoint
!
! Calculate the atomic contribution to E_OSEC
        if(NBelectrons.gt.0)then
!
        call OSEC_energy (Iatom, Ifound)
!
        end if
!
        deallocate(grid_points)
!
      end do ! Iatom
!
      if(MUN_prtlev.gt.0)write(UNIout,'(a,i8/)')'Total number of grid points: ',NApoints_tot
!
      write(UNIout,'(a,f20.12)')'Opposite-spin exponential cusp energy      = ',E_OSEC
!
      call CPU_time(t2)
      time = t2 - t1
!
! End of routine CFT_OSEC_energy
      call PRG_manager ('exit', 'CFT_OSEC_energy', 'dynamic_Ec%OSEC')
      end subroutine CFT_OSEC_energy
      subroutine OSEC_energy (Iatom, Ifound)
!************************************************************************************
!     Date last modified: February 17, 2017                                         *
!     Author: JWH                                                                   *
!     Description: Calculate the OSEC correlation energy, from an                   *
!                  uncorrelated on-top 2-RDM.                                       *
!************************************************************************************
! Modules:
      USE program_constants
      USE N_integration
      USE type_molecule
      USE type_density 
      USE module_grid_points
      USE NI_defaults
      USE type_basis_set
      USE QM_objects
      USE CFT_objects

      implicit none

! Input scalars:
      integer :: Iatom,Ifound
!
! Local scalars:
      integer :: IApoint, Rvac, Svac, Aocc, Bocc
      integer :: Iocc, SNO, ANO, BNO, INO, RNO, Istat
      double precision :: BWeight,XApt,YApt,ZApt,WApt, rho
      double precision :: phi2_a, phi2_b, phi2_i, phi2_r
      double precision :: phi2_s
      double precision :: lam, lam2, expo, pi, sqrtpi, erfo
      double precision :: G, Gii, G0ii, Rv(3)
      double precision :: sumE_OSEC_r, E_OSEC_r, f_OSEC_r
!
! Local arrays:
      double precision, allocatable, dimension(:) :: Gow, G0ow
      double precision, allocatable, dimension(:) :: phi
!
! Functions:
      double precision, external :: derf
!
! Begin: 
!
      allocate(Gow(Nbasis),G0ow(Nbasis))
      allocate(phi(Nbasis))
!
      pi = PI_VAL
      sqrtpi = dsqrt(PI_VAL)
!
      sumE_OSEC_r = ZERO
      do IApoint=1,NApts_atom
        XApt=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
        YApt=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
        ZApt=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
        WApt=Egridpts(Ifound,IApoint)%w
        ! Get Becke weights
        call BeckeW1 (XApt, YApt, ZApt, Iatom, Bweight)
        ! Get density
        call GET_rho_Atom (XApt, YApt, ZApt, rho)
!
        if(rho.lt.TOL2)then
!
          E_OSEC_r = ZERO
!
        else if(rho.lt.TOL1)then
        ! This assumes that for very small rho, G is approx 1/4 rho^2 (really an upper bound)
        ! and that aOS (alpha) is 1
!        
!          E_OSEC_r = Bweight*WApt&
!                  *(-pi*rho0**F4_3 + pi**F3_2*qOSaa*rho0**F5_3 - TWO*pi*qOSaa**2*rho0**2)&
!                  /(TWO*qOSaa**2 + FOUR*qOSaa**4*rho0**F2_3)
          E_OSEC_r = ZERO
!
        else
!
        ! Get orbital values
        Rv(1) = XApt
        Rv(2) = YApt
        Rv(3) = ZApt
        call GET_MO_point(Rv, Nbasis, NO, phi)
!
! Calculate "on-top" 2-RDM at IApoint (uncorrelated and correlated components)
!
        G = ZERO
!
! Total alpha-beta on-top density
!
        do Aocc = 1, Nocc
          aNO = NO_occ(Aocc)
          phi2_a = phi(aNO)**2
!
          do Bocc = 1, Nocc
            bNO = NO_occ(Bocc)
            phi2_b = phi(bNO)**2
!
            G = G + (R2RDMJab(aNO,bNO) +R2RDMJba(aNO,bNO) + R2RDMLab(aNO,bNO) + R2RDMLba(aNO,bNO))&
                   *phi2_a*phi2_b
!
          end do ! Bocc
        end do ! Aocc
!
! Orbital-wise (ow) alpha-beta on-top density
        Gow = ZERO
!
        do Iocc = 1, Nocc_act
          iNO = occ_act(Iocc)
          phi2_i = phi(iNO)**2
!
          Gow(iNO) = Gow(iNO) + ON(iNO)*phi2_i**2
!
          do Rvac = 1, Nstat_occ(iNO)
            rNO = occ_stat(iNO,Rvac)
            phi2_r = phi(rNO)**2
!
            Gow(iNO) = Gow(iNO) + Delta(iNO,rNO)*phi2_r**2
!
          end do ! Rvac
        end do ! Iocc
!
        G0ow = Gow
!
        do Iocc = 1, Nocc_act
          iNO = occ_act(Iocc)
          do Rvac = 1, Nstat_occ(iNO)
            rNO = occ_stat(iNO,Rvac)
            phi2_r = phi(rNO)**2
!
            do Svac = 1, Nstat_occ(iNO)
              if(Rvac.eq.Svac)cycle
              sNO = occ_stat(iNO,Svac)
              phi2_s = phi(sNO)**2
!
              Gow(iNO) = Gow(iNO) + dsqrt(Delta(iNO,rNO)*Delta(iNO,sNO))*phi2_r*phi2_s
!
            end do ! Svac
          end do ! Rvac
        end do ! Iocc
!
        do Istat = 1, Nstat
          iNO = stat_pair(Istat,1)
          rNO = stat_pair(Istat,2)
          phi2_i = phi(iNO)**2
          phi2_r = phi(rNO)**2
!          
          Gow(iNO) = Gow(iNO) - two*xi(iNO,rNO)*phi2_i*phi2_r
!
        end do ! Istat
!
! Opposite-spin exponential cusp energy
!
! Uncorrelated 2-RDM (G0)
! Correlated 2-RDM (G)
!
        E_OSEC_r = ZERO
!
! intermediates
        lam = qOS*rho**F1_3
        !lam = qOS*G**F1_6
        lam2 = lam**2
!
! depend on aOS
        expo = dexp(-aOS**2/(FOUR*lam2))
        erfo = ONE + derf(aOS/(TWO*lam))
!
! exponential cusp
        f_OSEC_r = Vfac*TWO*pi*(TWO*lam*(sqrtpi*lam - aOS)*expo + sqrtpi*(sqrtpi*aOS*lam - aOS**2&
                 - TWO*lam2)*erfo)/(lam2*(TWO*aOS*lam*expo&
                 + sqrtpi*(aOS**2 + TWO*lam2)*erfo))
!
        E_OSEC_r = E_OSEC_r + f_OSEC_r*G
!
! double-counting correction
        do Iocc = 1, Nocc_act
          iNO = occ_act(Iocc)
!
          G0ii = G0ow(iNO)
          Gii  = Gow(iNO)
!
          E_OSEC_r = E_OSEC_r + two*pi*cDC*Gii/G0ii*(G0ii - Gii)/lam2
!
        end do ! Iocc
!
        E_OSEC_r = Bweight*WApt*E_OSEC_r
!
        end if ! rho < TOL
!
! Add integration point to atomic contribution
        sumE_OSEC_r = sumE_OSEC_r + E_OSEC_r
!
      end do ! IApoint
!
      E_OSEC_atom(Iatom) = E_OSEC_atom(Iatom) + FourPi*sumE_OSEC_r
      E_OSEC = E_OSEC + E_OSEC_atom(Iatom)
!
      end subroutine OSEC_energy
      subroutine INI_OSEC_energy
!************************************************************************************
!     Date last modified: May 14, 2016                                              *
!     Author: JWH                                                                   *
!     Description: Initialization for OSEC energy.                                  *
!************************************************************************************
! Modules:
      USE program_constants
      USE QM_objects
      USE type_molecule
      USE CFT_objects

      implicit none

! Begin:
      if(allocated(E_OSEC_atom))then
        deallocate(E_OSEC_atom)
      end if
      allocate(E_OSEC_atom(Natoms))
      E_OSEC_atom = ZERO 
      E_OSEC = ZERO

      end subroutine INI_OSEC_energy
      subroutine CFT_FHC_energy
!*********************************************************************************************
!     Date last modified: April 12, 2016                                                     *
!     Author: JWH                                                                            *
!     Desciption: Compute the Fermi-hole correction correlation energy.                      *
!*********************************************************************************************
! Modules:
      USE program_constants
      USE program_timing
      USE program_files
      USE N_integration
      USE type_molecule
      USE QM_objects
      USE CFT_objects

      implicit none
!
! Input scalars:
      integer :: Iatom,Ifound,NApoints_tot,Znum,IApoint
!
! Local scalars:
      double precision :: t0,t1,t2,time
!
! Begin:
      call PRG_manager ('enter', 'CFT_FHC_energy', 'dynamic_Ec%FHC')
!
! Not sure what to get there (CFT energy?)
!      call GET_object ('QM', 'CMO', Wavefunction)
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
!
      call CPU_time(t0)
      call CPU_time(t1)      

      call BLD_BeckeW_XI
      call INI_FHC_energy
!
      call CFT_BLD_R2RDM
!
      NApoints_tot = 0
      do Iatom=1,Natoms
        Znum=CARTESIAN(Iatom)%Atomic_number
        if(Znum.le.0)cycle
        Ifound=GRID_loc(Znum)
        NApts_atom=NApoints_atom(Ifound)
!
        NApoints_tot = NApoints_tot + NApts_atom
!
! Generate a temporary array of grid points
        allocate(grid_points(NApts_atom))
        do IApoint=1,NApts_atom
          grid_points(IApoint)%X=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          grid_points(IApoint)%Y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          grid_points(IApoint)%Z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
          grid_points(IApoint)%w=Egridpts(Ifound,IApoint)%w
        end do ! IApoint
!
! Calculate the atomic contribution to E_LYP
        call FHC_energy (Iatom, Ifound)
!
        deallocate(grid_points)
!
      end do ! Iatom
!
      if(MUN_prtlev.gt.0)write(UNIout,'(a,i8/)')'Total number of grid points: ',NApoints_tot
!
      write(UNIout,'(a,f20.12)')'Fermi-hole correction energy (alpha-alpha) = ',E_FHCaa
      write(UNIout,'(a,f20.12)')'Fermi-hole correction energy (beta-beta)   = ',E_FHCbb
!
      call CPU_time(t2)
      time = t2 - t1
!
! End of routine CFT_FHC_energy
      call PRG_manager ('exit', 'CFT_FHC_energy', 'dynamic_Ec%FHC')
      end subroutine CFT_FHC_energy
      subroutine FHC_energy (Iatom, Ifound)
!************************************************************************************
!     Date last modified: February 17, 2017                                         *
!     Author: JWH                                                                   *
!     Description: Calculate the FHC correlation energy, from an                    *
!                  uncorrelated on-top 2-RDM.                                       *
!************************************************************************************
! Modules:
      USE program_constants
      USE N_integration
      USE type_molecule
      USE type_density 
      USE module_grid_points
      USE NI_defaults
      USE type_basis_set
      USE QM_objects
      USE CFT_objects

      implicit none

! Input scalars:
      integer :: Iatom,Ifound
!
! Local scalars:
      integer :: IApoint, Bopen, Bfroz
      integer :: JNO, SNO, ANO, BNO, INO, RNO, Istat, Aocc, Bocc, Jstat
      double precision :: BWeight,XApt,YApt,ZApt,WApt, rho
      double precision :: phi_i, phi_r
      double precision :: phi_j, phi_s, phi_a, phi_b
      double precision :: dphi2_a, dphi2_b, dphi2_i, dphi2_j, dphi2_r, dphi2_s
      double precision :: dphix_a, dphix_b, dphix_i, dphix_j, dphix_r, dphix_s
      double precision :: dphiy_a, dphiy_b, dphiy_i, dphiy_j, dphiy_r, dphiy_s
      double precision :: dphiz_a, dphiz_b, dphiz_i, dphiz_j, dphiz_r, dphiz_s
      double precision :: CEab, CEij, CErs, CEib, CErb, CEis, CEjr
      double precision :: del, del2, expo, pi, sqrtpi, erfo, E_FHC_hs, E_FHC_ls
      double precision :: E_FHCaa_r, sumE_FHCaa, Lapaa, Lapaa_hs, Lapaa_ls, sc
      double precision :: E_FHCbb_r, sumE_FHCbb, Lapbb, Lapbb_hs, Lapbb_ls, Ifac
      double precision :: Rv(3)
!
! Local arrays:
      double precision, allocatable, dimension(:) :: Gow, G0ow
      double precision, allocatable, dimension(:) :: phi
      double precision, allocatable, dimension(:,:) :: dphi
!
! Functions:
      double precision, external :: derf
!
! Begin: 
!
      allocate(Gow(Nbasis),G0ow(Nbasis))
      allocate(phi(Nbasis),dphi(Nbasis,3))
!
      pi = PI_VAL
      sqrtpi = dsqrt(PI_VAL)
!
      sumE_FHCaa = ZERO
      sumE_FHCbb = ZERO
!
      do IApoint=1,NApts_atom
        XApt=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
        YApt=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
        ZApt=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
        WApt=Egridpts(Ifound,IApoint)%w
        ! Get Becke weights
        call BeckeW1 (XApt, YApt, ZApt, Iatom, Bweight)
        ! Get density
        call GET_rho_Atom (XApt, YApt, ZApt, rho)
!
        E_FHCaa_r = ZERO
        E_FHCbb_r = ZERO
        Lapaa = ZERO ! alpha-alpha on-top Laplacian
        Lapbb = ZERO ! beta-beta on-top Laplacian
!
        if(rho.gt.TOL2)then
!
        ! Get orbital values and derivatives
        Rv(1) = XApt
        Rv(2) = YApt
        Rv(3) = ZApt
        call     GET_MO_point(Rv, Nbasis, NO, phi)
        call GET_MOgrad_point(Rv, Nbasis, NO, dphi)
!
        do Aocc = 1, Nocc 
          aNO = NO_occ(Aocc)
!
          phi_a = phi(aNO)
          dphix_a = dphi(aNO,1)
          dphiy_a = dphi(aNO,2)
          dphiz_a = dphi(aNO,3)
          dphi2_a = dphix_a**2 + dphiy_a**2 + dphiz_a**2
!
          do Bocc = 1, Nocc 
            bNO = NO_occ(Bocc)
!
            phi_b = phi(bNO)
            dphix_b = dphi(bNO,1)
            dphiy_b = dphi(bNO,2)
            dphiz_b = dphi(bNO,3)
            dphi2_b = dphix_b**2 + dphiy_b**2 + dphiz_b**2
!
            CEab = phi_a**2*dphi2_b + phi_b**2*dphi2_a&
                 - TWO*phi_a*phi_b*(dphix_a*dphix_b + dphiy_a*dphiy_b + dphiz_a*dphiz_b)
!
            Lapaa = Lapaa + R2RDMaa(aNO,bNO)*CEab
            Lapbb = Lapbb + R2RDMbb(aNO,bNO)*CEab
!
          end do ! bNO
        end do ! aNO
!
! Not needed if using a Delta based factor
! Now calculate orbital-wise 2-RDM for correlation length correction
!        Gow = ZERO
!
!        do Iocc = 1, Nocc_act
!          INO = occ_act(Iocc)
!          phi2_i = phi(IApoint,INO)**2
!
!          Gow(INO) = Gow(INO) + ON(INO)*phi2_i**2
!!
!        end do ! Iocc
!
!        do Istat = 1, Nstat
!          INO = stat_pair(Istat,1)
!          RNO = stat_pair(Istat,2)
!          phi2_i = phi(IApoint,INO)**2
!          phi2_r = phi(IApoint,RNO)**2
!!          
!          Gow(INO) = Gow(INO) + Delta(INO,RNO)*phi2_r**2
!!
!        end do ! Istat
!!
!        G0ow = Gow
!!
!! Static terms (affect intraorbital correlation)
!
!        do Iocc = 1, Nocc_act
!          INO = occ_act(Iocc)
!          phi2_i = phi(IApoint,INO)**2
!          do Jocc = 1, Nocc_act
!            if(Iocc.eq.Jocc)cycle
!            JNO = occ_act(Jocc)
!            phi2_j = phi(IApoint,JNO)**2
!!
!            Gow(INO) = Gow(INO) + zeta(INO,JNO)*phi2_i*phi2_j
!!
!          end do ! Bocc
!        end do ! Aocc
!! 
!! Decompose zeta for specific pairs
!       do Iocc = 1, Nocc_act
!          INO = occ_act(Iocc)
!          do Rvac = 1, Nstat_occ(INO)
!            RNO = occ_stat(INO,Rvac)
!            phi2_r = phi(IApoint,RNO)**2
!!
!            do Svac = 1, Nstat_occ(INO)
!              if(Rvac.eq.Svac)cycle
!              SNO = occ_stat(INO,Svac)
!              phi2_s = phi(IApoint,SNO)**2
!!
!              Gow(INO) = Gow(INO) + dsqrt(Delta(INO,RNO)*Delta(INO,SNO))*phi2_r*phi2_s
!!      
!            end do ! Jvac
!          end do ! Ivac
!        end do ! Iocc
!!
!        do Istat = 1, Nstat
!          INO = stat_pair(Istat,1)
!          RNO = stat_pair(Istat,2)
!          phi2_i = phi(IApoint,INO)**2
!          phi2_r = phi(IApoint,RNO)**2
!!
!          Gow(INO) = Gow(INO) - TWO*dsqrt(ON(INO)*Delta(INO,RNO))*phi2_i*phi2_r
!!
!        end do ! Istat
!!
! When statically correlated alpha-beta pairs interact with frozen electrons they interact with 
! partial alpha or beta electrons, this needs to be corrected to a full alpha and no beta (or vice versa)
!
        Lapaa_hs = Lapaa
        Lapaa_ls = Lapaa
        Lapbb_hs = Lapbb
        Lapbb_ls = Lapbb
!
        do Istat = 1, Nstat
          iNO = stat_pair(Istat,1)
          rNO = stat_pair(Istat,2)
!
!          Ifac = ONE - Gow(INO)/G0ow(INO)
          Ifac = TWO*xi(iNO,rNO)
!
          phi_i = phi(iNO)
          dphix_i = dphi(iNO,1)
          dphiy_i = dphi(iNO,2)
          dphiz_i = dphi(iNO,3)
          dphi2_i = dphix_i**2 + dphiy_i**2 + dphiz_i**2
!
          phi_r = phi(rNO)
          dphix_r = dphi(rNO,1)
          dphiy_r = dphi(rNO,2)
          dphiz_r = dphi(rNO,3)
          dphi2_r = dphix_r**2 + dphiy_r**2 + dphiz_r**2
!          
! Should only be over inactive closed-shell orbitals
          do Bfroz = 1, Nocc_froz
            bNO = occ_froz(Bfroz)
            if(bNO.eq.iNO)cycle
            if(bNO.eq.rNO)cycle
!
            phi_b = phi(bNO)
            dphix_b = dphi(bNO,1)
            dphiy_b = dphi(bNO,2)
            dphiz_b = dphi(bNO,3)
            dphi2_b = dphix_b**2 + dphiy_b**2 + dphiz_b**2
!
            CEib = phi_i**2*dphi2_b + phi_b**2*dphi2_i&
                 - TWO*phi_i*phi_b*(dphix_i*dphix_b + dphiy_i*dphiy_b + dphiz_i*dphiz_b)
!
            CErb = phi_r**2*dphi2_b + phi_b**2*dphi2_r&
                 - TWO*phi_r*phi_b*(dphix_r*dphix_b + dphiy_r*dphiy_b + dphiz_r*dphiz_b)
!
            sc = Ifac*ON(bNO)*(ON(iNO)*CEib + ON(rNO)*CErb)
            Lapaa_hs = Lapaa_hs + sc
            Lapaa_ls = Lapaa_ls - sc
            Lapbb_hs = Lapbb_hs + sc
            Lapbb_ls = Lapbb_ls - sc
!
          end do ! Bclosed
!
          do Bopen = 1, Nopen
            bNO = NO_open(Bopen)
            phi_b = phi(bNO)
            dphix_b = dphi(bNO,1)
            dphiy_b = dphi(bNO,2)
            dphiz_b = dphi(bNO,3)
            dphi2_b = dphix_b**2 + dphiy_b**2 + dphiz_b**2

            CEib = phi_i**2*dphi2_b + phi_b**2*dphi2_i&
                 - TWO*phi_i*phi_b*(dphix_i*dphix_b + dphiy_i*dphiy_b + dphiz_i*dphiz_b)
!
            CErb = phi_r**2*dphi2_b + phi_b**2*dphi2_r&
                 - TWO*phi_r*phi_b*(dphix_r*dphix_b + dphiy_r*dphiy_b + dphiz_r*dphiz_b)
!
            sc = Ifac*ON(bNO)*(ON(iNO)*CEib + ON(rNO)*CErb)
            Lapaa_hs = Lapaa_hs + sc
            Lapaa_ls = Lapaa_ls - sc
!
          end do ! Bopen
!
        end do ! Istat
!
! Inter-static-pair length correction
        do Istat = 1, Nstat
          iNO = stat_pair(Istat,1)
          rNO = stat_pair(Istat,2)
!
          phi_i = phi(iNO)
          dphix_i = dphi(iNO,1)
          dphiy_i = dphi(iNO,2)
          dphiz_i = dphi(iNO,3)
          dphi2_i = dphix_i**2 + dphiy_i**2 + dphiz_i**2
!
          phi_r = phi(rNO)
          dphix_r = dphi(rNO,1)
          dphiy_r = dphi(rNO,2)
          dphiz_r = dphi(rNO,3)
          dphi2_r = dphix_r**2 + dphiy_r**2 + dphiz_r**2
!
          do Jstat = 1, Nstat
            jNO = stat_pair(Jstat,1)
            sNO = stat_pair(Jstat,2)
            if(iNO.eq.jNO)cycle
            if(rNO.eq.sNO)cycle
!
            phi_j = phi(jNO)
            dphix_j = dphi(jNO,1)
            dphiy_j = dphi(jNO,2)
            dphiz_j = dphi(jNO,3)
            dphi2_j = dphix_j**2 + dphiy_j**2 + dphiz_j**2
!
            phi_s = phi(sNO)
            dphix_s = dphi(sNO,1)
            dphiy_s = dphi(sNO,2)
            dphiz_s = dphi(sNO,3)
            dphi2_s = dphix_s**2 + dphiy_s**2 + dphiz_s**2
!
            CEij = phi_i**2*dphi2_j + phi_j**2*dphi2_i&
                 - TWO*phi_i*phi_j*(dphix_i*dphix_j + dphiy_i*dphiy_j + dphiz_i*dphiz_j)
!
            CEis = phi_i**2*dphi2_s + phi_s**2*dphi2_i&
                 - TWO*phi_i*phi_s*(dphix_i*dphix_s + dphiy_i*dphiy_s + dphiz_i*dphiz_s)
!
            CEjr = phi_j**2*dphi2_r + phi_r**2*dphi2_j&
                 - TWO*phi_j*phi_r*(dphix_j*dphix_r + dphiy_j*dphiy_r + dphiz_j*dphiz_r)
!
            CErs = phi_s**2*dphi2_r + phi_r**2*dphi2_s&
                 - TWO*phi_s*phi_r*(dphix_s*dphix_r + dphiy_s*dphiy_r + dphiz_s*dphiz_r)
!
            sc = xi(iNO,rNO)*xi(jNO,sNO)*(CEij + CEis + CEjr + CErs)
!
            Lapaa_hs = Lapaa_hs + sc
            Lapaa_ls = Lapaa_ls - sc
            Lapbb_hs = Lapbb_hs + sc
            Lapbb_ls = Lapbb_ls - sc
!
          end do ! Jstat
        end do ! Istat
!
! Total 2-RDM version of FHC
!
! Parallel-spin Fermi-hole correction energy
!
        E_FHC_hs = ZERO
        E_FHC_ls = ZERO
!       
        if(Lapaa_hs.gt.TOL2)then
! intermediates
          del = qFHC*Lapaa_hs**F1_8
          del2 = del**2
          expo = dexp(-aFHC**2/(FOUR*del2))
          erfo = ONE + derf(aFHC/(TWO*del))
!
! exponential cusp
!
        E_FHC_hs = Vfac*Lapaa_hs*pi*(TWO*del*(THREE*sqrtpi*aFHC**2*del - TWO*aFHC**3 - N20*aFHC*del2&
                 + N12*sqrtpi*del**3)*expo + sqrtpi*(THREE*sqrtpi*aFHC**3*del - TWO*aFHC**4&
                 - N24*aFHC**2*del2 + N18*sqrtpi*aFHC*del**3 - N24*del**4)*erfo)&
                   /(SIX*del**4*(TWO*aFHC*del*(aFHC**2 + TEN*del2)*expo&
                 + sqrtpi*(aFHC**4 + N12*aFHC**2*del2 + N12*del**4)*erfo))
!
        end if ! Lapaa_hs > TOL2
!
        if(Lapaa_ls.gt.TOL2)then
! intermediates
          del = qFHC*Lapaa_ls**F1_8
          del2 = del**2
          expo = dexp(-aFHC**2/(FOUR*del2))
          erfo = ONE + derf(aFHC/(TWO*del))
!
! exponential cusp
!
        E_FHC_ls = Vfac*Lapaa_ls*pi*(TWO*del*(THREE*sqrtpi*aFHC**2*del - TWO*aFHC**3 - N20*aFHC*del2&
                 + N12*sqrtpi*del**3)*expo + sqrtpi*(THREE*sqrtpi*aFHC**3*del - TWO*aFHC**4&
                 - N24*aFHC**2*del2 + N18*sqrtpi*aFHC*del**3 - N24*del**4)*erfo)&
                   /(SIX*del**4*(TWO*aFHC*del*(aFHC**2 + TEN*del2)*expo&
                 + sqrtpi*(aFHC**4 + N12*aFHC**2*del2 + N12*del**4)*erfo))
!
        end if ! Lapaa_ls > TOL2
!
        E_FHCaa_r = F1_2*(E_FHC_hs + E_FHC_ls)
!
        E_FHC_hs = ZERO
        E_FHC_ls = ZERO
!       
        if(Lapbb_hs.gt.TOL2)then
! intermediates
          del = qFHC*Lapbb_hs**F1_8
          del2 = del**2
          expo = dexp(-aFHC**2/(FOUR*del2))
          erfo = ONE + derf(aFHC/(TWO*del))
!
! exponential cusp
!
        E_FHC_hs = Vfac*Lapbb_hs*pi*(TWO*del*(THREE*sqrtpi*aFHC**2*del - TWO*aFHC**3 - N20*aFHC*del2&
                 + N12*sqrtpi*del**3)*expo + sqrtpi*(THREE*sqrtpi*aFHC**3*del - TWO*aFHC**4&
                 - N24*aFHC**2*del2 + N18*sqrtpi*aFHC*del**3 - N24*del**4)*erfo)&
                   /(SIX*del**4*(TWO*aFHC*del*(aFHC**2 + TEN*del2)*expo&
                 + sqrtpi*(aFHC**4 + N12*aFHC**2*del2 + N12*del**4)*erfo))
!
        end if ! Lapbb_hs > TOL2
!
        if(Lapbb_ls.gt.TOL2)then
! intermediates
          del = qFHC*Lapbb_ls**F1_8
          del2 = del**2
          expo = dexp(-aFHC**2/(FOUR*del2))
          erfo = ONE + derf(aFHC/(TWO*del))
!
! exponential cusp
!
        E_FHC_ls = Vfac*Lapbb_ls*pi*(TWO*del*(THREE*sqrtpi*aFHC**2*del - TWO*aFHC**3 - N20*aFHC*del2&
                 + N12*sqrtpi*del**3)*expo + sqrtpi*(THREE*sqrtpi*aFHC**3*del - TWO*aFHC**4&
                 - N24*aFHC**2*del2 + N18*sqrtpi*aFHC*del**3 - N24*del**4)*erfo)&
                   /(SIX*del**4*(TWO*aFHC*del*(aFHC**2 + TEN*del2)*expo&
                 + sqrtpi*(aFHC**4 + N12*aFHC**2*del2 + N12*del**4)*erfo))
!
        end if ! Lapbb_ls > TOL2
!
        E_FHCbb_r = F1_2*(E_FHC_hs + E_FHC_ls)
!
        sumE_FHCaa = sumE_FHCaa + Bweight*WApt*E_FHCaa_r
        sumE_FHCbb = sumE_FHCbb + Bweight*WApt*E_FHCbb_r
!
        end if ! rho < TOL
!
      end do ! IApoint
!
      E_FHCaa_atom(Iatom) = E_FHCaa_atom(Iatom) + FourPi*sumE_FHCaa
      E_FHCaa = E_FHCaa + E_FHCaa_atom(Iatom)
      E_FHCbb_atom(Iatom) = E_FHCbb_atom(Iatom) + FourPi*sumE_FHCbb
      E_FHCbb = E_FHCbb + E_FHCbb_atom(Iatom)
!
      end subroutine FHC_energy
      subroutine INI_FHC_energy
!************************************************************************************
!     Date last modified: May 16, 2016                                              *
!     Author: JWH                                                                   *
!     Description: Initialization for FHC energy.                                   *
!************************************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE CFT_objects
      USE QM_objects

      implicit none

! Begin:
      if(allocated(E_FHCaa_atom))then
        deallocate(E_FHCaa_atom,E_FHCbb_atom)
      end if
      allocate(E_FHCaa_atom(Natoms),E_FHCbb_atom(Natoms))
      E_FHCaa_atom = ZERO 
      E_FHCbb_atom = ZERO 
      E_FHCaa = ZERO
      E_FHCbb = ZERO

      end subroutine INI_FHC_energy
      subroutine CFT_OSEC_norm
!*********************************************************************************************
!     Date last modified: May 14, 2016                                                       *
!     Author: JWH                                                                            *
!     Desciption: Compute the volume of  the opposite-spin exponential cusp.                 *
!*********************************************************************************************
! Modules:
      USE program_constants
      USE program_timing
      USE program_files
      USE N_integration
      USE type_molecule
      USE QM_objects
      USE CFT_objects

      implicit none
!
! Input scalars:
      integer :: Iatom,Ifound,NApoints_tot,Znum,IApoint
      integer :: Jatom,Jfound
!
! Local scalars:
      double precision :: t0,t1,t2,time
!
! Begin:
      call PRG_manager ('enter', 'CFT_OSEC_norm', 'dynamic_norm%OSEC')
!
! Not sure what to get there (CFT energy?)
!      call GET_object ('QM', 'CMO', Wavefunction)
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
!
      call CPU_time(t0)
      call CPU_time(t1)      

      call BLD_BeckeW_XI
      call INI_OSEC_norm
!
      NApoints_tot = 0
      do Iatom=1,Natoms
        Znum=CARTESIAN(Iatom)%Atomic_number
        if(Znum.le.0)cycle
        Ifound=GRID_loc(Znum)
        NApts_Iatom=NApoints_atom(Ifound)
!
        NApoints_tot = NApoints_tot + NApts_Iatom
!
! Generate a temporary array of grid points
        allocate(grid_points(NApts_Iatom))
        do IApoint=1,NApts_atom
          grid_points(IApoint)%X=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          grid_points(IApoint)%Y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          grid_points(IApoint)%Z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
          grid_points(IApoint)%w=Egridpts(Ifound,IApoint)%w
        end do ! IApoint
!
        do Jatom=1, Natoms
          Znum=CARTESIAN(Jatom)%Atomic_number
          if(Znum.le.0)cycle
          Jfound=GRID_loc(Znum)
          NApts_Jatom=NApoints_atom(Jfound)
!
! Calculate the atomic contribution to E_OSEC
          call OSEC_norm (Iatom, Ifound, Jatom, Jfound)
!
        end do ! Jatom
!
        deallocate(grid_points)
!
      end do ! Iatom
!
      if(MUN_prtlev.gt.0)write(UNIout,'(a,i8/)')'Total number of grid points: ',NApoints_tot
!
      write(UNIout,'(a,f20.12)')'Opposite-spin exponential cusp normalization = ',E_OSEC_norm


      call CPU_time(t2)
      time = t2 - t1
!
! End of routine CFT_OSEC_norm
      call PRG_manager ('exit', 'CFT_OSEC_norm', 'dynamic_norm%OSEC')
      end subroutine CFT_OSEC_norm
      subroutine OSEC_norm (Iatom, Ifound, Jatom, Jfound)
!************************************************************************************
!     Date last modified: May 14, 2016                                              *
!     Author: JWH                                                                   *
!     Description: Calculate the OSEC correlation norm, from an                     *
!                  uncorrelated on-top 2-RDM. (This was for old OSEC, needs         *
!                  modification for new version)                                    *
!************************************************************************************
! Modules:
      USE program_constants
      USE N_integration
      USE type_molecule
      USE type_density 
      USE module_grid_points
      USE NI_defaults
      USE type_basis_set
      USE QM_objects
      USE CFT_objects

      implicit none

! Input scalars:
      integer :: Iatom,Ifound
      integer :: Jatom,Jfound
!
! Local scalars:
      integer :: IApoint, JApoint
      integer :: Aocc, Bocc, Avac, Bvac, JNO, SNO, ANO, BNO, INO, RNO, Istat
      double precision :: XApt,YApt,ZApt,WApt, rho, sumE_OSEC_norm_r
      double precision :: phi2_a, phi2_b, GCF, E_OSEC_norm_r, phi2_i, phi2_r
      double precision :: phi2_j, phi2_s
      double precision :: lam, lam2, expo, pi, sqrtpi, erfo
      double precision :: gauss, prefac, RBweight, uBweight, u , u2
      double precision :: uxApt, uyApt, uzApt, uWApt
      double precision :: Rv(3)
!
! Local arrays:
      double precision, allocatable, dimension(:) :: phi
!
! Functions:
      double precision, external :: derf
!
! Begin: 
!
      allocate(phi(Nbasis))
!
      pi = PI_VAL
      sqrtpi = dsqrt(PI_VAL)
!
      sumE_OSEC_norm_r = ZERO
      do IApoint=1,NApts_Iatom
!
! R pts
        XApt=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
        YApt=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
        ZApt=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
        WApt=Egridpts(Ifound,IApoint)%w
        ! Get Becke weights
        call BeckeW1 (XApt, YApt, ZApt, Iatom, RBweight)
        ! Get density
        call GET_rho_Atom (XApt, YApt, ZApt, rho)
        if(rho.lt.TOL2)cycle
!
        ! Get orbital values
        Rv(1) = XApt
        Rv(2) = YApt
        Rv(3) = ZApt
        call GET_MO_point(Rv, Nbasis, NO, phi)
!
        ! Calculate "on-top" 2-RDM at IApoint
        GCF = 0.0D0
        do ANO = 1, Nbasis
          phi2_a = phi(ANO)**2
          GCF = GCF + ON(ANO)*(ONE - ON(ANO))*phi2_a**2
          do BNO = 1, Nbasis
            phi2_b = phi(BNO)**2
!
            GCF = GCF + ON(ANO)*ON(BNO)*phi2_a*phi2_b
!
          end do ! BNO
        end do ! ANO
        do Istat = 1, Nstat
          INO = stat_pair(Istat,1)
          RNO = stat_pair(Istat,2)
          phi2_i = phi(INO)**2
          phi2_r = phi(RNO)**2
!          
          GCF = GCF + TWO*(Delta(INO,RNO)*(ON(RNO) - ON(INO) - Delta(INO,RNO))&
                      - dsqrt(ON(INO)*Delta(INO,RNO)))&
                        *phi2_i*phi2_r
!
        end do ! Istat
!
        do Aocc = 1, Nocc_act
          INO = occ_act(Aocc)
          phi2_i = phi(INO)**2
          do Bocc = 1, Nocc_act
            if(Aocc.eq.Bocc)cycle
            JNO = occ_act(Bocc)
            phi2_j = phi(JNO)**2
!
            GCF = GCF + (zeta(INO,JNO) - eta(INO,JNO))*phi2_i*phi2_j
!
          end do ! Bocc
        end do ! Aocc
!
        do Avac = 1, Nvac_act
          RNO = vac_act(Avac)
          phi2_r = phi(RNO)**2
          do Bvac = 1, Nvac_act
            if(Avac.eq.Bvac)cycle
            SNO = vac_act(Bvac)
            phi2_s = phi(SNO)**2
!
            GCF = GCF + (zeta(RNO,SNO) - eta(RNO,SNO))*phi2_r*phi2_s
!
          end do ! Bvac
        end do ! Avac
!
! R dependent intermediates
!
        lam = qOS*rho**F1_3
        lam2 = lam**2
        expo = dexp(-aOS**2/(FOUR*lam2))
        erfo = ONE + derf(aOS/(TWO*lam))
!
        prefac = TWO*sqrtpi*lam2/(TWO*aOS*lam*expo + sqrtpi*(aOS**2 + TWO*lam2)*erfo)
!
        do JApoint=1,NApts_Jatom
! u pts
        uxApt=Egridpts(Jfound,JApoint)%X+CARTESIAN(Jatom)%X
        uyApt=Egridpts(Jfound,JApoint)%Y+CARTESIAN(Jatom)%Y
        uzApt=Egridpts(Jfound,JApoint)%Z+CARTESIAN(Jatom)%Z
        uWApt=Egridpts(Jfound,JApoint)%w
        ! Get Becke weights
        call BeckeW1 (uxApt, uyApt, uzApt, Jatom, uBweight)
!
! Opposite-spin exponential cusp normal
!
! u-dependent intermediates
!
        u2 = uxApt**2 + uyApt**2 + uzApt**2
        u = dsqrt(u2)
        gauss = dexp(-lam2*u2)
!
        E_OSEC_norm_r = RBweight*WApt*uBweight*uWApt&
                        *FourPi*GCF*(prefac*dexp(aOS*u - aOS**2/(FOUR*lam2) - lam2*u2) - gauss)
!
! Add integration point to atomic contribution
        sumE_OSEC_norm_r = sumE_OSEC_norm_r + E_OSEC_norm_r
!
      end do ! IApoint
      end do ! JApoint
!
      E_OSEC_norm = E_OSEC_norm + FourPi*sumE_OSEC_norm_r
!
      end subroutine OSEC_norm
      subroutine INI_OSEC_norm
!************************************************************************************
!     Date last modified: May 14, 2016                                              *
!     Author: JWH                                                                   *
!     Description: Initialization for OSEC norm.                                    *
!************************************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE CFT_objects

      implicit none

! Begin:
      if(allocated(E_OSEC_norm_atom))then
        deallocate(E_OSEC_norm_atom)
      end if
      allocate(E_OSEC_norm_atom(Natoms))
      E_OSEC_norm_atom = ZERO 
      E_OSEC_norm = ZERO

      end subroutine INI_OSEC_norm
      subroutine CFT_sr0OE_TEI
!*********************************************************************************************
!     Date last modified: June 21, 2021                                                      *
!     Author: JWH                                                                            *
!     Desciption: Caclulate two-index (ab) two-electron integrals if they are short-range    *
!                 (lambda) and 2-RDM is expanded to 0th order in u.                          *
!*********************************************************************************************
! Modules:
      USE program_constants
      USE program_timing
      USE program_files
      USE N_integration
      USE type_molecule
      USE QM_objects
      USE CFT_objects

      implicit none
!
! Input scalars:
      integer :: Iatom,Ifound,NApoints_tot,Znum,IApoint, Iocc, Jocc, iNO, jNO
!
! Local scalars:
      double precision :: t0,t1,t2,time
!
! Begin:
      call PRG_manager ('enter', 'CFT_sr0OE_TEI', 'UTILITY')
!
! Not sure what to get there (CFT energy?)
!      call GET_object ('QM', 'CMO', Wavefunction)
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
!
      call CPU_time(t0)
      call CPU_time(t1)      

      call BLD_BeckeW_XI
      call INI_sr0OE_TEI
!
      NApoints_tot = 0
      do Iatom=1,Natoms
        Znum=CARTESIAN(Iatom)%Atomic_number
        if(Znum.le.0)cycle
        Ifound=GRID_loc(Znum)
        NApts_atom=NApoints_atom(Ifound)
!
        NApoints_tot = NApoints_tot + NApts_atom
!
! Generate a temporary array of grid points
        allocate(grid_points(NApts_atom))
        do IApoint=1,NApts_atom
          grid_points(IApoint)%X=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          grid_points(IApoint)%Y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          grid_points(IApoint)%Z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
          grid_points(IApoint)%w=Egridpts(Ifound,IApoint)%w
        end do ! IApoint
!
! Calculate the atomic contribution
        if(NBelectrons.gt.0)then
!
        call sr0OE_TEI (Iatom, Ifound)
!
        end if
!
        deallocate(grid_points)
!
      end do ! Iatom
!
      if(MUN_prtlev.gt.0)write(UNIout,'(a,i8/)')'Total number of grid points: ',NApoints_tot
!
      write(6,*)' iNO   jNO   sr0OE   J_NO   K_NO'
      do Iocc = 1, Nocc
        iNO = NO_occ(Iocc)
        do Jocc = 1, Nocc
          jNO = NO_occ(Jocc)
          write(6,*)iNO,jNO,sr0OE(iNO,jNO),J_NO(iNO,jNO),K_NO(iNO,jNO)
        end do ! bNO
      end do ! aNO
!
      call CPU_time(t2)
      time = t2 - t1
!
! End of routine CFT_sr0OE_TEI
      call PRG_manager ('exit', 'CFT_sr0OE_TEI', 'UTILITY')
      end subroutine CFT_sr0OE_TEI
      subroutine sr0OE_TEI (Iatom, Ifound)
!************************************************************************************
!     Date last modified: June 21, 2021                                             *
!     Author: JWH                                                                   *
!     Description: Short-range 0th-order expansion (of 2-RDM) integrals.            *
!************************************************************************************
! Modules:
      USE program_constants
      USE N_integration
      USE type_molecule
      USE type_density 
      USE module_grid_points
      USE NI_defaults
      USE type_basis_set
      USE QM_objects
      USE CFT_objects

      implicit none

! Input scalars:
      integer :: Iatom,Ifound
!
! Local scalars:
      integer :: IApoint
      integer :: aNO, bNO
      double precision :: BWeight,XApt,YApt,ZApt,WApt, rho
      double precision :: phi2_a, phi2_b
      double precision :: lam2, pi, Rv(3)
      double precision, allocatable, dimension(:,:) :: sr0OE_r, sum_sr0OE_r
!
! Local arrays:
      double precision, allocatable, dimension(:) :: phi
!
! Begin: 
!
      allocate(sr0OE_r(Nbasis,Nbasis),sum_sr0OE_r(Nbasis,Nbasis))
      allocate(phi(Nbasis))
!
      sum_sr0OE_r = ZERO
!
      pi = PI_VAL
!
      do IApoint=1,NApts_atom
!
        sr0OE_r = ZERO
!
        XApt=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
        YApt=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
        ZApt=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
        WApt=Egridpts(Ifound,IApoint)%w
        ! Get Becke weights
        call BeckeW1 (XApt, YApt, ZApt, Iatom, Bweight)
        ! Get density
        call GET_rho_Atom (XApt, YApt, ZApt, rho)
!
        if(rho.lt.TOL2)then
!
          sr0OE_r = ZERO
!
        else if(rho.lt.TOL1)then
!
          sr0OE_r = ZERO
!
        else
!
        ! Get orbital values
        Rv(1) = XApt
        Rv(2) = YApt
        Rv(3) = ZApt
        call GET_MO_point(Rv, Nbasis, NO, phi)
!
        lam2 = qOS*rho**F1_3
        lam2 = lam2**2
!
        do aNO = 1, Nbasis
          phi2_a = phi(aNO)**2
!
          do bNO = 1, Nbasis
            phi2_b = phi(bNO)**2
!
            sr0OE_r(aNO,bNO) = phi2_a*phi2_b/lam2
!
          end do ! bNO
        end do ! aNO
!
        sum_sr0OE_r = sum_sr0OE_r + Bweight*WApt*sr0OE_r
!
        end if ! rho < TOL
!
      end do ! IApoint
!
      sr0OE = sr0OE + FourPi*two*pi*sum_sr0OE_r
!
      end subroutine sr0OE_TEI
      subroutine INI_sr0OE_TEI
!************************************************************************************
!     Date last modified: June 21, 2021                                             *
!     Author: JWH                                                                   *
!     Description: Initialization for short-range 0th order expansion integrals.    *
!************************************************************************************
! Modules:
      USE program_constants
      USE QM_objects
      USE type_molecule
      USE CFT_objects

      implicit none

! Begin:
      if(allocated(sr0OE))then
        deallocate(sr0OE)
      end if
      allocate(sr0OE(Nbasis,Nbasis))
      sr0OE = ZERO 

      end subroutine INI_sr0OE_TEI
      subroutine CFT_BLD_LRSRfactor
!************************************************************************************
!     Date last modified: June 21, 2021                                             *
!     Author: JWH                                                                   *
!     Description: Calculate the ratio of correlation for each alpha-beta pair.     *
!************************************************************************************
! Modules:
      USE program_constants
      USE QM_objects
      USE type_molecule
      USE CFT_objects
!
      implicit none
!
! Local scalars:
      integer :: iNO, Iocc, jNO, Jocc, Istat, Jstat, rNO, sNO, Rvac, Svac
!
! Local arrays:
      double precision, allocatable, dimension(:,:) :: E2e, E2eSR, E2e0, E2e0SR
!
! Begin:
!
      allocate(E2e(Nbasis,Nbasis),E2eSR(Nbasis,Nbasis))
      allocate(E2e0(Nbasis,Nbasis),E2e0SR(Nbasis,Nbasis))
!
      if(allocated(LRSR)) deallocate(LRSR)
      allocate(LRSR(Nbasis,Nbasis))
!
! Orbital-wise (ow) alpha-beta on-top density
!
      do Iocc = 1, Nocc_act
        iNO = occ_act(Iocc)
!
        E2e(iNO,iNO)   = E2e(iNO,iNO)   + on(iNO)*J_NO(iNO,iNO)
        E2eSR(iNO,iNO) = E2eSR(iNO,iNO) + on(iNO)*sr0OE(iNO,iNO)
!
        do Rvac = 1, Nstat_occ(iNO)
          rNO = occ_stat(iNO,Rvac)
!
          E2e(iNO,iNO)   = E2e(iNO,iNO)   + Delta(iNO,rNO)*J_NO(rNO,rNO)
          E2eSR(iNO,iNO) = E2eSR(iNO,iNO) + Delta(iNO,rNO)*sr0OE(rNO,rNO)
!
        end do ! Rvac
!
        do Jocc = 1, Nocc_act
          if(Iocc.eq.Jocc)cycle
          jNO = occ_act(Jocc)
!
          E2e(iNO,jNO)   = E2e(iNO,jNO)   + on(iNO)*on(jNO)*J_NO(iNO,jNO)
          E2eSR(iNO,jNO) = E2eSR(iNO,jNO) + on(iNO)*on(jNO)*sr0OE(iNO,jNO)
!
          do Rvac = 1, Nstat_occ(iNO)
            rNO = occ_stat(iNO,Rvac)
            do Svac = 1, Nstat_occ(jNO)
              sNO = occ_stat(jNO,Svac)
!
              E2e(iNO,jNO)   = E2e(iNO,jNO)   + Delta(iNO,rNO)*Delta(jNO,sNO)*J_NO(rNO,sNO)
              E2eSR(iNO,jNO) = E2eSR(iNO,jNO) + Delta(iNO,rNO)*Delta(jNO,sNO)*sr0OE(rNO,sNO)
!
            end do ! Svac
          end do ! Rvac
!
        end do ! Jocc
      end do ! Iocc
!
      E2e0 = E2e
      E2e0SR = E2eSR
!
      do Iocc = 1, Nocc_act
        iNO = occ_act(Iocc)
        do Rvac = 1, Nstat_occ(iNO)
          rNO = occ_stat(iNO,Rvac)
!
          do Svac = 1, Nstat_occ(iNO)
            if(Rvac.eq.Svac)cycle
            sNO = occ_stat(iNO,Svac)
!
            E2e(iNO,iNO)   = E2e(iNO,iNO)   + dsqrt(Delta(iNO,rNO)*Delta(iNO,sNO))*K_NO(rNO,sNO)
            E2eSR(iNO,iNO) = E2eSR(iNO,iNO) + dsqrt(Delta(iNO,rNO)*Delta(iNO,sNO))*sr0OE(rNO,sNO)
!      
          end do ! Svac
        end do ! Rvac
      end do ! Iocc
!
      do Istat = 1, Nstat
        iNO = stat_pair(Istat,1)
        rNO = stat_pair(Istat,2)
!          
        E2e(iNO,iNO)   = E2e(iNO,iNO)   - two*xi(iNO,rNO)*K_NO(iNO,rNO)
        E2eSR(iNO,iNO) = E2eSR(iNO,iNO) - two*xi(iNO,rNO)*sr0OE(iNO,rNO)
!
      end do ! Istat
!
      do Istat = 1, Nstat
        iNO = stat_pair(Istat,1)
        rNO = stat_pair(Istat,2)
!          
        do Jstat = 1, Nstat
          if(Istat.eq.Jstat)cycle
          jNO = stat_pair(Jstat,1)
          sNO = stat_pair(Jstat,2)
!          
          E2e(iNO,iNO)   = E2e(iNO,iNO)   - xi(iNO,rNO)*xi(jNO,sNO)&
                                           *(K_NO(iNO,jNO)&
                                           + K_NO(iNO,sNO)&
                                           + K_NO(rNO,jNO)&
                                           + K_NO(rNO,sNO))
          E2eSR(iNO,iNO) = E2eSR(iNO,iNO) - xi(iNO,rNO)*xi(jNO,sNO)&
                                           *(sr0OE(iNO,jNO)&
                                           + sr0OE(iNO,sNO)&
                                           + sr0OE(rNO,jNO)&
                                           + sr0OE(rNO,sNO))
!
        end do ! Jstat
      end do ! Istat
!
! Calculate factor multiple ways
!
      write(6,*)' Pair energies '
      do Iocc = 1, Nocc_act
        iNO = occ_act(Iocc)
        do Jocc = 1, Nocc_act
          jNO = occ_act(Jocc)
!         
          write(6,*) iNO, jNO
          write(6,*) 'E2e    = ', E2e(iNO,jNO)
          write(6,*) 'E2e0   = ', E2e0(iNO,jNO)
          write(6,*) 'E2eSR  = ', E2eSR(iNO,jNO)
          write(6,*) 'E2e0SR = ', E2e0SR(iNO,jNO)
!
        end do ! jNO     
      end do ! iNO     
!
      write(6,*)' Total energy '
      do Iocc = 1, Nocc_act
        iNO = occ_act(Iocc)
        do Jocc = 1, Nocc_act
          jNO = occ_act(Jocc)
!         
          LRSR(iNO,jNO) = (E2e(iNO,jNO) - E2eSR(iNO,jNO))/E2e(iNO,jNO)
          write(6,*) iNO, jNO, LRSR(iNO,jNO)
!
        end do ! Jocc 
      end do ! Iocc 
!
!      write(6,*)' Static correlation energy '
!      do Iocc = 1, Nocc_act
!        iNO = occ_act(Iocc)
!        
!        do Jocc = 1, Nocc_act
!          jNO = occ_act(Jocc)
!         
!          LRSR(iNO,jNO) = ((E2e0(iNO,jNO) - E2e(iNO,jNO)) - (E2e0SR(iNO,jNO) - E2eSR(iNO,jNO)))&
!                                                           /(E2e0(iNO,jNO) - E2e(iNO,jNO)) 
!          LRSR(iNO,jNO) = (E2e0(iNO,jNO) - E2e(iNO,jNO))/(E2e0SR(iNO,jNO) - E2eSR(iNO,jNO))
!          write(6,*) iNO, jNO, LRSR(iNO,jNO)
!
!        end do ! Jocc 
!      end do ! Iocc 
!
!      write(6,*)' Uncorrelated energy '
!      do Iocc = 1, Nocc_act
!        iNO = occ_act(Iocc)
!        
!        do Jocc = 1, Nocc_act
!          jNO = occ_act(Jocc)
!         
!          LRSR(iNO,jNO) = (E2e0(iNO,jNO) - E2e0SR(iNO,jNO))/E2e0(iNO,jNO) 
!          write(6,*) iNO, jNO, LRSR(iNO,jNO)
!
!        end do ! Jocc 
!      end do ! Iocc 
!
      end subroutine CFT_BLD_LRSRfactor
      subroutine CFT_TDF_energy
!*********************************************************************************************
!     Date last modified: April 11, 2022                                                     *
!     Author: JWH                                                                            *
!     Desciption: Calculate the 2-electron density functional energy.                        *
!*********************************************************************************************
! Modules:
      USE program_constants
      USE program_timing
      USE program_files
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE CFT_objects
      USE module_grid_points
      USE INT_objects

      implicit none
!
! Local scalars:
      integer :: Iatom,IApoint,Znum,Ifound
      double precision :: t0, t1, t2, time
      double precision :: rf, tdf, tdf_pt
!
! Local arrays:
      double precision, dimension(:), allocatable :: WeightsA, ONa, ONb
      double precision :: Rv(3)
!
! Begin:
      call PRG_manager ('enter', 'CFT_TDF_energy', 'UTILITY')
!
      if(allocated(grid_points))then
        deallocate(grid_points)
      end if 
!
! need this? Need to ensure density matrices are up to date for each iteration
! Get density matrices
      ! set alpha and beta occupancies
      allocate(ONa(Nbasis),ONb(Nbasis))
      ONa = ON
      ONb = ON
      ! open orbitals
      ONb(NBelectrons+1:NAelectrons) = zero
!
      if(.not.allocated(PM0_alpha))then
        allocate(PM0_alpha(MATlen),PM0_beta(MATlen))
      end if
      call DENBLD(NO,ONa,PM0_alpha,Nbasis,MATlen,Nbasis)
      call DENBLD(NO,ONb,PM0_beta,Nbasis,MATlen,Nbasis)
!
      call CFT_BLD_R2RDM
! combine alpha-beta and beta-alpha to cut work in half
      allocate(R2RDMJos(Nbasis,Nbasis),R2RDMLos(Nbasis,Nbasis))
      R2RDMJos = R2RDMJab + R2RDMJba
      R2RDMLos = R2RDMLab + R2RDMLba
!
! Get grid and weights
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
!      write(UNIout,'(/2a)')'Radial grid used for numerical integration: ',RADIAL_grid
      call BLD_BeckeW_XI
!      write(UNIout,'(2a)')'Weights used for numerical integration:      BECKE'
!
      call CPU_time(t0)
      call CPU_time(t1)
!
      tdf = zero
!
!      write(UNIout,'(a,F5.3)') 'q_TDF = ',q_TDF
!
      do Iatom=1,Natoms
        Znum=CARTESIAN(Iatom)%atomic_number
        if(Znum.le.0)cycle
        Ifound=GRID_loc(Znum)
        NApts_atom=NApoints_atom(Ifound)

        allocate(grid_points(1:NApts_atom))
        allocate(WeightsA(NApts_atom))
!
! Moving grid points on cartesian coordinate grid
        do IApoint=1,NApts_atom
          grid_points(IApoint)%X=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          grid_points(IApoint)%Y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          grid_points(IApoint)%Z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
        end do ! IApoint

        call GET_weights (grid_points, NApts_atom, Iatom, WeightsA)

        do IApoint=1,NApts_atom
! Extracule coordinates
          Rv(1) = grid_points(IApoint)%X
          Rv(2) = grid_points(IApoint)%Y
          Rv(3) = grid_points(IApoint)%Z
!
! Get the value and gradient of the renormalization function (Phi)
          call GET_RF_point(Rv, rf)
!
! Get gradient of the two-electron density functional
          call GET_TDF_point(Rv, rf, tdf_pt)
!
          tdf = tdf + tdf_pt*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
        end do ! IApoint
!
        deallocate(grid_points)
        deallocate(WeightsA)
!
      end do ! Iatom
!
      E_TDF = FourPI*tdf
!
! If not variational, use virial theorem
      if(.not.LTDFvar) E_TDF = E_TDF/2.0D0
!
!      write(6,*)'E_TDF = ',E_TDF 
!
      call CPU_time(t2)
      time = t2 - t1
!
      deallocate(R2RDMJos,R2RDMLos)
!
! End of routine CFT_TDF_energy
      call PRG_manager ('exit', 'CFT_TDF_energy', 'UTILITY')
      end subroutine CFT_TDF_energy
      subroutine GET_RF_point (Rv, rf)
!*******************************************************************************
!     Date last modified: April 11, 2022                                       *
!     Authors: JWH                                                             *
!     Description: Compute the renormalization function at the given point.    *
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE INT_objects
      USE CFT_objects

      implicit none
!
! Input arrays:
      double precision, intent(IN) :: Rv(3)
!
! Output scalars:
      double precision, intent(OUT) :: rf
!
! Local scalars:
      double precision :: num, den, g, u, cusp, q, lam, temp
      double precision :: rho
      double precision :: R2RDMos_grid, R2RDM_grid, wts
      integer :: I_ux, I_uy, I_uz
! 
! Local arrays:
      double precision :: uv(3), r1v(3), r2v(3), umag(3)
      double precision, allocatable, dimension(:) :: u_pts, u_wts
!
! Begin:
!
      q = q_TDF
!
      rf = zero
!
      call GET_den_point (Rv, rho)
!
      if(rho.gt.1.0D-12)then
!
! Current correlation length ansatz
!      call GET_R2RDM_OS_point (Rv, Rv, R2RDMos_grid)
!      R2RDM_grid = R2RDM_grid + R2RDMos_grid
!
      lam  = q*rho**F1_3
!      lam  = q*R2RDM_grid**(1.0D0/6.0D0)
!
! grid parameters (use density at R to determine these)          
      !u_scale = max(min(4.6D0/lam - 0.3D0, 2.0D0),1.0D0)
      allocate(u_pts(N_u_pts), u_wts(N_u_pts))
      call BLD_MK_grid(N_u_pts, u_scale, u_pts, u_wts)
!
! Integrate over u
      num  = zero
      den  = zero
!
      do I_ux = 1, N_u_pts
        umag(1) = u_pts(I_ux)
        do I_uy = 1, N_u_pts
          umag(2) = u_pts(I_uy)
          do I_uz = 1, N_u_pts
            umag(3) = u_pts(I_uz)
!
            R2RDM_grid  = zero
!
            wts = u_wts(I_ux)*u_wts(I_uy)*u_wts(I_uz)
!
            u = dsqrt(dot_product(umag,umag))
!
            g      = dexp(-(lam*u)**2)
            !g      = dexp(-lam*u)
            cusp   = dexp(u)
            !cusp   = 2.0D0 - dexp(-u)
            !cusp   = u
            !cusp   = (1.0D0 + lam - dexp(-lam*u))/lam
!
! (+,+,+)
            uv = umag
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_OS_point (r1v, r2v, R2RDMos_grid)
!
            R2RDM_grid = R2RDM_grid + R2RDMos_grid
!
! (+,+,-)
            uv(3) = -umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_OS_point (r1v, r2v, R2RDMos_grid)
!
            R2RDM_grid = R2RDM_grid + R2RDMos_grid
!
! (+,-,+)
            uv(1) =  umag(1)
            uv(2) = -umag(2)
            uv(3) =  umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_OS_point (r1v, r2v, R2RDMos_grid)
!
            R2RDM_grid = R2RDM_grid + R2RDMos_grid
!
! (-,+,+)
            uv(1) = -umag(1)
            uv(2) =  umag(2)
            uv(3) =  umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_OS_point (r1v, r2v, R2RDMos_grid)
!
            R2RDM_grid = R2RDM_grid + R2RDMos_grid
!
! (+,-,-)
            uv(1) =  umag(1)
            uv(2) = -umag(2)
            uv(3) = -umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_OS_point (r1v, r2v, R2RDMos_grid)
!
            R2RDM_grid = R2RDM_grid + R2RDMos_grid
!
! (-,+,-)
            uv(1) = -umag(1)
            uv(2) =  umag(2)
            uv(3) = -umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_OS_point (r1v, r2v, R2RDMos_grid)
!
            R2RDM_grid = R2RDM_grid + R2RDMos_grid
!
! (-,-,+)
            uv(1) = -umag(1)
            uv(2) = -umag(2)
            uv(3) =  umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_OS_point (r1v, r2v, R2RDMos_grid)
!
            R2RDM_grid = R2RDM_grid + R2RDMos_grid
!
! (-,-,-)
            uv = -umag
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_OS_point (r1v, r2v, R2RDMos_grid)
!
            R2RDM_grid = R2RDM_grid + R2RDMos_grid
!
! integrands
!
            temp = wts*R2RDM_grid*g
            num  = num + temp
            den  = den + temp*cusp
!
          end do ! I_uz
        end do ! I_uy
      end do ! I_ux
!
      if(den.gt.1.0D-10)then
        rf = num/den
      end if
!
      end if ! rho > tol
!
      end subroutine GET_RF_point
      subroutine GET_R2RDM_OS_point (rv, tp, R2RDMos_grid)
!*******************************************************************************
!     Date last modified: April 11, 2022                                       *
!     Authors: JWH                                                             *
!     Description: Compute the EPUM and Delta derivative of the opposite-spin  *
!                  two-electron density at a point, and the 2RDM itself.       *
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE INT_objects
      USE CFT_objects

      implicit none
!
! Input arrays:
      double precision, intent(IN) :: rv(3),tp(3)
!
! Output scalars:
      double precision, intent(OUT) :: R2RDMos_grid
!
! Local scalars:
      integer :: pNO, qNO, pocc, qocc
      double precision :: J, L
!
! Local arrays:
      double precision, allocatable, dimension(:) :: phi_rv, phi_tp
!
! Begin:
!
      R2RDMos_grid = zero
!
      allocate(phi_rv(Nbasis),phi_tp(Nbasis))
      call GET_MO_point(rv, Nbasis, NO, phi_rv)
      call GET_MO_point(tp, Nbasis, NO, phi_tp)
!
      do pocc = 1, Nocc
        pNO = NO_occ(pocc)
        do qocc = 1, Nocc
          qNO = NO_occ(qocc)
!
          J = phi_rv(pNO)**2*phi_tp(qNO)**2
          L = phi_rv(pNO)*phi_tp(pNO)*phi_rv(qNO)*phi_tp(qNO)
!
          R2RDMos_grid = R2RDMos_grid + R2RDMJos(pNO,qNO)*J + R2RDMLos(pNO,qNO)*L
!
        end do ! qNO
      end do ! pNO
!
      end subroutine GET_R2RDM_OS_point
      subroutine GET_TDF_point (Rv, rf, tdf)
!*******************************************************************************
!     Date last modified: April 11, 2022                                       *
!     Authors: JWH                                                             *
!     Description: Compute the EPUM and Delta gradient of the 2e density       *
!                  functional at the given point.                              *
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE INT_objects
      USE CFT_objects

      implicit none
!
! Input scalars:
      double precision, intent(IN) :: rf
!
! Input arrays:
      double precision, intent(IN) :: Rv(3)
!
! Output arrays:
      double precision, intent(OUT) :: tdf
!
! Local scalars:
      double precision :: g, u, cusp, q, lam
      double precision :: rho
      double precision :: R2RDMos_grid, wts, R2RDM_grid
      integer :: I_ux, I_uy, I_uz
! 
! Local arrays:
      double precision :: uv(3), r1v(3), r2v(3), umag(3)
      double precision, allocatable, dimension(:) :: u_pts, u_wts
!
! Begin:
!
      q = q_TDF
!
      tdf  = zero
!
      call GET_den_point (Rv, rho)
!
      if(rho.gt.1.0D-12)then
!
! Current correlation length ansatz
!      call GET_R2RDM_OS_point (Rv, Rv, R2RDMos_grid)
!      R2RDM_grid = R2RDM_grid + R2RDMos_grid
!
      lam  = q*rho**F1_3
!      lam  = q*R2RDM_grid**(1.0D0/6.0D0)
!
!
! grid parameters (use density at R to determine these)          
      !u_scale = max(min(4.6D0/lam - 0.3D0, 2.0D0),1.0D0)
      allocate(u_pts(N_u_pts), u_wts(N_u_pts))
      call BLD_MK_grid(N_u_pts, u_scale, u_pts, u_wts)
!
! Integrate over u
!
      do I_ux = 1, N_u_pts
        umag(1) = u_pts(I_ux)
        do I_uy = 1, N_u_pts
          umag(2) = u_pts(I_uy)
          do I_uz = 1, N_u_pts
            umag(3) = u_pts(I_uz)
!
            R2RDM_grid = zero
!
            wts = u_wts(I_ux)*u_wts(I_uy)*u_wts(I_uz)
!
            u = dsqrt(dot_product(umag,umag))
!
            g      = dexp(-(lam*u)**2)
            !g      = dexp(-lam*u)
            cusp   = dexp(u)
            !cusp   = 2.0D0 - dexp(-u)
            !cusp   = u
            !cusp   = (1.0D0 + lam - dexp(-lam*u))/lam
!
! (+,+,+)
            uv = umag
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_OS_point (r1v, r2v, R2RDMos_grid)
!
            R2RDM_grid = R2RDM_grid + R2RDMos_grid
!
! (+,+,-)
            uv(3) = -umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_OS_point (r1v, r2v, R2RDMos_grid)
!
            R2RDM_grid = R2RDM_grid + R2RDMos_grid
!
! (+,-,+)
            uv(1) =  umag(1)
            uv(2) = -umag(2)
            uv(3) =  umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_OS_point (r1v, r2v, R2RDMos_grid)
!
            R2RDM_grid = R2RDM_grid + R2RDMos_grid
!
! (-,+,+)
            uv(1) = -umag(1)
            uv(2) =  umag(2)
            uv(3) =  umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_OS_point (r1v, r2v, R2RDMos_grid)
!
            R2RDM_grid = R2RDM_grid + R2RDMos_grid
!
! (+,-,-)
            uv(1) =  umag(1)
            uv(2) = -umag(2)
            uv(3) = -umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_OS_point (r1v, r2v, R2RDMos_grid)
!
            R2RDM_grid = R2RDM_grid + R2RDMos_grid
!
! (-,+,-)
            uv(1) = -umag(1)
            uv(2) =  umag(2)
            uv(3) = -umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_OS_point (r1v, r2v, R2RDMos_grid)
!
            R2RDM_grid = R2RDM_grid + R2RDMos_grid
!
! (-,-,+)
            uv(1) = -umag(1)
            uv(2) = -umag(2)
            uv(3) =  umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_OS_point (r1v, r2v, R2RDMos_grid)
!
            R2RDM_grid = R2RDM_grid + R2RDMos_grid
!
! (-,-,-)
            uv = -umag
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_OS_point (r1v, r2v, R2RDMos_grid)
!
            R2RDM_grid = R2RDM_grid + R2RDMos_grid
!
! integrand
!
            tdf = tdf + wts*R2RDM_grid*g*(rf*cusp - one)/u
!            tdf = tdf + wts*R2RDM_grid*g*(rf*cusp - one)
!            tdf = tdf + wts*R2RDM_grid*g/u
!
          end do ! I_uz
        end do ! I_uy
      end do ! I_ux
!
      end if ! rho > tol
!
      end subroutine GET_TDF_point
      subroutine CFT_TDF_hess
!*********************************************************************************************
!     Date last modified: April 18, 2022                                                     *
!     Author: JWH                                                                            *
!     Desciption: Calculate the contribution to the EPUM and Delta gradient and hessian from *
!                 the 2-electron density functional.                                         *
!*********************************************************************************************
! Modules:
      USE program_constants
      USE program_timing
      USE program_files
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE CFT_objects
      USE module_grid_points
      USE INT_objects

      implicit none
!
! Local scalars:
      integer :: Iatom, IApoint, Znum, Ifound, Istat, Jstat, I, J
      double precision :: t0, t1, t2, time
      double precision :: rf
!
! Local arrays:
      double precision, dimension(:), allocatable :: WeightsA, rf_grad, tdf_grad_pt
      double precision, dimension(:,:), allocatable :: rf_hess, tdf_hess_pt
      double precision, dimension(:), allocatable :: ONa, ONb
      double precision :: Rv(3)
!
! Begin:
      call PRG_manager ('enter', 'CFT_TDF_hess', 'UTILITY')
!
! Get density matrices
      ! set alpha and beta occupancies
      allocate(ONa(Nbasis),ONb(Nbasis))
      ONa = ON
      ONb = ON
      ! open orbitals
      ONb(NBelectrons+1:NAelectrons) = zero
!
      if(.not.allocated(PM0_alpha))then
        allocate(PM0_alpha(MATlen),PM0_beta(MATlen))
      end if
      call DENBLD(NO,ONa,PM0_alpha,Nbasis,MATlen,Nbasis)
      call DENBLD(NO,ONb,PM0_beta,Nbasis,MATlen,Nbasis)
!
      call CFT_BLD_R2RDM
! combine alpha-beta and beta-alpha to cut work in half
      allocate(R2RDMJos(Nbasis,Nbasis),R2RDMLos(Nbasis,Nbasis))
      R2RDMJos = R2RDMJab + R2RDMJba
      R2RDMLos = R2RDMLab + R2RDMLba
!
      call CFT_BLD_dR2RDM
! combine alpha-beta and beta-alpha to cut work in half
      allocate(dR2RDMJos(Nbasis,Nbasis,Nstat),dR2RDMLos(Nbasis,Nbasis,Nstat))
      dR2RDMJos = dR2RDMJab + dR2RDMJba
      dR2RDMLos = dR2RDMLab + dR2RDMLba
!
      call CFT_BLD_d2R2RDM
! combine alpha-beta and beta-alpha to cut work in half
      allocate(d2R2RDMJos(Nbasis,Nbasis,Nstat,Nstat),d2R2RDMLos(Nbasis,Nbasis,Nstat,Nstat))
      d2R2RDMJos = d2R2RDMJab + d2R2RDMJba
      d2R2RDMLos = d2R2RDMLab + d2R2RDMLba
!
! calculate point independent coefficients of orbital products
      call CFT_BLD_hesscoeff
!
! Get grid and weights
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
!      write(UNIout,'(/2a)')'Radial grid used for numerical integration: ',RADIAL_grid
      call BLD_BeckeW_XI
!      write(UNIout,'(2a)')'Weights used for numerical integration:      BECKE'
!
      call CPU_time(t0)
      call CPU_time(t1)
!
      allocate(tdf_grad_pt(Npair+Nstat),rf_grad(Npair+Nstat))
      allocate(tdf_hess_pt(Npair+Nstat,Npair+Nstat),rf_hess(Npair+Nstat,Npair+Nstat))
      tdf_grad = zero
      tdf_hess = zero
!
!      write(UNIout,'(a,F5.3)') 'q_TDF= ',q_TDF
!
      do Iatom=1,Natoms
        Znum=CARTESIAN(Iatom)%atomic_number
        if(Znum.le.0)cycle
        Ifound=GRID_loc(Znum)
        NApts_atom=NApoints_atom(Ifound)

        allocate(grid_points(1:NApts_atom))
        allocate(WeightsA(NApts_atom))
!
! Moving grid points on cartesian coordinate grid
        do IApoint=1,NApts_atom
          grid_points(IApoint)%X=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          grid_points(IApoint)%Y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          grid_points(IApoint)%Z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
        end do ! IApoint

        call GET_weights (grid_points, NApts_atom, Iatom, WeightsA)

        do IApoint=1,NApts_atom
! Extracule coordinates
          Rv(1) = grid_points(IApoint)%X
          Rv(2) = grid_points(IApoint)%Y
          Rv(3) = grid_points(IApoint)%Z
!
! Get the value and gradient of the renormalization function (Phi)
          call GET_RF_hess_point(Rv, rf, rf_grad, rf_hess)
!
! Get gradient of the two-electron density functional
          call GET_TDF_hess_point(Rv, rf, rf_grad, rf_hess, tdf_grad_pt, tdf_hess_pt)
!
          tdf_grad = tdf_grad + tdf_grad_pt*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
          tdf_hess = tdf_hess + tdf_hess_pt*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
        end do ! IApoint
!
        deallocate(grid_points)
        deallocate(WeightsA)
!
      end do ! Iatom
!
      tdf_grad = FourPI*tdf_grad
      tdf_hess = FourPI*tdf_hess
!
      deallocate(tdf_grad_pt,rf_grad)
      deallocate(tdf_hess_pt,rf_hess)
!
! Convert Delta derivatives to theta derivatives using the chain rule
      do Istat = 1, Nstat
        tdf_grad(Npair+Istat) = -0.5D0*tdf_grad(Npair+Istat)*dsin(2.0D0*theta(Istat))
        ! EPUM-theta coupling
        tdf_hess(Npair+Istat,1:Npair) = -0.5D0*tdf_hess(Npair+Istat,1:Npair)*dsin(2.0D0*theta(Istat))
        tdf_hess(1:Npair,Npair+Istat) = tdf_hess(Npair+Istat,1:Npair)
!
        do Jstat = 1, Nstat 
          tdf_hess(Npair+Istat,Npair+Jstat) = tdf_hess(Npair+Istat,Npair+Jstat)&
                                              *0.25D0*dsin(2.0D0*theta(Istat))*dsin(2.0D0*theta(Jstat))
        end do ! Jstat
      end do ! Istat
!
      do Istat = 1, Nstat
        ! theta diagonal
        tdf_hess(Npair+Istat,Npair+Istat) = tdf_hess(Npair+Istat,Npair+Istat)&
                                           - tdf_grad(Npair+Istat)*dcos(2.0D0*theta(Istat))
      end do ! Istat
!
! should symmetrize hessian here
!
!      write(6,*)'tdf grad' 
!      do I = 1, Npair+Nstat
!        write(6,*) tdf_grad(I) 
!      end do
!
!      write(6,*)'tdf hess' 
!      do I = 1, Npair+Nstat
!        do J = 1, I
!          write(6,*) tdf_hess(I,J), tdf_hess(J,I) 
!        end do
!      end do
!
      deallocate(R2RDMJos,R2RDMLos)
      deallocate(dR2RDMJos,dR2RDMLos)
      deallocate(d2R2RDMJos,d2R2RDMLos)
!
      call CPU_time(t2)
      time = t2 - t1
!
! End of routine CFT_TDF_hess
      call PRG_manager ('exit', 'CFT_TDF_hess', 'UTILITY')
      end subroutine CFT_TDF_hess
      subroutine GET_RF_hess_point (Rv, rf, rf_grad, rf_hess)
!*******************************************************************************
!     Date last modified: April 18, 2022                                       *
!     Authors: JWH                                                             *
!     Description: Compute the EPUM and Delta hessian of the renormalization   *
!                  function at the given point.                                *
!                  Costs little extra for gradient, so calculate that too.     *
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE INT_objects
      USE CFT_objects

      implicit none
!
! Input arrays:
      double precision, intent(IN) :: Rv(3)
!
! Output scalars:
      double precision, intent(OUT) :: rf, rf_grad(Npair+Nstat), rf_hess(Npair+Nstat,Npair+Nstat)
!
! Local scalars:
      double precision :: num, den, g, u, cusp, q, lam, dg, dlam, d2g, d2lam
      double precision :: rho, temp
      double precision :: R2RDMos_grid, R2RDM_grid, wts
      integer :: I_ux, I_uy, I_uz, I, J
! 
! Local arrays:
      double precision :: uv(3), r1v(3), r2v(3), umag(3)
      double precision, allocatable, dimension(:) :: u_pts, u_wts
      double precision, allocatable, dimension(:) :: R2RDMos_grad, R2RDM_grad, rho_grad
      double precision, allocatable, dimension(:) :: g_grad, lam_grad, temp_grad, dnum, dden
      double precision, allocatable, dimension(:,:) :: R2RDMos_hess, R2RDM_hess, rho_hess
      double precision, allocatable, dimension(:,:) :: temp_hess, d2num, d2den, g_hess, lam_hess
      double precision, allocatable, dimension(:,:) :: lam_grad2, d2lam_grad, grad_prod
!
! Begin:
!
      q = q_TDF
!
      rf      = zero
      rf_grad = zero
      rf_hess = zero
!
      allocate(R2RDMos_grad(Npair+Nstat),R2RDM_grad(Npair+Nstat))
      allocate(R2RDMos_hess(Npair+Nstat,Npair+Nstat),R2RDM_hess(Npair+Nstat,Npair+Nstat))
      allocate(rho_grad(Npair+Nstat),g_grad(Npair+Nstat),lam_grad(Npair+Nstat))
      allocate(rho_hess(Npair+Nstat,Npair+Nstat),g_hess(Npair+Nstat,Npair+Nstat))
      allocate(lam_hess(Npair+Nstat,Npair+Nstat),lam_grad2(Npair+Nstat,Npair+Nstat))
      allocate(d2lam_grad(Npair+Nstat,Npair+Nstat),grad_prod(Npair+Nstat,Npair+Nstat))
      allocate(temp_grad(Npair+Nstat),dden(Npair+Nstat),dnum(Npair+Nstat))
      allocate(d2den(Npair+Nstat,Npair+Nstat),d2num(Npair+Nstat,Npair+Nstat))
!
      call GET_den_point (Rv, rho)
!
      if(rho.gt.1.0D-12)then
!
      call GET_den_hess_point (Rv, rho_grad, rho_hess)
!
! Current correlation length ansatz
      lam   = q*rho**F1_3
      dlam  = q/(3.0D0*rho**F2_3)
      d2lam = -2.0D0*q/(9.0D0*rho**F5_3)
      lam_grad = dlam*rho_grad
      lam_hess = dlam*rho_hess
!
      lam_grad2   = zero
      d2lam_grad  = zero
      do I = 1, Nstat + Npair
        do J = 1, Nstat + Npair
          lam_grad2(I,J)  = lam_grad(I)*lam_grad(J)
          d2lam_grad(I,J) = d2lam*rho_grad(I)*rho_grad(J)
        end do ! J
      end do ! I
!
! grid parameters (use density at R to determine these)          
      !u_scale = max(min(4.6D0/lam - 0.3D0, 2.0D0),1.0D0)
      allocate(u_pts(N_u_pts), u_wts(N_u_pts))
      call BLD_MK_grid(N_u_pts, u_scale, u_pts, u_wts)
!
! Integrate over u
      num   = zero
      den   = zero
      dnum  = zero
      dden  = zero
      d2num = zero
      d2den = zero
!
      do I_ux = 1, N_u_pts
        umag(1) = u_pts(I_ux)
        do I_uy = 1, N_u_pts
          umag(2) = u_pts(I_uy)
          do I_uz = 1, N_u_pts
            umag(3) = u_pts(I_uz)
!
            R2RDM_grid = zero
            R2RDM_grad = zero
            R2RDM_hess = zero
!
            wts = u_wts(I_ux)*u_wts(I_uy)*u_wts(I_uz)
!
            u = dsqrt(dot_product(umag,umag))
!
            g      = dexp(-(lam*u)**2)
            cusp   = dexp(u)
            !cusp   = 2.0D0 - dexp(-u)
!
            dg     = -2.0D0*g*u**2*lam
            g_grad = dg*lam_grad
!
            d2g    = 2.0D0*g*u**2*(2.0D0*(u*lam)**2 - 1.0D0)
            g_hess = d2g*lam_grad2 + dg*(d2lam_grad + lam_hess)
!
! (+,+,+)
            uv = umag
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_OS_hess_point (r1v, r2v, R2RDMos_grid, R2RDMos_grad, R2RDMos_hess)
!
            R2RDM_grid = R2RDM_grid + R2RDMos_grid
            R2RDM_grad = R2RDM_grad + R2RDMos_grad
            R2RDM_hess = R2RDM_hess + R2RDMos_hess
!
! (+,+,-)
            uv(3) = -umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_OS_hess_point (r1v, r2v, R2RDMos_grid, R2RDMos_grad, R2RDMos_hess)
!
            R2RDM_grid = R2RDM_grid + R2RDMos_grid
            R2RDM_grad = R2RDM_grad + R2RDMos_grad
            R2RDM_hess = R2RDM_hess + R2RDMos_hess
!
! (+,-,+)
            uv(1) =  umag(1)
            uv(2) = -umag(2)
            uv(3) =  umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_OS_hess_point (r1v, r2v, R2RDMos_grid, R2RDMos_grad, R2RDMos_hess)
!
            R2RDM_grid = R2RDM_grid + R2RDMos_grid
            R2RDM_grad = R2RDM_grad + R2RDMos_grad
            R2RDM_hess = R2RDM_hess + R2RDMos_hess
!
! (-,+,+)
            uv(1) = -umag(1)
            uv(2) =  umag(2)
            uv(3) =  umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_OS_hess_point (r1v, r2v, R2RDMos_grid, R2RDMos_grad, R2RDMos_hess)
!
            R2RDM_grid = R2RDM_grid + R2RDMos_grid
            R2RDM_grad = R2RDM_grad + R2RDMos_grad
            R2RDM_hess = R2RDM_hess + R2RDMos_hess
!
! (+,-,-)
            uv(1) =  umag(1)
            uv(2) = -umag(2)
            uv(3) = -umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_OS_hess_point (r1v, r2v, R2RDMos_grid, R2RDMos_grad, R2RDMos_hess)
!
            R2RDM_grid = R2RDM_grid + R2RDMos_grid
            R2RDM_grad = R2RDM_grad + R2RDMos_grad
            R2RDM_hess = R2RDM_hess + R2RDMos_hess
!
! (-,+,-)
            uv(1) = -umag(1)
            uv(2) =  umag(2)
            uv(3) = -umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_OS_hess_point (r1v, r2v, R2RDMos_grid, R2RDMos_grad, R2RDMos_hess)
!
            R2RDM_grid = R2RDM_grid + R2RDMos_grid
            R2RDM_grad = R2RDM_grad + R2RDMos_grad
            R2RDM_hess = R2RDM_hess + R2RDMos_hess
!
! (-,-,+)
            uv(1) = -umag(1)
            uv(2) = -umag(2)
            uv(3) =  umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_OS_hess_point (r1v, r2v, R2RDMos_grid, R2RDMos_grad, R2RDMos_hess)
!
            R2RDM_grid = R2RDM_grid + R2RDMos_grid
            R2RDM_grad = R2RDM_grad + R2RDMos_grad
            R2RDM_hess = R2RDM_hess + R2RDMos_hess
!
! (-,-,-)
            uv = -umag
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_OS_hess_point (r1v, r2v, R2RDMos_grid, R2RDMos_grad, R2RDMos_hess)
!
            R2RDM_grid = R2RDM_grid + R2RDMos_grid
            R2RDM_grad = R2RDM_grad + R2RDMos_grad
            R2RDM_hess = R2RDM_hess + R2RDMos_hess
!
! integrands
!
            temp = wts*R2RDM_grid*g
            num  = num + temp
            den  = den + temp*cusp
!
            temp_grad = wts*(R2RDM_grad*g + R2RDM_grid*g_grad)
            dnum = dnum + temp_grad
            dden = dden + temp_grad*cusp
!
            grad_prod = zero
            do I = 1, Npair + Nstat 
              do J = 1, I
                grad_prod(I,J) = R2RDM_grad(I)*g_grad(J) + R2RDM_grad(J)*g_grad(I)
                grad_prod(J,I) = grad_prod(I,J)
              end do ! J
            end do ! I
            temp_hess = wts*(R2RDM_hess*g + grad_prod + R2RDM_grid*g_hess)
            d2num = d2num + temp_hess
            d2den = d2den + temp_hess*cusp
!
          end do ! I_uz
        end do ! I_uy
      end do ! I_ux
!
      if(den.gt.1.0D-10)then
        rf = num/den
        rf_grad = (dnum*den - num*dden)/den**2
        do I = 1, Npair + Nstat
          do J = 1, I
            rf_hess(I,J) = (den**2*d2num(I,J) + 2.0D0*num*dden(I)*dden(J)&
                         - den*(dnum(I)*dden(J) + dden(I)*dnum(J) + num*d2den(I,J)))/den**3
            rf_hess(J,I) = rf_hess(I,J)
          end do ! J
        end do ! I
      end if
!
      end if ! rho > tol
!
      end subroutine GET_RF_hess_point
      subroutine GET_den_hess_point(rv, rho_grad, rho_hess)
!*******************************************************************
!   Date last modified: March 25, 2022                             *
!   Author: JWH                                                    *
!   Description: Calculate the EPUM gradient and hessian of the    *
!                one-electron density.                             * 
!*******************************************************************
! Modules:
      USE AO_values
      USE type_molecule
      USE module_grid_points
      USE program_constants
      USE QM_objects
      USE CFT_objects

      implicit none
!
! Input arrays:
      double precision, intent(IN) :: rv(3)
!
! Output arrays:
      double precision, intent(OUT) :: rho_grad(Npair+Nstat)
      double precision, intent(OUT) :: rho_hess(Npair+Nstat,Npair+Nstat)
!
! Local scalars:
      integer   :: Ipair, Jpair, Istat, kNO, tNO, pNO, qNO, rNO, sNO
      double precision :: na_p, nb_p, na_q, nb_q
      double precision :: phi_p, phi_q, phi_r, phi_s
!
! Local arrays:
      double precision, allocatable, dimension(:) :: phi_rv, ONa, ONb
      double precision, allocatable, dimension(:,:) :: don
!
! Begin:
!
! rv - grid point
!
      allocate(phi_rv(Nbasis))
!
      rho_grad = zero
      rho_hess = zero
!
      call GET_MO_point(rv, Nbasis, NO, phi_rv)
!
! set alpha and beta occupancies
      allocate(ONa(Nbasis),ONb(Nbasis))
      ONa = ON
      ONb = ON
      ! open orbitals
      ONb(NBelectrons+1:NAelectrons) = zero
!
! Delta derivative of occupancy
      allocate(don(Nbasis,Nstat))
      don = zero
      do Istat = 1, Nstat 
        kNO = stat_pair(Istat,1)
        tNO = stat_pair(Istat,2)
        don(kNO,Istat) = -1.0D0
        don(tNO,Istat) =  1.0D0
      end do ! Istat
!
      do Ipair = 1, Npair
        pNO = orbpr(Ipair,1)
        qNO = orbpr(Ipair,2)
        phi_p = phi_rv(pNO)
        phi_q = phi_rv(qNO)
        na_p = ONa(pNO)
        nb_p = ONb(pNO)
        na_q = ONa(qNO)
        nb_q = ONb(qNO)
!
        rho_grad(Ipair) = 2.0D0*(na_q + nb_q - na_p - nb_p)*phi_p*phi_q
!
        do Jpair = 1, Npair
          rNO = orbpr(Jpair,1)
          sNO = orbpr(Jpair,2)
          phi_r = phi_rv(rNO)
          phi_s = phi_rv(sNO)
!
          rho_hess(Ipair,Jpair) = chessrho(1,Ipair,Jpair)*phi_q*phi_s&
                                + chessrho(2,Ipair,Jpair)*phi_p*phi_r&
                                + chessrho(3,Ipair,Jpair)*phi_q*phi_r&
                                + chessrho(4,Ipair,Jpair)*phi_p*phi_s
!
        end do ! Jpair
!
        do Istat = 1, Nstat
          rho_hess(Ipair,Npair+Istat) = 4.0D0*(don(qNO,Istat) - don(pNO,Istat))*phi_p*phi_q
          rho_hess(Npair+Istat,Ipair) = rho_hess(Ipair,Npair+Istat)
        end do ! Istat
!
      end do ! Ipair
!
      do Istat = 1, Nstat
        kNO = stat_pair(Istat,1)
        tNO = stat_pair(Istat,2)
!
        rho_grad(Npair+Istat) = 2.0D0*(phi_rv(tNO)**2 - phi_rv(kNO)**2)
!
      end do ! Istat
!
! rho is linear in Delta, no hessian contribution
!
      deallocate(phi_rv)
!
      end subroutine GET_den_hess_point
      subroutine GET_R2RDM_OS_hess_point (rv, tp, R2RDMos_grid, R2RDMos_grad, R2RDMos_hess)
!*******************************************************************************
!     Date last modified: April 19, 2022                                       *
!     Authors: JWH                                                             *
!     Description: Compute the EPUM and Delta derivative and hessian of the    *
!                  OS two-electron density at a point, and the 2RDM itself.    *
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE INT_objects
      USE CFT_objects

      implicit none
!
! Input arrays:
      double precision, intent(IN) :: rv(3),tp(3)
!
! Output scalars:
      double precision, intent(OUT) :: R2RDMos_grid
      double precision, intent(OUT) :: R2RDMos_grad(Npair+Nstat)
      double precision, intent(OUT) :: R2RDMos_hess(Npair+Nstat,Npair+Nstat)
!
! Local arrays:
      double precision, allocatable, dimension(:) :: phi_rv, phi_tp
!
! Local scalars:
      integer :: pNO, qNO, aNO, Ipair, Jpair, Istat, Jstat, rNO, sNO, aocc, pocc, qocc
      double precision :: J, c1J, c2J, c3J, c4J
      double precision :: L, c1L, c2L, c3L, c4L
      double precision :: Jpr, Jqs, Jqr, Jps
      double precision :: Lpr, Lqs, Lqr, Lps
      double precision :: phia2, pqJ, prJ, qsJ, qrJ, psJ, pqK, prK, qsK, qrK, psK
      double precision :: prqs, prsq, psrq
!
! Begin:
!
      allocate(phi_rv(Nbasis),phi_tp(Nbasis))
! Get MOs at both points
      call GET_MO_point(rv, Nbasis, NO, phi_rv)
      call GET_MO_point(tp, Nbasis, NO, phi_tp)
!
      R2RDMos_grid = zero
      R2RDMos_grad = zero
      R2RDMos_hess = zero
!
      do pocc = 1, Nocc
        pNO = NO_occ(pocc)
        do qocc = 1, Nocc
          qNO = NO_occ(qocc)
!
          J = phi_rv(pNO)**2*phi_tp(qNO)**2
          L = phi_rv(pNO)*phi_tp(pNO)*phi_rv(qNO)*phi_tp(qNO)
!
          R2RDMos_grid = R2RDMos_grid + R2RDMJos(pNO,qNO)*J + R2RDMLos(pNO,qNO)*L
!
          do Istat = 1, Nstat
!
            R2RDMos_grad(Npair+Istat) = R2RDMos_grad(Npair+Istat) + dR2RDMJos(pNO,qNO,Istat)*J&
                                                                  + dR2RDMLos(pNO,qNO,Istat)*L
            do Jstat = 1, Istat
!
              R2RDMos_hess(Npair+Istat,Npair+Jstat) = R2RDMos_hess(Npair+Istat,Npair+Jstat)&
              + d2R2RDMJos(pNO,qNO,Istat,Jstat)*J + d2R2RDMLos(pNO,qNO,Istat,Jstat)*L
!
            end do ! Jstat
          end do ! Istat
!
        end do ! qNO
      end do ! pNO
!
      do Ipair = 1, Npair
        pNO = orbpr(Ipair,1)
        qNO = orbpr(Ipair,2)
        pqJ = phi_tp(pNO)*phi_tp(qNO)
        pqK = phi_rv(pNO)*phi_tp(qNO)
!
        do aocc = 1, Nocc
          aNO = NO_occ(aocc)
!
          J = phi_rv(aNO)**2*pqJ
          L = phi_rv(aNO)*phi_tp(aNO)*pqK
!
          R2RDMos_grad(Ipair) = R2RDMos_grad(Ipair)&
                              + (R2RDMJos(aNO,qNO) - R2RDMJos(aNO,pNO))*J&
                              + (R2RDMLos(aNO,qNO) - R2RDMLos(aNO,pNO))*L
!
          do Istat = 1, Nstat
            R2RDMos_hess(Ipair,Npair+Istat) = R2RDMos_hess(Ipair,Npair+Istat)&
                                            + (dR2RDMJos(aNO,qNO,Istat) - dR2RDMJos(aNO,pNO,Istat))*J&
                                            + (dR2RDMLos(aNO,qNO,Istat) - dR2RDMLos(aNO,pNO,Istat))*L
          end do ! Istat
        end do ! aNO
!
        do Jpair = 1, Ipair
          rNO = orbpr(Jpair,1)
          sNO = orbpr(Jpair,2)
          prJ = phi_tp(pNO)*phi_tp(rNO)
          qsJ = phi_tp(qNO)*phi_tp(sNO)
          qrJ = phi_tp(qNO)*phi_tp(rNO)
          psJ = phi_tp(pNO)*phi_tp(sNO)
          prK = phi_rv(pNO)*phi_tp(rNO)
          qsK = phi_rv(qNO)*phi_tp(sNO)
          qrK = phi_rv(qNO)*phi_tp(rNO)
          psK = phi_rv(pNO)*phi_tp(sNO)
!
          do aocc = 1, Nocc
            aNO = NO_occ(aocc)
!
            phia2 = phi_rv(aNO)**2
            Jpr = phia2*prJ
            Jqs = phia2*qsJ
            Jqr = phia2*qrJ
            Jps = phia2*psJ
            phia2 = phi_rv(aNO)*phi_tp(aNO)
            Lpr = phia2*prK
            Lqs = phia2*qsK
            Lqr = phia2*qrK
            Lps = phia2*psK
!
            c1J = chessJ(1,aNO,Ipair,Jpair) 
            c2J = chessJ(2,aNO,Ipair,Jpair) 
            c3J = chessJ(3,aNO,Ipair,Jpair) 
            c4J = chessJ(4,aNO,Ipair,Jpair) 
!
            c1L = chessL(1,aNO,Ipair,Jpair) 
            c2L = chessL(2,aNO,Ipair,Jpair) 
            c3L = chessL(3,aNO,Ipair,Jpair) 
            c4L = chessL(4,aNO,Ipair,Jpair) 
!
            R2RDMos_hess(Ipair,Jpair) = R2RDMos_hess(Ipair,Jpair) + c1J*Jqs + c1L*Lqs&
                                                                  + c2J*Jpr + c2L*Lpr&
                                                                  + c3J*Jqr + c3L*Lqr&
                                                                  + c4J*Jps + c4L*Lps 
!
          end do ! aNO
!
! Can make common factors with J and L above
          prqs = phi_rv(pNO)*phi_tp(rNO)*phi_rv(qNO)*phi_tp(sNO)
          prsq = phi_rv(pNO)*phi_tp(rNO)*phi_rv(sNO)*phi_tp(qNO)
          psrq = phi_rv(pNO)*phi_tp(sNO)*phi_rv(rNO)*phi_tp(qNO)
!          
          R2RDMos_hess(Ipair,Jpair) = R2RDMos_hess(Ipair,Jpair) + chessJ4i(Ipair,Jpair)*prqs&
                                                                + chessK4i(Ipair,Jpair)*(prsq + psrq)
          R2RDMos_hess(Jpair,Ipair) = R2RDMos_hess(Ipair,Jpair)
!
        end do ! Jpair
      end do ! Ipair
!
      do Istat = 1, Nstat
        !
        do Ipair = 1, Npair
          R2RDMos_hess(Npair+Istat,Ipair) = R2RDMos_hess(Ipair,Npair+Istat)
        end do ! Ipair 
        !
        do Jstat = 1, Istat-1
          R2RDMos_hess(Npair+Jstat,Npair+Istat) = R2RDMos_hess(Npair+Istat,Npair+Jstat)
        end do ! Jstat
        !
      end do ! Istat 
!
      R2RDMos_hess(1:Npair,Npair+1:Npair+Nstat) = 4.0D0*R2RDMos_hess(1:Npair,Npair+1:Npair+Nstat)
      R2RDMos_hess(Npair+1:Npair+Nstat,1:Npair) = 4.0D0*R2RDMos_hess(Npair+1:Npair+Nstat,1:Npair)
      R2RDMos_grad(1:Npair) = 4.0D0*R2RDMos_grad(1:Npair)
!
      end subroutine GET_R2RDM_OS_hess_point
      subroutine GET_TDF_hess_point(Rv, rf, rf_grad, rf_hess, tdf_grad_pt, tdf_hess_pt)
!*******************************************************************************
!     Date last modified: April 18, 2022                                       *
!     Authors: JWH                                                             *
!     Description: Compute the EPUM and Delta hessian of the renormalization   *
!                  function at the given point.                                *
!                  Costs little extra for gradient, so calculate that too.     *
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE INT_objects
      USE CFT_objects

      implicit none
!
! Input arrays:
      double precision, intent(IN) :: Rv(3), rf, rf_grad(Npair+Nstat), rf_hess(Npair+Nstat,Npair+Nstat)
!
! Output scalars:
      double precision, intent(OUT) :: tdf_grad_pt(Npair+Nstat), tdf_hess_pt(Npair+Nstat,Npair+Nstat)
!
! Local scalars:
      double precision :: g, u, cusp, q, lam, dg, dlam, d2g, d2lam
      double precision :: rho
      double precision :: R2RDMos_grid, R2RDM_grid, wts
      integer :: I_ux, I_uy, I_uz, I, J
! 
! Local arrays:
      double precision :: uv(3), r1v(3), r2v(3), umag(3)
      double precision, allocatable, dimension(:) :: u_pts, u_wts
      double precision, allocatable, dimension(:) :: R2RDMos_grad, R2RDM_grad, rho_grad
      double precision, allocatable, dimension(:) :: g_grad, lam_grad, temp_grad
      double precision, allocatable, dimension(:,:) :: R2RDMos_hess, R2RDM_hess, rho_hess
      double precision, allocatable, dimension(:,:) :: lam_grad2, d2lam_grad, g_hess, lam_hess
      double precision, allocatable, dimension(:,:) :: grad_prod1, grad_prod2, grad_prod3
!
! Begin:
!
      q = q_TDF
!
      tdf_grad_pt = zero
      tdf_hess_pt = zero
!
      allocate(R2RDMos_grad(Npair+Nstat),R2RDM_grad(Npair+Nstat))
      allocate(rho_grad(Npair+Nstat),g_grad(Npair+Nstat),lam_grad(Npair+Nstat))
      allocate(temp_grad(Npair+Nstat))
      allocate(rho_hess(Npair+Nstat,Npair+Nstat),lam_hess(Npair+Nstat,Npair+Nstat))
      allocate(R2RDMos_hess(Npair+Nstat,Npair+Nstat),R2RDM_hess(Npair+Nstat,Npair+Nstat))
      allocate(lam_grad2(Npair+Nstat,Npair+Nstat),d2lam_grad(Npair+Nstat,Npair+Nstat))
      allocate(g_hess(Npair+Nstat,Npair+Nstat),grad_prod1(Npair+Nstat,Npair+Nstat))
      allocate(grad_prod2(Npair+Nstat,Npair+Nstat),grad_prod3(Npair+Nstat,Npair+Nstat))
!
      call GET_den_point (Rv, rho)
!
      if(rho.gt.1.0D-12)then
!
      call GET_den_hess_point (Rv, rho_grad, rho_hess)
!
! Current correlation length ansatz
      lam   = q*rho**F1_3
      dlam  = q/(3.0D0*rho**F2_3)
      d2lam = -2.0D0*q/(9.0D0*rho**F5_3)
      lam_grad = dlam*rho_grad
      lam_hess = dlam*rho_hess
!
      lam_grad2   = zero
      d2lam_grad  = zero
      do I = 1, Nstat + Npair
        do J = 1, Nstat + Npair
          lam_grad2(I,J)  = lam_grad(I)*lam_grad(J)
          d2lam_grad(I,J) = d2lam*rho_grad(I)*rho_grad(J)
        end do ! J
      end do ! I
!
! grid parameters (use density at R to determine these)          
      !u_scale = max(min(4.6D0/lam - 0.3D0, 2.0D0),1.0D0)
      allocate(u_pts(N_u_pts), u_wts(N_u_pts))
      call BLD_MK_grid(N_u_pts, u_scale, u_pts, u_wts)
!
! Integrate over u
!
      do I_ux = 1, N_u_pts
        umag(1) = u_pts(I_ux)
        do I_uy = 1, N_u_pts
          umag(2) = u_pts(I_uy)
          do I_uz = 1, N_u_pts
            umag(3) = u_pts(I_uz)
!
            R2RDM_grid = zero
            R2RDM_grad = zero
            R2RDM_hess = zero
!
            wts = u_wts(I_ux)*u_wts(I_uy)*u_wts(I_uz)
!
            u = dsqrt(dot_product(umag,umag))
!
            g      = dexp(-(lam*u)**2)
            cusp   = dexp(u)
            !cusp   = 2.0D0 - dexp(-u)
!
            dg     = -2.0D0*g*u**2*lam
            g_grad = dg*lam_grad
!
            d2g    = 2.0D0*g*u**2*(2.0D0*(u*lam)**2 - 1.0D0)
            g_hess = d2g*lam_grad2 + dg*(d2lam_grad + lam_hess)
!
! (+,+,+)
            uv = umag
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_OS_hess_point (r1v, r2v, R2RDMos_grid, R2RDMos_grad, R2RDMos_hess)
!
            R2RDM_grid = R2RDM_grid + R2RDMos_grid
            R2RDM_grad = R2RDM_grad + R2RDMos_grad
            R2RDM_hess = R2RDM_hess + R2RDMos_hess
!
! (+,+,-)
            uv(3) = -umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_OS_hess_point (r1v, r2v, R2RDMos_grid, R2RDMos_grad, R2RDMos_hess)
!
            R2RDM_grid = R2RDM_grid + R2RDMos_grid
            R2RDM_grad = R2RDM_grad + R2RDMos_grad
            R2RDM_hess = R2RDM_hess + R2RDMos_hess
!
! (+,-,+)
            uv(1) =  umag(1)
            uv(2) = -umag(2)
            uv(3) =  umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_OS_hess_point (r1v, r2v, R2RDMos_grid, R2RDMos_grad, R2RDMos_hess)
!
            R2RDM_grid = R2RDM_grid + R2RDMos_grid
            R2RDM_grad = R2RDM_grad + R2RDMos_grad
            R2RDM_hess = R2RDM_hess + R2RDMos_hess
!
! (-,+,+)
            uv(1) = -umag(1)
            uv(2) =  umag(2)
            uv(3) =  umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_OS_hess_point (r1v, r2v, R2RDMos_grid, R2RDMos_grad, R2RDMos_hess)
!
            R2RDM_grid = R2RDM_grid + R2RDMos_grid
            R2RDM_grad = R2RDM_grad + R2RDMos_grad
            R2RDM_hess = R2RDM_hess + R2RDMos_hess
!
! (+,-,-)
            uv(1) =  umag(1)
            uv(2) = -umag(2)
            uv(3) = -umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_OS_hess_point (r1v, r2v, R2RDMos_grid, R2RDMos_grad, R2RDMos_hess)
!
            R2RDM_grid = R2RDM_grid + R2RDMos_grid
            R2RDM_grad = R2RDM_grad + R2RDMos_grad
            R2RDM_hess = R2RDM_hess + R2RDMos_hess
!
! (-,+,-)
            uv(1) = -umag(1)
            uv(2) =  umag(2)
            uv(3) = -umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_OS_hess_point (r1v, r2v, R2RDMos_grid, R2RDMos_grad, R2RDMos_hess)
!
            R2RDM_grid = R2RDM_grid + R2RDMos_grid
            R2RDM_grad = R2RDM_grad + R2RDMos_grad
            R2RDM_hess = R2RDM_hess + R2RDMos_hess
!
! (-,-,+)
            uv(1) = -umag(1)
            uv(2) = -umag(2)
            uv(3) =  umag(3)
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_OS_hess_point (r1v, r2v, R2RDMos_grid, R2RDMos_grad, R2RDMos_hess)
!
            R2RDM_grid = R2RDM_grid + R2RDMos_grid
            R2RDM_grad = R2RDM_grad + R2RDMos_grad
            R2RDM_hess = R2RDM_hess + R2RDMos_hess
!
! (-,-,-)
            uv = -umag
!
            r1v = Rv + 0.5D0*uv
            r2v = Rv - 0.5D0*uv
!
            call GET_R2RDM_OS_hess_point (r1v, r2v, R2RDMos_grid, R2RDMos_grad, R2RDMos_hess)
!
            R2RDM_grid = R2RDM_grid + R2RDMos_grid
            R2RDM_grad = R2RDM_grad + R2RDMos_grad
            R2RDM_hess = R2RDM_hess + R2RDMos_hess
!
! integrands
!
            tdf_grad_pt = tdf_grad_pt + wts*((R2RDM_grad*g + R2RDM_grid*g_grad)*(rf*cusp - one)/u&
                                        + R2RDM_grid*g*rf_grad*cusp/u)
!
            grad_prod1 = zero
            grad_prod2 = zero
            grad_prod3 = zero
            do I = 1, Npair + Nstat 
              do J = 1, I
                grad_prod1(I,J) = R2RDM_grad(I)*g_grad(J) + R2RDM_grad(J)*g_grad(I)
                grad_prod1(J,I) = grad_prod1(I,J)
                grad_prod2(I,J) = R2RDM_grad(I)*rf_grad(J) + R2RDM_grad(J)*rf_grad(I)
                grad_prod2(J,I) = grad_prod2(I,J)
                grad_prod3(I,J) = g_grad(I)*rf_grad(J) + g_grad(J)*rf_grad(I)
                grad_prod3(J,I) = grad_prod3(I,J)
              end do ! J
            end do ! I
!
            tdf_hess_pt = tdf_hess_pt&
                         + wts*((R2RDM_hess*g + grad_prod1 + R2RDM_grid*g_hess)*(rf*cusp - one)/u&
                         + (grad_prod2*g + R2RDM_grid*grad_prod3 + R2RDM_grid*g*rf_hess)*cusp/u)
!
          end do ! I_uz
        end do ! I_uy
      end do ! I_ux
!
      end if ! rho > tol
!
      end subroutine GET_TDF_hess_point
      subroutine GET_den_point(rv, rho)
!*******************************************************************
!   Date last modified: March 25, 2022                             *
!   Author: JWH                                                    *
!   Description: Calculate the EPUM gradient and hessian of the    *
!                one-electron density.                             * 
!*******************************************************************
! Modules:
      USE AO_values
      USE type_molecule
      USE module_grid_points
      USE program_constants
      USE QM_objects
      USE CFT_objects

      implicit none
!
! Input arrays:
      double precision, intent(IN) :: rv(3)
!
! Output arrays:
      double precision, intent(OUT) :: rho
!
! Local scalars:
      integer   :: pNO, pocc
!
! Local arrays:
      double precision, allocatable, dimension(:) :: phi_rv, ONa, ONb
!
! Begin:
!
! rv - grid point
!
      allocate(phi_rv(Nbasis))
!
      rho = zero
!
      call GET_MO_point(rv, Nbasis, NO, phi_rv)
!
! set alpha and beta occupancies
      allocate(ONa(Nbasis),ONb(Nbasis))
      ONa = ON
      ONb = ON
      ! open orbitals
      ONb(NBelectrons+1:NAelectrons) = zero
!
      do pocc = 1, Nocc
        pNO = NO_occ(pocc)
!
        rho = rho + (ONa(pNO) + ONb(pNO))*phi_rv(pNO)**2
      end do ! pNO
!
      deallocate(phi_rv)
!
      end subroutine GET_den_point
      subroutine CFT_TDF_mesh
!*********************************************************************************************
!     Date last modified: April 11, 2022                                                     *
!     Author: JWH                                                                            *
!     Desciption: Calculate the 2-electron density functional on a grid.                     *
!*********************************************************************************************
! Modules:
      USE program_constants
      USE program_timing
      USE program_files
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE CFT_objects
      USE module_grid_points
      USE INT_objects
      USE type_weights
      USE type_plotting

      implicit none
!
! Local scalars:
      integer :: Ipoint
      double precision :: t0, t1, t2, time
      double precision :: rf, tdf
!
! Local arrays:
      double precision, dimension(:), allocatable :: ONa, ONb, tdf_total
      double precision :: Rv(3)
!
! Begin:
      call PRG_manager ('enter', 'CFT_TDF_mesh', 'UTILITY')
!
! Grid menu allocates grid_points, deallocate here and build grid after
      if(allocated(grid_points))then
        deallocate(grid_points)
      end if
!
      call GET_object ('CFT', '2RDM', 'DELTANO')
!
      if(LMesh)then
        call BLD_GRID_MESH
      else
        call BLD_GRID_RADIAL
      end if
!
! Get density matrices
      ! set alpha and beta occupancies
      allocate(ONa(Nbasis),ONb(Nbasis))
      ONa = ON
      ONb = ON
      ! open orbitals
      ONb(NBelectrons+1:NAelectrons) = zero
!
      if(.not.allocated(PM0_alpha))then
        allocate(PM0_alpha(MATlen),PM0_beta(MATlen))
      end if
      call DENBLD(NO,ONa,PM0_alpha,Nbasis,MATlen,Nbasis)
      call DENBLD(NO,ONb,PM0_beta,Nbasis,MATlen,Nbasis)
!
      call CFT_BLD_R2RDM
! combine alpha-beta and beta-alpha to cut work in half
      allocate(R2RDMJos(Nbasis,Nbasis),R2RDMLos(Nbasis,Nbasis))
      R2RDMJos = R2RDMJab + R2RDMJba
      R2RDMLos = R2RDMLab + R2RDMLba
!
      allocate(tdf_total(NGridPoints))
!
      write(UNIout,'(a,F5.3)') 'q_TDF= ',q_TDF
!
      tdf_total = zero
!
      do Ipoint = 1, NGridPoints
!
        Rv(1) = grid_points(Ipoint)%x
        Rv(2) = grid_points(Ipoint)%y
        Rv(3) = grid_points(Ipoint)%z
!
        call GET_RF_point(Rv, rf)
!
        call GET_TDF_point(Rv, rf, tdf)
!
        tdf_total(Ipoint) = tdf
!
      end do ! Ipoint
!
      deallocate(R2RDMJos,R2RDMLos)
!
! Print the TDF value
!
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'TDF_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular 2e opposite-spin density functional'
        call PRT_GRID_MOL (tdf_total, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular 2e opposite-spin density functional'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (tdf_total, NGridPoints, 'F21.14')
      end if
!
      deallocate(grid_points)
!
! End of routine CFT_TDF_mesh
      call PRG_manager ('exit', 'CFT_TDF_mesh', 'UTILITY')
      end subroutine CFT_TDF_mesh
      subroutine CFT_ODF_energy
!*********************************************************************************************
!     Date last modified: May 25 11, 2022                                                    *
!     Author: JWH                                                                            *
!     Desciption: Calculate the on-top density functional energy.                            *
!*********************************************************************************************
! Modules:
      USE program_constants
      USE program_timing
      USE program_files
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE CFT_objects
      USE module_grid_points
      USE INT_objects

      implicit none
!
! Local scalars:
      integer :: Iatom,IApoint,Znum,Ifound
      double precision :: t0, t1, t2, time
      double precision :: rf, odf, odf_pt
!
! Local arrays:
      double precision, dimension(:), allocatable :: WeightsA, ONa, ONb
      double precision :: Rv(3)
!
! Begin:
      call PRG_manager ('enter', 'CFT_ODF_energy', 'UTILITY')
!
! need this? Need to ensure density matrices are up to date for each iteration
! Get density matrices
      ! set alpha and beta occupancies
      allocate(ONa(Nbasis),ONb(Nbasis))
      ONa = ON
      ONb = ON
      ! open orbitals
      ONb(NBelectrons+1:NAelectrons) = zero
!
      if(.not.allocated(PM0_alpha))then
        allocate(PM0_alpha(MATlen),PM0_beta(MATlen))
      end if
      call DENBLD(NO,ONa,PM0_alpha,Nbasis,MATlen,Nbasis)
      call DENBLD(NO,ONb,PM0_beta,Nbasis,MATlen,Nbasis)
!
      call CFT_BLD_R2RDM
! combine alpha-beta and beta-alpha to cut work in half
      allocate(R2RDMJos(Nbasis,Nbasis),R2RDMLos(Nbasis,Nbasis))
      R2RDMJos = R2RDMJab + R2RDMJba
      R2RDMLos = R2RDMLab + R2RDMLba
!
! Get grid and weights
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
!      write(UNIout,'(/2a)')'Radial grid used for numerical integration: ',RADIAL_grid
      call BLD_BeckeW_XI
!      write(UNIout,'(2a)')'Weights used for numerical integration:      BECKE'
!
      call CPU_time(t0)
      call CPU_time(t1)
!
      odf = zero
!
!      write(UNIout,'(a,F5.3)') 'q_TDF = ',q_TDF
!
      do Iatom=1,Natoms
        Znum=CARTESIAN(Iatom)%atomic_number
        if(Znum.le.0)cycle
        Ifound=GRID_loc(Znum)
        NApts_atom=NApoints_atom(Ifound)

        allocate(grid_points(1:NApts_atom))
        allocate(WeightsA(NApts_atom))
!
! Moving grid points on cartesian coordinate grid
        do IApoint=1,NApts_atom
          grid_points(IApoint)%X=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          grid_points(IApoint)%Y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          grid_points(IApoint)%Z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
        end do ! IApoint

        call GET_weights (grid_points, NApts_atom, Iatom, WeightsA)

        do IApoint=1,NApts_atom
! Extracule coordinates
          Rv(1) = grid_points(IApoint)%X
          Rv(2) = grid_points(IApoint)%Y
          Rv(3) = grid_points(IApoint)%Z
!
! Get gradient of the two-electron density functional
          call GET_ODF_point(Rv, odf_pt)
!
          odf = odf + odf_pt*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
!
        end do ! IApoint
!
        deallocate(grid_points)
        deallocate(WeightsA)
!
      end do ! Iatom
!
      E_ODF = FourPI*odf
!
! If not variational, use virial theorem
      if(.not.LTDFvar) E_ODF = E_ODF/2.0D0
!
!      write(6,*)'E_TDF = ',E_TDF 
!
      call CPU_time(t2)
      time = t2 - t1
!
      deallocate(R2RDMJos,R2RDMLos)
!
! End of routine CFT_ODF_energy
      call PRG_manager ('exit', 'CFT_ODF_energy', 'UTILITY')
      end subroutine CFT_ODF_energy
      subroutine GET_ODF_point (Rv, odf)
!*******************************************************************************
!     Date last modified: May 25, 2022                                         *
!     Authors: JWH                                                             *
!     Description: Compute the EPUM and Delta gradient of the 2e density       *
!                  functional at the given point.                              *
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE INT_objects
      USE CFT_objects

      implicit none
!
! Input arrays:
      double precision, intent(IN) :: Rv(3)
!
! Output arrays:
      double precision, intent(OUT) :: odf
!
! Local scalars:
      double precision :: q, lam, lam2, erfo, expo, pi, erfc, sqrtpi
      double precision :: rho
      double precision :: G
!
! Functions:
      double precision :: derf
! 
! Begin:
!
      pi = PI_VAL
      sqrtpi = dsqrt(pi)
!
      q = q_TDF
!
      odf  = zero
!
      call GET_den_point (Rv, rho)
!
      if(rho.gt.1.0D-12)then
!
! Current correlation length ansatz
        lam  = q*rho**F1_3
        lam2 = lam**2
        expo = dexp(-0.25D0/lam2)
        erfo = one + derf(0.5D0/lam)
        erfc = one - derf(0.5D0/lam)
!
        call GET_R2RDM_OS_point (Rv, Rv, G)
!
! cusp = dexp(u)
!
!        odf = G*two*pi*(two*lam*(sqrtpi*lam - one)*expo + sqrtpi*(sqrtpi*lam - one&
!             - two*lam2)*erfo)/(lam2*(two*lam*expo + sqrtpi*(one + two*lam2)*erfo))
!
! cusp = 2 - dexp(-u)
!
        if(lam.gt.0.1D0)then
        odf = G*two*pi*(two*lam*(one + sqrtpi*lam)*expo - sqrtpi*(one + sqrtpi*lam + two*lam2)*erfc)&
                /(lam2*(sqrtpi*(one + two*lam2)*erfc - two*lam*(one + two*sqrtpi*lam)*expo))
        else
        odf = G*(128.0D0*lam**7*(one - 37.0D0*lam2)/sqrtpi + 32.0D0*(lam**4 - 25.0D0*lam**6&
                 + 534.0D0*lam**8) - two*pi*(one - 6.0D0*lam2 + 60.0D0*lam**4 - 840.0D0*lam**6&
                 + 15120.0D0*lam**8) + 8.0D0*sqrtpi*(lam - 13.0D0*lam**3 + 198.0D0*lam**5&
                 - 3672.0D0*lam**7 + 81600.0D0*lam**9))
        end if 
!
      end if ! rho > tol
!
      end subroutine GET_ODF_point
      subroutine CFT_ODF_mesh
!*********************************************************************************************
!     Date last modified: May 25, 2022                                                       *
!     Author: JWH                                                                            *
!     Desciption: Calculate the on-top density functional on a mesh.                         *
!*********************************************************************************************
! Modules:
      USE program_constants
      USE program_timing
      USE program_files
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE CFT_objects
      USE module_grid_points
      USE INT_objects
      USE type_weights
      USE type_plotting

      implicit none
!
! Local scalars:
      integer :: Ipoint
      double precision :: t0, t1, t2, time
      double precision :: rf, odf
!
! Local arrays:
      double precision, dimension(:), allocatable :: ONa, ONb, odf_total
      double precision :: Rv(3)
!
! Begin:
      call PRG_manager ('enter', 'CFT_ODF_mesh', 'UTILITY')
!
      call GET_object ('CFT', '2RDM', 'DELTANO')
!
      if(LMesh)then
        call BLD_GRID_MESH
      else
        call BLD_GRID_RADIAL 
      end if
!
! Get density matrices
      ! set alpha and beta occupancies
      allocate(ONa(Nbasis),ONb(Nbasis))
      ONa = ON
      ONb = ON
      ! open orbitals
      ONb(NBelectrons+1:NAelectrons) = zero
!
      if(.not.allocated(PM0_alpha))then
        allocate(PM0_alpha(MATlen),PM0_beta(MATlen))
      end if
      call DENBLD(NO,ONa,PM0_alpha,Nbasis,MATlen,Nbasis)
      call DENBLD(NO,ONb,PM0_beta,Nbasis,MATlen,Nbasis)
!
      call CFT_BLD_R2RDM
! combine alpha-beta and beta-alpha to cut work in half
      allocate(R2RDMJos(Nbasis,Nbasis),R2RDMLos(Nbasis,Nbasis))
      R2RDMJos = R2RDMJab + R2RDMJba
      R2RDMLos = R2RDMLab + R2RDMLba
!
      allocate(odf_total(NGridPoints))
!
      write(UNIout,'(a,F5.3)') 'q_TDF= ',q_TDF
!
      odf_total = zero
!
      do Ipoint = 1, NGridPoints
!
        Rv(1) = grid_points(Ipoint)%x
        Rv(2) = grid_points(Ipoint)%y
        Rv(3) = grid_points(Ipoint)%z
!
        call GET_ODF_point(Rv, odf)
!
        odf_total(Ipoint) = odf
!
      end do ! Ipoint
!
      deallocate(R2RDMJos,R2RDMLos)
!
! Print the ODF value
!
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'ODF_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular opposite-spin on-top density functional'
        call PRT_GRID_MOL (odf_total, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular opposite-spin on-top density functional'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (odf_total, NGridPoints, 'F21.14')
      end if
!
      deallocate(grid_points)
!
! End of routine CFT_ODF_mesh
      call PRG_manager ('exit', 'CFT_ODF_mesh', 'UTILITY')
      end subroutine CFT_ODF_mesh
      subroutine GET_OD_point (Rv, od)
!*******************************************************************************
!     Date last modified: May 18, 2023                                         *
!     Authors: JWH                                                             *
!     Description: Compute the on-top density at the given point.              *
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE INT_objects
      USE CFT_objects

      implicit none
!
! Input arrays:
      double precision, intent(IN) :: Rv(3)
!
! Output arrays:
      double precision, intent(OUT) :: od
!
! Local scalars:
      double precision :: G
!
! Begin:
!
      call GET_R2RDM_OS_point (Rv, Rv, G)
!
      od = G
!
      end subroutine GET_OD_point
      subroutine CFT_OD_mesh
!*********************************************************************************************
!     Date last modified: May 25, 2022                                                       *
!     Author: JWH                                                                            *
!     Desciption: Calculate the on-top density on a mesh.                                    *
!*********************************************************************************************
! Modules:
      USE program_constants
      USE program_timing
      USE program_files
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE CFT_objects
      USE module_grid_points
      USE INT_objects
      USE type_weights
      USE type_plotting

      implicit none
!
! Local scalars:
      integer :: Ipoint
      double precision :: t0, t1, t2, time
      double precision :: od
!
! Local arrays:
      double precision, dimension(:), allocatable :: ONa, ONb, od_total
      double precision :: Rv(3)
!
! Begin:
      call PRG_manager ('enter', 'CFT_OD_mesh', 'UTILITY')
!
      call GET_object ('CFT', '2RDM', 'DELTANO')
!
      if(LMesh)then
        call BLD_GRID_MESH
      else
        call BLD_GRID_RADIAL
      end if
!
! Get density matrices
      ! set alpha and beta occupancies
      allocate(ONa(Nbasis),ONb(Nbasis))
      ONa = ON
      ONb = ON
      ! open orbitals
      ONb(NBelectrons+1:NAelectrons) = zero
!
      if(.not.allocated(PM0_alpha))then
        allocate(PM0_alpha(MATlen),PM0_beta(MATlen))
      end if
      call DENBLD(NO,ONa,PM0_alpha,Nbasis,MATlen,Nbasis)
      call DENBLD(NO,ONb,PM0_beta,Nbasis,MATlen,Nbasis)
!
      call CFT_BLD_R2RDM
! combine alpha-beta and beta-alpha to cut work in half
      allocate(R2RDMJos(Nbasis,Nbasis),R2RDMLos(Nbasis,Nbasis))
      R2RDMJos = R2RDMJab + R2RDMJba
      R2RDMLos = R2RDMLab + R2RDMLba
!
      allocate(od_total(NGridPoints))
!
      od_total = zero
!
      do Ipoint = 1, NGridPoints
!
        Rv(1) = grid_points(Ipoint)%x
        Rv(2) = grid_points(Ipoint)%y
        Rv(3) = grid_points(Ipoint)%z
!
        call GET_OD_point(Rv, od)
!
        od_total(Ipoint) = od
!
      end do ! Ipoint
!
      deallocate(R2RDMJos,R2RDMLos)
!
! Print the OD values
!
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'OD_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular on-top density'
        call PRT_GRID_MOL (od_total, NGridPoints, 'F21.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular on-top density'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (od_total, NGridPoints, 'F21.14')
      end if
!
      deallocate(grid_points)
!
! End of routine CFT_OD_mesh
      call PRG_manager ('exit', 'CFT_OD_mesh', 'UTILITY')
      end subroutine CFT_OD_mesh
      subroutine GET_ODugrad_OS_point (Rv, ODugrad_os)
!*******************************************************************************
!     Date last modified: June 9, 2023                                         *
!     Authors: JWH                                                             *
!     Description: Calculate the u gradient of the opposite-spin on-top        *
!                  density at a point.                                         *
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE INT_objects
      USE CFT_objects

      implicit none
!
! Input arrays:
      double precision, intent(IN) :: Rv(3)
!
! Output arrays:
      double precision, intent(OUT) :: ODugrad_os(3)
!
! Local scalars:
      integer :: pNO, qNO, pocc, qocc
      double precision :: dJ(3), dL(3)
!
! Local arrays:
      double precision, allocatable, dimension(:) :: phi
      double precision, allocatable, dimension(:,:) :: dphi
!
! Begin:
!
      ODugrad_os = zero
!
      allocate(phi(Nbasis),dphi(Nbasis,3))
      call GET_MO_point(Rv, Nbasis, NO, phi)
      call GET_MOgrad_point(Rv, Nbasis, NO, dphi)
!
      do pocc = 2, Nocc
        pNO = NO_occ(pocc)
        do qocc = 1, pocc-1
          qNO = NO_occ(qocc)
!
! Vanishes for p = q and cancels for p,q and q,p
          dJ(1:3) = phi(pNO)*phi(qNO)*(dphi(pNO,1:3)*phi(qNO) - phi(pNO)*dphi(qNO,1:3))
          ! dL is always zero
!
          ODugrad_os = ODugrad_os + R2RDMJos(pNO,qNO)*dJ
!
        end do ! qNO
      end do ! pNO
!
      end subroutine GET_ODugrad_OS_point
      subroutine CFT_ODugrad_OS_mesh
!*********************************************************************************************
!     Date last modified: June 12, 2023                                                      *
!     Author: JWH                                                                            *
!     Desciption: Calculate the opposite-spin on-top density gradient on a mesh.             *
!*********************************************************************************************
! Modules:
      USE program_constants
      USE program_timing
      USE program_files
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE CFT_objects
      USE module_grid_points
      USE INT_objects
      USE type_weights
      USE type_plotting

      implicit none
!
! Local scalars:
      integer :: Ipoint
      double precision :: t0, t1, t2, time
!
! Local arrays:
      double precision, dimension(:), allocatable :: ONa, ONb
      double precision, dimension(:), allocatable :: odugradx_tot, odugrady_tot, odugradz_tot
      double precision, dimension(:), allocatable :: odugrad_tot
      double precision :: Rv(3)
      double precision :: odugrad(3)
!
! Begin:
      call PRG_manager ('enter', 'CFT_ODugrad_OS_mesh', 'UTILITY')
!
      call GET_object ('CFT', '2RDM', 'DELTANO')
!
      if(LMesh)then
        call BLD_GRID_MESH
      else
        call BLD_GRID_RADIAL
      end if
!
! Get density matrices
      ! set alpha and beta occupancies
      allocate(ONa(Nbasis),ONb(Nbasis))
      ONa = ON
      ONb = ON
      ! open orbitals
      ONb(NBelectrons+1:NAelectrons) = zero
!
      if(.not.allocated(PM0_alpha))then
        allocate(PM0_alpha(MATlen),PM0_beta(MATlen))
      end if
      call DENBLD(NO,ONa,PM0_alpha,Nbasis,MATlen,Nbasis)
      call DENBLD(NO,ONb,PM0_beta,Nbasis,MATlen,Nbasis)
!
      call CFT_BLD_R2RDM
! combine alpha-beta and beta-alpha to cut work in half
      allocate(R2RDMJos(Nbasis,Nbasis),R2RDMLos(Nbasis,Nbasis))
      R2RDMJos = R2RDMJab + R2RDMJba
      R2RDMLos = R2RDMLab + R2RDMLba
!
      allocate(odugrad_tot(NGridPoints))
      allocate(odugradx_tot(NGridPoints))
      allocate(odugrady_tot(NGridPoints))
      allocate(odugradz_tot(NGridPoints))
!
      odugradx_tot = zero
      odugrady_tot = zero
      odugradz_tot = zero
!
      do Ipoint = 1, NGridPoints
!
        Rv(1) = grid_points(Ipoint)%x
        Rv(2) = grid_points(Ipoint)%y
        Rv(3) = grid_points(Ipoint)%z
!
        call GET_ODugrad_OS_point(Rv, odugrad)
!
        odugrad_tot(Ipoint)  = dsqrt(odugrad(1)**2 + odugrad(2)**2 + odugrad(3)**2)
!
      end do ! Ipoint
!
      deallocate(R2RDMJos,R2RDMLos)
!
! Print the gradient length value
!
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'ODugrad_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular on-top density u gradient length'
        call PRT_GRID_MOL (odugrad_tot, NGridPoints, 'F30.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular on-top density u gradient length'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (odugrad_tot, NGridPoints, 'F30.14')
      end if
!
      deallocate(grid_points)
!
! End of routine CFT_ODugrad_OS_mesh
      call PRG_manager ('exit', 'CFT_ODugrad_OS_mesh', 'UTILITY')
      end subroutine CFT_ODugrad_OS_mesh
      subroutine GET_ODulap_OS_point (Rv, ODulap_os)
!*******************************************************************************
!     Date last modified: June 12, 2023                                        *
!     Authors: JWH                                                             *
!     Description: Calculate the u Laplacian of the opposite-spin on-top       *
!                  density at a point.                                         *
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE INT_objects
      USE CFT_objects

      implicit none
!
! Input arrays:
      double precision, intent(IN) :: Rv(3)
!
! Output arrays:
      double precision, intent(OUT) :: ODulap_os
!
! Local scalars:
      integer :: pNO, qNO, pocc, qocc
      double precision :: d2J, d2L
      double precision :: phip, dxphip, dyphip, dzphip, dphip2, d2phip
      double precision :: phiq, dxphiq, dyphiq, dzphiq, dphiq2, d2phiq
!
! Local arrays:
      double precision, allocatable, dimension(:) :: phi, d2phi
      double precision, allocatable, dimension(:,:) :: dphi
!
! Begin:
!
      ODulap_os = zero
!
      allocate(phi(Nbasis),dphi(Nbasis,3),d2phi(Nbasis))
      call GET_MO_point(Rv, Nbasis, NO, phi)
      call GET_MOgrad_point(Rv, Nbasis, NO, dphi)
      call GET_MOlap_point(Rv, Nbasis, NO, d2phi)
!
      do pocc = 1, Nocc
        pNO = NO_occ(pocc)
        phip   =  phi(pNO)
        dxphip = dphi(pNO,1)
        dyphip = dphi(pNO,2)
        dzphip = dphi(pNO,3)
        dphip2 = dxphip**2 + dyphip**2 + dzphip**2
        d2phip = d2phi(pNO)
        do qocc = 1, Nocc
          qNO = NO_occ(qocc)
          phiq   =  phi(qNO)
          dxphiq = dphi(qNO,1)
          dyphiq = dphi(qNO,2)
          dzphiq = dphi(qNO,3)
          dphiq2 = dxphiq**2 + dyphiq**2 + dzphiq**2
          d2phiq = d2phi(qNO)
!
          d2J = 0.5D0*(phip**2*dphiq2 + phiq**2*dphip2 + phip*phiq**2*d2phip + phiq*phip**2*d2phiq&
              - 4.0D0*(dxphip*dxphiq + dyphip*dyphiq + dzphip*dzphiq))
!
          d2L = 0.5D0*(phip*phiq**2*d2phip + phiq*phip**2*d2phiq - phip**2*dphiq2 - phiq**2*dphip2)
!
          ODulap_os = ODulap_os + R2RDMJos(pNO,qNO)*d2J + R2RDMLos(pNO,qNO)*d2L
!
        end do ! qNO
      end do ! pNO
!
      end subroutine GET_ODulap_OS_point
      subroutine CFT_ODulap_OS_mesh
!*********************************************************************************************
!     Date last modified: June 12, 2023                                                      *
!     Author: JWH                                                                            *
!     Desciption: Calculate the opposite-spin on-top density Laplacian on a mesh.            *
!*********************************************************************************************
! Modules:
      USE program_constants
      USE program_timing
      USE program_files
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE CFT_objects
      USE module_grid_points
      USE INT_objects
      USE type_weights
      USE type_plotting

      implicit none
!
! Local scalars:
      integer :: Ipoint
      double precision :: t0, t1, t2, time
!
! Local arrays:
      double precision, dimension(:), allocatable :: ONa, ONb
      double precision, dimension(:), allocatable :: odulap_total
      double precision :: Rv(3)
      double precision :: odulap
!
! Begin:
      call PRG_manager ('enter', 'CFT_ODulap_OS_mesh', 'UTILITY')
!
      call GET_object ('CFT', '2RDM', 'DELTANO')
!
      if(LMesh)then
        call BLD_GRID_MESH
      else
        call BLD_GRID_RADIAL
      end if
!
! Get density matrices
      ! set alpha and beta occupancies
      allocate(ONa(Nbasis),ONb(Nbasis))
      ONa = ON
      ONb = ON
      ! open orbitals
      ONb(NBelectrons+1:NAelectrons) = zero
!
      if(.not.allocated(PM0_alpha))then
        allocate(PM0_alpha(MATlen),PM0_beta(MATlen))
      end if
      call DENBLD(NO,ONa,PM0_alpha,Nbasis,MATlen,Nbasis)
      call DENBLD(NO,ONb,PM0_beta,Nbasis,MATlen,Nbasis)
!
      call CFT_BLD_R2RDM
! combine alpha-beta and beta-alpha to cut work in half
      allocate(R2RDMJos(Nbasis,Nbasis),R2RDMLos(Nbasis,Nbasis))
      R2RDMJos = R2RDMJab + R2RDMJba
      R2RDMLos = R2RDMLab + R2RDMLba
!
      allocate(odulap_total(NGridPoints))
!
      odulap_total = zero
!
      do Ipoint = 1, NGridPoints
!
        Rv(1) = grid_points(Ipoint)%x
        Rv(2) = grid_points(Ipoint)%y
        Rv(3) = grid_points(Ipoint)%z
!
        call GET_ODulap_OS_point(Rv, odulap)
!
        odulap_total(Ipoint) = odulap
!
      end do ! Ipoint
!
      deallocate(R2RDMJos,R2RDMLos)
!
! Print the Laplacian
!
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'ODulap_mesh'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a)')'Molecular on-top density u Laplacian'
        call PRT_GRID_MOL (odulap_total, NGridPoints, 'F30.14')
        close (unit=Plotfile_unit)
      else ! print to standard output
        write(PlotFunction,'(a)')'Molecular on-top density u Laplacian'
        Plotfile_unit=UNIout
        call PRT_GRID_MOL (odulap_total, NGridPoints, 'F30.14')
      end if
!
      deallocate(grid_points)
!
! End of routine CFT_ODulap_OS_mesh
      call PRG_manager ('exit', 'CFT_ODulap_OS_mesh', 'UTILITY')
      end subroutine CFT_ODulap_OS_mesh
