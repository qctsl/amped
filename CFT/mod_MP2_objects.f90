      MODULE MP2_objects
!**************************************************************************************************
!     Date last modified: October 25, 2018                                                        *
!     Author: JWH                                                                                 *
!     Description: MP2 objects                                                                   *
!**************************************************************************************************
      implicit none
!
      double precision                                  :: E_MP2, maxMP2res
      double precision, allocatable, dimension(:)       :: onx, vnx
!      double precision, allocatable, dimension(:,:)     :: foo, fov, fvv
!      double precision, allocatable, dimension(:,:,:,:) :: t2, r2
!      double precision, allocatable, dimension(:,:,:,:) :: oovv
!
      integer :: maxMP2iter
!
! OIMP2 and ZAPT2 objects
      double precision, allocatable, dimension(:,:) :: F_ZAPT
      double precision :: E_ZAPT2(8) 
      double precision, allocatable, dimension(:) :: e_ZAPT, ep_ZAPT, em_ZAPT
!
      end MODULE MP2_objects
