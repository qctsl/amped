      subroutine BLD_IMPORT_BASIS
!*****************************************************************************************************************
!     Date last modified: July 14, 2016                                                             Version 2.0  *
!     Author: P. L. Warburton                                                                                    *
!     Description: Define a basis set based on import from a Gaussian general basis set file                     *
!*****************************************************************************************************************
! Modules:
      USE MENU_BLD_basis

      implicit none
!
! Local scalars:
      logical :: LKept
      integer :: line, subline, start, end
!
! Begin:
      line=0
      start=0
      end=0
      Type(1:30)(1:16)=' '
      do IELEM=1,NELEMTS
       subline=0
       ELEMENT=ELMLIST(IELEM)
       call SET_BASIS_IMPORT(line,subline,start,end)
       IGstart=1
       do Ishell=1,NshellIN
        IGend=IGstart+Ngauss(Ishell)-1
        do Igauss=IGstart,IGend
         Gexp(Igauss)=Gexp(Igauss)*ScaleF(Ishell)*ScaleF(Ishell)
        end do
        call SAVE_shell
        IGstart=IGend+1
       end do ! Ishell
      end do ! IELEM
      deallocate(basisimport)
!
! End of routine BLD_IMPORT_BASIS
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine SET_BASIS_IMPORT(line,subline,start,end)
!*****************************************************************************************************************
!     Date last modified: July 14, 2016                                                             Version 2.0  *
!     Author: P. L. Warburton                                                                                    *
!     Description: Import the basis set for each element in the imported Gaussian output                         *
!*****************************************************************************************************************
! Modules:
      USE type_basis_set

      implicit none
!
! Local scalars:
      logical :: LKept,stars
      integer :: line,subline,loop,start,end
      double precision :: dbtmp
      character*2 :: ttype
!
! Begin:
      Igauss=0
      stars=.false.
      NShellIN=0
      do while (.not.stars)
       line=line+1
       subline=subline+1
       if(index(basisimport(line),'****').ne.0) then
        stars=.true.
        end=line
        start=end-subline+1
       end if
       end do ! while       
       do loop=start,end
        if(loop.eq.start) cycle
        if(loop.eq.end) then
         if(index(basisimport(loop),'****').eq.0) CALL PRG_STOP('Error building imported basis set!')
         cycle
        end if
        if(index(basisimport(loop)(1:3),'   ').eq.0) then
         ttype=trim(basisimport(loop)(2:3))
         NShellIN=NShellIN+1
         read(basisimport(loop)(4:7),'(i3)') Ngauss(NShellIN)
         read(basisimport(loop)(8:16),'(e8.6)') ScaleF(NShellIN)
         write(Type(NShellIN),*) NShellIN
         Type(NShellIN)=trim(Type(NShellIN)(2:len_trim(Type(NShellIN))))//trim(ttype)
         cycle
        end if ! basisimport
! reading primitives
        Igauss=Igauss+1        
        select case(ttype)
         case('F')
          read(basisimport(loop)(7:22),*) dbtmp
          Gexp(Igauss)=dbtmp
          read(basisimport(loop)(23:40),*) dbtmp
          C1(Igauss)=dbtmp
!          write(UNIout,'(i3,1x,a3,1x,2(e18.11,1x))') Igauss,Type(NShellIN),Gexp(Igauss),C1(Igauss)
         case('D')
          read(basisimport(loop)(7:22),*) dbtmp
          Gexp(Igauss)=dbtmp
          read(basisimport(loop)(23:40),*) dbtmp
          C1(Igauss)=dbtmp
!          write(UNIout,'(i3,1x,a3,1x,2(e18.11,1x))') Igauss,Type(NShellIN),Gexp(Igauss),C1(Igauss)
         case('P')
          read(basisimport(loop)(7:22),*) dbtmp
          Gexp(Igauss)=dbtmp
          read(basisimport(loop)(23:40),*) dbtmp
          C1(Igauss)=dbtmp
!          write(UNIout,'(i3,1x,a3,1x,2(e18.11,1x))') Igauss,Type(NShellIN),Gexp(Igauss),C1(Igauss)
         case('S')
          read(basisimport(loop)(7:22),*) dbtmp
          Gexp(Igauss)=dbtmp
          read(basisimport(loop)(23:40),*) dbtmp
          C1(Igauss)=dbtmp
!          write(UNIout,'(i3,1x,a3,1x,2(e18.11,1x))') Igauss,Type(NShellIN),Gexp(Igauss),C1(Igauss)
         case('SP')
          read(basisimport(loop)(7:22),*) dbtmp
          Gexp(Igauss)=dbtmp
          read(basisimport(loop)(23:40),*) dbtmp
          C1(Igauss)=dbtmp
          read(basisimport(loop)(41:57),*) dbtmp
          C2(Igauss)=dbtmp
!          write(UNIout,'(i3,1x,a3,1x,3(e18.11,1x))') Igauss,Type(NShellIN),Gexp(Igauss),C1(Igauss),C2(Igauss)
         case default
          write(UNIout,'(a)')'Error importing shell, ',ttype(1:len_trim(ttype))
          call PRG_stop ('Shell type not available in MUNgauss')
        end select
       end do ! loop



     end subroutine SET_BASIS_IMPORT
! END CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine BLD_IMPORT_BASIS
