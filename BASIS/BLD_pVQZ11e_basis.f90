      subroutine BLD_pVQZ11e_BASIS
!*****************************************************************************************************************
!     Date last modified: May 16, 2011                                                              Version 2.0  *
!     Author: V. R. Grandy, R. A. Poirier                                                                        *
!     Description: Define a basis set.                                                                           *
!*****************************************************************************************************************
! Modules:
      USE MENU_BLD_basis

      implicit none
!
! Local scalars:
      integer :: Znum
!
! Begin:
      Type(1:30)(1:16)=' '
! 1) Loop over all the elements in the molecule
      do IELEM=1,NELEMTS
!       ELEMENT=ELMLIST(IELEM)
        Znum=CARTESIAN(1)%Atomic_number
        call SET_BASIS_pVQZ11e

        write(6,'(a,i4)')' Znum: ',Znum
!       ScaleF(1)=0.2297D0*Znum+0.0558D0
!       ScaleF(2)=0.2542D0*Znum-0.1482D0
!       ScaleF(3)=0.4727D0*Znum-0.6575D0
!       ScaleF(4)=0.4004D0*Znum-0.2153D0
!       ScaleF(5)=3.6286D0*Znum-6.7413D0
      
        IGstart=1
        do Ishell=1,NshellIN
          IGend=IGstart+Ngauss(Ishell)-1
            do Igauss=IGstart,IGend
              Gexp(Igauss)=Gexp(Igauss)*ScaleF(Ishell)*ScaleF(Ishell)
            end do
            call SAVE_shell
          IGstart=IGend+1
        end do ! Ishell
      end do ! IELEM
!
! End of routine BLD_pVQZ11e_BASIS
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine SET_BASIS_pVQZ11e
!*****************************************************************************************************************
!     Date last modified: May 16, 2011                                                              Version 2.0  *
!     Author: V. R. Grandy, R.A. Poirier                                                                         *
!     Description: Set basis set defaults (can only be called after a molecule has been defined.                 *
!*****************************************************************************************************************
! Modules:

      implicit none
!
! Begin:
      NshellIN=13
      Type(1)='1S'
      Ngauss(1)=16
      Gexp(1)=1224000.0000000
      C1(1)=0.478894D-05
      Gexp(2)=183200.0000000
      C1(2)=0.372395D-04
      Gexp(3)=41700.0000000
      C1(3)=0.195831D-03
      Gexp(4)=11810.0000000
      C1(4)=0.826698D-03
      Gexp(5)=3853.0000000
      C1(5)=0.300251D-02
      Gexp(6)=1391.0000000
      C1(6)=0.970310D-02
      Gexp(7)=542.5000000
      C1(7)=0.282337D-01
      Gexp(8)=224.9000000
      C1(8)=0.732058D-01
      Gexp(9)=97.9300000
      C1(9)=0.162897D+00
      Gexp(10)=44.3100000
      C1(10)=0.288708D+00
      Gexp(11)=20.6500000
      C1(11)=0.346829D+00
      Gexp(12)=9.7290000
      C1(12)=0.206865D+00
      Gexp(13)=4.2280000
      C1(13)=0.328009D-01
      Gexp(14)=1.9690000
      C1(14)=-0.647736D-03
      Gexp(15)=0.8890000
      C1(15)=0.145878D-02
      Gexp(16)=0.3964000
      C1(16)=-0.178346D-03

      Type(2)='2S'
      Ngauss(2)=16
      Gexp(17)=1224000.0000000
      C1(17)=-0.116958D-05
      Gexp(18)=183200.0000000
      C1(18)=-0.909110D-05
      Gexp(19)=41700.0000000
      C1(19)=-0.478499D-04
      Gexp(20)=11810.0000000
      C1(20)=-0.201962D-03
      Gexp(21)=3853.0000000
      C1(21)=-0.735837D-03
      Gexp(22)=1391.0000000
      C1(22)=-0.238746D-02
      Gexp(23)=542.5000000
      C1(23)=-0.704969D-02
      Gexp(24)=224.9000000
      C1(24)=-0.187856D-01
      Gexp(25)=97.9300000
      C1(25)=-0.446153D-01
      Gexp(26)=44.3100000
      C1(26)=-0.897741D-01
      Gexp(27)=20.6500000
      C1(27)=-0.142940D+00
      Gexp(28)=9.7290000
      C1(28)=-0.124315D+00
      Gexp(29)=4.2280000
      C1(29)=0.999648D-01
      Gexp(30)=1.9690000
      C1(30)=0.417080D+00
      Gexp(31)=0.8890000
      C1(31)=0.475123D+00
      Gexp(32)=0.3964000
      C1(32)=0.163268D+00

      Type(3)='3S'
      Ngauss(3)=16
      Gexp(33)=1224000.0000000
      C1(33)=0.175871D-06
      Gexp(34)=183200.0000000
      C1(34)=0.136594D-05
      Gexp(35)=41700.0000000
      C1(35)=0.719795D-05
      Gexp(36)=11810.0000000
      C1(36)=0.303349D-04
      Gexp(37)=3853.0000000
      C1(37)=0.110752D-03
      Gexp(38)=1391.0000000
      C1(38)=0.358596D-03
      Gexp(39)=542.5000000
      C1(39)=0.106272D-02
      Gexp(40)=224.9000000
      C1(40)=0.282687D-02
      Gexp(41)=97.9300000
      C1(41)=0.676742D-02
      Gexp(42)=44.3100000
      C1(42)=0.136480D-01
      Gexp(43)=20.6500000
      C1(43)=0.222814D-01
      Gexp(44)=9.7290000
      C1(44)=0.196011D-01
      Gexp(45)=4.2280000
      C1(45)=-0.167708D-01
      Gexp(46)=1.9690000
      C1(46)=-0.773734D-01
      Gexp(47)=0.8890000
      C1(47)=-0.113501D+00
      Gexp(48)=0.3964000
      C1(48)=-0.139130D+00

      Type(4)='4S'
      Ngauss(4)=1
      Gexp(49)=0.0699300
      C1(49)=1.0000000

      Type(5)='5S'
      Ngauss(5)=1
      Gexp(50)=0.0328900
      C1(50)=1.0000000

      Type(6)='6S'
      Ngauss(6)=1
      Gexp(51)=0.0161200
      C1(51)=1.0000000

      Type(7)='7S'
      Ngauss(7)=1
      Gexp(52)=0.0050300
      C1(52)=1.0000000

      Type(8)='1P'
      Ngauss(8)=9
      Gexp(53)=413.4000000
      C1(53)=0.908196D-03
      Gexp(54)=97.9800000
      C1(54)=0.741773D-02
      Gexp(55)=31.3700000
      C1(55)=0.357464D-01
      Gexp(56)=11.6200000
      C1(56)=0.118520D+00
      Gexp(57)=4.6710000
      C1(57)=0.261403D+00
      Gexp(58)=1.9180000
      C1(58)=0.378395D+00
      Gexp(59)=0.7775000
      C1(59)=0.334632D+00
      Gexp(60)=0.3013000
      C1(60)=0.126844D+00
      Gexp(61)=0.2275000
      C1(61)=-0.147117D-01

      Type(9)='2P'
      Ngauss(9)=9
      Gexp(62)=413.4000000
      C1(62)=-0.901741D-04
      Gexp(63)=97.9800000
      C1(63)=-0.739342D-03
      Gexp(64)=31.3700000
      C1(64)=-0.357309D-02
      Gexp(65)=11.6200000
      C1(65)=-0.120142D-01
      Gexp(66)=4.6710000
      C1(66)=-0.267178D-01
      Gexp(67)=1.9180000
      C1(67)=-0.392753D-01
      Gexp(68)=0.7775000
      C1(68)=-0.376083D-01
      Gexp(69)=0.3013000
      C1(69)=-0.433228D-01
      Gexp(70)=0.2275000
      C1(70)=0.518003D-01

      Type(10)='3P'
      Ngauss(10)=1
      Gexp(71)=0.0752700
      C1(71)=1.0000000

      Type(11)='4P'
      Ngauss(11)=1
      Gexp(72)=0.0312600
      C1(72)=1.0000000

      Type(12)='5P'
      Ngauss(12)=1
      Gexp(73)=0.0134200
      C1(73)=1.0000000

      Type(13)='6P'
      Ngauss(13)=1
      Gexp(74)=0.0077200
      C1(74)=1.0000000

      select case(Znum) ! Can USE Znum
      case(1)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(2)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(3)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(4)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(5)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(6)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(7)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(8)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(9)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(10)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(11)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(12)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(13)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(14)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(15)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(16)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(17)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(18)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(19)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(20)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(21)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(22)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(23)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(24)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(25)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(26)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(27)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(28)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(29)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(30)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(31)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(32)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(33)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(34)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(35)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(36)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case default
        write(UNIout,'(a)')'pVQZ11e basis set not available for element, ',Element(1:len_trim(Element))
        call PRG_stop ('pVQZ11e basis set not available for element')
      end select
      return
      end subroutine SET_BASIS_pVQZ11e
! END CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine BLD_pVQZ11e_BASIS
