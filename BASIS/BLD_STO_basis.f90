      subroutine BLD_STO_BASIS
!*****************************************************************************************************************
!     Date last modified: February 16, 2000                                                         Version 2.0  *
!     Author: R. A. Poirier                                                                                      *
!     Description: Read-in a general basis set.                                                                  *
!*****************************************************************************************************************
! Modules:
      USE MENU_BLD_basis

      implicit none
!
! Local scalars:
      integer :: Ngaussian
      logical :: LKept
!
! Begin:
      Type(1:30)(1:16)=' '
      if(index(BASIC_name, '2G').ne.0)then
        Ngaussian=2
      else if(index(BASIC_name, '3G').ne.0)then
        Ngaussian=3
      else if(index(BASIC_name, '4G').ne.0)then
        Ngaussian=4
      else if(index(BASIC_name, '5G').ne.0)then
        Ngaussian=5
      else if(index(BASIC_name, '6G').ne.0)then
        Ngaussian=6
      else
        write(UNIout,'(a)')'Basis set: ',BASIS_name(1:len_trim(BASIS_name)),' not available'
          call PRG_stop ('Basis set not available')
      end if
! 1) Loop over all the elements in the molecule
      do IELEM=1,NELEMTS
        ELEMENT=ELMLIST(IELEM)
        call SET_BASIS_STO

        IGstart=1
        do Ishell=1,NshellIN
          Ngauss(Ishell)=Ngaussian
          LKept=.true.
! Determine if this shell must be kept:
          if(Type(Ishell)(1:5).eq.'D-POL'.and..not.DPOLAR)LKept=.false.
!
          if(LKept)then ! Save this shell:
            select case(Ngaussian)
            case(2)
              call SET_BASIS_STO2G (Type(Ishell))
            case(3)
              call SET_BASIS_STO3G (Type(Ishell))
            case(4)
              call SET_BASIS_STO4G (Type(Ishell))
            case(5)
              call SET_BASIS_STO5G (Type(Ishell))
            case(6)
              call SET_BASIS_STO6G (Type(Ishell))
            end select
            IGend=Ngauss(Ishell)
! Now scale the exponents
            do Igauss=1,IGend
              Gexp(Igauss)=Gexp(Igauss)*ScaleF(Ishell)*ScaleF(Ishell)
            end do
            call SAVE_STO_shell
          end if ! LKept
          IGstart=1
        end do ! Ishell
      end do ! IELEM
!
! End of routine BLD_STO_BASIS
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine SAVE_STO_shell
!*****************************************************************************************************************
!     Date last modified: February 7, 2001                                                          Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Set basis set defaults (can only be called after a molecule has been defined.                 *
!*****************************************************************************************************************
! Modules:

      implicit none
!
! Local scalars:
      integer :: Igauss
      integer :: lentyp
      integer :: Nshells
! Begin:
      BasisW%Nshells=BasisW%Nshells+1
      Nshells=BasisW%Nshells
      if(Nshells.gt.MAX_shells)then
        stop 'Number of shells exceeds MAX_shells'
      end if
      lentyp=len_trim(Type(Ishell))
      BasisW%shell(Nshells)%SHLTLB=Type(Ishell)(1:lentyp)
      BasisW%shell(Nshells)%SHLATM=ZANLIST(Ielem)
      BasisW%shell(Nshells)%SHLSCL=ScaleF(Ishell)
      BasisW%shell(Nshells)%EXPBGN=Nprimitives+1
      do Igauss=IGstart,IGend
        Nprimitives=Nprimitives+1
        if(Nprimitives.gt.MAX_primitives)then
          stop 'Number of shells exceeds MAX_primitives'
        end if
        BasisW%gaussian(Nprimitives)%exp=GEXP(Igauss)
        BasisW%gaussian(Nprimitives)%contrc=C1(Igauss)
      end do ! Igauss
      BasisW%shell(Nshells)%EXPEND=Nprimitives

! First check for polarization functions _ NOTE: the order is important
      if(index(Type(Ishell),'P-POL').ne.0)then
        BasisW%shell(Nshells)%SHLTYP='P '
      else if(index(Type(Ishell),'D-POL').ne.0)then
        BasisW%shell(Nshells)%SHLTYP='D '
      else if(index(Type(Ishell),'F-POL').ne.0)then
        BasisW%shell(Nshells)%SHLTYP='F '

!     else if(index(Type(Ishell),'SP-DIFFUSE').ne.0)then ! Split the sp-shell
      else if(index(Type(Ishell),'SP').ne.0)then ! Split the sp-shell for SP or SP-DIFFUSE
        BasisW%shell(Nshells)%SHLTYP='S '
        Nshells=Nshells+1
        if(Nshells.gt.MAX_shells)then
          stop 'Number of shells exceeds MAX_shells'
        end if
        BasisW%shell(Nshells)%SHLTLB=Type(Ishell)(1:lentyp)
        BasisW%shell(Nshells)%SHLTYP='P '
        BasisW%shell(Nshells)%SHLATM=ZANLIST(IELEM)
        BasisW%shell(Nshells)%SHLSCL=ScaleF(Ishell)
        BasisW%shell(Nshells)%EXPBGN=Nprimitives+1
        do Igauss=IGstart,IGend
          Nprimitives=Nprimitives+1
        if(Nprimitives.gt.MAX_primitives)then
          stop 'Number of shells exceeds MAX_primitives'
        end if
          BasisW%gaussian(Nprimitives)%exp=GEXP(Igauss)
          BasisW%gaussian(Nprimitives)%contrc=C2(Igauss)
        end do ! Igauss
        BasisW%shell(Nshells)%EXPEND=Nprimitives

      else if(index(Type(Ishell),'S-DIFFUSE').ne.0)then
        BasisW%shell(Nshells)%SHLTYP='S '
      else if(index(Type(Ishell),'S').ne.0)then
        BasisW%shell(Nshells)%SHLTYP='S '
      else if(index(Type(Ishell),'P').ne.0)then
        BasisW%shell(Nshells)%SHLTYP='P '
      else if(index(Type(Ishell),'D').ne.0)then
        BasisW%shell(Nshells)%SHLTYP='D '
      else if(index(Type(Ishell),'F').ne.0)then
        BasisW%shell(Nshells)%SHLTYP='F '
      else
        write(UNIout,'(1X,A)')' error_READ_BASIS_SET> Invalid shell type ',Type(Ishell)
      end if
      BasisW%Nshells=Nshells
      return
      end subroutine SAVE_STO_shell
      subroutine SET_BASIS_STO
!*****************************************************************************************************************
!     Date last modified: February 7, 2001                                                          Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Set basis set defaults (can only be called after a molecule has been defined.                 *
!*****************************************************************************************************************
! Modules:

      implicit none

      Ishell=0
      select case(Element)
      case('HYDROGEN')
        NshellIN= 1
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)=  1.24000000
      case('HELIUM')
        NshellIN= 1
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)=  1.69000000
      case('LITHIUM')
        NshellIN= 3
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)=  2.69000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)=  0.80000000
        Ishell=Ishell+1
        Type(Ishell)='D-POLARIZATION'
        ScaleF(Ishell)=  0.44700000
      case('BERYLLIUM')
        NshellIN= 3
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)=  3.68000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)=  1.15000000
        Ishell=Ishell+1
        Type(Ishell)='D-POLARIZATION'
        ScaleF(Ishell)=  0.63200000
      case('BORON')
        NshellIN= 3
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)=  4.68000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)=  1.50000000
        Ishell=Ishell+1
        Type(Ishell)='D-POLARIZATION'
        ScaleF(Ishell)=  0.77500000
      case('CARBON')
        NshellIN= 3
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)=  5.67000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)=  1.72000000
        Ishell=Ishell+1
        Type(Ishell)='D-POLARIZATION'
        ScaleF(Ishell)=  0.89400000
      case('NITROGEN')
        NshellIN= 3
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)=  6.67000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)=  1.95000000
        Ishell=Ishell+1
        Type(Ishell)='D-POLARIZATION'
        ScaleF(Ishell)=  0.89400000
      case('OXYGEN')
        NshellIN= 3
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)=  7.66000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)=  2.25000000
        Ishell=Ishell+1
        Type(Ishell)='D-POLARIZATION'
        ScaleF(Ishell)=  0.89400000
      case('FLUORINE')
        NshellIN= 3
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)=  8.65000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)=  2.55000000
        Ishell=Ishell+1
        Type(Ishell)='D-POLARIZATION'
        ScaleF(Ishell)=  0.89400000
      case('NEON')
        NshellIN= 2
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)=  9.64000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)=  2.88000000
      case('SODIUM')
        NshellIN= 4
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 10.61000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)=  3.48000000
        Ishell=Ishell+1
        Type(Ishell)='3SP'
        ScaleF(Ishell)=  1.75000000
        Ishell=Ishell+1
        Type(Ishell)='D-POLARIZATION'
        ScaleF(Ishell)=  0.59161000
      case('MAGNESIUM')
        NshellIN= 4
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 11.59000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)=  3.90000000
        Ishell=Ishell+1
        Type(Ishell)='3SP'
        ScaleF(Ishell)=  1.70000000
        Ishell=Ishell+1
        Type(Ishell)='D-POLARIZATION'
        ScaleF(Ishell)=  0.59161000
      case('ALUMINUM')
        NshellIN= 4
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 12.56000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)=  4.36000000
        Ishell=Ishell+1
        Type(Ishell)='3SP'
        ScaleF(Ishell)=  1.70000000
        Ishell=Ishell+1
        Type(Ishell)='D-POLARIZATION'
        ScaleF(Ishell)=  0.59161000
      case('SILICON')
        NshellIN= 4
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 13.53000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)=  4.83000000
        Ishell=Ishell+1
        Type(Ishell)='3SP'
        ScaleF(Ishell)=  1.75000000
        Ishell=Ishell+1
        Type(Ishell)='D-POLARIZATION'
        ScaleF(Ishell)=  0.59161000
      case('PHOSPHORUS')
        NshellIN= 4
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 14.50000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)=  5.31000000
        Ishell=Ishell+1
        Type(Ishell)='3SP'
        ScaleF(Ishell)=  1.90000000
        Ishell=Ishell+1
        Type(Ishell)='D-POLARIZATION'
        ScaleF(Ishell)=  0.59161000
      case('SULFUR')
        NshellIN= 4
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 15.47000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)=  5.79000000
        Ishell=Ishell+1
        Type(Ishell)='3SP'
        ScaleF(Ishell)=  2.05000000
        Ishell=Ishell+1
        Type(Ishell)='D-POLARIZATION'
        ScaleF(Ishell)=  0.59161000
      case('CHLORINE')
        NshellIN= 4
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 16.43000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)=  6.26000000
        Ishell=Ishell+1
        Type(Ishell)='3SP'
        ScaleF(Ishell)=  2.10000000
        Ishell=Ishell+1
        Type(Ishell)='D-POLARIZATION'
        ScaleF(Ishell)=  0.59161000
      case('ARGON')
        NshellIN= 4
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 17.40000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)=  6.74000000
        Ishell=Ishell+1
        Type(Ishell)='3SP'
        ScaleF(Ishell)=  2.33000000
        Ishell=Ishell+1
        Type(Ishell)='D-POLARIZATION'
        ScaleF(Ishell)=  0.59161000
      case('POTASSIUM')
        NshellIN= 4
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 18.61000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)=  7.26000000
        Ishell=Ishell+1
        Type(Ishell)='3SP'
        ScaleF(Ishell)=  2.75000000
        Ishell=Ishell+1
        Type(Ishell)='4SP'
        ScaleF(Ishell)=  1.43000000
      case('CALCIUM')
        NshellIN= 4
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 19.58000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)=  7.74000000
        Ishell=Ishell+1
        Type(Ishell)='3SP'
        ScaleF(Ishell)=  3.01000000
        Ishell=Ishell+1
        Type(Ishell)='4SP'
        ScaleF(Ishell)=  1.36000000
      case('SCANDIUM')
        NshellIN= 5
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 20.56000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)=  8.22000000
        Ishell=Ishell+1
        Type(Ishell)='3SP*'
        ScaleF(Ishell)=  3.21000000
        Ishell=Ishell+1
        Type(Ishell)='3D*'
        ScaleF(Ishell)=  1.10000000
        Ishell=Ishell+1
        Type(Ishell)='4SP'
        ScaleF(Ishell)=  1.60000000
      case('TITANIUM')
        NshellIN= 5
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 21.54000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)=  8.70000000
        Ishell=Ishell+1
        Type(Ishell)='3SP*'
        ScaleF(Ishell)=  3.44000000
        Ishell=Ishell+1
        Type(Ishell)='3D*'
        ScaleF(Ishell)=  1.90000000
        Ishell=Ishell+1
        Type(Ishell)='4SP'
        ScaleF(Ishell)=  1.70000000
      case('VANADIUM')
        NshellIN= 5
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 22.53000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)=  9.18000000
        Ishell=Ishell+1
        Type(Ishell)='3SP*'
        ScaleF(Ishell)=  3.67000000
        Ishell=Ishell+1
        Type(Ishell)='3D*'
        ScaleF(Ishell)=  2.55000000
        Ishell=Ishell+1
        Type(Ishell)='4SP'
        ScaleF(Ishell)=  1.70000000
      case('CHROMIUM')
        NshellIN= 5
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 23.52000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)=  9.66000000
        Ishell=Ishell+1
        Type(Ishell)='3SP*'
        ScaleF(Ishell)=  3.89000000
        Ishell=Ishell+1
        Type(Ishell)='3D*'
        ScaleF(Ishell)=  3.05000000
        Ishell=Ishell+1
        Type(Ishell)='4SP'
        ScaleF(Ishell)=  1.75000000
      case('MANGANESE')
        NshellIN= 5
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 24.50000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)= 10.13000000
        Ishell=Ishell+1
        Type(Ishell)='3SP*'
        ScaleF(Ishell)=  4.11000000
        Ishell=Ishell+1
        Type(Ishell)='3D*'
        ScaleF(Ishell)=  3.45000000
        Ishell=Ishell+1
        Type(Ishell)='4SP'
        ScaleF(Ishell)=  1.65000000
      case('IRON')
        NshellIN= 5
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 25.49000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)= 10.61000000
        Ishell=Ishell+1
        Type(Ishell)='3SP*'
        ScaleF(Ishell)=  4.33000000
        Ishell=Ishell+1
        Type(Ishell)='3D*'
        ScaleF(Ishell)=  3.75000000
        Ishell=Ishell+1
        Type(Ishell)='4SP'
        ScaleF(Ishell)=  1.55000000
      case('COBALT')
        NshellIN= 5
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 26.47000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)= 11.09000000
        Ishell=Ishell+1
        Type(Ishell)='3SP*'
        ScaleF(Ishell)=  4.55000000
        Ishell=Ishell+1
        Type(Ishell)='3D*'
        ScaleF(Ishell)=  4.10000000
        Ishell=Ishell+1
        Type(Ishell)='4SP'
        ScaleF(Ishell)=  1.55000000
      case('NICKEL')
        NshellIN= 5
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 27.46000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)= 11.56000000
        Ishell=Ishell+1
        Type(Ishell)='3SP*'
        ScaleF(Ishell)=  4.76000000
        Ishell=Ishell+1
        Type(Ishell)='3D*'
        ScaleF(Ishell)=  4.35000000
        Ishell=Ishell+1
        Type(Ishell)='4SP'
        ScaleF(Ishell)=  1.60000000
      case('COPPER')
        NshellIN= 5
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 28.44000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)= 12.04000000
        Ishell=Ishell+1
        Type(Ishell)='3SP*'
        ScaleF(Ishell)=  4.98000000
        Ishell=Ishell+1
        Type(Ishell)='3D*'
        ScaleF(Ishell)=  4.60000000
        Ishell=Ishell+1
        Type(Ishell)='4SP'
        ScaleF(Ishell)=  1.60000000
      case('ZINC')
        NshellIN= 5
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 29.43000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)= 12.52000000
        Ishell=Ishell+1
        Type(Ishell)='3SP*'
        ScaleF(Ishell)=  5.19000000
        Ishell=Ishell+1
        Type(Ishell)='3D*'
        ScaleF(Ishell)=  4.90000000
        Ishell=Ishell+1
        Type(Ishell)='4SP'
        ScaleF(Ishell)=  1.90000000
      case('GALLIUM')
        NshellIN= 5
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 30.42000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)= 12.99000000
        Ishell=Ishell+1
        Type(Ishell)='3SP*'
        ScaleF(Ishell)=  5.26000000
        Ishell=Ishell+1
        Type(Ishell)='3D*'
        ScaleF(Ishell)=  5.26000000
        Ishell=Ishell+1
        Type(Ishell)='4SP'
        ScaleF(Ishell)=  1.80000000
      case('GERMANIUM')
        NshellIN= 5
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 31.40000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)= 13.47000000
        Ishell=Ishell+1
        Type(Ishell)='3SP*'
        ScaleF(Ishell)=  5.58000000
        Ishell=Ishell+1
        Type(Ishell)='3D*'
        ScaleF(Ishell)=  5.58000000
        Ishell=Ishell+1
        Type(Ishell)='4SP'
        ScaleF(Ishell)=  2.00000000
      case('ARSENIC')
        NshellIN= 5
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 32.39000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)= 13.94000000
        Ishell=Ishell+1
        Type(Ishell)='3SP*'
        ScaleF(Ishell)=  5.90000000
        Ishell=Ishell+1
        Type(Ishell)='3D*'
        ScaleF(Ishell)=  5.90000000
        Ishell=Ishell+1
        Type(Ishell)='4SP'
        ScaleF(Ishell)=  2.12000000
      case('SELENIUM')
        NshellIN= 5
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 33.37000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)= 14.40000000
        Ishell=Ishell+1
        Type(Ishell)='3SP*'
        ScaleF(Ishell)=  6.22000000
        Ishell=Ishell+1
        Type(Ishell)='3D*'
        ScaleF(Ishell)=  6.22000000
        Ishell=Ishell+1
        Type(Ishell)='4SP'
        ScaleF(Ishell)=  2.22000000
      case('BROMINE')
        NshellIN= 5
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 34.36000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)= 14.87000000
        Ishell=Ishell+1
        Type(Ishell)='3SP*'
        ScaleF(Ishell)=  6.54000000
        Ishell=Ishell+1
        Type(Ishell)='3D*'
        ScaleF(Ishell)=  6.54000000
        Ishell=Ishell+1
        Type(Ishell)='4SP'
        ScaleF(Ishell)=  2.38000000
      case('KRYPTON')
        NshellIN= 5
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 35.34000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)= 15.34000000
        Ishell=Ishell+1
        Type(Ishell)='3SP*'
        ScaleF(Ishell)=  6.86000000
        Ishell=Ishell+1
        Type(Ishell)='3D*'
        ScaleF(Ishell)=  6.86000000
        Ishell=Ishell+1
        Type(Ishell)='4SP'
        ScaleF(Ishell)=  2.54000000
      case('RUBIDIUM')
        NshellIN= 6
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 36.32000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)= 15.81000000
        Ishell=Ishell+1
        Type(Ishell)='3SP*'
        ScaleF(Ishell)=  7.18000000
        Ishell=Ishell+1
        Type(Ishell)='3D*'
        ScaleF(Ishell)=  7.18000000
        Ishell=Ishell+1
        Type(Ishell)='4SP'
        ScaleF(Ishell)=  3.02000000
        Ishell=Ishell+1
        Type(Ishell)='5SP'
        ScaleF(Ishell)=  1.90000000
      case('STRONTIUM')
        NshellIN= 6
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 37.31000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)= 16.28000000
        Ishell=Ishell+1
        Type(Ishell)='3SP*'
        ScaleF(Ishell)=  7.49000000
        Ishell=Ishell+1
        Type(Ishell)='3D*'
        ScaleF(Ishell)=  7.49000000
        Ishell=Ishell+1
        Type(Ishell)='4SP'
        ScaleF(Ishell)=  3.16000000
        Ishell=Ishell+1
        Type(Ishell)='5SP'
        ScaleF(Ishell)=  1.80000000
      case('YTTRIUM')
        NshellIN= 7
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 38.29000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)= 16.72000000
        Ishell=Ishell+1
        Type(Ishell)='3SP*'
        ScaleF(Ishell)=  7.97000000
        Ishell=Ishell+1
        Type(Ishell)='3D*'
        ScaleF(Ishell)=  7.97000000
        Ishell=Ishell+1
        Type(Ishell)='4SP*'
        ScaleF(Ishell)=  3.29000000
        Ishell=Ishell+1
        Type(Ishell)='4D*'
        ScaleF(Ishell)=  1.40000000
        Ishell=Ishell+1
        Type(Ishell)='5SP'
        ScaleF(Ishell)=  1.80000000
      case('ZIRCONIUM')
        NshellIN= 7
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 39.27000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)= 17.19000000
        Ishell=Ishell+1
        Type(Ishell)='3SP*'
        ScaleF(Ishell)=  8.21000000
        Ishell=Ishell+1
        Type(Ishell)='3D*'
        ScaleF(Ishell)=  8.21000000
        Ishell=Ishell+1
        Type(Ishell)='4SP*'
        ScaleF(Ishell)=  3.48000000
        Ishell=Ishell+1
        Type(Ishell)='4D*'
        ScaleF(Ishell)=  1.95000000
        Ishell=Ishell+1
        Type(Ishell)='5SP'
        ScaleF(Ishell)=  1.90000000
      case('NIOBIUM')
        NshellIN= 7
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 40.26000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)= 17.66000000
        Ishell=Ishell+1
        Type(Ishell)='3SP*'
        ScaleF(Ishell)=  8.51000000
        Ishell=Ishell+1
        Type(Ishell)='3D*'
        ScaleF(Ishell)=  8.51000000
        Ishell=Ishell+1
        Type(Ishell)='4SP*'
        ScaleF(Ishell)=  3.67000000
        Ishell=Ishell+1
        Type(Ishell)='4D*'
        ScaleF(Ishell)=  2.40000000
        Ishell=Ishell+1
        Type(Ishell)='5SP'
        ScaleF(Ishell)=  1.90000000
      case('MOLYBDENUM')
        NshellIN= 7
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 41.24000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)= 18.12000000
        Ishell=Ishell+1
        Type(Ishell)='3SP*'
        ScaleF(Ishell)=  8.82000000
        Ishell=Ishell+1
        Type(Ishell)='3D*'
        ScaleF(Ishell)=  8.82000000
        Ishell=Ishell+1
        Type(Ishell)='4SP*'
        ScaleF(Ishell)=  3.87000000
        Ishell=Ishell+1
        Type(Ishell)='4D*'
        ScaleF(Ishell)=  2.70000000
        Ishell=Ishell+1
        Type(Ishell)='5SP'
        ScaleF(Ishell)=  1.95000000
      case('TECHNETIUM')
        NshellIN= 7
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 42.22000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)= 18.59000000
        Ishell=Ishell+1
        Type(Ishell)='3SP*'
        ScaleF(Ishell)=  9.14000000
        Ishell=Ishell+1
        Type(Ishell)='3D*'
        ScaleF(Ishell)=  9.14000000
        Ishell=Ishell+1
        Type(Ishell)='4SP*'
        ScaleF(Ishell)=  4.05000000
        Ishell=Ishell+1
        Type(Ishell)='4D*'
        ScaleF(Ishell)=  3.00000000
        Ishell=Ishell+1
        Type(Ishell)='5SP'
        ScaleF(Ishell)=  1.85000000
      case('RUTHENIUM')
        NshellIN= 7
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 43.21000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)= 19.05000000
        Ishell=Ishell+1
        Type(Ishell)='3SP*'
        ScaleF(Ishell)=  9.45000000
        Ishell=Ishell+1
        Type(Ishell)='3D*'
        ScaleF(Ishell)=  9.45000000
        Ishell=Ishell+1
        Type(Ishell)='4SP*'
        ScaleF(Ishell)=  4.24000000
        Ishell=Ishell+1
        Type(Ishell)='4D*'
        ScaleF(Ishell)=  3.20000000
        Ishell=Ishell+1
        Type(Ishell)='5SP'
        ScaleF(Ishell)=  1.75000000
      case('RHODIUM')
        NshellIN= 7
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 44.19000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)= 19.51000000
        Ishell=Ishell+1
        Type(Ishell)='3SP*'
        ScaleF(Ishell)=  9.77000000
        Ishell=Ishell+1
        Type(Ishell)='3D*'
        ScaleF(Ishell)=  9.77000000
        Ishell=Ishell+1
        Type(Ishell)='4SP*'
        ScaleF(Ishell)=  4.41000000
        Ishell=Ishell+1
        Type(Ishell)='4D*'
        ScaleF(Ishell)=  3.45000000
        Ishell=Ishell+1
        Type(Ishell)='5SP'
        ScaleF(Ishell)=  1.75000000
      case('PALLADIUM')
        NshellIN= 7
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 45.17000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)= 19.97000000
        Ishell=Ishell+1
        Type(Ishell)='3SP*'
        ScaleF(Ishell)= 10.09000000
        Ishell=Ishell+1
        Type(Ishell)='3D*'
        ScaleF(Ishell)= 10.09000000
        Ishell=Ishell+1
        Type(Ishell)='4SP*'
        ScaleF(Ishell)=  4.59000000
        Ishell=Ishell+1
        Type(Ishell)='4D*'
        ScaleF(Ishell)=  3.60000000
        Ishell=Ishell+1
        Type(Ishell)='5SP'
        ScaleF(Ishell)=  1.80000000
      case('SILVER')
        NshellIN= 7
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 46.15000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)= 20.43000000
        Ishell=Ishell+1
        Type(Ishell)='3SP*'
        ScaleF(Ishell)= 10.41000000
        Ishell=Ishell+1
        Type(Ishell)='3D*'
        ScaleF(Ishell)= 10.41000000
        Ishell=Ishell+1
        Type(Ishell)='4SP*'
        ScaleF(Ishell)=  4.76000000
        Ishell=Ishell+1
        Type(Ishell)='4D*'
        ScaleF(Ishell)=  3.75000000
        Ishell=Ishell+1
        Type(Ishell)='5SP'
        ScaleF(Ishell)=  1.80000000
      case('CADMIUM')
        NshellIN= 7
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 47.14000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)= 20.88000000
        Ishell=Ishell+1
        Type(Ishell)='3SP*'
        ScaleF(Ishell)= 10.74000000
        Ishell=Ishell+1
        Type(Ishell)='3D*'
        ScaleF(Ishell)= 10.74000000
        Ishell=Ishell+1
        Type(Ishell)='4SP*'
        ScaleF(Ishell)=  4.93000000
        Ishell=Ishell+1
        Type(Ishell)='4D*'
        ScaleF(Ishell)=  3.95000000
        Ishell=Ishell+1
        Type(Ishell)='5SP'
        ScaleF(Ishell)=  2.10000000
      case('INDIUM')
        NshellIN= 7
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 48.12000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)= 21.33000000
        Ishell=Ishell+1
        Type(Ishell)='3SP*'
        ScaleF(Ishell)= 11.08000000
        Ishell=Ishell+1
        Type(Ishell)='3D*'
        ScaleF(Ishell)= 11.08000000
        Ishell=Ishell+1
        Type(Ishell)='4SP*'
        ScaleF(Ishell)=  4.65000000
        Ishell=Ishell+1
        Type(Ishell)='4D*'
        ScaleF(Ishell)=  4.65000000
        Ishell=Ishell+1
        Type(Ishell)='5SP'
        ScaleF(Ishell)=  2.05000000
      case('TIN')
        NshellIN= 7
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 49.10000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)= 21.79000000
        Ishell=Ishell+1
        Type(Ishell)='3SP*'
        ScaleF(Ishell)= 11.39000000
        Ishell=Ishell+1
        Type(Ishell)='3D*'
        ScaleF(Ishell)= 11.39000000
        Ishell=Ishell+1
        Type(Ishell)='4SP*'
        ScaleF(Ishell)=  4.89000000
        Ishell=Ishell+1
        Type(Ishell)='4D*'
        ScaleF(Ishell)=  4.89000000
        Ishell=Ishell+1
        Type(Ishell)='5SP'
        ScaleF(Ishell)=  2.15000000
      case('ANTIMONY')
        NshellIN= 7
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 50.08000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)= 22.25000000
        Ishell=Ishell+1
        Type(Ishell)='3SP*'
        ScaleF(Ishell)= 11.71000000
        Ishell=Ishell+1
        Type(Ishell)='3D*'
        ScaleF(Ishell)= 11.71000000
        Ishell=Ishell+1
        Type(Ishell)='4SP*'
        ScaleF(Ishell)=  5.12000000
        Ishell=Ishell+1
        Type(Ishell)='4D*'
        ScaleF(Ishell)=  5.12000000
        Ishell=Ishell+1
        Type(Ishell)='5SP'
        ScaleF(Ishell)=  2.20000000
      case('TELLURIUM')
        NshellIN= 7
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 51.07000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)= 22.71000000
        Ishell=Ishell+1
        Type(Ishell)='3SP*'
        ScaleF(Ishell)= 12.03000000
        Ishell=Ishell+1
        Type(Ishell)='3D*'
        ScaleF(Ishell)= 12.03000000
        Ishell=Ishell+1
        Type(Ishell)='4SP*'
        ScaleF(Ishell)=  5.36000000
        Ishell=Ishell+1
        Type(Ishell)='4D*'
        ScaleF(Ishell)=  5.36000000
        Ishell=Ishell+1
        Type(Ishell)='5SP'
        ScaleF(Ishell)=  2.28000000
      case('IODINE')
        NshellIN= 7
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 52.05000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)= 23.17000000
        Ishell=Ishell+1
        Type(Ishell)='3SP*'
        ScaleF(Ishell)= 12.35000000
        Ishell=Ishell+1
        Type(Ishell)='3D*'
        ScaleF(Ishell)= 12.35000000
        Ishell=Ishell+1
        Type(Ishell)='4SP*'
        ScaleF(Ishell)=  5.59000000
        Ishell=Ishell+1
        Type(Ishell)='4D*'
        ScaleF(Ishell)=  5.59000000
        Ishell=Ishell+1
        Type(Ishell)='5SP'
        ScaleF(Ishell)=  2.42000000
      case('XENON')
        NshellIN= 7
        Ishell=Ishell+1
        Type(Ishell)='1S'
        ScaleF(Ishell)= 53.03000000
        Ishell=Ishell+1
        Type(Ishell)='2SP'
        ScaleF(Ishell)= 23.63000000
        Ishell=Ishell+1
        Type(Ishell)='3SP*'
        ScaleF(Ishell)= 12.66000000
        Ishell=Ishell+1
        Type(Ishell)='3D*'
        ScaleF(Ishell)= 12.66000000
        Ishell=Ishell+1
        Type(Ishell)='4SP*'
        ScaleF(Ishell)=  5.82000000
        Ishell=Ishell+1
        Type(Ishell)='4D*'
        ScaleF(Ishell)=  5.82000000
        Ishell=Ishell+1
        Type(Ishell)='5SP'
        ScaleF(Ishell)=  2.57000000
      case default
        write(UNIout,'(a)')'STO-NG basis set not available for element, ',Element(1:len_trim(Element))
        call PRG_stop ('STO-NG basis set not available for element')
      end select
      end subroutine SET_BASIS_STO
      subroutine SET_BASIS_STO2G (Stype)
!*****************************************************************************************************************
!     Date last modified: February 7, 2001                                                          Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Set basis set defaults (can only be called after a molecule has been defined.                 *
!*****************************************************************************************************************
! Modules:

      implicit none

! Input scalar:
      character(*) :: Stype
!
! Begin:
      Ngauss(Ishell)=2
      select case(Stype)
      case('1S')
        Gexp(1)=  8.518186635000D-01
        C1(1)=  4.301284983000D-01
        C2(1)=  0.000000000000D+00
        Gexp(2)=  1.516232927000D-01
        C1(2)=  6.789135305000D-01
        C2(2)=  0.000000000000D+00
      case('2SP')
        Gexp(1)=  3.842442531000D-01
        C1(1)=  4.947176920000D-02
        C2(1)=  5.115407076000D-01
        Gexp(2)=  9.745448900000D-02
        C1(2)=  9.637824081000D-01
        C2(2)=  6.128198961000D-01
      case('3SP*')
        Gexp(1)=  2.236020000000D-01
        C1(1)= -3.121720000000D-01
        C2(1)=  2.940030000000D-01
        Gexp(2)=  6.915730000000D-02
        C1(2)=  1.225400000000D+00
        C2(2)=  7.800420000000D-01
      case('3D*')
        Gexp(1)=  2.236020000000D-01
        C1(1)=  6.006040000000D-01
        C2(1)=  0.000000000000D+00
        Gexp(2)=  6.915730000000D-02
        C1(2)=  5.292040000000D-01
        C2(2)=  0.000000000000D+00
      case('4SP*')
        Gexp(1)=  1.260110000000D-01
        C1(1)= -6.561970000000D-01
        C2(1)=  1.270080000000D-01
        Gexp(2)=  5.009010000000D-02
        C1(2)=  1.503540000000D+00
        C2(2)=  8.985330000000D-01
      case('4D*')
        Gexp(1)=  1.260110000000D-01
        C1(1)=  5.432750000000D-01
        C2(1)=  0.000000000000D+00
        Gexp(2)=  5.009010000000D-02
        C1(2)=  5.420870000000D-01
        C2(2)=  0.000000000000D+00
      case('3SP')
        Gexp(1)=  1.939790354000D-01
        C1(1)= -2.983986044000D-01
        C2(1)=  3.480471912000D-01
        Gexp(2)=  6.655227113000D-02
        C1(2)=  1.227982887000D+00
        C2(2)=  7.222523221000D-01
      case('3D')
        Gexp(1)=  2.777427345000D-01
        C1(1)=  4.666137923000D-01
        C2(1)=  0.000000000000D+00
        Gexp(2)=  8.336507714000D-02
        C1(2)=  6.644706516000D-01
        C2(2)=  0.000000000000D+00
      case('4SP')
        Gexp(1)=  1.242349951000D-01
        C1(1)= -6.615603018000D-01
        C2(1)=  1.309890515000D-01
        Gexp(2)=  4.999746691000D-02
        C1(2)=  1.510754134000D+00
        C2(2)=  8.946431268000D-01
      case('4S')
        Gexp(1)=  2.441785453000D-01
        C1(1)= -3.046656896000D-01
        C2(1)=  0.000000000000D+00
        Gexp(2)=  4.051097664000D-02
        C1(2)=  1.146877294000D+00
        C2(2)=  0.000000000000D+00
      case('4P')
        Gexp(1)=  6.190052680000D-02
        C1(1)=  8.743116767000D-01
        C2(1)=  0.000000000000D+00
        Gexp(2)=  2.648418407000D-02
        C1(2)=  1.513640107000D-01
        C2(2)=  0.000000000000D+00
      case('5SP')
        Gexp(1)=  8.124320000000D-02
        C1(1)= -1.093690000000D+00
        C2(1)= -1.047400000000D-01
        Gexp(2)=  3.930360000000D-02
        C1(2)=  1.881370000000D+00
        C2(2)=  1.087630000000D+00
      case('D-POLARIZATION')
        Ngauss(Ishell)=1
        Gexp(1)=    1.000000000000
        C1(1)=  1.000000000000
      case default
        write(UNIout,'(a)')'STO-2G basis set not available for shell, ',Stype(1:len_trim(Stype))
        call PRG_stop ('STO-2G basis set not available for shell')
      end select
      return
      end subroutine SET_BASIS_STO2G
      subroutine SET_BASIS_STO3G (Stype)
!*****************************************************************************************************************
!     Date last modified: February 7, 2001                                                          Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Set basis set defaults (can only be called after a molecule has been defined.                 *
!*****************************************************************************************************************
! Modules:

      implicit none

! Input scalar:
      character(*) :: Stype
!
! Begin:
      select case(Stype)
      case('1S')
        Gexp(1)=    2.227660584000
        C1(1)=  0.154328967300
        C2(1)=  0.000000000000
        Gexp(2)=    0.405771156200
        C1(2)=  0.535328142300
        C2(2)=  0.000000000000
        Gexp(3)=    0.109817510400
        C1(3)=  0.444634542200
        C2(3)=  0.000000000000
      case('2SP')
        Gexp(1)=    0.994202729600
        C1(1)= -0.099967229190
        C2(1)=  0.155916275000
        Gexp(2)=    0.231031333300
        C1(2)=  0.399512826100
        C2(2)=  0.607683718600
        Gexp(3)=    0.075138560000
        C1(3)=  0.700115468900
        C2(3)=  0.391957393100
      case('3SP*')
        Gexp(1)=    0.455950000000
        C1(1)= -0.227764000000
        C2(1)=  0.004951510000
        Gexp(2)=    0.139079000000
        C1(2)=  0.217544000000
        C2(2)=  0.577766000000
        Gexp(3)=    0.053661200000
        C1(3)=  0.916677000000
        C2(3)=  0.484646000000
      case('3D*')
        Gexp(1)=    0.455950000000
        C1(1)=  0.219768000000
        C2(1)=  0.000000000000
        Gexp(2)=    0.139079000000
        C1(2)=  0.655547000000
        C2(2)=  0.000000000000
        Gexp(3)=    0.053661200000
        C1(3)=  0.286573000000
        C2(3)=  0.000000000000
      case('4SP*')
        Gexp(1)=    0.233486000000
        C1(1)= -0.330610000000
        C2(1)= -0.128393000000
        Gexp(2)=    0.090918200000
        C1(2)=  0.057611000000
        C2(2)=  0.585205000000
        Gexp(3)=    0.040022400000
        C1(3)=  1.115580000000
        C2(3)=  0.543944000000
      case('4D*')
        Gexp(1)=    0.233486000000
        C1(1)=  0.125066000000
        C2(1)=  0.000000000000
        Gexp(2)=    0.090918200000
        C1(2)=  0.668679000000
        C2(2)=  0.000000000000
        Gexp(3)=    0.040022400000
        C1(3)=  0.305247000000
        C2(3)=  0.000000000000
      case('3SP')
        Gexp(1)=    0.482854080600
        C1(1)= -0.219620369000
        C2(1)=  0.010587604290
        Gexp(2)=    0.134715062900
        C1(2)=  0.225595433600
        C2(2)=  0.595167005300
        Gexp(3)=    0.052726562580
        C1(3)=  0.900398426000
        C2(3)=  0.462001012000
      case('3D')
        Gexp(1)=    0.522911222500
        C1(1)=  0.168659606000
        C2(1)=  0.000000000000
        Gexp(2)=    0.163959587600
        C1(2)=  0.584798481700
        C2(2)=  0.000000000000
        Gexp(3)=    0.063866300210
        C1(3)=  0.405677952300
        C2(3)=  0.000000000000
      case('D-POLARIZATION')
        Ngauss(Ishell)=1
        Gexp(1)=    1.000000000000
        C1(1)=  1.000000000000
      case('4SP')
        Gexp(1)=    0.246458140000
        C1(1)= -0.308844121500
        C2(1)= -0.121546860000
        Gexp(2)=    0.090958553740
        C1(2)=  0.019606411660
        C2(2)=  0.571522760400
        Gexp(3)=    0.040168256360
        C1(3)=  1.131034442000
        C2(3)=  0.549894947100
      case('4S')
        Gexp(1)=    0.226793875300
        C1(1)= -0.334904832300
        C2(1)=  0.000000000000
        Gexp(2)=    0.044481780190
        C1(2)=  1.056744667000
        C2(2)=  0.000000000000
        Gexp(3)=    0.021952946640
        C1(3)=  0.125666168000
        C2(3)=  0.000000000000
      case('4P')
        Gexp(1)=    0.485969222000
        C1(1)= -0.061478234110
        C2(1)=  0.000000000000
        Gexp(2)=    0.074302169180
        C1(2)=  0.660417223400
        C2(2)=  0.000000000000
        Gexp(3)=    0.036533409230
        C1(3)=  0.393263949500
        C2(3)=  0.000000000000
      case('5SP')
        Gexp(1)=    0.134901000000
        C1(1)= -0.384264000000
        C2(1)= -0.348169000000
        Gexp(2)=    0.072636100000
        C1(2)= -0.197257000000
        C2(2)=  0.629032000000
        Gexp(3)=    0.032084600000
        C1(3)=  1.375500000000
        C2(3)=  0.666283000000
      case default
        write(UNIout,'(a)')'STO-3G basis set not available for shell, ',Stype(1:len_trim(Stype))
        call PRG_stop ('STO-3G basis set not available for shell')
      end select
      return
      end subroutine SET_BASIS_STO3G
      subroutine SET_BASIS_STO4G (Stype)
!*****************************************************************************************************************
!     Date last modified: February 7, 2001                                                          Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Set basis set defaults (can only be called after a molecule has been defined.                 *
!*****************************************************************************************************************
! Modules:

      implicit none

! Input scalar:
      character(*) :: Stype
!
! Begin:
      select case(Stype)
      case('1S')
        Gexp(1)=    5.216844534000
        C1(1)=  0.056752420800
        C2(1)=  0.000000000000
        Gexp(2)=    0.954618276000
        C1(2)=  0.260141355000
        C2(2)=  0.000000000000
        Gexp(3)=    0.265203410200
        C1(3)=  0.532846114300
        C2(3)=  0.000000000000
        Gexp(4)=    0.088018627740
        C1(4)=  0.291625440500
        C2(4)=  0.000000000000
      case('2SP')
        Gexp(1)=    2.323503675000
        C1(1)= -0.062207145650
        C2(1)=  0.043684348840
        Gexp(2)=    0.502988690600
        C1(2)=  0.000029768046
        C2(2)=  0.286379398400
        Gexp(3)=    0.163540671900
        C1(3)=  0.558854922100
        C2(3)=  0.583575314100
        Gexp(4)=    0.062810442130
        C1(4)=  0.497767321800
        C2(4)=  0.246313437800
      case('3SP*')
        Gexp(1)=    0.864219000000
        C1(1)= -0.088888600000
        C2(1)= -0.258036000000
        Gexp(2)=    0.258758000000
        C1(2)= -0.202619000000
        C2(2)=  0.159141000000
        Gexp(3)=    0.102634000000
        C1(3)=  0.545089000000
        C2(3)=  0.628412000000
        Gexp(4)=    0.045628000000
        C1(4)=  0.651702000000
        C2(4)=  0.300984000000
      case('3D*')
        Gexp(1)=    0.864219000000
        C1(1)=  0.069207700000
        C2(1)=  0.000000000000
        Gexp(2)=    0.258758000000
        C1(2)=  0.372816000000
        C2(2)=  0.000000000000
        Gexp(3)=    0.102634000000
        C1(3)=  0.566409000000
        C2(3)=  0.000000000000
        Gexp(4)=    0.045628000000
        C1(4)=  0.158595000000
        C2(4)=  0.000000000000
      case('4SP*')
        Gexp(1)=    0.450425000000
        C1(1)= -0.058468800000
        C2(1)= -0.674634000000
        Gexp(2)=    0.160793000000
        C1(2)= -0.439804000000
        C2(2)=  0.020779300000
        Gexp(3)=    0.071634800000
        C1(3)=  0.531922000000
        C2(3)=  0.688454000000
        Gexp(4)=    0.035114800000
        C1(4)=  0.810747000000
        C2(4)=  0.351482000000
      case('4D*')
        Gexp(1)=    0.450425000000
        C1(1)=  0.003869940000
        C2(1)=  0.000000000000
        Gexp(2)=    0.160793000000
        C1(2)=  0.297612000000
        C2(2)=  0.000000000000
        Gexp(3)=    0.071634800000
        C1(3)=  0.620373000000
        C2(3)=  0.000000000000
        Gexp(4)=    0.035114800000
        C1(4)=  0.173553000000
        C2(4)=  0.000000000000
      case('3SP')
        Gexp(1)=    0.921707243900
        C1(1)= -0.085290196440
        C2(1)= -0.250494518100
        Gexp(2)=    0.253473012300
        C1(2)= -0.213207403400
        C2(2)=  0.168660446100
        Gexp(3)=    0.099764764720
        C1(3)=  0.592084392800
        C2(3)=  0.640955315100
        Gexp(4)=    0.044399908330
        C1(4)=  0.611558474600
        C2(4)=  0.277950895700
      case('3D')
        Gexp(1)=    0.918584671500
        C1(1)=  0.057990577050
        C2(1)=  0.000000000000
        Gexp(2)=    0.292046110900
        C1(2)=  0.304558134900
        C2(2)=  0.000000000000
        Gexp(3)=    0.118756889000
        C1(3)=  0.560135803800
        C2(3)=  0.000000000000
        Gexp(4)=    0.052867558960
        C1(4)=  0.243242331300
        C2(4)=  0.000000000000
      case('4SP')
        Gexp(1)=    0.438952779100
        C1(1)= -0.058935598130
        C2(1)= -0.718052265700
        Gexp(2)=    0.162444995200
        C1(2)= -0.428923026100
        C2(2)=  0.037268634250
        Gexp(3)=    0.069839763740
        C1(3)=  0.547299846800
        C2(3)=  0.696584224900
        Gexp(4)=    0.034692046100
        C1(4)=  0.785369203000
        C2(4)=  0.331023685000
      case('4S')
        Gexp(1)=    0.324221283300
        C1(1)= -0.112068282200
        C2(1)=  0.000000000000
        Gexp(2)=    0.166321717700
        C1(2)= -0.284542686300
        C2(2)=  0.000000000000
        Gexp(3)=    0.050810974510
        C1(3)=  0.890987378800
        C2(3)=  0.000000000000
        Gexp(4)=    0.028290666000
        C1(4)=  0.351781120500
        C2(4)=  0.000000000000
      case('4P')
        Gexp(1)=    1.492607880000
        C1(1)= -0.006035216774
        C2(1)=  0.000000000000
        Gexp(2)=    0.432761927200
        C1(2)= -0.060133108740
        C2(2)=  0.000000000000
        Gexp(3)=    0.075531560640
        C1(3)=  0.645151820000
        C2(3)=  0.000000000000
        Gexp(4)=    0.037062721830
        C1(4)=  0.411792382000
        C2(4)=  0.000000000000
      case('5SP')
        Gexp(1)=    0.257771000000
        C1(1)=  0.040451100000
        C2(1)= -0.085861500000
        Gexp(2)=    0.118964000000
        C1(2)= -0.657669000000
        C2(2)= -0.109015000000
        Gexp(3)=    0.052707800000
        C1(3)=  0.379252000000
        C2(3)=  0.723404000000
        Gexp(4)=    0.028703600000
        C1(4)=  1.038590000000
        C2(4)=  0.411743000000
      case('D-POLARIZATION')
        Ngauss(Ishell)=1
        Gexp(1)=    1.000000000000
        C1(1)=  1.000000000000
      case default
        write(UNIout,'(a)')'STO-4G basis set not available for shell, ',Stype(1:len_trim(Stype))
        call PRG_stop ('STO-4G basis set not available for shell')
      end select
      return
      end subroutine SET_BASIS_STO4G
      subroutine SET_BASIS_STO5G (Stype)
!*****************************************************************************************************************
!     Date last modified: February 7, 2001                                                          Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Set basis set defaults (can only be called after a molecule has been defined.                 *
!*****************************************************************************************************************
! Modules:

      implicit none

! Input scalar:
      character(*) :: Stype
!
! Begin:
      select case(Stype)
      case('1S')
        Gexp(1)=  1.130563696000D+01
        C1(1)=  2.214055312000D-02
        C2(1)=  0.000000000000D+00
        Gexp(2)=  2.071728178000D+00
        C1(2)=  1.135411520000D-01
        C2(2)=  0.000000000000D+00
        Gexp(3)=  5.786484833000D-01
        C1(3)=  3.318161484000D-01
        C2(3)=  0.000000000000D+00
        Gexp(4)=  1.975724573000D-01
        C1(4)=  4.825700713000D-01
        C2(4)=  0.000000000000D+00
        Gexp(5)=  7.445271746000D-02
        C1(5)=  1.935721966000D-01
        C2(5)=  0.000000000000D+00
      case('2SP')
        Gexp(1)=  5.036294248000D+00
        C1(1)= -2.940855088000D-02
        C2(1)=  1.255609128000D-02
        Gexp(2)=  1.032503477000D+00
        C1(2)= -6.532746883000D-02
        C2(2)=  1.075576962000D-01
        Gexp(3)=  3.290598322000D-01
        C1(3)=  1.289973181000D-01
        C2(3)=  3.735975367000D-01
        Gexp(4)=  1.279200125000D-01
        C1(4)=  6.122899938000D-01
        C2(4)=  5.102395637000D-01
        Gexp(5)=  5.449486448000D-02
        C1(5)=  3.461205655000D-01
        C2(5)=  1.568281801000D-01
      case('3SP*')
        Gexp(1)=  1.560010000000D+00
        C1(1)= -2.996080000000D-02
        C2(1)= -1.665120000000D-01
        Gexp(2)=  4.589580000000D-01
        C1(2)= -1.549050000000D-01
        C2(2)= -9.090220000000D-01
        Gexp(3)=  1.818190000000D-01
        C1(3)= -5.893270000000D-02
        C2(3)=  2.985630000000D-01
        Gexp(4)=  8.295170000000D-02
        C1(4)=  7.044190000000D-01
        C2(4)=  5.832010000000D-01
        Gexp(5)=  4.009490000000D-02
        C1(5)=  4.445300000000D-01
        C2(5)=  1.867090000000D-01
      case('3D*')
        Gexp(1)=  1.560010000000D+00
        C1(1)=  2.129970000000D-02
        C2(1)=  0.000000000000D+00
        Gexp(2)=  4.589580000000D-01
        C1(2)=  1.576720000000D-01
        C2(2)=  0.000000000000D+00
        Gexp(3)=  1.818190000000D-01
        C1(3)=  4.485170000000D-01
        C2(3)=  0.000000000000D+00
        Gexp(4)=  8.295170000000D-02
        C1(4)=  4.495370000000D-01
        C2(4)=  0.000000000000D+00
        Gexp(5)=  4.009490000000D-02
        C1(5)=  9.122490000000D-02
        C2(5)=  0.000000000000D+00
      case('4SP*')
        Gexp(1)=  8.147330000000D-01
        C1(1)= -1.625230000000D-03
        C2(1)= -2.252720000000D-01
        Gexp(2)=  2.730430000000D-01
        C1(2)= -2.021420000000D-01
        C2(2)= -8.330810000000D-01
        Gexp(3)=  1.196700000000D-01
        C1(3)= -3.157020000000D-01
        C2(3)=  2.024710000000D-01
        Gexp(4)=  5.981040000000D-02
        C1(4)=  8.082100000000D-01
        C2(4)=  6.764310000000D-01
        Gexp(5)=  3.146520000000D-02
        C1(5)=  5.572170000000D-01
        C2(5)=  2.185970000000D-01
      case('4D*')
        Gexp(1)=  8.147330000000D-01
        C1(1)= -7.366290000000D-03
        C2(1)=  0.000000000000D+00
        Gexp(2)=  2.730430000000D-01
        C1(2)=  6.959760000000D-02
        C2(2)=  0.000000000000D+00
        Gexp(3)=  1.196700000000D-01
        C1(3)=  4.238050000000D-01
        C2(3)=  0.000000000000D+00
        Gexp(4)=  5.981040000000D-02
        C1(4)=  5.073950000000D-01
        C2(4)=  0.000000000000D+00
        Gexp(5)=  3.146520000000D-02
        C1(5)=  1.003610000000D-01
        C2(5)=  0.000000000000D+00
      case('3SP')
        Gexp(1)=  1.709113496000D+00
        C1(1)= -2.662203391000D-02
        C2(1)= -1.566883448000D-01
        Gexp(2)=  4.654245585000D-01
        C1(2)= -1.603484072000D-01
        C2(2)=  7.214920506000D-01
        Gexp(3)=  1.785129144000D-01
        C1(3)= -4.779473307000D-02
        C2(3)=  3.170854762000D-01
        Gexp(4)=  8.067420191000D-02
        C1(4)=  7.275158441000D-01
        C2(4)=  5.818821382000D-01
        Gexp(5)=  3.914669014000D-02
        C1(5)=  4.123846408000D-01
        C2(5)=  1.701799824000D-01
      case('3D')
        Gexp(1)=  1.539033958000D+00
        C1(1)=  2.020869128000D-02
        C2(1)=  0.000000000000D+00
        Gexp(2)=  4.922090297000D-01
        C1(2)=  1.321157923000D-01
        C2(2)=  0.000000000000D+00
        Gexp(3)=  2.029756928000D-01
        C1(3)=  3.911240346000D-01
        C2(3)=  0.000000000000D+00
        Gexp(4)=  9.424112917000D-02
        C1(4)=  4.779609701000D-01
        C2(4)=  0.000000000000D+00
        Gexp(5)=  4.569058269000D-02
        C1(5)=  1.463662294000D-01
        C2(5)=  0.000000000000D+00
      case('4SP')
        Gexp(1)=  7.857764224000D-01
        C1(1)= -1.628249797000D-03
        C2(1)= -2.399519595000D-01
        Gexp(2)=  2.685947984000D-01
        C1(2)= -2.138931336000D-01
        C2(2)= -8.118321551000D-01
        Gexp(3)=  1.155680976000D-01
        C1(3)= -3.039583456000D-01
        C2(3)=  2.238628944000D-01
        Gexp(4)=  5.839870334000D-02
        C1(4)=  8.491175450000D-01
        C2(4)=  6.726900408000D-01
        Gexp(5)=  3.082651925000D-02
        C1(5)=  5.164147260000D-01
        C2(5)=  2.003802746000D-01
      case('4S')
        Gexp(1)=  2.980263783000D+00
        C1(1)=  1.513948997000D-03
        C2(1)=  0.000000000000D+00
        Gexp(2)=  3.792228833000D-01
        C1(2)= -7.316801548000D-02
        C2(2)=  0.000000000000D+00
        Gexp(3)=  1.789717224000D-01
        C1(3)= -3.143703799000D-01
        C2(3)=  0.000000000000D+00
        Gexp(4)=  5.002110360000D-02
        C1(4)=  9.032615169000D-01
        C2(4)=  0.000000000000D+00
        Gexp(5)=  2.789361681000D-02
        C1(5)=  3.294210848000D-01
        C2(5)=  0.000000000000D+00
      case('4P')
        Gexp(1)=  1.091977298000D+00
        C1(1)= -1.143929558000D-02
        C2(1)=  0.000000000000D+00
        Gexp(2)=  3.719985051000D-01
        C1(2)= -6.322651538000D-02
        C2(2)=  0.000000000000D+00
        Gexp(3)=  8.590019352000D-02
        C1(3)=  4.398907721000D-01
        C2(3)=  0.000000000000D+00
        Gexp(4)=  4.786503860000D-02
        C1(4)=  5.245859166000D-01
        C2(4)=  0.000000000000D+00
        Gexp(5)=  2.730479990000D-02
        C1(5)=  1.017072253000D-01
        C2(5)=  0.000000000000D+00
      case('5SP')
        Gexp(1)=  4.822620000000D-01
        C1(1)=  3.037660000000D-02
        C2(1)= -1.257670000000D-01
        Gexp(2)=  1.813520000000D-01
        C1(2)= -1.825060000000D-01
        C2(2)= -1.563940000000D-01
        Gexp(3)=  8.588910000000D-02
        C1(3)= -6.591490000000D-01
        C2(3)=  6.500750000000D-01
        Gexp(4)=  4.617410000000D-02
        C1(4)=  9.186930000000D-01
        C2(4)=  7.859820000000D-01
        Gexp(5)=  2.582550000000D-02
        C1(5)=  6.952740000000D-01
        C2(5)=  2.582280000000D-01
      case('D-POLARIZATION')
        Gexp(1)=  1.000000000000D+00
        C1(1)=  1.000000000000D+00
      case default
        write(UNIout,'(a)')'STO-5G basis set not available for shell, ',Stype(1:len_trim(Stype))
        call PRG_stop ('STO-5G basis set not available for shell')
      end select
      return
      end subroutine SET_BASIS_STO5G
      subroutine SET_BASIS_STO6G (Stype)
!*****************************************************************************************************************
!     Date last modified: February 7, 2001                                                          Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Set basis set defaults (can only be called after a molecule has been defined.                 *
!*****************************************************************************************************************
! Modules:

      implicit none

! Input scalar:
      character(*) :: Stype
!
! Begin:
      select case(Stype)
      case('1S')
        Gexp(1)=   23.103031490000
        C1(1)=  0.009163596280
        C2(1)=  0.000000000000
        Gexp(2)=    4.235915534000
        C1(2)=  0.049361492940
        C2(2)=  0.000000000000
        Gexp(3)=    1.185056519000
        C1(3)=  0.168538304900
        C2(3)=  0.000000000000
        Gexp(4)=    0.407098898200
        C1(4)=  0.370562799700
        C2(4)=  0.000000000000
        Gexp(5)=    0.158088415100
        C1(5)=  0.416491529800
        C2(5)=  0.000000000000
        Gexp(6)=    0.065109539540
        C1(6)=  0.130334084100
        C2(6)=  0.000000000000
      case('2SP')
        Gexp(1)=   10.308693720000
        C1(1)= -0.013252788090
        C2(1)=  0.003759696623
        Gexp(2)=    2.040359519000
        C1(2)= -0.046991710140
        C2(2)=  0.037679369840
        Gexp(3)=    0.634142217700
        C1(3)= -0.033785371510
        C2(3)=  0.173896743500
        Gexp(4)=    0.243977368500
        C1(4)=  0.250241786100
        C2(4)=  0.418036434700
        Gexp(5)=    0.105959537400
        C1(5)=  0.595117252600
        C2(5)=  0.425859547700
        Gexp(6)=    0.048569008600
        C1(6)=  0.240706176300
        C2(6)=  0.101708295500
      case('3SP*')
        Gexp(1)=    2.730510000000
        C1(1)= -0.009737400000
        C2(1)= -0.810494000000
        Gexp(2)=    0.786247000000
        C1(2)= -0.072658800000
        C2(2)= -0.171548000000
        Gexp(3)=    0.309251000000
        C1(3)= -0.171616000000
        C2(3)=  0.073697900000
        Gexp(4)=    0.141338000000
        C1(4)=  0.128978000000
        C2(4)=  0.396515000000
        Gexp(5)=    0.070339100000
        C1(5)=  0.728861000000
        C2(5)=  0.497808000000
        Gexp(6)=    0.036123500000
        C1(6)=  0.301332000000
        C2(6)=  0.117483000000
      case('3D*')
        Gexp(1)=    2.730510000000
        C1(1)=  0.006633430000
        C2(1)=  0.000000000000
        Gexp(2)=    0.786247000000
        C1(2)=  0.059581800000
        C2(2)=  0.000000000000
        Gexp(3)=    0.309251000000
        C1(3)=  0.240195000000
        C2(3)=  0.000000000000
        Gexp(4)=    0.141338000000
        C1(4)=  0.464811000000
        C2(4)=  0.000000000000
        Gexp(5)=    0.070339100000
        C1(5)=  0.343409000000
        C2(5)=  0.000000000000
        Gexp(6)=    0.036123500000
        C1(6)=  0.053890600000
        C2(6)=  0.000000000000
      case('4SP*')
        Gexp(1)=    1.374340000000
        C1(1)=  0.003713770000
        C2(1)= -0.006877310000
        Gexp(2)=    0.446516000000
        C1(2)= -0.052122000000
        C2(2)= -0.508511000000
        Gexp(3)=    0.192451000000
        C1(3)= -0.307763000000
        C2(3)= -0.420645000000
        Gexp(4)=    0.095828200000
        C1(4)= -0.064512500000
        C2(4)=  0.365240000000
        Gexp(5)=    0.051664200000
        C1(5)=  0.898078000000
        C2(5)=  0.591591000000
        Gexp(6)=    0.028589400000
        C1(6)=  0.369096000000
        C2(6)=  0.134413000000
      case('4D*')
        Gexp(1)=    1.374340000000
        C1(1)= -0.004346380000
        C2(1)=  0.000000000000
        Gexp(2)=    0.446516000000
        C1(2)=  0.004891230000
        C2(2)=  0.000000000000
        Gexp(3)=    0.192451000000
        C1(3)=  0.163480000000
        C2(3)=  0.000000000000
        Gexp(4)=    0.095828200000
        C1(4)=  0.480748000000
        C2(4)=  0.000000000000
        Gexp(5)=    0.051664200000
        C1(5)=  0.390692000000
        C2(5)=  0.000000000000
        Gexp(6)=    0.028589400000
        C1(6)=  0.057866300000
        C2(6)=  0.000000000000
      case('3SP')
        Gexp(1)=    3.080165240000
        C1(1)= -0.007943126362
        C2(1)= -0.713935890700
        Gexp(2)=    0.824895920200
        C1(2)= -0.071002641720
        C2(2)= -0.182927707000
        Gexp(3)=    0.309344734900
        C1(3)= -0.178502692500
        C2(3)=  0.076216214280
        Gexp(4)=    0.138468389700
        C1(4)=  0.151063505800
        C2(4)=  0.414509859700
        Gexp(5)=    0.068520949510
        C1(5)=  0.735491476700
        C2(5)=  0.488962147100
        Gexp(6)=    0.035313336900
        C1(6)=  0.276059312300
        C2(6)=  0.105881652100
      case('3D')
        Gexp(1)=    2.488296923000
        C1(1)=  0.007283828112
        C2(1)=  0.000000000000
        Gexp(2)=    0.798148785300
        C1(2)=  0.053867993630
        C2(2)=  0.000000000000
        Gexp(3)=    0.331132749000
        C1(3)=  0.207213914900
        C2(3)=  0.000000000000
        Gexp(4)=    0.155911446300
        C1(4)=  0.426626909200
        C2(4)=  0.000000000000
        Gexp(5)=    0.078777347320
        C1(5)=  0.384310020400
        C2(5)=  0.000000000000
        Gexp(6)=    0.040584843630
        C1(6)=  0.089028275460
        C2(6)=  0.000000000000
      case('4SP')
        Gexp(1)=    1.365350000000
        C1(1)=  0.003775060000
        C2(1)= -0.007052080000
        Gexp(2)=    0.439321000000
        C1(2)= -0.055859700000
        C2(2)= -0.525951000000
        Gexp(3)=    0.187707000000
        C1(3)= -0.319295000000
        C2(3)= -0.377345000000
        Gexp(4)=    0.093602700000
        C1(4)= -0.027647800000
        C2(4)=  0.387477000000
        Gexp(5)=    0.050522600000
        C1(5)=  0.904920000000
        C2(5)=  0.579167000000
        Gexp(6)=    0.028093500000
        C1(6)=  0.340626000000
        C2(6)=  0.122182000000
      case('4S')
        Gexp(1)=    3.232838646000
        C1(1)=  0.001374817488
        C2(1)=  0.000000000000
        Gexp(2)=    0.360578880200
        C1(2)= -0.086663900430
        C2(2)=  0.000000000000
        Gexp(3)=    0.171790548700
        C1(3)= -0.313062730900
        C2(3)=  0.000000000000
        Gexp(4)=    0.052776664870
        C1(4)=  0.781278739700
        C2(4)=  0.000000000000
        Gexp(5)=    0.031634002840
        C1(5)=  0.438924798800
        C2(5)=  0.000000000000
        Gexp(6)=    0.018740930910
        C1(6)=  0.024871787560
        C2(6)=  0.000000000000
      case('4P')
        Gexp(1)=    2.389722618000
        C1(1)= -0.001665913575
        C2(1)=  0.000000000000
        Gexp(2)=    0.796094782600
        C1(2)= -0.016574649710
        C2(2)=  0.000000000000
        Gexp(3)=    0.341554138000
        C1(3)= -0.059585133780
        C2(3)=  0.000000000000
        Gexp(4)=    0.088474345250
        C1(4)=  0.405311555400
        C2(4)=  0.000000000000
        Gexp(5)=    0.049582483340
        C1(5)=  0.543395818900
        C2(5)=  0.000000000000
        Gexp(6)=    0.028169297840
        C1(6)=  0.120497049100
        C2(6)=  0.000000000000
      case('5SP')
        Gexp(1)=    0.770142000000
        C1(1)=  0.012674500000
        C2(1)= -0.001105680000
        Gexp(2)=    0.023483900000
        C1(2)=  0.436850000000
        C2(2)=  0.149315000000
        Gexp(3)=    0.040025500000
        C1(3)=  1.104320000000
        C2(3)=  0.696458000000
        Gexp(4)=    0.069534400000
        C1(4)= -0.323200000000
        C2(4)=  0.321033000000
        Gexp(5)=    0.130185000000
        C1(5)= -0.430756000000
        C2(5)= -0.162848000000
        Gexp(6)=    0.275627000000
        C1(6)=  0.003266700000
        C2(6)= -0.062431400000
      case('D-POLARIZATION')
        Ngauss(Ishell)=1
        Gexp(1)=    1.000000000000
        C1(1)=  1.000000000000
      case default
        write(UNIout,'(a)')'STO-6G basis set not available for shell, ',Stype(1:len_trim(Stype))
        call PRG_stop ('STO-6G basis set not available for shell')
      end select
      return
      end subroutine SET_BASIS_STO6G
! END CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine BLD_STO_BASIS
