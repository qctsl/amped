      subroutine BLD_aug_cc_pVTZ_modp_BASIS
!*****************************************************************************************
!     Date last modified: April 29, 2016                                    Version 1.0  *
!     Author: JWH                                                                        *
!     Description: Define a basis set.                                                   *
!*****************************************************************************************
! Modules:
      USE MENU_BLD_basis

      implicit none
!
! Local scalars:
      logical :: LKept
!
! Begin:
      Type(1:30)(1:16)=' '
! 1) Loop over all the elements in the molecule
      do IELEM=1,NELEMTS
        ELEMENT=ELMLIST(IELEM)
        call SET_BASIS_aug_cc_pVTZ_modp

        IGstart=1
        do Ishell=1,NshellIN
          LKept=.true.
! Determine if this shell must be kept:
          if(Type(Ishell)(1:5).eq.'P-POL'.and..not.PPOLAR)LKept=.false.
          if(Type(Ishell)(1:5).eq.'D-POL'.and..not.DPOLAR)LKept=.false.
          if(Type(Ishell)(1:6).eq.'D2-POL'.and..not.D2POLAR)LKept=.false.
          if(Type(Ishell)(1:6).eq.'D2-POL')Type='D-POLARIZATION '
          if(Type(Ishell)(1:5).eq.'F-POL'.and..not.FPOLAR)LKept=.false.
          if(Type(Ishell)(1:5).eq.'F-POL'.and..not.FPOLAR)LKept=.false.
          if(Type(Ishell)(1:5).eq.'S-DIF'.and..not.HDIFFUSE)LKept=.false.
          if(Type(Ishell)(1:6).eq.'SP-DIF'.and..not.DIFFUSE)LKept=.false.
 
          IGend=IGstart+Ngauss(Ishell)-1
          if(LKept)then ! Save this shell:
            do Igauss=IGstart,IGend
              Gexp(Igauss)=Gexp(Igauss)*ScaleF(Ishell)*ScaleF(Ishell)
            end do
            call SAVE_shell
          end if ! LKept
          IGstart=IGend+1
        end do ! Ishell
      end do ! IELEM
!
! End of routine BLD_aug_cc_pVTZ_modp_BASIS
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine SET_BASIS_aug_cc_pVTZ_modp
!*************************************************************************************************
!     Date last modified: July 4, 2011                                           Version 1.0     *
!     Author: R.A. Poirier                                                                       *
!     Description: Set basis set defaults (can only be called after a molecule has been defined. *
!*************************************************************************************************
! Modules:

      implicit none

!
! Begin:
      Igauss=0
      Ishell=0
      select case(Element)
      case ( 'HYDROGEN' )
          NshellIN=10
          Ishell=Ishell+1
          Type(Ishell) = '1S'
          ScaleF(Ishell) = 1.00
          Ngauss(Ishell) = 5
          Igauss=Igauss+1
          Gexp(Igauss) = 3.387000D+01
            C1(Igauss) = 6.068000D-03
          Igauss=Igauss+1
          Gexp(Igauss) = 5.095000D+00
            C1(Igauss) = 4.530800D-02
          Igauss=Igauss+1
          Gexp(Igauss) = 1.159000D+00
            C1(Igauss) = 2.028220D-01
          Igauss=Igauss+1
          Gexp(Igauss) = 3.258000D-01
            C1(Igauss) = 5.039030D-01
          Igauss=Igauss+1
          Gexp(Igauss) = 1.027000D-01
            C1(Igauss) = 3.834210D-01
          Ishell=Ishell+1
          Type(Ishell) = '2S'
          ScaleF(Ishell) = 1.00
          Ngauss(Ishell) = 1
          Igauss=Igauss+1
          Gexp(Igauss) = 3.258000D-01
            C1(Igauss) = 1.000000D+00
          Ishell=Ishell+1
          Type(Ishell) = '3S'
          ScaleF(Ishell) = 1.00
          Ngauss(Ishell) = 1
          Igauss=Igauss+1
          Gexp(Igauss) = 1.027000D-01
            C1(Igauss) = 1.000000D+00
          Ishell=Ishell+1
          Type(Ishell) = '4S'
          ScaleF(Ishell) = 1.00
          Ngauss(Ishell) = 1
          Igauss=Igauss+1
          Gexp(Igauss) = 0.0252600D0
            C1(Igauss) = 1.0000000D0
          Ishell=Ishell+1
          Type(Ishell) = '1P'
          ScaleF(Ishell) = 1.00
          Ngauss(Ishell) = 5
          Igauss=Igauss+1
          Gexp(Igauss) = 3.387000D+01
            C1(Igauss) = 6.068000D-03
          Igauss=Igauss+1
          Gexp(Igauss) = 5.095000D+00
            C1(Igauss) = 4.530800D-02
          Igauss=Igauss+1
          Gexp(Igauss) = 1.159000D+00
            C1(Igauss) = 2.028220D-01
          Igauss=Igauss+1
          Gexp(Igauss) = 3.258000D-01
            C1(Igauss) = 5.039030D-01
          Igauss=Igauss+1
          Gexp(Igauss) = 1.027000D-01
            C1(Igauss) = 3.834210D-01
          Ishell=Ishell+1
          Type(Ishell) = '2P'
          ScaleF(Ishell) = 1.00
          Ngauss(Ishell) = 1
          Igauss=Igauss+1
          Gexp(Igauss) = 3.258000D-01
            C1(Igauss) = 1.000000D+00
          Ishell=Ishell+1
          Type(Ishell) = '3P'
          ScaleF(Ishell) = 1.00
          Ngauss(Ishell) = 1
          Igauss=Igauss+1
          Gexp(Igauss) = 1.027000D-01
            C1(Igauss) = 1.000000D+00
          Ishell=Ishell+1
          Type(Ishell) = '4P'
          ScaleF(Ishell) = 1.00
          Ngauss(Ishell) = 1
          Igauss=Igauss+1
          Gexp(Igauss) = 0.0252600D0
            C1(Igauss) = 1.0000000D0
          Ishell=Ishell+1
          Type(Ishell) = '3D'
          ScaleF(Ishell) = 1.00
          Ngauss(Ishell) = 1
          Igauss=Igauss+1
          Gexp(Igauss) = 1.057000D+00
            C1(Igauss) = 1.0000000D0
          Ishell=Ishell+1
          Type(Ishell) = '4D'
          ScaleF(Ishell) = 1.00
          Ngauss(Ishell) = 1
          Igauss=Igauss+1
          Gexp(Igauss) = 0.2470000D0
            C1(Igauss) = 1.0000000D0

      case default
        write(UNIout,'(a)')'aug-cc-pVTZ_modp basis set not available for element, ',Element(1:len_trim(Element))
        call PRG_stop ('aug-cc-pVTZ_modp basis set not available for element')
      end select
      return
      end subroutine SET_BASIS_aug_cc_pVTZ_modp
! END CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine BLD_aug_cc_pVTZ_modp_BASIS

