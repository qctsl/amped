      subroutine BLD_pVQZ16e_BASIS
!*****************************************************************************************************************
!     Date last modified: May 16, 2011                                                              Version 2.0  *
!     Author: V. R. Grandy, R. A. Poirier                                                                        *
!     Description: Define a basis set.                                                                           *
!*****************************************************************************************************************
! Modules:
      USE MENU_BLD_basis

      implicit none
!
! Local scalars:
      integer :: Znum
!
! Begin:
      Type(1:30)(1:16)=' '
! 1) Loop over all the elements in the molecule
      do IELEM=1,NELEMTS
!       ELEMENT=ELMLIST(IELEM)
        Znum=CARTESIAN(1)%Atomic_number
        call SET_BASIS_pVQZ16e

        write(6,'(a,i4)')' Znum: ',Znum
!       ScaleF(1)=0.2297D0*Znum+0.0558D0
!       ScaleF(2)=0.2542D0*Znum-0.1482D0
!       ScaleF(3)=0.4727D0*Znum-0.6575D0
!       ScaleF(4)=0.4004D0*Znum-0.2153D0
!       ScaleF(5)=3.6286D0*Znum-6.7413D0
      
        IGstart=1
        do Ishell=1,NshellIN
          IGend=IGstart+Ngauss(Ishell)-1
            do Igauss=IGstart,IGend
              Gexp(Igauss)=Gexp(Igauss)*ScaleF(Ishell)*ScaleF(Ishell)
            end do
            call SAVE_shell
          IGstart=IGend+1
        end do ! Ishell
      end do ! IELEM
!
! End of routine BLD_pVQZ16e_BASIS
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine SET_BASIS_pVQZ16e
!*****************************************************************************************************************
!     Date last modified: May 16, 2011                                                              Version 2.0  *
!     Author: V. R. Grandy, R.A. Poirier                                                                         *
!     Description: Set basis set defaults (can only be called after a molecule has been defined.                 *
!*****************************************************************************************************************
! Modules:

      implicit none
!
! Begin:
      NshellIN=13
      Type(1)='1S'
      Ngauss(1)=13
      Gexp(1)=727800.0000000
      C1(1)=0.236025D-04
      Gexp(2)=109000.0000000
      C1(2)=0.183482D-03
      Gexp(3)=24800.0000000
      C1(3)=0.964278D-03
      Gexp(4)=7014.0000000
      C1(4)=0.406537D-02
      Gexp(5)=2278.0000000
      C1(5)=0.146973D-01
      Gexp(6)=814.7000000
      C1(6)=0.465081D-01
      Gexp(7)=313.4000000
      C1(7)=0.125508D+00
      Gexp(8)=127.7000000
      C1(8)=0.268433D+00
      Gexp(9)=54.4800000
      C1(9)=0.384809D+00
      Gexp(10)=23.8500000
      C1(10)=0.265372D+00
      Gexp(11)=9.4280000
      C1(11)=0.437326D-01
      Gexp(12)=4.2900000
      C1(12)=-0.378807D-02
      Gexp(13)=1.9090000
      C1(13)=0.218083D-02

      Type(2)='2S'
      Ngauss(2)=13
      Gexp(14)=727800.0000000
      C1(14)=-0.652179D-05
      Gexp(15)=109000.0000000
      C1(15)=-0.506631D-04
      Gexp(16)=24800.0000000
      C1(16)=-0.266833D-03
      Gexp(17)=7014.0000000
      C1(17)=-0.112601D-02
      Gexp(18)=2278.0000000
      C1(18)=-0.411186D-02
      Gexp(19)=814.7000000
      C1(19)=-0.132454D-01
      Gexp(20)=313.4000000
      C1(20)=-0.377004D-01
      Gexp(21)=127.7000000
      C1(21)=-0.898554D-01
      Gexp(22)=54.4800000
      C1(22)=-0.167098D+00
      Gexp(23)=23.8500000
      C1(23)=-0.169354D+00
      Gexp(24)=9.4280000
      C1(24)=0.127824D+00
      Gexp(25)=4.2900000
      C1(25)=0.564862D+00
      Gexp(26)=1.9090000
      C1(26)=0.431767D+00

      Type(3)='3S'
      Ngauss(3)=13
      Gexp(27)=727800.0000000
      C1(27)=0.189406D-05
      Gexp(28)=109000.0000000
      C1(28)=0.146948D-04
      Gexp(29)=24800.0000000
      C1(29)=0.775460D-04
      Gexp(30)=7014.0000000
      C1(30)=0.326509D-03
      Gexp(31)=2278.0000000
      C1(31)=0.119686D-02
      Gexp(32)=814.7000000
      C1(32)=0.384799D-02
      Gexp(33)=313.4000000
      C1(33)=0.110539D-01
      Gexp(34)=127.7000000
      C1(34)=0.264645D-01
      Gexp(35)=54.4800000
      C1(35)=0.508771D-01
      Gexp(36)=23.8500000
      C1(36)=0.530030D-01
      Gexp(37)=9.4280000
      C1(37)=-0.425518D-01
      Gexp(38)=4.2900000
      C1(38)=-0.250853D+00
      Gexp(39)=1.9090000
      C1(39)=-0.333152D+00

      Type(4)='4S'
      Ngauss(4)=1
      Gexp(40)=0.6270000
      C1(40)=1.0000000

      Type(5)='5S'
      Ngauss(5)=1
      Gexp(41)=0.2873000
      C1(41)=1.0000000

      Type(6)='6S'
      Ngauss(6)=1
      Gexp(42)=0.1172000
      C1(42)=1.0000000

      Type(7)='7S'
      Ngauss(7)=1
      Gexp(43)=0.0428000
      C1(43)=1.0000000

      Type(8)='1P'
      Ngauss(8)=8
      Gexp(44)=1546.0000000
      C1(44)=0.441183D-03
      Gexp(45)=366.4000000
      C1(45)=0.377571D-02
      Gexp(46)=118.4000000
      C1(46)=0.198360D-01
      Gexp(47)=44.5300000
      C1(47)=0.742063D-01
      Gexp(48)=18.3800000
      C1(48)=0.197327D+00
      Gexp(49)=7.9650000
      C1(49)=0.351851D+00
      Gexp(50)=3.5410000
      C1(50)=0.378687D+00
      Gexp(51)=1.5910000
      C1(51)=0.170931D+00

      Type(9)='2P'
      Ngauss(9)=8
      Gexp(52)=1546.0000000
      C1(52)=-0.113110D-03
      Gexp(53)=366.4000000
      C1(53)=-0.958581D-03
      Gexp(54)=118.4000000
      C1(54)=-0.513471D-02
      Gexp(55)=44.5300000
      C1(55)=-0.192641D-01
      Gexp(56)=18.3800000
      C1(56)=-0.535980D-01
      Gexp(57)=7.9650000
      C1(57)=-0.960333D-01
      Gexp(58)=3.5410000
      C1(58)=-0.118183D+00
      Gexp(59)=1.5910000
      C1(59)=0.923194D-02

      Type(10)='3P'
      Ngauss(10)=1
      Gexp(60)=0.6205000
      C1(60)=1.0000000

      Type(11)='4P'
      Ngauss(11)=1
      Gexp(61)=0.2420000
      C1(61)=1.0000000

      Type(12)='5P'
      Ngauss(12)=1
      Gexp(62)=0.0901400
      C1(62)=1.0000000

      Type(13)='6P'
      Ngauss(13)=1
      Gexp(63)=0.0317000
      C1(63)=1.0000000

      select case(Znum) ! Can USE Znum
      case(1)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(2)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(3)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(4)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(5)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(6)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(7)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(8)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(9)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(10)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(11)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(12)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(13)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(14)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(15)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(16)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(17)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(18)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(19)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(20)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(21)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(22)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(23)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(24)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(25)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(26)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(27)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(28)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(29)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(30)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(31)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(32)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(33)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(34)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(35)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(36)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case default
        write(UNIout,'(a)')'pVQZ16e basis set not available for element, ',Element(1:len_trim(Element))
        call PRG_stop ('pVQZ16e basis set not available for element')
      end select
      return
      end subroutine SET_BASIS_pVQZ16e
! END CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine BLD_pVQZ16e_BASIS
