      subroutine BLD_basis_list
!***********************************************************************
!     Date last modified: July 8, 2014                    Version 1.0  *
!     Author: J.W. Hollett                                             *
!     Description: Identify and count the number of each type of basis *
!                  function and store in lists.                        *
!***********************************************************************
! Modules:
      USE type_basis_set
!
      implicit none
!
! Local scalars:
      integer :: I_shell, I_s, I_p, I_d
!
! Begin:
      call PRG_manager ('enter', 'BLD_basis_list', 'UTILITY')
!
! Get listing of shells of each type
      N_s=0
      N_p=0
      N_d=0
      do I_shell = 1, Basis%Nshells
        if(Basis%shell(I_shell)%Xtype.eq.0)then
          N_s = N_s +1
        end if
        if(Basis%shell(I_shell)%Xtype.eq.1)then
          N_p = N_p +1
        end if
        if(Basis%shell(I_shell)%Xtype.eq.2)then
          N_d = N_d +1
        end if
      end do ! I_shell
      if(allocated(s_list))then
        deallocate(s_list,p_list,d_list)
      end if
      allocate(s_list(N_s),p_list(N_p),d_list(N_d))
      I_s = 0
      I_p = 0
      I_d = 0
      do I_shell = 1, Basis%Nshells
        if(Basis%shell(I_shell)%Xtype.eq.0)then
          I_s = I_s + 1
          s_list(I_s) = I_shell
        end if
        if(Basis%shell(I_shell)%Xtype.eq.1)then
          I_p = I_p + 1
          p_list(I_p) = I_shell
        end if
        if(Basis%shell(I_shell)%Xtype.eq.2)then
          I_d = I_d + 1
          d_list(I_d) = I_shell
        end if
      end do ! I_shell
!
! End of routine BLD_basis_list
      call PRG_manager ('exit', 'BLD_basis_list', 'UTILITY')
      end subroutine BLD_basis_list
      subroutine BLD_GAMESSbasis
!***********************************************************************
!     Date last modified: May 13, 2019                    Version 1.0  *
!     Author: J.W. Hollett                                             *
!     Description: Identify some key parameters in the GAMESS style    *
!                  of basis set ordering.                              *
!***********************************************************************
! Modules:
      USE type_basis_set
      USE type_molecule
!
      implicit none
!
! Local scalars:
      integer :: I_shell, Iatom, I_atmshl
!
! Begin:
      call PRG_manager ('enter', 'BLD_GAMESSbasis', 'UTILITY')
!
      if(allocated(Ns_atom))then
        deallocate(Ns_atom,Np_atom,Nd_atom,frstAOatom)
      end if
      allocate(Ns_atom(Natoms),Np_atom(Natoms),Nd_atom(Natoms))
      allocate(frstAOatom(Natoms))
!
      Ns_atom = 0
      Np_atom = 0
      Nd_atom = 0
      do I_shell = 1, Basis%Nshells
        do I_atmshl = Basis%shell(I_shell)%frstshl,Basis%shell(I_shell)%lastshl
          Iatom = Basis%atmshl(I_atmshl)%atmlst
!
          if(Basis%shell(I_shell)%Xtype.eq.0)then
            Ns_atom(Iatom) = Ns_atom(Iatom) + 1
          end if
          if(Basis%shell(I_shell)%Xtype.eq.1)then
            Np_atom(Iatom) = Np_atom(Iatom) + 1
          end if
          if(Basis%shell(I_shell)%Xtype.eq.2)then
            Nd_atom(Iatom) = Nd_atom(Iatom) + 1
          end if
!
        end do ! I_atmshl
      end do ! I_shell
!
! Determine index of first AO on each atom
      frstAOatom(1) = 1
      do Iatom = 2, Natoms
        frstAOatom(Iatom) = sum(Ns_atom(1:Iatom-1)) + 3*sum(Np_atom(1:Iatom-1))&
                                                    + 6*sum(Nd_atom(1:Iatom-1)) + 1
      end do ! Iatom
!
!      write(6,*)' atom    frstAOatom '
!      do Iatom = 1, Natoms
!        
!      end do 
!
! End of routine BLD_basis_list
      call PRG_manager ('exit', 'BLD_GAMESSbasis', 'UTILITY')
      end subroutine BLD_GAMESSbasis
