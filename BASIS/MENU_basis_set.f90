      subroutine MENU_basis_set
!***********************************************************************
!     Date last modified: January  28, 1994                Version 1.0 *
!     Author: R. A. Poirier                                            *
!     Description: Set up Basis.                                       *
!***********************************************************************
! Modules:
      USE program_constants
      USE program_parser
      USE menu_gets
      USE type_basis_set
      USE QM_defaults
      USE QM_objects

      implicit none
!
! Local scalars:
      integer lendir,lenstr
      logical done,LPRTBAS
      integer, parameter :: MAX_dirlen=132   ! Maximum length for name of directory
      character(len=MAX_dirlen) BASIS_dir
!
! Begin:
! Menu:
      LPRTBAS=.false.
      done=.false.
      do while (.not.done)
        call new_token(' Basis:')
        if(token(1:4).eq.'HELP')then
          write(UNIout,'(a)') &
      ' Command BASis :', &
      '   Purpose: Define the basis set', &
      '   Syntax :', &
      '   DIr = <name>: Directory for the Basis Set file ', &
      '   NAme = <name>: e.g.  3-21G, 6-31G**, GENeral ... ', &
      '   PRrint : print the basis set (default=no print)', &
      '   end'
!
! NAme
        else if(token(1:2).eq.'NA') then
          call GET_value (BASIS_name, lenstr)
          Basis_set_name=BASIS_name
!         write(6,*)'MENU: > BASIS_name: ',Basis_name(1:lenstr)
! DIrectory
        else if(token(1:2).eq.'DI') then
          call GET_value (BASIS_DIR, lendir)
! Print
        else if(token(1:2).eq.'PR') then
          LPRTBAS=.TRUE.

        else
          call MENU_end (done)
        end if
      end do ! while
!
      call GET_basis_set (BASIS_name, Current_Basis, LPRTBAS)
      Basis=>Current_Basis
      Nbasis=Basis%Nbasis
      MATlen=Nbasis*(Nbasis+1)/2

      call RESET_basis_set

      if(.not.LPRTBAS)then
        write(UNIout,'(2a,I8/)')Basis%name(1:len_trim(Basis%name)), &
           ' Basis Set - Total number of basis functions: ',Basis%Nbasis
      end if

      if(LBasis_defined)then
        call PRG_reset
      end if

      LBasis_defined=.true.
!
! End of routine MENU_basis_set
      return
      end subroutine MENU_basis_set
