      subroutine BLD_GEN_BASIS
!*****************************************************************************************************************
!     Date last modified: February 16, 2000                                                         Version 2.0  *
!     Author: R. A. Poirier                                                                                      *
!     Description: Define a basis set.                                                                           *
!*****************************************************************************************************************
! Modules:
      USE MENU_BLD_basis

      implicit none
!
! Local scalars:
      logical :: LKept
!
! Begin:
      Type(1:30)(1:16)=' '
! 1) Loop over all the elements in the molecule
        if(BASIC_name(1:5).eq.'3-21G')then
          call BLD_321G_BASIS
        else if(BASIC_name(1:5).eq.'6-31G')then
          call BLD_631G_BASIS
        else if(BASIC_name(1:6).eq.'6-311G')then
          call BLD_6311G_BASIS
        else if(BASIC_name(1:5).eq.'G10S')then
          call BLD_G10S_BASIS
        else if(BASIC_name(1:11).eq.'aug-cc-pVDZ')then
          call BLD_aug_cc_pVDZ_BASIS
        else if(BASIC_name(1:16).eq.'aug-cc-pVTZ-modp')then
          call BLD_aug_cc_pVTZ_modp_BASIS
        else if(BASIC_name(1:16).eq.'aug-cc-pVTZ-muon')then
          call BLD_aug_cc_pVTZ_muon_BASIS
        else if(BASIC_name(1:21).eq.'aug(s,p)-cc-pVTZ-modp')then
          call BLD_augsp_cc_pVTZ_modp_BASIS
        else if(BASIC_name(1:11).eq.'aug-cc-pVTZ')then
          call BLD_aug_cc_pVTZ_BASIS
        else if(BASIC_name(1:12).eq.'aug-cc-pCVTZ')then
          call BLD_aug_cc_pCVTZ_BASIS
        else if(BASIC_name(1:7).eq.'cc-pVTZ')then
          call BLD_cc_pVTZ_BASIS
        else if(BASIC_name(1:11).eq.'aug-cc-pVQZ')then
          call BLD_aug_cc_pVQZ_BASIS
        else if(BASIC_name(1:11).eq.'aug-cc-pV5Z')then
          call BLD_aug_cc_pV5Z_BASIS
        else if(BASIC_name(1:13).eq.'aug-cc-pV5Z-S')then
          call BLD_aug_cc_pV5Z_S_BASIS
        else if(BASIC_name(1:13).eq.'aug-cc-pV5Z-P')then
          call BLD_aug_cc_pV5Z_P_BASIS
        else if(BASIC_name(1:13).eq.'aug-cc-pV5Z-D')then
          call BLD_aug_cc_pV5Z_D_BASIS
        else if(BASIC_name(1:7).eq.'cc-pVQZ')then
          call BLD_cc_pVQZ_BASIS
        else if(BASIC_name(1:7).eq.'cc-pVDZ')then
          call BLD_cc_pVDZ_BASIS
        else if(BASIC_name(1:7).eq.'pVTZ-2e')then
          call BLD_pVTZ2e_BASIS
        else if(BASIC_name(1:7).eq.'pVTZ-3e')then
          call BLD_pVTZ3e_BASIS
        else if(BASIC_name(1:7).eq.'pVTZ-4e')then
          call BLD_pVTZ4e_BASIS
        else if(BASIC_name(1:8).eq.'pCVQZ-4e')then
          call BLD_pCVQZ4e_BASIS
        else if(BASIC_name(1:7).eq.'pVTZ-5e')then
          call BLD_pVTZ5e_BASIS
        else if(BASIC_name(1:7).eq.'pVTZ-6e')then
          call BLD_pVTZ6e_BASIS
        else if(BASIC_name(1:7).eq.'pVTZ-7e')then
          call BLD_pVTZ7e_BASIS
        else if(BASIC_name(1:7).eq.'pVTZ-8e')then
          call BLD_pVTZ8e_BASIS
        else if(BASIC_name(1:7).eq.'pVTZ-9e')then
          call BLD_pVTZ9e_BASIS
        else if(BASIC_name(1:8).eq.'pVTZ-10e')then
          call BLD_pVTZ10e_BASIS
        else if(BASIC_name(1:7).eq.'pVQZ-2e')then
          call BLD_pVQZ2e_BASIS
        else if(BASIC_name(1:7).eq.'pVQZ-3e')then
          call BLD_pVQZ3e_BASIS
        else if(BASIC_name(1:7).eq.'pVQZ-4e')then
          call BLD_pVQZ4e_BASIS
        else if(BASIC_name(1:7).eq.'pVQZ-5e')then
          call BLD_pVQZ5e_BASIS
        else if(BASIC_name(1:7).eq.'pVQZ-6e')then
          call BLD_pVQZ6e_BASIS
        else if(BASIC_name(1:7).eq.'pVQZ-7e')then
          call BLD_pVQZ7e_BASIS
        else if(BASIC_name(1:7).eq.'pVQZ-8e')then
          call BLD_pVQZ8e_BASIS
        else if(BASIC_name(1:7).eq.'pVQZ-9e')then
          call BLD_pVQZ9e_BASIS
        else if(BASIC_name(1:7).eq.'pVQZ-10e')then
          call BLD_pVQZ10e_BASIS
        else if(BASIC_name(1:8).eq.'pVQZ-11e')then
          call BLD_pVQZ11e_BASIS
        else if(BASIC_name(1:8).eq.'pVQZ-12e')then
          call BLD_pVQZ12e_BASIS
        else if(BASIC_name(1:8).eq.'pVQZ-13e')then
          call BLD_pVQZ13e_BASIS
        else if(BASIC_name(1:8).eq.'pVQZ-14e')then
          call BLD_pVQZ14e_BASIS
        else if(BASIC_name(1:8).eq.'pVQZ-15e')then
          call BLD_pVQZ15e_BASIS
        else if(BASIC_name(1:8).eq.'pVQZ-16e')then
          call BLD_pVQZ16e_BASIS
        else if(BASIC_name(1:8).eq.'pVQZ-17e')then
          call BLD_pVQZ17e_BASIS
        else if(BASIC_name(1:8).eq.'pVQZ-18e')then
          call BLD_pVQZ18e_BASIS
        else if(BASIC_name(1:4).eq.'pVQZ')then
          call BLD_pVQZ_BASIS
        else if(BASIC_name(1:6).eq.'simple')then
          call BLD_simple_BASIS
        else if(BASIC_name(1:6).eq.'IMPORT')then
          call BLD_IMPORT_BASIS
        else
          write(UNIout,'(a)')'Basis set: ',BASIC_name(1:len_trim(BASIC_name)),' not available'
          call PRG_stop ('Basis set not available')
        end if
!
! End of routine BLD_GEN_BASIS
      return
      end subroutine BLD_GEN_BASIS
