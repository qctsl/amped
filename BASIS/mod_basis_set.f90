      MODULE type_basis_set
!*****************************************************************************************************************
!     Date last modified: January 18, 2001                                                          Version 1.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Names of allowed basis sets.                                                                  *
!*****************************************************************************************************************
      implicit none

! Basis set defaults:
      logical :: LBasis_defined ! .TRUE. once the basis set has been defined
      logical :: LBasis_Guess ! .TRUE. once the guess basis set has been defined
      logical :: LSorted ! .TRUE. (default) Basis set not sorted only if using the Gaussian density
!     integer :: impcount
      character(len=128) :: BASIS_name
      character(len=80), allocatable, dimension(:) :: basisimport

! Dimensioned Nprimitives
      type basis_primitives
        double precision EXP    ! The gaussian exponents
        double precision CONTRC ! The contraction coefficients
      end type basis_primitives
!
! Dimensioned Nshells
      type basis_shells
        integer SHLATM   ! can be changed to a work array, only used in setbas
        integer EXPBGN   ! first exponent belonging to given shell
        integer EXPend   ! last exponent belonging to given shell
        integer Xtype    ! shell type (0 for S, 1 for P, 2 for D and 3 for F)
        integer Xstart   ! 1 for S, 2 for P, 5 for D, 11 for F
        integer Xend     ! 1 for S, 4 for P, 10 for D, 20 for F
        integer frstSHL  ! First atom-shell (specific atom) that given shell belongs to
        integer lastSHL  ! Last atom-shell (specific atom) that given shell belongs to
        double precision SHLSCL ! scale factor for exponents of the given shell
        character(len=2) ::SHLTYP  ! S, P, D or F
        character(len=16) :: SHLTLB ! 1S, 2S, 3D, ...
        logical LCOREshl ! Special array to determine if a shell is core or valence
      end type basis_shells

! Dimensioned Natmshls (Number of atom-shells)
      type basis_atom_shells
        integer ATMLST  ! The atom number corresponding to the atom-shell
        integer frstAO  ! The first AO belonging to the given atom-shell
        integer lastAO  ! The last AO belonging to the given atom-shell
      end type basis_atom_shells

      type basis_set
        character(len=128) :: name
        integer :: NDORBS,NFORBS             ! 5 or 6 D / 7 or 10 F
        integer :: NFDShells
        integer :: NBasis
        integer :: Nprimitives
        type (basis_primitives), dimension(:), pointer :: gaussian=>null()
        integer :: Nshells
        type (basis_shells), dimension(:), pointer :: shell=>null()
        integer :: Natmshls
        type (basis_atom_shells), dimension(:), pointer :: atmshl=>null()
        integer :: NCCOEFFS
        integer, dimension(:), pointer :: CCPNTR=>null()
        double precision, dimension(:), pointer :: CCBYAO=>null()
        integer :: NAOtype
        character(len=8), dimension(:), pointer :: AOtype=>null()
        integer, dimension(:), pointer :: AOtoATOM=>null()   ! Nbasis
        double precision, dimension(:,:), pointer ::  minEABP=>null()
      end type basis_set

!     double precision, dimension(:), allocatable :: AOprod ! AO product at a point r (matlen)
!     double precision, dimension(:), allocatable :: dAOprodX,dAOprodY,dAOprodZ ! d/dx of AOprod

      type (basis_set), save, target :: Current_basis  ! Current basis set
      type (basis_set), save, target :: GUESS_basis    ! Basis set projecting from for initial guess
      type (basis_set), pointer :: basis=>null() ! Current basis set
!
! List of basis function types for Obara-Saika RR code (can probably be improved)
      integer :: N_s, N_p , N_d
      integer, dimension(:), allocatable :: s_list, p_list, d_list
!
! Some values for interpreting GAMESS basis set ordering
      integer, dimension(:), allocatable :: Ns_atom, Np_atom, Nd_atom, frstAOatom
!
!
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine NORMALIZE_BASIS (BasisIN, &
                                  ICODE)
!***********************************************************************
!     Date last modified: April 30, 1991                   Version 1.0 *
!     Author:                                                          *
!     Description: Normalize or un-normalize the Gaussian functions.   *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants

      implicit none
!
! Input:
      integer ICODE
      type (basis_set) BasisIN
!
! Local scalars:
      integer I,J,K,N,NF
      double precision APB,AXB,CJK,E,FCON,PI34,ROOT2,ROOT3
!
! Local array:
      double precision OVRLAP(4)
!
! Local parameters:
      double precision FORPT5,F15,ONEPT5,ONEP25,ONEP75,PT75,TENM8,THRPT5,TWOPT5,TWOP25
      parameter (TENM8=1.0D-8,PT75=0.75D0,ONEP25=1.25D0,ONEPT5=1.5D0,ONEP75=1.75D0,TWOP25=2.25D0,TWOPT5=2.5D0,THRPT5=3.5D0, &
                 FORPT5=4.5D0,F15=15.0D0)
!
! Begin:
      PI34=PI_VAL**PT75
      ROOT2=DSQRT(TWO)
      ROOT3=DSQRT(THREE)
      FCON=TWO*ROOT2/(PI34*DSQRT(F15))
!
      IF(ICODE.EQ.0) then
!
! Un-normalize the normalized primitives.
         do I=1,BasisIN%Nshells
         do J=BasisIN%shell(I)%EXPBGN,BasisIN%shell(I)%EXPEND
         E=BasisIN%gaussian(J)%exp+BasisIN%gaussian(J)%exp
         IF(BasisIN%shell(I)%SHLTYP(1:2).eq.'D ')then
           BasisIN%gaussian(J)%CONTRC=BasisIN%gaussian(J)%CONTRC*ROOT3*PI34/(TWO*E**ONEP75)
         else IF(BasisIN%shell(I)%SHLTYP(1:2).eq.'F ')then
           BasisIN%gaussian(J)%CONTRC=BasisIN%gaussian(J)%CONTRC/(FCON*E**TWOP25)
         else IF(BasisIN%shell(I)%SHLTYP(1:2).eq.'S ')then
           BasisIN%gaussian(J)%CONTRC=BasisIN%gaussian(J)%CONTRC*PI34/E**PT75
         else IF(BasisIN%shell(I)%SHLTYP(1:2).eq.'P ')then
           BasisIN%gaussian(J)%CONTRC=BasisIN%gaussian(J)%CONTRC*PI34/(ROOT2*E**ONEP25)
         end if
         end do !J
         end do !I
         return
      end if
!
! Normalize scaled Gaussian functions.
! Ensure contracted AO's are normalized.
      do I=1,BasisIN%Nshells
      OVRLAP(1:4)=ZERO
!
      do J=BasisIN%shell(I)%EXPBGN,BasisIN%shell(I)%EXPEND
      do K=BasisIN%shell(I)%EXPBGN,BasisIN%shell(I)%EXPEND
      AXB=FOUR*BasisIN%gaussian(J)%exp*BasisIN%gaussian(K)%exp
      APB=BasisIN%gaussian(J)%exp+BasisIN%gaussian(K)%exp
      CJK=BasisIN%gaussian(J)%CONTRC*BasisIN%gaussian(K)%CONTRC
!
      IF(BasisIN%shell(I)%SHLTYP(1:2).eq.'S ')then
        OVRLAP(1)=OVRLAP(1)+CJK*(AXB**PT75)/(APB**ONEPT5)
        OVRLAP(2)=ZERO
      else IF(BasisIN%shell(I)%SHLTYP(1:2).eq.'P ')then
        OVRLAP(1)=ZERO
        OVRLAP(2)=OVRLAP(2)+BasisIN%gaussian(J)%CONTRC*BasisIN%gaussian(K)%CONTRC*(AXB**ONEP25)/(APB**TWOPT5)
      else IF(BasisIN%shell(I)%SHLTYP(1:2).eq.'D ')then
        OVRLAP(3)=OVRLAP(3)+CJK*(AXB**ONEP75)/(APB**THRPT5)
      else IF(BasisIN%shell(I)%SHLTYP(1:2).eq.'F ')then
        OVRLAP(4)=OVRLAP(4)+CJK*(AXB**TWOP25)/(APB**FORPT5)
      else
        write(UNIout,*)'error_NORMALIZE_BASIS> elligal SHLTYP',BasisIN%shell(I)%SHLTYP
      end if
      end do ! K
      end do ! J
!
      do K=1,4
      IF(OVRLAP(K).GT.TENM8)then
        OVRLAP(K)=ONE/DSQRT(OVRLAP(K))
      else
        OVRLAP(K)=ZERO
      end if
      end do ! K
!
! Normalize the AO's
      do J=BasisIN%shell(I)%EXPBGN,BasisIN%shell(I)%EXPEND
      IF(BasisIN%shell(I)%SHLTYP(1:2).eq.'S ')then
        BasisIN%gaussian(J)%CONTRC=BasisIN%gaussian(J)%CONTRC*OVRLAP(1)
      else IF(BasisIN%shell(I)%SHLTYP(1:2).eq.'P ')then
        BasisIN%gaussian(J)%CONTRC=BasisIN%gaussian(J)%CONTRC*OVRLAP(2)
      else IF(BasisIN%shell(I)%SHLTYP(1:2).eq.'D ')then
        BasisIN%gaussian(J)%CONTRC=BasisIN%gaussian(J)%CONTRC*OVRLAP(3)
      else IF(BasisIN%shell(I)%SHLTYP(1:2).eq.'F ')then
        BasisIN%gaussian(J)%CONTRC=BasisIN%gaussian(J)%CONTRC*OVRLAP(4)
      end if
      end do ! J
      end do ! I
!
! Normalize the primitives
      do I=1,BasisIN%Nshells
      do J=BasisIN%shell(I)%EXPBGN,BasisIN%shell(I)%EXPEND
      E=TWO*BasisIN%gaussian(J)%exp
      IF(BasisIN%shell(I)%SHLTYP(1:2).eq.'S ')then
        BasisIN%gaussian(J)%CONTRC=BasisIN%gaussian(J)%CONTRC*E**PT75/PI34
      else IF(BasisIN%shell(I)%SHLTYP(1:2).eq.'P ')then
        BasisIN%gaussian(J)%CONTRC=BasisIN%gaussian(J)%CONTRC*ROOT2*E**ONEP25/PI34
      else IF(BasisIN%shell(I)%SHLTYP(1:2).eq.'D ')then
        BasisIN%gaussian(J)%CONTRC=BasisIN%gaussian(J)%CONTRC*TWO*E**ONEP75/(ROOT3*PI34)
      else IF(BasisIN%shell(I)%SHLTYP(1:2).eq.'F ')then
        BasisIN%gaussian(J)%CONTRC=BasisIN%gaussian(J)%CONTRC*FCON*(E**TWOP25)
      end if
      end do !J
      end do !I
!
! Print normalized coefficients?
! End of routine NORMALIZE_BASIS
      return
      end subroutine NORMALIZE_BASIS
      subroutine PRT_BASIS (BasisIN)
!***********************************************************************
!     Date last modified: November 29, 1996                Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: Print the user-defined basis set.                   *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE type_elements

      implicit none
!
! Input:
      type (basis_set) BasisIN
!
! Local scalars:
      integer IPRIM,Iatmshl,Ishell,IA,ICENT,IOLD,Igauss,frstAO,lastAO,lenstr
      double precision CD,CF,CP,CS
      character(len=12) :: CATOM
      character(len=4) :: COREshl
      logical :: LFirst_shell,LERR,SEP
!
! Begin:
      lenstr=len_trim(BasisIN%name)
      write(UNIout,1000)BasisIN%name(1:lenstr),BasisIN%NDORBS,BasisIN%NFORBS
 1000 FORMAT('Basis set (normalized) for this calculation (exponents are scaled:)',1X,a,' (',i1,'D/',i2,'F)'/)
      write(UNIout,1050)
      write(UNIout,1060)
      write(UNIout,1050)
      write(UNIout,1070)
      write(UNIout,1050)
!
      IOLD=0
      lastAO=0
      SEP=.FALSE.
      do Ishell=1,BasisIN%Nshells
      LFirst_shell=.true.
      COREshl='    '
      if(BasisIN%shell(Ishell)%LCOREshl)then
        COREshl='CORE'
      end if
      do Iatmshl=BasisIN%shell(Ishell)%frstSHL,BasisIN%shell(Ishell)%lastSHL
        ICENT=BasisIN%atmshl(Iatmshl)%ATMLST
        IA=CARTESIAN(ICENT)%Atomic_number
        IF(IA.NE.IOLD)then ! Only write a separation line in different atom
          IOLD=IA
          IF(SEP)write(UNIout,1170)
          SEP=.TRUE.
          IF(IA.GT.0)then
            CATOM=Element_names(IA)
          else IF(IABS(IA).EQ.999)then
            CATOM=Floating
          else IF(IA.LT.0)then
            CATOM=Element_names(IABS(IA))
            CATOM(12:12)='*'
          end if
          write(UNIout,1080)CATOM
        end if ! IA.NE.IOLD

        if(LFirst_shell)write(UNIout,'(18x,a,i5,2x,a)')'Shell ',Ishell,COREshl
        frstAO=lastAO+1
        IF(BasisIN%shell(Ishell)%Xtype.EQ.0)then ! S Shell.
        lastAO=frstAO
        write(UNIout,1090)frstAO,BasisIN%shell(Ishell)%SHLTLB,BasisIN%shell(Ishell)%SHLSCL
        else IF(BasisIN%shell(Ishell)%Xtype.EQ.1)then !  P Shell.
        lastAO=lastAO+3
        write(UNIout,1100)frstAO,lastAO,BasisIN%shell(Ishell)%SHLTLB,BasisIN%shell(Ishell)%SHLSCL
        else IF(BasisIN%shell(Ishell)%Xtype.EQ.2)then ! D Shell.
        lastAO=lastAO+BasisIN%NDORBS
        write(UNIout,1100)frstAO,lastAO,BasisIN%shell(Ishell)%SHLTLB,BasisIN%shell(Ishell)%SHLSCL
        else IF(BasisIN%shell(Ishell)%Xtype.EQ.3)then ! F Shell.
        lastAO=lastAO+BasisIN%NFORBS
        write(UNIout,1100)frstAO,lastAO,BasisIN%shell(Ishell)%SHLTLB,BasisIN%shell(Ishell)%SHLSCL
        end if

        CS=ZERO
        CP=ZERO
        CD=ZERO
        CF=ZERO
        do Igauss=BasisIN%shell(Ishell)%EXPBGN,BasisIN%shell(Ishell)%EXPEND
          LERR=.FALSE.
          IF(BasisIN%shell(Ishell)%Xtype.EQ.0)then
            CS=BasisIN%gaussian(Igauss)%CONTRC
            IF(CS.EQ.ZERO)LERR=.TRUE.
          else IF(BasisIN%shell(Ishell)%Xtype.EQ.1)then
            CP=BasisIN%gaussian(Igauss)%CONTRC
            IF(CP.EQ.ZERO)LERR=.TRUE.
          else IF(BasisIN%shell(Ishell)%Xtype.EQ.2)then
            CD=BasisIN%gaussian(Igauss)%CONTRC
            IF(CD.EQ.ZERO)LERR=.TRUE.
          else IF(BasisIN%shell(Ishell)%Xtype.EQ.3)then
            CF=BasisIN%gaussian(Igauss)%CONTRC
            IF(CF.EQ.ZERO)LERR=.TRUE.
          end if ! BasisIN%shell(Ishell)%Xtype
!
          write(UNIout,1110)BasisIN%gaussian(Igauss)%exp,BasisIN%gaussian(Igauss)%CONTRC
          IF(BasisIN%gaussian(Igauss)%exp.LE.ZERO)then ! Scale factor or exponent is zero or negative.
            write(UNIout,'(A)')'ERROR> PRT_BASIS: scale factor or exponent is zero or negative'
            stop 'scale factor or exponent is zero or negative'
          end if ! BasisIN%gaussian(Igauss)%exp
!
          IF(LERR)then
! Found zero contraction coefficient in a shell.
          write(UNIout,'(3A,I4,A,I4,A)')'ERROR> PRT_BASIS: ',BasisIN%shell(Ishell)%Xtype, &
                                        ' contraction coefficient is zero for shell ', &
                                            Ishell,' (CATOM ',BasisIN%atmshl(Ishell)%ATMLST,')'
          stop ' orbital contraction coefficient is zero'
          end if ! LERR
        end do ! Igauss
        LFirst_shell=.false.
      end do ! Iatmshl
      end do ! Ishell
!
      write(UNIout,1050)
      write(UNIout,'(a,i10/)')'Total number of basis functions: ',BasisIN%Nbasis
!
! End of routine PRT_BASIS
      return
!
 1050 FORMAT(1X,87('*'))
 1060 FORMAT(15X,'ATOMIC ORBITAL',24X,'GAUSSIAN FUNCTIONS')
 1070 FORMAT(1X,'ATOM',7X,'NO.',4X,'TYPE',12X,'SCALE FACTOR',4X,'EXPONENT',8X,'COEFF')
 1080 FORMAT(1X,A12)
 1090 FORMAT(5X,I5,8X,A,F11.5)
 1100 FORMAT(5X,I5,'-',I5,2X,A,F11.5)
 1110 FORMAT(48X,1PE14.7,E15.7)
 1170 FORMAT(1X,87('-'))
!
      end subroutine PRT_BASIS
      subroutine BLD_AOtoATOM (BasisIN)
!***********************************************************************
!     Date last modified: February 24, 1992                Version 1.0 *
!     Author: R. A. Poirier                                            *
!     Description: Initialize general basis set arrays.                *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants

      implicit none
!
! Input type:
      type (basis_set) BasisIN
!
! Local scalars:
      integer Iao,Iatom,Iatmshl,Ibasis,Ifrst,Ilast,Irange,Ishell,Istart,Iend
!
! Begin:

      if(.not.associated(BasisIN%AOtoATOM))then
        allocate (BasisIN%AOtoATOM(1:BasisIN%Nbasis))
      else
        if(size(BasisIN%AOtoATOM).ne.BasisIN%Nbasis)then
          deallocate (BasisIN%AOtoATOM)
          allocate (BasisIN%AOtoATOM(1:BasisIN%Nbasis))
        end if
      end if

      BasisIN%AOtoATOM(1:BasisIN%Nbasis)=0
!
      Ibasis=0
      do Ishell=1,BasisIN%Nshells
        Istart=BasisIN%shell(Ishell)%Xstart
        IEND=BasisIN%shell(Ishell)%XEND
        Irange=IEND-Istart+1
        Ifrst=BasisIN%shell(Ishell)%frstSHL
        Ilast=BasisIN%shell(Ishell)%lastSHL
        do Iatmshl=Ifrst,Ilast
        Iatom=BasisIN%atmshl(Iatmshl)%ATMLST
        do Iao=1,Irange
          Ibasis=Ibasis+1
          BasisIN%AOtoATOM(Ibasis)=Iatom
        end do ! Iao
        end do ! Iatmshl
      end do ! Ishell
!
      IF(BasisIN%Nbasis.NE.Ibasis)then
! Nbasis and number of basis functions in table do not match.
        write(UNIout,'(2(A,I4))')'ERROR> BLD_AOtoATOM: Nbasis = ',BasisIN%Nbasis,',   Ibasis = ',Ibasis
        stop 'ERROR in BLD_AOtoATOM'
      end if
!
! End of routine BLD_AOtoATOM
      return
      end subroutine BLD_AOtoATOM
      subroutine BLD_CC_BY_AO (BasisIN)
!***********************************************************************
!     Date last modified: March 5, 1996                    Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: Expand the contraction coefficients to AOs          *
!                  Including normalization factors                     *
!***********************************************************************
! Modules:
      USE program_constants

      implicit none
!
! Input type:
      type (basis_set) BasisIN
!
! Local scalars:
      integer Igauss,Ishell,LAMAX
      double precision ROOT3,ROOT5
!
! Work array:
      double precision, dimension(:), allocatable :: CCBYAOw
!
! Parameters:
      double precision FIVE
      parameter (FIVE=5.0D0)
!
! Begin:
      if(.not.associated(BasisIN%CCPNTR))then
        allocate (BasisIN%CCPNTR(BasisIN%Nshells))
      else
        deallocate (BasisIN%CCPNTR)
        allocate (BasisIN%CCPNTR(BasisIN%Nshells))
      end if

! Work array
      allocate (CCBYAOw(BasisIN%Nprimitives*BasisIN%Nbasis))

      ROOT3=DSQRT(THREE)
      ROOT5=DSQRT(FIVE)
!
! Obtain information for Ishell.
      BasisIN%NCCOEFFS=0
      do Ishell=1,BasisIN%Nshells
      LAMAX=BasisIN%shell(Ishell)%Xtype+1
      BasisIN%CCPNTR(Ishell)=BasisIN%NCCOEFFS+1
      do Igauss=BasisIN%shell(Ishell)%EXPBGN,BasisIN%shell(Ishell)%EXPEND
      select case (LAMAX)
      case (1)                   ! S
        BasisIN%NCCOEFFS=BasisIN%NCCOEFFS+1
        CCBYAOw(BasisIN%NCCOEFFS)=BasisIN%gaussian(Igauss)%CONTRC
      case (2)            ! P
        BasisIN%NCCOEFFS=BasisIN%NCCOEFFS+1
        CCBYAOw(BasisIN%NCCOEFFS)=BasisIN%gaussian(Igauss)%CONTRC
        BasisIN%NCCOEFFS=BasisIN%NCCOEFFS+1
        CCBYAOw(BasisIN%NCCOEFFS)=BasisIN%gaussian(Igauss)%CONTRC
        BasisIN%NCCOEFFS=BasisIN%NCCOEFFS+1
        CCBYAOw(BasisIN%NCCOEFFS)=BasisIN%gaussian(Igauss)%CONTRC
      case (3)            ! D
        BasisIN%NCCOEFFS=BasisIN%NCCOEFFS+1
        CCBYAOw(BasisIN%NCCOEFFS)=BasisIN%gaussian(Igauss)%CONTRC
        BasisIN%NCCOEFFS=BasisIN%NCCOEFFS+1
        CCBYAOw(BasisIN%NCCOEFFS)=BasisIN%gaussian(Igauss)%CONTRC
        BasisIN%NCCOEFFS=BasisIN%NCCOEFFS+1
        CCBYAOw(BasisIN%NCCOEFFS)=BasisIN%gaussian(Igauss)%CONTRC
        BasisIN%NCCOEFFS=BasisIN%NCCOEFFS+1
        CCBYAOw(BasisIN%NCCOEFFS)=BasisIN%gaussian(Igauss)%CONTRC*ROOT3
        BasisIN%NCCOEFFS=BasisIN%NCCOEFFS+1
        CCBYAOw(BasisIN%NCCOEFFS)=BasisIN%gaussian(Igauss)%CONTRC*ROOT3
        BasisIN%NCCOEFFS=BasisIN%NCCOEFFS+1
        CCBYAOw(BasisIN%NCCOEFFS)=BasisIN%gaussian(Igauss)%CONTRC*ROOT3
      case (4)             ! F
        BasisIN%NCCOEFFS=BasisIN%NCCOEFFS+1
        CCBYAOw(BasisIN%NCCOEFFS)=BasisIN%gaussian(Igauss)%CONTRC
        BasisIN%NCCOEFFS=BasisIN%NCCOEFFS+1
        CCBYAOw(BasisIN%NCCOEFFS)=BasisIN%gaussian(Igauss)%CONTRC
        BasisIN%NCCOEFFS=BasisIN%NCCOEFFS+1
        CCBYAOw(BasisIN%NCCOEFFS)=BasisIN%gaussian(Igauss)%CONTRC
        BasisIN%NCCOEFFS=BasisIN%NCCOEFFS+1
        CCBYAOw(BasisIN%NCCOEFFS)=BasisIN%gaussian(Igauss)%CONTRC*ROOT5
        BasisIN%NCCOEFFS=BasisIN%NCCOEFFS+1
        CCBYAOw(BasisIN%NCCOEFFS)=BasisIN%gaussian(Igauss)%CONTRC*ROOT5
        BasisIN%NCCOEFFS=BasisIN%NCCOEFFS+1
        CCBYAOw(BasisIN%NCCOEFFS)=BasisIN%gaussian(Igauss)%CONTRC*ROOT5
        BasisIN%NCCOEFFS=BasisIN%NCCOEFFS+1
        CCBYAOw(BasisIN%NCCOEFFS)=BasisIN%gaussian(Igauss)%CONTRC*ROOT5
        BasisIN%NCCOEFFS=BasisIN%NCCOEFFS+1
        CCBYAOw(BasisIN%NCCOEFFS)=BasisIN%gaussian(Igauss)%CONTRC*ROOT5
        BasisIN%NCCOEFFS=BasisIN%NCCOEFFS+1
        CCBYAOw(BasisIN%NCCOEFFS)=BasisIN%gaussian(Igauss)%CONTRC*ROOT5
        BasisIN%NCCOEFFS=BasisIN%NCCOEFFS+1
        CCBYAOw(BasisIN%NCCOEFFS)=BasisIN%gaussian(Igauss)%CONTRC*ROOT3
      end select
      end do ! Igauss
      end do ! Ishell

      if(.not.associated(BasisIN%CCBYAO))then
        allocate (BasisIN%CCBYAO(BasisIN%NCCOEFFS))
      else
        deallocate (BasisIN%CCBYAO)
        allocate (BasisIN%CCBYAO(BasisIN%NCCOEFFS))
      end if

      BasisIN%CCBYAO(1:BasisIN%NCCOEFFS)=CCBYAOw(1:BasisIN%NCCOEFFS)

      deallocate (CCBYAOw)
!
! End of routine BLD_CC_BY_AO
      return
      end subroutine BLD_CC_BY_AO
      subroutine BLD_AOtype (BasisIN)
!***********************************************************************
!     Date last modified: February 24, 1992                Version 1.0 *
!     Author: R. A. Poirier                                            *
!     Description: Initialize general basis set arrays.                *
!***********************************************************************
! Modules:
      USE program_constants

      implicit none
!
! Input type:
      type (basis_set) BasisIN
!
! Local scalars:
      integer Iatmshl,Ishell,J
      character (len=8) :: Type
!
! Begin:
!
      if(associated(BasisIN%AOtype))then
        deallocate (BasisIN%AOtype)
      end if
      allocate (BasisIN%AOtype(BasisIN%Nbasis))

      J=0
      do Ishell=1,BasisIN%Nshells
      Type=BasisIN%shell(Ishell)%shltlb
      Type(3:8)=' '
      do Iatmshl=BasisIN%shell(Ishell)%frstSHL,BasisIN%shell(Ishell)%lastSHL
      if(BasisIN%shell(Ishell)%Xtype.eq.0)then
        J=J+1
        BasisIN%AOtype(J)=Type
      else If(BasisIN%shell(Ishell)%Xtype.eq.1)then
        J=J+1
        Type(3:3)='x'
        BasisIN%AOtype(J)=Type
        J=J+1
        Type(3:3)='y'
        BasisIN%AOtype(J)=Type
        J=J+1
        Type(3:3)='z'
        BasisIN%AOtype(J)=Type
      else If(BasisIN%shell(Ishell)%Xtype.eq.2.and.BasisIN%NDORBS.EQ.6)then
        J=J+1
        Type(3:5)='xx '
        BasisIN%AOtype(J)=Type
        J=J+1
        Type(3:5)='yy '
        BasisIN%AOtype(J)=Type
        J=J+1
        Type(3:5)='zz '
        BasisIN%AOtype(J)=Type
        J=J+1
        Type(3:5)='xy '
        BasisIN%AOtype(J)=Type
        J=J+1
        Type(3:5)='xz '
        BasisIN%AOtype(J)=Type
        J=J+1
        Type(3:5)='yz '
        BasisIN%AOtype(J)=Type
      else If(BasisIN%shell(Ishell)%Xtype.eq.2.and.BasisIN%NDORBS.EQ.5)then
        J=J+1
        BasisIN%AOtype(J)='D0      '
        J=J+1
        BasisIN%AOtype(J)='D1+     '
        J=J+1
        BasisIN%AOtype(J)='D1-     '
        J=J+1
        BasisIN%AOtype(J)='D2+     '
        J=J+1
        BasisIN%AOtype(J)='D2-     '
      else If(BasisIN%shell(Ishell)%Xtype.eq.3.and.BasisIN%NFORBS.EQ.10)then
        J=J+1
        Type(3:5)='xxx'
        BasisIN%AOtype(J)=Type
        J=J+1
        Type(3:5)='yyy'
        BasisIN%AOtype(J)=Type
        J=J+1
        Type(3:5)='zzz'
        BasisIN%AOtype(J)=Type
        J=J+1
        Type(3:5)='xyy'
        BasisIN%AOtype(J)=Type
        J=J+1
        Type(3:5)='xxy'
        BasisIN%AOtype(J)=Type
        J=J+1
        Type(3:5)='xxz'
        BasisIN%AOtype(J)=Type
        J=J+1
        Type(3:5)='xzz'
        BasisIN%AOtype(J)=Type
        J=J+1
        Type(3:5)='yzz'
        BasisIN%AOtype(J)=Type
        J=J+1
        Type(3:5)='yyz'
        BasisIN%AOtype(J)=Type
        J=J+1
        Type(3:5)='xyz'
        BasisIN%AOtype(J)=Type
      else If(BasisIN%shell(Ishell)%Xtype.eq.3.and.BasisIN%NFORBS.EQ.7)then
        J=J+1
        BasisIN%AOtype(J)='F0      '
        J=J+1
        BasisIN%AOtype(J)='F1+     '
        J=J+1
        BasisIN%AOtype(J)='F1-     '
        J=J+1
        BasisIN%AOtype(J)='F2+     '
        J=J+1
        BasisIN%AOtype(J)='F2-     '
        J=J+1
        BasisIN%AOtype(J)='F3+     '
        J=J+1
        BasisIN%AOtype(J)='F3-     '
      end if ! Xtype
      end do ! Iatmshl
      end do ! Ishell
!
      BasisIN%NAOtype=J
!
! End of routine BLD_AOtype
      return
      end subroutine BLD_AOtype
      subroutine MIN_GEXP (BasisIN)
!***********************************************************************
!     Date last modified: June 11, 1997                    Version 1.0 *
!     Author: James Xidos                                              *
!     Description: Create matrix min[Gi*Gj/(Gi+Gj)]                    *
!***********************************************************************
! Modules:

      implicit none
!
! Input type:
      type (basis_set) BasisIN
!
! Local scalars:
      integer IGBGN,IGEND,JGBGN,JGEND,Igauss,Jgauss,Ishell,Jshell
      double precision EABP,MGEXP
!
! Begin:
!
      if(associated(BasisIN%minEABP))then
        deallocate (BasisIN%minEABP)
      end if
      allocate (BasisIN%minEABP(BasisIN%Nshells,BasisIN%Nshells))

      do Ishell=1,BasisIN%NshellS
        IGBGN=BasisIN%shell(Ishell)%EXPBGN
        IGEND=BasisIN%shell(Ishell)%EXPEND
!
      do Jshell=1,Ishell
        JGBGN=BasisIN%shell(Jshell)%EXPBGN
        JGEND=BasisIN%shell(Jshell)%EXPEND

        MGEXP=99999.0D0
        do Igauss=IGBGN,IGEND
        do Jgauss=JGBGN,JGEND
          EABP=BasisIN%gaussian(Igauss)%exp*BasisIN%gaussian(Jgauss)%exp/ &
              (BasisIN%gaussian(Igauss)%exp+BasisIN%gaussian(Jgauss)%exp)
          IF(EABP.LT.MGEXP)MGEXP=EABP
        end do ! Igauss
        end do ! Jgauss

        BasisIN%minEABP(Ishell,Jshell)=MGEXP
        BasisIN%minEABP(Jshell,Ishell)=MGEXP

      end do ! Jshell
      end do ! Ishell
!
! End of routine MIN_GEXP
      return
      end subroutine MIN_GEXP
      subroutine BLD_LCOREshl (BasisIN)
!***********************************************************************
!     Date last modified: February 24, 1992                Version 1.0 *
!     Author: R. A. Poirier                                            *
!     Description: If a shell belong to a core LCOREshl(Ishell)=.true. *
!     WARNING: It is assumed that the shells are in the correct order! *
!     This routine requires improvements!                              *
!***********************************************************************
! Modules:
      USE type_elements

      implicit none
!
! Input type:
      type (basis_set) BasisIN
!
! Local scalars:
      integer Ishell,IZ
!
! Local work arrays:
      integer, allocatable :: Nshell_done(:)
      logical, allocatable :: Latom_done(:)
!
! Begin:

      BasisIN%shell(1:BasisIN%Nshells)%LCOREshl=.false.

      allocate (Latom_done(1:Nelements), Nshell_done(1:Nelements))
      Latom_done(1:Nelements)=.false.
      Nshell_done(1:Nelements)=0
!
      do Ishell=1,BasisIN%Nshells
        IZ=BasisIN%shell(Ishell)%shlatm
        if(Latom_done(IZ))cycle
      IF(IZ.GE.3.and.IZ.LE.10)then ! Li to Ne
        if(Nshell_done(IZ).lt.1)then
          BasisIN%shell(Ishell)%LCOREshl=.true.
          Latom_done(IZ)=.true.
        end if
        Nshell_done(IZ)=Nshell_done(IZ)+1
      else IF(IZ.GE.11.and.IZ.LE.18)then ! Na to Ar
        if(Nshell_done(IZ).lt.3)then
          BasisIN%shell(Ishell)%LCOREshl=.true.
        else
          Latom_done(IZ)=.true.
        end if
        Nshell_done(IZ)=Nshell_done(IZ)+1
      else IF(IZ.GE.19.and.IZ.LE.30)then ! K to Zn
        if(Nshell_done(IZ).lt.5)then
          BasisIN%shell(Ishell)%LCOREshl=.true.
        else
          Latom_done(IZ)=.true.
        end if
      else IF(IZ.GE.31.and.IZ.LE.36)then ! Ga to Kr
        if(Nshell_done(IZ).lt.6)then
          BasisIN%shell(Ishell)%LCOREshl=.true.
        else
          Latom_done(IZ)=.true.
        end if
      else IF(IZ.GE.37.and.IZ.LE.48)then ! Rb to Cd
        if(Nshell_done(IZ).lt.8)then
          BasisIN%shell(Ishell)%LCOREshl=.true.
        else
          Latom_done(IZ)=.true.
        end if
      else IF(IZ.GE.49.and.IZ.LE.54)then ! In to Xe
        if(Nshell_done(IZ).lt.9)then
          BasisIN%shell(Ishell)%LCOREshl=.true.
        else
          Latom_done(IZ)=.true.
        end if
        Nshell_done(IZ)=Nshell_done(IZ)+1
      end if
      end do ! Ishell

      deallocate (Latom_done, Nshell_done)
!
! End of routine BLD_LCOREshl
      return
      end subroutine BLD_LCOREshl

      end MODULE type_basis_set
