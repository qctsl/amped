      subroutine BLD_pVQZ2e_BASIS
!*****************************************************************************************************************
!     Date last modified: May 13 2011                                                               Version 2.0  *
!     Author: V. R. Grandy, R. A. Poirier                                                                        *
!     Description: Define a basis set.                                                                           *
!*****************************************************************************************************************
! Modules:
      USE MENU_BLD_basis

      implicit none
!
! Local scalars:
      integer :: Znum
!
! Begin:
      Type(1:30)(1:16)=' '
! 1) Loop over all the elements in the molecule
      do IELEM=1,NELEMTS
!       ELEMENT=ELMLIST(IELEM)
        Znum=CARTESIAN(1)%Atomic_number
        call SET_BASIS_pVQZ2e

!        write(6,'(a,i4)')' Znum: ',Znum
! Round the value to 3 decimal places because we sometimes read in MOs from GAMESS and quantum package
        ScaleF(1:NshellIN)= dble(dnint((0.275246D0 + 0.438437D0*dble(Znum))*1.0D03))/1.0D03
!        ScaleF(1)=0.528065091596639*Znum-0.0571108546218522
!        ScaleF(2)=0.558526497478992*Znum-0.125498994957985
!        ScaleF(3)=0.600245491596639*Znum-0.210700568907567
!        ScaleF(4)=0.643009711484594*Znum-0.289143489635855
!        ScaleF(5)=0.095919581232493*Znum-0.0869038719887962
      
        IGstart=1
        do Ishell=1,NshellIN
          IGend=IGstart+Ngauss(Ishell)-1
            do Igauss=IGstart,IGend
              Gexp(Igauss)=Gexp(Igauss)*ScaleF(Ishell)*ScaleF(Ishell)
! Round the value to 7 decimal places because we sometimes read in MOs from GAMESS and quantum package
              Gexp(Igauss)=dble(dnint(Gexp(Igauss)*1.0D07))/1.0D07
            end do
            call SAVE_shell
          IGstart=IGend+1
        end do ! Ishell
      end do ! IELEM
!
! End of routine BLD_pVQZ2e_BASIS
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine SET_BASIS_pVQZ2e
!*****************************************************************************************************************
!     Date last modified: May 13, 2011                                                              Version 2.0  *
!     Author: V. R. Grandy, R.A. Poirier                                                                         *
!     Description: Set basis set defaults (can only be called after a molecule has been defined.                 *
!*****************************************************************************************************************
! Modules:

      implicit none
!
! Begin:
      Igauss=0
      Ishell=0
!
      NshellIN=12
      Ishell=Ishell+1
      Type(Ishell) = '1S'
      ScaleF(Ishell) = 1.00
      Ngauss(Ishell) = 4
      Igauss=Igauss+1
      Gexp(Igauss) = 528.5000000D0
        C1(Igauss) =   0.0009400D0
      Igauss=Igauss+1
      Gexp(Igauss) = 79.3100000D0
        C1(Igauss) =  0.0072140D0
      Igauss=Igauss+1
      Gexp(Igauss) = 18.0500000D0
        C1(Igauss) =  0.0359750D0
      Igauss=Igauss+1
      Gexp(Igauss) = 5.0850000D0
        C1(Igauss) = 0.1277820D0
      Ishell=Ishell+1
      Type(Ishell) = '2S'
      ScaleF(Ishell) = 1.00
      Ngauss(Ishell) = 1
      Igauss=Igauss+1
      Gexp(Igauss) = 1.6090000D0
        C1(Igauss) = 1.0000000D0
      Ishell=Ishell+1
      Type(Ishell) = '3S'
      ScaleF(Ishell) = 1.00
      Ngauss(Ishell) = 1
      Igauss=Igauss+1
      Gexp(Igauss) = 0.5363000D0
        C1(Igauss) = 1.0000000D0
      Ishell=Ishell+1
      Type(Ishell) = '4S'
      ScaleF(Ishell) = 1.00
      Ngauss(Ishell) = 1
      Igauss=Igauss+1
      Gexp(Igauss) = 0.1833000D0
        C1(Igauss) = 1.0000000D0
      Ishell=Ishell+1
      Type(Ishell) = '5S'
      ScaleF(Ishell) = 1.00
      Ngauss(Ishell) = 1
      Igauss=Igauss+1
      Gexp(Igauss) = 0.0481900D0
        C1(Igauss) = 1.0000000D0
      Ishell=Ishell+1
      Type(Ishell) = '2P'
      ScaleF(Ishell) = 1.00
      Ngauss(Ishell) = 1
      Igauss=Igauss+1
      Gexp(Igauss) = 5.9940000D0
        C1(Igauss) = 1.0000000D0
      Ishell=Ishell+1
      Type(Ishell) = '3P'
      ScaleF(Ishell) = 1.00
      Ngauss(Ishell) = 1
      Igauss=Igauss+1
      Gexp(Igauss) = 1.7450000D0
        C1(Igauss) = 1.0000000D0
      Ishell=Ishell+1
      Type(Ishell) = '4P'
      ScaleF(Ishell) = 1.00
      Ngauss(Ishell) = 1
      Igauss=Igauss+1
      Gexp(Igauss) = 0.5600000D0
        C1(Igauss) = 1.0000000D0
      Ishell=Ishell+1
      Type(Ishell) = '5P'
      ScaleF(Ishell) = 1.00
      Ngauss(Ishell) = 1
      Igauss=Igauss+1
      Gexp(Igauss) = 0.1626000D0
        C1(Igauss) = 1.0000000D0
      Ishell=Ishell+1
      Type(Ishell) = '3D'
      ScaleF(Ishell) = 1.00
      Ngauss(Ishell) = 1
      Igauss=Igauss+1
      Gexp(Igauss) = 4.2990000D0
        C1(Igauss) = 1.0000000D0
      Ishell=Ishell+1
      Type(Ishell) = '4D'
      ScaleF(Ishell) = 1.00
      Ngauss(Ishell) = 1
      Igauss=Igauss+1
      Gexp(Igauss) = 1.2230000D0
        C1(Igauss) = 1.0000000D0
      Ishell=Ishell+1
      Type(Ishell) = '5D'
      ScaleF(Ishell) = 1.00
      Ngauss(Ishell) = 1
      Igauss=Igauss+1
      Gexp(Igauss) = 0.3510000D0
        C1(Igauss) = 1.0000000D0
!
      return
      end subroutine SET_BASIS_pVQZ2e
! END CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine BLD_pVQZ2e_BASIS
