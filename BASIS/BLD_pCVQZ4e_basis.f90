      subroutine BLD_pCVQZ4e_BASIS
!********************************************************************************************************
!     Date last modified: July 27, 2022                                                    Version 2.0  *
!     Author: JWH                                                                                       *
!     Description: Define a basis set.                                                                  *
!*****************************************************************************************************************
! Modules:
      USE MENU_BLD_basis

      implicit none
!
! Local scalars:
      integer :: Znum
!
! Begin:
      Type(1:30)(1:16)=' '
! 1) Loop over all the elements in the molecule
      do IELEM=1,NELEMTS
!       ELEMENT=ELMLIST(IELEM)
        Znum=CARTESIAN(1)%Atomic_number
        call SET_BASIS_pCVQZ4e

! Round the value to 3 decimal places because we sometimes read in MOs from GAMESS and quantum package
        ScaleF(1:NshellIN)= dble(dnint((-0.291067D0 + 0.3233D0*dble(Znum))*1.0D03))/1.0D03
!      
        IGstart=1
        do Ishell=1,NshellIN
          IGend=IGstart+Ngauss(Ishell)-1
            do Igauss=IGstart,IGend
              Gexp(Igauss)=Gexp(Igauss)*ScaleF(Ishell)*ScaleF(Ishell)
! Round the value to 7 decimal places because we sometimes read in MOs from GAMESS and quantum package
              Gexp(Igauss)=dble(dnint(Gexp(Igauss)*1.0D07))/1.0D07
            end do
            call SAVE_shell
          IGstart=IGend+1
        end do ! Ishell
      end do ! IELEM
!
! End of routine BLD_pVQZ4e_BASIS
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine SET_BASIS_pCVQZ4e
!********************************************************************************************************
!     Date last modified: July 27, 2022                                                    Version 2.0  *
!     Author: JWH                                                                                       *
!     Description: Set basis set defaults (can only be called after a molecule has been defined.        *
!********************************************************************************************************
! Modules:

      implicit none
!
! Begin:
      Igauss=0
      Ishell=0
!
      NshellIN=23
      Ishell=Ishell+1
      Type(Ishell) = '1S'
      ScaleF(Ishell) = 1.00
      Ngauss(Ishell) = 12
      Igauss=Igauss+1
      Gexp(Igauss) = 14630.0000000D0
        C1(Igauss) = 0.0000920D0
      Igauss=Igauss+1
      Gexp(Igauss) = 2191.0000000D0
        C1(Igauss) = 0.0007130D0
      Igauss=Igauss+1
      Gexp(Igauss) = 498.2000000D0
        C1(Igauss) = 0.0037350D0
      Igauss=Igauss+1
      Gexp(Igauss) = 140.9000000D0
        C1(Igauss) = 0.0154680D0
      Igauss=Igauss+1
      Gexp(Igauss) = 45.8600000D0
        C1(Igauss) = 0.0528740D0
      Igauss=Igauss+1
      Gexp(Igauss) = 16.4700000D0
        C1(Igauss) = 0.1456940D0
      Igauss=Igauss+1
      Gexp(Igauss) = 6.3190000D0
        C1(Igauss) = 0.3026810D0
      Igauss=Igauss+1
      Gexp(Igauss) = 2.5350000D0
        C1(Igauss) = 0.4049360D0
      Igauss=Igauss+1
      Gexp(Igauss) = 1.0350000D0
        C1(Igauss) = 0.2223870D0
      Igauss=Igauss+1
      Gexp(Igauss) = 0.2528000D0
        C1(Igauss) = 0.0129120D0
      Igauss=Igauss+1
      Gexp(Igauss) = 0.1052000D0
        C1(Igauss) = -0.0046480D0
      Igauss=Igauss+1
      Gexp(Igauss) = 0.0426100D0
        C1(Igauss) = 0.0012280D0
      Ishell=Ishell+1
      Type(Ishell) = '2S'
      ScaleF(Ishell) = 1.00
      Ngauss(Ishell) = 12
      Igauss=Igauss+1
      Gexp(Igauss) = 14630.0000000D0
        C1(Igauss) = -0.0000170D0
      Igauss=Igauss+1
      Gexp(Igauss) = 2191.0000000D0
        C1(Igauss) = -0.0001300D0
      Igauss=Igauss+1
      Gexp(Igauss) = 498.2000000D0
        C1(Igauss) = -0.0006790D0
      Igauss=Igauss+1
      Gexp(Igauss) = 140.9000000D0
        C1(Igauss) = -0.0028570D0
      Igauss=Igauss+1
      Gexp(Igauss) = 45.8600000D0
        C1(Igauss) = -0.0098130D0
      Igauss=Igauss+1
      Gexp(Igauss) = 16.4700000D0
        C1(Igauss) = -0.0286090D0
      Igauss=Igauss+1
      Gexp(Igauss) = 6.3190000D0
        C1(Igauss) = -0.0637600D0
      Igauss=Igauss+1
      Gexp(Igauss) = 2.5350000D0
        C1(Igauss) = -0.1172310D0
      Igauss=Igauss+1
      Gexp(Igauss) = 1.0350000D0
        C1(Igauss) = -0.1212020D0
      Igauss=Igauss+1
      Gexp(Igauss) = 0.2528000D0
        C1(Igauss) = 0.2532290D0
      Igauss=Igauss+1
      Gexp(Igauss) = 0.1052000D0
        C1(Igauss) = 0.5818370D0
      Igauss=Igauss+1
      Gexp(Igauss) = 0.0426100D0
        C1(Igauss) = 0.2919540D0
      Ishell=Ishell+1
      Type(Ishell) = '3S'
      ScaleF(Ishell) = 1.00
      Ngauss(Ishell) = 1
      Igauss=Igauss+1
      Gexp(Igauss) = 21.7260000D0
        C1(Igauss) = 1.0000000D0
      Ishell=Ishell+1
      Type(Ishell) = '4S'
      ScaleF(Ishell) = 1.00
      Ngauss(Ishell) = 1
      Igauss=Igauss+1
      Gexp(Igauss) = 7.8660000D0
        C1(Igauss) = 1.0000000D0
      Ishell=Ishell+1
      Type(Ishell) = '5S'
      ScaleF(Ishell) = 1.00
      Ngauss(Ishell) = 1
      Igauss=Igauss+1
      Gexp(Igauss) = 2.8480000D0
        C1(Igauss) = 1.0000000D0
      Ishell=Ishell+1
      Type(Ishell) = '6S'
      ScaleF(Ishell) = 1.00
      Ngauss(Ishell) = 1
      Igauss=Igauss+1
      Gexp(Igauss) = 0.2528000D0
        C1(Igauss) = 1.0000000D0
      Ishell=Ishell+1
      Type(Ishell) = '7S'
      ScaleF(Ishell) = 1.00
      Ngauss(Ishell) = 1
      Igauss=Igauss+1
      Gexp(Igauss) = 0.1052000D0
        C1(Igauss) = 1.0000000D0
      Ishell=Ishell+1
      Type(Ishell) = '8S'
      ScaleF(Ishell) = 1.00
      Ngauss(Ishell) = 1
      Igauss=Igauss+1
      Gexp(Igauss) = 0.0426100D0
        C1(Igauss) = 1.0000000D0
      Ishell=Ishell+1
      Type(Ishell) = '9S'
      ScaleF(Ishell) = 1.00
      Ngauss(Ishell) = 1
      Igauss=Igauss+1
      Gexp(Igauss) = 0.0143900D0
        C1(Igauss) = 1.0000000D0
      Ishell=Ishell+1
      Type(Ishell) = '2P'
      ScaleF(Ishell) = 1.00
      Ngauss(Ishell) = 6
      Igauss=Igauss+1
      Gexp(Igauss) = 14.0300000D0
        C1(Igauss) = 0.0040990D0
      Igauss=Igauss+1
      Gexp(Igauss) = 3.1680000D0
        C1(Igauss) = 0.0256260D0
      Igauss=Igauss+1
      Gexp(Igauss) = 0.9024000D0
        C1(Igauss) = 0.1037680D0
      Igauss=Igauss+1
      Gexp(Igauss) = 0.3036000D0
        C1(Igauss) = 0.3115470D0
      Igauss=Igauss+1
      Gexp(Igauss) = 0.1130000D0
        C1(Igauss) = 0.4981280D0
      Igauss=Igauss+1
      Gexp(Igauss) = 0.0428600D0
        C1(Igauss) = 0.2588760D0
      Ishell=Ishell+1
      Type(Ishell) = '3P'
      ScaleF(Ishell) = 1.00
      Ngauss(Ishell) = 1
      Igauss=Igauss+1
      Gexp(Igauss) = 30.8980000D0
        C1(Igauss) = 1.0000000D0
      Ishell=Ishell+1
      Type(Ishell) = '4P'
      ScaleF(Ishell) = 1.00
      Ngauss(Ishell) = 1
      Igauss=Igauss+1
      Gexp(Igauss) = 10.3650000D0
        C1(Igauss) = 1.0000000D0
      Ishell=Ishell+1
      Type(Ishell) = '5P'
      ScaleF(Ishell) = 1.00
      Ngauss(Ishell) = 1
      Igauss=Igauss+1
      Gexp(Igauss) = 3.4770000D0
        C1(Igauss) = 1.0000000D0
      Ishell=Ishell+1
      Type(Ishell) = '6P'
      ScaleF(Ishell) = 1.00
      Ngauss(Ishell) = 1
      Igauss=Igauss+1
      Gexp(Igauss) = 0.3036000D0
        C1(Igauss) = 1.0000000D0
      Ishell=Ishell+1
      Type(Ishell) = '7P'
      ScaleF(Ishell) = 1.00
      Ngauss(Ishell) = 1
      Igauss=Igauss+1
      Gexp(Igauss) = 0.1130000D0
        C1(Igauss) = 1.0000000D0
      Ishell=Ishell+1
      Type(Ishell) = '8P'
      ScaleF(Ishell) = 1.00
      Ngauss(Ishell) = 1
      Igauss=Igauss+1
      Gexp(Igauss) = 0.0428600D0
        C1(Igauss) = 1.0000000D0
      Ishell=Ishell+1
      Type(Ishell) = '9P'
      ScaleF(Ishell) = 1.00
      Ngauss(Ishell) = 1
      Igauss=Igauss+1
      Gexp(Igauss) = 0.0065000D0
        C1(Igauss) = 1.0000000D0
      Ishell=Ishell+1
      Type(Ishell) = '3D'
      ScaleF(Ishell) = 1.00
      Ngauss(Ishell) = 1
      Igauss=Igauss+1
      Gexp(Igauss) = 30.3900000D0
        C1(Igauss) = 1.0000000D0
      Ishell=Ishell+1
      Type(Ishell) = '4D'
      ScaleF(Ishell) = 1.00
      Ngauss(Ishell) = 1
      Igauss=Igauss+1
      Gexp(Igauss) = 6.0920000D0
        C1(Igauss) = 1.0000000D0
      Ishell=Ishell+1
      Type(Ishell) = '5D'
      ScaleF(Ishell) = 1.00
      Ngauss(Ishell) = 1
      Igauss=Igauss+1
      Gexp(Igauss) = 1.0720000D0
        C1(Igauss) = 1.0000000D0
      Ishell=Ishell+1
      Type(Ishell) = '6D'
      ScaleF(Ishell) = 1.00
      Ngauss(Ishell) = 1
      Igauss=Igauss+1
      Gexp(Igauss) = 0.4410000D0
        C1(Igauss) = 1.0000000D0
      Ishell=Ishell+1
      Type(Ishell) = '7D'
      ScaleF(Ishell) = 1.00
      Ngauss(Ishell) = 1
      Igauss=Igauss+1
      Gexp(Igauss) = 0.1811000D0
        C1(Igauss) = 1.0000000D0
      Ishell=Ishell+1
      Type(Ishell) = '8D'
      ScaleF(Ishell) = 1.00
      Ngauss(Ishell) = 1
      Igauss=Igauss+1
      Gexp(Igauss) = 0.0554000D0
        C1(Igauss) = 1.0000000D0
!      Ishell=Ishell+1
!      Type(Ishell) = '4F'
!      ScaleF(Ishell) = 1.00
!      Ngauss(Ishell) = 1
!      Igauss=Igauss+1
!      Gexp(Igauss) = 12.4820000D0
!        C1(Igauss) = 1.0000000D0
!      Ishell=Ishell+1
!      Type(Ishell) = '5F'
!      ScaleF(Ishell) = 1.00
!      Ngauss(Ishell) = 1
!      Igauss=Igauss+1
!      Gexp(Igauss) = 0.4810000D0
!        C1(Igauss) = 1.0000000D0
!      Ishell=Ishell+1
!      Type(Ishell) = '6F'
!      ScaleF(Ishell) = 1.00
!      Ngauss(Ishell) = 1
!      Igauss=Igauss+1
!      Gexp(Igauss) = 0.2550000D0
!        C1(Igauss) = 1.0000000D0
!      Ishell=Ishell+1
!      Type(Ishell) = '7F'
!      ScaleF(Ishell) = 1.00
!      Ngauss(Ishell) = 1
!      Igauss=Igauss+1
!      Gexp(Igauss) = 0.0930000D0
!        C1(Igauss) = 1.0000000D0
!      Ishell=Ishell+1
!      Type(Ishell) = '5G'
!      ScaleF(Ishell) = 1.00
!      Ngauss(Ishell) = 1
!      Igauss=Igauss+1
!      Gexp(Igauss) = 0.4150000D0
!        C1(Igauss) = 1.0000000D0
!      Ishell=Ishell+1
!      Type(Ishell) = '6G'
!      ScaleF(Ishell) = 1.00
!      Ngauss(Ishell) = 1
!      Igauss=Igauss+1
!      Gexp(Igauss) = 0.1834000D0
!        C1(Igauss) = 1.0000000D0
!
      return
      end subroutine SET_BASIS_pCVQZ4e
! END CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine BLD_pCVQZ4e_BASIS
