      subroutine BLD_pVQZ7e_BASIS
!*****************************************************************************************************************
!     Date last modified: May 13, 2011                                                              Version 2.0  *
!     Author: V. R. Grandy, R. A. Poirier                                                                        *
!     Description: Define a basis set.                                                                           *
!*****************************************************************************************************************
! Modules:
      USE MENU_BLD_basis

      implicit none
!
! Local scalars:
      integer :: Znum
!
! Begin:
      Type(1:30)(1:16)=' '
! 1) Loop over all the elements in the molecule
      do IELEM=1,NELEMTS
!       ELEMENT=ELMLIST(IELEM)
        Znum=CARTESIAN(1)%Atomic_number
        call SET_BASIS_pVQZ7e

        write(6,'(a,i4)')' Znum: ',Znum
!       ScaleF(1)=0.2297D0*Znum+0.0558D0
!       ScaleF(2)=0.2542D0*Znum-0.1482D0
!       ScaleF(3)=0.4727D0*Znum-0.6575D0
!       ScaleF(4)=0.4004D0*Znum-0.2153D0
!       ScaleF(5)=3.6286D0*Znum-6.7413D0
      
        IGstart=1
        do Ishell=1,NshellIN
          IGend=IGstart+Ngauss(Ishell)-1
            do Igauss=IGstart,IGend
              Gexp(Igauss)=Gexp(Igauss)*ScaleF(Ishell)*ScaleF(Ishell)
            end do
            call SAVE_shell
          IGstart=IGend+1
        end do ! Ishell
      end do ! IELEM
!
! End of routine BLD_pVQZ7e_BASIS
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine SET_BASIS_pVQZ7e
!*****************************************************************************************************************
!     Date last modified: May 13, 2011                                                              Version 2.0  *
!     Author: V. R. Grandy, R.A. Poirier                                                                         *
!     Description: Set basis set defaults (can only be called after a molecule has been defined.                 *
!*****************************************************************************************************************
! Modules:

      implicit none
!
! Begin:
      NshellIN=11
      Type(1)='1S'
      Ngauss(1)=9
      Gexp(1)=45840.0000000
      C1(1)=0.0000920
      Gexp(2)=6868.0000000
      C1(2)=0.0007170
      Gexp(3)=1563.0000000
      C1(3)=0.0037490
      Gexp(4)=442.4000000
      C1(4)=0.0155320
      Gexp(5)=144.3000000
      C1(5)=0.0531460
      Gexp(6)=52.1800000
      C1(6)=0.1467870
      Gexp(7)=20.3400000
      C1(7)=0.3046630
      Gexp(8)=8.3810000
      C1(8)=0.3976840
      Gexp(9)=3.5290000
      C1(9)=0.2176410

      Type(2)='2S'
      Ngauss(2)=9
      Gexp(10)=45840.0000000
      C1(10)=-0.0000200
      Gexp(11)=6868.0000000
      C1(11)=-0.0001590
      Gexp(12)=1563.0000000
      C1(12)=-0.0008240
      Gexp(13)=442.4000000
      C1(13)=-0.0034780
      Gexp(14)=144.3000000
      C1(14)=-0.0119660
      Gexp(15)=52.1800000
      C1(15)=-0.0353880
      Gexp(16)=20.3400000
      C1(16)=-0.0800770
      Gexp(17)=8.3810000
      C1(17)=-0.1467220
      Gexp(18)=3.5290000
      C1(18)=-0.1163600

      Type(3)='3S'
      Ngauss(3)=1
      Gexp(19)=1.0540000
      C1(19)=1.0000000

      Type(4)='4S'
      Ngauss(4)=1
      Gexp(20)=0.4118000
      C1(20)=1.0000000

      Type(5)='5S'
      Ngauss(5)=1
      Gexp(21)=0.1552000
      C1(21)=1.0000000

      Type(6)='6S'
      Ngauss(6)=1
      Gexp(22)=0.0546400
      C1(22)=1.0000000

      Type(7)='1P'
      Ngauss(7)=3
      Gexp(23)=49.3300000
      C1(23)=0.0055330
      Gexp(24)=11.3700000
      C1(24)=0.0379620
      Gexp(25)=3.4350000
      C1(25)=0.1490280

      Type(8)='2P'
      Ngauss(8)=1
      Gexp(26)=1.1820000
      C1(26)=1.0000000

      Type(9)='3P'
      Ngauss(9)=1
      Gexp(27)=0.4173000
      C1(27)=1.0000000

      Type(10)='4P'
      Ngauss(10)=1
      Gexp(28)=0.1428000
      C1(28)=1.0000000

      Type(11)='5P'
      Ngauss(11)=1
      Gexp(29)=0.0440200
      C1(29)=1.0000000

      select case(Znum) ! Can USE Znum
      case(1)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000

      case(2)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000

      case(3)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000

      case(4)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000

      case(5)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000

      case(6)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000

      case(7)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000

      case(8)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000

      case(9)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000

      case(10)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000

      case(11)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000

      case(12)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000

      case(13)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000

      case(14)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000

      case(15)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000

      case(16)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000

      case(17)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000

      case(18)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000

      case(19)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000

      case(20)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000

      case(21)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000

      case(22)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000

      case(23)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000

      case(24)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000

      case(25)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000

      case(26)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000

      case(27)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000

      case(28)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000

      case(29)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000

      case(30)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000

      case(31)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000

      case(32)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000

      case(33)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000

      case(34)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000

      case(35)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000

      case(36)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000

      case default
        write(UNIout,'(a)')'pVQZ7e basis set not available for element, ',Element(1:len_trim(Element))
        call PRG_stop ('pVQZ7e basis set not available for element')
      end select
      return
      end subroutine SET_BASIS_pVQZ7e
! END CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine BLD_pVQZ7e_BASIS
