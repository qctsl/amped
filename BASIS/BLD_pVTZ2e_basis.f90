      subroutine BLD_pVTZ2e_BASIS
!*****************************************************************************************************************
!     Date last modified: February 16, 2000                                                         Version 2.0  *
!     Author: R. A. Poirier                                                                                      *
!     Description: Define a basis set.                                                                           *
!*****************************************************************************************************************
! Modules:
      USE MENU_BLD_basis

      implicit none
!
! Local scalars:
      integer :: Znum
      logical :: LKept
!
! Begin:
      Type(1:30)(1:16)=' '
! 1) Loop over all the elements in the molecule
      do IELEM=1,NELEMTS
        ELEMENT=ELMLIST(IELEM)
        call SET_BASIS_pVTZ2e

        Znum=CARTESIAN(1)%Atomic_number
        write(6,'(a,i4)')' Znum: ',Znum
        ScaleF(1:9)=0.252983D0*Znum+0.439507D0
!        ScaleF(1)=0.2297D0*Znum+0.0558D0
!        ScaleF(2)=0.2542D0*Znum-0.1482D0
!        ScaleF(3)=0.4727D0*Znum-0.6575D0
!        ScaleF(4)=0.4004D0*Znum-0.2153D0
!        ScaleF(5)=3.6286D0*Znum-6.7413D0
      
        IGstart=1
        do Ishell=1,NshellIN
          LKept=.true.
! Determine if this shell must be kept:
          if(Type(Ishell)(1:5).eq.'P-POL'.and..not.PPOLAR)LKept=.false.
          if(Type(Ishell)(1:5).eq.'D-POL'.and..not.DPOLAR)LKept=.false.
          if(Type(Ishell)(1:6).eq.'D2-POL'.and..not.D2POLAR)LKept=.false.
          if(Type(Ishell)(1:6).eq.'D2-POL')Type='D-POLARIZATION '
          if(Type(Ishell)(1:5).eq.'F-POL'.and..not.FPOLAR)LKept=.false.
          if(Type(Ishell)(1:5).eq.'F-POL'.and..not.FPOLAR)LKept=.false.
          if(Type(Ishell)(1:5).eq.'S-DIF'.and..not.HDIFFUSE)LKept=.false.
          if(Type(Ishell)(1:6).eq.'SP-DIF'.and..not.DIFFUSE)LKept=.false.
!
          IGend=IGstart+Ngauss(Ishell)-1
          if(LKept)then ! Save this shell:
            do Igauss=IGstart,IGend
              Gexp(Igauss)=Gexp(Igauss)*ScaleF(Ishell)*ScaleF(Ishell)
            end do
            call SAVE_shell
          end if ! LKept
          IGstart=IGend+1
        end do ! Ishell
      end do ! IELEM
!
! End of routine BLD_pVTZ2e_BASIS
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine SET_BASIS_pVTZ2e
!*****************************************************************************************************************
!     Date last modified: February 7, 2001                                                          Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Set basis set defaults (can only be called after a molecule has been defined.                 *
!*****************************************************************************************************************
! Modules:

      implicit none
!
! Begin:
      Igauss=0
      Ishell=0
!
          NshellIN=9
          Ishell=Ishell+1
          Type(Ishell) = '1S'
          ScaleF(Ishell) = 1.00
          Ngauss(Ishell) = 4
          Igauss=Igauss+1
          Gexp(Igauss) = 234.0000000D0
            C1(Igauss) = 0.0025870D0
          Igauss=Igauss+1
          Gexp(Igauss) = 35.1600000D0
            C1(Igauss) = 0.0195330D0
          Igauss=Igauss+1
          Gexp(Igauss) = 7.9890000D0
            C1(Igauss) = 0.0909980D0
          Igauss=Igauss+1
          Gexp(Igauss) = 2.2120000D0
            C1(Igauss) = 0.2720500D0
          Ishell=Ishell+1
          Type(Ishell) = '2S'
          ScaleF(Ishell) = 1.00
          Ngauss(Ishell) = 1
          Igauss=Igauss+1
          Gexp(Igauss) = 0.6669000D0
            C1(Igauss) = 1.0000000D0
          Ishell=Ishell+1
          Type(Ishell) = '3S'
          ScaleF(Ishell) = 1.00
          Ngauss(Ishell) = 1
          Igauss=Igauss+1
          Gexp(Igauss) = 0.2089000D0
            C1(Igauss) = 1.0000000D0
          Ishell=Ishell+1
          Type(Ishell) = '4S'
          ScaleF(Ishell) = 1.00
          Ngauss(Ishell) = 1
          Igauss=Igauss+1
          Gexp(Igauss) = 0.0513800D0
            C1(Igauss) = 1.0000000D0
          Ishell=Ishell+1
          Type(Ishell) = '2P'
          ScaleF(Ishell) = 1.00
          Ngauss(Ishell) = 1
          Igauss=Igauss+1
          Gexp(Igauss) = 3.0440000D0
            C1(Igauss) = 1.0000000D0
          Ishell=Ishell+1
          Type(Ishell) = '3P'
          ScaleF(Ishell) = 1.00
          Ngauss(Ishell) = 1
          Igauss=Igauss+1
          Gexp(Igauss) = 0.7580000D0
            C1(Igauss) = 1.0000000D0
          Ishell=Ishell+1
          Type(Ishell) = '4P'
          ScaleF(Ishell) = 1.00
          Ngauss(Ishell) = 1
          Igauss=Igauss+1
          Gexp(Igauss) = 0.1993000D0
            C1(Igauss) = 1.0000000D0
          Ishell=Ishell+1
          Type(Ishell) = '3D'
          ScaleF(Ishell) = 1.00
          Ngauss(Ishell) = 1
          Igauss=Igauss+1
          Gexp(Igauss) = 1.9650000D0
            C1(Igauss) = 1.0000000D0
          Ishell=Ishell+1
          Type(Ishell) = '4D'
          ScaleF(Ishell) = 1.00
          Ngauss(Ishell) = 1
          Igauss=Igauss+1
          Gexp(Igauss) = 0.4592000D0
            C1(Igauss) = 1.0000000D0
!
      return
      end subroutine SET_BASIS_pVTZ2e
! END CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine BLD_pVTZ2e_BASIS
