      subroutine BLD_aug_cc_pV5Z_S_BASIS
!*****************************************************************************************
!     Date last modified: April 29, 2016                                    Version 1.0  *
!     Author: JWH                                                                        *
!     Description: Define a basis set.                                                   *
!*****************************************************************************************
! Modules:
      USE MENU_BLD_basis

      implicit none
!
! Local scalars:
      logical :: LKept
!
! Begin:
      Type(1:30)(1:16)=' '
! 1) Loop over all the elements in the molecule
      do IELEM=1,NELEMTS
        ELEMENT=ELMLIST(IELEM)
        call SET_BASIS_aug_cc_pV5Z_S

        IGstart=1
        do Ishell=1,NshellIN
          LKept=.true.
! Determine if this shell must be kept:
          if(Type(Ishell)(1:5).eq.'P-POL'.and..not.PPOLAR)LKept=.false.
          if(Type(Ishell)(1:5).eq.'D-POL'.and..not.DPOLAR)LKept=.false.
          if(Type(Ishell)(1:6).eq.'D2-POL'.and..not.D2POLAR)LKept=.false.
          if(Type(Ishell)(1:6).eq.'D2-POL')Type='D-POLARIZATION '
          if(Type(Ishell)(1:5).eq.'F-POL'.and..not.FPOLAR)LKept=.false.
          if(Type(Ishell)(1:5).eq.'F-POL'.and..not.FPOLAR)LKept=.false.
          if(Type(Ishell)(1:5).eq.'S-DIF'.and..not.HDIFFUSE)LKept=.false.
          if(Type(Ishell)(1:6).eq.'SP-DIF'.and..not.DIFFUSE)LKept=.false.
 
          IGend=IGstart+Ngauss(Ishell)-1
          if(LKept)then ! Save this shell:
            do Igauss=IGstart,IGend
              Gexp(Igauss)=Gexp(Igauss)*ScaleF(Ishell)*ScaleF(Ishell)
            end do
            call SAVE_shell
          end if ! LKept
          IGstart=IGend+1
        end do ! Ishell
      end do ! IELEM
!
! End of routine BLD_aug_cc_pV5Z_S_BASIS
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine SET_BASIS_aug_cc_pV5Z_S
!*************************************************************************************************
!     Date last modified: July 4, 2011                                           Version 1.0     *
!     Author: R.A. Poirier                                                                       *
!     Description: Set basis set defaults (can only be called after a molecule has been defined. *
!*************************************************************************************************
! Modules:

      implicit none

!
! Begin:
! Currently only H, Li, N and F are put in, the rest is from cc-pVDZ (adding the rest as needed)
      Igauss=0
      Ishell=0
      select case(Element)
      case ( 'HELIUM' )
          NshellIN=22
          Ishell=Ishell+1
          Type(Ishell) = '1S'
          ScaleF(Ishell) = 1.00
          Ngauss(Ishell) = 8
          Igauss=Igauss+1
          Gexp(Igauss) = 1.145000D+03
            C1(Igauss) = 3.590000D-04
          Igauss=Igauss+1
          Gexp(Igauss) = 1.717000D+02
            C1(Igauss) = 2.771000D-03
          Igauss=Igauss+1
          Gexp(Igauss) = 3.907000D+01
            C1(Igauss) = 1.425100D-02
          Igauss=Igauss+1
          Gexp(Igauss) = 1.104000D+01
            C1(Igauss) = 5.556600D-02
          Igauss=Igauss+1
          Gexp(Igauss) = 3.566000D+00
            C1(Igauss) = 1.620910D-01
          Igauss=Igauss+1
          Gexp(Igauss) = 1.240000D+00
            C1(Igauss) = 3.321970D-01
          Igauss=Igauss+1
          Gexp(Igauss) = 4.473000D-01
            C1(Igauss) = 4.196150D-01
          Igauss=Igauss+1
          Gexp(Igauss) = 1.640000D-01
            C1(Igauss) = 1.861280D-01
          Ishell=Ishell+1
          Type(Ishell) = '2S'
          ScaleF(Ishell) = 1.00
          Ngauss(Ishell) = 1
          Igauss=Igauss+1
          Gexp(Igauss) = 3.566000D+00
            C1(Igauss) = 1.000000D+00
          Ishell=Ishell+1
          Type(Ishell) = '3S'
          ScaleF(Ishell) = 1.00
          Ngauss(Ishell) = 1
          Igauss=Igauss+1
          Gexp(Igauss) = 1.240000D+00
            C1(Igauss) = 1.000000D+00
          Ishell=Ishell+1
          Type(Ishell) = '4S'
          ScaleF(Ishell) = 1.00
          Ngauss(Ishell) = 1
          Igauss=Igauss+1
          Gexp(Igauss) = 4.473000D-01
            C1(Igauss) = 1.000000D+00
          Ishell=Ishell+1
          Type(Ishell) = '5S'
          ScaleF(Ishell) = 1.00
          Ngauss(Ishell) = 1
          Igauss=Igauss+1
          Gexp(Igauss) = 1.640000D-01
            C1(Igauss) = 1.000000D+00
          Ishell=Ishell+1
          Type(Ishell) = '6S'
          ScaleF(Ishell) = 1.00
          Ngauss(Ishell) = 1
          Igauss=Igauss+1
          Gexp(Igauss) = 0.0466400D0
            C1(Igauss) = 1.0000000D0
          Ishell=Ishell+1
          Type(Ishell) = '7S'
          ScaleF(Ishell) = 1.00
          Ngauss(Ishell) = 1
          Igauss=Igauss+1
          Gexp(Igauss) = 0.0248723D0
            C1(Igauss) = 1.0000000D0
          Ishell=Ishell+1
          Type(Ishell) = '8S'
          ScaleF(Ishell) = 1.00
          Ngauss(Ishell) = 1
          Igauss=Igauss+1
          Gexp(Igauss) = 0.0132640D0
            C1(Igauss) = 1.0000000D0
          Ishell=Ishell+1
          Type(Ishell) = '9S'
          ScaleF(Ishell) = 1.00
          Ngauss(Ishell) = 1
          Igauss=Igauss+1
          Gexp(Igauss) = 0.0070735D0
            C1(Igauss) = 1.0000000D0
          Ishell=Ishell+1
          Type(Ishell) = '10S'
          ScaleF(Ishell) = 1.00
          Ngauss(Ishell) = 1
          Igauss=Igauss+1
          Gexp(Igauss) = 0.0037722D0
            C1(Igauss) = 1.0000000D0
          Ishell=Ishell+1
          Type(Ishell) = '11S'
          ScaleF(Ishell) = 1.00
          Ngauss(Ishell) = 1
          Igauss=Igauss+1
          Gexp(Igauss) = 0.00201162D0
            C1(Igauss) = 1.0000000D0
          Ishell=Ishell+1
          Type(Ishell) = '12S'
          ScaleF(Ishell) = 1.00
          Ngauss(Ishell) = 1
          Igauss=Igauss+1
          Gexp(Igauss) = 0.00107276D0
            C1(Igauss) = 1.0000000D0
          Ishell=Ishell+1
          Type(Ishell) = '13S'
          ScaleF(Ishell) = 1.00
          Ngauss(Ishell) = 1
          Igauss=Igauss+1
          Gexp(Igauss) = 0.00057209D0
            C1(Igauss) = 1.0000000D0
          Ishell=Ishell+1
          Type(Ishell) = '2P'
          ScaleF(Ishell) = 1.00
          Ngauss(Ishell) = 1
          Igauss=Igauss+1
          Gexp(Igauss) = 1.015300D+01
            C1(Igauss) = 1.000000D+00
          Ishell=Ishell+1
          Type(Ishell) = '3P'
          ScaleF(Ishell) = 1.00
          Ngauss(Ishell) = 1
          Igauss=Igauss+1
          Gexp(Igauss) = 3.627000D+00
            C1(Igauss) = 1.000000D+00
          Ishell=Ishell+1
          Type(Ishell) = '4P'
          ScaleF(Ishell) = 1.00
          Ngauss(Ishell) = 1
          Igauss=Igauss+1
          Gexp(Igauss) = 1.296000D+00
            C1(Igauss) = 1.000000D+00
          Ishell=Ishell+1
          Type(Ishell) = '5P'
          ScaleF(Ishell) = 1.00
          Ngauss(Ishell) = 1
          Igauss=Igauss+1
          Gexp(Igauss) = 4.630000D-01
            C1(Igauss) = 1.000000D+00
          Ishell=Ishell+1
          Type(Ishell) = '6P'
          ScaleF(Ishell) = 1.00
          Ngauss(Ishell) = 1
          Igauss=Igauss+1
          Gexp(Igauss) = 0.1400000D0
            C1(Igauss) = 1.0000000D0
          Ishell=Ishell+1
          Type(Ishell) = '3D'
          ScaleF(Ishell) = 1.00
          Ngauss(Ishell) = 1
          Igauss=Igauss+1
          Gexp(Igauss) = 7.666000D+00
            C1(Igauss) = 1.000000D+00
          Ishell=Ishell+1
          Type(Ishell) = '4D'
          ScaleF(Ishell) = 1.00
          Ngauss(Ishell) = 1
          Igauss=Igauss+1
          Gexp(Igauss) = 2.647000D+00
            C1(Igauss) = 1.000000D+00
          Ishell=Ishell+1
          Type(Ishell) = '5D'
          ScaleF(Ishell) = 1.00
          Ngauss(Ishell) = 1
          Igauss=Igauss+1
          Gexp(Igauss) = 9.140000D-01
            C1(Igauss) = 1.000000D+00
          Ishell=Ishell+1
          Type(Ishell) = '6D'
          ScaleF(Ishell) = 1.00
          Ngauss(Ishell) = 1
          Igauss=Igauss+1
          Gexp(Igauss) = 0.2892000D0
            C1(Igauss) = 1.0000000D0
!          Ishell=Ishell+1
!          Type(Ishell) = '4F'
!          ScaleF(Ishell) = 1.00
!          Ngauss(Ishell) = 1
!          Igauss=Igauss+1
!          Gexp(Igauss) = 5.411000D+00
!            C1(Igauss) = 1.000000D+00
!          Ishell=Ishell+1
!          Type(Ishell) = '5F'
!          ScaleF(Ishell) = 1.00
!          Ngauss(Ishell) = 1
!          Igauss=Igauss+1
!          Gexp(Igauss) = 1.707000D+00
!            C1(Igauss) = 1.000000D+00
!          Ishell=Ishell+1
!          Type(Ishell) = '6F'
!          ScaleF(Ishell) = 1.00
!          Ngauss(Ishell) = 1
!          Igauss=Igauss+1
!          Gexp(Igauss) = 0.5345000D0
!            C1(Igauss) = 1.0000000D0
!          Ishell=Ishell+1
!          Type(Ishell) = '5G'
!          ScaleF(Ishell) = 1.00
!          Ngauss(Ishell) = 1
!          Igauss=Igauss+1
!          Gexp(Igauss) = 3.430000D+00
!            C1(Igauss) = 1.0000000D0
!          Ishell=Ishell+1
!          Type(Ishell) = '6G'
!          ScaleF(Ishell) = 1.00
!          Ngauss(Ishell) = 1
!          Igauss=Igauss+1
!          Gexp(Igauss) = 0.7899000D0
!            C1(Igauss) = 1.0000000D0

      case default
        write(UNIout,'(a)')'aug-cc-pV5Z_S basis set not available for element, ',Element(1:len_trim(Element))
        call PRG_stop ('aug-cc-pV5Z_S basis set not available for element')
      end select
      return
      end subroutine SET_BASIS_aug_cc_pV5Z_S
! END CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine BLD_aug_cc_pV5Z_S_BASIS

