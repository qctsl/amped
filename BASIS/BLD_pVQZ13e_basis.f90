      subroutine BLD_pVQZ13e_BASIS
!*****************************************************************************************************************
!     Date last modified: May 16, 2011                                                              Version 2.0  *
!     Author: V. R. Grandy, R. A. Poirier                                                                        *
!     Description: Define a basis set.                                                                           *
!*****************************************************************************************************************
! Modules:
      USE MENU_BLD_basis

      implicit none
!
! Local scalars:
      integer :: Znum
!
! Begin:
      Type(1:30)(1:16)=' '
! 1) Loop over all the elements in the molecule
      do IELEM=1,NELEMTS
!       ELEMENT=ELMLIST(IELEM)
        Znum=CARTESIAN(1)%Atomic_number
        call SET_BASIS_pVQZ13e

        write(6,'(a,i4)')' Znum: ',Znum
!       ScaleF(1)=0.2297D0*Znum+0.0558D0
!       ScaleF(2)=0.2542D0*Znum-0.1482D0
!       ScaleF(3)=0.4727D0*Znum-0.6575D0
!       ScaleF(4)=0.4004D0*Znum-0.2153D0
!       ScaleF(5)=3.6286D0*Znum-6.7413D0
      
        IGstart=1
        do Ishell=1,NshellIN
          IGend=IGstart+Ngauss(Ishell)-1
            do Igauss=IGstart,IGend
              Gexp(Igauss)=Gexp(Igauss)*ScaleF(Ishell)*ScaleF(Ishell)
            end do
            call SAVE_shell
          IGstart=IGend+1
        end do ! Ishell
      end do ! IELEM
!
! End of routine BLD_pVQZ13e_BASIS
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine SET_BASIS_pVQZ13e
!*****************************************************************************************************************
!     Date last modified: May 16, 2011                                                              Version 2.0  *
!     Author: V. R. Grandy, R.A. Poirier                                                                         *
!     Description: Set basis set defaults (can only be called after a molecule has been defined.                 *
!*****************************************************************************************************************
! Modules:

      implicit none
!
! Begin:
      NshellIN=13
      Type(1)='1S'
      Ngauss(1)=13
      Gexp(1)=419600.0000000
      C1(1)=0.278219D-04
      Gexp(2)=62830.0000000
      C1(2)=0.216330D-03
      Gexp(3)=14290.0000000
      C1(3)=0.113754D-02
      Gexp(4)=4038.0000000
      C1(4)=0.479635D-02
      Gexp(5)=1312.0000000
      C1(5)=0.172389D-01
      Gexp(6)=470.5000000
      C1(6)=0.538066D-01
      Gexp(7)=181.8000000
      C1(7)=0.141326D+00
      Gexp(8)=74.4600000
      C1(8)=0.289268D+00
      Gexp(9)=31.9000000
      C1(9)=0.384825D+00
      Gexp(10)=13.9600000
      C1(10)=0.232852D+00
      Gexp(11)=5.1800000
      C1(11)=0.293330D-01
      Gexp(12)=2.2650000
      C1(12)=-0.300574D-02
      Gexp(13)=0.9664000
      C1(13)=0.166673D-02

      Type(2)='2S'
      Ngauss(2)=13
      Gexp(14)=419600.0000000
      C1(14)=-0.723754D-05
      Gexp(15)=62830.0000000
      C1(15)=-0.561733D-04
      Gexp(16)=14290.0000000
      C1(16)=-0.296528D-03
      Gexp(17)=4038.0000000
      C1(17)=-0.124913D-02
      Gexp(18)=1312.0000000
      C1(18)=-0.455101D-02
      Gexp(19)=470.5000000
      C1(19)=-0.144393D-01
      Gexp(20)=181.8000000
      C1(20)=-0.403464D-01
      Gexp(21)=74.4600000
      C1(21)=-0.922618D-01
      Gexp(22)=31.9000000
      C1(22)=-0.164510D+00
      Gexp(23)=13.9600000
      C1(23)=-0.141296D+00
      Gexp(24)=5.1800000
      C1(24)=0.195365D+00
      Gexp(25)=2.2650000
      C1(25)=0.572475D+00
      Gexp(26)=0.9664000
      C1(26)=0.374041D+00

      Type(3)='3S'
      Ngauss(3)=13
      Gexp(27)=419600.0000000
      C1(27)=0.167150D-05
      Gexp(28)=62830.0000000
      C1(28)=0.129641D-04
      Gexp(29)=14290.0000000
      C1(29)=0.685101D-04
      Gexp(30)=4038.0000000
      C1(30)=0.288274D-03
      Gexp(31)=1312.0000000
      C1(31)=0.105276D-02
      Gexp(32)=470.5000000
      C1(32)=0.333878D-02
      Gexp(33)=181.8000000
      C1(33)=0.939217D-02
      Gexp(34)=74.4600000
      C1(34)=0.216047D-01
      Gexp(35)=31.9000000
      C1(35)=0.395873D-01
      Gexp(36)=13.9600000
      C1(36)=0.349180D-01
      Gexp(37)=5.1800000
      C1(37)=-0.528415D-01
      Gexp(38)=2.2650000
      C1(38)=-0.191878D+00
      Gexp(39)=0.9664000
      C1(39)=-0.254115D+00

      Type(4)='4S'
      Ngauss(4)=1
      Gexp(40)=0.2447000
      C1(40)=1.0000000

      Type(5)='5S'
      Ngauss(5)=1
      Gexp(41)=0.1184000
      C1(41)=1.0000000

      Type(6)='6S'
      Ngauss(6)=1
      Gexp(42)=0.0502100
      C1(42)=1.0000000

      Type(7)='7S'
      Ngauss(7)=1
      Gexp(43)=0.0183000
      C1(43)=1.0000000

      Type(8)='1P'
      Ngauss(8)=8
      Gexp(44)=891.3000000
      C1(44)=0.491755D-03
      Gexp(45)=211.3000000
      C1(45)=0.415843D-02
      Gexp(46)=68.2800000
      C1(46)=0.212538D-01
      Gexp(47)=25.7000000
      C1(47)=0.764058D-01
      Gexp(48)=10.6300000
      C1(48)=0.194277D+00
      Gexp(49)=4.6020000
      C1(49)=0.334428D+00
      Gexp(50)=2.0150000
      C1(50)=0.375026D+00
      Gexp(51)=0.8706000
      C1(51)=0.204041D+00

      Type(9)='2P'
      Ngauss(9)=8
      Gexp(52)=891.3000000
      C1(52)=-0.888695D-04
      Gexp(53)=211.3000000
      C1(53)=-0.745823D-03
      Gexp(54)=68.2800000
      C1(54)=-0.387025D-02
      Gexp(55)=25.7000000
      C1(55)=-0.139350D-01
      Gexp(56)=10.6300000
      C1(56)=-0.366860D-01
      Gexp(57)=4.6020000
      C1(57)=-0.627797D-01
      Gexp(58)=2.0150000
      C1(58)=-0.789602D-01
      Gexp(59)=0.8706000
      C1(59)=-0.288589D-01

      Type(10)='3P'
      Ngauss(10)=1
      Gexp(60)=0.2972000
      C1(60)=1.0000000

      Type(11)='4P'
      Ngauss(11)=1
      Gexp(61)=0.1100000
      C1(61)=1.0000000

      Type(12)='5P'
      Ngauss(12)=1
      Gexp(62)=0.0398900
      C1(62)=1.0000000

      Type(13)='6P'
      Ngauss(13)=1
      Gexp(63)=0.0121000
      C1(63)=1.0000000

      select case(Znum) ! Can USE Znum
      case(1)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(2)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(3)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(4)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(5)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(6)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(7)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(8)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(9)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(10)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(11)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(12)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(13)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(14)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(15)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(16)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(17)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(18)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(19)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(20)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(21)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(22)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(23)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(24)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(25)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(26)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(27)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(28)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(29)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(30)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(31)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(32)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(33)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(34)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(35)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(36)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case default
        write(UNIout,'(a)')'pVQZ13e basis set not available for element, ',Element(1:len_trim(Element))
        call PRG_stop ('pVQZ13e basis set not available for element')
      end select
      return
      end subroutine SET_BASIS_pVQZ13e
! END CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine BLD_pVQZ13e_BASIS
