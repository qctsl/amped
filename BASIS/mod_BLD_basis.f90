      MODULE MENU_BLD_basis
!*****************************************************************************************************************
!     Date last modified: January 18, 2001                                                          Version 1.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: This module is used in defining the basis set only.                                           *
!*****************************************************************************************************************
! Modules:
      USE program_constants
      USE program_parser
      USE menu_gets
      USE type_basis_set
      USE type_molecule

      implicit none
!
! Local scalars:
      integer :: Nelemts
      logical :: PPOLAR,DPOLAR,D2polar,FPOLAR,DIFFUSE,HDIFFUSE
      integer :: Iatmshl,Ielem,Igauss,IGstart,IGend,Iprim,Ishell,Nprimitives,NshellIN
      integer, parameter :: MAX_Bname=132   ! Maximum length for Basis name of basis set
      integer, parameter :: MAX_shells=1100      ! Maximum number of shells and atom shells
      integer, parameter :: MAX_primitives=3000  ! Maximum number of primitives
      integer, parameter :: MAX_shellsIN=30      ! Maximum number of shells for a given atom
      integer, parameter :: MAX_gaussians=120    ! Maximum number of gaussians for a given atom
      character(len=MAX_Bname) :: BASIC_name
      character*16 :: ELEMENT
!
! Work arrays: Atoms
      integer :: Ngauss(MAX_shellsIN)
      double precision :: ScaleF(MAX_shellsIN)
      double precision :: Gexp(MAX_gaussians),C1(MAX_gaussians),C2(MAX_gaussians)
      character(len=16) :: Type(MAX_shellsIN)
      integer, dimension(:), allocatable :: ZANLIST
      character*16, dimension(:), allocatable :: ELMLIST
!
      type (basis_set), save :: BasisW

CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine GET_BASIC_NAME (BASIS_nameIN)
!*****************************************************************************************************************
!     Date last modified: February 16, 2000                                                         Version 2.0  *
!     Author: R. A. Poirier                                                                                      *
!     Description: Extract the basic basis set name (used to define the file name) and make sure the basis set   *
!                  name is consistent.                                                                           *
!*****************************************************************************************************************
! Modules:

      implicit none
!
! Input scalars:
      character*(*) BASIS_nameIN
!
! Local scalars
      integer :: Gpos,start
      integer :: LenName,LenBname
      character(len=8) :: Polarization
      character(len=len_trim(BASIS_nameIN)) :: Bname_local
!
! Begin:
! Get basic name first
      Polarization(1:)=' '
      Bname_local=BASIS_nameIN
      call CNV_SLtoU (Bname_local)
      LenName=len_trim(BASIS_nameIN)
      BASIC_name(1:)=' '
      if(index(Bname_local,'6-311').ne.0)then
        Gpos=index(Bname_local,'G')
        BASIC_name='6-311G'
        Polarization=BASIS_nameIN(Gpos+1:LenName)
      else if(index(Bname_local,'6-31').ne.0)then
        Gpos=index(Bname_local,'G')
        BASIC_name='6-31G'
        Polarization=BASIS_nameIN(Gpos+1:LenName)
      else if(index(Bname_local,'3-21').ne.0)then
        Gpos=index(Bname_local,'G')
        BASIC_name='3-21G'
        Polarization=BASIS_nameIN(Gpos+1:LenName)
      else if(index(Bname_local,'STO').ne.0)then
        Gpos=index(Bname_local,'G')
        BASIC_name=BASIS_nameIN(1:Gpos)
        Polarization=BASIS_nameIN(Gpos+1:LenName)
      else if(index(Bname_local,'G10S').ne.0)then ! Special uncontracted basis sets
        BASIC_name='G10S'
      else if(index(Bname_local,'AUG-CC-PVDZ').ne.0)then ! Special uncontracted basis sets
        BASIC_name='aug-cc-pVDZ'
        BASIS_nameIN(1:11)='aug-cc-pVDZ'
      else if(index(Bname_local,'AUG-CC-PVTZ-MODP').ne.0)then ! Special uncontracted basis sets
        BASIC_name='aug-cc-pVTZ-modp'
        BASIS_nameIN(1:16)='aug-cc-pVTZ-modp'
      else if(index(Bname_local,'AUG-CC-PVTZ-MUON').ne.0)then ! Special uncontracted basis sets
        BASIC_name='aug-cc-pVTZ-muon'
        BASIS_nameIN(1:16)='aug-cc-pVTZ-muon'
      else if(index(Bname_local,'AUG(S,P)-CC-PVTZ-MODP').ne.0)then ! Special uncontracted basis sets
        BASIC_name='aug(s,p)-cc-pVTZ-modp'
        BASIS_nameIN(1:21)='aug(s,p)-cc-pVTZ-modp'
      else if(index(Bname_local,'AUG-CC-PVTZ').ne.0)then ! Special uncontracted basis sets
        BASIC_name='aug-cc-pVTZ'
        BASIS_nameIN(1:11)='aug-cc-pVTZ'
      else if(index(Bname_local,'AUG-CC-PCVTZ').ne.0)then ! Special uncontracted basis sets
        BASIC_name='aug-cc-pCVTZ'
        BASIS_nameIN(1:11)='aug-cc-pCVTZ'
      else if(index(Bname_local,'AUG-CC-PVQZ').ne.0)then ! Special uncontracted basis sets
        BASIC_name='aug-cc-pVQZ'
        BASIS_nameIN(1:11)='aug-cc-pVQZ'
      else if(index(Bname_local,'AUG-CC-PV5Z').ne.0)then ! Special uncontracted basis sets
        BASIC_name='aug-cc-pV5Z'
        BASIS_nameIN(1:11)='aug-cc-pV5Z'
      else if(index(Bname_local,'AUG-CC-PV5Z-S').ne.0)then ! Special uncontracted basis sets
        BASIC_name='aug-cc-pV5Z-S'
        BASIS_nameIN(1:13)='aug-cc-pV5Z-S'
      else if(index(Bname_local,'AUG-CC-PV5Z-P').ne.0)then ! Special uncontracted basis sets
        BASIC_name='aug-cc-pV5Z-P'
        BASIS_nameIN(1:13)='aug-cc-pV5Z-P'
      else if(index(Bname_local,'AUG-CC-PV5Z-D').ne.0)then ! Special uncontracted basis sets
        BASIC_name='aug-cc-pV5Z-D'
        BASIS_nameIN(1:13)='aug-cc-pV5Z-D'
      else if(index(Bname_local,'CC-PVDZ').ne.0)then ! Special uncontracted basis sets
        BASIC_name='cc-pVDZ'
        BASIS_nameIN(1:11)='cc-pVDZ'
      else if(index(Bname_local,'CC-PVTZ').ne.0)then ! Special uncontracted basis sets
        BASIC_name='cc-pVTZ'
        BASIS_nameIN(1:11)='cc-pVTZ'
      else if(index(Bname_local,'CC-PVQZ').ne.0)then ! Special uncontracted basis sets
        BASIC_name='cc-pVQZ'
        BASIS_nameIN(1:11)='cc-pVQZ'
      else if(index(Bname_local,'CC-PVQ').ne.0)then ! Special uncontracted basis sets
        BASIC_name='cc-pVQZ'
        BASIS_nameIN(1:11)='cc-pVQZ'
      else if(index(Bname_local,'CC-PVT').ne.0)then ! Special uncontracted basis sets
        BASIC_name='cc-pVTZ'
        BASIS_nameIN(1:11)='cc-pVTZ'
      else if(index(Bname_local,'PVTZ-2E').ne.0)then ! Special basis sets
        BASIC_name='pVTZ-2e'
        BASIS_nameIN(1:)='pVTZ-2e'
      else if(index(Bname_local,'PVTZ-3E').ne.0)then ! Special basis sets
        BASIC_name='pVTZ-3e'
        BASIS_nameIN(1:)='pVTZ-3e'
      else if(index(Bname_local,'PVTZ-4E').ne.0)then ! Special basis sets
        BASIC_name='pVTZ-4e'
        BASIS_nameIN(1:)='pVTZ-4e'
      else if(index(Bname_local,'PCVQZ-4E').ne.0)then ! Special basis sets
        BASIC_name='pCVQZ-4e'
        BASIS_nameIN(1:)='pCVQZ-4e'
      else if(index(Bname_local,'PVTZ-5E').ne.0)then ! Special basis sets
        BASIC_name='pVTZ-5e'
        BASIS_nameIN(1:)='pVTZ-5e'
      else if(index(Bname_local,'PVTZ-6E').ne.0)then ! Special basis sets
        BASIC_name='pVTZ-6e'
        BASIS_nameIN(1:)='pVTZ-6e'
      else if(index(Bname_local,'PVTZ-7E').ne.0)then ! Special basis sets
        BASIC_name='pVTZ-7e'
        BASIS_nameIN(1:)='pVTZ-7e'
      else if(index(Bname_local,'PVTZ-8E').ne.0)then ! Special basis sets
        BASIC_name='pVTZ-8e'
        BASIS_nameIN(1:)='pVTZ-8e'
      else if(index(Bname_local,'PVTZ-9E').ne.0)then ! Special basis sets
        BASIC_name='pVTZ-9e'
        BASIS_nameIN(1:)='pVTZ-9e'
      else if(index(Bname_local,'PVTZ-10E').ne.0)then ! Special basis sets
        BASIC_name='pVTZ-10e'
        BASIS_nameIN(1:)='pVTZ-10e'
      else if(index(Bname_local,'PVQZ-2E').ne.0)then ! Special basis sets
        BASIC_name='pVQZ-2e'
        BASIS_nameIN(1:7)='pVQZ-2e'
      else if(index(Bname_local,'PVQZ-3E').ne.0)then ! Special basis sets
        BASIC_name='pVQZ-3e'
        BASIS_nameIN(1:7)='pVQZ-3e'
      else if(index(Bname_local,'PVQZ-4E').ne.0)then ! Special basis sets
        BASIC_name='pVQZ-4e'
        BASIS_nameIN(1:7)='pVQZ-4e'
      else if(index(Bname_local,'PVQZ-5E').ne.0)then ! Special basis sets
        BASIC_name='pVQZ-5e'
        BASIS_nameIN(1:7)='pVQZ-5e'
      else if(index(Bname_local,'PVQZ-6E').ne.0)then ! Special basis sets
        BASIC_name='pVQZ-6e'
        BASIS_nameIN(1:7)='pVQZ-6e'
      else if(index(Bname_local,'PVQZ-7E').ne.0)then ! Special basis sets
        BASIC_name='pVQZ-7e'
        BASIS_nameIN(1:7)='pVQZ-7e'
      else if(index(Bname_local,'PVQZ-8E').ne.0)then ! Special basis sets
        BASIC_name='pVQZ-8e'
        BASIS_nameIN(1:7)='pVQZ-8e'
      else if(index(Bname_local,'PVQZ-9E').ne.0)then ! Special basis sets
        BASIC_name='pVQZ-9e'
        BASIS_nameIN(1:7)='pVQZ-9e'
      else if(index(Bname_local,'PVQZ-10E').ne.0)then ! Special basis sets
        BASIC_name='pVQZ-10e'
        BASIS_nameIN(1:7)='pVQZ-10e'
      else if(index(Bname_local,'PVQZ-11E').ne.0)then ! Special basis sets
        BASIC_name='pVQZ-11e'
        BASIS_nameIN(1:7)='pVQZ-11e'
      else if(index(Bname_local,'PVQZ-12E').ne.0)then ! Special basis sets
        BASIC_name='pVQZ-12e'
        BASIS_nameIN(1:7)='pVQZ-12e'
      else if(index(Bname_local,'PVQZ-13E').ne.0)then ! Special basis sets
        BASIC_name='pVQZ-13e'
        BASIS_nameIN(1:7)='pVQZ-13e'
      else if(index(Bname_local,'PVQZ-14E').ne.0)then ! Special basis sets
        BASIC_name='pVQZ-14e'
        BASIS_nameIN(1:7)='pVQZ-14e'
      else if(index(Bname_local,'PVQZ-15E').ne.0)then ! Special basis sets
        BASIC_name='pVQZ-15e'
        BASIS_nameIN(1:7)='pVQZ-15e'
      else if(index(Bname_local,'PVQZ-16E').ne.0)then ! Special basis sets
        BASIC_name='pVQZ-16e'
        BASIS_nameIN(1:7)='pVQZ-16e'
      else if(index(Bname_local,'PVQZ-17E').ne.0)then ! Special basis sets
        BASIC_name='pVQZ-17e'
        BASIS_nameIN(1:7)='pVQZ-17e'
      else if(index(Bname_local,'PVQZ-18E').ne.0)then ! Special basis sets
        BASIC_name='pVQZ-18e'
        BASIS_nameIN(1:7)='pVQZ-18e'
      else if(index(Bname_local,'4E').ne.0)then ! Special uncontracted basis sets
        BASIC_name='41111-4e'
        BASIS_nameIN(1:7)='41111-4e'
      else if(index(Bname_local,'SIMPLE').ne.0)then ! Special uncontracted basis sets
        BASIC_name='simple'
        BASIS_nameIN(1:6)='simple'
      else if(index(Bname_local,'IMPORT').ne.0)then ! Basis set import from Gaussian output
        BASIC_name='IMPORT'
        BASIS_nameIN(1:7)='IMPORT'
      else
        write(UNIout,'(2a)')'ERROR> GET_BASIC_NAME: No such basis set: ',BASIS_nameIN
        call PRG_stop ('GET_BASIC_NAME: No such basis set')
      end if
! Change name to new convention"
      if(index(Bname_local,'3-21G(*)').ne.0)then ! Special case for 3-21G(*) - d's on second row only
! Do nothing
      else
      start=index(Polarization,'**')
      if(start.ne.0)Polarization(1:)='(d,p)'
      start=index(Polarization,'*')
      if(start.ne.0)Polarization(1:)='(d)'
      start=index(Polarization,'D')
      if(start.ne.0)Polarization(start:start)='d'
      start=index(Polarization,'P')
      if(start.ne.0)Polarization(start:start)='p'
      start=index(Polarization,'F')
      if(start.ne.0)Polarization(start:start)='f'
      end if

      Gpos=index(BASIS_nameIN,'G')
      if(Gpos.gt.1)BASIS_nameIN(Gpos+1:)=Polarization(1:) ! Could be G10S (Gpos=1)
!
      PPOLAR=.false.
      DPOLAR=.false.
      D2polar=.false.
      FPOLAR=.false.
      DIFFUSE=.false.
      HDIFFUSE=.false.
!
      if(index(BASIS_nameIN,'(*)').ne.0)D2polar=.true. ! For 3-21G(*)
      if(index(BASIS_nameIN,'(d)').ne.0)then
        DPOLAR=.true.
        D2polar=.true.
      end if
      if(index(BASIS_nameIN,'(f)').ne.0)FPOLAR=.true.
      if(index(BASIS_nameIN,'(d,p)').ne.0)then
        PPOLAR=.true.
        DPOLAR=.true.
      end if
      if(index(BASIS_nameIN,'(df,p)').ne.0)then
        PPOLAR=.true.
        DPOLAR=.true.
        FPOLAR=.true.
      end if
      if(index(BASIS_nameIN,'+').ne.0)DIFFUSE=.true.
      if(index(BASIS_nameIN,'++').ne.0)HDIFFUSE=.true.

!
! End of routine GET_BASIC_NAME
      return
      end subroutine GET_BASIC_NAME
      subroutine LOAD_BASIS_SET (BASIS_nameIN)
!*********************************************************************************************************
!     Date last modified: February 16, 2000                                                 Version 2.0  *
!     Author: R. A. Poirier                                                                              *
!     Description: Set up Basis.                                                                         *
!*********************************************************************************************************
! Modules:

      implicit none
!
! Input scalar:
      character(*) BASIS_nameIN
!
! Local scalars:
      integer Iatmshl,Iatom,Ibasis,Ilast,Ifrst,Ishell,TNbasis
!
! Begin:
      call PRG_manager ('enter', 'LOAD_BASIS_SET', 'UTILITY')
!
      call BLD_LCOREshl (BasisW)
! Sort shells:
      if(LSorted)call SORT_SHELLS (BasisW%NFDShells, BasisW%Nshells, BasisW%Nprimitives)
!
      call SHELL_INFO (BasisW%Nshells)

      Ibasis=1
      Ilast=0
      Ifrst=1
      do Ishell=1,BasisW%Nshells
      do Iatom=1,Molecule%Natoms
        if(BasisW%shell(Ishell)%SHLATM.EQ.MOL_atoms(Iatom)%Atomic_number)then
        Ilast=Ilast+1
        if(Ilast.gt.MAX_shells)then
          stop 'Number of shells exceeds MAX_shells'
        end if
        BasisW%atmshl(ILast)%ATMLST=Iatom
        end if ! BasisW%shell(Ishell)%SHLATM
      end do ! IatomS
        BasisW%shell(Ishell)%frstSHL=Ifrst
        BasisW%shell(Ishell)%lastSHL=Ilast
        Ifrst=Ilast+1
      end do ! Ishell
      BasisW%Natmshls=Ilast
!
! For now only support 6D/10F
      BasisW%NDORBS=6
      BasisW%NFORBS=10
!
! Define TNbasis
      TNbasis=0
      do Ishell=1,BasisW%Nshells
      if(BasisW%shell(Ishell)%Xtype.EQ.0)then
        Ibasis=1
      else if(BasisW%shell(Ishell)%Xtype.EQ.1)then
        Ibasis=3
      else if(BasisW%shell(Ishell)%Xtype.EQ.2)then
        Ibasis=BasisW%NDORBS
      else if(BasisW%shell(Ishell)%Xtype.EQ.3)then
        Ibasis=BasisW%NFORBS
      end if ! Xtype
      do Iatmshl=BasisW%shell(Ishell)%frstSHL,BasisW%shell(Ishell)%lastSHL
        BasisW%atmshl(Iatmshl)%frstAO=TNbasis+1
        TNbasis=TNbasis+Ibasis
        BasisW%atmshl(Iatmshl)%lastAO=TNbasis
      end do ! Iatmshl
      end do ! Ishell
      BasisW%Nbasis=TNbasis
!
! Deallocate the work arrays
      deallocate (ZANLIST, ELMLIST)
!
! End of routine LOAD_BASIS_SET
      call PRG_manager ('exit', 'LOAD_BASIS_SET', 'UTILITY')
      return
      end subroutine LOAD_BASIS_SET
      subroutine GET_element_list
!*****************************************************************************************************************
!     Date last modified: February 16, 2000                                                                      *
!     Author: R. A. Poirier                                                                                      *
!     Description: Get a list of the elements present in the molecule.                                           *
!*****************************************************************************************************************
! Modules:
      USE type_elements

      implicit none
!
! Local scalars:
      integer :: Iatom,IANUM
      logical :: Lfound
!
! Begin:
! First get the atomic number of the current atom with name = atmnam
      NELEMTS=0
      do IANUM=1,Nelements
      Iatom=1
      Lfound=.false.
      do while (.not.Lfound)
        if(MOL_atoms(Iatom)%Atomic_number.EQ.IANUM)then
          Lfound=.true.
          NELEMTS=NELEMTS+1
          ELMLIST(NELEMTS)=Element_names(IANUM)//'    '
          ZANLIST(NELEMTS)=MOL_atoms(Iatom)%Atomic_number
        end if
        Iatom=Iatom+1
        if(Iatom.GT.molecule%Natoms)Lfound=.true.
      end do ! WHILE
      end do ! IANUM
!
! End of routine GET_element_list
      return
      end subroutine GET_element_list
      subroutine SHELL_INFO (Nshells)! Dimensions
!***********************************************************************
!     Date last modified: June 24, 1994                    Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!                                                                      *
!***********************************************************************
! Modules:

      implicit none
!
! Includes:
!
! Input scalars:
      integer Nshells
!
! Local scalars:
      integer Ishell
!
! Begin:
      call PRG_manager ('enter', 'SHELL_INFO', 'UTILITY')
!
! Obtain information for Ishell.
      do Ishell=1,Nshells
      if(BasisW%shell(Ishell)%shltyp.EQ.'S ')then
        BasisW%shell(Ishell)%Xtype=0
        BasisW%shell(Ishell)%Xstart=1
        BasisW%shell(Ishell)%XEND=1
      else if(BasisW%shell(Ishell)%shltyp.EQ.'P ')then
        BasisW%shell(Ishell)%Xtype=1
        BasisW%shell(Ishell)%Xstart=2
        BasisW%shell(Ishell)%XEND=4
      else if(BasisW%shell(Ishell)%shltyp.EQ.'D ')then
        BasisW%shell(Ishell)%Xtype=2
        BasisW%shell(Ishell)%Xstart=5
        BasisW%shell(Ishell)%XEND=10
      else if(BasisW%shell(Ishell)%shltyp.EQ.'F ')then
        BasisW%shell(Ishell)%Xtype=3
        BasisW%shell(Ishell)%Xstart=11
        BasisW%shell(Ishell)%XEND=20
      end if
      end do ! Ishell
!
! End of routine SHELL_INFO
      call PRG_manager ('exit', 'SHELL_INFO', 'UTILITY')
      return
      end subroutine SHELL_INFO
      subroutine SORT_SHELLS (NFDShells,  & ! Output
                              Nshells, &
                              Nprimitives)   ! Dimensions
!***********************************************************************
!     Date last modified: November 14, 1996                Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: This routine reorders the basis set objects (7)     *
!     generated by the menu (FDPS).  All other basis set               *
!     objects are created from these two objects.  All codes depend on *
!     the FDPS order.                                                  *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer Nshells,Nprimitives
!
! Work arrays:
      integer, dimension(:), allocatable :: ORDATM
      integer, dimension(:), allocatable :: ORDBGN
      integer, dimension(:), allocatable :: ORDEND
      double precision, dimension(:), allocatable :: ORDCC1
      double precision, dimension(:), allocatable :: ORDGAU
      double precision, dimension(:), allocatable :: ORDSCL
      character*2, dimension(:), allocatable :: ORDSHL
      character*16, dimension(:), allocatable :: ORDTLB
      logical, dimension(:), allocatable :: ORDLCORE
!
! Output scalars:
      integer NFDShells
!
! Local scalars:
      integer Ishell,IEXP,IPRIM,NEXP,NLOC,lenstr,nrec
      character*16 string
!
! Begin:
!
! Create work arrays
      allocate (ORDATM(1:Nshells), ORDBGN(1:Nshells), ORDEND(1:Nshells), ORDCC1(1:Nprimitives), ORDGAU(1:Nprimitives), &
                 ORDSCL(1:Nshells), ORDSHL(1:Nshells), ORDTLB(1:Nshells), ORDLCORE(1:Nshells))
!
! Find all SP shells and split to S and P
      do Ishell=1,Nshells
        nrec=index(BasisW%shell(Ishell)%SHLTLB,'SP')
        if(nrec.ne.0)then
        lenstr=len_trim(BasisW%shell(Ishell)%SHLTLB)
        string(1:)=BasisW%shell(Ishell)%SHLTLB(1:)
        BasisW%shell(Ishell)%SHLTLB(nrec+1:lenstr)=string(nrec+2:lenstr)
        BasisW%shell(Ishell+1)%SHLTLB(nrec:lenstr)=string(nrec+1:lenstr)
        end if
      end do ! Ishell
!
! First find all F's
      NEXP=0
      NLOC=0
      NFDShells=0
      do Ishell=1,Nshells
        if(BasisW%shell(Ishell)%shltyp.EQ.'F ')then
        NFDShells=NFDShells+1
        NLOC=NLOC+1
        ORDSHL(NLOC)=BasisW%shell(Ishell)%shltyp
        ORDTLB(NLOC)=BasisW%shell(Ishell)%SHLTLB
        ORDATM(NLOC)=BasisW%shell(Ishell)%SHLATM
        ORDSCL(NLOC)=BasisW%shell(Ishell)%SHLSCL
        ORDLCORE(NLOC)=BasisW%shell(Ishell)%LCOREshl
        ORDBGN(NLOC)=NEXP+1
        do IEXP=BasisW%shell(Ishell)%EXPBGN,BasisW%shell(Ishell)%EXPEND
        NEXP=NEXP+1
        ORDGAU(NEXP)=BasisW%gaussian(Iexp)%exp
        ORDCC1(NEXP)=BasisW%gaussian(Iexp)%contrc
        end do ! IEXP
        ORDEND(NLOC)=NEXP
        end if
      end do ! Ishell
!
! Find all D's
      do Ishell=1,Nshells
        if(BasisW%shell(Ishell)%shltyp.EQ.'D ')then
        NFDShells=NFDShells+1
        NLOC=NLOC+1
        ORDSHL(NLOC)=BasisW%shell(Ishell)%shltyp
        ORDTLB(NLOC)=BasisW%shell(Ishell)%SHLTLB
        ORDATM(NLOC)=BasisW%shell(Ishell)%SHLATM
        ORDSCL(NLOC)=BasisW%shell(Ishell)%SHLSCL
        ORDLCORE(NLOC)=BasisW%shell(Ishell)%LCOREshl
        ORDBGN(NLOC)=NEXP+1
        do IEXP=BasisW%shell(Ishell)%EXPBGN,BasisW%shell(Ishell)%EXPEND
        NEXP=NEXP+1
        ORDGAU(NEXP)=BasisW%gaussian(Iexp)%exp
        ORDCC1(NEXP)=BasisW%gaussian(Iexp)%contrc
        end do ! IEXP
        ORDEND(NLOC)=NEXP
        end if
      end do ! Ishell
!
! Find all SP's
      do Ishell=1,Nshells
        if(BasisW%shell(Ishell)%shltyp.EQ.'SP')then
        NLOC=NLOC+1
        ORDSHL(NLOC)=BasisW%shell(Ishell)%shltyp
        ORDTLB(NLOC)=BasisW%shell(Ishell)%SHLTLB
        ORDATM(NLOC)=BasisW%shell(Ishell)%SHLATM
        ORDSCL(NLOC)=BasisW%shell(Ishell)%SHLSCL
        ORDLCORE(NLOC)=BasisW%shell(Ishell)%LCOREshl
        ORDBGN(NLOC)=NEXP+1
        do IEXP=BasisW%shell(Ishell)%EXPBGN,BasisW%shell(Ishell)%EXPEND
        NEXP=NEXP+1
        ORDGAU(NEXP)=BasisW%gaussian(Iexp)%exp
        ORDCC1(NEXP)=BasisW%gaussian(Iexp)%contrc
        end do ! IEXP
        ORDEND(NLOC)=NEXP
        end if
      end do ! Ishell
!
! Find all P's
      do Ishell=1,Nshells
        if(BasisW%shell(Ishell)%shltyp.EQ.'P ')then
        NLOC=NLOC+1
        ORDSHL(NLOC)=BasisW%shell(Ishell)%shltyp
        ORDTLB(NLOC)=BasisW%shell(Ishell)%SHLTLB
        ORDATM(NLOC)=BasisW%shell(Ishell)%SHLATM
        ORDSCL(NLOC)=BasisW%shell(Ishell)%SHLSCL
        ORDLCORE(NLOC)=BasisW%shell(Ishell)%LCOREshl
        ORDBGN(NLOC)=NEXP+1
        do IEXP=BasisW%shell(Ishell)%EXPBGN,BasisW%shell(Ishell)%EXPEND
        NEXP=NEXP+1
        ORDGAU(NEXP)=BasisW%gaussian(Iexp)%exp
        ORDCC1(NEXP)=BasisW%gaussian(Iexp)%contrc
        end do ! IEXP
        ORDEND(NLOC)=NEXP
        end if
      end do ! Ishell
!
! Find all S's
      do Ishell=1,Nshells
        if(BasisW%shell(Ishell)%shltyp.EQ.'S ')then
        NLOC=NLOC+1
        ORDSHL(NLOC)=BasisW%shell(Ishell)%shltyp
        ORDTLB(NLOC)=BasisW%shell(Ishell)%SHLTLB
        ORDATM(NLOC)=BasisW%shell(Ishell)%SHLATM
        ORDSCL(NLOC)=BasisW%shell(Ishell)%SHLSCL
        ORDLCORE(NLOC)=BasisW%shell(Ishell)%LCOREshl
        ORDBGN(NLOC)=NEXP+1
        do IEXP=BasisW%shell(Ishell)%EXPBGN,BasisW%shell(Ishell)%EXPEND
        NEXP=NEXP+1
        ORDGAU(NEXP)=BasisW%gaussian(Iexp)%exp
        ORDCC1(NEXP)=BasisW%gaussian(Iexp)%contrc
        end do ! IEXP
        ORDEND(NLOC)=NEXP
        end if
      end do ! Ishell
!
! Now copy the reordered shells and atomic numbers.
      do Ishell=1,Nshells
        BasisW%shell(Ishell)%shltyp=ORDSHL(Ishell)
        BasisW%shell(Ishell)%SHLTLB=ORDTLB(Ishell)
        BasisW%shell(Ishell)%SHLATM=ORDATM(Ishell)
        BasisW%shell(Ishell)%SHLSCL=ORDSCL(Ishell)
        BasisW%shell(Ishell)%EXPBGN=ORDBGN(Ishell)
        BasisW%shell(Ishell)%EXPEND=ORDEND(Ishell)
        BasisW%shell(Ishell)%LCOREshl=ORDLCORE(Ishell)
      end do ! Ishell
!
      do IPRIM=1,Nprimitives
        BasisW%gaussian(Iprim)%exp=ORDGAU(IPRIM)
        BasisW%gaussian(Iprim)%contrc=ORDCC1(IPRIM)
      end do ! IPRIM
!
!     if(MUN_prtlev.gt.0)write(UNIout,'(a)')'The basis set has now been re-ordered FDPS'
!
! Delete work arrays:
      deallocate (ORDATM, ORDBGN, ORDEND, ORDCC1, ORDGAU, ORDSCL, ORDSHL, ORDTLB, ORDLCORE)
!
! End of routine SORT_SHELLS
      return
      end subroutine sort_shells
      subroutine SAVE_shell
!*****************************************************************************************************************
!     Date last modified: February 7, 2001                                                          Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Set basis set defaults (can only be called after a molecule has been defined.                 *
!*****************************************************************************************************************
! Modules:

      implicit none
!
! Local scalars:
      integer :: Igauss
      integer :: lentyp
      integer :: Nshells
!
! Begin:
      BasisW%Nshells=BasisW%Nshells+1
      Nshells=BasisW%Nshells
      if(Nshells.gt.MAX_shells)then
        stop 'Number of shells exceeds MAX_shells'
      end if
      lentyp=len_trim(Type(Ishell))
      BasisW%shell(Nshells)%SHLTLB=Type(Ishell)(1:lentyp)
      BasisW%shell(Nshells)%SHLATM=ZANLIST(Ielem)
      BasisW%shell(Nshells)%SHLSCL=ScaleF(Ishell)
      BasisW%shell(Nshells)%EXPBGN=Nprimitives+1
      do Igauss=IGstart,IGend
        Nprimitives=Nprimitives+1
        if(Nprimitives.gt.MAX_primitives)then
          stop 'Number of shells exceeds MAX_primitives'
        end if
        BasisW%gaussian(Nprimitives)%exp=GEXP(Igauss)
        BasisW%gaussian(Nprimitives)%contrc=C1(Igauss)
      end do ! Igauss
      BasisW%shell(Nshells)%EXPEND=Nprimitives
! First check for polarization functions _ NOTE: the order is important
      if(index(Type(Ishell),'P-POL').ne.0)then
        BasisW%shell(Nshells)%SHLTYP='P '
      else if(index(Type(Ishell),'D-POL').ne.0)then
        BasisW%shell(Nshells)%SHLTYP='D '
      else if(index(Type(Ishell),'F-POL').ne.0)then
        BasisW%shell(Nshells)%SHLTYP='F '
      else if(index(Type(Ishell),'SP-DIFFUSE').ne.0)then
        BasisW%shell(Nshells)%SHLTYP='S '
! Split the sp-shell
        Nshells=Nshells+1
        if(Nshells.gt.MAX_shells)then
          stop 'Number of shells exceeds MAX_shells'
        end if
        BasisW%shell(Nshells)%SHLTLB=Type(Ishell)(1:lentyp)
        BasisW%shell(Nshells)%SHLTYP='P '
        BasisW%shell(Nshells)%SHLATM=ZANLIST(IELEM)
        BasisW%shell(Nshells)%SHLSCL=ScaleF(Ishell)
        BasisW%shell(Nshells)%EXPBGN=Nprimitives+1
        do Igauss=IGstart,IGend
          Nprimitives=Nprimitives+1
          if(Nprimitives.gt.MAX_primitives)then
            stop 'Number of shells exceeds MAX_primitives'
          end if
          BasisW%gaussian(Nprimitives)%exp=GEXP(Igauss)
          BasisW%gaussian(Nprimitives)%contrc=C2(Igauss)
        end do ! Igauss
        BasisW%shell(Nshells)%EXPEND=Nprimitives
      else if(index(Type(Ishell),'S-DIFFUSE').ne.0)then
        BasisW%shell(Nshells)%SHLTYP='S '
      else if(index(Type(Ishell),'SP').ne.0)then
        BasisW%shell(Nshells)%SHLTYP='S '
! Split the sp-shell
        Nshells=Nshells+1
        BasisW%shell(Nshells)%SHLTLB=Type(Ishell)(1:lentyp)
        BasisW%shell(Nshells)%SHLTYP='P '
        BasisW%shell(Nshells)%SHLATM=ZANLIST(IELEM)
        BasisW%shell(Nshells)%SHLSCL=ScaleF(Ishell)
        BasisW%shell(Nshells)%EXPBGN=Nprimitives+1
        do Igauss=IGstart,IGend
          Nprimitives=Nprimitives+1
          if(Nprimitives.gt.MAX_primitives)then
            stop 'Number of shells exceeds MAX_primitives'
          end if
          BasisW%gaussian(Nprimitives)%exp=GEXP(Igauss)
          BasisW%gaussian(Nprimitives)%contrc=C2(Igauss)
        end do ! Igauss
        BasisW%shell(Nshells)%EXPEND=Nprimitives
      else if(index(Type(Ishell),'S').ne.0)then
        BasisW%shell(Nshells)%SHLTYP='S '
      else if(index(Type(Ishell),'P').ne.0)then
        BasisW%shell(Nshells)%SHLTYP='P '
      else if(index(Type(Ishell),'D').ne.0)then
        BasisW%shell(Nshells)%SHLTYP='D '
      else if(index(Type(Ishell),'F').ne.0)then
        BasisW%shell(Nshells)%SHLTYP='F '
      else
        write(UNIout,'(1X,A)')' error_SAVE_shell> Invalid shell type ',Type(Ishell)
      end if
      BasisW%Nshells=Nshells
      return
      end subroutine SAVE_shell
! END CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end MODULE MENU_BLD_basis
      subroutine COPY_BASIS_SET (BasisIN, BasisOUT)
!***********************************************************************
!     Date last modified: February 7, 2001                 Version 1.0 *
!     Author: R. A. Poirier                                            *
!     Description: Move basis set IN to basis set OUT.                 *
!***********************************************************************
! Modules:
      USE type_basis_set

      implicit none
!
! Input object:
      type (basis_set) BasisIN,BasisOUT
!
! Local scalars:
      integer Iatmshl,Ibasis,ICCoeff,Iprim,Ishell
!
! Begin:
      call PRG_manager ('enter', 'COPY_BASIS_SET', 'UTILITY')
!
! Now define the scalars
      BasisOUT%name=BasisIN%name
      BasisOUT%NFDShells=BasisIN%NFDShells
      BasisOUT%Nshells=BasisIN%Nshells
      BasisOUT%Natmshls=BasisIN%Natmshls
      BasisOUT%Nprimitives=BasisIN%Nprimitives
      BasisOUT%NDorbs=BasisIN%NDorbs
      BasisOUT%NForbs=BasisIN%NForbs
      BasisOUT%NBasis=BasisIN%Nbasis
      BasisOUT%NCCOEFFS=BasisIN%NCCOEFFS
!
! Allocate the basis set (deallocate if allocated)
      if(associated(BasisOUT%gaussian))then
        deallocate (BasisOUT%gaussian, BasisOUT%shell, BasisOUT%atmshl, BasisOUT%AOtoATOM, BasisOUT%AOtype, &
                    BasisOUT%CCbyAO, BasisOUT%ccpntr)
      end if
      allocate (BasisOUT%gaussian(1:BasisOUT%Nprimitives), BasisOUT%shell(1:BasisOUT%Nshells), &
                BasisOUT%ccpntr(1:BasisOUT%Nshells), &
                BasisOUT%atmshl(1:BasisOUT%Natmshls), BasisOUT%AOtoATOM(BasisOUT%Nbasis), &
                BasisOUT%AOtype(BasisOUT%Nbasis), BasisOUT%CCbyAO(BasisOUT%NCCOEFFS), &
                BasisOUT%minEABP(BasisIN%Nshells,BasisOUT%Nshells))
!
! Now copy all the arrays
      do Ishell=1,BasisOUT%Nshells
        BasisOUT%shell(Ishell)%SHLTYP=BasisIN%shell(Ishell)%shltyp
        BasisOUT%shell(Ishell)%SHLTLB=BasisIN%shell(Ishell)%SHLTLB
        BasisOUT%shell(Ishell)%SHLATM=BasisIN%shell(Ishell)%SHLATM
        BasisOUT%shell(Ishell)%SHLSCL=BasisIN%shell(Ishell)%SHLSCL
        BasisOUT%shell(Ishell)%EXPBGN=BasisIN%shell(Ishell)%EXPBGN
        BasisOUT%shell(Ishell)%EXPEND=BasisIN%shell(Ishell)%EXPEND
        BasisOUT%shell(Ishell)%Xtype=BasisIN%shell(Ishell)%Xtype
        BasisOUT%shell(Ishell)%Xstart=BasisIN%shell(Ishell)%Xstart
        BasisOUT%shell(Ishell)%XEND=BasisIN%shell(Ishell)%XEND
        BasisOUT%shell(Ishell)%frstSHL=BasisIN%shell(Ishell)%frstSHL
        BasisOUT%shell(Ishell)%lastSHL=BasisIN%shell(Ishell)%lastSHL
        BasisOUT%ccpntr(Ishell)=BasisIN%ccpntr(Ishell)
      end do ! Ishell

      BasisOUT%minEABP(1:BasisOUT%Nshells,1:BasisOUT%Nshells)=BasisIN%minEABP(1:BasisIN%Nshells,1:BasisIN%Nshells)
!
      do IPRIM=1,BasisOUT%Nprimitives
        BasisOUT%gaussian(Iprim)%exp=BasisIN%gaussian(Iprim)%exp
        BasisOUT%gaussian(Iprim)%contrc=BasisIN%gaussian(Iprim)%contrc
      end do ! IPRIM

      do Iatmshl=1,BasisOUT%Natmshls
        BasisOUT%atmshl(Iatmshl)%ATMLST=BasisIN%atmshl(Iatmshl)%ATMLST
        BasisOUT%atmshl(Iatmshl)%frstAO=BasisIN%atmshl(Iatmshl)%frstAO
        BasisOUT%atmshl(Iatmshl)%lastAO=BasisIN%atmshl(Iatmshl)%lastAO
      end do ! Iatmshl=1,BasisOUT%Natmshls

      do Ibasis=1,BasisOUT%Nbasis
        BasisOUT%AOtoATOM(Ibasis)=BasisIN%AOtoATOM(Ibasis)
        BasisOUT%AOtype(Ibasis)=BasisIN%AOtype(Ibasis)
      end do ! Ibasis

      do ICCoeff=1,BasisOUT%NCCOEFFS
        BasisOUT%CCbyAO(ICCoeff)=BasisIN%CCbyAO(ICCoeff)
      end do ! Ibasis

!
! End of routine COPY_BASIS_SET
      call PRG_manager ('exit', 'COPY_BASIS_SET', 'UTILITY')
      return
      end subroutine COPY_BASIS_SET
      subroutine MOVE_BASIS_SET (BasisIN, BasisOUT)
!***********************************************************************
!     Date last modified: February 7, 2001                 Version 1.0 *
!     Author: R. A. Poirier                                            *
!     Description: Move basis set IN to basis set OUT.                 *
!***********************************************************************
! Modules:
      USE type_basis_set
      USE QM_objects

      implicit none
!
! Input object:
      type (basis_set) BasisIN,BasisOUT
!
! Local scalars:
      integer Iatmshl,Iprim,Ishell
!
! Begin:
      call PRG_manager ('enter', 'MOVE_BASIS_SET', 'UTILITY')
!
! Now define the scalars
      BasisOUT%name=BasisIN%name
      BasisOUT%NFDShells=BasisIN%NFDShells
      BasisOUT%Nshells=BasisIN%Nshells
      BasisOUT%Natmshls=BasisIN%Natmshls
      BasisOUT%Nprimitives=BasisIN%Nprimitives
      BasisOUT%NDorbs=BasisIN%NDorbs
      BasisOUT%NForbs=BasisIN%NForbs
      BasisOUT%NBasis=BasisIN%Nbasis
!
! Allocate the basis set (deallocate if allocated)
      if(associated(BasisOUT%gaussian))then
        deallocate (BasisOUT%gaussian, BasisOUT%shell, BasisOUT%atmshl)
      end if
      allocate (BasisOUT%gaussian(1:BasisOUT%Nprimitives), BasisOUT%shell(1:BasisOUT%Nshells), &
                BasisOUT%atmshl(1:BasisOUT%Natmshls))
!
! Now copy all the arrays
      do Ishell=1,BasisOUT%Nshells
        BasisOUT%shell(Ishell)%SHLTYP=BasisIN%shell(Ishell)%shltyp
        BasisOUT%shell(Ishell)%SHLTLB=BasisIN%shell(Ishell)%SHLTLB
        BasisOUT%shell(Ishell)%SHLATM=BasisIN%shell(Ishell)%SHLATM
        BasisOUT%shell(Ishell)%SHLSCL=BasisIN%shell(Ishell)%SHLSCL
        BasisOUT%shell(Ishell)%EXPBGN=BasisIN%shell(Ishell)%EXPBGN
        BasisOUT%shell(Ishell)%EXPEND=BasisIN%shell(Ishell)%EXPEND
        BasisOUT%shell(Ishell)%Xtype=BasisIN%shell(Ishell)%Xtype
        BasisOUT%shell(Ishell)%Xstart=BasisIN%shell(Ishell)%Xstart
        BasisOUT%shell(Ishell)%XEND=BasisIN%shell(Ishell)%XEND
        BasisOUT%shell(Ishell)%frstSHL=BasisIN%shell(Ishell)%frstSHL
        BasisOUT%shell(Ishell)%lastSHL=BasisIN%shell(Ishell)%lastSHL
        BasisOUT%shell(Ishell)%LCOREshl=BasisIN%shell(Ishell)%LCOREshl
      end do ! Ishell
!
      do IPRIM=1,BasisOUT%Nprimitives
        BasisOUT%gaussian(Iprim)%exp=BasisIN%gaussian(Iprim)%exp
        BasisOUT%gaussian(Iprim)%contrc=BasisIN%gaussian(Iprim)%contrc
      end do ! IPRIM

      do Iatmshl=1,BasisOUT%Natmshls
        BasisOUT%atmshl(Iatmshl)%ATMLST=BasisIN%atmshl(Iatmshl)%ATMLST
        BasisOUT%atmshl(Iatmshl)%frstAO=BasisIN%atmshl(Iatmshl)%frstAO
        BasisOUT%atmshl(Iatmshl)%lastAO=BasisIN%atmshl(Iatmshl)%lastAO
      end do ! Iatmshl=1,BasisOUT%Natmshls

!
! Deallocate the work arrays
      deallocate (BasisIN%gaussian, BasisIN%shell, BasisIN%atmshl)
!
! End of routine MOVE_BASIS_SET
      call PRG_manager ('exit', 'MOVE_BASIS_SET', 'UTILITY')
      return
      end subroutine MOVE_BASIS_SET
      subroutine RESET_basis_set
!*****************************************************************************************************************
!     Date last modified: February 16, 2000                                                         Version 2.0  *
!     Author: R. A. Poirier                                                                                      *
!     Description: Set up Basis.                                                                                 *
!*****************************************************************************************************************
! Modules:
      USE program_constants
      USE type_basis_set

      implicit none
!
! Local scalars:
!
! Begin:
      call PRG_manager ('enter', 'RESET_basis_set', 'UTILITY')

!     PRG_cycles=0
!     call INI_I2E
!
! End of routine RESET_basis_set
      call PRG_manager ('exit', 'RESET_basis_set', 'UTILITY')
      return
      end subroutine RESET_basis_set
      subroutine GET_basis_set (Basis_nameIN, BasisIN, LPRT_basis)
!***********************************************************************
!     Date last modified: February 6, 2000                 Version 1.0 *
!     Author: R. A. Poirier                                            *
!     Description: Given a basis set name, build the type basis_set.   *
!***********************************************************************
! Modules:
      USE MENU_BLD_basis

      implicit none
!
! Input object:
      type (basis_set) :: BasisIN
      logical :: LPRT_basis
      character(*) :: BASIS_nameIN
!
! Begin:
      call PRG_manager ('enter', 'GET_basis_set', 'UTILITY')
!
! Allocate the work arrays
      allocate (BasisW%gaussian(1:MAX_primitives), BasisW%shell(1:MAX_shells), BasisW%atmshl(1:MAX_shells))
!
! Read the basis set
      allocate (ZANLIST(molecule%Natoms), ELMLIST(molecule%Natoms))
      call Get_element_list
      call GET_BASIC_NAME (BASIS_nameIN)
!
! Initialize:
      BasisW%name=BASIS_nameIN
      BasisW%shell(1:MAX_shells)%SHLTYP='  '
      BasisW%shell(1)%SHLTYP='??'
      BasisW%shell(1:MAX_shells)%SHLTLB='                '
      BasisW%shell(1:MAX_shells)%SHLSCL=ONE
      BasisW%shell(1:MAX_shells)%SHLATM=0
      BasisW%shell(1:MAX_shells)%EXPBGN=0
      BasisW%shell(1:MAX_shells)%EXPEND=0
      BasisW%shell(1:MAX_shells)%LCOREshl=.false.
      BasisW%gaussian(1:MAX_primitives)%exp=ZERO
      BasisW%gaussian(1:MAX_primitives)%CONTRC=ZERO

      BasisW%Nprimitives=0
      BasisW%Nshells=0
      Nprimitives=0
!
! Build the array that identifies core shells before sorting
! Read the basis set from the appropriate file
      if(index(BASIS_nameIN, 'STO').NE.0)then
        call BLD_STO_BASIS
      else
        call BLD_GEN_BASIS
      end if
      BasisW%Nprimitives=Nprimitives
      call LOAD_BASIS_SET (BASIS_nameIN)
!
! Now move BasisW to BasisIN
      call MOVE_BASIS_SET (BasisW, BasisIN)
!
      if(LPRT_basis)then
        call PRT_BASIS (BasisIN)
      end if
!
      call NORMALIZE_BASIS (BasisIN, 1)
!
! Build contraction coefficients by AO
      call BLD_CC_BY_AO (BasisIN)
!
! Build ao labels
      call BLD_AOtype (BasisIN)
!
! Now build AO to atom pointer
      call BLD_AOtoATOM (BasisIN)
!
! Now intialize the integral routines
      call MIN_GEXP (BasisIN)        ! Depends on the basis set (not used)
!
! End of routine GET_basis_set
      call PRG_manager ('exit', 'GET_basis_set', 'UTILITY')
      return
      end subroutine GET_basis_set
      subroutine Basis_set_file_name (BasisIN, BasisOUT)
!*************************************************************************************************
!     Date last modified: May 4, 2016                                                            *
!     Author: R. A. Poirier                                                                      *
!     Description: Modify the basis set name to a format to be used in file names                *
!*************************************************************************************************
! Modules:
      USE MENU_BLD_basis

      implicit none
!
! Input scalar:
      character(*) :: BasisIN,BasisOUT
!
! Local scalars:
      integer :: ICharIN,ICharOUT,lenstr
      logical :: LPolarization
      character(len=16) :: Polarization
      character(len=len_trim(BasisIN)) :: BasisTEMP
!
! Begin:
      call PRG_manager ('enter', 'Basis_set_file_name', 'UTILITY')
!
      LPolarization=.false.
      ICharOUT=index(BasisIN,'(')
      if(ICharOUT.ne.0)then
        Polarization=BasisIN(ICharOUT:)
        LPolarization=.true.
        ICharOUT=ICharOUT-1
        BasisTEMP(1:)=BasisIN(1:ICharOUT)
      else 
        BasisTEMP(1:)=BasisIN(1:)
      end if
      call CNV_SLtoU (BasisTEMP)
      lenstr=len_trim(BasisTEMP)
      BasisOUT(1:)=' '
!
! Deal with special cases first
      ICharOUT=index(BasisTEMP,'**')
      if(ICharOUT.ne.0)then
        BasisTEMP(ICharOUT:)='dp'
      end if
      ICharOUT=index(BasisTEMP,'*')
      if(ICharOUT.ne.0)then
        BasisTEMP(ICharOUT:)='d'
      end if
      ICharOUT=index(BasisTEMP,'/')
      if(ICharOUT.ne.0)then
        BasisTEMP(ICharOUT:)='_'
      end if
!
! Other special cases
      if(index(BasisTEMP,'AUG-CC-PVDZ').ne.0)then
        BasisTEMP(1:)='aug_cc_pVDZ'
      else if(index(BasisTEMP,'AUG-CC-PVTZ').ne.0)then
        BasisTEMP(1:)='aug_cc_pVTZ'
      else if(index(BasisTEMP,'AUG-CC-PVTZ-MODP').ne.0)then
        BasisTEMP(1:)='aug_cc_pVTZ_modp'
      else if(index(BasisTEMP,'AUG(S,P)-CC-PVTZ-MODP').ne.0)then
        BasisTEMP(1:)='augsp_cc_pVTZ_modp'
      else if(index(BasisTEMP,'AUG-CC-PCVTZ').ne.0)then
        BasisTEMP(1:)='aug_cc_pCVTZ'
      else if(index(BasisTEMP,'AUG-CC-PVQZ').ne.0)then
        BasisTEMP(1:)='aug_cc_pVQZ'
      else if(index(BasisTEMP,'AUG-CC-PV5Z').ne.0)then
        BasisTEMP(1:)='aug_cc_pV5Z'
      else if(index(BasisTEMP,'CC-PVDZ').ne.0)then
        BasisTEMP(1:)='cc_pVDZ'
      else if(index(BasisTEMP,'CC-PVTZ').ne.0)then
        BasisTEMP(1:)='cc_pVTZ'
      else if(index(BasisTEMP,'CC-PVQZ').ne.0)then
        BasisTEMP(1:)='cc_pVQZ'
      else if(index(BasisTEMP,'LANL2DZ').ne.0)then ! Do nothing
        BasisTEMP(1:)='LANL2DZ'
      else if(index(BasisTEMP,'SDD').ne.0)then ! Do nothing
        BasisTEMP(1:)='SDD'
      else if(index(BasisTEMP,'SIMPLE').ne.0)then
        BasisTEMP(1:)='simple'
      end if
!
      if(LPolarization)then
        call CNV_SUtoL (Polarization)
        BasisTEMP=BasisTEMP(1:len_trim(BasisTEMP))//Polarization(1:len_trim(Polarization))
      end if
!
      lenstr=len_trim(BasisTEMP)
      ICharOUT=0
      do ICharIN=1,lenstr
      if(BasisTEMP(ICharIN:ICharIN).eq.'+')then
        ICharOUT=ICharOUT+1
        BasisOUT(ICharOUT:ICharOUT)='p'
      else if(BasisTEMP(ICharIN:ICharIN).eq.'(')then
      else if(BasisTEMP(ICharIN:ICharIN).eq.')')then
      else if(BasisTEMP(ICharIN:ICharIN).eq.',')then
      else if(BasisTEMP(ICharIN:ICharIN).eq.'-')then
      else
        ICharOUT=ICharOUT+1
        BasisOUT(ICharOUT:ICharOUT)=BasisTEMP(ICharIN:ICharIN)
      end if
      end do
!
! End of routine Basis_set_file_name
      call PRG_manager ('exit', 'Basis_set_file_name', 'UTILITY')
      return
      end subroutine Basis_set_file_name
