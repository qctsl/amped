      subroutine BLD_pVQZ12e_BASIS
!*****************************************************************************************************************
!     Date last modified: May 16, 2011                                                              Version 2.0  *
!     Author: V. R. Grandy, R. A. Poirier                                                                        *
!     Description: Define a basis set.                                                                           *
!*****************************************************************************************************************
! Modules:
      USE MENU_BLD_basis

      implicit none
!
! Local scalars:
      integer :: Znum
!
! Begin:
      Type(1:30)(1:16)=' '
! 1) Loop over all the elements in the molecule
      do IELEM=1,NELEMTS
!       ELEMENT=ELMLIST(IELEM)
        Znum=CARTESIAN(1)%Atomic_number
        call SET_BASIS_pVQZ12e

        write(6,'(a,i4)')' Znum: ',Znum
        ScaleF(1)=0.2297D0*Znum+0.0558D0
        ScaleF(2)=0.2542D0*Znum-0.1482D0
        ScaleF(3)=0.4727D0*Znum-0.6575D0
        ScaleF(4)=0.4004D0*Znum-0.2153D0
        ScaleF(5)=3.6286D0*Znum-6.7413D0
      
        IGstart=1
        do Ishell=1,NshellIN
          IGend=IGstart+Ngauss(Ishell)-1
            do Igauss=IGstart,IGend
              Gexp(Igauss)=Gexp(Igauss)*ScaleF(Ishell)*ScaleF(Ishell)
            end do
            call SAVE_shell
          IGstart=IGend+1
        end do ! Ishell
      end do ! IELEM
!
! End of routine BLD_pVQZ12e_BASIS
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine SET_BASIS_pVQZ12e
!*****************************************************************************************************************
!     Date last modified: May 16, 2011                                                              Version 2.0  *
!     Author: V. R. Grandy, R.A. Poirier                                                                         *
!     Description: Set basis set defaults (can only be called after a molecule has been defined.                 *
!*****************************************************************************************************************
! Modules:

      implicit none
!
! Begin:
      NshellIN=13
      Type(1)='1S'
      Ngauss(1)=13
      Gexp(1)=327600.0000000
      C1(1)=0.309608D-04
      Gexp(2)=49050.0000000
      C1(2)=0.240954D-03
      Gexp(3)=11150.0000000
      C1(3)=0.126660D-02
      Gexp(4)=3152.0000000
      C1(4)=0.533359D-02
      Gexp(5)=1025.0000000
      C1(5)=0.190770D-01
      Gexp(6)=368.8000000
      C1(6)=0.588058D-01
      Gexp(7)=143.2000000
      C1(7)=0.151454D+00
      Gexp(8)=58.9600000
      C1(8)=0.300716D+00
      Gexp(9)=25.4000000
      C1(9)=0.381149D+00
      Gexp(10)=11.1500000
      C1(10)=0.213584D+00
      Gexp(11)=4.0040000
      C1(11)=0.231210D-01
      Gexp(12)=1.7010000
      C1(12)=-0.230757D-02
      Gexp(13)=0.7060000
      C1(13)=0.128900D-02

      Type(2)='2S'
      Ngauss(2)=13
      Gexp(14)=327600.0000000
      C1(14)=-0.783173D-05
      Gexp(15)=49050.0000000
      C1(15)=-0.607935D-04
      Gexp(16)=11150.0000000
      C1(16)=-0.321197D-03
      Gexp(17)=3152.0000000
      C1(17)=-0.134955D-02
      Gexp(18)=1025.0000000
      C1(18)=-0.490570D-02
      Gexp(19)=368.8000000
      C1(19)=-0.153561D-01
      Gexp(20)=143.2000000
      C1(20)=-0.423409D-01
      Gexp(21)=58.9600000
      C1(21)=-0.940603D-01
      Gexp(22)=25.4000000
      C1(22)=-0.163425D+00
      Gexp(23)=11.1500000
      C1(23)=-0.124754D+00
      Gexp(24)=4.0040000
      C1(24)=0.235623D+00
      Gexp(25)=1.7010000
      C1(25)=0.577563D+00
      Gexp(26)=0.7060000
      C1(26)=0.335232D+00

      Type(3)='3S'
      Ngauss(3)=13
      Gexp(27)=327600.0000000
      C1(27)=0.150908D-05
      Gexp(28)=49050.0000000
      C1(28)=0.117134D-04
      Gexp(29)=11150.0000000
      C1(29)=0.618980D-04
      Gexp(30)=3152.0000000
      C1(30)=0.260088D-03
      Gexp(31)=1025.0000000
      C1(31)=0.946218D-03
      Gexp(32)=368.8000000
      C1(32)=0.296595D-02
      Gexp(33)=143.2000000
      C1(33)=0.821245D-02
      Gexp(34)=58.9600000
      C1(34)=0.183977D-01
      Gexp(35)=25.4000000
      C1(35)=0.326657D-01
      Gexp(36)=11.1500000
      C1(36)=0.257315D-01
      Gexp(37)=4.0040000
      C1(37)=-0.535351D-01
      Gexp(38)=1.7010000
      C1(38)=-0.156895D+00
      Gexp(39)=0.7060000
      C1(39)=-0.206659D+00

      Type(4)='4S'
      Ngauss(4)=1
      Gexp(40)=0.1410000
      C1(40)=1.0000000

      Type(5)='5S'
      Ngauss(5)=1
      Gexp(41)=0.0680800
      C1(41)=1.0000000

      Type(6)='6S'
      Ngauss(6)=1
      Gexp(42)=0.0306300
      C1(42)=1.0000000

      Type(7)='7S'
      Ngauss(7)=1
      Gexp(43)=0.0123900
      C1(43)=1.0000000

      Type(8)='1P'
      Ngauss(8)=9
      Gexp(44)=539.6000000
      C1(44)=0.833969D-03
      Gexp(45)=127.9000000
      C1(45)=0.689215D-02
      Gexp(46)=41.0200000
      C1(46)=0.337874D-01
      Gexp(47)=15.2500000
      C1(47)=0.114401D+00
      Gexp(48)=6.1660000
      C1(48)=0.259514D+00
      Gexp(49)=2.5610000
      C1(49)=0.385095D+00
      Gexp(50)=1.0600000
      C1(50)=0.335373D+00
      Gexp(51)=0.4176000
      C1(51)=0.110641D+00
      Gexp(52)=0.2690000
      C1(52)=-0.121315D-01

      Type(9)='2P'
      Ngauss(9)=9
      Gexp(53)=539.6000000
      C1(53)=-0.132076D-03
      Gexp(54)=127.9000000
      C1(54)=-0.109538D-02
      Gexp(55)=41.0200000
      C1(55)=-0.539495D-02
      Gexp(56)=15.2500000
      C1(56)=-0.185572D-01
      Gexp(57)=6.1660000
      C1(57)=-0.427375D-01
      Gexp(58)=2.5610000
      C1(58)=-0.647684D-01
      Gexp(59)=1.0600000
      C1(59)=-0.627818D-01
      Gexp(60)=0.4176000
      C1(60)=-0.244912D-01
      Gexp(61)=0.2690000
      C1(61)=0.104761D+00

      Type(10)='3P'
      Ngauss(10)=1
      Gexp(62)=0.1223000
      C1(62)=1.0000000

      Type(11)='4P'
      Ngauss(11)=1
      Gexp(63)=0.0547600
      C1(63)=1.0000000

      Type(12)='5P'
      Ngauss(12)=1
      Gexp(64)=0.0238800
      C1(64)=1.0000000

      Type(13)='6P'
      Ngauss(13)=1
      Gexp(65)=0.0070600
      C1(65)=1.0000000

      select case(Znum) ! Can USE Znum
      case(1)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(2)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(3)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(4)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(5)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(6)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(7)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(8)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(9)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(10)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(11)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(12)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(13)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(14)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(15)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(16)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(17)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(18)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(19)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(20)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(21)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(22)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(23)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(24)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(25)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(26)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(27)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(28)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(29)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(30)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(31)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(32)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(33)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(34)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(35)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case(36)
        ScaleF(1)=  1.00000000
        ScaleF(2)=  1.00000000
        ScaleF(3)=  1.00000000
        ScaleF(4)=  1.00000000
        ScaleF(5)=  1.00000000
        ScaleF(6)=  1.00000000
        ScaleF(7)=  1.00000000
        ScaleF(8)=  1.00000000
        ScaleF(9)=  1.00000000
        ScaleF(10)=  1.00000000
        ScaleF(11)=  1.00000000
        ScaleF(12)=  1.00000000
        ScaleF(13)=  1.00000000

      case default
        write(UNIout,'(a)')'pVQZ12e basis set not available for element, ',Element(1:len_trim(Element))
        call PRG_stop ('pVQZ12e basis set not available for element')
      end select
      return
      end subroutine SET_BASIS_pVQZ12e
! END CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine BLD_pVQZ12e_BASIS
