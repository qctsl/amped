      SUBROUTINE BLD_AIMDFT_objects
!*******************************************************************************
!       Date last modified July 22, 2015                                       *
!       Author: Ibrahim Awad                                                   *
!       Description:                                                           * 
!*******************************************************************************
! MODULEs:
      USE program_objects

      implicit none

! Local scalar:
      integer :: Iobject
!
! Begin:
      OBJ_AIMDFT(1:Max_objects)%modality = 'other'
      OBJ_AIMDFT(1:Max_objects)%class = 'AIMDFT'
      OBJ_AIMDFT(1:Max_objects)%depend = .true.
      NAIMDFTobjects = 0
!
! Class of (AIMDFT)
      NAIMDFTobjects = NAIMDFTobjects + 1
      OBJ_AIMDFT(NAIMDFTobjects)%name = 'FRAGCART'
      OBJ_AIMDFT(NAIMDFTobjects)%modality = 'DBASE'
      OBJ_AIMDFT(NAIMDFTobjects)%routine = 'Add_To_DB'
      
      NAIMDFTobjects = NAIMDFTobjects + 1
      OBJ_AIMDFT(NAIMDFTobjects)%name = 'FRAGCART'
      OBJ_AIMDFT(NAIMDFTobjects)%modality = 'PM'
      OBJ_AIMDFT(NAIMDFTobjects)%routine = 'PRINTPM0'

      NAIMDFTobjects = NAIMDFTobjects + 1
      OBJ_AIMDFT(NAIMDFTobjects)%name = 'FRAGCART'
      OBJ_AIMDFT(NAIMDFTobjects)%modality = 'GRIDS'
      OBJ_AIMDFT(NAIMDFTobjects)%routine = 'Store_result_direct'

      NAIMDFTobjects = NAIMDFTobjects + 1
      OBJ_AIMDFT(NAIMDFTobjects)%name = 'MOLECULE'
      OBJ_AIMDFT(NAIMDFTobjects)%modality = 'BUILD'
      OBJ_AIMDFT(NAIMDFTobjects)%routine = 'CalcMoleProperties'

      NAIMDFTobjects = NAIMDFTobjects + 1
      OBJ_AIMDFT(NAIMDFTobjects)%name = 'MOLECULE'
      OBJ_AIMDFT(NAIMDFTobjects)%modality = 'ROTATE'
      OBJ_AIMDFT(NAIMDFTobjects)%routine = 'get_databaseform'

      NAIMDFTobjects = NAIMDFTobjects + 1
      OBJ_AIMDFT(NAIMDFTobjects)%name = 'FRAGMENT'
      OBJ_AIMDFT(NAIMDFTobjects)%modality = 'DISPLAY'
      OBJ_AIMDFT(NAIMDFTobjects)%routine = 'FragmentsDisplay'

      NAIMDFTobjects = NAIMDFTobjects + 1
      OBJ_AIMDFT(NAIMDFTobjects)%name = 'FRAGMENT'
      OBJ_AIMDFT(NAIMDFTobjects)%modality = 'BUILD'
      OBJ_AIMDFT(NAIMDFTobjects)%routine = 'Build_FragFiles'

!     NAIMDFTobjects = NAIMDFTobjects + 1
!     OBJ_AIMDFT(NAIMDFTobjects)%name = 'FRAGCART'
!     OBJ_AIMDFT(NAIMDFTobjects)%modality = 'MESH'
!     OBJ_AIMDFT(NAIMDFTobjects)%routine = 'DENFRAG'
      
! Dummy Class
      NAIMDFTobjects = NAIMDFTobjects + 1
      OBJ_AIMDFT(NAIMDFTobjects)%name = '?'
      OBJ_AIMDFT(NAIMDFTobjects)%modality = '?'
      OBJ_AIMDFT(NAIMDFTobjects)%routine = '?'
!
      do Iobject=1,NAIMDFTobjects
        OBJ_AIMDFT(Iobject)%exist=.false.
        OBJ_AIMDFT(Iobject)%Current=.false.
      end do

      return
      end SUBROUTINE BLD_AIMDFT_objects
