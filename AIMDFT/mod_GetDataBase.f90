      MODULE GetDataBaseInfo
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:                                                             *
!*******************************************************************************
!     
      USE mod_rotation
      USE AIMDFT_Files
      USE program_constants     
 
      implicit none
      
CONTAINS !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      
      SUBROUTINE GetMoleculeAtomsIndex(MolIdxInfo,&
                                       SearchSym,&
                                       SearchSymNum,&
                                       IndexFoundNum)
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description: Return the required information from index file             *
!                  for the given unique symbols, where diffrent                *
!                  conditions were applied.                                    *
!*******************************************************************************
!       
      USE module_grid_points
      USE NI_defaults
      USE type_Weights
      USE QM_defaults

      
      implicit none
      
      integer, intent(out) :: IndexFoundNum
      integer :: iSymbol
      integer :: File_Unit
      integer :: Reason
      logical :: Lerror
      character (SymMax):: CurrentSearchSymbol
      character (20) :: Radnote
      type(IndexFile) :: CurrentIndexInfo 

      integer, intent(in) :: SearchSymNum
      character (SymMax), dimension (:), allocatable, intent(in) ::  SearchSym     
      type (IndexFile), dimension(:), allocatable, intent(out) :: MolIdxInfo
      
      allocate (MolIdxInfo(Natoms))     

      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
      ! check the database and return index
      IndexFoundNum=0 ! count number of unique symbols in the database for mol.
      MolIdxInfo%Availability=.false.  
      do iSymbol=1, SearchSymNum
         call GET_unit (IndexFilePath, File_Unit, Lerror)
         open(UNIT=File_Unit,file=IndexFilePath, status='old',form='formatted')
         CurrentSearchSymbol=SearchSym(iSymbol)
         read(File_Unit,*)
         do ! read the index file
            read(File_Unit,fmt_index,IOSTAT=Reason)                       &
                                        CurrentIndexInfo%IndexNum,        &
                                        CurrentIndexInfo%Symbol,          &
                                        CurrentIndexInfo%NAtoms,          &
                                        CurrentIndexInfo%GridsNum,        &
                                        CurrentIndexInfo%GridType,        &
                                        CurrentIndexInfo%GridTypeExtra,   &
                                        CurrentIndexInfo%Method,          &
                                        CurrentIndexInfo%BasisSet,        &
                                        CurrentIndexInfo%DEN_Partitioning,&
                                        CurrentIndexInfo%CoreSize,        &
                                        CurrentIndexInfo%OPT
            if (Reason.lt.0) exit
            if (trim(ADJUSTL(CurrentIndexInfo%Symbol)).ne.&
            trim(ADJUSTL(CurrentSearchSymbol))) cycle  ! first condition
            if (trim(ADJUSTL(CurrentIndexInfo%GridType)).ne.&
            trim(ADJUSTL(RADIAL_grid))) cycle  ! second condition
            if (trim(RADIAL_grid).eq.'GILL') then
               Radnote=""
               write(Radnote,'(I2,A1,I2,A1,I2,A1,I3,A1)') NRPoints_Gill, &
                                                    & '(',NApoints_Gill(1),',',&
                                                          NApoints_Gill(2),',',&
                                                          NApoints_Gill(3),')'
               ! second condition for GILL
               if (trim(ADJUSTL(Radnote)).ne.&
               trim(ADJUSTL(CurrentIndexInfo%GridTypeExtra))) cycle  
            end if
            if (trim(ADJUSTL(CurrentIndexInfo%BasisSet)).ne.&
            trim(ADJUSTL(Basis_set_name))) cycle  ! third condition
            if (trim(ADJUSTL(CurrentIndexInfo%DEN_Partitioning)).ne.&
            trim(ADJUSTL(DEN_Partitioning))) cycle  ! forth condition
            if (trim(ADJUSTL(CurrentIndexInfo%OPT)).ne.&
            trim(ADJUSTL(OPT_status))) cycle  ! fifth condition
 
            IndexFoundNum=IndexFoundNum+1
            MolIdxInfo(iSymbol) = CurrentIndexInfo
            MolIdxInfo(iSymbol)%Availability = .true. 
         end do ! end of read index file
         close(UNIT=File_Unit)
      end do ! end loop of condition
      
      write (*,'(/A,I0)') "Number of indexs available in the database    =   ",&
                           IndexFoundNum
      write (*,'(A,I0)') "Number of indexs NOT available in the database =  ", &
                           SearchSymNum-IndexFoundNum
      return
      end SUBROUTINE GetMoleculeAtomsIndex
      SUBROUTINE GetMaxIndex (MaxIndex)
!***********************************************************************
!     Date last modified:                                              *
!     Author: Ibrahim Awad                                             *
!     Description: Return the maximum index number from index file     *
!***********************************************************************
! 
      implicit none
      
      type(IndexFile) :: CurrentIndexInfo 
      integer :: File_unit
      integer :: Reason
      logical :: Lerror
      
      integer, intent(out) :: MaxIndex
      CurrentIndexInfo%IndexNum=0 
      MaxIndex=0
      call GET_unit (trim(IndexFilePath), File_unit, Lerror)
      open(UNIT=File_unit, file=trim(IndexFilePath), &
           status='old' ,form='formatted') 
      read(File_Unit,*)
         Do
            read(File_Unit,fmt_index,IOSTAT=Reason)  &
                                            CurrentIndexInfo%IndexNum,        &
                                            CurrentIndexInfo%Symbol,          &
                                            CurrentIndexInfo%NAtoms,          &
                                            CurrentIndexInfo%GridsNum,        &
                                            CurrentIndexInfo%GridType,        &
                                            CurrentIndexInfo%GridTypeExtra,   &
                                            CurrentIndexInfo%Method,          &
                                            CurrentIndexInfo%BasisSet,        &
                                            CurrentIndexInfo%DEN_Partitioning,&
                                            CurrentIndexInfo%CoreSize,        &
                                            CurrentIndexInfo%OPT
                                             
            if(CurrentIndexInfo%IndexNum.gt.MaxIndex) MaxIndex=&
                                                      CurrentIndexInfo%IndexNum
            if(Reason.lt.0) exit  
         end do
      close(unit=File_unit)
      write(*,'(A,I0)')"MaxIndex  =  ", MaxIndex
      return
      end SUBROUTINE GetMaxIndex
      SUBROUTINE GetFragmentDBCart (MolIdxInfo,UniqueSymNum,FragmentCart)
!***********************************************************************
!     Date last modified:                                              *
!     Author: Ibrahim Awad                                             *
!     Description: Return the cartesian coordinates of the fragments   * 
!                  from the database cartesian file.                   *
!***********************************************************************
!       
      
      implicit none
      
      integer, intent(in) :: UniqueSymNum
      type(IndexFile),dimension(:),allocatable, intent(in) :: MolIdxInfo
      type(FragAtomInfo),dimension(:,:),allocatable, intent(out) :: FragmentCart
      
      integer :: iatom, latom, indexi, newindex
      integer :: File_unit, Reason
      character(len=8) :: Element
      double precision :: XCart, YCart, ZCart
      logical :: Lerror
      
      allocate (FragmentCart(Natoms,Natoms))
      
      ! Return the coordinate of fragments in the database for the wanted index
      ! loop over the wanted index 
      do indexi=1, UniqueSymNum
        ! check if the symbol is available within the database
        if (.not.MolIdxInfo(indexi)%Availability) cycle
        ! loop over the atoms of the fragment
        do iatom=1 , MolIdxInfo(indexi)%NAtoms
            call GET_unit (CartFilePath, File_unit, Lerror)
            open(UNIT=File_unit,file=CartFilePath, &
                 status='old',form='formatted') 
            read(File_unit,*)
            do ! start, reading the file
              read(File_unit,fmt_cart,IOSTAT=Reason) newindex, &
                                                   & latom,    &
                                                   & Element,  &
                                                   & XCart,    &
                                                   & YCart,    &
                                                   & ZCart
!              read(File_unit,fmt_cart,IOSTAT=Reason) XCart, YCart, ZCart
              if (Reason.lt.0) exit 
              if(newindex.eq.MolIdxInfo(indexi)%IndexNum.and.latom.eq.iatom)then
                 FragmentCart(indexi,iatom)%element=Element
                 FragmentCart(indexi,iatom)%x=XCart
                 FragmentCart(indexi,iatom)%y=YCart
                 FragmentCart(indexi,iatom)%z=ZCart
                 exit
              end if
            end do ! reading the file
            close(unit=File_unit)
        end do ! iatom
      end do ! indexi
      end SUBROUTINE GetFragmentDBCart
      SUBROUTINE GetFragmentsProperties (MolIdxInfo,UniqueSymNum,FragProp)
!***********************************************************************
!     Date last modified:                                              *
!     Author: Ibrahim Awad                                             *
!     Description: Return the cartesian coordinates of the fragments   * 
!                  from the database cartesian file.                   *
!***********************************************************************
!       
      
      implicit none
      
      integer, intent(in) :: UniqueSymNum
      type (IndexFile), dimension(:), allocatable, intent(in) :: MolIdxInfo
      type (PropFile), dimension(:), allocatable, intent(out) :: FragProp
      
      integer :: indexi, newindex
      integer :: File_unit, Reason
      double precision :: EleNum, EV_Anal, EV_Num, EX, EXJ, ET
      double precision :: EC, Kaa, Jaa, Vee_A, ECK, EC_AnaNum
      logical :: Lerror
      
      allocate (FragProp(UniqueSymNum))
      
      ! Return the coordinate of fragments in the database for the wanted index
      ! loop over the wanted index 
      FragProp%Availability=.false.
      do indexi=1, UniqueSymNum
         ! check if the symbol is available within the database
         if (.not.MolIdxInfo(indexi)%Availability) cycle
         FragProp(indexi)%Availability=.true.
         ! loop over the atoms of the fragment
         call GET_unit (PropFilePath, File_unit, Lerror)
         open(UNIT=File_unit,file=PropFilePath, status='old',form='formatted') 
         read(File_unit,*)
         do ! start, reading the file
             read(File_unit,fmt_prop,IOSTAT=Reason) &
                & newindex,                 &
                & EleNum,            &
                & EX,                       &
                & EXJ,                      &
                & ET,                       &
                & EV_Anal,                  &
                & EV_Num,                   &
                & EC_AnaNum,                &
                & EC,                       &
                & ECK,                      &
                & Jaa,                      &
                & vee_A 
                     

             if (Reason.lt.0) exit 
             if (newindex.eq.MolIdxInfo(indexi)%IndexNum) then
                 FragProp(indexi)%IndexNum=newindex
                 FragProp(indexi)%ElectonNum=EleNum
                 FragProp(indexi)%K=EX
                 FragProp(indexi)%KHF=EXJ
                 FragProp(indexi)%T=ET
                 FragProp(indexi)%Vne_A=EV_Anal
                 FragProp(indexi)%Vne_N=EV_Num
                 FragProp(indexi)%Vee_AN=EC_AnaNum
                 FragProp(indexi)%J=EC
                 FragProp(indexi)%JHF=ECK
                 FragProp(indexi)%Jaa=Jaa
                 FragProp(indexi)%VeeA=Vee_A
                 exit
             end if
         end do ! reading the file
         close(unit=File_unit)
      end do ! indexi
      end SUBROUTINE GetFragmentsProperties
      end MODULE GetDataBaseInfo
