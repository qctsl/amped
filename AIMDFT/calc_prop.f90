      SUBROUTINE CalcMoleProperties()
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:                                                             *
!*******************************************************************************
! MODULEs:

      USE AIMDFT_type 
      USE type_molecule 

      implicit none
    
      interface
         SUBROUTINE Build_Molecule_DB(DatabaseInform,FragProp,FRgrids,FragAtoms)
          use aimdft_type
          implicit none
          type (IndexFile), dimension (:), allocatable :: DatabaseInform
          type (PropFile), dimension(:), allocatable :: FragProp
          type (GridsDataBase), dimension(:,:), allocatable :: FRgrids 
          type (FragAtomInfo), dimension(:,:), allocatable :: FragAtoms
         end SUBROUTINE Build_Molecule_DB
         SUBROUTINE Build_Molecule_Direct(DatabaseInform,FragProp,FRgrids)
          use aimdft_type
          USE type_molecule
          implicit none
          type (IndexFile), dimension (:), allocatable :: DatabaseInform
          type (PropFile), dimension(:), allocatable :: FragProp
          type (GridsDataBase), dimension(:,:), allocatable :: FRgrids 
         end SUBROUTINE Build_Molecule_Direct
         SUBROUTINE CalcMolePropertiesDB(DatabaseInform,FragProp,FRgrids) 
          use aimdft_type
          implicit none
          type (IndexFile), dimension (:), allocatable :: DatabaseInform
          type (PropFile), dimension(:), allocatable :: FragProp
          type (GridsDataBase), dimension(:,:), allocatable :: FRgrids 
         end SUBROUTINE CalcMolePropertiesDB
      end interface

      type (IndexFile), dimension (:), allocatable :: DatabaseInform
      type (PropFile), dimension(:), allocatable :: FragProp
      type (GridsDataBase), dimension(:,:), allocatable :: FRgrids 
      type (FragAtomInfo), dimension(:,:), allocatable :: FragAtoms

      allocate (DatabaseInform(Natoms)) 
      allocate (FragProp(Natoms)) 
      allocate (FragAtoms(Natoms,Natoms)) 
      allocate (FRgrids(Natoms,MaxGridPnt))   

      if(DirectMethod) then
         call Build_Molecule_Direct(DatabaseInform,FragProp,FRgrids)
      else
         call Build_Molecule_DB(DatabaseInform,FragProp,FRgrids,FragAtoms) 
      end if

      call CalcMolePropertiesDB(DatabaseInform,FragProp,FRgrids)
      deallocate (DatabaseInform) 
      deallocate (FragProp) 
      deallocate (FragAtoms) 
      deallocate (FRgrids)   
      return
      end SUBROUTINE CalcMoleProperties
