      MODULE rotation_Cart
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:                                                             *
!*******************************************************************************
!
      USE mod_rotation

      implicit none

CONTAINS !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      
      SUBROUTINE DoCartRotation(OldCarts,NewCarts,pointnumber,RR,TT)
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description: Apply the rotation and translation matrices on fragment     *
!*******************************************************************************
!
      implicit none

      type (FragAtomInfo), dimension(:), intent(in), allocatable  :: OldCarts 
      type (FragAtomInfo), dimension(:), intent(out), allocatable :: NewCarts
      double precision, dimension (3,3), intent(in) :: RR
      double precision, dimension (3), intent(in) :: TT
      integer, intent(in) :: pointnumber
      
      double precision, dimension (:,:), allocatable :: CurrCart,  NewCart 
      integer :: ifound
      
      allocate(NewCarts(pointnumber))
      allocate (CurrCart(pointnumber,1:3))
      allocate (NewCart(pointnumber,1:3)) 
      
      NewCarts=OldCarts
 
      do ifound=1, pointnumber  !loop over cart
         CurrCart(ifound,1)=OldCarts(ifound)%x  
         CurrCart(ifound,2)=OldCarts(ifound)%y
         CurrCart(ifound,3)=OldCarts(ifound)%z
      end do
      call DoRotation(CurrCart,NewCart,pointnumber,RR,TT)
      do ifound=1, pointnumber  !loop over cart 
         NewCarts(ifound)%x=NewCart(ifound,1)
         NewCarts(ifound)%y=NewCart(ifound,2)
         NewCarts(ifound)%z=NewCart(ifound,3)
      end do
      deallocate(CurrCart, NewCart)
      return
      end SUBROUTINE DoCartRotation
      SUBROUTINE GetCartRotMatrix(OldCarts,NewCarts,pointnumber,RR,TT)
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:  Get the the least square method rotation and translation   *
!                   matrices                                                   *
!*******************************************************************************
!
      implicit none

      type (FragAtomInfo), dimension(:), intent(in), allocatable  :: OldCarts 
      type (FragAtomInfo), dimension(:), intent(in), allocatable :: NewCarts
      double precision, dimension (3,3), intent(out)  :: RR
      double precision, dimension (3), intent(out)  :: TT
      
      integer, intent(in) :: pointnumber
      
      double precision, dimension (:,:), allocatable :: CurrCart,  NewCart 
      integer :: ifound, pointnumber2
      
      allocate (CurrCart(pointnumber,1:3))  
      allocate (NewCart(pointnumber,1:3))  

      do ifound=1, pointnumber  !loop over cart atoms
         CurrCart(ifound,1)=OldCarts(ifound)%x
         CurrCart(ifound,2)=OldCarts(ifound)%y
         CurrCart(ifound,3)=OldCarts(ifound)%z
            
         NewCart(ifound,1)=NewCarts(ifound)%x
         NewCart(ifound,2)=NewCarts(ifound)%y
         NewCart(ifound,3)=NewCarts(ifound)%z
      end do
      pointnumber2=pointnumber
      if (pointnumber.gt.3) pointnumber2=4
      call GetRotMatrix (CurrCart,NewCart,pointnumber2,RR,TT)
      
      deallocate(CurrCart, NewCart)
      return
      end SUBROUTINE GetCartRotMatrix
      SUBROUTINE GetCartRotMatrixW(OldCarts,NewCarts,pointnumber,RR,TT)
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description: iGet the the least square method rotation and translation   *
!                   matrices after applying weights                            *
!*******************************************************************************
!
      implicit none

      type (FragAtomInfo), dimension(:), intent(in), allocatable :: OldCarts 
      type (FragAtomInfo), dimension(:), intent(in), allocatable :: NewCarts
      double precision, dimension (3,3), intent(out) :: RR
      double precision, dimension (3), intent(out) :: TT
      
      integer, intent(in) :: pointnumber
      
      double precision, dimension (:,:), allocatable :: CurrCart,  NewCart 
      integer :: ifound,pointnumber2
      integer, parameter :: weight0=2, weight1=1, weight2=0
      integer :: Newpointnumber, Newifound, countfound
      
      ! calculate number of points
      Newpointnumber=0
      do ifound=1, pointnumber  !loop over cart atoms
         if (OldCarts(ifound)%level.eq.0) Newpointnumber=2000
         if (OldCarts(ifound)%level.eq.1) Newpointnumber=Newpointnumber+&
                              OldCarts(ifound)%Atomic_Number**(weight1+2)
         if (OldCarts(ifound)%level.eq.2) Newpointnumber=&
               Newpointnumber+OldCarts(ifound)%Atomic_Number**(weight2+2)
       ! if (OldCarts(ifound)%level.gt.2) Newpointnumber=&
       ! Newpointnumber+OldCarts(ifound)%Atomic_Number
      end do
      allocate (CurrCart(Newpointnumber,1:3))  
      allocate (NewCart(Newpointnumber,1:3))  
      countfound=0
      do ifound=1, pointnumber  !loop over cart atoms
         if (OldCarts(ifound)%level.eq.0) then
            do Newifound=1, 2000
               countfound=countfound+1
               CurrCart(countfound,1)=OldCarts(ifound)%x
               CurrCart(countfound,2)=OldCarts(ifound)%y
               CurrCart(countfound,3)=OldCarts(ifound)%z
                 
               NewCart(countfound,1)=NewCarts(ifound)%x
               NewCart(countfound,2)=NewCarts(ifound)%y
               NewCart(countfound,3)=NewCarts(ifound)%z
            end do
         end if
         if (OldCarts(ifound)%level.eq.1) then
            do Newifound=1, OldCarts(ifound)%Atomic_Number**(weight1+2)
               countfound=countfound+1
               CurrCart(countfound,1)=OldCarts(ifound)%x
               CurrCart(countfound,2)=OldCarts(ifound)%y
               CurrCart(countfound,3)=OldCarts(ifound)%z
                 
               NewCart(countfound,1)=NewCarts(ifound)%x
               NewCart(countfound,2)=NewCarts(ifound)%y
               NewCart(countfound,3)=NewCarts(ifound)%z
            end do
         end if
         if (OldCarts(ifound)%level.eq.2) then
            
            do Newifound=1, OldCarts(ifound)%Atomic_Number**(weight2+2)
               countfound=countfound+1
               CurrCart(countfound,1)=OldCarts(ifound)%x
               CurrCart(countfound,2)=OldCarts(ifound)%y
               CurrCart(countfound,3)=OldCarts(ifound)%z
                
               NewCart(countfound,1)=NewCarts(ifound)%x
               NewCart(countfound,2)=NewCarts(ifound)%y
               NewCart(countfound,3)=NewCarts(ifound)%z
            end do
         end if
      !  if (OldCarts(ifound)%level.gt.2) then
      !     do Newifound=1, OldCarts(ifound)%Atomic_Number**2
      !        countfound=countfound+1
      !        CurrCart(countfound,1)=OldCarts(ifound)%x
      !        CurrCart(countfound,2)=OldCarts(ifound)%y
      !        CurrCart(countfound,3)=OldCarts(ifound)%z
      !           
      !        NewCart(countfound,1)=NewCarts(ifound)%x
      !        NewCart(countfound,2)=NewCarts(ifound)%y
      !        NewCart(countfound,3)=NewCarts(ifound)%z
      !     end do
      !  end if
      end do
      call GetRotMatrix (NewCart,CurrCart,countfound,RR,TT)
      deallocate(CurrCart, NewCart)
      return
      end SUBROUTINE GetCartRotMatrixW   
      SUBROUTINE GetDBCart(OldCarts,NewCarts,pointnumber,RR,TT)
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description: Get the database coordinates and the rotation and the       *
!                  translation matrics                                         *
!*******************************************************************************
!      
      implicit none

      type (FragAtomInfo), dimension(:), intent(in), allocatable :: OldCarts 
      type (FragAtomInfo), dimension(:), intent(out), allocatable :: NewCarts
      double precision, dimension (3,3), intent(out) :: RR
      double precision, dimension (3), intent(out) :: TT
      
      integer, intent(in) :: pointnumber
      
      double precision, dimension (:,:), allocatable :: CurrCart,  NewCart 
      integer :: ifound
      
      allocate (NewCarts(pointnumber))
      allocate (CurrCart(pointnumber,1:3))  
      allocate (NewCart(pointnumber,1:3))
      
      NewCarts=OldCarts  
           
      do ifound=1, pointnumber  !loop over cart atoms
         CurrCart(ifound,1)=OldCarts(ifound)%x
         CurrCart(ifound,2)=OldCarts(ifound)%y
         CurrCart(ifound,3)=OldCarts(ifound)%z
      end do
      call GetStoredRotMatrix (CurrCart,pointnumber,RR,TT)
      call DoRotation (CurrCart,NewCart,pointnumber,RR,TT)
      do ifound=1, pointnumber  !loop over cart atoms
         NewCarts(ifound)%x=NewCart(ifound,1)
         NewCarts(ifound)%y=NewCart(ifound,2)
         NewCarts(ifound)%z=NewCart(ifound,3)
      end do
      deallocate(CurrCart, NewCart)
      return
      end SUBROUTINE GetDBCart 
      SUBROUTINE GetDBFormFragments(FragAtoms,NewFragAtoms,FragInfo)
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description: Get the database coordinates and the rotation and the       *
!                  translation matrics for the fragments                       *
!*******************************************************************************
!   
      implicit none

      type(FragAtomInfo),dimension(:,:),intent(in), allocatable  :: FragAtoms 
      type(FragAtomInfo),dimension(:,:),intent(out), allocatable :: NewFragAtoms
      type(FragsInfo),dimension(:),intent(inout), allocatable :: FragInfo    
      
      double precision, dimension (3,3) :: RR
      double precision, dimension (3) :: TT
      
      double precision, dimension (:,:), allocatable :: CurrCart,  NewCart
      integer :: ifound,JAtom,pointnumbers
      
      allocate (NewFragAtoms(Natoms,Natoms))

      NewFragAtoms=FragAtoms

      do JAtom=1, NAtoms !loop over all atoms
         pointnumbers=FragInfo(JAtom)%NAtoms
         allocate (CurrCart(1:pointnumbers,1:3))  
         allocate (NewCart(1:pointnumbers,1:3))  
         do ifound=1, pointnumbers  !loop over cart
            CurrCart(ifound,1)=FragAtoms(Jatom,ifound)%x  
            CurrCart(ifound,2)=FragAtoms(Jatom,ifound)%y 
            CurrCart(ifound,3)=FragAtoms(Jatom,ifound)%z
         end do
         call GetStoredRotMatrix (CurrCart,pointnumbers,RR,TT)
         FragInfo(Jatom)%RMat=RR
         FragInfo(Jatom)%TMat=TT
         call DoRotation (CurrCart,NewCart,pointnumbers,RR,TT)
         do ifound=1, pointnumbers  !loop over cart
            NewFragAtoms(Jatom,ifound)%x=NewCart(ifound,1)
            NewFragAtoms(Jatom,ifound)%y=NewCart(ifound,2)
            NewFragAtoms(Jatom,ifound)%z=NewCart(ifound,3)
         end do
         deallocate(CurrCart, NewCart)
      end do !loop over all atoms
      return
      end SUBROUTINE GetDBFormFragments
      end MODULE rotation_Cart
