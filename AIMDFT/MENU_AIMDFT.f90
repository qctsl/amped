      SUBROUTINE MENU_AIMDFT
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:                                                             *
!*******************************************************************************
! MODULEs:
      USE program_parser
      USE program_constants
      USE MENU_gets
      USE AIMDFT_type

      implicit none
!
! Local scalars:
      integer :: length
      logical done
      ChangeTerminalAtoms=.false.
!
! Begin:
      call PRG_manager ('enter', 'MENU_AIMDFT', 'UTILITY')
! Defaults:
      done=.false.
! Menu:
      do while (.not.done)
      call new_token (' AIMDFT:')
!
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      ' Command AIMDFT:', &
      '   Purpose: Request AIMDFT codes', &
      '   Syntax :', &
      '   Level_Number = <integer>, Override the default', &
      '   TErminal = <integer>, Override the default', &
      '   end'
!
! ROTlist
      else if(token(1:3).eq.'ROT')then
        call GET_value(ROTlist, NROTlist)

! Level_Number
      else if(token(1:1).EQ.'L')then
          call GET_value (Level_Number)
    
! Terminal Atom
      else if(token(1:1).EQ.'T')then
         call GET_value (TerminalAtom)
         
! INDexNUM (needed to RUN the input files for the fragments)
      else if(token(1:1).EQ.'I')then
         call GET_value (IndexNumber)

!DIrectMethod
      else if(token(1:1).EQ.'D')then
         DirectMethod=.true.
         
!Use the Becke weight from the Database
!      else if(token(1:5).EQ.'NODBW')then
!         DatabaseBWeight=.False.

! CHangeTerm
      else if(token(1:2).EQ.'CH')then
         ChangeTerminalAtoms=.true.

! OPT
      else if(token(1:1).EQ.'O')then
         OPT_status="OPT"

      else
         call MENU_end (done)
      end if
!
      end do !(.not.done)

!
! end of routine MENU_AIMDFT
      call PRG_manager ('exit', 'MENU_AIMDFT', 'UTILITY')
      return
      end
