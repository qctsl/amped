      MODULE Sorted_AIMDFT
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:                                                             *
!*******************************************************************************
! 

      USE AddTerminal
           
      implicit none
      
CONTAINS !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
      SUBROUTINE GetSortCartTerm(level,SortFragAtoms,SortFragInfo)
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:                                                             *
!*******************************************************************************
!
 
      implicit none
      
      integer, intent(in) :: level
      type(FragAtomInfo),dimension(:,:),intent(out),allocatable::SortFragAtoms
      type (FragsInfo), dimension(:), intent(out), allocatable :: SortFragInfo
      
      allocate (SortFragAtoms(Natoms,Natoms))
      allocate (SortFragInfo(Natoms))
      
      CALL GenerateFragments(level,SortFragAtoms,SortFragInfo)
      CALL AddTerminalAtoms(level,SortFragAtoms,SortFragInfo)
      ! +1 >>> Also sort the new terminals
      CALL SortFType(level+1,SortFragAtoms,SortFragInfo) 
      
      return
      end SUBROUTINE GetSortCartTerm
      SUBROUTINE SortLevel(level,SortFragAtoms,SortFragInfo)
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:                                                             *
!*******************************************************************************
!
      implicit none

      integer, intent(in) :: level
      type(FragAtomInfo),dimension(:,:),intent(inout),allocatable::SortFragAtoms
      type(FragsInfo),dimension(:),intent(inout),allocatable :: SortFragInfo    
      type(FragAtomInfo), dimension(:,:), allocatable :: TEMPFragAtoms 
      integer :: currvalue, nextvalue, inext
      integer :: Iatom, Jatom, ilevel, NtempOrd, curr, next
      integer, dimension(:,:), allocatable :: countLevel
      integer, dimension(:), allocatable :: excludeLis
      integer, dimension(10) :: tempOrd
      character (SymMax) :: TempCopyChar
      logical :: swap
       
      allocate (TEMPFragAtoms(Natoms,Natoms))  
      allocate (countLevel(Natoms,level))
      allocate (excludeLis(Natoms)) 
               
      ! FIRST :::: Sort atoms by levels
      ! loop over all fragments for Jatoms
      do Jatom=1, NAtoms
         ! exclude dummy atoms
         if (Cartesian(JAtom)%Atomic_Number.eq.0) cycle
         NtempOrd=SortFragInfo(Jatom)%NAtoms
         swap=.true.
         do while(swap)
            swap=.false.
            do curr=1,NtempOrd-1
               next=curr+1
               currvalue=SortFragAtoms(Jatom,curr)%level
               nextvalue=SortFragAtoms(Jatom,next)%level
               if(currvalue.gt.nextvalue)then
                  TEMPFragAtoms(Jatom,curr)=SortFragAtoms(Jatom,curr)
                  SortFragAtoms(Jatom,curr)=SortFragAtoms(Jatom,next)
                  SortFragAtoms(Jatom,next)=TEMPFragAtoms(Jatom,curr)
                  swap=.true.
               end if 
            end do ! do curr
         end do ! do while(swap)         
      end do  ! all atoms (atom by atom) 
      
      ! Check each level if there is a UNIQUE livel that has just one atom, 
      ! then fix there positions
      countLevel=0
      do ilevel=1, level  ! level loop
         do Jatom=1, NAtoms ! fragments loop
            do Iatom=1, SortFragInfo(Jatom)%NAtoms ! atoms within the fragments
               if (SortFragAtoms(Jatom,Iatom)%level.ne.ilevel) cycle
               if (SortFragAtoms(Jatom,Iatom)%sorted) cycle
               countLevel(Jatom,ilevel)=countLevel(Jatom,ilevel)+1
            end do ! subatom within fragment
         end do  ! all atoms (atom by atom) (Fragments)
      end do  ! level by level
      ! Fixed the value for the UNIQUE atoms within ilevel
      do ilevel=1, level  ! level loop
         do Jatom=1, NAtoms ! fragments loop
            if (countLevel(Jatom, ilevel).eq.1) then
               do Iatom=1, SortFragInfo(Jatom)%NAtoms 
                  if (SortFragAtoms(Jatom,Iatom)%level.ne.ilevel) cycle
                  if (SortFragAtoms(Jatom,Iatom)%sorted) cycle
                  SortFragAtoms(Jatom,Iatom)%sorted=.true.
               end do ! subatom within fragment
            end if          
         end do ! all atoms (atom by atom) (Fragments)     
      end do  ! level by level
      deallocate (TEMPFragAtoms)  
      deallocate (countLevel)
      deallocate (excludeLis)
      end SUBROUTINE SortLevel
      
      SUBROUTINE SortZNUM(level,SortFragAtoms,SortFragInfo)
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:                                                             *
!*******************************************************************************
!
      implicit none

      integer, dimension(:), allocatable :: CountAtomicN, CountAtomicI
      integer, dimension(:), allocatable :: CountAtomicT,excludeLis
      integer :: NtempOrd, TempCopy, curr,next, iZ, Z_Vali, Z_Valj, excludeN
      integer, intent(in) :: level
      type(FragAtomInfo),dimension(:,:),intent(inout),allocatable::SortFragAtoms
      type(FragsInfo),dimension(:),intent(inout),allocatable::SortFragInfo    
      type(FragAtomInfo), dimension(:,:), allocatable :: TEMPFragAtoms 
      integer :: currvalue, nextvalue, inext, Iatom, Jatom, ilevel
      integer, dimension(:,:), allocatable :: countLevel
      integer, dimension(10) :: tempOrd
      character (SymMax) :: TempCopyChar
      logical :: swap
       
      allocate (CountAtomicN(Natoms)) 
      allocate (CountAtomicI(Natoms))
      allocate (CountAtomicT(Natoms))
      allocate (excludeLis(Natoms))             
      allocate (TEMPFragAtoms(Natoms,Natoms))  
      allocate (countLevel(Natoms,level))
                           
      CALL SortLevel(level,SortFragAtoms,SortFragInfo)
      
      ! SECOND :::: Sort atoms by ATOMIC NUMBER and keep the level
      ! Now loop within each level for fragment Jatom (fixed the level)
      do ilevel=1, level  ! level loop
          do Jatom=1, NAtoms
              ! exclude dummy atoms
              if (Cartesian(JAtom)%Atomic_Number.eq.0) cycle
         
              NtempOrd=SortFragInfo(Jatom)%NAtoms
              swap=.true.
              do while(swap)
                  !if (FragAtoms(Jatom,ifound)%level.ne.ilevel) cycle  
                  swap=.false.
                  do curr=1,NtempOrd-1
                      !Just  ilevel
                      if (SortFragAtoms(Jatom,curr)%level.ne.ilevel) cycle 
                      if (SortFragAtoms(Jatom,curr)%sorted) cycle
                      ! set curr subatom
                      currvalue=SortFragAtoms(Jatom,curr)%Atomic_Number
                 
                      ! search for the next subatom in the same level, 
                      ! then compare
                      next=-1
                      do inext=curr+1, NtempOrd
                         if (SortFragAtoms(Jatom,inext)%sorted) cycle
                         if (SortFragAtoms(Jatom,inext)%level.eq.ilevel) then
                            nextvalue=SortFragAtoms(Jatom,inext)%Atomic_Number
                            next=inext
                            ! Found subatom in the same level with the curr atom
                            exit 
                         end if
                      end do
                      ! There just one atom at that level (NO next atom)
                      if (next.eq.-1) cycle 

                      ! compare 
                      if(currvalue.lt.nextvalue)then
                          TEMPFragAtoms(Jatom,curr)=SortFragAtoms(Jatom,curr)
                          SortFragAtoms(Jatom,curr)=SortFragAtoms(Jatom,next)
                          SortFragAtoms(Jatom,next)=TEMPFragAtoms(Jatom,curr)
                          swap=.true.
                      endif 
                  end do ! do curr
                  !write(*,*) currvalue, "<", nextvalue, v1,v2
              end do ! do while(swap)
          end do  ! all atoms (atom by atom)
      end do  ! level by level
      
      ! NEXT step is to Check each level if there is a UNIQUE atom with ....
      ! ... (no other atoms have same Z), then fix its position
      do Jatom=1, NAtoms ! fragments loop
          do ilevel=1, level  ! level loop
              iZ=0
              CountAtomicN=0
              do Iatom=1, SortFragInfo(Jatom)%NAtoms !atoms within the fragments
                  if (SortFragAtoms(Jatom,Iatom)%level.ne.ilevel) cycle
                  if (SortFragAtoms(Jatom,Iatom)%sorted) cycle
                  iZ=iZ+1
                  CountAtomicN(iZ)=SortFragAtoms(Jatom,Iatom)%Atomic_Number
                  CountAtomicI(iZ)=Iatom
              end do ! subatom within fragment
              ! now check and put the value within array
              excludeN=0
              excludeLis=0
              do Z_Vali=1, iZ
                  do Z_Valj=Z_Vali+1,iZ
                      if (Z_Vali.eq.Z_Valj) cycle
                      if(CountAtomicN(Z_Vali).eq.CountAtomicN(Z_ValJ)) then
                          if (.not.ANY( excludeLis==CountAtomicI(Z_Vali))) then
                              excludeN=excludeN+1
                              excludeLis(excludeN)=CountAtomicI(Z_Vali)
                           end if
                          if (.not.ANY( excludeLis==CountAtomicI(Z_ValJ))) then
                              excludeN=excludeN+1
                              excludeLis(excludeN)=CountAtomicI(Z_ValJ)
                           end if
                      end if
                  end do
              end do              
              do Iatom=1, SortFragInfo(Jatom)%NAtoms !atoms within the fragments
                  if (SortFragAtoms(Jatom,Iatom)%level.ne.ilevel) cycle
                  if (SortFragAtoms(Jatom,Iatom)%sorted) cycle
                  if (.not.ANY( excludeLis==Iatom)) &
                  SortFragAtoms(Jatom,Iatom)%sorted=.true.
              end do ! subatom within fragment              
          end do  ! level by level 
      end do ! all atoms (atom by atom) (Fragments)
      deallocate (CountAtomicN) 
      deallocate (CountAtomicI)
      deallocate (CountAtomicT)
      deallocate (excludeLis)             
      deallocate (TEMPFragAtoms)  
      deallocate (countLevel)
      return
      end SUBROUTINE SortZNUM
      
      SUBROUTINE SortFType(level,SortFragAtoms,SortFragInfo)
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:                                                             *
!*******************************************************************************
!
      implicit none
      
      integer, dimension(:), allocatable :: CountAtomicN, CountAtomicI
      integer, dimension(:), allocatable :: CountAtomicT,excludeLis
      integer :: NtempOrd, TempCopy, curr,next, iZ, Z_Vali, Z_Valj, excludeN
      integer, intent(in) :: level
      type(FragAtomInfo),dimension(:,:),intent(inout),allocatable::SortFragAtoms
      type(FragsInfo),dimension(:),intent(inout),allocatable::SortFragInfo    
      type(FragAtomInfo),dimension(:,:),allocatable::TEMPFragAtoms 
      integer :: currvalue, nextvalue, inext, Iatom, Jatom, ilevel
      integer, dimension(:,:), allocatable :: countLevel
      integer, dimension(10) :: tempOrd
      character (SymMax) :: TempCopyChar
      logical :: swap
       
      allocate (CountAtomicN(Natoms)) 
      allocate (CountAtomicI(Natoms))
      allocate (CountAtomicT(Natoms))
      allocate (excludeLis(Natoms))             
      allocate (TEMPFragAtoms(Natoms,Natoms))  
      allocate (countLevel(Natoms,level))
       
      CALL SortLevel(level,SortFragAtoms,SortFragInfo)
      CALL SortZNUM(level,SortFragAtoms,SortFragInfo)
      
      ! Third :::: Sort atoms by ATOMIC TYPE for the fragment 
      ! without the NEW termenal and keep the level 
      ! Now loop within each level for fragment Jatom (fixed the level)
      do ilevel=1, level  ! level loop
          do Jatom=1, NAtoms
              ! exclude dummy atoms
              if (Cartesian(JAtom)%Atomic_Number.eq.0) cycle
         
              NtempOrd=SortFragInfo(Jatom)%NAtoms
              swap=.true.
              do while(swap)
              !if (FragAtoms(Jatom,ifound)%level.ne.ilevel) cycle !Just  ilevel 
                  swap=.false.
                  do curr=1,NtempOrd-1
                      if (SortFragAtoms(Jatom,curr)%level.ne.ilevel) cycle 
                      if (SortFragAtoms(Jatom,curr)%sorted) cycle
                      ! set curr subatom
                      currvalue=SortFragAtoms(Jatom,curr)%type
                 
                      ! search for the next subatom in the same level,
                      ! then compare
                      next=-1
                      do inext=curr+1, NtempOrd
                          ! exclude T sorted 
                          if (SortFragAtoms(Jatom,inext)%sorted) cycle
                          ! same atomic number
                          if (SortFragAtoms(Jatom,curr)%Atomic_Number.ne.&
                          SortFragAtoms(Jatom,inext)%Atomic_Number) cycle
                          if (SortFragAtoms(Jatom,inext)%level.eq.ilevel) then
                              nextvalue=SortFragAtoms(Jatom,inext)%type
                              next=inext
                              exit ! Found subatom in the same level and same
                                   ! atomic number with the curr atom
                          end if
                      end do
                      if (next.eq.-1) cycle ! There just one atom at that level 

                      ! compare 
                      if(currvalue.lt.nextvalue)then
                          TEMPFragAtoms(Jatom,curr)=SortFragAtoms(Jatom,curr)
                          SortFragAtoms(Jatom,curr)=SortFragAtoms(Jatom,next)
                          SortFragAtoms(Jatom,next)=TEMPFragAtoms(Jatom,curr)
                          swap=.true.
                      endif 
                  end do ! do curr
                  !write(*,*) currvalue, "<", nextvalue, v1,v2
              end do ! do while(swap)
          end do  ! all atoms (atom by atom)
      end do  ! level by level
      
      ! NEXT step is to Check each level if there is a UNIQUE atom with ....
      ! ... (no other atoms have same Z and type), then fix its position
      do Jatom=1, NAtoms ! fragments loop
          do ilevel=1, level  ! level loop
              iZ=0
              CountAtomicN=0
              do Iatom=1, SortFragInfo(Jatom)%NAtoms !atoms within the fragments
                  if (SortFragAtoms(Jatom,Iatom)%level.ne.ilevel) cycle
                  if (SortFragAtoms(Jatom,Iatom)%sorted) cycle
                  iZ=iZ+1
                  CountAtomicT(iZ)=SortFragAtoms(Jatom,Iatom)%type
                  CountAtomicN(iZ)=SortFragAtoms(Jatom,Iatom)%Atomic_Number
                  CountAtomicI(iZ)=Iatom
              end do ! subatom within fragment
              ! now check and put the value within array
              excludeN=0
              excludeLis=0
              do Z_Vali=1, iZ
                  do Z_Valj=Z_Vali+1,iZ
                      if (Z_Vali.eq.Z_Valj) cycle
                      if(CountAtomicN(Z_Vali).eq.CountAtomicN(Z_ValJ).and.&
                      CountAtomicT(Z_Vali).eq.CountAtomicT(Z_ValJ)) then
                          if (.not.ANY( excludeLis==CountAtomicI(Z_Vali))) then
                              excludeN=excludeN+1
                              excludeLis(excludeN)=CountAtomicI(Z_Vali)
                           end if
                          if (.not.ANY( excludeLis==CountAtomicI(Z_ValJ))) then
                              excludeN=excludeN+1
                              excludeLis(excludeN)=CountAtomicI(Z_ValJ)
                           end if
                      end if
                  end do
              end do              
              do Iatom=1, SortFragInfo(Jatom)%NAtoms !atoms within the fragments
                  if (SortFragAtoms(Jatom,Iatom)%level.ne.ilevel) cycle
                  if (SortFragAtoms(Jatom,Iatom)%sorted) cycle
                  if (.not.ANY( excludeLis==Iatom))&
                  SortFragAtoms(Jatom,Iatom)%sorted=.true.
              end do ! subatom within fragment              
          end do  ! level by level 
      end do ! all atoms (atom by atom) (Fragments)
      deallocate (CountAtomicN) 
      deallocate (CountAtomicI)
      deallocate (CountAtomicT)
      deallocate (excludeLis)             
      deallocate (TEMPFragAtoms)  
      deallocate (countLevel)
      return
      end SUBROUTINE SortFType
      SUBROUTINE GetSortCart(level,SortFragAtoms,SortFragInfo)   ! not completed
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:                                                             *
!*******************************************************************************
!
      implicit none

      integer, intent(in) :: level
      type(FragAtomInfo),dimension(:,:),intent(out),allocatable :: SortFragAtoms
      type(FragsInfo),dimension(:),intent(out),allocatable :: SortFragInfo   
      type(FragAtomInfo),dimension(:,:),allocatable :: FragAtoms,TEMPFragAtoms 
      type(FragsInfo),dimension(:),allocatable :: FragInfo  ! (main atom)
      integer, dimension(:), allocatable:: sortindex,sorttype
      integer :: Iatom,Jatom,ifound,ilevel,kfound,mfound,ifoundlevel
      integer :: startilevel,nosimilar,nsim,munatom
      integer :: currvalue, nextvalue, inext
      integer, dimension(:,:), allocatable :: countLevel
      integer, dimension(:), allocatable :: CountAtomicT,excludeLis
      integer, dimension(:), allocatable :: CountAtomicN, CountAtomicI
      double precision, dimension(:), allocatable :: SumZ, SumZ_x
      double precision, dimension(:), allocatable :: SumZ_y, SumZ_z
      double precision, dimension(:,:), allocatable :: Z_x ,Z_y ,Z_z
      double precision, dimension(:,:), allocatable :: Cent_x, Cent_y, Cent_z
      double precision, dimension(:,:), allocatable :: Cent_R
      integer, dimension(10) :: tempOrd
      integer :: NtempOrd, TempCopy, curr,next, iZ, Z_Vali, Z_Valj
      integer :: excludeN, testi
      character (SymMax) :: TempCopyChar
      logical :: swap
       
      !>>>>>>>>>>>>>Begin >>>>>>>>>>>>>>>>>>>
      allocate (SortFragAtoms(Natoms,Natoms))
      allocate (SortFragInfo(Natoms))
      allocate (FragAtoms(Natoms,Natoms))
      allocate (TEMPFragAtoms(Natoms,Natoms))  
      allocate (FragInfo(Natoms)) 
      allocate (countLevel(Natoms,level))
      allocate (CountAtomicN(Natoms)) 
      allocate (CountAtomicI(Natoms))
      allocate (CountAtomicT(Natoms))
      allocate (excludeLis(Natoms)) 
      allocate (SumZ(Natoms))
      allocate (SumZ_x(Natoms))
      allocate (SumZ_y(Natoms))
      allocate (SumZ_z(Natoms))
      allocate (Z_x(Natoms,Natoms))
      allocate (Z_y(Natoms,Natoms))
      allocate (Z_z(Natoms,Natoms)) 
      allocate (Cent_x(Natoms,Natoms))
      allocate (Cent_y(Natoms,Natoms))
      allocate (Cent_z(Natoms,Natoms))
      allocate (Cent_R(Natoms,Natoms))
      

      CALL GenerateFragments(level,SortFragAtoms,SortFragInfo)              
      CALL SortLevel(level,SortFragAtoms,SortFragInfo)
      CALL SortZNUM(level,SortFragAtoms,SortFragInfo)
      CALL SortFType(level,SortFragAtoms,SortFragInfo)
      
     
      if(.false.) then
      ! Forth :::: Sort atoms by their far from center ......
      !            of mass (Atomic Number) .....
      !        ... for each fragment without the NEW termenal ...
      !        ... and keep all the pevious conditions
      
      ! Now calculate the center of mass (used Z instead of atomic mass)
      ! Forth-a:::: calculate total Z for each fragment without the termenals
      do Jatom=1, NAtoms ! fragments loop
          SumZ(Jatom)=0.0d0
          SumZ_x(Jatom)=0.0d0; SumZ_y(Jatom)=0.0d0 ; SumZ_z(Jatom)=0.0d0
          do Iatom=1, FragInfo(Jatom)%NAtoms ! atoms within the fragments
              ! Calculate Z*x,Z*y, and Z*z for each atom
              Z_x(Jatom,Iatom)=SortFragAtoms(Jatom,Iatom)%Atomic_Number*&
                               SortFragAtoms(Jatom,Iatom)%x
              Z_y(Jatom,Iatom)=SortFragAtoms(Jatom,Iatom)%Atomic_Number*&
                               SortFragAtoms(Jatom,Iatom)%y
              Z_z(Jatom,Iatom)=SortFragAtoms(Jatom,Iatom)%Atomic_Number*&
                               SortFragAtoms(Jatom,Iatom)%z
              ! Find the sum each of Z*x,Z*y, and Z*z                          
              SumZ_x(Jatom)=SumZ_x(Jatom)+Z_x(Jatom,Iatom)
              SumZ_y(Jatom)=SumZ_y(Jatom)+Z_y(Jatom,Iatom)
              SumZ_z(Jatom)=SumZ_z(Jatom)+Z_z(Jatom,Iatom)
              ! calculate Total sum of Z
              SumZ(Jatom)=SumZ(Jatom)+&
              dble(SortFragAtoms(Jatom,Iatom)%Atomic_Number)
          end do ! subatom within fragment 
        ! calculate center of mass \[X = \frac{{\sum {Z \cdot x} }}{{\sum Z }}\]
          do Iatom=1, FragInfo(Jatom)%NAtoms ! atoms within the fragments
              Cent_x(Jatom,Iatom)=(Z_x(Jatom,Iatom)-SumZ_x(Jatom))/SumZ(Jatom)
              Cent_y(Jatom,Iatom)=(Z_y(Jatom,Iatom)-SumZ_y(Jatom))/SumZ(Jatom)
              Cent_z(Jatom,Iatom)=(Z_z(Jatom,Iatom)-SumZ_z(Jatom))/SumZ(Jatom)
              Cent_R(Jatom,Iatom)=dsqrt( Cent_x(Jatom,Iatom)**2 + &
                                       & Cent_y(Jatom,Iatom)**2 + &
                                       & Cent_z(Jatom,Iatom)**2)
              
              write(*,*) Jatom, Iatom, Cent_R(Jatom,Iatom),&
                         SortFragAtoms(Jatom,Iatom)%element
          end do ! subatom within fragment 
          !SumZ_x(Jatom)=SumZ_x(Jatom)/SumZ(Jatom)
          !SumZ_y(Jatom)=SumZ_y(Jatom)/SumZ(Jatom)
          !SumZ_z(Jatom)=SumZ_z(Jatom)/SumZ(Jatom)              
         !write(*,'(I3,4F16.8)') Jatom, SumZ(Jatom), &
         !                       SumZ_x(Jatom), SumZ_y(Jatom), SumZ_z(Jatom)
      end do ! all atoms (atom by atom) (Fragments)
      end if ! just comment
                 
      CALL CalcFragConnect(SortFragAtoms,SortFragInfo)
      deallocate(FragAtoms,TEMPFragAtoms, FragInfo, countLevel)
      deallocate(CountAtomicN,CountAtomicI,excludeLis,CountAtomicT) 
      deallocate(SumZ,SumZ_x,SumZ_y,SumZ_z,Z_x ,Z_y ,Z_z,)
      deallocate(Cent_x,Cent_y,Cent_z,Cent_R)
      end SUBROUTINE GetSortCart
      end MODULE Sorted_AIMDFT
