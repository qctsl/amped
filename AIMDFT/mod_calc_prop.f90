      SUBROUTINE CalcMolePropertiesDB(DatabaseInform, FragProp, FRgrids)
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:                                                             *
!*******************************************************************************
! MODULEs:

      USE module_grid_points
      USE mod_build    
      USE GetDataBaseInfo    
      
      implicit none
      
      ! Grid points by Jatom after RR
      type (GridsDataBase), dimension(:,:), allocatable, INTENT(IN) :: FRgrids  
      type (IndexFile), dimension (:), allocatable, INTENT(IN) :: DatabaseInform
      type (PropFile), dimension(:), allocatable, INTENT(IN) :: FragProp
      type (type_grid_points), dimension(:,:), allocatable :: grid_point_DB

      double precision, dimension(:,:), allocatable :: Bweights
      double precision, dimension(:,:), allocatable :: PWeights
      double precision, dimension(:,:), allocatable :: Vpot_r2

      double precision, dimension(:), allocatable :: EleNum 
      double precision, dimension(:), allocatable :: pot_energy
      double precision, dimension(:), allocatable :: colA_energy
      double precision, dimension(:), allocatable :: colN_energy

      double precision :: EleNum_tot
      double precision :: pot_energy_tot
      double precision :: colA_energy_tot
      double precision :: colN_energy_tot
      double precision :: EleNum_tot_prop
      double precision :: pot_energy_A_tot_prop
      double precision :: VeeAN_energy_tot_prop 
      double precision :: HFcoul_energy_tot_prop
      double precision :: HFexchange_tot_prop
      double precision :: coul_energy_tot_prop
      double precision :: exchange_tot_prop
      double precision :: kinetic_tot_prop
      double precision :: Vnn_tot
      double precision :: col_FullNum
      double precision :: Virial

      integer :: avlN, Jatom, points_num

      CHARACTER (64) :: message

      allocate (EleNum(Natoms))
      allocate (pot_energy(Natoms))
      allocate (colA_energy(Natoms))
      allocate (colN_energy(Natoms))

      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
       
      ! compute the weight for all grid points
      allocate (grid_point_DB(Natoms,MaxGridPnt))   
      allocate(PWeights(NAtoms,MaxGridPnt))
      allocate(Bweights(NAtoms,MaxGridPnt))
      allocate(Vpot_r2(NAtoms,MaxGridPnt))

      grid_point_DB(1:Natoms,1:MaxGridPnt)%x=FRgrids(1:Natoms,1:MaxGridPnt)%x
      grid_point_DB(1:Natoms,1:MaxGridPnt)%y=FRgrids(1:Natoms,1:MaxGridPnt)%y
      grid_point_DB(1:Natoms,1:MaxGridPnt)%z=FRgrids(1:Natoms,1:MaxGridPnt)%z
    
      do Jatom=1, Natoms
         if (.not.DatabaseInform(Jatom)%Availability) cycle
         points_num=DatabaseInform(Jatom)%GridsNum
         CALL GET_weights (grid_point_DB(Jatom,1:points_num), points_num, &
                           Jatom, Bweights(Jatom,1:points_num))
      end do      
      deallocate (grid_point_DB)

      write(*,'(/A)') '╔╦╗┬ ┬┌─┐  ╦═╗┌─┐┌─┐┬ ┬┬  ┌┬┐┌─┐'
      write(*,'(A)')  ' ║ ├─┤├┤   ╠╦╝├┤ └─┐│ ││   │ └─┐'
      write(*,'(A)')  ' ╩ ┴ ┴└─┘  ╩╚═└─┘└─┘└─┘┴─┘ ┴ └─┘'

      call calc_atomic_prop()

      message = 'Current partitioning weight'
      PWeights(1:Natoms,1:MaxGridPnt)=Bweights(1:Natoms,1:MaxGridPnt)
      call calc_density_prop(message)
      call prt_tot_energy

      message = 'Database partitioning weight'
      PWeights(1:Natoms,1:MaxGridPnt)=FRgrids(1:Natoms,1:MaxGridPnt)%BW
      call calc_density_prop(message)
      call prt_tot_energy

      call calc_error_density()

      !Print warning message
      avlN=0 
      do Jatom=1, NAtoms
      if (Cartesian(JAtom)%Atomic_Number.eq.0) cycle
      if (.not.DatabaseInform(Jatom)%Availability) then
          avlN=avlN+1
          write(*,'(A36,I3,A17)') "WARNING :: the fragment of atom# ", Jatom,&
                                  " is not avaliable"
      end if
      end do
      if (avlN.ne.0) write(*,'(A12,I2,A19)') 'There are ', avlN, &
                                             ' warning messege[s]'
!      call Stores_error_files()
      deallocate(EleNum)
CONTAINS !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      SUBROUTINE prt_tot_energy ()
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:   Calculate Number of electrons                             *
!*******************************************************************************
! MODULEs:
      implicit none
      
      call calc_Vnn()
      write(*,'(A32, F16.8)') "ThE Total Energy          =", Vnn_tot + &
                                                      pot_energy_tot + & 
                                                     colN_energy_tot + &
                                                    kinetic_tot_prop + &
                                                    HFexchange_tot_prop
                                                 
      if (kinetic_tot_prop .ne. ZERO) then
         write(*,'(A32, F16.8/)')  "ThE Virial                =", -(Vnn_tot + &
                                                             pot_energy_tot + &
                                                            colN_energy_tot + &
                                                       HFexchange_tot_prop) / &
                                                       kinetic_tot_prop
      end if

      end SUBROUTINE  prt_tot_energy      
      SUBROUTINE calc_density_prop(cw)
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:   Calculate Number of electrons                             *
!*******************************************************************************
! MODULEs:
      implicit none

      CHARACTER (64), INTENT(IN) :: cw


      call calc_elec()
      call calc_Vne()
      call calc_Vee_FullNum()
      call calc_Vee_anal()

      write(*,'(/A)')'+--------------------------------------------------------+'
      write(*,'(A)') '| Calculated molecule properties using the stored        |'
      write(*,'(A)') '| electron density in each radial grid point             |'
      write(*,'(A8,A48,A2)') '| NOTE: ',cw,' |'
      write(*,'(A)') '+-----+----------------+----------------+----------------+'
      write(*,'(A)') '|Atom#|   Electrons    |    Vne         | Vee            |'
      write(*,'(A)') '+-----+----------------+----------------+----------------+'
      
      do Jatom=1, NAtoms
        if (.not.FragProp(Jatom)%Availability) cycle
      write(*,'(A1,I5,3(A1,F16.8),A1)') '|',Jatom, &
                                     &  '|',EleNum(Jatom), &
                                     &  '|',-pot_energy(Jatom), &
                                     &  '|', colN_energy(Jatom), &
                                     &  '|'
      end do
      write(*,'(A)') '+-----+----------------+----------------+----------------+'
      write(*,'(A6,3(A1,F16.8),A1)') '|Sum= ', &
                                      & '|',EleNum_tot ,&
                                      & '|',pot_energy_tot  ,&
                                      & '|',colN_energy_tot       ,'|'
      write(*,'(A)') '+-----+----------------+----------------+----------------+'

      end SUBROUTINE calc_density_prop
      SUBROUTINE calc_elec()
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:   Calculate Number of electrons                             *
!*******************************************************************************
! MODULEs:
      implicit none
      
      double precision :: point_charge
      integer :: Ipoint, Jatom
      
      EleNum=0.0d0
      EleNum_tot=0.0d0
      do Jatom=1, NAtoms
        if (.not.DatabaseInform(Jatom)%Availability) cycle
        do Ipoint=1, DatabaseInform(Jatom)%GridsNum
          point_charge  =  FRgrids(Jatom,Ipoint)%D*&
                           FRgrids(Jatom,Ipoint)%W*&
                           PWeights(Jatom,Ipoint) 
          EleNum(Jatom) = EleNum(Jatom)+point_charge
        end do 
        EleNum(Jatom) = FourPi*EleNum(Jatom)
        EleNum_tot = EleNum_tot + EleNum(Jatom)
      end do
      return
      end SUBROUTINE calc_elec
      SUBROUTINE calc_Vne()
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:   Calculate V_ne, Potential                                 *
!*******************************************************************************
! MODULEs:
      implicit none
      
      double precision :: point_charge, NWeight, temp
      integer :: Ipoint, Jatom, Iatom, JZ_num

      pot_energy_tot=ZERO
      pot_energy = ZERO     
      do Iatom=1, Natoms
      JZ_num = CARTESIAN(Iatom)%Atomic_Number
      if (JZ_num .le. 0) cycle    
      do Jatom = 1,Natoms
        if (.not.DatabaseInform(Jatom)%Availability) cycle
        do Ipoint = 1,DatabaseInform(Jatom)%GridsNum   
          point_charge  =  FRgrids(Jatom,Ipoint)%D*&
                           FRgrids(Jatom,Ipoint)%W*&
                           PWeights(Jatom,Ipoint)
          temp = JZ_num*point_charge/    &
             dsqrt((FRgrids(Jatom,Ipoint)%x-CARTESIAN(Iatom)%X)**2 + &
                   (FRgrids(Jatom,Ipoint)%y-CARTESIAN(Iatom)%Y)**2 + &
                   (FRgrids(Jatom,Ipoint)%z-CARTESIAN(Iatom)%Z)**2)
          pot_energy(Iatom) = pot_energy(Iatom) + temp
         end do ! Ipoint
      end do ! Jatom
      pot_energy(Iatom)= FourPi*pot_energy(Iatom)
      pot_energy_tot = pot_energy_tot - pot_energy(Iatom)
      end do
      return
      end SUBROUTINE calc_Vne
      SUBROUTINE calc_Vnn()
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:   Calculate V_nn, Potential                                 *
!*******************************************************************************
! MODULEs:
      implicit none
      
      double precision :: temp
      integer :: Jatom, Iatom, JZ_num, IZ_num
      double precision ::  pot_energy_point

      Vnn_tot=ZERO
      do Iatom=1, Natoms
      pot_energy_point = ZERO     
      IZ_num = CARTESIAN(Iatom)%Atomic_Number
      if (IZ_num .le. 0) cycle    
      do Jatom = Iatom+1, Natoms
        JZ_num = CARTESIAN(Jatom)%Atomic_Number
        if (JZ_num .le. 0) cycle    
          temp = JZ_num*IZ_num/    &
             dsqrt((CARTESIAN(Jatom)%x-CARTESIAN(Iatom)%X)**2 + &
                   (CARTESIAN(Jatom)%y-CARTESIAN(Iatom)%Y)**2 + &
                   (CARTESIAN(Jatom)%z-CARTESIAN(Iatom)%Z)**2)
          pot_energy_point = pot_energy_point + temp
      end do ! Jatom
      Vnn_tot = Vnn_tot + pot_energy_point
      end do
      write(*,'(/A32,F16.8)')   "Nuclear repulsion (Vnn)   =", Vnn_tot
      return
      end SUBROUTINE calc_Vnn
      SUBROUTINE calc_Vee_anal()
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:   Calculate V_ee (half analytical, from the fragments),     *
!                    Coulomb repulsion                                         *
!*******************************************************************************
! MODULEs:
      implicit none   
      
      double precision :: point_charge, NWeight
      integer :: Ipoint, Jatom, JZ_num
      
      colA_energy = ZERO     
      colA_energy_tot=ZERO
      do Jatom=1, Natoms
      JZ_num = CARTESIAN(Jatom)%Atomic_Number
      if (JZ_num .le. 0) cycle    
      do Ipoint = 1,DatabaseInform(Jatom)%GridsNum
        if (.not.DatabaseInform(Jatom)%Availability) cycle
          point_charge  =  FRgrids(Jatom,Ipoint)%VpotA*&
                           FRgrids(Jatom,Ipoint)%D*&
                           FRgrids(Jatom,Ipoint)%W*&
                           PWeights(Jatom,Ipoint)/TWO
          colA_energy(Jatom) = colA_energy(Jatom) + point_charge
      end do 
      colA_energy(Jatom) = FourPi*colA_energy(Jatom)
      colA_energy_tot = colA_energy_tot + colA_energy(Jatom)
      end do    
      return
      end SUBROUTINE calc_Vee_anal
      SUBROUTINE calc_Vee_FullNum() 
 !******************************************************************************
 !    Date last modified:                                                      *
 !    Author: Ibrahim Awad                                                     *
 !    Description:   Calculate V_ee (Full Numerical), Coulomb repulsion        *
 !******************************************************************************
 ! MODULEs:
      implicit none
      
      double precision :: point_charge, NWeight 
      integer :: Ipoint, Jatom, JZ_num
      double precision :: NWeightI, NWeightJ, temp, Pcharge
      integer :: Jpoint, Iatom
 
      colN_energy=ZERO
      colN_energy_tot=ZERO
      do Iatom=1, Natoms
         JZ_num = CARTESIAN(Iatom)%Atomic_Number
         if (JZ_num .le. 0) cycle
         do Ipoint = 1,DatabaseInform(Iatom)%GridsNum
            if (.not.DatabaseInform(Iatom)%Availability) cycle
            do Jatom=1, Natoms
               JZ_num = CARTESIAN(Jatom)%Atomic_Number
               if (JZ_num .le. 0) cycle
               do Jpoint = 1,DatabaseInform(Jatom)%GridsNum
                  if (.not.DatabaseInform(Jatom)%Availability) cycle
                  if(Ipoint.eq.Jpoint .and. Iatom.eq.Jatom) cycle
                  NWeightJ=PWeights(Jatom,Jpoint)
                  NWeightI=PWeights(Iatom,Ipoint)
                  Pcharge=FRgrids(Iatom,Ipoint)%D*FRgrids(Jatom,Jpoint)%D &
                       & *FRgrids(Iatom,Ipoint)%W*FRgrids(Jatom,Jpoint)%W &
                       & *NWeightJ*NWeightI
                  temp = Pcharge/    &
                  DSQRT((FRgrids(Jatom,Jpoint)%x-FRgrids(Iatom,Ipoint)%x)**2+ &
                        (FRgrids(Jatom,Jpoint)%y-FRgrids(Iatom,Ipoint)%y)**2+ &
                        (FRgrids(Jatom,Jpoint)%z-FRgrids(Iatom,Ipoint)%z)**2)
                  colN_energy(Iatom)=colN_energy(Iatom)+temp
               end do ! Jpoint = Ipoint + 1,DatabaseInform(Jatom)%GridsNum
            end do ! Jatom=1, Natoms
         end do ! Ipoint = 1,DatabaseInform(Iatom)%GridsNum
         colN_energy(Iatom)=0.5d0*FourPi*FourPi*colN_energy(Iatom)
         colN_energy_tot=colN_energy_tot+colN_energy(Iatom)
      end do ! Iatom=1, Natoms
      return
      end SUBROUTINE calc_Vee_FullNum
      SUBROUTINE calc_Vpotr2_pt(Iatom,Ipoint,vpot_pt) 
 !******************************************************************************
 !    Date last modified:                                                      *
 !    Author: Ibrahim Awad                                                     *
 !    Description:   Calculate V_ee (Full Numerical), Coulomb repulsion        *
 !******************************************************************************
 ! MODULEs:
      implicit none
      
      integer :: Jatom,Jpoint,JZ_num,Iatom,Ipoint
      double precision :: NWeightJ, PchargeJ, densityXYZ,GET_density_at_xyz
      double precision :: vpot_pt, SQRTr
 
      vpot_pt=ZERO
      do Jatom=1, Natoms
         JZ_num = CARTESIAN(Jatom)%Atomic_Number
         if (JZ_num .le. 0) cycle
         if (.not.DatabaseInform(Jatom)%Availability) cycle
         do Jpoint = 1,DatabaseInform(Jatom)%GridsNum
            if(Ipoint.eq.Jpoint .and. Iatom.eq.Jatom) cycle
              NWeightJ=PWeights(Jatom,Jpoint)
              PchargeJ=FRgrids(Jatom,Jpoint)%W*&
                       NWeightJ*FRgrids(Jatom,Jpoint)%D  !densityXYZ!
              SQRTr=DSQRT((FRgrids(Jatom,Jpoint)%x-FRgrids(Iatom,Ipoint)%x)**2+&
                  (FRgrids(Jatom,Jpoint)%y-FRgrids(Iatom,Ipoint)%y)**2+ &
                  (FRgrids(Jatom,Jpoint)%z-FRgrids(Iatom,Ipoint)%z)**2)
              vpot_pt = vpot_pt + PchargeJ/SQRTr    
         end do ! Ipoint = 1,DatabaseInform(Iatom)%GridsNum
      end do ! Iatom=1, Natoms
      vpot_pt=vpot_pt*FourPi                      
      return
      end SUBROUTINE calc_Vpotr2_pt
      SUBROUTINE calc_Vee_FullNum2() 
 !******************************************************************************
 !    Date last modified:                                                      *
 !    Author: Ibrahim Awad                                                     *
 !    Description:   Calculate V_ee (Full Numerical), Coulomb repulsion        *
 !******************************************************************************
 ! MODULEs:
      implicit none
      
      double precision :: point_chargeI, NWeight 
      integer :: Ipoint, Jatom, JZ_num
      double precision :: NWeightI, vpot_pt, temp
      double precision :: PchargeI, densityXYZ, GET_density_at_xyz
      integer :: Jpoint, Iatom
 
      colN_energy=ZERO
      colN_energy_tot=ZERO
      do Iatom=1, Natoms
         JZ_num = CARTESIAN(Iatom)%Atomic_Number
         if (JZ_num .le. 0) cycle
         do Ipoint = 1,DatabaseInform(Iatom)%GridsNum
            if (.not.DatabaseInform(Iatom)%Availability) cycle
            NWeightI=PWeights(Iatom,Ipoint)
            PchargeI=FRgrids(Iatom,Ipoint)%W*NWeightI*FRgrids(Iatom,Ipoint)%D
            call calc_Vpotr2_pt(Iatom,Ipoint,vpot_pt)
            temp = PchargeI*vpot_pt!FRgrids(Iatom,Ipoint)%VpotA!
            colN_energy(Iatom)=colN_energy(Iatom)+temp
         end do ! Ipoint = 1,DatabaseInform(Iatom)%GridsNum
         colN_energy(Iatom)=0.50d0*FourPi*colN_energy(Iatom)
         colN_energy_tot=colN_energy_tot+colN_energy(Iatom)
      end do ! Iatom=1, Natoms
      return
      end SUBROUTINE calc_Vee_FullNum2
      SUBROUTINE calc_atomic_prop()
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:   Calculate Number of electrons                             *
!*******************************************************************************
! MODULEs:
      implicit none
      
      integer :: Jatom
      
      EleNum_tot_prop=ZERO
      pot_energy_A_tot_prop=ZERO
!      pot_energy_N_tot_prop=ZERO
      VeeAN_energy_tot_prop=ZERO
      HFexchange_tot_prop=ZERO
      HFcoul_energy_tot_prop=ZERO
      exchange_tot_prop=ZERO
      coul_energy_tot_prop=ZERO
      kinetic_tot_prop=ZERO

      write(*,'(/A)')'+--------------------------------------------------------&
        &---------------------------------------------------------------------+'
      write(*,'(A)') '|    Calculated molecule properties using the stored frag&
        &ments properties                                                     |'
      write(*,'(A)') '|    (already calculated, using fragment weigths and geom&
        &etry)                                                                |'
      write(*,'(A)') '+-----+--------------+--------------+--------------+-----&
        &---------+--------------+--------------+--------------+--------------+'
      write(*,'(A)') '|Atom#|   Electrons  | Potential Vne| Vee Ana/Num  | HF &
       &Coulomb J |HF Exchange K |  Coulomb J   |  Exchange K  |  Kinetic T   |'
      write(*,'(A)') '+-----+--------------+--------------+--------------+-----&
        &---------+--------------+--------------+--------------+--------------+'
      do Jatom=1, NAtoms
        if (.not.FragProp(Jatom)%Availability) cycle
        write(*,'(A1,I5,8(A1,F14.8),A1)') '|',Jatom, &
                     & '|',FragProp(Jatom)%ElectonNum, &
                     & '|',FragProp(Jatom)%Vne_A, &
         !           & '|',FragProp(Jatom)%Vne_N, & ! No need same as Vne_A
                     & '|',FragProp(Jatom)%Vee_AN, &
                     & '|',FragProp(Jatom)%JHF, &
                     & '|',FragProp(Jatom)%KHF, &
                     & '|',FragProp(Jatom)%J,   &
                     & '|',FragProp(Jatom)%K,   &
                     & '|',FragProp(Jatom)%T, '|'

        EleNum_tot_prop=EleNum_tot_prop+FragProp(Jatom)%ElectonNum
        pot_energy_A_tot_prop=pot_energy_A_tot_prop+FragProp(Jatom)%Vne_A
 !       pot_energy_N_tot_prop = pot_energy_N_tot_prop + FragProp(Jatom)%Vne_N
        VeeAN_energy_tot_prop=VeeAN_energy_tot_prop+FragProp(Jatom)%Vee_AN
        HFcoul_energy_tot_prop=HFcoul_energy_tot_prop + FragProp(Jatom)%JHF
        HFexchange_tot_prop=HFexchange_tot_prop + FragProp(Jatom)%KHF
        coul_energy_tot_prop=coul_energy_tot_prop + FragProp(Jatom)%J
        exchange_tot_prop=exchange_tot_prop + FragProp(Jatom)%K
        kinetic_tot_prop=kinetic_tot_prop + FragProp(Jatom)%T
      end do
      write(*,'(A)') '+-----+--------------+--------------+--------------+-----&
        &---------+--------------+--------------+--------------+--------------+'
      write(*,'(A6,8(A1,F14.8),A1)') '|Sum= ', &
                                      & '|',EleNum_tot_prop ,&
                                      & '|',pot_energy_A_tot_prop  ,&
                             !        No need same as Vne_A
                             !        & '|',pot_energy_N_tot_prop  ,& 
                                      & '|',VeeAN_energy_tot_prop ,&
                                      & '|',HFcoul_energy_tot_prop ,&
                                      & '|',HFexchange_tot_prop    ,&
                                      & '|',coul_energy_tot_prop   ,&
                                      & '|',exchange_tot_prop      ,&
                                      & '|',kinetic_tot_prop       ,'|'
      write(*,'(A)') '+-----+--------------+--------------+--------------+-----&
                     &---------+--------------+--------------+--------------+--&
                     &------------+'
      return
      end SUBROUTINE calc_atomic_prop
      SUBROUTINE calc_error_density()
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description: Print the correct and the AIMDFT number of electrons        *
!*******************************************************************************
! MODULEs:
      implicit none

      double precision :: densityXYZ, GET_density_at_xyz
      integer :: Ipoint
      integer :: Jatom
      logical :: Lerror
      character (60) :: PlotFilenames
      double precision :: xpoint, ypoint, zpoint
      double precision :: density_w,ABSerror1, ABSerror2
      double precision :: AtomTotError,TotError1,TotError2
      double precision :: TDensity, TAIMDensity1,TAIMDensity2
      double precision :: AIMDFTsity_w1,AIMDFTsity_w2
      double precision, dimension(:), allocatable ::  AtomDensity 
      double precision, dimension(:), allocatable ::  AtomAIMDFTDensity1
      double precision, dimension(:), allocatable ::  AtomAIMDFTDensity2
      double precision, dimension(:), allocatable ::  ErrorDensity_w1
      double precision, dimension(:), allocatable ::  ErrorDensity_w2
      
      allocate(AtomDensity(NAtoms))
      allocate(AtomAIMDFTDensity1(NAtoms))
      allocate(AtomAIMDFTDensity2(NAtoms))
      allocate(ErrorDensity_w1(NAtoms))
      allocate(ErrorDensity_w2(NAtoms))

      TDensity=ZERO
      TAIMDensity1=ZERO
      TAIMDensity2=ZERO
      TotError1=ZERO
      TotError2=ZERO
      AtomDensity=ZERO
      AtomAIMDFTDensity1=ZERO
      AtomAIMDFTDensity2=ZERO
      ErrorDensity_w1=ZERO
      ErrorDensity_w2=ZERO
      write(*,'(/A6,A12,6A14)') 'Atom#','AIM_FD','AIM_CW','HF','|Error FD|', &
                                     & '|Error CW|','|%Error FD|','|%Error CW|'
      do Jatom=1, NAtoms
         if (.not.DatabaseInform(Jatom)%Availability) cycle
         do Ipoint=1, DatabaseInform(Jatom)%GridsNum

            xpoint=FRgrids(Jatom,Ipoint)%x-CARTESIAN(Jatom)%x
            ypoint=FRgrids(Jatom,Ipoint)%y-CARTESIAN(Jatom)%y
            zpoint=FRgrids(Jatom,Ipoint)%z-CARTESIAN(Jatom)%z

            densityXYZ=GET_density_at_xyz(FRgrids(Jatom,Ipoint)%x,&
                                          FRgrids(Jatom,Ipoint)%y,&
                                          FRgrids(Jatom,Ipoint)%z)
            density_w=densityXYZ*FRgrids(Jatom,Ipoint)%W*BWeights(Jatom,Ipoint)
            AIMDFTsity_w1=FRgrids(Jatom,Ipoint)%D*&
                          FRgrids(Jatom,Ipoint)%W*&
                          FRgrids(Jatom,Ipoint)%BW
            AIMDFTsity_w2=FRgrids(Jatom,Ipoint)%D*&
                          FRgrids(Jatom,Ipoint)%W*&
                          BWeights(Jatom,Ipoint)
            ABSerror1=AIMDFTsity_w1-density_w
            ABSerror2=AIMDFTsity_w2-density_w

            AtomDensity(Jatom)=AtomDensity(Jatom)+density_w
            AtomAIMDFTDensity1(Jatom)=AtomAIMDFTDensity1(Jatom)+AIMDFTsity_w1
            AtomAIMDFTDensity2(Jatom)=AtomAIMDFTDensity2(Jatom)+AIMDFTsity_w2
         end do 

         AtomDensity(Jatom)=FourPi*AtomDensity(Jatom)
         AtomAIMDFTDensity1(Jatom)=FourPi*AtomAIMDFTDensity1(Jatom)
         AtomAIMDFTDensity2(Jatom)=FourPi*AtomAIMDFTDensity2(Jatom)

         TDensity=TDensity+AtomDensity(Jatom)
         TAIMDensity1=TAIMDensity1+AtomAIMDFTDensity1(Jatom)
         TAIMDensity2=TAIMDensity2+AtomAIMDFTDensity2(Jatom)

         ErrorDensity_w1(Jatom)=AtomAIMDFTDensity1(Jatom)-AtomDensity(Jatom)
         ErrorDensity_w2(Jatom)=AtomAIMDFTDensity2(Jatom)-AtomDensity(Jatom)

         TotError1=TotError1+ErrorDensity_w1(Jatom)
         TotError2=TotError2+ErrorDensity_w2(Jatom)

         if(AtomDensity(Jatom).ne.ZERO) then
         write(*,'(I4,7F14.8)') Jatom, &
                        & AtomAIMDFTDensity1(Jatom),  &
                        & AtomAIMDFTDensity2(Jatom),  &
                        & AtomDensity(Jatom),         &
                        & ErrorDensity_w1(Jatom),     &
                        & ErrorDensity_w2(Jatom),     &
                        & ErrorDensity_w1(Jatom)/AtomDensity(Jatom)*100.0d0, &
                        & ErrorDensity_w2(Jatom)/AtomDensity(Jatom)*100.0d0
         end if
      end do
      if (TDensity.ne.ZERO) then
         write(*,'(A4,7F14.8)') 'Sum=', TAIMDensity1,TAIMDensity2, &
                                      & TDensity,TotError1,TotError2, &
                                      & TotError1/TDensity*100.0d0, &
                                      & TotError2/TDensity*100.0d0
      end if
      return
      end SUBROUTINE calc_error_density
      SUBROUTINE Stores_error_files() 
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description: ! Just to check the denisty at each radial grid point....   *
!*******************************************************************************
! MODULEs:
      implicit none
      
      double precision :: densityXYZ, GET_density_at_xyz
      integer :: File_unit
      integer :: Ipoint
      integer :: Jatom
      logical :: Lerror
      character (60) :: PlotFilenames
      double precision :: xpoint, ypoint, zpoint
      double precision :: ABSerror1, ABSerror2
      double precision :: density_w, AIMDFTsity_w1,AIMDFTsity_w2

      do Jatom=1, NAtoms
         if (.not.DatabaseInform(Jatom)%Availability) cycle
         !<<<<<<<<<<<IMPORTANT:::::: should be change
         call INPUT_File_FRAG ('GRIDError_','.dat',Jatom, PlotFilenames)   
         call GET_unit (PlotFilenames, File_unit, Lerror)        
         open(UNIT=File_unit,file=PlotFilenames, &
              status='REPLACE',form='formatted')
         write(File_unit,'(3A12,A18,6A17)') 'X','Y','Z','Bweight','RadWeight', &
                                          & 'AIM_Density_FW','AIM_Density_CW', &
                                          & 'Density_w','Diff1','Diff2'
         do Ipoint=1, DatabaseInform(Jatom)%GridsNum

            xpoint=FRgrids(Jatom,Ipoint)%x-CARTESIAN(Jatom)%x
            ypoint=FRgrids(Jatom,Ipoint)%y-CARTESIAN(Jatom)%y
            zpoint=FRgrids(Jatom,Ipoint)%z-CARTESIAN(Jatom)%z

            densityXYZ=GET_density_at_xyz (FRgrids(Jatom,Ipoint)%x, &
                                           FRgrids(Jatom,Ipoint)%y, &
                                           FRgrids(Jatom,Ipoint)%z)
            density_w=densityXYZ*&
                      FRgrids(Jatom,Ipoint)%W*&
                      BWeights(Jatom,Ipoint)
            AIMDFTsity_w1=FRgrids(Jatom,Ipoint)%D*&
                          FRgrids(Jatom,Ipoint)%W*&
                          FRgrids(Jatom,Ipoint)%BW
            AIMDFTsity_w2=FRgrids(Jatom,Ipoint)%D*&
                          FRgrids(Jatom,Ipoint)%W*&
                          BWeights(Jatom,Ipoint)
            ABSerror1=AIMDFTsity_w1-density_w
            ABSerror2=AIMDFTsity_w2-density_w

            write(File_unit,Pfmt_rho_dd)   xpoint,   &
                                         & ypoint,   &
                                         & zpoint,   &
                                         & FRgrids(Jatom,Ipoint)%BW,   &
                                         & FRgrids(Jatom,Ipoint)%W,   &
                                         & AIMDFTsity_w1,   &
                                         & AIMDFTsity_w2,   &
                                         & density_w   ,    &
                                         & ABSerror1,       &
                                         & ABSerror2
         end do 
         close(UNIT=File_unit)
      end do
      return
      end SUBROUTINE Stores_error_files
      end SUBROUTINE CalcMolePropertiesDB 
