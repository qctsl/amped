      MODULE AddTerminal
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:                                                             *
!*******************************************************************************
! 
      USE mod_fragment
               
      implicit none
      
CONTAINS !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 

      SUBROUTINE AddTerminalAtoms(adlevel,FragAtoms,FragInfo) 
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:                                                             *
!*******************************************************************************
!
      USE mod_math

      implicit none

      type (FragAtomInfo),dimension(:,:),intent(inout),allocatable :: FragAtoms
      type (FragsInfo),dimension(:),intent(inout),allocatable :: FragInfo    
      integer, intent(in) :: adlevel
   
      type (FragAtomInfo), dimension(:), allocatable :: termatoms
      type (FragAtomInfo), dimension(:), allocatable :: DBterm
      type (FragAtomInfo), dimension(:), allocatable :: Newtermatoms  
      type (FragAtomInfo), dimension(:), allocatable :: NewDBterm
            
      double precision, dimension (:,:), allocatable :: RR,RRI
      double precision, dimension (:) , allocatable :: TT
      double precision, dimension (1:3) :: NewPoint, point
          
      integer :: loop3, oldloop3, iloop, NewLoop
      integer :: Jatom, iMUNatom, ifound, ifound2, ifound3
      double precision :: bondtype
      logical :: termenalTF
      
      allocate (termatoms(Natoms))
      allocate (DBterm(Natoms))
      allocate (Newtermatoms(Natoms))
      allocate (NewDBterm(Natoms))
      allocate (RR(3,3))
      allocate (RRI(3,3))
      allocate (TT(3))
      
      CALL GET_object ('MOL', 'GRAPH', 'CONNECT')
      CALL GET_object ('QM', 'BOND_ORDER', 'MEYER')
      
      do JAtom=1, NAtoms
         ! loop over number of adjacent atoms for Jatom
         do ifound=1, FragInfo(Jatom)%NAtoms      
            iMUNatom=FragAtoms(Jatom,ifound)%MUNIdx
            if(ChangeTerminalAtoms) then
               termenalTF=.true.
            else
               ! In the case if the atom is termenal then leave it same
               termenalTF=(FragInfo(iMUNatom)%NumNeighbourAtoms.ne.1) 
            end if
            if ((FragAtoms(Jatom,ifound)%level.ne.(adlevel))) cycle
            if (.not.termenalTF) cycle
            
            ! found one atom (2 >>> to be in z-axes) 
            termatoms(2)=FragAtoms(Jatom,ifound)  
            do ifound2=1, FragInfo(Jatom)%NAtoms
               ! Look for atom with (level-1) of the intrest atom
               if (FragAtoms(Jatom,ifound2)%level.ne.(adlevel-1)) cycle 
               ! find the parent atom
               if (Connect(FragAtoms(Jatom,ifound2)%MUNIdx,&
                               termatoms(2)%MUNIdx).ne.1) cycle 
               termatoms(1)=FragAtoms(Jatom,ifound2) ! found the origin atom
            end do
            loop3=2  ! number of termenal atoms    
            do ifound3=1, FragInfo(Jatom)%NAtoms
               if (Connect(termatoms(1)%MUNIdx,&
                 FragAtoms(Jatom,ifound3)%MUNIdx).ne.1) cycle
               if (FragAtoms(Jatom,ifound3)%MUNIdx.eq.termatoms(2)%MUNIdx) cycle
               loop3=loop3+1
               termatoms(loop3)=FragAtoms(Jatom,ifound3) ! the termenal atoms   
            end do
            ! find the bond type of the intrest atom with the origin
            bondtype= bond_order(termatoms(2)%MUNIdx,termatoms(1)%MUNIdx)
   !         write(*,*) termatoms(2)%MUNIdx,termatoms(1)%MUNIdx,bondtype
            CALL GetDBCart(termatoms,DBterm,loop3,RR,TT)
            CALL INV33(RR,RRI)
            
            oldloop3=loop3
            if(bondtype.le.0.5)then ! Partial single bond (TS?)
               CALL getterm1 (DBterm,loop3,NewDBterm,NewLoop)
            else if(bondtype.le.1.15)then ! Single bond
               CALL getterm2 (DBterm,loop3,NewDBterm,NewLoop)
            else if(bondtype.le.1.6)then ! Aromatic double bond
               CALL getterm3 (DBterm,loop3,NewDBterm,NewLoop)
            else if(bondtype.le.2.2)then ! Double bond
               CALL getterm4 (DBterm,loop3,NewDBterm,NewLoop)
            else if(bondtype.le.2.7)then ! Aromatic triple bond
               CALL getterm5 (DBterm,loop3,NewDBterm,NewLoop)
            else if(bondtype.le.3.2)then ! Triple bond
               CALL getterm6 (DBterm,loop3,NewDBterm,NewLoop)
            end if
             
            do iloop=loop3+1, NewLoop 
              point=(/NewDBterm(iloop)%x,NewDBterm(iloop)%y,NewDBterm(iloop)%z/)
              NewPoint=Matmul(RRI,(point-TT))
              Newtermatoms(iloop)%x=NewPoint(1)
              Newtermatoms(iloop)%y=NewPoint(2)
              Newtermatoms(iloop)%z=NewPoint(3)
            end do
             
            FragAtoms(Jatom,ifound)%Atomic_Number=NewDBterm(2)%Atomic_Number
            FragAtoms(Jatom,ifound)%element=NewDBterm(2)%element
             
            do iloop=loop3+1, NewLoop
               FragInfo(Jatom)%NAtoms=FragInfo(Jatom)%NAtoms+1
               FragAtoms(Jatom,FragInfo(Jatom)%NAtoms)=Newtermatoms(iloop)
               FragAtoms(Jatom,FragInfo(Jatom)%NAtoms)%Atomic_Number=&
                                        NewDBterm(iloop)%Atomic_Number
               FragAtoms(Jatom,FragInfo(Jatom)%NAtoms)%element=&
                                        NewDBterm(iloop)%element
               FragAtoms(Jatom,FragInfo(Jatom)%NAtoms)%type=999
               FragAtoms(Jatom,FragInfo(Jatom)%NAtoms)%ctype=&
                                        NewDBterm(iloop)%ctype 
               FragAtoms(Jatom,FragInfo(Jatom)%NAtoms)%factor=-1
               FragAtoms(Jatom,FragInfo(Jatom)%NAtoms)%level=adlevel+1 
               FragAtoms(Jatom,FragInfo(Jatom)%NAtoms)%MUNIdx=-1      
            end do
         end do ! ifound=1, SATOMS(Jatom)
      end do ! JAtom=1, NAtoms
      
      CALL CalcFragConnect(FragAtoms,FragInfo)
      CALL UpdateFragTypes(FragAtoms,FragInfo)
      deallocate(termatoms,DBterm,Newtermatoms,NewDBterm,RR,TT,RRI)     
      return
      end SUBROUTINE AddTerminalAtoms
      SUBROUTINE getterm1 (DBterm,loop3,NewDBterm,NewLoop)
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:                                                             *
!*******************************************************************************
! 
      implicit none
      
      type (FragAtomInfo), dimension(:), allocatable, intent(in) :: DBterm
      integer, intent(in) :: loop3  ! +1 for new atoms
      type (FragAtomInfo), dimension(:), allocatable, intent(out) :: NewDBterm
      integer, intent(out) :: NewLoop  ! +1 for new atoms
      type (FragAtomInfo), dimension(:), allocatable  :: newatoms
      
      double precision :: CH_bond, Anglea
      integer :: NNewatoms, iloop
      
      allocate (NewDBterm(Natoms))
      
      NewDBterm=DBterm
      NNewatoms=0
      
    !  select case(NewDBterm(2)%Atomic_Number) 
    !    case(1)  ! H atom
         !!!!!!!!!!!!!!!!!
    !    case(2)  ! He atom
         !!!!!!!!!!!!!!!!!
    !    case(3)
         !!!!!!!!!!!!!!!!!
    !    case(6)  ! C atom
         !!!!!!!!!!!!!!!!!              
    !    case(8)  ! O atom
         !!!!!!!!!!!!!!!!!
    !    CASE DEFAULT
    !      NewDBterm(2)%Atomic_Number=TerminalAtom
    !      NewDBterm(2)%element=element_symbols(TerminalAtom)  
    !  end select
      
      NewLoop=loop3
      do iloop=1, NNewatoms
          NewLoop=NewLoop+1    
          NewDBterm(NewLoop)%x=newatoms(iloop)%x+DBterm(2)%x
          NewDBterm(NewLoop)%y=newatoms(iloop)%y+DBterm(2)%y
          NewDBterm(NewLoop)%z=newatoms(iloop)%z+DBterm(2)%z
          NewDBterm(NewLoop)%Atomic_Number=newatoms(iloop)%Atomic_Number
          NewDBterm(NewLoop)%element=newatoms(iloop)%element
          NewDBterm(NewLoop)%ctype=newatoms(iloop)%ctype
      end do
      
      if (allocated(newatoms)) deallocate(newatoms) ! if claUSE is necessary   
      end SUBROUTINE getterm1
      
      SUBROUTINE getterm2 (DBterm,loop3,NewDBterm,NewLoop)
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:                                                             *
!*******************************************************************************
! 
      implicit none
      
      type (FragAtomInfo), dimension(:), allocatable, intent(in) :: DBterm
      integer, intent(in) :: loop3  ! +1 for new atoms
      type (FragAtomInfo), dimension(:), allocatable, intent(out) :: NewDBterm
      integer, intent(out) :: NewLoop  ! +1 for new atoms
      type (FragAtomInfo), dimension(:), allocatable  :: newatoms
      
      double precision :: HO_bond, Anglea, CH_bond, NH_bond
      integer :: NNewatoms, iloop
      
      allocate (NewDBterm(Natoms))
      
      NewDBterm=DBterm
      NNewatoms=0
      
      select case(NewDBterm(2)%Atomic_Number) 
        case(6)  ! C atom 
         NNewatoms=3
         allocate (newatoms(NNewatoms))
          ! NEW ATOMS FOR THE TERMENAL
          ! Single bond (-CH3)
          CH_bond=2.04781965 ! Bohr
          !Anglea=31.65 *0.0174533 ! Need Degree to RAD
          !H1
          newatoms(1)%x=0.0d0
          newatoms(1)%y=-1.93061734
          newatoms(1)%z=0.68284856
          newatoms(1)%Atomic_Number=TerminalAtom
          newatoms(1)%element=element_symbols(TerminalAtom)
          newatoms(1)%ctype="-CH3"
          !H2
          newatoms(2)%x=1.67199927
          newatoms(2)%y=0.96543892
          newatoms(2)%z=0.68284856
          newatoms(2)%Atomic_Number=TerminalAtom
          newatoms(2)%element=element_symbols(TerminalAtom)
          newatoms(2)%ctype="-CH3"
          !H3
          newatoms(3)%x=-1.67199914
          newatoms(3)%y= 0.96557695
          newatoms(3)%z= 0.68284856
          newatoms(3)%Atomic_Number=TerminalAtom
          newatoms(3)%element=element_symbols(TerminalAtom)
          newatoms(3)%ctype="-CH3"

        case(7)  ! N atom 
         NNewatoms=2
         allocate (newatoms(NNewatoms))
          ! NEW ATOMS FOR THE TERMENAL
          ! Single bond (-NH2)
          NH_bond=1.89445030 ! Bohr
          !Anglea=31.65 *0.0174533 ! Need Degree to RAD
          !H1
          newatoms(1)%x=0.0d0
          newatoms(1)%y=-1.80992957
          newatoms(1)%z=0.55955062
          newatoms(1)%Atomic_Number=TerminalAtom
          newatoms(1)%element=element_symbols(TerminalAtom)
          newatoms(1)%ctype="-NH2"
          !H2
          newatoms(2)%x=-1.64331915
          newatoms(2)%y=0.75857027
          newatoms(2)%z=0.55955062
          newatoms(2)%Atomic_Number=TerminalAtom
          newatoms(2)%element=element_symbols(TerminalAtom)
          newatoms(2)%ctype="-NH2"

         
        case(8)  ! O atom 
                       
          ! NEW ATOMS FOR THE TERMENAL
          ! Hydrogen bond (H-O)
          NNewatoms=1
          allocate (newatoms(NNewatoms))

          HO_bond=1.86970623 ! Bohr
          Anglea=14.5d0 *0.0174533 ! Need Degree to RAD 
          !H1 for O atom
          newatoms(1)%x=0.0d0
          newatoms(1)%y=-HO_bond*Cos(Anglea)
          newatoms(1)%z=HO_bond*Sin(Anglea)
          newatoms(1)%Atomic_Number=TerminalAtom
          newatoms(1)%element=element_symbols(TerminalAtom)
          newatoms(1)%ctype="-OH" 
    !    case(8)  ! O atom
         !!!!!!!!!!!!!!!!!
        CASE DEFAULT
          NewDBterm(2)%Atomic_Number=TerminalAtom
          NewDBterm(2)%element=element_symbols(TerminalAtom)
      end select
      
      NewLoop=loop3
      do iloop=1, NNewatoms
          NewLoop=NewLoop+1    
          NewDBterm(NewLoop)%x=newatoms(iloop)%x+DBterm(2)%x
          NewDBterm(NewLoop)%y=newatoms(iloop)%y+DBterm(2)%y
          NewDBterm(NewLoop)%z=newatoms(iloop)%z+DBterm(2)%z
          NewDBterm(NewLoop)%Atomic_Number=newatoms(iloop)%Atomic_Number
          NewDBterm(NewLoop)%element=newatoms(iloop)%element
          NewDBterm(NewLoop)%ctype=newatoms(iloop)%ctype
      end do
      
      if (allocated(newatoms)) deallocate(newatoms) ! if claUSE is necessary
      
      
      end SUBROUTINE getterm2
      
      SUBROUTINE getterm3 (DBterm,loop3,NewDBterm,NewLoop)
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:                                                             *
!*******************************************************************************
! 
      implicit none
      
      type (FragAtomInfo), dimension(:), allocatable, intent(in) :: DBterm
      integer, intent(in) :: loop3  ! +1 for new atoms
      type (FragAtomInfo), dimension(:), allocatable, intent(out) :: NewDBterm
      integer, intent(out) :: NewLoop  ! +1 for new atoms
      type (FragAtomInfo), dimension(:), allocatable  :: newatoms
      
      double precision :: CH_bond, Anglea
      integer :: NNewatoms, iloop
      
      allocate (NewDBterm(Natoms))
      
      NewDBterm=DBterm
      NNewatoms=0
      
    !  select case(NewDBterm(2)%Atomic_Number) 
    !    case(1)  ! H atom
         !!!!!!!!!!!!!!!!!
    !    case(2)  ! He atom
         !!!!!!!!!!!!!!!!!
    !    case(3)
         !!!!!!!!!!!!!!!!!
    !    case(6)  ! C atom
         !!!!!!!!!!!!!!!!!              
    !    case(8)  ! O atom
         !!!!!!!!!!!!!!!!!
    !    CASE DEFAULT
    !      NewDBterm(2)%Atomic_Number=TerminalAtom
    !      NewDBterm(2)%element=element_symbols(TerminalAtom)  
    !  end select
      
      NewLoop=loop3
      do iloop=1, NNewatoms
          NewLoop=NewLoop+1    
          NewDBterm(NewLoop)%x=newatoms(iloop)%x+DBterm(2)%x
          NewDBterm(NewLoop)%y=newatoms(iloop)%y+DBterm(2)%y
          NewDBterm(NewLoop)%z=newatoms(iloop)%z+DBterm(2)%z
          NewDBterm(NewLoop)%Atomic_Number=newatoms(iloop)%Atomic_Number
          NewDBterm(NewLoop)%element=newatoms(iloop)%element
          NewDBterm(NewLoop)%ctype=newatoms(iloop)%ctype  
      end do
      
      if (allocated(newatoms)) deallocate(newatoms) ! if claUSE is necessary
      end SUBROUTINE getterm3
      SUBROUTINE getterm4 (DBterm,loop3,NewDBterm,NewLoop)
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:                                                             *
!*******************************************************************************
! 
      implicit none
      
      type (FragAtomInfo), dimension(:), allocatable, intent(in) :: DBterm
      integer, intent(in) :: loop3  ! +1 for new atoms
      type (FragAtomInfo), dimension(:), allocatable, intent(out) :: NewDBterm
      integer, intent(out) :: NewLoop  ! +1 for new atoms
      type (FragAtomInfo), dimension(:), allocatable  :: newatoms
      
      double precision :: CH_bond, Anglea
      integer :: NNewatoms, iloop
      
      allocate (NewDBterm(Natoms))
      
      NewDBterm=DBterm
      NNewatoms=0
      
      select case(NewDBterm(2)%Atomic_Number) 
    !    case(1)  ! H atom
         !!!!!!!!!!!!!!!!!
    !    case(2)  ! He atom
         !!!!!!!!!!!!!!!!!
    !    case(3)
         !!!!!!!!!!!!!!!!!
         case(6)  ! C atom 
         NNewatoms=2
         allocate (newatoms(NNewatoms))
          ! NEW ATOMS FOR THE TERMENAL
          ! Double bond (=CH2)
          CH_bond=2.03456411 ! Bohr
          Anglea=31.65 *0.0174533 ! Need Degree to RAD
          !H1
          newatoms(1)%x=0.0d0
          newatoms(1)%y=CH_bond*Cos(Anglea)
          newatoms(1)%z=CH_bond*Sin(Anglea)
          newatoms(1)%Atomic_Number=TerminalAtom
          newatoms(1)%element=element_symbols(TerminalAtom)
          newatoms(1)%ctype="=CH2"
          !H2
          newatoms(2)%x=0.0d0
          newatoms(2)%y=-CH_bond*Cos(Anglea)
          newatoms(2)%z= CH_bond*Sin(Anglea)
          newatoms(2)%Atomic_Number=TerminalAtom
          newatoms(2)%element=element_symbols(TerminalAtom)
          newatoms(2)%ctype="=CH2"
         
                       
    !    case(8)  ! O atom
         !!!!!!!!!!!!!!!!!
    !    CASE DEFAULT
    !      NewDBterm(2)%Atomic_Number=TerminalAtom
    !      NewDBterm(2)%element=element_symbols(TerminalAtom)  
       end select
      
      
      NewLoop=loop3
      do iloop=1, NNewatoms
          NewLoop=NewLoop+1    
          NewDBterm(NewLoop)%x=newatoms(iloop)%x+DBterm(2)%x
          NewDBterm(NewLoop)%y=newatoms(iloop)%y+DBterm(2)%y
          NewDBterm(NewLoop)%z=newatoms(iloop)%z+DBterm(2)%z
          NewDBterm(NewLoop)%Atomic_Number=newatoms(iloop)%Atomic_Number
          NewDBterm(NewLoop)%element=newatoms(iloop)%element
          NewDBterm(NewLoop)%ctype=newatoms(iloop)%ctype 
      end do   
         
      if (allocated(newatoms)) deallocate(newatoms) ! if claUSE is necessary
      end SUBROUTINE getterm4
       
      SUBROUTINE getterm5 (DBterm,loop3,NewDBterm,NewLoop)
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:                                                             *
!*******************************************************************************
!
      implicit none
      
      type (FragAtomInfo), dimension(:), allocatable, intent(in) :: DBterm
      integer, intent(in) :: loop3  ! +1 for new atoms
      type (FragAtomInfo), dimension(:), allocatable, intent(out) :: NewDBterm
      integer, intent(out) :: NewLoop  ! +1 for new atoms
      type (FragAtomInfo), dimension(:), allocatable  :: newatoms
      
      double precision :: CH_bond, Anglea
      integer :: NNewatoms, iloop
      
      allocate (NewDBterm(Natoms))
      
      NewDBterm=DBterm
      NNewatoms=0
      
    !  select case(NewDBterm(2)%Atomic_Number) 
    !    case(1)  ! H atom
         !!!!!!!!!!!!!!!!!
    !    case(2)  ! He atom
         !!!!!!!!!!!!!!!!!
    !    case(3)
         !!!!!!!!!!!!!!!!!
    !    case(6)  ! C atom
         !!!!!!!!!!!!!!!!!              
    !    case(8)  ! O atom
         !!!!!!!!!!!!!!!!!
    !    CASE DEFAULT
    !      NewDBterm(2)%Atomic_Number=TerminalAtom
    !      NewDBterm(2)%element=element_symbols(TerminalAtom)  
    !  end select
      
      
      NewLoop=loop3
      do iloop=1, NNewatoms
          NewLoop=NewLoop+1    
          NewDBterm(NewLoop)%x=newatoms(iloop)%x+DBterm(2)%x
          NewDBterm(NewLoop)%y=newatoms(iloop)%y+DBterm(2)%y
          NewDBterm(NewLoop)%z=newatoms(iloop)%z+DBterm(2)%z
          NewDBterm(NewLoop)%Atomic_Number=newatoms(iloop)%Atomic_Number
          NewDBterm(NewLoop)%element=newatoms(iloop)%element
          NewDBterm(NewLoop)%ctype=newatoms(iloop)%ctype
      end do
      
      if (allocated(newatoms)) deallocate(newatoms) ! if claUSE is necessary
      end SUBROUTINE getterm5
      
      SUBROUTINE getterm6 (DBterm,loop3,NewDBterm,NewLoop)
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:                                                             *
!*******************************************************************************
! 
      implicit none
      
      type (FragAtomInfo), dimension(:), allocatable, intent(in) :: DBterm
      integer, intent(in) :: loop3  ! +1 for new atoms
      type (FragAtomInfo), dimension(:), allocatable, intent(out) :: NewDBterm
      integer, intent(out) :: NewLoop  ! +1 for new atoms
      type (FragAtomInfo), dimension(:), allocatable  :: newatoms
      
      double precision :: CH_bond, Anglea
      integer :: NNewatoms, iloop
      
      allocate (NewDBterm(Natoms))
      
      NewDBterm=DBterm
      NNewatoms=0
      
    !  select case(NewDBterm(2)%Atomic_Number) 
    !    case(1)  ! H atom
         !!!!!!!!!!!!!!!!!
    !    case(2)  ! He atom
         !!!!!!!!!!!!!!!!!
    !    case(3)
         !!!!!!!!!!!!!!!!!
    !    case(6)  ! C atom
         !!!!!!!!!!!!!!!!!              
    !    case(8)  ! O atom
         !!!!!!!!!!!!!!!!!
    !    CASE DEFAULT
    !      NewDBterm(2)%Atomic_Number=TerminalAtom
    !      NewDBterm(2)%element=element_symbols(TerminalAtom)  
    !  end select
        
      NewLoop=loop3
      do iloop=1, NNewatoms
          NewLoop=NewLoop+1    
          NewDBterm(NewLoop)%x=newatoms(iloop)%x+DBterm(2)%x
          NewDBterm(NewLoop)%y=newatoms(iloop)%y+DBterm(2)%y
          NewDBterm(NewLoop)%z=newatoms(iloop)%z+DBterm(2)%z
          NewDBterm(NewLoop)%Atomic_Number=newatoms(iloop)%Atomic_Number
          NewDBterm(NewLoop)%element=newatoms(iloop)%element
          NewDBterm(NewLoop)%ctype=newatoms(iloop)%ctype 
      end do
      
      if (allocated(newatoms)) deallocate(newatoms) ! if claUSE is necessary
      end SUBROUTINE getterm6
      end MODULE AddTerminal
