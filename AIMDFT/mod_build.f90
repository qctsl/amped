      MODULE mod_build
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:                                                             *
!*******************************************************************************
!     
      USE mod_rotation
      USE AIMDFT_Files
      USE program_constants     
 
      implicit none
      
CONTAINS !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>    
     
      SUBROUTINE UpdateFragTypes (FragAtoms,FragInfo)
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:  Update the types of fragments                              *
!*******************************************************************************
!
      USE GRAPH_objects

      implicit none
!
      type(FragAtomInfo),dimension(:,:), intent(inout), allocatable :: FragAtoms
      type(FragsInfo),dimension(:), intent(inout), allocatable :: FragInfo
      integer :: Ilist, Nlist, Iterm, MinZ, ZI
      integer :: Ivertex, Jvertex
      integer :: Itype, Jatom
      character(len=16) :: CAtype
      character(len=16) :: Ctype
!
      integer, dimension(:), allocatable :: Zlist    ! List of atomic numbers

      do Jatom=1, Natoms
          Nvertices=FragInfo(Jatom)%NAtoms
          allocate (Zlist(Nvertices))     
          ! Now define atom types for each vertex
          do Ivertex=1, Nvertices
              ZI=FragAtoms(Jatom,Ivertex)%Atomic_Number
              FragAtoms(Jatom,Ivertex)%type=0
              if(ZI.le.0)cycle ! dummy atom
              Zlist(1:Nvertices)=999
              Nlist=0
              do Jvertex=1,Nvertices ! Find all atoms bonded to Ivertex
                  if((FragInfo(Jatom)%Connect(Ivertex,Jvertex)).ne.0)then
                      Nlist=Nlist+1
                      Zlist(Nlist)=FragAtoms(Jatom,Jvertex)%Atomic_Number
                  end if
              end do ! Jvertex!
              Iterm=0
              Itype=0
!             Define type for Ivertex, sum in order of increasing Z
              do while (Iterm.lt.Nlist) !
              MinZ=MINval(Zlist)
              do Ilist=1,Nlist
                  ZI=Zlist(Ilist)
                  if(ZI.le.MinZ)then
                      Iterm=Iterm+1
                      Itype=Itype+(ZI-2)*Iterm
                      Zlist(Ilist)=999
                  end if
              end do ! Ilist
          end do ! while
          FragAtoms(Jatom,Ivertex)%type=Itype
      end do ! Ivertex
      deallocate (Zlist)
      end do ! Jatom
      return
      end SUBROUTINE UpdateFragTypes
      SUBROUTINE CalcFragConnect(FragAtoms,FragInfo)
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description: Calculate the connectivity matrix for the fragments         *
!*******************************************************************************
! 

      USE type_elements

      implicit none

      type(FragAtomInfo),dimension(:,:), intent(inout), allocatable :: FragAtoms
      type(FragsInfo),dimension(:), intent(inout), allocatable :: FragInfo 
      integer :: Jatom, IatomN
       
      ! In future you can USE differents type of radii
      call Frag_CONN (BS_RADII, NELEMENTS, BS_scalef, FragAtoms, FragInfo)
      return
      end SUBROUTINE CalcFragConnect
      SUBROUTINE Frag_CONN (RADII,   & ! Bragg-Slater atomic radii                          
                 ELMLEN,  & ! Length of data array RADII
                 FUDGE,   & ! Fudge factor for Connectivity determination                          
                 FragAtoms, &
                 FragInfo)  
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description: Calculate the connectivity matrix for the fragments         *
!                  using Bragg-Slater atomic radii                             *
!*******************************************************************************
!

      implicit none
      
      type (FragAtomInfo), dimension(:,:), intent(inout), allocatable :: FragAtoms
      type (FragsInfo), dimension(:), intent(inout), allocatable :: FragInfo

      integer, intent(in) :: ELMLEN
      double precision, intent(in) :: RADII(ELMLEN)
      double precision, intent(in) :: FUDGE

      integer :: Iatom, Jatom, Matom, IIAN, JIAN, IatomN      
      double precision :: DisSep, FUDGE_save

      do Jatom=1, Natoms
         if (CARTESIAN(Jatom)%Atomic_Number.le.0) cycle
         IatomN=FragInfo(Jatom)%NAtoms
         if(IatomN.lt.2) cycle          
         FragInfo(Jatom)%Connect=0
         do Iatom=1,IatomN
            IIAN=FragAtoms(Jatom,Iatom)%Atomic_Number
            if(IIAN.le.0)cycle ! Don't check if not 'real'.
            do Matom=Iatom+1,IatomN
               JIAN=FragAtoms(Jatom,Matom)%Atomic_Number
               if (JIAN.le.0) cycle ! Don't check if not 'real'.
               FUDGE_save=FUDGE
               ! Special case for HF molecule
               if((IIAN.eq.1.and.JIAN.eq.9).or.(IIAN.eq.9.and.JIAN.eq.1))then 
                  FUDGE_save=1.273
               end if
               DisSep=dsqrt((FragAtoms(Jatom,Iatom)%x-&
                             FragAtoms(Jatom,Matom)%x)**2 + &
                          & (FragAtoms(Jatom,Iatom)%y-&
                             FragAtoms(Jatom,Matom)%y)**2 + &
                          & (FragAtoms(Jatom,Iatom)%z-&
                             FragAtoms(Jatom,Matom)%z)**2)
               if(bohr_to_angstrom*DisSep-FUDGE_save*&
                  (RADII(IIAN)+RADII(JIAN)).le.ZERO)then
                  FragInfo(Jatom)%Connect(Iatom,Matom)=1
               end if !
               FragInfo(Jatom)%Connect(Matom,Iatom)=&
               FragInfo(Jatom)%Connect(Iatom,Matom) ! Connectivity is symmetric.
            end do ! ! Matom
         end do ! ! Iatom
      end do ! ! Jatom
      return
      end SUBROUTINE Frag_CONN
      end MODULE mod_build
