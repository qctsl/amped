      MODULE mod_fragment
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:                                                             *
!*******************************************************************************
!
      USE program_files
      USE rotation_Cart
      USE type_elements !needed for element_symbols()
      USE GRAPH_objects ! needed for Connect()
      USE QM_objects ! needed for bond_order()
      USE mod_build
      USE GetDataBaseInfo
      USE GetMolecularProperties ! to use GetLevelConnect function
      
      implicit none
      
CONTAINS !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 

      SUBROUTINE GenerateFragments(level,FragAtoms,FragInfo)  
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description: Partitioning the molecule to its fragments                  * 
!*******************************************************************************
!
      implicit none

      integer, intent(in) :: level
      type (FragAtomInfo), dimension(:,:), intent(out), allocatable :: FragAtoms
      type (FragsInfo), dimension(:), intent(out), allocatable :: FragInfo    

      integer :: Iatom
      integer :: Jatom
      integer :: ifound
      integer :: ilevel
      type (TwoDiArrInt), dimension(:), allocatable :: LevelConnect !   
      logical, dimension(:), allocatable :: found
      
      allocate (FragAtoms(Natoms,Natoms))
      allocate (FragInfo(Natoms))
      allocate (LevelConnect(level))  
      allocate (found(Natoms))  

      CALL GetLevelConnect(level,LevelConnect)
      
      do Jatom=1, NAtoms
          if (Cartesian(JAtom)%Atomic_Number.eq.0) cycle
          FragInfo(Jatom)%NAtoms=1
          FragInfo(Jatom)%NumNeighbourAtoms=0
      
          ! Defined the main atom (Jatom) element_symbols
          FragAtoms(Jatom,1)%level=0
          FragAtoms(Jatom,1)%MUNIdx=Jatom  ! the index of atom in MUNgauss
          FragAtoms(Jatom,1)%Atomic_Number=Cartesian(Jatom)%Atomic_Number
          FragAtoms(Jatom,1)%x=Cartesian(Jatom)%x
          FragAtoms(Jatom,1)%y=Cartesian(Jatom)%y
          FragAtoms(Jatom,1)%z=Cartesian(Jatom)%z
          FragAtoms(Jatom,1)%element=&
              element_symbols(Cartesian(Jatom)%Atomic_Number)
          FragAtoms(Jatom,1)%ctype=""
          FragAtoms(Jatom,1)%sorted= .true.
        
          ifound=1  ! index for each sub-atom found for Jatom

          do Iatom=1, NAtoms
              if (Cartesian(IAtom)%Atomic_Number.eq.0) cycle
              found=.false.
              do ilevel=1, level
                  if ((LevelConnect(ilevel)%ijarray(Iatom,Jatom).ne.1).or.&
                  (found(Iatom)).or.Iatom.eq.Jatom) cycle
                  ifound=ifound+1
                  found(Iatom)=.true.
                  FragInfo(Jatom)%NAtoms=FragInfo(Jatom)%NAtoms+1
                  if (ilevel.eq.1) FragInfo(Jatom)%NumNeighbourAtoms=&
                  FragInfo(Jatom)%NumNeighbourAtoms+1
                  FragAtoms(Jatom,ifound)%level=ilevel
                  FragAtoms(Jatom,ifound)%MUNIdx=Iatom !  MUNgauss index
                  FragAtoms(Jatom,ifound)%Atomic_Number=&
                  Cartesian(IAtom)%Atomic_Number
                  FragAtoms(Jatom,ifound)%x=Cartesian(IAtom)%x
                  FragAtoms(Jatom,ifound)%y=Cartesian(IAtom)%y
                  FragAtoms(Jatom,ifound)%z=Cartesian(IAtom)%z
                  FragAtoms(Jatom,ifound)%element=&
                  element_symbols(Cartesian(Iatom)%Atomic_Number)
                  FragAtoms(Jatom,ifound)%ctype=""
                  FragAtoms(Jatom,ifound)%sorted= .false.     
              end do ! ilevel
          end do  ! Iatom     
      end do ! Jatom
      
      CALL CalcFragConnect(FragAtoms,FragInfo)
      CALL UpdateFragTypes(FragAtoms,FragInfo)
      
      deallocate(LevelConnect, found)
      return
      end SUBROUTINE GenerateFragments
  
      SUBROUTINE PRNTCART(FragAtoms,FragInfo)
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:  Print the cartesian coordinate for the molecule fragments  *
!*******************************************************************************
!
      implicit none

      type (FragAtomInfo), dimension(:,:), intent(in), allocatable  :: FragAtoms
      type (FragsInfo), dimension(:), intent(in), allocatable :: FragInfo 
          
      integer :: ifound, JAtom
      
      do JAtom=1, NAtoms
         if (Cartesian(JAtom)%Atomic_Number.eq.0) cycle
         write(UNIout,'(/A9,I3,A8,I3)') "Fragment#", JAtom, &
         ", Atoms#", FragInfo(Jatom)%NAtoms
         write(UNIout,'(A5,3A13,1X,4A9)') "!Atom ","X","Y","Z",&
         "Ctype","Ftype","Level","Sorted" 
         do ifound=1, FragInfo(Jatom)%NAtoms 
            write(UNIout,'(A5,3X,3F13.8,3X,A7,i5,i8,l8)') &
                      & FragAtoms(Jatom,ifound)%element,               &
                      & FragAtoms(Jatom,ifound)%x*CartPrnFactor,       &
                      & FragAtoms(Jatom,ifound)%y*CartPrnFactor,       &
                      & FragAtoms(Jatom,ifound)%z*CartPrnFactor,       &
                      & FragAtoms(Jatom,ifound)%Ctype,                 &
                      & FragAtoms(Jatom,ifound)%type,                  &
                    ! & FragAtoms(Jatom,ifound)%factor,                &
                      & FragAtoms(Jatom,ifound)%level,                 &
                      & FragAtoms(Jatom,ifound)%sorted
          end do
      end do
      end SUBROUTINE PRNTCART
      end MODULE mod_fragment
