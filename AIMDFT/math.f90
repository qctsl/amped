      MODULE mod_math
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:                                                             *
!*******************************************************************************
!     
      implicit none

CONTAINS !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      
SUBROUTINE LOGO()
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:  The AIMDFT logo                                            *
!*******************************************************************************
!
      implicit none

      write(*,*) "     ___  _____ ___  _________ ______  _____   " 
      write(*,*) "    / _ \|_   _||  \/  ||  _  \|  ___||_   _|  " 
      write(*,*) "   / /_\ \ | |  | .  . || | | || |_     | |    " 
      write(*,*) "   |  _  | | |  | |\/| || | | ||  _|    | |    " 
      write(*,*) "   | | | |_| |_ | |  | || |/ / | |      | |    " 
      write(*,*) "   \_| |_/\___/ \_|  |_/|___/  \_|      \_/    " 
      write(*,*) "                                               " 

      end SUBROUTINE LOGO
      SUBROUTINE  SwapI(a, b)
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:  Swap two integer values                                    *
!*******************************************************************************
!
      implicit none
      
      integer, intent(inout) :: a, b
      integer                :: Temp

      Temp=a ; a=b ; b=Temp
      end SUBROUTINE  SwapI      
      SUBROUTINE  SwapR(a, b)
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:  Swap two real values                                       *
!*******************************************************************************
!
      implicit none
      
      double precision, intent(inout) :: a, b
      double precision                :: Temp

      Temp=a ; a=b ; b=Temp
      end SUBROUTINE  SwapR
      double precision FUNCTION DET33(A)
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description: Calculate the determinant of a matrix (3x3), real values.   *
!*******************************************************************************
!
      implicit none

      double precision, dimension(3,3), intent(in)  :: A

      DET33 = A(1,1)*A(2,2)*A(3,3)  &
            - A(1,1)*A(2,3)*A(3,2)  &
            - A(1,2)*A(2,1)*A(3,3)  &
            + A(1,2)*A(2,3)*A(3,1)  &
            + A(1,3)*A(2,1)*A(3,2)  &
            - A(1,3)*A(2,2)*A(3,1)
      RETURN
      end FUNCTION DET33     
      double precision FUNCTION VectorsDistance(dim_num,v1,v2)
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description: Calculate the distance between two vectors                  *
!*******************************************************************************
!
      implicit none

      integer, intent(in) :: dim_num
      double precision, dimension(dim_num), intent(in) :: v1, v2
  
      VectorsDistance=dsqrt(sum((v1(1:dim_num)-v2(1:dim_num))**2))

      RETURN
      end FUNCTION VectorsDistance      
      SUBROUTINE INV33(A,T)
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description: Calculate the inverse of a matrix (3x3), real values.       *
!*******************************************************************************
!
      implicit none

      double precision, dimension (3,3), intent(in) :: A
      double precision, dimension (3,3), intent(out) :: T

      T(1,1) =  (A(2,2)*A(3,3)-A(2,3)*A(3,2))/DET33(A)
      T(1,2) = -(A(1,2)*A(3,3)-A(1,3)*A(3,2))/DET33(A)
      T(1,3) =  (A(1,2)*A(2,3)-A(1,3)*A(2,2))/DET33(A)
      T(2,1) = -(A(2,1)*A(3,3)-A(2,3)*A(3,1))/DET33(A)
      T(2,2) =  (A(1,1)*A(3,3)-A(1,3)*A(3,1))/DET33(A)
      T(2,3) = -(A(1,1)*A(2,3)-A(1,3)*A(2,1))/DET33(A)
      T(3,1) =  (A(2,1)*A(3,2)-A(2,2)*A(3,1))/DET33(A)
      T(3,2) = -(A(1,1)*A(3,2)-A(1,2)*A(3,1))/DET33(A)
      T(3,3) =  (A(1,1)*A(2,2)-A(1,2)*A(2,1))/DET33(A)
      
      end SUBROUTINE INV33 
      SUBROUTINE RANDSEED(R)
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description: Return randum number depend on the seed time.               *
!*******************************************************************************
!
      implicit none

      integer, INTENT(OUT) :: R
      integer :: randnumI,seedsize,values(1:8)
      character(len=6) :: randnumS
      integer, allocatable :: seed(:) 
      double precision :: randnum

      call date_and_time(values=values)
      call random_seed(size=seedsize)
      allocate(seed(seedsize))
      seed(:) = values(8)
      call random_seed(put=seed)
      call random_number(randnum)
      randnumI = FLOOR(100000*randnum)
!     write(randnumS,'(I6.6)') randnumI
      R = randnumI

      end SUBROUTINE RANDSEED 
      end MODULE mod_math
