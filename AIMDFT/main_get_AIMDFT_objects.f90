      SUBROUTINE GET_AIMDFT_object (class, Objname, Modality)
!*******************************************************************************
!     Date last modified: July 22, 2015                                        *
!     Author: Ibrahim Awad I. Awad                                             *
!     Description: Objects belonging to the class AIMDFT                       *
!*******************************************************************************
! MODULEs:
      USE program_files
      USE program_objects

      implicit none
!
! Input scalar:
      character*(*) :: class
      character*(*) :: Objname
      character*(*) :: Modality
!
! Local scalars:
      integer :: Object_number
!
! Begin:
      call PRG_manager ('enter', 'GET_AIMDFT_object', 'UTILITY')

      call GET_object_number (OBJ_AIMDFT,     &
                              NAIMDFTobjects, &
                              FirstAIMDFT,    &
                              Objname,        &
                              Modality,       &
                              Object_number)

      if(OBJ_AIMDFT(Object_number)%current)then
        call PRG_manager ('exit', 'GET_AIMDFT_object', 'UTILITY')
        return
      end if

      OBJ_AIMDFT(Object_number)%exist=.true.
      OBJ_AIMDFT(Object_number)%Current=.true.
      Object(ObjNum)%exist=.true.
      Object(ObjNum)%Current=.true.

      select_Object: select case (Objname)
      case ('FRAGCART') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
        case('DBASE')    
         call Add_To_DB
        case('GRIDS')
         call Store_result_direct 
        case('PM')
         call PRINTPM0
        case default
           write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                      '" for Modality "',Modality(1:len_trim(Modality)),'"'
       stop 'No such object'
      end select

      case ('MOLECULE') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
        case('BUILD')
         call CalcMoleProperties
        case('ROTATE')
         call get_databaseform
        case default
           write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                      '" for Modality "',Modality(1:len_trim(Modality)),'"'
       stop 'No such object'
      end select

      case ('FRAGMENT') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
        case('DISPLAY')
         call FragmentsDisplay
        case('BUILD')
         call Build_FragFiles
        case default
           write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                      '" for Modality "',Modality(1:len_trim(Modality)),'"'
       stop 'No such object'
      end select

      case default
        write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                       '" for Modality "',Modality(1:len_trim(Modality)),'"'
        stop 'No such object'

      end select select_Object

      call PRG_manager ('exit', 'GET_AIMDFT_object', 'UTILITY')
      return
      end SUBROUTINE GET_AIMDFT_object
