      SUBROUTINE FragmentsDisplay()
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description: Display the current and database cartesian coordinates      *
!                  for the molecule fragments                                  *
!*******************************************************************************
!
      USE Sorted_AIMDFT
      USE mod_math
      USE symbol_AIMDFT

      implicit none
      
      character (SymMax), dimension (:), allocatable :: symbols
      type (FragsInfo), dimension(:), allocatable :: FragInfo    
      type (FragAtomInfo), dimension(:,:), allocatable :: FragAtoms, DBFragAtoms
       
      double precision, dimension (:,:), allocatable :: RR
      double precision, dimension (:), allocatable :: TT  
      integer :: Iatom,JAtom,ifound
      
      !type (TwoDiArrInt), dimension(:), allocatable :: LevelConnect z
      
      allocate (FragInfo(Natoms))  
      allocate (symbols(Natoms))    
      allocate (FragAtoms(Natoms,Natoms))  
      allocate (DBFragAtoms(Natoms,Natoms))  
      allocate (RR(1:3,1:3))  
      allocate (TT(1:3))  
      !allocate (LevelConnect(Level_Number))  

      CALL LOGO() 
    
 !     CALL GetLevelConnect(Level_Number,LevelConnect)     
 !     write(*,*) "**********************************"
 !     write(*,*) "          Adjacency matrix        "
 !     write(*,*) "**********************************"
 !     do i=1, Level_Number
 !         write(*,*) "for level # ", i
 !         write(*,*) "____________________"
 !         call PRT_matrix(LevelConnect(i)%ijarray,NAtoms,NAtoms)
 !     end do
      
      CALL GetSortCartTerm (Level_Number,FragAtoms,FragInfo)
     
      write(*,'(/A)') "*************************************"
      write(*,'(A)')  "    Current cartesian coordinates    "
      write(*,'(A)')  "*************************************"  
      CALL PRNTCART(FragAtoms,FragInfo)
     
      CALL GetDBFormFragments(FragAtoms,DBFragAtoms,FragInfo)     
      write(*,'(/A)') "*************************************"
      write(*,'(A)')  "    Database cartesian coordinates   "
      write(*,'(A)')  "*************************************"   
      CALL PRNTCART(DBFragAtoms,FragInfo)

      CALL GetFragSymbols(Level_Number,symbols)
      write(*,'(/A)') "*************************************"
      write(*,'(A)')  "     Molecule atoms symbols          "
      write(*,'(A)')  "*************************************"   
      do Iatom=1, Natoms
          if (Cartesian(IAtom)%Atomic_Number.eq.0) cycle
          write(*,'(I5,3X,A)') Iatom,trim(symbols(Iatom))
      end do

      deallocate (FragInfo)  
      deallocate (symbols)    
      deallocate (FragAtoms)  
      deallocate (DBFragAtoms)  
      deallocate (RR)  
      deallocate (TT)       
      return
      end SUBROUTINE FragmentsDisplay
