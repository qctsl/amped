      SUBROUTINE AIMDFT_PRINT_manager (ObjName, Modality)
!*******************************************************************************
!     Date last modified: April 24, 2008                           Version 2.0 *
!     Author: Ibrahim Awad R.A. Poirier                                        *
!     Description: Calls the appropriate routine to "Command" the object       *
!     nameIN Command can be "PRINT" or "KILL"                                  *
!     Pwhat can be "OBJECT", "DOCUMENTATION" or "TABLE"                        *
!*******************************************************************************
! MODULEs:
      USE program_files
      USE program_objects

      implicit none
!
! Input scalars:
      character*(*), intent(in) :: ObjName,Modality
!
! Local scalars:
!
! Begin:
      call PRG_manager ('enter', 'AIMDFT_PRINT_manager', 'UTILITY')

      SelectObject:   select case (ObjName)
      case ('FRAGCART')
      case ('MOLECULE')
      case ('FRAGMENT')
      case default
        write(UNIout,*)'AIMDFT_PRINT_manager>'
        write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                       '" for Modality "',Modality(1:len_trim(Modality)),'"'
      end select SelectObject
!
! end of function AIMDFT_PRINT_manager
      call PRG_manager ('exit', 'AIMDFT_PRINT_manager', 'UTILITY')
      return
      end SUBROUTINE AIMDFT_PRINT_manager
