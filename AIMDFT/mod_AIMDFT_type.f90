      MODULE AIMDFT_type
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:                                                             *
!*******************************************************************************
!     

      USE program_constants

      implicit none

!     Parameters for fragmant atom
      integer               :: Level_Number
      integer, parameter    :: SymMax = 64
      integer               :: TerminalAtom              ! Add H as termenal 
      logical               :: ChangeTerminalAtoms
      logical               :: DirectMethod
      logical               :: DatabaseBWeight
      integer               :: IndexNumber
      integer               :: MaxGridPnt
      logical               :: RotUpdate

      integer, parameter :: MAX_ROTlist=4 ! Maximum number of ROT in ROTlist
      integer :: NROTlist
      integer, dimension(:) :: ROTlist(Max_ROTlist)

      ! the output unit (Angstrom)
      double precision, parameter :: CartPrnFactor = Bohr_to_Angstrom 
      
      ! Database types
      character (128)  :: DirLoc             ! Database Location 
      character (128)  :: DBFileExt          ! Database file type (Default txt)
      character (128)  :: CsvSep             ! 
      character (128)  :: IndexFileName      ! 
      character (128)  :: CartFileName
      character (128)  :: PropFileName
 
      character (128)  :: homedir
      character (len=:), allocatable :: IndexFilePath
      character (len=:), allocatable :: CartFilePath
      character (len=:), allocatable :: PropFilePath
      character (len=:), allocatable :: GridPath 

      character (128)  :: fmt_index
      character (128)  :: fmt_cart
      character (128)  :: fmt_prop
      character (128)  :: fmt_index_title
      character (128)  :: fmt_cart_title
      character (128)  :: fmt_prop_title

      character(len=8)   :: OPT_status

      character(len=8)   :: fmt_xyz
      character(len=8)   :: fmt_w
      character(len=8)   :: fmt_GridPts
      character(len=8)   :: fmt_rho      
      character(len=32)  :: Pfmt_rho
      character(len=32)  :: Pfmt_rho_w
      character(len=256) :: Pfmt_rho_w_v
      character(len=64)  :: Pfmt_rho_dd

      type :: CartesianCoordinate
          double precision  :: X
          double precision  :: Y
          double precision  :: Z
      end type CartesianCoordinate

      type :: GridsDataBase
          double precision  :: X
          double precision  :: Y
          double precision  :: Z
          double precision  :: W        ! Total Angular Weight
          double precision  :: BW       ! Becke Weight
          double precision  :: D        ! Electron Density
          double precision  :: Q        ! W*BW*D
          double precision  :: VpotA    ! I1E_V12dr2 (analytical term)
          double precision  :: VpotN    ! I1E_V12dr2 (numerical term)
      end type GridsDataBase

      type  ::  FragAtomInfo
          integer             :: Atomic_Number
          integer             :: MUNIdx  ! the index of atom in MUNgauss
          double precision    :: X
          double precision    :: Y
          double precision    :: Z
          character(len=8)    :: ELEMENT   ! Atom Symbol (e.g H for Hydrogen)
          integer             :: type      ! Atom type
          integer             :: number    ! Total number of a given atom type
          integer             :: factor    ! Valence factor
          character(len=16)   :: Ctype     ! The atom type as a character string
          integer             :: level = 0
          double precision    :: bond_order
          logical             :: sorted = .false. ! Have UNiqE Type
      end type FragAtomInfo
      
      type :: FragsInfo
          ! Number of fragment atoms
          integer                                     :: NAtoms     
          ! Number of neighbour atoms       
          integer                                     :: NumNeighbourAtoms 
          ! (atoms number : level)
          character (SymMax) , dimension(10000,10) :: symbol  
          ! Rotation matrix          
          double precision, dimension (3,3)           :: RMat   
          ! Translation matrix           
          double precision, dimension (3)             :: TMat     
          ! connectivity matrix         
          integer, dimension(100,100)                 :: Connect           
      end type FragsInfo

      ! Two dimension array (integer Values)
      type :: TwoDiArrInt
          integer, dimension(:,:), allocatable    :: ijarray  
      end type TwoDiArrInt

      type :: TypeFragMat
          double precision, dimension (3,3)    :: RMat
          double precision, dimension (3)      :: TMat
          double precision, dimension (3)      :: TMatCorr
      end type TypeFragMat
      
      type :: IndexFile
          integer                  :: IndexNum         ! column # 1
          character(len=SymMax) :: Symbol           ! column # 2
          integer                  :: NAtoms           ! column # 3
          integer                  :: GridsNum         ! column # 4 Grids number
          character (len=32)       :: GridType         ! column # 5 Grid type
          character (len=32)       :: GridTypeExtra    ! column # 6  
          character (len=32)       :: Method           ! column # 7 RHF, ....
          character (len=32)       :: BasisSet         ! column # 8 Basis set
          character (len=16)       :: DEN_Partitioning ! column # 9 Becke, ...
          character (len=16)       :: CoreSize         ! column # 10 Average,...
          character (len=8)        :: OPT              ! column # 11 NO_OPT, OPT
          logical                  :: Availability     ! Availability
      end type IndexFile

      type :: PropFile
          integer                  :: IndexNum         ! column # 1
          double precision         :: ElectonNum       ! column # 2
          double precision         :: K                ! column # 3
          double precision         :: KHF              ! column # 4
          double precision         :: T                ! column # 5
          double precision         :: Vne_A            ! column # 6
          double precision         :: Vne_N            ! column # 7
          double precision         :: Vee_AN           ! column # 8
          double precision         :: J                ! column # 9
          double precision         :: JHF              ! column # 10
          double precision         :: Jaa              ! column # 11
          double precision         :: VeeA             ! column # 12
          logical                  :: Availability     ! Availability
      end type PropFile
      
      end MODULE AIMDFT_type
