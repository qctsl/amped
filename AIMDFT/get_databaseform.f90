      SUBROUTINE get_databaseform()
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description: Display the current and database cartesian coordinates      *
!                  for the molecule fragments                                  *
!*******************************************************************************
!
      USE AIMDFT_type
      USE type_molecule
      USE mod_rotation

      implicit none

      double precision, dimension (:,:), allocatable :: OldCart
      double precision, dimension (:,:), allocatable :: ROTCart
      double precision, dimension (:,:), allocatable :: NewCart           
      double precision, dimension (:,:), allocatable :: RR
      double precision, dimension (:), allocatable :: TT  
      integer :: Iatom, JAtom, IROT
       
      allocate (RR(1:3,1:3))  
      allocate (TT(1:3))
      allocate (ROTCart(NAtoms,3))   
      allocate (OldCart(NAtoms,3))
      allocate (NewCart(NAtoms,3)) 

      if(NROTlist.eq.0) then  ! If no atoms specified, stop
      write(*,*) "Error: No NROTlist"
      call exit()
      end if
      
      OldCart(1:Natoms,1)=Cartesian(1:Natoms)%x
      OldCart(1:Natoms,2)=Cartesian(1:Natoms)%y
      OldCart(1:Natoms,3)=Cartesian(1:Natoms)%z

      do IROT=1,NROTlist
         Iatom=ROTlist(IROT)
         ROTCart(IROT,1:3) = OldCart(Iatom,1:3)
      end do

      call GetStoredRotMatrix (ROTCart,NROTlist,RR,TT)
      call DoRotation (OldCart,NewCart,Natoms,RR,TT)

      if (RotUpdate) then
          Cartesian(1:Natoms)%x=NewCart(1:Natoms,1)
          Cartesian(1:Natoms)%y=NewCart(1:Natoms,2)
          Cartesian(1:Natoms)%z=NewCart(1:Natoms,3)
          write(*,'(/A,/A)') &
          "RotUpdate is TRUE","The Cartesian coordinates were updated"      
          write(*,'(A)')     "The new coordinates are: (Angstrom unit)"
          write(*,'(A4,3A14)') "Atom","X","Y","Z"
          do Iatom=1, Natoms
              write(*,'(A4,3F14.8)') Cartesian(Iatom)%element, &
                              & NewCart(Iatom,1)*CartPrnFactor, &
                              & NewCart(Iatom,2)*CartPrnFactor, &
                              & NewCart(Iatom,3)*CartPrnFactor
          end do
          write(*,*) "____________________________________________"
      end if

      deallocate (ROTCart,OldCart,NewCart)
      deallocate (RR)  
      deallocate (TT)       
      return
      end SUBROUTINE get_databaseform
