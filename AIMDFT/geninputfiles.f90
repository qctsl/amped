      SUBROUTINE Build_FragFiles()
!*****e*************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:                                                             *
!*******************************************************************************
! MODULEs:

      USE module_grid_points
      USE NI_defaults
      USE type_Weights
      USE QM_defaults
      USE Sorted_AIMDFT
      USE symbol_AIMDFT
 
      implicit none
!
! Local scalars:

      type (IndexFile), dimension(:), allocatable :: MolIdxInfo
      type (FragsInfo), dimension(:), allocatable :: FragInfo
      type (FragAtomInfo), dimension(:,:), allocatable :: SortedFragmentAtom
      type (FragAtomInfo), dimension(:,:), allocatable :: DBFragmentAtom
      
      character (SymMax), dimension (:), allocatable ::  symbols
      character (SymMax), dimension (:), allocatable ::  UniSym
      integer, dimension (:), allocatable :: MUNatom
      
      integer :: katom
      integer :: UniSymMum
      integer :: IndexFoundNum
      integer :: fileunit
      integer :: ifound
      integer :: newindex
      integer :: maxindex
      integer :: iSym

      character (4)  :: OPT_YN
      character (60) :: cmdd
      character (60) :: radline
      character (60) :: outfile
      character (60) :: inputfile
      logical :: Lerror

      allocate (MolIdxInfo(Natoms))
      allocate (FragInfo(Natoms))
      allocate (SortedFragmentAtom(Natoms,Natoms))
      allocate (DBFragmentAtom(Natoms,Natoms))
      allocate (symbols(Natoms))
      allocate (UniSym(Natoms))
      allocate (MUNatom(Natoms))

      CALL Check_DB_files()                         
      CALL GetSortCartTerm(Level_Number,SortedFragmentAtom,FragInfo)
      CALL GetDBFormFragments(SortedFragmentAtom,DBFragmentAtom,FragInfo)
      CALL GetFragSymbols(Level_Number,symbols)
      CALL GetUniSym (Level_Number,UniSym,MUNatom,UniSymMum)
      CALL GetMoleculeAtomsIndex (MolIdxInfo,UniSym,UniSymMum,IndexFoundNum)     
      CALL GetMaxIndex (maxindex) ! Find Maxindex 
           
      write(*,'(A)') "************************************"
      write(*,'(A)') " The unique Symbols in the molecule "
      write(*,'(A)') "************************************"      
      do iSym=1, UniSymMum
        write (*,'(A,I4,A,A)') "SYM# ", iSym, " is ", trim(UniSym(iSym))
      end do
        write(*,'(A)') "AIMDFT-AIMDFT-AIMDFT-AIMDFT-AIMDFT-AIMDFT-AIMDFT-AIMDFT"                   
      !! WORKING WITH the nonavaliable fragments within database 
      ! add the new symbols
      newindex=maxindex
      do ifound=1, UniSymMum    
         if (MolIdxInfo(ifound)%Availability) cycle
         newindex=newindex+1  
         ! generate input files
         CALL INPUT_File_FRAG ('INPUT_','.dat',newindex,inputfile) 
         CALL INPUT_File_FRAG ('INPUT_','.out',newindex,outfile) 
         CALL GET_unit (inputfile, fileunit, Lerror)
         open(UNIT=fileunit, file=inputfile, status='REPLACE')       
         write(fileunit,'(A)') &
         "MOLECULE MUltiplicity =  1 CHarge =  0  UNit = Angstrom"
         write(fileunit,'(A)')'TITle = "'//UniSym(ifound)//'"'
         write(fileunit,'(A)')'CArtesian'       
         do katom=1,FragInfo(MUNatom(ifound))%NAtoms
             write(fileunit,'(A5,3f26.20)') &  
             &  DBFragmentAtom(MUNatom(ifound), &
             &  katom)%element, &
             &  DBFragmentAtom(MUNatom(ifound),katom)%x*Bohr_to_Angstrom, &
             &  DBFragmentAtom(MUNatom(ifound),katom)%y*Bohr_to_Angstrom, &
             &  DBFragmentAtom(MUNatom(ifound),katom)%z*Bohr_to_Angstrom
         end do
         write(fileunit,'(A)')'end ! CArtesian'
         write(fileunit,'(A/)')'end ! MOLECULE'
         write(fileunit,'(A12,A14,A4/)')'basis name= ',&
                                        trim(Basis_set_name),' end'
      
         write(fileunit,'(A25,I4.4,A9/)')'SET RUN NAME = "RUN_FRAG_',&
                                         newindex,'" end end' 
      !   write(fileunit,'(A/)') &
      !   'GRID MESH ORigin= ( 0.0 -5.0 -2.0 ) &
      !    MESH = ( 0.02 0.02 0.02 ) NX=0 NY=500 NZ=350 end end'  
         write(fileunit,'(A/)')'PLOT ATOM=false Molecule=false end'  
         OPT_YN=""
         if (trim(OPT_status).eq.'OPT') then
            OPT_YN="OPT"
         end if
         write(fileunit,'(A20,I4,A8,I3,A2,A4,A4/)')'AIMDFT IndexNumber=',&
               & newindex,' Level=',Level_Number,' ',OPT_YN,'end'
         write(fileunit,'(A20,A10,A4/)') 'PARtitioning Scheme=',&
               & trim(DEN_Partitioning),' end'
  
         if (trim(RADIAL_grid).eq.'GILL') then
            write(radline,'(A20,I3,A10,I2,I3,I4,A10)') &
           &'RADial Gill RPoints=',NRPoints_Gill ,&
           &' APoints=(', NApoints_Gill(1),NApoints_Gill(2),NApoints_Gill(3),&
           &' ) end end'
         else
            write(radline,'(A22,A7,A8)') 'NUmercial RADial GRID=',&
                                          trim(RADIAL_grid),' end end'
         endif        
          
         write(fileunit,'(A/)') radline 

         write(fileunit,'(A/)')'output object=MOL:INTERNAL%ZMATRIX end'
         if (trim(OPT_status).eq.'OPT') then
            write(fileunit,'(A/)')'OPT ITER=100 RUN end'
         end if
        ! write(fileunit,'(A/)')'output object=QM:RADIAL_DENSITY%MESH end'
         write(fileunit,'(A/)')'output object=AIMDFT:FRAGCART%DBASE end'
         write(fileunit,'(A)') 'stop'
         close(UNIT=fileunit)
         !write(cmdd,'(a7,a15,a1,a15)')"mgauss<",inputfile,">" ,outfile
         ! Running the input file
         write(*,'(///A,A/,A/)') "*** RUNing the inputfile :: ", inputfile
         write(*,'(/A)') "*****************************************"
         write(*,*) "Working on symbol ",UniSym(ifound)
         write(*,*) "IndexNUM ", newindex 
         write(*,'(A/)') "*****************************************"       
         write(cmdd,'(a7,a15)')"mgauss<",inputfile
         CALL SYSTEM(cmdd)            
         write(cmdd,'(a3,a15)')"rm ",inputfile
         CALL SYSTEM(cmdd)            
      end do   
      write(*,'(A///)')
      deallocate (FragInfo)  
      deallocate (SortedFragmentAtom)  
      deallocate (symbols)  
      deallocate (UniSym)  
      deallocate (MUNatom) 
      deallocate (MolIdxInfo)
      deallocate (DBFragmentAtom) 
! end of routine DENSITY_MESH
!      call PRG_manager ('exit', 'DENSITY_MESH', 'UTILITY')
CONTAINS !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      SUBROUTINE Check_DB_files()  ! needed to add titles for database files 
!*****e*************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:                                                             *
!*******************************************************************************
! MODULEs

      implicit none

      logical :: index_exists,cart_exists,prop_exists
      integer :: File_unit
      logical :: Lerror

      ! file_exists will be TRUE
      INQUIRE(FILE=trim(IndexFilePath), EXIST=index_exists)       
      INQUIRE(FILE=trim(CartFilePath), EXIST=cart_exists)         
      INQUIRE(FILE=trim(PropFilePath), EXIST=prop_exists)   
      if (.not.index_exists) then
      CALL GET_unit (trim(IndexFilePath), File_unit, Lerror)
      open(UNIT=File_unit,file=trim(IndexFilePath), &
           status='new',form='formatted')
      write(File_unit,fmt_index_title) "Index#",          &
                                           & "Symbol",    &
                                           & "Atom#",     &
                                           & "Grid#",     &
                                           & "Grid",      &
                                           & "Method",    &
                                           & "Basisset",  &
                                           & "Weight",    &
                                           & "CoreSize",  &
                                           & "Opt/NO-opt"
      close(unit=File_unit)
      end if

      if (.not.cart_exists) then
      CALL GET_unit (trim(CartFilePath), File_unit, Lerror)
      open(UNIT=File_unit,file=trim(CartFilePath), &
           status='new',form='formatted')
      write(File_unit,fmt_cart_title) "#","#","Sym","X","Y","Z" 
      close(unit=File_unit)
      end if

      if (.not.prop_exists) then
      CALL GET_unit (trim(PropFilePath), File_unit, Lerror)
      open(UNIT=File_unit,file=trim(PropFilePath), &
           status='new',form='formatted')
      write(File_unit,fmt_prop_title) &
                                      & "#",            &
                                      & "Electrons",    &
                                      & "K",            &
                                      & "K_HF",         &
                                      & "T",            &
                                      & "Vne_Ana",      &
                                      & "Vne_Num",      &
                                      & "Vee(Ana/Num)", &
                                      & "J",            &
                                      & "J_HF",         &
                                      & "Jaa=Kaa",      &
                                      & "Vee_DFT_AtomA"


      close(unit=File_unit)
      end if

      end SUBROUTINE Check_DB_files
      end SUBROUTINE Build_FragFiles
