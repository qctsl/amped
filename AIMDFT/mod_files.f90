      MODULE AIMDFT_Files
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:                                                             *
!*******************************************************************************

      USE AIMDFT_type

      implicit none
      
CONTAINS !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      SUBROUTINE INPUT_File_GRID (IndexNum, GridFilename)
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description: Create a filename for Ploting data and get a unit.          *
!*******************************************************************************
! MODULEs:

      implicit none
!
      integer, intent(in) :: IndexNum
      character(len=132), allocatable, intent(out) :: GridFilename
      character(64) :: FileID, FilePOST
      
      character(15) :: Plot, CPlot
      integer :: lenstr
!
! Begin:
      FileID="GRID_"
      FilePOST=".dat"
      write(Plot,'(i4.4)') IndexNum
      call RemoveBlanks (Plot, CPlot, lenstr)  
      GridFilename=trim(GridPath)//trim(FileID)//CPlot(1:lenstr)//trim(FilePOST)
       
      return
      end SUBROUTINE INPUT_File_GRID 
      SUBROUTINE INPUT_File_FRAG (FileID, FilePOST, IndexNum, PlotFilename)
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description: Create a filename for Ploting data and get a unit.          *
!*******************************************************************************
! MODULEs:

      implicit none
!
      integer, intent(in) :: IndexNum
      character(60), intent(out) :: PlotFilename
      character*(*), intent(in) :: FileID, FilePOST
      
      character(15) :: Plot, CPlot
      integer :: lenstr
!
! Begin:
      write(Plot,'(i4.4)') IndexNum
      call RemoveBlanks (Plot, CPlot, lenstr)  
      PlotFilename=trim(FileID)//CPlot(1:lenstr)//trim(FilePOST)          
       
      return
      end SUBROUTINE INPUT_File_FRAG 
      end MODULE AIMDFT_Files
