      SUBROUTINE Store_result_direct()
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:                                                             *
!*******************************************************************************
! MODULEs

      USE type_density
      USE QM_defaults
      USE QM_objects
      USE N_integration
      USE NI_defaults
      USE type_molecule
      USE AIMDFT_type
      USE AIMDFT_files
      USE type_plotting
      USE GetMolecularProperties

      implicit none
      
      character (60):: PlotFilenames
      
      double precision, dimension(:), allocatable :: Vpotl
      double precision, dimension(:), allocatable :: V12dr2
      double precision :: TraceAB
      double precision :: Xpt,Ypt,Zpt

      logical :: Lerror
      integer :: IApoint
      integer :: Znum, Iatom
      integer :: kfound
      integer :: File_unit
      integer ::  newindex
     
      CALL GET_object ('GRID', 'RADIAL', RADIAL_grid)
!      CALL GET_object ('QM', 'ENERGY_COMPONENTS', Wavefunction)
      
      newindex=IndexNumber
 
      Iatom=1 ! Just add the intrest atom within the fragment.
      NAIMprint=1
      AIMprint(1)=1

!      write(*,*)
      CALL GET_object ('QM', 'ENERGY_VEE', 'NUMERICAL')
!      CALL GET_object ('QM', 'ENERGY_EXCHANGE', 'NUMERICAL')

!      CALL GET_object ('QM', 'ENERGY_COULOMB', 'MO')
      write(*,*)
      CALL GET_object ('QM', 'ENERGY_COULOMB', 'NUMERICAL')
      write(*,*)
      CALL GET_object ('QM', 'ENERGY_KINETIC', 'NUMERICAL')
      write(*,*)
      CALL GET_object ('QM', 'ENERGY_VNE', 'NUMERICAL')
      write(*,*)

      Znum=CARTESIAN(Iatom)%Atomic_Number
      kfound=GRID_loc(Znum)
      NApts_atom=NApoints_atom(kfound)        
      
      allocate(grid_points(1:NApts_atom))
     ! Save grid points
      do IApoint=1,NApts_atom
         grid_points(IApoint)%X=Egridpts(Iatom,IApoint)%X+CARTESIAN(Iatom)%X
         grid_points(IApoint)%Y=Egridpts(Iatom,IApoint)%Y+CARTESIAN(Iatom)%Y
         grid_points(IApoint)%Z=Egridpts(Iatom,IApoint)%Z+CARTESIAN(Iatom)%Z
         grid_points(IApoint)%w=Egridpts(Iatom,IApoint)%w
      end do ! IApoint

      ! compute the weight for all grid points
    
      allocate(Bweights(NApts_atom))          
      CALL GET_weights (grid_points, NApts_atom, Iatom, Bweights)

      ! compute the charge for all grid points                  
      allocate (rho_Atom(NApts_atom))          
      CALL GET_density (grid_points, NApts_atom, rho_Atom)
      
      ! compute the V12dr2 for all grid points! 
      ! < IT IS Not nessessary to calculate it.
      allocate(Vpotl(1:NApts_atom))
      allocate(V12dr2(1:MATlen))   
      do IApoint=1,NApts_atom        
         Xpt=grid_points(IApoint)%X
         Ypt=grid_points(IApoint)%Y
         Zpt=grid_points(IApoint)%Z
         CALL I1E_V12dr2 (V12dr2, MATlen,Xpt,Ypt,Zpt) 
         Vpotl(IApoint)=-TraceAB (PM0, V12dr2, NBasis)
      end do ! IApoint
      
      CALL Store_grid_points_direct
      CALL add_to_prop_file('Molecule_Properties.txt')
      
      deallocate (rho_Atom)
      deallocate (V12dr2)
      deallocate (Vpotl)
      deallocate (grid_points)
      
CONTAINS !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      SUBROUTINE Store_grid_points_direct()
      
      implicit none
         
      CALL INPUT_File_FRAG ('GRID_DIRECT_','.dat',IndexNumber, PlotFilenames)  
      CALL GET_unit (PlotFilenames, File_unit, Lerror)
      open(UNIT=File_unit,file=PlotFilenames, status='REPLACE',form='formatted')
      write(File_unit,'(A20,7A24)') "X","Y","Z","Rho","BW","AW","Q=w.W.Rho","Vpol"
      do IApoint=1,NApts_atom
!    if (rho_Atom(IApoint)*grid_points(IApoint)%w*Bweights(IApoint).lt.1.0D-15)&
!    cycle 
        write(File_unit,Pfmt_rho_w_v) grid_points(IApoint)%X,  &
                                  &   grid_points(IApoint)%Y,  &
                                  &   grid_points(IApoint)%Z,  &                                   
                                  &   rho_Atom(IApoint),       &
                                  &   Bweights(IApoint),       &
                                  &   grid_points(IApoint)%w,  &
                                  &   rho_Atom(IApoint)*&
                                  grid_points(IApoint)%w*Bweights(IApoint),  &
                                  &   Vpotl(IApoint)  
      end do ! Ipoint
      close(UNIT=File_unit) 
      end SUBROUTINE Store_grid_points_direct
      SUBROUTINE add_to_prop_file(FilePath)
!*****e*************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:                                                             *
!*******************************************************************************
! MODULEs


      implicit none
      
      character (20) :: Radnote
      logical :: Lerror
      integer :: File_unit
      double precision :: EV_Anal, EV_Num, EX, ET, EC, EXJ, ECK, EC_AnaNum
      double precision :: EleNum, vee_A
      character (*) :: FilePath


      write(*,'(a,I5)') "  Atomic properties for atom #", Iatom 
      write(*,'(a)')    "  +++++++++++++++++++++++++++++++++++++++"   
      CALL CalAtomicEle(grid_points, NApts_atom, rho_Atom, Bweights, EleNum) !
      write(*,'(a,F15.10)')"Number of Electrons, N           = ", EleNum

      EX = K_Atomic(Iatom)
      write(*,'(a,F15.10)')"Pure Exchange, K (Σ2K_ab)        = ", EX 

      EXJ = KHF_Atomic(Iatom)
      write(*,'(a,F15.10)')"HF Exchange, KHF (Σ2K_ab+ΣKaa)   = ", EXJ 
      
      ET = Atomic_Kinetic(Iatom)
      write(*,'(a,F15.10)')"Kinetic energy Numerical, T      = ", ET

      ! Calculate the atomic potential energy for intrest fragment atom.
      CALL get_Vne_Atom_Analytical(EV_Anal)
      write(*,'(a,F15.10)')"Potential Energy Analytical, Vne = ", EV_Anal      

      EV_Num = Atomic_Vne(Iatom)
      write(*,'(a,F15.10)')"Potential Energy Numerical, Vne  = ", EV_Num
      
      EC_AnaNum = Atomic_Coulomb(Iatom)
      write(*,'(a,F15.10)')"Coulomb Energy Anal/Num, Vee     = ", EC_AnaNum
      
      EC = J_Atomic(Iatom)
      write(*,'(a,F15.10)')"Pure Coulomb, J (Σ4J_ab+ΣJaa)    = ", EC

      ECK = JHF_Atomic(Iatom)
      write(*,'(a,F15.10)')"HF Coulomb, JHF (Σ4J_ab+Σ2Jaa)   = ", ECK

      write(*,'(a,F15.10)')"Jaa = Kaa                        = ", ECK-EC

      CALL CalcVeeSelfA(grid_points, NApts_atom, rho_Atom, Bweights, vee_A) 
      write(*,'(a,F15.10)')"Coulomb Numerically Over A       = ", vee_A
      write(*,*)

      CALL GET_unit (trim(FilePath), File_unit, Lerror)
      open(UNIT=File_unit,file=trim(FilePath), &
           status='old',form='formatted',position="append")
      write(File_unit,fmt_prop) newindex,   &
                & EleNum,                   &
                & EX,                       &
                & EXJ,                      &
                & ET,                       &
                & EV_Anal,                  &
                & EV_Num,                   &
                & EC_AnaNum,                &
                & EC,                       &
                & ECK,                      &
                & ECK-EC,                   &
                & vee_A 
                     
      close(unit=File_unit)
      
      end SUBROUTINE add_to_prop_file

      end SUBROUTINE Store_result_direct
