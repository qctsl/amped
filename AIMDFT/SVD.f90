      SUBROUTINE SVD(A,U,S,V,M,N)
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: sukhbinder Singh                                                 *
!     Modified by: Ibrahim Awad                                                *
!     Description: Program computes the matrix singular value decomposition.   *
!                  Using Lapack library.                                       *
!*******************************************************************************
! MODULEs:

      double precision, dimension(M,N) :: A
      double precision, dimension(M,M) :: U(M,M)
      double precision, dimension(N,N) :: VT, V
      double precision, dimension(N)   :: S
      double precision, dimension(:), ALLOCATABLE  :: WORK

      integer :: LDA, M, N, LWORK, LDVT, INFO, I, J
      character(1) :: JOBU, JOBVT

      JOBU='A'
      JOBVT='A'
      LDA=M
      LDU=M
      LDVT=N

      lwork=MAX(1,3*MIN(M,N)+MAX(M,N),5*MIN(M,N))

      allocate(WORK(lwork))

      CALL DGESVD(JOBU, JOBVT, M, N, A, LDA, S, U, LDU, VT, LDVT, &
      &                   WORK, LWORK, INFO )

      DO I=1, 3
         DO J=1, 3
            V(J,I)=VT(I,J)
         end do  
      end do 
      
      deallocate(WORK)
   
      end SUBROUTINE SVD 
