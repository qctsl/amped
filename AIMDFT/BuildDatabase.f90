      SUBROUTINE Add_To_DB
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:                                                             *
!*******************************************************************************
! MODULEs

      USE type_density
      USE QM_defaults
      USE N_integration
      USE NI_defaults
      USE Sorted_AIMDFT
      USE symbol_AIMDFT
      USE type_plotting
      USE GetMolecularProperties

      implicit none
      
      character (SymMax), dimension (:), allocatable ::  symbols
      type (FragsInfo), dimension(:), allocatable :: FragInfo
      type (FragAtomInfo), dimension(:,:), allocatable :: DatabaseFragAtoms
      type (FragAtomInfo), dimension(:,:), allocatable :: SortedFragAtoms
      integer, dimension(:), allocatable :: atomAdj
      double precision, dimension(:), allocatable :: Vpotl
      double precision, dimension(:), allocatable :: V12dr2
      double precision :: TraceAB
      double precision :: Xpt,Ypt,Zpt

      integer :: Iatom, newindex, NApts_atom_New, Jatom
      integer :: kfound
      integer :: IApoint
      integer :: Znum

      allocate (FragInfo(Natoms))  
      allocate (SortedFragAtoms(Natoms,Natoms))  
      allocate (DatabaseFragAtoms(Natoms,Natoms))  
      allocate (symbols(Natoms))  
      allocate (atomAdj(Natoms)) 
      
      CALL GET_object ('GRID', 'RADIAL', RADIAL_grid)
      CALL GET_object ('QM', 'ENERGY_COMPONENTS', Wavefunction)
      CALL GetSortCartTerm(NAtoms,SortedFragAtoms,FragInfo)
      CALL GetDBFormFragments(SortedFragAtoms,DatabaseFragAtoms,FragInfo)
      CALL GetFragSymbols(Level_Number,symbols)    

      newindex=IndexNumber ! add the new symbols       
      
      Iatom=1 ! Just add the intrest atom within the fragment.
      NAIMprint=1
      AIMprint(1)=1
      
!      write(*,*)
      CALL GET_object ('QM', 'ENERGY_VEE', 'NUMERICAL')
!      CALL GET_object ('QM', 'ENERGY_EXCHANGE', 'NUMERICAL')

!      CALL GET_object ('QM', 'ENERGY_COULOMB', 'MO')
!      write(*,*)
      CALL GET_object ('QM', 'ENERGY_COULOMB', 'NUMERICAL')
      write(*,*)
      CALL GET_object ('QM', 'ENERGY_KINETIC', 'NUMERICAL')
      write(*,*)
      CALL GET_object ('QM', 'ENERGY_VNE', 'NUMERICAL')
      write(*,*)
      
      Znum=CARTESIAN(Iatom)%Atomic_Number
      kfound=GRID_loc(Znum)
      NApts_atom=NApoints_atom(kfound)
      allocate(grid_points(1:NApts_atom))
      ! Save grid points
      do IApoint=1,NApts_atom
         grid_points(IApoint)%X=Egridpts(Iatom,IApoint)%X+CARTESIAN(Iatom)%X
         grid_points(IApoint)%Y=Egridpts(Iatom,IApoint)%Y+CARTESIAN(Iatom)%Y
         grid_points(IApoint)%Z=Egridpts(Iatom,IApoint)%Z+CARTESIAN(Iatom)%Z
         grid_points(IApoint)%w=Egridpts(Iatom,IApoint)%w
      end do

      ! compute the weight for all grid points
      allocate(Bweights(NApts_atom))
      CALL GET_weights (grid_points, NApts_atom, Iatom, Bweights)

      ! compute the charge for all grid points
      allocate (rho_Atom(NApts_atom))          
      CALL GET_density (grid_points, NApts_atom, rho_Atom)

      CALL FindAdj(1,Iatom,atomAdj)  

      ! compute the V12dr2 for all grid points  
      allocate(Vpotl(1:NApts_atom))
      allocate(V12dr2(1:MATlen))   
      do IApoint=1,NApts_atom        
         Xpt=grid_points(IApoint)%X
         Ypt=grid_points(IApoint)%Y
         Zpt=grid_points(IApoint)%Z
         CALL I1E_V12dr2 (V12dr2, MATlen,Xpt,Ypt,Zpt) 
         Vpotl(IApoint)=-TraceAB (PM0, V12dr2, NBasis)
      end do ! IApoint

      CALL Store_grid_points()
      CALL add_to_index_file()
      CALL add_to_cart_file()    
      CALL add_to_prop_file(PropFilePath)

      deallocate (rho_Atom)
      deallocate (V12dr2)
      deallocate (Vpotl)
      deallocate (grid_points)
      deallocate (FragInfo)  
      deallocate (SortedFragAtoms)  
      deallocate (DatabaseFragAtoms)  
      deallocate (symbols)  
      
CONTAINS !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    
      SUBROUTINE Store_grid_points()
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:                                                             *
!*******************************************************************************
! MODULEs

      implicit none
      
      type (FragAtomInfo), dimension(:), allocatable :: temp1
      type (FragAtomInfo), dimension(:), allocatable :: temp2
      character (len=132), allocatable:: GridFilenames
      logical :: Lerror
      integer :: File_unit
      
     ! Save grid points
      allocate (temp1(NApts_atom))
      allocate (temp2(NApts_atom))

      temp1%x=grid_points%x;temp1%y=grid_points%y;temp1%z=grid_points%z

      CALL DoCartRotation(temp1,temp2,NApts_atom,&
                          FragInfo(Iatom)%RMat,FragInfo(Iatom)%TMat)
      CALL INPUT_File_GRID (newindex, GridFilenames) ! just frag atom 
      CALL GET_unit (GridFilenames, File_unit, Lerror)

      open(UNIT=File_unit,file=GridFilenames, status='REPLACE',form='formatted')
      write(File_unit,'(A20,7A24)') "X","Y","Z","Rho",&
                                    "BW","AW","Q=w.W.Rho","Vpol"
      NApts_atom_New=0
      do IApoint=1,NApts_atom
!     if(rho_Atom(IApoint)*grid_points(IApoint)%w*Bweights(IApoint).lt.1.0D-09)&
!     cycle 
        NApts_atom_New=NApts_atom_New+1
        write(File_unit,Pfmt_rho_w_v) temp2(IApoint)%X,                        &
              &   temp2(IApoint)%Y,                                            &
              &   temp2(IApoint)%Z,                                            &                                   
              &   rho_Atom(IApoint),                                           &
              &   Bweights(IApoint),                                           &
              &   grid_points(IApoint)%w,                                      &
              &   rho_Atom(IApoint)*grid_points(IApoint)%w*Bweights(IApoint),  &
              &   Vpotl(IApoint) 
      end do ! Ipoint
      close(UNIT=File_unit) 
      deallocate(temp1) 
      deallocate(temp2)
      end SUBROUTINE Store_grid_points
      SUBROUTINE add_to_index_file()
!*****e*************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:                                                             *
!*******************************************************************************
! MODULEs

      USE type_Weights
      
      implicit none
      
      character (28) :: Radnote, coreSize
      logical :: Lerror
      integer :: File_unit
      
      Radnote=""
      coreSize=""
      if (trim(RADIAL_grid).eq.'GILL') then
         write(Radnote,'(I2,A1,I2,A1,I2,A1,I3,A1)') NRPoints_Gill,&
         & '(',NApoints_Gill(1),',',NApoints_Gill(2),',',NApoints_Gill(3),')'
      endif 
      if (trim(DEN_Partitioning).eq.'IAWAD') then
         write(coreSize,'(A)') Last_Core
      endif   
      
      write(*,'(/A)') "*****************************************"
      write(*,*) "The fragment with symbol ", trim(Symbols(Iatom))
      write(*,*) "has been added to the database with index # ", newindex 
      write(*,'(A/)') "*****************************************"     
      CALL GET_unit (trim(IndexFilePath), File_unit, Lerror)
      open(UNIT=File_unit,file=trim(IndexFilePath), &
           status='old',form='formatted',position="append")
      write(File_unit,fmt_index) newindex,  &
                & Symbols(Iatom),           &
                & NAtoms,                   &
                & NApts_atom_New,           &
                & trim(RADIAL_grid),        &
                & trim(Radnote),            &
                & trim(Wavefunction),       &
                & trim(Basis_set_name),     &
                & trim(DEN_Partitioning),   &
                & trim(coreSize),           &
                & trim(OPT_status)
      close(unit=File_unit)
      
      end SUBROUTINE add_to_index_file
      SUBROUTINE add_to_prop_file(PropFilePath)
!*****e*************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:                                                             *
!*******************************************************************************
! MODULEs

      implicit none
      
      character (20) :: Radnote
      logical :: Lerror
      integer :: File_unit
      double precision :: EV_Anal, EV_Num, EX, ET, EC, EXJ, ECK, EC_AnaNum
      double precision :: EleNum, vee_A
      character (*) :: PropFilePath
      

      write(*,'(a,I5)') "  Atomic properties for atom #", Iatom 
      write(*,'(a)')    "  +++++++++++++++++++++++++++++++++++++++"   
      CALL CalAtomicEle(grid_points, NApts_atom, rho_Atom, Bweights, EleNum) !
      write(*,'(a,F15.10)')"Number of Electrons, N           = ", EleNum

      EX = K_Atomic(Iatom)
      write(*,'(a,F15.10)')"Pure Exchange, K (Σ2K_ab)        = ", EX 

      EXJ = KHF_Atomic(Iatom)
      write(*,'(a,F15.10)')"HF Exchange, KHF (Σ2K_ab+ΣKaa)   = ", EXJ 
      
      ET = Atomic_Kinetic(Iatom)
      write(*,'(a,F15.10)')"Kinetic energy Numerical, T      = ", ET

      ! Calculate the atomic potential energy for intrest fragment atom.
      CALL get_Vne_Atom_Analytical(EV_Anal)
      write(*,'(a,F15.10)')"Potential Energy Analytical, Vne = ", EV_Anal      

      EV_Num = Atomic_Vne(Iatom)
      write(*,'(a,F15.10)')"Potential Energy Numerical, Vne  = ", EV_Num
      
      EC_AnaNum = Atomic_Coulomb(Iatom)
      write(*,'(a,F15.10)')"Coulomb Energy Anal/Num, Vee     = ", EC_AnaNum
      
      EC = J_Atomic(Iatom)
      write(*,'(a,F15.10)')"Pure Coulomb, J (Σ4J_ab+ΣJaa)    = ", EC

      ECK = JHF_Atomic(Iatom)
      write(*,'(a,F15.10)')"HF Coulomb, JHF (Σ4J_ab+Σ2Jaa)   = ", ECK

      write(*,'(a,F15.10)')"Jaa = Kaa                        = ", ECK-EC

      CALL CalcVeeSelfA(grid_points, NApts_atom, rho_Atom, Bweights, vee_A) 
      write(*,'(a,F15.10)')"Coulomb Numerically Over A       = ", vee_A
      write(*,*)


      CALL GET_unit (trim(PropFilePath), File_unit, Lerror)
      open(UNIT=File_unit,file=trim(PropFilePath), &
           status='old',form='formatted',position="append")
      write(File_unit,fmt_prop) newindex,   &
                & EleNum,            &
                & EX,                       &
                & EXJ,                      &
                & ET,                       &
                & EV_Anal,                  &
                & EV_Num,                   &
                & EC_AnaNum,                &
                & EC,                       &
                & ECK,                      &
                & ECK-EC,                   &
                & vee_A 
                     
      close(unit=File_unit)
      
      end SUBROUTINE add_to_prop_file
      SUBROUTINE add_to_cart_file()
!*****e*************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:                                                             *
!*******************************************************************************
! MODULEs

      implicit none
      integer :: katom 
      integer :: File_unit
      logical :: Lerror
      
      CALL GET_unit (trim(CartFilePath), File_unit, Lerror)
      open(UNIT=File_unit,file=trim(CartFilePath), &
           status='old',form='formatted',position="append")     
                  
      do katom=1,NAtoms
         write(File_unit,fmt_cart) newindex,                                 & 
                                 & katom,                                    &
                                 & DatabaseFragAtoms(Iatom,katom)%element,   &
                                 & DatabaseFragAtoms(Iatom,katom)%x,         &
                                 & DatabaseFragAtoms(Iatom,katom)%y,         &
                                 & DatabaseFragAtoms(Iatom,katom)%z
      end do
      close(unit=File_unit)      
      
      end SUBROUTINE add_to_cart_file
      end SUBROUTINE Add_To_DB
