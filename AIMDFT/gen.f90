      subroutine PRINTPM0
!*****e**********************************************************************************
!     Date last modified: August 15, 2012                                               *
!     Authors: R.A. Poirier                                                             *
!     Description:                                                                      *
!                 Calculate the charge density on a set of grid points (mesh)           *
!****************************************************************************************
! Modules:
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE module_grid_points
      USE type_density
      USE type_molecule
      USE type_plotting

      implicit none
!
! Local scalars:
      integer :: Ipoint, lenstr
      double precision :: TraceAB
      double precision :: PMATOM(MATlen)
      integer :: File_unit
      logical :: Lerror,exist
      character (32) :: BasisSetN, Wavef
      
      double precision :: AOprod(MATlen),rx, ry, rz,rxx, ryy, rzz, AOprodAtom(NAtoms,MATlen)
!
! Begin:
!
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)
      call Gaussian_products (rx, ry, rz, AOprod, MATlen)
      write(*,*) '**********************'
!     call Gaussian_products_atoms (rxx, ryy, rzz, AOprodAtom, MATlen)
            write(*,*) '**********************'
      CALL GET_unit ("test.txt", File_unit, Lerror)
            write(*,*) '**********************'
      inquire(file="test.txt", exist=exist)
      if (exist) then
        open(File_unit, file="test.txt", status="old", position="append", action="write")
      else
        open(File_unit, file="test.txt", status="new", action="write")
      end if
      if (CARTESIAN(1)%Atomic_number.eq.1) then
      
      call RemoveBlanks(Basis_set_name, BasisSetN, lenstr)
      
      write(File_unit,'(8x,4a)') 'Basis_name="',BasisSetN(1:lenstr),'"'
      call RemoveBlanks(Wavefunction, wavef, lenstr)
      write(File_unit,'(8x,3a)') 'wave_function="',wavef(1:lenstr),'"'
      write(File_unit,'(8x,a)') 'select case(Znum)'      
      endif
      
      write(File_unit,'(10x,a,i3,a)') "case(",CARTESIAN(1)%Atomic_number,")"
      do Ipoint=1,MATlen
          write(File_unit,'(12x,a,i3,a2,ES20.12,2x)') 'PMATOM(',Ipoint,')=',PM0(Ipoint)
      end do ! Ipoint
      close(File_unit)
      
      
      
      return
      end subroutine PRINTPM0
