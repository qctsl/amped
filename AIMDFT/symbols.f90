      MODULE symbol_AIMDFT
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:                                                             *
!*******************************************************************************
!      
      USE Sorted_AIMDFT

      implicit none
      
CONTAINS !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 

      SUBROUTINE GetUniSym(level, UniSym, UniSymMUNIdx, UniSymMum)
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description: Return a unique symbols array and its count                 *
!                  within the symbols list                                     *
!*******************************************************************************
! 
      implicit none
      
      integer, intent(in) ::  level
      character (SymMax), dimension (:), allocatable, intent(out) :: UniSym
      integer, dimension (:), allocatable, intent(out) :: UniSymMUNIdx
      character (SymMax), dimension (:), allocatable :: symbols

      integer, intent(out) :: UniSymMum
      integer :: Jatom
      
      allocate (UniSym(NAtoms))
      allocate (symbols(Natoms))
      allocate (UniSymMUNIdx(Natoms))

      CALL GetFragSymbols(level,symbols) 

      UniSym = ""
      UniSymMum = 0   ! count the unique symbols within the molecule 
      do Jatom=1, NAtoms
         if (Cartesian(Jatom)%Atomic_Number.eq.0) cycle ! exclude dummy atoms
         if (ANY(UniSym.eq.symbols(Jatom))) cycle ! Founded before
         UniSymMum=UniSymMum+1
         UniSym(UniSymMum)=symbols(Jatom)
         UniSymMUNIdx(UniSymMum)=Jatom  ! Just the first match
      end do
      deallocate(symbols)
      return
      end SUBROUTINE GetUniSym
      SUBROUTINE GetFragSymbols(level,symbols) 
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description: Generate unique symbols for the molecule fragements         *
!                  [e.g. 7( 6( 6( 8 8 6) 1( 6) 1( 6)) 1() 1()) ]               *
!*******************************************************************************
!

      implicit none 

      integer, intent(in) :: level
      character (SymMax), dimension (:), intent(out), allocatable :: symbols

      type (FragAtomInfo), dimension(:,:), allocatable :: FragAtoms 
      type (FragsInfo), dimension(:), allocatable :: FragInfo ! (main atom)
      
      character (SymMax), dimension(:), allocatable :: tempOrdValue
      integer, dimension(10) :: tempOrd
      integer :: NtempOrd, TempCopy, curr,next
      character (SymMax) :: TempCopyChar
      character (10) :: atomicN
      character (len=1), parameter :: SepSymbol=","

      integer :: ilevel
      integer :: ifound
      integer :: Matom
      integer :: Jatom
      logical :: swap
      
      allocate (symbols(Natoms))
      allocate (FragAtoms(Natoms,Natoms))  
      allocate (FragInfo(Natoms)) 
      allocate (tempOrdValue(Natoms)) 
           
     
      CALL GetSortCart(level,FragAtoms,FragInfo)
      
      ! JUST ::::defined symbol for level 1 
      do Jatom=1, NAtoms     
      if (Cartesian(JAtom)%Atomic_Number.eq.0) cycle ! exclude dummay atoms
         do Matom=1, Natoms  ! loop for subatoms
            ! Defind the MainAtom
            write(atomicN,'(I0)') Cartesian(Jatom)%Atomic_Number 
            FragInfo(Matom)%symbol(Jatom,1)=trim(atomicN)//"("
            NtempOrd=0
            tempOrd=0
            do ifound=1, FragInfo(Jatom)%NAtoms  ! loop over subatoms
               if (FragAtoms(Jatom,ifound)%level.ne.1) cycle !Just level 1
               ! exclude subatom=Matom
               if (FragAtoms(Jatom,ifound)%MUNIdx.eq.Matom) cycle 
               ! exclude Mainatom=Mainatom
               if (FragAtoms(Jatom,ifound)%MUNIdx.eq.Jatom) cycle 
               NtempOrd=NtempOrd+1
               tempOrd(NtempOrd)=FragAtoms(Jatom,ifound)%Atomic_Number
            end do
            swap=.true.
            do while(swap)
               swap=.false.
               do curr=1,NtempOrd-1
                  next=curr+1
                  if (tempOrd(curr).lt.tempOrd(next)) then
                     TempCopy=tempOrd(curr)
                     tempOrd(curr)=tempOrd(next)
                     tempOrd(next)=TempCopy
                     swap=.true.
                  endif !(valueB.lt.valueA)
               end do !curr
            end do !while(swap)
            do ifound=1, NtempOrd
               if (ifound.ne.NtempOrd) then
               ! the new subatom
               write(atomicN,'(I0,A1)') tempOrd(ifound),Sepsymbol   
               else
               write(atomicN,'(I0)') tempOrd(ifound)   ! the new subatom
               end if
               ! add the new subatom
               FragInfo(Matom)%symbol(Jatom,1)=&
               trim(FragInfo(Matom)%symbol(Jatom,1))//trim(atomicN)//"" 
            end do 
            ! just level 1, the neighber
            FragInfo(Matom)%symbol(Jatom,1)=&
            trim(FragInfo(Matom)%symbol(Jatom,1))//")" 
            if (Matom.eq.Jatom) symbols(Jatom)=&
            trim(FragInfo(Matom)%symbol(Jatom,1))
         end do  !Matom
      end do ! Jatom 
      
      ! Now from the symbols of the 1st level, 
      ! we will build the symbols for ALL levels
      do ilevel=2, level
         do Jatom=1, NAtoms
            do Matom=1, Natoms ! loop over dumatoms
               ! Defind the Mainatom
               write(atomicN,'(I0)') Cartesian(Jatom)%Atomic_Number 
               FragInfo(Matom)%symbol(Jatom,ilevel)=trim(atomicN)//"("                  
               NtempOrd=0
               tempOrd=0
               tempOrdValue=""
               do ifound=1, FragInfo(Jatom)%NAtoms  ! loop over subatoms
                 if (FragAtoms(Jatom,ifound)%level.ne.1) cycle !Just level 1
                 ! exclude subatom=Matom
                 if (FragAtoms(Jatom,ifound)%MUNIdx.eq.Matom) cycle 
                 ! exclude MainAtom=Mainatom
                 if (FragAtoms(Jatom,ifound)%MUNIdx.eq.Jatom) cycle 
                 NtempOrd=NtempOrd+1
                 tempOrd(NtempOrd)= FragAtoms(Jatom,ifound)%Atomic_Number
                 tempOrdValue(NtempOrd)=&
                 FragInfo(Matom)%symbol(FragAtoms(Jatom,ifound)%MUNIdx,ilevel-1)
               end do
               swap=.true.
               do while(swap)
                  swap=.false.
                  do curr=1,NtempOrd-1
                     next=curr+1
                     if (tempOrd(curr).lt.tempOrd(next)) then
                        TempCopy=tempOrd(curr)
                        TempCopyChar=tempOrdValue(curr)
                        tempOrd(curr)=tempOrd(next)
                        tempOrdValue(curr)=tempOrdValue(next)
                        tempOrd(next)=TempCopy
                        tempOrdValue(next)=trim(TempCopyChar)
                        swap=.true.
                     end if !(valueB.lt.valueA)
                  end do !curr
               end do !while(swap)                  
               do ifound=1, NtempOrd
                  FragInfo(Matom)%symbol(Jatom,ilevel)=&
                  trim(FragInfo(Matom)%symbol(Jatom,ilevel))// & ! like x=x+??
                  & tempOrdValue(ifound)//"" ! adding the new subatom
               end do
               !just level 1, the neighber
               FragInfo(Matom)%symbol(Jatom,ilevel)=&
               trim(FragInfo(Matom)%symbol(Jatom,ilevel))//")"    
               if (Matom.eq.Jatom) symbols(Jatom) =&
                trim(FragInfo(Matom)%symbol(Jatom,ilevel))
            end do ! Matom
         end do ! Jatom
      end do ! level
      deallocate(FragAtoms, FragInfo, tempOrdValue)
      return
      end SUBROUTINE GetFragSymbols
      end MODULE symbol_AIMDFT
