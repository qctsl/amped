      SUBROUTINE SET_defaults_AIMDFT
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:                                                             *
!*******************************************************************************
! 
      USE AIMDFT_type
      
      implicit none
            
!     Parameters for fragmant atom
      Level_Number    = 2
      TerminalAtom    = 1       ! Add H as termenal 
      IndexNumber     = 0
      MaxGridPnt  = 150000
 
      DirectMethod    = .False.  ! Read from the database.
      DatabaseBWeight = .True.   ! Use the Bweight from the database 
                                 ! (Do not generate new weights)
      OPT_status      = "NO_OPT"
      RotUpdate       = .True. ! update the new coordiants rotation

!     DataBase Parameters
      CALL get_environment_variable("HOME",homedir) 
      DirLoc                     = trim(homedir)//"/MUNgauss/AIMDFT/DATABASE/"

      DBFileExt      = "txt"
      CsvSep                     = ","     
      IndexFileName              = "Tables_Index"
      CartFileName               = "Carts_Index"
      PropFileName         = "Properties_Index"
       
!     Database file locations
      IndexFilePath   = trim(DirLoc)//trim(IndexFileName)//"."//trim(DBFileExt) 
      CartFilePath    = trim(DirLoc)//trim(CartFileName)//"."//trim(DBFileExt)
      PropFilePath    = trim(DirLoc)//trim(PropFileName)//"."//trim(DBFileExt)
      GridPath = trim(DirLoc)      

!     Output format 
      fmt_GridPts='f26.16'
      fmt_rho='f26.16' 
      fmt_xyz='f24.16'
      fmt_w='E24.16'

      Pfmt_rho='(3'//trim(fmt_GridPts)//','//trim(fmt_rho)//')' 
      Pfmt_rho_w='(3'//trim(fmt_w)//','//trim(fmt_w)//','//trim(fmt_w)//')'
      Pfmt_rho_w_v='(3'//trim(fmt_w)//',4'//trim(fmt_w)//','//trim(fmt_xyz)//')'
      Pfmt_rho_dd='(10'//trim(fmt_w)//')'
      
      write(fmt_index,'(A8,I0,A22)')"(I5,2X,A",SymMax,",I2,I8,A8,A15,A8,4A14)"
      fmt_index_title='(A5,A16,A30,A7,A7,A26,A12,3A12)'

      fmt_cart='(I5,I3,3X,A3,3'//fmt_xyz//')'
      fmt_cart_title='(A5,A3,3X,A3,A22,A26,A28)'

      fmt_prop='(I5,11'//fmt_xyz//')'
      fmt_prop_title='(A5,11A24)'    

      end SUBROUTINE SET_defaults_AIMDFT

