      MODULE mod_rotation
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description:                                                             *
!*******************************************************************************
!
      USE AIMDFT_type
      USE type_molecule
      
      implicit none

CONTAINS !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

      SUBROUTINE GetCentroidCart (A,n,T)
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description: Calculate the centroid (Geometry center) of n coordinates   *
!                  points                                                      *
!*******************************************************************************
! MODULEs:

      implicit none
      
      ! A=(?:1), Y=(?,2), Z=(?,3) 
      double precision, dimension (:,:), intent(in), allocatable :: A 
      double precision, dimension (:), intent(out), allocatable :: T
      integer, intent(in) :: n ! Number of coordinates.
      integer :: i, j

      allocate(T(1:3)) ! (X,Y,Z) values

      T=0.0d0;
      do i=1, n 
         do j=1, 3 ! loop over X, Y and Z values
            T(j)=T(j)+A(i,j)
         end do
      end do
      do j=1, 3 
      T(j)=T(j)/n
      end do
      return      
      end SUBROUTINE GetCentroidCart  
      SUBROUTINE GetRotMatrix (NewCart,CurrCart,numberpoints,R,T)
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description: Return the rotation and translation matrices for a set of   *
!                  points using least square method for fitting (SVD) .        *
!*******************************************************************************
!

      implicit none
  
      double precision, dimension(:,:), intent(in), allocatable :: NewCart
      double precision, dimension(:,:), intent(in), allocatable :: CurrCart
      integer, intent(in) :: numberpoints

      double precision, dimension (3,3), intent(out) :: R
      double precision, dimension (3), intent(out) :: T
      
      double precision, dimension (:), allocatable :: Aver_A, Aver_B 
      double precision, dimension (:,:), allocatable :: H_a, H_b ! large arrays

      double precision, dimension (1:3) :: temp_Ha, temp_Hb, SSS
      double precision, dimension (1:3,1:3) :: H_matrix,HH,AAA,UUU,VVV,VT,UT

      integer :: i,j,k,l
      
      allocate (H_a(1:numberpoints,1:3))        
      allocate (H_b(1:numberpoints,1:3))
      allocate (Aver_A(1:3))       
      allocate (Aver_B(1:3))

      ! calculate average of matrix A and matrix B 
      call GetCentroidCart (NewCart, numberpoints, Aver_A)  
      call GetCentroidCart (CurrCart, numberpoints, Aver_B)

      ! calculate H matrix 
      H_matrix=0.0d0;  H_a=0.0d0 ; H_b=0.0d0;
      do i=1, numberpoints
         do j=1,3
            H_a(i,j)=NewCart(i,j)-Aver_A(j) !H_a(i,j)=A(i,j)-A(1,j)
            H_b(i,j)=CurrCart(i,j)-Aver_B(j) !H_b(i,j)=B(i,j)-B(1,j)
         end do
      end do 
      do i=1, numberpoints
         temp_Ha=0.0d0; temp_Hb=0.0d0
         do j=1,3
            temp_Ha(j)=H_a(i,j)
            temp_Hb(j)=H_b(i,j)
         end do
         HH=0.0d0
         do k=1,3
            do l=1,3          
               HH(k,l)=temp_Ha(k)*temp_Hb(l)     
            end do
         end do
         H_matrix=H_matrix+ HH
      end do
      CALL SVD(H_matrix,UUU,SSS,VVV,3,3) !Calculate SVD
      VT=transpose(VVV)
      UT=transpose(UUU)    
      R=matmul(VVV,UT) !Calculate Rotation matrix
      T=matmul(-R,Aver_A)+Aver_B
      deallocate(H_a, H_b, Aver_A, Aver_B)
      return
      end SUBROUTINE GetRotMatrix
      SUBROUTINE DoRotation (OldCart,NewCart,numberpoints,R,T)
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description: Get the new coordinate after applying the rotation and      *
!                  the translation matrices.                                   *
!*******************************************************************************
!
      implicit none 

      integer, intent(in) :: numberpoints
      double precision, dimension (:,:), intent(in), allocatable :: OldCart
      double precision, dimension (3,3), intent(in) :: R
      double precision, dimension (3), intent(in) :: T
      double precision, dimension (:,:), intent(out), allocatable :: NewCart      

      double precision, dimension (1:3) :: NewPoint, point
      integer::i

      allocate(NewCart(1:numberpoints,3)) 

      do i=1, numberpoints        
         point=(/OldCart(i,1),OldCart(i,2),OldCart(i,3)/)
         NewPoint= Matmul(R,point) + T
         NewCart(i,1)=NewPoint(1)   ! X points
         NewCart(i,2)=NewPoint(2)   ! Y points
         NewCart(i,3)=NewPoint(3)   ! Z points
      end do
      return
      end SUBROUTINE DoRotation
      SUBROUTINE GetOldPoints (OldCart,NewCart,numberpoints,R,T)
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description: Get the old coordinate before applying the rotation and     *
!                  the translation matrices.                                   *
!*******************************************************************************
!
      USE mod_math

      implicit none 

      double precision, dimension (:,:), intent(out), allocatable :: OldCart
      double precision, dimension (3,3), intent(in) :: R
      double precision, dimension (3), intent(in) :: T
      double precision, dimension(:,:), intent(in), allocatable :: NewCart      
      integer, intent(in) :: numberpoints
      
      double precision, dimension (:,:), allocatable :: RI
      double precision, dimension (1:3) :: NewPoint, point
      integer::i

      allocate(OldCart(1:numberpoints,3)) 
      allocate(RI(3,3))

      call INV33(R,RI)
      do i=1, numberpoints        
         point=(/NewCart(i,1),NewCart(i,2),NewCart(i,3)/)
         NewPoint=Matmul(RI,(point-T))
         OldCart(i,1)=NewPoint(1)
         OldCart(i,2)=NewPoint(2)
         OldCart(i,3)=NewPoint(3)
      end do
      return
      end SUBROUTINE GetOldPoints     
      SUBROUTINE GetStoredRotMatrix (OldCart,numberpoints,R,T)
!*******************************************************************************
!     Date last modified:                                                      *
!     Author: Ibrahim Awad                                                     *
!     Description: Return the rotation and translation matrices that needed    *
!                  to get the standard form.                                   *
!*******************************************************************************
!
      implicit none

      double precision, dimension (:,:), intent(in), allocatable :: OldCart
      double precision, dimension (3,3), intent(out) :: R
      double precision, dimension (3), intent(out) :: T      
      integer, intent(in) :: numberpoints

      double precision, dimension (:,:), allocatable :: Tempcart 

      double precision :: AB_side,AC_side,BC_side
      double precision :: AD_side,BD_side,S_ABC,S_ABD
      double precision :: CD_side,CG_side,DG_side,Y_along4C
      double precision :: Y_along3,Z_along3,signang_ABC,signang_ABD
      double precision :: Y_alongC4,Z_alongC4,Y_along4,Z_along4,EG_side
      double precision :: ABS_ABC,BCS_ABC,ACS_ABC,ABS_ABD,BDS_ABD
      double precision :: ADS_ABD,ss1,ss2,ss3,dd,X4,X4_1,X4_2,DG_sideC,CD_sideC
      integer::I,J,signa_ABC,signa_ABD,numberw,signa_CDG,signang_CDG

      allocate (Tempcart(1:numberpoints,1:3))

      numberw=1
      TempCart=0.0d0
      SELECT CASE (numberpoints)
      CASE (1)
         TempCart(1,1:3)=0.0d0
         numberw=1
      CASE (2)
         numberw=2
         TempCart(1:2,1:3)=0.0d0
         TempCart(2,3)=DSQRT((OldCart(2,1)-OldCart(1,1))**2+(OldCart(2,2)-&
                              OldCart(1,2))**2+(OldCart(2,3)-OldCart(1,3))**2)
         !TempCart(3,1)=0.0d0;TempCart(3,2)=0.0d0;TempCart(3,3)=0.0
      CASE (3:)
         numberw=3
         ! ABC triangle  
         AB_side=DSQRT((OldCart(2,1)-OldCart(1,1))**2+(OldCart(2,2)-&
                        OldCart(1,2))**2+(OldCart(2,3)-OldCart(1,3))**2)
         AC_side=DSQRT((OldCart(3,1)-OldCart(1,1))**2+(OldCart(3,2)-&
                        OldCart(1,2))**2+(OldCart(3,3)-OldCart(1,3))**2)
         BC_side=DSQRT((OldCart(2,1)-OldCart(3,1))**2+(OldCart(2,2)-&
                        OldCart(3,2))**2+(OldCart(2,3)-OldCart(3,3))**2)
         S_ABC = (AB_side+BC_side+AC_side)/2.0
         ABS_ABC=S_ABC-AB_side
         ACS_ABC=S_ABC-AC_side
         BCS_ABC=S_ABC-BC_side
         signang_ABC=AB_side**2+AC_side**2-BC_side**2
         signa_ABC=1
         if (signang_ABC.le.0) signa_ABC=-1
         Y_along3=DSQRT((4.0*S_ABC*ABS_ABC*ACS_ABC*BCS_ABC)/(AB_side**2))
         Z_along3=DSQRT(AC_side**2-Y_along3**2)*signa_ABC
         TempCart=0.0d0
         TempCart(2,3)=AB_side ! Z-axes for second point
         TempCart(3,2)=Y_along3
         TempCart(3,3)=Z_along3 
         if (numberpoints.ne.3) then
            numberw=4
            ! ABD triangle
            AD_side=DSQRT((OldCart(4,1)-OldCart(1,1))**2+(OldCart(4,2)-&
                           OldCart(1,2))**2+(OldCart(4,3)-OldCart(1,3))**2)
            BD_side=DSQRT((OldCart(2,1)-OldCart(4,1))**2+(OldCart(2,2)-&
                           OldCart(4,2))**2+(OldCart(2,3)-OldCart(4,3))**2)
            S_ABD = (AB_side+BD_side+AD_side)/2.0
            ABS_ABD=S_ABD-AB_side
            ADS_ABD=S_ABD-AD_side
            BDS_ABD=S_ABD-BD_side
            signang_ABD=AB_side**2+AD_side**2-BD_side**2
            signa_ABD=1
            if (signang_ABD.le.0) signa_ABD=-1
            ! perpendicular of triangle ABD
            Y_alongC4=DSQRT((4.0*S_ABD*ABS_ABD*ADS_ABD*BDS_ABD)/(AB_side**2))  
            Z_along4=DSQRT(AD_side**2-Y_alongC4**2) *signa_ABD ! Z axes >>>OK<<<
            ! equation of plane ss1.X+ss2.Y+ss3.Z+dd=0 >>>
            ! >> Using Vectors to Describe a Plane
            ss1=(OldCart(2,2)-OldCart(1,2))*(OldCart(3,3)-OldCart(1,3))-&
                (OldCart(2,3)-OldCart(1,3))*(OldCart(3,2)-OldCart(1,2))
            ss2=(OldCart(2,3)-OldCart(1,3))*(OldCart(3,1)-OldCart(1,1))-&
                (OldCart(2,1)-OldCart(1,1))*(OldCart(3,3)-OldCart(1,3))
            ss3=(OldCart(2,1)-OldCart(1,1))*(OldCart(3,2)-OldCart(1,2))-&
                (OldCart(2,2)-OldCart(1,2))*(OldCart(3,1)-OldCart(1,1))
            dd=-(ss1*OldCart(1,1)+ss2*OldCart(1,2)+ss3*OldCart(1,3))
            ! Distance from a point to a plane
            X4_1=(ss1*OldCart(4,1)+ss2*OldCart(4,2)+ss3*OldCart(4,3)+dd)
            X4_2=DSQRT(ss1**2+ss2**2+ss3**2)
            if (abs(X4_2).lt.1.0d-06) then
               X4=0.0d0
            else
               X4=X4_1/X4_2
            end if       
            !Sign Y4 ?????? CDG triangle 
            CD_sideC=DSQRT((OldCart(3,1)-OldCart(4,1))**2+(OldCart(3,2)-&
                            OldCart(4,2))**2+(OldCart(3,3)-OldCart(4,3))**2)
            CD_side=DSQRT(CD_sideC**2-X4**2)     
            CG_side=Y_along3
            !>>>>>>>>By USing the new coordinate Just Y and Z
            EG_side=Z_along3-Z_along4
            Y_along4C=DSQRT(Y_alongC4**2-X4**2)  ! without sign
            DG_side=DSQRT(Y_along4C**2+EG_side**2)
           !DG_side=DSQRT((0-OldCart(4,1))**2+(0-OldCart(4,2))**2+(DG_sideC)**2)
    
            signang_CDG=CG_side**2+DG_side**2-CD_side**2
            signa_CDG=1
            if (signang_CDG.le.0) signa_CDG=-1
            Y_along4=Y_along4C*signa_CDG
            X4=abs(X4) ! we want X alwayes positive
            TempCart(4,1)=X4 ! Z-axes for second point
            TempCart(4,2)=Y_along4
            TempCart(4,3)=Z_along4
         end if
      end SELECT 
      call GetRotMatrix (OldCart,TempCart,numberw,R,T)
      deallocate(TempCart) 
      return
      end SUBROUTINE GetStoredRotMatrix
      end MODULE mod_rotation    
