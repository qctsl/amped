      subroutine OC
!***********************************************************************
!     Date last modified: January 8, 1999                 Version 1.0  *
!     Author: R. A. Poirier                                            *
!     Description: Optimally conditioned (OC) method, with gradients.  *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE OPT_defaults
      USE OPT_objects
      USE mod_type_hessian
      USE OPT_OC
      USE matrix_print

      implicit none
!
! Local scalars:
      integer I,ifLAG,J
      double precision CONDNO
!
! Local function:
      double precision OPT_gradient_length
!
! Local parameters:
      double precision FKBND,TEN
      parameter (FKBND=1.7D38,TEN=10.0D0)
!
! Begin:
      call PRG_manager ('enter', 'OC', 'UTILITY')
!
!  Check input parameters.
      LLowerBoundReset=.false.
      if(ISTEP.NE.0.and.ISTEP.LT.3)then ! converged ?
        FVAL0=OPT_function%value
        GRDLTH=OPT_gradient_length (PARGRD, Noptpr)
      end if

      OC_grads(1:Noptpr)=PARGRD(1:Noptpr)
!
! Branch depending on ISTEP.
      HMODE=1
      I=ISTEP+1
      GO TO (2000,425,470,130),I

  130 FVALB=TWO*FVAL0-ELBOUND
      FVALA=FVAL0
!
! Step 1C:  Initialize norm of updating vector to zero.
  140 OPTFNS=ZERO
!
! Step 2:  Conduct search.  Begin by printing trace if requested.
  200 FCONVR=ZERO
      do I=1,Noptpr
        FCONVR=FCONVR+FK0OC(I)*FK0OC(I)
      end do ! I=1

      OPTFP0=-FCONVR

      GRDLTH=OPT_gradient_length (OC_grads, Noptpr)

      if(Istep.ne.3)then  ! If not first entry
        PARset(1:Noptpr)=OC_params(1:Noptpr)
        PARgrd(1:Noptpr)=OC_grads(1:Noptpr)
        call OPT_print
        ITRACE(1:ITRAMX)=' '
        Ntrace=0
      end if
!
      OPTITR=OPTITR+1
!
! Step 2A:  Main convergence test.
      if(FCONVR.LE.OPTEPS)GO TO 420
!
! Step 2B:  Compute Euclidean step, search direction and directional derivative.
      do J=1,Noptpr
        SOC(J)=-FK0OC(J)
      end do ! J=1

      call LTMVEC (IHessian_Factored, Noptpr, Noptpr, SOC, VOC)
!
! Step 2C:  Compute initial step along V.
      if((FVAL0-ELBOUND).LT.ZERO)then
        call CHFMIN (OPT_function%value, GRDLTH, Noptpr, ELBOUND)
        FVALB=FVAL0+FVAL0-ELBOUND
        write(UNIout,1050)ELBOUND
        LLowerBoundReset=.true.
      end if

      FCONVR=DMIN1(-TWO*(FVAL0-ELBOUND)/OPTFP0,-TWO*(FVALB-FVAL0)/OPTFP0)
      if(FCONVR.GE.ONE)then
        if(Ntrace.LT.ITRAMX)then
          Ntrace=Ntrace+1
          ITRACE(Ntrace)='U'
        end if
        FCONVR=ONE
      end if

      FVALB=FVALA
      FVALA=FVAL0
      ICONV=0
      ISTEP=0
!
! Step 2D:  Call line search routine.
 2000 continue
      call DSRCH (IHessian_Factored, PARSET, PARGRD, FKOC, FK0OC, FMOC, SOC, VOC, OC_grads, &
                  OC_params, OPTFP0, FCONVR, OPTB0, ICONV, ITRACE, Noptpr, &
                  OPT_function%value, FVAL0, FMINUS, FVALS, OPTEPS, OPTFL0, OPTFL, Ntrace)
      if(ICONV.EQ.0)GO TO 300
      if(ICONV.EQ.1)then
          GO TO 900
      end if
!
! Step 2E:  Restart, unless three successive restarts.
      OPTNSR=OPTNSR+1
      if(OPTNSR.GE.3)then
        write(UNIout,'(2a)')'ERROR> OC: 3 SUCCESSIVE OPT_function%value REDUCTIONS LESS THAN', &
        ' OPTEPS - OPTIMIZATION TERMINATED'
        OPTEND=.TRUE.
        OPTNSR=0
        if(GRDLTH.LE.TEN*OPTACC)then
          write(UNIout,'(2A/17X,A)')'%MUNgauss WARNING> GRADIENT LENGTH IS CONVERGED TO WITHIN', &
          ' TEN*ACCURACY ONLY > THE CALCULATION HAS BEEN ALLOWED TO continue'
          OPTEND=.TRUE.
        end if
        GO TO 430
      end if
!
! Do a restart
      OPTRST=OPTRST+1
      if(OPTRST.GT.OPTMXR)GO TO 450
      do I=1,Noptpr
        WORK(I)=ONE
        if(HMODE.EQ.1)WORK(I)=DIAGFC(I)
      end do ! I=1
      call SETLT8 (IHessian_Factored, Noptpr, Noptpr, WORK)
      GO TO 130
!
! Step 3A:  Check if update is necessary.
  300 continue
      OPTFMS=ZERO
      OPTNSR=0
      do I=1,Noptpr
        OPTFMS=OPTFMS+FMOC(I)*FMOC(I)
      end do ! I=1
      if(OPTFMS.LT.OPTEPS)GO TO 140
!
! Step 3B:  Form vectors P and Q.
      call FORMPQ (FK0OC, FMOC, FNOC, POC, QOC, SOC, ITRACE, ITRAMX, OPTFNS, FCONVR, &
                   Ntrace, OPTEPS, OPTFMS, OPTB0, Noptpr)
!
! Step 3C:  Update IHessian_Factored and other quantities.
      call LTMVEC (IHessian_Factored, Noptpr, Noptpr, QOC, PARSET)
      ISW=1
      if(OPTFNS.GT.ZERO)ISW=2
      call LTOLSP (Noptpr, Noptpr, IHessian_Factored, POC, FK0OC, FNOC, ISW, WORK)
!
! Compute last column.
      do I=1,Noptpr
         WORK(I)=WORK(I)+PARSET(I)*POC(Noptpr)
      end do ! I=1
      call LSPTOL (Noptpr, Noptpr, IHessian_Factored, FK0OC, FNOC, FMOC, ISW, WORK)
      call MODCON (Noptpr, Noptpr, IHessian_Factored, FKBND, ITRACE, ITRAMX, CONDNO, ifLAG, Ntrace)
      if(ifLAG.NE.1)GO TO 200
! Diagonal elements of IHessian_Factored too small.
      write(UNIout,'(a)')'ERROR> OC: DIAGONAL ELEMENTS OF INVERSE HESSIAN TOO SMALL - OPTIMIZATION TERMINATED'
      OPTEND=.TRUE.
      GO TO 430
!
! Function values converged within OPTEPS - Reset to best parameters.
  420 ISTEP=1
      if(OPT_function%value.NE.FVAL0)GO TO 510
  425 write(UNIout,1150)
      OPTEND=.TRUE.
      OPT_converged=.true.
!
  430 continue
!
! Copy parameters.
      PARSET(1:Noptpr)=OC_params(1:Noptpr)
      PARGRD(1:Noptpr)=OC_grads(1:Noptpr)

      ISTEP=3
      GO TO 900
!
! Too many restarts.
  450 write(UNIout,'(A)')'ERROR> OC: TOO MANY Restarts - OPTIMIZATION TERMINATED'
      OPTEND=.TRUE.
      GO TO 430
  470 write(UNIout,1040)
      OPTEND=.TRUE.
      OPT_converged=.true.
      GO TO 430
!
! Reset to best parameters.
  510 continue
      write(UNIout,1010)
      PARSET(1:Noptpr)=OC_params(1:Noptpr)
      PARGRD(1:Noptpr)=OC_grads(1:Noptpr)
!
  900 continue
!
! End of routine OC
      call PRG_manager ('exit', 'OC', 'UTILITY')
      return
!
 1010 FORMAT('RESET TO BEST parameters')
 1040 FORMAT('*** Parameter gradient length converged within OPTACC')
 1050 FORMAT('*** Function value BELOW LOWER BOUND - LOWER BOUND SET TO',F18.9)
 1150 FORMAT('*** Function value CONVERGED WITHIN OPTEPS')
      END
      subroutine LTTRVC (FL,     & ! array in linear format
                         M,      & ! # columns of array
                         N,      & ! # rows of array (dimension of vector)
                         VEC,    & ! Vector
                         RES)   ! (Array)'*(Vector)
!***********************************************************************
!     Date last modified: June 25, 1992                   Version 1.0  *
!     Author:                                                          *
!     Description: Multiply the transpose of array FL into the vector  *
!                  VEC and return the result in RES.                   *
!***********************************************************************
! Modules:
      USE program_constants

      implicit none
!
! Input scalars:
      integer M,N
! Input arrays:
      double precision FL(M*(M+1)/2),RES(M),VEC(N)
!
! Local scalars:
      integer I,IPNTR,J
      double precision T
!
! Begin:
      call PRG_manager ('enter', 'LTTRVC', 'UTILITY')
!
      IPNTR=1
      do I=1,M
        T=ZERO
        do J=I,N
          T=T+FL(IPNTR)*VEC(J)
          IPNTR=IPNTR+1
        end do ! J=I
        RES(I)=T
      end do ! I=1
!
! End of routine LTTRVC
      call PRG_manager ('exit', 'LTTRVC', 'UTILITY')
      return
      END
      subroutine LTMVEC (FL,     & ! array in linear format
                         M,      & ! # rows of array
                         N,      & ! # columns of array (dimension of vector)
                         VEC,    & ! Vector
                         RES)   ! (Array)*(Vector)
!***********************************************************************
!     Date last modified: June 25, 1992                   Version 1.0  *
!     Author:                                                          *
!     Description: Multiply array FL into the vector VEC, and return   *
!                  the result in RES.                                  *
!***********************************************************************
! Modules:
      USE program_constants

      implicit none
!
! Input scalars:
      integer M,N
!
! Input arrays:
      double precision FL(M*(M+1)/2),RES(N),VEC(M)
!
! Local scalars:
      integer I,IPTFL,IUB,J
      double precision T
!
! Begin:
      call PRG_manager ('enter', 'LTMVEC', 'UTILITY')
!
      do I=1,N
        T=ZERO
        IPTFL=I
        IUB=MIN0(I,M)
        do J=1,IUB
          T=T+FL(IPTFL)*VEC(J)
          IPTFL=IPTFL+(N-J)
        end do ! J=1
        RES(I)=T
      end do ! I=1
!
! End of routine LTMVEC
      call PRG_manager ('exit', 'LTMVEC', 'UTILITY')
      return
      END
      subroutine DSRCH (IHessian_Factored,  & ! Hessian matrix used in optimization
                        PARSET,  & ! parameter set
                        PARGRD,  & ! parameter gradients
                        FK,      & ! Current Euclidean gradient
                        FK0,     & ! Euclidean gradient at OC_params
                        FM,      & ! S - K'
                        S,       & ! Euclidean search direction
                        V,       & ! parameter search direction
                        OC_grads,      & ! Starting gradients (OC_params)
                        OC_params,      & ! Starting point of search
                        FP0,    & ! Estimate of directional derivative
                        T,       & ! Initial step
                        B0,     & ! Average second derivative along V
                        ifLAG,   & ! Controls linear search
                        ITRACE,  & ! Current trace element
                        Noptpr,  & ! Number of optimizable parameters
                        OPT_function_value,    & ! Total OPT_function_value (upon entry)
                        FVAL0,   & ! Best current OPT_function_value
                        FMINUS, &
                        FVALS, &
                        OPTEPS, &
                        OPTFL0, &
                        OPTFL, &
                        Ntrace)
!*************************************************************************************************
!     Date last modified: June 25, 1992                                             Version 1.0  *
!     Author:                                                                                    *
!     Description:                                                                               *
!     This routine uses function and derivative evaluations to find                              *
!     a point OC_params+IHessian_Factored*S along the line through OC_params in the direction    *
!     of IHessian_Factored*S where                                                               *
!     a) the value F(OC_params+IHessian_Factored*S) of the objective function is less            *
!        than its starting value F(OC_params), and                                               *
!     b) a "reasonable" linear approximation can be made to the                                  *
!        dependence of the Euclidean gradient FK(OC_params+IHessian_Factored*S*T) on T           *
!        near the T for which F(OC_params+IHessian_Factored*S*T) is a minimum.                   *
!                                                                                                *
!     This routine does not seek accurate approximations to the location of a minimum.           *
!     OC_params : On input, contains the starting point of the search.                           *
!          On output, contains the new point developed by the search.                            *
!     S  : On input, contains the Euclidean search direction.                                    *
!          On output, contains the Euclidean step.                                               *
!     V  : On input, contains the parameter search direction.                                    *
!          On output, contains the parameter step.                                               *
!     FVAL0: Always contains the value of the function at the point held in OC_params.           *
!     OC_grads : Always contains the gradient of the function at the point held in OC_params.    *
!     FK0: Always contains the Euclidean gradient of the function at the point held in OC_params.*
!     FP0: Always contains the estimate of the directional derivative                            *
!          at the point held in OC_params along the direction V.                                 *
!     T  : On input, is the initial step.                                                        *
!     B0: On output, gives the average from 0 to 1 of the second                                 *
!          derivative of F(OC_params+V*T) with respect to T (this is also                        *
!          the change in directional derivative corresponding to the                             *
!          current step V).                                                                      *
!     FM : On output, is the difference vector S-K', where K' is an                              *
!          estimate of the rate of change of K=OC_params+V*T wrt T.                              *
!     ifLAG: On input, ifLAG=0 indicates that this is the first call, and ifLAG=1 indicates      *
!            subsequent calls during the same line search.                                       *
!          On output, ifLAG=0 specifies normal termination, and                                  *
!          ifLAG=1 indicates that another function evaluation has                                *
!          been requested.                                                                       *
!          ifLAG=2 specifies that the reduction in function value is                             *
!          not significant.                                                                      *
!     X,G: N-dimensional work vectors.                                                           *
!     FK : M-dimensional work vector.                                                            *
! 30 Jan 1995 - CCP - Fixed up some uninitialized scalars                                        *
!*************************************************************************************************
! Modules:
      USE OPT_defaults
      USE program_constants

      implicit none
!
! Input scalars:
      integer ifLAG,Noptpr,Ntrace
      double precision B0,FP0,OPT_function_value,T,FVAL0
!
! Input arrays:
      double precision FK(Noptpr),FK0(Noptpr),FM(Noptpr),OC_grads(Noptpr), &
                       IHessian_Factored(Noptpr*(Noptpr+1)/2), &
                       PARGRD(Noptpr),PARSET(Noptpr),S(Noptpr),V(Noptpr),OC_params(Noptpr)
      character*1 ITRACE(ITRAMX)
!
! Local scalars:
      integer I,J,M,N
      double precision OPTEPS,OPTFL0,FVALS,FMINUS,OPTFP,OPTFL
!
! Begin:
      call PRG_manager ('enter', 'DSRCH', 'UTILITY')
!
      M=Noptpr
      N=M
      if(ifLAG.EQ.0)then
!
! STEP 1: Initialize.
         OPTFL=OPTFL0
         FMINUS=FVAL0
!
! STEP 2: Develop next iterate.
         do J=1,M
            S(J)=S(J)*T
         end do ! J=1
         do I=1,N
            V(I)=V(I)*T
         end do ! I=1
         FP0=FP0*T
         do I=1,N
            PARSET(I)=OC_params(I)+V(I)
         end do ! I=1
!
! Exit if expected reduction based upon linear approx. < OPTEPS.
         if(-FP0.LT.OPTEPS)GO TO 600
         ifLAG=1
         GO TO 900
      end if ! ifLAG.EQ.0
!
! Re-entry.
  700 continue
      ifLAG=0
      FVALS=OPT_function_value
      if(FVALS.GE.FMINUS)then
         OPTFL=PT5
         T=OPTFL
        if(Ntrace.LT.ITRAMX)then
          Ntrace=Ntrace+1
          ITRACE(Ntrace)='H'
        end if
         do J=1,M
           S(J)=S(J)*T
         end do ! J=1
         do I=1,N
           V(I)=V(I)*T
         end do ! I=1
         FP0=FP0*T
         do I=1,N
           PARSET(I)=OC_params(I)+V(I)
         end do ! I=1
!
! Exit if expected reduction based upon linear approx. < OPTEPS.
         if(-FP0.LT.OPTEPS)GO TO 600
         ifLAG=1
         GO TO 900
      end if ! FVALS.GE.FMINUS
!
! STEP 3:  Compute Euclidean gradient, and develop OPTFP, B0 and FM(see output parameters above).
  300 call LTTRVC (IHessian_Factored, M, N, PARGRD, FK)

      OPTFP=ZERO
      do I=1,M
        OPTFP=OPTFP+FK(I)*S(I)
      end do ! I=1
      B0=OPTFP-FP0
      do J=1,M
        FM(J)=S(J)+FK0(J)-FK(J)
      end do ! J=1
      if(FVALS.GE.FVAL0)then
         FMINUS=FVALS
         OPTFL=PT5
        if(Ntrace.LT.ITRAMX)then
          Ntrace=Ntrace+1
          ITRACE(Ntrace)='B'
        end if
         GO TO 400
      end if
!
! Step 3A:  Further reduction has been obtained.
! Copy PARSET and PARGRD to OC_params and OC_grads.
      do I=1,N
        OC_params(I)=PARSET(I)
        OC_grads(I)=PARGRD(I)
      end do ! I=1
      FVAL0=FVALS
! Copy FK to FK0.
      do I=1,M
        FK0(I)=FK(I)
      end do ! I=1
      FP0=OPTFP
      if(FP0.GE.ZERO)then
        OPTFL=PT5
        do J=1,M
          S(J)=-S(J)
          FM(J)=-FM(J)
        end do ! J=1
        do I=1,N
          V(I)=-V(I)
        end do ! I=1
        FP0=-FP0
        if(Ntrace.LT.ITRAMX)then
          Ntrace=Ntrace+1
          ITRACE(Ntrace)='P'
        end if

      end if
!
! Step 4:  Exit if minimum of quadratic approximation lies within the extrapolation bound.
  400 continue
      if(FP0+B0*OPTFL.GE.ZERO)GO TO 900
      T=OPTFL
        if(Ntrace.LT.ITRAMX)then
          Ntrace=Ntrace+1
          ITRACE(Ntrace)='E'
        end if
         do J=1,M
           S(J)=S(J)*T
         end do ! J=1
         do I=1,N
           V(I)=V(I)*T
         end do ! I=1
         FP0=FP0*T
         do I=1,N
           PARSET(I)=OC_params(I)+V(I)
         end do ! I=1
!
! Exit if expected reduction based upon linear approx. < OPTEPS.
         if(-FP0.LT.OPTEPS)GO TO 600
         ifLAG=1
         GO TO 900
  600 continue
      ifLAG=2
        if(Ntrace.LT.ITRAMX)then
          Ntrace=Ntrace+1
          ITRACE(Ntrace)='T'
        end if
!
  900 continue
!
! End of routine DSRCH
      call PRG_manager ('exit', 'DSRCH', 'UTILITY')
      return
      END
      subroutine SETLT8 (FL,     & ! Array in linear format
                         M,      & ! # rows of array
                         N,      & ! # columns of array
                         DIAGM) ! Diagonal values to be set in array
!***********************************************************************
!     Date last modified: June 25, 1992                   Version 1.0  *
!     Author:                                                          *
!     Description: Set the diagonal elements of FL to the values in    *
!                  DIAGM, and the remainder of FL to zero.             *
!***********************************************************************
! Modules:
      USE program_constants

      implicit none
!
! Input scalars:
      integer M,N
!
! Input arrays:
      double precision DIAGM(M),FL(M*(M+1)/2)
!
! Local scalars:
      integer I,ICOL,IPTFL,LENFL
!
! Begin:
      call PRG_manager ('enter', 'SETLT8', 'UTILITY')
!
      LENFL=M*(M+1)/2

      do I=1,LENFL
        FL(I)=ZERO
      end do ! I=1
!
      IPTFL=1
      do ICOL=1,M
        FL(IPTFL)=ONE/DSQRT(DABS(DIAGM(ICOL)))
        IPTFL=IPTFL+(N-ICOL+1)
      end do ! ICOL=1
!
! End of routine SETLT8
      call PRG_manager ('exit', 'SETLT8', 'UTILITY')
      return
      END
      subroutine FORMPQ (FK0,     & ! Euclidean gradient at current iterate
                         FM,      & ! S - K'
                         FN,      & ! Updating vector
                         P,       & ! first vector for rank 1 update
                         Q,       & ! vector for rank 1 update
                         S,       & ! Euclidean step
                         ITRACE,  & ! Current trace
                         ITRAMX,  & ! Maximum number of trace
                         OPTFNS, &
                         FCONVR, &
                         Ntrace, &
                         OPTEPS, &
                         OPTFMS, &
                         OPTB0, &
                         Noptpr) ! Number of optimizable parameters
!***********************************************************************
!     Date last modified: June 25, 1992                   Version 1.0  *
!     Author:                                                          *
!     Description:                                                     *
!     This routine develops the vectors P and Q that are used to       *
!     update the Jacobian matrix IHessian_Factored, where (IHessian_Factored)(IHessian_Factored)' is    *
!     the approximation to the inverse Hessian.                        *
!     The updated Jacobian matrix is given by (IHessian_Factored)*(I+Q*P').       *
!                                                                      *
!     The calling sequence is as follows:                              *
!     M   : On input, is the dimension of Euclidean space.             *
!     S   : On input, is the Euclidean step.                           *
!     FK0 : On input, is the Euclidean gradient at the current iterate.*
!           On output, contains the updated Euclidean gradient at the  *
!           current iterate.                                           *
!     FN  : On input, is the updating vector, which in the quadratic   *
!           case is orthogonal to proceeding Euclidean steps.          *
!           On output, contains the new updating vector.               *
!     FM  : On input, is the difference vector S-K' where K' is an     *
!           estimate of the rate of change of Euclidean gradient       *
!           along the search direction.                                *
!     OPTFNS: On input, is the Euclidean norm of FN.                   *
!             On output, contains the Euclidean norm of updated FN.    *
!     OPTFMS: On input, is the Euclidean norm of FM.                   *
!     OPTB0 : On input, is the quantity B0 returned by the search      *
!             routine.                                                 *
!     P   : On output, is the first vector used to determine the       *
!           Rank 1 update.                                             *
!     Q   : On output, is the vector used to determine the Rank 1      *
!           update.                                                    *
!                                                                      *
!***********************************************************************
! Modules:
      USE program_constants

      implicit none
!
! Input scalars:
      integer ITRAMX,Noptpr,Ntrace
!
! Input arrays:
      double precision FK0(Noptpr),FM(Noptpr),FN(Noptpr),P(Noptpr), &
                       Q(Noptpr),S(Noptpr)
      character*1 ITRACE(ITRAMX)
!
! Local scalars:
      integer J,M,N
      double precision B,C,DEL,FMU,FNU,GA,TEMP,TEMP1,TEMP2,TEMP3, &
                       TEMP4,OPTEPS,FCONVR,OPTFNS,OPTFMS,OPTB0, &
                       OPTFL
!
! Begin:
      call PRG_manager ('enter', 'FORMPQ', 'UTILITY')
!
! Step 1: Compute initial quantities and check if M and N define a plane.
      N=Noptpr
      M=N
      FNU=ZERO
      do J=1,M
        FNU=FNU+FM(J)*S(J)
      end do ! J=1
      FMU=FNU-OPTFMS
      if(OPTFNS.NE.ZERO)then
        FCONVR=ZERO
        do J=1,M
          FCONVR=FCONVR+FM(J)*FN(J)
        end do ! J=1
        B=FCONVR/OPTFMS
        OPTFNS=OPTFNS-FCONVR*B
        if(OPTFNS.LT.OPTEPS)then
          FCONVR=ZERO
          if(Ntrace.LT.ITRAMX)then
            Ntrace=Ntrace+1
            ITRACE(Ntrace)='N'
          end if
        else
          do J=1,M
            FN(J)=FN(J)-FM(J)*B
          end do ! J=1
          FCONVR=ZERO
          do J=1,M
            FCONVR=FCONVR+FN(J)*S(J)
          end do ! J=1
          FCONVR=FCONVR/OPTFNS
        end if
!
! Step 2:  Check if projected vectors can be used.
         do J=1,M
           FN(J)=FN(J)*FCONVR
         end do ! J=1
         OPTFNS=ZERO
         do J=1,M
           OPTFNS=OPTFNS+FN(J)*FN(J)
         end do ! J=1
         B=FMU*FNU/OPTFMS+OPTFNS
         if(OPTEPS.LE.B)GO TO 400
      end if
!
! Step 3:  Change to full vectors.
  300 TEMP=FNU/OPTFMS
      do J=1,M
        FN(J)=S(J)-FM(J)*TEMP
      end do ! J=1
      OPTFNS=ZERO
      do J=1,M
        OPTFNS=OPTFNS+FN(J)*FN(J)
      end do ! J=1
      B=OPTB0
        if(Ntrace.LT.ITRAMX)then
          Ntrace=Ntrace+1
          ITRACE(Ntrace)='R'
        end if
!
! Step 4:  Set parameters for optimally conditioned update.
  400 if(OPTFMS*OPTFNS.LE.FMU*FNU)then
         if(Ntrace.LT.ITRAMX)then
           Ntrace=Ntrace+1
           ITRACE(Ntrace)='1'
         end if
         DEL=DSQRT(FNU/FMU)
         GA=ZERO
      else
         FCONVR=B-FMU
         C=B+FNU
         DEL=DSQRT(C/FCONVR)
         GA=DSQRT((ONE-(FMU/OPTFMS)*(FNU/OPTFNS))*(B/FCONVR))/B
         if(C.LT.FCONVR)GA=-GA
      end if
!
! Step 5:  Form P, Q and FK0.
  500 OPTFL=FNU+FMU*DEL+OPTFMS*(OPTFNS*GA)
      TEMP1=DEL-OPTFNS*GA
      TEMP2=GA*FNU
      TEMP3=(ONE+OPTFNS*GA)/OPTFL
      TEMP4=GA*FMU/OPTFL
      do J=1,M
        P(J)=FM(J)*TEMP1+FN(J)*TEMP2
        Q(J)=FM(J)*TEMP3-FN(J)*TEMP4
      end do ! J=1
      TEMP1=ZERO
      do J=1,M
        TEMP1=TEMP1+Q(J)*FK0(J)
      end do ! J=1
      do J=1,M
        FK0(J)=FK0(J)+P(J)*TEMP1
      end do ! J=1
      if(OPTFNS.NE.ZERO)then
         TEMP1=OPTFNS*(ONE+GA*FMU*(FNU/OPTFL))
         TEMP2=(ONE+DEL)*FMU*(FNU/OPTFL)
         do J=1,M
            FN(J)=FM(J)*TEMP1-FN(J)*TEMP2
         end do ! J=1
         OPTFNS=ZERO
         do J=1,M
            OPTFNS=OPTFNS+FN(J)*FN(J)
         end do ! J=1
      end if
!
  900 continue
!
! End of routine FORMPQ
      call PRG_manager ('exit', 'FORMPQ', 'UTILITY')
      return
      END
      subroutine LTOLSP (M,      & ! # columns of matrix
                         N,      & ! # rows of matrix
                         FL,     & ! Lower triangular matrix
                         Q,      & ! Vector to be reduced
                         FK,     & ! Vector to be rotated (1)
                         FN,     & ! Vector to be rotated (2) (optional)
                         ISW,    & ! Number of vectors to be rotated (1-2)
                         W)     ! Last column of updated Jacobian
!***********************************************************************
!     Date last modified: June 25, 1992                   Version 1.0  *
!     Author:                                                          *
!     Description:                                                     *
!     Given a lower triangular matrix and a vector Q, this routine     *
!     reduces Q to the multiple of the Mth unit vector which has the   *
!     same form as Q, using a sequence of Givens transformations.      *
!     These are chosen so that when the lower triangular matrix is     *
!     post-multiplied by the same sequence, only the last column of    *
!     the lower triangular matrix fills in.                            *
!     The Givens transformations are not stored.  Instead, the routine *
!     provides the option of applying the sequence of rotations to     *
!     up to two additional vectors passed to it.                       *
!                                                                      *
!     FL : On input, stores the N x M lower triangular Jacobian        *
!          matrix by columns; on output, stores the first M-1 columns  *
!          of the updated Jacobian matrix.                             *
!     Q  : Contains the vector to be reduced to the multiple of the    *
!          Mth unit vector of the same norm.                           *
!     FK,FN : Two M-dimensional vectors which can also be rotated      *
!          using the sequence of Givens transformations generated.     *
!     ISW: Counter giving the number of vectors in the above list      *
!          to be rotated.  E.g., if ISW=1, then FK is to be rotated    *
!          and FN is a dummy vector.                                   *
!     W  : On output, contains the last column of the updated          *
!          Jacobian matrix.                                            *
!                                                                      *
!***********************************************************************
! Modules:
      USE program_constants

      implicit none
!
! Input scalars:
      integer ISW,M,N
!
! Input arrays:
      double precision FK(M),FL(M*(M+1)/2),FN(M),Q(M),W(N)
!
! Local scalars:
      integer I,J,JJ,LENFL,LENGTH,LPNTR,LPT1,LWPT,MM1,NM,NM1
      double precision ALPHA,ALPHA1,BETA,BETA1,DELTA,ENU,ETA,GAMMA, &
                       SAVE1,SAVE2,SIGMA,T
!
! Begin:
      call PRG_manager ('enter', 'LTOLSP', 'UTILITY')
!
! Initialize variables.
      LENFL=M*(M+1)/2
      NM=N-M
      MM1=M-1
      NM1=NM+1
      LPNTR=LENFL-NM1
      if(M.NE.1)then
!
! Initialize pointers into FL.
        LENGTH=2+NM
        LPT1=LENFL-3-2*NM
        LWPT=M-2
!
! Initialize vector W to last column of FL.
        do I=1,MM1
          W(I)=ZERO
        end do ! I=1
      end if
      do I=1,NM1
        W(MM1+I)=FL(LPNTR+I)
      end do ! I=1
      if(M.NE.1)then
!
! Start process.
        do JJ=1,MM1
          J=M-JJ
          ALPHA=Q(J)
          BETA=Q(M)
!
! Develop Givens transformation.
          ETA=DMAX1(DABS(ALPHA),DABS(BETA))
          if(ETA.NE.ZERO)then
            ALPHA1=ALPHA/ETA
            BETA1=BETA/ETA
            DELTA=DSQRT(ALPHA1*ALPHA1+BETA1*BETA1)
            GAMMA=-BETA1/DELTA
            SIGMA=ALPHA1/DELTA
            ENU=ETA*DELTA
!
! Update Q.
            Q(J)=ZERO
            Q(M)=ENU
!
! Update FL.
            do I=1,LENGTH
              SAVE1=FL(LPT1+I)
              SAVE2=W(LWPT+I)
              FL(LPT1+I)=GAMMA*SAVE1+SIGMA*SAVE2
              W(LWPT+I)=SIGMA*SAVE1-GAMMA*SAVE2
            end do ! I=1
!
! Update FN and FK.
            if(ISW.GT.0)then
              SAVE1=FK(J)
              SAVE2=FK(M)
              FK(J)=GAMMA*SAVE1+SIGMA*SAVE2
              FK(M)=SIGMA*SAVE1-GAMMA*SAVE2
              if(ISW.GT.1)then
                SAVE1=FN(J)
                SAVE2=FN(M)
                FN(J)=GAMMA*SAVE1+SIGMA*SAVE2
                FN(M)=SIGMA*SAVE1-GAMMA*SAVE2
              end if
            end if
          end if
! Update pointers and jump back.
          LENGTH=LENGTH+1
          LWPT=LWPT-1
          LPT1=LPT1-LENGTH
        end do ! JJ=1
      else
!
! Special section for M=1, added by Mike Peterson.
        if(Q(1).NE.ZERO)then
          if(Q(1).LT.ZERO)then
            T=-ONE
          else
            T=ONE
          end if
          Q(1)=DABS(Q(1))
          W(1)=T*W(1)
          if(ISW.GT.0)then
            FK(1)=T*FK(1)
            if(ISW.EQ.2)FN(1)=T*FN(1)
          end if
        end if
      end if
!
! End of routine LTOLSP
      call PRG_manager ('exit', 'LTOLSP', 'UTILITY')
      return
      END
      subroutine LSPTOL (M,      & ! # columns of matrix
                         N,      & ! # rows of matrix
                         FL,     & ! Lower triangular matrix
                         FK,     & ! Vector to be rotated (1)
                         FN,     & ! Vector to be rotated (2) (optional)
                         FM,     & ! Vector to be rotated (3) (optional)
                         ISW,    & ! Number of vectors to be rotated
                         W)     ! Last column of Jacobian matrix
!***********************************************************************
!     Date last modified: June 25, 1992                   Version 1.0  *
!     Author:                                                          *
!     Description:                                                     *
!     Given a matrix which is lower triangular except for its last     *
!     column which is full, this routine post-multiplies the matrix    *
!     by a sequence of Givens rotations chosen so that the matrix is   *
!     reduced to a lower triangular matrix.  The rotations are not     *
!     stored.  Instead, the routine provides the option of applying    *
!     the rotations to up to two additional vectors passed to it.      *
!                                                                      *
!     FL : On input, stores the first M-1 columns of the Jacobian      *
!          matrix in column order; on output, stores the updated       *
!          Jacobian matrix, now lower triangular.                      *
!     FK,FN,FM: Three M-dimensional vectors which can also be rotated  *
!          using the sequence of Givens rotations generated.           *
!     ISW: Counter giving the number of vectors in the above list to   *
!          be rotated.  E.g., if ISW=2, then FK and FN are to be       *
!          rotated and FM is a dummy vector.                           *
!     W  : On input, stores the last column of Jacobian matrix.        *
!                                                                      *
!***********************************************************************
! Modules:
      USE program_constants

      implicit none
!
! Input scalars:
      integer ISW,M,N
!
! Input arrays:
      double precision FK(M),FL(M*(M+1)/2),FM(M),FN(M),W(N)
!
! Local scalars:
      integer I,J,LENFL,LENGTH,LPNTR,LPT1,LWPT,MM1,NM,NM1
      double precision ALPHA,ALPHA1,BETA,BETA1,DELTA,ETA,GAMMA,SAVE1,SAVE2,SIGMA,T
!
! Begin:
      call PRG_manager ('enter', 'LSPTOL', 'UTILITY')
!
      LENFL=M*(M+1)/2
! Initialize variables.
      NM=N-M
      MM1=M-1
      NM1=NM+1
      LPNTR=LENFL-NM1
      if(M.NE.1)then
! Initialize pointers.
        LENGTH=N
        LPT1=0
        LWPT=0
! Find transformation.
         do J=1,MM1
           ALPHA=FL(LPT1+1)
           BETA=W(J)
! Compute Givens transformation.
           ETA=DMAX1(DABS(ALPHA),DABS(BETA))
           if(ETA.NE.ZERO)then
             ALPHA1=ALPHA/ETA
             BETA1=BETA/ETA
             DELTA=DSQRT(ALPHA1*ALPHA1+BETA1*BETA1)
             SIGMA=BETA1/DELTA
             GAMMA=ALPHA1/DELTA
             do I=1, LENGTH
               SAVE1=FL(LPT1+I)
               SAVE2=W(LWPT+I)
               FL(LPT1+I)=GAMMA*SAVE1+SIGMA*SAVE2
               W(LWPT+I)=SIGMA*SAVE1-GAMMA*SAVE2
             end do ! I=1
! Update FK, FN and FM.
             if(ISW.GT.0)then
               SAVE1=FK(J)
               SAVE2=FK(M)
               FK(J)=GAMMA*SAVE1+SIGMA*SAVE2
               FK(M)=SIGMA*SAVE1-GAMMA*SAVE2
               if(ISW.GT.1)then
                 SAVE1=FN(J)
                 SAVE2=FN(M)
                 FN(J)=GAMMA*SAVE1+SIGMA*SAVE2
                 FN(M)=SIGMA*SAVE1-GAMMA*SAVE2
                 if(ISW.GT.2)then
                   SAVE1=FM(J)
                   SAVE2=FM(M)
                   FM(J)=GAMMA*SAVE1+SIGMA*SAVE2
                   FM(M)=SIGMA*SAVE1-GAMMA*SAVE2
                 end if
               end if
             end if
           end if
! Update pointers and jump back.
   20    LENGTH=LENGTH-1
         LPT1=LPT1+LENGTH+1
         LWPT=LWPT+1
         end do ! J=1
      end if
!
! Set last column FL suitably.
   35 do I=1,NM1
        FL(LPNTR+I)=W(MM1+I)
      end do ! I=1

      if(M.EQ.1)then
!
! Special section for M=1, added by Mike Peterson.
        if(W(1).NE.ZERO)then
          if(W(1).LT.ZERO)then
            T=-ONE
          else
            T=ONE
          end if
          FL(1)=T*FL(1)
          if(ISW.GT.0)then
            FK(1)=T*FK(1)
            if(ISW.GE.2)FN(1)=T*FN(1)
            if(ISW.EQ.3)FM(1)=T*FM(1)
          end if
        end if
      end if
!
! End of routine LSPTOL
      call PRG_manager ('exit', 'LSPTOL', 'UTILITY')
      return
      END
      subroutine MODCON (M,       & ! # columns of Jacobian
                         N,       & ! # rows of Jacobian
                         FL,      & ! Jacobian
                         FKBND,   & ! Upper bound on cond(J)
                         ITRACE,  & ! Current trace
                         ITRAMX,  & ! Maximum trace
                         CONDNO,  & ! cond(J)
                         ifLAG,   & ! Status flag
                         IPT)    ! Next available trace element
!***********************************************************************
!     Date last modified: June 25, 1992                   Version 1.0  *
!     Author:                                                          *
!     Description:                                                     *
!     This routine modifies the Jacobian matrix if an estimate of its  *
!     condition number exceeds a specified lower bound.                *
!                                                                      *
!     FKBND : On input, an upper bound.  If the estimate of the        *
!             condition number exceeds FKBND, then this routine        *
!             modifies the matrix contained in FL.                     *
!     CONDNO: On output, contains the square of the largest to the     *
!             smallest diagonal element of the Jacobian matrix on      *
!             entry to this routine.  This is an estimate of the       *
!             condition number of the inverse Hessian approximation.   *
!     ifLAG : On output, ifLAG=0 means FL unmodified.                  *
!             ifLAG=1 - largest element on diagonal is .LT. 1.0D-35.   *
!             ifLAG=2 - matrix modified.                               *
!                                                                      *
!***********************************************************************
! Modules:
      USE program_constants

      implicit none
!
! Input scalars:
      integer ifLAG,IPT,ITRAMX,M,N
      double precision CONDNO,FKBND
!
! Input arrays:
      double precision FL(M*(M+1)/2)
      character*1 ITRACE(ITRAMX)
!
! Local scalars:
      integer ILB,IUB,J,NUM
      double precision BIG,OVERFL,OVFL2,SMALL,TEMP,TEMP1,TEMP2,TEMP3,TEN38,UNDFL,W
      logical LFLAG
!
      parameter (TEN38=1.7D38)
!
! Begin:
      call PRG_manager ('enter', 'MODCON', 'UTILITY')
!
! Machine-dependent program_constants.
      OVERFL=1.0D35
      UNDFL=1.0D-35
      OVFL2=TEN38
      LFLAG=.FALSE.
!
! Step 0: Initialize.
      ifLAG=0
!
! Step 1: Find largest and smallest elements of FL.
      BIG=DABS(FL(1))
      SMALL=BIG
      if(M.NE.1)then
        ILB=N+1
        do J=2,M
          TEMP=DABS(FL(ILB))
          if(BIG.LT.TEMP)BIG=TEMP
          if(SMALL.GT.TEMP)SMALL=TEMP
          ILB=ILB+(N-J+1)
        end do ! J=2
      end if
      if(BIG.LE.UNDFL)then
        ifLAG=1
      else
        if(SMALL.LT.ONE.and.BIG.GE.SMALL*OVERFL)then
        if(IPT.LE.ITRAMX)then
          ITRACE(IPT)='X'
          IPT=IPT+1
        end if
          CONDNO=OVFL2
!
! Step 2: Estimate condition number and return it below bound.
        else
          CONDNO=(BIG/SMALL)**2
          if(FKBND.GE.CONDNO)LFLAG=.TRUE.
        end if
!
! Step 3: Modify condition number.
        if(.not.LFLAG)then
          if(IPT.LE.ITRAMX)then
            ITRACE(IPT)='C'
            IPT=IPT+1
          end if
          ifLAG=2
          W=DLOG(FKBND)/DLOG(CONDNO)
          ILB=1
          do J=1,M
            NUM=-1
            if(FL(ILB).GE.ZERO)NUM=1
            TEMP1=DABS(FL(ILB))
            if(BIG.GT.TEMP1*OVERFL)TEMP1=BIG*UNDFL
            TEMP2=TEMP1**W
            FL(ILB)=NUM*TEMP2
            TEMP3=TEMP2/TEMP1
            IUB=ILB+(N-J)
            ILB=ILB+1
            do while (ILB.LE.IUB)
              FL(ILB)=FL(ILB)*TEMP3
              ILB=ILB+1
            end do
          end do
        end if ! .not.LFLAG
      end if !BIG.LE.UNDFL
!
! End of routine MODCON
      call PRG_manager ('exit', 'MODCON', 'UTILITY')
      return
      END
      subroutine CHFMIN (OPT_function_value,   & ! Total OPT_function_value
                         GRDLTH, & ! Normalized gradient length
                         Noptpr, & ! Number of optimization parameters
                         FMin)  ! Lower bound
!***********************************************************************
!     Date last modified: April 13, 2018                               *
!     Author: . A. Poirier                                             *
!     Description: Checks if OPT_function_value is lower than lower    *
!     bound, resets lower bound if necessary.                          *
!***********************************************************************
! Modules:
      USE program_constants

      implicit none
!
! Input scalars:
      integer :: Noptpr
      double precision :: OPT_function_value,GRDLTH,FMin

! Local scalars:
      double precision :: GL3,C0FMin,C1FMin,MaxChange
!
      parameter (C0FMin=0.0073D0,C1FMin=0.144386D0,MaxChange=0.025D0)
!
! Begin:
      call PRG_manager ('enter', 'CHFMin', 'UTILITY')
!
      GL3=DABS(6.0D0+log(GRDLTH))
      if(FMin.GE.OPT_function_value)then
         FMin=C0FMin*GL3
! Do not allow FMin to be lower than the current by more than 0.50 hartrees
         FMin=min(FMin, MaxChange)
         FMin=OPT_function_value-FMin
      end if
!
! End of routine CHFMIN
      call PRG_manager ('exit', 'CHFMIN', 'UTILITY')
      return
      END
      subroutine SET_defaults_OC
!***********************************************************************
!     Date last modified: January 10, 2000                 Version 1.0 *
!     Description:                                                     *
!***********************************************************************
! Modules:
      USE program_constants
      USE OPT_objects
      USE OPT_OC

      implicit none
!
! Local scalars:
      integer I
!
! Begin:
      call PRG_manager ('enter', 'SET_defaults_OC', 'UTILITY')
!
! Initialize and set defaults if necessary.
      OPTNSR=0 ! number of successive restarts
      OPTRST=0 ! total restart
      OPTMXR=1000000 ! maximum total restart
      ISTEP=3 ! Set step flag to 3
      ICONV=0 ! control trace
      ISW=1   ! givens rotate
      OPTFP0=0.0D0 ! Estimate of directional derivative
      OPTB0=0.0D0  ! Estimate of directional Hessian
      OPTEPS=2.50D-08 ! energy convergence accuracy (predicted change)
      OPTFL0=10.0D0 ! extrapolation factor
      OPTFL=10.0D0 ! extrapolation factor current
      OPTFNS=0.0D0 ! update vector norm
      OPTFMS=0.0D0 ! diff s kprime norm
      FCONVR=0.0D0 ! energy convergence
      FVALA=0.0D0 ! energy A
      FVALB=0.0D0 ! energy B
      FMINUS=0.0D0 ! energy minus
      FVALS=0.0D0 ! energy search
      ELBOUND=0.0D0 ! estimate of energy lower bound
!
! End of routine SET_defaults_OC
      call PRG_manager ('exit', 'SET_defaults_OC', 'UTILITY')
      return
      END
      subroutine INI_OPT_OC
!***********************************************************************************
!     Date last modified:  May 9, 2001                                             *
!     Author:  R. A. Poirier                                                       *
!     Description:  Initialize OC optimization                                     *
!***********************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE OPT_defaults
      USE mod_type_hessian
      USE OPT_objects
      USE OPT_OC

      implicit none
!
! Begin:
      call PRG_manager ('enter', 'INI_OPT_OC', 'UTILITY')
!
!      call SET_defaults_OC
      call INI_OPT_DIIS                   ! for possible switch
!
! Allocate work arrays:
      allocate (POC(1:Noptpr), QOC(1:Noptpr), SOC(1:Noptpr), VOC(1:Noptpr), WORK(1:Noptpr))
      allocate (FKOC(1:Noptpr), FMOC(1:Noptpr), FNOC(1:Noptpr), FK0OC(1:Noptpr))
      allocate (OC_grads(1:Noptpr), OC_params(1:Noptpr))
      OC_params(1:Noptpr)=PARSET(1:Noptpr)
      FKOC(1:Noptpr)=ZERO
      FK0OC(1:Noptpr)=ZERO
      FMOC(1:Noptpr)=ZERO
      FNOC(1:Noptpr)=ZERO
      POC(1:Noptpr)=ZERO
      QOC(1:Noptpr)=ZERO
      SOC(1:Noptpr)=ZERO
      VOC(1:Noptpr)=ZERO
!
! Allow restarts until maximum iterations exceeded.
      HMODE=1
      FVALI=OPT_function%value
      FVAL0=FVALI

! Calculate a lower bound if not provided by the user.
      call CHFMIN (OPT_function%value, GRDLTH, Noptpr, ELBOUND)
!
      write(UNIout,1210)HMODE,ELBOUND  ! deleted OPTTYP not used?
 1210 FORMAT('OCMODE =',I4,' LOWER BOUND =',F17.9)
      write(UNIout,1230)OPTEPS,OPTFL0
 1230 FORMAT('OPTEPS =',E13.5,5X,'EXTRAPOLATION FACTOR =',0PF9.5)
!
      call LTTRVC (IHessian_Factored, Noptpr, Noptpr, PARGRD, FK0OC)
!
! end of routine
      call PRG_manager ('exit', 'INI_OPT_OC', 'UTILITY')
      return
      end
      subroutine END_OPT_OC
!***********************************************************************************
!     Date last modified:  May 9, 2001                                             *
!     Author:  R. A. Poirier                                                       *
!     Description:  Initialize OC optimization                                     *
!***********************************************************************************
! Modules:
      USE OPT_OC

      implicit none
!
! Begin:
      call PRG_manager ('enter', 'END_OPT_OC', 'UTILITY')
!
! Deallocate work arrays:
      deallocate (FKOC, FK0OC, FMOC, FNOC, POC, QOC, SOC, VOC, WORK)
      deallocate (OC_grads, OC_params) ! a OC work array
!
! end of routine
      call PRG_manager ('exit', 'END_OPT_OC', 'UTILITY')
      return
      end
