      module OPT_DOC
!***********************************************************************
!     Date last modified: January 8, 1999                  Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: scalars used by DOC                                 *
!***********************************************************************
      implicit none
!
! Scalars:
      integer :: LCOL      ! hessian column for parameter update
      integer :: ZVALUE
      double precision :: RVALUE
      double precision :: TST          ! initial trace step
      double precision :: S1     ! final trace step
      double precision :: GRSTEP ! gradient numerical step length
      double precision :: OPTFP ! trace hessian estimate current
      double precision :: BDOC ! trace hessian
!
      end module OPT_DOC
