      subroutine CPXYZ_TO_OPT
!***********************************************************************
!     Date last modified: January 27, 1998                Version 2.0  *
!     Author: R.A. Poirier                                             *
!     Description: Set parameters for optimization.                    *
!***********************************************************************
! Modules:
      USE program_files
      USE type_basis_set
      USE OPT_defaults
      USE OPT_objects

      implicit none
!
! Local scalars:
!
! Begin:
      call PRG_manager ('enter', 'CPXYZ_TO_OPT', 'UTILITY')
!
! First determine Noptpr
      Noptpr=3
      if(.not.allocated(PARset))then
        allocate (OPT_par(Noptpr))
        allocate (PARset(Noptpr))
      else
        allocate (OPT_par(Noptpr))
        deallocate (PARset)
        allocate (PARset(Noptpr))
      end if

! Copy X,Y,Z to PARSET

!
! End of routine CPXYZ_TO_OPT
      call PRG_manager ('exit', 'CPXYZ_TO_OPT', 'UTILITY')
      return
      end
      subroutine OPT_TO_CPXYZ
!***********************************************************************
!     Date last modified: January 28, 1998                 Version 2.0 *
!     Author: R. A. Poirier                                            *
!     Description: Update the Cartesian coordinates.                   *
!***********************************************************************
! Modules:
      USE type_basis_set
      USE OPT_objects

      implicit none
!
! Local scalars:
!
! Begin:
      call PRG_manager ('enter', 'OPT_TO_CPXYZ', 'UTILITY')
!
! Copy PARSET to critic point (X,Y,Z)
!
! End of routine OPT_TO_CPXYZ
      call PRG_manager ('exit', 'OPT_TO_CPXYZ', 'UTILITY')
      return
      end
