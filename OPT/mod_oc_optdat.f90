      module OPT_OC
!***********************************************************************
!     Date last modified: January 8, 1999                  Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: scalars used by OC, DOC and BFGS                    *
!***********************************************************************
      implicit none
!
      integer :: OPTNSR ! number of successive restarts
      integer :: OPTRST ! total restart
      integer :: OPTMXR ! maximum total restart
      integer :: HMODE ! hessian mode
      integer :: ISTEP   ! control
      integer :: ICONV ! control trace
      integer :: ISW   ! givens rotate
      double precision :: OPTFP0 ! Estimate of directional derivative
      double precision :: OPTB0  ! Estimate of directional Hessian
      double precision :: OPTEPS       ! energy convergence accuracy (predicted change)
      double precision :: OPTFL0 ! extrapolation factor
      double precision :: OPTFL ! extrapolation factor current
      double precision :: OPTFNS ! update vector norm
      double precision :: OPTFMS ! diff s kprime norm
      double precision :: FCONVR ! energy convergence
      double precision :: FVALI ! energy initial
      double precision :: FVALA ! energy A
      double precision :: FVALB ! energy B
      double precision :: FMINUS ! energy minus
      double precision :: FVALS ! energy search
      double precision :: ELBOUND ! estimate of energy lower bound
!
! Noptpr
      double precision, dimension(:), allocatable :: FKOC
      double precision, dimension(:), allocatable :: FK0OC
      double precision, dimension(:), allocatable :: FMOC
      double precision, dimension(:), allocatable :: FNOC
      double precision, dimension(:), allocatable :: POC
      double precision, dimension(:), allocatable :: QOC
      double precision, dimension(:), allocatable :: SOC
      double precision, dimension(:), allocatable :: VOC
      double precision, dimension(:), allocatable :: WORK
      double precision, dimension(:), allocatable :: OC_params
      double precision, dimension(:), allocatable :: OC_grads
!
      end module OPT_OC
