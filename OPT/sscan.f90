      subroutine SURFACE_SCAN
!***********************************************************************
!     Date last modified: June 26 ,2001                   Version 1.0  *
!     Author: R. A. Poirier                                            *
!     Description: Perform a surface scan on a set of parameters       *
!***********************************************************************
! Modules:
      USE program_files
      USE program_objects
      USE OPT_defaults
      USE type_surface_scan
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE OPT_objects

      implicit none
!
! Work arrays
      double precision, dimension(:), allocatable :: RESULTS
      double precision, dimension(:), allocatable :: WORK
!
! Local scalars:
      integer Iparam,IPOINT,NPOINTS,NRESULTS,Tresults,PARAM
      double precision E0TOT
      logical LPRINT
!
! Local functions:
      integer NSSCAN
!
! Begin:
      call PRG_manager ('enter', 'SURFACE_SCAN', 'UTILITY')
!
      call GET_object ('OPT', 'PARAMETERS', OPT_parameters%name)
      call GET_object ('QM', 'ENERGY', Wavefunction)  ! Only RHF for now
!
      Tresults=SSCAN%Nparameters*SSCAN%Npoints_total
      allocate (RESULTS(1:Tresults), WORK(1:Noptpr))

      E0tot=ENERGY%total
      WORK(1:Noptpr)=PARSET(1:Noptpr) ! copy parameters to work array
!
      NRESULTS=0
      do Iparam=1,SSCAN%Nparameters
      NPOINTS=SSCAN%points(Iparam)

      IF(NPOINTS.GT.0)then
      PARAM=SSCAN%parameter(Iparam)
      PARSET(PARAM)=SSCAN%from(Iparam)
      do IPOINT=1,NPOINTS ! Surface scan for parameter Iparam
      PARAM=SSCAN%parameter(Iparam)
      PARSET(PARAM)=PARSET(PARAM)+SSCAN%by(Iparam)
!
        CALL OPT_TO_COOR
        call RESET_objects  !  function, gradients ... must be recalculated
        call GET_object ('QM', 'ENERGY', Wavefunction)  ! Only RHF for now
        NRESULTS=NRESULTS+1
        if(NRESULTS.gt.Tresults)then
          write(UNIout,'(a)')'ERROR> SURFACE_SCAN: Number of points exceeds array size'
          call PRG_stop ('Number of points exceeds array size, this should NOT be possible!')
        end if
        RESULTS(NRESULTS)=ENERGY%total ! save the result

      end do ! IPOINT
      end if ! NPOINTS

      PARSET(1:Noptpr)=WORK(1:Noptpr) ! reset parameters

      end do ! Iparam
!
! Reset points
      CALL SSCAN_PRT (RESULTS, WORK, E0TOT, NRESULTS)
!
      deallocate (RESULTS, WORK)
!
! End of routine SURFACE_SCAN
      call PRG_manager ('exit', 'SURFACE_SCAN', 'UTILITY')
      return
      END
      subroutine SSCAN_PRT (RESULTS, &
                            WORK, &
                            E0TOT, &
                            NRESULTS)
!***********************************************************************
!     Date last modified: June 26 ,2001                   Version 1.0  *
!     Author: R. A. Poirier                                            *
!     Description: Print the results of the surface scan               *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE OPT_defaults
      USE type_molecule
      USE type_surface_scan
      USE OPT_objects

      implicit none
!
! Input scalars:
      integer NRESULTS
      double precision E0TOT
!
! Input arrays:
      double precision RESULTS(NRESULTS),WORK(Noptpr)
!
! Local scalars:
      integer Iparam,IPOINT,IRESULT,NPOINTS,PARAM
      double precision VAL,ECHANGE
!
! Begin:
      call PRG_manager ('enter', 'SSCAN_PRT', 'UTILITY')
!
      IRESULT=0
      write(UNIout,*)'Results of Surface Scan:'
      write(UNIout,*)'Energies in kJ/mol are relative to the initial structure'

      do Iparam=1,SSCAN%Nparameters
        PARAM=SSCAN%parameter(Iparam)
        PARSET(1:Noptpr)=WORK(1:Noptpr)
        VAL=PARSET(PARAM)
        write(UNIout,'(a,i5)')'For parameter = ',SSCAN%parameter(Iparam)
        write(UNIout,'(2x,a,5x,2a)')'Parameter ','Energy(Hartrees) ','Energy(kJ/mol)'
        NPOINTS=SSCAN%points(Iparam)
        IF(NPOINTS.GT.0)then
          do IPOINT=1,NPOINTS
            IRESULT=IRESULT+1
            VAL=VAL+SSCAN%by(Iparam)
            ECHANGE=(RESULTS(IRESULT)-E0TOT)*hartree_to_kJ
            write(UNIout,'(F10.4,1x,F20.6,3x,F8.2)')VAL,RESULTS(IRESULT),ECHANGE
          end do ! IPOINT
        end if ! NPOINTS
      end do ! Iparam
!
! End of routine SSCAN_PRT
      call PRG_manager ('exit', 'SSCAN_PRT', 'UTILITY')
      return
      END
