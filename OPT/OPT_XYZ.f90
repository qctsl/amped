      subroutine XYZ_TO_OPT
!***********************************************************************
!     Date last modified: January 27, 1998                Version 2.0  *
!     Author: R.A. Poirier                                             *
!     Description: Set Cartesian coordinate parameters and             *
!                  gradients for optimization.                         *
!***********************************************************************
! Modules:
      USE type_molecule
      USE OPT_objects

      implicit none
!
! Local scalars:
      integer Iatom,Ioptpr,N1
!
! Begin:
      call PRG_manager ('enter', 'XYZ_TO_OPT', 'UTILITY')
!
      NICOOR=0
! First determine Noptpr
      Noptpr=0
      do Iatom=1,Natoms
        IF(CARTESIAN(Iatom)%Atomic_number.GT.0)then
          Noptpr=Noptpr+3
        end if
      end do ! Iatom
!
      if(.not.allocated(PARSET))then
        allocate(OPT_par(Noptpr))
        allocate(PARSET(Noptpr))
        allocate(PAR_type(Noptpr))
      end if
!
      N1=1
      do Iatom=1,Natoms
        IF(CARTESIAN(Iatom)%Atomic_number.GT.0)then
          OPT_par(N1)%value=CARTESIAN(Iatom)%X
          OPT_par(N1)%type='X'
          OPT_par(N1)%opt=.true.
          OPT_par(N1+1)%value=CARTESIAN(Iatom)%Y
          OPT_par(N1+1)%type='Y'
          OPT_par(N1+1)%opt=.true.
          OPT_par(N1+2)%value=CARTESIAN(Iatom)%Z
          OPT_par(N1+2)%type='Z'
          OPT_par(N1+2)%opt=.true.
          PARSET(N1)=CARTESIAN(Iatom)%X
          PARSET(N1+1)=CARTESIAN(Iatom)%Y
          PARSET(N1+2)=CARTESIAN(Iatom)%Z
          N1=N1+3
        end if
      end do ! Iatom

      do Ioptpr=1,Noptpr
        PAR_type(Ioptpr)=.true.
      end do ! Ioptpr
!
! End of routine XYZ_TO_OPT
      call PRG_manager ('exit', 'XYZ_TO_OPT', 'UTILITY')
      return
      END
      subroutine OPT_TO_XYZ
!***********************************************************************
!     Date last modified: January 28, 1998                 Version 2.0 *
!     Author: R. A. Poirier                                            *
!     Description: Update the Cartesian coordinates.                   *
!***********************************************************************
! Modules:
      USE type_molecule
      USE OPT_objects

      implicit none
!
! Local scalars:
      integer Iatom,K
!
! Begin:
      call PRG_manager ('enter', 'OPT_TO_XYZ', 'UTILITY')
!
      K=1
      do Iatom=1,Natoms
        IF(CARTESIAN(Iatom)%Atomic_number.GT.0)then
          CARTESIAN(Iatom)%X=OPT_par(K)%value
          CARTESIAN(Iatom)%Y=OPT_par(K+1)%value
          CARTESIAN(Iatom)%Z=OPT_par(K+2)%value
          CARTESIAN(Iatom)%X=PARSET(K)
          CARTESIAN(Iatom)%Y=PARSET(K+1)
          CARTESIAN(Iatom)%Z=PARSET(K+2)
          K=K+3
        end if
      end do ! Iatom
!      end if
!
! End of routine OPT_TO_XYZ
      call PRG_manager ('exit', 'OPT_TO_XYZ', 'UTILITY')
      return
      END
      subroutine XYZ_GRD_OPT
!***********************************************************************
!     Date last modified: January 27, 1998                Version 2.0  *
!     Author: R.A. Poirier                                             *
!     Description: Set Cartesian coordinate parameters and             *
!                  gradients for optimization.                         *
!***********************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE QM_objects
      USE OPT_objects
      USE gradients

      implicit none
!
! Local scalars:
      integer Iatom,N1,N2
!
! Begin:
      call PRG_manager ('enter', 'XYZ_GRD_OPT', 'UTILITY')
!
      call GET_object ('MOL', 'GRADIENTS', 'XYZ')

      if(.not.allocated(PARGRD))then
        allocate (PARGRD(Noptpr))
      end if

      PARGRD(1:Noptpr)=ZERO
      N1=1
      N2=1
! Ignore dummy atoms
      do Iatom=1,Natoms
        IF(CARTESIAN(Iatom)%Atomic_number.GT.0)then
          OPT_par(N2)%gradient=GRDXYZ(N1)
          OPT_par(N2+1)%gradient=GRDXYZ(N1+1)
          OPT_par(N2+2)%gradient=GRDXYZ(N1+2)
          PARGRD(N2)=GRDXYZ(N1)
          PARGRD(N2+1)=GRDXYZ(N1+1)
          PARGRD(N2+2)=GRDXYZ(N1+2)
          N2=N2+3
        end if
        N1=N1+3
      end do ! Iatom
!
! End of routine XYZ_GRD_OPT
      call PRG_manager ('exit', 'XYZ_GRD_OPT', 'UTILITY')
      return
      END
      subroutine SET_XYZ_STEPS
!***********************************************************************
!     Date last modified: June ,1997                    Version 1.0 *
!     Author: R.A. Poirier                                             *
!     DescRiption:                               *
!                          *
!***********************************************************************
! Modules:
      USE type_molecule
      USE mod_type_hessian
      USE OPT_defaults
      USE OPT_objects

      implicit none
!
! Local scalars:
      integer Ioptpr
      double precision step
!
! Begin:
      call PRG_manager ('enter', 'SET_XYZ_STEPS', 'UTILITY')
!
      do Ioptpr=1,Noptpr
        DSTEP(Ioptpr)=Hstep_xyz
      end do
!
! End of routine SET_XYZ_STEPS
      call PRG_manager ('exit', 'SET_XYZ_STEPS', 'UTILITY')
      return
      END
