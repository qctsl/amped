      module opt_nr
!***********************************************************************
!     Date last modified: May 9, 2001                      Version 2.0 *
!     Author: R.A. Poirier                                             *
!     Description:  Newton-Raphson  scalars, arrays, ...                      *
!***********************************************************************
! Modules
      implicit none
!
! Local arrays:
      double precision, dimension(:), allocatable :: y
      double precision, dimension(:,:), allocatable :: invHessian
!
      end module opt_nr
      subroutine Newton_Raphson
!******************************************************************
!     Date last modified:  Sept. 30, 1999                         *
!     Author:  R. A. Poirier                                      *
!     Description:  Subroutine based on the Newton-Rhaphson method *
!******************************************************************
! Modules:
      USE program_files
      USE matrix_print
      USE OPT_defaults
      USE OPT_objects
      USE opt_nr
      USE mod_type_hessian

      implicit none
!
! Local scalars:
      integer Ioptpr,Joptpr,J,I,IER
      double precision deltaglth,deltaE
      double precision,save :: prevglth,PREVE
!
! Begin:
      call PRG_manager ('enter', 'Newton_Raphson', 'UTILITY')
!
! Print relavent information
      if(OPTITR.gt.0)then
        call OPT_print
      end if
! Increment the iteration number
      OPTITR = OPTITR + 1
!
      invHessian=Hessian_matrix
      call INVERT (invHessian, Noptpr, Noptpr, IER)
      if(IER.NE.0)then
        write(UNIout,*)'ERROR> Newton_Raphson: Invert failed'
        stop 'ERROR> Newton_Raphson: Invert failed'
      end if

      if (OPTITR/=1) then
! Form deltaglth, the difference in gradient length
! between two successive iterations (used to test convergence)
         deltaglth = prevglth - GRDLTH
! Calculate deltax, deltag, deltaE
         deltaE = OPT_function%value - PREVE
      end if

! Save values to form deltax and deltag on next iteration
      PREVPAR(1:Noptpr) = PARSET(1:Noptpr)
      PREVGRD(1:Noptpr) = PARGRD(1:Noptpr)
      PREVE = OPT_function%value

! Find INVHESS*PARGRD for Newton Raphson step,
! ie. solve HESS*y = PARGRD for y
!      call sys_solve (Hessian_matrix, PARGRD, Noptpr, y)
       y=matmul(invHessian, pargrd)
!
! Do a Newton-Raphson step
      PARSET = PARSET - y

! Save the current gradient length for the next iteration
      prevglth = GRDLTH
!
! end of routine
      call PRG_manager ('exit', 'Newton_Raphson', 'UTILITY')
      return
      end
      subroutine INI_OPT_NR
!***********************************************************************************
!     Date last modified:  May 9, 2001                                             *
!     Author:  R. A. Poirier                                                       *
!     Description:  Initialize OC optimization                                     *
!***********************************************************************************
! Modules:
      USE program_constants
      USE OPT_defaults
      USE OPT_objects
      USE opt_nr

      implicit none
!
! Begin:
      call PRG_manager ('enter', 'INI_OPT_NR', 'UTILITY')
!
      allocate (y(Noptpr), invHessian(Noptpr,Noptpr))
!
! end of routine
      call PRG_manager ('exit', 'INI_OPT_NR', 'UTILITY')
      return
      end
      subroutine END_OPT_NR
!***********************************************************************************
!     Date last modified:  May 9, 2001                                             *
!     Author:  R. A. Poirier                                                       *
!     Description:  Initialize OC optimization                                     *
!***********************************************************************************
! Modules:
      USE OPT_defaults
      USE OPT_objects
      USE opt_nr

      implicit none
!
! Begin:
      call PRG_manager ('enter', 'END_OPT_NR', 'UTILITY')
!
      deallocate (y, invHessian)
!
! end of routine
      call PRG_manager ('exit', 'END_OPT_NR', 'UTILITY')
      return
      end
