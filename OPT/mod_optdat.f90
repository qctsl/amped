      module OPT_objects
!***********************************************************************
!     Date last modified: December 18, 1998                Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: Optimization parameters.                            *
!***********************************************************************
      implicit none
!
! See munini for defaults
! NFEVAL: Number of function evaluations
! OPTACC: Gradient length accuracy required for convergence
! MAX_OPTITR: Maximum number of iterations
! OPTITR: Number of iterations (counter)
! OPTEND: .true. if optimization has converged or terminated due to an error
!
      integer :: IFEVAL      ! Number of function evaluations (element), DIIS
      integer :: IFEVALP1    ! IFEVAL+1, DIIS
      integer :: IDIISmax    ! Maximum number of points saved for diis
      integer :: NDIIS_save    ! Number of points saved for diis by previous OPT method
      integer :: DISITM      ! Maximum number of DIIS iterations
      integer :: Ntrace ! Actual number of elements in trace
      integer :: Noptpr
      integer :: NFEVAL    ! Total number of optimization function evaluations
      integer :: OPTITR    ! Optimization iteration number

      double precision :: MIN_fn_val
      double precision :: GRDLTH       ! Gradient length
      double precision :: PREV_grdlth  ! Previous gradient length
      double precision :: MIN_grdlth   ! Minimum (best) gradient length
      double precision :: FVAL0        ! Energy zero

      logical :: OPTend         ! Has optimization terminated (for any reason)
      logical :: OPT_converged  ! Has the optimization converged
      logical :: NEW_iteration ! Is this a new iteration? (controls printing)
      logical :: LLowerBoundReset  ! Used in OC for deaqling with failures
!
! Arrays:
      logical, dimension(:), allocatable :: PAR_type   ! Parameter type (.true. for Stretch, else false)
      double precision, dimension(:), save, allocatable :: PARSET
      double precision, dimension(:), save, allocatable :: PARGRD
      double precision, dimension(:), save, allocatable :: BEST_grads
      double precision, dimension(:), save, allocatable :: BEST_params
      character*1, dimension(:), save, allocatable :: ITRACE ! ITRAMX
!
! Required for TS_BFGS/DIIS
      double precision, dimension(:,:), save, allocatable :: deltaHESS
      double precision, dimension(:), save, allocatable :: PREVPAR, PREVGRD
!
! DIIS arrays
      double precision, dimension(:,:), allocatable :: XDIIS ! Parameters by iteration
      double precision, dimension(:,:), allocatable :: GDIIS ! Gradients by iteration
      double precision, dimension(:), allocatable :: ARRENG ! Array of Energies along optimization
      double precision, dimension(:), allocatable :: ARRGRD ! Array of gradient length along optimization
!
! Domain defines the range of values the parameters are allowed to have (Used by GA)
      type type_domain
        double precision :: high
        double precision :: low
      end type type_domain

      type(type_domain), dimension(:), allocatable :: GA_domain
!
! Optimization information as derived types
      type type_OPT_parameters
        double precision :: value
        double precision :: gradient
        logical :: opt ! T or F
        character(len=16) :: type ! bond, angle, XYZ, scale factor, ...
        character(len=16) :: units      !  Example, bohr
      end type type_OPT_parameters

      type type_function
        character(len=16) :: name       !  Example, energy
        character(len=16) :: units      !  Example, hartree
        character(len=16) :: parameters !  Example, XYZ
        double precision :: value       !  
      end type type_function

      type type_parameters
        character(len=16) :: name       !  Example, XYZ
        character(len=16) :: units      !  Example, bohr
      end type type_parameters

      type type_gradients
        character(len=16) :: method     !  Analytical or Numerical
        character(len=16) :: units      !  Example, hartree/bohr
      end type type_gradients

      type type_hessian
        character(len=16) :: method     !  Analytical or Numerical
        character(len=16) :: units      !  Example, hartree/bohr**2
        character(len=16) :: difference !  Example, Forward (FDIFF), backward (BDIFF), central (CDIFF) difference or none
        logical :: Symmetrized          !  T or F
        logical :: RemoveNegEigs        !  T or F
        integer :: NnegEig              !  0, 1, 2, ...
      end type type_hessian

      type(type_function) :: OPT_function
      type(type_parameters) :: OPT_parameters
      type(type_gradients) :: OPT_gradients
      type(type_hessian) :: OPT_hessian
      type(type_OPT_parameters), dimension(:), allocatable :: OPT_par

      end module OPT_objects
