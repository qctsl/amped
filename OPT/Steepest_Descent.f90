      subroutine OPT_Steepest_Descent
!***********************************************************************************
!     Date last modified:  May 3, 2001                                             *
!     Author:  Sharene Bungay and R. A. Poirier                                    *
!     Description:  Subroutine based on the Newton-Rhapson method using a TS-BFGS  *
!     update                                                                       *
!***********************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE matrix_print
      USE OPT_defaults
      USE OPT_objects

      implicit none
!
! Local scalars:
      integer Ioptpr
      double precision :: Max_step
!
! Begin:
      call PRG_manager ('enter', 'OPT_STEEPEST_DESCENT', 'UTILITY')
!
! Set current parameters and gradients as best (for printing)
      Ntrace=1

      if(OPTITR.GT.0)then
        call OPT_print
      end if
!
! Increment the iteration number
      OPTITR = OPTITR + 1
!
! Save values to form deltax and deltag on next iteration
      PREVPAR(1:Noptpr) = PARSET(1:Noptpr)
      PREVGRD(1:Noptpr) = PARGRD(1:Noptpr)
!
      do Ioptpr=1,Noptpr
        parset(Ioptpr)=parset(Ioptpr)-PARGRD(Ioptpr)
!       write(UNIout,'(a,F20.6)')'Steepest_Descent> Step size set to: ',-pargrd(Ioptpr)
      end do
!
! end of routine
      call PRG_manager ('exit', 'OPT_STEEPEST_DESCENT', 'UTILITY')
      return
      end
