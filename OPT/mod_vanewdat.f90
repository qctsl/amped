      module OPT_VA
!***********************************************************************
!     Date last modified: January 8, 1999                  Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: scalars used by va                                *
!***********************************************************************
      implicit none
!
      integer :: NTEST ! energy decrease test
      integer :: ISTEP ! Control for GOTO
      integer :: NTPAR ! Increase Marquardt parameter
      integer :: NOPTP2  ! 2*Noptpr
      integer :: MaxEincr ! Maximum number of consecutive energy increases
      double precision :: OPTDMX         ! Estimate of x0 - x*
      double precision :: OPTAVA         ! Gradient Accuracy for VA
      double precision :: OPTDSS
      double precision :: OPTDM          ! Square of DMAX
      double precision :: MQPMIN         ! Least value of Marquardt parameter
      double precision :: DPAR           ! 10*OPTDM
      double precision :: FMIN           ! Smallest value of gradient square
      double precision :: DD             ! Current value of gradient square
      double precision :: TINC           ! Step length increase test
      double precision :: MARQPM         ! Marquardt parameter
      double precision :: PPAR           ! Square of Marquardt parameter
      double precision :: FNP            ! Right Hand Side Thingie
      double precision :: DirSQ          ! Direction squared
      double precision :: DTEST          ! Used to test that linear independence is maintained
      double precision :: VAstep_size    !  Default step size
      logical :: LCONV                   ! Final parameters?
!
      double precision, dimension(:), allocatable ::   WC  ! Direction Count
      double precision, dimension(:,:), allocatable :: DirMatrix! Direction Matrix
      double precision, dimension(:), allocatable ::   WW  ! Scratch Vector
      double precision, dimension(:,:), allocatable :: HESSinv  ! Generalized inverse Hessian
      double precision, dimension(:), allocatable ::   WORK1
      double precision, dimension(:), allocatable ::   deltaq
      double precision, dimension(:), allocatable ::   deltag
!
      end module OPT_VA
