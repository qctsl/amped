      subroutine Numerical_First
!***************************************************************************
!     Date last modified:   April 4, 2011                      Version 2.0 *
!     Author: R.A. Poirier                                                 *
!     Description: Compute the first derivative using a central difference *
!***************************************************************************
! Modules:
      USE program_files
      USE type_molecule
      USE OPT_defaults
      USE OPT_objects

      implicit none
!
! Local scalars:
      integer::Ioptpr       
      double precision :: ScaleF,error,Fn_plus,Fn_minus,Step

! Local arrays:
      double precision, dimension(:), allocatable :: PARset0
      double precision, dimension(:), allocatable :: PARset1

! Parameters for NGstep:   NGstep=2*p*SQRT(ABS(error/(1+ABS(OPT_function%value))))
      parameter (error=1.0D-7)
      parameter (ScaleF=30.0)

! Begin:
      call PRG_manager ('enter', 'Numerical_First', 'UTILITY')
!
! Obtain PARset:     
      call GET_object ('OPT', 'PARAMETERS', OPT_parameters%name) ! Defines PARset(Noptpr)
!      
! Allocate arrays:    
      allocate(PARset0(Noptpr))
      if(.not.allocated(PARgrd))allocate(PARgrd(Noptpr))
      
! Save to PARset0:       
      PARset0=PARset

! Define step size "NGstep":      
!     NGstep=ScaleF*SQRT(ABS(error/(1.0D0+ABS(OPT_function%value))))
!     write(UNIout,'(a,f12.6)')'Step size for numerical first derivative: ',NGstep
      write(UNIout,'(a)')'PARgrd:'
!     write(UNIout,'(a,f12.6)')'NGstep: ',NGstep

! Compute first derivative:      
      Step=NGstep
      do Ioptpr=1,Noptpr
!        Step=PARset0(Ioptpr)*NGstep
         PARset(Ioptpr)=PARset0(Ioptpr)+Step            ! Change the (i)th component 
   
         call OPT_TO_COOR
         call OPT_functions                               ! Obtain Fn_plus 
         Fn_plus=OPT_function%value 
       
         PARset(Ioptpr)=PARset0(Ioptpr)-Step            ! Change the (i)th component
       
         call OPT_TO_COOR
         call OPT_functions                               ! Obtain Fn_minus
         Fn_minus=OPT_function%value
      
         PARset(Ioptpr)=PARset0(Ioptpr)                  ! Reset PARset
    
!     write(UNIout,'(a,f12.6)')'Fn_plus: ',Fn_plus
!     write(UNIout,'(a,f12.6)')'Fn_minus: ',Fn_minus
         PARgrd(Ioptpr)=(Fn_plus-Fn_minus)/(2.0D0*Step)      ! Compute the first derivative
      write(UNIout,'(f12.6)')PARgrd(Ioptpr)
      end do 

! Now make sure the corrdinates and function value correspond to PARset0
      call OPT_TO_COOR
      call OPT_functions
        
      deallocate(PARset0)                                     ! Deallocate

! End of routine OPTCLC
      call PRG_manager ('exit', 'Numerical_First', 'UTILITY')
      return
      end
  
