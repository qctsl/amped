      module mod_type_hessian
!***********************************************************************
!     Date last modified: January 8, 1999                 Version 1.0  *
!     Author: R.A. Poirier                                             *
!     Description: scalars used for Hessian calculation                *
!***********************************************************************
! Modules

      implicit none
!
! NFEVALH: Number of function evaluations for Hessian calculation
      integer :: NFEVALH
      integer :: NnegEig ! Number of negative eigenvalues in original hessian
      logical :: LCUBIC ! Cubic update terms (only used by diis)?
      character*8, dimension(:), allocatable :: HESTYP
!
      double precision, dimension(:,:), allocatable :: Hessian_Matrix ! MATSQG_HESSIAN_GUESS
      double precision, dimension(:,:), allocatable :: IHessian_Matrix ! MATSQG_HESSIAN_INVERSE_GUESS
      double precision, dimension(:), allocatable :: IHessian_Factored ! MATLOT_HESSIAN_INVERSE_FACTORED
      double precision, dimension(:), allocatable :: Hessian_Factored ! MATLOT_HESSIAN_FACTORED
      double precision, dimension(:,:), allocatable :: PAR_conversion ! parameter conversion factor
!
      double precision, dimension(:,:), allocatable :: STHIRD ! Estimates of third derivatives
!
! Work arrays
      double precision, dimension(:), allocatable :: DSTEP
      double precision, dimension(:), allocatable :: DIAGFC   ! Diagonal guess of the Hessian
!
      end module mod_type_hessian
