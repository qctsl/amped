      subroutine NRIC_TO_OPT
!*************************************************************************************
! Date last modified: 1 April, 2000                                      Version 1.0 *
! Author: Michelle Shaw                                                              *
! Description: Updates the stretch, bend, torsion, and out-of-plane bend coordinates *
!              for the next optimization step.                                       *
!*************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE redundant_coordinates
      USE OPT_defaults
      USE OPT_objects

      implicit none
!
! Local scalars:
      integer Ntotal
!
! Begin:
      call PRG_manager ('enter', 'NRIC_TO_OPT', 'UTILITY')

      call GET_object ('MOL', 'INTERNAL', 'RIC')
!
! Define Noptpr
      Noptpr=0
      if(RIC%Nbonds.gt.0.and.OPT_coord%bonds)then
        Noptpr=RIC%Nbonds
      end if
      if(RIC%Nangles.gt.0.and.OPT_coord%angles)then
        Noptpr=Noptpr+RIC%Nangles
      end if
      if(RIC%Ntorsions.gt.0.and.OPT_coord%torsions)then
        Noptpr=Noptpr+RIC%Ntorsions
      end if
      if(RIC%Noopbends.gt.0.and.OPT_coord%oopbends)then
        Noptpr=Noptpr+RIC%Noopbends
      end if
      IF(OPT_function%name(1:2).EQ.'MM')then
      if(RIC%Nnonbonded.gt.0.and.OPT_coord%nonbonded)then
        Noptpr=Noptpr+RIC%Nnonbonded
      end if
      end if

      if(.not.allocated(PARSET))then
        allocate (OPT_par(Noptpr))
        allocate (PARSET(Noptpr))
        allocate (GA_domain(Noptpr)) ! highest and lowest allowed values
        allocate (PAR_type(Noptpr))
      end if

      OPT_par(1:Noptpr)%opt=.false.
      PAR_type(1:Noptpr)=.false.
      Ntotal=0
! Parameters are combined here, starting with the stretches, then the bends, and finally the torsions.
      if(RIC%Nbonds.gt.0.and.OPT_coord%bonds)then
        OPT_par(1:RIC%Nbonds)%value=RIC%bond(1:RIC%Nbonds)%value
        OPT_par(1:RIC%Nbonds)%type='bond'
        OPT_par(1:RIC%Nbonds)%opt=.true.
        PARSET(1:RIC%Nbonds)=RIC%bond(1:RIC%Nbonds)%value
        PAR_type(1:RIC%Nbonds)=.true.
        GA_domain(1:RIC%Nbonds)%high=5.0D0
        GA_domain(1:RIC%Nbonds)%low=1.0D0
        Ntotal=RIC%Nbonds
      end if

      if(RIC%Nangles.gt.0.and.OPT_coord%angles)then
        OPT_par(Ntotal+1:Ntotal+RIC%Nangles)%value=RIC%angle(1:RIC%Nangles)%value
        OPT_par(Ntotal+1:Ntotal+RIC%Nangles)%type='angle'
        OPT_par(Ntotal+1:Ntotal+RIC%Nangles)%opt=.true.
        PARSET(Ntotal+1:Ntotal+RIC%Nangles)=RIC%angle(1:RIC%Nangles)%value
        GA_domain(Ntotal+1:Ntotal+RIC%Nangles)%high=3.0D0
        GA_domain(Ntotal+1:Ntotal+RIC%Nangles)%low=0.1D0
        Ntotal=Ntotal+RIC%Nangles
      end if

      if(RIC%Ntorsions.gt.0.and.OPT_coord%torsions)then
        OPT_par(Ntotal+1:Ntotal+RIC%Ntorsions)%value=RIC%torsion(1:RIC%Ntorsions)%value
        OPT_par(Ntotal+1:Ntotal+RIC%Ntorsions)%type='torsion'
        OPT_par(Ntotal+1:Ntotal+RIC%Ntorsions)%opt=.true.
        PARSET(Ntotal+1:Ntotal+RIC%Ntorsions)=RIC%torsion(1:RIC%Ntorsions)%value
        GA_domain(Ntotal+1:Ntotal+RIC%Ntorsions)%high=PI_val
        GA_domain(Ntotal+1:Ntotal+RIC%Ntorsions)%low=-PI_val
        Ntotal=Ntotal+RIC%Ntorsions
      end if

      if(RIC%Noopbends.gt.0.and.OPT_coord%oopbends)then
        OPT_par(Ntotal+1:Ntotal+RIC%Noopbends)%value=RIC%oopbend(1:RIC%Noopbends)%value
        OPT_par(Ntotal+1:Ntotal+RIC%Noopbends)%type='oopbend'
        OPT_par(Ntotal+1:Ntotal+RIC%Noopbends)%opt=.true.
        PARSET(Ntotal+1:Ntotal+RIC%Noopbends)=RIC%oopbend(1:RIC%Noopbends)%value
        GA_domain(Ntotal+1:Ntotal+RIC%Noopbends)%high=3.14D0
        GA_domain(Ntotal+1:Ntotal+RIC%Noopbends)%low=0.0D0
        Ntotal=Ntotal+RIC%Noopbends
      end if

      IF(OPT_function%name(1:2).EQ.'MM')then
      if(RIC%Nnonbonded.gt.0.and.OPT_coord%nonbonded)then
        OPT_par(Ntotal+1:Ntotal+RIC%Nnonbonded)%value=RIC%nonbonded(1:RIC%Nnonbonded)%value
        OPT_par(Ntotal+1:Ntotal+RIC%Nnonbonded)%type='nonbonded'
        OPT_par(Ntotal+1:Ntotal+RIC%Nnonbonded)%opt=.true.
        PARSET(Ntotal+1:Ntotal+RIC%Nnonbonded)=RIC%nonbonded(1:RIC%Nnonbonded)%value
        GA_domain(Ntotal+1:Ntotal+RIC%Nnonbonded)%high=10.0D0
        GA_domain(Ntotal+1:Ntotal+RIC%Nnonbonded)%low=1.0D0
        Ntotal=Ntotal+RIC%Nnonbonded
      end if
      end if
!
      if(Ntotal.ne.Noptpr)then
        write(UNIout,*)'NRIC_TO_OPT> Ntotal .ne. Noptpr'
        write(UNIout,*)'NRIC_TO_OPT> Ntotal: ',Ntotal
        write(UNIout,*)'NRIC_TO_OPT> Noptpr: ',Noptpr
        stop 'NRIC_TO_OPT> Ntotal .ne. Noptpr'
      end if
!
! End of routine NRIC_TO_OPT
      call PRG_manager ('exit', 'NRIC_TO_OPT', 'UTILITY')
      return
      end subroutine NRIC_TO_OPT
      subroutine OPT_TO_NRIC
!*************************************************************************************
! Date last modified: 16 August, 2000                                    Version 1.0 *
! Author: Michelle Shaw                                                              *
! Description: Updates the redundant coordinates for calculation of energies,        *
!              gradients, and second derivatives for the next optimization step.     *
!*************************************************************************************
! Modules:
      USE program_files
      USE type_molecule
      USE redundant_coordinates
      USE OPT_defaults
      USE OPT_objects

      implicit none
!
! Local scalars:
      integer Ntotal
!
! Begin:
      call PRG_manager ('enter', 'OPT_TO_NRIC', 'UTILITY')
!
      Ntotal=0
! Parameters are combined here, starting with the stretches, then the bends, and finally the torsions.
      if(RIC%Nbonds.gt.0.and.OPT_coord%bonds)then
        RIC%bond(1:RIC%Nbonds)%value=OPT_par(1:RIC%Nbonds)%value
        RIC%bond(1:RIC%Nbonds)%value=PARSET(1:RIC%Nbonds)
        Ntotal=RIC%Nbonds
      end if

      if(RIC%Nangles.gt.0.and.OPT_coord%angles)then
        RIC%angle(1:RIC%Nangles)%value=OPT_par(Ntotal+1:Ntotal+RIC%Nangles)%value
        RIC%angle(1:RIC%Nangles)%value=PARSET(Ntotal+1:Ntotal+RIC%Nangles)
        Ntotal=Ntotal+RIC%Nangles
      end if

      if(RIC%Ntorsions.gt.0.and.OPT_coord%torsions)then
        RIC%torsion(1:RIC%Ntorsions)%value=OPT_par(Ntotal+1:Ntotal+RIC%Ntorsions)%value
        RIC%torsion(1:RIC%Ntorsions)%value=PARSET(Ntotal+1:Ntotal+RIC%Ntorsions)
        Ntotal=Ntotal+RIC%Ntorsions
      end if

      if(RIC%Noopbends.gt.0.and.OPT_coord%oopbends)then
        RIC%oopbend(1:RIC%Noopbends)%value=OPT_par(Ntotal+1:Ntotal+RIC%Noopbends)%value
        RIC%oopbend(1:RIC%Noopbends)%value=PARSET(Ntotal+1:Ntotal+RIC%Noopbends)
        Ntotal=Ntotal+RIC%Noopbends
      end if

      IF(OPT_function%name(1:2).EQ.'MM')then
      if(RIC%Nnonbonded.gt.0.and.OPT_coord%nonbonded)then
        RIC%nonbonded(1:RIC%Nnonbonded)%value=OPT_par(Ntotal+1:Ntotal+RIC%Nnonbonded)%value
        RIC%nonbonded(1:RIC%Nnonbonded)%value=PARSET(Ntotal+1:Ntotal+RIC%Nnonbonded)
        Ntotal=Ntotal+RIC%Nnonbonded
      end if
      end if
!
      if(Ntotal.ne.Noptpr)then
        write(UNIout,*)'OPT_TO_NRIC> Ntotal .ne. Noptpr'
        write(UNIout,*)'OPT_TO_NRIC> Ntotal: ',Ntotal
        write(UNIout,*)'OPT_TO_NRIC> Noptpr: ',Noptpr
        stop 'OPT_TO_NRIC> Ntotal .ne. Noptpr'
      end if
!
! End of routine OPT_TO_NRIC
      call PRG_manager ('exit', 'OPT_TO_NRIC', 'UTILITY')
      return
      end subroutine OPT_TO_NRIC
