      subroutine VANEW
!***********************************************************************
!     Date last modified: November  1, 1994                            *
!     Description: Subroutine based on VA05AD minimization of sum of   *
!                  squares technique, for general critical points,     *
!                  with gradients. Hessian evaluation in separate      *
!                  subroutine.                                         *
!***********************************************************************
! Modules:
      USE OPT_defaults
      USE program_constants
      USE matrix_print
      USE OPT_objects
      USE OPT_VA
      USE mod_type_hessian

      implicit none
!
! Local scalars:
      integer I,J,KK,KK2
      double precision FSQ,T,DN,SP,SUMX,SUMF,PRED,DMULT,AP,AD,PTM,ANMULT,SUM,PJ,SS,ST,SPP,DW
!
! Local parameters:
      double precision PT01,PT1,PT25,PT9,SIX,TEN
      parameter (SIX=6.0D0,TEN=10.0D0,PT01=0.01D0,PT1=0.1D0,PT25=0.25D0,PT9=0.9D0)
!
! Begin:
      call PRG_manager ('enter', 'VANEW', 'UTILITY')
!
      if(optitr.gt.0)then
        call OPT_print
      end if
      OPTITR=OPTITR+1 ! Start a new iteration

      Ntrace=0
!
! Start***********************************************************************************************************
      WORK1(1:Noptpr)=PARGRD(1:Noptpr) ! Make sure the gradients are not modified
!
!     Calculate the sum of squares.
      FSQ=ZERO
      do I=1,Noptpr
        FSQ=FSQ+WORK1(I)*WORK1(I)
      end do ! I
!
! DIIS here
      if(FSQ.LE.OPTAVA)then
        write(UNIout,'(a)')'*** GRADIENT (FSQ) CONVERGED TO DESIRED ACCURACY (OPTAVA) ***'
        OPTend=.TRUE.
        OPT_converged=.true.
        FMIN=FSQ
      end if ! FSQ.LE.OPTAVA
!
      if(ISTEP.NE.2.and.ISTEP.NE.4) then
        if(FSQ.GE.FMIN) then
          if(DD.LE.OPTDSS) then
            NTEST=NTEST-1
            if(NTEST.LE.0) then
              write(UNIout,'(a)')'ERROR> VANEW:'
              write(UNIout,'(a)')'GRADIENT LENGTH NO LONGER DECREASES ...'
              write(UNIout,'(a)')'*** OPTIMIZATION TERMINATED ***'
              write(UNIout,'(a)')'THIS MAY BE DUE TO THE VALUES OF VAstep_size AND OPTAVA OR TO LOSS OF RANK IN THE JACOBIAN'
              write(UNIout,'(a)')'ISTEP: ',ISTEP
              write(UNIout,'(a,E12.6)')'FSQ: ',FSQ
              write(UNIout,'(a,E12.6)')'FMIN: ',FMIN
              write(UNIout,'(a,E12.6)')'DD: ',DD
              write(UNIout,'(a,E12.6)')'OPTDSS: ',OPTDSS
              write(UNIout,'(a,i8)')'NTEST: ',NTEST
            end if ! NTEST.LE.0
          end if ! DD.LE.OPTDSS
        else
          NTEST=MaxEincr
        end if ! FSQ.GE.FMIN
      end if ! ISTEP.NE.2.and.ISTEP.NE.4
!
! Check if terminated due to error
      if(OPTend.and..not.OPT_converged)then ! Restore the best values of PARSET and PARGRD.
      do I=1,Noptpr
        WORK1(I)=BEST_grads(I)
        PARSET(I)=BEST_params(I)
      end do ! I
      end if

      if(OPT_converged.or.OPTend)then
!     Print the final force constant matrix estimate.
!      write(UNIout,'(a)')' Hessian in Hartree/bohr**2 '
!      call PRT_matrix (Hessian_matrix, Noptpr, Noptpr)
!
      call PRG_manager ('exit', 'VANEW', 'UTILITY')
      return
      end if ! OPT_converged.or.OPTend
!
!RAP ISTEP is initially 4!!!
      GO TO (49,47,47,64),ISTEP
!
   64 continue
      if(Ntrace.LT.ITRAMX)then
        Ntrace=Ntrace+1
        ITRACE(Ntrace)='4'
      end if
!
! Next predict the descent and Marquardt minima.
      DirSQ=ZERO
      DN=ZERO
      SP=ZERO
      do I=1,Noptpr
        SUMX=ZERO
        SUMF=ZERO
        do J=1,Noptpr
          SUMX=SUMX-Hessian_matrix(I,J)*BEST_grads(J)
          SUMF=SUMF-HESSinv(I,J)*BEST_grads(J)
        end do ! J
        DirSQ=DirSQ+SUMX*SUMX
        DN=DN+SUMF*SUMF
        SP=SP+SUMX*SUMF
        PARSET(I)=SUMX
        WORK1(I)=SUMF
      end do ! I
!
! Predict the reduction in WORK1(X) due to the Marquardt step, and also predict the length of the steepest descent step.
      PRED=SP+SP
      DMULT=ZERO
      do I=1,Noptpr
        AP=ZERO
        AD=ZERO
        do J=1,Noptpr
          AP=AP+Hessian_matrix(J,I)*WORK1(J)
          AD=AD+Hessian_matrix(J,I)*PARSET(J)
        end do ! J
        PRED=PRED-AP*AP
        DMULT=DMULT+AD*AD
      end do ! I
!
! Test for convergence.
      if(DN.LE.OPTDM) then
        AP=DSQRT(DN)
        if(PRED+TWO*PPAR*AP*(OPTDMX-AP).LE.OPTAVA)then
          write(UNIout,'(a)')'*** CONVERGED ON "PRED+TWO*PPAR*AP*(OPTDMX-AP).LE.OPTAVA" ***'
          OPTend=.TRUE.
          OPT_converged=.true.
        end if
      else
        if(PRED+PPAR*(OPTDM-DN).LE.OPTAVA)then
          write(UNIout,'(a)')'*** CONVERGED ON "PRED+PPAR*(OPTDM-DN).LE.OPTAVA" ***'
          write(UNIout,'(a,E12.6)')'PRED: ',PRED
          write(UNIout,'(a,E12.6)')'PPAR: ',PPAR
          write(UNIout,'(a,E12.6)')'OPTDM: ',OPTDM
          write(UNIout,'(a,E12.6)')'DN: ',DN
          write(UNIout,'(a,E12.6)')'OPTAVA: ',OPTAVA
          OPTend=.TRUE.
          OPT_converged=.true.
        end if
      end if ! DN.LE.OPTDM
!
      if(OPTend)then ! Restore the best values of PARSET and PARGRD.
        do I=1,Noptpr
          WORK1(I)=BEST_grads(I)
          PARSET(I)=BEST_params(I)
        end do ! I
        call PRG_manager ('exit', 'VANEW', 'UTILITY')
        return
      end if
!
!     Test whether to apply the full Marquardt correction.
      DMULT=DirSQ/DMULT
      DirSQ=DirSQ*DMULT*DMULT

   71 ISTEP=2
!
      if(DN.GT.DD)GO TO 73
        if(MARQPM.GT.MQPMIN)then ! Test that the Marquardt parameter has its least value.
          MARQPM=MQPMIN ! Set the Marquardt parameter to its least value.
          PPAR=MARQPM*MARQPM
          NTPAR=0
          call VA_resetH (Hessian_matrix, HESSinv, WW, MARQPM, Noptpr)
          GO TO 64
        end if !
        DD=DMAX1(DN,OPTDSS)
        DirSQ=PT25*DN
        TINC=ONE
!
        if(DN.GE.OPTDSS)GO TO 132
        ISTEP=3
!
! Calculate a new set of parameters, and then do a function evaluation
        do I=1,Noptpr
          PARSET(I)=BEST_params(I)+WORK1(I)
        end do ! I
        call PRG_manager ('exit', 'VANEW', 'UTILITY')
        return
!
! Test whether to increase the Marquardt parameter.
   73 continue
!
      if(DN.LE.DPAR)then
        NTPAR=0
      else if(NTPAR.LE.0) then
        NTPAR=1
        PTM=DN
      else
        NTPAR=NTPAR+1
        PTM=DMIN1(PTM,DN)
        if(NTPAR.GE.MaxEincr) then
          if(Ntrace.LT.ITRAMX)then
            Ntrace=Ntrace+1
            ITRACE(Ntrace)='M'
          end if
          MARQPM=MARQPM*(PTM/OPTDM)**PT25       ! Set the larger value of the Marquardt parameter.
          if(SIX*DD.LT.OPTDM) then
            AP=DSQRT(PRED/DN)
            if(AP.GT.MARQPM)MARQPM=DMIN1(AP,MARQPM*(OPTDM/(SIX*DD))**PT25)
          end if ! SIX*DD.LT.OPTDM
          PPAR=MARQPM*MARQPM
          NTPAR=0
          call VA_resetH (Hessian_matrix, HESSinv, WW, MARQPM, Noptpr)
          GO TO 64
        end if ! NTPAR.GE.MaxEincr
      end if ! DN.LE.DPAR
!
! Test whether to use the steepest descent direction.
      if(DirSQ.GE.DD)then
!
! Test whether the initial value of DD has been set.
        if(DD.LE.0) then
          DD=DMIN1(OPTDM,DirSQ)
          if(DD.LT.OPTDSS)then
            DD=OPTDSS
            GO TO 71
          end if ! DD.LT.OPTDSS
        end if ! DD.LE.0
!
!     Set the multiplier of the steepest descent direction.
        ANMULT=ZERO
        DMULT=DMULT*DSQRT(DD/DirSQ)
      else
!
!     Interpolate between the steepest descent and Marquardt directions.
        SP=SP*DMULT
        ANMULT=(DD-DirSQ)/((SP-DirSQ)+DSQRT((SP-DD)**2+(DN-DD)*(DD-DirSQ)))
        DMULT=DMULT*(ONE-ANMULT)
      end if ! DirSQ.GE.DD
!
!     Calculate the correction to X, and its angle with the first direction.
      DN=ZERO
      SP=ZERO
      do I=1,Noptpr
        WORK1(I)=DMULT*PARSET(I)+ANMULT*WORK1(I)
        DN=DN+WORK1(I)*WORK1(I)
        SP=SP+WORK1(I)*DirMatrix(I,1)
      end do ! I
      DirSQ=PT25*DN
!
! Test whether an extra step is needed for independence.
      if(WC(1).GT.DTEST.and.SP*SP.LT.DirSQ)then
!
! Take the extra step and update the direction matrix.
      write(UNIout,'(a)')'VANEW: extra step'
        do I=1,Noptpr
          PARSET(I)=BEST_params(I)+VAstep_size*DirMatrix(I,1)
          if(I.LT.Noptpr)WC(I)=WC(I+1)+ONE
        end do ! I
        WC(Noptpr)=ONE
        if(Noptpr.GT.1) then
          do I=1,Noptpr
            SP=DirMatrix(I,1)
            do J=2,Noptpr
              DirMatrix(I,J-1)=DirMatrix(I,J)
            end do ! J
            DirMatrix(I,Noptpr)=SP
          end do ! I
        end if ! Noptpr.GT.1
!
      call PRG_manager ('exit', 'VANEW', 'UTILITY')
      return
      end if ! WC(1).GT.DTEST.and.SP*SP.LT.DirSQ
  132 continue
!
!     Express the new direction in terms of those of the direction matrix, and update the counts in WC.
      if(Noptpr.GE.2) then
        SP=ZERO
        DW=ZERO
        do I=1,Noptpr
          PARSET(I)=DW
          DW=ZERO
          do J=1,Noptpr
            DW=DW+WORK1(J)*DirMatrix(J,I)
          end do ! J
          if(ISTEP.NE.1)then
            WC(I)=WC(I)+ONE
            SP=SP+DW*DW
            if(SP.GT.DirSQ)then
              ISTEP=1
              KK=I
              PARSET(1)=DW
              if(I.LT.Noptpr)WC(I)=WC(I+1)+ONE
            end if ! (SP.GT.DirSQ)
          else
            PARSET(I)=DW
            if(I.LT.Noptpr)WC(I)=WC(I+1)+ONE
          end if ! (ISTEP.NE.1)
        end do ! I
        WC(Noptpr)=ONE
!
!     Reorder the directions so that KK is first.
        if(KK.GT.1) then
          KK2=KK+2
          do I=1,Noptpr
            SP=DirMatrix(I,KK)
            do J=2,KK
              DirMatrix(I,KK2-J)=DirMatrix(I,KK2-J-1)
            end do ! J
            DirMatrix(I,1)=SP
          end do ! I
        end if ! KK.GT.1
!
!     Generate the new orthogonal direction matrix.
        WW(1:Noptpr)=ZERO
        SP=PARSET(1)*PARSET(1)
        do I=2,Noptpr
          DirSQ=DSQRT(SP*(SP+PARSET(I)*PARSET(I)))
          DW=SP/DirSQ
          DirSQ=PARSET(I)/DirSQ
          SP=SP+PARSET(I)*PARSET(I)
          do J=1,Noptpr
            WW(J)=WW(J)+PARSET(I-1)*DirMatrix(J,I-1)
            DirMatrix(J,I-1)=DW*DirMatrix(J,I)-DirSQ*WW(J)
          end do ! J
        end do ! I
        SP=ONE/DSQRT(DN)
        do I=1,Noptpr
          DirMatrix(I,Noptpr)=SP*WORK1(I)
        end do ! I
!
      else
        ISTEP=1
!
      end if ! Noptpr.GE.2
!
!     Predict the new right hand sides.
      FNP=ZERO
      do I=1,Noptpr
        SUM=BEST_grads(I)
        do J=1,Noptpr
          SUM=SUM+Hessian_matrix(J,I)*WORK1(J)
        end do ! J
        WW(I)=SUM
        FNP=FNP+SUM*SUM
      end do ! I
!
! Calculate the next vector PARSET and return.
      do I=1,Noptpr
        PARSET(I)=BEST_params(I)+WORK1(I)
      end do ! I
      call PRG_manager ('exit', 'VANEW', 'UTILITY')
      return
!
!     Update the step size.
   49 DMULT=PT9*FMIN+PT1*FNP-FSQ
      if(Ntrace.LT.ITRAMX)then
        Ntrace=Ntrace+1
        ITRACE(Ntrace)='S'
      end if
!
      if(DMULT.LT.ZERO) then
        DD=DMAX1(OPTDSS,PT25*DD)
        TINC=ONE
!
        if(FSQ.LT.FMIN)then
          FMIN=FSQ
!          do I=1,Noptpr
!            SP=PARSET(I)
!            PARSET(I)=BEST_params(I)
!            BEST_params(I)=SP
!          end do ! I
!          do I=1,Noptpr
!            SP=WORK1(I)
!            WORK1(I)=BEST_grads(I)
!            BEST_grads(I)=SP
!          end do ! I
        else
          PREVPAR(1:Noptpr)=PARSET(1:Noptpr)
          PREVGRD(1:Noptpr)=WORK1(1:Noptpr)
        end if
        GO TO 107
      else
!
!     Try the test to decide whether to increase the step length.
        SP=ZERO
        SS=ZERO
        do I=1,Noptpr
          T=WORK1(I)-WW(I)
          SP=SP+DABS(WORK1(I)*T)
          SS=SS+T*T
        end do ! I
        PJ=ONE+DMULT/(SP+DSQRT(SP*SP+DMULT*SS))
        SP=DMIN1(FOUR,TINC,PJ)
        TINC=PJ/SP
        DD=DMIN1(OPTDM,SP*DD)
!
        FMIN=FSQ
!        do I=1,Noptpr
!          PREVpar(I)=BEST_params(I)
!          BEST_params(I)=PARSET(I)
!          SP=PARSET(I)
!          PARSET(I)=BEST_params(I)
!          BEST_params(I)=SP
!        end do ! I
!        do I=1,Noptpr
!          PREVgrd(I)=BEST_grads(I)
!          BEST_grads(I)=WORK1(I)
!          SP=WORK1(I)
!          WORK1(I)=BEST_grads(I)
!          BEST_grads(I)=SP
!        end do ! I
!
        GO TO 107
      end if ! DMULT.LT.ZERO
!
!     If WORK1(X) improves, store the new value of X.
   47 continue
      if(Ntrace.LT.ITRAMX)then
        Ntrace=Ntrace+1
        ITRACE(Ntrace)='X'
      end if
!
      if(FSQ.LT.FMIN)then
        FMIN=FSQ
!        do I=1,Noptpr
!          SP=PARSET(I)
!          PARSET(I)=BEST_params(I)
!          BEST_params(I)=SP
!        end do ! I
!        do I=1,Noptpr
!          SP=WORK1(I)
!          WORK1(I)=BEST_grads(I)
!          BEST_grads(I)=SP
!        end do ! I
      else
        PREVPAR(1:Noptpr)=PARSET(1:Noptpr)
        PREVGRD(1:Noptpr)=WORK1(1:Noptpr)
      end if ! FSQ.LT.FMIN

  110 continue
      if(ISTEP.GE.3) then
        ISTEP=2
        if(FMIN.GT.OPTAVA)then
        do I=1,Noptpr
          PARSET(I)=BEST_params(I)+VAstep_size*DirMatrix(I,1)
          if(I.LT.Noptpr)WC(I)=WC(I+1)+ONE
        end do ! I
        WC(Noptpr)=ONE
        if(Noptpr.GT.1) then
          do I=1,Noptpr
            SP=DirMatrix(I,1)
            do J=2,Noptpr
              DirMatrix(I,J-1)=DirMatrix(I,J)
            end do ! J
            DirMatrix(I,Noptpr)=SP
          end do ! I
        end if ! Noptpr.GT.1

        call PRG_manager ('exit', 'VANEW', 'UTILITY')
        return
        else
          write(UNIout,'(a)')'VANEW> FMIN.GT.OPTAVA'
          stop 'VANEW> FMIN.GT.OPTAVA'
        end if ! FMIN.GT.OPTAVA
      end if ! ISTEP.GE.3
!
!     Calculate the changes in X and in F.
  107 DirSQ=ZERO
      do I=1,Noptpr
        PARSET(I)=PREVpar(I)-BEST_params(I)
        DirSQ=DirSQ+PARSET(I)*PARSET(I)
      end do ! I
      do I=1,Noptpr
        WORK1(I)=PREVgrd(I)-BEST_grads(I)
      end do ! I
!
!     Calculate the generalized inverse times the change in X.
      SS=ZERO
      do I=1,NOPTP2
        SP=ZERO
        do J=1,Noptpr
          SP=SP+HESSinv(J,I)*PARSET(J)
        end do ! J
        WW(Noptpr+I)=SP
        SS=SS+SP*SP
      end do ! I
!
!     Calculate J times the change in F.
!     Also apply projection to the generalized inverse.
      do I=1,Noptpr
        ST=ZERO
        do J=1,NOPTP2
          ST=ST+HESSinv(I,J)*WW(Noptpr+J)
        end do ! J
        ST=ST/SS
        do J=1,NOPTP2
          HESSinv(I,J)=HESSinv(I,J)-ST*WW(Noptpr+J)
        end do ! J
        ST=PPAR*PARSET(I)
        do J=1,Noptpr
          ST=ST+Hessian_matrix(I,J)*WORK1(J)
        end do ! J
        WW(I)=ST
      end do ! I
!
!     Revise J and calculate row vector for correction to inverse.
      SP=ZERO
      SPP=ZERO
      do I=1,Noptpr
        SS=WORK1(I)
        ST=SS
        do J=1,Noptpr
          SS=SS-Hessian_matrix(J,I)*PARSET(J)
          ST=ST-HESSinv(J,I)*WW(J)
        end do ! J
        SS=SS/DirSQ
        WW(Noptpr+I)=ST
        SP=SP+WORK1(I)*ST
        SPP=SPP+ST*ST
        do J=1,Noptpr
          Hessian_matrix(J,I)=Hessian_matrix(J,I)+SS*PARSET(J)
        end do ! J=1
      end do ! I=1
!
      do I=1,Noptpr
        ST=MARQPM*PARSET(I)
        do J=1,Noptpr
          ST=ST-HESSinv(J,Noptpr+I)*WW(J)
        end do ! J
        WW(NOPTP2+I)=ST
        SP=SP+MARQPM*PARSET(I)*ST
        SPP=SPP+ST*ST
      end do ! I
      if(Ntrace.LT.ITRAMX)then
        Ntrace=Ntrace+1
        ITRACE(Ntrace)='U'
      end if
!
!     Test that the scalar product is sufficiently accurate.
      if(PT01*SPP.LE.DABS(SP-SPP))then
        call VA_resetH (Hessian_matrix, HESSinv, WW, MARQPM, Noptpr)
        GO TO 64
      end if
!
!     Calculate the new generalized inverse.
      do I=1,Noptpr
        ST=PARSET(I)
        do J=1,Noptpr
          ST=ST-HESSinv(I,J)*WORK1(J)
        end do ! J
        SS=ZERO
        do J=1,Noptpr
          SS=SS+HESSinv(I,Noptpr+J)*PARSET(J)
        end do ! J
        ST=(ST-MARQPM*SS)/SP
        do J=1,NOPTP2
          HESSinv(I,J)=HESSinv(I,J)+ST*WW(Noptpr+J)
        end do ! J
      end do ! I
!
      if(Ntrace.LT.ITRAMX)then
        Ntrace=Ntrace+1
        ITRACE(Ntrace)='I'
      end if

      GO TO 64
!
! End of routine VANEW
!      call PRG_manager ('exit', 'VANEW', 'UTILITY')
!      return
      end
      subroutine SET_defaults_VA
!***********************************************************************
!     Date last modified: November  1, 1994                            *
!     Description: Subroutine based on VA05AD minimization of sum of   *
!                  squares technique, for general critical points,     *
!                  with gradients. Hessian evaluation in separate      *
!                  subroutine.                                         *
!***********************************************************************
! Modules:
      USE OPT_defaults
      USE program_constants
      USE matrix_print
      USE OPT_objects
      USE OPT_VA
      USE mod_type_hessian

      implicit none
!
! Local scalars:
      integer I,J
!
! Local parameters:
      double precision TEN
      parameter (TEN=10.0D0)
!
! Begin:
      call PRG_manager ('enter', 'SET_defaults_VA', 'UTILITY')
!
      OPTDMX=1.0D-01*Angstrom_to_Bohr
      VAstep_size=0.02D0*Angstrom_to_Bohr
      OPTDSS=VAstep_size*VAstep_size
      OPTDM=OPTDMX*OPTDMX
      DPAR=TEN*OPTDM
!
! End of routine SET_defaults_VA
      call PRG_manager ('exit', 'SET_defaults_VA', 'UTILITY')
      return
      end
      subroutine VA_resetH (Hessian_matrix, &
                            WI, &
                            WW, &
                            MARQPM, &
                            Noptpr)
!*************************************************************************
!     Date last modified: June 8, 2001                                   *
!     Author: R. A. Poirier                                              *
!     Description: Symmetrize the Hessian and create the inverse Hessian,*
!     incorporating the Marquart parameter.                              *
!*************************************************************************
! Modules:
      USE program_constants
      USE matrix_print

      implicit none
!
! Input scalars:
      integer Noptpr
      double precision MARQPM
!
! Input arrays:
      double precision Hessian_matrix(Noptpr,Noptpr)
!
! Local scalars:
      integer I,J
      double precision HIJ
!
! Output arrays:
      double precision WI(Noptpr,2*Noptpr),WW(4*Noptpr)
!
! Begin:
      call PRG_manager ('enter', 'VA_resetH', 'UTILITY')
!
!     Symmetrize Hessian_matrix
      do I=1,Noptpr
        do J=1,I
          HIJ=PT5*(Hessian_matrix(I,J)+Hessian_matrix(J,I))
          Hessian_matrix(I,J)=HIJ
          Hessian_matrix(J,I)=HIJ
        end do ! J
      end do ! I
!
      do I=1,Noptpr
        do J=1,Noptpr
          WI(J,I)=Hessian_matrix(J,I)
        end do ! J
      end do ! I
      do I=1,Noptpr
        do J=1,Noptpr
          WI(J,Noptpr+I)=ZERO
        end do ! J
        WI(I,Noptpr+I)=MARQPM
      end do ! I
!
!     Calculate the generalized inverse of J.
      call MB11MP (Noptpr, 2*Noptpr, WI, WW)
!
! End of routine VA_resetH
      call PRG_manager ('exit', 'VA_resetH', 'UTILITY')
      return
      end
      subroutine end_OPT_VA
!***********************************************************************************
!     Date last modified:  May 9, 2001                                             *
!     Author:  R. A. Poirier                                                       *
!     Description:  Initialize TS-BFGS optimization                                *
!***********************************************************************************
! Modules:
      USE OPT_VA

      implicit none
!
! Begin:
      call PRG_manager ('enter', 'end_OPT_VA', 'UTILITY')
!
! Deallocate work arrays:
      deallocate (WC, DirMatrix, HESSinv, WW, WORK1)
!
! end of routine
      call PRG_manager ('exit', 'end_OPT_VA', 'UTILITY')
      return
      end
      subroutine INI_OPT_VA
!***********************************************************************
!     Date last modified: May 10, 2001                     Version 2.0 *
!     Description: Initializes VA05AD                                  *
!***********************************************************************
! Modules:
      USE OPT_defaults
      USE program_constants
      USE matrix_print
      USE OPT_objects
      USE OPT_VA
      USE mod_type_hessian

      implicit none
!
! Local scalars:
      integer I,J
!
! Local parameters:
      double precision TEN
      parameter (TEN=10.0D0)
!
! Begin:
      call PRG_manager ('enter', 'INI_OPT_VA', 'UTILITY')
!
! Set the Marquardt parameter to its default value. Note that OPTAVA requires Noptpr!
      OPTAVA=OPTACC*OPTACC*dble(Noptpr)
      MQPMIN=DSQRT(OPTAVA)/OPTDMX
      MARQPM=MQPMIN ! Set the Marquardt parameter to its least value.
      PPAR=MQPMIN*MQPMIN
!
      NTEST=3
      ISTEP=4
      FMIN=ZERO
      do I=1,Noptpr
        FMIN=FMIN+PARGRD(I)*PARGRD(I)
      end do ! I
!
      allocate (WC(Noptpr), DirMatrix(Noptpr, Noptpr), HESSinv(Noptpr, 2*Noptpr), &
                WW(4*Noptpr), WORK1(Noptpr))
!, deltaq(Noptpr), deltag(Noptpr))
      WW(1:4*Noptpr)=ZERO
      NTPAR=0
      DD=ZERO
      TINC=ONE
      NOPTP2=Noptpr+Noptpr
! Set the Marquardt parameter to its default value.
      FNP=ZERO
      DirSQ=ZERO
      LCONV=.false.
!
! Set the direction matrix.
      do I=1,Noptpr
        do J=1,Noptpr
          DirMatrix(J,I)=ZERO
        end do ! J
        DirMatrix(I,I)=ONE
        WC(I)=ONE+DBLE(Noptpr-I)
      end do ! I
!
      call VA_resetH (Hessian_matrix, HESSinv, WW, MARQPM, Noptpr)
!
! MaxEincr - Maximum number of times OPT_function%value can increase in a row.
      MaxEincr = Noptpr+2
!
! DTEST is used in a test to maintain linear independence.
      DTEST=DBLE(NOPTP2)-PT5
!
      write(UNIout,'(a,F12.4)')'Marquart paramater: ', MQPMIN
!
! End of routine INI_OPT_VA
      call PRG_manager ('exit', 'INI_OPT_VA', 'UTILITY')
      return
      end
