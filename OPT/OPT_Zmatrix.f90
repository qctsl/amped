      subroutine ZMT_TO_OPT
!***********************************************************************
!     Date last modified: August 5, 2003                  Version 2.0  *
!     Author: R.A. Poirier                                             *
!     Description: Set Z-matrix coordinate parameters and              *
!                  gradients for optimization.                         *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE OPT_defaults
      USE OPT_objects

      implicit none
!
! Local scalars:
      integer Ioptpr,IZMVAR,NOPT,Nstart
!
! Begin:
      call PRG_manager ('enter', 'ZMT_TO_OPT', 'UTILITY')
!
      write(UNIout,'(a)')' GEOMETRY OPTIMIZATION CONSTRAINTS GENERATED FROM THE FREE FORMAT Z-MATRIX VARIABLES'
      NICOOR=NMCOOR
! First determine Noptpr
      Noptpr=0
      if(OPT_coord%bonds)then
!     write(6,*)'BONDS:'
      do IZMVAR=1,Zmatrix%Nbonds
!       write(6,*)'IZMVAR,Zmatrix%define(IZMVAR)%name: ',IZMVAR,Zmatrix%define(IZMVAR)%name
        IF(Zmatrix%define(IZMVAR)%opt)then
          Noptpr=Noptpr+1
        end if
      end do ! IZMVAR
      end if

      if(OPT_coord%angles)then
!     write(6,*)'ANGLES:'
      Nstart=Zmatrix%Nbonds
      do IZMVAR=Nstart+1,Nstart+Zmatrix%Nangles
!       write(6,*)'IZMVAR,Zmatrix%define(IZMVAR)%name: ',IZMVAR,Zmatrix%define(IZMVAR)%name
        IF(Zmatrix%define(IZMVAR)%opt)then
          Noptpr=Noptpr+1
        end if
      end do ! IZMVAR
      end if

      if(OPT_coord%torsions)then
!     write(6,*)'TORSIONS:'
      Nstart=Zmatrix%Nbonds+Zmatrix%Nangles
      do IZMVAR=Nstart+1,Nstart+Zmatrix%Ntorsions
!       write(6,*)'IZMVAR,Zmatrix%define(IZMVAR)%name: ',IZMVAR,Zmatrix%define(IZMVAR)%name
        IF(Zmatrix%define(IZMVAR)%opt)then
          Noptpr=Noptpr+1
        end if
      end do ! IZMVAR
      end if

      if(Noptpr.eq.0)then
        write(UNIout,'(a,i10)')'ERROR> No optimization parameters have been found'
        call PRG_stop ('No Z-matrix parameters for optimization')
      end if
 
      write(UNIout,'(a,i10)')'Number of optimization parameters = ',Noptpr

      if(.not.allocated(PARSET))then
        allocate(OPT_par(Noptpr))
        allocate(PARSET(Noptpr))
        allocate(GA_domain(Noptpr)) ! highest and lowest allowed values
        allocate(PAR_type(Noptpr))
      else
        deallocate (OPT_par, PARSET, GA_domain, PAR_type)
        allocate(OPT_par(Noptpr))
        allocate (PARSET(Noptpr))
        allocate (GA_domain(Noptpr)) ! highest and lowest allowed values
        allocate (PAR_type(Noptpr))
      end if
!
      do Ioptpr=1,Noptpr
        OPT_par(Ioptpr)%opt=.false.
      end do

      NOPT=0
! Do bonds first:
      if(OPT_coord%bonds)then
      do IZMVAR=1,Zmatrix%Nbonds
        IF(Zmatrix%define(IZMVAR)%opt)then
          NOPT=NOPT+1
          OPT_par(NOPT)%value=Zmatrix%define(IZMVAR)%value
          OPT_par(NOPT)%type='bond'
          OPT_par(NOPT)%opt=.true.
          PARSET(NOPT)=Zmatrix%define(IZMVAR)%value
          GA_domain(Nopt)%high=5.0D0
          GA_domain(Nopt)%low=1.0D0
          PAR_type(NOPT)=.true.
        end if
      end do ! IZMVAR
      end if
!
! Angles converted to radians:
      if(OPT_coord%angles)then
      Nstart=Zmatrix%Nbonds
      do IZMVAR=Nstart+1,Nstart+Zmatrix%Nangles
        IF(Zmatrix%define(IZMVAR)%opt)then
          NOPT=NOPT+1
          OPT_par(NOPT)%value=Zmatrix%define(IZMVAR)%value*Deg_to_Radian
          OPT_par(NOPT)%type='angle'
          OPT_par(NOPT)%opt=.true.
          PARSET(NOPT)=Zmatrix%define(IZMVAR)%value*Deg_to_Radian
          GA_domain(Nopt)%high=3.0D0
          GA_domain(Nopt)%low=0.1D0
          PAR_type(NOPT)=.false.
        end if
      end do ! IZMVAR
      end if
!
! Torsions are converted to radians:
      if(OPT_coord%torsions)then
      Nstart=Zmatrix%Nbonds+Zmatrix%Nangles
      do IZMVAR=Nstart+1,Nstart+Zmatrix%Ntorsions
        IF(Zmatrix%define(IZMVAR)%opt)then
          NOPT=NOPT+1
          OPT_par(NOPT)%value=Zmatrix%define(IZMVAR)%value*Deg_to_Radian
          OPT_par(NOPT)%type='torsion'
          OPT_par(NOPT)%opt=.true.
          PARSET(NOPT)=Zmatrix%define(IZMVAR)%value*Deg_to_Radian
          GA_domain(Nopt)%high=PI_val
          GA_domain(Nopt)%low=-PI_val
          PAR_type(NOPT)=.false.
        end if
      end do ! IZMVAR
      end if

      if(NOPT.ne.Noptpr)then
        write(UNIout,*)'ZMT_TO_OPT> NOPT NE Noptpr'
        stop 'NOPT NE Noptpr'
      end if
!
!
! End of routine ZMT_TO_OPT
      call PRG_manager ('exit', 'ZMT_TO_OPT', 'UTILITY')
      return
      END
      subroutine OPT_TO_ZMT
!***********************************************************************
!     Date last modified: January 28, 1998                 Version 2.0 *
!     Author: R. A. Poirier                                            *
!     Description: Update the Z-matrix parameters.                     *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE OPT_defaults
      USE OPT_objects

      implicit none
!
! Local scalars:
      integer Iatom,Ioptpr,IVAL,IZMVAR,Nstart
      double precision Radian_to_Deg
!
! Begin:
      call PRG_manager ('enter', 'OPT_TO_ZMT', 'UTILITY')
!
      Radian_to_Deg=ONE/Deg_to_Radian
      Ioptpr=0
! Update Z-Matrix variables
! Bond Lengths
      if(OPT_coord%bonds)then
      do IZMVAR=1,Zmatrix%Nbonds
        IF(Zmatrix%define(IZMVAR)%opt)then
          Ioptpr=Ioptpr+1
          Zmatrix%define(IZMVAR)%value=OPT_par(Ioptpr)%value
          Zmatrix%define(IZMVAR)%value=PARSET(Ioptpr)
        end if ! Zmatrix%define
      end do ! IZMVAR
      do Iatom=2,Natoms
        IVAL=Zmatrix%bond(Iatom)%map
        IF(IVAL.GT.0)then
          Zmatrix%bond(Iatom)%length=Zmatrix%define(IVAL)%value
        else if(IVAL.LT.0)then
          Zmatrix%bond(Iatom)%length=-Zmatrix%define(-IVAL)%value
        end if ! IVAL
      end do ! Iatom
      end if

! Bond Angles
      if(OPT_coord%angles)then
      Nstart=Zmatrix%Nbonds
      do IZMVAR=Nstart+1,Nstart+Zmatrix%Nangles
        IF(Zmatrix%define(IZMVAR)%opt)then
          Ioptpr=Ioptpr+1
          Zmatrix%define(IZMVAR)%value=OPT_par(Ioptpr)%value*Radian_to_Deg
          Zmatrix%define(IZMVAR)%value=PARSET(Ioptpr)*Radian_to_Deg
        end if ! Zmatrix%define
      end do ! IZMVAR
      do Iatom=3,Natoms
        IVAL=Zmatrix%angle(Iatom)%map
        IF(IVAL.GT.0)then
          Zmatrix%angle(Iatom)%value=Zmatrix%define(IVAL)%value
        else if(IVAL.LT.0)then
          Zmatrix%angle(Iatom)%value=-Zmatrix%define(-IVAL)%value
        end if ! IVAL
      end do ! Iatom
      end if
!
! Torsion Angles
      if(OPT_coord%torsions)then
      Nstart=Zmatrix%Nbonds+Zmatrix%Nangles
      do IZMVAR=Nstart+1,Nstart+Zmatrix%Ntorsions
        IF(Zmatrix%define(IZMVAR)%opt)then
          Ioptpr=Ioptpr+1
          Zmatrix%define(IZMVAR)%value=OPT_Par(Ioptpr)%value*Radian_to_Deg
          Zmatrix%define(IZMVAR)%value=PARSET(Ioptpr)*Radian_to_Deg
        end if ! Zmatrix%define
      end do ! IZMVAR
      do Iatom=4,Natoms
        IVAL=Zmatrix%torsion(Iatom)%map
        IF(IVAL.GT.0)then
          Zmatrix%torsion(Iatom)%value=Zmatrix%define(IVAL)%value
        else if(IVAL.LT.0)then
          Zmatrix%torsion(Iatom)%value=-Zmatrix%define(-IVAL)%value
        end if ! IVAL
      end do ! Iatom
      end if

      if(Ioptpr.ne.Noptpr)then
        write(UNIout,*)'OPT_TO_ZMT> Ioptpr NE Noptpr'
        stop 'NOPT NE Noptpr'
      end if
!
! End of routine OPT_TO_ZMT
      call PRG_manager ('exit', 'OPT_TO_ZMT', 'UTILITY')
      return
      END
      subroutine ZMT_GRD_OPT
!***********************************************************************
!     Date last modified: January 27, 1998                Version 2.0  *
!     Author: R.A. Poirier                                             *
!     Description: Set Z-matrix coordinate parameters and              *
!                  gradients for optimization.                         *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE OPT_defaults
      USE OPT_objects
      USE QM_objects
      USE gradients

      implicit none
!
! Local scalars:
      integer Iatom,IMCOOR,Ioptpr,IZMVAR,IZVAR,Nstart
      double precision VSIGN
!
      double precision, dimension(:), allocatable :: GRDw
!
! Begin:
      call PRG_manager ('enter', 'ZMT_GRD_OPT', 'UTILITY')
!
      call GET_object ('MOL', 'GRADIENTS', 'ZM')
!
      if(.not.allocated(PARGRD))then
        allocate (PARGRD(Noptpr))
      end if

      OPT_par(1:Noptpr)%gradient=ZERO
      PARGRD(1:Noptpr)=ZERO
!
      allocate (GRDw(1:Zmatrix%Ndefines))
      GRDw(1:Zmatrix%Ndefines)=ZERO
!
! Stretches:
      do Iatom=2,Natoms
        IZVAR=Zmatrix%bond(Iatom)%map
        IF(IZVAR.GT.0)then
        IF(Zmatrix%define(IZVAR)%opt)then
          GRDw(IZVAR)=GRDw(IZVAR)+GRD_ZM(Iatom-1)
        end if
        end if
      end do ! Iatom
!
! Bends
      IF(Natoms.GE.3)then
      do Iatom=3,Natoms
        IZVAR=Zmatrix%angle(Iatom)%map
        VSIGN=DSIGN(1.0D0,DBLE(IZVAR))
        IZVAR=IABS(IZVAR)
        IF(IZVAR.GT.0)then
        IF(Zmatrix%define(IZVAR)%opt)then
          IMCOOR=Natoms+Iatom-3
          GRDw(IZVAR)=GRDw(IZVAR)+GRD_ZM(IMCOOR)*VSIGN
        end if
        end if
      end do ! Iatom
!
! Torsions
      IF(Natoms.GE.4)then
      do Iatom=4,Natoms
        IZVAR=Zmatrix%torsion(Iatom)%map
        VSIGN=DSIGN(1.0D0,DBLE(IZVAR))
        IZVAR=IABS(IZVAR)
        IF(IZVAR.GT.0)then
        IF(Zmatrix%define(IZVAR)%opt)then
          IMCOOR=2*Natoms+Iatom-6
          GRDw(IZVAR)=GRDw(IZVAR)+GRD_ZM(IMCOOR)*VSIGN
        end if
        end if
      end do ! Iatom
      end if ! Natoms.GE.4
      end if ! Natoms.GE.3
!
      Ioptpr=0
      if(OPT_coord%bonds)then
      do IZMVAR=1,Zmatrix%Nbonds
        if(Zmatrix%define(IZMVAR)%opt)then
          Ioptpr=Ioptpr+1
          OPT_par(Ioptpr)%gradient=GRDw(IZMVAR)
          PARGRD(Ioptpr)=GRDw(IZMVAR)
        end if
      end do ! IZMVAR
      end if ! OPT_coord
      if(OPT_coord%angles)then
      Nstart=Zmatrix%Nbonds
      do IZMVAR=Nstart+1,Nstart+Zmatrix%Nangles
        if(Zmatrix%define(IZMVAR)%opt)then
          Ioptpr=Ioptpr+1
          OPT_par(Ioptpr)%gradient=GRDw(IZMVAR)
          PARGRD(Ioptpr)=GRDw(IZMVAR)
        end if
      end do ! IZMVAR
      end if ! OPT_coord
      if(OPT_coord%torsions)then
      Nstart=Zmatrix%Nbonds+Zmatrix%Nangles
      do IZMVAR=Nstart+1,Nstart+Zmatrix%Ntorsions
        if(Zmatrix%define(IZMVAR)%opt)then
          Ioptpr=Ioptpr+1
          OPT_par(Ioptpr)%gradient=GRDw(IZMVAR)
          PARGRD(Ioptpr)=GRDw(IZMVAR)
        end if
      end do ! IZMVAR
      end if ! OPT_coord

      if(Ioptpr.ne.Noptpr)then
        write(UNIout,'(a)')'ERROR> ZMT_GRD_OPT: failure with Zmatrix%define'
        stop 'ERROR> ZMT_GRD_OPT: failure with Zmatrix%define'
      end if

      deallocate (GRDw)
!
! End of routine ZMT_GRD_OPT
      call PRG_manager ('exit', 'ZMT_GRD_OPT', 'UTILITY')
      return
      END
      subroutine SET_ZM_STEPS
!***********************************************************************
!     Date last modified: June ,1997                       Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!***********************************************************************
! Modules:
      USE type_molecule
      USE OPT_defaults
      USE OPT_objects
      USE mod_type_hessian

      implicit none
!
! Local scalars:
      integer Ioptpr,Nstart,IZMVAR,IBOND,IBEND,ITORS
!
! Begin:
      call PRG_manager ('enter', 'SET_ZM_STEPS', 'UTILITY')
!
      Ioptpr=0
! Stretchs
      if(OPT_coord%bonds)then
      do IBOND=1,Zmatrix%Nbonds
        IF(Zmatrix%define(IBOND)%opt)then
          Ioptpr=Ioptpr+1
          DSTEP(Ioptpr)=STRETCH_step
        end if
      end do
      end if ! OPT_coord
!
! Bends
      if(OPT_coord%angles)then
      Nstart=Zmatrix%Nbonds
      do IBEND=Nstart+1,Nstart+Zmatrix%Nangles
        IF(Zmatrix%define(IBEND)%opt)then
          Ioptpr=Ioptpr+1
          DSTEP(Ioptpr)=BEND_step
        end if
      end do
      end if ! OPT_coord
!
! Torsions
      if(OPT_coord%torsions)then
      Nstart=Zmatrix%Nbonds+Zmatrix%Nangles
      do ITORS=Nstart+1,Nstart+Zmatrix%Ntorsions
        IF(Zmatrix%define(ITORS)%opt)then
          Ioptpr=Ioptpr+1
          DSTEP(Ioptpr)=TORSION_step
        end if
      end do
      end if ! OPT_coord
!
! End of routine SET_ZM_STEPS
      call PRG_manager ('exit', 'SET_ZM_STEPS', 'UTILITY')
      return
      end
      subroutine OPT_param_input
!***********************************************************************
!     Date last modified: March 16, 1998                               *
!     Author: R. A. Poirier                                            *
!     Description: Free Format  optimization parameter input routine.  *
!                  Read optimization parameters and update the Zmatrix *
!***********************************************************************
! Modules:
      USE program_files
      USE type_molecule
      USE OPT_objects

      implicit none
!
! Local scalars:
!
! Begin:
      call PRG_manager ('enter', 'OPT_param_input', 'UTILITY')
!
      if(OPT_parameters%name(1:1).EQ.'Z')then
        call OPT_Zmatrix_update
      else if(OPT_parameters%name(1:3).eq.'XYZ')then
        call OPT_Cartesian_update
      else
        write(UNIout,'(a)')'Can only update the Z-matrix or Cartesian'
        stop 'OPT_param_input ERROR> Can only update the Z-matrix or Cartesian'
      end if
        
!
! End of routine OPT_param_input
      call PRG_manager ('exit', 'OPT_param_input', 'UTILITY')
      return
      end subroutine OPT_param_input
      subroutine OPT_Zmatrix_update
!***********************************************************************
!     Date last modified: March 16, 1998                               *
!     Author: R. A. Poirier                                            *
!     Description: Free Format  optimization parameter input routine.  *
!                  Read optimization parameters and update the Zmatrix *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE program_parser
      USE type_elements
      USE OPT_objects

      implicit none
!
! Local scalars:
      integer I,Ioptpr,IS1,IS2,lenline,lenstr
      double precision VALUE
      character*1 LINE(MAX_string)
      character(len=MAX_string) string,TEMPLINE
!
! Begin:
      call PRG_manager ('enter', 'OPT_ZMATRIX_UPDATE', 'UTILITY')
!
! Clear everything first.
!
      call ZMT_to_OPT ! create PARSET form old Z-matrix
      Ioptpr=0
      if(Lecho)write(UNIout,*)' Free Format OPT parameters input:'
!
  100 IS1=0
      IS2=0
      READ(UNITIN,'(a)')TEMPLINE
      if(Lecho)write(UNIout,'(1x,a,a80)')'input: ',TEMPLINE
      LENLINE=len_trim(TEMPLINE)
      if(LENLINE.eq.0.or.TEMPLINE(1:1).eq.'!')GO TO 100      ! Blank line or comment
!
! Allow for comments on input line
      if(index(TEMPLINE,'!').ne.0)then
        LENLINE=index(TEMPLINE,'!')-1
      end if

      do I=1,LENLINE
        LINE(I)=TEMPLINE(I:I)
      end do
!
! Check for an END
      call CNV_SLtoU (TEMPLINE(1:LENLINE))
      if(index(TEMPLINE,'END').eq.0)then
!
! Get parameter value
      Do i=1,10        ! up to 10 parameter / line
      call GET_WORD_MOL (LINE, IS1, IS2, STRING, LENSTR, LENLINE)
      if(lenstr.gt.1)then
      call CNV_StoD (string(1:lenstr), VALUE)
      Ioptpr=Ioptpr+1
      PARSET(Ioptpr)=VALUE
      end if
      end do ! i
      go to 100
!
      end if ! if(index(TEMPLINE,'END').eq.0)

      call OPT_to_ZMT

      write(UNIout,*)' Updated Z-matrix using the OPT parameters provided:'
      call PRT_FZM
!
! End of routine OPT_Zmatrix_update
      call PRG_manager ('exit', 'OPT_ZMATRIX_UPDATE', 'UTILITY')
      return
      end subroutine OPT_Zmatrix_update
      subroutine OPT_Cartesian_update
!***********************************************************************
!     Date last modified: March 16, 1998                               *
!     Author: R. A. Poirier                                            *
!     Description: Free Format  optimization parameter input routine.  *
!                  Read optimization parameters and update the Zmatrix *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE program_parser
      USE type_elements
      USE OPT_objects

      implicit none
!
! Local scalars:
      integer I,J,Ioptpr,IS1,IS2,lenline,lenstr
      double precision VALUE
      character*1 LINE(MAX_string)
      character(len=MAX_string) string,TEMPLINE
!
! Begin:
      call PRG_manager ('enter', 'OPT_CARTESIAN_UPDATE', 'UTILITY')
!
! Clear everything first.
!
      call XYZ_to_OPT ! create PARSET form old Z-matrix
      Ioptpr=0
      if(Lecho)write(UNIout,'(a)')' Cartesian OPT parameters input:'
!
  100 IS1=0
      IS2=0
      READ(UNITIN,'(a)')TEMPLINE
      if(Lecho)write(UNIout,'(1x,a,a80)')'input: ',TEMPLINE
      LENLINE=len_trim(TEMPLINE)
      if(LENLINE.eq.0.or.TEMPLINE(1:1).eq.'!')GO TO 100      ! Blank line or comment
!
! Allow for comments on input line
      if(index(TEMPLINE,'!').ne.0)then
        LENLINE=index(TEMPLINE,'!')-1
      end if

      do I=1,LENLINE
        LINE(I)=TEMPLINE(I:I)
      end do
!
! Check for an END
      call CNV_SLtoU (TEMPLINE(1:LENLINE))
      if(index(TEMPLINE,'END').eq.0)then
!
! Get parameter value
      Do i=1,10        ! up to 10 parameter / line
      call GET_WORD_MOL (LINE, IS1, IS2, STRING, LENSTR, LENLINE)
      if(lenstr.gt.1)then
      call CNV_StoD (string(1:lenstr), VALUE)
      Ioptpr=Ioptpr+1
      PARSET(Ioptpr)=VALUE
      end if
      end do ! i
      go to 100
      end if ! if(index(TEMPLINE,'END').eq.0)
!
      J=MIN0(Noptpr,10)
      write(UNIout,1052)(PARSET(I),I=1,J)
      if(Noptpr.GT.10)write(UNIout,1051)(PARSET(I),I=11,Noptpr)

      call OPT_TO_XYZ ! create PARSET form new Cartesian
      write(UNIout,*)' Updated Cartesian using the OPT parameters provided:'
      call PRT_XYZ
 1051 FORMAT(3X,1PE13.5,9E13.5)
 1052 FORMAT(' X:',10(1PE13.5))

!
! End of routine OPT_Cartesian_update
      call PRG_manager ('exit', 'OPT_CARTESIAN_UPDATE', 'UTILITY')
      return
      end subroutine OPT_Cartesian_update
