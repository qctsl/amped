      subroutine CPXYZ_TO_OPT
!***********************************************************************
!     Date last modified: January 27, 1998                Version 2.0  *
!     Author: R.A. Poirier                                             *
!     Description: Set Cartesian coordinate parameters and             *
!                  gradients for optimization.                         *
!***********************************************************************
! Modules:
      USE OPT_objects

      implicit none
!
! Local scalars:
!
! Begin:
      call PRG_manager ('enter', 'CPXYZ_TO_OPT', 'UTILITY')
!
! First determine Noptpr
      Noptpr=3
!
      if(.not.allocated(PARSET))then
        allocate(OPT_par(Noptpr))
        allocate(PARSET(Noptpr))
      end if
!
! Copy critical point (X,Y,Z) to PARSET
!
! End of routine CPXYZ_TO_OPT
      call PRG_manager ('exit', 'CPXYZ_TO_OPT', 'UTILITY')
      return
      end
      subroutine OPT_TO_CPXYZ
!***********************************************************************
!     Date last modified: January 28, 1998                 Version 2.0 *
!     Author: R. A. Poirier                                            *
!     Description: Update the Cartesian coordinates.                   *
!***********************************************************************
! Modules:
      USE OPT_objects

      implicit none
!
! Local scalars:
!
! Begin:
      call PRG_manager ('enter', 'OPT_TO_CPXYZ', 'UTILITY')
!
! Copy PARSET to critical point (X,Y,Z)
!
! End of routine OPT_TO_CPXYZ
      call PRG_manager ('exit', 'OPT_TO_CPXYZ', 'UTILITY')
      return
      end
      subroutine CPXYZ_GRD_OPT
!***********************************************************************
!     Date last modified: January 27, 1998                Version 2.0  *
!     Author: R.A. Poirier                                             *
!     Description: Set Cartesian coordinate parameters and             *
!                  gradients for optimization.                         *
!***********************************************************************
! Modules:
      USE program_constants
      USE OPT_objects

      implicit none
!
! Local scalars:
!
! Begin:
      call PRG_manager ('enter', 'CPXYZ_GRD_OPT', 'UTILITY')
!
!     call GET_object ('MOL', 'GRADIENTS', 'XYZ')

      if(.not.allocated(PARGRD))then
        allocate (PARGRD(Noptpr))
      end if

! Copy critical point gradient (X,Y,Z) to PARGRD
!
! End of routine CPXYZ_GRD_OPT
      call PRG_manager ('exit', 'CPXYZ_GRD_OPT', 'UTILITY')
      return
      end
