      subroutine DOC
!***********************************************************************
!     Date last modified: June 10, 1992                                *
!     Author: R. A. Poirier                                            *
!     Description: Optimally conditioned (DOC) method, without         *
!                  gradients.                                          *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE OPT_defaults
      USE OPT_objects
      USE mod_type_hessian
      USE OPT_OC
      USE OPT_DOC

      implicit none
!
! Local scalars:
      integer I,IFLAG,J
      double precision CONDNO,FKBND,TEMP1,TEN38
!
      parameter (TEN38=1.7D38)
!
! Begin:
      call PRG_manager ('enter', 'DOC', 'UTILITY')
!
! Machine-dependent program_constants.
      FKBND=TEN38
!
! Step 1A: Initialize and set defaults if necessary.
! Check input parameters.
!
! Branch depending on ISTEP.
      I=ISTEP+1
!
!     write(6,*)'ISTEP:',Istep
!     write(6,*)PARSET(1:Noptpr)
      GO TO (2000,3010,3030,3040,3050,115),I
! Initial entry
  115 continue
!
!     write(6,*)'WORK:'
!     write(6,*)WORK(1:Noptpr)
!     write(6,*)IHessian_factored(1:Noptpr*(Noptpr+1)/2)
      
      do J=1,Noptpr
        PARSET(J)=BEST_params(J)+WORK(J)*GRSTEP
      end do ! J=1
!     write(6,*)'ISTEP:',Istep
!     write(6,*)PARSET(1:Noptpr)
      Istep=1

      GO TO 2010 ! Return
!
! Re-entry (ISTEP=1).
 3010 FVALI=OPT_function%value
!     write(6,*)'ISTEP ',ISTEP
!     write(6,*)'FVALI ',FVALI
!     write(6,*)'FVAL0 ',FVAL0
!     write(6,*)'GRSTEP ',GRSTEP
!     write(6,*)'LCOL ',LCOL
      FK0OC(LCOL)=(FVALI-FVAL0)/GRSTEP
      LCOL=LCOL+1
!
      IF(LCOL.LE.Noptpr)then ! GO TO 3000
      call PICK (IHessian_factored, WORK, Noptpr, LCOL)
!     write(6,*)'WORK:'
!     write(6,*)WORK(1:Noptpr)
      do J=1,Noptpr
        PARSET(J)=BEST_params(J)+WORK(J)*GRSTEP
      end do ! J=1
!
      GO TO 2010
      end if
!
! Step 1C: Set variables for initial step.
  135 FVALB=TWO*FVAL0-ELBOUND
      FVALA=FVAL0
!
! Step 1D: Indicate initialization of updating vector.
  140 OPTFNS=ZERO
!
! Step 2: Conduct search.  Begin by printing trace if requested.
  200  continue
      IF(OPTITR.NE.0)then
        call OPT_print_DOC
      end if
!
  210 ITRACE(1:ITRAMX)=' '
!
! Ntrace points to next available 'trace' element.
      Ntrace=1
!
! Step 2A: Main convergence test.
      FCONVR=ZERO
      do I=1,Noptpr
         TST=FK0OC(I)
         SOC(I)=-TST
         FCONVR=FCONVR+TST*TST
      end do ! I=1
!
      IF(FCONVR.LE.OPTEPS)GO TO 520
      OPTITR=OPTITR+1
!
! Step 2B: Set Euclidean search direction and compute initial step.
      TST=DSQRT(FCONVR)
      OPTFP0=-TST
      IF((FVAL0-ELBOUND).LT.ZERO)then
         call SET_ELBOUND
         FVALB=FVAL0+FVAL0-ELBOUND
         write(UNIout,1050)ELBOUND
      end if
!
  240 TEMP1=DMIN1(TWO*(FVALB-FVAL0)/FCONVR,TWO*(FVAL0-ELBOUND)/FCONVR)
      IF(TEMP1.GE.ONE)then
         IF(Ntrace.LT.ITRAMX)then
           ITRACE(Ntrace)='U'
           Ntrace=Ntrace+1
         end if
         TEMP1=ONE
      end if
!
  250 FVALB=FVALA
      FVALA=FVAL0
      TST=TST*TEMP1
!
! Step 2C: Transform so last column of IHessian_factored lies along search direction.
      ISW=1
      IF(OPTFNS.GT.ZERO)ISW=2
!
      call LTOLSP (Noptpr, Noptpr, IHessian_factored, SOC, FK0OC, FNOC, ISW, VOC)
!
! Step 3: Conduct search.
      ICONV=0
      ISTEP=0
!
 2000 continue
      call SEARCH (BEST_params, VOC, PARSET, Itrace, ITRAMX, Noptpr, OPT_function%value, FVAL0, NFEVAL, Ntrace)
!
      IF(ICONV)254,320,2010
!
! Restart, unless three successive restarts.
  254 OPTNSR=OPTNSR+1
!
      IF(OPTNSR.GE.3)then
         write(UNIout,'(2A)')'%MUNgauss ERROR> 3 SUCCESSIVE OPT_function%value REDUCTIONS LESS THAN', &
       ' OPTEPS - OPTIMIZATION TERMINATED'
         OPTNSR=0
         GO TO 630
      end if
  255 OPTRST=OPTRST+1
!
      IF(OPTRST.GT.OPTMXR)GO TO 650
      do I=1,Noptpr
         WORK(I)=ONE
         IF(HMODE.EQ.1)WORK(I)=DIAGFC(I)
      end do ! I=1
!
      call SETLT8 (IHessian_factored, Noptpr, Noptpr, WORK)
!
      GO TO 135
!
! Step 4: Estimate remaining M-1 components of Euclidean gradient.
  320 OPTNSR=0
      TST=ONE-OPTB0
      FMOC(Noptpr)=TST
      FK0OC(Noptpr)=OPTFP0
      OPTFMS=TST*TST
!
      IF(Noptpr.LT.2)GO TO 420
      LCOL=1
 3020 call PICK (IHessian_factored, WORK, Noptpr, LCOL)
!     write(6,*)'WORK:'
!     write(6,*)WORK(1:Noptpr)
      do J=1,Noptpr
        PARSET(J)=BEST_params(J)+WORK(J)*GRSTEP
      end do ! J=1
      ISTEP=2
!
      GO TO 2010
!
! Re-entry (ISTEP=2).
 3030 FVALI=OPT_function%value
      TST=(FVALI-FVAL0)/GRSTEP
      FK0OC(LCOL)=TST
      TST=TST/S1
      FMOC(LCOL)=-TST
      OPTFMS=OPTFMS+TST*TST
      SOC(LCOL)=ZERO
      LCOL=LCOL+1
!
      IF(LCOL.LE.Noptpr-1)GO TO 3020
  420 SOC(Noptpr)=ONE
!     write(6,*)'After 420'
!
! Step 5A: Check if update is necessary.
      IF(OPTFMS.LT.OPTEPS)then
         call LSPTOL (Noptpr, Noptpr, IHessian_factored, FK0OC, FNOC, FNOC, ISW, VOC)
         GO TO 140
      end if
!     write(6,*)'After 5A'
!
! Step 5B: Form quantities P and Q.
  500 continue 
      call FORMPQ (FK0OC, FMOC, FNOC, POC, QOC, SOC, ITRACE, ITRAMX, &
                   OPTFNS, FCONVR, Ntrace, OPTEPS, OPTFMS, OPTB0, &
                   Noptpr)
!
! Step 5C: Update IHessian_factored and other quantities.
      call LSPVEC (IHessian_factored, Noptpr, VOC, QOC, PARSET)
      ISW=2
      IF(OPTFNS.GT.ZERO)ISW=3
!
      call LSPTOL (Noptpr, Noptpr, IHessian_factored, FK0OC, POC, FNOC, ISW, VOC)
      ISW=ISW-1
!
      call LTOLSP (Noptpr, Noptpr, IHessian_factored, POC, FK0OC, FNOC, ISW, WORK)
!
! Compute last column.
      TST=POC(Noptpr)
      do I=1,Noptpr
         WORK(I)=WORK(I)+PARSET(I)*TST
      end do ! I=1
!
      call LSPTOL (Noptpr, Noptpr, IHessian_factored, FK0OC, FNOC, FNOC, ISW, WORK)
      call MODCON (Noptpr, Noptpr, IHessian_factored, FKBND, ITRACE, ITRAMX, CONDNO, IFLAG, Ntrace)
!
      IF(IFLAG.NE.1)GO TO 200
! Diagonal elements of IHessian_factored too small.
      write(UNIout,'(A)')'%MUNgauss ERROR> DIAGONAL ELEMENTS OF INVERSE HESSIAN TOO SMALL' &
       //' - OPTIMIZATION TERMINATED'
      GO TO 630
!
! Step 6: Do special step if this gives an improvement.
  520 write(UNIout,1150)
      call LTMVEC (IHessian_factored, Noptpr, Noptpr, FK0OC, VOC)
      do J=1,Noptpr
         PARSET(J)=BEST_params(J)-VOC(J)
      end do ! J=1
      ISTEP=3
      GO TO 2010
!
! Re-entry (ISTEP=3).
 3040 FVALI=OPT_function%value
!
      IF(FVAL0.LT.FVALI)GO TO 560
      BEST_params(1:Noptpr)=PARSET(1:Noptpr)
      FVAL0=FVALI
      write(UNIout,1040)
      OPTEND=.TRUE.
      OPT_converged=.true.
!
      GO TO 630
  560 write(UNIout,1090)
!
! Reset to best parameter set.
      PARSET(1:Noptpr)=BEST_params(1:Noptpr)
      ISTEP=4
      GO TO 2010
!
! Re-entry (ISTEP=4).
 3050 FVAL0=OPT_function%value
      OPTEND=.TRUE.
!
! Copy parameters back to PARSET.
  630 continue
      PARSET(1:Noptpr)=BEST_params(1:Noptpr)
      ISTEP=5
      OPTEND=.TRUE.
!
 2010 continue
! End of routine DOC
      call PRG_manager ('exit', 'DOC', 'UTILITY')
      return
!
! Error exits.
! Too many restarts.
  650 write(UNIout,'(A)')'ERROR> Too many restarts - OPTIMIZATION TERMINATED'
      GO TO 630
!
 1020 FORMAT('RESULTS AT ITERATION ',I4/'Final parameters:'//(1X,6(I6,1PD16.7)))
 1030 FORMAT('OPT_function%value =',F17.9)
 1040 FORMAT('SPECIAL STEP GAVE IMPROVEMENT')
 1050 FORMAT('*** Function value BELOW LOWER BOUND - LOWER BOUND RESET TO',F18.9)
 1070 FORMAT(I8,' function EVALUATIONS,',I10,' Restarts'/)
 1080 FORMAT('ITERATION ',I5,',  OPT_function =',F17.9,I8,' function EVALUATIONS,   TRACE: ',20A1/' X:',10(1PD13.5))
 1081 FORMAT(3X,1PD13.5,9D13.5)
 1090 FORMAT('SPECIAL STEP GAVE NO IMPROVEMENT - RESET TO BEST',' parameter set')
 1150 FORMAT('*** Function value CONVERGED WITHIN EPS - TRY SPECIAL',' STEP')
!
! End of routine DOC
      call PRG_manager ('exit', 'DOC', 'UTILITY')
      return
      END
      subroutine SEARCH (X0,      & ! Starting point of the search
                         V,       & ! Direction of search
                         PARSET,  & ! Current parameter set
                         ITRACE,  & ! Current trace element
                         ITRAMX,  & ! Maximum number of trace elements
                         Noptpr,  & ! Number of optimizable parameters
                         OPT_function_value,    & ! Current total OPT_function_value
                         FVAL0,    & !
                         NFEVAL,   & ! Number of function evaluations
                         Ntrace)
!***********************************************************************
!     Date last modified: June 10, 1992                                *
!     Author:                                                          *
!     Description:                                                     *
!     This routine uses just function evaluations to find a point      *
!     X0+V*TST along the line through X0 in the direction V, where     *
!     a) the value F(X0+V*TST) of the objective function is less than  *
!        the starting value F(X0), and                                 *
!     b) a "reasonable" quadratic approximation can be made for the    *
!        dependence of F(X0+V*TST) on TST near the minimum.            *
!     This routine does not seek an accurate approximation to the      *
!     minimum along this line.                                         *
!                                                                      *
!     X0    : On input, contains the starting point of the search.     *
!             On output, contains the new point developed by the       *
!             search (N-dimensional vector).                           *
!     V     : On input, gives the direction of search (N-dimensional   *
!             vector).                                                 *
!     Noptpr: On input, gives the number of variables.                 *
!     FVAL0 : Always contains the value of the function at X0.         *
!     OPTFP0: Always contains an estimate of the derivative along V    *
!             at X0.  Assumed negative on input.                       *
!     OPTEPS: On input, is a number specifying a lower bound on        *
!             decreases in the objective function which are deemed     *
!             significant.                                             *
!     OPTFL0: On input, is a number specifying the maximum factor by   *
!             which the step length can be increased when              *
!             extrapolating.                                           *
!     OPTB0 : On output, gives the second derivative of a quadratic    *
!             approximation to the function along the line X0+V*TST.   *
!     TST   : On input, is the initial step.                           *
!     S1    : On output, is the number which, when multiplied by V,    *
!             gives the step taken.                                    *
!     GRSTEP: On input, is a number specifying the length of the steps *
!             in Euclidean space used to estimate components of the    *
!             gradient.                                                *
!     IFLAG : On input, IFLAG=0 indicates that this is the first call. *
!             IFLAG>0 indicates subsequent calls during the same line  *
!             search.                                                  *
!             On output, IFLAG=0 specifies normal termination.         *
!             IFLAG>1 indicates that another function evaluation has   *
!             been requested.                                          *
!             IFLAG=-1 specifies that the reduction in function value  *
!             is not significant.                                      *
!***********************************************************************
! Modules:
      USE program_constants
      USE OPT_OC
      USE OPT_DOC

      implicit none
!
! Input scalars:
      integer ITRAMX,Noptpr,Ntrace
      double precision FVAL0,OPT_function_value
!
! Input arrays:
      double precision PARSET(Noptpr),X0(Noptpr),V(Noptpr)
      character*1 Itrace(ITRAMX)
!
! Local scalars:
      integer I,NFEVAL
      double precision FET
!
! Begin:
      call PRG_manager ('enter', 'SEARCH', 'UTILITY')
!
! Step 0: Initialize.
      IF(ICONV-1)10,115,320
   10 OPTFL=OPTFL0
      BDOC=ZERO
      S1=ZERO
      FMINUS=FVAL0
!
! Step 1: Obtain better point.
  100 IF(-OPTFP0*TST.LT.OPTEPS)GO TO 500
      do I=1,Noptpr
        PARSET(I)=X0(I)+V(I)*TST
      end do ! I=1
      ICONV=1
      GO TO 600
!
! Re-entry (ICONV=1).
  115 ICONV=0
      FET=OPT_function_value
!      NFEVAL=NFEVAL+1
      IF(FET.GE.FVAL0)then
         TST=TST/TWO
         OPTFL=PT5
         IF(FET.LT.FMINUS)GO TO 300
         IF(Ntrace.LT.ITRAMX)then
           ITRACE(Ntrace)='H'
           Ntrace=Ntrace+1
         end if
         GO TO 100
      end if
!
! Step 2: Lay basis for suitable quadratic interpolation.
  200 FVALS=(FET-FVAL0)/TST
      BDOC=(FVALS-OPTFP0)/TST
      OPTFP=TST
      S1=S1+TST
      TST=TST*OPTFL
      X0(1:Noptpr)=PARSET(1:Noptpr)
      FVAL0=FET
      OPTFP0=TWO*FVALS-OPTFP0
      IF(OPTFL.GE.ONE.and.OPTFP0/TST+TWO*BDOC.LT.ZERO)then
         IF(Ntrace.LT.ITRAMX)then
           ITRACE(Ntrace)='E'
           Ntrace=Ntrace+1
         end if
         GO TO 100
      end if
!
! Step 3: Estimate directional derivative at new point, compute
!         quadratic fit and exit if satisfactory fit has been made.
  300 do I=1,Noptpr
        PARSET(I)=X0(I)+V(I)*GRSTEP
      end do ! I=1
      ICONV=2
      GO TO 600
!
! Re-entry (ICONV=2).
  320 ICONV=0
      FET=OPT_function_value
!      NFEVAL=NFEVAL+1
      OPTFP0=(FET-FVAL0)/GRSTEP
      BDOC=(OPTFP0-FVALS)/(GRSTEP+OPTFP)
      OPTFP0=OPTFP0-BDOC*GRSTEP
      IF(OPTFP0/TST+TWO*BDOC.GE.ZERO)GO TO 520
      IF(OPTFL.GE.ONE)then
         IF(Ntrace.LT.ITRAMX)then
           ITRACE(Ntrace)='M'
           Ntrace=Ntrace+1
         end if
        GO TO 100
      end if
!
! Step 4: Minimum bounded.
  400 BDOC=-OPTFP0/OPTFP
      GO TO 520
!
! Step 5: Tidy up.
  500 ICONV=-1
      IF(Ntrace.LT.ITRAMX)then
        ITRACE(Ntrace)='T'
        Ntrace=Ntrace+1
      end if
  520 OPTB0=TWO*BDOC
!
  600 continue
!
! End of routine SEARCH
      call PRG_manager ('exit', 'SEARCH', 'UTILITY')
      return
      END
      subroutine LSPVEC (FL,     & ! Matrix: Lower Trapezoidal (A)
                         N,      & ! # rows of matrix
                         SPIKE,  & ! last column of matrix (B)
                         VEC,    & ! Vector V
                         RES)   ! (A:B)V
!***********************************************************************
!     Date last modified: June 10, 1992                                *
!     Author:                                                          *
!     Description:                                                     *
!     Multiply a matrix which is lower trapezoidal in all but the last *
!     column, which is dense, into the vector VEC and return the       *
!     result in RES.  The first M-1 columns of the matrix are stored   *
!     by columns in the one dimensional array FL, and the last column  *
!     is stored in SPIKE.                                              *
!                                                                      *
!***********************************************************************
! Modules:
      USE program_constants

      implicit none
!
! Input scalars:
      integer N
!
! Input arrays:
      double precision FL(N*(N+1)/2),RES(N),SPIKE(N),VEC(N)
!
! Local scalars:
      integer I,IPTFL,IUB,J
      double precision T,VM
!
! Begin:
      call PRG_manager ('enter', 'LSPVEC', 'UTILITY')
!
      VM=VEC(N)
      do I=1,N
        T=ZERO
        IPTFL=I
        IUB=MIN0(I,N-1)
        do J=1,IUB
          T=T+FL(IPTFL)*VEC(J)
          IPTFL=IPTFL+N-J
        end do
        RES(I)=T+SPIKE(I)*VM
      end do ! I=1
!
! End of routine LSPVEC
      call PRG_manager ('exit', 'LSPVEC', 'UTILITY')
      return
      END
      subroutine PICK (FL,     & ! Array in linear format
                       COLI,   & ! Vector (Column I of Array)
                       N,      & ! # rows of Array
                       I)     ! Number of column to be copied
!***********************************************************************
!     Date last modified: June 10, 1992                                *
!     Author:                                                          *
!     Description: Pick the Ith column of FL and return the result in  *
!                  COLI.                                               *
!***********************************************************************
! Modules:
      USE program_constants

      implicit none
!
! Input scalars:
      integer I,N
!
! Input arrays:
      double precision COLI(N),FL(N*(N+1)/2)
!
! Local scalars:
      integer ICT,IM1,IPT,J
!
! Begin:
      call PRG_manager ('enter', 'PICK', 'UTILITY')
!
      IF(I.LE.1)then
        IPT=0
      else
        IM1=I-1
        do J=1,IM1
          COLI(J)=ZERO
        end do ! J=1
        IPT=IM1*(2*N-I+2)/2
      end if
      ICT=1
      do J=I,N
        COLI(J)=FL(IPT+ICT)
        ICT=ICT+1
      end do ! J=1
!
! End of routine PICK
      call PRG_manager ('exit', 'PICK', 'UTILITY')
      return
      END
      subroutine INI_OPT_DOC
!***********************************************************************************
!     Date last modified:  May 9, 2001                                             *
!     Author:  R. A. Poirier                                                       *
!     Description:  Initialize DOC optimization                                *
!***********************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE OPT_defaults
      USE mod_type_hessian
      USE OPT_objects
      USE OPT_OC
      USE OPT_DOC

      implicit none
!
! Begin:
      call PRG_manager ('enter', 'INI_OPT_DOC', 'UTILITY')
!
! Allocate work arrays:
      allocate (POC(1:Noptpr), QOC(1:Noptpr), SOC(1:Noptpr), VOC(1:Noptpr), WORK(1:Noptpr))
      allocate (FKOC(1:Noptpr), FMOC(1:Noptpr), FNOC(1:Noptpr), FK0OC(1:Noptpr))
      FKOC(1:Noptpr)=ZERO
      FK0OC(1:Noptpr)=ZERO
      FMOC(1:Noptpr)=ZERO
      FNOC(1:Noptpr)=ZERO
      POC(1:Noptpr)=ZERO
      QOC(1:Noptpr)=ZERO
      SOC(1:Noptpr)=ZERO
      VOC(1:Noptpr)=ZERO
!
      S1=0.0D0     ! final trace step
      OPTFP=0.0D0 ! trace hessian estimate current
      OPTEPS=0.25D-05

!     GRSTEP=5.0D-03 ! gradient numerical step length
      Istep=5
      LCOL=1
      HMODE=1
      FVALI=OPT_function%value
      FVAL0=FVALI
      call SET_ELBOUND
      write(UNIout,1210)HMODE,ELBOUND  ! deleted OPTTYP not used?
 1210 FORMAT('OCMODE =',I4,' LOWER BOUND =',F17.9)
      write(UNIout,1230)OPTEPS,OPTFL0
 1230 FORMAT('OPTEPS =',E13.5,5X,'EXTRAPOLATION FACTOR =',0PF9.5)
!
      call PICK (IHessian_factored, WORK, Noptpr, LCOL)
!
! end of routine
      call PRG_manager ('exit', 'INI_OPT_DOC', 'UTILITY')
      return
      end
      subroutine END_OPT_DOC
!***********************************************************************************
!     Date last modified:  May 9, 2001                                             *
!     Author:  R. A. Poirier                                                       *
!     Description:  Initialize DOC optimization                                *
!***********************************************************************************
! Modules:
      USE OPT_defaults
      USE OPT_objects
      USE OPT_OC

      implicit none
!
! Begin:
      call PRG_manager ('enter', 'END_OPT_DOC', 'UTILITY')
!
! Deallocate work arrays:
      deallocate (WORK)
      deallocate (FKOC, FK0OC, FMOC, FNOC, POC, QOC, SOC, VOC)
!
! end of routine
      call PRG_manager ('exit', 'END_OPT_DOC', 'UTILITY')
      return
      end
      subroutine SET_ELBOUND
!***********************************************************************************
!     Date last modified:  May 9, 2001                                             *
!     Author:  R. A. Poirier                                                       *
!     Description:  Initialize DOC optimization                                *
!***********************************************************************************
! Modules:
      USE program_files
      USE OPT_defaults
      USE OPT_objects
      USE OPT_OC
      USE OPT_DOC

      implicit none
!
      double precision C0FMIN,C1FMIN
      parameter (C0FMIN=0.0119701D0,C1FMIN=11.9655D0)
! Begin:
      call PRG_manager ('enter', 'SET_ELBOUND', 'UTILITY')
!
      if(OPT_parameters%name(1:3).EQ.'XYZ')then         ! Cartesians
        ELBOUND=OPT_function%value-C0FMIN-C1FMIN*FCONVR*TST/DSQRT(DBLE(Noptpr))
        OPTEPS=2.50D-08 ! energy convergence accuracy (predicted change)
      else if(OPT_parameters%name(1:4).EQ.'Z-MA')then     ! Z-matrix
        ELBOUND=OPT_function%value-C0FMIN-C1FMIN*FCONVR*TST/DSQRT(DBLE(Noptpr))
        OPTEPS=2.50D-08 ! energy convergence accuracy (predicted change)
      else if(OPT_parameters%name(1:5).EQ.'SSCAN')then    ! Surface Scan
        ELBOUND=OPT_function%value-C0FMIN-C1FMIN*FCONVR*TST/DSQRT(DBLE(Noptpr))
        OPTEPS=2.50D-08 ! energy convergence accuracy (predicted change)
      else if(OPT_parameters%name(1:3).EQ.'PIC')then    ! Proper/Natural Internal Coordinates
        ELBOUND=OPT_function%value-C0FMIN-C1FMIN*FCONVR*TST/DSQRT(DBLE(Noptpr))
        OPTEPS=2.50D-08 ! energy convergence accuracy (predicted change)
      else if(OPT_parameters%name(1:3).EQ.'RIC')then      ! Redundant coordinates
        ELBOUND=OPT_function%value-C0FMIN-C1FMIN*FCONVR*TST/DSQRT(DBLE(Noptpr))
        OPTEPS=2.50D-08 ! energy convergence accuracy (predicted change)
      else if(OPT_parameters%name(1:4).EQ.'NRIC')then      ! Non Redundant coordinates
        ELBOUND=OPT_function%value-C0FMIN-C1FMIN*FCONVR*TST/DSQRT(DBLE(Noptpr))
        OPTEPS=2.50D-08 ! energy convergence accuracy (predicted change)
      else if(OPT_parameters%name(1:10).EQ.'ROSENBROCK')then
        ELBOUND=OPT_function%value-C0FMIN-C1FMIN*FCONVR*TST/DSQRT(DBLE(Noptpr))
        OPTEPS=2.50D-08 ! energy convergence accuracy (predicted change)
      else if(OPT_parameters%name(1:8).EQ.'CHONGZAK')then
        ELBOUND=OPT_function%value-C0FMIN-C1FMIN*FCONVR*TST/DSQRT(DBLE(Noptpr))
        OPTEPS=2.50D-08 ! energy convergence accuracy (predicted change)
      else if(OPT_parameters%name(1:7).EQ.'RVALUES')then
        OPTEPS=1.0D00
        ELBOUND=0.5D0 ! Set to 0.5 mh (appears to be the best)
      else if(OPT_parameters%name(1:8).EQ.'SFACTORS')then
        OPTEPS=2.50D-08 ! energy convergence accuracy (predicted change)
        ELBOUND=OPT_function%value-0.001
      else
        write(UNIout,'(a)')'ERROR> SET_ELBOUND: illegal optimization coordinates'
      end if
!
! end of routine
      call PRG_manager ('exit', 'SET_ELBOUND', 'UTILITY')
      return
      end
