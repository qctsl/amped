      subroutine BFGS
!***********************************************************************
!     Date last modified: May 10, 1995                     Version 1.0 *
!     Author: R. A. Poirier                                            *
!     Broyden-Fletcher-Goldfarb-Shanno (BFGS) method, with gradients.  *
!     Array dimensions:                                                *
!     Noptpr - Number of parameters to be optimized.                   *
!     GSC    - Lower bound on step length                              *
!     Hessian_factored - Hessian approximation (Noptpr*(Noptpr+1)/2).            *
!     BEST_grads  - Gradient vector (Noptpr).                               *
!     Sscale  - parameter scale factors (Noptpr).                       *
!     PARAM  - parameter set (Noptpr).                                 *
!     DIAGFC,FK,FN,FM - Work arrays (Noptpr).                          *
!     PARSET,PARGRD  - parameter values,gradients (Noptpr).            *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE OPT_defaults
      USE OPT_objects
      USE mod_type_hessian
      USE OPT_OC
      USE OPT_BFGS

      implicit none
!
! Local scalar:
      integer HESLEN
      integer I,IR,K
      double precision GL1,GL2
!
! Local parameters:
      double precision ANINE,PT01,PT1,PT7
      parameter (ANINE=9.0D0,PT01=0.01D0,PT1=0.1D0,PT7=0.7D0)
!
! Begin:
      call PRG_manager ('enter', 'BFGS', 'UTILITY')
!
      HESLEN=Noptpr*(Noptpr+1)/2

      IF(OPTITR.GT.0)GO TO 186
! Set some variables for the first iteration.
  100 FDIFF=ZERO
  110 FVALA=FVAL0
      SAVEFV=1
      FN(1:Noptpr)=BEST_params(1:Noptpr)
      FM(1:Noptpr)=BEST_grads(1:Noptpr)

! Begin the iteration by giving the required printing.
  130 continue

      IF(SPECIT.GT.0)GO TO 171
      if(OPTITR.GT.0)then
        PARSET(1:Noptpr)=BEST_params(1:Noptpr)
        PARgrd(1:Noptpr)=BEST_grads(1:Noptpr)
        call OPT_print
!       call OPT_print (BEST_params, BEST_grads, ITRACE, Noptpr, ITRAMX, Ntrace, OPTITR, NFEVAL, GRDLTH, OPT_function%value)
      end if
      OPTITR=OPTITR+1
!
! Calculate the search direction of the iteration.
      WORK1(1:Noptpr)=-FM(1:Noptpr)

      call MC11ED (Hessian_factored, HESLEN, Noptpr, WORK1, FK, Noptpr)
!
! Calculate a lower bound on the step-length and the initial directional derivative.
      GSC=ZERO
      DGRDA=ZERO
      do I=1,Noptpr
         GSC=DMAX1(GSC,DABS(WORK1(I)/Sscale(I)))
         DGRDA=DGRDA+FM(I)*WORK1(I)
      end do ! I=1
!
! Test if the search direction is downhill.
      IF(DGRDA.GE.ZERO)GO TO 320
!
! Set the initial step-length of the line search.
      STMIN=ZERO
      STEPBD=ZERO
      STEPLB=OPTACC ! Nov. 1st, 2002
!      STEPLB=OPTACC/GSC
      ELBOUND=FVALA
      GMIN=DGRDA
      STEPLN=ONE

      IF(FDIFF.LE.ZERO) then
        STEPLN=DMIN1(STEPLN,ONE/GSC)
      else
        STEPLN=DMIN1(STEPLN,(FDIFF+FDIFF)/(-DGRDA))
      end if

  170 GSC=STMIN+STEPLN
      GO TO 175
!
! Special Iteration - Reset to best parameter set.
  171 GSC=ZERO
      write(UNIout,1000)

  175 continue
!
! Calculate another function value and gradient.
      do I=1,Noptpr
        PARSET(I)=FN(I)+GSC*WORK1(I)
      end do ! I=1

      GO TO 500
!
! Subsequent entries to BFGS start here.
  186 FVALB=OPT_function%value
!
! Is this a special iteration?
      IF(SPECIT.GT.0)GO TO 200
! Store this function value if it is the smallest so far.
      SAVEFV=MIN0(2,SAVEFV)
      IF(FVALB.GT.FVAL0)GO TO 220
      IF(FVALB.LT.FVAL0)GO TO 200
      GL1=ZERO
      GL2=ZERO
      do I=1,Noptpr
        GL1=GL1+(Sscale(I)*BEST_grads(I))**2
        GL2=GL2+(Sscale(I)*PARGRD(I))**2
      end do ! I=1

      IF(GL2.GE.GL1)GO TO 220
  200 SAVEFV=3
      FVAL0=FVALB
      BEST_params(1:Noptpr)=PARSET(1:Noptpr)
      BEST_grads(1:Noptpr)=PARGRD(1:Noptpr)
!
! Branch if just finished a special iteration.
      IF(SPECIT-1)220,250,335
!
! Calculate the directional derivative at the new point.
  220 DGRDB=ZERO
      do I=1,Noptpr
        DGRDB=DGRDB+PARGRD(I)*WORK1(I)
      end do ! I=1
!
! Branch if we have found a new lower bound on the step-length.
      IF(FVALB-FVALA.LE.PT1*GSC*DGRDA)GO TO 280
!
! Finish the iteration if the current step is STEPLB.
      IF(STEPLN.GT.STEPLB)then
! Calculate a new step-length by cubic interpolation.
  270 STEPBD=STEPLN
      GSC=GMIN+DGRDB-THREE*(FVALB-ELBOUND)/STEPLN
      GSC=GMIN/(GSC+GMIN-DSQRT(DMAX1(GSC*GSC-GMIN*DGRDB,ZERO)))
      STEPLN=STEPLN*DMAX1(PT1,GSC)

      GO TO 170
      end if
!
  240 IF(SAVEFV.GE.2)GO TO 110
!
! Step length is converged.  Reset to best parameter set.
      SPECIT=1

      IF(OPT_function%value.ne.FVAL0)GO TO 110
  250 write(UNIout,'(a)')'Step length converged within accuracy'
      write(UNIout,'(a)')'Function is no longer decreasing'
      OPTEND=.TRUE.
      OPT_converged=.true.
      SPECIT=0

      GO TO 400
!
! Set the new bounds on the step-length.
  280 STEPBD=STEPBD-STEPLN
      STMIN=GSC
      ELBOUND=FVALB
      GMIN=DGRDB
!
! Calculate a new step-length by extrapolation.
      STEPLN=ANINE*STMIN
      IF(STEPBD.GT.ZERO)STEPLN=PT5*STEPBD
      GSC=DGRDA+THREE*DGRDB-FOUR*(FVALB-FVALA)/STMIN
      IF(GSC.GT.ZERO)then
        STEPLN=DMIN1(STEPLN,STMIN*DMAX1(ONE,-DGRDB/GSC))
      end if

      IF(DGRDB.LT.PT7*DGRDA)GO TO 170
!
! Test for convergence of the iterations.
      SAVEFV=4-SAVEFV

      IF(STMIN+STEPLN.LE.STEPLB)GO TO 240
!
! Revise the second derivative matrix.
      IR=-Noptpr
      do I=1,Noptpr
        FN(I)=PARSET(I)
        PARSET(I)=FM(I)
        WORK1(I)=PARGRD(I)-FM(I)
        FM(I)=PARGRD(I)
      end do ! I=1

      call MC11AD (Hessian_factored, HESLEN, Noptpr, PARSET, ONE/DGRDA, FK, IR, 1, ZERO)
      IR=-IR
      call MC11AD (Hessian_factored, HESLEN, Noptpr, WORK1, ONE/(STMIN*(DGRDB-DGRDA)), WORK1, IR, 0, ZERO)
!
! Branch if the rank of the new matrix is deficient.
      IF(IR.LT.Noptpr)GO TO 340
!
! Begin another iteration.
      FDIFF=FVALA-FVALB
      FVALA=FVALB
      OPTNSR=0

      GO TO 130
!
! Search direction is uphill - Try to restart.
  320 continue
      IF(SAVEFV.GE.2)GO TO 110
      GO TO 340
!
  335 write(UNIout,1070)
      OPTEND=.TRUE.
      OPT_converged=.true.
      SPECIT=0
!
      GO TO 400
!
! Rank of Hessian is deficient - Try to restart.
  340 OPTNSR=OPTNSR+1

      IF(OPTNSR.GE.3)GO TO 350
      OPTRST=OPTRST+1

      write(UNIout,1010)
!
! Set the Hessian to a diagonal matrix depending on Sscale(I).
   20 GSC=ZERO
      do I=1,Noptpr
        GSC=DMAX1(GSC,DABS(BEST_grads(I)*Sscale(I)))
      end do ! I=1
      IF(GSC.LE.ZERO)GSC=ONE

      Hessian_factored(1:HESLEN)=ZERO

      K=1
      do I=1,Noptpr
         Hessian_factored(K)=PT01*GSC/Sscale(I)**2
         K=K+Noptpr-I+1
      end do ! I=1

      go to 100
!
! 3 successive restarts - Abort optimization.
  350 write(UNIout,'(A/16X,A/16X,A)')'ERROR> BFGS: 3 SUCCESSIVE Restarts - ' &
       //'OPTIMIZATION TERMINATED > THIS MAY BE DUE TO LOSS OF RANK IN THE HESSIAN', &
       ' OR THE SEARCH DIRECTION LYING UPHILL'
      OPTEND=.TRUE.
  400 continue
!
! Copy the parameters into array PARSET.
      PARSET(1:Noptpr)=BEST_params(1:Noptpr)
!
  500 continue
!
 1000 format('Reset to best parameters')
 1010 format('HESSIAN NOT POSITIVE DEFINITE - REPLACED BY DIAGONAL',' MATRIX')
 1070 format('GRADIENT LENGTH CONVERGED WITHIN OPTACC')
!
! End of routine BFGS
      call PRG_manager ('exit', 'BFGS', 'UTILITY')
      return
      END
      subroutine MC11AD (A,    & !
                         NT,   & !
                         N,    & !
                         Z,    & !
                         SIG,  & !
                         W,    & !
                         IR,   & !
                         MK,   & !
                         EPS) !
!***********************************************************************
!     Date last modified: May 10, 1995                     Version 1.0 *
!     Author:                                                          *
!     Description: Update factors given in A by SIG*Z*(Z transpose).   *
!***********************************************************************
! Modules:
      USE program_constants

      implicit none
!
! Input scalars:
      integer IR,MK,N,NT
      double precision EPS,SIG
! Input arrays:
      double precision A(NT),W(N),Z(N)
! Local scalars:
      integer I,IJ,IP,J,MM,NP
      double precision AL,B,GM,R,TI,TIM,V,Y
!
! Begin:
      call PRG_manager ('enter', 'MC11AD', 'UTILITY')
!
      IF(N.LE.1)then
        A(1)=A(1)+SIG*Z(1)**2
        IR=1
        IF(A(1).LE.ZERO)then
          A(1)=ZERO
          IR=0
        end if
      else
!
        NP=N+1
        IF(SIG.GT.ZERO) then
          MM=0
          TIM=ONE/SIG
        else
          IF(SIG.EQ.ZERO.or.IR.EQ.0)GO TO 100
          TI=ONE/SIG
          IJ=1
          IF(MK.NE.0)then
            do I=1,N
              IF(A(IJ).NE.ZERO)TI=TI+W(I)**2/A(IJ)
              IJ=IJ+NP-I
            end do ! I=1
          else
            do I=1,N
              W(I)=Z(I)
            end do ! I=1
            do I=1,N
              IP=I+1
              V=W(I)
              IF(A(IJ).LE.ZERO)then
                W(I)=ZERO
                IJ=IJ+NP-I
              else
                TI=TI+V*V/A(IJ)
                IF(I.NE.N)then
                  do J=IP,N
                    IJ=IJ+1
                    W(J)=W(J)-V*A(IJ)
                  end do ! J=IP
                end if ! I.NE.N
                IJ=IJ+1
              end if
            end do ! I=1
          end if ! MK
!
          IF(IR.LE.0)then
            TI=ZERO
            IR=-IR-1
            MM=1
            TIM=TI
            do I=1,N
              J=NP-I
              IJ=IJ-I
              IF(A(IJ).NE.ZERO)TIM=TI-W(J)**2/A(IJ)
              W(J)=TI
              TI=TIM
            end do ! I=1
          else if(TI.GT.ZERO)then
            TI=EPS/SIG
            IF(EPS.EQ.ZERO)IR=IR-1
            MM=1
            TIM=TI
            do I=1,N
              J=NP-I
              IJ=IJ-I
              IF(A(IJ).NE.ZERO)TIM=TI-W(J)**2/A(IJ)
              W(J)=TI
              TI=TIM
            end do ! I=1
          else if(MK.LE.1)then
            MM=0
            TIM=ONE/SIG
          else
            MM=1
            TIM=TI
            do I=1,N
              J=NP-I
              IJ=IJ-I
              IF(A(IJ).NE.ZERO)TIM=TI-W(J)**2/A(IJ)
              W(J)=TI
              TI=TIM
            end do ! I=1
          end if
        end if
!
        IJ=1
        do I=1,N
          IP=I+1
          V=Z(I)
          IF(A(IJ).LE.ZERO)then                         ! GT changed to LE (RAP Sept 2, 2001)
            IF(IR.GT.0.or.SIG.LT.ZERO.or.V.EQ.ZERO)then
              TI=TIM
              IJ=IJ+NP-I
            else
              IR=1-IR
              A(IJ)=V*V/TIM
              IF(I.NE.N)then
                do J=IP,N
                  IJ=IJ+1
                  A(IJ)=Z(J)/V
                end do ! J=IP
              end if
              GO TO 100
            end if ! IR.GT.0.or.SIG.LT.ZERO.or.V.EQ.ZERO
          else
            AL=V/A(IJ)
!
            IF(MM.LE.0) then
              TI=TIM+V*AL
            else
              TI=W(I)
            end if
!
            R=TI/TIM
            A(IJ)=A(IJ)*R
            IF(R.EQ.ZERO)GO TO 70
            IF(I.EQ.N)GO TO 70
            B=AL/TI
            IF(R.GT.FOUR)then
              GM=TIM/TI
              do J=IP,N
                IJ=IJ+1
                Y=A(IJ)
                A(IJ)=B*Z(J)+Y*GM
                Z(J)=Z(J)-V*Y
              end do ! J=IP
            else
              do J=IP,N
                IJ=IJ+1
                Z(J)=Z(J)-V*A(IJ)
                A(IJ)=A(IJ)+B*Z(J)
              end do ! J=IP
            end if ! R.GT.FOUR
            TIM=TI
            IJ=IJ+1
          end if ! A(IJ).GT.ZERO
        end do ! I=1
   70   IF(IR.LT.0)IR=-IR
!

  100   continue
      end if
!
! End of routine MC11AD
      call PRG_manager ('exit', 'MC11AD', 'UTILITY')
      return
      END
      subroutine MC11BD (A,   & ! Matrix to be factorized
                         NT,  & ! Length of A
                         N,   & ! # rows of A
                         IR) !
!***********************************************************************
!     Date last modified: May 10, 1995                     Version 1.0 *
!     Author:                                                          *
!     Description:                                                     *
!     Factorize a matrix given in A.                                   *
!     H is stored in the factored form L*D*L', where L is a lower      *
!     triangular matrix with unit diagonal (L' is L transpose) and     *
!     D is a diagonal matrix.  Only the lower triangle of L is         *
!     actually stored, with the elements of D being stored in place    *
!     of the diagonal elements of L, which are all one.                *
!                                                                      *
!***********************************************************************
! Modules:
      USE program_constants

      implicit none
!
! Input scalars:
      integer IR,N,NT
! Input array:
      double precision A(NT)
! Local scalars:
      integer I,II,IJ,IK,IP,JK,NI,NP
      double precision AA,V
!
! Begin:
      call PRG_manager ('enter', 'MC11BD', 'UTILITY')
!
      IR=N
      IF(N.GT.1)then
!
        NP=N+1
        II=1
        do I=2,N
          AA=A(II)
          NI=II+NP-I
          IF(AA.LE.ZERO)then
            A(II)=ZERO
            IR=IR-1
            II=NI+1
          else
            IP=II+1
            II=NI+1
            JK=II
            do IJ=IP,NI
              V=A(IJ)/AA
              do IK=IJ,NI
                A(JK)=A(JK)-A(IK)*V
                JK=JK+1
              end do ! IK
              A(IJ)=V
            end do ! IJ=IP
          end if
        end do ! I=2
!
        IF(A(II).LE.ZERO)then
          A(II)=ZERO
          IR=IR-1
        end if
      else
        IF(A(1).LE.ZERO)then
          A(1)=ZERO
          IR=0
        end if
      end if
!
      continue
!
! End of routine MC11BD
      call PRG_manager ('exit', 'MC11BD', 'UTILITY')
      return
      END
      subroutine MC11ED (A,   & !
                         NT,  & !
                         N,   & !
                         Z,   & ! Vector
                         W,   & !
                         IR) !
!***********************************************************************
!     Date last modified: May 10, 1995                     Version 1.0 *
!     Author:                                                          *
!     Description: Multiply a vector Z by the inverse of the factors   *
!                  given in A.                                         *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer IR,N,NT
! Input arrays:
      double precision A(NT),W(N),Z(N)
! Local scalars:
      integer I,II,IJ,IP,I1,J,NIP,NP
      double precision V
!
! Begin:
      call PRG_manager ('enter', 'MC11ED', 'UTILITY')
!
      IF(IR.GE.N)then
        W(1)=Z(1)
        IF(N.GT.1)GO TO 400
        Z(1)=Z(1)/A(1)
      end if
      GO TO 500
!
  400 do I=2,N
        IJ=I
        I1=I-1
        V=Z(I)
        do J=1,I1
          V=V-A(IJ)*Z(J)
          IJ=IJ+N-J
        end do ! J=1
        W(I)=V
        Z(I)=V
      end do ! I=2
      Z(N)=Z(N)/A(IJ)
      NP=N+1
      do NIP=2,N
        I=NP-NIP
        II=IJ-NIP
        V=Z(I)/A(II)
        IP=I+1
        IJ=II
        do J=IP,N
          II=II+1
          V=V-A(II)*Z(J)
        end do ! J=IP
        Z(I)=V
      end do ! NIP=2
!
  500 continue
!
! End of routine MC11ED
      call PRG_manager ('exit', 'MC11ED', 'UTILITY')
      return
      END
      subroutine SET_defaults_BFGS
!***********************************************************************
!     Date last modified: January 10, 2000                 Version 1.0 *
!     Description:                                                     *
!***********************************************************************
! Modules:
      USE program_constants
      USE OPT_defaults
      USE OPT_objects
      USE mod_type_hessian
      USE OPT_BFGS

      implicit none
!
! Local scalars:
      integer I
!
! Local parameters:
!
! Begin:
      call PRG_manager ('enter', 'SET_defaults_BFGS', 'UTILITY')
!
      SAVEFV=1       ! control bfgs save
      SPECIT=0       ! special iteration flag
      FDIFF=0.0D0    ! Difference between A & B Energies
      DGRDA=0.0D0    ! Gradient at point A
      DGRDB=0.0D0    ! Gradient at point B
      GMIN=0.0D0     ! Minimum gradient on trace
      STMIN=0.0D0    ! Minimum trace step
      STEPLN=0.0D0   ! Trace step length
      STEPBD=0.0D0   ! Bound on step
      STEPLB=0.0D0   ! Lower bound on step
      GSC=0.0D0      ! Gradient Scaling Factor (Lower bound on step length)
      FSCAL=0.01D0*Angstrom_to_Bohr  ! parameter scale value

!
! End of routine SET_defaults_BFGS
      call PRG_manager ('exit', 'SET_defaults_BFGS', 'UTILITY')
      return
      END
      subroutine INI_OPT_BFGS
!***********************************************************************************
!     Date last modified:  May 9, 2001                                             *
!     Author:  R. A. Poirier                                                       *
!     Description:  Initialize BFGS optimization                                *
!***********************************************************************************
! Modules:
      USE program_constants
      USE OPT_defaults
      USE OPT_objects
      USE OPT_BFGS
      USE mod_type_hessian

      implicit none
!
! Begin:
      call PRG_manager ('enter', 'INI_OPT_BFGS', 'UTILITY')
!
      call SET_defaults_BFGS
!
! Initialize BFGS
      allocate (FM(1:Noptpr), FN(1:Noptpr), FK(1:Noptpr), Sscale(1:Noptpr), WORK1(1:Noptpr))
      FN(1:Noptpr)=PARSET(1:Noptpr)
      FM(1:Noptpr)=PARGRD(1:Noptpr)
      FK(1:Noptpr)=ZERO
      WORK1(1:Noptpr)=DIAGFC(1:Noptpr)
      Sscale(1:Noptpr)=FSCAL
      FVAL0=OPT_function%value
!
! end of routine
      call PRG_manager ('exit', 'INI_OPT_BFGS', 'UTILITY')
      return
      end
      subroutine END_OPT_BFGS
!***********************************************************************************
!     Date last modified:  May 9, 2001                                             *
!     Author:  R. A. Poirier                                                       *
!     Description:  Initialize BFGS optimization                                   *
!***********************************************************************************
! Modules:
      USE OPT_BFGS

      implicit none
!
! Begin:
      call PRG_manager ('enter', 'END_OPT_BFGS', 'UTILITY')
!
      deallocate (FM, FN, FK, Sscale, WORK1)
!
! end of routine
      call PRG_manager ('exit', 'END_OPT_BFGS', 'UTILITY')
      return
      end
