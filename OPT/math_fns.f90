      subroutine Rosenbrock_TO_OPT
!************************************************************************
!     Date last modified: May 19, 2003                     Version 2.0  *
!     Author: R.A. Poirier                                              *
!     Description: Set Rosenbrock parameters for optimization.          *
!     Define the domain for which the function f(x,y) is valid          *
!************************************************************************
! Modules:
      USE OPT_defaults
      USE OPT_objects

      implicit none
!
! Begin:
      call PRG_manager ('enter', 'Rosenbrock_TO_OPT', 'UTILITY')
!
      if(.not.allocated(PARSET))then
        Noptpr=2
        allocate (OPT_par(Noptpr))
        allocate (PARSET(Noptpr))
        allocate (GA_domain(Noptpr))
        OPT_par(1)%value=Rosenbrock_X
        OPT_par(2)%value=Rosenbrock_Y
        PARSET(1)=Rosenbrock_X
        PARSET(2)=Rosenbrock_Y
        GA_domain(1)%high=100.0D0
        GA_domain(1)%low=-100.0D0
        GA_domain(2)%high=100.0D0
        GA_domain(2)%low=-100.0D0
      end if
!
! End of routine Rosenbrock_TO_OPT
      call PRG_manager ('exit', 'Rosenbrock_TO_OPT', 'UTILITY')
      return
      END
      subroutine rosenbrock_fn
!**************************************************************************
! Description: Takes (x,y) values and evaluates the rosenbrock function   *
!              f(x,y).                                                    *
!**************************************************************************
! Modules:
      USE OPT_objects

      implicit none
!
! Local scalars:
      double precision x, y

! Begin:
      x=OPT_par(1)%value
      y=OPT_par(2)%value
      x = parset(1)
      y = parset(2)
      OPT_function%value = 100*(y-(x**2))**2 + (1-x)**2
      return
      end
      subroutine rosenbrock_grad
!**************************************************************************
! Description:  Takes the (x,y) values and sets the pargrd elements       *
!               to df/dx, df/dy respectively.                             *
!**************************************************************************
! Modules:
      USE OPT_objects

      implicit none

! Local scalars:
      double precision x,y
      double precision dfdx, dfdy

! Begin:
      if(.not.allocated(PARGRD))then
!       allocate (OPT_par(Noptpr))
        allocate (PARGRD(Noptpr))
      end if

      x=OPT_par(1)%value
      y=OPT_par(2)%value
      x = parset(1)
      y = parset(2)

      dfdx = -400*x*(y-(x**2))-2*(1-x)
      dfdy = 200*(y-x**2)

      OPT_par(1)%gradient=dfdx
      OPT_par(2)%gradient=dfdy
      pargrd(1) = dfdx
      pargrd(2) = dfdy

      return
      end
      subroutine rosenbrock_hess
!*************************************************************************
! Description:  Takes the (x,y) values and sets the second derivative    *
!               hessian matrix.                                          *
!*************************************************************************
! Modules:
      USE OPT_objects
      USE mod_type_hessian

      implicit none

! Local scalars:
      double precision x,y
      double precision dfdx2, dfdy2, dfdxdy

      if(.not.allocated(Hessian_matrix))then
        allocate (Hessian_matrix(Noptpr,Noptpr))
      end if

      x=OPT_par(1)%value
      y=OPT_par(2)%value
      x = parset(1)
      y = parset(2)

      dfdx2 = 1200*(x**2)-400*y+2
      dfdy2 = 200
      dfdxdy = -400*x

      Hessian_matrix(1,1) = dfdx2
      Hessian_matrix(1,2) = dfdxdy
      Hessian_matrix(2,1) = dfdxdy
      Hessian_matrix(2,2) = dfdy2

      return
      end
      subroutine ChongZak_TO_OPT
!************************************************************************
!     Date last modified: May 19, 2003                     Version 2.0  *
!     Author: R.A. Poirier                                              *
!     Description: Set ChongZak parameters for optimization.            *
!     Define the domain for which the function f(x,y) is valid          *
!************************************************************************
! Modules:
      USE OPT_defaults
      USE OPT_objects

      implicit none
!
! Begin:
      call PRG_manager ('enter', 'ChongZak_TO_OPT', 'UTILITY')
!
      if(.not.allocated(PARSET))then
        Noptpr=2
        allocate (OPT_par(Noptpr))
        allocate (PARSET(Noptpr))
        allocate (GA_domain(Noptpr))
        OPT_par(1)%value=ChongZak_X
        OPT_par(2)%value=ChongZak_X
        PARSET(1)=ChongZak_X
        PARSET(2)=ChongZak_Y
        GA_domain(1)%high=2.0D0
        GA_domain(1)%low=-2.0D0
        GA_domain(2)%high=2.0D0
        GA_domain(2)%low=-2.0D0
      end if
!
! End of routine ChongZak_TO_OPT
      call PRG_manager ('exit', 'ChongZak_TO_OPT', 'UTILITY')
      return
      END
      subroutine chongzak_fn
!**********************************************************************
! Description:  Takes (x,y) values and sets function value to f(x,y)  *
!**********************************************************************
! Modules:
      use OPT_objects

      implicit none

! Local scalars:
      double precision x, y

! Begin:
      x=OPT_par(1)%value
      y=OPT_par(2)%value
      x = parset(1)
      y = parset(2)
      OPT_function%value = 3*(1-x)**2*exp(-(x**2)-(y+1)**2) &
                         -10*((x*(0.5D0))-(x**3)-(y**4))*exp(-(x**2)-(y**2)) &
                         -(1.0D0/3.0D0)*exp(-(x+1)**2-(y**2))
!      write(6,*)'ChongZak x,y: ',x,y
!      write(6,*)'Chong_Zak OPT_function%value: ',OPT_function%value

      return
      end
      subroutine chongzak_grad
!**********************************************************************
! Description:  Takes the (x,y) values and sets the pargrd elements   *
!               to df/dx, df/dy respectively.                         *
!**********************************************************************
! Modules:
      use OPT_objects

      implicit none

! Local scalars:
      double precision x,y
      double precision temp1, temp2, temp3
      double precision dfdx, dfdy

! Begin:
      if(.not.allocated(PARGRD))then
         allocate(PARGRD(Noptpr))
      end if

      x=OPT_par(1)%value
      y=OPT_par(2)%value
      x = parset(1)
      y = parset(2)

      temp1 = exp(-(x**2)-(y+1)**2)
      temp2 = exp(-(x**2)-(y**2))
      temp3 = exp(-(x+1)**2-(y**2))

      dfdx = -6*(1-x)*temp1 - 6*((1-x)**2)*x*temp1 &
             -10*((0.5D0)-3*(x**2))*temp2 + 20*((0.5D0)*x-(x**3)-(y**4))*x*temp2 &
             -(1.0D0/3.0D0)*(-2*x-2)*temp3
!      write(6,*)'dfdx = ',dfdx

      dfdy = 3*((1-x)**2)*(-2*y-2)*temp1 + 40*(y**3)*temp2 &
             +20*((0.5D0)*x-(x**3)-(y**4))*y*temp2 + (2.0D0/3.0D0)*y*temp3
!      write(6,*)'dfdy = ',dfdy

      OPT_par(1)%gradient=dfdx
      OPT_par(2)%gradient=dfdy
      pargrd(1) = dfdx
      pargrd(2) = dfdy

      return
      end
      subroutine chongzak_hess
!*********************************************************************
! Description:  Takes the (x,y) values and set the second derivative *
!               hessian matrix.                                      *
!*********************************************************************
! Modules:
      use OPT_objects
      USE mod_type_hessian

      implicit none

! Local scalars:
      double precision x,y
      double precision temp1, temp2, temp3
      double precision dfdx2, dfdy2, dfdxdy

      if(.not.allocated(Hessian_matrix))then
         allocate(Hessian_matrix(Noptpr,Noptpr))
      end if

      x=OPT_par(1)%value
      y=OPT_par(2)%value
      x = parset(1)
      y = parset(2)

      temp1 = exp(-(x**2)-(y+1)**2)
      temp2 = exp(-(x**2)-(y**2))
      temp3 = exp(-(x+1)**2-(y**2))

      dfdx2 = 6*temp1 + 24*(1-x)*x*temp1 - 6*((1-x)**2)*temp1 &
              + 12*((1-x)**2)*(x**2)*temp1 + 60*x*temp2 &
              + 40*((0.5D0)-3*(x**2))*x*temp2 &
              + 20*((0.5D0)*x-(x**3)-(y**4))*temp2 &
              - 40*((0.5D0)*x-(x**3)-(y**4))*(x**2)*temp2 &
              + (2.0D0/3.0D0)*temp3 - (1.0D0/3.0D0)*((-2*x-2)**2)*temp3

      dfdy2 = -6*((1-x)**2)*temp1 + 3*((1-x)**2)*((-2*y-2)**2)*temp1 &
              + 120*(y**2)*temp2 - 160*(y**4)*temp2 &
              + 20*((0.5D0)*x-(x**3)-(y**4))*temp2 &
              - 40*((0.5D0)*x-(x**3)-(y**4))*(y**2)*temp2 &
              + (2.0D0/3.0D0)*temp3 - (4.0D0/3.0D0)*(y**2)*temp3

      dfdxdy = -6*(1-x)*(-2*y-2)*temp1 - 6*((1-x)**2)*x*(-2*y-2)*temp1 &
               + 20*((0.5D0)-3*(x**2))*y*temp2 &
               - 80*(y**3)*x*temp2 - 40*((0.5D0)*x-(x**3)-(y**4))*x*y*temp2 &
               + (2.0D0/3.0D0)*(-2*x-2)*y*temp3

      Hessian_matrix(1,1) = dfdx2
      Hessian_matrix(1,2) = dfdxdy
      Hessian_matrix(2,1) = dfdxdy
      Hessian_matrix(2,2) = dfdy2

      return
      end
