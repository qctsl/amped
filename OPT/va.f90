      subroutine VA
!***********************************************************************
!     Date last modified: October  19, 1994                            *
!     Author: R. A. Poirier and Cory C. Pye                                              *
!     Description: Subroutine based on VA05AD minimization of sum of   *
!                  squares technique, for general critical points,     *
!                  with gradients. Hessian evaluation in separate      *
!                  subroutine.                                         *
!***********************************************************************
! Modules:
      USE OPT_defaults
      USE program_constants
      USE matrix_print
      USE OPT_objects
      USE OPT_VA
      USE OPT_DIIS
      USE mod_type_hessian

      implicit none
!
! Local scalars:
      integer I,J,NT,KK,KK2
      double precision FSQ,T,DN,SP,SUMX,SUMF,PRED,DMULT,AP,AD,PTM,ANMULT,SUM,PJ,SS,ST,SPP,DW
!
! Local parameters:
      double precision PT01,PT1,PT25,PT9,SIX,TEN
      parameter (SIX=6.0D0,TEN=10.0D0,PT01=0.01D0,PT1=0.1D0,PT25=0.25D0,PT9=0.9D0)
!
! Begin:
      call PRG_manager ('enter', 'VA', 'UTILITY')
!
      WORK1=PARGRD
!
      if(optitr.gt.0)then
        call OPT_print
      end if
      OPTITR=OPTITR+1 ! Start a new iteration
      Ntrace=0
!
!     Initial entry.
! DIIS here
! Incremment IFEVAL in case switch to DIIS.  That way the last iterate is kept.
!      IFEVAL=IFEVAL+1
!
!     NT - Maximum number of times OPT_function%value can increase in a row.
!
      NT = Noptpr+2
!
!     Calculate the sum of squares.
!
      FSQ=ZERO
      do I=1,Noptpr
        FSQ=FSQ+WORK1(I)*WORK1(I)
      end do ! I
      IF(LCONV)GO TO 7
!
!     Test for error return because WORK1(X) does not decrease.
      IF(ISTEP.NE.2.and.ISTEP.NE.4) then
        IF(FSQ.GE.FMIN) then
          IF(DD.LE.OPTDSS) then
            NTEST=NTEST-1
            IF(NTEST.LE.0) then
              write(UNIout,'(a)')' GRADIENT LENGTH NO LONGER DECREASES ...'
              write(UNIout,'(a)')' *** OPTIMIZATION TERMINATED ***'
              write(UNIout,'(a)')' THIS MAY BE DUE TO THE VALUES OF VAstep_size AND OPTAVA', & 
                                 ' OR TO LOSS OF RANK IN THE JACOBIAN'

              write(UNIout,*)' VA> ISTEP: ',ISTEP
              write(UNIout,*)' VA> FSQ: ',FSQ
              write(UNIout,*)' VA> FMIN: ',FMIN
              write(UNIout,*)' VA> DD: ',DD
              write(UNIout,*)' VA> OPTDSS: ',OPTDSS
              write(UNIout,*)' VA> NTEST: ',NTEST
              OPTEND=.TRUE.
              GO TO 18
            end if ! NTEST.LE.0
          end if ! DD.LE.OPTDSS
        else
          NTEST=NT
        end if ! FSQ.GE.FMIN
      end if ! ISTEP.NE.2.and.ISTEP.NE.4
!
!RAP ISTEP is initially 4!!!
      GO TO (49,47,47,48),ISTEP
!
   48 continue
      if(Ntrace.LT.ITRAMX)then
        Ntrace=Ntrace+1
        ITRACE(Ntrace)='4'
      end if

      FMIN=FSQ
!
!     Print the force constant matrix.
!     Set the direction matrix.
      do I=1,Noptpr
        do J=1,Noptpr
          DirMatrix(J,I)=ZERO
        end do ! J
        DirMatrix(I,I)=ONE
        WC(I)=ONE+DBLE(Noptpr-I)
      end do ! I
!
! Set the Marquardt parameter to its least value.
   24 MARQPM=MQPMIN
!
!     Copy the Jacobian and append the Marquardt matrix.
   25 PPAR=MARQPM*MARQPM
      NTPAR=0
!
!     Symmetrize HESS if LHESSYM is .TRUE. .
   63 IF(LHESSYM) then
        do I=1,Noptpr
          do J=1,I
            T=PT5*(Hessian_matrix(I,J)+Hessian_matrix(J,I))
            Hessian_matrix(I,J)=T
            Hessian_matrix(J,I)=T
          end do ! J
        end do ! I
      end if ! LHESSYM
!
      do I=1,Noptpr
        do J=1,Noptpr
          HESSinv(J,I)=Hessian_matrix(J,I)
        end do ! J
      end do ! I
      do I=1,Noptpr
        do J=1,Noptpr
          HESSinv(J,Noptpr+I)=ZERO
        end do ! J
        HESSinv(I,Noptpr+I)=MARQPM
      end do ! I
!
!     Calculate the generalized inverse of J.
      call MB11MP (Noptpr, NOPTP2, HESSinv, WW)
!
!     Start the iteration by testing FMIN.
   64 continue
      IF(FMIN.LE.OPTAVA) GO TO 7
!
!     Next predict the descent and Marquardt minima.
      DirSQ=ZERO
      DN=ZERO
      SP=ZERO
      do I=1,Noptpr
        SUMX=ZERO
        SUMF=ZERO
        do J=1,Noptpr
          SUMX=SUMX-Hessian_matrix(I,J)*BEST_grads(J)
          SUMF=SUMF-HESSinv(I,J)*BEST_grads(J)
        end do ! J
        DirSQ=DirSQ+SUMX*SUMX
        DN=DN+SUMF*SUMF
        SP=SP+SUMX*SUMF
        PARSET(I)=SUMX
        WORK1(I)=SUMF
      end do ! I
!
!     Predict the reduction in WORK1(X) due to the Marquardt step,
!     and also predict the length of the steepest descent step.
      PRED=SP+SP
      DMULT=ZERO
      do I=1,Noptpr
        AP=ZERO
        AD=ZERO
        do J=1,Noptpr
          AP=AP+Hessian_matrix(J,I)*WORK1(J)
          AD=AD+Hessian_matrix(J,I)*PARSET(J)
        end do ! J
        PRED=PRED-AP*AP
        DMULT=DMULT+AD*AD
      end do ! I
!
!     Test for convergence.
      IF(DN.LE.OPTDM) then
        AP=DSQRT(DN)
        IF(PRED+TWO*PPAR*AP*(OPTDMX-AP).LE.OPTAVA) GO TO 7
      else
        IF(PRED+PPAR*(OPTDM-DN).LE.OPTAVA) GO TO 7
      end if ! DN.LE.ZERO
!
!     Test whether to apply the full Marquardt correction.
      DMULT=DirSQ/DMULT
      DirSQ=DirSQ*DMULT*DMULT

   71 ISTEP=2
!
      IF(DN.GT.DD) GO TO 73
!
!     Test that the Marquardt parameter has its least value.
      IF(MARQPM.GT.MQPMIN)GO TO 24
      DD=DMAX1(DN,OPTDSS)
      DirSQ=PT25*DN
      TINC=ONE
!
      IF(DN.GE.OPTDSS) GO TO 132
      ISTEP=3
!
!     Calculate the next vector X, and then call FSEVAL.
      do I=1,Noptpr
        PARSET(I)=BEST_params(I)+WORK1(I)
      end do ! I
      GO TO 4
!
!     Test whether to increase the Marquardt parameter.
   73 continue
!
      IF(DN.LE.DPAR)then
        NTPAR=0
        GO TO 33
      else if(NTPAR.LE.0) then
        NTPAR=1
        PTM=DN
        GO TO 33
      end if
      NTPAR=NTPAR+1
      PTM=DMIN1(PTM,DN)
!
      IF(NTPAR.GE.NT) then
      if(Ntrace.LT.ITRAMX)then
        Ntrace=Ntrace+1
        ITRACE(Ntrace)='M'
      end if
!
! Set the larger value of the Marquardt parameter.
        MARQPM=MARQPM*(PTM/OPTDM)**PT25
!
        IF(SIX*DD.LT.OPTDM) then
          AP=DSQRT(PRED/DN)
          IF(AP.GT.MARQPM)MARQPM=DMIN1(AP,MARQPM*(OPTDM/(SIX*DD))**PT25)
        end if ! SIX*DD.LT.OPTDM
!
        GO TO 25
      end if ! NTPAR.GE.NT
!
!     Test whether to use the steepest descent direction.
   33 continue
!
      IF(DirSQ.GE.DD)then
!
!     Test whether the initial value of DD has been set.
        IF(DD.LE.0) then
          DD=DMIN1(OPTDM,DirSQ)
          IF(DD.LT.OPTDSS)then
            DD=OPTDSS
            GO TO 71
          end if ! DD.LT.OPTDSS
        end if ! DD.LE.0
!
!     Set the multiplier of the steepest descent direction.
        ANMULT=ZERO
        DMULT=DMULT*DSQRT(DD/DirSQ)
      else
!
!     Interpolate between the steepest descent and Marquardt directions.
        SP=SP*DMULT
        ANMULT=(DD-DirSQ)/((SP-DirSQ)+DSQRT((SP-DD)**2+(DN-DD)*(DD-DirSQ)))
        DMULT=DMULT*(ONE-ANMULT)
      end if ! DirSQ.GE.DD
!
!     Calculate the correction to X, and its angle with the first
!     direction.
      DN=ZERO
      SP=ZERO
      do I=1,Noptpr
        WORK1(I)=DMULT*PARSET(I)+ANMULT*WORK1(I)
        DN=DN+WORK1(I)*WORK1(I)
        SP=SP+WORK1(I)*DirMatrix(I,1)
      end do ! I
      DirSQ=PT25*DN
!
!     Test whether an extra step is needed for independence.
      IF(WC(1).GT.DTEST.and.SP*SP.LT.DirSQ)then
!
!     Take the extra step and update the direction matrix.
        do I=1,Noptpr
          PARSET(I)=BEST_params(I)+VAstep_size*DirMatrix(I,1)
          IF(I.LT.Noptpr)WC(I)=WC(I+1)+ONE
        end do ! I
        WC(Noptpr)=ONE
        IF(Noptpr.GT.1) then
          do I=1,Noptpr
            SP=DirMatrix(I,1)
            do J=2,Noptpr
              DirMatrix(I,J-1)=DirMatrix(I,J)
            end do ! J
            DirMatrix(I,Noptpr)=SP
          end do ! I
        end if ! Noptpr.GT.1
!
        GO TO 4
      end if ! WC(1).GT.DTEST.and.SP*SP.LT.DirSQ
!
!     Express the new direction in terms of those of the direction
!     matrix, and update the counts in WC.
  132 IF(Noptpr.GE.2) then
        SP=ZERO
        DW=ZERO
        do I=1,Noptpr
          PARSET(I)=DW
          DW=ZERO
          do J=1,Noptpr
            DW=DW+WORK1(J)*DirMatrix(J,I)
          end do ! J
          IF(ISTEP.NE.1) then
            WC(I)=WC(I)+ONE
            SP=SP+DW*DW
            IF(SP.GT.DirSQ) then
              ISTEP=1
              KK=I
              PARSET(1)=DW
              IF(I.LT.Noptpr)WC(I)=WC(I+1)+ONE
            end if ! (SP.GT.DirSQ)
          else
            PARSET(I)=DW
            IF(I.LT.Noptpr)WC(I)=WC(I+1)+ONE
          end if ! (ISTEP.NE.1)
        end do ! I
        WC(Noptpr)=ONE
!
!     Reorder the directions so that KK is first.
        IF(KK.GT.1) then
          KK2=KK+2
          do I=1,Noptpr
            SP=DirMatrix(I,KK)
            do J=2,KK
              DirMatrix(I,KK2-J)=DirMatrix(I,KK2-J-1)
            end do ! J
            DirMatrix(I,1)=SP
          end do ! I
        end if ! KK.GT.1
!
!     Generate the new orthogonal direction matrix.
        do I=1,Noptpr
          WW(I)=ZERO
        end do ! I
        SP=PARSET(1)*PARSET(1)
        do I=2,Noptpr
          DirSQ=DSQRT(SP*(SP+PARSET(I)*PARSET(I)))
          DW=SP/DirSQ
          DirSQ=PARSET(I)/DirSQ
          SP=SP+PARSET(I)*PARSET(I)
          do J=1,Noptpr
            WW(J)=WW(J)+PARSET(I-1)*DirMatrix(J,I-1)
            DirMatrix(J,I-1)=DW*DirMatrix(J,I)-DirSQ*WW(J)
          end do ! J
        end do ! I
        SP=ONE/DSQRT(DN)
        do I=1,Noptpr
          DirMatrix(I,Noptpr)=SP*WORK1(I)
        end do ! I
!
      else
        ISTEP=1
!
      end if ! Noptpr.GE.2
!
!     Predict the new right hand sides.
      FNP=ZERO
      do I=1,Noptpr
        SUM=BEST_grads(I)
        do J=1,Noptpr
          SUM=SUM+Hessian_matrix(J,I)*WORK1(J)
        end do ! J
        WW(I)=SUM
        FNP=FNP+SUM*SUM
      end do ! I
!
!     Calculate the next vector X, and then call FSEVAL.
      do I=1,Noptpr
        PARSET(I)=BEST_params(I)+WORK1(I)
      end do ! I
!
      GO TO 4
!
!     Update the step size.
   49 DMULT=PT9*FMIN+PT1*FNP-FSQ
      if(Ntrace.LT.ITRAMX)then
        Ntrace=Ntrace+1
        ITRACE(Ntrace)='S'
      end if
!
      IF(DMULT.LT.ZERO) then
        DD=DMAX1(OPTDSS,PT25*DD)
        TINC=ONE
!
        IF(FSQ.LT.FMIN)then
          FMIN=FSQ
!          do I=1,Noptpr
!            SP=PARSET(I)
!            PARSET(I)=BEST_params(I)
!            BEST_params(I)=SP
!          end do ! I
!          do I=1,Noptpr
!            SP=WORK1(I)
!            WORK1(I)=BEST_grads(I)
!            BEST_grads(I)=SP
!          end do ! I
!
          GO TO 110
        else
          PREVPAR(1:Noptpr)=PARSET(1:Noptpr)
          PREVGRD(1:Noptpr)=WORK1(1:Noptpr)
          GO TO 107
        end if
      else
!
! Try the test to decide whether to increase the step length.
        SP=ZERO
        SS=ZERO
        do I=1,Noptpr
          T=WORK1(I)-WW(I)
          SP=SP+DABS(WORK1(I)*T)
          SS=SS+T*T
        end do ! I
        PJ=ONE+DMULT/(SP+DSQRT(SP*SP+DMULT*SS))
        SP=DMIN1(FOUR,TINC,PJ)
        TINC=PJ/SP
        DD=DMIN1(OPTDM,SP*DD)
!
        FMIN=FSQ
!        do I=1,Noptpr
!          SP=PARSET(I)
!          PARSET(I)=BEST_params(I)
!          BEST_params(I)=SP
!        end do ! I
!        do I=1,Noptpr
!          SP=WORK1(I)
!          WORK1(I)=BEST_grads(I)
!          BEST_grads(I)=SP
!        end do ! I
!
        GOTO 110
      end if ! DMULT.LT.ZERO
!
!     If WORK1(X) improves, store the new value of X.
   47 continue
      if(Ntrace.LT.ITRAMX)then
        Ntrace=Ntrace+1
        ITRACE(Ntrace)='X'
      end if
      IF(FSQ.LT.FMIN)then
        FMIN=FSQ
!        do I=1,Noptpr
!          SP=PARSET(I)
!          PARSET(I)=BEST_params(I)
!          BEST_params(I)=SP
!        end do ! I
!        do I=1,Noptpr
!          SP=WORK1(I)
!          WORK1(I)=BEST_grads(I)
!          BEST_grads(I)=SP
!        end do ! I
!
      else
        PREVPAR(1:Noptpr)=PARSET(1:Noptpr)
        PREVGRD(1:Noptpr)=WORK1(1:Noptpr)
      end if ! FSQ.LT.FMIN
!
  110 continue
      IF(ISTEP.GE.3) then
        ISTEP=2
        IF(FMIN.GT.OPTAVA)then
          do I=1,Noptpr
            PARSET(I)=BEST_params(I)+VAstep_size*DirMatrix(I,1)
            IF(I.LT.Noptpr)WC(I)=WC(I+1)+ONE
          end do ! I
          WC(Noptpr)=ONE
          IF(Noptpr.GT.1) then
            do I=1,Noptpr
              SP=DirMatrix(I,1)
              do J=2,Noptpr
                DirMatrix(I,J-1)=DirMatrix(I,J)
              end do ! J
              DirMatrix(I,Noptpr)=SP
            end do ! I
          end if ! Noptpr.GT.1
!
          GO TO 4
        else
          GO TO 7
        end if ! FMIN.GT.OPTAVA
      end if ! ISTEP.GE.3
!
!     Calculate the changes in PARSET and in PARGRD.
  107 DirSQ=ZERO
!      write(6,*)'107'
      do I=1,Noptpr
!        write(6,*)I,PREVPAR(I),BEST_params(I)
        PARSET(I)=PREVpar(I)-BEST_params(I)
        DirSQ=DirSQ+PARSET(I)*PARSET(I)
      end do ! I
      do I=1,Noptpr
!        write(6,*)I,PREVGRD(I),BEST_grads(I)
        WORK1(I)=PREVGRD(I)-BEST_grads(I)
      end do ! I
!
!     Calculate the generalized inverse times the change in X.
      SS=ZERO
      do I=1,NOPTP2
        SP=ZERO
        do J=1,Noptpr
          SP=SP+HESSinv(J,I)*PARSET(J)
        end do ! J
        WW(Noptpr+I)=SP
        SS=SS+SP*SP
      end do ! I
!
!     Calculate J times the change in F.
!     Also apply projection to the generalized inverse.
      do I=1,Noptpr
        ST=ZERO
        do J=1,NOPTP2
          ST=ST+HESSinv(I,J)*WW(Noptpr+J)
        end do ! J
        ST=ST/SS
        do J=1,NOPTP2
          HESSinv(I,J)=HESSinv(I,J)-ST*WW(Noptpr+J)
        end do ! J
        ST=PPAR*PARSET(I)
        do J=1,Noptpr
          ST=ST+Hessian_matrix(I,J)*WORK1(J)
        end do ! J
        WW(I)=ST
      end do ! I
!
!     Revise J and calculate row vector for correction to inverse.
      SP=ZERO
      SPP=ZERO
      do I=1,Noptpr
        SS=WORK1(I)
        ST=SS
        do J=1,Noptpr
          SS=SS-Hessian_matrix(J,I)*PARSET(J)
          ST=ST-HESSinv(J,I)*WW(J)
        end do ! J
        SS=SS/DirSQ
        WW(Noptpr+I)=ST
        SP=SP+WORK1(I)*ST
        SPP=SPP+ST*ST
        do J=1,Noptpr
          Hessian_matrix(J,I)=Hessian_matrix(J,I)+SS*PARSET(J)
        end do ! J=1
      end do ! I=1
!
      do I=1,Noptpr
        ST=MARQPM*PARSET(I)
        do J=1,Noptpr
          ST=ST-HESSinv(J,Noptpr+I)*WW(J)
        end do ! J
        WW(NOPTP2+I)=ST
        SP=SP+MARQPM*PARSET(I)*ST
        SPP=SPP+ST*ST
      end do ! I

      if(Ntrace.LT.ITRAMX)then
        Ntrace=Ntrace+1
        ITRACE(Ntrace)='U'
      end if
!
!     Test that the scalar product is sufficiently accurate.
      IF(PT01*SPP.LE.DABS(SP-SPP))GO TO 63
!
!     Calculate the new generalized inverse.
      do I=1,Noptpr
        ST=PARSET(I)
        do J=1,Noptpr
          ST=ST-HESSinv(I,J)*WORK1(J)
        end do ! J
        SS=ZERO
        do J=1,Noptpr
          SS=SS+HESSinv(I,Noptpr+J)*PARSET(J)
        end do ! J
        ST=(ST-MARQPM*SS)/SP
        do J=1,NOPTP2
          HESSinv(I,J)=HESSinv(I,J)+ST*WW(Noptpr+J)
        end do ! J
      end do ! I

      if(Ntrace.LT.ITRAMX)then
        Ntrace=Ntrace+1
        ITRACE(Ntrace)='I'
      end if
!
      GO TO 64
!
!     Print final solution.
    7 continue
      OPTEND=.TRUE.
      OPT_converged=.true.
      IF(LCONV)GO TO 19
!
!     Restore the best values of PARSET and F.
   18 do I=1,Noptpr
        WORK1(I)=BEST_grads(I)
        PARSET(I)=BEST_params(I)
      end do ! I
!
      IF(FSQ.GT.FMIN) then
!
!     Reset to the best calculated point.
        write(UNIout,1080)
 1080 FORMAT('0THE FOLLOWING CALCULATION IS FOR THE BEST COORDINATES FOUND SO FAR')
        LCONV=.TRUE.
        GO TO 3
      end if ! FSQ.GE.FMIN
!
   19 continue
      LCONV=.FALSE.
!
!     Print the final force constant matrix estimate.
!      write(UNIout,'(a)')' Hessian in Hartree/bohr**2 '
!      call PRT_matrix (Hessian_matrix, Noptpr, Noptpr)
!
      GO TO 3
!
!     Test whether there have been MAX_optitr iterations.
    4 continue
!
!     Get a new gradient.
    3 continue
!
! End of routine VA
      call PRG_manager ('exit', 'VA', 'UTILITY')
      return
      END
      subroutine MB11MP (Noptpr, N, A, W)
!***********************************************************************
!     Date last modified: May 4, 2001                                  *
!     Author: Mike Peterson, U. of Toronto, Canada/ R.A. Poirier       *
!     Description: Harwell routine MB11AD.                             *
!                  Last library update May 18, 1970.                   *
!     Minor modifications by Cory Pye, Nov 17th, 1994                  *
!       remove arithmetic IF's, replace DO..continue with DO..end do   *
!***********************************************************************
! Modules:
      USE program_constants

      implicit none
!
      integer I,IR,J,KK,KP,Noptpr,N,NCW,NRW
      double precision A(Noptpr,N),BSQ,RMAX,SIGMA,SUM,W(2*N)
!
!     Partition the working space array W.
!     The first partition holds the first components of the vectors of the elementary transformations.
!     The second partition records row interchanges.
      NRW=Noptpr
!     The third partition records column interchanges.
      NCW=Noptpr+Noptpr
!
!     Set the initial records of row and column interchanges.
      do I=1,Noptpr
        W(NRW+I)=PT5+DBLE(I)
      end do
      do I=1,N
        W(NCW+I)=PT5+DBLE(I)
      end do
!     'KK' counts the separate elementary transformations.
      KK=1
!
!     Find largest row and make row interchanges.
    3 RMAX=ZERO
      do I=KK,Noptpr
        SUM=ZERO
        do J=KK,N
          SUM=SUM+A(I,J)**2
        end do ! J=KK
        IF(RMAX.LT.SUM)then
          RMAX=SUM
          IR=I
        end if ! RMAX.LT.SUM
      end do ! I=KK
!
      IF(IR.GT.KK)then
        SUM=W(NRW+KK)
        W(NRW+KK)=W(NRW+IR)
        W(NRW+IR)=SUM
        do J=1,N
          SUM=A(KK,J)
          A(KK,J)=A(IR,J)
          A(IR,J)=SUM
        end do ! J=1
      end if ! IR.GT.KK
!
!     Find largest element of pivotal row, and make column interchanges.
      RMAX=ZERO
      SUM=ZERO
      do J=KK,N
        SUM=SUM+A(KK,J)**2
        IF(RMAX.LT.DABS(A(KK,J))) then
          RMAX=DABS(A(KK,J))
          IR=J
        end if ! RMAX.LT.DABS(A(KK,J))
      end do ! J=KK
!
      IF(IR.GT.KK)then
        RMAX=W(NCW+KK)
        W(NCW+KK)=W(NCW+IR)
        W(NCW+IR)=RMAX
        do I=1,Noptpr
          RMAX=A(I,KK)
          A(I,KK)=A(I,IR)
          A(I,IR)=RMAX
        end do ! I=1
      end if ! IR.GT.KK
!
!     Replace the pivotal row by the vector of the transformation.
      SIGMA=DSQRT(SUM)
      BSQ=DSQRT(SUM+SIGMA*DABS(A(KK,KK)))
      W(KK)=DSIGN(SIGMA+DABS(A(KK,KK)),A(KK,KK))/BSQ
      A(KK,KK)=-DSIGN(SIGMA,A(KK,KK))
      KP=KK+1
      IF(KP.LE.N)then
        do J=KP,N
          A(KK,J)=A(KK,J)/BSQ
        end do
!
!     Apply the transformation to the remaining rows of A.
        IF(KP.LE.Noptpr)then
          do I=KP,Noptpr
            SUM=W(KK)*A(I,KK)
            do J=KP,N
              SUM=SUM+A(KK,J)*A(I,J)
            end do ! J=KP
            A(I,KK)=A(I,KK)-SUM*W(KK)
            do J=KP,N
              A(I,J)=A(I,J)-SUM*A(KK,J)
            end do ! J=KP
          end do ! I=KP
          KK=KP
          GO TO 3
        end if ! KP.LE.Noptpr
      end if ! KP.LE.N
!
!     At this stage the reduction of A is complete. Now we build up the generalized inverse.
!     Apply the first elementary transformation.
      KK=Noptpr
      KP=Noptpr+1
      SUM=W(Noptpr)/A(Noptpr,Noptpr)
      IF(N.GT.Noptpr)then
        do J=KP,N
          A(Noptpr,J)=-SUM*A(Noptpr,J)
        end do ! J=KP
      end if ! N.GT.Noptpr
      A(Noptpr,Noptpr)=ONE/A(Noptpr,Noptpr)-SUM*W(Noptpr)
!
!     Now apply the other (Noptpr-1) transformations.
   36 KP=KK
      KK=KP-1
!      IF(KK)37,37,38
!
      IF(KK.gt.0)then
!     First transform the last (Noptpr-KK) rows.
   38 do I=KP,Noptpr
        SUM=ZERO
        do J=KP,N
          SUM=SUM+A(KK,J)*A(I,J)
        end do ! J=KP
        do J=KP,N
          A(I,J)=A(I,J)-SUM*A(KK,J)
        end do ! J=KP
        W(I)=-SUM*W(KK)
      end do ! I=KP
!
!     Now calculate the new row in position KK.
      do J=KP,N
        SUM=-W(KK)*A(KK,J)
        do I=KP,Noptpr
          SUM=SUM-A(I,KK)*A(I,J)
        end do ! I=KP
        A(KK,J)=SUM/A(KK,KK)
      end do ! J=KP
!
!     Revise the column in position KK.
      SUM=ONE-W(KK)**2
      do I=KP,Noptpr
        SUM=SUM-A(I,KK)*W(I)
        A(I,KK)=W(I)
      end do ! I=KP
      A(KK,KK)=SUM/A(KK,KK)
      GO TO 36
      end if ! KK .lt.0
!
!     Restore the row interchanges.
   37 do I=1,Noptpr
   46   IR=IDINT(W(NRW+I))
        IF(I.LT.IR) then
          SUM=W(NRW+I)
          W(NRW+I)=W(NRW+IR)
          W(NRW+IR)=SUM
          do J=1,N
            SUM=A(I,J)
            A(I,J)=A(IR,J)
            A(IR,J)=SUM
          end do ! J=1
          GO TO 46
        end if ! I.LT.IR
      end do ! I=1,Noptpr
!
!     Restore the column interchanges.
      do J=1,N
   50   IR=IDINT(W(NCW+J))
        IF(J.LT.IR) then
          SUM=W(NCW+J)
          W(NCW+J)=W(NCW+IR)
          W(NCW+IR)=SUM
          do I=1,Noptpr
            SUM=A(I,J)
            A(I,J)=A(I,IR)
            A(I,IR)=SUM
          end do
          GO TO 50
        end if ! J.LT.IR
      end do ! J=1
      return
      END
