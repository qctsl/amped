      module OPT_DIIS
!***********************************************************************
!     Date last modified: May 10, 2001                     Version 2.0 *
!     Author: R.A. Poirier                                             *
!     Description: Work arrays and scalars used by DIIS                *
!***********************************************************************
      implicit none
!
! scalars
      integer :: MAXCUB      ! Cubic iterate tolerance
      integer :: NNEWTONR    ! Number of consecutive Newton-Rhapsons
      double precision :: CONTOL ! Cubic accuracy
      logical :: UPDATE ! Hessian update?
      character(len=8) :: STRATEGY  ! Throw away strategy
!
! Work arrays
      double precision, dimension(:), allocatable :: DELTAX ! change in X
      double precision, dimension(:), allocatable :: DELTAG ! change in G
      double precision, dimension(:), allocatable :: VHDELX ! H*DELTAX
      double precision, dimension(:), allocatable :: CBDIIS ! DIIS Coefficients (errors)
      double precision, dimension(:), allocatable :: DIDIIS !
      double precision, dimension(:), allocatable :: DJDIIS !
      double precision, dimension(:,:), allocatable :: BDIIS ! DIIS B-Matrix (errors)

!
      end module OPT_DIIS
