      subroutine BLD_DHessian
!***********************************************************************
!     Date last modified: July 07, 1992                  Version 1.0   *
!     Author: R. A. Poirier                                            *
!     Description: Initialize diagonal Hessian matrix.                 *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE OPT_defaults
      USE type_molecule
      USE type_basis_set
      USE OPT_objects
      USE mod_type_hessian

      implicit none
!
! Local scalars:
      integer I,K
      double precision FACTOR
      double precision AFCTORS,BFCTORS,CIJMIN,CIJEXT
!
! Local array:
      double precision DIJ(5,5)
!
! Bond Length - Use Badger's Rule to estimate:  R.M. Badger, J. Chem. Phys., 2, 128 (1934).
! The program_constants have been refitted. CIJ and DIJ (Anstroms) are used in Badger's Rule for stretching force
! program_constants. They were fitted to some experimental force program_constants, then CIJ was increased by 10% to adjust
! for the chronic over-estimation by ab initio calculations.
! CIJ for minimal basis sets was found by increasing CIJEXT by 33%.
      data CIJMIN/5.485D0/,CIJEXT/4.124D0/
      data DIJ/-0.128D0,0.177D0,0.368D0,0.414D0,0.530D0,0.177D0,0.562D0,0.763D0,0.853D0,0.996D0,  & ! Angstroms
                0.368D0,0.763D0,1.008D0,1.078D0,1.226D0,0.414D0,0.853D0,1.078D0,1.124D0,1.268D0, &
                0.530D0,0.996D0,1.226D0,1.268D0,1.373D0/
      data AFCTORS/0.0023D0/,BFCTORS/0.07D0/  ! B. Schlegel value
!
! Work array:
      double precision, dimension(:), allocatable :: DFCw
!
! Begin:
      call PRG_manager ('enter', 'BLD_DHessian', 'HESSIAN%GUESS')
!
! Object:
      if(.not.allocated(DIAGFC))then
        allocate (DIAGFC(Noptpr))
      end if
!
      if(MUN_prtlev.gt.0)then
        write(UNIout,'(a/a,F8.4,5X,a,F8.4,5X,a,F8.4)')'INITIAL HESSIAN GUESS CONSTANTS (MDYNE/ANGSTROM):', &
                                                    'BEND:',FBends,'TORSION:',FTorsions,'CARTESIAN:',FCartesians
      end if
      IF(FBends.LE.ZERO.or.FTorsions.LE.ZERO.or.FCartesians.LE.ZERO)then
        write(UNIout,'(a)')'ERROR> BLD_DHessian: Invalid replacement force constant(s) read - VALUES MUST BE GREATER THAN 0.0'
        stop 'ERROR> BLD_DHessian: Invalid replacement force constant(s) read'
      end if
!
      DIAGFC(1:Noptpr)=ONE
!
      IF(OPT_parameters%name(1:3).EQ.'XYZ')then       ! Cartesian coordinates
        DIAGFC(1:Noptpr)=FCartesians/FCconv_str
      else if(OPT_parameters%name(1:4).EQ.'Z-MA')then  ! ZMAT internal coordinates
        call BLD_DHessian_ZM
      else if(OPT_parameters%name(1:3).EQ.'PIC')then ! Proper internal coordinates
!        call BLD_DHessian_PIC
      else if(OPT_parameters%name(1:2).EQ.'MM')then
        DIAGFC(1:Noptpr)=ONE
      else
        DIAGFC(1:Noptpr)=ONE
      end if
!
! End of routine BLD_DHessian
      call PRG_manager ('exit', 'BLD_DHessian', 'HESSIAN%GUESS')
      return
!
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine BLD_DHessian_ZM
!***********************************************************************
!     Date last modified: July 07, 1992                   Version 1.0  *
!     Author: R. A. Poirier                                            *
!     Description: Initialize diagonal force constant matrix for ZMAT. *
!***********************************************************************
! Modules:
      USE type_elements

      implicit none
!
! Local scalars:
      integer Iatom,IA,IB,Ioptpr,IZMVAR,IZVAR,Nstart,N1,N2,K,Z0,Z1,Z2,Z3
      double precision AEX,AS,BEX,BP,BS,CEX,CIJ,CP,CS,FACTOR,DELRAD
!
! Begin:
      call PRG_manager ('enter', 'BLD_DHessian_ZM', 'UTILITY')
!
      call GET_object ('MOL', 'ATOMIC', 'DISTANCES')

      allocate (DFCw(1:Zmatrix%Ndefines))
      DFCw(1:Zmatrix%Ndefines)=ZERO
!
      K=Natoms+Natoms-3
      FACTOR=Angstrom_to_Bohr**2/Fconv
      CIJ=CIJMIN*FACTOR
      IF(Basis%name(1:3).NE.'STO')CIJ=CIJEXT*FACTOR
!
! Must do all bonds first:
      IF(Natoms.GE.2)then
      do Iatom=2,Natoms
! Bond Lengths
        IZVAR=Zmatrix%bond(Iatom)%map
        IF(IZVAR.NE.0)then
        IF(Zmatrix%define(IZVAR)%opt)then
          N1=Zmatrix%atoms(Iatom)%atom1
          Z1=CARTESIAN(N1)%Atomic_number
          Z0=CARTESIAN(Iatom)%Atomic_number
          IF(Z1.LE.0.or.Z0.LE.0)then ! If either atom is a dummy, use FCartesians instead.
            DFCw(IZVAR)=DFCw(IZVAR)+FCartesians/FCconv_str
          else
! Get row of periodic system.
            IA=(Z1+13)/8
            IF(Z1.GT.18)IA=(Z1+53)/18
            IB=(Z0+13)/8
            IF(Z0.GT.18)IB=(Z0+53)/18
            DFCw(IZVAR)=DFCw(IZVAR)+CIJ/((DISMAT(Iatom,N1)-Angstrom_to_Bohr*DIJ(IA,IB))**3)
          end if
        end if ! Zmatrix%define(IZVAR)%opt
        end if ! IZVAR.NE.0
      end do ! Iatom
!
! Must do all bends next:
      IF(Natoms.GE.3)then
      do Iatom=3,Natoms
! Angles Hartrees/A**2 (I hope)
        Z0=CARTESIAN(Iatom)%Atomic_number
        N1=Zmatrix%atoms(Iatom)%atom1
        Z1=CARTESIAN(N1)%Atomic_number
        N2=Zmatrix%atoms(Iatom)%atom2
        Z2=CARTESIAN(N2)%Atomic_number
!        IF(Z0.EQ.0.or.Z1.EQ.0.or.Z2.EQ.0)then
!          FBends=FCartesians/FCconv_str
!        else if(Z0.EQ.1.and.Z2.EQ.1)then
        IF(Z0.EQ.1.or.Z2.EQ.1)then
          FBends=0.160  ! Both terminal atoms are Hydrogen ! B. Schlegel value
        else
          FBends=0.250  ! B. Schlegel value
        end if
!
        IZVAR=Zmatrix%angle(Iatom)%map
        IF(IZVAR.NE.0)then
          IZVAR=IABS(IZVAR)
          IF(Zmatrix%define(IZVAR)%opt)then
            DFCw(IZVAR)=DFCw(IZVAR)+FBends
          end if ! Zmatrix%define(IZVAR)%opt
        end if ! IZVAR.NE.0
      end do ! Iatom
!
! Must do all torsions next:
      IF(Natoms.GE.4)then
      do Iatom=4,Natoms
        N1=Zmatrix%atoms(Iatom)%atom1
        N2=Zmatrix%atoms(Iatom)%atom2
        Z1=CARTESIAN(N1)%Atomic_number
        Z2=CARTESIAN(N2)%Atomic_number
! Torsion Angle - But is it the Second Euler Angle?
        IZVAR=Zmatrix%torsion(Iatom)%map
        IF(IZVAR.NE.0)then
          IZVAR=IABS(IZVAR)
          IF(Zmatrix%define(IZVAR)%opt)then
            IF(Zmatrix%atoms(Iatom)%atom4.NE.0)then
              DFCw(IZVAR)=DFCw(IZVAR)+FBends
            else ! Torsion
              IF(Z1.NE.0.and.Z2.NE.0)then
                DELRAD=DISMAT(N1,N2)-Angstrom_to_Bohr*(BS_RADII(Z1)+BS_RADII(Z2))
                IF(DELRAD.LT.ZERO)then
                  FTorsions=(AFCTORS-BFCTORS*DELRAD)*100.0D0
                else
                  FTorsions=AFCTORS*100.0D0
                end if
              end if
              DFCw(IZVAR)=DFCw(IZVAR)+FTorsions
            end if
          end if ! Zmatrix%define(IZVAR)%opt
        end if ! IZVAR.NE.0
      end do ! Iatom=4
      end if ! Natoms.GE.4
      end if ! Natoms.GE.3
      end if ! Natoms.GE.2
!
      Ioptpr=0
      if(OPT_coord%bonds)then
      do IZMVAR=1,Zmatrix%Nbonds
        if(Zmatrix%define(IZMVAR)%opt)then
          Ioptpr=Ioptpr+1
          DIAGFC(Ioptpr)=DFCw(IZMVAR)
        end if
      end do ! IZMVAR
      end if ! OPT_coord

      if(OPT_coord%angles)then
      Nstart=Zmatrix%Nbonds
      do IZMVAR=Nstart+1,Nstart+Zmatrix%Nangles
        if(Zmatrix%define(IZMVAR)%opt)then
          Ioptpr=Ioptpr+1
          DIAGFC(Ioptpr)=DFCw(IZMVAR)
        end if
      end do ! IZMVAR
      end if ! OPT_coord

      if(OPT_coord%torsions)then
      Nstart=Zmatrix%Nbonds+Zmatrix%Nangles
      do IZMVAR=Nstart+1,Nstart+Zmatrix%Ntorsions
        if(Zmatrix%define(IZMVAR)%opt)then
          Ioptpr=Ioptpr+1
          DIAGFC(Ioptpr)=DFCw(IZMVAR)
        end if
      end do ! IZMVAR
      end if ! OPT_coord

      if(Ioptpr.ne.Noptpr)then
        write(UNIout,*)'ERROR> BLD_DHessian_ZM: failure with Zmatrix%define'
        stop 'error> BLD_DHessian_ZM: failure with Zmatrix%define'
      end if

      deallocate (DFCw)
!
! End of routine BLD_DHessian_ZM
      call PRG_manager ('exit', 'BLD_DHessian_ZM', 'UTILITY')
      return
      end subroutine BLD_DHessian_ZM
      end subroutine BLD_DHessian
