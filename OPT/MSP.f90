      subroutine OPT_MSP
!***********************************************************************************
!     Date last modified:  May 3, 2001                                             *
!     Author:  Sharene Bungay and R. A. Poirier                                    *
!     Description:  Subroutine based on the Newton-Rhapson method using a TS-BFGS  *
!     update                                                                       *
!***********************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE matrix_print
      USE OPT_defaults
      USE OPT_objects
      USE opt_tsbfgs
      USE mod_type_hessian

      implicit none
!
! Local scalars:
      integer Ioptpr,J,I
      double precision :: Max_step
!
! Begin:
      call PRG_manager ('enter', 'OPT_MSP', 'UTILITY')
!
! Set current parameters and gradients as best (for printing)
      Ntrace=1

      if(OPTITR.GT.0)then
        call OPT_print
      end if
      deltaHESS=ZERO
!
! Increment the iteration number
      OPTITR = OPTITR + 1
!
! Update the Hessian using TS-BFGS update
      if(LUpdate)then
        call UPDATE_OPT_MSP (PARSET, PARGRD, Hessian_matrix, PREVPAR, PREVGRD, deltaHESS, Noptpr)
      end if

! Save values to form deltax and deltag on next iteration
      PREVPAR(1:Noptpr) = PARSET(1:Noptpr)
      PREVGRD(1:Noptpr) = PARGRD(1:Noptpr)
!
! Find INVHESS*PARGRD for Newton Raphson step, i.e.. solve HESS*y = PARGRD for y
      y=ZERO
      call sys_solve (Hessian_matrix+deltaHESS, PARGRD, Noptpr, y)
!
      do Ioptpr=1,Noptpr
      if(dabs(y(Ioptpr)).GT.MAXstepsize)then
        Lupdate=.false.
        write(UNIout,'(a,i10)')'MSP> Warning MAXstepsize exceeded for parameter: ',Ioptpr
        write(UNIout,'(a,F20.6)')'MSP> Step size: ',y(Ioptpr)
        y(Ioptpr)=dsign(MAXstepsize, y(Ioptpr))
        write(UNIout,'(a,F20.6)')'MSP> Step size set to: ',y(Ioptpr)
      end if
      end do
!
! Only updated Hessian if MAXstepsize not exceeded
      if(Lupdate)then
        Hessian_matrix = Hessian_matrix + deltaHESS
      end if
!
! Do a Newton-Raphson step
      PARSET = PARSET - y
      LUpdate=.true.
!
! end of routine
      call PRG_manager ('exit', 'OPT_MSP', 'UTILITY')
      return
      end
      subroutine UPDATE_OPT_MSP (PARSET,     & ! Optimizable parameters
                                 PARGRD,     & ! gradients
                                 Hessian_matrix,       & ! Hessian approximation
                                 PREVPAR, PREVGRD, deltaHESS, &
                                 Noptpr)    ! Number of parameters
!*****************************************************************************************
!  Date last modified:  May 3, 2001                                                      *
!  Author:  R. A. Poirier                                                                *
!  Description:  Implement hybrid Murtagh-Sargent/Powell Hessian update                  *
!*****************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE OPT_defaults
      USE matrix_print
      USE opt_tsbfgs

      implicit none
!
! Input Scalars:
      integer Noptpr
!
! Input Arrays:
      double precision PARSET(Noptpr),PARGRD(Noptpr),Hessian_matrix(Noptpr,Noptpr)
      double precision PREVPAR(Noptpr),PREVGRD(Noptpr),deltaHESS(Noptpr,Noptpr)
!
!
! Local scalars:
      integer Ioptpr, Joptpr
      double precision, dimension(1,1) :: phi,scalar_JJ,scalar_Jx,scalar_xx
      double precision :: Jx,JJxx,xx

      double precision :: Tsmall
      parameter (Tsmall=1.0D-10)
!
! Begin
      deltax(1:Noptpr,1) = PARSET(1:Noptpr) - PREVPAR(1:Noptpr)
      deltag(1:Noptpr,1) = PARGRD(1:Noptpr) - PREVGRD(1:Noptpr)
!      write(6,*) 'deltax : ', deltax
!      write(6,*) 'deltag : ', deltag

      Jvec = deltag - matmul(Hessian_matrix, deltax)
      scalar_Jx=matmul(transpose(Jvec), deltax)
      scalar_xx=matmul(transpose(deltax), deltax)
      scalar_JJ=matmul(transpose(Jvec), Jvec)
!
!      write(6,*)'scalar_Jx: ',scalar_Jx(1,1)
!      write(6,*)'scalar_xx: ',scalar_xx(1,1)
!      write(6,*)'scalar_JJ: ',scalar_JJ(1,1)
! Begin OPT_MSP formula

! Construct phi as the square cosine of the angle between deltax and j vectors
      JJxx=scalar_xx(1,1)*scalar_JJ(1,1)
      Jx=scalar_Jx(1,1)
      xx=scalar_xx(1,1)

      if(xx.le.Tsmall)then
        write(UNIout,'(a)')'WARNING> MSP: Hessian not updated'
        write(6,*)'PARSET: ',PARSET
        write(6,*)'PREVPAR: ',PREVPAR
        deltaHESS = ZERO
      else
        phi(1,1)= ONE-Jx**2/JJxx
        frac1 = (ONE/Jx)*matmul(Jvec, transpose(Jvec))
        frac2 = (Jx/xx**2)*matmul(deltax, transpose(deltax))
        frac2 = -frac2+(ONE/xx)*(matmul(Jvec, transpose(deltax)) + matmul(deltax, transpose(Jvec)))
        deltaHESS = (ONE-phi(1,1))*frac1 + phi(1,1)*frac2
      end if

! Write the scalars:
!      write(6,'(4(a,1PE10.2))')'phi: ',phi(1,1)
!      write(6,*)
!      write(6,*)'frac1 = '
!      call PRT_matrix (frac1, Noptpr, Noptpr)
!      write(6,*)'frac2 = '
!      call PRT_matrix (frac2, Noptpr, Noptpr)
!
!      write(6,*)' MSP> ****CHANGES TO THE HESSIAN****'
!      call PRT_matrix (deltaHESS, Noptpr, Noptpr)
      return
      end
      subroutine INI_OPT_MSP
!***********************************************************************************
!     Date last modified:  May 9, 2001                                             *
!     Author:  R. A. Poirier                                                       *
!     Description:  Initialize TS-BFGS optimization                                *
!***********************************************************************************
! Modules:
      USE program_files
      USE OPT_defaults
      USE OPT_objects
      USE opt_tsbfgs

      implicit none
!
! Begin:
      call PRG_manager ('enter', 'INI_OPT_MSP', 'UTILITY')
!
      write(UNIout,'(a,f12.5)')'MAXstepsize:',MAXstepsize
      LUpdate=.false. ! No update on the first entry

      allocate (deltax(Noptpr,1), deltag(Noptpr,1), Jvec(Noptpr,1))
      allocate (y(Noptpr))
      allocate (deltaHESS(Noptpr,Noptpr), frac1(Noptpr,Noptpr), frac2(Noptpr,Noptpr))

! end of routine
      call PRG_manager ('exit', 'INI_OPT_MSP', 'UTILITY')
      return
      end
      subroutine END_OPT_MSP
!***********************************************************************************
!     Date last modified:  May 9, 2001                                             *
!     Author:  R. A. Poirier                                                       *
!     Description:  Initialize TS-BFGS optimization                                *
!***********************************************************************************
! Modules:
      USE OPT_objects
      USE opt_tsbfgs

      implicit none
!
! Begin:
      call PRG_manager ('enter', 'END_OPT_MSP', 'UTILITY')
!
      deallocate (y)
      deallocate (Jvec, deltaHESS, frac1, frac2)
      deallocate (deltax, deltag)

! end of routine
      call PRG_manager ('exit', 'END_OPT_MSP', 'UTILITY')
      return
      end
