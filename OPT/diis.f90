      subroutine SAVE_for_DIIS
!*************************************************************************************************************************
!     Date last modified: July 25, 2002                                                                     Version 1.2  *
!     Author: R.A. Poirier                                                                                               *
!     Description: Save parameters and gradients for possible use by DIIS.                                               *
!*************************************************************************************************************************
! Modules:
      USE program_files
      USE OPT_defaults
      USE OPT_objects
      USE OPT_DIIS

      implicit none
!
! Update the parameter and gradient lists for potential use by DIIS
      if(IFeval.lt.DISITM)then
        IFEVAL=IFEVAL+1
        XDIIS(1:Noptpr,IFEVAL)=PARSET(1:Noptpr)
        GDIIS(1:Noptpr,IFEVAL)=PARGRD(1:Noptpr)
        ARRENG(IFEVAL)=OPT_function%value
        ARRGRD(IFEVAL)=GRDLTH
      else
      write(UNIout,'(2a/)')'NOTE: dimension of DIIS arrays exceeded - ', &
                           'old parameter set eliminated'
        call THROW_away
      end if
      IFevalP1=IFeval+1
!
      return
      end subroutine SAVE_for_DIIS
      subroutine DIIS
!***********************************************************************
!     Date last modified: June 10, 1992                   Version 1.0  *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!     Optimization using Pulay's GDIIS method.                         *
!     Ref: P. Csaszar and P. Pulay, J. Mol. Struct, 114 (1984) 31      *
!          T. H. Fischer, J. Almlof, J. Phys. Chem., 96 (1992) 9768    *
!     Array dimensions:                                                *
!     PARSET - Current parameter set (Noptpr).                         *
!     PARGRD - Current gradient vector (Noptpr).                       *
!     IHessian_matrix - Hessian inverse approximation (Noptpr,Noptpr)           *
! 14 Deb 1995 - CCP - Added BFGS update.                               *
!  7 Feb 1995 - CCP - Added throwaway strategy stuff.                  *
!  1 Dec 1994 - CCP - Hessian made from diagonal into full matrix.     *
!                                                                      *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE OPT_defaults
      USE matrix_print
      USE OPT_objects
      USE mod_type_hessian
      USE OPT_DIIS

      implicit none
!
! Local scalars:
      integer I,IER,J,K,NEX1,L,THROW,THROW2
      double precision FIVM4,SI,SJ,SMIN,SUM,SUMSQ,TENM4,TENP10,THRGRD,THRENG,THRERR,ALPHA,ALPDV
!
      parameter (TENP10=1.0D10,TENM4=1.0D-4,FIVM4=5.0D-4)
!
! Begin:
      call PRG_manager ('enter', 'DIIS', 'UTILITY')
!
      if(IFEVAL.GT.1)then
        call OPT_print
      else ! for now
        Itrace(1:ITRAMX)=' '
        Ntrace=1
      end if
!
      NEX1=DISITM-1
      OPTITR=OPTITR+1
!
! Initialize work arrays
      DELTAX(1:Noptpr)=ZERO
      DELTAG(1:Noptpr)=ZERO
      VHDELX(1:Noptpr)=ZERO
!
      if(IFEVAL.GT.NEX1)GO TO 900
!
!RAP (changed optitr to IFEVAL)
! At first function evaluation do a Newton-Raphson type step
      if(IFEVAL.EQ.1)then
      if(TS_BFGS_update)then
        call INI_OPT_TSBFGS
        PREVPAR(1:Noptpr)=PARSET(1:Noptpr)
        PREVGRD(1:Noptpr)=PARGRD(1:Noptpr)
      end if
!
        do I=1,Noptpr
          DELTAX(I)=ZERO
          do J=1,Noptpr
            DELTAX(I)=DELTAX(I)-PARGRD(J)*IHessian_matrix(I,J)
          end do
        end do
!
        if(LCUBIC)then
          call HUPDATE_cubic (pargrd, Noptpr)
        end if
!
        do I=1,Noptpr
          PARSET(I)=PARSET(I)+DELTAX(I)
        end do ! I=1
!
! End of routine DIIS
      call PRG_manager ('exit', 'DIIS', 'UTILITY')
      return
      end if ! IFEVAL.EQ.1
!
      if(TS_BFGS_update)then
      write(UNIout,'(a)')'Updating Hessian using the TS-BFGS update:'
      deltaHESS=ZERO
      call UPDATE_TS_BFGS (PARGRD, PARSET, IHessian_matrix, PREVGRD, PREVPAR, deltaHESS, Noptpr) ! Switch in parset and pargrd
      IHessian_matrix = IHessian_matrix + deltaHESS
      PREVPAR(1:Noptpr)=PARSET(1:Noptpr)
      PREVGRD(1:Noptpr)=PARGRD(1:Noptpr)
!
! Update Hessian by BFGS formula if requested.
      else if(UPDATE)then
!
! Calculate delta X, delta G and alpha.
        ALPHA=ZERO
        do I=1,Noptpr
          DELTAX(I)=PARSET(I)-XDIIS(I,IFEVAL-1)
          DELTAG(I)=PARGRD(I)-GDIIS(I,IFEVAL-1)
          ALPHA=ALPHA+DELTAX(I)*DELTAG(I)
        end do ! I=1
        ALPHA=ONE/ALPHA
!
! Calculate v, alpha(1+alpha*deltaX'*v)
        ALPDV=ZERO
        do I=1,Noptpr
          VHDELX(I)=ZERO
          do J=1,Noptpr
            VHDELX(I)=VHDELX(I)+IHessian_matrix(I,J)*DELTAG(J)
          end do ! J=1
          ALPDV=ALPDV+VHDELX(I)*DELTAG(I)
        end do ! I=1
        ALPDV=ALPHA*(ONE+ALPHA*ALPDV)
!
!       Update Hessian by BFGS formula.
        do I=1,Noptpr
          do J=1,Noptpr
            IHessian_matrix(I,J)=IHessian_matrix(I,J)+ALPDV*DELTAX(I)*DELTAX(J)-ALPHA*(DELTAX(I)*VHDELX(J)+DELTAX(J)*VHDELX(I))
          end do
        end do
      end if ! UPDATE
!
      if(IFEVALP1.GT.Noptpr)then
      write(UNIout,'(2a/)')'NOTE: Problem has become overdetermined - ', &
                           'old parameter set eliminated'
        call THROW_AWAY
        IFEVALP1=IFEVAL+1
      end if ! (IFEVALP1.GT.Noptpr)
!
! Construct the B-matrix
      do I=1,IFEVAL ! Errors DI at FE i
        do K=1,Noptpr
          DIDIIS(K)=ZERO
          do J=1, Noptpr
            DIDIIS(K)=DIDIIS(K)+IHessian_matrix(K,J)*GDIIS(J,I)
          end do ! J
        end do ! K

        do J=1,I  ! Errors DJ at FE j
          do K=1,Noptpr
            DJDIIS(K)=ZERO
            do L=1,Noptpr
              DJDIIS(K)=DJDIIS(K)+IHessian_matrix(K,L)*GDIIS(L,J)
            end do ! L
          end do ! K

          SUM=ZERO
          do K=1,Noptpr
            SUM=SUM+DIDIIS(K)*DJDIIS(K)
          end do ! K
          BDIIS(I,J)=SUM
          BDIIS(J,I)=SUM
        end do ! J
        BDIIS(I,IFEVALP1)=ONE
        BDIIS(IFEVALP1,I)=ONE
      end do ! I

      BDIIS(IFEVALP1,IFEVALP1)=ZERO
!
! Find SMIN - the smallest diagonal element
      SMIN=TENP10
! Invert the B matrix and find CB's.

      do I=1,IFEVAL
        DIDIIS(I)=ONE/DSQRT(DABS(BDIIS(I,I)))
      end do ! I
      DIDIIS(IFEVALP1)=ONE

      do I=1,IFEVALP1
        do J=I,IFEVALP1
          BDIIS(I,J)=BDIIS(I,J)*DIDIIS(I)*DIDIIS(J)
          BDIIS(J,I)=BDIIS(I,J)
          if(DABS(BDIIS(I,J)).LT.SMIN.and.I.NE.J)SMIN=BDIIS(I,J)
        end do ! J=1
      end do ! I=1
!
      call INVERT (BDIIS, IFEVALP1, DISITM, IER)
      if(IER.NE.0)then
        write(UNIout,'(a)')'ERROR> DIIS: INVERT FAILED'
        OPTEND=.TRUE.
!
! End of routine DIIS
        call PRG_manager ('exit', 'DIIS', 'UTILITY')
        return
      end if
!
! Undo scaling
      do I=1,IFEVALP1
        do J=I,IFEVALP1
          BDIIS(I,J)=BDIIS(I,J)*DIDIIS(I)*DIDIIS(J)
          BDIIS(J,I)=BDIIS(I,J)
        end do ! J=1
      end do ! I=1
!
      do I=1,IFEVAL
        CBDIIS(I)=BDIIS(I,IFEVALP1)
      end do
!
      do I=1,Noptpr
        SI=ZERO
        SJ=ZERO
        do J=1,IFEVAL
          SI=SI+CBDIIS(J)*XDIIS(I,J)
          SJ=SJ+CBDIIS(J)*GDIIS(I,J)
        end do ! J=1
        DIDIIS(I)=SI
        DJDIIS(I)=SJ
        PARSET(I)=DIDIIS(I)
      end do ! I=1
!
        do I=1,Noptpr
          DELTAX(I)=ZERO
          do J=1,Noptpr
            DELTAX(I)=DELTAX(I)-DJDIIS(J)*IHessian_matrix(I,J)
          end do
        end do
!
! Update Parameter set.
      if(LCUBIC)then
        call HUPDATE_cubic (DJDIIS, Noptpr)
      end if

      do I=1,Noptpr
        PARSET(I)=PARSET(I)+DELTAX(I)
      end do ! I=1
!
! End of routine DIIS
      call PRG_manager ('exit', 'DIIS', 'UTILITY')
      return
! Error exits.
!
  900 write(UNIout,'(a)')'ERROR> DIIS: MAXIMUM ITERATION COUNT EXCEEDED - '// &
       'OPTIMIZATION TERMINATED'
      OPTEND=.TRUE.
!
! End of routine DIIS
      call PRG_manager ('exit', 'DIIS', 'UTILITY')
      return
      END
      subroutine HUPDATE_cubic (pargrd, Noptpr)
!***********************************************************************
!     Date last modified: February  16, 1995              Version 1.0  *
!     Author: Cory C. Pye                                              *
!     Description: Calculates step for DIIS with cubic correction.     *
!     Ref: P. Pulay, G. Fogarazi, F. Pang, J. E. Boggs, J. Am. Chem.   *
!          Soc., 101 (1979) 2550 (see p 2557)                          *
! 16 Feb 1995 - CCP - Created                                          *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE OPT_defaults
      USE mod_type_hessian
      USE OPT_DIIS

      implicit none
!
! Input scalars:
      integer Noptpr
!
! Input arrays:
      double precision pargrd(Noptpr)
!
! Local scalars:
      integer I,J,ILOOP
      double precision TEMP
      logical CONVERGED
!
! Begin:
      call PRG_manager ('enter', 'HUPDATE_cubic', 'UTILITY')
!
! Step 1: calculate normal Newton-Raphson type step.
!
! Now calculate cubic correction iteratively.
      CONVERGED=.FALSE.
      ILOOP=1
      do while(.not.CONVERGED.and.ILOOP.LE.MAXCUB)
!
! Step 2: Calculate anharmonic correction to gradients
        do I=1,Noptpr
          DELTAG(I)=STHIRD(I,I)*DELTAX(I)*DELTAX(I)
          do J=1,Noptpr
            DELTAG(I)=DELTAG(I)+STHIRD(I,J)*DELTAX(I)*DELTAX(J)+STHIRD(J,I)*DELTAX(J)*DELTAX(J)*PT5
          end do
        end do
!
! Step 3: Calculate anharmonic step
        do I=1,Noptpr
          VHDELX(I)=DELTAX(I)
          DELTAX(I)=ZERO
            do J=1,Noptpr
              DELTAX(I)=DELTAX(I)-IHessian_matrix(I,J)*(PARGRD(J)+DELTAG(J))
          end do
        end do
!
! Check for convergence.
        TEMP=ZERO
        do I=1,Noptpr
          TEMP=TEMP+(DELTAX(I)-VHDELX(I))*(DELTAX(I)-VHDELX(I))
        end do

        TEMP=SQRT(TEMP)
        CONVERGED=TEMP.LT.CONTOL
!
        ILOOP=ILOOP+1
!
! Safeguard against oscillation in case FPI diverges slowly.
!        do I=1,Noptpr
!          DELTAX(I)=PT5*(DELTAX(I)+VHDELX(I))
!        end do

      end do ! .not.CONVERGED.and.ILOOP.LE.MAXCUB
!
      if(.not.CONVERGED) then
        write(UNIout,1000) MAXCUB
        write(UNIout,1005)
        STOP
      else
        write(UNIout,1010) ILOOP
        write(UNIout,1020) TEMP
      end if ! .not.CONVERGED
!
! End of routine HUPDATE_cubic
      call PRG_manager ('exit', 'HUPDATE_cubic', 'UTILITY')
      return
 1000 FORMAT ('0CUBIC ITERATE STEP NOT CONVERGED AFTER ITERATION ',I3)
 1005 FORMAT ('0USING NORMAL NEWTON-RAPHSON-type STEP')
 1010 FORMAT ('0CUBIC ITERATE STEP CONVERGED AFTER ITERATION ',I3)
 1020 FORMAT ('CONVERGENCE = ',E9.2)
      END
      subroutine THROW_AWAY
!***********************************************************************
!     Date last modified: April 28, 1998                  Version 1.0  *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!                                                                      *
!***********************************************************************
! Modules:
      USE program_files
      USE OPT_defaults
      USE OPT_objects
      USE OPT_DIIS

      implicit none
!
! Local scalars:
      integer I,K,THROW,THROW2
      double precision THRENG,THRERR,THRGRD
!
! Begin:
      call PRG_manager ('enter', 'THROW_AWAY', 'UTILITY')
!
! Throw away 'worst' information based on strategy given.
!
! Strategies:
!   OLDEST - throw away first point. (only strategy in old MUNgauss, and
!          - only if Noptpr < DISITM )
!   OPT_function%value - throw away highest OPT_function%value point.
!   GRAD   - throw away point with largest gradient. (for transition states?)
!   ERROR  - throw away point with largest error vector |Hg| .
!
      if(STRATEGY(1:6).EQ.'OLDEST')then
        THROW=1
      else if(STRATEGY(1:8).EQ.'FUNCTION') then
        THROW=1
        THRENG=ARRENG(1)
        do I=2,MIN(IFEVAL,DISITM)
          if(ARRENG(I).GE.THRENG) then
            THROW=I
            THRENG=ARRENG(I)
          end if
        if(I.NE.MIN(IFEVAL,DISITM))THROW2=THROW
        end do
      else if(STRATEGY(1:4).EQ.'GRAD') then
        THROW=1
        THRGRD=ARRGRD(1)
        do I=2,MIN(IFEVAL,DISITM)
          if(ARRGRD(I).GE.THRGRD) then
            THROW=I
            THRGRD=ARRGRD(I)
          end if
        if(I.NE.MIN(IFEVAL,DISITM))THROW2=THROW
        end do
      else if(STRATEGY(1:5).EQ.'ERROR') then
        THROW=1
        THRERR=BDIIS(1,1)
        do I=2,MIN(IFEVAL,DISITM)
          if(BDIIS(I,I).GE.THRERR) then
            THROW=I
            THRERR=BDIIS(I,I)
          end if
        if(I.NE.MIN(IFEVAL,DISITM))THROW2=THROW
        end do
      else
        write(UNIout,*) 'ILLEGAL DIIS THROWAWAY METHOD - USING DEFAULT'
        THROW=1
      end if
!
! Check to see if point thrown out is current point. If it is, throw away second-worst point.
      if(THROW.EQ.MIN(IFEVAL,DISITM)) then
        write(UNIout,'(a)')'last POINT IS WORST - THROWING AWAY FIRST INSTEAD'
!       THROW=1
        THROW=THROW2
      end if
!
      IFEVAL=IFEVAL-1
!
      do I=THROW,IFEVAL
        do K=1,Noptpr
          GDIIS(K,I)=GDIIS(K,I+1)
          XDIIS(K,I)=XDIIS(K,I+1)
        end do
        ARRGRD(I)=ARRGRD(I+1)
        ARRENG(I)=ARRENG(I+1)
      end do
!
! End of routine THROW_AWAY
      call PRG_manager ('exit', 'THROW_AWAY', 'UTILITY')
      return
      END
      subroutine INI_OPT_DIIS
!***********************************************************************************
!     Date last modified:  May 11, 2001                                             *
!     Author:  R. A. Poirier                                                       *
!     Description: Initialize DIIS optimization                                    *
!***********************************************************************************
! Modules:
      USE program_constants
      USE OPT_defaults
      USE OPT_objects
      USE OPT_DIIS

      implicit none
!
! Begin:
      call PRG_manager ('enter', 'INI_OPT_DIIS', 'UTILITY')
!
      DISITM=max(Noptpr, DISITM) ! Set the dimensions of the DIIS arrays to be .ge. Noptpr
      IFEVAL=0       ! Number of function evaluations (element)
      IFEVALP1=IFEVAL+1
!
!      call GET_object ('OPT:HESSIAN%INVERSE')
!      if(LCUBIC)then
!        call GET_object ('QM:THIRD_DERIVATIVES%SEMI_DIAGONAL')
!      end if

      if(.not.allocated(DELTAX))then
        allocate (DELTAX(Noptpr), DELTAG(Noptpr), VHDELX(Noptpr), ARRENG(1:DISITM), &
                  ARRGRD(1:DISITM), BDIIS(1:DISITM,1:DISITM), &
                  DIDIIS(1:Noptpr), DJDIIS(1:Noptpr), CBDIIS(1:DISITM))
        ARRENG(1:DISITM)=ZERO
        ARRGRD(1:DISITM)=ZERO
        BDIIS(1:DISITM,1:DISITM)=ZERO
        DIDIIS(1:Noptpr)=ZERO
        DJDIIS(1:Noptpr)=ZERO
        CBDIIS(1:DISITM)=ZERO
      end if
!
! May have been allocated if SWITCH_to='DIIS'
      if(.not.allocated(XDIIS))then
        allocate (XDIIS(1:Noptpr,1:DISITM), GDIIS(1:Noptpr,1:DISITM))
        XDIIS(1:Noptpr,1:DISITM)=ZERO
        GDIIS(1:Noptpr,1:DISITM)=ZERO
      end if
!
! end of routine
      call PRG_manager ('exit', 'INI_OPT_DIIS', 'UTILITY')
      return
      end
      subroutine END_OPT_DIIS
!***********************************************************************************
!     Date last modified:  May 9, 2001                                             *
!     Author:  R. A. Poirier                                                       *
!     Description:  Terminate DIIS optimization                                    *
!***********************************************************************************
! Modules:
      USE OPT_defaults
      USE OPT_objects
      USE OPT_DIIS

      implicit none
!
! Begin:
      call PRG_manager ('enter', 'END_OPT_DIIS', 'UTILITY')
!
      deallocate (DELTAX, DELTAG, VHDELX, ARRENG, ARRGRD, BDIIS, DIDIIS, DJDIIS, CBDIIS)
!
      if(allocated(XDIIS))then
        deallocate (XDIIS, GDIIS)
      end if

      if(TS_BFGS_update)call END_OPT_TSBFGS
!
! end of routine
      call PRG_manager ('exit', 'END_OPT_DIIS', 'UTILITY')
      return
      end
      subroutine SET_defaults_DIIS
!***********************************************************************
!     Date last modified: January 10, 2000                 Version 1.0 *
!     Description:                                                     *
!***********************************************************************
! Modules:
      USE program_constants
      USE OPT_defaults
      USE OPT_objects
      USE mod_type_hessian
      USE OPT_DIIS

      implicit none
!
! Local scalars:
      integer I
!
! Begin:
      call PRG_manager ('enter', 'SET_defaults_DIIS', 'UTILITY')
!
      DISITM=40      ! Maximum number of DIIS iterations
      MAXCUB=50      ! Cubic iterate tolerance
      NNEWTONR=0     ! Number of consecutive Newton-Rhaphsons
      CONTOL=1.0D-07 ! Cubic accuracy
      UPDATE=.false. ! Hessian update?
      LCUBIC=.false. ! Cubic update?
      STRATEGY="OLDEST"  ! Throw away strategy
!
! End of routine SET_defaults_DIIS
      call PRG_manager ('exit', 'SET_defaults_DIIS', 'UTILITY')
      return
      END
