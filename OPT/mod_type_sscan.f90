      MODULE type_surface_scan
!*******************************************************************************************************
!     Date last modified: May 25, 2001                                                                 *
!     Author: R.A. Poirier                                                                             *
!     Description: Surface Scan parameters                                                             *
!*******************************************************************************************************
      implicit none
!
      type surface_scan_definition
        integer :: Npoints_total           ! Total number of points
        integer :: Nparameters             ! Number of parameters to scan on
        integer, dimension(:), pointer :: parameter => null() ! List of parameters to scan on
        integer, dimension(:), pointer :: points => null()    ! Number of points for given parameter
        double precision, dimension(:), pointer :: FROM => null() ! Starting value of the parameter
        double precision, dimension(:), pointer :: TO => null()   ! Final value of the parameter
        double precision, dimension(:), pointer :: BY => null()   ! The increment for the parameter
      end type surface_scan_definition

      type (surface_scan_definition), save :: SSCAN

      end MODULE type_surface_scan
