      subroutine SET_defaults_HESS
!*****************************************************************************************************************
!     Date last modified: March 31, 2000                                                            Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Set code defaults.                                                                            *
!*****************************************************************************************************************
! Modules:
      USE OPT_defaults
      USE OPT_objects
      USE program_constants
!
! Begin:
!
      Hessian_method(1:)='NUMERICAL'
      OPT_hessian%method='DIAGONAL'
      OPT_hessian%difference='NONE'
!      DFLTSTEP=0.02*Angstrom_to_Bohr
!      stretch_step=0.02*Angstrom_to_Bohr
!      bend_step=0.02
!      torsion_step=0.02
      Hstep_xyz=0.005                 ! All in a.u.
      DFLTSTEP=0.005                 ! All in a.u.
      stretch_step=0.005
      bend_step=0.005
      torsion_step=0.005
      LHESSIAN=.false.
      LprintHeval=.false.
      LHessian_previous=.false.
!
      Scale_method='MAXSTEP'
!      HMQPMIN=0.00D0
      HMQPMIN=0.05D0
      MXSTEP=1.00D0       ! Equivalent to using Steepest-descent
      HSCALE=1.0D0        ! Hessian scale factor
      FBends=1.0D0        ! Factor for bends
      FTorsions=0.5D0     ! Factor for torsions
      Fcartesians=10.0D0  ! Factor for cartesian force program_constants
      LHESSYM=.true.      ! Symmetrize Hessian?
      AUTOSC=.false.      ! Automatically determine scale factor and scale the Hessian?
!      AUTOSC=.true.      ! Automatically determine scale factor and scale the Hessian?
      REMEIGS=.false.      ! Remove extra negative eigenvalues from Hessian? Found to help in general.
!
      return
      end subroutine SET_defaults_HESS
      subroutine MENU_hessian
!***********************************************************************
!     Date last modified: May 10, 1995                     Version 1.0 *
!     Author: Cory C. Pye                                              *
!     Description: Set up Hessian calculation from menu.               *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE program_parser
      USE program_objects
      USE OPT_defaults
      USE OPT_objects
      USE mod_type_hessian
      USE menu_gets

      implicit none
!
! Local scalars
      integer lenstr,nlist
      integer :: MAXlist
      parameter (MAXlist=100)
      double precision :: Rlist(MAXlist) ! temporary (testing)
      logical done,lrun
      character*8 CURHES
      character(len=MAX_string) CURRENT_coordinates
      character(len=MAX_string) string
!
! Begin:
      call PRG_manager ('enter', 'MENU_hessian', 'UTILITY')
!
! Defaults:
      done=.false.
      lrun=.false.
!
      LHESSIAN=.true.
!
! Menu:
      do while (.not.done)
      call new_token (' Hessian:')
!
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      ' Command HESSian:', &
      '   Purpose: Set (initial) Hessian matrix', &
      '   Syntax :', &
      '   HESS ', &
      '    AUtoscale=<command> calculate Hessian scaling factor', &
      '    BAckward_difference=<command>  set parameters for backward difference', &
      '    BENd_step=<real> Step size (Radians) for Bends in ', &
                               'Hessian evaluation', &
      '    CEntral_difference=<command>  set parameters for central difference', &
      '    CHECK <command> print cheap part of Hessian', &
      '    COOrdinates = <string>: Zmat, Normal, Cartesian', &
      '    CUBic=<logical> Compute the semi-diagonal third derivative', &
      '    FOrward_difference=<command>  set parameters for forward difference', &
      '    Identity=<command>  set parameters for identity (1)', &
      '    MEthod =<string> Set method for Hessian evaluation (NUMemrical ANAlytical or MM)', &
      '    PRint=<logical> Print Function Evaluations', &
      '    MARquart =<real> Marquart parameter to add to Hessian (default=0.05D0)', &
      '    MAXstep =<real> Maximum step allowed (default=1.00D0)', &
      '    PREvious=<logical> Use the hessian from a previous calculation', &
      '    REmove=<logical> Remove extra negative eigenvalues', &
      '    RList=<real list> List of real numbers', &
      '    RUN <command> calculates Hessian', &
      '    SCale=<real> Hessian scaling factor', &
      '    STEP=<real> Step size for Hessian evaluation', &
      '    STRetch_step=<real> Step size (Angstroms) for Stretches in ', &
                               'Hessian evaluation', &
      '    SYMmetrize=<logical> Symmetrize Hessian?', &
      '    TORsion_step=<real> Step size (Radians) for Torsions in ', &
                               'Hessian evaluation', &
      '   end'

! AUtoscale
      else if(token(1:2).EQ.'AU')then
        call SET_AUTOscale

! Backward Difference
      else if(token(1:2).EQ.'BA')then
        LBDIFF=.true.
        CURHES='BDIFF'
        call SETTYP (CURHES)

! Central Difference
      else if(token(1:2).EQ.'CE')then
        LFDIFF=.true.
        LBDIFF=.true.
        CURHES='CDIFF'
        call SETTYP (CURHES)

! COOrdinates for hessian evaluation
      else if(token(1:3).EQ.'COO')then
        call GET_value (OPT_parameters%name, lenstr)
        if(OPT_parameters%name(1:1).eq.'Z')then
          OPT_parameters%name(1:)='Z-MATRIX'
        else if(OPT_parameters%name(1:1).eq.'P')then
          OPT_parameters%name(1:)='PIC'
        else if(OPT_parameters%name(1:1).eq.'C')then
          OPT_parameters%name(1:)='CARTESIANS'
        else if(OPT_parameters%name(1:1).eq.'X')then
          OPT_parameters%name(1:)='CARTESIANS'
        else if(OPT_parameters%name(1:1).eq.'R')then
          OPT_parameters%name(1:)='RIC'
        else
          write(UNIout,'(2a)')'ERROR> MENU_OPT: Illegal optimization-coordinates',OPT_parameters%name
          stop 'ERROR> MENU_OPT: Illegal optimization-coordinates'
        end if

! CUBic
      else if(token(1:3).EQ.'CUB')then
        call GET_value (LCUBIC)

! Forward Difference
      else if(token(1:2).EQ.'FO')then
        LFDIFF=.true.
        CURHES='FDIFF'
        call SETTYP (CURHES)

! IDentity
      else if(token(1:2).EQ.'ID')then
        CURHES='IDENT'
        call SETTYP (CURHES)

! MAX step size
      else if(token(1:3).EQ.'MAX')then
        call GET_value (MXSTEP)
        write(UNIout,'(a,f7.4,a)')'Max Step size set to: ',MXSTEP,' Bohr'

! MARquart
      else if(token(1:3).EQ.'MAR')then
        call GET_value (HMQPMIN)
        write(UNIout,'(a,f7.4)')'Marquart set to: ',HMQPMIN

! MEthod
      else if(token(1:2).EQ.'ME')then
        call GET_value (string, lenstr)
        if(index(string, 'NUM').ne.0)then
          Hessian_method(1:)='NUMERICAL'
          OPT_hessian%method='NUMERICAL'
        else if(index(string, 'ANA').ne.0)then
          Hessian_method(1:)='ANALYTICAL'
          OPT_hessian%method='ANALYTICAL'
        else
          write(UNIout,*)'SETFCC> illegal Hessian METHOD'
          stop 'SETFCC> illegal Hessian METHOD'
        end if

! PREvious
      else if(token(1:3).EQ.'PRE')then
        CURRENT_coordinates=OPT_parameters%name ! save 
        call GET_value (Lhessian_previous)
        OPT_method_previous=OPT_method
        LHessian_previous=.true.
        write(UNIout,'(a)')'Using the previous Hessian'
        call KILL_objects (.false.) ! Except the Hessian
        call set_defaults_PROGRAM         ! reset program defaults everytime a new molecule is defined
        OPT_parameters%name=CURRENT_coordinates ! save 

      write(UNIout,'(/a)')'--------------------------------- Performing a new calculation ---------------------------------'
      write(UNIout,'(a)')'Using the same molecule'

! PRint
      else if(token(1:2).EQ.'PR')then
        LprintHeval=.true.

! REmove
      else if(token(1:2).EQ.'RE')then
        call GET_value (REMEIGS)
        write(UNIout,'(a,l1)')'Remove negative eigenvalues set: ',REMEIGS
!
! RList
      else if(token(1:2).EQ.'RL')then
        call GET_value (Rlist, nlist)
        write(UNIout,'(10F12.6)')Rlist(1:nlist)

! STRetch step
      else if(token(1:3).EQ.'STR')then
        call GET_value (STRETCH_STEP)
        write(UNIout,'(a,f7.4,a)')'Step size for stretches set to: ',STRETCH_step,' Bohr'

! BENd step
      else if(token(1:3).EQ.'BEN')then
        call GET_value (BEND_STEP)
        write(UNIout,'(a,f7.4,a)')'Step size for bends set to: ',BEND_step,' Radians'

! TORsion step
      else if(token(1:3).EQ.'TOR')then
        call GET_value (TORSION_STEP)
        write(UNIout,'(a,f7.4,a)')'Step size for torsions set to: ',TORSION_step,' Radians'

! CHECK
      else if(token(1:5).EQ.'CHECK')then

! RUN
      else if(token(1:5).EQ.'RUN')then
                         lrun = .true.

! SCaleHessian
      else if(token(1:2).EQ.'SC')then
        call GET_value (HSCALE)

! STEP
      else if(token(1:4).EQ.'STEP')then
        call GET_value (DFLTSTEP)
        STRETCH_STEP=DFLTSTEP
        BEND_STEP=DFLTSTEP
        TORSION_STEP=DFLTSTEP
        Hstep_xyz=DFLTSTEP
        write(UNIout,'(a,f7.4,a)')'Step size for all parameters set to: ',DFLTSTEP,' Anstroms/Radians'

! SYMmetrize
      else if(token(1:3).EQ.'SYM')then
        call GET_value(LHESSYM)

      else
           call MENU_end (done)
      end if
!
      end do !(.not.done)
!
      if(lrun)call GET_object ('OPT', 'HESSIAN', Hessian_method(1:len_trim(Hessian_method)))
!
! End of routine MENU_hessian
      call PRG_manager ('exit', 'MENU_hessian', 'UTILITY')
      return
      END
      subroutine SETTYP (CURHES) ! Method of evaluation
!***********************************************************************
!     Date last modified: May 10, 1995                     Version 1.0 *
!     Author: Cory C. Pye                                              *
!     Description: Set the Hessian evaluation type by row.             *
!***********************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE program_parser
      USE OPT_defaults
      USE OPT_objects
      USE mod_type_hessian
      USE menu_gets

      implicit none
!
! Input scalars:
      character*8 CURHES
!
! Work arrays:
      logical, dimension(:), allocatable :: LHSTYP
      character*8, dimension(:), allocatable :: HESPLS
      character*8, dimension(:), allocatable :: PARNAM
!
! Local scalars:
      integer DIMLST,CURNUM,NBNDLS,NANGLS,nlist,NTORLS,I,lenstr
      character(len=MAX_string) CURLST
      logical done,lrun
!
! Begin:
      call PRG_manager ('enter', 'SETTYP', 'UTILITY')
!
      if(.not.allocated(HESTYP))then
        call GET_object ('OPT', 'PARAMETERS', OPT_parameters%name)
        allocate (HESTYP(1:Noptpr))
        HESTYP(1:Noptpr)='NONE'
      end if
!
! Work arrays:
      allocate (PARNAM(1:Noptpr))
      allocate (HESPLS(1:Noptpr), LHSTYP(1:Noptpr))

      call PLSBLD (PARNAM, Noptpr, NBNDLS, NANGLS, NTORLS) ! for Z-matrix only

! Initialize work array
      LHSTYP(1:Noptpr)=.false.

! Defaults:
      done=.false.
      DIMLST = Noptpr
!
! Menu:
      do while (.not.done)
      call new_token ('Setup Hessian:')
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      '   Purpose: Set up part of Hessian for calculation', &
      '   Syntax :', &
      '     PAr = <string list>: example: ( CH HOC ) (Z-matrix)', &
      '       or  <integer list>: example: ( 1 4 ) (parameter index)', &
      '     GRoup = <string>: example: BONDS or ANGLES, ... ', &
      '       BONDS: sets all bonds', &
      '       ANGlES: sets all angles', &
      '       TORSIONS: sets all torsions', &
      '       ALL: sets all parameters', &
      '       GEOM: sets all geometry parameters', &
      '       BASIS: sets all basis set parameters', &
      '       RING: sets all ring parameters', &
      '   end'

! PAr
      else if (token(1:2).eq.'PA') then
         call GET_Clist (HESPLS, 8, DIMLST, nlist)
           do I=1,nlist
             CURLST = HESPLS(I)
             if(CHK_string(CURLST,'+-0123456789.'))then
               call CNV_StoI (CURLST, CURNUM)
             else
               call HSINDX (PARNAM,CURLST,CURNUM,Noptpr)
             end if
             call CHKPAR (PARNAM,LHSTYP,CURNUM,Noptpr)
             HESTYP(CURNUM)=CURHES
           end do

! GRoup
      else if (token(1:2).eq.'GR')then
         call GET_value (CURLST, lenstr)
         if(CURLST.eq.'BONDS')then
           do I=1,NBNDLS
             call CHKPAR (PARNAM, LHSTYP, I, Noptpr)
             HESTYP(I)=CURHES
           end do
         else if(CURLST.eq.'ANGLES')then
           do I=NBNDLS+1, NBNDLS+NANGLS
             call CHKPAR (PARNAM, LHSTYP, I, Noptpr)
             HESTYP(I)=CURHES
           end do
         else if(CURLST.eq.'TORSIONS')then
           do I=NBNDLS+NANGLS+1, NBNDLS+NANGLS+NTORLS
             call CHKPAR (PARNAM, LHSTYP, I, Noptpr)
             HESTYP(I)=CURHES
           end do
         else if(CURLST.eq.'ALL')then
           do I=1,Noptpr
             call CHKPAR (PARNAM, LHSTYP, I, Noptpr)
             HESTYP(I)=CURHES
           end do
         else if(CURLST.eq.'GEOM')then
           do I=1, NBNDLS+NANGLS+NTORLS
             call CHKPAR (PARNAM, LHSTYP, I, Noptpr)
             HESTYP(I)=CURHES
           end do
         else if(CURLST.eq.'BASIS')then
           do I=NBNDLS+NANGLS+NTORLS+1,Noptpr
             call CHKPAR (PARNAM, LHSTYP, I, Noptpr)
             HESTYP(I)=CURHES
           end do
         end if ! CURLST

      else if (token(1:4).eq.'RING') then
              write(UNIout,*) 'RING command not yet implemented.'
      else
        call MENU_end(done)
      end if
      end do ! while
!
      deallocate (PARNAM)
      deallocate (HESPLS, LHSTYP)
!
! End of routine SETTYP
      call PRG_manager ('exit', 'SETTYP', 'UTILITY')
      return
      END
      subroutine CHKPAR (PARNAM,LHSTYP,I,Noptpr)
!***********************************************************************
!     Date last modified: May 10, 1995                     Version 1.0 *
!     Author: Cory C. Pye                                              *
!     Description: Check if parameter has been previously defined.     *
!***********************************************************************
! Modules:
      USE program_files
      USE OPT_defaults

      implicit none
!
! Input scalars:
      integer I,Noptpr
      logical LHSTYP(Noptpr)
      character*8 PARNAM(Noptpr)
!
! Begin
      IF(LHSTYP(I))then
         write (UNIout, '(a)')'ERROR> CHKPAR: Hessian type already specified for parameter:'
         write (UNIout,'(i3,a)')I,PARNAM(I)
         stop 'Error: Hessian type already specified for parameter'
      end if
      END
      subroutine HSINDX (PARNAM,  & ! Array of parameter labels
                         CURLST,  & ! Current search token
                         CURNUM,  & ! Position of token in list
                         Noptpr) ! # of optimizable parameters
!***********************************************************************
!     Date last modified: May 10, 1995                     Version 1.0 *
!     Author: Cory C. Pye                                              *
!     Description: Search for specified Hessian parameter in list.     *
!***********************************************************************
! Modules:
      USE program_files
      USE program_parser
      USE OPT_defaults

      implicit none
!
! Input scalars:
      integer Noptpr,CURNUM
      character*8 CURLST
!
! Input arrays:
      character*8 PARNAM(Noptpr)
!
! Local scalars:
      integer I
      logical done,lrun,FOUND
!
! Begin:
      call PRG_manager ('enter', 'HSINDX', 'UTILITY')
!
! Defaults:
      done=.false.
      lrun=.false.
!
      FOUND=.FALSE.
      CURNUM=1

      do while (.not.FOUND.and.CURNUM.LE.Noptpr)
        IF(CURLST.EQ.PARNAM(CURNUM))then
          FOUND=.TRUE.
        else
          CURNUM=CURNUM+1
        end if
      end do

      IF(.not.FOUND)then
         write (UNIout,'(3a)')'ERROR> HSINDX: Following parameter not in Z-matrix but specified in', &
         'setting of Hessian row evaluation type: ',CURLST
         stop
      end if

! End of routine HSINDX
      call PRG_manager ('exit', 'HSINDX', 'UTILITY')
      return
      END
      subroutine PLSBLD (PARNAM, Noptpr, NBNDLS, NANGLS, NTORLS)
!***********************************************************************
!     Date last modified: May 10, 1995                     Version 1.0 *
!     Author: Cory C. Pye                                              *
!     Description: Build parameter list                                *
!***********************************************************************
! Modules:
      USE OPT_defaults
      USE type_molecule

      implicit none
!
! Local scalars:
      integer Noptpr
!
! Input array:
      character*8 PARNAM(Noptpr)
!
! Local scalars:
      integer IZMVAR,NOPT
      integer NBNDLS,NANGLS,NTORLS
!
! Begin:
      call PRG_manager ('enter', 'PLSBLD', 'UTILITY')
!
      PARNAM(1:Noptpr)='        '
!
      NOPT=0
      NBNDLS=0
      NANGLS=0
      NTORLS=0
!
! Number of bond parameters
      do IZMVAR=1,Zmatrix%Nbonds
        IF(Zmatrix%define(IZMVAR)%opt)then
          NOPT=NOPT+1
          NBNDLS=NBNDLS+1
          PARNAM(NOPT) = Zmatrix%define(IZMVAR)%name
        end if
      end do

      IF(Zmatrix%Nangles.GT.0)then
      do IZMVAR=Zmatrix%Nbonds+1,Zmatrix%Nbonds+Zmatrix%Nangles
        IF(Zmatrix%define(IZMVAR)%opt)then
          NOPT=NOPT+1
          NANGLS=NANGLS+1
          PARNAM(NOPT) = Zmatrix%define(IZMVAR)%name
        end if
      end do
      end if

      IF(Zmatrix%Ntorsions.GT.0)then
      do IZMVAR=Zmatrix%Nbonds+Zmatrix%Nangles+1,Zmatrix%Ndefines
        IF(Zmatrix%define(IZMVAR)%opt)then
          NOPT=NOPT+1
          NTORLS=NTORLS+1
          PARNAM(NOPT) = Zmatrix%define(IZMVAR)%name
        end if
      end do
      end if
!
! End of routine PLSBLD
      call PRG_manager ('exit', 'PLSBLD', 'UTILITY')
      return
      END
      subroutine SET_autoscale
!***********************************************************************
!     Date last modified: June 4, 2001                     Version 1.0 *
!     Author: R. A. Poirier                                            *
!     Description: Set up Hessian calculation from menu.               *
!***********************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE program_parser
      USE OPT_defaults
      USE OPT_objects
      USE menu_gets

      implicit none
!
! Local scalars
      integer lenstr
      character(len=MAX_string) string
      logical done,lrun
!
! Begin:
      call PRG_manager ('enter', 'SET_autoscale', 'UTILITY')
!
! Defaults:
      done=.false.
      lrun=.false.
      AUTOSC=.true.
!
! Menu:
      do while (.not.done)
      call new_token (' AUTOscale:')
!
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      ' Command AUTOscale:', &
      '   Purpose: Set Hessian autoscaling options', &
      '   Syntax :', &
      '    AUtoscale ', &
      '    method=<string>  Marquart or maxstepsize', &
      '    MAXstep =<real> MXSTEP (default=0.02D0)', &
      '    MARquart =<real> Marquart parameter to add to Hessian (default=0.05D0)', &
      '    REmove=<logical> Remove extra negative eigenvalues (defaults=true)', &
      '    SCale=<real> Hessian scaling factor', &
      '   end'

! MAX step size
      else if(token(1:3).EQ.'MAX')then
        call GET_value (MXSTEP)
        write(UNIout,'(a,f7.4,a)')'Max Step size set to: ',MXSTEP,' Bohr'

! MARquart
      else if(token(1:3).EQ.'MAR')then
        call GET_value (HMQPMIN)
        write(UNIout,'(a,f7.4)')'Marquart set to: ',HMQPMIN

! MEthod
      else if(token(1:2).EQ.'ME')then
        call GET_value (string, lenstr)
        if(index(string, 'MAR').ne.0)then
          Scale_method(1:)='MARQUART'
        else if(index(string, 'MAX').ne.0)then
          Scale_method(1:)='MAXSTEP'
        else if(index(string, 'ST').ne.0)then
          Scale_method(1:)='SD'
        else if(index(string, 'SD').ne.0)then
          Scale_method(1:)='SD'
        else if(index(string, 'SH').ne.0)then
          Scale_method(1:)='SH'
        else
          write(UNIout,*)'SET_autoscale> illegal auto scaling METHOD'
          stop 'SET_autoscale> illegal auto scaling  METHOD'
        end if
        write(UNIout,'(2a)')'Hessian scaling method set to: ',Scale_method(1:8)
!
! REmove
      else if(token(1:2).EQ.'RE')then
        call GET_value (REMEIGS)

! SCaleHessian
      else if(token(1:2).EQ.'SC')then
        call GET_value (HSCALE)

      else
           call MENU_end (done)
      end if
!
      end do !(.not.done)
!
! End of routine SET_autoscale
      call PRG_manager ('exit', 'SET_autoscale', 'UTILITY')
      return
      END
