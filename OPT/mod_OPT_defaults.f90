      module OPT_defaults
!*****************************************************************************************************************
!     Date last modified: January 18, 2001                                                          Version 1.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Names of allowed basis sets.                                                                  *
!*****************************************************************************************************************
      implicit none
!
! Optimization defaults:
      integer, parameter :: ITRAMX=20 ! Maximum number of elements in trace
      integer :: MAX_OPTITR ! Maximum number of iterations
      integer :: BLDITM    ! Maximum iterations for building cartesians for PICs

      double precision :: OPTACC ! Gradient length accuracy required for convergence
      double precision :: GRDSWITCH  ! Gradient length for switch to DIIS
      double precision :: MAXstepsize ! Maximum step size allowed
      double precision :: ChongZak_X   ! Values of X variable for ChongZak function
      double precision :: ChongZak_Y   ! Values of Y variable for ChongZak function
      double precision :: Rosenbrock_X   ! Values of X variable for Rosenbrock function
      double precision :: Rosenbrock_Y   ! Values of Y variable for Rosenbrock function


      logical :: LSWITCH        ! switch to DIIS?
      logical :: LAcceptable    ! Used by GA to determine if the geometry is acceptable
      logical :: LRestart       ! restart?
      logical :: TS_BFGS_update ! Print results of this iteration?
      character(len=16) :: SWITCH_TO     ! Optimization method to switch to depending on GRDSWITCH (OC, BFGS, ...)
      character(len=16) :: OPT_method    ! Optimization method (OC, BFGS, ...)
      character(len=16) :: OPT_method_previous    ! Previous Optimization method (OC, BFGS, ...)
!     character(len=16) :: OPT_coordinates        ! Coordinates (Z-MATRIX, Cartesians, ...)
      character(len=16) :: Convergence_criteria  ! Converge on gradient length, function value, ...
!
! Gradient
      double precision :: NGstep  ! Default step size for numerical firt derivative
      character(len=132) :: Gradient_method ! Numerical, analytical (defaults)

      type type_opt_coordinates
        character(len=16) :: coordinates        ! Coordinates (Z-MATRIX, Cartesians, ...)
        logical :: bonds
        logical :: angles
        logical :: torsions
        logical :: oopbends
        logical :: nonbonded
      end type type_opt_coordinates

      type (type_opt_coordinates) :: OPT_coord
!
! Hessian calculation defaults:
      double precision :: HMQPMIN     ! Marquart paramater for Hessian
      double precision :: MXSTEP      ! Hessian scaled such that takes maximum step MXSTEP
      double precision :: HSCALE      ! Hessian scale factor
      double precision :: FBends      ! Factor for bends
      double precision :: FTorsions   ! Factor for torsions
      double precision :: Fcartesians ! Factor for cartesian force constants
      double precision :: Hstep_XYZ    ! Default step size for numerical differentiation (cartesians)
      double precision :: DFLTSTEP      ! Default step size for numerical differentiation
      double precision :: stretch_step  ! Default step size for stretches
      double precision :: bend_step     ! Default step size for bends
      double precision :: torsion_step  ! Default step size for torsions
      logical :: LHESSIAN       ! Calculate a Hessian?
      logical :: LHESSYM        ! Symmetrize Hessian?
      logical :: AUTOSC         ! Automatically determine scale factor and scale the Hessian?
      logical :: REMEIGS        ! Remove extra negative eigenvalues from Hessian?
      logical :: LFDIFF         ! Any forward or central difference steps
      logical :: LBDIFF         ! Any backward or central difference steps
      logical :: LprintHeval    ! Print Hessian evaluation steps
      logical :: LHessian_previous    ! Use previous Hessian if true
      character(len=132) :: Hessian_method ! Numerical (default), analytical (not available) or MM (analytical)
      character(len=132) :: Scale_method ! Method of scaling the Hessian (MARquart, MAXstepsize or Steepest-Descent(SD))
!
      end module OPT_defaults

