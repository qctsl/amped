      subroutine MENU_SSCAN
!***********************************************************************
!     Date last modified: April 1, 1997                    Version 1.0 *
!     Author:R.A. Poirier                                              *
!     Description: Prepare list of parameters for surface scan.        *
!***********************************************************************
! Modules:
      USE program_parser
      USE program_constants
      USE OPT_objects
      USE menu_gets
      USE type_molecule
      USE type_surface_scan

      implicit none
!
! Local scalars:
      integer Ival,lenpar,Iparam,nparam,numfrom,numto,numby,TPOINTS
      double precision byval,fromval,toval
      logical done,lrun
      character(len=MAX_string) Lpar
!
! Local derived types:
      type(surface_scan_definition) SSCAN_work
!
! Begin:
      call PRG_manager ('enter', 'MENU_SSCAN', 'UTILITY')
!
      SSCAN_work%Nparameters=3*Natoms
      allocate (SSCAN_work%parameter(SSCAN_work%Nparameters), SSCAN_work%from(SSCAN_work%Nparameters), &
                SSCAN_work%points(SSCAN_work%Nparameters), SSCAN_work%to(SSCAN_work%Nparameters), &
                SSCAN_work%by(SSCAN_work%Nparameters))
      call DEF_SSCAN
      nparam=0
      numby=0
      numto=0
      numfrom=0

! Treat a surface as an optimization:
      OPT_parameters%name='SSCAN'

      done=.false.
!
! Menu:
      do while (.not.done)
      call new_token (' SScan:')
!
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      ' Command SScan :', &
      '   Purpose: Perform a surface scan', &
      '   Syntax :', &
      '   BY = <real>: examples: (10.0)', &
      '   FRom = <real>: examples: (180.0)', &
      '   PAram = <string or integer>: examples: C1C2C3 or 20', &
      '   RUN command: Will cause execution', &
      '   TO = <real>: examples: (360.0)', &
      '   end'
!
! BY
      else if(token(1:2).EQ.'BY')then
              call GET_value (byval)
              numby=numby+1
              SSCAN_work%by(numby)=byval
! FRom
      else if(token(1:2).EQ.'FR')then
              call GET_value (fromval)
              numfrom=numfrom+1
              SSCAN_work%from(nparam)=fromval
! PAram
      else if(token(1:2).EQ.'PA')then
        call GET_value (Lpar, lenpar)
        nparam=nparam+1
        if(CHK_string(Lpar, '0123456789'))then
          call CNV_StoI (Lpar, Ival)
          SSCAN_work%parameter(nparam)=Ival
        else
          call save_Zval (SSCAN_work%parameter, Lpar, nparam, NMCOOR)
        end if
! RUN
      else if(token(1:3).EQ.'RUN')then
           lrun=.true.
! TO
      else if(token(1:2).EQ.'TO')then
           call GET_value (toval)
           numto=numto+1
           SSCAN_work%to(numto)=toval

      else
           call MENU_end (done)
      end if
!
      end do ! .not.done
!
      if(numto.ne.nparam.or.numby.ne.nparam)then
        write(UNIout,*)'error set_sscan> TO, BY, do not match', &
                       ' the number of parameters'
        stop
      end if

      if(numfrom.gt.nparam)then
        write(UNIout,*)'error set_sscan> too many FROMs '
        stop
      end if
!
      SSCAN%Nparameters=Nparam
      allocate (SSCAN%parameter(SSCAN%Nparameters), SSCAN%from(SSCAN%Nparameters), &
                SSCAN%points(SSCAN%Nparameters), SSCAN%to(SSCAN%Nparameters), &
                SSCAN%by(SSCAN%Nparameters))
! Determine the number of points for each parameter
      call TSSPOINTS (NPARAM, TPOINTS)

      SSCAN%Npoints_total=Tpoints
      SSCAN%parameter(1:SSCAN%Nparameters)=SSCAN_work%parameter(1:SSCAN%Nparameters)
      SSCAN%from(1:SSCAN%Nparameters)=SSCAN_work%from(1:SSCAN%Nparameters)
      SSCAN%points(1:SSCAN%Nparameters)=SSCAN_work%points(1:SSCAN%Nparameters)
      SSCAN%to(1:SSCAN%Nparameters)=SSCAN_work%to(1:SSCAN%Nparameters)
      SSCAN%by(1:SSCAN%Nparameters)=SSCAN_work%by(1:SSCAN%Nparameters)

      do Iparam=1,SSCAN%Nparameters
        if(SSCAN%from(Iparam).gt.SSCAN%to(Iparam))SSCAN%by(Iparam)=-SSCAN%by(Iparam)
      end do

      deallocate (SSCAN_work%parameter, SSCAN_work%from, SSCAN_work%points, SSCAN_work%to, SSCAN_work%by)

      if(TPOINTS.eq.0)then
        write(UNIout,*)'ERROR> MENU_SSCAN: No points requested for surface scan'
      end if
!
      if(lrun)call SURFACE_SCAN
!
! End of routine MENU_SSCAN
      call PRG_manager ('exit', 'MENU_SSCAN', 'UTILITY')
      return
!
      contains
      subroutine TSSPOINTS (NPARAM, &
                            TPOINTS)
!***********************************************************************
!     Date last modified: February 19, 1998                Version 1.0 *
!     Author:R.A. Poirier                                              *
!     Description: Prepare list of parameters for surface scan.        *
!***********************************************************************
! Modules:
      USE program_constants

      implicit none
!
! Input scalars:
      integer NPARAM,TPOINTS
!
! Local scalars:
      integer IPARAM
!
!Output arrays:
      double precision DIFF,SMALL
      parameter (SMALL=1.0D-04)
!
! Begin:
      call PRG_manager ('enter', 'TSSPOINTS', 'UTILITY')
!
      TPOINTS=0
      do IPARAM=1,NPARAM
        DIFF=SSCAN_work%to(IPARAM)-SSCAN_work%from(IPARAM)
        if(dabs(DIFF).LE.SMALL.or.SSCAN_work%by(IPARAM).eq.ZERO)then
          SSCAN_work%points(IPARAM)=0
        else
          SSCAN_work%points(IPARAM)=INT(DIFF/SSCAN_work%by(IPARAM))
          TPOINTS=TPOINTS+SSCAN_work%points(IPARAM)
        end if
      end do ! IPARAM
!
! End of routine TSSPOINTS
      call PRG_manager ('exit', 'TSSPOINTS', 'UTILITY')
      return
      end subroutine TSSPOINTS
      subroutine DEF_SSCAN
!***********************************************************************
!     Date last modified: February 19, 1998                Version 1.0 *
!     Author:R.A. Poirier                                              *
!     Description: Prepare list of parameters for surface scan.        *
!***********************************************************************
! Modules:

      implicit none
!
! Local scalars:
      integer IZMVAR
!
! Begin:
      call PRG_manager ('enter', 'DEF_SSCAN', 'UTILITY')
!
      do IZMVAR=1,Zmatrix%Ndefines
        SSCAN_work%parameter(IZMVAR)=IZMVAR
        SSCAN_work%from(IZMVAR)=Zmatrix%define(IZMVAR)%VALUE
        SSCAN_work%to(IZMVAR)=Zmatrix%define(IZMVAR)%VALUE
        SSCAN_work%by(IZMVAR)=0.0D0
      end do ! IZMVAR
!
! End of routine DEF_SSCAN
      call PRG_manager ('exit', 'DEF_SSCAN', 'UTILITY')
      return
      end subroutine DEF_SSCAN
      subroutine SAVE_Zval (ARRAY,  & ! Input
                            Lpar,   & ! Input
                            NVAL,   & ! Intput
                            MAXVAL)
!***********************************************************************
!     Date last modified: February 27 ,1998                Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!                                                                      *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer NVAL,MAXVAL,IVAL
      character*(*) Lpar
!
! Input arrays:
      integer ARRAY(MAXVAL)
!
! Local scalars:
      integer IZMVAR,lennam,lenpar
      logical found
!
! Begin:
      call PRG_manager ('enter', 'SAVE_ZVAL', 'UTILITY')
!
      found=.false.
      lenpar=len_trim(Lpar)

      do IZMVAR=1,Zmatrix%Ndefines
      lennam=len_trim(Zmatrix%define(IZMVAR)%name)

      IF(Lpar(1:lenpar).EQ.Zmatrix%define(IZMVAR)%name(1:lennam))then
        found=.true.
        ARRAY(NVAL)=IZMVAR
      end if
      end do ! IZMVAR

      if(.not.found)then
        write(UNIout,*)' error_SAVE_Zval> parameter ',Lpar(1:lenpar),' not found'
        stop ' error_SAVE_Zval>'
      end if
!
! End of routine SAVE_Zval
      call PRG_manager ('exit', 'SAVE_ZVAL', 'UTILITY')
      return
      end subroutine SAVE_Zval

      end subroutine MENU_SSCAN
