      module OPT_BFGS
!***********************************************************************
!     Date last modified: May 10, 2001                     Version 2.0 *
!     Author: R.A. Poirier                                             *
!     Description: Work arrays and scalars used by BFGS                *
!***********************************************************************
      USE program_constants

      implicit none
!
! Scalars
      integer :: SAVEFV=1 ! control bfgs save
      integer :: SPECIT=0 ! special iteration flag
      double precision :: FDIFF=0.0D0  ! Difference between A & B Energies
      double precision :: DGRDA=0.0D0  ! Gradient at point A
      double precision :: DGRDB=0.0D0  ! Gradient at point B
      double precision :: GMIN=0.0D0   ! Minimum gradient on trace
      double precision :: STMIN=0.0D0  ! Minimum trace step
      double precision :: STEPLN=0.0D0  ! Trace step length
      double precision :: STEPBD=0.0D0  ! Bound on step
      double precision :: STEPLB=0.0D0  ! Lower bound on step
      double precision :: GSC=0.0D0  ! Gradient Scaling Factor (Lower bound on step length)
      double precision :: FSCAL=0.01D0*Angstrom_to_Bohr  ! parameter scale value
!
! Work arrays
      double precision, dimension(:), allocatable :: FM
      double precision, dimension(:), allocatable :: FN
      double precision, dimension(:), allocatable :: FK
      double precision, dimension(:), allocatable :: WORK1
      double precision, dimension(:), allocatable :: Sscale ! parameter scale factors
!
      end module OPT_BFGS
