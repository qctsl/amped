      module opt_tsbfgs
!***********************************************************************
!     Date last modified: May 9, 2001                      Version 2.0 *
!     Author: R.A. Poirier                                             *
!     Description:  TS-BFGS  scalars, arrays, ...                      *
!***********************************************************************
! Modules
      implicit none
!
! Local arrays:
      double precision, dimension(:), allocatable :: y
      double precision, dimension(:,:), allocatable:: Jvec,temp2,temp3,kvec
      double precision, dimension(:), allocatable:: Eigval
      double precision, dimension(:,:), allocatable::  frac1,frac2
      double precision, dimension(:,:), allocatable:: posdefHESS, Eigvec, Diag_Eigval
      double precision, dimension(:,:), allocatable:: deltax,deltag
! Declare local scalars in TS-BFGS formula as 1x1 matrices for array manipulation purposes
!
! Local parameters
      double precision, parameter :: threshold_gx=5.0D-06
      logical :: LUpdate
!
      end module opt_tsbfgs
      subroutine TS_BFGS
!***********************************************************************************
!     Date last modified:  May 3, 2001                                             *
!     Author:  Sharene Bungay and R. A. Poirier                                    *
!     Description:  Subroutine based on the Newton-Rhapson method using a TS-BFGS  *
!     update                                                                       *
!***********************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE matrix_print
      USE OPT_defaults
      USE OPT_objects
      USE opt_tsbfgs
      USE mod_type_hessian

      implicit none
!
! Local scalars:
      integer Ioptpr,J,I
      double precision :: Max_step,Steepest_descent
!
! Begin:
      call PRG_manager ('enter', 'TS_BFGS', 'UTILITY')
!
! Set current parameters and gradients as best (for printing)
      Ntrace=1

!      write(UNIout,*)'TS_BFGS> Hessian matrix:'
!      call PRT_matrix (Hessian_matrix, Noptpr, Noptpr)

      if(OPTITR.GT.0)then
        call OPT_print
      end if
      deltaHESS=ZERO
!
! Increment the iteration number
      OPTITR = OPTITR + 1
!
! Update the Hessian using TS-BFGS update
      if(LUpdate)then
        call UPDATE_TS_BFGS (PARSET, PARGRD, Hessian_matrix, PREVPAR, PREVGRD, deltaHESS, Noptpr)
      end if

! Save values to form deltax and deltag on next iteration
      PREVPAR(1:Noptpr) = PARSET(1:Noptpr)
      PREVGRD(1:Noptpr) = PARGRD(1:Noptpr)
      PREV_grdlth=GRDLTH
!
! Find INVHESS*PARGRD for Newton Raphson step, i.e.. solve HESS*y = PARGRD for y
      call sys_solve (Hessian_matrix+deltaHESS, PARGRD, Noptpr, y)

      do Ioptpr=1,Noptpr
      if(dabs(y(Ioptpr)).GT.MAXstepsize)then
        Lupdate=.false.
        write(UNIout,'(a,i10)')'TS_BFGS> Warning MAXstepsize exceeded for parameter: ',Ioptpr
        write(UNIout,'(a,f20.5)')'TS_BFGS> Step size: ',y(Ioptpr)
        Steepest_descent=dsign(dabs(PARGRD(Ioptpr)), y(Ioptpr)) ! Set step to steepest-descent (NO GOOD)
        write(UNIout,'(a,f20.5)')'TS_BFGS> Steepest-descent: ',Steepest_descent
!        y(Ioptpr)=Steepest_descent
        y(Ioptpr)=dsign(MAXstepsize, y(Ioptpr))
        write(UNIout,'(a,f20.5)')'TS_BFGS> Step size set to: ',y(Ioptpr)
      end if
      end do
!
! Only updated Hessian if MAXstepsize not exceeded
      if(Lupdate)then
        Hessian_matrix = Hessian_matrix + deltaHESS
      end if
!
! Update parameters:
      PARSET = PARSET - y
!
      LUpdate=.true. ! From now on update Hessian
!
! end of routine
      call PRG_manager ('exit', 'TS_BFGS', 'UTILITY')
      return
      end
      subroutine UPDATE_TS_BFGS (PARSET,     & ! Optimizable parameters
                                 PARGRD,     & ! gradients
                                 Hessian_matrix,       & ! Hessian approximation
                                 PREVPAR, PREVGRD, deltaHESS, &
                                 Noptpr)    ! Number of parameters
!*****************************************************************************************
!  Date last modified:  May 3, 2001                                                      *
!  Author:  Sharene Bungay                                                               *
!  Description:  implement TS-BFGS Hessian update                                        *
! subroutine needs the following additions:
!  - a check for exceeding maximum step size and scale if necessary.
!  - a check to determine if the gradient has decreased (deltaglth>0),
!    if so assume the step is good and continue, if not scale the step
!    down.  If scaling doesn't work, try changing lambda (increase) and
!    solve the modified system, (H+lambda*I)q=g, corresponding to
!    a modified newton-raphson step.
!  - if increasing lambda doesn't help, assume defeat and quit.
!  - if lambda has changed (is non-zero) and a subsequent step
!    is good then decrease lambda for the next iteration.
!*****************************************************************************************
! Modules:
      USE program_constants
      USE matrix_print
      USE opt_tsbfgs

      implicit none
!
! Input Scalars:
      integer Noptpr
!
! Input Arrays:
      double precision PARSET(Noptpr),PARGRD(Noptpr),Hessian_matrix(Noptpr,Noptpr)
      double precision PREVPAR(Noptpr),PREVGRD(Noptpr),deltaHESS(Noptpr,Noptpr)
!
      double precision, dimension(1,1) :: phi,scalar1,scalar_gx,temp4
!
! Local scalars:
      double precision bottom,aterm,temp5
      integer I,Ioptpr, Joptpr
!
! Begin
      deltax(1:Noptpr,1) = PARSET(1:Noptpr) - PREVPAR(1:Noptpr)
      deltag(1:Noptpr,1) = PARGRD(1:Noptpr) - PREVGRD(1:Noptpr)

!      write(6,*)'deltax,deltag:'
!      do i=1,Noptpr
!      write(6,*)i,deltax(i,1),deltag(i,1)
!      end do
      scalar_gx=matmul(transpose(deltag), deltax)
!      write(6,*)'scalar_gx:'
!      write(6,*)scalar_gx
!
! Begin TS_BFGS formula

! Get the eigenvalues and eigenvectors of the current Hessian
      call Diag_Hessian (Hessian_matrix, Noptpr, Eigval, Eigvec, .false.)

! Put eigenvalues on diagonal elements of Diag_Eigval
      Diag_Eigval(1:Noptpr,1:Noptpr)=ZERO
      do Ioptpr=1,Noptpr
        Diag_Eigval(Ioptpr,Ioptpr)=DABS(Eigval(Ioptpr))
      end do
!
!      write(6,*)'Diag_Eigval:'
!      do i=1,Noptpr
!      write(6,*)i,Diag_Eigval(i,i)
!      end do

! Form a positive definite Hessian
      posdefHESS = matmul(matmul(Eigvec,Diag_Eigval),transpose(Eigvec))

!      write(6,*)'posdefHESS:'
!      call PRT_matrix (posdefHESS, Noptpr, Noptpr)

      Jvec = deltag - matmul(Hessian_matrix, deltax)

!      write(6,*)'Jvec:'
!      do i=1,Noptpr
!      write(6,*)i,Jvec(i,1)
!      end do
! Construct phi as the square cosine of the angle between deltax and j vectors
      phi=(matmul(transpose(deltax), Jvec))**2/matmul(matmul(transpose(deltax),deltax),matmul(transpose(Jvec), Jvec))
!      write(6,*)'phi:'
!      write(6,*)phi
!
! Calculates the missing term:
      do Ioptpr=1,Noptpr
        temp2(Ioptpr,1)=ONE/deltag(Ioptpr,1)
      end do
      scalar1=matmul(matmul(transpose(temp2), Hessian_matrix), deltax) ! correct
      scalar1(1,1)=scalar1(1,1)/dble(Noptpr)
      aterm=ONE-scalar1(1,1)
      aterm=aterm/scalar_gx(1,1)
      aterm=dabs(aterm)  ! best
!      write(6,*)'using abs(aterm) with Hessian'
!      write(6,*)'aterm: ',aterm
!
! Test other ways of solving for the aterm
!      scalar1=matmul(matmul(transpose(deltax), Hessian_matrix), deltax) ! correct
!      aterm1=(scalar_gx(1,1)-scalar1(1,1))/scalar_gx(1,1)**2
!      aterm1=dabs(aterm1)  ! best
!      write(6,*)'aterm1(deltax): ',aterm1
!      aterm = aterm1

!      scalar1=matmul(matmul(transpose(deltag), Hessian_matrix), deltax) ! correct
!      scalar_gg=matmul(transpose(deltag), deltag)
!      aterm2=(scalar_gg(1,1)-scalar1(1,1))/(scalar_gg(1,1)*scalar_gx(1,1))
!      aterm2=dabs(aterm2)  ! best
!      write(6,*)'aterm2(deltax): ',aterm2
!      aterm = aterm2
!
      temp2 = aterm*phi(1,1)*scalar_gx(1,1)*deltag
      temp3 = (ONE-phi(1,1))*matmul(posdefHESS, deltax)

! temp4 and temp5 are scalars and contribute to the denominator
      temp4 = matmul(matmul(transpose(deltax), posdefHESS), deltax)
      temp5 = scalar_gx(1,1)*scalar_gx(1,1)

      bottom = (ONE-phi(1,1))*temp4(1,1) + phi(1,1)*aterm*temp5

!      write(6,*)'bottom:'
!      write(6,*)bottom

      kvec=(temp2+temp3)/bottom

! terms in the final formula
      frac1 = matmul(Jvec, transpose(kvec)) + matmul((kvec), transpose(Jvec))

      scalar1 = matmul(transpose(deltax), Jvec)
      frac2=scalar1(1,1)*matmul(kvec, transpose(kvec))

      deltaHESS = frac1 - frac2

!      write(6,*)'frac1:'
!      call PRT_matrix (frac1, Noptpr, Noptpr)

!      write(6,*)'frac2:'
!      call PRT_matrix (frac2, Noptpr, Noptpr)

!      write(6,*)'deltaHESS:'
!      call PRT_matrix (deltaHESS, Noptpr, Noptpr)

      return
      end
      subroutine sys_solve(A   , & ! Matrix
                           b   , & ! vector
                           n   , & ! dimension of matrix/vector
                           x   )! solution of Ax=b
!***************************************************************
!     Date last modified:  Oct 3, 1999                         *
!     Author:  Sharene Bungay                                  *
!     Description:  Solve linear system Ax=b using             *
!                   LU Factorization                           *
!***************************************************************
      implicit none

! Input scalars:
      integer n
!
! Input arrays:
      double precision A(n,n)
      double precision b(n),x(n)
!
! Local scalars:
      integer i, j, k, p
      double precision temp
!
! Local arrays:
      double precision, dimension(:,:), allocatable::M
      double precision, dimension(:,:), allocatable::L
      double precision, dimension(:,:), allocatable::U
      double precision, dimension(:), allocatable::y
!
! Begin:
!
      allocate(M(n,n), L(n,n), U(n,n), y(n))
!
! Initialize L and U to zero
      L = 0.0D0
      U = 0.0D0
!
! Determine L and U using Doolittle factorization
      do k=1,n
         L(k,k)=1
         do j=k,n
            temp = 0.0D0
            do p=1,k-1
               temp = temp + L(k,p)*U(p,j)
            end do
            U(k,j) = A(k,j) - temp
          end do
          do i=k+1,n
             temp = 0.0D0
             do p=1,k-1
                temp = temp + L(i,p)*U(p,k)
             end do
             L(i,k) = (A(i,k) - temp)/U(k,k)
          end do
      end do

! Solve the lower-triangular system Ly=b for y (using forward sustitution)
      call forsub (L, y, b, n)

! Solve the upper-triangular system Ux=y for x (using backward sustitution)
      call backsub (U, x, y, n)

! The current x is the solution to the original system Ax=b

      return
      end
      subroutine forsub(L, y, b, n)
!***************************************************************
!     Date last modified:  Oct 3, 1999                         *
!     Author:  Sharene Bungay                                  *
!     Description:  Perform forward sustitution                *
!***************************************************************
      implicit none

! Input scalars:
      integer n

! Input arrays:
      double precision L(n,n), y(n), b(n)

! Local scalars:
      integer i, j
      double precision temp

! Begin:

      y(n) = b(n)/L(n,n)

      do i=1,n
      temp = 0.0D0
         do j=1,i-1
            temp = temp + L(i,j)*y(j)
         end do
         y(i) = (b(i) - temp)/L(i,i)
      end do

      return
      end
      subroutine backsub(U, x, y, n)
!***************************************************************
!     Date last modified:  Oct 3, 1999                         *
!     Author:  Sharene Bungay                                  *
!     Description:  Perform backward sustitution               *
!***************************************************************
      implicit none

! Input scalars:
      integer n

! Input arrays:
      double precision U(n,n), x(n), y(n)

! Local scalars:
      integer i, j
      double precision temp

! Begin:

      x(n) = y(n)/U(n,n)

      do i=n-1,1,-1
      temp = 0.0D0
         do j=i+1,n
            temp = temp + U(i,j)*x(j)
         end do
         x(i) = (y(i) - temp)/U(i,i)
      end do

      return
      end
      subroutine INI_OPT_TSBFGS
!***********************************************************************************
!     Date last modified:  May 9, 2001                                             *
!     Author:  R. A. Poirier                                                       *
!     Description:  Initialize TS-BFGS optimization                                *
!***********************************************************************************
! Modules:
      USE OPT_defaults
      USE OPT_objects
      USE opt_tsbfgs

      implicit none
!
! Begin:
      call PRG_manager ('enter', 'INI_OPT_TSBFGS', 'UTILITY')
!
      LUpdate=.false. ! No update on the first entry

      allocate (deltax(Noptpr,1), deltag(Noptpr,1))
      allocate (deltaHESS(Noptpr,Noptpr))
      allocate (y(Noptpr))
      allocate (Jvec(Noptpr,1), temp2(Noptpr,1), temp3(Noptpr,1), kvec(Noptpr,1))
      allocate (frac1(Noptpr,Noptpr), frac2(Noptpr,Noptpr))
      allocate (Eigval(Noptpr), Eigvec(Noptpr,Noptpr), posdefHESS(Noptpr,Noptpr))
      allocate (Diag_Eigval(Noptpr,Noptpr))
!
! end of routine
      call PRG_manager ('exit', 'INI_OPT_TSBFGS', 'UTILITY')
      return
      end
      subroutine END_OPT_TSBFGS
!***********************************************************************************
!     Date last modified:  May 9, 2001                                             *
!     Author:  R. A. Poirier                                                       *
!     Description:  Initialize TS-BFGS optimization                                *
!***********************************************************************************
! Modules:
      USE OPT_objects
      USE opt_tsbfgs

      implicit none
!
! Begin:
      call PRG_manager ('enter', 'END_OPT_TSBFGS', 'UTILITY')
!
      deallocate (y)
      deallocate (Jvec, temp2, temp3, kvec, frac1, frac2)
      deallocate (Eigval, Eigvec, posdefHESS, Diag_Eigval)
      deallocate (deltax, deltag)

! end of routine
      call PRG_manager ('exit', 'END_OPT_TSBFGS', 'UTILITY')
      return
      end
