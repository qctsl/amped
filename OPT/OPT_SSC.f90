      subroutine SSC_TO_OPT
!***********************************************************************
!     Date last modified: January 28, 1998                 Version 2.0 *
!     Author: R. A. Poirier                                            *
!     Description: Update the Z-matrix parameters.                     *
!***********************************************************************
! Modules:
      USE type_molecule
      USE OPT_objects

      implicit none
!
! Local scalars:
      integer Iatom,IPARAM,Ioptpr
!
! Begin:
      call PRG_manager ('enter', 'SSC_TO_OPT', 'UTILITY')
!
      NICOOR=NMCOOR
! First determine Noptpr
      if(Natoms.eq.2)then
        Noptpr=1
      else
        Noptpr=3*Natoms-6
      end if

      if(.not.allocated(PARSET))then
        allocate(OPT_par(Noptpr))
        allocate(PARSET(Noptpr))
      end if

      IPARAM=0
! Bond Lengths
      do Iatom=2,Natoms
        IPARAM=IPARAM+1
        OPT_par(Iparam)%value=Zmatrix%bond(Iatom)%length
        OPT_par(Iparam)%type='bond'
        PARSET(IPARAM)=Zmatrix%bond(Iatom)%length
      end do ! Iatom
!
! Bond Angles
      do Iatom=3,Natoms
        IPARAM=IPARAM+1
        OPT_par(Iparam)%value=Zmatrix%angle(Iatom)%value
        OPT_par(Iparam)%type='angle'
        PARSET(IPARAM)=Zmatrix%angle(Iatom)%value
      end do ! Iatom
!
! Torsion Angles
      do Iatom=4,Natoms
        IPARAM=IPARAM+1
        OPT_par(Iparam)%value=Zmatrix%torsion(Iatom)%value
        OPT_par(Iparam)%type='torsion'
        PARSET(IPARAM)=Zmatrix%torsion(Iatom)%value
      end do ! Iatom

      do Ioptpr=1,Noptpr
        OPT_par(Iparam)%OPT=.true.
      end do
!
! End of routine SSC_TO_OPT
      call PRG_manager ('exit', 'SSC_TO_OPT', 'UTILITY')
      return
      END
      subroutine OPT_TO_SSC
!***********************************************************************
!     Date last modified: January 28, 1998                 Version 2.0 *
!     Author: R. A. Poirier                                            *
!     Description: Update the Z-matrix parameters.                     *
!***********************************************************************
! Modules:
      USE type_molecule
      USE OPT_objects

      implicit none
!
! Local scalars:
      integer Iatom,IPARAM
!
! Begin:
      call PRG_manager ('enter', 'OPT_TO_SSC', 'UTILITY')
!
      IPARAM=0
! Bond Lengths
      do Iatom=2,Natoms
        IPARAM=IPARAM+1
        Zmatrix%bond(Iatom)%length=OPT_par(Iparam)%value
        Zmatrix%bond(Iatom)%length=PARSET(IPARAM)
      end do ! Iatom
!
! Bond Angles
      do Iatom=3,Natoms
        IPARAM=IPARAM+1
        Zmatrix%angle(Iatom)%value=OPT_par(Iparam)%value
        Zmatrix%angle(Iatom)%value=PARSET(IPARAM)
      end do ! Iatom
!
! Torsion Angles
      do Iatom=4,Natoms
        IPARAM=IPARAM+1
        Zmatrix%torsion(Iatom)%value=OPT_par(Iparam)%value
        Zmatrix%torsion(Iatom)%value=PARSET(IPARAM)
      end do ! Iatom

! End of routine OPT_TO_SSC
      call PRG_manager ('exit', 'OPT_TO_SSC', 'UTILITY')
      return
      END
