      subroutine BLD_Hessian_matrix (Modality)
!****************************************************************************
!     Date last modified: June 11, 2004                        Version 1.0  *
!     Author: R.A. Poirier                                                  *
!     Description: Get the Force constant matrix.                           *
!****************************************************************************
! Modules:
      USE OPT_defaults
      USE program_constants
      USE mod_type_hessian
      USE OPT_objects

      implicit none
!
! Local scalars:
      integer Ioptpr
      character*(*) Modality
!
! Begin:
      call PRG_manager ('enter', 'BLD_Hessian_matrix', 'HESSIAN%'//Modality)

      select case(Modality)

      case ('ANALYTICAL')
        call Hessian_analytical

      case ('NUMERICAL') ! Depends on function
        call GET_object ('OPT', 'PARAMETERS', OPT_parameters%name) ! Always get the parameters (this defines Noptpr)
        Noptpr=size(PARSET)
        if(.not.allocated(Hessian_matrix))then
          allocate (Hessian_matrix(Noptpr,Noptpr))
        else
          deallocate (Hessian_matrix)
          allocate (Hessian_matrix(Noptpr,Noptpr))
        end if
        Hessian_matrix(1:Noptpr,1:Noptpr)=ZERO
        call GET_object ('OPT', 'HESSIAN', 'GUESS')
        do Ioptpr=1,Noptpr ! Copy default into Hessian.
          Hessian_matrix(Ioptpr,Ioptpr)=DIAGFC(Ioptpr)
        end do ! Ioptpr

        if(.not.allocated(HESTYP))then
          allocate (HESTYP(1:Noptpr))
          HESTYP(1:Noptpr)='FDIFF' ! Not given by menu thus just using diagonal
        end if

        call NFC_matrix (Hessian_matrix, Noptpr)

      end select
!
! End of routine BLD_Hessian_matrix
      call PRG_manager ('exit', 'BLD_Hessian_matrix', 'HESSIAN%'//Modality)
      return
      end subroutine BLD_Hessian_matrix
      subroutine ForceConstant_matrix (Modality)
!****************************************************************************
!     Date last modified: June 11, 2004                        Version 1.0  *
!     Author: R.A. Poirier                                                  *
!     Description: Get the Force constant matrix.                           *
!****************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE OPT_defaults
      USE matrix_print
      USE OPT_objects
      USE QM_objects
      USE type_molecule
      USE mod_type_hessian

      implicit none
!
! Input scalars:
      character*(*) :: Modality
!
! Local scalars:
      integer :: FCdim
!
      double precision, dimension(:,:), allocatable :: FCint
!
! Begin:
      call PRG_manager ('enter', 'ForceConstant_matrix', 'FORCE_CONSTANT%'//Modality)
!
      if(Modality.eq.'ANALYTICAL')then
        OPT_parameters%name(1:3)='XYZ'
      else if(OPT_parameters%name(1:8).EQ.'Z-MATRIX'.and.NDummies.ne.0)then
        write(UNIout,'(/a)')'ForceConstant_matrix> ERROR'
        write(UNIout,'(a)')'Z-matrix contains dummies, must use cartesian coordinates, i.e.,'
        write(UNIout,'(a)')'GEOMetry  COORD = CARTESIANS END'
        call PRG_stop ('Z-matrix contains dummies')
      end if

      FCdim=3*NReal_atoms
      if(.not.allocated(Force_constant))then
        allocate (Force_constant(FCdim,FCdim))
      else
        deallocate (Force_constant)
        allocate (Force_constant(FCdim,FCdim))
      end if

      Force_constant(1:FCdim,1:FCdim)=ZERO

      select case(Modality)
      case ('ANALYTICAL')
        call AFC_matrix (Force_constant, FCdim)
      case ('NUMERICAL') ! Always get the parameters (this defines Noptpr)
        call GET_object ('OPT', 'PARAMETERS', OPT_parameters%name) ! May require PAR_type for printing (should avoid this)
        Noptpr=size(PARSET)
        if(.not.allocated(HESTYP))then
          allocate (HESTYP(1:Noptpr))
        end if
        if(LFDIFF.and.LBDIFF)then
          write(UNIout,'(a)')'Force constant matrix calculated using the Central difference'
          HESTYP(1:Noptpr)='CDIFF'
        else if(LFDIFF)then
          write(UNIout,'(a)')'Force constant matrix calculated using the Forward difference'
          HESTYP(1:Noptpr)='FDIFF'
        else if(LBDIFF)then
          write(UNIout,'(a)')'Force constant matrix calculated using the Backward difference'
          HESTYP(1:Noptpr)='BDIFF'
        else ! Use default
          write(UNIout,'(a)')'Force constant matrix calculated using the Forward difference'
          HESTYP(1:Noptpr)='FDIFF'
          LFDIFF=.true.
        end if
        allocate (FCint(Noptpr,Noptpr))
        call NFC_matrix (FCint, Noptpr)
      end select


      if(OPT_parameters%name(1:3).EQ.'XYZ')then
        Force_Constant=FCint
      else if (OPT_parameters%name(1:8).EQ.'Z-MATRIX')then
        call GET_object ('MOL', 'BMATRIX', 'ZM')
        if(Noptpr.ne.NMCOOR)then
          write(UNIout,'(a)')'Noptpr and NMCOOR do not match'
          call PRG_stop ('Noptpr and NMCOOR do not match')
        end if
        write(UNIout,*)' Force constant converted from internal to cartesian '
        Force_Constant=matmul(matmul(transpose(Bmatrix), FCint), Bmatrix)
      else ! STOP if the coordinate system is NOT Z-matrix OR xyz.
        write(UNIout,*)'coordinate conversion not available'
        stop 'coordinate conversion not available'
      end if
!
! The following prints the transformed force constant matrix.
      deallocate (FCint)
!
! End of routine ForceConstant_matrix
      call PRG_manager ('exit', 'ForceConstant_matrix', 'FORCE_CONSTANT%'//Modality)
      return
      end subroutine ForceConstant_matrix
      subroutine BLD_IHessian
!***********************************************************************
!     Date last modified: December   , 1994                            *
!     Author: Cory C. Pye  and R. A. Poirier                           *
!     Description: Create and scale the inverse Hessian                *
!***********************************************************************
! Modules:
      USE program_files
      USE OPT_defaults
      USE OPT_objects
      USE mod_type_hessian
      USE matrix_print

      implicit none
!
! Local scalars:
      integer IER
!
! Begin:
      call PRG_manager ('enter', 'BLD_IHessian', 'HESSIAN%INVERSE')
!
      call GET_object ('OPT', 'HESSIAN', Hessian_method(1:len_trim(Hessian_method)))
!
! Object:
      if(.not.allocated(IHessian_matrix))then
        allocate (IHessian_matrix(1:Noptpr,1:Noptpr))
      end if
!
! Copy Hessian array into Hessian inverse array
      IHessian_matrix(1:Noptpr,1:Noptpr)=Hessian_matrix(1:Noptpr,1:Noptpr)
!
! Invert in place
      call INVERT (IHessian_matrix, Noptpr, Noptpr, IER)
!
! Check to see if inversion was successful.
      IF(IER.EQ.-2) then
        write(UNIout,'(a)')'ERROR> BLD_IHessian:'
        write(UNIout,'(a)')'Inversion of Hessian was unsuccessful'
        stop 'ERROR> BLD_IHessian Inversion of Hessian was unsuccessful'
      end IF
!
! End of routine BLD_IHessian
      call PRG_manager ('exit', 'BLD_IHessian', 'HESSIAN%INVERSE')
      return
      end
      subroutine BLD_IHessian_factored
!***********************************************************************
!     Date last modified: December  5, 1994                            *
!     Author: Cory C. Pye                                              *
!     Description: This routine factorizes the inverse Hessian H into  *
!                  H=JJ'.                                              *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE OPT_defaults
      USE matrix_print
      USE OPT_objects
      USE mod_type_hessian

      implicit none
!
! Local scalars:
      integer I,J,K,IR
      integer HESLEN
      logical HESPDF
      double precision TEMP
!
! Begin:
      call PRG_manager ('enter', 'BLD_IHessian_factored', 'HESSIAN%INVERSE_FACTORED')
!
      call GET_object ('OPT', 'HESSIAN', 'INVERSE')
      HESLEN=Noptpr*(Noptpr+1)/2
!
! Object:
      if(.not.allocated(IHessian_factored))then
        allocate (IHessian_factored(1:HESLEN))
      end if
!
      IHessian_factored(1:HESLEN)=ZERO
!
! Copy lower triangle of H into J.
      K=0
      do I=1,Noptpr
        do J=I, Noptpr
          K=K+1
          IHessian_factored(K)=IHessian_matrix(J,I)
        end DO
      end DO
!
! Factor H=LDL'
      call MC11BD (IHessian_factored, HESLEN, Noptpr, IR)

      IF(IR.LT.Noptpr) then
        write(UNIout,'(a)')'BLD_IHessian_factored_warning> HESSIAN NOT POSITIVE DEFINITE'
        HESPDF=.FALSE.
      else
        HESPDF=.TRUE.
      end IF
!
! Make H=LDL' = > LX(LX)' where X^2=D
      IF(HESPDF)then
        K=1
        do I=1,Noptpr
          TEMP=DSQRT(IHessian_factored(K))
          IHessian_factored(K)=TEMP
          K=K+1
          do J=I+1,Noptpr
            IHessian_factored(K)=IHessian_factored(K)*TEMP
            K=K+1
          end DO
        end DO
      end IF
!
! End of routine BLD_IHessian_factored
      call PRG_manager ('exit', 'BLD_IHessian_factored', 'HESSIAN%INVERSE_FACTORED')
      return
      end
      subroutine BLD_Hessian_factored
!***********************************************************************
!     Date last modified: December  5, 1994                            *
!     Author: Cory C. Pye                                              *
!     Description: This routine factorizes the inverse Hessian H into  *
!                  H=JJ'.                                              *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE OPT_defaults
      USE OPT_objects
      USE mod_type_hessian

      implicit none
!
! Local scalars:
      integer HESLEN
      logical HESPDF
      integer I,J,K,IR
!
! Begin:
      call PRG_manager ('enter', 'BLD_Hessian_factored', 'HESSIAN%FACTORED')
!
      call GET_object ('OPT', 'HESSIAN', Hessian_method(1:len_trim(Hessian_method)))

      HESLEN=Noptpr*(Noptpr+1)/2
!
! Object:
      if(.not.allocated(Hessian_factored))then
        allocate (Hessian_factored(1:HESLEN))
      end if
!
      Hessian_factored(1:HESLEN)=ZERO
!
! Copy lower triangle of H into J.
      K=0
      do I=1,Noptpr
        do J=I, Noptpr
          K=K+1
          Hessian_factored(K)=Hessian_matrix(J,I)
        end DO
      end DO
!
! Factor H=LDL'
      call MC11BD (Hessian_factored, HESLEN, Noptpr, IR)

      IF(IR.LT.Noptpr) then
        write(UNIout,'(a)')'BLD_Hessian_factored_warning> HESSIAN NOT POSITIVE DEFINITE'
        HESPDF=.FALSE.
      else
        HESPDF=.TRUE.
      end IF
!
! End of routine BLD_Hessian_factored
      call PRG_manager ('exit', 'BLD_Hessian_factored', 'HESSIAN%FACTORED')
      return
      end
      subroutine BLD_Bconstraints (Bconstraints, Noptpr, Nint)
!***********************************************************************
!     Date last modified: January 27, 1998                Version 2.0  *
!     Author: R.A. Poirier                                             *
!     Description: Construct the constraints matrix                    *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE OPT_defaults
      USE type_molecule
      USE matrix_print

      implicit none
!
! Input scalars:
      integer Noptpr,Nint
!
! Local scalars:
      integer Iatom,IMCOOR,IZMVAR,IZVAR
      double precision :: VSIGN
!
      double precision :: Bconstraints(Noptpr,Nint)
!
! Begin:
      call PRG_manager ('enter', 'BLD_Bconstraints', 'UTILITY')
!
      Bconstraints(1:Noptpr,1:Nint)=ZERO

! Stretches:
      do Iatom=2,Natoms
        IZVAR=Zmatrix%bond(Iatom)%map
        IF(IZVAR.GT.0)then
        IF(Zmatrix%define(IZVAR)%opt)then
          IMCOOR=Iatom-1
          Bconstraints(IZVAR,IMCOOR)=ONE
        end if
        end if
      end do ! Iatom
!
! Bends
      IF(Natoms.GE.3)then
      do Iatom=3,Natoms
        IZVAR=Zmatrix%angle(Iatom)%map
        IZVAR=IABS(IZVAR)
        IF(IZVAR.GT.0)then
        IF(Zmatrix%define(IZVAR)%opt)then
          IMCOOR=Natoms+Iatom-3
          Bconstraints(IZVAR,IMCOOR)=ONE
        end if
        end if
      end do ! Iatom
!
! Torsions
      IF(Natoms.GE.4)then
      do Iatom=4,Natoms
        IZVAR=Zmatrix%torsion(Iatom)%map
        VSIGN=DSIGN(1.0D0,DBLE(IZVAR))
        IZVAR=IABS(IZVAR)
        IF(IZVAR.GT.0)then
        IF(Zmatrix%define(IZVAR)%opt)then
          IMCOOR=2*Natoms+Iatom-6
          Bconstraints(IZVAR,IMCOOR)=VSIGN
        end if
        end if
      end do ! Iatom
      end if ! Natoms.GE.4
      end if ! Natoms.GE.3
!
      write(UNIout,*)'Constraints matrix:'
      call PRT_matrix (Bconstraints, Noptpr, Nint)
!
! End of routine BLD_Bconstraints
      call PRG_manager ('exit', 'BLD_Bconstraints', 'UTILITY')
      return
      END
      subroutine Remove_Neigs (WJ,       & ! Force constant / Hessian
                               N,        & ! Dimension
                               NnegEig)    ! Number of negative eigenvalues
!***********************************************************************
!     Date last modified: April 13, 1998                               *
!     Author: R. A. Poirier                                            *
!     Description:                                                     *
!     Remove any extra negative eignvalues.                            *
!     W is a work vectors of length N.                         *
!     WFC is a copy of the Hessian.                                    *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE OPT_defaults
      USE matrix_print

      implicit none
!
! Input scalars:
      integer N
      integer NnegEig
!
! Input arrays:
      double precision WJ(N,N)
!
! Work arrays:
      double precision, dimension(:,:), allocatable :: WFC
      double precision, dimension(:), allocatable :: W
      double precision, dimension(:), allocatable :: work
!
! Local scalars:
      integer I,J,JJ,K
      double precision T
!
! Local parameters:
      double precision TENM3
      parameter (TENM3=1.0D-3)
!
      allocate (work(N), W(N), WFC(N,N))
!
! Save Hessian in work array
      WFC(1:N,1:N)=WJ(1:N,1:N)
      if(NnegEig.gt.1)then ! Remove extra negative eigenvalues:
      write(UNIout,'(a)')'****** Warning ******'
      write(UNIout,'(a)')'Attempting to remove extra negative eigenvalues '
      call MATRIX_diagonalize (WJ, WJ, W, N, -2, .true.) ! Get eigenvalues of Hessian (in a.u.)
        do I=2,N
          W(I)=DABS(W(I))
        end DO
!
! Back transform
      do i=1,n
        do k=1,n
          work(k)=WJ(i,k)*W(k)
        end do ! k

      do j=1,n
        WFC(i,j)=zero
        do k=1,n
          WFC(i,j)=WFC(i,j)+Work(k)*WJ(j,k)
        end do ! k
      end do ! j
      end do ! i
        WJ(1:N,1:N)=WFC(1:N,1:N)
        write(UNIout,'(/a)')' Hessian in Hartree/bohr**2 (Extra negative eigenvalues removed)'
        call PRT_matrix (WJ, N, N)
      end if ! NnegEig.gt.1
!
      deallocate (work, W, WFC)
!
      return
      end
      subroutine Scale_Hessian (Hessian_matrix, &
                                PARGRD, &
                                Noptpr)
!***********************************************************************
!     Date last modified: December   , 1994                            *
!     Author: R. A. Poirier                                            *
!     Description: Create and scale the inverse Hessian                *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE OPT_defaults
      USE matrix_print

      implicit none
!
! Input scalar:
      integer Noptpr
!
! Input arrays:
      double precision Hessian_matrix(Noptpr,Noptpr),PARGRD(Noptpr)
!
! Local scalars:
      integer IER,IPAR,JPAR,Kpar
      double precision MAXval,Scale,Scale_max,SPARI
      integer I,J,K
!
! Work arrays:
      double precision lambda(1,1)
      double precision, dimension(:), allocatable :: WW
      double precision, dimension(:,:), allocatable :: WFC
! For testing
      double precision, dimension(:,:), allocatable :: IHwork
      double precision, dimension(:,:), allocatable :: EigvalM
      double precision, dimension(:), allocatable :: Eigval
      double precision, dimension(:,:), allocatable :: Eigvec
      double precision, dimension(:), allocatable :: Work
      double precision, dimension(:,:), allocatable :: steps
!
! Local parameters:
      double precision MIN_eig,TENM3
      parameter (TENM3=1.0D-3,MIN_eig=0.02D0)
!
! Begin:
      call PRG_manager ('enter', 'Scale_Hessian', 'UTILITY')
!
      call GET_object ('OPT', 'HESSIAN', 'INVERSE') ! essential ??

      write(UNIout,'(a)') &
           'Scale_Hessian> Hessian before scaling (Hartree/bohr**2)'
      call PRT_matrix (Hessian_matrix, Noptpr, Noptpr)

      select case (Scale_method)

      case ('MAXSTEP')
      write(UNIout,'(a,1PE14.6)')'Using MAXSTEP to scale Hessian'
      write(UNIout,'(a,1PE14.6)')'Maximum step allowed in normal coordinates: ',MXSTEP
!      write(UNIout,*)'Minimum eigenvalue NOT SET: '
      write(UNIout,'(a,1PE14.6)')'Minimum eigenvalue set to: ',MIN_eig
!
! for testing
      allocate (IHwork(Noptpr,Noptpr), Eigvec(Noptpr,Noptpr), Eigval(Noptpr), EigvalM(Noptpr,Noptpr), &
                Work(Noptpr), steps(Noptpr,1))
      EigvalM(1:Noptpr,1:Noptpr)=ZERO
      IHwork=Hessian_matrix ! Copy hessian into work array (for testing)
! Get eigenvalues and eigenvectors of Hessian (IHwork)
      call MATRIX_diagonalize (IHwork, Eigvec, Eigval, Noptpr, 2, .false.) ! No sorting
      write(UNIout,'(/a)')' Eigenvalues of (un)symmetrized Hessian (Hartree/bohr**2)'
      do I=1,Noptpr
        write(UNIout,'(1X,I3,F15.6)')I,Eigval(I)
      end do ! I
      work=matmul(transpose(Eigvec), pargrd)
      write(UNIout,'(/a)')' gradients and step size in Normal coordinates and new eigenvalues'
      do I=1,Noptpr
        steps(I,1)=-work(I)/Eigval(I)
        if(dabs(steps(I,1)).gt.MXSTEP)then
          write(UNIout,'(a,I8)')'Step size to large for parameter ',I
          write(UNIout,'(a,F15.6)')'Step size = ',Steps(I,1)
          Eigval(I)=-work(I)/dsign(MXSTEP,steps(I,1))
          write(UNIout,'(a,F15.6)')'Eigenvalue set to ',Eigval(I)
        end if
        if(dabs(Eigval(I)).lt.MIN_eig)then
          write(UNIout,'(a,1PE14.6,a)')'Eigenvalue: ',Eigval(I),' too small'
          Eigval(I)=dsign(MIN_eig,Eigval(I))         ! Prevents large steps
          write(UNIout,'(a,1PE14.6,a)')'Eigenvalue set to: ',Eigval(I)
        end if
        write(UNIout,'(1X,I3,F15.6,1x,F15.6,1x,F15.6)')I,work(I),steps(I,1),Eigval(I)
        EigvalM(I,I)=Eigval(I)
      end do ! I
      Hessian_matrix=matmul(matmul(Eigvec, EigvalM), transpose(Eigvec))

      deallocate (IHwork, Eigval, Eigvec, EigvalM, Work, steps)

      case ('MARQUART')
      allocate (WFC(Noptpr,2*Noptpr), WW(4*Noptpr))
      write(UNIout,'(a,1PE14.6)')'Scale_Hessian> Marquart parameter: ',HMQPMIN
      do I=1,Noptpr
        do J=1,Noptpr
          WFC(J,I)=Hessian_matrix(J,I)
          WFC(J,Noptpr+I)=ZERO
        end do ! J
        WFC(I,Noptpr+I)=HMQPMIN
      end do ! I
!
! Calculate the generalized inverse in WFC and copy to Hessian
      call MB11MP (Noptpr, 2*Noptpr, WFC, WW)
      do I=1,Noptpr
        do J=1,Noptpr
          Hessian_matrix(J,I)=WFC(J,I)
        end do ! J
      end do ! I

      write(UNIout,'(a)')'Scale_Hessian> Inverse Hessian using MB11MP'
      call PRT_matrix (Hessian_matrix, Noptpr, Noptpr)
!
! Find maximum Newton-Raphson step size
      write(UNIout,'(a,1PE14.6)')'Maximum step allowed: ',MXSTEP
      do IPAR=1,Noptpr
        SPARI=ZERO
        do JPAR=1,Noptpr
          SPARI=SPARI-Hessian_matrix(IPAR,JPAR)*PARGRD(JPAR)
        end do ! JPAR
!
        if(DABS(SPARI).GT.MXSTEP)then
          write(UNIout,'(a,i10,a,1PE14.6)')'For parameter ',IPAR,' Step = ',SPARI
          write(UNIout,'(a,i10,a,1PE14.6)')'For parameter ',IPAR,' Steepest descent = ',-pargrd(IPAR)
        end if
      end do ! IPAR
!
! Regenerate the Hessian based on the new inverse
      call invert (Hessian_matrix, Noptpr, Noptpr, IER)

      deallocate (WW, WFC)

      case ('SD')
      write(UNIout,'(a)')'Maximum step allowed set to MXSTEP in OPT coordinates:'
      write(UNIout,'(a,1PE14.6)')'MXSTEP = ',MXSTEP
      allocate (IHwork(Noptpr,Noptpr))
      IHwork=Hessian_matrix ! Copy hessian into work array
      call invert (IHwork, Noptpr, Noptpr, IER)
      Scale_max=ONE
!
! Find maximum Newton-Raphson step size
      do IPAR=1,Noptpr
        SPARI=ZERO
        do JPAR=1,Noptpr
          SPARI=SPARI-IHwork(IPAR,JPAR)*PARGRD(JPAR)
        end do ! JPAR
        write(UNIout,'(a,i10,a,1PE14.6)')'For parameter ',IPAR,' Step = ',SPARI
!
        if(DABS(SPARI).GT.MXSTEP)then
          Scale=MXSTEP/dabs(SPARI)
          write(UNIout,'(a,i10,a,1PE14.6,a,0PF12.2)')'For parameter ',IPAR,' Step = ',SPARI,' scale= ',Scale
          if(Scale.lt.Scale_max)Scale_max=Scale
          write(UNIout,'(a,i10,a,1PE14.6)')'For parameter ',IPAR,' Steepest descent = ',-pargrd(IPAR)
        end if
      end do ! IPAR

      write(UNIout,'(a,F12.2)')' scale max= ',Scale_max
      do IPAR=1,Noptpr
      do KPAR=Ipar,Noptpr
        Hessian_matrix(IPAR,KPAR)=Hessian_matrix(IPAR,KPAR)/Scale_max
        Hessian_matrix(KPAR,IPAR)=Hessian_matrix(IPAR,KPAR) ! make sure H remains symmetric
      end do ! KPAR
      end do ! IPAR

      deallocate (IHwork)
!

      case ('SH')
      write(UNIout,'(a)')'Use (H-lambda*I)'
      allocate (IHwork(Noptpr,Noptpr), steps(Noptpr,1), WFC(Noptpr,1))
      IHwork=Hessian_matrix ! Copy hessian into work array
      call invert (IHwork, Noptpr, Noptpr, IER)
! Find maximum Newton-Raphson step size
      do IPAR=1,Noptpr
        SPARI=ZERO
        do JPAR=1,Noptpr
          SPARI=SPARI-IHwork(IPAR,JPAR)*PARGRD(JPAR)
        end do ! JPAR
        write(UNIout,'(a,i10,a,1PE14.6)')'For parameter ',IPAR,' Step = ',SPARI
        Steps(Ipar,1)=SPARI
!
        if(DABS(SPARI).GT.MXSTEP)then
          Steps(Ipar,1)=dsign(MXSTEP, SPARI)
        write(UNIout,'(a,i10,a,1PE14.6)')'For parameter ',IPAR,' Step set to ',Steps(Ipar,1)
        end if
      end do ! IPAR

      WFC(1:Noptpr,1)=pargrd(1:Noptpr)
      Lambda=matmul(transpose(Steps), matmul(Hessian_matrix, Steps))
      Lambda=Lambda+matmul(transpose(Steps), WFC)
      Lambda=Lambda/matmul(transpose(Steps), Steps)
      write(UNIout,'(a,f12.5)')'Lambda = ',Lambda
      do IPAR=1,Noptpr
        Hessian_matrix(IPAR,IPAR)=Hessian_matrix(IPAR,IPAR)-Lambda(1,1)
      end do ! IPAR

      deallocate (IHwork, steps, WFC)

      end select
!
      write(UNIout,'(a)')'Scale_Hessian> Hessian after scaling (Hartree/bohr**2)'
      call PRT_matrix (Hessian_matrix, Noptpr, Noptpr)
!
! End of routine Scale_Hessian
      call PRG_manager ('exit', 'Scale_Hessian', 'UTILITY')
      return
      end
      subroutine FC_convert (FC,   & ! Force constant / Hessian
                             N)      ! Dimension
!***********************************************************************
!     Date last modified: April 13, 1998                               *
!     Author: R. A. Poirier                                            *
!     Description:                                                     *
!     Converts the Hessian from a.u. to Mdyne/Angstrom.                 *
!***********************************************************************
! Modules:
      USE program_constants
      USE OPT_objects

      implicit none
!
! Input scalars:
      integer N
!
! Input arrays:
      double precision FC(N,N)
!
! Local scalars:
      integer I,J
!
      do I=1,N
      do J=1,N
        if(PAR_type(I).and.PAR_type(J))then      ! stretch-stretch
          FC(I,J)=FC(I,J)*FCconv_str
        else if(PAR_type(I).or.PAR_type(J))then  ! stretch-bend
          FC(I,J)=FC(I,J)*Fconv
        else
          FC(I,J)=FC(I,J)*FCconv                 ! bend-bend
        end if
      end do ! J
      end do ! I

      return
      end
      subroutine PRT_hessian (FC,       & ! Force constant / Hessian
                              N)       ! Dimension
!***********************************************************************
!     Date last modified: July 28, 2004                                *
!     Author: R. A. Poirier                                            *
!     Description:                                                     *
!     Print the force constant matrix in units of Mdyne/Angstrom.      *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE OPT_defaults
      USE matrix_print
      USE OPT_objects

      implicit none
!
! Input scalars:
      integer N
!
! Input arrays:
      double precision FC(N,N)
!
! Work array:
      double precision, dimension(:,:), allocatable :: Work
!
! Local scalars:
      integer I,J
!
      allocate (Work(N,N))

      if(.not.allocated(PAR_type))then
        write(UNIout,'(a)')'ERROR> PRT_hessian:'
        write(UNIout,'(a)')'PAR_type not define for hessian parameters'
        call PRG_stop ('PAR_type not define for hessian parameters')
      end if
      Work=FC
      do I=1,N
      do J=1,N
        if(PAR_type(I).and.PAR_type(J))then      ! stretch-stretch
          Work(I,J)=Work(I,J)*FCconv_str
        else if(PAR_type(I).or.PAR_type(J))then  ! stretch-bend
          Work(I,J)=Work(I,J)*Fconv
        else
          Work(I,J)=Work(I,J)*FCconv                 ! bend-bend
        end if
      end do ! J
      end do ! I

      write(UNIout,'(/a)')' Hessian in Mdyne/Angstrom'
      call PRT_matrix (Work, N, N)

      deallocate (Work)

      return
      end
      subroutine OBJ_hessian (nameIN, &
                              Command)
!***********************************************************************
!     Date last modified: April 13, 1998                               *
!     Author: R. A. Poirier                                            *
!     Description:                                                     *
!     Print the force constant matrix.                                 *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE OPT_defaults
      USE matrix_print
      USE mod_type_hessian
      USE matrix_load

      implicit none
!
!Input scalars:
      character(len=*) nameIN,Command
!
! Local scalars:
      integer GET_object_number
!
! Begin:
      call PRG_manager ('enter', 'OBJ_HESSIAN', 'UTILITY')
!
      COMMAND_name: select case (Command)
      case ('KILL')
! deallocate arrays
!
      case ('PRINT')
        !call GET_object(nameIN) ! what?? replaced with line below JWH 2022/09/26
        call GET_object ('OPT', 'HESSIAN', Hessian_method(1:len_trim(Hessian_method)))
        write(UNIout,'(a)')'Hessian'
!         call PRT_Hessian (Hessian_matrix, size(Hessian_matrix,1)) ! REquires PAR_type!!!!!!!!!

        call PRT_matrix (Hessian_matrix, size(Hessian_matrix,1), size(Hessian_matrix,2))

      case ('FREE')
        DUMP_to_file=.true.
        header_dim1=size(Hessian_matrix,1)
        header_dim2=size(Hessian_matrix,2)
        call WRITE_header (nameIN)
        call PRT_matrix (Hessian_matrix, size(Hessian_matrix,1), size(Hessian_matrix,2))
        DUMP_to_file=.false.

      case ('LOAD')
        write(UNIout,'(a)')'Loading of object from disk testing stage only'
        call READ_header (header_unit)
        if(.not.allocated(Hessian_matrix))then
          allocate (Hessian_matrix(header_dim1, header_dim2))
        else
          deallocate (Hessian_matrix)
          allocate (Hessian_matrix(header_dim1, header_dim2))
        end if
        call LOAD_matrix (Hessian_matrix, header_dim1, header_dim2)
        write(UNIout,'(a)')'Hessian'
        call PRT_matrix (Hessian_matrix, header_dim1, header_dim2)
      end select COMMAND_name
!
      call PRG_manager ('exit', 'OBJ_HESSIAN', 'UTILITY')
      return
      end
      subroutine Diag_Hessian  (Hessian,   & ! Force constant / Hessian
                                Noptpr,    & ! Dimension
                                Eigval,  & ! output eigenvalues
                                Eigvec,  & ! output eigenvectors
                                Lsort)  ! sort?
!***********************************************************************
!     Date last modified: April 13, 1998                               *
!     Author: R. A. Poirier                                            *
!     Description:                                                     *
!     Print the force constant matrix and diagonalize it.              *
!     W is a  work vectors of length MAX.                       *
!     WFC is a copy of the Hessian.                                    *
!     MAX is the dimension of Hessian, which is filled to size N.      *
!                                                                      *
!***********************************************************************
! Modules:
      USE program_constants

      implicit none
!
! Input scalars:
      integer Noptpr
      logical Lsort
!
! Input arrays:
      double precision Hessian(Noptpr,Noptpr)
!
! Output arrays:
      double precision Eigval(Noptpr),Eigvec(Noptpr, Noptpr)
!
! Work arrays:
!
! Local scalars:
      integer I,J
      double precision TEMP
!
! Begin:
      call PRG_manager ('enter', 'DIAG_Hessian', 'UTILITY')
!
!     Make Hessian symmetrical.
      do J=1,Noptpr
      do I=1,J
        TEMP=PT5*(Hessian(I,J)+Hessian(J,I))
        Hessian(I,J)=TEMP
        Hessian(J,I)=TEMP
      end DO
      end DO
!
! Get eigenvalues and eigenvectors of Hessian
      call MATRIX_diagonalize (Hessian, Eigvec, Eigval, Noptpr, 2, Lsort)
!
      call PRG_manager ('exit', 'DIAG_Hessian', 'UTILITY')
      return
      end
