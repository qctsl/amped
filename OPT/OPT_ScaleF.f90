      subroutine Sfactor_TO_OPT
!***********************************************************************
!     Date last modified: January 27, 1998                Version 2.0  *
!     Author: R.A. Poirier                                             *
!     Description: Set parameters for optimization.                    *
!***********************************************************************
! Modules:
      USE program_files
      USE type_basis_set
      USE OPT_defaults
      USE OPT_objects

      implicit none
!
! Local scalars:
      integer :: Ishell
!
! Begin:
      call PRG_manager ('enter', 'Sfactor_TO_OPT', 'UTILITY')
!
      REMeigs=.true.
      write(6,*)'Current Basis set is: ',basis%name

! First determine Noptpr
      Noptpr=Basis%Nshells
      if(.not.allocated(PARset))then
        allocate (OPT_par(Noptpr))
        allocate (PARset(Noptpr))
      else
        allocate (OPT_par(Noptpr))
        deallocate (PARset)
        allocate (PARset(Noptpr))
      end if

      do Ishell=1,Basis%Nshells
        OPT_par(Ishell)%value=Basis%shell(Ishell)%SHLSCL
        PARset(Ishell)=Basis%shell(Ishell)%SHLSCL
      end do

!
! End of routine Sfactor_TO_OPT
      call PRG_manager ('exit', 'Sfactor_TO_OPT', 'UTILITY')
      return
      end
      subroutine OPT_TO_Sfactor
!***********************************************************************
!     Date last modified: January 28, 1998                 Version 2.0 *
!     Author: R. A. Poirier                                            *
!     Description: Update the Cartesian coordinates.                   *
!***********************************************************************
! Modules:
      USE type_basis_set
      USE OPT_objects

      implicit none
!
! Local scalars:
      integer :: Igauss,IGBGN,IGEND,Ishell
      double precision :: Gexp,SFold,SFold2,SFnew,SFnew2
!
! Begin:
      call PRG_manager ('enter', 'OPT_TO_Sfactor', 'UTILITY')
!
      do Ishell=1,Basis%Nshells
        OPT_par(Ishell)%value=dabs(OPT_par(Ishell)%value) ! Just in case
        PARset(Ishell)=dabs(PARset(Ishell)) ! Just in case
        SFold=Basis%shell(Ishell)%SHLSCL
        SFold2=SFold*SFold
        IGBGN=Basis%shell(Ishell)%EXPBGN
        IGEND=Basis%shell(Ishell)%EXPEND
        SFnew=PARset(Ishell)
        SFnew2=SFnew*SFnew
        Basis%shell(Ishell)%SHLSCL=SFnew

! Unscale and re-scale the Gaussians for this shell
      do Igauss=IGBGN,IGEND
        Gexp=Basis%gaussian(Igauss)%exp/SFold2
        Basis%gaussian(Igauss)%exp=Gexp*SFnew2
      end do ! Igauss
      end do ! Ishell
!
! End of routine OPT_TO_Sfactor
      call PRG_manager ('exit', 'OPT_TO_Sfactor', 'UTILITY')
      return
      end
