      subroutine dGAdGB_products (rx, ry, rz, dxdxGprod, dydyGprod, dzdzGprod, MATlen)
!****************************************************************************************
!     Date last modified: May 6, 2017 by Ibrahim Awad                                   *
!     Authors: Ahmad and R.A. Poirier                                                   *
!     Description: Compute d/dx, d/dy and d/dz of the product of two Gaussians          *
!                                                                                       *
!****************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE type_basis_set
      USE matrix_print
      USE QM_defaults

      implicit none
!
! Input scalars:
      integer :: MATlen
      double precision :: rx,ry,rz
      double precision :: dxdxGprod(MATlen),dydyGprod(MATlen),dzdzGprod(MATlen)
!
! Local scalars:
      integer :: Ishell,Jshell,Ifrst,Jfrst,Ilast,Jlast
      integer :: Istart,Jstart,Iend,Jend
      integer :: Iatmshl,Jatmshl
      integer :: LAMAX,LBMAX
      integer :: Iatom,Jatom,LS,LL,LNUM,AONUM
      integer :: Irange,Jrange,MINrange,MAXrange,MINtype,MAXtype
      integer :: Igauss,Jgauss
      integer :: IGBGN,JGBGN,IGend,JGend
      integer :: Iao,Jao,AOI,AOJ
      integer :: I,J,K,INDEX,DEFCASE,IJao
      double precision :: COEF_IJ,COEF_SXY,COEF_PXY
      double precision :: COEF_XXXY,COEF_XYXY,SQRT3,SQRT5,SUMDSQ,Gama,Alpha,Beta,RABSQ,SASB,SASB_K,SASB_exp
      double precision :: Ax,Bx,Ay,By,Az,Bz,Px,Py,Pz,xA,yA,zA,xB,yB,zB,rpx,rpy,rpz
      double precision :: dGAx,dGAy,dGAz
      double precision :: dGBx,dGBy,dGBz, dEGAALL,dEGBALL
      logical :: LIatmshl
!
! Local arrays  
      double precision :: dAdB(441,3)     ! dxAdxB,dyAdyB,dzAdzB  , save derivatives
      double precision :: GFA(21),GFB(21),dGFA(21,3),DGFB(21,3)
      double precision :: dEGA(3)=ZERO, dEGB(3)=ZERO

! Parameters:
      double precision FIVE
      parameter (FIVE=5.0D0)
!
! Begin:
      SQRT3=DSQRT(THREE) 
      SQRT5=DSQRT(FIVE)
!
      dxdxGprod(1:MATlen)=ZERO
      dydyGprod(1:MATlen)=ZERO
      dzdzGprod(1:MATlen)=ZERO
!
! Begin loop over shells.
!
! Loop over elemental SHELLs
! Loop over Ishell.
      do Ishell=1,Basis%Nshells
        LAMAX=Basis%shell(Ishell)%Xtype+1
        Istart=Basis%shell(Ishell)%Xstart
        Iend=Basis%shell(Ishell)%XEND
        Irange=Iend-Istart+1
        IGBGN=Basis%shell(Ishell)%EXPBGN
        IGEND=Basis%shell(Ishell)%EXPEND
        Ifrst=Basis%shell(Ishell)%frstSHL
!
! Loop over Jshell.
      do Jshell=1,Ishell
        LBMAX=Basis%shell(Jshell)%Xtype+1
        Jstart=Basis%shell(Jshell)%Xstart
        Jend=Basis%shell(Jshell)%XEND
        Jrange=Jend-Jstart+1
        JGBGN=Basis%shell(Jshell)%EXPBGN
        JGEND=Basis%shell(Jshell)%EXPEND
        Jfrst=Basis%shell(Jshell)%frstSHL
!
! Define CASE:
      if(LAMAX.lt.LBMAX)then
       MINrange=Irange
       MAXrange=Jrange
       MINtype=LAMAX
       MAXtype=LBMAX
      else
       MINrange=Jrange
       MAXrange=Irange
       MINtype=LBMAX
       MAXtype=LAMAX
      end if
!
!
      Ilast= Basis%shell(Ishell)%lastSHL
      Jlast= Basis%shell(Jshell)%lastSHL
!
      call AB_dGproducts
!
! End of loop over shells.
      
      end do ! Jshell
      end do ! Ishell
!
! End of routine dGAdGB_PRODUCTS
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine AB_dGproducts
!****************************************************************************************
!     Date last modified: May 6, 2017 by Ibrahim Awad                                             *
!     Authors: Ahmad and R.A. Poirier                                                   *
!     Description:                                                                      *
!                                                                                       *
!****************************************************************************************
! Modules:

      implicit none
!
! Loop over all atomic shells
      do Iatmshl=Ifrst,Ilast
        Iatom=Basis%atmshl(Iatmshl)%ATMLST
!
        Ax=CARTESIAN(Iatom)%X     ! vector A={Ax,Ay,Az}     
        Ay=CARTESIAN(Iatom)%Y
        Az=CARTESIAN(Iatom)%Z
!
        AOI=Basis%atmshl(Iatmshl)%frstAO-1
        IF(Ishell.EQ.Jshell)Jlast=Iatmshl
      do Jatmshl=Jfrst,Jlast
        Jatom=Basis%atmshl(Jatmshl)%ATMLST
!
        Bx=CARTESIAN(Jatom)%X     ! vector B={Bx,By,Bz}
        By=CARTESIAN(Jatom)%Y
        Bz=CARTESIAN(Jatom)%Z
!
        AOJ=Basis%atmshl(Jatmshl)%frstAO-1
        LIatmshl=.false.
        if(Iatmshl.eq.Jatmshl)LIatmshl=.true.

        RABSQ=(Ax-Bx)*(Ax-Bx)+(Ay-By)*(Ay-By)+(Az-Bz)*(Az-Bz)     ! (A-B)**2

        dAdB(1:441,1:3)=ZERO     ! will handle up to <f|f>
!
! Loop over primitive gaussians
      do Igauss=IGBGN,IGEND
        Alpha=Basis%gaussian(Igauss)%exp
      do Jgauss=JGBGN,JGEND
        Beta=Basis%gaussian(Jgauss)%exp
!
        Gama=Alpha+Beta
        Px=(Alpha*Ax+Beta*Bx)/Gama     ! vector P
        Py=(Alpha*Ay+Beta*By)/Gama
        Pz=(Alpha*Az+Beta*Bz)/Gama
        rpx = Px-rx     ! vector rp=P-r
        rpy = Py-ry
        rpz = Pz-rz
        SUMDSQ = rpx*rpx + rpy*rpy + rpz*rpz   ! (rp)**2
        SASB_K=DEXP(-Alpha*Beta*RABSQ/Gama)      ! K 
        SASB_exp=DEXP(-Gama*SUMDSQ)              ! exp
        SASB=SASB_K*SASB_exp                         ! K*exp
!
        xA=rx-Ax     ! vector rA=(r-A)
        yA=ry-Ay
        zA=rz-Az
        xB=rx-Bx     ! vector rB=(r-B)
        yB=ry-By
        zB=rz-Bz

      COEF_IJ = Basis%gaussian(Igauss)%CONTRC*Basis%gaussian(Jgauss)%CONTRC*SASB

      call Gaussian_function (xA, yA, zA, GFA, MINtype)
      call Gaussian_function (xB, yB, zB, GFB, MAXtype)
      call DelGaussian_function (xA, yA, zA, dGFA, MINtype)
      call DelGaussian_function (xB, yB, zB, dGFB, MAXtype)
      call DELExpGaussian_function (xA, yA, zA, Alpha, dEGA)
      call DELExpGaussian_function (xB, yB, zB, Beta , dEGB)
      LNUM=0
        do LS=1,MINrange
           do LL=1,MAXrange
              LNUM=LNUM+1
              do AONUM=1, 3 ! dx, dy, dz

                 !Rule >>>> d(fg)=df.g+f.dg
                 dEGAALL=dGFA(LS,AONUM)+GFA(LS)*dEGA(AONUM)
                 dEGBALL=dGFB(LL,AONUM)+GFB(LL)*dEGB(AONUM)

                 dAdB(LNUM,AONUM)=dAdB(LNUM,AONUM)+COEF_IJ*dEGAALL*dEGBALL
              end do
           end do
        end do
!
      end do ! Jgauss
      end do ! Igauss 
      
        IF(LIatmshl)then
          INDEX=0
          do I=1,Irange
          do J=1,Jrange
            INDEX=INDEX+1
            IF(I.GE.J)then  ! Corrected August 1,2003 had (J.ge.I)
              Iao=AOI+I
              Jao=AOJ+J
              IJao=Iao*(Iao-1)/2+Jao
              dxdxGprod(IJao)=dAdb(INDEX,1)
              dydyGprod(IJao)=dAdB(INDEX,2)
              dzdzGprod(IJao)=dAdB(INDEX,3)
            end if
          end do ! J   
          end do ! I
        else
              INDEX=0
          do I=1,Irange
          do J=1,Jrange
            INDEX=INDEX+1
            Iao=AOI+I
            Jao=AOJ+J
            IJao=Iao*(Iao-1)/2+Jao
              dxdxGprod(IJao)=dAdb(INDEX,1)
              dydyGprod(IJao)=dAdB(INDEX,2)
              dzdzGprod(IJao)=dAdB(INDEX,3)
          end do ! J
          end do ! I
        end if ! LIatmshl

      end do ! Jatmshl
      end do ! Iatmshl

      return
      end subroutine AB_dGproducts
! END CONTAINS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine dGAdGB_PRODUCTS
