      subroutine dGAdGB_products (rx, ry, rz, dxdxGprod, dydyGprod, dzdzGprod, MATlen)
!****************************************************************************************
!     Date last modified: December 3, 2015                                              *
!     Authors: Ahmad and R.A. Poirier                                                   *
!     Description: Compute d/dx, d/dy and d/dz of the product of two Gaussians          *
!                                                                                       *
!****************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE type_basis_set
      USE matrix_print
      USE QM_defaults

      implicit none
!
! Input scalars:
      integer :: MATlen
      double precision :: rx,ry,rz
      double precision :: dxdxGprod(MATlen),dydyGprod(MATlen),dzdzGprod(MATlen)
!
! Local scalars:
      integer :: Ishell,Jshell,Ifrst,Jfrst,Ilast,Jlast
      integer :: Istart,Jstart,Iend,Jend
      integer :: Iatmshl,Jatmshl
      integer :: LAMAX,LBMAX
      integer :: Iatom,Jatom
      integer :: Irange,Jrange
      integer :: Igauss,Jgauss
      integer :: IGBGN,JGBGN,IGend,JGend
      integer :: Iao,Jao,AOI,AOJ
      integer :: I,J,K,INDEX,DEFCASE,IJao
      double precision :: COEF_IJ,COEF_SXY,COEF_PXY
      double precision :: COEF_XXXY,COEF_XYXY,SQRT3,SUMDSQ,Gama,Alpha,Beta,RABSQ,SASB,SASB_K,SASB_exp
      double precision :: Ax,Bx,Ay,By,Az,Bz,Px,Py,Pz,xA,yA,zA,xB,yB,zB,rpx,rpy,rpz
      double precision :: dGAx,dGAy,dGAz
      double precision :: dGBx,dGBy,dGBz
      logical :: LIatmshl
!
! Local arrays  
      double precision :: dAdxdBdx(100),dAdydBdy(100),dAdzdBdz(100)     ! save derivatives

! Begin:
      SQRT3=DSQRT(THREE)
!
      dxdxGprod(1:MATlen)=ZERO
      dydyGprod(1:MATlen)=ZERO
      dzdzGprod(1:MATlen)=ZERO
!
! Begin loop over shells.
!
! Loop over elemental SHELLs
! Loop over Ishell.
      do Ishell=1,Basis%Nshells
        LAMAX=Basis%shell(Ishell)%Xtype+1
        Istart=Basis%shell(Ishell)%Xstart
        Iend=Basis%shell(Ishell)%XEND
        Irange=Iend-Istart+1
        IGBGN=Basis%shell(Ishell)%EXPBGN
        IGEND=Basis%shell(Ishell)%EXPEND
        Ifrst=Basis%shell(Ishell)%frstSHL
!
! Loop over Jshell.
      do Jshell=1,Ishell
        LBMAX=Basis%shell(Jshell)%Xtype+1
        Jstart=Basis%shell(Jshell)%Xstart
        Jend=Basis%shell(Jshell)%XEND
        Jrange=Jend-Jstart+1
        JGBGN=Basis%shell(Jshell)%EXPBGN
        JGEND=Basis%shell(Jshell)%EXPEND
        Jfrst=Basis%shell(Jshell)%frstSHL
!
! Define CASE:
      if(LAMAX.lt.LBMAX)then
       DEFCASE=4*(LAMAX-1)+LBMAX
      else
       DEFCASE=4*(LBMAX-1)+LAMAX
      end if
!
      Ilast= Basis%shell(Ishell)%lastSHL
      Jlast= Basis%shell(Jshell)%lastSHL
!
      call AB_dGproducts
!
! End of loop over shells.
      
      end do ! Jshell
      end do ! Ishell
!
! End of routine dGAdGB_PRODUCTS
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine AB_dGproducts
!****************************************************************************************
!     Date last modified: December 3, 2015                                              *
!     Authors: Ahmad and R.A. Poirier                                                   *
!     Description:                                                                      *
!                                                                                       *
!****************************************************************************************
! Modules:

      implicit none
!
! Loop over all atomic shells
      do Iatmshl=Ifrst,Ilast
        Iatom=Basis%atmshl(Iatmshl)%ATMLST
!
        Ax=CARTESIAN(Iatom)%X     ! vector A={Ax,Ay,Az}     
        Ay=CARTESIAN(Iatom)%Y
        Az=CARTESIAN(Iatom)%Z
!
        AOI=Basis%atmshl(Iatmshl)%frstAO-1
        IF(Ishell.EQ.Jshell)Jlast=Iatmshl
      do Jatmshl=Jfrst,Jlast
        Jatom=Basis%atmshl(Jatmshl)%ATMLST
!
        Bx=CARTESIAN(Jatom)%X     ! vector B={Bx,By,Bz}
        By=CARTESIAN(Jatom)%Y
        Bz=CARTESIAN(Jatom)%Z
!
        AOJ=Basis%atmshl(Jatmshl)%frstAO-1
        LIatmshl=.false.
        if(Iatmshl.eq.Jatmshl)LIatmshl=.true.

        RABSQ=(Ax-Bx)*(Ax-Bx)+(Ay-By)*(Ay-By)+(Az-Bz)*(Az-Bz)     ! (A-B)**2

        dAdxdBdx(1:100)=ZERO     ! will handle up to <f|f>
        dAdydBdy(1:100)=ZERO
        dAdzdBdz(1:100)=ZERO
!
! Loop over primitive gaussians
      do Igauss=IGBGN,IGEND
        Alpha=Basis%gaussian(Igauss)%exp
      do Jgauss=JGBGN,JGEND
        Beta=Basis%gaussian(Jgauss)%exp
!
        Gama=Alpha+Beta
        Px=(Alpha*Ax+Beta*Bx)/Gama     ! vector P
        Py=(Alpha*Ay+Beta*By)/Gama
        Pz=(Alpha*Az+Beta*Bz)/Gama
        rpx = Px-rx     ! vector rp=P-r
        rpy = Py-ry
        rpz = Pz-rz
        SUMDSQ = rpx*rpx + rpy*rpy + rpz*rpz   ! (rp)**2
        SASB_K=DEXP(-Alpha*Beta*RABSQ/Gama)      ! K 
        SASB_exp=DEXP(-Gama*SUMDSQ)              ! exp
        SASB=SASB_K*SASB_exp                         ! K*exp
!
        xA=rx-Ax     ! vector rA=(r-A)
        yA=ry-Ay
        zA=rz-Az
        xB=rx-Bx     ! vector rB=(r-B)
        yB=ry-By
        zB=rz-Bz
!
      dGAx=-TWO*Alpha*xA     ! derivative of GA with respect to x
      dGAy=-TWO*Alpha*yA     ! derivative of GA with respect to y
      dGAz=-TWO*Alpha*zA     ! derivative of GA with respect to z 
      dGBx=-TWO*Beta*xB     ! derivative of GB with respect to x
      dGBy=-TWO*Beta*yB     ! derivative of GB with respect to y
      dGBz=-TWO*Beta*zB     ! derivative of GB with respect to z 

      COEF_IJ = Basis%gaussian(Igauss)%CONTRC*Basis%gaussian(Jgauss)%CONTRC
      select case (DEFCASE)
     case (1)
! S|S
        dAdxdBdx(1)=dAdxdBdx(1)+COEF_IJ*dGAx*dGBx*SASB
        dAdydBdy(1)=dAdydBdy(1)+COEF_IJ*dGAy*dGBy*SASB
        dAdzdBdz(1)=dAdzdBdz(1)+COEF_IJ*dGAz*dGBz*SASB
      case (2)
! S|Px
        dAdxdBdx(1)=dAdxdBdx(1)+COEF_IJ*dGAx*(xB*dGBx+ONE)*SASB
        dAdydBdy(1)=dAdydBdy(1)+COEF_IJ*dGAy*(xB*dGBy)*SASB
        dAdzdBdz(1)=dAdzdBdz(1)+COEF_IJ*dGAz*(xB*dGBz)*SASB
! S|Py
        dAdxdBdx(2)=dAdxdBdx(2)+COEF_IJ*dGAx*(yB*dGBx)*SASB
        dAdydBdy(2)=dAdydBdy(2)+COEF_IJ*dGAy*(yB*dGBy+ONE)*SASB
        dAdzdBdz(2)=dAdzdBdz(2)+COEF_IJ*dGAz*(yB*dGBz)*SASB
! S|Pz
        dAdxdBdx(3)=dAdxdBdx(3)+COEF_IJ*dGAx*(zB*dGBx)*SASB
        dAdydBdy(3)=dAdydBdy(3)+COEF_IJ*dGAy*(zB*dGBy)*SASB
        dAdzdBdz(3)=dAdzdBdz(3)+COEF_IJ*dGAz*(zB*dGBz+ONE)*SASB
      case (3)
!S|Dxx
        COEF_SXY = SQRT3*COEF_IJ
        dAdxdBdx(1)=dAdxdBdx(1)+COEF_IJ*dGAx*(xB*xB*dGBx+TWO*xB)*SASB
        dAdydBdy(1)=dAdydBdy(1)+COEF_IJ*dGAy*(xB*xB*dGBy)*SASB
        dAdzdBdz(1)=dAdzdBdz(1)+COEF_IJ*dGAz*(xB*xB*dGBz)*SASB
!S|Dyy
        dAdxdBdx(2)=dAdxdBdx(2)+COEF_IJ*dGAx*(yB*yB*dGBx)*SASB
        dAdydBdy(2)=dAdydBdy(2)+COEF_IJ*dGAy*(yB*yB*dGBy+TWO*yB)*SASB
        dAdzdBdz(2)=dAdzdBdz(2)+COEF_IJ*dGAz*(yB*yB*dGBz)*SASB
!S|Dzz
        dAdxdBdx(3)=dAdxdBdx(3)+COEF_IJ*dGAx*(zB*zB*dGBx)*SASB
        dAdydBdy(3)=dAdydBdy(3)+COEF_IJ*dGAy*(zB*zB*dGBy)*SASB
        dAdzdBdz(3)=dAdzdBdz(3)+COEF_IJ*dGAz*(zB*zB*dGBz+TWO*zB)*SASB
!S|Dxy
        dAdxdBdx(4)=dAdxdBdx(4)+COEF_SXY*dGAx*(xB*yB*dGBx+yB)*SASB
        dAdydBdy(4)=dAdydBdy(4)+COEF_SXY*dGAy*(xB*yB*dGBy+xB)*SASB
        dAdzdBdz(4)=dAdzdBdz(4)+COEF_SXY*dGAz*(xB*yB*dGBz)*SASB
!S|Dxz
        dAdxdBdx(5)=dAdxdBdx(5)+COEF_SXY*dGAx*(xB*zB*dGBx+zB)*SASB
        dAdydBdy(5)=dAdydBdy(5)+COEF_SXY*dGAy*(xB*zB*dGBy)*SASB
        dAdzdBdz(5)=dAdzdBdz(5)+COEF_SXY*dGAz*(xB*zB*dGBz+xB)*SASB
!S|Dyz
        dAdxdBdx(6)=dAdxdBdx(6)+COEF_SXY*dGAx*(yB*zB*dGBx)*SASB
        dAdydBdy(6)=dAdydBdy(6)+COEF_SXY*dGAy*(yB*zB*dGBy+zB)*SASB
        dAdzdBdz(6)=dAdzdBdz(6)+COEF_SXY*dGAz*(yB*zB*dGBz+yB)*SASB
      case (6)
!PxPx
        dAdxdBdx(1)=dAdxdBdx(1)+COEF_IJ*(xA*dGAx+ONE)*(xB*dGBx+ONE)*SASB
        dAdydBdy(1)=dAdydBdy(1)+COEF_IJ*xA*dGAy*(xB*dGBy)*SASB
        dAdzdBdz(1)=dAdzdBdz(1)+COEF_IJ*xA*dGAz*(xB*dGBz)*SASB
!PxPy
        dAdxdBdx(2)=dAdxdBdx(2)+COEF_IJ*(xA*dGAx+ONE)*(yB*dGBx)*SASB
        dAdydBdy(2)=dAdydBdy(2)+COEF_IJ*xA*dGAy*(yB*dGBy+ONE)*SASB
        dAdzdBdz(2)=dAdzdBdz(2)+COEF_IJ*xA*dGAz*(yB*dGBz)*SASB
!PxPz
        dAdxdBdx(3)=dAdxdBdx(3)+COEF_IJ*(xA*dGAx+ONE)*(zB*dGBx)*SASB
        dAdydBdy(3)=dAdydBdy(3)+COEF_IJ*xA*dGAy*(zB*dGBy)*SASB
        dAdzdBdz(3)=dAdzdBdz(3)+COEF_IJ*xA*dGAz*(zB*dGBz+ONE)*SASB
!PyPx
        dAdxdBdx(4)=dAdxdBdx(4)+COEF_IJ*yA*dGAx*(xB*dGBx+ONE)*SASB
        dAdydBdy(4)=dAdydBdy(4)+COEF_IJ*(yA*dGAy+ONE)*(xB*dGBy)*SASB
        dAdzdBdz(4)=dAdzdBdz(4)+COEF_IJ*yA*dGAz*(xB*dGBz)*SASB
!PyPy
        dAdxdBdx(5)=dAdxdBdx(5)+COEF_IJ*yA*dGAx*(yB*dGBx)*SASB
        dAdydBdy(5)=dAdydBdy(5)+COEF_IJ*(yA*dGAy+ONE)*(yB*dGBy+ONE)*SASB
        dAdzdBdz(5)=dAdzdBdz(5)+COEF_IJ*yA*dGAz*(yB*dGBz)*SASB
!PyPz
        dAdxdBdx(6)=dAdxdBdx(6)+COEF_IJ*yA*dGAx*(zB*dGBx)*SASB
        dAdydBdy(6)=dAdydBdy(6)+COEF_IJ*(yA*dGAy+ONE)*(zB*dGBy)*SASB
        dAdzdBdz(6)=dAdzdBdz(6)+COEF_IJ*yA*dGAz*(zB*dGBz+ONE)*SASB
!PzPx
        dAdxdBdx(7)=dAdxdBdx(7)+COEF_IJ*zA*dGAx*(xB*dGBx+ONE)*SASB
        dAdydBdy(7)=dAdydBdy(7)+COEF_IJ*zA*dGAy*(xB*dGBy)*SASB
        dAdzdBdz(7)=dAdzdBdz(7)+COEF_IJ*(zA*dGAz+ONE)*(xB*dGBz)*SASB
!PzPy
        dAdxdBdx(8)=dAdxdBdx(8)+COEF_IJ*zA*dGAx*(yB*dGBx)*SASB
        dAdydBdy(8)=dAdydBdy(8)+COEF_IJ*zA*dGAy*(yB*dGBy+ONE)*SASB
        dAdzdBdz(8)=dAdzdBdz(8)+COEF_IJ*(zA*dGAz+ONE)*(yB*dGBz)*SASB
!PzPz
        dAdxdBdx(9)=dAdxdBdx(9)+COEF_IJ*zA*dGAx*(zB*dGBx)*SASB
        dAdydBdy(9)=dAdydBdy(9)+COEF_IJ*zA*dGAy*(zB*dGBy)*SASB
        dAdzdBdz(9)=dAdzdBdz(9)+COEF_IJ*(zA*dGAz+ONE)*(zB*dGBz+ONE)*SASB
!Px|Dxx
      case (7)
        COEF_PXY = SQRT3*COEF_IJ
        dAdxdBdx(1)=dAdxdBdx(1)+COEF_IJ*(xA*dGAx+ONE)*(xB*xB*dGBx+TWO*xB)*SASB
        dAdydBdy(1)=dAdydBdy(1)+COEF_IJ*xA*dGAy*(xB*xB*dGBy)*SASB
        dAdzdBdz(1)=dAdzdBdz(1)+COEF_IJ*xA*dGAz*(xB*xB*dGBz)*SASB
!Px|Dyy
        dAdxdBdx(2)=dAdxdBdx(2)+COEF_IJ*(xA*dGAx+ONE)*(yB*yB*dGBx)*SASB
        dAdydBdy(2)=dAdydBdy(2)+COEF_IJ*xA*dGAy*(yB*yB*dGBy+TWO*yB)*SASB
        dAdzdBdz(2)=dAdzdBdz(2)+COEF_IJ*xA*dGAz*(yB*yB*dGBz)*SASB
!Px|Dzz
        dAdxdBdx(3)=dAdxdBdx(3)+COEF_IJ*(xA*dGAx+ONE)*(zB*zB*dGBx)*SASB
        dAdydBdy(3)=dAdydBdy(3)+COEF_IJ*xA*dGAy*(zB*zB*dGBy)*SASB
        dAdzdBdz(3)=dAdzdBdz(3)+COEF_IJ*xA*dGAz*(zB*zB*dGBz+TWO*zB)*SASB
!Px|Dxy
        dAdxdBdx(4)=dAdxdBdx(4)+COEF_PXY*(xA*dGAx+ONE)*(xB*yB*dGBx+yB)*SASB
        dAdydBdy(4)=dAdydBdy(4)+COEF_PXY*xA*dGAy*(xB*yB*dGBy+xB)*SASB
        dAdzdBdz(4)=dAdzdBdz(4)+COEF_PXY*xA*dGAz*(xB*yB*dGBz)*SASB
!Px|Dxz
        dAdxdBdx(5)=dAdxdBdx(5)+COEF_PXY*(xA*dGAx+ONE)*(xB*zB*dGBx+zB)*SASB
        dAdydBdy(5)=dAdydBdy(5)+COEF_PXY*xA*dGAy*(xB*zB*dGBy)*SASB
        dAdzdBdz(5)=dAdzdBdz(5)+COEF_PXY*xA*dGAz*(xB*zB*dGBz+xB)*SASB
!Px|Dyz
        dAdxdBdx(6)=dAdxdBdx(6)+COEF_PXY*(xA*dGAx+ONE)*(yB*zB*dGBx)*SASB
        dAdydBdy(6)=dAdydBdy(6)+COEF_PXY*xA*dGAy*(yB*zB*dGBy+zB)*SASB
        dAdzdBdz(6)=dAdzdBdz(6)+COEF_PXY*xA*dGAx*(yB*zB*dGBz+yB)*SASB
!Py|Dxx
        dAdxdBdx(7)=dAdxdBdx(7)+COEF_IJ*yA*dGAx*(xB*xB*dGBx+TWO*xB)*SASB
        dAdydBdy(7)=dAdydBdy(7)+COEF_IJ*(yA*dGAy+ONE)*(xB*xB*dGBy)*SASB
        dAdzdBdz(7)=dAdzdBdz(7)+COEF_IJ*yA*dGAz*(xB*xB*dGBz)*SASB
!Py|Dyy
        dAdxdBdx(8)=dAdxdBdx(8)+COEF_IJ*yA*dGAx*(yB*yB*dGBx)*SASB
        dAdydBdy(8)=dAdydBdy(8)+COEF_IJ*(yA*dGAy+ONE)*(yB*yB*dGBy+TWO*yB)*SASB
        dAdzdBdz(8)=dAdzdBdz(8)+COEF_IJ*yA*dGAz*(yB*yB*dGBz)*SASB
!Py|Dzz
        dAdxdBdx(9)=dAdxdBdx(9)+COEF_IJ*yA*dGAx*(zB*zB*dGBx)*SASB
        dAdydBdy(9)=dAdydBdy(9)+COEF_IJ*(yA*dGAy+ONE)*(zB*zB*dGBy)*SASB
        dAdzdBdz(9)=dAdzdBdz(9)+COEF_IJ*yA*dGAz*(zB*zB*dGBz+TWO*zB)*SASB
!Py|Dxy
        dAdxdBdx(10)=dAdxdBdx(10)+COEF_PXY*yA*dGAx*(xB*yB*dGBx+yB)*SASB
        dAdydBdy(10)=dAdydBdy(10)+COEF_PXY*(yA*dGAy+ONE)*(xB*yB*dGBy+xB)*SASB
        dAdzdBdz(10)=dAdzdBdz(10)+COEF_PXY*yA*dGAz*(xB*yB*dGBz)*SASB
!Py|Dxz
        dAdxdBdx(11)=dAdxdBdx(11)+COEF_PXY*yA*dGAx*(xB*zB*dGBx+zB)*SASB
        dAdydBdy(11)=dAdydBdy(11)+COEF_PXY*(yA*dGAy+ONE)*(xB*zB*dGBy)*SASB
        dAdzdBdz(11)=dAdzdBdz(11)+COEF_PXY*yA*dGAz*(xB*zB*dGBz+xB)*SASB
!Py|Dyz
        dAdxdBdx(12)=dAdxdBdx(12)+COEF_PXY*yA*dGAx*(yB*zB*dGBx)*SASB
        dAdydBdy(12)=dAdydBdy(12)+COEF_PXY*(yA*dGAy+ONE)*(yB*zB*dGBy+zB)*SASB
        dAdzdBdz(12)=dAdzdBdz(12)+COEF_PXY*yA*dGAz*(yB*zB*dGBz+yB)*SASB
!Pz|Dxx
        dAdxdBdx(13)=dAdxdBdx(13)+COEF_IJ*zA*dGAx*(xB*xB*dGBx+TWO*xB)*SASB
        dAdydBdy(13)=dAdydBdy(13)+COEF_IJ*zA*dGAy*(xB*xB*dGBy)*SASB
        dAdzdBdz(13)=dAdzdBdz(13)+COEF_IJ*(zA*dGAz+ONE)*(xB*xB*dGBz)*SASB
!Pz|Dyy
        dAdxdBdx(14)=dAdxdBdx(14)+COEF_IJ*zA*dGAx*(yB*yB*dGBx)*SASB
        dAdydBdy(14)=dAdydBdy(14)+COEF_IJ*zA*dGAy*(yB*yB*dGBy+TWO*yB)*SASB
        dAdzdBdz(14)=dAdzdBdz(14)+COEF_IJ*(zA*dGAz+ONE)*(yB*yB*dGBz)*SASB
!Pz|Dzz
        dAdxdBdx(15)=dAdxdBdx(15)+COEF_IJ*zA*dGAx*(zB*zB*dGBx)*SASB
        dAdydBdy(15)=dAdydBdy(15)+COEF_IJ*zA*dGAy*(zB*zB*dGBy)*SASB
        dAdzdBdz(15)=dAdzdBdz(15)+COEF_IJ*(zA*dGAz+ONE)*(zB*zB*dGBz+TWO*zB)*SASB
!Pz|Dxy
        dAdxdBdx(16)=dAdxdBdx(16)+COEF_PXY*zA*dGAx*(xB*yB*dGBx+yB)*SASB
        dAdydBdy(16)=dAdydBdy(16)+COEF_PXY*zA*dGAy*(xB*yB*dGBy+xB)*SASB
        dAdzdBdz(16)=dAdzdBdz(16)+COEF_PXY*(zA*dGAz+ONE)*(xB*yB*dGBz)*SASB
!Pz|Dxz
        dAdxdBdx(17)=dAdxdBdx(17)+COEF_PXY*zA*dGAx*(xB*zB*dGBx+zB)*SASB
        dAdydBdy(17)=dAdydBdy(17)+COEF_PXY*zA*dGAy*(xB*zB*dGBy)*SASB
        dAdzdBdz(17)=dAdzdBdz(17)+COEF_PXY*(zA*dGAz+ONE)*(xB*zB*dGBz+xB)*SASB
!Pz|Dyz
        dAdxdBdx(18)=dAdxdBdx(18)+COEF_PXY*zA*dGAx*(yB*zB*dGBx)*SASB
        dAdydBdy(18)=dAdydBdy(18)+COEF_PXY*zA*dGAy*(yB*zB*dGBy+zB)*SASB
        dAdzdBdz(18)=dAdzdBdz(18)+COEF_PXY*(zA*dGAz+ONE)*(yB*zB*dGBz+yB)*SASB
      case (11)
        COEF_XXXY = SQRT3*COEF_IJ
        COEF_XYXY = THREE*COEF_IJ
!Dxx|Dxx
        dAdxdBdx(1)=dAdxdBdx(1)+COEF_IJ*(xA*xA*dGAx+TWO*xA)*(xB*xB*dGBx+TWO*xB)*SASB
        dAdydBdy(1)=dAdydBdy(1)+COEF_IJ*(xA*xA*dGAy)*(xB*xB*dGBy)*SASB
        dAdzdBdz(1)=dAdzdBdz(1)+COEF_IJ*(xA*xA*dGAz)*(xB*xB*dGBz)*SASB
!Dxx|Dyy
        dAdxdBdx(2)=dAdxdBdx(2)+COEF_IJ*(xA*xA*dGAx+TWO*xA)*(yB*yB*dGBx)*SASB
        dAdydBdy(2)=dAdydBdy(2)+COEF_IJ*(xA*xA*dGAy)*(yB*yB*dGBy+TWO*yB)*SASB
        dAdzdBdz(2)=dAdzdBdz(2)+COEF_IJ*(xA*xA*dGAz)*(yB*yB*dGBz)*SASB
!Dxx|Dzz
        dAdxdBdx(3)=dAdxdBdx(3)+COEF_IJ*(xA*xA*dGAx+TWO*xA)*(zB*zB*dGBx)*SASB
        dAdydBdy(3)=dAdydBdy(3)+COEF_IJ*(xA*xA*dGAy)*(zB*zB*dGBy)*SASB
        dAdzdBdz(3)=dAdzdBdz(3)+COEF_IJ*(xA*xA*dGAz)*(zB*zB*dGBz+TWO*zB)*SASB
!Dxx|Dxy
        dAdxdBdx(4)=dAdxdBdx(4)+COEF_XXXY*(xA*xA*dGAx+TWO*xA)*(xB*yB*dGBx+yB)*SASB
        dAdydBdy(4)=dAdydBdy(4)+COEF_XXXY*(xA*xA*dGAy)*(xB*yB*dGBy+xB)*SASB
        dAdzdBdz(4)=dAdzdBdz(4)+COEF_XXXY*(xA*xA*dGAz)*(xB*yB*dGBz)*SASB
!Dxx|Dxz
        dAdxdBdx(5)=dAdxdBdx(5)+COEF_XXXY*(xA*xA*dGAx+TWO*xA)*(xB*zB*dGBx+zB)*SASB
        dAdydBdy(5)=dAdydBdy(5)+COEF_XXXY*(xA*xA*dGAy)*(xB*zB*dGBy)*SASB
        dAdzdBdz(5)=dAdzdBdz(5)+COEF_XXXY*(xA*xA*dGAz)*(xB*zB*dGBz+xB)*SASB
!Dxx|Dyz
        dAdxdBdx(6)=dAdxdBdx(6)+COEF_XXXY*(xA*xA*dGAx+TWO*xA)*(yB*zB*dGBx)*SASB
        dAdydBdy(6)=dAdydBdy(6)+COEF_XXXY*(xA*xA*dGAy)*(yB*zB*dGBy+zB)*SASB
        dAdzdBdz(6)=dAdzdBdz(6)+COEF_XXXY*(xA*xA*dGAz)*(yB*zB*dGBz+yB)*SASB
!Dyy|Dxx
        dAdxdBdx(7)=dAdxdBdx(7)+COEF_IJ*(yA*yA*dGAx)*(xB*xB*dGBx+TWO*xB)*SASB
        dAdydBdy(7)=dAdydBdy(7)+COEF_IJ*(yA*yA*dGAy+TWO*yA)*(xB*xB*dGBy)*SASB
        dAdzdBdz(7)=dAdzdBdz(7)+COEF_IJ*(yA*yA*dGAz)*(xB*xB*dGBz)*SASB
!Dyy|Dyy
        dAdxdBdx(8)=dAdxdBdx(8)+COEF_IJ*(yA*yA*dGAx)*(yB*yB*dGBx)*SASB
        dAdydBdy(8)=dAdydBdy(8)+COEF_IJ*(yA*yA*dGAy+TWO*yA)*(yB*yB*dGBy+TWO*yB)*SASB
        dAdzdBdz(8)=dAdzdBdz(8)+COEF_IJ*(yA*yA*dGAz)*(yB*yB*dGBz)*SASB
!Dyy|Dzz
        dAdxdBdx(9)=dAdxdBdx(9)+COEF_IJ*(yA*yA*dGAx)*(zB*zB*dGBx)*SASB
        dAdydBdy(9)=dAdydBdy(9)+COEF_IJ*(yA*yA*dGAy+TWO*yA)*(zB*zB*dGBy)*SASB
        dAdzdBdz(9)=dAdzdBdz(9)+COEF_IJ*(yA*yA*dGAz)*(zB*zB*dGBz+TWO*zB)*SASB
!Dyy|Dxy
        dAdxdBdx(10)=dAdxdBdx(10)+COEF_XXXY*(yA*yA*dGAx)*(xB*yB*dGBx+yB)*SASB
        dAdydBdy(10)=dAdydBdy(10)+COEF_XXXY*(yA*yA*dGAy+TWO*yA)*(xB*yB*dGBy+xB)*SASB
        dAdzdBdz(10)=dAdzdBdz(10)+COEF_XXXY*(yA*yA*dGAz)*(xB*yB*dGBz)*SASB
!Dyy|Dxz
        dAdxdBdx(11)=dAdxdBdx(11)+COEF_XXXY*(yA*yA*dGAx)*(xB*zB*dGBx+zB)*SASB
        dAdydBdy(11)=dAdydBdy(11)+COEF_XXXY*(yA*yA*dGAy+TWO*yA)*(xB*zB*dGBy)*SASB
        dAdzdBdz(11)=dAdzdBdz(11)+COEF_XXXY*(yA*yA*dGAz)*(xB*zB*dGBz+xB)*SASB
!Dyy|Dyz
        dAdxdBdx(12)=dAdxdBdx(12)+COEF_XXXY*(yA*yA*dGAx)*(yB*zB*dGBx)*SASB
        dAdydBdy(12)=dAdydBdy(12)+COEF_XXXY*(yA*yA*dGAy+TWO*yA)*(yB*zB*dGBy+zB)*SASB
        dAdzdBdz(12)=dAdzdBdz(12)+COEF_XXXY*(yA*yA*dGAz)*(yB*zB*dGBz+yB)*SASB
!Dzz|Dxx
        dAdxdBdx(13)=dAdxdBdx(13)+COEF_IJ*(zA*zA*dGAx)*(xB*xB*dGBx+TWO*xB)*SASB
        dAdydBdy(13)=dAdydBdy(13)+COEF_IJ*(zA*zA*dGAy)*(xB*xB*dGBy)*SASB
        dAdzdBdz(13)=dAdzdBdz(13)+COEF_IJ*(zA*zA*dGAz+TWO*zA)*(xB*xB*dGBz)*SASB
!Dzz|Dyy
        dAdxdBdx(14)=dAdxdBdx(14)+COEF_IJ*(zA*zA*dGAx)*(yB*yB*dGBx)*SASB
        dAdydBdy(14)=dAdydBdy(14)+COEF_IJ*(zA*zA*dGAy)*(yB*yB*dGBy+TWO*yB)*SASB
        dAdzdBdz(14)=dAdzdBdz(14)+COEF_IJ*(zA*zA*dGAz+TWO*zA)*(yB*yB*dGBz)*SASB
!Dzz|Dzz
        dAdxdBdx(15)=dAdxdBdx(15)+COEF_IJ*(zA*zA*dGAx)*(zB*zB*dGBx)*SASB
        dAdydBdy(15)=dAdydBdy(15)+COEF_IJ*(zA*zA*dGAy)*(zB*zB*dGBy)*SASB
        dAdzdBdz(15)=dAdzdBdz(15)+COEF_IJ*(zA*zA*dGAz+TWO*zA)*(zB*zB*dGBz+TWO*zB)*SASB
!Dzz|Dxy
        dAdxdBdx(16)=dAdxdBdx(16)+COEF_XXXY*(zA*zA*dGAx)*(xB*yB*dGBx+yB)*SASB
        dAdydBdy(16)=dAdydBdy(16)+COEF_XXXY*(zA*zA*dGAy)*(xB*yB*dGBy+xB)*SASB
        dAdzdBdz(16)=dAdzdBdz(16)+COEF_XXXY*(zA*zA*dGAz+TWO*zA)*(xB*yB*dGBz)*SASB
!Dzz|Dxz
        dAdxdBdx(17)=dAdxdBdx(17)+COEF_XXXY*(zA*zA*dGAx)*(xB*zB*dGBx+zB)*SASB
        dAdydBdy(17)=dAdydBdy(17)+COEF_XXXY*(zA*zA*dGAy)*(xB*zB*dGBy)*SASB
        dAdzdBdz(17)=dAdzdBdz(17)+COEF_XXXY*(zA*zA*dGAz+TWO*zA)*(xB*zB*dGBz+xB)*SASB
!Dzz|Dyz
        dAdxdBdx(18)=dAdxdBdx(18)+COEF_XXXY*(zA*zA*dGAx)*(yB*zB*dGBx)*SASB
        dAdydBdy(18)=dAdydBdy(18)+COEF_XXXY*(zA*zA*dGAy)*(yB*zB*dGBy+zB)*SASB
        dAdzdBdz(18)=dAdzdBdz(18)+COEF_XXXY*(zA*zA*dGAz+TWO*zA)*(yB*zB*dGBz+yB)*SASB
!Dxy|Dxx
        dAdxdBdx(19)=dAdxdBdx(19)+COEF_XXXY*(xA*yA*dGAx+yA)*(xB*xB*dGBx+TWO*xB)*SASB
        dAdydBdy(19)=dAdydBdy(19)+COEF_XXXY*(xA*yA*dGAy+xA)*(xB*xB*dGBy)*SASB
        dAdzdBdz(19)=dAdzdBdz(19)+COEF_XXXY*(xA*yA*dGAz)*(xB*xB*dGBz)*SASB
!Dxy|Dyy
        dAdxdBdx(20)=dAdxdBdx(20)+COEF_XXXY*(xA*yA*dGAx+yA)*(yB*yB*dGBx)*SASB
        dAdydBdy(20)=dAdydBdy(20)+COEF_XXXY*(xA*yA*dGAy+xA)*(yB*yB*dGBy+TWO*yB)*SASB
        dAdzdBdz(20)=dAdzdBdz(20)+COEF_XXXY*(xA*yA*dGAz)*(yB*yB*dGBz)*SASB
!Dxy|Dzz
        dAdxdBdx(21)=dAdxdBdx(21)+COEF_XXXY*(xA*yA*dGAx+yA)*(zB*zB*dGBx)*SASB
        dAdydBdy(21)=dAdydBdy(21)+COEF_XXXY*(xA*yA*dGAy+xA)*(zB*zB*dGBy)*SASB
        dAdzdBdz(21)=dAdzdBdz(21)+COEF_XXXY*(xA*yA*dGAz)*(zB*zB*dGBz+TWO*zB)*SASB
!Dxy|Dxy
        dAdxdBdx(22)=dAdxdBdx(22)+COEF_XYXY*(xA*yA*dGAx+yA)*(xB*yB*dGBx+yB)*SASB
        dAdydBdy(22)=dAdydBdy(22)+COEF_XYXY*(xA*yA*dGAy+xA)*(xB*yB*dGBy+xB)*SASB
        dAdzdBdz(22)=dAdzdBdz(22)+COEF_XYXY*(xA*yA*dGAz)*(xB*yB*dGBz)*SASB
!Dxy|Dxz
        dAdxdBdx(23)=dAdxdBdx(23)+COEF_XYXY*(xA*yA*dGAx+yA)*(xB*zB*dGBx+zB)*SASB
        dAdydBdy(23)=dAdydBdy(23)+COEF_XYXY*(xA*yA*dGAy+xA)*(xB*zB*dGBy)*SASB
        dAdzdBdz(23)=dAdzdBdz(23)+COEF_XYXY*(xA*yA*dGAz)*(xB*zB*dGBz+xB)*SASB
!Dxy|Dyz
        dAdxdBdx(24)=dAdxdBdx(24)+COEF_XYXY*(xA*yA*dGAx+yA)*(yB*zB*dGBx)*SASB
        dAdydBdy(24)=dAdydBdy(24)+COEF_XYXY*(xA*yA*dGAy+xA)*(yB*zB*dGBy+zB)*SASB
        dAdzdBdz(24)=dAdzdBdz(24)+COEF_XYXY*(xA*yA*dGAz)*(yB*zB*dGBz+yB)*SASB
!Dxz|Dxx
        dAdxdBdx(25)=dAdxdBdx(25)+COEF_XXXY*(xA*zA*dGAx+zA)*(xB*xB*dGBx+TWO*xB)*SASB
        dAdydBdy(25)=dAdydBdy(25)+COEF_XXXY*(xA*zA*dGAy)*(xB*xB*dGBy)*SASB
        dAdzdBdz(25)=dAdzdBdz(25)+COEF_XXXY*(xA*zA*dGAz+xA)*(xB*xB*dGBz)*SASB
!Dxz|Dyy
        dAdxdBdx(26)=dAdxdBdx(26)+COEF_XXXY*(xA*zA*dGAx+zA)*(yB*yB*dGBx)*SASB
        dAdydBdy(26)=dAdydBdy(26)+COEF_XXXY*(xA*zA*dGAy)*(yB*yB*dGBy+TWO*yB)*SASB
        dAdzdBdz(26)=dAdzdBdz(26)+COEF_XXXY*(xA*zA*dGAz+xA)*(yB*yB*dGBz)*SASB
!Dxz|Dzz
        dAdxdBdx(27)=dAdxdBdx(27)+COEF_XXXY*(xA*zA*dGAx+zA)*(zB*zB*dGBx)*SASB
        dAdydBdy(27)=dAdydBdy(27)+COEF_XXXY*(xA*zA*dGAy)*(zB*zB*dGBy)*SASB
        dAdzdBdz(27)=dAdzdBdz(27)+COEF_XXXY*(xA*zA*dGAz+xA)*(zB*zB*dGBz+TWO*zB)*SASB
!Dxz|Dxy
        dAdxdBdx(28)=dAdxdBdx(28)+COEF_XYXY*(xA*zA*dGAx+zA)*(xB*yB*dGBx+yB)*SASB
        dAdydBdy(28)=dAdydBdy(28)+COEF_XYXY*(xA*zA*dGAy)*(xB*yB*dGBy+xB)*SASB
        dAdzdBdz(28)=dAdzdBdz(28)+COEF_XYXY*(xA*zA*dGAz+xA)*(xB*yB*dGBz)*SASB
!Dxz|Dxz
        dAdxdBdx(29)=dAdxdBdx(29)+COEF_XYXY*(xA*zA*dGAx+zA)*(xB*zB*dGBx+zB)*SASB
        dAdydBdy(29)=dAdydBdy(29)+COEF_XYXY*(xA*zA*dGAy)*(xB*zB*dGBy)*SASB
        dAdzdBdz(29)=dAdzdBdz(29)+COEF_XYXY*(xA*zA*dGAz+xA)*(xB*zB*dGBz+xB)*SASB
!Dxz|Dyz
        dAdxdBdx(30)=dAdxdBdx(30)+COEF_XYXY*(xA*zA*dGAx+zA)*(yB*zB*dGBx)*SASB
        dAdydBdy(30)=dAdydBdy(30)+COEF_XYXY*(xA*zA*dGAy)*(yB*zB*dGBy+zB)*SASB
        dAdzdBdz(30)=dAdzdBdz(30)+COEF_XYXY*(xA*zA*dGAz+xA)*(yB*zB*dGBz+yB)*SASB
!Dyz|Dxx
        dAdxdBdx(31)=dAdxdBdx(31)+COEF_XXXY*(yA*zA*dGAx)*(xB*xB*dGBx+TWO*xB)*SASB
        dAdydBdy(31)=dAdydBdy(31)+COEF_XXXY*(yA*zA*dGAy+zA)*(xB*xB*dGBy)*SASB
        dAdzdBdz(31)=dAdzdBdz(31)+COEF_XXXY*(yA*zA*dGAz+yA)*(xB*xB*dGBz)*SASB
!Dyz|Dyy
        dAdxdBdx(32)=dAdxdBdx(32)+COEF_XXXY*(yA*zA*dGAx)*(yB*yB*dGBx)*SASB
        dAdydBdy(32)=dAdydBdy(32)+COEF_XXXY*(yA*zA*dGAy+zA)*(yB*yB*dGBy+TWO*yB)*SASB
        dAdzdBdz(32)=dAdzdBdz(32)+COEF_XXXY*(yA*zA*dGAz+yA)*(yB*yB*dGBz)*SASB
!Dyz|Dzz
        dAdxdBdx(33)=dAdxdBdx(33)+COEF_XXXY*(yA*zA*dGAx)*(zB*zB*dGBx)*SASB
        dAdydBdy(33)=dAdydBdy(33)+COEF_XXXY*(yA*zA*dGAy+zA)*(zB*zB*dGBy)*SASB
        dAdzdBdz(33)=dAdzdBdz(33)+COEF_XXXY*(yA*zA*dGAz+yA)*(zB*zB*dGBz+TWO*zB)*SASB
!Dyz|Dxy
        dAdxdBdx(34)=dAdxdBdx(34)+COEF_XYXY*(yA*zA*dGAx)*(xB*yB*dGBx+yB)*SASB
        dAdydBdy(34)=dAdydBdy(34)+COEF_XYXY*(yA*zA*dGAy+zA)*(xB*yB*dGBy+xB)*SASB
        dAdzdBdz(34)=dAdzdBdz(34)+COEF_XYXY*(yA*zA*dGAz+yA)*(xB*yB*dGBz)*SASB
!Dyz|Dxz
        dAdxdBdx(35)=dAdxdBdx(35)+COEF_XYXY*(yA*zA*dGAx)*(xB*zB*dGBx+zB)*SASB
        dAdydBdy(35)=dAdydBdy(35)+COEF_XYXY*(yA*zA*dGAy+zA)*(xB*zB*dGBy)*SASB
        dAdzdBdz(35)=dAdzdBdz(35)+COEF_XYXY*(yA*zA*dGAz+yA)*(xB*zB*dGBz+xB)*SASB
!Dyz|Dyz
        dAdxdBdx(36)=dAdxdBdx(36)+COEF_XYXY*(yA*zA*dGAx)*(yB*zB*dGBx)*SASB
        dAdydBdy(36)=dAdydBdy(36)+COEF_XYXY*(yA*zA*dGAy+zA)*(yB*zB*dGBy+zB)*SASB
        dAdzdBdz(36)=dAdzdBdz(36)+COEF_XYXY*(yA*zA*dGAz+yA)*(yB*zB*dGBz+yB)*SASB

      case default
        write(UNIout,*)'ERROR> dGAdGB_PRODUCTS: DEFCASE type should not exist'
        stop ' ERROR> dGAdGB_PRODUCTS: DEFCASE type should not exist'
      end select
!
      end do ! Jgauss
      end do ! Igauss 
      
        IF(LIatmshl)then
          INDEX=0
          do I=1,Irange
          do J=1,Jrange
            INDEX=INDEX+1
            IF(I.GE.J)then  ! Corrected August 1,2003 had (J.ge.I)
              Iao=AOI+I
              Jao=AOJ+J
              IJao=Iao*(Iao-1)/2+Jao
              dxdxGprod(IJao)=dAdxdBdx(INDEX)
              dydyGprod(IJao)=dAdydBdy(INDEX)
              dzdzGprod(IJao)=dAdzdBdz(INDEX)
            end if
          end do ! J   
          end do ! I
        else
              INDEX=0
          do I=1,Irange
          do J=1,Jrange
            INDEX=INDEX+1
            Iao=AOI+I
            Jao=AOJ+J
            IJao=Iao*(Iao-1)/2+Jao
            dxdxGprod(IJao)=dAdxdBdx(INDEX)
            dydyGprod(IJao)=dAdydBdy(INDEX)
            dzdzGprod(IJao)=dAdzdBdz(INDEX)
          end do ! J
          end do ! I
        end if ! LIatmshl

      end do ! Jatmshl
      end do ! Iatmshl

      return
      end subroutine AB_dGproducts
! END CONTAINS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine dGAdGB_PRODUCTS
