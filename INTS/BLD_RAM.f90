      subroutine BLD_RAMint
!***********************************************************************
!     Date last modified: May 29, 2024                    Version 1.0  *
!     Author: J.W. Hollett                                             *
!     Description: Build RAM array over AOs by calling RR routine to   *
!                  calculate classes of ints (e.g. [ssss], [dsdp]).    *
!***********************************************************************
! Modules:
      USE type_basis_set
      USE type_molecule
      USE QM_objects
      USE INT_objects

      implicit none
!
! Local scalars:
      integer :: I_s, J_s, K_s, L_s, firstg_I, lastg_I, firstg_J, lastg_J
      integer :: firstg_K, lastg_K, firstg_L, lastg_L, LI, LJ, LK, LL, K_shell, L_shell
      integer :: a_gauss, b_gauss, c_gauss, d_gauss, I_shell, J_shell, P_int, D_int
      integer :: I_p, J_p, K_p, L_p, ijkl_index
      integer :: I_d, J_d, K_d, L_d
      integer :: iAO, jAO, kAO, lAO
      integer :: I_atmshl, J_atmshl, K_atmshl, L_atmshl
!
! Local arrays:
      double precision :: R_A(3), R_B(3), R_C(3), R_D(3), ram(1296)
      double precision :: ramAO_temp(1296)
!
! Begin:
      call PRG_manager ('enter', 'BLD_RAMint', 'RAMINT%AO')
!
      allocate(ramAO(Nbasis**4))
      ramAO = 0.0D0
!
! Get lists of basis functions
      call BLD_basis_list
!
! Break up loops into function types for easier sorting of integrals
!
! First [ssss] type integrals
!
      do I_s = 1, N_s ! loop over s shells
        LI = 0
        I_shell = s_list(I_s)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_s = 1, N_s ! loop over s shells
          LJ = 0
          J_shell = s_list(J_s)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_s = 1, N_s ! loop over s shells
            LK = 0
            K_shell = s_list(K_s)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_s = 1, N_s ! loop over s shells
              LL = 0
              L_shell = s_list(L_s)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              iAO = Basis%atmshl(I_atmshl)%frstAO ! AOs
              jAO = Basis%atmshl(J_atmshl)%frstAO
              kAO = Basis%atmshl(K_atmshl)%frstAO ! AOs
              lAO = Basis%atmshl(L_atmshl)%frstAO
              ijkl_index = Nbasis**3*(iAO-1)&
                         + Nbasis**2*(jAO-1)&
                         + Nbasis*(kAO-1)&
                         + lAO
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                      ramAO(ijkl_index) = ramAO(ijkl_index)&
                                         + Basis%gaussian(a_gauss)%CONTRC&
                                         *Basis%gaussian(b_gauss)%CONTRC&
                                         *Basis%gaussian(c_gauss)%CONTRC&
                                         *Basis%gaussian(d_gauss)%CONTRC*ram(1)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
      if(N_p.gt.0)then
!
! [psss] type integrals
!
      do I_p = 1, N_p ! loop over p shells
        LI = 1
        I_shell = p_list(I_p)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_s = 1, N_s ! loop over s shells
          LJ = 0
          J_shell = s_list(J_s)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_s = 1, N_s ! loop over s shells
            LK = 0
            K_shell = s_list(K_s)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_s = 1, N_s ! loop over s shells
              LL = 0
              L_shell = s_list(L_s)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                      ramAO_temp(1:3) = ramAO_temp(1:3)&
                                      + Basis%gaussian(a_gauss)%CONTRC&
                                       *Basis%gaussian(b_gauss)%CONTRC&
                                       *Basis%gaussian(c_gauss)%CONTRC&
                                       *Basis%gaussian(d_gauss)%CONTRC*ram(1:3)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
! These are RAM integrals, 2-fold symmetry
! [ij|kl] = [kl|ij]
!
              jAO = Basis%atmshl(J_atmshl)%frstAO ! s
              kAO = Basis%atmshl(K_atmshl)%frstAO ! s
              lAO = Basis%atmshl(L_atmshl)%frstAO ! s
              P_int = 0
              do iAO = Basis%atmshl(I_atmshl)%frstAO,Basis%atmshl(I_atmshl)%lastAO ! p
!
                P_int = P_int + 1
                ! [psss] 
                ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                ramAO(ijkl_index) = ramAO_temp(P_int)
                ! [ssps] 
                ijkl_index = Nbasis**3*(kAO-1) + Nbasis**2*(lAO-1) + Nbasis*(iAO-1) + jAO
                ramAO(ijkl_index) = ramAO_temp(P_int)
!
              end do ! iAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
! [spss] type integrals
!
      do I_s = 1, N_s ! loop over s shells
        LI = 0
        I_shell = s_list(I_s)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_p = 1, N_p ! loop over p shells
          LJ = 1
          J_shell = p_list(J_p)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_s = 1, N_s ! loop over s shells
            LK = 0
            K_shell = s_list(K_s)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_s = 1, N_s ! loop over s shells
              LL = 0
              L_shell = s_list(L_s)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                      ramAO_temp(1:3) = ramAO_temp(1:3)&
                                      + Basis%gaussian(a_gauss)%CONTRC&
                                       *Basis%gaussian(b_gauss)%CONTRC&
                                       *Basis%gaussian(c_gauss)%CONTRC&
                                       *Basis%gaussian(d_gauss)%CONTRC*ram(1:3)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
! These are RAM integrals, 2-fold symmetry
! [ij|kl] = [kl|ij]
!
              iAO = Basis%atmshl(I_atmshl)%frstAO ! s
              kAO = Basis%atmshl(K_atmshl)%frstAO ! s
              lAO = Basis%atmshl(L_atmshl)%frstAO ! s
              P_int = 0
              do jAO = Basis%atmshl(J_atmshl)%frstAO,Basis%atmshl(J_atmshl)%lastAO ! p
!
                P_int = P_int + 1
                ! [spss] 
                ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                ramAO(ijkl_index) = ramAO_temp(P_int)
                ! [sssp] 
                ijkl_index = Nbasis**3*(kAO-1) + Nbasis**2*(lAO-1) + Nbasis*(iAO-1) + jAO
                ramAO(ijkl_index) = ramAO_temp(P_int)
!
              end do ! jAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
! [ppss] type integrals
!
      do I_p = 1, N_p ! loop over p shells
        LI = 1 
        I_shell = p_list(I_p)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_p = 1, N_p ! loop over p shells
          LJ = 1
          J_shell = p_list(J_p)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_s = 1, N_s ! loop over s shells
            LK = 0
            K_shell = s_list(K_s)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_s = 1, N_s ! loop over s shells
              LL = 0
              L_shell = s_list(L_s)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                      ramAO_temp(1:9) = ramAO_temp(1:9)&
                                      + Basis%gaussian(a_gauss)%CONTRC&
                                       *Basis%gaussian(b_gauss)%CONTRC&
                                       *Basis%gaussian(c_gauss)%CONTRC&
                                       *Basis%gaussian(d_gauss)%CONTRC*ram(1:9)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
! These are RAM integrals, 2-fold symmetry
! [ij|kl] = [kl|ij]
!
              kAO = Basis%atmshl(K_atmshl)%frstAO ! s
              lAO = Basis%atmshl(L_atmshl)%frstAO ! s
              P_int = 0
              do iAO = Basis%atmshl(I_atmshl)%frstAO,Basis%atmshl(I_atmshl)%lastAO ! p 
                do jAO = Basis%atmshl(J_atmshl)%frstAO,Basis%atmshl(J_atmshl)%lastAO ! p
!
                  P_int = P_int + 1
                  ! [ppss]
                  ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                  ramAO(ijkl_index) = ramAO_temp(P_int)
                  ! [sspp]
                  ijkl_index = Nbasis**3*(kAO-1) + Nbasis**2*(lAO-1) + Nbasis*(iAO-1) + jAO
                  ramAO(ijkl_index) = ramAO_temp(P_int)
!
                end do ! jAO
              end do ! iAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
! [psps] type integrals
!
      do I_p = 1, N_p ! loop over p shells
        LI = 1 
        I_shell = p_list(I_p)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_s = 1, N_s ! loop over s shells
          LJ = 0 
          J_shell = s_list(J_s)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_p = 1, N_p ! loop over p shells
            LK = 1
            K_shell = p_list(K_p)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_s = 1, N_s ! loop over s shells
              LL = 0
              L_shell = s_list(L_s)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                      ramAO_temp(1:9) = ramAO_temp(1:9)&
                                      + Basis%gaussian(a_gauss)%CONTRC&
                                       *Basis%gaussian(b_gauss)%CONTRC&
                                       *Basis%gaussian(c_gauss)%CONTRC&
                                       *Basis%gaussian(d_gauss)%CONTRC*ram(1:9)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
! These are RAM integrals, 2-fold symmetry
! [ij|kl] = [kl|ij]
!
              jAO = Basis%atmshl(J_atmshl)%frstAO ! s
              lAO = Basis%atmshl(L_atmshl)%frstAO ! s
              P_int = 0
              do iAO = Basis%atmshl(I_atmshl)%frstAO,Basis%atmshl(I_atmshl)%lastAO ! p 
                do kAO = Basis%atmshl(K_atmshl)%frstAO,Basis%atmshl(K_atmshl)%lastAO ! p
!
                  P_int = P_int + 1
                  ! [psps]
                  ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                  ramAO(ijkl_index) = ramAO_temp(P_int)
!
                end do ! kAO
              end do ! iAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
! [pssp] type integrals
!
      do I_p = 1, N_p ! loop over p shells
        LI = 1 
        I_shell = p_list(I_p)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_s = 1, N_s ! loop over s shells
          LJ = 0 
          J_shell = s_list(J_s)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_s = 1, N_s ! loop over s shells
            LK = 0
            K_shell = s_list(K_s)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_p = 1, N_p ! loop over p shells
              LL = 1
              L_shell = p_list(L_p)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                      ramAO_temp(1:9) = ramAO_temp(1:9)&
                                      + Basis%gaussian(a_gauss)%CONTRC&
                                       *Basis%gaussian(b_gauss)%CONTRC&
                                       *Basis%gaussian(c_gauss)%CONTRC&
                                       *Basis%gaussian(d_gauss)%CONTRC*ram(1:9)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
! These are RAM integrals, 2-fold symmetry
! [ij|kl] = [kl|ij]
!
              jAO = Basis%atmshl(J_atmshl)%frstAO ! s
              kAO = Basis%atmshl(K_atmshl)%frstAO ! s
              P_int = 0
              do iAO = Basis%atmshl(I_atmshl)%frstAO,Basis%atmshl(I_atmshl)%lastAO ! p 
                do lAO = Basis%atmshl(L_atmshl)%frstAO,Basis%atmshl(L_atmshl)%lastAO ! p
!
                  P_int = P_int + 1
                  ! [pssp]
                  ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                  ramAO(ijkl_index) = ramAO_temp(P_int)
                  ! [spps]
                  ijkl_index = Nbasis**3*(kAO-1) + Nbasis**2*(lAO-1) + Nbasis*(iAO-1) + jAO
                  ramAO(ijkl_index) = ramAO_temp(P_int)
!
                end do ! kAO
              end do ! iAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
! [spsp] type integrals
!
      do I_s = 1, N_s ! loop over s shells
        LI = 0 
        I_shell = s_list(I_s)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_p = 1, N_p ! loop over p shells
          LJ = 1 
          J_shell = p_list(J_p)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_s = 1, N_s ! loop over s shells
            LK = 0
            K_shell = s_list(K_s)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_p = 1, N_p ! loop over p shells
              LL = 1
              L_shell = p_list(L_p)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                      ramAO_temp(1:9) = ramAO_temp(1:9)&
                                      + Basis%gaussian(a_gauss)%CONTRC&
                                       *Basis%gaussian(b_gauss)%CONTRC&
                                       *Basis%gaussian(c_gauss)%CONTRC&
                                       *Basis%gaussian(d_gauss)%CONTRC*ram(1:9)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
! These are RAM integrals, 2-fold symmetry
! [ij|kl] = [kl|ij]
!
              iAO = Basis%atmshl(I_atmshl)%frstAO ! s
              kAO = Basis%atmshl(K_atmshl)%frstAO ! s
              P_int = 0
              do jAO = Basis%atmshl(J_atmshl)%frstAO,Basis%atmshl(J_atmshl)%lastAO ! p 
                do lAO = Basis%atmshl(L_atmshl)%frstAO,Basis%atmshl(L_atmshl)%lastAO ! p
!
                  P_int = P_int + 1
                  ! [spsp]
                  ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                  ramAO(ijkl_index) = ramAO_temp(P_int)
!
                end do ! kAO
              end do ! iAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
! [ppps] type integrals
!
      do I_p = 1, N_p ! loop over p shells
        LI = 1 
        I_shell = p_list(I_p)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_p = 1, N_p ! loop over p shells
          LJ = 1
          J_shell = p_list(J_p)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_p = 1, N_p ! loop over p shells
            LK = 1 
            K_shell = p_list(K_p)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_s = 1, N_s ! loop over s shells
              LL = 0 
              L_shell = s_list(L_s)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                        ramAO_temp(1:27) = ramAO_temp(1:27)&
                                          + Basis%gaussian(a_gauss)%CONTRC&
                                          *Basis%gaussian(b_gauss)%CONTRC&
                                          *Basis%gaussian(c_gauss)%CONTRC&
                                          *Basis%gaussian(d_gauss)%CONTRC*ram(1:27)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
! These are RAM integrals, 2-fold symmetry
! [ij|kl] = [kl|ij]
!
              lAO = Basis%atmshl(L_atmshl)%frstAO ! s
              P_int = 0
              do iAO = Basis%atmshl(I_atmshl)%frstAO,Basis%atmshl(I_atmshl)%lastAO ! p
                do jAO = Basis%atmshl(J_atmshl)%frstAO,Basis%atmshl(J_atmshl)%lastAO ! p
                  do kAO = Basis%atmshl(K_atmshl)%frstAO,Basis%atmshl(K_atmshl)%lastAO ! p
!
                    P_int = P_int + 1
                    ![ppps]
                    ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                    ramAO(ijkl_index) = ramAO_temp(P_int)
                    ![pspp]
                    ijkl_index = Nbasis**3*(kAO-1) + Nbasis**2*(lAO-1) + Nbasis*(iAO-1) + jAO
                    ramAO(ijkl_index) = ramAO_temp(P_int)
!
                  end do ! kAO
                end do ! jAO
              end do ! iAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
! [ppsp] type integrals
!
      do I_p = 1, N_p ! loop over p shells
        LI = 1 
        I_shell = p_list(I_p)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_p = 1, N_p ! loop over p shells
          LJ = 1
          J_shell = p_list(J_p)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_s = 1, N_s ! loop over s shells
            LK = 0 
            K_shell = s_list(K_s)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_p = 1, N_p ! loop over p shells
              LL = 1 
              L_shell = p_list(L_p)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                        ramAO_temp(1:27) = ramAO_temp(1:27)&
                                         + Basis%gaussian(a_gauss)%CONTRC&
                                          *Basis%gaussian(b_gauss)%CONTRC&
                                          *Basis%gaussian(c_gauss)%CONTRC&
                                          *Basis%gaussian(d_gauss)%CONTRC*ram(1:27)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
! These are RAM integrals, 2-fold symmetry
! [ij|kl] = [kl|ij]
!
              kAO = Basis%atmshl(K_atmshl)%frstAO ! s
              P_int = 0
              do iAO = Basis%atmshl(I_atmshl)%frstAO,Basis%atmshl(I_atmshl)%lastAO ! p
                do jAO = Basis%atmshl(J_atmshl)%frstAO,Basis%atmshl(J_atmshl)%lastAO ! p
                  do lAO = Basis%atmshl(L_atmshl)%frstAO,Basis%atmshl(L_atmshl)%lastAO ! p
!
                    P_int = P_int + 1
                    ![ppsp]
                    ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                    ramAO(ijkl_index) = ramAO_temp(P_int)
                    ![sppp]
                    ijkl_index = Nbasis**3*(kAO-1) + Nbasis**2*(lAO-1) + Nbasis*(iAO-1) + jAO
                    ramAO(ijkl_index) = ramAO_temp(P_int)
!
                  end do ! kAO
                end do ! jAO
              end do ! iAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
! [pppp] type integrals
!
      do I_p = 1, N_p ! loop over p shells
        LI = 1 
        I_shell = p_list(I_p)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_p = 1, N_p ! loop over p shells
          LJ = 1
          J_shell = p_list(J_p)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_p = 1, N_p ! loop over p shells
            LK = 1 
            K_shell = p_list(K_p)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_p = 1, N_p ! loop over p shells
              LL = 1 
              L_shell = p_list(L_p)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                      ramAO_temp(1:81) = ramAO_temp(1:81)&
                                       + Basis%gaussian(a_gauss)%CONTRC&
                                        *Basis%gaussian(b_gauss)%CONTRC&
                                        *Basis%gaussian(c_gauss)%CONTRC&
                                        *Basis%gaussian(d_gauss)%CONTRC*ram(1:81)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
!
              P_int = 0
              do iAO = Basis%atmshl(I_atmshl)%frstAO,Basis%atmshl(I_atmshl)%lastAO ! p
                do jAO = Basis%atmshl(J_atmshl)%frstAO,Basis%atmshl(J_atmshl)%lastAO ! p
                  do kAO = Basis%atmshl(K_atmshl)%frstAO,Basis%atmshl(K_atmshl)%lastAO ! p
                    do lAO = Basis%atmshl(L_atmshl)%frstAO,Basis%atmshl(L_atmshl)%lastAO ! p
!
                      P_int = P_int + 1
                      ! [pppp]
                      ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                      ramAO(ijkl_index) = ramAO_temp(P_int)
!
                    end do ! lAO
                  end do ! kAO
                end do ! jAO
              end do ! iAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
      end if ! N_p > 0
!
      if(N_d.gt.0)then
!
! [dsss] type integrals
!
      do I_d = 1, N_d ! loop over d shells
        LI = 2 
        I_shell = d_list(I_d)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_s = 1, N_s ! loop over s shells
          LJ = 0
          J_shell = s_list(J_s)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_s = 1, N_s ! loop over s shells
            LK = 0 
            K_shell = s_list(K_s)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_s = 1, N_s ! loop over s shells
              LL = 0 
              L_shell = s_list(L_s)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                      ramAO_temp(1:6) = ramAO_temp(1:6)&
                                        + Basis%gaussian(a_gauss)%CONTRC&
                                        *Basis%gaussian(b_gauss)%CONTRC&
                                        *Basis%gaussian(c_gauss)%CONTRC&
                                        *Basis%gaussian(d_gauss)%CONTRC*ram(1:6)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
! These are RAM integrals, 2-fold symmetry
! [ij|kl] = [kl|ij]
!
              jAO = Basis%atmshl(J_atmshl)%frstAO ! s
              kAO = Basis%atmshl(K_atmshl)%frstAO ! s
              lAO = Basis%atmshl(L_atmshl)%frstAO ! s
!            
              D_int = 0
              do iAO = Basis%atmshl(I_atmshl)%frstAO,Basis%atmshl(I_atmshl)%lastAO ! d
!
                D_int = D_int + 1
                ! [dsss]
                ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                ramAO(ijkl_index) = ramAO_temp(D_int)
                ! [ssds]
                ijkl_index = Nbasis**3*(kAO-1) + Nbasis**2*(lAO-1) + Nbasis*(iAO-1) + jAO
                ramAO(ijkl_index) = ramAO_temp(D_int)
!
              end do ! iAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
! [sdss] type integrals
!
      do I_s = 1, N_s ! loop over s shells
        LI = 0 
        I_shell = s_list(I_s)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_d = 1, N_d ! loop over d shells
          LJ = 2
          J_shell = d_list(J_d)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_s = 1, N_s ! loop over s shells
            LK = 0 
            K_shell = s_list(K_s)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_s = 1, N_s ! loop over s shells
              LL = 0 
              L_shell = s_list(L_s)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                      ramAO_temp(1:6) = ramAO_temp(1:6)&
                                        + Basis%gaussian(a_gauss)%CONTRC&
                                        *Basis%gaussian(b_gauss)%CONTRC&
                                        *Basis%gaussian(c_gauss)%CONTRC&
                                        *Basis%gaussian(d_gauss)%CONTRC*ram(1:6)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
! These are RAM integrals, 2-fold symmetry
! [ij|kl] = [kl|ij]
!
              iAO = Basis%atmshl(I_atmshl)%frstAO ! s
              kAO = Basis%atmshl(K_atmshl)%frstAO ! s
              lAO = Basis%atmshl(L_atmshl)%frstAO ! s
!            
              D_int = 0
              do jAO = Basis%atmshl(J_atmshl)%frstAO,Basis%atmshl(J_atmshl)%lastAO ! d
!
                D_int = D_int + 1
                ! [sdss]
                ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                ramAO(ijkl_index) = ramAO_temp(D_int)
                ! [sssd]
                ijkl_index = Nbasis**3*(kAO-1) + Nbasis**2*(lAO-1) + Nbasis*(iAO-1) + jAO
                ramAO(ijkl_index) = ramAO_temp(D_int)
!
              end do ! iAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
! [dpss] type integrals
!
      do I_d = 1, N_d ! loop over d shells
        LI = 2 
        I_shell = d_list(I_d)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_p = 1, N_p ! loop over p shells
          LJ = 1
          J_shell = p_list(J_p)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_s = 1, N_s ! loop over s shells
            LK = 0 
            K_shell = s_list(K_s)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_s = 1, N_s ! loop over s shells
              LL = 0 
              L_shell = s_list(L_s)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                      ramAO_temp(1:18) = ramAO_temp(1:18)&
                                        + Basis%gaussian(a_gauss)%CONTRC&
                                        *Basis%gaussian(b_gauss)%CONTRC&
                                        *Basis%gaussian(c_gauss)%CONTRC&
                                        *Basis%gaussian(d_gauss)%CONTRC*ram(1:18)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
! These are RAM integrals, 2-fold symmetry
! [ij|kl] = [kl|ij]
!
              kAO = Basis%atmshl(K_atmshl)%frstAO ! s
              lAO = Basis%atmshl(L_atmshl)%frstAO ! s
!
              D_int = 0
              do iAO = Basis%atmshl(I_atmshl)%frstAO,Basis%atmshl(I_atmshl)%lastAO ! d
                do jAO = Basis%atmshl(J_atmshl)%frstAO,Basis%atmshl(J_atmshl)%lastAO ! p
!
                  D_int = D_int + 1
                  ! [dpss]
                  ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                  ramAO(ijkl_index) = ramAO_temp(D_int)
                  ! [ssdp]
                  ijkl_index = Nbasis**3*(kAO-1) + Nbasis**2*(lAO-1) + Nbasis*(iAO-1) + jAO
                  ramAO(ijkl_index) = ramAO_temp(D_int)
!
                end do ! jAO
              end do ! iAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
! [dsps] type integrals
!
      do I_d = 1, N_d ! loop over d shells
        LI = 2 
        I_shell = d_list(I_d)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_s = 1, N_s ! loop over s shells
          LJ = 0
          J_shell = s_list(J_s)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_p = 1, N_p ! loop over p shells
            LK = 1 
            K_shell = p_list(K_p)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_s = 1, N_s ! loop over s shells
              LL = 0 
              L_shell = s_list(L_s)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                      ramAO_temp(1:18) = ramAO_temp(1:18)&
                                        + Basis%gaussian(a_gauss)%CONTRC&
                                        *Basis%gaussian(b_gauss)%CONTRC&
                                        *Basis%gaussian(c_gauss)%CONTRC&
                                        *Basis%gaussian(d_gauss)%CONTRC*ram(1:18)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
! These are RAM integrals, 2-fold symmetry
! [ij|kl] = [kl|ij]
!
              jAO = Basis%atmshl(J_atmshl)%frstAO ! s
              lAO = Basis%atmshl(L_atmshl)%frstAO ! s
!
              D_int = 0
              do iAO = Basis%atmshl(I_atmshl)%frstAO,Basis%atmshl(I_atmshl)%lastAO ! d
                do kAO = Basis%atmshl(K_atmshl)%frstAO,Basis%atmshl(K_atmshl)%lastAO ! p
!
                  D_int = D_int + 1
                  ! [dsps]
                  ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                  ramAO(ijkl_index) = ramAO_temp(D_int)
                  ! [psds]
                  ijkl_index = Nbasis**3*(kAO-1) + Nbasis**2*(lAO-1) + Nbasis*(iAO-1) + jAO
                  ramAO(ijkl_index) = ramAO_temp(D_int)
!
                end do ! jAO
              end do ! iAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
! [dssp] type integrals
!
      do I_d = 1, N_d ! loop over d shells
        LI = 2 
        I_shell = d_list(I_d)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_s = 1, N_s ! loop over s shells
          LJ = 0
          J_shell = s_list(J_s)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_s = 1, N_s ! loop over s shells
            LK = 0 
            K_shell = s_list(K_s)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_p = 1, N_p ! loop over p shells
              LL = 1 
              L_shell = p_list(L_p)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                      ramAO_temp(1:18) = ramAO_temp(1:18)&
                                        + Basis%gaussian(a_gauss)%CONTRC&
                                        *Basis%gaussian(b_gauss)%CONTRC&
                                        *Basis%gaussian(c_gauss)%CONTRC&
                                        *Basis%gaussian(d_gauss)%CONTRC*ram(1:18)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
! These are RAM integrals, 2-fold symmetry
! [ij|kl] = [kl|ij]
!
              jAO = Basis%atmshl(J_atmshl)%frstAO ! s
              kAO = Basis%atmshl(K_atmshl)%frstAO ! s
!
              D_int = 0
              do iAO = Basis%atmshl(I_atmshl)%frstAO,Basis%atmshl(I_atmshl)%lastAO ! d
                do lAO = Basis%atmshl(L_atmshl)%frstAO,Basis%atmshl(L_atmshl)%lastAO ! p
!
                  D_int = D_int + 1
                  ! [dssp]
                  ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                  ramAO(ijkl_index) = ramAO_temp(D_int)
                  ! [spds]
                  ijkl_index = Nbasis**3*(kAO-1) + Nbasis**2*(lAO-1) + Nbasis*(iAO-1) + jAO
                  ramAO(ijkl_index) = ramAO_temp(D_int)
!
                end do ! jAO
              end do ! iAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
! [pdss] type integrals
!
      do I_p = 1, N_p ! loop over p shells
        LI = 1 
        I_shell = p_list(I_p)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_d = 1, N_d ! loop over d shells
          LJ = 2
          J_shell = d_list(J_d)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_s = 1, N_s ! loop over s shells
            LK = 0 
            K_shell = s_list(K_s)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_s = 1, N_s ! loop over s shells
              LL = 0 
              L_shell = s_list(L_s)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                      ramAO_temp(1:18) = ramAO_temp(1:18)&
                                        + Basis%gaussian(a_gauss)%CONTRC&
                                        *Basis%gaussian(b_gauss)%CONTRC&
                                        *Basis%gaussian(c_gauss)%CONTRC&
                                        *Basis%gaussian(d_gauss)%CONTRC*ram(1:18)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
! These are RAM integrals, 2-fold symmetry
! [ij|kl] = [kl|ij]
!
              kAO = Basis%atmshl(K_atmshl)%frstAO ! s
              lAO = Basis%atmshl(L_atmshl)%frstAO ! s
!
              D_int = 0
              do iAO = Basis%atmshl(I_atmshl)%frstAO,Basis%atmshl(I_atmshl)%lastAO ! p
                do jAO = Basis%atmshl(J_atmshl)%frstAO,Basis%atmshl(J_atmshl)%lastAO ! d
!
                  D_int = D_int + 1
                  ! [pdss]
                  ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                  ramAO(ijkl_index) = ramAO_temp(D_int)
                  ! [sspd]
                  ijkl_index = Nbasis**3*(kAO-1) + Nbasis**2*(lAO-1) + Nbasis*(iAO-1) + jAO
                  ramAO(ijkl_index) = ramAO_temp(D_int)
!
                end do ! jAO
              end do ! iAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
! [sdps] type integrals
!
      do I_s = 1, N_s ! loop over s shells
        LI = 0 
        I_shell = s_list(I_s)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_d = 1, N_d ! loop over d shells
          LJ = 2
          J_shell = d_list(J_d)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_p = 1, N_p ! loop over p shells
            LK = 1 
            K_shell = p_list(K_p)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_s = 1, N_s ! loop over s shells
              LL = 0 
              L_shell = s_list(L_s)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                      ramAO_temp(1:18) = ramAO_temp(1:18)&
                                        + Basis%gaussian(a_gauss)%CONTRC&
                                        *Basis%gaussian(b_gauss)%CONTRC&
                                        *Basis%gaussian(c_gauss)%CONTRC&
                                        *Basis%gaussian(d_gauss)%CONTRC*ram(1:18)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
! These are RAM integrals, 2-fold symmetry
! [ij|kl] = [kl|ij]
!
              iAO = Basis%atmshl(I_atmshl)%frstAO ! s
              lAO = Basis%atmshl(L_atmshl)%frstAO ! s
!
              D_int = 0
              do jAO = Basis%atmshl(J_atmshl)%frstAO,Basis%atmshl(J_atmshl)%lastAO ! d
                do kAO = Basis%atmshl(K_atmshl)%frstAO,Basis%atmshl(K_atmshl)%lastAO ! p
!
                  D_int = D_int + 1
                  ! [sdps]
                  ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                  ramAO(ijkl_index) = ramAO_temp(D_int)
                  ! [pssd]
                  ijkl_index = Nbasis**3*(kAO-1) + Nbasis**2*(lAO-1) + Nbasis*(iAO-1) + jAO
                  ramAO(ijkl_index) = ramAO_temp(D_int)
!
                end do ! kAO
              end do ! jAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
! [sdsp] type integrals
!
      do I_s = 1, N_s ! loop over s shells
        LI = 0 
        I_shell = s_list(I_s)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_d = 1, N_d ! loop over d shells
          LJ = 2
          J_shell = d_list(J_d)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_s = 1, N_s ! loop over s shells
            LK = 0 
            K_shell = s_list(K_s)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_p = 1, N_p ! loop over p shells
              LL = 1 
              L_shell = p_list(L_p)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                      ramAO_temp(1:18) = ramAO_temp(1:18)&
                                        + Basis%gaussian(a_gauss)%CONTRC&
                                        *Basis%gaussian(b_gauss)%CONTRC&
                                        *Basis%gaussian(c_gauss)%CONTRC&
                                        *Basis%gaussian(d_gauss)%CONTRC*ram(1:18)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
! These are RAM integrals, 2-fold symmetry
! [ij|kl] = [kl|ij]
!
              iAO = Basis%atmshl(I_atmshl)%frstAO ! s
              kAO = Basis%atmshl(K_atmshl)%frstAO ! s
!
              D_int = 0
              do jAO = Basis%atmshl(J_atmshl)%frstAO,Basis%atmshl(J_atmshl)%lastAO ! d
                do lAO = Basis%atmshl(L_atmshl)%frstAO,Basis%atmshl(L_atmshl)%lastAO ! p
!
                  D_int = D_int + 1
                  ! [sdsp]
                  ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                  ramAO(ijkl_index) = ramAO_temp(D_int)
                  ! [spsd]
                  ijkl_index = Nbasis**3*(kAO-1) + Nbasis**2*(lAO-1) + Nbasis*(iAO-1) + jAO
                  ramAO(ijkl_index) = ramAO_temp(D_int)
!
                end do ! lAO
              end do ! jAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
! [ddss] type integrals
!
      do I_d = 1, N_d ! loop over d shells
        LI = 2 
        I_shell = d_list(I_d)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_d = 1, N_d ! loop over d shells
          LJ = 2
          J_shell = d_list(J_d)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_s = 1, N_s ! loop over s shells
            LK = 0 
            K_shell = s_list(K_s)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_s = 1, N_s ! loop over s shells
              LL = 0 
              L_shell = s_list(L_s)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                       ramAO_temp(1:36) = ramAO_temp(1:36)&
                                        + Basis%gaussian(a_gauss)%CONTRC&
                                        *Basis%gaussian(b_gauss)%CONTRC&
                                        *Basis%gaussian(c_gauss)%CONTRC&
                                        *Basis%gaussian(d_gauss)%CONTRC*ram(1:36)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
! These are RAM integrals, 2-fold symmetry
! [ij|kl] = [kl|ij]
!
              kAO = Basis%atmshl(K_atmshl)%frstAO ! s
              lAO = Basis%atmshl(L_atmshl)%frstAO ! s
!
              D_int = 0
              do iAO = Basis%atmshl(I_atmshl)%frstAO,Basis%atmshl(I_atmshl)%lastAO ! d
                do jAO = Basis%atmshl(J_atmshl)%frstAO,Basis%atmshl(J_atmshl)%lastAO ! d
!
                  D_int = D_int + 1
                  ! [ddss]
                  ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                  ramAO(ijkl_index) = ramAO_temp(D_int)
                  ! [ssdd]
                  ijkl_index = Nbasis**3*(kAO-1) + Nbasis**2*(lAO-1) + Nbasis*(iAO-1) + jAO
                  ramAO(ijkl_index) = ramAO_temp(D_int)
!
                end do ! jAO
              end do ! iAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
! [dsds] type integrals
!
      do I_d = 1, N_d ! loop over d shells
        LI = 2 
        I_shell = d_list(I_d)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_s = 1, N_s ! loop over s shells
          LJ = 0
          J_shell = s_list(J_s)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_d = 1, N_d ! loop over d shells
            LK = 2 
            K_shell = d_list(K_d)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_s = 1, N_s ! loop over s shells
              LL = 0 
              L_shell = s_list(L_s)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                       ramAO_temp(1:36) = ramAO_temp(1:36)&
                                        + Basis%gaussian(a_gauss)%CONTRC&
                                        *Basis%gaussian(b_gauss)%CONTRC&
                                        *Basis%gaussian(c_gauss)%CONTRC&
                                        *Basis%gaussian(d_gauss)%CONTRC*ram(1:36)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
! These are RAM integrals, 2-fold symmetry
! [ij|kl] = [kl|ij]
!
              jAO = Basis%atmshl(J_atmshl)%frstAO ! s
              lAO = Basis%atmshl(L_atmshl)%frstAO ! s
!
              D_int = 0
              do iAO = Basis%atmshl(I_atmshl)%frstAO,Basis%atmshl(I_atmshl)%lastAO ! d
                do kAO = Basis%atmshl(K_atmshl)%frstAO,Basis%atmshl(K_atmshl)%lastAO ! d
!
                  D_int = D_int + 1
                  ! [dsds]
                  ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                  ramAO(ijkl_index) = ramAO_temp(D_int)
!
                end do ! kAO
              end do ! iAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
! [dssd] type integrals
!
      do I_d = 1, N_d ! loop over d shells
        LI = 2 
        I_shell = d_list(I_d)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_s = 1, N_s ! loop over s shells
          LJ = 0
          J_shell = s_list(J_s)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_s = 1, N_s ! loop over s shells
            LK = 0 
            K_shell = s_list(K_s)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_d = 1, N_d ! loop over d shells
              LL = 2 
              L_shell = d_list(L_d)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                       ramAO_temp(1:36) = ramAO_temp(1:36)&
                                        + Basis%gaussian(a_gauss)%CONTRC&
                                        *Basis%gaussian(b_gauss)%CONTRC&
                                        *Basis%gaussian(c_gauss)%CONTRC&
                                        *Basis%gaussian(d_gauss)%CONTRC*ram(1:36)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
! These are RAM integrals, 2-fold symmetry
! [ij|kl] = [kl|ij]
!
              jAO = Basis%atmshl(J_atmshl)%frstAO ! s
              kAO = Basis%atmshl(K_atmshl)%frstAO ! s
!
              D_int = 0
              do iAO = Basis%atmshl(I_atmshl)%frstAO,Basis%atmshl(I_atmshl)%lastAO ! d
                do lAO = Basis%atmshl(L_atmshl)%frstAO,Basis%atmshl(L_atmshl)%lastAO ! d
!
                  D_int = D_int + 1
                  ! [dssd]
                  ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                  ramAO(ijkl_index) = ramAO_temp(D_int)
                  ! [sdds]
                  ijkl_index = Nbasis**3*(kAO-1) + Nbasis**2*(lAO-1) + Nbasis*(iAO-1) + jAO
                  ramAO(ijkl_index) = ramAO_temp(D_int)
!
                end do ! lAO
              end do ! iAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
! [sdsd] type integrals
!
      do I_s = 1, N_s ! loop over s shells
        LI = 0 
        I_shell = s_list(I_s)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_d = 1, N_d ! loop over d shells
          LJ = 2
          J_shell = d_list(J_d)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_s = 1, N_s ! loop over s shells
            LK = 0 
            K_shell = s_list(K_s)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_d = 1, N_d ! loop over d shells
              LL = 2 
              L_shell = d_list(L_d)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                       ramAO_temp(1:36) = ramAO_temp(1:36)&
                                        + Basis%gaussian(a_gauss)%CONTRC&
                                        *Basis%gaussian(b_gauss)%CONTRC&
                                        *Basis%gaussian(c_gauss)%CONTRC&
                                        *Basis%gaussian(d_gauss)%CONTRC*ram(1:36)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
! These are RAM integrals, 2-fold symmetry
! [ij|kl] = [kl|ij]
!
              iAO = Basis%atmshl(I_atmshl)%frstAO ! s
              kAO = Basis%atmshl(K_atmshl)%frstAO ! s
!
              D_int = 0
              do jAO = Basis%atmshl(J_atmshl)%frstAO,Basis%atmshl(J_atmshl)%lastAO ! d
                do lAO = Basis%atmshl(L_atmshl)%frstAO,Basis%atmshl(L_atmshl)%lastAO ! d
!
                  D_int = D_int + 1
                  ! [sdsd]
                  ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                  ramAO(ijkl_index) = ramAO_temp(D_int)
!
                end do ! lAO
              end do ! jAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
! [dpps] type integrals
!
      do I_d = 1, N_d ! loop over d shells
        LI = 2 
        I_shell = d_list(I_d)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_p = 1, N_p ! loop over p shells
          LJ = 1
          J_shell = p_list(J_p)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_p = 1, N_p ! loop over p shells
            LK = 1 
            K_shell = p_list(K_p)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_s = 1, N_s ! loop over s shells
              LL = 0 
              L_shell = s_list(L_s)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                       ramAO_temp(1:54) = ramAO_temp(1:54)&
                                        + Basis%gaussian(a_gauss)%CONTRC&
                                        *Basis%gaussian(b_gauss)%CONTRC&
                                        *Basis%gaussian(c_gauss)%CONTRC&
                                        *Basis%gaussian(d_gauss)%CONTRC*ram(1:54)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
! These are RAM integrals, 2-fold symmetry
! [ij|kl] = [kl|ij]
!
              lAO = Basis%atmshl(L_atmshl)%frstAO ! s
!
              D_int = 0
              do iAO = Basis%atmshl(I_atmshl)%frstAO,Basis%atmshl(I_atmshl)%lastAO ! d
                do jAO = Basis%atmshl(J_atmshl)%frstAO,Basis%atmshl(J_atmshl)%lastAO ! p
                  do kAO = Basis%atmshl(K_atmshl)%frstAO,Basis%atmshl(K_atmshl)%lastAO ! p
!
                    D_int = D_int + 1
                    ! [dpps]
                    ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                    ramAO(ijkl_index) = ramAO_temp(D_int)
                    ! [psdp]
                    ijkl_index = Nbasis**3*(kAO-1) + Nbasis**2*(lAO-1) + Nbasis*(iAO-1) + jAO
                    ramAO(ijkl_index) = ramAO_temp(D_int)
!
                  end do ! kAO
                end do ! jAO
              end do ! iAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
! [dpsp] type integrals
!
      do I_d = 1, N_d ! loop over d shells
        LI = 2 
        I_shell = d_list(I_d)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_p = 1, N_p ! loop over p shells
          LJ = 1
          J_shell = p_list(J_p)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_s = 1, N_s ! loop over s shells
            LK = 0 
            K_shell = s_list(K_s)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_p = 1, N_p ! loop over p shells
              LL = 1 
              L_shell = p_list(L_p)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                      ramAO_temp(1:54) = ramAO_temp(1:54)&
                                        + Basis%gaussian(a_gauss)%CONTRC&
                                        *Basis%gaussian(b_gauss)%CONTRC&
                                        *Basis%gaussian(c_gauss)%CONTRC&
                                        *Basis%gaussian(d_gauss)%CONTRC*ram(1:54)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
! These are RAM integrals, 2-fold symmetry
! [ij|kl] = [kl|ij]
!
              kAO = Basis%atmshl(K_atmshl)%frstAO ! s
!
              D_int = 0
              do iAO = Basis%atmshl(I_atmshl)%frstAO,Basis%atmshl(I_atmshl)%lastAO ! d
                do jAO = Basis%atmshl(J_atmshl)%frstAO,Basis%atmshl(J_atmshl)%lastAO ! p
                  do lAO = Basis%atmshl(L_atmshl)%frstAO,Basis%atmshl(L_atmshl)%lastAO ! p
!
                    D_int = D_int + 1
                    ! [dpsp]
                    ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                    ramAO(ijkl_index) = ramAO_temp(D_int)
                    ! [spdp]
                    ijkl_index = Nbasis**3*(kAO-1) + Nbasis**2*(lAO-1) + Nbasis*(iAO-1) + jAO
                    ramAO(ijkl_index) = ramAO_temp(D_int)
!
                  end do ! lAO
                end do ! jAO
              end do ! iAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
! [dspp] type integrals
!
      do I_d = 1, N_d ! loop over d shells
        LI = 2 
        I_shell = d_list(I_d)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_s = 1, N_s ! loop over s shells
          LJ = 0
          J_shell = s_list(J_s)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_p = 1, N_p ! loop over p shells
            LK = 1 
            K_shell = p_list(K_p)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_p = 1, N_p ! loop over p shells
              LL = 1 
              L_shell = p_list(L_p)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                      ramAO_temp(1:54) = ramAO_temp(1:54)&
                                        + Basis%gaussian(a_gauss)%CONTRC&
                                        *Basis%gaussian(b_gauss)%CONTRC&
                                        *Basis%gaussian(c_gauss)%CONTRC&
                                        *Basis%gaussian(d_gauss)%CONTRC*ram(1:54)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
! These are RAM integrals, 2-fold symmetry
! [ij|kl] = [kl|ij]
!
              jAO = Basis%atmshl(J_atmshl)%frstAO ! s
!
              D_int = 0
              do iAO = Basis%atmshl(I_atmshl)%frstAO,Basis%atmshl(I_atmshl)%lastAO ! d
                do kAO = Basis%atmshl(K_atmshl)%frstAO,Basis%atmshl(K_atmshl)%lastAO ! p
                  do lAO = Basis%atmshl(L_atmshl)%frstAO,Basis%atmshl(L_atmshl)%lastAO ! p
!
                    D_int = D_int + 1
                    ! [dspp]
                    ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                    ramAO(ijkl_index) = ramAO_temp(D_int)
                    ! [ppds]
                    ijkl_index = Nbasis**3*(kAO-1) + Nbasis**2*(lAO-1) + Nbasis*(iAO-1) + jAO
                    ramAO(ijkl_index) = ramAO_temp(D_int)
!
                  end do ! lAO
                end do ! kAO
              end do ! iAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
! [pdps] type integrals
!
      do I_p = 1, N_p ! loop over p shells
        LI = 1 
        I_shell = p_list(I_p)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_d = 1, N_d ! loop over d shells
          LJ = 2
          J_shell = d_list(J_d)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_p = 1, N_p ! loop over p shells
            LK = 1 
            K_shell = p_list(K_p)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_s = 1, N_s ! loop over s shells
              LL = 0 
              L_shell = s_list(L_s)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                      ramAO_temp(1:54) = ramAO_temp(1:54)&
                                        + Basis%gaussian(a_gauss)%CONTRC&
                                        *Basis%gaussian(b_gauss)%CONTRC&
                                        *Basis%gaussian(c_gauss)%CONTRC&
                                        *Basis%gaussian(d_gauss)%CONTRC*ram(1:54)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
! These are RAM integrals, 2-fold symmetry
! [ij|kl] = [kl|ij]
!
              lAO = Basis%atmshl(L_atmshl)%frstAO ! s
!
              D_int = 0
              do iAO = Basis%atmshl(I_atmshl)%frstAO,Basis%atmshl(I_atmshl)%lastAO ! p
                do jAO = Basis%atmshl(J_atmshl)%frstAO,Basis%atmshl(J_atmshl)%lastAO ! d
                  do kAO = Basis%atmshl(K_atmshl)%frstAO,Basis%atmshl(K_atmshl)%lastAO ! p
!
                    D_int = D_int + 1
                    ! [pdps]
                    ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                    ramAO(ijkl_index) = ramAO_temp(D_int)
                    ! [pspd]
                    ijkl_index = Nbasis**3*(kAO-1) + Nbasis**2*(lAO-1) + Nbasis*(iAO-1) + jAO
                    ramAO(ijkl_index) = ramAO_temp(D_int)
!
                  end do ! kAO
                end do ! jAO
              end do ! iAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
! [pdsp] type integrals
!
      do I_p = 1, N_p ! loop over p shells
        LI = 1 
        I_shell = p_list(I_p)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_d = 1, N_d ! loop over d shells
          LJ = 2
          J_shell = d_list(J_d)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_s = 1, N_s ! loop over s shells
            LK = 0 
            K_shell = s_list(K_s)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_p = 1, N_p ! loop over p shells
              LL = 1 
              L_shell = p_list(L_p)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                      ramAO_temp(1:54) = ramAO_temp(1:54)&
                                        + Basis%gaussian(a_gauss)%CONTRC&
                                        *Basis%gaussian(b_gauss)%CONTRC&
                                        *Basis%gaussian(c_gauss)%CONTRC&
                                        *Basis%gaussian(d_gauss)%CONTRC*ram(1:54)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
! These are RAM integrals, 2-fold symmetry
! [ij|kl] = [kl|ij]
!
              kAO = Basis%atmshl(K_atmshl)%frstAO ! s
!
              D_int = 0
              do iAO = Basis%atmshl(I_atmshl)%frstAO,Basis%atmshl(I_atmshl)%lastAO ! p
                do jAO = Basis%atmshl(J_atmshl)%frstAO,Basis%atmshl(J_atmshl)%lastAO ! d
                  do lAO = Basis%atmshl(L_atmshl)%frstAO,Basis%atmshl(L_atmshl)%lastAO ! p
!
                    D_int = D_int + 1
                    ! [pdsp]
                    ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                    ramAO(ijkl_index) = ramAO_temp(D_int)
                    ! [sppd]
                    ijkl_index = Nbasis**3*(kAO-1) + Nbasis**2*(lAO-1) + Nbasis*(iAO-1) + jAO
                    ramAO(ijkl_index) = ramAO_temp(D_int)
!
                  end do ! lAO
                end do ! jAO
              end do ! iAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
! [sdpp] type integrals
!
      do I_s = 1, N_s ! loop over s shells
        LI = 0 
        I_shell = s_list(I_s)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_d = 1, N_d ! loop over d shells
          LJ = 2
          J_shell = d_list(J_d)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_p = 1, N_p ! loop over p shells
            LK = 1 
            K_shell = p_list(K_p)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_p = 1, N_p ! loop over p shells
              LL = 1 
              L_shell = p_list(L_p)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                      ramAO_temp(1:54) = ramAO_temp(1:54)&
                                        + Basis%gaussian(a_gauss)%CONTRC&
                                        *Basis%gaussian(b_gauss)%CONTRC&
                                        *Basis%gaussian(c_gauss)%CONTRC&
                                        *Basis%gaussian(d_gauss)%CONTRC*ram(1:54)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
! These are RAM integrals, 2-fold symmetry
! [ij|kl] = [kl|ij]
!
              iAO = Basis%atmshl(I_atmshl)%frstAO ! s
!
              D_int = 0
              do jAO = Basis%atmshl(J_atmshl)%frstAO,Basis%atmshl(J_atmshl)%lastAO ! d
                do kAO = Basis%atmshl(K_atmshl)%frstAO,Basis%atmshl(K_atmshl)%lastAO ! p
                  do lAO = Basis%atmshl(L_atmshl)%frstAO,Basis%atmshl(L_atmshl)%lastAO ! p
!
                    D_int = D_int + 1
                    ! [sdpp]
                    ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                    ramAO(ijkl_index) = ramAO_temp(D_int)
                    ! [ppsd]
                    ijkl_index = Nbasis**3*(kAO-1) + Nbasis**2*(lAO-1) + Nbasis*(iAO-1) + jAO
                    ramAO(ijkl_index) = ramAO_temp(D_int)
!
                  end do ! lAO
                end do ! kAO
              end do ! jAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
! [ddps] type integrals
!
      do I_d = 1, N_d ! loop over d shells
        LI = 2 
        I_shell = d_list(I_d)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_d = 1, N_d ! loop over d shells
          LJ = 2
          J_shell = d_list(J_d)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_p = 1, N_p ! loop over p shells
            LK = 1 
            K_shell = p_list(K_p)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_s = 1, N_s ! loop over s shells
              LL = 0
              L_shell = s_list(L_s)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                      ramAO_temp(1:108) = ramAO_temp(1:108)&
                                        + Basis%gaussian(a_gauss)%CONTRC&
                                        *Basis%gaussian(b_gauss)%CONTRC&
                                        *Basis%gaussian(c_gauss)%CONTRC&
                                        *Basis%gaussian(d_gauss)%CONTRC*ram(1:108)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
! These are RAM integrals, 2-fold symmetry
! [ij|kl] = [kl|ij]
!
              lAO = Basis%atmshl(L_atmshl)%frstAO ! s
!
              D_int = 0
              do iAO = Basis%atmshl(I_atmshl)%frstAO,Basis%atmshl(I_atmshl)%lastAO ! d
                do jAO = Basis%atmshl(J_atmshl)%frstAO,Basis%atmshl(J_atmshl)%lastAO ! d
                  do kAO = Basis%atmshl(K_atmshl)%frstAO,Basis%atmshl(K_atmshl)%lastAO ! p
!
                    D_int = D_int + 1
                    ! [ddps]
                    ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                    ramAO(ijkl_index) = ramAO_temp(D_int)
                    ! [psdd]
                    ijkl_index = Nbasis**3*(kAO-1) + Nbasis**2*(lAO-1) + Nbasis*(iAO-1) + jAO
                    ramAO(ijkl_index) = ramAO_temp(D_int)
!
                  end do ! kAO
                end do ! jAO
              end do ! iAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
! [dpds] type integrals
!
      do I_d = 1, N_d ! loop over d shells
        LI = 2 
        I_shell = d_list(I_d)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_p = 1, N_p ! loop over p shells
          LJ = 1
          J_shell = p_list(J_p)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_d = 1, N_d ! loop over d shells
            LK = 2 
            K_shell = d_list(K_d)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_s = 1, N_s ! loop over s shells
              LL = 0
              L_shell = s_list(L_s)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                      ramAO_temp(1:108) = ramAO_temp(1:108)&
                                        + Basis%gaussian(a_gauss)%CONTRC&
                                        *Basis%gaussian(b_gauss)%CONTRC&
                                        *Basis%gaussian(c_gauss)%CONTRC&
                                        *Basis%gaussian(d_gauss)%CONTRC*ram(1:108)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
! These are RAM integrals, 2-fold symmetry
! [ij|kl] = [kl|ij]
!
              lAO = Basis%atmshl(L_atmshl)%frstAO ! s
!
              D_int = 0
              do iAO = Basis%atmshl(I_atmshl)%frstAO,Basis%atmshl(I_atmshl)%lastAO ! d
                do jAO = Basis%atmshl(J_atmshl)%frstAO,Basis%atmshl(J_atmshl)%lastAO ! p
                  do kAO = Basis%atmshl(K_atmshl)%frstAO,Basis%atmshl(K_atmshl)%lastAO ! d
!
                    D_int = D_int + 1
                    ! [dpds]
                    ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                    ramAO(ijkl_index) = ramAO_temp(D_int)
                    ! [dsdp]
                    ijkl_index = Nbasis**3*(kAO-1) + Nbasis**2*(lAO-1) + Nbasis*(iAO-1) + jAO
                    ramAO(ijkl_index) = ramAO_temp(D_int)
!
                  end do ! kAO
                end do ! jAO
              end do ! iAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
! [ddsp] type integrals
!
      do I_d = 1, N_d ! loop over d shells
        LI = 2 
        I_shell = d_list(I_d)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_d = 1, N_d ! loop over d shells
          LJ = 2
          J_shell = d_list(J_d)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_s = 1, N_s ! loop over s shells
            LK = 0 
            K_shell = s_list(K_s)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_p = 1, N_p ! loop over p shells
              LL = 1
              L_shell = p_list(L_p)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                      ramAO_temp(1:108) = ramAO_temp(1:108)&
                                        + Basis%gaussian(a_gauss)%CONTRC&
                                        *Basis%gaussian(b_gauss)%CONTRC&
                                        *Basis%gaussian(c_gauss)%CONTRC&
                                        *Basis%gaussian(d_gauss)%CONTRC*ram(1:108)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
! These are RAM integrals, 2-fold symmetry
! [ij|kl] = [kl|ij]
!
              kAO = Basis%atmshl(K_atmshl)%frstAO ! s
!
              D_int = 0
              do iAO = Basis%atmshl(I_atmshl)%frstAO,Basis%atmshl(I_atmshl)%lastAO ! d
                do jAO = Basis%atmshl(J_atmshl)%frstAO,Basis%atmshl(J_atmshl)%lastAO ! d
                  do lAO = Basis%atmshl(L_atmshl)%frstAO,Basis%atmshl(L_atmshl)%lastAO ! p
!
                    D_int = D_int + 1
                    ! [ddsp]
                    ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                    ramAO(ijkl_index) = ramAO_temp(D_int)
                    ! [spdd]
                    ijkl_index = Nbasis**3*(kAO-1) + Nbasis**2*(lAO-1) + Nbasis*(iAO-1) + jAO
                    ramAO(ijkl_index) = ramAO_temp(D_int)
!
                  end do ! lAO
                end do ! jAO
              end do ! iAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
! [dpsd] type integrals
!
      do I_d = 1, N_d ! loop over d shells
        LI = 2 
        I_shell = d_list(I_d)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_p = 1, N_p ! loop over p shells
          LJ = 1
          J_shell = p_list(J_p)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_s = 1, N_s ! loop over s shells
            LK = 0 
            K_shell = s_list(K_s)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_d = 1, N_d ! loop over d shells
              LL = 2
              L_shell = d_list(L_d)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                      ramAO_temp(1:108) = ramAO_temp(1:108)&
                                        + Basis%gaussian(a_gauss)%CONTRC&
                                        *Basis%gaussian(b_gauss)%CONTRC&
                                        *Basis%gaussian(c_gauss)%CONTRC&
                                        *Basis%gaussian(d_gauss)%CONTRC*ram(1:108)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
! These are RAM integrals, 2-fold symmetry
! [ij|kl] = [kl|ij]
!
              kAO = Basis%atmshl(K_atmshl)%frstAO ! s
!
              D_int = 0
              do iAO = Basis%atmshl(I_atmshl)%frstAO,Basis%atmshl(I_atmshl)%lastAO ! d
                do jAO = Basis%atmshl(J_atmshl)%frstAO,Basis%atmshl(J_atmshl)%lastAO ! p
                  do lAO = Basis%atmshl(L_atmshl)%frstAO,Basis%atmshl(L_atmshl)%lastAO ! d
!
                    D_int = D_int + 1
                    ! [dpsd]
                    ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                    ramAO(ijkl_index) = ramAO_temp(D_int)
                    ! [sddp]
                    ijkl_index = Nbasis**3*(kAO-1) + Nbasis**2*(lAO-1) + Nbasis*(iAO-1) + jAO
                    ramAO(ijkl_index) = ramAO_temp(D_int)
!
                  end do ! lAO
                end do ! jAO
              end do ! iAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
! [dspd] type integrals
!
      do I_d = 1, N_d ! loop over d shells
        LI = 2 
        I_shell = d_list(I_d)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_s = 1, N_s ! loop over s shells
          LJ = 0
          J_shell = s_list(J_s)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_p = 1, N_p ! loop over p shells
            LK = 1
            K_shell = p_list(K_p)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_d = 1, N_d ! loop over d shells
              LL = 2
              L_shell = d_list(L_d)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                      ramAO_temp(1:108) = ramAO_temp(1:108)&
                                        + Basis%gaussian(a_gauss)%CONTRC&
                                        *Basis%gaussian(b_gauss)%CONTRC&
                                        *Basis%gaussian(c_gauss)%CONTRC&
                                        *Basis%gaussian(d_gauss)%CONTRC*ram(1:108)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
! These are RAM integrals, 2-fold symmetry
! [ij|kl] = [kl|ij]
!
              jAO = Basis%atmshl(J_atmshl)%frstAO ! s
!
              D_int = 0
              do iAO = Basis%atmshl(I_atmshl)%frstAO,Basis%atmshl(I_atmshl)%lastAO ! d
                do kAO = Basis%atmshl(K_atmshl)%frstAO,Basis%atmshl(K_atmshl)%lastAO ! p
                  do lAO = Basis%atmshl(L_atmshl)%frstAO,Basis%atmshl(L_atmshl)%lastAO ! d
!
                    D_int = D_int + 1
                    ! [dspd]
                    ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                    ramAO(ijkl_index) = ramAO_temp(D_int)
                    ! [pdds]
                    ijkl_index = Nbasis**3*(kAO-1) + Nbasis**2*(lAO-1) + Nbasis*(iAO-1) + jAO
                    ramAO(ijkl_index) = ramAO_temp(D_int)
!
                  end do ! lAO
                end do ! kAO
              end do ! iAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
! [pdsd] type integrals
!
      do I_p = 1, N_p ! loop over p shells
        LI = 1
        I_shell = p_list(I_p)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_d = 1, N_d ! loop over d shells
          LJ = 2
          J_shell = d_list(J_d)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_s = 1, N_s ! loop over s shells
            LK = 0
            K_shell = s_list(K_s)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_d = 1, N_d ! loop over d shells
              LL = 2
              L_shell = d_list(L_d)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                      ramAO_temp(1:108) = ramAO_temp(1:108)&
                                        + Basis%gaussian(a_gauss)%CONTRC&
                                        *Basis%gaussian(b_gauss)%CONTRC&
                                        *Basis%gaussian(c_gauss)%CONTRC&
                                        *Basis%gaussian(d_gauss)%CONTRC*ram(1:108)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
! These are RAM integrals, 2-fold symmetry
! [ij|kl] = [kl|ij]
!
              kAO = Basis%atmshl(K_atmshl)%frstAO ! s
!
              D_int = 0
              do iAO = Basis%atmshl(I_atmshl)%frstAO,Basis%atmshl(I_atmshl)%lastAO ! p
                do jAO = Basis%atmshl(J_atmshl)%frstAO,Basis%atmshl(J_atmshl)%lastAO ! d
                  do lAO = Basis%atmshl(L_atmshl)%frstAO,Basis%atmshl(L_atmshl)%lastAO ! d
!
                    D_int = D_int + 1
                    ! [pdsd]
                    ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                    ramAO(ijkl_index) = ramAO_temp(D_int)
                    ! [sdpd]
                    ijkl_index = Nbasis**3*(kAO-1) + Nbasis**2*(lAO-1) + Nbasis*(iAO-1) + jAO
                    ramAO(ijkl_index) = ramAO_temp(D_int)
!
                  end do ! lAO
                end do ! jAO
              end do ! iAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
! [ddds] type integrals
!
      do I_d = 1, N_d ! loop over d shells
        LI = 2 
        I_shell = d_list(I_d)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_d = 1, N_d ! loop over d shells
          LJ = 2
          J_shell = d_list(J_d)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_d = 1, N_d ! loop over d shells
            LK = 2 
            K_shell = d_list(K_d)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_s = 1, N_s ! loop over s shells
              LL = 0
              L_shell = s_list(L_s)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                      ramAO_temp(1:216) = ramAO_temp(1:216)&
                                        + Basis%gaussian(a_gauss)%CONTRC&
                                        *Basis%gaussian(b_gauss)%CONTRC&
                                        *Basis%gaussian(c_gauss)%CONTRC&
                                        *Basis%gaussian(d_gauss)%CONTRC*ram(1:216)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
! These are RAM integrals, 2-fold symmetry
! [ij|kl] = [kl|ij]
!
              lAO = Basis%atmshl(L_atmshl)%frstAO ! s
!
              D_int = 0
              do iAO = Basis%atmshl(I_atmshl)%frstAO,Basis%atmshl(I_atmshl)%lastAO ! d
                do jAO = Basis%atmshl(J_atmshl)%frstAO,Basis%atmshl(J_atmshl)%lastAO ! d
                  do kAO = Basis%atmshl(K_atmshl)%frstAO,Basis%atmshl(K_atmshl)%lastAO ! d
!
                    D_int = D_int + 1
                    ! [ddds]
                    ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                    ramAO(ijkl_index) = ramAO_temp(D_int)
                    ! [dsdd]
                    ijkl_index = Nbasis**3*(kAO-1) + Nbasis**2*(lAO-1) + Nbasis*(iAO-1) + jAO
                    ramAO(ijkl_index) = ramAO_temp(D_int)
!
                  end do ! kAO
                end do ! jAO
              end do ! iAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
! [ddsd] type integrals
!
      do I_d = 1, N_d ! loop over d shells
        LI = 2 
        I_shell = d_list(I_d)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_d = 1, N_d ! loop over d shells
          LJ = 2
          J_shell = d_list(J_d)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_s = 1, N_s ! loop over s shells
            LK = 0 
            K_shell = s_list(K_s)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_d = 1, N_d ! loop over d shells
              LL = 2
              L_shell = d_list(L_d)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                      ramAO_temp(1:216) = ramAO_temp(1:216)&
                                        + Basis%gaussian(a_gauss)%CONTRC&
                                        *Basis%gaussian(b_gauss)%CONTRC&
                                        *Basis%gaussian(c_gauss)%CONTRC&
                                        *Basis%gaussian(d_gauss)%CONTRC*ram(1:216)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
! These are RAM integrals, 2-fold symmetry
! [ij|kl] = [kl|ij]
!
              kAO = Basis%atmshl(K_atmshl)%frstAO ! s
!
              D_int = 0
              do iAO = Basis%atmshl(I_atmshl)%frstAO,Basis%atmshl(I_atmshl)%lastAO ! d
                do jAO = Basis%atmshl(J_atmshl)%frstAO,Basis%atmshl(J_atmshl)%lastAO ! d
                  do lAO = Basis%atmshl(L_atmshl)%frstAO,Basis%atmshl(L_atmshl)%lastAO ! d
!
                    D_int = D_int + 1
                    ! [ddsd]
                    ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                    ramAO(ijkl_index) = ramAO_temp(D_int)
                    ! [sddd]
                    ijkl_index = Nbasis**3*(kAO-1) + Nbasis**2*(lAO-1) + Nbasis*(iAO-1) + jAO
                    ramAO(ijkl_index) = ramAO_temp(D_int)
!
                  end do ! kAO
                end do ! jAO
              end do ! iAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
! [dppp] type integrals
!
      do I_d = 1, N_d ! loop over d shells
        LI = 2 
        I_shell = d_list(I_d)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_p = 1, N_p ! loop over p shells
          LJ = 1
          J_shell = p_list(J_p)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_p = 1, N_p ! loop over p shells
            LK = 1 
            K_shell = p_list(K_p)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_p = 1, N_p ! loop over p shells
              LL = 1 
              L_shell = p_list(L_p)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                      ramAO_temp(1:162) = ramAO_temp(1:162)&
                                        + Basis%gaussian(a_gauss)%CONTRC&
                                        *Basis%gaussian(b_gauss)%CONTRC&
                                        *Basis%gaussian(c_gauss)%CONTRC&
                                        *Basis%gaussian(d_gauss)%CONTRC*ram(1:162)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
! These are RAM integrals, 2-fold symmetry
! [ij|kl] = [kl|ij]
!
              D_int = 0
              do iAO = Basis%atmshl(I_atmshl)%frstAO,Basis%atmshl(I_atmshl)%lastAO ! d
                do jAO = Basis%atmshl(J_atmshl)%frstAO,Basis%atmshl(J_atmshl)%lastAO ! p
                  do kAO = Basis%atmshl(K_atmshl)%frstAO,Basis%atmshl(K_atmshl)%lastAO ! p
                    do lAO = Basis%atmshl(L_atmshl)%frstAO,Basis%atmshl(L_atmshl)%lastAO ! p
!
                      D_int = D_int + 1
                      ! [dppp]
                      ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                      ramAO(ijkl_index) = ramAO_temp(D_int)
                      ! [ppdp]
                      ijkl_index = Nbasis**3*(kAO-1) + Nbasis**2*(lAO-1) + Nbasis*(iAO-1) + jAO
                      ramAO(ijkl_index) = ramAO_temp(D_int)
!
                    end do ! lAO
                  end do ! kAO
                end do ! jAO
              end do ! iAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
! [pdpp] type integrals
!
      do I_p = 1, N_p ! loop over p shells
        LI = 1 
        I_shell = p_list(I_p)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_d = 1, N_d ! loop over d shells
          LJ = 2
          J_shell = d_list(J_d)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_p = 1, N_p ! loop over p shells
            LK = 1 
            K_shell = p_list(K_p)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_p = 1, N_p ! loop over p shells
              LL = 1 
              L_shell = p_list(L_p)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                      ramAO_temp(1:162) = ramAO_temp(1:162)&
                                        + Basis%gaussian(a_gauss)%CONTRC&
                                        *Basis%gaussian(b_gauss)%CONTRC&
                                        *Basis%gaussian(c_gauss)%CONTRC&
                                        *Basis%gaussian(d_gauss)%CONTRC*ram(1:162)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
! These are RAM integrals, 2-fold symmetry
! [ij|kl] = [kl|ij]
!
              D_int = 0
              do iAO = Basis%atmshl(I_atmshl)%frstAO,Basis%atmshl(I_atmshl)%lastAO ! p
                do jAO = Basis%atmshl(J_atmshl)%frstAO,Basis%atmshl(J_atmshl)%lastAO ! d
                  do kAO = Basis%atmshl(K_atmshl)%frstAO,Basis%atmshl(K_atmshl)%lastAO ! p
                    do lAO = Basis%atmshl(L_atmshl)%frstAO,Basis%atmshl(L_atmshl)%lastAO ! p
!
                      D_int = D_int + 1
                      ! [pdpp]
                      ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                      ramAO(ijkl_index) = ramAO_temp(D_int)
                      ! [pppd]
                      ijkl_index = Nbasis**3*(kAO-1) + Nbasis**2*(lAO-1) + Nbasis*(iAO-1) + jAO
                      ramAO(ijkl_index) = ramAO_temp(D_int)
!
                    end do ! lAO
                  end do ! kAO
                end do ! jAO
              end do ! iAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
! [ddpp] type integrals
!
      do I_d = 1, N_d ! loop over d shells
        LI = 2 
        I_shell = d_list(I_d)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_d = 1, N_d ! loop over d shells
          LJ = 2
          J_shell = d_list(J_d)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_p = 1, N_p ! loop over p shells
            LK = 1 
            K_shell = p_list(K_p)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_p = 1, N_p ! loop over p shells
              LL = 1
              L_shell = p_list(L_p)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                      ramAO_temp(1:324) = ramAO_temp(1:324)&
                                        + Basis%gaussian(a_gauss)%CONTRC&
                                        *Basis%gaussian(b_gauss)%CONTRC&
                                        *Basis%gaussian(c_gauss)%CONTRC&
                                        *Basis%gaussian(d_gauss)%CONTRC*ram(1:324)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
! These are RAM integrals, 2-fold symmetry
! [ij|kl] = [kl|ij]
!
              D_int = 0
              do iAO = Basis%atmshl(I_atmshl)%frstAO,Basis%atmshl(I_atmshl)%lastAO ! d
                do jAO = Basis%atmshl(J_atmshl)%frstAO,Basis%atmshl(J_atmshl)%lastAO ! d
                  do kAO = Basis%atmshl(K_atmshl)%frstAO,Basis%atmshl(K_atmshl)%lastAO ! p
                    do lAO = Basis%atmshl(L_atmshl)%frstAO,Basis%atmshl(L_atmshl)%lastAO ! p
!
                      D_int = D_int + 1
                      ! [ddpp]
                      ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                      ramAO(ijkl_index) = ramAO_temp(D_int)
                      ! [ppdd]
                      ijkl_index = Nbasis**3*(kAO-1) + Nbasis**2*(lAO-1) + Nbasis*(iAO-1) + jAO
                      ramAO(ijkl_index) = ramAO_temp(D_int)
!
                    end do ! lAO
                  end do ! kAO
                end do ! jAO
              end do ! iAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
! [dpdp] type integrals
!
      do I_d = 1, N_d ! loop over d shells
        LI = 2 
        I_shell = d_list(I_d)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_p = 1, N_p ! loop over p shells
          LJ = 1
          J_shell = p_list(J_p)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_d = 1, N_d ! loop over d shells
            LK = 2 
            K_shell = d_list(K_d)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_p = 1, N_p ! loop over p shells
              LL = 1
              L_shell = p_list(L_p)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                      ramAO_temp(1:324) = ramAO_temp(1:324)&
                                        + Basis%gaussian(a_gauss)%CONTRC&
                                        *Basis%gaussian(b_gauss)%CONTRC&
                                        *Basis%gaussian(c_gauss)%CONTRC&
                                        *Basis%gaussian(d_gauss)%CONTRC*ram(1:324)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
! These are RAM integrals, 2-fold symmetry
! [ij|kl] = [kl|ij]
!
              D_int = 0
              do iAO = Basis%atmshl(I_atmshl)%frstAO,Basis%atmshl(I_atmshl)%lastAO ! d
                do jAO = Basis%atmshl(J_atmshl)%frstAO,Basis%atmshl(J_atmshl)%lastAO ! p
                  do kAO = Basis%atmshl(K_atmshl)%frstAO,Basis%atmshl(K_atmshl)%lastAO ! d
                    do lAO = Basis%atmshl(L_atmshl)%frstAO,Basis%atmshl(L_atmshl)%lastAO ! p
!
                      D_int = D_int + 1
                      ! [dpdp]
                      ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                      ramAO(ijkl_index) = ramAO_temp(D_int)
!
                    end do ! lAO
                  end do ! kAO
                end do ! jAO
              end do ! iAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
! [dppd] type integrals
!
      do I_d = 1, N_d ! loop over d shells
        LI = 2 
        I_shell = d_list(I_d)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_p = 1, N_p ! loop over p shells
          LJ = 1
          J_shell = p_list(J_p)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_p = 1, N_p ! loop over p shells
            LK = 1 
            K_shell = p_list(K_p)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_d = 1, N_d ! loop over d shells
              LL = 2
              L_shell = d_list(L_d)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                      ramAO_temp(1:324) = ramAO_temp(1:324)&
                                        + Basis%gaussian(a_gauss)%CONTRC&
                                        *Basis%gaussian(b_gauss)%CONTRC&
                                        *Basis%gaussian(c_gauss)%CONTRC&
                                        *Basis%gaussian(d_gauss)%CONTRC*ram(1:324)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
! These are RAM integrals, 2-fold symmetry
! [ij|kl] = [kl|ij]
!
              D_int = 0
              do iAO = Basis%atmshl(I_atmshl)%frstAO,Basis%atmshl(I_atmshl)%lastAO ! d
                do jAO = Basis%atmshl(J_atmshl)%frstAO,Basis%atmshl(J_atmshl)%lastAO ! p
                  do kAO = Basis%atmshl(K_atmshl)%frstAO,Basis%atmshl(K_atmshl)%lastAO ! p
                    do lAO = Basis%atmshl(L_atmshl)%frstAO,Basis%atmshl(L_atmshl)%lastAO ! d
!
                      D_int = D_int + 1
                      ! [dppd]
                      ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                      ramAO(ijkl_index) = ramAO_temp(D_int)
                      ! [pddp]
                      ijkl_index = Nbasis**3*(kAO-1) + Nbasis**2*(lAO-1) + Nbasis*(iAO-1) + jAO
                      ramAO(ijkl_index) = ramAO_temp(D_int)
!
                    end do ! lAO
                  end do ! kAO
                end do ! jAO
              end do ! iAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
! [pdpd] type integrals
!
      do I_p = 1, N_p ! loop over p shells
        LI = 1 
        I_shell = p_list(I_p)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_d = 1, N_d ! loop over d shells
          LJ = 2
          J_shell = d_list(J_d)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_p = 1, N_p ! loop over p shells
            LK = 1 
            K_shell = p_list(K_p)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_d = 1, N_d ! loop over d shells
              LL = 2
              L_shell = d_list(L_d)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                      ramAO_temp(1:324) = ramAO_temp(1:324)&
                                        + Basis%gaussian(a_gauss)%CONTRC&
                                        *Basis%gaussian(b_gauss)%CONTRC&
                                        *Basis%gaussian(c_gauss)%CONTRC&
                                        *Basis%gaussian(d_gauss)%CONTRC*ram(1:324)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
! These are RAM integrals, 2-fold symmetry
! [ij|kl] = [kl|ij]
!
              D_int = 0
              do iAO = Basis%atmshl(I_atmshl)%frstAO,Basis%atmshl(I_atmshl)%lastAO ! p
                do jAO = Basis%atmshl(J_atmshl)%frstAO,Basis%atmshl(J_atmshl)%lastAO ! d
                  do kAO = Basis%atmshl(K_atmshl)%frstAO,Basis%atmshl(K_atmshl)%lastAO ! p
                    do lAO = Basis%atmshl(L_atmshl)%frstAO,Basis%atmshl(L_atmshl)%lastAO ! d
!
                      D_int = D_int + 1
                      ! [pdpd]
                      ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                      ramAO(ijkl_index) = ramAO_temp(D_int)
!
                    end do ! lAO
                  end do ! kAO
                end do ! jAO
              end do ! iAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
! [dddp] type integrals
!
      do I_d = 1, N_d ! loop over d shells
        LI = 2 
        I_shell = d_list(I_d)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_d = 1, N_d ! loop over d shells
          LJ = 2
          J_shell = d_list(J_d)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_d = 1, N_d ! loop over d shells
            LK = 2 
            K_shell = d_list(K_d)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_p = 1, N_p ! loop over p shells
              LL = 1
              L_shell = p_list(L_p)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                      ramAO_temp(1:648) = ramAO_temp(1:648)&
                                        + Basis%gaussian(a_gauss)%CONTRC&
                                        *Basis%gaussian(b_gauss)%CONTRC&
                                        *Basis%gaussian(c_gauss)%CONTRC&
                                        *Basis%gaussian(d_gauss)%CONTRC*ram(1:648)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
! These are RAM integrals, 2-fold symmetry
! [ij|kl] = [kl|ij]
!
              D_int = 0
              do iAO = Basis%atmshl(I_atmshl)%frstAO,Basis%atmshl(I_atmshl)%lastAO ! d
                do jAO = Basis%atmshl(J_atmshl)%frstAO,Basis%atmshl(J_atmshl)%lastAO ! d
                  do kAO = Basis%atmshl(K_atmshl)%frstAO,Basis%atmshl(K_atmshl)%lastAO ! d
                    do lAO = Basis%atmshl(L_atmshl)%frstAO,Basis%atmshl(L_atmshl)%lastAO ! p
!
                      D_int = D_int + 1
                      ! [dddp]
                      ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                      ramAO(ijkl_index) = ramAO_temp(D_int)
                      ! [dpdd]
                      ijkl_index = Nbasis**3*(kAO-1) + Nbasis**2*(lAO-1) + Nbasis*(iAO-1) + jAO
                      ramAO(ijkl_index) = ramAO_temp(D_int)
!
                    end do ! lAO
                  end do ! kAO
                end do ! jAO
              end do ! iAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
! [ddpd] type integrals
!
      do I_d = 1, N_d ! loop over d shells
        LI = 2 
        I_shell = d_list(I_d)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_d = 1, N_d ! loop over d shells
          LJ = 2
          J_shell = d_list(J_d)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_p = 1, N_p ! loop over p shells
            LK = 1 
            K_shell = p_list(K_p)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_d = 1, N_d ! loop over d shells
              LL = 2
              L_shell = d_list(L_d)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                      ramAO_temp(1:648) = ramAO_temp(1:648)&
                                        + Basis%gaussian(a_gauss)%CONTRC&
                                        *Basis%gaussian(b_gauss)%CONTRC&
                                        *Basis%gaussian(c_gauss)%CONTRC&
                                        *Basis%gaussian(d_gauss)%CONTRC*ram(1:648)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
! These are RAM integrals, 2-fold symmetry
! [ij|kl] = [kl|ij]
!
              D_int = 0
              do iAO = Basis%atmshl(I_atmshl)%frstAO,Basis%atmshl(I_atmshl)%lastAO ! d
                do jAO = Basis%atmshl(J_atmshl)%frstAO,Basis%atmshl(J_atmshl)%lastAO ! d
                  do kAO = Basis%atmshl(K_atmshl)%frstAO,Basis%atmshl(K_atmshl)%lastAO ! p
                    do lAO = Basis%atmshl(L_atmshl)%frstAO,Basis%atmshl(L_atmshl)%lastAO ! d
!
                      D_int = D_int + 1
                      ! [ddpd]
                      ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                      ramAO(ijkl_index) = ramAO_temp(D_int)
                      ! [pddd]
                      ijkl_index = Nbasis**3*(kAO-1) + Nbasis**2*(lAO-1) + Nbasis*(iAO-1) + jAO
                      ramAO(ijkl_index) = ramAO_temp(D_int)
!
                    end do ! lAO
                  end do ! kAO
                end do ! jAO
              end do ! iAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
! [dddd] type integrals
!
      do I_d = 1, N_d ! loop over d shells
        LI = 2 
        I_shell = d_list(I_d)
        firstg_I = Basis%shell(I_shell)%EXPBGN
        lastg_I = Basis%shell(I_shell)%EXPEND
        do I_atmshl = Basis%shell(I_shell)%frstshl, Basis%shell(I_shell)%lastshl ! atom shells
        R_A(1) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%X
        R_A(2) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Y
        R_A(3) = CARTESIAN(Basis%atmshl(I_atmshl)%ATMLST)%Z
!
        do J_d = 1, N_d ! loop over d shells
          LJ = 2
          J_shell = d_list(J_d)
          firstg_J = Basis%shell(J_shell)%EXPBGN
          lastg_J = Basis%shell(J_shell)%EXPEND
          do J_atmshl = Basis%shell(J_shell)%frstshl, Basis%shell(J_shell)%lastshl ! atom shells
          R_B(1) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%X
          R_B(2) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Y
          R_B(3) = CARTESIAN(Basis%atmshl(J_atmshl)%ATMLST)%Z
!
          do K_d = 1, N_d ! loop over d shells
            LK = 2 
            K_shell = d_list(K_d)
            firstg_K = Basis%shell(K_shell)%EXPBGN
            lastg_K = Basis%shell(K_shell)%EXPEND
            do K_atmshl = Basis%shell(K_shell)%frstshl, Basis%shell(K_shell)%lastshl ! atom shells
            R_C(1) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%X
            R_C(2) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Y
            R_C(3) = CARTESIAN(Basis%atmshl(K_atmshl)%ATMLST)%Z
!
            do L_d = 1, N_d ! loop over d shells
              LL = 2
              L_shell = d_list(L_d)
              firstg_L = Basis%shell(L_shell)%EXPBGN
              lastg_L = Basis%shell(L_shell)%EXPEND
              do L_atmshl = Basis%shell(L_shell)%frstshl, Basis%shell(L_shell)%lastshl ! atom shells
              R_D(1) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%X
              R_D(2) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Y
              R_D(3) = CARTESIAN(Basis%atmshl(L_atmshl)%ATMLST)%Z
!
              ramAO_temp = 0.0D0
!
              do a_gauss = firstg_I, lastg_I ! primitives
                do b_gauss = firstg_J, lastg_J
                  do c_gauss = firstg_K, lastg_K ! primitives
                    do d_gauss = firstg_L, lastg_L
!                       
                      call RAMint (R_A,R_B,R_C,R_D,&
                                   Basis%gaussian(a_gauss)%exp,Basis%gaussian(b_gauss)%exp,&
                                   Basis%gaussian(c_gauss)%exp,Basis%gaussian(d_gauss)%exp,&
                                   LI,LJ,LK,LL,ram) 
! contract
                     ramAO_temp(1:1296) = ramAO_temp(1:1296)&
                                        + Basis%gaussian(a_gauss)%CONTRC&
                                        *Basis%gaussian(b_gauss)%CONTRC&
                                        *Basis%gaussian(c_gauss)%CONTRC&
                                        *Basis%gaussian(d_gauss)%CONTRC*ram(1:1296)
                    end do ! d_gauss
                  end do ! c_gauss
                end do ! b_gauss
              end do ! a_gauss
!
! Put integrals in correct places in ramAO
!
              D_int = 0
              do iAO = Basis%atmshl(I_atmshl)%frstAO,Basis%atmshl(I_atmshl)%lastAO ! d
                do jAO = Basis%atmshl(J_atmshl)%frstAO,Basis%atmshl(J_atmshl)%lastAO ! d
                  do kAO = Basis%atmshl(K_atmshl)%frstAO,Basis%atmshl(K_atmshl)%lastAO ! d
                    do lAO = Basis%atmshl(L_atmshl)%frstAO,Basis%atmshl(L_atmshl)%lastAO ! d
!
                      D_int = D_int + 1
                      ! [dddd]
                      ijkl_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(kAO-1) + lAO
                      ramAO(ijkl_index) = ramAO_temp(D_int)
!
                    end do ! lAO
                  end do ! kAO
                end do ! jAO
              end do ! iAO
!
              end do ! L_atmshl
            end do ! L_shell
            end do ! K_atmshl
          end do ! K_shell
          end do ! J_atmshl
        end do ! J_shell
        end do ! I_atmshl
      end do ! I_shell
!
      end if ! N_d > 0
!
! End of routine BLD_RAMint
      call PRG_manager ('exit', 'BLD_RAMint', 'RAMINT%AO')
      return
      end subroutine BLD_RAMint
