      subroutine I1E_Exch (IApoint, MATlen)
!***********************************************************************
!     Date last modified: February 9, 2006                 Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: Attempt at calculating the Exchange numerically.    *
! NOTE must loop over grid points
!***********************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE type_basis_set
      USE INT_objects
      USE module_grid_points

      implicit none

!
! Input scalars:
      integer :: MATlen
      integer :: IApoint
!
! Local scalars:
      integer :: Ishell,Jshell,Ifrst,Jfrst,Ilast,Jlast
      integer :: Istart,Jstart,Iend,Jend
      integer :: Iatmshl,Jatmshl
      integer :: LAMAX,LBMAX
      integer :: Iatom,Jatom
      integer :: Irange,Jrange
      integer :: Igauss,Jgauss
      integer :: Iaos,Jaos
      integer :: IGBGN,JGBGN,IGend,JGend
      integer :: LPMAX,I,IA,IJX,IJY,IJZ,INTC,IX,IXP,IY,IYP,IZ,IZERO,IZP,J,JX,JY,JZ,LIM1DS,NZERO
      integer :: LENTQ
!     double precision :: V12dr2(MATlen)
      double precision ::TRACLO

      double precision ABX,ABY,ABZ,ARABSQ,ARG,AS,ASXA,ASYA,ASZA,BS,COEF,EP,EPI,EPIO2,PCX,PCY,PCZ,PEXP, &
                       PX,PY,PZ,RPCSQ,TWOASQ,TWOP,TWOPI,TWOPT2,XAP,XBP, &
                       XK,YAP,YBP,ZAP,ZBP,ZCONST,ZT,ZTEMP
      double precision :: XA,XB,XC,YA,YB,YC,ZA,ZB,ZC,RABSQ
      integer INDIX(20),INDIY(20),INDIZ(20),INDJX(20),INDJY(20),INDJZ(20)
      double precision :: TP(7),WP(7)
      double precision A(45),CA(20),CB(20),CCX(192),CCY(192),CCZ(192),EEPB(100),EEPV(100), &
                       SX(32),SY(32),SZ(32), &
                       S1C(6),TWOCX(9),TWOCY(9),TWOCZ(9),XIP(80),YIP(80),ZIP(80)

      double precision :: CUT1,CUT2
      parameter (TWOPI=TWO*PI_VAL,CUT1=-75.0D0,CUT2=1.0D+04)
      DATA INDJX/1,2,1,1,3,1,1,2,2,1,4,1,1,2,3,3,2,1,1,2/,INDJY/1,1,2,1,1,3,1,2,1,2,1,4,1,3,2,1,1,2,3,2/, &
           INDJZ/1,1,1,2,1,1,3,1,2,2,1,1,4,1,1,2,3,3,2,2/
! Begin:
      call PRG_manager ('enter', 'I1E_Exch', 'UTILITY')
!
      V12dr2=ZERO
!
! Calculate the potential at grid point
      XC=grid_points(IApoint)%X
      YC=grid_points(IApoint)%Y
      ZC=grid_points(IApoint)%Z

      call RYSSET
!
! Fill INDIX.
      do I=1,20
        INDIX(I)=4*(INDJX(I)-1)
        INDIY(I)=4*(INDJY(I)-1)
        INDIZ(I)=4*(INDJZ(I)-1)
      end do ! I
!
! Loop over shells.
!     Exch_potl(1:Nbasis,1:CMO%NoccMO) = 0.0D0
! Loop over Ishell.
      do Ishell=1,Basis%Nshells
      LAMAX=Basis%shell(Ishell)%Xtype+1
      Istart=Basis%shell(Ishell)%Xstart
      Iend=Basis%shell(Ishell)%XEND
      Irange=Iend-Istart+1
      IGBGN=Basis%shell(Ishell)%EXPBGN
      IGEND=Basis%shell(Ishell)%EXPEND
      Ifrst=Basis%shell(Ishell)%frstSHL
!
! Loop over Jshell.
      do Jshell=1,Ishell
      LBMAX=Basis%shell(Jshell)%Xtype+1
      Jstart=Basis%shell(Jshell)%Xstart
      Jend=Basis%shell(Jshell)%XEND
      Jrange=Jend-Jstart+1
      JGBGN=Basis%shell(Jshell)%EXPBGN
      JGEND=Basis%shell(Jshell)%EXPEND
      Jfrst=Basis%shell(Jshell)%frstSHL
!
      Ilast= Basis%shell(Ishell)%lastSHL
      Jlast= Basis%shell(Jshell)%lastSHL
!
! Loop over Iatmshl
      do Iatmshl=Ifrst,Ilast
      XA=CARTESIAN(Basis%atmshl(Iatmshl)%ATMLST)%X
      YA=CARTESIAN(Basis%atmshl(Iatmshl)%ATMLST)%Y
      ZA=CARTESIAN(Basis%atmshl(Iatmshl)%ATMLST)%Z
      Iaos=Basis%atmshl(Iatmshl)%frstAO-1
!
! Loop over Jatmshl
      if(Ishell.EQ.Jshell)Jlast=Iatmshl
      do Jatmshl=Jfrst,Jlast
      XB=CARTESIAN(Basis%atmshl(Jatmshl)%ATMLST)%X
      YB=CARTESIAN(Basis%atmshl(Jatmshl)%ATMLST)%Y
      ZB=CARTESIAN(Basis%atmshl(Jatmshl)%ATMLST)%Z
      Jaos=Basis%atmshl(Jatmshl)%frstAO-1
!
      LPMAX=LAMAX+LBMAX-1
      LIM1DS=(LPMAX+3)/2
      LENTQ=Irange*Jrange
      NZERO=(LAMAX+LBMAX-2)/2+1
      ABX=XB-XA
      ABY=YB-YA
      ABZ=ZB-ZA
      RABSQ=ABX*ABX+ABY*ABY+ABZ*ABZ
!
      EEPV(1:LENTQ)=ZERO
!
! Loop over primitive Gaussians.
      do Igauss=IGBGN,IGEND
      AS=Basis%gaussian(Igauss)%exp
      TWOASQ=TWO*AS*AS
      ASXA=AS*XA
      ASYA=AS*YA
      ASZA=AS*ZA
      ARABSQ=AS*RABSQ
      call FILLCC (LAMAX, Basis%gaussian(Igauss)%CONTRC, CA)

      do Jgauss=JGBGN,JGEND
      BS=Basis%gaussian(Jgauss)%exp
      call FILLCC (LBMAX, Basis%gaussian(Jgauss)%CONTRC, CB)
      EP=AS+BS
      EPI=ONE/EP
      EPIO2=PT5*EPI
      TWOP=EP+EP
      ARG=-BS*ARABSQ*EPI
      PEXP=ZERO
      if(ARG.le.CUT1)cycle
      PEXP=DEXP(ARG)
!     if(ARG.GT.CUT1)PEXP=DEXP(ARG)
      ZTEMP=TWOPI*EPI*PEXP
      PX=(ASXA+BS*XB)*EPI
      PY=(ASYA+BS*YB)*EPI
      PZ=(ASZA+BS*ZB)*EPI
      XAP=PX-XA
      XBP=PX-XB
      YAP=PY-YA
      YBP=PY-YB
      ZAP=PZ-ZA
      ZBP=PZ-ZB
      call GETCC1 (CCX, XAP, XBP, LAMAX+2, LBMAX+2)
      call GETCC1 (CCY, YAP, YBP, LAMAX+2, LBMAX+2)
      call GETCC1 (CCZ, ZAP, ZBP, LAMAX+2, LBMAX+2)
!
! Zero accumulation area.
      EEPB(1:LENTQ)=ZERO

      ZT=ZTEMP
      PCX=XC-PX
      PCY=YC-PY
      PCZ=ZC-PZ
      RPCSQ=PCX*PCX+PCY*PCY+PCZ*PCZ
      ARG=EP*RPCSQ
      if(ARG.gt.CUT2)cycle
      call RPOLX (NZERO, ARG, TP, WP)
      call GETA1 (A, EPIO2, 0, LPMAX)
!
! Loop over zeroes of Rys polynomial.
      do IZERO=1,NZERO
      TWOPT2=TWOP*TP(IZERO)
      ZCONST=ZT*WP(IZERO)
      call GET2C (TWOCX, PCX, ONE, A, TWOPT2, 0, LPMAX)
      call GET2C (TWOCY, PCY, ONE, A, TWOPT2, 0, LPMAX)
      call GET2C (TWOCZ, PCZ, ZCONST, A, TWOPT2, 0, LPMAX)
      call GET3C (XIP, 0, TWOCX, CCX, LAMAX, LBMAX)
      call GET3C (YIP, 0, TWOCY, CCY, LAMAX, LBMAX)
      call GET3C (ZIP, 0, TWOCZ, CCZ, LAMAX, LBMAX)
!
! Loop over atomic orbitals.
      INTC=0
      do I=Istart,Iend
      IX=INDIX(I)
      IY=INDIY(I)
      IZ=INDIZ(I)
!
      do J=Jstart,Jend
      JX=INDJX(J)
      JY=INDJY(J)
      JZ=INDJZ(J)
      INTC=INTC+1
      EEPB(INTC)=EEPB(INTC)+XIP(IX+JX)*YIP(IY+JY)*ZIP(IZ+JZ)
      end do ! J
      end do ! I
! End of AO loop.
!
      end do ! IZERO
! End of loop over Rys zeroes.
!
! Apply the contraction coefficients.
! The potential integrals, is in EEPB
      INTC=0
      do I=Istart,Iend
      do J=Jstart,Jend
      INTC=INTC+1
      COEF=CA(I)*CB(J)
      EEPV(INTC)=EEPV(INTC)-EEPB(INTC)*COEF          ! new
      end do ! J
      end do ! I
!
      end do ! Jgauss
      end do ! Igauss
! End of loop over Gaussians.
!
! FILMAT takes the integrals in EEPV and stores them in their proper places in V12dr2.
      call FILMAT (EEPV, V12dr2, MATlen, Iend, Jend, &
                         Iatmshl, Jatmshl, Irange, Jrange, Iaos, Jaos)
!
      end do ! Jatmshl
      end do ! Iatmshl
      end do ! Jshell
      end do ! Ishell
! End of loop over shells.
!
! End of routine I1E_Exch
      call PRG_manager ('exit', 'I1E_Exch', 'UTILITY')
      return
      end subroutine I1E_Exch
