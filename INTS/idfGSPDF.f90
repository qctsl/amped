      subroutine I2E_SHLn
!***********************************************************************
!     Date last modified: July 22, 1998                     Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version.                                       *
!     Two-electron integral output routine.                            *
!     The two-electron integrals are sorted into type and written on   *
!     disk if the buffer is full.  Some integrals are always kept in   *
!     core                                                             *
!***********************************************************************
! Modules:
      USE mod_idfclc

      implicit none
!
! Local scalars:
!
! Begin:
! First load integrals to the buffers, if the INCORE are full process them
      if(Mtype.eq.1)then
        call I2E_ijkl_buffers ! Special case (fast routine)
      else
        call I2E_shell_buffers
      end if
!
! End of routine I2E_SHLn
      return
      end subroutine I2E_SHLn
      subroutine I2E_ijkl_buffers
!***********************************************************************
!     Date last modified: July 22, 1998                    Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version.                                       *
!     Two-electron integral storage routine (ijkl integrals only.      *
!     The two-electron integrals are stored in buffers (_buff).        *
!     When the buffers (_buff) are full they are: loaded to INCORE or  *
!     put ONDISK or processed (DIRECT)                                 *
!***********************************************************************
! Modules:
      USE mod_idfclc
      USE QM_objects
      USE INT_objects

      implicit none
!
! Local scalars:
      integer I,J,K,L,IJKLix,ILJKix,IKJLix,JKRNG,JLRNG,JKLRNG,KLRNG,NUMINT
      integer :: Iao,Jao,Kao,Lao
!
! Begin:
! Determine numbers of the first atomic orbitals in each of the four shells.
      IJKLix=0
      KLRNG=Krange*Lrange
      JKRNG=Jrange*Krange
      JLRNG=Jrange*Lrange
      JKLRNG=Jrange*KLRNG
!
! Loop over set of integrals defined by shells Ishell, Jshell, etc.
      do I=1,Irange
      do J=1,Jrange
      do K=1,Krange
      do L=1,Lrange
      IJKLix=IJKLix+1
      NUMINT=0
      IKJLix=(I-1)*JKLRNG+(K-1)*JLRNG+(J-1)*Lrange+L
      ILJKix=(I-1)*JKLRNG+(L-1)*JKRNG+(J-1)*Krange+K
      IF(DABS(IJKLS(IJKLix)).GT.I2E_ACC)then
        NUMINT=NUMINT+1
      else
        IJKLS(IJKLix)=ZERO
      end if

      IF(DABS(ILJKS(ILJKix)).GT.I2E_ACC)then
        NUMINT=NUMINT+1
      else
        ILJKS(ILJKix)=ZERO
      end if

      IF(DABS(IKJLS(IKJLix)).GT.I2E_ACC)then
        NUMINT=NUMINT+1
      else
        IKJLS(IKJLix)=ZERO
      end if
!
      IF(NUMINT.NE.0) then
! Integral over atomic orbitals (IJ/KL).
      Iao=AOI+I
      Jao=AOJ+J
      Kao=AOK+K
      Lao=AOL+L
!
! Atomic orbitals are such that  I GE J GE K GE L
      NTINT(8)=NTINT(8)+NUMINT
      IJKL_count=IJKL_count+1
      IF(IJKL_count.gt.IJKL_MAX)then ! Buffer is full, dump it to disk or process it
        if(LDirect_SCF)then
          Lall_INCORE=.false.
          call BLD_DGR_ijkl (IJKL_INCORE, IJKL_MAX, PM0, DGM)
        else ! put on disk
          if(integral_combinations)call I2E_COMB_ijkl (IJKL_INCORE, IJKL_MAX) ! Create combinations
          IJKL_Nbuffers=IJKL_Nbuffers+1
          call WRI_ijkl (IJKL_INCORE, IJKL_MAX, IJKL_unit, IJKL_MAX)
          Lijkl_ONDISK=.TRUE.
          ANY_ONDISK=.TRUE.
        end if ! LDirect_SCF
        IJKL_count=1
      end if ! IJKL_count.gt.IJKL_MAX
      IJKL_INCORE(IJKL_count)%I=Iao
      IJKL_INCORE(IJKL_count)%J=Jao
      IJKL_INCORE(IJKL_count)%K=Kao
      IJKL_INCORE(IJKL_count)%L=Lao
      IJKL_INCORE(IJKL_count)%ijkl=IJKLS(IJKLix)
      IJKL_INCORE(IJKL_count)%ikjl=IKJLS(IKJLix)
      IJKL_INCORE(IJKL_count)%iljk=ILJKS(ILJKix)
!
      end if ! NUMINT.NE.0

      end do ! L
      end do ! K
      end do ! J
      end do ! I
!
! End of routine I2E_ijkl_buffers
      return
      end subroutine I2E_ijkl_buffers
      subroutine I2E_shell_buffers
!***********************************************************************
!     Date last modified: July 22, 1998                     Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version.                                       *
!     Two-electron integral output routine.                            *
!     The two-electron integrals are sorted into type and written on   *
!     disk if the buffer is full.  Some integrals are always kept in   *
!     core                                                             *
!***********************************************************************
! Modules:
      USE mod_idfclc
      USE QM_objects
      USE INT_objects

      implicit none
!
! Local scalars:
      integer :: I,J,K,L,NCNT1,NUMINT
      integer :: Iao,Jao,Kao,Lao
!
! Begin:
! Determine numbers of the first atomic orbitals in each of the four shells.
      NCNT1=0
!
! Loop over set of integrals defined by shells Ishell, Jshell, etc.
      do I=1,Irange
      do J=1,Jrange
      do K=1,Krange
      do L=1,Lrange
      NCNT1=NCNT1+1
      NUMINT=0

      IF(DABS(IJKLS(NCNT1)).GT.I2E_ACC)then
        NUMINT=NUMINT+1
      else
        IJKLS(NCNT1)=ZERO
      end if

      IF(DABS(ILJKS(NCNT1)).GT.I2E_ACC)then
        NUMINT=NUMINT+1
      else
        ILJKS(NCNT1)=ZERO
      end if

      IF(DABS(IKJLS(NCNT1)).GT.I2E_ACC)then
        NUMINT=NUMINT+1
      else
        IKJLS(NCNT1)=ZERO
      end if
!
      IF(NUMINT.NE.0) then
!
! Integral over atomic orbitals (IJ/KL).
      Iao=AOI+I
      Jao=AOJ+J
      Kao=AOK+K
      Lao=AOL+L
!
! Atomic orbitals are such that  I GE J GE K GE L
! The following code determines the integral type
      IF(Iao.GT.Jao)then
        IF(Jao.GT.Kao)then
          IF(Kao.GT.Lao)then
! No coincidences - Type 8 integral (IJ,KL).
            NTINT(8)=NTINT(8)+NUMINT
            IJKL_count=IJKL_count+1
            IF(IJKL_count.gt.IJKL_MAX)then ! Buffer is full, dump it to disk or process it
              if(LDirect_SCF)then
                Lall_INCORE=.false.
                call BLD_DGR_ijkl (IJKL_INCORE, IJKL_MAX, PM0, DGM)
              else ! put on disk
                if(integral_combinations)call I2E_COMB_ijkl (IJKL_INCORE, IJKL_MAX) ! Create combinations
                IJKL_Nbuffers=IJKL_Nbuffers+1
                call WRI_ijkl (IJKL_INCORE, IJKL_MAX, IJKL_unit, IJKL_MAX)
                Lijkl_ONDISK=.TRUE.
                ANY_ONDISK=.TRUE.
              end if ! LDirect_SCF
              IJKL_count=1
            end if ! IJKL_count.gt.IJKL_MAX
            IJKL_INCORE(IJKL_count)%I=Iao
            IJKL_INCORE(IJKL_count)%J=Jao
            IJKL_INCORE(IJKL_count)%K=Kao
            IJKL_INCORE(IJKL_count)%L=Lao
            IJKL_INCORE(IJKL_count)%ijkl=IJKLS(NCNT1)
            IJKL_INCORE(IJKL_count)%ikjl=ILJKS(NCNT1)
            IJKL_INCORE(IJKL_count)%iljk=IKJLS(NCNT1)
!
          else ! Kao=Lao
!
! (IJ,KK) converted to (IK,JK) - type 2
            NTINT(2)=NTINT(2)+NUMINT
            IJKJ_count=IJKJ_count+1
            IF(IJKJ_count.gt.IJKJ_MAX)then
              if(LDirect_SCF)then
                Lall_INCORE=.false.
                call BLD_DGR_ijkj (IJKJ_INCORE, IJKJ_MAX, PM0, DGM)
              else
                if(integral_combinations)call I2E_COMB_ijkj (IJKJ_INCORE, IJKJ_MAX) ! Create combinations
                IJKJ_Nbuffers=IJKJ_Nbuffers+1
                call WRI_ijkj (IJKJ_INCORE, IJKJ_MAX, IJKJ_unit, IJKJ_MAX)
                Lijkj_ONDISK=.TRUE.
                ANY_ONDISK=.TRUE.
              end if
              IJKJ_count=1
            end if ! IJKJ_count.gt.IJKJ_MAX
            IJKJ_INCORE(IJKJ_count)%I=Iao
            IJKJ_INCORE(IJKJ_count)%J=Kao
            IJKJ_INCORE(IJKJ_count)%K=Jao
            IJKJ_INCORE(IJKJ_count)%ijkj=ILJKS(NCNT1)
            IJKJ_INCORE(IJKJ_count)%ikjj=IJKLS(NCNT1)

          end if ! Kao.GT.Lao

        else if(Kao.GT.Lao)then
!
! (IJ,JL) All INCORE
          NTINT(3)=NTINT(3)+NUMINT
          IJJL_count=IJJL_count+1
          IF(IJJL_count.gt.IJJL_MAX)then
            if(LDirect_SCF)then
              Lall_INCORE=.false.
              call BLD_DGR_ijjl (IJJL_INCORE, IJJL_MAX, PM0, DGM)
            else
              if(integral_combinations)call I2E_COMB_ijjl (IJJL_INCORE, IJJL_MAX) ! Create combinations
              IJJL_Nbuffers=IJJL_Nbuffers+1
              call WRI_ijjl (IJJL_INCORE, IJJL_MAX, IJJL_unit, IJJL_MAX)
              Lijjl_ONDISK=.TRUE.
              ANY_ONDISK=.TRUE.
            end if
            IJJL_count=1
          end if ! IJJL_count.gt.IJJL_MAX
          IJJL_INCORE(IJJL_count)%I=Iao
          IJJL_INCORE(IJJL_count)%J=Jao
          IJJL_INCORE(IJJL_count)%L=Lao
          IJJL_INCORE(IJJL_count)%ijjl=IJKLS(NCNT1)
          IJJL_INCORE(IJJL_count)%iljj=ILJKS(NCNT1)

        else ! K=L
!
! (IJ,JJ) All INCORE
          IF(LSAVE_ijjj)then
          NTINT(5)=NTINT(5)+NUMINT
          IJJJ_count=IJJJ_count+1
          IF(IJJJ_count.LE.IJJJ_MAX)then
            IJJJ_INCORE(IJJJ_count)%I=Iao
            IJJJ_INCORE(IJJJ_count)%J=Jao
            IJJJ_INCORE(IJJJ_count)%ijjj=IJKLS(NCNT1)
          else
            write(UNIout,'(a)') &
            'ERROR> I2E_shell_buffers: too many IJJJ integrals'
            stop 'ERROR> I2E_shell_buffers: too many IJJJ integrals'
          end if
        end if ! Jao.GT.Kao
        end if ! LSAVE_ijjj
!
      else if(Jao.GT.Kao)then
        IF(Kao.GT.Lao)then
!
! (II,KL).
          NTINT(1)=NTINT(1)+NUMINT
          IIKL_count=IIKL_count+1
          IF(IIKL_count.gt.IIKL_MAX)then
            if(LDirect_SCF)then
              Lall_INCORE=.false.
              call BLD_DGR_iikl (IIKL_INCORE, IIKL_MAX, PM0, DGM)
            else
              if(integral_combinations)call I2E_COMB_iikl (IIKL_INCORE, IIKL_MAX) ! Create combinations
              IIKL_Nbuffers=IIKL_Nbuffers+1
              call WRI_iikl (IIKL_INCORE, IIKL_MAX, IIKL_unit, IIKL_MAX)
              Liikl_ONDISK=.TRUE.
              ANY_ONDISK=.TRUE.
            end if
            IIKL_count=1
          end if ! IIKL_count.gt.IIKL_MAX
          IIKL_INCORE(IIKL_count)%I=Iao
          IIKL_INCORE(IIKL_count)%K=Kao
          IIKL_INCORE(IIKL_count)%L=Lao
          IIKL_INCORE(IIKL_count)%iikl=IJKLS(NCNT1)
          IIKL_INCORE(IIKL_count)%ikil=ILJKS(NCNT1)
!
        else ! Kao=Lao
!
! (II,KK) All INCORE
        if(LSAVE_IIKK)then ! Prevent from storing them again for a Direct SCF
          NTINT(4)=NTINT(4)+NUMINT
          IIKK_count=IIKK_count+1
          IF(IIKK_count.LE.IIKK_MAX)then
            IIKK_INCORE(IIKK_count)%I=Iao
            IIKK_INCORE(IIKK_count)%K=Kao
            IIKK_INCORE(IIKK_count)%iikk=IJKLS(NCNT1)
            IIKK_INCORE(IIKK_count)%ikik=ILJKS(NCNT1)
          else
            write(UNIout,'(a)') &
            'ERROR> I2E_shell_buffers: too many IIKK integrals'
            stop 'ERROR> I2E_shell_buffers: too many IIKK integrals'
          end if
        end if ! LSAVE_IIKK

        end if ! Kao.GT.Lao
!
      else if(Kao.GT.Lao)then
!
! (II,IL) All INCORE
        if(LSAVE_IIIL)then ! Prevent from storing them again for a Direct SCF
        NTINT(6)=NTINT(6)+NUMINT
        IIIL_count=IIIL_count+1
        IF(IIIL_count.LE.IIIL_MAX)then
          IIIL_INCORE(IIIL_count)%I=Iao
          IIIL_INCORE(IIIL_count)%L=Lao
          IIIL_INCORE(IIIL_count)%iiil=IJKLS(NCNT1)
        else
          write(UNIout,'(a)') &
          'ERROR> I2E_shell_buffers: too many IIIL integrals'
          stop 'ERROR> I2E_shell_buffers: too many IIIL integrals'
        end if
        end if ! LSAVE_IIIL
!
      else ! Kao=Lao
!
! (II,II) All INCORE
        if(LSAVE_IIII)then ! Prevent from storing them again for a Direct SCF
        NTINT(7)=NTINT(7)+NUMINT
        IIII_count=IIII_count+1
        IF(IIII_count.LE.IIII_MAX)then
          IIII_INCORE(IIII_count)%I=Iao
          IIII_INCORE(IIII_count)%iiii=IJKLS(NCNT1)
        else
          write(UNIout,'(a)') &
          'ERROR> I2E_shell_buffers too many IIII integrals'
          stop 'ERROR> I2E_shell_buffers: too many IIII integrals'
        end if
        end if ! LSAVE_IIII
      end if ! Iao.GT.Jao

      end if ! NUMINT.NE.0

      end do ! L
      end do ! K
      end do ! J
      end do ! I
!
! End of routine I2E_shell_buffers
      return
      end subroutine I2E_shell_buffers
      subroutine DEF_shells (JatmshlIN, KatmshlIN, LatmshlIN, ICase)
!***********************************************************************
!     Date last modified: January 25, 2006                 Version 2.0 *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!***********************************************************************
! Modules:
      USE mod_idfclc

      implicit none

      integer, intent(IN) :: JatmshlIN,KatmshlIN,LatmshlIN,ICase
!
! Local scalars:
      integer :: Jrange0,Krange0,Lrange0
!
      if(ICase.eq.1)then
        LBmax=LBMAX0
        LCmax=LCmax0
        LDmax=LDmax0
        Jstart=Jstart0
        Kstart=Kstart0
        Lstart=Lstart0
        Jend=Jend0
        Kend=Kend0
        Lend=Lend0
        Jrange=Jend-Jstart+1
        Krange=Kend-Kstart+1
        Lrange=Lend-Lstart+1
        NgaussB=NgaussB0
        NgaussC=NgaussC0
        NgaussD=NgaussD0
        GexpB(1:NgaussB)=GexpB0(1:NgaussB)
        GccB(1:NgaussB*Jrange)=GccB0(1:NgaussB*Jrange)
        GexpC(1:NgaussC)=GexpC0(1:NgaussC)
        GccC(1:NgaussC*Krange)=GccC0(1:NgaussC*Krange)
        GexpD(1:NgaussD)=GexpD0(1:NgaussD)
        GccD(1:NgaussD*Lrange)=GccD0(1:NgaussD*Lrange)
        EABPmin=EABPmin0
        ECDPmin=ECDPmin0
        Jatom=Jatom0
        Katom=Katom0
        Latom=Latom0
      else if(ICase.eq.2)then
        LBmax=LDMAX0
        LCmax=LBmax0
        LDmax=LCmax0
        Jstart=Lstart0
        Kstart=Jstart0
        Lstart=Kstart0
        Jend=Lend0
        Kend=Jend0
        Lend=Kend0
        NgaussB=NgaussD0
        NgaussC=NgaussB0
        NgaussD=NgaussC0
        Jrange=Jend-Jstart+1
        Krange=Kend-Kstart+1
        Lrange=Lend-Lstart+1
        Jrange0=Jend0-Jstart0+1
        Krange0=Kend0-Kstart0+1
        Lrange0=Lend0-Lstart0+1
        GexpB(1:NgaussB)=GexpD0(1:NgaussD0)
        GccB(1:NgaussB*Jrange)=GccD0(1:NgaussD0*Lrange0)
        GexpC(1:NgaussC)=GexpB0(1:NgaussB0)
        GccC(1:NgaussC*Krange)=GccB0(1:NgaussB0*Jrange0)
        GexpD(1:NgaussD)=GexpC0(1:NgaussC0)
        GccD(1:NgaussD*Lrange)=GccC0(1:NgaussC0*Krange0)
        EABPmin=EADPmin
        ECDPmin=EBCPmin
        Jatom=Latom0
        Katom=Jatom0
        Latom=Katom0
      else if(ICase.eq.3)then
        LBmax=LCMAX0
        LCmax=LBmax0
        LDmax=LDmax0
        Jstart=Kstart0
        Kstart=Jstart0
        Lstart=Lstart0
        Jend=Kend0
        Kend=Jend0
        Lend=Lend0
        NgaussB=NgaussC0
        NgaussC=NgaussB0
        NgaussD=NgaussD0
        Jrange=Jend-Jstart+1
        Krange=Kend-Kstart+1
        Lrange=Lend-Lstart+1
        Jrange0=Jend0-Jstart0+1
        Krange0=Kend0-Kstart0+1
        Lrange0=Lend0-Lstart0+1
        GexpB(1:NgaussB)=GexpC0(1:NgaussC0)
        GccB(1:NgaussB*Jrange)=GccC0(1:NgaussC0*Krange0)
        GexpC(1:NgaussC)=GexpB0(1:NgaussB0)
        GccC(1:NgaussC*Krange)=GccB0(1:NgaussB0*Jrange0)
        GexpD(1:NgaussD)=GexpD0(1:NgaussD0)
        GccD(1:NgaussD*Lrange)=GccD0(1:NgaussD0*Lrange0)
        EABPmin=EACPmin
        ECDPmin=EBDPmin
        Jatom=Katom0
        Katom=Jatom0
        Latom=Latom0
      end if

      LABMAX=LAMAX+LBMAX-1
      LCDMAX=LCMAX+LDMAX-1
      LPQMAX=LABMAX+LCDMAX-1
!Jloop______________________________________________
      RABSQ=SHRABSQ(Iatom,Jatom)
      ABEXP=RABSQ*EABPmin
      XB=CARTESIAN(Jatom)%X
      YB=CARTESIAN(Jatom)%Y
      ZB=CARTESIAN(Jatom)%Z
!Kloop______________________________________________
      XC=CARTESIAN(Katom)%X
      YC=CARTESIAN(Katom)%Y
      ZC=CARTESIAN(Katom)%Z
!Lloop______________________________________________
      FIRST_loop=.false.
      RCDSQ=SHRABSQ(Katom,Latom)
      CDEXP=RCDSQ*ECDPmin
!
      XD=CARTESIAN(Latom)%X
      YD=CARTESIAN(Latom)%Y
      ZD=CARTESIAN(Latom)%Z
!
      LRABCD=.false.
      LRAB=.false.
      LRCD=.false.
      if(RABSQ.NE.ZERO.and.RCDSQ.NE.ZERO)LRABCD=.true.
      if(RABSQ.NE.ZERO)LRAB=.true.
      if(RCDSQ.NE.ZERO)LRCD=.true.
      Iatmshl_EQ_Jatmshl=.FALSE.
      IF(Iatmshl.EQ.JatmshlIN)Iatmshl_EQ_Jatmshl=.TRUE.
      Iatmshl_EQ_Katmshl=.FALSE.
      IF(Iatmshl.EQ.KatmshlIN)Iatmshl_EQ_Katmshl=.TRUE.
      Katmshl_EQ_Latmshl=.FALSE.
      IF(KatmshlIN.EQ.LatmshlIN)Katmshl_EQ_Latmshl=.TRUE.
      IJIJ=.FALSE.
      IF(Iatmshl_EQ_Katmshl.and.JatmshlIN.EQ.LatmshlIN)IJIJ=.TRUE.
      IIKK=.FALSE.
      IF(Iatmshl_EQ_Jatmshl.and.Katmshl_EQ_Latmshl)IIKK=.TRUE.
      Jatmshl_EQ_Latmshl=.FALSE.
      IF(JatmshlIN.EQ.LatmshlIN)Jatmshl_EQ_Latmshl=.TRUE.

      return
      end subroutine DEF_shells
      subroutine I2ER_DDDD
!***********************************************************************
!     Date last modified: September 23, 1996               Version 2.0 *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!***********************************************************************
! Modules:
      USE mod_idfclc
!
      implicit none
!
! Local Scalars
      integer I
!
      call PRG_manager ('enter', 'I2ER_DDDD', 'UTILITY')

      include "start_atmshl_loop"

      case=1
      call DEF_shells (Jatmshl, Katmshl, Latmshl, Case)
      if((ABEXP+CDEXP).le.I2E_expcut)then
        Int_pointer=>IJKLs
        call I2ER_GDDDD
      end if

      if(LTWOINT)then
      case=2
      call DEF_shells (Latmshl, Jatmshl, Katmshl, Case)
      if((ABEXP+CDEXP).le.I2E_expcut)then
        Int_pointer=>ILJKs
        call I2ER_GDDDD
      end if
      end if

      if(Mtype.eq.1)then
      case=3
      call DEF_shells (Katmshl, Jatmshl, Latmshl, Case)
      if((ABEXP+CDEXP).le.I2E_expcut)then
        Int_pointer=>IKJLs
        call I2ER_GDDDD
      end if
      end if

      include "end_atmshl_loop"

! End of routine I2ER_DDDD
      call PRG_manager ('exit', 'I2ER_DDDD', 'UTILITY')
      return

CONTAINS !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine I2ER_GDDDD
!***********************************************************************
!     Date last modified: September 23, 1996               Version 2.0 *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!***********************************************************************
! Modules:
!
      implicit none
!
! Local Scalars
      integer :: I,COR_INDEX
      integer :: IZERO,INTC,IV,JendM,KendM,LendM,Igauss,Jgauss,Kgauss,Lgauss
      integer :: IX,IY,IZ,JX,JY,JZ,KX,KY,KZ,LX,LY,LZ
      integer :: Iao,Jao,Kao,Lao
      double precision :: CC1,CC2,CC3
!
! Local Arrays
      double precision :: XINT(7)
      double precision :: A2DF(174),CCPX(48),CCPY(48),CCPZ(48),CCQX(48),CCQY(48),CCQZ(48)
      double precision :: G2DFX(13),G2DFY(13),G2DFZ(13),XIP(256),YIP(256),ZIP(256)
      double precision :: AS,BS,CS,DS
      double precision :: RHO,TWORHO,DXYZ,ZTEMP,RHOT2,ZCONST
      double precision :: EPAB,EPABI,EPABA,EPABB,EQCD,EQCDI,EQCDC,EQCDD,EABCD,EP2I,EABCDI
      double precision :: XAP,YAP,ZAP,XBP,YBP,ZBP,XCQ,YCQ,ZCQ,XDQ,YDQ,ZDQ,PQX,PQY,PQZ,RPQSQ
      integer INDIX(6),INDIY(6),INDIZ(6),INDJX(6),INDJY(6),INDJZ(6),INDKX(6),INDKY(6),INDKZ(6),INDLX(6), &
              INDLY(6),INDLZ(6)
      DATA INDIX/128,0,0,64,64,0/,INDIY/0,128,0,64,0,64/,INDIZ/0,0,128,0,64,64/, &
           INDJX/32,0,0,16,16,0/,INDJY/0,32,0,16,0,16/,INDJZ/0,0,32,0,16,16/, &
           INDKX/8,0,0,4,4,0/,INDKY/0,8,0,4,0,4/,INDKZ/0,0,8,0,4,4/, &
           INDLX/3,1,1,2,2,1/,INDLY/1,3,1,2,1,2/,INDLZ/1,1,3,1,2,2/, &
           XINT/1.0D0,2.0D0,3.0D0,4.0D0,5.0D0,6.0D0,7.0D0/
!
! Begin:
! Clear the contracted function integral array TQ.
      A2DF(1:174)=ZERO
!
      CCPX(1:48)=ZERO
      CCPY(1:48)=ZERO
      CCPZ(1:48)=ZERO
      CCQX(1:48)=ZERO
      CCQY(1:48)=ZERO
      CCQZ(1:48)=ZERO
!
! Commence loops over Gaussian expansion.
      include 'mungauss_gaussian_AB'
      if(LRAB)then
      EPABA=AS*EPABI
      EPABB=BS*EPABI
      XAP= EPABB*(XB-XA)
      YAP= EPABB*(YB-YA)
      ZAP= EPABB*(ZB-ZA)
      XBP=-EPABA*(XB-XA)
      YBP=-EPABA*(YB-YA)
      ZBP=-EPABA*(ZB-ZA)
      call GETCC_XYZ (CCPX, CCPY, CCPZ, XAP, YAP, ZAP, XBP, YBP, ZBP, LAMAX, LBMAX)
      end if

      include 'mungauss_gaussian_CD'
      TQprim(1:1296)=ZERO

      IF(EXPARG.LE.I2E_expcut)then
      if(LRCD)then
      EQCDC=CS*EQCDI
      EQCDD=DS*EQCDI
      XCQ= EQCDD*(XD-XC)
      YCQ= EQCDD*(YD-YC)
      ZCQ= EQCDD*(ZD-ZC)
      XDQ=-EQCDC*(XD-XC)
      YDQ=-EQCDC*(YD-YC)
      ZDQ=-EQCDC*(ZD-ZC)
      call GETCC_XYZ (CCQX, CCQY, CCQZ, XCQ, YCQ, ZCQ, XDQ, YDQ, ZDQ, LCMAX, LDMAX)
      end if
!
      PQX=(CS*XC+DS*XD)*EQCDI-(AS*XA+BS*XB)*EPABI
      PQY=(CS*YC+DS*YD)*EQCDI-(AS*YA+BS*YB)*EPABI
      PQZ=(CS*ZC+DS*ZD)*EQCDI-(AS*ZA+BS*ZB)*EPABI

      RPQSQ=PQX*PQX+PQY*PQY+PQZ*PQZ
!
      EABCD=EPAB*EQCD
      EABCDI=ONE/(EPAB+EQCD)
      RHO=EABCD*EABCDI
      ZTEMP=PIconst*DEXP(-EXPARG)*DSQRT(EABCDI)/EABCD
      EP2I=ONE/(EPAB+EPAB)
      call IJKLA2 (A2DF, LABMAX, LCDMAX, EQCD, EP2I)
!
      DXYZ=RHO*RPQSQ
      call RPOLX (NZERO, DXYZ, TP, WP)
      TWORHO=RHO+RHO

      do IZERO=1,NZERO
      RHOT2=TWORHO*TP(IZERO)
      ZCONST=ZTEMP*WP(IZERO)
      if(ZCONST.le.I2E_PQCUT2)cycle
      G2DFX(1)=ONE
      G2DFY(1)=ONE
      G2DFZ(1)=ZCONST
      XIP(1)=ONE
      YIP(1)=ONE
      ZIP(1)=ZCONST
!
      G2DFX(2)=RHOT2*PQX
      G2DFY(2)=RHOT2*PQY
      G2DFZ(2)=RHOT2*PQZ*ZCONST
!
      do IV=3,LPQMAX
        G2DFX(IV)=RHOT2*(PQX*G2DFX(IV-1)-XINT(IV-2)*G2DFX(IV-2))
        G2DFY(IV)=RHOT2*(PQY*G2DFY(IV-1)-XINT(IV-2)*G2DFY(IV-2))
        G2DFZ(IV)=RHOT2*(PQZ*G2DFZ(IV-1)-XINT(IV-2)*G2DFZ(IV-2))
      end do
!
! Find two-center integrals.
      if(LRABCD)then
        call DDDD4C (A2DF, G2DFX, CCQX, CCPX, XIP)
        call DDDD4C (A2DF, G2DFY, CCQY, CCPY, YIP)
        call DDDD4C (A2DF, G2DFZ, CCQZ, CCPZ, ZIP)
      else if(LRCD)then
        call DDDD3CQ (A2DF, G2DFX, CCQX, XIP)
        call DDDD3CQ (A2DF, G2DFY, CCQY, YIP)
        call DDDD3CQ (A2DF, G2DFZ, CCQZ, ZIP)
      else if(LRAB)then
        call DDDD3CP (A2DF, G2DFX, CCPX, XIP)
        call DDDD3CP (A2DF, G2DFY, CCPY, YIP)
        call DDDD3CP (A2DF, G2DFZ, CCPZ, ZIP)
      else
        call DDDD2C (A2DF, G2DFX, XIP)
        call DDDD2C (A2DF, G2DFY, YIP)
        call DDDD2C (A2DF, G2DFZ, ZIP)
      end if ! LRABCD
!
! Commence loop over atomic orbitals.
      INTC=0
      JendM=6
      KendM=6
      do Iao=1,6
        IF(Iatmshl_EQ_Jatmshl)JendM=Iao
        IF(IJIJ)KendM=Iao
        IX=INDIX(Iao)
        IY=INDIY(Iao)
        IZ=INDIZ(Iao)
      do Jao=1,JendM
        JX=INDJX(Jao)+IX
        JY=INDJY(Jao)+IY
        JZ=INDJZ(Jao)+IZ
      do Kao=1,KendM
        LendM=6
        IF(Katmshl_EQ_Latmshl)LendM=Kao
        IF(IJIJ.and.Iao.EQ.Kao)LendM=Jao
        KX=INDKX(Kao)+JX
        KY=INDKY(Kao)+JY
        KZ=INDKZ(Kao)+JZ
      do Lao=1,LendM
        LX=INDLX(Lao)+KX
        LY=INDLY(Lao)+KY
        LZ=INDLZ(Lao)+KZ
        INTC=INTC+1
        TQprim(INTC)=TQprim(INTC)+(XIP(LX)*YIP(LY)*ZIP(LZ))
      end do ! L
      end do ! K
      end do ! J
      end do ! I
      end do ! IZERO

! Apply contraction coefficients.
      INTC=0
      COR_INDEX = 0
      JendM=6
      KendM=6
      do Iao=1,6
      CC1=GccA(CCA+Iao)
      IF(Iatmshl_EQ_Jatmshl)JendM=Iao
      IF(IJIJ)KendM=Iao
      do Jao=1,JendM
      CC2=CC1*GccB(CCB+Jao)
      do Kao=1,KendM
      CC3=CC2*GccC(CCC+Kao)
      LendM=6
      IF(Katmshl_EQ_Latmshl)LendM=Kao
      IF(IJIJ.and.Iao.EQ.Kao)LendM=Jao
      do Lao=1,LendM
        INTC=INTC+1
        COR_INDEX =(Iao-1)*216 + (Jao-1)*36 + (Kao-1)*6 + Lao
        Int_pointer(COR_INDEX)=Int_pointer(COR_INDEX)+TQprim(INTC)*CC3*GccD(CCD+Lao)
      end do ! Lao
      end do ! Kao
      end do ! Jao
      end do ! Iao

      end if ! EXPARG.LE.I2E_expcut
      CCD=CCD+6
      end do Dloop ! Lgauss
      CCC=CCC+6
      end do Cloop ! Kgauss
      CCB=CCB+6
      end do Bloop ! Jgauss
      CCA=CCA+6
      end do Aloop ! Igauss
! End of loop over gaussians

! End of routine I2ER_DDDD
      return
      end subroutine I2ER_GDDDD
      end subroutine I2ER_DDDD

      subroutine DDDD4C (A2,         & ! Input
                         G2,         & ! Input
                         CCQ,        & ! Input
                         CCP,        & ! Input
                         WIP)       ! Output
!***********************************************************************
!     Date last modified: January 31, 1997                 Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!     Specialized version of DDDD4C                                    *
!								       *
!***********************************************************************
! Modules:

      implicit none
!
! Input arrays:
      double precision A2(174),CCQ(48),CCP(48),G2(13)
!
! Output array:
      double precision WIP(256)
!
! Local arrays:
      double precision I2P(33),I3P(75)
!
! Begin:
!
      I2P(33)=A2(92)*G2(1)+A2(93)*G2(3)+A2(94)*G2(5)+A2(95)*G2(7)+A2(96)*G2(9)
      I2P(32)=A2(88)*G2(2)+A2(89)*G2(4)+A2(90)*G2(6)+A2(91)*G2(8)
      I2P(31)=A2(84)*G2(1)+A2(85)*G2(3)+A2(86)*G2(5)+A2(87)*G2(7)
      I2P(30)=A2(81)*G2(2)+A2(82)*G2(4)+A2(83)*G2(6)
      I2P(29)=A2(78)*G2(1)+A2(79)*G2(3)+A2(80)*G2(5)
      I2P(26)=A2(65)*G2(2)+A2(66)*G2(4)+A2(67)*G2(6)+A2(68)*G2(8)
      I2P(25)=A2(62)*G2(3)+A2(63)*G2(5)+A2(64)*G2(7)
      I2P(24)=A2(59)*G2(2)+A2(60)*G2(4)+A2(61)*G2(6)
      I2P(23)=A2(57)*G2(3)+A2(58)*G2(5)
      I2P(22)=A2(55)*G2(2)+A2(56)*G2(4)
      I2P(19)=A2(42)*G2(1)+A2(43)*G2(3)+A2(44)*G2(5)+A2(45)*G2(7)
      I2P(18)=A2(39)*G2(2)+A2(40)*G2(4)+A2(41)*G2(6)
      I2P(17)=A2(36)*G2(1)+A2(37)*G2(3)+A2(38)*G2(5)
      I2P(16)=A2(34)*G2(2)+A2(35)*G2(4)
      I2P(15)=A2(32)*G2(1)+A2(33)*G2(3)
      I2P(12)=A2(22)*G2(2)+A2(23)*G2(4)+A2(24)*G2(6)
      I2P(11)=A2(20)*G2(3)+A2(21)*G2(5)
      I2P(10)=A2(18)*G2(2)+A2(19)*G2(4)
      I2P(9)=A2(17)*G2(3)
      I2P(8)=A2(16)*G2(2)
      I2P(5)=A2(6)*G2(1)+A2(7)*G2(3)+A2(8)*G2(5)
      I2P(4)=A2(4)*G2(2)+A2(5)*G2(4)
      I2P(3)=A2(2)*G2(1)+A2(3)*G2(3)
      I2P(2)=A2(1)*G2(2)
      I2P(1)=G2(1)

      I3P(75)=CCQ(22)*I2P(29)+CCQ(23)*I2P(30)+CCQ(24)*I2P(31)+CCQ(25)*I2P(32)+I2P(33)
      I3P(74)=CCQ(19)*I2P(29)+CCQ(20)*I2P(30)+CCQ(21)*I2P(31)+I2P(32)
      I3P(73)=CCQ(17)*I2P(29)+CCQ(18)*I2P(30)+I2P(31)
      I3P(71)=CCQ(10)*I2P(29)+CCQ(11)*I2P(30)+CCQ(12)*I2P(31)+I2P(32)
      I3P(70)=CCQ(8)*I2P(29)+CCQ(9)*I2P(30)+I2P(31)
      I3P(69)=CCQ(7)*I2P(29)+I2P(30)
      I3P(67)=CCQ(2)*I2P(29)+CCQ(3)*I2P(30)+I2P(31)
      I3P(66)=CCQ(1)*I2P(29)+I2P(30)

      I3P(59)=CCQ(22)*I2P(22)+CCQ(23)*I2P(23)+CCQ(24)*I2P(24)+CCQ(25)*I2P(25)+I2P(26)
      I3P(58)=CCQ(19)*I2P(22)+CCQ(20)*I2P(23)+CCQ(21)*I2P(24)+I2P(25)
      I3P(57)=CCQ(17)*I2P(22)+CCQ(18)*I2P(23)+I2P(24)
      I3P(55)=CCQ(10)*I2P(22)+CCQ(11)*I2P(23)+CCQ(12)*I2P(24)+I2P(25)
      I3P(54)=CCQ(8)*I2P(22)+CCQ(9)*I2P(23)+I2P(24)
      I3P(53)=CCQ(7)*I2P(22)+I2P(23)
      I3P(51)=CCQ(2)*I2P(22)+CCQ(3)*I2P(23)+I2P(24)
      I3P(50)=CCQ(1)*I2P(22)+I2P(23)

      I3P(43)=CCQ(22)*I2P(15)+CCQ(23)*I2P(16)+CCQ(24)*I2P(17)+CCQ(25)*I2P(18)+I2P(19)
      I3P(42)=CCQ(19)*I2P(15)+CCQ(20)*I2P(16)+CCQ(21)*I2P(17)+I2P(18)
      I3P(41)=CCQ(17)*I2P(15)+CCQ(18)*I2P(16)+I2P(17)
      I3P(39)=CCQ(10)*I2P(15)+CCQ(11)*I2P(16)+CCQ(12)*I2P(17)+I2P(18)
      I3P(38)=CCQ(8)*I2P(15)+CCQ(9)*I2P(16)+I2P(17)
      I3P(37)=CCQ(7)*I2P(15)+I2P(16)
      I3P(35)=CCQ(2)*I2P(15)+CCQ(3)*I2P(16)+I2P(17)
      I3P(34)=CCQ(1)*I2P(15)+I2P(16)

      I3P(27)=CCQ(22)*I2P(8)+CCQ(23)*I2P(9)+CCQ(24)*I2P(10)+CCQ(25)*I2P(11)+I2P(12)
      I3P(26)=CCQ(19)*I2P(8)+CCQ(20)*I2P(9)+CCQ(21)*I2P(10)+I2P(11)
      I3P(25)=CCQ(17)*I2P(8)+CCQ(18)*I2P(9)+I2P(10)
      I3P(23)=CCQ(10)*I2P(8)+CCQ(11)*I2P(9)+CCQ(12)*I2P(10)+I2P(11)
      I3P(22)=CCQ(8)*I2P(8)+CCQ(9)*I2P(9)+I2P(10)
      I3P(21)=CCQ(7)*I2P(8)+I2P(9)
      I3P(19)=CCQ(2)*I2P(8)+CCQ(3)*I2P(9)+I2P(10)
      I3P(18)=CCQ(1)*I2P(8)+I2P(9)

      I3P(11)=CCQ(22)*I2P(1)+CCQ(23)*I2P(2)+CCQ(24)*I2P(3)+CCQ(25)*I2P(4)+I2P(5)
      I3P(10)=CCQ(19)*I2P(1)+CCQ(20)*I2P(2)+CCQ(21)*I2P(3)+I2P(4)
      I3P(9)=CCQ(17)*I2P(1)+CCQ(18)*I2P(2)+I2P(3)
      I3P(7)=CCQ(10)*I2P(1)+CCQ(11)*I2P(2)+CCQ(12)*I2P(3)+I2P(4)
      I3P(6)=CCQ(8)*I2P(1)+CCQ(9)*I2P(2)+I2P(3)
      I3P(5)=CCQ(7)*I2P(1)+I2P(2)
      I3P(3)=CCQ(2)*I2P(1)+CCQ(3)*I2P(2)+I2P(3)
      I3P(2)=CCQ(1)*I2P(1)+I2P(2)

      WIP(171)=CCP(22)*I3P(11)+CCP(23)*I3P(27)+CCP(24)*I3P(43)+CCP(25)*I3P(59)+I3P(75)
      WIP(170)=CCP(22)*I3P(10)+CCP(23)*I3P(26)+CCP(24)*I3P(42)+CCP(25)*I3P(58)+I3P(74)
      WIP(169)=CCP(22)*I3P(9)+CCP(23)*I3P(25)+CCP(24)*I3P(41)+CCP(25)*I3P(57)+I3P(73)
      WIP(167)=CCP(22)*I3P(7)+CCP(23)*I3P(23)+CCP(24)*I3P(39)+CCP(25)*I3P(55)+I3P(71)
      WIP(166)=CCP(22)*I3P(6)+CCP(23)*I3P(22)+CCP(24)*I3P(38)+CCP(25)*I3P(54)+I3P(70)
      WIP(165)=CCP(22)*I3P(5)+CCP(23)*I3P(21)+CCP(24)*I3P(37)+CCP(25)*I3P(53)+I3P(69)
      WIP(163)=CCP(22)*I3P(3)+CCP(23)*I3P(19)+CCP(24)*I3P(35)+CCP(25)*I3P(51)+I3P(67)
      WIP(162)=CCP(22)*I3P(2)+CCP(23)*I3P(18)+CCP(24)*I3P(34)+CCP(25)*I3P(50)+I3P(66)
      WIP(161)=CCP(22)*I2P(1)+CCP(23)*I2P(8)+CCP(24)*I2P(15)+CCP(25)*I2P(22)+I2P(29)
      WIP(155)=CCP(19)*I3P(11)+CCP(20)*I3P(27)+CCP(21)*I3P(43)+I3P(59)
      WIP(154)=CCP(19)*I3P(10)+CCP(20)*I3P(26)+CCP(21)*I3P(42)+I3P(58)
      WIP(153)=CCP(19)*I3P(9)+CCP(20)*I3P(25)+CCP(21)*I3P(41)+I3P(57)
      WIP(151)=CCP(19)*I3P(7)+CCP(20)*I3P(23)+CCP(21)*I3P(39)+I3P(55)
      WIP(150)=CCP(19)*I3P(6)+CCP(20)*I3P(22)+CCP(21)*I3P(38)+I3P(54)
      WIP(149)=CCP(19)*I3P(5)+CCP(20)*I3P(21)+CCP(21)*I3P(37)+I3P(53)
      WIP(147)=CCP(19)*I3P(3)+CCP(20)*I3P(19)+CCP(21)*I3P(35)+I3P(51)
      WIP(146)=CCP(19)*I3P(2)+CCP(20)*I3P(18)+CCP(21)*I3P(34)+I3P(50)
      WIP(145)=CCP(19)*I2P(1)+CCP(20)*I2P(8)+CCP(21)*I2P(15)+I2P(22)
      WIP(139)=CCP(17)*I3P(11)+CCP(18)*I3P(27)+I3P(43)
      WIP(138)=CCP(17)*I3P(10)+CCP(18)*I3P(26)+I3P(42)
      WIP(137)=CCP(17)*I3P(9)+CCP(18)*I3P(25)+I3P(41)
      WIP(135)=CCP(17)*I3P(7)+CCP(18)*I3P(23)+I3P(39)
      WIP(134)=CCP(17)*I3P(6)+CCP(18)*I3P(22)+I3P(38)
      WIP(133)=CCP(17)*I3P(5)+CCP(18)*I3P(21)+I3P(37)
      WIP(131)=CCP(17)*I3P(3)+CCP(18)*I3P(19)+I3P(35)
      WIP(130)=CCP(17)*I3P(2)+CCP(18)*I3P(18)+I3P(34)
      WIP(129)=CCP(17)*I2P(1)+CCP(18)*I2P(8)+I2P(15)
      WIP(107)=CCP(10)*I3P(11)+CCP(11)*I3P(27)+CCP(12)*I3P(43)+I3P(59)
      WIP(106)=CCP(10)*I3P(10)+CCP(11)*I3P(26)+CCP(12)*I3P(42)+I3P(58)
      WIP(105)=CCP(10)*I3P(9)+CCP(11)*I3P(25)+CCP(12)*I3P(41)+I3P(57)
      WIP(103)=CCP(10)*I3P(7)+CCP(11)*I3P(23)+CCP(12)*I3P(39)+I3P(55)
      WIP(102)=CCP(10)*I3P(6)+CCP(11)*I3P(22)+CCP(12)*I3P(38)+I3P(54)
      WIP(101)=CCP(10)*I3P(5)+CCP(11)*I3P(21)+CCP(12)*I3P(37)+I3P(53)
      WIP(99)=CCP(10)*I3P(3)+CCP(11)*I3P(19)+CCP(12)*I3P(35)+I3P(51)
      WIP(98)=CCP(10)*I3P(2)+CCP(11)*I3P(18)+CCP(12)*I3P(34)+I3P(50)
      WIP(97)=CCP(10)*I2P(1)+CCP(11)*I2P(8)+CCP(12)*I2P(15)+I2P(22)
      WIP(91)=CCP(8)*I3P(11)+CCP(9)*I3P(27)+I3P(43)
      WIP(90)=CCP(8)*I3P(10)+CCP(9)*I3P(26)+I3P(42)
      WIP(89)=CCP(8)*I3P(9)+CCP(9)*I3P(25)+I3P(41)
      WIP(87)=CCP(8)*I3P(7)+CCP(9)*I3P(23)+I3P(39)
      WIP(86)=CCP(8)*I3P(6)+CCP(9)*I3P(22)+I3P(38)
      WIP(85)=CCP(8)*I3P(5)+CCP(9)*I3P(21)+I3P(37)
      WIP(83)=CCP(8)*I3P(3)+CCP(9)*I3P(19)+I3P(35)
      WIP(82)=CCP(8)*I3P(2)+CCP(9)*I3P(18)+I3P(34)
      WIP(81)=CCP(8)*I2P(1)+CCP(9)*I2P(8)+I2P(15)
      WIP(75)=CCP(7)*I3P(11)+I3P(27)
      WIP(74)=CCP(7)*I3P(10)+I3P(26)
      WIP(73)=CCP(7)*I3P(9)+I3P(25)
      WIP(71)=CCP(7)*I3P(7)+I3P(23)
      WIP(70)=CCP(7)*I3P(6)+I3P(22)
      WIP(69)=CCP(7)*I3P(5)+I3P(21)
      WIP(67)=CCP(7)*I3P(3)+I3P(19)
      WIP(66)=CCP(7)*I3P(2)+I3P(18)
      WIP(65)=CCP(7)*I2P(1)+I2P(8)
      WIP(43)=CCP(2)*I3P(11)+CCP(3)*I3P(27)+I3P(43)
      WIP(42)=CCP(2)*I3P(10)+CCP(3)*I3P(26)+I3P(42)
      WIP(41)=CCP(2)*I3P(9)+CCP(3)*I3P(25)+I3P(41)
      WIP(39)=CCP(2)*I3P(7)+CCP(3)*I3P(23)+I3P(39)
      WIP(38)=CCP(2)*I3P(6)+CCP(3)*I3P(22)+I3P(38)
      WIP(37)=CCP(2)*I3P(5)+CCP(3)*I3P(21)+I3P(37)
      WIP(35)=CCP(2)*I3P(3)+CCP(3)*I3P(19)+I3P(35)
      WIP(34)=CCP(2)*I3P(2)+CCP(3)*I3P(18)+I3P(34)
      WIP(33)=CCP(2)*I2P(1)+CCP(3)*I2P(8)+I2P(15)
      WIP(27)=CCP(1)*I3P(11)+I3P(27)
      WIP(26)=CCP(1)*I3P(10)+I3P(26)
      WIP(25)=CCP(1)*I3P(9)+I3P(25)
      WIP(23)=CCP(1)*I3P(7)+I3P(23)
      WIP(22)=CCP(1)*I3P(6)+I3P(22)
      WIP(21)=CCP(1)*I3P(5)+I3P(21)
      WIP(19)=CCP(1)*I3P(3)+I3P(19)
      WIP(18)=CCP(1)*I3P(2)+I3P(18)
      WIP(17)=CCP(1)*I2P(1)+I2P(8)
      WIP(11)=I3P(11)
      WIP(10)=I3P(10)
      WIP(9)=I3P(9)
      WIP(7)=I3P(7)
      WIP(6)=I3P(6)
      WIP(5)=I3P(5)
      WIP(3)=I3P(3)
      WIP(2)=I3P(2)
      WIP(1)=G2(1)
!
! End of routine DDDD4C
      return
      end subroutine DDDD4C
      subroutine DDDD3CP (A2,         & ! Input
                          G2,         & ! Input
                          CCP,        & ! Input
                          WIP)       ! Output
!***********************************************************************
!     Date last modified: January 31, 1997                 Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!     Specialized version of DDDD3CP                                    *
!                                                                      *
!***********************************************************************
! Modules:

      implicit none
!
! Input arrays:
      double precision A2(174),CCP(48),G2(13)
!
! Output array:
      double precision WIP(256)
!
! Local arrays:
      double precision I2P(33)
!
! Begin:
!
      I2P(33)=A2(92)*G2(1)+A2(93)*G2(3)+A2(94)*G2(5)+A2(95)*G2(7)+A2(96)*G2(9)
      I2P(32)=A2(88)*G2(2)+A2(89)*G2(4)+A2(90)*G2(6)+A2(91)*G2(8)
      I2P(31)=A2(84)*G2(1)+A2(85)*G2(3)+A2(86)*G2(5)+A2(87)*G2(7)
      I2P(30)=A2(81)*G2(2)+A2(82)*G2(4)+A2(83)*G2(6)
      I2P(29)=A2(78)*G2(1)+A2(79)*G2(3)+A2(80)*G2(5)
      I2P(26)=A2(65)*G2(2)+A2(66)*G2(4)+A2(67)*G2(6)+A2(68)*G2(8)
      I2P(25)=A2(62)*G2(3)+A2(63)*G2(5)+A2(64)*G2(7)
      I2P(24)=A2(59)*G2(2)+A2(60)*G2(4)+A2(61)*G2(6)
      I2P(23)=A2(57)*G2(3)+A2(58)*G2(5)
      I2P(22)=A2(55)*G2(2)+A2(56)*G2(4)
      I2P(19)=A2(42)*G2(1)+A2(43)*G2(3)+A2(44)*G2(5)+A2(45)*G2(7)
      I2P(18)=A2(39)*G2(2)+A2(40)*G2(4)+A2(41)*G2(6)
      I2P(17)=A2(36)*G2(1)+A2(37)*G2(3)+A2(38)*G2(5)
      I2P(16)=A2(34)*G2(2)+A2(35)*G2(4)
      I2P(15)=A2(32)*G2(1)+A2(33)*G2(3)
      I2P(12)=A2(22)*G2(2)+A2(23)*G2(4)+A2(24)*G2(6)
      I2P(11)=A2(20)*G2(3)+A2(21)*G2(5)
      I2P(10)=A2(18)*G2(2)+A2(19)*G2(4)
      I2P(9)=A2(17)*G2(3)
      I2P(8)=A2(16)*G2(2)
      I2P(5)=A2(6)*G2(1)+A2(7)*G2(3)+A2(8)*G2(5)
      I2P(4)=A2(4)*G2(2)+A2(5)*G2(4)
      I2P(3)=A2(2)*G2(1)+A2(3)*G2(3)
      I2P(2)=A2(1)*G2(2)
      I2P(1)=G2(1)

      WIP(171)=CCP(22)*I2P(5)+CCP(23)*I2P(12)+CCP(24)*I2P(19)+CCP(25)*I2P(26)+I2P(33)
      WIP(170)=CCP(22)*I2P(4)+CCP(23)*I2P(11)+CCP(24)*I2P(18)+CCP(25)*I2P(25)+I2P(32)
      WIP(169)=CCP(22)*I2P(3)+CCP(23)*I2P(10)+CCP(24)*I2P(17)+CCP(25)*I2P(24)+I2P(31)
      WIP(167)=CCP(22)*I2P(4)+CCP(23)*I2P(11)+CCP(24)*I2P(18)+CCP(25)*I2P(25)+I2P(32)
      WIP(166)=CCP(22)*I2P(3)+CCP(23)*I2P(10)+CCP(24)*I2P(17)+CCP(25)*I2P(24)+I2P(31)
      WIP(165)=CCP(22)*I2P(2)+CCP(23)*I2P(9)+CCP(24)*I2P(16)+CCP(25)*I2P(23)+I2P(30)
      WIP(163)=CCP(22)*I2P(3)+CCP(23)*I2P(10)+CCP(24)*I2P(17)+CCP(25)*I2P(24)+I2P(31)
      WIP(162)=CCP(22)*I2P(2)+CCP(23)*I2P(9)+CCP(24)*I2P(16)+CCP(25)*I2P(23)+I2P(30)
      WIP(161)=CCP(22)*I2P(1)+CCP(23)*I2P(8)+CCP(24)*I2P(15)+CCP(25)*I2P(22)+I2P(29)
      WIP(155)=CCP(19)*I2P(5)+CCP(20)*I2P(12)+CCP(21)*I2P(19)+I2P(26)
      WIP(154)=CCP(19)*I2P(4)+CCP(20)*I2P(11)+CCP(21)*I2P(18)+I2P(25)
      WIP(153)=CCP(19)*I2P(3)+CCP(20)*I2P(10)+CCP(21)*I2P(17)+I2P(24)
      WIP(151)=CCP(19)*I2P(4)+CCP(20)*I2P(11)+CCP(21)*I2P(18)+I2P(25)
      WIP(150)=CCP(19)*I2P(3)+CCP(20)*I2P(10)+CCP(21)*I2P(17)+I2P(24)
      WIP(149)=CCP(19)*I2P(2)+CCP(20)*I2P(9)+CCP(21)*I2P(16)+I2P(23)
      WIP(147)=CCP(19)*I2P(3)+CCP(20)*I2P(10)+CCP(21)*I2P(17)+I2P(24)
      WIP(146)=CCP(19)*I2P(2)+CCP(20)*I2P(9)+CCP(21)*I2P(16)+I2P(23)
      WIP(145)=CCP(19)*I2P(1)+CCP(20)*I2P(8)+CCP(21)*I2P(15)+I2P(22)
      WIP(139)=CCP(17)*I2P(5)+CCP(18)*I2P(12)+I2P(19)
      WIP(138)=CCP(17)*I2P(4)+CCP(18)*I2P(11)+I2P(18)
      WIP(137)=CCP(17)*I2P(3)+CCP(18)*I2P(10)+I2P(17)
      WIP(135)=CCP(17)*I2P(4)+CCP(18)*I2P(11)+I2P(18)
      WIP(134)=CCP(17)*I2P(3)+CCP(18)*I2P(10)+I2P(17)
      WIP(133)=CCP(17)*I2P(2)+CCP(18)*I2P(9)+I2P(16)
      WIP(131)=CCP(17)*I2P(3)+CCP(18)*I2P(10)+I2P(17)
      WIP(130)=CCP(17)*I2P(2)+CCP(18)*I2P(9)+I2P(16)
      WIP(129)=CCP(17)*I2P(1)+CCP(18)*I2P(8)+I2P(15)
      WIP(107)=CCP(10)*I2P(5)+CCP(11)*I2P(12)+CCP(12)*I2P(19)+I2P(26)
      WIP(106)=CCP(10)*I2P(4)+CCP(11)*I2P(11)+CCP(12)*I2P(18)+I2P(25)
      WIP(105)=CCP(10)*I2P(3)+CCP(11)*I2P(10)+CCP(12)*I2P(17)+I2P(24)
      WIP(103)=CCP(10)*I2P(4)+CCP(11)*I2P(11)+CCP(12)*I2P(18)+I2P(25)
      WIP(102)=CCP(10)*I2P(3)+CCP(11)*I2P(10)+CCP(12)*I2P(17)+I2P(24)
      WIP(101)=CCP(10)*I2P(2)+CCP(11)*I2P(9)+CCP(12)*I2P(16)+I2P(23)
      WIP(99)=CCP(10)*I2P(3)+CCP(11)*I2P(10)+CCP(12)*I2P(17)+I2P(24)
      WIP(98)=CCP(10)*I2P(2)+CCP(11)*I2P(9)+CCP(12)*I2P(16)+I2P(23)
      WIP(97)=CCP(10)*I2P(1)+CCP(11)*I2P(8)+CCP(12)*I2P(15)+I2P(22)
      WIP(91)=CCP(8)*I2P(5)+CCP(9)*I2P(12)+I2P(19)
      WIP(90)=CCP(8)*I2P(4)+CCP(9)*I2P(11)+I2P(18)
      WIP(89)=CCP(8)*I2P(3)+CCP(9)*I2P(10)+I2P(17)
      WIP(87)=CCP(8)*I2P(4)+CCP(9)*I2P(11)+I2P(18)
      WIP(86)=CCP(8)*I2P(3)+CCP(9)*I2P(10)+I2P(17)
      WIP(85)=CCP(8)*I2P(2)+CCP(9)*I2P(9)+I2P(16)
      WIP(83)=CCP(8)*I2P(3)+CCP(9)*I2P(10)+I2P(17)
      WIP(82)=CCP(8)*I2P(2)+CCP(9)*I2P(9)+I2P(16)
      WIP(81)=CCP(8)*I2P(1)+CCP(9)*I2P(8)+I2P(15)
      WIP(75)=CCP(7)*I2P(5)+I2P(12)
      WIP(74)=CCP(7)*I2P(4)+I2P(11)
      WIP(73)=CCP(7)*I2P(3)+I2P(10)
      WIP(71)=CCP(7)*I2P(4)+I2P(11)
      WIP(70)=CCP(7)*I2P(3)+I2P(10)
      WIP(69)=CCP(7)*I2P(2)+I2P(9)
      WIP(67)=CCP(7)*I2P(3)+I2P(10)
      WIP(66)=CCP(7)*I2P(2)+I2P(9)
      WIP(65)=CCP(7)*I2P(1)+I2P(8)
      WIP(43)=CCP(2)*I2P(5)+CCP(3)*I2P(12)+I2P(19)
      WIP(42)=CCP(2)*I2P(4)+CCP(3)*I2P(11)+I2P(18)
      WIP(41)=CCP(2)*I2P(3)+CCP(3)*I2P(10)+I2P(17)
      WIP(39)=CCP(2)*I2P(4)+CCP(3)*I2P(11)+I2P(18)
      WIP(38)=CCP(2)*I2P(3)+CCP(3)*I2P(10)+I2P(17)
      WIP(37)=CCP(2)*I2P(2)+CCP(3)*I2P(9)+I2P(16)
      WIP(35)=CCP(2)*I2P(3)+CCP(3)*I2P(10)+I2P(17)
      WIP(34)=CCP(2)*I2P(2)+CCP(3)*I2P(9)+I2P(16)
      WIP(33)=CCP(2)*I2P(1)+CCP(3)*I2P(8)+I2P(15)
      WIP(27)=CCP(1)*I2P(5)+I2P(12)
      WIP(26)=CCP(1)*I2P(4)+I2P(11)
      WIP(25)=CCP(1)*I2P(3)+I2P(10)
      WIP(23)=CCP(1)*I2P(4)+I2P(11)
      WIP(22)=CCP(1)*I2P(3)+I2P(10)
      WIP(21)=CCP(1)*I2P(2)+I2P(9)
      WIP(19)=CCP(1)*I2P(3)+I2P(10)
      WIP(18)=CCP(1)*I2P(2)+I2P(9)
      WIP(17)=CCP(1)*I2P(1)+I2P(8)
      WIP(11)=I2P(5)
      WIP(10)=I2P(4)
      WIP(9)=I2P(3)
      WIP(7)=I2P(4)
      WIP(6)=I2P(3)
      WIP(5)=I2P(2)
      WIP(3)=I2P(3)
      WIP(2)=I2P(2)
      WIP(1)=G2(1)
!
! End of routine DDDD3CP
      return
      end subroutine DDDD3CP
      subroutine DDDD3CQ (A2,         & ! Input
                          G2,         & ! Input
                          CCQ,        & ! Input
                          WIP)       ! Output
!***********************************************************************
!     Date last modified: January 31, 1997                 Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!     Specialized version of DDDD3CQ                                    *
! 								       *
!***********************************************************************
! Modules:

      implicit none
!
! Input arrays:
      double precision A2(174),CCQ(48),G2(13)
!
! Output array:
      double precision WIP(256)
!
! Local arrays:
      double precision I2P(33),I3P(75)
!
! Begin:
!
      I2P(33)=A2(92)*G2(1)+A2(93)*G2(3)+A2(94)*G2(5)+A2(95)*G2(7)+A2(96)*G2(9)
      I2P(32)=A2(88)*G2(2)+A2(89)*G2(4)+A2(90)*G2(6)+A2(91)*G2(8)
      I2P(31)=A2(84)*G2(1)+A2(85)*G2(3)+A2(86)*G2(5)+A2(87)*G2(7)
      I2P(30)=A2(81)*G2(2)+A2(82)*G2(4)+A2(83)*G2(6)
      I2P(29)=A2(78)*G2(1)+A2(79)*G2(3)+A2(80)*G2(5)
      I2P(26)=A2(65)*G2(2)+A2(66)*G2(4)+A2(67)*G2(6)+A2(68)*G2(8)
      I2P(25)=A2(62)*G2(3)+A2(63)*G2(5)+A2(64)*G2(7)
      I2P(24)=A2(59)*G2(2)+A2(60)*G2(4)+A2(61)*G2(6)
      I2P(23)=A2(57)*G2(3)+A2(58)*G2(5)
      I2P(22)=A2(55)*G2(2)+A2(56)*G2(4)
      I2P(19)=A2(42)*G2(1)+A2(43)*G2(3)+A2(44)*G2(5)+A2(45)*G2(7)
      I2P(18)=A2(39)*G2(2)+A2(40)*G2(4)+A2(41)*G2(6)
      I2P(17)=A2(36)*G2(1)+A2(37)*G2(3)+A2(38)*G2(5)
      I2P(16)=A2(34)*G2(2)+A2(35)*G2(4)
      I2P(15)=A2(32)*G2(1)+A2(33)*G2(3)
      I2P(12)=A2(22)*G2(2)+A2(23)*G2(4)+A2(24)*G2(6)
      I2P(11)=A2(20)*G2(3)+A2(21)*G2(5)
      I2P(10)=A2(18)*G2(2)+A2(19)*G2(4)
      I2P(9)=A2(17)*G2(3)
      I2P(8)=A2(16)*G2(2)
      I2P(5)=A2(6)*G2(1)+A2(7)*G2(3)+A2(8)*G2(5)
      I2P(4)=A2(4)*G2(2)+A2(5)*G2(4)
      I2P(3)=A2(2)*G2(1)+A2(3)*G2(3)
      I2P(2)=A2(1)*G2(2)
      I2P(1)=G2(1)

      I3P(75)=CCQ(22)*I2P(29)+CCQ(23)*I2P(30)+CCQ(24)*I2P(31)+CCQ(25)*I2P(32)+I2P(33)
      I3P(74)=CCQ(19)*I2P(29)+CCQ(20)*I2P(30)+CCQ(21)*I2P(31)+I2P(32)
      I3P(73)=CCQ(17)*I2P(29)+CCQ(18)*I2P(30)+I2P(31)
      I3P(71)=CCQ(10)*I2P(29)+CCQ(11)*I2P(30)+CCQ(12)*I2P(31)+I2P(32)
      I3P(70)=CCQ(8)*I2P(29)+CCQ(9)*I2P(30)+I2P(31)
      I3P(69)=CCQ(7)*I2P(29)+I2P(30)
      I3P(67)=CCQ(2)*I2P(29)+CCQ(3)*I2P(30)+I2P(31)
      I3P(66)=CCQ(1)*I2P(29)+I2P(30)
      I3P(59)=CCQ(22)*I2P(22)+CCQ(23)*I2P(23)+CCQ(24)*I2P(24)+CCQ(25)*I2P(25)+I2P(26)
      I3P(58)=CCQ(19)*I2P(22)+CCQ(20)*I2P(23)+CCQ(21)*I2P(24)+I2P(25)
      I3P(57)=CCQ(17)*I2P(22)+CCQ(18)*I2P(23)+I2P(24)
      I3P(55)=CCQ(10)*I2P(22)+CCQ(11)*I2P(23)+CCQ(12)*I2P(24)+I2P(25)
      I3P(54)=CCQ(8)*I2P(22)+CCQ(9)*I2P(23)+I2P(24)
      I3P(53)=CCQ(7)*I2P(22)+I2P(23)
      I3P(51)=CCQ(2)*I2P(22)+CCQ(3)*I2P(23)+I2P(24)
      I3P(50)=CCQ(1)*I2P(22)+I2P(23)
      I3P(43)=CCQ(22)*I2P(15)+CCQ(23)*I2P(16)+CCQ(24)*I2P(17)+CCQ(25)*I2P(18)+I2P(19)
      I3P(42)=CCQ(19)*I2P(15)+CCQ(20)*I2P(16)+CCQ(21)*I2P(17)+I2P(18)
      I3P(41)=CCQ(17)*I2P(15)+CCQ(18)*I2P(16)+I2P(17)
      I3P(39)=CCQ(10)*I2P(15)+CCQ(11)*I2P(16)+CCQ(12)*I2P(17)+I2P(18)
      I3P(38)=CCQ(8)*I2P(15)+CCQ(9)*I2P(16)+I2P(17)
      I3P(37)=CCQ(7)*I2P(15)+I2P(16)
      I3P(35)=CCQ(2)*I2P(15)+CCQ(3)*I2P(16)+I2P(17)
      I3P(34)=CCQ(1)*I2P(15)+I2P(16)
      I3P(27)=CCQ(22)*I2P(8)+CCQ(23)*I2P(9)+CCQ(24)*I2P(10)+CCQ(25)*I2P(11)+I2P(12)
      I3P(26)=CCQ(19)*I2P(8)+CCQ(20)*I2P(9)+CCQ(21)*I2P(10)+I2P(11)
      I3P(25)=CCQ(17)*I2P(8)+CCQ(18)*I2P(9)+I2P(10)
      I3P(23)=CCQ(10)*I2P(8)+CCQ(11)*I2P(9)+CCQ(12)*I2P(10)+I2P(11)
      I3P(22)=CCQ(8)*I2P(8)+CCQ(9)*I2P(9)+I2P(10)
      I3P(21)=CCQ(7)*I2P(8)+I2P(9)
      I3P(19)=CCQ(2)*I2P(8)+CCQ(3)*I2P(9)+I2P(10)
      I3P(18)=CCQ(1)*I2P(8)+I2P(9)
      I3P(11)=CCQ(22)*I2P(1)+CCQ(23)*I2P(2)+CCQ(24)*I2P(3)+CCQ(25)*I2P(4)+I2P(5)
      I3P(10)=CCQ(19)*I2P(1)+CCQ(20)*I2P(2)+CCQ(21)*I2P(3)+I2P(4)
      I3P(9)=CCQ(17)*I2P(1)+CCQ(18)*I2P(2)+I2P(3)
      I3P(7)=CCQ(10)*I2P(1)+CCQ(11)*I2P(2)+CCQ(12)*I2P(3)+I2P(4)
      I3P(6)=CCQ(8)*I2P(1)+CCQ(9)*I2P(2)+I2P(3)
      I3P(5)=CCQ(7)*I2P(1)+I2P(2)
      I3P(3)=CCQ(2)*I2P(1)+CCQ(3)*I2P(2)+I2P(3)
      I3P(2)=CCQ(1)*I2P(1)+I2P(2)

      WIP(171)=I3P(75)
      WIP(170)=I3P(74)
      WIP(169)=I3P(73)
      WIP(167)=I3P(71)
      WIP(166)=I3P(70)
      WIP(165)=I3P(69)
      WIP(163)=I3P(67)
      WIP(162)=I3P(66)
      WIP(161)=I2P(29)
      WIP(155)=I3P(59)
      WIP(154)=I3P(58)
      WIP(153)=I3P(57)
      WIP(151)=I3P(55)
      WIP(150)=I3P(54)
      WIP(149)=I3P(53)
      WIP(147)=I3P(51)
      WIP(146)=I3P(50)
      WIP(145)=I2P(22)
      WIP(139)=I3P(43)
      WIP(138)=I3P(42)
      WIP(137)=I3P(41)
      WIP(135)=I3P(39)
      WIP(134)=I3P(38)
      WIP(133)=I3P(37)
      WIP(131)=I3P(35)
      WIP(130)=I3P(34)
      WIP(129)=I2P(15)
      WIP(107)=I3P(59)
      WIP(106)=I3P(58)
      WIP(105)=I3P(57)
      WIP(103)=I3P(55)
      WIP(102)=I3P(54)
      WIP(101)=I3P(53)
      WIP(99)=I3P(51)
      WIP(98)=I3P(50)
      WIP(97)=I2P(22)
      WIP(91)=I3P(43)
      WIP(90)=I3P(42)
      WIP(89)=I3P(41)
      WIP(87)=I3P(39)
      WIP(86)=I3P(38)
      WIP(85)=I3P(37)
      WIP(83)=I3P(35)
      WIP(82)=I3P(34)
      WIP(81)=I2P(15)
      WIP(75)=I3P(27)
      WIP(74)=I3P(26)
      WIP(73)=I3P(25)
      WIP(71)=I3P(23)
      WIP(70)=I3P(22)
      WIP(69)=I3P(21)
      WIP(67)=I3P(19)
      WIP(66)=I3P(18)
      WIP(65)=I2P(8)
      WIP(43)=I3P(43)
      WIP(42)=I3P(42)
      WIP(41)=I3P(41)
      WIP(39)=I3P(39)
      WIP(38)=I3P(38)
      WIP(37)=I3P(37)
      WIP(35)=I3P(35)
      WIP(34)=I3P(34)
      WIP(33)=I2P(15)
      WIP(27)=I3P(27)
      WIP(26)=I3P(26)
      WIP(25)=I3P(25)
      WIP(23)=I3P(23)
      WIP(22)=I3P(22)
      WIP(21)=I3P(21)
      WIP(19)=I3P(19)
      WIP(18)=I3P(18)
      WIP(17)=I2P(8)
      WIP(11)=I3P(11)
      WIP(10)=I3P(10)
      WIP(9)=I3P(9)
      WIP(7)=I3P(7)
      WIP(6)=I3P(6)
      WIP(5)=I3P(5)
      WIP(3)=I3P(3)
      WIP(2)=I3P(2)
      WIP(1)=G2(1)
!
! End of routine DDDD3CQ
      return
      end subroutine DDDD3CQ
      subroutine DDDD2C (A2,          & ! Input
                         G2,          & ! Input
                         WIP)        ! Output
!***********************************************************************
!     Date last modified: January 31, 1997                 Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!     Specialized version of DDDD2C                                    *
!                                                                      *
!***********************************************************************
! Modules:

      implicit none
!
! Input arrays:
      double precision A2(174),G2(13)
!
! Output array:
      double precision WIP(256)
!
! Begin:
!
      WIP(171)=A2(92)*G2(1)+A2(93)*G2(3)+A2(94)*G2(5)+A2(95)*G2(7)+A2(96)*G2(9)
      WIP(170)=A2(88)*G2(2)+A2(89)*G2(4)+A2(90)*G2(6)+A2(91)*G2(8)
      WIP(169)=A2(84)*G2(1)+A2(85)*G2(3)+A2(86)*G2(5)+A2(87)*G2(7)
      WIP(167)=A2(88)*G2(2)+A2(89)*G2(4)+A2(90)*G2(6)+A2(91)*G2(8)
      WIP(166)=A2(84)*G2(1)+A2(85)*G2(3)+A2(86)*G2(5)+A2(87)*G2(7)
      WIP(165)=A2(81)*G2(2)+A2(82)*G2(4)+A2(83)*G2(6)
      WIP(163)=A2(84)*G2(1)+A2(85)*G2(3)+A2(86)*G2(5)+A2(87)*G2(7)
      WIP(162)=A2(81)*G2(2)+A2(82)*G2(4)+A2(83)*G2(6)
      WIP(161)=A2(78)*G2(1)+A2(79)*G2(3)+A2(80)*G2(5)
      WIP(155)=A2(65)*G2(2)+A2(66)*G2(4)+A2(67)*G2(6)+A2(68)*G2(8)
      WIP(154)=A2(62)*G2(3)+A2(63)*G2(5)+A2(64)*G2(7)
      WIP(153)=A2(59)*G2(2)+A2(60)*G2(4)+A2(61)*G2(6)
      WIP(151)=A2(62)*G2(3)+A2(63)*G2(5)+A2(64)*G2(7)
      WIP(150)=A2(59)*G2(2)+A2(60)*G2(4)+A2(61)*G2(6)
      WIP(149)=A2(57)*G2(3)+A2(58)*G2(5)
      WIP(147)=A2(59)*G2(2)+A2(60)*G2(4)+A2(61)*G2(6)
      WIP(146)=A2(57)*G2(3)+A2(58)*G2(5)
      WIP(145)=A2(55)*G2(2)+A2(56)*G2(4)
      WIP(139)=A2(42)*G2(1)+A2(43)*G2(3)+A2(44)*G2(5)+A2(45)*G2(7)
      WIP(138)=A2(39)*G2(2)+A2(40)*G2(4)+A2(41)*G2(6)
      WIP(137)=A2(36)*G2(1)+A2(37)*G2(3)+A2(38)*G2(5)
      WIP(135)=A2(39)*G2(2)+A2(40)*G2(4)+A2(41)*G2(6)
      WIP(134)=A2(36)*G2(1)+A2(37)*G2(3)+A2(38)*G2(5)
      WIP(133)=A2(34)*G2(2)+A2(35)*G2(4)
      WIP(131)=A2(36)*G2(1)+A2(37)*G2(3)+A2(38)*G2(5)
      WIP(130)=A2(34)*G2(2)+A2(35)*G2(4)
      WIP(129)=A2(32)*G2(1)+A2(33)*G2(3)
      WIP(107)=A2(65)*G2(2)+A2(66)*G2(4)+A2(67)*G2(6)+A2(68)*G2(8)
      WIP(106)=A2(62)*G2(3)+A2(63)*G2(5)+A2(64)*G2(7)
      WIP(105)=A2(59)*G2(2)+A2(60)*G2(4)+A2(61)*G2(6)
      WIP(103)=A2(62)*G2(3)+A2(63)*G2(5)+A2(64)*G2(7)
      WIP(102)=A2(59)*G2(2)+A2(60)*G2(4)+A2(61)*G2(6)
      WIP(101)=A2(57)*G2(3)+A2(58)*G2(5)
      WIP(99)=A2(59)*G2(2)+A2(60)*G2(4)+A2(61)*G2(6)
      WIP(98)=A2(57)*G2(3)+A2(58)*G2(5)
      WIP(97)=A2(55)*G2(2)+A2(56)*G2(4)
      WIP(91)=A2(42)*G2(1)+A2(43)*G2(3)+A2(44)*G2(5)+A2(45)*G2(7)
      WIP(90)=A2(39)*G2(2)+A2(40)*G2(4)+A2(41)*G2(6)
      WIP(89)=A2(36)*G2(1)+A2(37)*G2(3)+A2(38)*G2(5)
      WIP(87)=A2(39)*G2(2)+A2(40)*G2(4)+A2(41)*G2(6)
      WIP(86)=A2(36)*G2(1)+A2(37)*G2(3)+A2(38)*G2(5)
      WIP(85)=A2(34)*G2(2)+A2(35)*G2(4)
      WIP(83)=A2(36)*G2(1)+A2(37)*G2(3)+A2(38)*G2(5)
      WIP(82)=A2(34)*G2(2)+A2(35)*G2(4)
      WIP(81)=A2(32)*G2(1)+A2(33)*G2(3)
      WIP(75)=A2(22)*G2(2)+A2(23)*G2(4)+A2(24)*G2(6)
      WIP(74)=A2(20)*G2(3)+A2(21)*G2(5)
      WIP(73)=A2(18)*G2(2)+A2(19)*G2(4)
      WIP(71)=A2(20)*G2(3)+A2(21)*G2(5)
      WIP(70)=A2(18)*G2(2)+A2(19)*G2(4)
      WIP(69)=A2(17)*G2(3)
      WIP(67)=A2(18)*G2(2)+A2(19)*G2(4)
      WIP(66)=A2(17)*G2(3)
      WIP(65)=A2(16)*G2(2)
      WIP(43)=A2(42)*G2(1)+A2(43)*G2(3)+A2(44)*G2(5)+A2(45)*G2(7)
      WIP(42)=A2(39)*G2(2)+A2(40)*G2(4)+A2(41)*G2(6)
      WIP(41)=A2(36)*G2(1)+A2(37)*G2(3)+A2(38)*G2(5)
      WIP(39)=A2(39)*G2(2)+A2(40)*G2(4)+A2(41)*G2(6)
      WIP(38)=A2(36)*G2(1)+A2(37)*G2(3)+A2(38)*G2(5)
      WIP(37)=A2(34)*G2(2)+A2(35)*G2(4)
      WIP(35)=A2(36)*G2(1)+A2(37)*G2(3)+A2(38)*G2(5)
      WIP(34)=A2(34)*G2(2)+A2(35)*G2(4)
      WIP(33)=A2(32)*G2(1)+A2(33)*G2(3)
      WIP(27)=A2(22)*G2(2)+A2(23)*G2(4)+A2(24)*G2(6)
      WIP(26)=A2(20)*G2(3)+A2(21)*G2(5)
      WIP(25)=A2(18)*G2(2)+A2(19)*G2(4)
      WIP(23)=A2(20)*G2(3)+A2(21)*G2(5)
      WIP(22)=A2(18)*G2(2)+A2(19)*G2(4)
      WIP(21)=A2(17)*G2(3)
      WIP(19)=A2(18)*G2(2)+A2(19)*G2(4)
      WIP(18)=A2(17)*G2(3)
      WIP(17)=A2(16)*G2(2)
      WIP(11)=A2(6)*G2(1)+A2(7)*G2(3)+A2(8)*G2(5)
      WIP(10)=A2(4)*G2(2)+A2(5)*G2(4)
      WIP(9)=A2(2)*G2(1)+A2(3)*G2(3)
      WIP(7)=A2(4)*G2(2)+A2(5)*G2(4)
      WIP(6)=A2(2)*G2(1)+A2(3)*G2(3)
      WIP(5)=A2(1)*G2(2)
      WIP(3)=A2(2)*G2(1)+A2(3)*G2(3)
      WIP(2)=A2(1)*G2(2)
      WIP(1)=G2(1)
!
! End of routine DDDD2C
      return
      end subroutine DDDD2C
! END CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine I2ER_SPDF
!***********************************************************************
!     Date last modified: September 23, 1996               Version 2.0 *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!***********************************************************************
! Modules:
      USE mod_idfclc

      implicit none
!
! Local Scalars
!
      call PRG_manager ('enter', 'I2ER_SPDF', 'UTILITY')

      include "start_atmshl_loop"

      case=1
      call DEF_shells (Jatmshl, Katmshl, Latmshl, Case)
      if((ABEXP+CDEXP).le.I2E_expcut)then
        Int_pointer=>IJKLs
        call I2ER_GSPDF
      end if

      if(LTWOINT)then
      case=2
      call DEF_shells (Latmshl, Jatmshl, Katmshl, Case)
      if((ABEXP+CDEXP).le.I2E_expcut)then
        Int_pointer=>ILJKs
        call I2ER_GSPDF
      end if
      end if

      if(Mtype.eq.1)then
      case=3
      call DEF_shells (Katmshl, Jatmshl, Latmshl, Case)
      if((ABEXP+CDEXP).le.I2E_expcut)then
        Int_pointer=>IKJLs
        call I2ER_GSPDF
      end if
      end if

      include "end_atmshl_loop"

! End of routine I2ER_SPDF
      call PRG_manager ('exit', 'I2ER_SPDF', 'UTILITY')
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine I2ER_GSPDF
!***********************************************************************
!     Date last modified: September 23, 1996               Version 2.0 *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!***********************************************************************
! Modules:

      implicit none
!
! Local Scalars
      integer :: COR_INDEX
      integer :: IX,IY,IZ,JX,JY,JZ,KX,KY,KZ,LX,LY,LZ
      integer :: Iao,Jao,Kao,Lao
      integer :: IZERO,INTC,IV,JendM,KendM,LendM,Igauss,Jgauss,Kgauss,Lgauss,NKL,NJKL
      double precision :: CC1,CC2,CC3
      double precision :: AS,BS,CS,DS
      double precision :: RHO,TWORHO,DXYZ,ZTEMP,RHOT2,ZCONST
      double precision :: EPAB,EPABI,EPABA,EPABB,EQCD,EQCDI,EQCDC,EQCDD,EABCD,EP2I,EABCDI
      double precision :: XAP,YAP,ZAP,XBP,YBP,ZBP,XCQ,YCQ,ZCQ,XDQ,YDQ,ZDQ,PQX,PQY,PQZ,RPQSQ
!
! Local Arrays
      double precision :: G2DFX(13),G2DFY(13),G2DFZ(13),XIP(256),YIP(256),ZIP(256)
      double precision :: A2DF(174),CCPX(48),CCPY(48),CCPZ(48),CCQX(48),CCQY(48),CCQZ(48)
      double precision:: XINT(11)
      integer :: INDIX(20),INDIY(20),INDIZ(20),INDJX(20),INDJY(20),INDJZ(20),INDKX(20),INDKY(20),INDKZ(20),&
      INDLX(20),INDLY(20),INDLZ(20)

!
      DATA INDIX/0,64,0,0,128,0,0,64,64,0,192,0,0,64,128,128,64,0,0,64/, &
           INDIY/0,0,64,0,0,128,0,64,0,64,0,192,0,128,64,0,0,64,128,64/, &
           INDIZ/0,0,0,64,0,0,128,0,64,64,0,0,192,0,0,64,128,128,64,64/, &
           INDJX/0,16,0,0,32,0,0,16,16,0,48,0,0,16,32,32,16,0,0,16/, &
           INDJY/0,0,16,0,0,32,0,16,0,16,0,48,0,32,16,0,0,16,32,16/, &
           INDJZ/0,0,0,16,0,0,32,0,16,16,0,0,48,0,0,16,32,32,16,16/, &
           INDKX/0,4,0,0,8,0,0,4,4,0,12,0,0,4,8,8,4,0,0,4/, &
           INDKY/0,0,4,0,0,8,0,4,0,4,0,12,0,8,4,0,0,4,8,4/, &
           INDKZ/0,0,0,4,0,0,8,0,4,4,0,0,12,0,0,4,8,8,4,4/, &
           INDLX/1,2,1,1,3,1,1,2,2,1,4,1,1,2,3,3,2,1,1,2/, &
           INDLY/1,1,2,1,1,3,1,2,1,2,1,4,1,3,2,1,1,2,3,2/, &
           INDLZ/1,1,1,2,1,1,3,1,2,2,1,1,4,1,1,2,3,3,2,2/, &
           XINT/1.0D0,2.0D0,3.0D0,4.0D0,5.0D0,6.0D0,7.0D0,8.0D0,9.0D0,10.0D0,11.0D0/
!
! Begin:
      A2DF(1:174)=ZERO
      CCPX(1:48)=ZERO
      CCPY(1:48)=ZERO
      CCPZ(1:48)=ZERO
      CCQX(1:48)=ZERO
      CCQY(1:48)=ZERO
      CCQZ(1:48)=ZERO

!*************************************************************************************
      include 'mungauss_gaussian_AB'
      include 'mungauss_gaussian_CD'

      TQprim(1:LENTQ)=ZERO

      IF(EXPARG.LE.I2E_expcut)then
! NOTE: Must zero CCP's if this step is skipped!!!
      if(LRAB)then
        EPABA=AS*EPABI
        EPABB=BS*EPABI
        XAP= EPABB*(XB-XA)
        YAP= EPABB*(YB-YA)
        ZAP= EPABB*(ZB-ZA)
        XBP=-EPABA*(XB-XA)
        YBP=-EPABA*(YB-YA)
        ZBP=-EPABA*(ZB-ZA)
        call GETCC_XYZ (CCPX, CCPY, CCPZ, XAP, YAP, ZAP, XBP, YBP, ZBP, LAMAX, LBMAX)
      end if

      if(LRCD)then
        EQCDC=CS*EQCDI
        EQCDD=DS*EQCDI
        XCQ= EQCDD*(XD-XC)
        YCQ= EQCDD*(YD-YC)
        ZCQ= EQCDD*(ZD-ZC)
        XDQ=-EQCDC*(XD-XC)
        YDQ=-EQCDC*(YD-YC)
        ZDQ=-EQCDC*(ZD-ZC)
        call GETCC_XYZ (CCQX, CCQY, CCQZ, XCQ, YCQ, ZCQ, XDQ, YDQ, ZDQ, LCMAX, LDMAX)
      end if
!
      PQX=(CS*XC+DS*XD)*EQCDI-(AS*XA+BS*XB)*EPABI
      PQY=(CS*YC+DS*YD)*EQCDI-(AS*YA+BS*YB)*EPABI
      PQZ=(CS*ZC+DS*ZD)*EQCDI-(AS*ZA+BS*ZB)*EPABI

      RPQSQ=PQX*PQX+PQY*PQY+PQZ*PQZ
!
      EABCD=EPAB*EQCD
      EABCDI=ONE/(EPAB+EQCD)
      RHO=EABCD*EABCDI

      ZTEMP=PIconst*DEXP(-EXPARG)*DSQRT(EABCDI)/EABCD

      EP2I=ONE/(EPAB+EPAB)
      call IJKLA2 (A2DF, LABMAX, LCDMAX, EQCD, EP2I)
!
      DXYZ=RHO*RPQSQ
      call RPOLX (NZERO, DXYZ, TP, WP)
      TWORHO=RHO+RHO

      do IZERO=1,NZERO
      RHOT2=TWORHO*TP(IZERO)
      ZCONST=ZTEMP*WP(IZERO)
      if(ZCONST.le.I2E_PQCUT2)cycle
      G2DFX(1)=ONE
      G2DFY(1)=ONE
      G2DFZ(1)=ZCONST
      XIP(1)=ONE
      YIP(1)=ONE
      ZIP(1)=G2DFZ(1)
!
      IF(LPQMAX.GE.2)then
      G2DFX(2)=RHOT2*PQX
      G2DFY(2)=RHOT2*PQY
      G2DFZ(2)=RHOT2*PQZ*G2DFZ(1)
!
      IF(LPQMAX.GE.3)then
        do IV=3,LPQMAX
          G2DFX(IV)=RHOT2*(PQX*G2DFX(IV-1)-XINT(IV-2)*G2DFX(IV-2))
          G2DFY(IV)=RHOT2*(PQY*G2DFY(IV-1)-XINT(IV-2)*G2DFY(IV-2))
          G2DFZ(IV)=RHOT2*(PQZ*G2DFZ(IV-1)-XINT(IV-2)*G2DFZ(IV-2))
        end do
      end if ! LPQMAX.GE.3
!
      IF(LRABCD)then
        call ABCD4C (A2DF, G2DFX, CCQX, CCPX, XIP, LAMAX, LBMAX, LCMAX, LDMAX)
        call ABCD4C (A2DF, G2DFY, CCQY, CCPY, YIP, LAMAX, LBMAX, LCMAX, LDMAX)
        call ABCD4C (A2DF, G2DFZ, CCQZ, CCPZ, ZIP, LAMAX, LBMAX, LCMAX, LDMAX)
      else IF(LRCD)then
        call AACD3C (A2DF, G2DFX, CCQX, XIP, LAMAX, LBMAX, LCMAX, LDMAX)
        call AACD3C (A2DF, G2DFY, CCQY, YIP, LAMAX, LBMAX, LCMAX, LDMAX)
        call AACD3C (A2DF, G2DFZ, CCQZ, ZIP, LAMAX, LBMAX, LCMAX, LDMAX)
      else IF(LRAB)then
        call ABCC3C (A2DF, G2DFX, CCPX, XIP, LAMAX, LBMAX, LCMAX, LDMAX)
        call ABCC3C (A2DF, G2DFY, CCPY, YIP, LAMAX, LBMAX, LCMAX, LDMAX)
        call ABCC3C (A2DF, G2DFZ, CCPZ, ZIP, LAMAX, LBMAX, LCMAX, LDMAX)
      else
        call AABB2C (A2DF, G2DFX, XIP, LAMAX, LBMAX, LCMAX, LDMAX)
        call AABB2C (A2DF, G2DFY, YIP, LAMAX, LBMAX, LCMAX, LDMAX)
        call AABB2C (A2DF, G2DFZ, ZIP, LAMAX, LBMAX, LCMAX, LDMAX)
      end if ! LRABCD
      end if ! LPQMAX.GE.2
!
! Commence loop over atomic orbitals.
      INTC=0
      JendM=Jend
      KendM=Kend
      do Iao=Istart,Iend
        IF(Iatmshl_EQ_Jatmshl)JendM=Iao
        IF(IJIJ)KendM=Iao
        IX=INDIX(Iao)
        IY=INDIY(Iao)
        IZ=INDIZ(Iao)
        do Jao=Jstart,JendM
          JX=INDJX(Jao)+IX
          JY=INDJY(Jao)+IY
          JZ=INDJZ(Jao)+IZ
          do Kao=Kstart,KendM
            LendM=Lend
            IF(Katmshl_EQ_Latmshl)LendM=Kao
            IF(IJIJ.and.Iao.EQ.Kao)LendM=Jao
            KX=INDKX(Kao)+JX
            KY=INDKY(Kao)+JY
            KZ=INDKZ(Kao)+JZ
            do Lao=Lstart,LendM
              LX=INDLX(Lao)+KX
              LY=INDLY(Lao)+KY
              LZ=INDLZ(Lao)+KZ
              INTC=INTC+1
              TQprim(INTC)=TQprim(INTC)+(XIP(LX)*YIP(LY)*ZIP(LZ))
            end do ! Lao
           end do ! Kao
        end do ! Jao
       end do ! Iao
      end do ! IZERO
      end if ! EXPARG.LE.I2E_expcut

! Apply contraction coefficients.
!**********************************************************************
      NJKL = Jrange*Krange*Lrange
      NKL = Krange*Lrange

      INTC = 0
      COR_INDEX = 0
      JendM = Jrange
      KendM = Krange
      do Iao=1,Irange
        CC1=GccA(Iao+CCA)
        IF(Iatmshl_EQ_Jatmshl)JendM = Iao
        IF(IJIJ)KendM = Iao
          do Jao=1,JendM
            CC2=CC1*GccB(Jao+CCB)
            do Kao=1,KendM
              CC3=CC2*GccC(Kao+CCC)
              LendM=Lrange
              IF(Katmshl_EQ_Latmshl)LendM=Kao
              IF(IJIJ.and.Iao.EQ.Kao)LendM=Jao
                do Lao=1,LendM
                  INTC=INTC+1
                  COR_INDEX=(Iao-1)*NJKL+(Jao-1)*NKL+(Kao-1)*Lrange+Lao
                  Int_pointer(COR_INDEX)=Int_pointer(COR_INDEX)+TQprim(INTC)*CC3*GccD(Lao+CCD)
                end do ! Lao
            end do ! Kao
         end do ! Jao
      end do ! Iao	
!
      CCD=CCD+Lrange
      end do Dloop ! Lgauss
      CCC=CCC+Krange
      end do Cloop ! Kgauss
      CCB=CCB+Jrange
      end do Bloop ! Jgauss
      CCA=CCA+Irange
      end do Aloop ! Igauss

! End of loop over gaussians
!
! End of routine I2ER_GSPDF
      return
      end subroutine I2ER_GSPDF
      end subroutine I2ER_SPDF
      subroutine I2ER_SSSS
!***********************************************************************
!     Date last modified: September 23, 1996               Version 2.0 *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!***********************************************************************
! Modules:
      USE mod_idfclc

      implicit none
!
! Local Scalars
      integer :: I
!
      call PRG_manager ('enter', 'I2ER_SSSS', 'UTILITY')

      include "start_atmshl_loop"

      case=1
      call DEF_shells (Jatmshl, Katmshl, Latmshl, Case)
      if((ABEXP+CDEXP).le.I2E_expcut)then
        Int_pointer=>IJKLs
        call I2ER_GSSSS
      end if

      if(LTWOINT)then
      case=2
      call DEF_shells (Latmshl, Jatmshl, Katmshl, Case)
      if((ABEXP+CDEXP).le.I2E_expcut)then
        Int_pointer=>ILJKs
        call I2ER_GSSSS
      end if
      end if

      if(Mtype.eq.1)then
      case=3
      call DEF_shells (Katmshl, Jatmshl, Latmshl, Case)
      if((ABEXP+CDEXP).le.I2E_expcut)then
        Int_pointer=>IKJLs
        call I2ER_GSSSS
      end if
      end if

      include "end_atmshl_loop"

! End of routine I2ER_SSSS
      call PRG_manager ('exit', 'I2ER_SSSS', 'UTILITY')
      return

CONTAINS !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine I2ER_GSSSS
!***********************************************************************
!     Date last modified: September 23, 1996               Version 2.0 *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!***********************************************************************
! Modules:

      implicit none
!
! Local Scalars
      integer :: Igauss,Jgauss,Kgauss,Lgauss
      double precision :: AS,BS,CS,DS
      double precision :: RHO,DXYZ,ZTEMP,RHOT2,ZCONST
      double precision :: PQX,PQY,PQZ,RPQSQ
      double precision :: EPAB,EPABI,EQCD,EQCDI,EABCD,EABCDI
      double precision :: CC4
!
! Begin:
! Commence loops over Gaussian expansion.
      include 'mungauss_gaussian_AB'
      include 'mungauss_gaussian_CD'

      IF(EXPARG.LE.I2E_expcut)then
      PQX=(CS*XC+DS*XD)*EQCDI-(AS*XA+BS*XB)*EPABI
      PQY=(CS*YC+DS*YD)*EQCDI-(AS*YA+BS*YB)*EPABI
      PQZ=(CS*ZC+DS*ZD)*EQCDI-(AS*ZA+BS*ZB)*EPABI

      RPQSQ=PQX*PQX+PQY*PQY+PQZ*PQZ
!
      EABCD=EPAB*EQCD
      EABCDI=ONE/(EPAB+EQCD)
      RHO=EABCD*EABCDI
      ZTEMP=PIconst*DEXP(-EXPARG)*DSQRT(EABCDI)/EABCD
!
      DXYZ=RHO*RPQSQ
      call RPOLX (1, DXYZ, TP, WP)
      ZCONST=ZTEMP*WP(1)
      IF(ZCONST.GT.I2E_PQCUT2)then
        CC4=GccA(CCA+1)*GccB(CCB+1)*GccC(CCC+1)*GccD(CCD+1)
        Int_pointer(1)=Int_pointer(1)+ZCONST*CC4
      end if ! ZCONST.GT.I2E_PQCUT2

      end if ! EXPARG.LE.I2E_expcut
      CCD=CCD+1
      end do Dloop ! Lgauss
      CCC=CCC+1
      end do Cloop ! Kgauss
      CCB=CCB+1
      end do Bloop ! Jgauss
      CCA=CCA+1
      end do Aloop ! Igauss
! End of loop over gaussians

! End of routine I2ER_SSSS
      return
      end subroutine I2ER_GSSSS
      end subroutine I2ER_SSSS
      subroutine I2ER_SSSP
!***********************************************************************
!     Date last modified: September 23, 1996               Version 2.0 *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!***********************************************************************
! Modules:
      USE mod_idfclc
!
      implicit none
! Local Scalars
!     integer I
      double precision :: RHO,TWORHO,DXYZ,ZTEMP,RHOT2,ZCONST
!
      call PRG_manager ('enter', 'I2ER_SSSP', 'UTILITY')

      include "start_atmshl_loop"

      case=1
      call DEF_shells (Jatmshl, Katmshl, Latmshl, Case)
      if((ABEXP+CDEXP).le.I2E_expcut)then
        Int_pointer=>IJKLs
        call I2ER_GSSSP
      end if

      if(LTWOINT)then
      case=2
      call DEF_shells (Latmshl, Jatmshl, Katmshl, Case)
      if((ABEXP+CDEXP).le.I2E_expcut)then
        Int_pointer=>ILJKs
        call I2ER_GSSSP
      end if
      end if

      if(Mtype.eq.1)then
      case=3
      call DEF_shells (Katmshl, Jatmshl, Latmshl, Case)
      if((ABEXP+CDEXP).le.I2E_expcut)then
        Int_pointer=>IKJLs
        call I2ER_GSSSP
      end if
      end if

      include "end_atmshl_loop"

! End of routine I2ER_SSSP
      call PRG_manager ('exit', 'I2ER_SSSP', 'UTILITY')
      return

CONTAINS!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine I2ER_GSSSP
!***********************************************************************
!     Date last modified: September 30, 1997               Version 2.0 *
!     Author: R.A. Poirier                                             *
!     Description:   Calculate sssp integrals (p-shell ONLY)           *
!***********************************************************************
! Modules:
!
      implicit none
!
! Local Scalars
      integer :: Igauss,Jgauss,Kgauss,Lgauss,NKL
      double precision :: AS,BS,CS,DS
      double precision :: A2DF,ARHOT2,CC4
      double precision :: CC1,CC2,CC3
      double precision :: CCQX,CCQY,CCQZ
      double precision :: PQX,PQY,PQZ,RHO,RPQSQ,EQCD,EQCDI,EQCDC,EPAB,EPABA,EPABI,EABCD,EABCDI
!
! Local Arrays
!
! Begin:
! Commence loops over Gaussian expansion.
      include 'mungauss_gaussian_AB'
      include 'mungauss_gaussian_CD'

      TQprim(1:3)=ZERO

      IF(EXPARG.LE.I2E_expcut)then
      if(LABmax.eq.1)then
      A2DF=-ONE/(EQCD+EQCD)
      if(LRCD)then
        EQCDC=CS*EQCDI
        CCQX=-EQCDC*(XD-XC)
        CCQY=-EQCDC*(YD-YC)
        CCQZ=-EQCDC*(ZD-ZC)
      else
        CCQX=ZERO
        CCQY=ZERO
        CCQZ=ZERO
      end if
      else
      A2DF=ONE/(EPAB+EPAB)
      if(LRAB)then
        EPABA=AS*EPABI
        CCQX=-EPABA*(XB-XA)
        CCQY=-EPABA*(YB-YA)
        CCQZ=-EPABA*(ZB-ZA)
      else
        CCQX=ZERO
        CCQY=ZERO
        CCQZ=ZERO
      end if
      end if
!
      PQX=(CS*XC+DS*XD)*EQCDI-(AS*XA+BS*XB)*EPABI
      PQY=(CS*YC+DS*YD)*EQCDI-(AS*YA+BS*YB)*EPABI
      PQZ=(CS*ZC+DS*ZD)*EQCDI-(AS*ZA+BS*ZB)*EPABI

      RPQSQ=PQX*PQX+PQY*PQY+PQZ*PQZ
!
      EABCD=EPAB*EQCD
      EABCDI=ONE/(EPAB+EQCD)
      RHO=EABCD*EABCDI
!
      DXYZ=RHO*RPQSQ
      call RPOLX (1, DXYZ, TP, WP)
      TWORHO=RHO+RHO
      ARHOT2=A2DF*TWORHO*TP(1)
      ZCONST=PIconst*DEXP(-EXPARG)*DSQRT(EABCDI)/EABCD*WP(1)
      IF(ZCONST.GT.I2E_PQCUT2)then
        TQprim(1)=TQprim(1)+(CCQX+ARHOT2*PQX)*ZCONST
        TQprim(2)=TQprim(2)+(CCQY+ARHOT2*PQY)*ZCONST
        TQprim(3)=TQprim(3)+(CCQZ+ARHOT2*PQZ)*ZCONST
      end if ! ZCONST.GT.I2E_PQCUT2
!
! Apply contraction coefficients. WHAT IF SPSS case!!!
      CC1=GccA(CCA+1)
      CC2=CC1*GccB(CCB+1)
      CC3=CC2*GccC(CCC+1)
      CC4=CC3*GccD(CCD+1)

      Int_pointer(1)=Int_pointer(1)+TQprim(1)*CC4
      Int_pointer(2)=Int_pointer(2)+TQprim(2)*CC4
      Int_pointer(3)=Int_pointer(3)+TQprim(3)*CC4

      end if ! EXPARG.LE.I2E_expcut
      CCD=CCD+Lrange
      end do Dloop ! Lgauss
      CCC=CCC+Krange
      end do Cloop ! Kgauss
      CCB=CCB+Jrange
      end do Bloop ! Jgauss
      CCA=CCA+1 ! Always an S
      end do Aloop ! Igauss
! End of loop over gaussians
      Lsort=.false.
!
! End of routine I2ER_SSSP
      return
      end subroutine I2ER_GSSSP
      end subroutine I2ER_SSSP
      subroutine I2ER_SPPP
!***********************************************************************
!     Date last modified: September 23, 1996               Version 2.0 *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!***********************************************************************
! Modules:
      USE mod_idfclc
!
      implicit none
!
! Local Scalars
!     integer I
      integer :: Iao,Jao,Kao,Lao
      double precision :: RHO,TWORHO,DXYZ,ZTEMP,RHOT2,ZCONST
!
! Local Arrays
      double precision :: G2DFX(13),G2DFY(13),G2DFZ(13),XIP(256),YIP(256),ZIP(256)
      double precision :: A2DF(174),CCPX(48),CCPY(48),CCPZ(48),CCQX(48),CCQY(48),CCQZ(48)
!
      call PRG_manager ('enter', 'I2ER_SPPP', 'UTILITY')

      include "start_atmshl_loop"

      case=1
      call DEF_shells (Jatmshl, Katmshl, Latmshl, Case)
      if((ABEXP+CDEXP).le.I2E_expcut)then
        Int_pointer=>IJKLs
        call I2ER_GSPPP
      end if

      if(LTWOINT)then
      case=2
      call DEF_shells (Latmshl, Jatmshl, Katmshl, Case)
      if((ABEXP+CDEXP).le.I2E_expcut)then
        Int_pointer=>ILJKs
        call I2ER_GSPPP
      end if
      end if

      if(Mtype.eq.1)then
      case=3
      call DEF_shells (Katmshl, Jatmshl, Latmshl, Case)
      if((ABEXP+CDEXP).le.I2E_expcut)then
        Int_pointer=>IKJLs
        call I2ER_GSPPP
      end if
      end if
      include "end_atmshl_loop"

! End of routine I2ER_SPPP
      call PRG_manager ('exit', 'I2ER_SPPP', 'UTILITY')
      return

CONTAINS!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine I2ER_GSPPP
!***********************************************************************
!     Date last modified: September 23, 1996               Version 2.0 *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!***********************************************************************
! Modules:
!
      implicit none
!
! Local Scalars
      integer :: COR_INDEX
      integer :: IZERO,INTC,IV,JendM,KendM,LendM,Igauss,Jgauss,Kgauss,Lgauss
      double precision :: CC4
      double precision :: AS,BS,CS,DS
      double precision :: EPAB,EPABI,EPABA,EPABB,EQCD,EQCDI,EQCDC,EQCDD,EABCD,EP2I,EABCDI
      double precision :: XAP,YAP,ZAP,XBP,YBP,ZBP,XCQ,YCQ,ZCQ,XDQ,YDQ,ZDQ,PQX,PQY,PQZ,RPQSQ
!
! Local Arrays
!
! Begin:
      A2DF(1:174)=ZERO
! For now!!
      G2DFX(1:4)=ZERO
      G2DFY(1:4)=ZERO
      G2DFZ(1:4)=ZERO
!
      CCPX(1:48)=ZERO
      CCPY(1:48)=ZERO
      CCPZ(1:48)=ZERO
      CCQX(1:48)=ZERO
      CCQY(1:48)=ZERO
      CCQZ(1:48)=ZERO
!
! Commence loops over Gaussian expansion.
      include 'mungauss_gaussian_AB'
      if(LRAB)then
        EPABA=AS*EPABI
        EPABB=BS*EPABI
        XAP= EPABB*(XB-XA)
        YAP= EPABB*(YB-YA)
        ZAP= EPABB*(ZB-ZA)
        XBP=-EPABA*(XB-XA)
        YBP=-EPABA*(YB-YA)
        ZBP=-EPABA*(ZB-ZA)
        call GETCC_XYZ (CCPX, CCPY, CCPZ, XAP, YAP, ZAP, XBP, YBP, ZBP, LAMAX, LBMAX)
      end if

      include 'mungauss_gaussian_CD'
      TQPrim(1:27)=ZERO

      IF(EXPARG.LE.I2E_expcut)then
      if(LRCD)then
        EQCDC=CS*EQCDI
        EQCDD=DS*EQCDI
        XCQ= EQCDD*(XD-XC)
        YCQ= EQCDD*(YD-YC)
        ZCQ= EQCDD*(ZD-ZC)
        XDQ=-EQCDC*(XD-XC)
        YDQ=-EQCDC*(YD-YC)
        ZDQ=-EQCDC*(ZD-ZC)
        call GETCC_XYZ (CCQX, CCQY, CCQZ, XCQ, YCQ, ZCQ, XDQ, YDQ, ZDQ, LCMAX, LDMAX)
      end if
!
      PQX=(CS*XC+DS*XD)*EQCDI-(AS*XA+BS*XB)*EPABI
      PQY=(CS*YC+DS*YD)*EQCDI-(AS*YA+BS*YB)*EPABI
      PQZ=(CS*ZC+DS*ZD)*EQCDI-(AS*ZA+BS*ZB)*EPABI

      RPQSQ=PQX*PQX+PQY*PQY+PQZ*PQZ
!
      EABCD=EPAB*EQCD
      EABCDI=ONE/(EPAB+EQCD)
      RHO=EABCD*EABCDI
      ZTEMP=PIconst*DEXP(-EXPARG)*DSQRT(EABCDI)/EABCD

      EP2I=ONE/(EPAB+EPAB)
      call IJKLA2 (A2DF, LABMAX, LCDMAX, EQCD, EP2I)
!
      DXYZ=RHO*RPQSQ
      call RPOLX (NZERO, DXYZ, TP, WP)
      TWORHO=RHO+RHO

      do IZERO=1,NZERO
      RHOT2=TWORHO*TP(IZERO)
      ZCONST=ZTEMP*WP(IZERO)
      if(ZCONST.le.I2E_PQCUT2)cycle
      G2DFX(1)=ONE
      G2DFY(1)=ONE
      G2DFZ(1)=ZCONST
      XIP(1)=ONE
      YIP(1)=ONE
      ZIP(1)=ZCONST
!
      G2DFX(2)=RHOT2*PQX
      G2DFY(2)=RHOT2*PQY
      G2DFZ(2)=RHOT2*PQZ*ZCONST
!
      G2DFX(3)=RHOT2*(PQX*G2DFX(2)-ONE)
      G2DFY(3)=RHOT2*(PQY*G2DFY(2)-ONE)
      G2DFZ(3)=RHOT2*(PQZ*G2DFZ(2)-ZCONST)
!
      G2DFX(4)=RHOT2*(PQX*G2DFX(3)-TWO*G2DFX(2))
      G2DFY(4)=RHOT2*(PQY*G2DFY(3)-TWO*G2DFY(2))
      G2DFZ(4)=RHOT2*(PQZ*G2DFZ(3)-TWO*G2DFZ(2))
!
! Find two-center integrals.
      if(LRABCD)then
        call SPPP4C
      else if(LRCD)then
        call SPPP3CQ
      else if(LRAB)then
        call SPPP3CP
      else
        call SPPP2C
      end if ! LRABCD

! Commence loop over atomic orbitals.
        TQprim( 1)=TQprim( 1)+(XIP(22)*YIP( 1)*ZIP( 1))
        TQprim( 2)=TQprim( 2)+(XIP(21)*YIP( 2)*ZIP( 1))
        TQprim( 3)=TQprim( 3)+(XIP(21)*YIP( 1)*ZIP( 2))
        TQprim( 4)=TQprim( 4)+(XIP(18)*YIP( 5)*ZIP( 1))
        TQprim( 5)=TQprim( 5)+(XIP(17)*YIP( 6)*ZIP( 1))
        TQprim( 6)=TQprim( 6)+(XIP(17)*YIP( 5)*ZIP( 2))
        TQprim( 7)=TQprim( 7)+(XIP(18)*YIP( 1)*ZIP( 5))
        TQprim( 8)=TQprim( 8)+(XIP(17)*YIP( 2)*ZIP( 5))
        TQprim( 9)=TQprim( 9)+(XIP(17)*YIP( 1)*ZIP( 6))
        TQprim(10)=TQprim(10)+(XIP( 6)*YIP(17)*ZIP( 1))
        TQprim(11)=TQprim(11)+(XIP( 5)*YIP(18)*ZIP( 1))
        TQprim(12)=TQprim(12)+(XIP( 5)*YIP(17)*ZIP( 2))
        TQprim(13)=TQprim(13)+(XIP( 2)*YIP(21)*ZIP( 1))
        TQprim(14)=TQprim(14)+(XIP( 1)*YIP(22)*ZIP( 1))
        TQprim(15)=TQprim(15)+(XIP( 1)*YIP(21)*ZIP( 2))
        TQprim(16)=TQprim(16)+(XIP( 2)*YIP(17)*ZIP( 5))
        TQprim(17)=TQprim(17)+(XIP( 1)*YIP(18)*ZIP( 5))
        TQprim(18)=TQprim(18)+(XIP( 1)*YIP(17)*ZIP( 6))
        TQprim(19)=TQprim(19)+(XIP( 6)*YIP( 1)*ZIP(17))
        TQprim(20)=TQprim(20)+(XIP( 5)*YIP( 2)*ZIP(17))
        TQprim(21)=TQprim(21)+(XIP( 5)*YIP( 1)*ZIP(18))
        TQprim(22)=TQprim(22)+(XIP( 2)*YIP( 5)*ZIP(17))
        TQprim(23)=TQprim(23)+(XIP( 1)*YIP( 6)*ZIP(17))
        TQprim(24)=TQprim(24)+(XIP( 1)*YIP( 5)*ZIP(18))
        TQprim(25)=TQprim(25)+(XIP( 2)*YIP( 1)*ZIP(21))
        TQprim(26)=TQprim(26)+(XIP( 1)*YIP( 2)*ZIP(21))
        TQprim(27)=TQprim(27)+(XIP( 1)*YIP( 1)*ZIP(22))
      end do ! IZERO

! Apply contraction coefficients.
      CC4=GccA(CCA+1)*GccB(CCB+1)*GccC(CCC+1)*GccD(CCD+1)
      do Jao=1,3
      do Kao=1,3
      LendM=3
      IF(Katmshl_EQ_Latmshl)LendM=Kao
      do Lao=1,LendM
        COR_INDEX=(Jao-1)*9+(Kao-1)*3+Lao
        Int_pointer(COR_INDEX)=Int_pointer(COR_INDEX)+TQPrim(COR_INDEX)*CC4
      end do ! Lao
      end do ! Kao
      end do ! Jao

      end if ! EXPARG.LE.I2E_expcut
      CCD=CCD+3
      end do Dloop ! Lgauss
      CCC=CCC+3
      end do Cloop ! Kgauss
      CCB=CCB+3
      end do Bloop ! Jgauss
      CCA=CCA+1
      end do Aloop ! Igauss
! End of loop over gaussians

! End of routine I2ER_SPPP
      return
      end subroutine I2ER_GSPPP
      subroutine SPPP4C

      implicit none
!
! Local arrays:
      double precision :: I2P(4),I3P(3)

      I2P(4)=A2DF(17)*G2DFx(3)
      I2P(3)=A2DF(16)*G2DFx(2)
      I2P(2)=A2DF(2)+A2DF(3)*G2DFx(3)
      I2P(1)=A2DF(1)*G2DFx(2)
      I3P(3)=CCQx(8)+CCQx(9)*I2P(1)+I2P(2)
      I3P(2)=CCQx(7)+I2P(1)
      I3P(1)=CCQx(1)+I2P(1)
      XIP(22)=CCPx(1)*I3P(3)+CCQx(8)*I2P(3)+CCQx(9)*I2P(4)+A2DF(18)*G2DFx(2)+A2DF(19)*G2DFx(4)
      XIP(21)=CCPx(1)*I3P(2)+CCQx(7)*I2P(3)+I2P(4)
      XIP(18)=CCPx(1)*I3P(1)+CCQx(1)*I2P(3)+I2P(4)
      XIP(17)=CCPx(1)+I2P(3)
      XIP(6)=I3P(3)
      XIP(5)=I3P(2)
      XIP(2)=I3P(1)
      XIP(1)=ONE

      I2P(4)=A2DF(17)*G2DFy(3)
      I2P(3)=A2DF(16)*G2DFy(2)
      I2P(2)=A2DF(2)+A2DF(3)*G2DFy(3)
      I2P(1)=A2DF(1)*G2DFy(2)
      I3P(3)=CCQy(8)+CCQy(9)*I2P(1)+I2P(2)
      I3P(2)=CCQy(7)+I2P(1)
      I3P(1)=CCQy(1)+I2P(1)
      YIP(22)=CCPy(1)*I3P(3)+CCQy(8)*I2P(3)+CCQy(9)*I2P(4)+A2DF(18)*G2DFy(2)+A2DF(19)*G2DFy(4)
      YIP(21)=CCPy(1)*I3P(2)+CCQy(7)*I2P(3)+I2P(4)
      YIP(18)=CCPy(1)*I3P(1)+CCQy(1)*I2P(3)+I2P(4)
      YIP(17)=CCPy(1)+I2P(3)
      YIP(6)=I3P(3)
      YIP(5)=I3P(2)
      YIP(2)=I3P(1)
      YIP(1)=ONE

      I2P(4)=A2DF(17)*G2DFz(3)
      I2P(3)=A2DF(16)*G2DFz(2)
      I2P(2)=A2DF(2)*ZCONST+A2DF(3)*G2DFz(3)
      I2P(1)=A2DF(1)*G2DFz(2)
      I3P(3)=CCQz(8)*ZCONST+CCQz(9)*I2P(1)+I2P(2)
      I3P(2)=CCQz(7)*ZCONST+I2P(1)
      I3P(1)=CCQz(1)*ZCONST+I2P(1)
      ZIP(22)=CCPz(1)*I3P(3)+CCQz(8)*I2P(3)+CCQz(9)*I2P(4)+A2DF(18)*G2DFz(2)+A2DF(19)*G2DFz(4)
      ZIP(21)=CCPz(1)*I3P(2)+CCQz(7)*I2P(3)+I2P(4)
      ZIP(18)=CCPz(1)*I3P(1)+CCQz(1)*I2P(3)+I2P(4)
      ZIP(17)=CCPz(1)*ZCONST+I2P(3)
      ZIP(6)=I3P(3)
      ZIP(5)=I3P(2)
      ZIP(2)=I3P(1)
      ZIP(1)=ZCONST

      return
      end subroutine SPPP4C
      subroutine SPPP3CP

      implicit none
!
! Local arrays:
      double precision :: I2P(4)

      I2P(4)=A2DF(17)*G2DFx(3)
      I2P(3)=A2DF(16)*G2DFx(2)
      I2P(2)=A2DF(2)+A2DF(3)*G2DFx(3)
      I2P(1)=A2DF(1)*G2DFx(2)
      XIP(22)=CCPx(1)*I2P(2)+A2DF(18)*G2DFx(2)+A2DF(19)*G2DFx(4)
      XIP(21)=CCPx(1)*I2P(1)+I2P(4)
      XIP(18)=CCPx(1)*I2P(1)+I2P(4)
      XIP(17)=CCPx(1)+I2P(3)
      XIP(6)=I2P(2)
      XIP(5)=I2P(1)
      XIP(2)=I2P(1)
      XIP(1)=ONE

      I2P(4)=A2DF(17)*G2DFy(3)
      I2P(3)=A2DF(16)*G2DFy(2)
      I2P(2)=A2DF(2)+A2DF(3)*G2DFy(3)
      I2P(1)=A2DF(1)*G2DFy(2)
      YIP(22)=CCPy(1)*I2P(2)+A2DF(18)*G2DFy(2)+A2DF(19)*G2DFy(4)
      YIP(21)=CCPy(1)*I2P(1)+I2P(4)
      YIP(18)=CCPy(1)*I2P(1)+I2P(4)
      YIP(17)=CCPy(1)+I2P(3)
      YIP(6)=I2P(2)
      YIP(5)=I2P(1)
      YIP(2)=I2P(1)
      YIP(1)=ONE

      I2P(4)=A2DF(17)*G2DFz(3)
      I2P(3)=A2DF(16)*G2DFz(2)
      I2P(2)=A2DF(2)*ZCONST+A2DF(3)*G2DFz(3)
      I2P(1)=A2DF(1)*G2DFz(2)
      ZIP(22)=CCPz(1)*I2P(2)+A2DF(18)*G2DFz(2)+A2DF(19)*G2DFz(4)
      ZIP(21)=CCPz(1)*I2P(1)+I2P(4)
      ZIP(18)=CCPz(1)*I2P(1)+I2P(4)
      ZIP(17)=CCPz(1)*ZCONST+I2P(3)
      ZIP(6)=I2P(2)
      ZIP(5)=I2P(1)
      ZIP(2)=I2P(1)
      ZIP(1)=ZCONST

      return
      end subroutine SPPP3CP
      subroutine SPPP3CQ

      implicit none
!
! Local arrays:
      double precision :: I2P(3)

      I2P(3)=A2DF(17)*G2DFx(3)
      I2P(2)=A2DF(16)*G2DFx(2)
      I2P(1)=A2DF(1)*G2DFx(2)
      XIP(22)=CCQx(8)*I2P(2)+CCQx(9)*I2P(3)+A2DF(18)*G2DFx(2)+A2DF(19)*G2DFx(4)
      XIP(21)=CCQx(7)*I2P(2)+I2P(3)
      XIP(18)=CCQx(1)*I2P(2)+I2P(3)
      XIP(17)=I2P(2)
      XIP(6)=CCQx(8)+CCQx(9)*I2P(1)+A2DF(2)+A2DF(3)*G2DFx(3)
      XIP(5)=CCQx(7)+I2P(1)
      XIP(2)=CCQx(1)+I2P(1)
      XIP(1)=ONE

      I2P(3)=A2DF(17)*G2DFy(3)
      I2P(2)=A2DF(16)*G2DFy(2)
      I2P(1)=A2DF(1)*G2DFy(2)
      YIP(22)=CCQy(8)*I2P(2)+CCQy(9)*I2P(3)+A2DF(18)*G2DFy(2)+A2DF(19)*G2DFy(4)
      YIP(21)=CCQy(7)*I2P(2)+I2P(3)
      YIP(18)=CCQy(1)*I2P(2)+I2P(3)
      YIP(17)=I2P(2)
      YIP(6)=CCQy(8)+CCQy(9)*I2P(1)+A2DF(2)+A2DF(3)*G2DFy(3)
      YIP(5)=CCQy(7)+I2P(1)
      YIP(2)=CCQy(1)+I2P(1)
      YIP(1)=ONE

      I2P(3)=A2DF(17)*G2DFz(3)
      I2P(2)=A2DF(16)*G2DFz(2)
      I2P(1)=A2DF(1)*G2DFz(2)
      ZIP(22)=CCQz(8)*I2P(2)+CCQz(9)*I2P(3)+A2DF(18)*G2DFz(2)+A2DF(19)*G2DFz(4)
      ZIP(21)=CCQz(7)*I2P(2)+I2P(3)
      ZIP(18)=CCQz(1)*I2P(2)+I2P(3)
      ZIP(17)=I2P(2)
      ZIP(6)=CCQz(8)*ZCONST+CCQz(9)*I2P(1)+A2DF(2)*ZCONST+A2DF(3)*G2DFz(3)
      ZIP(5)=CCQz(7)*ZCONST+I2P(1)
      ZIP(2)=CCQz(1)*ZCONST+I2P(1)
      ZIP(1)=ZCONST

      return
      end subroutine SPPP3CQ
      subroutine SPPP2C

      implicit none
!
! Local arrays:
      double precision :: I2P(2)

      I2P(2)=A2DF(17)*G2DFx(3)
      I2P(1)=A2DF(1)*G2DFx(2)
      XIP(22)=A2DF(18)*G2DFx(2)+A2DF(19)*G2DFx(4)
      XIP(21)=I2P(2)
      XIP(18)=I2P(2)
      XIP(17)=A2DF(16)*G2DFx(2)
      XIP(6)=A2DF(2)+A2DF(3)*G2DFx(3)
      XIP(5)=I2P(1)
      XIP(2)=I2P(1)
      XIP(1)=ONE

      I2P(2)=A2DF(17)*G2DFy(3)
      I2P(1)=A2DF(1)*G2DFy(2)
      YIP(22)=A2DF(18)*G2DFy(2)+A2DF(19)*G2DFy(4)
      YIP(21)=I2P(2)
      YIP(18)=I2P(2)
      YIP(17)=A2DF(16)*G2DFy(2)
      YIP(6)=A2DF(2)+A2DF(3)*G2DFy(3)
      YIP(5)=I2P(1)
      YIP(2)=I2P(1)
      YIP(1)=ONE

      I2P(2)=A2DF(17)*G2DFz(3)
      I2P(1)=A2DF(1)*G2DFz(2)
      ZIP(22)=A2DF(18)*G2DFz(2)+A2DF(19)*G2DFz(4)
      ZIP(21)=I2P(2)
      ZIP(18)=I2P(2)
      ZIP(17)=A2DF(16)*G2DFz(2)
      ZIP(6)=A2DF(2)*ZCONST+A2DF(3)*G2DFz(3)
      ZIP(5)=I2P(1)
      ZIP(2)=I2P(1)
      ZIP(1)=ZCONST

      return
      end subroutine SPPP2C
      end subroutine I2ER_SPPP
      subroutine I2ER_PPPP
!***********************************************************************
!     Date last modified: September 23, 1996               Version 2.0 *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!***********************************************************************
! Modules:
      USE mod_idfclc
!
      implicit none
!
! Local Scalars
      integer I
!
      call PRG_manager ('enter', 'I2ER_PPPP', 'UTILITY')

      include "start_atmshl_loop"

      case=1
      call DEF_shells (Jatmshl, Katmshl, Latmshl, Case)
      if((ABEXP+CDEXP).le.I2E_expcut)then
        Int_pointer=>IJKLs
        call I2ER_GPPPP
      end if

      if(LTWOINT)then
      case=2
      call DEF_shells (Latmshl, Jatmshl, Katmshl, Case)
      if((ABEXP+CDEXP).le.I2E_expcut)then
        Int_pointer=>ILJKs
        call I2ER_GPPPP
      end if
      end if

      if(Mtype.eq.1)then
      case=3
      call DEF_shells (Katmshl, Jatmshl, Latmshl, Case)
      if((ABEXP+CDEXP).le.I2E_expcut)then
        Int_pointer=>IKJLs
        call I2ER_GPPPP
      end if
      end if
      include "end_atmshl_loop"

! End of routine I2ER_PPPP
      call PRG_manager ('exit', 'I2ER_PPPP', 'UTILITY')
      return

CONTAINS !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine I2ER_GPPPP
!***********************************************************************
!     Date last modified: September 23, 1996               Version 2.0 *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!***********************************************************************
! Modules:
!
      implicit none
!
! Local Scalars
      integer          :: COR_INDEX
      integer :: IX,IY,IZ,JX,JY,JZ,KX,KY,KZ,LX,LY,LZ
      integer :: Iao,Jao,Kao,Lao
      integer :: IZERO,INTC,IV,JendM,KendM,LendM,Igauss,Jgauss,Kgauss,Lgauss
      double precision :: CC4, XINT(11)
      double precision :: AS,BS,CS,DS
      double precision :: RHO,TWORHO,DXYZ,ZTEMP,RHOT2,ZCONST
      double precision :: EPAB,EPABI,EPABA,EPABB,EQCD,EQCDI,EQCDC,EQCDD,EABCD,EP2I,EABCDI
      double precision :: XAP,YAP,ZAP,XBP,YBP,ZBP,XCQ,YCQ,ZCQ,XDQ,YDQ,ZDQ,PQX,PQY,PQZ,RPQSQ
!
! Local Arrays
      double precision :: G2DFX(13),G2DFY(13),G2DFZ(13),XIP(256),YIP(256),ZIP(256)
      double precision :: A2DF(174),CCPX(48),CCPY(48),CCPZ(48),CCQX(48),CCQY(48),CCQZ(48)
      integer INDIX(4),INDIY(4),INDIZ(4),INDJX(4),INDJY(4),INDJZ(4),INDKX(4),INDKY(4),INDKZ(4),INDLX(4), &
              INDLY(4),INDLZ(4)
      DATA INDIX/0,64,0,0/,INDIY/0,0,64,0/,INDIZ/0,0,0,64/,INDJX/0,16,0,0/,INDJY/0,0,16,0/,INDJZ/0,0,0,16/, &
           INDKX/0,4,0,0/,INDKY/0,0,4,0/,INDKZ/0,0,0,4/,INDLX/1,2,1,1/,INDLY/1,1,2,1/,INDLZ/1,1,1,2/, &
           XINT/1.0D0,2.0D0,3.0D0,4.0D0,5.0D0,6.0D0,7.0D0,8.0D0,9.0D0,10.0D0,11.0D0/
!
! Begin:
      A2DF(1:174)=ZERO
      CCPX(1:48)=ZERO
      CCPY(1:48)=ZERO
      CCPZ(1:48)=ZERO
      CCQX(1:48)=ZERO
      CCQY(1:48)=ZERO
      CCQZ(1:48)=ZERO
!
! Commence loops over Gaussian expansion.
      include 'mungauss_gaussian_AB'
      if(LRAB)then
        EPABA=AS*EPABI
        EPABB=BS*EPABI
        XAP= EPABB*(XB-XA)
        YAP= EPABB*(YB-YA)
        ZAP= EPABB*(ZB-ZA)
        XBP=-EPABA*(XB-XA)
        YBP=-EPABA*(YB-YA)
        ZBP=-EPABA*(ZB-ZA)
        call GETCC_XYZ (CCPX, CCPY, CCPZ, XAP, YAP, ZAP, XBP, YBP, ZBP, LAMAX, LBMAX)
      end if

      include 'mungauss_gaussian_CD'
      TQprim(1:81)=ZERO

      IF(EXPARG.LE.I2E_expcut)then
      if(LRCD)then
        EQCDC=CS*EQCDI
        EQCDD=DS*EQCDI
        XCQ= EQCDD*(XD-XC)
        YCQ= EQCDD*(YD-YC)
        ZCQ= EQCDD*(ZD-ZC)
        XDQ=-EQCDC*(XD-XC)
        YDQ=-EQCDC*(YD-YC)
        ZDQ=-EQCDC*(ZD-ZC)
        call GETCC_XYZ (CCQX, CCQY, CCQZ, XCQ, YCQ, ZCQ, XDQ, YDQ, ZDQ, LCMAX, LDMAX)
      end if
!
      PQX=(CS*XC+DS*XD)*EQCDI-(AS*XA+BS*XB)*EPABI
      PQY=(CS*YC+DS*YD)*EQCDI-(AS*YA+BS*YB)*EPABI
      PQZ=(CS*ZC+DS*ZD)*EQCDI-(AS*ZA+BS*ZB)*EPABI
      RPQSQ=PQX*PQX+PQY*PQY+PQZ*PQZ
!
      EABCD=EPAB*EQCD
      EABCDI=ONE/(EPAB+EQCD)
      RHO=EABCD*EABCDI
      ZTEMP=PIconst*DEXP(-EXPARG)*DSQRT(EABCDI)/EABCD

      EP2I=ONE/(EPAB+EPAB)
      call IJKLA2 (A2DF, LABMAX, LCDMAX, EQCD, EP2I)
!
      DXYZ=RHO*RPQSQ
      call RPOLX (NZERO, DXYZ, TP, WP)
      TWORHO=RHO+RHO

      do IZERO=1,NZERO
      RHOT2=TWORHO*TP(IZERO)
      ZCONST=ZTEMP*WP(IZERO)
      if(ZCONST.le.I2E_PQCUT2)cycle
      G2DFX(1)=ONE
      G2DFY(1)=ONE
      G2DFZ(1)=ZCONST
!
      G2DFX(2)=RHOT2*PQX
      G2DFY(2)=RHOT2*PQY
      G2DFZ(2)=RHOT2*PQZ*ZCONST
!
      do IV=3,LPQMAX
        G2DFX(IV)=RHOT2*(PQX*G2DFX(IV-1)-XINT(IV-2)*G2DFX(IV-2))
        G2DFY(IV)=RHOT2*(PQY*G2DFY(IV-1)-XINT(IV-2)*G2DFY(IV-2))
        G2DFZ(IV)=RHOT2*(PQZ*G2DFZ(IV-1)-XINT(IV-2)*G2DFZ(IV-2))
      end do
! Find two-center integrals.
!
      XIP(1)=ONE
      YIP(1)=ONE
      ZIP(1)=ZCONST
      if(LRABCD)then
        call PPPP4C (A2DF, G2DFX, CCQX, CCPX, XIP)
        call PPPP4C (A2DF, G2DFY, CCQY, CCPY, YIP)
        call PPPP4C (A2DF, G2DFZ, CCQZ, CCPZ, ZIP)
      else if(LRCD)then
        call PPPP3CQ (A2DF, G2DFX, CCQX, XIP)
        call PPPP3CQ (A2DF, G2DFY, CCQY, YIP)
        call PPPP3CQ (A2DF, G2DFZ, CCQZ, ZIP)
      else if(LRAB)then
        call PPPP3CP (A2DF, G2DFX, CCPX, XIP)
        call PPPP3CP (A2DF, G2DFY, CCPY, YIP)
        call PPPP3CP (A2DF, G2DFZ, CCPZ, ZIP)
      else
        call PPPP2C (A2DF, G2DFX, XIP)
        call PPPP2C (A2DF, G2DFY, YIP)
        call PPPP2C (A2DF, G2DFZ, ZIP)
      end if ! LRABCD
!
! Commence loop over atomic orbitals.
      INTC=0
      JendM=Jend
      KendM=Kend
      do Iao=Istart,Iend
        IF(Iatmshl_EQ_Jatmshl)JendM=Iao
        IF(IJIJ)KendM=Iao
        IX=INDIX(Iao)
        IY=INDIY(Iao)
        IZ=INDIZ(Iao)
      do Jao=Jstart,JendM
        JX=INDJX(Jao)+IX
        JY=INDJY(Jao)+IY
        JZ=INDJZ(Jao)+IZ
      do Kao=Kstart,KendM
        LendM=Lend
        IF(Katmshl_EQ_Latmshl)LendM=Kao
        IF(IJIJ.and.Iao.EQ.Kao)LendM=Jao
        KX=INDKX(Kao)+JX
        KY=INDKY(Kao)+JY
        KZ=INDKZ(Kao)+JZ
      do Lao=Lstart,LendM
        LX=INDLX(Lao)+KX
        LY=INDLY(Lao)+KY
        LZ=INDLZ(Lao)+KZ
        INTC=INTC+1
        TQprim(INTC)=TQprim(INTC)+(XIP(LX)*YIP(LY)*ZIP(LZ))
      end do ! L
      end do ! K
      end do ! J
      end do ! I
!
      end do ! IZERO

! Apply contraction coefficients.
      INTC=0
      JendM=3
      KendM=3
      CC4=GccA(CCA+1)*GccB(CCB+1)*GccC(CCC+1)*GccD(CCD+1)
      do Iao=1,3
      IF(Iatmshl_EQ_Jatmshl)JendM=Iao
      IF(IJIJ)KendM=Iao
      do Jao=1,JendM
      do Kao=1,KendM
      LendM=3
      IF(Katmshl_EQ_Latmshl)LendM=Kao
      IF(IJIJ.and.Iao.EQ.Kao)LendM=Jao
      do Lao=1,LendM
        INTC=INTC+1
        COR_INDEX=(Iao-1)*27+(Jao-1)*9+(Kao-1)*3+Lao
        Int_pointer(COR_INDEX)=Int_pointer(COR_INDEX)+TQprim(INTC)*CC4
      end do ! Lao
      end do ! Kao
      end do ! Jao
      end do ! Iao

      end if ! EXPARG.LE.I2E_expcut
      CCD=CCD+3
      end do Dloop ! Lgauss
      CCC=CCC+3
      end do Cloop ! Kgauss
      CCB=CCB+3
      end do Bloop ! Jgauss
      CCA=CCA+3
      end do Aloop ! Igauss
! End of loop over gaussians
!
! End of routine I2ER_PPPP
      return
      end subroutine I2ER_GPPPP
      end subroutine I2ER_PPPP
      subroutine PPPP4C (A2,          & ! Input
                         G2,          & ! Input
                         CCQ,        & ! Input
                         CCP,        & ! Input
                         WIP)       ! Output
!***********************************************************************
!     Date last modified: January 29, 1997                 Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!     Specialized version of ABCD4C                                    *
!***********************************************************************
! Modules:

      implicit none
!
! Input arrays:
      double precision A2(174),CCQ(48),CCP(48),G2(13)
!
! Output array:
      double precision WIP(256)
!
! Local arrays:
      double precision I2P(17),I3P(38)
!
! Begin:
!
      I2P(17)=A2(36)*G2(1)+A2(37)*G2(3)+A2(38)*G2(5)
      I2P(16)=A2(34)*G2(2)+A2(35)*G2(4)
      I2P(15)=A2(32)*G2(1)+A2(33)*G2(3)
      I2P(10)=A2(18)*G2(2)+A2(19)*G2(4)
      I2P(9)=A2(17)*G2(3)
      I2P(8)=A2(16)*G2(2)
      I2P(3)=A2(2)*G2(1)+A2(3)*G2(3)
      I2P(2)=A2(1)*G2(2)
      I2P(1)=G2(1)

      I3P(38)=CCQ(8)*I2P(15)+CCQ(9)*I2P(16)+I2P(17)
      I3P(37)=CCQ(7)*I2P(15)+I2P(16)
      I3P(34)=CCQ(1)*I2P(15)+I2P(16)

      I3P(22)=CCQ(8)*I2P(8)+CCQ(9)*I2P(9)+I2P(10)
      I3P(21)=CCQ(7)*I2P(8)+I2P(9)
      I3P(18)=CCQ(1)*I2P(8)+I2P(9)

      I3P(6)=CCQ(8)*I2P(1)+CCQ(9)*I2P(2)+I2P(3)
      I3P(5)=CCQ(7)*I2P(1)+I2P(2)
      I3P(2)=CCQ(1)*I2P(1)+I2P(2)

      WIP(86)=CCP(8)*I3P(6)+CCP(9)*I3P(22)+I3P(38)
      WIP(85)=CCP(8)*I3P(5)+CCP(9)*I3P(21)+I3P(37)
      WIP(82)=CCP(8)*I3P(2)+CCP(9)*I3P(18)+I3P(34)
      WIP(81)=CCP(8)*I2P(1)+CCP(9)*I2P(8)+I2P(15)
      WIP(70)=CCP(7)*I3P(6)+I3P(22)
      WIP(69)=CCP(7)*I3P(5)+I3P(21)
      WIP(66)=CCP(7)*I3P(2)+I3P(18)
      WIP(65)=CCP(7)*I2P(1)+I2P(8)
      WIP(22)=CCP(1)*I3P(6)+I3P(22)
      WIP(21)=CCP(1)*I3P(5)+I3P(21)
      WIP(18)=CCP(1)*I3P(2)+I3P(18)
      WIP(17)=CCP(1)*I2P(1)+I2P(8)
      WIP(6)=I3P(6)
      WIP(5)=I3P(5)
      WIP(2)=I3P(2)
      WIP(1)=G2(1)
!
! End of routine PPPP4C
      return
      end subroutine PPPP4C
      subroutine PPPP3CP (A2,          & ! Input
                          G2,          & ! Input
                          CCP,        & ! Input
                          WIP)       ! Output
!***********************************************************************
!     Date last modified: January 29, 1997                 Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!     Specialized version of ABCC3C                                    *
!                                                                      *
!***********************************************************************
! Modules:

      implicit none
!
! Input arrays:
      double precision A2(174),CCP(48),G2(13)
!
! Output array:
      double precision WIP(256)
!
! Local arrays:
      double precision I2P(16)
!
! Begin:
!
      I2P(16)=A2(34)*G2(2)+A2(35)*G2(4)
      I2P(15)=A2(32)*G2(1)+A2(33)*G2(3)
      I2P(10)=A2(18)*G2(2)+A2(19)*G2(4)
      I2P(9)=A2(17)*G2(3)
      I2P(8)=A2(16)*G2(2)
      I2P(3)=A2(2)*G2(1)+A2(3)*G2(3)
      I2P(2)=A2(1)*G2(2)
      I2P(1)=G2(1)

      WIP(86)=CCP(8)*I2P(3)+CCP(9)*I2P(10)+ &
                A2(36)*G2(1)+A2(37)*G2(3)+A2(38)*G2(5)
      WIP(85)=CCP(8)*I2P(2)+CCP(9)*I2P(9)+I2P(16)
      WIP(82)=CCP(8)*I2P(2)+CCP(9)*I2P(9)+I2P(16)
      WIP(81)=CCP(8)*I2P(1)+CCP(9)*I2P(8)+I2P(15)
      WIP(70)=CCP(7)*I2P(3)+I2P(10)
      WIP(69)=CCP(7)*I2P(2)+I2P(9)
      WIP(66)=CCP(7)*I2P(2)+I2P(9)
      WIP(65)=CCP(7)*I2P(1)+I2P(8)
      WIP(22)=CCP(1)*I2P(3)+I2P(10)
      WIP(21)=CCP(1)*I2P(2)+I2P(9)
      WIP(18)=CCP(1)*I2P(2)+I2P(9)
      WIP(17)=CCP(1)*I2P(1)+I2P(8)
      WIP(6)=I2P(3)
      WIP(5)=I2P(2)
      WIP(2)=I2P(2)
      WIP(1)=G2(1)
!
! End of routine PPPP3CP
      return
      end subroutine PPPP3CP
      subroutine PPPP3CQ (A2,         & ! Input
                          G2,         & ! Input
                          CCQ,        & ! Input
                          WIP)       ! Output
!***********************************************************************
!     Date last modified: January 29, 1997                 Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!     Specialized version of AACD3C                                    *
! 								       *
!***********************************************************************
! Modules:

      implicit none
!
! Input arrays:
      double precision A2(174),CCQ(48),G2(13)
!
! Output array:
      double precision WIP(256)
!
! Local arrays:
      double precision I2P(17),I3P(22)
!
! Begin:
!
      I2P(17)=A2(36)*G2(1)+A2(37)*G2(3)+A2(38)*G2(5)
      I2P(16)=A2(34)*G2(2)+A2(35)*G2(4)
      I2P(15)=A2(32)*G2(1)+A2(33)*G2(3)
      I2P(10)=A2(18)*G2(2)+A2(19)*G2(4)
      I2P(9)=A2(17)*G2(3)
      I2P(8)=A2(16)*G2(2)
      I2P(3)=A2(2)*G2(1)+A2(3)*G2(3)
      I2P(2)=A2(1)*G2(2)
      I2P(1)=G2(1)

      I3P(22)=CCQ(8)*I2P(8)+CCQ(9)*I2P(9)+I2P(10)
      I3P(21)=CCQ(7)*I2P(8)+I2P(9)
      I3P(18)=CCQ(1)*I2P(8)+I2P(9)

      WIP(86)=CCQ(8)*I2P(15)+CCQ(9)*I2P(16)+I2P(17)
      WIP(85)=CCQ(7)*I2P(15)+I2P(16)
      WIP(82)=CCQ(1)*I2P(15)+I2P(16)
      WIP(81)=I2P(15)
      WIP(70)=I3P(22)
      WIP(69)=I3P(21)
      WIP(66)=I3P(18)
      WIP(65)=I2P(8)
      WIP(22)=I3P(22)
      WIP(21)=I3P(21)
      WIP(18)=I3P(18)
      WIP(17)=I2P(8)
      WIP(6)=CCQ(8)*I2P(1)+CCQ(9)*I2P(2)+I2P(3)
      WIP(5)=CCQ(7)*I2P(1)+I2P(2)
      WIP(2)=CCQ(1)*I2P(1)+I2P(2)
      WIP(1)=G2(1)
!
! End of routine PPPP3CQ
      return
      end subroutine PPPP3CQ
      subroutine PPPP2C (A2,         & ! Input
                         G2,         & ! Input
                         WIP)       ! Output
!***********************************************************************
!     Date last modified: January 29, 1997                 Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!     Specialized version of AABB2C                                    *
!                                                                      *
!***********************************************************************
! Modules:

      implicit none
!
! Input arrays:
      double precision A2(174),G2(13)
!
! Output array:
      double precision WIP(256)
!
! Begin:
!
      WIP(86)=A2(36)*G2(1)+A2(37)*G2(3)+A2(38)*G2(5)
      WIP(85)=A2(34)*G2(2)+A2(35)*G2(4)
      WIP(82)=A2(34)*G2(2)+A2(35)*G2(4)
      WIP(81)=A2(32)*G2(1)+A2(33)*G2(3)
      WIP(70)=A2(18)*G2(2)+A2(19)*G2(4)
      WIP(69)=A2(17)*G2(3)
      WIP(66)=A2(17)*G2(3)
      WIP(65)=A2(16)*G2(2)
      WIP(22)=A2(18)*G2(2)+A2(19)*G2(4)
      WIP(21)=A2(17)*G2(3)
      WIP(18)=A2(17)*G2(3)
      WIP(17)=A2(16)*G2(2)
      WIP(6)=A2(2)*G2(1)+A2(3)*G2(3)
      WIP(5)=A2(1)*G2(2)
      WIP(2)=A2(1)*G2(2)
      WIP(1)=G2(1)
!
! End of routine PPPP2C
      return
      end subroutine PPPP2C
      subroutine I2ER_SSPP
!***********************************************************************
!     Date last modified: September 23, 1996               Version 2.0 *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!***********************************************************************
! Modules:
      USE mod_idfclc
!
      implicit none
!
! Local Scalars
!     integer I
      integer :: IZERO,INTC,IV,JendM,KendM,LendM,Igauss,Jgauss,Kgauss,Lgauss
      double precision :: RHO,TWORHO,DXYZ,ZTEMP,RHOT2,ZCONST
!
! Local Arrays
      double precision :: A2DF(174),CCPX(48),CCPY(48),CCPZ(48),CCQX(48),CCQY(48),CCQZ(48)
      double precision :: G2DFX(13),G2DFY(13),G2DFZ(13),XIP(256),YIP(256),ZIP(256)
!
      call PRG_manager ('enter', 'I2ER_SSPP', 'UTILITY')

      include "start_atmshl_loop"

      case=1
      call DEF_shells (Jatmshl, Katmshl, Latmshl, Case)
      if((ABEXP+CDEXP).le.I2E_expcut)then
        Int_pointer=>IJKLs
        call I2ER_GSSPP
      end if

      if(LTWOINT)then
      case=2
      call DEF_shells (Latmshl, Jatmshl, Katmshl, Case)
      if((ABEXP+CDEXP).le.I2E_expcut)then
        Int_pointer=>ILJKs
        call I2ER_GSSPP
      end if
      end if

      if(Mtype.eq.1)then
      case=3
      call DEF_shells (Katmshl, Jatmshl, Latmshl, Case)
      if((ABEXP+CDEXP).le.I2E_expcut)then
        Int_pointer=>IKJLs
        call I2ER_GSSPP
      end if
      end if
      include "end_atmshl_loop"

! End of routine I2ER_SSPP
      call PRG_manager ('exit', 'I2ER_SSPP', 'UTILITY')
      return

CONTAINS !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine I2ER_GSSPP
!***********************************************************************
!     Date last modified: September 30, 1997               Version 2.0 *
!     Author: R.A. Poirier                                             *
!     Description:   Calculate sspp integrals (p-shell ONLY)           *
!***********************************************************************
! Modules:
!
      implicit none
!
! Local Scalars
      integer :: COR_INDEX
      integer :: Iao,Jao,Kao,Lao
      double precision :: CC4,EQ2I
      double precision :: AS,BS,CS,DS
      integer :: IZERO,INTC,IV,JendM,KendM,LendM,Igauss,Jgauss,Kgauss,Lgauss,NKL
      double precision :: EPAB,EPABI,EPABA,EPABB,EQCD,EQCDI,EQCDC,EQCDD,EABCD,EP2I,EABCDI
      double precision :: XAP,YAP,ZAP,XBP,YBP,ZBP,XCQ,YCQ,ZCQ,XDQ,YDQ,ZDQ,PQX,PQY,PQZ,RPQSQ
!
! Local Arrays
!
! Begin:
      NZERO=2
      TQ(1:9)=ZERO
      CCPx(1:48)=ZERO
      CCPy(1:48)=ZERO
      CCPz(1:48)=ZERO
      CCQx(1:48)=ZERO
      CCQy(1:48)=ZERO
      CCQz(1:48)=ZERO

! Commence loops over Gaussian expansion.
      include 'mungauss_gaussian_AB'
      if(LRAB)then
        EPABA=AS*EPABI
        EPABB=BS*EPABI
        XAP= EPABB*(XB-XA)
        YAP= EPABB*(YB-YA)
        ZAP= EPABB*(ZB-ZA)
        XBP=-EPABA*(XB-XA)
        YBP=-EPABA*(YB-YA)
        ZBP=-EPABA*(ZB-ZA)
        call GETCC_XYZ (CCPX, CCPY, CCPZ, XAP, YAP, ZAP, XBP, YBP, ZBP, LAMAX, LBMAX)
      end if

      include 'mungauss_gaussian_CD'
      TQprim(1:9)=ZERO

      IF(EXPARG.LE.I2E_expcut)then
      if(LRCD)then
        EQCDC=CS*EQCDI
        EQCDD=DS*EQCDI
        XCQ= EQCDD*(XD-XC)
        YCQ= EQCDD*(YD-YC)
        ZCQ= EQCDD*(ZD-ZC)
        XDQ=-EQCDC*(XD-XC)
        YDQ=-EQCDC*(YD-YC)
        ZDQ=-EQCDC*(ZD-ZC)
        call GETCC_XYZ (CCQX, CCQY, CCQZ, XCQ, YCQ, ZCQ, XDQ, YDQ, ZDQ, LCMAX, LDMAX)
      end if
!
      PQX=(CS*XC+DS*XD)*EQCDI-(AS*XA+BS*XB)*EPABI
      PQY=(CS*YC+DS*YD)*EQCDI-(AS*YA+BS*YB)*EPABI
      PQZ=(CS*ZC+DS*ZD)*EQCDI-(AS*ZA+BS*ZB)*EPABI

      RPQSQ=PQX*PQX+PQY*PQY+PQZ*PQZ
!
      EABCD=EPAB*EQCD
      EABCDI=ONE/(EPAB+EQCD)
      RHO=EABCD*EABCDI

      ZTEMP=PIconst*DEXP(-EXPARG)*DSQRT(EABCDI)/EABCD
!
      EQ2I=ONE/(EQCD+EQCD)
      EP2I=ONE/(EPAB+EPAB)
      if(LABmax.eq.1)then ! SSPP
        A2DF(1)=-EQ2I
        A2DF(2)= EQ2I
        A2DF(3)= EQ2I*EQ2I
      else ! SPSP
        A2DF(1)=-EQ2I
        A2DF(2)= EP2I
        A2DF(3)=-EP2I*EQ2I
      end if ! LABMAX

      DXYZ=RHO*RPQSQ
      call RPOLX (NZERO, DXYZ, TP, WP)
      TWORHO=RHO+RHO

RYSloop: do IZERO=1,NZERO
      RHOT2=TWORHO*TP(IZERO)
      ZCONST=ZTEMP*WP(IZERO)
      if(ZCONST.le.I2E_PQCUT2)cycle RYSloop
!
      G2DFX(1)=ONE
      G2DFY(1)=ONE
      G2DFZ(1)=ZCONST
!
      G2DFX(2)=RHOT2*PQX
      G2DFY(2)=RHOT2*PQY
      G2DFZ(2)=RHOT2*PQZ*ZCONST
!
      G2DFX(3)=RHOT2*(PQX*G2DFX(2)-ONE)
      G2DFY(3)=RHOT2*(PQY*G2DFY(2)-ONE)
      G2DFZ(3)=RHOT2*(PQZ*G2DFZ(2)-ZCONST)

      call SSPP4C ! Generate X,Y,ZIP

! Commence loop over atomic orbitals.
        TQprim( 1)=TQprim( 1)+(XIP( 6)        *ZCONST )
        TQprim( 2)=TQprim( 2)+(XIP( 5)*YIP( 2)*ZCONST )
        TQprim( 3)=TQprim( 3)+(XIP( 5)        *ZIP( 2))
        TQprim( 4)=TQprim( 4)+(XIP( 2)*YIP( 5)*ZCONST )
        TQprim( 5)=TQprim( 5)+(        YIP( 6)*ZCONST )
        TQprim( 6)=TQprim( 6)+(        YIP( 5)*ZIP( 2))
        TQprim( 7)=TQprim( 7)+(XIP( 2)        *ZIP( 5))
        TQprim( 8)=TQprim( 8)+(        YIP( 2)*ZIP( 5))
        TQprim( 9)=TQprim( 9)+(                ZIP( 6))
      end do RYSloop ! IZERO

! Apply contraction coefficients.
      CC4=GccA(CCA+1)*GccB(CCB+1)*GccC(CCC+1)*GccD(CCD+1)
      TQ( 1)=TQ( 1)+TQprim( 1)*CC4
      TQ( 2)=TQ( 2)+TQprim( 2)*CC4
      TQ( 3)=TQ( 3)+TQprim( 3)*CC4
      TQ( 4)=TQ( 4)+TQprim( 4)*CC4
      TQ( 5)=TQ( 5)+TQprim( 5)*CC4
      TQ( 6)=TQ( 6)+TQprim( 6)*CC4
      TQ( 7)=TQ( 7)+TQprim( 7)*CC4
      TQ( 8)=TQ( 8)+TQprim( 8)*CC4
      TQ( 9)=TQ( 9)+TQprim( 9)*CC4

      end if ! EXPARG.LE.I2E_expcut
      CCD=CCD+3
      end do Dloop ! Lgauss
      CCC=CCC+Krange
      end do Cloop ! Kgauss
      CCB=CCB+Jrange
      end do Bloop ! Jgauss
      CCA=CCA+1
      end do Aloop ! Igauss
! End of loop over gaussians

      NKL = Krange*Lrange
      JendM=Jrange
      KendM=Krange
      IF(Iatmshl_EQ_Jatmshl)JendM=1
      IF(IJIJ)KendM=1
      do Jao=1,JendM
        do Kao=1,KendM
          LendM=Lrange
          IF(Katmshl_EQ_Latmshl)LendM=Kao
          IF(IJIJ.and.Kao.EQ.1)LendM=Jao
          do Lao=1,LendM
            COR_INDEX=(Jao-1)*NKL+(Kao-1)*Lrange+Lao
            Int_pointer(COR_INDEX)=TQ(COR_INDEX)
          end do ! Lao
        end do ! Kao
      end do ! Jao

      Lsort=.true.

! End of routine I2ER_SSPP
      return
!
      end subroutine I2ER_GSSPP
      subroutine SSPP4C

      implicit none
!
! Local arrays:
      double precision :: I2P(8),I3P(2)
! SSPP
      if(LABmax.eq.1)then
      I2P(2)=A2DF(1)*G2DFx(2)
      XIP(6)=CCQx(8)+CCQx(9)*I2P(2)+A2DF(2)*G2DFx(1)+A2DF(3)*G2DFx(3)
      XIP(5)=CCQx(7)+I2P(2)
      XIP(2)=CCQx(1)+I2P(2)
      XIP(1)=ONE

      I2P(2)=A2DF(1)*G2DFy(2)
      YIP(6)=CCQy(8)+CCQy(9)*I2P(2)+A2DF(2)*G2DFy(1)+A2DF(3)*G2DFy(3)
      YIP(5)=CCQy(7)+I2P(2)
      YIP(2)=CCQy(1)+I2P(2)
      YIP(1)=ONE

      I2P(2)=A2DF(1)*G2DFz(2)
      ZIP(6)=CCQz(8)*ZCONST+CCQz(9)*I2P(2)+A2DF(2)*ZCONST+A2DF(3)*G2DFz(3)
      ZIP(5)=CCQz(7)*ZCONST+I2P(2)
      ZIP(2)=CCQz(1)*ZCONST+I2P(2)
      ZIP(1)=ZCONST

      else
      I2P(8)=A2DF(2)*G2DFx(2)
      I2P(2)=A2DF(1)*G2DFx(2)
      I3P(2)=CCQx(1)+I2P(2)
      XIP(6)=CCPx(1)*I3P(2)+CCQx(1)*I2P(8)+A2DF(3)*G2DFx(3)
      XIP(5)=CCPx(1)+I2P(8)
      XIP(2)=I3P(2)
      XIP(1)=ONE

      I2P(8)=A2DF(2)*G2DFy(2)
      I2P(2)=A2DF(1)*G2DFy(2)
      I3P(2)=CCQy(1)+I2P(2)
      YIP(6)=CCPy(1)*I3P(2)+CCQy(1)*I2P(8)+A2DF(3)*G2DFy(3)
      YIP(5)=CCPy(1)+I2P(8)
      YIP(2)=I3P(2)
      YIP(1)=ONE

      I2P(8)=A2DF(2)*G2DFz(2)
      I2P(2)=A2DF(1)*G2DFz(2)
      I3P(2)=CCQz(1)*ZCONST+I2P(2)
      ZIP(6)=CCPz(1)*I3P(2)+CCQz(1)*I2P(8)+A2DF(3)*G2DFz(3)
      ZIP(5)=CCPz(1)*ZCONST+I2P(8)
      ZIP(2)=I3P(2)
      ZIP(1)=ZCONST
      end if
      return
      end subroutine SSPP4C
      end subroutine I2ER_SSPP
      subroutine I2ER_SSSD
!***********************************************************************
!     Date last modified: September 23, 1996               Version 2.0 *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!***********************************************************************
! Modules:
      USE mod_idfclc
!
      implicit none
!
! Local Scalars
!     integer I
      double precision :: RHO,TWORHO,DXYZ,ZTEMP,RHOT2,ZCONST
!
! Local Arrays
      double precision :: G2DFX(13),G2DFY(13),G2DFZ(13),XIP(256),YIP(256),ZIP(256)
      double precision :: A2DF(174),CCPX(48),CCPY(48),CCPZ(48),CCQX(48),CCQY(48),CCQZ(48)
!
      call PRG_manager ('enter', 'I2ER_SSSD', 'UTILITY')

      include "start_atmshl_loop"

      case=1
      call DEF_shells (Jatmshl, Katmshl, Latmshl, Case)
      if((ABEXP+CDEXP).le.I2E_expcut)then
        Int_pointer=>IJKLs
        call I2ER_GSSSD
      end if

      if(LTWOINT)then
      case=2
      call DEF_shells (Latmshl, Jatmshl, Katmshl, Case)
      if((ABEXP+CDEXP).le.I2E_expcut)then
        Int_pointer=>ILJKs
        call I2ER_GSSSD
      end if
      end if

      if(Mtype.eq.1)then
      case=3
      call DEF_shells (Katmshl, Jatmshl, Latmshl, Case)
      if((ABEXP+CDEXP).le.I2E_expcut)then
        Int_pointer=>IKJLs
        call I2ER_GSSSD
      end if
      end if
      include "end_atmshl_loop"

! End of routine I2ER_SSSD
      call PRG_manager ('exit', 'I2ER_SSSD', 'UTILITY')
      return

CONTAINS !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine I2ER_GSSSD
!***********************************************************************
!     Date last modified: September 23, 1996               Version 2.0 *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!***********************************************************************
! Modules:
!     
      implicit none
!
! Local Scalars
      integer :: Izero
      integer :: Igauss,Jgauss,Kgauss,Lgauss
      double precision :: CC3a,CC4a,CC4,EQ2I
      double precision :: AS,BS,CS,DS
      double precision :: EPAB,EPABI,EPABA,EPABB,EQCD,EQCDI,EQCDC,EQCDD,EABCD,EP2I,EABCDI
      double precision :: XAP,YAP,ZAP,XBP,YBP,ZBP,XCQ,YCQ,ZCQ,XDQ,YDQ,ZDQ,PQX,PQY,PQZ,RPQSQ
!
! Begin:
     NZERO=2
!
! Clear the contracted function integral array TQ.
      A2DF(1:33)=ZERO
!
      CCQX(1:48)=ZERO
      CCQY(1:48)=ZERO
      CCQZ(1:48)=ZERO
      CCPX(1:48)=ZERO
      CCPY(1:48)=ZERO
      CCPZ(1:48)=ZERO
!
      TQ(1:6)=ZERO

      include 'mungauss_gaussian_AB'
       if(LRAB)then
        EPABA=AS*EPABI
        EPABB=BS*EPABI
        XAP= EPABB*(XB-XA)
        YAP= EPABB*(YB-YA)
        ZAP= EPABB*(ZB-ZA)
        XBP=-EPABA*(XB-XA)
        YBP=-EPABA*(YB-YA)
        ZBP=-EPABA*(ZB-ZA)
        call GETCC_XYZ (CCPX, CCPY, CCPZ, XAP, YAP, ZAP, XBP, YBP, ZBP, LAMAX, LBMAX)
      end if

      include 'mungauss_gaussian_CD'
      TQprim(1:6)=ZERO

      if(EXPARG.le.I2E_expcut)then! Dloop ! Must check this (also skipping contractions
      if(LRCD)then
        EQCDC=CS*EQCDI
        EQCDD=DS*EQCDI
        XCQ= EQCDD*(XD-XC)
        YCQ= EQCDD*(YD-YC)
        ZCQ= EQCDD*(ZD-ZC)
        XDQ=-EQCDC*(XD-XC)
        YDQ=-EQCDC*(YD-YC)
        ZDQ=-EQCDC*(ZD-ZC)
        call GETCC_XYZ (CCQX, CCQY, CCQZ, XCQ, YCQ, ZCQ, XDQ, YDQ, ZDQ, LCMAX, LDMAX)
      end if

      PQX=(CS*XC+DS*XD)*EQCDI-(AS*XA+BS*XB)*EPABI
      PQY=(CS*YC+DS*YD)*EQCDI-(AS*YA+BS*YB)*EPABI
      PQZ=(CS*ZC+DS*ZD)*EQCDI-(AS*ZA+BS*ZB)*EPABI

      RPQSQ=PQX*PQX+PQY*PQY+PQZ*PQZ
 
      EABCD=EPAB*EQCD
      EABCDI=ONE/(EPAB+EQCD)
      RHO=EABCD*EABCDI

      ZTEMP=PIconst*DEXP(-EXPARG)*DSQRT(EABCDI)/EABCD

      EP2I=ONE/(EPAB+EPAB)
      EQ2I=ONE/(EQCD+EQCD)

!     call SSSDA2 (A2DF, LABMAX, LCDMAX, EQCD, EP2I)
!***************************************************
      if(LABmax.eq.1)then
       A2DF(1)=-EQ2I
       A2DF(2)=EQ2I
       A2DF(3)=EQ2I*EQ2I
      else
       A2DF(1)=-EQ2I
       A2DF(16)=EP2I
       A2DF(32)=EP2I
       A2DF(33)=EP2I*EP2I
      end if
 
      DXYZ=RHO*RPQSQ
      call RPOLX (NZERO, DXYZ, TP, WP)
      TWORHO=RHO+RHO

!**************************************************
! Rys loop section of code
Rysloop:do Izero=1,Nzero
        RHOT2=TWORHO*TP(Izero)
        ZCONST=ZTEMP*WP(Izero)
        if(ZCONST.le.I2E_PQCUT2)cycle Rysloop
        G2DFX(1)=ONE
        G2DFY(1)=ONE
        G2DFZ(1)=ZCONST
        G2DFX(2)=RHOT2*PQX
        G2DFY(2)=RHOT2*PQY
        G2DFZ(2)=RHOT2*PQZ*ZCONST
        G2DFX(3)=RHOT2*(PQX*G2DFX(2)-ONE)
        G2DFY(3)=RHOT2*(PQY*G2DFY(2)-ONE)
        G2DFZ(3)=RHOT2*(PQZ*G2DFZ(2)-ZCONST)

        call SSSD4C ! ABCD4C

        TQprim(1)=TQprim(1)+(XIP(3)*       ZCONST)
        TQprim(2)=TQprim(2)+(       YIP(3)*ZCONST)
        TQprim(3)=TQprim(3)+(             ZIP( 3))
        TQprim(4)=TQprim(4)+(XIP(2)*YIP(2)*ZCONST)
        TQprim(5)=TQprim(5)+(XIP(2)      *ZIP( 2))
        TQprim(6)=TQprim(6)+(      YIP(2)*ZIP( 2))
      end do Rysloop ! Izero

! Contraction coefficient section of code
      CC4=GccA(CCA+1)*GccB(CCB+1)*GccC(CCC+1)*GccD(CCD+1)
      if(LABmax.eq.1)then
        CC4a=GccA(CCA+1)*GccB(CCB+1)*GccC(CCC+1)*GccD(CCD+4)
      else
        CC4a=GccA(CCA+1)*GccB(CCB+4)*GccC(CCC+1)*GccD(CCD+1)
      end if

      TQ(1)=TQ(1)+TQprim(1)*CC4
      TQ(2)=TQ(2)+TQprim(2)*CC4
      TQ(3)=TQ(3)+TQprim(3)*CC4
      TQ(4)=TQ(4)+TQprim(4)*CC4a
      TQ(5)=TQ(5)+TQprim(5)*CC4a
      TQ(6)=TQ(6)+TQprim(6)*CC4a

      end if ! EXPARG
      CCD=CCD+Lrange
      end do Dloop ! Lgauss
      CCC=CCC+1
      end do Cloop ! Kgauss
      CCB=CCB+Jrange
      end do Bloop ! Jgauss
      CCA=CCA+1
      end do Aloop ! Igauss
!     End of loop over gaussians
! Should deal with shell coincidences here

      Int_pointer(1:6)=TQ(1:6)

      Lsort=.false.

      return
      end subroutine I2ER_GSSSD
      subroutine SSSD4C

      implicit none
!
! Local arrays:
      double precision :: I2P(8)

!SSSD
      if(LABmax.eq.1)then
      I2P(2)=A2DF(1)*G2DFx(2)
      XIP(3)=CCQx(2)+CCQx(3)*I2P(2)+A2DF(2)+A2DF(3)*G2DFx(3)
      XIP(2)=CCQx(1)+I2P(2)
      XIP(1)=ONE
!
      I2P(2)=A2DF(1)*G2DFy(2)
      YIP(3)=CCQy(2)+CCQy(3)*I2P(2)+A2DF(2)+A2DF(3)*G2DFy(3)
      YIP(2)=CCQy(1)+I2P(2)
      YIP(1)=ONE
!
      I2P(2)=A2DF(1)*G2DFz(2)
      ZIP(3)=CCQz(2)*ZCONST+CCQz(3)*I2P(2)+A2DF(2)*ZCONST+A2DF(3)*G2DFz(3)
      ZIP(2)=CCQz(1)*ZCONST+I2P(2)
      ZIP(1)=ZCONST
!SDSS
      else
      I2P(8)=A2DF(16)*G2DFx(2)
      XIP(3)=CCPx(2)+CCPx(3)*I2P(8)+A2DF(32)+A2DF(33)*G2DFx(3)
      XIP(2)=CCPx(1)+I2P(8)
      XIP(1)=ONE
!
      I2P(8)=A2DF(16)*G2DFy(2)
      YIP(3)=CCPy(2)+CCPy(3)*I2P(8)+A2DF(32)+A2DF(33)*G2DFy(3)
      YIP(2)=CCPy(1)+I2P(8)
      YIP(1)=ONE
!
      I2P(8)=A2DF(16)*G2DFz(2)
      ZIP(3)=CCPz(2)*ZCONST+CCPz(3)*I2P(8)+A2DF(32)*ZCONST+A2DF(33)*G2DFz(3)
      ZIP(2)=CCPz(1)*ZCONST+I2P(8)
      ZIP(1)=ZCONST
      end if
      end subroutine SSSD4C
      end subroutine I2ER_SSSD
      subroutine I2ER_SSDD
!***********************************************************************
!     Date last modified: September 23, 1996               Version 2.0 *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!***********************************************************************
! Modules:
      USE mod_idfclc
!
      implicit none
!
! Local Scalars
!     integer I
      integer :: IZERO,INTC,IV,JendM,KendM,LendM,Igauss,Jgauss,Kgauss,Lgauss,NKL
      double precision :: RHO,TWORHO,DXYZ,ZTEMP,RHOT2,ZCONST
!
! Local Arrays
      double precision :: G2DFX(13),G2DFY(13),G2DFZ(13),XIP(256),YIP(256),ZIP(256)
      double precision :: A2DF(174),CCPX(48),CCPY(48),CCPZ(48),CCQX(48),CCQY(48),CCQZ(48)
!
      call PRG_manager ('enter', 'I2ER_SSDD', 'UTILITY')

      include "start_atmshl_loop"

      case=1
      call DEF_shells (Jatmshl, Katmshl, Latmshl, Case)
      if((ABEXP+CDEXP).le.I2E_expcut)then
        Int_pointer=>IJKLs
        call I2ER_GSSDD
      end if

      if(LTWOINT)then
      case=2
      call DEF_shells (Latmshl, Jatmshl, Katmshl, Case)
      if((ABEXP+CDEXP).le.I2E_expcut)then
        Int_pointer=>ILJKs
        call I2ER_GSSDD
      end if
      end if

      if(Mtype.eq.1)then
      case=3
      call DEF_shells (Katmshl, Jatmshl, Latmshl, Case)
      if((ABEXP+CDEXP).le.I2E_expcut)then
        Int_pointer=>IKJLs
        call I2ER_GSSDD
      end if
      end if
      include "end_atmshl_loop"

! End of routine I2ER_SSDD
      call PRG_manager ('exit', 'I2ER_SSDD', 'UTILITY')
      return

CONTAINS !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine I2ER_GSSDD
!***********************************************************************
!     Date last modified: September 23, 1996               Version 2.0 *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!***********************************************************************
! Modules:
!     
      implicit none
!
! Local Scalars
      integer :: Izero
      integer :: Iao,Jao,Kao,Lao
      integer ::  COR_INDEX
      double precision :: CC4,CC4a,CC4b,CC4c,CC4d,EQ2I
      double precision :: AS,BS,CS,DS
      double precision :: EPAB,EPABI,EPABA,EPABB,EQCD,EQCDI,EQCDC,EQCDD,EABCD,EP2I,EABCDI
      double precision :: XAP,YAP,ZAP,XBP,YBP,ZBP,XCQ,YCQ,ZCQ,XDQ,YDQ,ZDQ,PQX,PQY,PQZ,RPQSQ
!
!
! Begin:
     NZERO=3
!
! Clear the contracted function integral array TQ.
      A2DF(1:38)=ZERO
!
      CCQX(1:48)=ZERO
      CCQY(1:48)=ZERO
      CCQZ(1:48)=ZERO
      CCPX(1:48)=ZERO
      CCPY(1:48)=ZERO
      CCPZ(1:48)=ZERO
!
      TQ(1:36)=ZERO

      include 'mungauss_gaussian_AB'
       if(LRAB)then
        EPABA=AS*EPABI
        EPABB=BS*EPABI
        XAP= EPABB*(XB-XA)
        YAP= EPABB*(YB-YA)
        ZAP= EPABB*(ZB-ZA)
        XBP=-EPABA*(XB-XA)
        YBP=-EPABA*(YB-YA)
        ZBP=-EPABA*(ZB-ZA)
        call GETCC_XYZ (CCPX, CCPY, CCPZ, XAP, YAP, ZAP, XBP, YBP, ZBP, LAMAX, LBMAX)
      end if

      include 'mungauss_gaussian_CD'
      TQprim(1:36)=ZERO

      if(EXPARG.le.I2E_expcut)then !Dloop ! Must check this (also skipping contractions
      if(LRCD)then
        EQCDC=CS*EQCDI
        EQCDD=DS*EQCDI
        XCQ= EQCDD*(XD-XC)
        YCQ= EQCDD*(YD-YC)
        ZCQ= EQCDD*(ZD-ZC)
        XDQ=-EQCDC*(XD-XC)
        YDQ=-EQCDC*(YD-YC)
        ZDQ=-EQCDC*(ZD-ZC)
        call GETCC_XYZ (CCQX, CCQY, CCQZ, XCQ, YCQ, ZCQ, XDQ, YDQ, ZDQ, LCMAX, LDMAX)
      end if

      PQX=(CS*XC+DS*XD)*EQCDI-(AS*XA+BS*XB)*EPABI
      PQY=(CS*YC+DS*YD)*EQCDI-(AS*YA+BS*YB)*EPABI
      PQZ=(CS*ZC+DS*ZD)*EQCDI-(AS*ZA+BS*ZB)*EPABI

      RPQSQ=PQX*PQX+PQY*PQY+PQZ*PQZ
 
      EABCD=EPAB*EQCD
      EABCDI=ONE/(EPAB+EQCD)
      RHO=EABCD*EABCDI

      ZTEMP=PIconst*DEXP(-EXPARG)*DSQRT(EABCDI)/EABCD

      EQ2I=ONE/(EQCD+EQCD)
      EP2I=ONE/(EPAB+EPAB)
!****************************************************
!      call SSDDA2 (A2DF, LABMAX, LCDMAX, EQCD, EP2I)

      if(LABmax.eq.1)then
       A2DF(1)=-EQ2I
       A2DF(2)=EQ2I
       A2DF(3)=EQ2I*EQ2I
       A2DF(4)=-EQ2I*(EQ2I+EQ2I+EQ2I)
       A2DF(5)=-EQ2I*A2DF(3)
       A2DF(6)=EQ2I*THREE*EQ2I
       A2DF(7)=EQ2I*(THREE*A2DF(3)-A2DF(4))
       A2DF(8)=-EQ2I*A2DF(5)
      else
       A2DF(1)=-EQ2I
       A2DF(2)=EQ2I
       A2DF(3)=EQ2I*EQ2I
       A2DF(16)=EP2I
       A2DF(17)=-EP2I*EQ2I
       A2DF(18)=EP2I*EQ2I
       A2DF(19)=EP2I*A2DF(3)
       A2DF(32)=EP2I
       A2DF(33)=EP2I*EP2I
       A2DF(34)=EP2I*(-EQ2I)
       A2DF(35)=EP2I*A2DF(17)
       A2DF(36)=EP2I*EQ2I
       A2DF(37)=EP2I*(A2DF(3)+A2DF(18))
       A2DF(38)=EP2I*A2DF(19)
      end if

      DXYZ=RHO*RPQSQ
      call RPOLX (NZERO, DXYZ, TP, WP)
      TWORHO=RHO+RHO

!***************************************************
! Rys loop section of code
Rysloop:do Izero=1,Nzero
      RHOT2=TWORHO*TP(Izero)
      ZCONST=ZTEMP*WP(Izero)
      if(ZCONST.le.I2E_PQCUT2)cycle Rysloop
      G2DFX(1)=ONE
      G2DFY(1)=ONE
      G2DFZ(1)=ZCONST
      G2DFX(2)=RHOT2*PQX
      G2DFY(2)=RHOT2*PQY
      G2DFZ(2)=RHOT2*PQZ*ZCONST
      G2DFX(3)=RHOT2*(PQX*G2DFX(2)-ONE)
      G2DFY(3)=RHOT2*(PQY*G2DFY(2)-ONE)
      G2DFZ(3)=RHOT2*(PQZ*G2DFZ(2)-ZCONST)
      G2DFX(4)=RHOT2*(PQX*G2DFX(3)- TWO*G2DFX(2))
      G2DFY(4)=RHOT2*(PQY*G2DFY(3)- TWO*G2DFY(2))
      G2DFZ(4)=RHOT2*(PQZ*G2DFZ(3)- TWO*G2DFZ(2))
      G2DFX(5)=RHOT2*(PQX*G2DFX(4)- THREE*G2DFX(3))
      G2DFY(5)=RHOT2*(PQY*G2DFY(4)- THREE*G2DFY(3))
      G2DFZ(5)=RHOT2*(PQZ*G2DFZ(4)- THREE*G2DFZ(3))

      call SSDD4C ! ABCD4C

      TQprim( 1)=TQprim( 1)+(XIP(11)*        ZCONST)
      TQprim( 2)=TQprim( 2)+(XIP( 9)*YIP( 3)*ZCONST)
      TQprim( 3)=TQprim( 3)+(XIP( 9)*        ZIP( 3))
      TQprim( 4)=TQprim( 4)+(XIP(10)*YIP( 2)*ZCONST)
      TQprim( 5)=TQprim( 5)+(XIP(10)*        ZIP( 2))
      TQprim( 6)=TQprim( 6)+(XIP( 9)*YIP( 2)*ZIP( 2))
      TQprim( 7)=TQprim( 7)+(XIP( 3)*YIP( 9)*ZCONST)
      TQprim( 8)=TQprim( 8)+(        YIP(11)*ZCONST)
      TQprim( 9)=TQprim( 9)+(        YIP( 9)*ZIP( 3))
      TQprim(10)=TQprim(10)+(XIP( 2)*YIP(10)*ZCONST)
      TQprim(11)=TQprim(11)+(XIP( 2)*YIP( 9)*ZIP( 2))
      TQprim(12)=TQprim(12)+(        YIP(10)*ZIP( 2))
      TQprim(13)=TQprim(13)+(XIP( 3)*        ZIP( 9))
      TQprim(14)=TQprim(14)+(        YIP( 3)*ZIP( 9))
      TQprim(15)=TQprim(15)+(                ZIP(11))
      TQprim(16)=TQprim(16)+(XIP( 2)*YIP( 2)*ZIP( 9))
      TQprim(17)=TQprim(17)+(XIP( 2)*        ZIP(10))
      TQprim(18)=TQprim(18)+(        YIP( 2)*ZIP(10))
      TQprim(19)=TQprim(19)+(XIP( 7)*YIP( 5)*ZCONST)
      TQprim(20)=TQprim(20)+(XIP( 5)*YIP( 7)*ZCONST)
      TQprim(21)=TQprim(21)+(XIP( 5)*YIP( 5)*ZIP( 3))
      TQprim(22)=TQprim(22)+(XIP( 6)*YIP( 6)*ZCONST)
      TQprim(23)=TQprim(23)+(XIP( 6)*YIP( 5)*ZIP( 2))
      TQprim(24)=TQprim(24)+(XIP( 5)*YIP( 6)*ZIP( 2))
      TQprim(25)=TQprim(25)+(XIP( 7)*        ZIP( 5))
      TQprim(26)=TQprim(26)+(XIP( 5)*YIP( 3)*ZIP( 5))
      TQprim(27)=TQprim(27)+(XIP( 5)*        ZIP( 7))
      TQprim(28)=TQprim(28)+(XIP( 6)*YIP( 2)*ZIP( 5))
      TQprim(29)=TQprim(29)+(XIP( 6)*        ZIP( 6))
      TQprim(30)=TQprim(30)+(XIP( 5)*YIP( 2)*ZIP( 6))
      TQprim(31)=TQprim(31)+(XIP( 3)*YIP( 5)*ZIP( 5))
      TQprim(32)=TQprim(32)+(        YIP( 7)*ZIP( 5))
      TQprim(33)=TQprim(33)+(        YIP( 5)*ZIP( 7))
      TQprim(34)=TQprim(34)+(XIP( 2)*YIP( 6)*ZIP( 5))
      TQprim(35)=TQprim(35)+(XIP( 2)*YIP( 5)*ZIP( 6))
      TQprim(36)=TQprim(36)+(        YIP( 6)*ZIP( 6))
      end do Rysloop ! Izero

! Contraction coefficient section of code
      CC4=GccA(CCA+1)
      CC4a=CC4*GccB(CCB+1)*GccC(CCC+1)*GccD(CCD+1)
      CC4b=CC4*GccB(CCB+1)*GccC(CCC+1)*GccD(CCD+4)
      if(LABMAX.eq.1)then
        CC4c=CC4*GccB(CCB+1)*GccC(CCC+4)*GccD(CCD+1)
        CC4d=CC4*GccB(CCB+1)*GccC(CCC+4)*GccD(CCD+4)
      else
        CC4c=CC4*GccB(CCB+4)*GccC(CCC+1)*GccD(CCD+1)
        CC4d=CC4*GccB(CCB+4)*GccC(CCC+1)*GccD(CCD+4)
      end if

      TQ( 1)=TQ( 1)+TQprim( 1)*CC4a
      TQ( 2)=TQ( 2)+TQprim( 2)*CC4a
      TQ( 3)=TQ( 3)+TQprim( 3)*CC4a
      TQ( 4)=TQ( 4)+TQprim( 4)*CC4b
      TQ( 5)=TQ( 5)+TQprim( 5)*CC4b
      TQ( 6)=TQ( 6)+TQprim( 6)*CC4b
      TQ( 7)=TQ( 7)+TQprim( 7)*CC4a
      TQ( 8)=TQ( 8)+TQprim( 8)*CC4a
      TQ( 9)=TQ( 9)+TQprim( 9)*CC4a
      TQ(10)=TQ(10)+TQprim(10)*CC4b
      TQ(11)=TQ(11)+TQprim(11)*CC4b
      TQ(12)=TQ(12)+TQprim(12)*CC4b
      TQ(13)=TQ(13)+TQprim(13)*CC4a
      TQ(14)=TQ(14)+TQprim(14)*CC4a
      TQ(15)=TQ(15)+TQprim(15)*CC4a
      TQ(16)=TQ(16)+TQprim(16)*CC4b
      TQ(17)=TQ(17)+TQprim(17)*CC4b
      TQ(18)=TQ(18)+TQprim(18)*CC4b
      TQ(19)=TQ(19)+TQprim(19)*CC4c
      TQ(20)=TQ(20)+TQprim(20)*CC4c
      TQ(21)=TQ(21)+TQprim(21)*CC4c
      TQ(22)=TQ(22)+TQprim(22)*CC4d
      TQ(23)=TQ(23)+TQprim(23)*CC4d
      TQ(24)=TQ(24)+TQprim(24)*CC4d
      TQ(25)=TQ(25)+TQprim(25)*CC4c
      TQ(26)=TQ(26)+TQprim(26)*CC4c
      TQ(27)=TQ(27)+TQprim(27)*CC4c
      TQ(28)=TQ(28)+TQprim(28)*CC4d
      TQ(29)=TQ(29)+TQprim(29)*CC4d
      TQ(30)=TQ(30)+TQprim(30)*CC4d
      TQ(31)=TQ(31)+TQprim(31)*CC4c
      TQ(32)=TQ(32)+TQprim(32)*CC4c
      TQ(33)=TQ(33)+TQprim(33)*CC4c
      TQ(34)=TQ(34)+TQprim(34)*CC4d
      TQ(35)=TQ(35)+TQprim(35)*CC4d
      TQ(36)=TQ(36)+TQprim(36)*CC4d

      end if ! EXPARG
      CCD=CCD+6
      end do Dloop ! Lgauss
      CCC=CCC+Krange
      end do Cloop ! Kgauss
      CCB=CCB+Jrange
      end do Bloop ! Jgauss
      CCA=CCA+1
      end do Aloop ! Igauss
!     End of loop over gaussians
! Should deal with shell coincidences here

      NKL=Krange*Lrange
      JendM=Jrange
      KendM=Krange
      IF(Iatmshl_EQ_Jatmshl)JendM=1
      IF(IJIJ)KendM=1
      do Jao=1,JendM
        do Kao=1,KendM
          LendM=Lrange
          IF(Katmshl_EQ_Latmshl)LendM=Kao
          IF(IJIJ.and.Kao.EQ.1)LendM=Jao
          do Lao=1,LendM
            COR_INDEX=(Jao-1)*NKL+(Kao-1)*Lrange+Lao
            Int_pointer(COR_INDEX)=TQ(COR_INDEX)
          end do ! Lao
        end do ! Kao
      end do ! Jao

      Lsort=.true.

      return
      end subroutine I2ER_GSSDD
      subroutine SSDD4C 

      implicit none
!
! Local arrays:
      double precision :: I2P(16),I3P(19)
    
!SSDDx
      if(LABmax.eq.1)then 
       I2P(5)=A2DF(6)+A2DF(7)*G2DFx(3)+A2DF(8)*G2DFx(5)
       I2P(4)=A2DF(4)*G2DFx(2)+A2DF(5)*G2DFx(4)
       I2P(3)=A2DF(2)+A2DF(3)*G2DFx(3)
       I2P(2)=A2DF(1)*G2DFx(2)
       XIP(11)=CCQx(22)+CCQx(23)*I2P(2)+CCQx(24)*I2P(3)+CCQx(25)*I2P(4)+I2P(5)
       XIP(10)=CCQx(19)+CCQx(20)*I2P(2)+CCQx(21)*I2P(3)+I2P(4)
       XIP(9)=CCQx(17)+CCQx(18)*I2P(2)+I2P(3)
       XIP(7)=CCQx(10)+CCQx(11)*I2P(2)+CCQx(12)*I2P(3)+I2P(4)
       XIP(6)=CCQx(8)+CCQx(9)*I2P(2)+I2P(3)
       XIP(5)=CCQx(7)+I2P(2)
       XIP(3)=CCQx(2)+CCQx(3)*I2P(2)+I2P(3)
       XIP(2)=CCQx(1)+I2P(2)
       XIP(1)=ONE
!SSDDy
       I2P(5)=A2DF(6)+A2DF(7)*G2DFy(3)+A2DF(8)*G2DFy(5)
       I2P(4)=A2DF(4)*G2DFy(2)+A2DF(5)*G2DFy(4)
       I2P(3)=A2DF(2)+A2DF(3)*G2DFy(3)
       I2P(2)=A2DF(1)*G2DFy(2)
       YIP(11)=CCQy(22)+CCQy(23)*I2P(2)+CCQy(24)*I2P(3)+CCQy(25)*I2P(4)+I2P(5)
       YIP(10)=CCQy(19)+CCQy(20)*I2P(2)+CCQy(21)*I2P(3)+I2P(4)
       YIP(9)=CCQy(17)+CCQy(18)*I2P(2)+I2P(3)
       YIP(7)=CCQy(10)+CCQy(11)*I2P(2)+CCQy(12)*I2P(3)+I2P(4)
       YIP(6)=CCQy(8)+CCQy(9)*I2P(2)+I2P(3)
       YIP(5)=CCQy(7)+I2P(2)
       YIP(3)=CCQy(2)+CCQy(3)*I2P(2)+I2P(3)
       YIP(2)=CCQy(1)+I2P(2)
       YIP(1)=ONE
!SSDDz
      I2P(5)=A2DF(6)*ZCONST+A2DF(7)*G2DFz(3)+A2DF(8)*G2DFz(5)
      I2P(4)=A2DF(4)*G2DFz(2)+A2DF(5)*G2DFz(4)
      I2P(3)=A2DF(2)*ZCONST+A2DF(3)*G2DFz(3)
      I2P(2)=A2DF(1)*G2DFz(2)
      ZIP(11)=CCQz(22)*ZCONST+CCQz(23)*I2P(2)+CCQz(24)*I2P(3)+CCQz(25)*I2P(4)+I2P(5)
      ZIP(10)=CCQz(19)*ZCONST+CCQz(20)*I2P(2)+CCQz(21)*I2P(3)+I2P(4)
      ZIP(9)=CCQz(17)*ZCONST+CCQz(18)*I2P(2)+I2P(3)
      ZIP(7)=CCQz(10)*ZCONST+CCQz(11)*I2P(2)+CCQz(12)*I2P(3)+I2P(4)
      ZIP(6)=CCQz(8)*ZCONST+CCQz(9)*I2P(2)+I2P(3)
      ZIP(5)=CCQz(7)*ZCONST+I2P(2)
      ZIP(3)=CCQz(2)*ZCONST+CCQz(3)*I2P(2)+I2P(3)
      ZIP(2)=CCQz(1)*ZCONST+I2P(2)
      ZIP(1)=ZCONST
     else

!SDSDX
       I2P(16)=A2DF(34)*G2DFx(2)+A2DF(35)*G2DFx(4)
       I2P(15)=A2DF(32)+A2DF(33)*G2DFx(3)
       I2P(10)=A2DF(18)*G2DFx(2)+A2DF(19)*G2DFx(4)
       I2P(9)=A2DF(17)*G2DFx(3)
       I2P(8)=A2DF(16)*G2DFx(2)
       I2P(3)=A2DF(2)+A2DF(3)*G2DFx(3)
       I2P(2)=A2DF(1)*G2DFx(2)
       I3P(19)=CCQx(2)*I2P(8)+CCQx(3)*I2P(9)+I2P(10)
       I3P(18)=CCQx(1)*I2P(8)+I2P(9)
       I3P(3)=CCQx(2)+CCQx(3)*I2P(2)+I2P(3)
       I3P(2)=CCQx(1)+I2P(2)
       XIP(11)=CCPx(2)*I3P(3)+CCPx(3)*I3P(19)+CCQx(2)*I2P(15)+CCQx(3)*I2P(16)+A2DF(36)+ &
               A2DF(37)*G2DFx(3)+A2DF(38)*G2DFx(5)
       XIP(10)=CCPx(2)*I3P(2)+CCPx(3)*I3P(18)+CCQx(1)*I2P(15)+I2P(16)
       XIP(9)=CCPx(2)+CCPx(3)*I2P(8)+I2P(15)
       XIP(7)=CCPx(1)*I3P(3)+I3P(19)
       XIP(6)=CCPx(1)*I3P(2)+I3P(18)
       XIP(5)=CCPx(1)+I2P(8)
       XIP(3)=I3P(3)
       XIP(2)=I3P(2)
       XIP(1)=ONE

       I2P(16)=A2DF(34)*G2DFy(2)+A2DF(35)*G2DFy(4)
       I2P(15)=A2DF(32)+A2DF(33)*G2DFy(3)
       I2P(10)=A2DF(18)*G2DFy(2)+A2DF(19)*G2DFy(4)
       I2P(9)=A2DF(17)*G2DFy(3)
       I2P(8)=A2DF(16)*G2DFy(2)
       I2P(3)=A2DF(2)+A2DF(3)*G2DFy(3)
       I2P(2)=A2DF(1)*G2DFy(2)
       I3P(19)=CCQy(2)*I2P(8)+CCQy(3)*I2P(9)+I2P(10)
       I3P(18)=CCQy(1)*I2P(8)+I2P(9)
       I3P(3)=CCQy(2)+CCQy(3)*I2P(2)+I2P(3)
       I3P(2)=CCQy(1)+I2P(2)
       YIP(11)=CCPy(2)*I3P(3)+CCPy(3)*I3P(19)+CCQy(2)*I2P(15)+CCQy(3)*I2P(16)+A2DF(36)+ &
               A2DF(37)*G2DFy(3)+A2DF(38)*G2DFy(5)
       YIP(10)=CCPy(2)*I3P(2)+CCPy(3)*I3P(18)+CCQy(1)*I2P(15)+I2P(16)
       YIP(9)=CCPy(2)+CCPy(3)*I2P(8)+I2P(15)
       YIP(7)=CCPy(1)*I3P(3)+I3P(19)
       YIP(6)=CCPy(1)*I3P(2)+I3P(18)
       YIP(5)=CCPy(1)+I2P(8)
       YIP(3)=I3P(3)
       YIP(2)=I3P(2)
       YIP(1)=ONE
!SDSDz     
       I2P(16)=A2DF(34)*G2DFz(2)+A2DF(35)*G2DFz(4)
       I2P(15)=A2DF(32)*ZCONST+A2DF(33)*G2DFz(3)
       I2P(10)=A2DF(18)*G2DFz(2)+A2DF(19)*G2DFz(4)
       I2P(9)=A2DF(17)*G2DFz(3)
       I2P(8)=A2DF(16)*G2DFz(2)
       I2P(3)=A2DF(2)*ZCONST+A2DF(3)*G2DFz(3)
       I2P(2)=A2DF(1)*G2DFz(2)
       I3P(19)=CCQz(2)*I2P(8)+CCQz(3)*I2P(9)+I2P(10)
       I3P(18)=CCQz(1)*I2P(8)+I2P(9)
       I3P(3)=CCQz(2)*ZCONST+CCQz(3)*I2P(2)+I2P(3)
       I3P(2)=CCQz(1)*ZCONST+I2P(2)
       ZIP(11)=CCPz(2)*I3P(3)+CCPz(3)*I3P(19)+CCQz(2)*I2P(15)+CCQz(3)*I2P(16)+A2DF(36)*ZCONST+ &
               A2DF(37)*G2DFz(3)+A2DF(38)*G2DFz(5)
       ZIP(10)=CCPz(2)*I3P(2)+CCPz(3)*I3P(18)+CCQz(1)*I2P(15)+I2P(16)
       ZIP(9)=CCPz(2)*ZCONST+CCPz(3)*I2P(8)+I2P(15)
       ZIP(7)=CCPz(1)*I3P(3)+I3P(19)
       ZIP(6)=CCPz(1)*I3P(2)+I3P(18)
       ZIP(5)=CCPz(1)*ZCONST +I2P(8)
       ZIP(3)=I3P(3)
       ZIP(2)=I3P(2)
       ZIP(1)=ZCONST
      end if
      return
      end subroutine SSDD4C
      end subroutine I2ER_SSDD
      subroutine I2ER_SDDD
!***********************************************************************
!     Date last modified: September 23, 1996               Version 2.0 *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!***********************************************************************
! Modules:
      USE mod_idfclc
!
      implicit none
!
! Local Scalars
!     integer I
      double precision :: RHO,TWORHO,DXYZ,ZTEMP,RHOT2,ZCONST
!
      call PRG_manager ('enter', 'I2ER_SDDD', 'UTILITY')

      include "start_atmshl_loop"

      case=1
      call DEF_shells (Jatmshl, Katmshl, Latmshl, Case)
      if((ABEXP+CDEXP).le.I2E_expcut)then
        Int_pointer=>IJKLs
        call I2ER_GSDDD
      end if

      if(LTWOINT)then
      case=2
      call DEF_shells (Latmshl, Jatmshl, Katmshl, Case)
      if((ABEXP+CDEXP).le.I2E_expcut)then
        Int_pointer=>ILJKs
        call I2ER_GSDDD
      end if
      end if

      if(Mtype.eq.1)then
      case=3
      call DEF_shells (Katmshl, Jatmshl, Latmshl, Case)
      if((ABEXP+CDEXP).le.I2E_expcut)then
        Int_pointer=>IKJLs
        call I2ER_GSDDD
      end if
      end if
      include "end_atmshl_loop"

! End of routine I2ER_SDDD
      call PRG_manager ('exit', 'I2ER_SDDD', 'UTILITY')
      return

CONTAINS !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine I2ER_GSDDD
!***********************************************************************
!     Date last modified: September 23, 1996               Version 2.0 *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!***********************************************************************
! Modules:
!     
      implicit none
!
! Local Scalars
      integer :: I,JLRNG,COR_INDEX
      integer :: IX,IY,IZ,JX,JY,JZ,KX,KY,KZ,LX,LY,LZ
      integer :: Iao,Jao,Kao,Lao
      integer :: IZERO,INTC,IV,JendM,KendM,LendM,Igauss,Jgauss,Kgauss,Lgauss
      double precision :: CC1,CC2,CC3
      double precision :: AS,BS,CS,DS
      double precision :: EPAB,EPABI,EPABA,EPABB,EQCD,EQCDI,EQCDC,EQCDD,EABCD,EP2I,EABCDI
      double precision :: XAP,YAP,ZAP,XBP,YBP,ZBP,XCQ,YCQ,ZCQ,XDQ,YDQ,ZDQ,PQX,PQY,PQZ,RPQSQ
!
! Local Arrays
      double precision :: XINT(5) 
      double precision :: G2DFX(13),G2DFY(13),G2DFZ(13),XIP(256),YIP(256),ZIP(256)
      double precision :: A2DF(174),CCPX(48),CCPY(48),CCPZ(48),CCQX(48),CCQY(48),CCQZ(48)
      integer :: INDJX(6),INDJY(6),INDJZ(6),INDKX(6),INDKY(6),INDKZ(6),INDLX(6),INDLY(6),INDLZ(6)
      DATA INDJX/32,0,0,16,16,0/,INDJY/0,32,0,16,0,16/,INDJZ/0,0,32,0,16,16/, &
           INDKX/8,0,0,4,4,0/,INDKY/0,8,0,4,0,4/,INDKZ/0,0,8,0,4,4/, &
           INDLX/3,1,1,2,2,1/,INDLY/1,3,1,2,1,2/,INDLZ/1,1,3,1,2,2/,XINT/1.0D0,2.0D0,3.0D0,4.0D0,5.0D0/
!
! Begin:
      LPQMAX=7
!
! Clear the contracted function integral array TQ.
      A2DF(1:174)=ZERO
!
      CCPX(1:48)=ZERO
      CCPY(1:48)=ZERO
      CCPZ(1:48)=ZERO
      CCQX(1:48)=ZERO
      CCQY(1:48)=ZERO
      CCQZ(1:48)=ZERO
!
! Commence loops over Gaussian expansion.
      include 'mungauss_gaussian_AB'
      if(LRAB)then
      EPABA=AS*EPABI
      EPABB=BS*EPABI
      XAP= EPABB*(XB-XA)
      YAP= EPABB*(YB-YA)
      ZAP= EPABB*(ZB-ZA)
      XBP=-EPABA*(XB-XA)
      YBP=-EPABA*(YB-YA)
      ZBP=-EPABA*(ZB-ZA)
      call GETCC_XYZ (CCPX, CCPY, CCPZ, XAP, YAP, ZAP, XBP, YBP, ZBP, LAMAX, LBMAX)
      end if

      include 'mungauss_gaussian_CD'
      TQprim(1:216)=ZERO

      IF(EXPARG.LE.I2E_expcut)then
      if(LRCD)then
      EQCDC=CS*EQCDI
      EQCDD=DS*EQCDI
      XCQ= EQCDD*(XD-XC)
      YCQ= EQCDD*(YD-YC)
      ZCQ= EQCDD*(ZD-ZC)
      XDQ=-EQCDC*(XD-XC)
      YDQ=-EQCDC*(YD-YC)
      ZDQ=-EQCDC*(ZD-ZC)
      call GETCC_XYZ (CCQX, CCQY, CCQZ, XCQ, YCQ, ZCQ, XDQ, YDQ, ZDQ, LCMAX, LDMAX)
      end if
!
      PQX=(CS*XC+DS*XD)*EQCDI-(AS*XA+BS*XB)*EPABI
      PQY=(CS*YC+DS*YD)*EQCDI-(AS*YA+BS*YB)*EPABI
      PQZ=(CS*ZC+DS*ZD)*EQCDI-(AS*ZA+BS*ZB)*EPABI

      RPQSQ=PQX*PQX+PQY*PQY+PQZ*PQZ
      EABCD=EPAB*EQCD
      EABCDI=ONE/(EPAB+EQCD)
      RHO=EABCD*EABCDI
      ZTEMP=PIconst*DEXP(-EXPARG)*DSQRT(EABCDI)/EABCD

      EP2I=ONE/(EPAB+EPAB)
      call IJKLA2 (A2DF, LABMAX, LCDMAX, EQCD, EP2I)
!
      DXYZ=RHO*RPQSQ
      call RPOLX (NZERO, DXYZ, TP, WP)
      TWORHO=RHO+RHO

      do IZERO=1,NZERO
      RHOT2=TWORHO*TP(IZERO)
      ZCONST=ZTEMP*WP(IZERO)
      if(ZCONST.le.I2E_PQCUT2)cycle
      G2DFX(1)=ONE
      G2DFY(1)=ONE
      G2DFZ(1)=ZCONST
      XIP(1)=ONE
      YIP(1)=ONE
      ZIP(1)=ZCONST
!
      G2DFX(2)=RHOT2*PQX
      G2DFY(2)=RHOT2*PQY
      G2DFZ(2)=RHOT2*PQZ*ZCONST
!
      do IV=3,LPQMAX
        G2DFX(IV)=RHOT2*(PQX*G2DFX(IV-1)-XINT(IV-2)*G2DFX(IV-2))
        G2DFY(IV)=RHOT2*(PQY*G2DFY(IV-1)-XINT(IV-2)*G2DFY(IV-2))
        G2DFZ(IV)=RHOT2*(PQZ*G2DFZ(IV-1)-XINT(IV-2)*G2DFZ(IV-2))
      end do
! Find two-center integrals.
      if(LRABCD)then
        call ABCD4C (A2DF, G2DFX, CCQX, CCPX, XIP, LAMAX, LBMAX, LCMAX, LDMAX)
        call ABCD4C (A2DF, G2DFY, CCQY, CCPY, YIP, LAMAX, LBMAX, LCMAX, LDMAX)
        call ABCD4C (A2DF, G2DFZ, CCQZ, CCPZ, ZIP, LAMAX, LBMAX, LCMAX, LDMAX)
      else if(LRCD)then
        call AACD3C (A2DF, G2DFX, CCQX, XIP, LAMAX, LBMAX, LCMAX, LDMAX)
        call AACD3C (A2DF, G2DFY, CCQY, YIP, LAMAX, LBMAX, LCMAX, LDMAX)
        call AACD3C (A2DF, G2DFZ, CCQZ, ZIP, LAMAX, LBMAX, LCMAX, LDMAX)
      else if(LRAB)then
        call ABCC3C (A2DF, G2DFX, CCPX, XIP, LAMAX, LBMAX, LCMAX, LDMAX)
        call ABCC3C (A2DF, G2DFY, CCPY, YIP, LAMAX, LBMAX, LCMAX, LDMAX)
        call ABCC3C (A2DF, G2DFZ, CCPZ, ZIP, LAMAX, LBMAX, LCMAX, LDMAX)
      else
        call AABB2C (A2DF, G2DFX, XIP, LAMAX, LBMAX, LCMAX, LDMAX)
        call AABB2C (A2DF, G2DFY, YIP, LAMAX, LBMAX, LCMAX, LDMAX)
        call AABB2C (A2DF, G2DFZ, ZIP, LAMAX, LBMAX, LCMAX, LDMAX)
      end if ! LRABCD

!
! Commence loop over atomic orbitals.
      INTC=0
      do Jao=1,6
        JX=INDJX(Jao)
        JY=INDJY(Jao)
        JZ=INDJZ(Jao)
      do Kao=1,6
        LendM=6
        IF(Katmshl_EQ_Latmshl)LendM=Kao
        KX=INDKX(Kao)+JX
        KY=INDKY(Kao)+JY
        KZ=INDKZ(Kao)+JZ
      do Lao=1,LendM
        LX=INDLX(Lao)+KX
        LY=INDLY(Lao)+KY
        LZ=INDLZ(Lao)+KZ
        INTC=INTC+1
        TQprim(INTC)=TQprim(INTC)+(XIP(LX)*YIP(LY)*ZIP(LZ))
      end do ! L
      end do ! K
      end do ! J
      end do ! IZERO

! Apply contraction coefficients.
      INTC=0
      COR_INDEX = 0
      CC1=GccA(CCA+1)
      do Jao=1,6
      CC2=CC1*GccB(CCB+Jao)
      do Kao=1,6
      CC3=CC2*GccC(CCC+Kao)
      LendM=6
      IF(Katmshl_EQ_Latmshl)LendM=Kao
      do Lao=1,LendM
        INTC=INTC+1
        COR_INDEX = (Jao-1)*36 + (Kao-1)*6 + Lao
        Int_pointer(COR_INDEX)=Int_pointer(COR_INDEX)+TQprim(INTC)*CC3*GccD(CCD+Lao)
      end do ! Lao
      end do ! Kao
      end do ! Jao

      end if ! EXPARG.LE.I2E_expcut
      CCD=CCD+6
      end do Dloop ! Lgauss
      CCC=CCC+6
      end do Cloop ! Kgauss
      CCB=CCB+6
      end do Bloop ! Jgauss
      CCA=CCA+1
      end do Aloop ! Igauss
! End of loop over gaussians
!
! End of routine I2ER_SDDD
      return
      end subroutine I2ER_GSDDD
      end subroutine I2ER_SDDD
