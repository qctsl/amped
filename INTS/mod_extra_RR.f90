      MODULE extra_RR
!**************************************************************************************************
!     Date last modified: May 22, 2024                                                            *
!     Author: JWH                                                                                 *
!     Description:  Intermediates for 5-term recurrence relation for 3D-extracule density integrals.
!**************************************************************************************************
      implicit none
!
! RR coefficients
      double precision, dimension(3) :: RRa1, RRb1, RRc1, RRd1
      double precision :: RRa2, RRa3, RRa4, RRa5
      double precision :: RRb2, RRb3, RRb4, RRb5
      double precision :: RRc2, RRc3, RRc4, RRc5
      double precision :: RRd2, RRd3, RRd4, RRd5
!
! Integrals
      double precision :: G
      double precision, allocatable, dimension(:) :: Int1000, Int1100, Int1010, Int0110, Int1110
      double precision, allocatable, dimension(:) :: Int0101, Int0111, Int1111, Int1101
      double precision, allocatable, dimension(:) :: Int0100, Int2000, Int0200, Int0020, Int2100
      double precision, allocatable, dimension(:) :: Int2010, Int1200, Int2200, Int2020, Int2210
      double precision, allocatable, dimension(:) :: Int2120, Int2110, Int1210, Int1120, Int2211
      double precision, allocatable, dimension(:) :: Int2121, Int1220, Int2002, Int1102, Int2001
      double precision, allocatable, dimension(:) :: Int1020, Int2102, Int2101, Int2220, Int2221
      double precision, allocatable, dimension(:) :: Int0210, Int1002, Int1001, Int1221, Int0002
      double precision, allocatable, dimension(:) :: Int0010, Int0001, Int0220, Int0000
      double precision, allocatable, dimension(:) :: Int0201, Int2011, Int0211, Int1201, Int2111
      double precision, allocatable, dimension(:) :: Int1211, Int0202, Int2201, Int2012, Int2112
      double precision, allocatable, dimension(:) :: Int1212, Int2202, Int0102, Int1202, Int0120
      double precision, allocatable, dimension(:) :: Int2212, Int2222 
!
! Others
      integer :: Lvec_d(6,3), xyz(3), v_1i(3), imat(3,3)
      double precision, parameter :: sq3 = dsqrt(3.0D0)
      double precision, parameter :: pi  = 3.14159265358979323846264338327950288419716939937510D0
!
      end MODULE extra_RR
