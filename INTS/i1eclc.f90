      subroutine I1ECLC
!***********************************************************************
!     Date last modified: November 6, 1996                 Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!***********************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE type_basis_set
      USE QM_objects
      USE INT_objects
      USE matrix_print

      implicit none
!
! Local scalars:
      integer :: Ishell,Jshell,Ifrst,Jfrst,Ilast,Jlast
      integer :: Istart,Jstart,Iend,Jend
      integer :: Iatmshl,Jatmshl
      integer :: LAMAX,LBMAX
      integer :: Iatom
      integer :: Irange,Jrange
      integer :: Igauss,Jgauss
      integer :: Iaos,JaoS
      integer :: IGBGN,JGBGN,IGend,JGend
      integer :: LPMAX,I,IA,IJX,IJY,IJZ,INTC,IX,IXP,IY,IYP,IZ,IZERO,IZP,J,JX,JY,JZ,LIM1DS,NZERO
      integer :: LENTQ
      double precision ABX,ABY,ABZ,ARABSQ,ARG,AS,ASXA,ASYA,ASZA,BS,COEF,CUT1,EP,EPI,EPIO2,PCX,PCY,PCZ,PEXP, &
                       PX,PY,PZ,RPCSQ,STERM,SYZ,TEMPK,TEMPS,TK,TWOASQ,TWOP,TWOPI,TWOPT2,XAP,XBP,XIIM1, &
                       XK,YAP,YBP,YIIM1,ZAP,ZBP,ZCONST,ZIIM1,ZT,ZTEMP
      double precision :: XA,XB,XC,YA,YB,YC,ZA,ZB,ZC,RABSQ
!
! Local arrays:
      integer INDIX(20),INDIY(20),INDIZ(20),INDJX(20),INDJY(20),INDJZ(20)
      double precision :: TP(7),WP(7)
      double precision A(45),CA(20),CB(20),CCX(192),CCY(192),CCZ(192),EEPA(100),EEPB(100),EEPT(100),EEPV(100), &
                       SS(100),SX(32),SY(32),SZ(32), &
                       S1C(6),TWOCX(9),TWOCY(9),TWOCZ(9),XIP(80),YIP(80),ZIP(80)
!
      parameter (TWOPI=TWO*PI_VAL,CUT1=-75.0D0)
!
      DATA INDJX/1,2,1,1,3,1,1,2,2,1,4,1,1,2,3,3,2,1,1,2/,INDJY/1,1,2,1,1,3,1,2,1,2,1,4,1,3,2,1,1,2,3,2/, &
           INDJZ/1,1,1,2,1,1,3,1,2,2,1,1,4,1,1,2,3,3,2,2/
!
! Begin:
      call PRG_manager ('enter', 'I1ECLC', '1EINT%AO')
!
! Object:
      if(.not.allocated(HCORE))then
        allocate (HCORE((Basis%Nbasis*(Basis%Nbasis+1))/2), VINT((Basis%Nbasis*(Basis%Nbasis+1))/2), &
                  Tint((Basis%Nbasis*(Basis%Nbasis+1))/2), OVRLAP((Basis%Nbasis*(Basis%Nbasis+1))/2))
      else
        if(Basis%Nbasis.ne.size(HCORE,1))then
          deallocate (HCORE, VINT, Tint, OVRLAP)
          allocate (HCORE((Basis%Nbasis*(Basis%Nbasis+1))/2), VINT((Basis%Nbasis*(Basis%Nbasis+1))/2), &
                    Tint((Basis%Nbasis*(Basis%Nbasis+1))/2), OVRLAP((Basis%Nbasis*(Basis%Nbasis+1))/2))
        end if
      end if
!
      call RYSSET
!
! Fill INDIX.
      do I=1,20
        INDIX(I)=4*(INDJX(I)-1)
        INDIY(I)=4*(INDJY(I)-1)
        INDIZ(I)=4*(INDJZ(I)-1)
      end do ! I
!
! Loop over shells.
! Loop over Ishell.
      do Ishell=1,Basis%Nshells
      LAMAX=Basis%shell(Ishell)%Xtype+1
      Istart=Basis%shell(Ishell)%Xstart
      Iend=Basis%shell(Ishell)%XEND
      Irange=Iend-Istart+1
      IGBGN=Basis%shell(Ishell)%EXPBGN
      IGEND=Basis%shell(Ishell)%EXPEND
      Ifrst=Basis%shell(Ishell)%frstSHL
!
! Loop over Jshell.
      do Jshell=1,Ishell
      LBMAX=Basis%shell(Jshell)%Xtype+1
      Jstart=Basis%shell(Jshell)%Xstart
      Jend=Basis%shell(Jshell)%XEND
      Jrange=Jend-Jstart+1
      JGBGN=Basis%shell(Jshell)%EXPBGN
      JGEND=Basis%shell(Jshell)%EXPEND
      Jfrst=Basis%shell(Jshell)%frstSHL
!
      Ilast= Basis%shell(Ishell)%lastSHL
      Jlast= Basis%shell(Jshell)%lastSHL
!
! Loop over Iatmshl
      do Iatmshl=Ifrst,Ilast
      XA=CARTESIAN(Basis%atmshl(Iatmshl)%ATMLST)%X
      YA=CARTESIAN(Basis%atmshl(Iatmshl)%ATMLST)%Y
      ZA=CARTESIAN(Basis%atmshl(Iatmshl)%ATMLST)%Z
      Iaos=Basis%atmshl(Iatmshl)%frstAO-1
!
! Loop over Jatmshl
      IF(Ishell.EQ.Jshell)Jlast=Iatmshl
      do Jatmshl=Jfrst,Jlast
      XB=CARTESIAN(Basis%atmshl(Jatmshl)%ATMLST)%X
      YB=CARTESIAN(Basis%atmshl(Jatmshl)%ATMLST)%Y
      ZB=CARTESIAN(Basis%atmshl(Jatmshl)%ATMLST)%Z
      Jaos=Basis%atmshl(Jatmshl)%frstAO-1
!
      LPMAX=LAMAX+LBMAX-1
      LIM1DS=(LPMAX+3)/2
      LENTQ=Irange*Jrange
      NZERO=(LAMAX+LBMAX-2)/2+1
      ABX=XB-XA
      ABY=YB-YA
      ABZ=ZB-ZA
      RABSQ=ABX*ABX+ABY*ABY+ABZ*ABZ
!
      do I=1,LENTQ
        SS(I)=ZERO
        EEPA(I)=ZERO
        EEPT(I)=ZERO
        EEPV(I)=ZERO
      end do ! I
!
! Loop over primitive Gaussians.
!
      do Igauss=IGBGN,IGEND
      AS=Basis%gaussian(Igauss)%exp
      TWOASQ=TWO*AS*AS
      ASXA=AS*XA
      ASYA=AS*YA
      ASZA=AS*ZA
      ARABSQ=AS*RABSQ
      call FILLCC (LAMAX, Basis%gaussian(Igauss)%CONTRC, CA)

      do Jgauss=JGBGN,JGEND
      BS=Basis%gaussian(Jgauss)%exp
      call FILLCC (LBMAX, Basis%gaussian(Jgauss)%CONTRC, CB)
      EP=AS+BS
      EPI=ONE/EP
      EPIO2=PT5*EPI
      TWOP=EP+EP
      ARG=-BS*ARABSQ*EPI
      PEXP=ZERO
      IF(ARG.GT.CUT1)PEXP=DEXP(ARG)
      ZTEMP=TWOPI*EPI*PEXP
      PX=(ASXA+BS*XB)*EPI
      PY=(ASYA+BS*YB)*EPI
      PZ=(ASZA+BS*ZB)*EPI
      XAP=PX-XA
      XBP=PX-XB
      YAP=PY-YA
      YBP=PY-YB
      ZAP=PZ-ZA
      ZBP=PZ-ZB
      call GETCC1 (CCX, XAP, XBP, LAMAX+2, LBMAX+2)
      call GETCC1 (CCY, YAP, YBP, LAMAX+2, LBMAX+2)
      call GETCC1 (CCZ, ZAP, ZBP, LAMAX+2, LBMAX+2)
!
! Zero accumulation area.
      do I=1,LENTQ
        EEPB(I)=ZERO
      end do ! I
!
! Loop over atoms.
      do Iatom=1,Natoms
      IA=CARTESIAN(Iatom)%Atomic_number
      IF(IA.GT.0)then
      XC=CARTESIAN(Iatom)%X
      YC=CARTESIAN(Iatom)%Y
      ZC=CARTESIAN(Iatom)%Z
      ZT=ZTEMP*DBLE(IA)
      PCX=XC-PX
      PCY=YC-PY
      PCZ=ZC-PZ
      RPCSQ=PCX*PCX+PCY*PCY+PCZ*PCZ
      ARG=EP*RPCSQ
      call RPOLX (NZERO, ARG, TP, WP)
      call GETA1 (A, EPIO2, 0, LPMAX)
!
! Loop over zeroes of Rys polynomial.
      do IZERO=1,NZERO
      TWOPT2=TWOP*TP(IZERO)
      ZCONST=ZT*WP(IZERO)
      call GET2C (TWOCX, PCX, ONE, A, TWOPT2, 0, LPMAX)
      call GET2C (TWOCY, PCY, ONE, A, TWOPT2, 0, LPMAX)
      call GET2C (TWOCZ, PCZ, ZCONST, A, TWOPT2, 0, LPMAX)
      call GET3C (XIP, 0, TWOCX, CCX, LAMAX, LBMAX)
      call GET3C (YIP, 0, TWOCY, CCY, LAMAX, LBMAX)
      call GET3C (ZIP, 0, TWOCZ, CCZ, LAMAX, LBMAX)
!
! Loop over atomic orbitals.
      INTC=0
      do I=Istart,Iend
      IX=INDIX(I)
      IY=INDIY(I)
      IZ=INDIZ(I)
!
      do J=Jstart,Jend
      JX=INDJX(J)
      JY=INDJY(J)
      JZ=INDJZ(J)
      INTC=INTC+1
      EEPB(INTC)=EEPB(INTC)+XIP(IX+JX)*YIP(IY+JY)*ZIP(IZ+JZ)
      end do ! J
      end do ! I
! End of AO loop.
!
      end do ! IZERO
! End of loop over Rys zeroes.
!
      end if ! IA
      end do ! Iatom
! End of loop over atoms.
!
! Apply the contraction coefficients.
! The potential integrals, already in EEPB, will be combined with
! the kinetic integrals to form the core Hamiltonian (H=T+V).
!
! Calculate the overlap and kinetic energy integrals.
      STERM=DSQRT(EPI*PI_VAL)
      call GET1CS (S1C, STERM, EPIO2, (LPMAX+5)/2)
      call GET2CS (SX, S1C, CCX, LAMAX+2, LBMAX)
      call GET2CS (SY, S1C, CCY, LAMAX+2, LBMAX)
      do I=1,LIM1DS
      S1C(I)=S1C(I)*PEXP
      end do ! I
      call GET2CS (SZ, S1C, CCZ, LAMAX+2, LBMAX)
!
! Begin loop over atomic orbitals for overlap and kinetic energy
! integrals.
!
      INTC=0
      do I=Istart,Iend
      IX=INDIX(I)
      IY=INDIY(I)
      IZ=INDIZ(I)
      IXP=INDJX(I)
      IYP=INDJY(I)
      IZP=INDJZ(I)
      XK=DBLE(2*(IXP+IYP+IZP)-3)*AS
      XIIM1=DBLE((IXP-1)*(IXP-2))
      YIIM1=DBLE((IYP-1)*(IYP-2))
      ZIIM1=DBLE((IZP-1)*(IZP-2))
      do J=Jstart,Jend
      JX=INDJX(J)
      JY=INDJY(J)
      JZ=INDJZ(J)
      IJX=IX+JX
      IJY=IY+JY
      IJZ=IZ+JZ
      INTC=INTC+1
      COEF=CA(I)*CB(J)
      SYZ=SY(IJY)*SZ(IJZ)
      TEMPS=SX(IJX)*SYZ
      SS(INTC)=SS(INTC)+TEMPS*COEF
      TEMPK=TEMPS*XK
      TEMPK=TEMPK-TWOASQ*(SX(IJX+8)*SYZ+SX(IJX)*(SY(IJY+8)*SZ(IJZ)+SY(IJY)*SZ(IJZ+8)))
      TK=ZERO
      IF(IXP.GT.2)TK=XIIM1*SX(IJX-8)*SYZ
      IF(IYP.GT.2)TK=TK+YIIM1*SX(IJX)*SY(IJY-8)*SZ(IJZ)
      IF(IZP.GT.2)TK=TK+ZIIM1*SX(IJX)*SY(IJY)*SZ(IJZ-8)
      TEMPK=TEMPK-PT5*TK
      ! Divide by mass of 'electron'
      TEMPK = TEMPK/masse
      EEPA(INTC)=EEPA(INTC)+(TEMPK-EEPB(INTC))*COEF
      EEPV(INTC)=EEPV(INTC)-EEPB(INTC)*COEF          ! new
      EEPT(INTC)=EEPT(INTC)+TEMPK*COEF     ! new
      end do ! J
      end do ! I
!
      end do ! Jgauss
      end do ! Igauss
! End of loop over Gaussians.
!
! FILMAT takes the integrals in SS, EEPA and EEPB and stores them in
! their proper places in OVRLAP, HCORE and VINT.
      call FILMAT (SS, OVRLAP, MATlen, Iend, Jend, &
                         Iatmshl, Jatmshl, Irange, Jrange, Iaos, JaoS)
      call FILMAT (EEPA, HCORE, MATlen, Iend, Jend, &
                         Iatmshl, Jatmshl, Irange, Jrange, Iaos, JaoS)
      call FILMAT (EEPV, VINT, MATlen, Iend, Jend, &
                         Iatmshl, Jatmshl, Irange, Jrange, Iaos, JaoS)
      call FILMAT (EEPT, Tint, MATlen, Iend, Jend, &
                         Iatmshl, Jatmshl, Irange, Jrange, Iaos, JaoS)
!
      end do ! Jatmshl
      end do ! Iatmshl
      end do ! Jshell
      end do ! Ishell
!
! End of loop over shells.
!        write(UNIout,*)'OVERLAP INTEGRALS'
!        call PRT_matrix (OVRLAP, MATlen, Basis%Nbasis)
!        write(UNIout,*)'CORE HAMILTONIAN (H=T+V)'
!        call PRT_matrix (HCORE, MATlen, Basis%Nbasis)
!        write(UNIout,*)'NUCLEAR ATTRACTION (V)'
!        call PRT_matrix (VINT, MATlen, Basis%Nbasis)
!        write(UNIout,*)'KINETIC (T)'
!        call PRT_matrix (TINT, MATlen, Basis%Nbasis)
!
! End of routine I1ECLC
      call PRG_manager ('exit', 'I1ECLC', '1EINT%AO')
      return
      end subroutine I1ECLC
      subroutine DIPOLE_1e
!***********************************************************************
!     Date last modified: November 6, 1996                 Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version.                                       *
!     Calculation of X, Y and Z dipole moment integrals.               *
!***********************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE type_basis_set
      USE QM_objects
      USE INT_objects
      USE matrix_print

      implicit none
!
! Local scalars:
      integer :: Ishell,Jshell,Ifrst,Jfrst,Ilast,Jlast
      integer :: Istart,Jstart,Iend,Jend
      integer :: Iatmshl,Jatmshl
      integer :: LAMAX,LBMAX
      integer :: Iatom
      integer :: Irange,Jrange
      integer :: Igauss,Jgauss
      integer :: Iaos,JaoS
      integer :: IGBGN,JGBGN,IGend,JGend
      integer LPMAX,I,INTC,IX,IXNEW,IY,IYNEW,IZ,IZNEW,J,JX,JY,JZ,LIM1DS
      integer :: LENTQ
      double precision ABX,ABY,ABZ,ARABSQ,ARGP,AS,ASXA,ASYA,ASZA,BS,COEF,CUT1,EP,EPI,EPIO2,PEXP,PX,PY,PZ, &
                       STERM,XAP,XBP,YAP,YBP,ZAP,ZBP
      double precision :: XA,XB,YA,YB,ZA,ZB,RABSQ
!
! Local arrays:
      integer INDIX(20),INDIY(20),INDIZ(20),INDJX(20),INDJY(20),INDJZ(20)
      double precision CA(20),CB(20),CCX(192),CCY(192),CCZ(192),DDX(100),DDY(100),DDZ(100),SX(32),SY(32),SZ(32),S1C(6)
!
      parameter (CUT1=-100.0D0)
!
      DATA INDJX/1,2,1,1,3,1,1,2,2,1,4,1,1,2,3,3,2,1,1,2/,INDJY/1,1,2,1,1,3,1,2,1,2,1,4,1,3,2,1,1,2,3,2/, &
           INDJZ/1,1,1,2,1,1,3,1,2,2,1,1,4,1,1,2,3,3,2,2/
!
! Begin:
      call PRG_manager ('enter', 'DIPOLE_1e', '1EINT%DIPOLE')
!
! Object:
      if(.not.allocated(DipoleX_AO))then
        allocate (DipoleX_AO((Basis%Nbasis*(Basis%Nbasis+1))/2), DipoleY_AO((Basis%Nbasis*(Basis%Nbasis+1))/2), &
                  DipoleZ_AO((Basis%Nbasis*(Basis%Nbasis+1))/2))
      else
        if(Basis%Nbasis.ne.size(DipoleX_AO,1))then
          deallocate (DipoleX_AO, DipoleY_AO, DipoleZ_AO)
        allocate (DipoleX_AO((Basis%Nbasis*(Basis%Nbasis+1))/2), DipoleY_AO((Basis%Nbasis*(Basis%Nbasis+1))/2), &
                  DipoleZ_AO((Basis%Nbasis*(Basis%Nbasis+1))/2))
        end if
      end if

      DipoleX_AO=ZERO
      DipoleY_AO=ZERO
      DipoleZ_AO=ZERO

      call RYSSET
!
      do I=1,20
        INDIX(I)=4*(INDJX(I)-1)
        INDIY(I)=4*(INDJY(I)-1)
        INDIZ(I)=4*(INDJZ(I)-1)
      end do ! I
!
! Loop over Ishell.
      do Ishell=1,Basis%Nshells
      LAMAX=Basis%shell(Ishell)%Xtype+1
      Istart=Basis%shell(Ishell)%Xstart
      Iend=Basis%shell(Ishell)%XEND
      Irange=Iend-Istart+1
      IGBGN=Basis%shell(Ishell)%EXPBGN
      IGEND=Basis%shell(Ishell)%EXPEND
      Ifrst=Basis%shell(Ishell)%frstSHL
!
! Loop over Jshell.
      do Jshell=1,Ishell
      LBMAX=Basis%shell(Jshell)%Xtype+1
      Jstart=Basis%shell(Jshell)%Xstart
      Jend=Basis%shell(Jshell)%XEND
      Jrange=Jend-Jstart+1
      JGBGN=Basis%shell(Jshell)%EXPBGN
      JGEND=Basis%shell(Jshell)%EXPEND
      Jfrst=Basis%shell(Jshell)%frstSHL

      Ilast= Basis%shell(Ishell)%lastSHL
      Jlast= Basis%shell(Jshell)%lastSHL
!
! Loop over Iatmshl
      do Iatmshl=Ifrst,Ilast
      XA=CARTESIAN(Basis%atmshl(Iatmshl)%ATMLST)%X
      YA=CARTESIAN(Basis%atmshl(Iatmshl)%ATMLST)%Y
      ZA=CARTESIAN(Basis%atmshl(Iatmshl)%ATMLST)%Z
      Iaos=Basis%atmshl(Iatmshl)%frstAO-1
!
! Loop over Jatmshl
      IF(Ishell.EQ.Jshell)Jlast=Iatmshl
      do Jatmshl=Jfrst,Jlast
      XB=CARTESIAN(Basis%atmshl(Jatmshl)%ATMLST)%X
      YB=CARTESIAN(Basis%atmshl(Jatmshl)%ATMLST)%Y
      ZB=CARTESIAN(Basis%atmshl(Jatmshl)%ATMLST)%Z
      Jaos=Basis%atmshl(Jatmshl)%frstAO-1
!
      LPMAX=LAMAX+LBMAX-1
      LIM1DS=(LPMAX+2)/2
      LENTQ=Irange*Jrange
      ABX=XB-XA
      ABY=YB-YA
      ABZ=ZB-ZA
      RABSQ=ABX*ABX+ABY*ABY+ABZ*ABZ
      do I=1,LENTQ
      DDX(I)=ZERO
      DDY(I)=ZERO
      DDZ(I)=ZERO
      end do ! I
!
! Begin loop over primitive Gaussians.
      do Igauss=IGBGN,IGEND
      AS=Basis%gaussian(Igauss)%exp
      ASXA=AS*XA
      ASYA=AS*YA
      ASZA=AS*ZA
      ARABSQ=AS*RABSQ
      call FILLCC (LAMAX, Basis%gaussian(Igauss)%CONTRC, CA)

      do Jgauss=JGBGN,JGEND
      BS=Basis%gaussian(Jgauss)%exp
      call FILLCC (LBMAX, Basis%gaussian(Jgauss)%CONTRC, CB)
      EP=AS+BS
      EPI=ONE/EP
      EPIO2=PT5*EPI
      ARGP=-BS*ARABSQ*EPI
      PEXP=ZERO
      IF(ARGP.GT.CUT1)PEXP=DEXP(ARGP)
      PX=(ASXA+BS*XB)*EPI
      PY=(ASYA+BS*YB)*EPI
      PZ=(ASZA+BS*ZB)*EPI
      XAP=PX-XA
      YAP=PY-YA
      ZAP=PZ-ZA
      XBP=PX-XB
      YBP=PY-YB
      ZBP=PZ-ZB
      call GETCC1 (CCX, XAP, XBP, LAMAX+1, LBMAX+1)
      call GETCC1 (CCY, YAP, YBP, LAMAX+1, LBMAX+1)
      call GETCC1 (CCZ, ZAP, ZBP, LAMAX+1, LBMAX+1)
      STERM=DSQRT(EPI*PI_VAL)
      call GET1CS (S1C, STERM, EPIO2, (LPMAX+3)/2)
      call GET2CS (SX, S1C, CCX, LAMAX+1, LBMAX+1)
      call GET2CS (SY, S1C, CCY, LAMAX+1, LBMAX+1)
      do I=1,LIM1DS
      S1C(I)=S1C(I)*PEXP
      end do ! I
      call GET2CS (SZ, S1C, CCZ, LAMAX+1, LBMAX+1)
!
! Begin loop over atomic orbitals.
      INTC=0
      do I=Istart,Iend
      IX=INDIX(I)
      IY=INDIY(I)
      IZ=INDIZ(I)
      IXNEW=IX+4
      IYNEW=IY+4
      IZNEW=IZ+4
      do J=Jstart,Jend
      JX=INDJX(J)
      JY=INDJY(J)
      JZ=INDJZ(J)
      INTC=INTC+1
      COEF=CA(I)*CB(J)
! Dipole moment integrals.
      DDX(INTC)=DDX(INTC)+COEF*SY(IY+JY)*SZ(IZ+JZ)*(XA*SX(IX+JX)+SX(IXNEW+JX))
      DDY(INTC)=DDY(INTC)+COEF*SX(IX+JX)*SZ(IZ+JZ)*(YA*SY(IY+JY)+SY(IYNEW+JY))
      DDZ(INTC)=DDZ(INTC)+COEF*SX(IX+JX)*SY(IY+JY)*(ZA*SZ(IZ+JZ)+SZ(IZNEW+JZ))
      end do ! J
      end do ! I
! End of loop over atomic orbitals.
!
      end do ! Jgauss
      end do ! Igauss
! End of loop over Gaussians.
!
! FILMAT takes the integrals in DDX, DDY and DDZ, and stores them
      call FILMAT (DDX, DipoleX_AO, MATlen, Iend, Jend, &
                         Iatmshl, Jatmshl, Irange, Jrange, Iaos, JaoS)
      call FILMAT (DDY, DipoleY_AO, MATlen, Iend, Jend, &
                         Iatmshl, Jatmshl, Irange, Jrange, Iaos, JaoS)
      call FILMAT (DDZ, DipoleZ_AO, MATlen, Iend, Jend, &
                         Iatmshl, Jatmshl, Irange, Jrange, Iaos, JaoS)
      end do ! Jatmshl
      end do ! Iatmshl
      end do ! Jshell
      end do ! Ishell
! End of loop over shells.
!
! End of routine DIPOLE_1e
      call PRG_manager ('exit', 'DIPOLE_1e', '1EINT%DIPOLE')
      return
      end subroutine DIPOLE_1e
      subroutine VDIPOLE
!***********************************************************************
!     Date last modified: November 6, 1996                 Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Calculation of DELX, DELY, DELZ dipole velocity integrals.       *
!***********************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE type_basis_set
      USE QM_objects
      USE INT_objects
      USE matrix_print

      implicit none
!
! Local scalars:
      integer :: Ishell,Jshell,Ifrst,Jfrst,Ilast,Jlast
      integer :: Istart,Jstart,Iend,Jend
      integer :: Iatmshl,Jatmshl
      integer :: LAMAX,LBMAX
      integer :: Iatom
      integer :: Irange,Jrange
      integer :: Igauss,Jgauss
      integer :: Iaos,JaoS
      integer :: IGBGN,JGBGN,IGend,JGend
      integer I,INTC,IX,IY,IZ,J,JX,JY,JZ,LIM1DS,LPMAX
      integer :: LENTQ
      double precision :: ABX,ABY,ABZ,ARABSQ,ARGP,AS,ASXA,ASYA,ASZA,BS,COEF, &
                       CUT1,EP,EPI,EPIO2,PEXP,PX,PY,PZ, &
                       STERM,TERMX,TERMY,TERMZ,XAP,XBP,YAP,YBP,ZAP,ZBP
      double precision :: XA,XB,YA,YB,ZA,ZB,RABSQ
!
! Local arrays:
      integer :: INDIX(20),INDIY(20),INDIZ(20),INDJX(20),INDJY(20),INDJZ(20)
      double precision :: CA(20),CB(20),CCX(192),CCY(192),CCZ(192), &
                       DDX(100),DDY(100),DDZ(100),SX(32),SY(32),SZ(32),S1C(6)
!
      parameter (CUT1=-100.0D0)
!
      DATA INDJX/1,2,1,1,3,1,1,2,2,1,4,1,1,2,3,3,2,1,1,2/, &
           INDJY/1,1,2,1,1,3,1,2,1,2,1,4,1,3,2,1,1,2,3,2/, &
           INDJZ/1,1,1,2,1,1,3,1,2,2,1,1,4,1,1,2,3,3,2,2/
!
!
! Begin:
      call PRG_manager ('enter', 'VDIPOLE', 'VDIPOLE%AO')
!
      if(.not.allocated(VDDx))then
        allocate (VDDx(MATlen), VDDy(MATlen), VDDz(MATlen))
      else
        deallocate (VDDx, VDDy, VDDz)
        allocate (VDDx(MATlen), VDDy(MATlen), VDDz(MATlen))
      end if

      do I=1,20
        INDIX(I)=4*(INDJX(I)-1)
        INDIY(I)=4*(INDJY(I)-1)
        INDIZ(I)=4*(INDJZ(I)-1)
      end do ! I
!
! Loop over Ishell.
      do Ishell=1,Basis%Nshells
      LAMAX=Basis%shell(Ishell)%Xtype+1
      Istart=Basis%shell(Ishell)%Xstart
      Iend=Basis%shell(Ishell)%XEND
      Irange=Iend-Istart+1
      IGBGN=Basis%shell(Ishell)%EXPBGN
      IGEND=Basis%shell(Ishell)%EXPEND
      Ifrst=Basis%shell(Ishell)%frstSHL
!
! Loop over Jshell.
      do Jshell=1,Ishell
      LBMAX=Basis%shell(Jshell)%Xtype+1
      Jstart=Basis%shell(Jshell)%Xstart
      Jend=Basis%shell(Jshell)%XEND
      Jrange=Jend-Jstart+1
      JGBGN=Basis%shell(Jshell)%EXPBGN
      JGEND=Basis%shell(Jshell)%EXPEND
      Jfrst=Basis%shell(Jshell)%frstSHL

      Ilast= Basis%shell(Ishell)%lastSHL
      Jlast= Basis%shell(Jshell)%lastSHL
!
! Loop over Iatmshl
      do Iatmshl=Ifrst,Ilast
      XA=CARTESIAN(Basis%atmshl(Iatmshl)%ATMLST)%X
      YA=CARTESIAN(Basis%atmshl(Iatmshl)%ATMLST)%Y
      ZA=CARTESIAN(Basis%atmshl(Iatmshl)%ATMLST)%Z
      Iaos=Basis%atmshl(Iatmshl)%frstAO-1
!
! Loop over Jatmshl
      IF(Ishell.EQ.Jshell)Jlast=Iatmshl
      do Jatmshl=Jfrst,Jlast
      XB=CARTESIAN(Basis%atmshl(Jatmshl)%ATMLST)%X
      YB=CARTESIAN(Basis%atmshl(Jatmshl)%ATMLST)%Y
      ZB=CARTESIAN(Basis%atmshl(Jatmshl)%ATMLST)%Z
      Jaos=Basis%atmshl(Jatmshl)%frstAO-1
!
      LPMAX=LAMAX+LBMAX-1
      LIM1DS=(LPMAX+2)/2
      LENTQ=Irange*Jrange
      ABX=XB-XA
      ABY=YB-YA
      ABZ=ZB-ZA
      RABSQ=ABX*ABX+ABY*ABY+ABZ*ABZ
      do I=1,LENTQ
      DDX(I)=ZERO
      DDY(I)=ZERO
      DDZ(I)=ZERO
      end do ! I
!
! Begin loop over primitive Gaussians.
      do Igauss=IGBGN,IGEND
      AS=Basis%gaussian(Igauss)%exp
      ASXA=AS*XA
      ASYA=AS*YA
      ASZA=AS*ZA
      ARABSQ=AS*RABSQ
      call FILLCC (LAMAX, Basis%gaussian(Igauss)%CONTRC, CA)
      do Jgauss=JGBGN,JGEND
      BS=Basis%gaussian(Jgauss)%exp
      call FILLCC (LBMAX, Basis%gaussian(Jgauss)%CONTRC, CB)
      EP=AS+BS
      EPI=ONE/EP
      EPIO2=PT5*EPI
      ARGP=-BS*ARABSQ*EPI
      PEXP=ZERO
      IF(ARGP.GT.CUT1)PEXP=DEXP(ARGP)
      PX=(ASXA+BS*XB)*EPI
      PY=(ASYA+BS*YB)*EPI
      PZ=(ASZA+BS*ZB)*EPI
      XAP=PX-XA
      YAP=PY-YA
      ZAP=PZ-ZA
      XBP=PX-XB
      YBP=PY-YB
      ZBP=PZ-ZB
      call GETCC1 (CCX, XAP, XBP, LAMAX+1, LBMAX+1)
      call GETCC1 (CCY, YAP, YBP, LAMAX+1, LBMAX+1)
      call GETCC1 (CCZ, ZAP, ZBP, LAMAX+1, LBMAX+1)
      STERM=DSQRT(EPI*PI_VAL)
      call GET1CS (S1C, STERM, EPIO2, (LPMAX+3)/2)
      call GET2CS (SX, S1C, CCX, LAMAX+1, LBMAX+1)
      call GET2CS (SY, S1C, CCY, LAMAX+1, LBMAX+1)
      do I=1,LIM1DS
      S1C(I)=S1C(I)*PEXP
      end do ! I
      call GET2CS (SZ, S1C, CCZ, LAMAX+1, LBMAX+1)
!
! Begin loop over atomic orbitals.
      INTC=0
      do I=Istart,Iend
      IX=INDIX(I)
      IY=INDIY(I)
      IZ=INDIZ(I)
      do J=Jstart,Jend
      JX=INDJX(J)
      JY=INDJY(J)
      JZ=INDJZ(J)
      INTC=INTC+1
      COEF=CA(I)*CB(J)
! Dipole velocity integrals.
      TERMX=-TWO*BS*SX(IX+JX+1)
      TERMY=-TWO*BS*SY(IY+JY+1)
      TERMZ=-TWO*BS*SZ(IZ+JZ+1)
      IF(JX.NE.1)TERMX=TERMX+DBLE(JX-1)*SX(IX+JX-1)
      IF(JY.NE.1)TERMY=TERMY+DBLE(JY-1)*SY(IY+JY-1)
      IF(JZ.NE.1)TERMZ=TERMZ+DBLE(JZ-1)*SZ(IZ+JZ-1)
      DDX(INTC)=DDX(INTC)+COEF*SY(IY+JY)*SZ(IZ+JZ)*TERMX
      DDY(INTC)=DDY(INTC)+COEF*SX(IX+JX)*SZ(IZ+JZ)*TERMY
      DDZ(INTC)=DDZ(INTC)+COEF*SX(IX+JX)*SY(IY+JY)*TERMZ
      end do ! J
      end do ! I
!
! End of loop over atomic orbitals.
      end do ! Jgauss
      end do ! Igauss
! End of loop over Gaussians.
!
! FILMAT takes the integrals in DDX, DDY and DDZ, and stores them in the
! proper places in VDDX, VDDY and VDDZ.
      call FILMAT (DDX, VDDX, MATlen, Iend, Jend, &
                         Iatmshl, Jatmshl, Irange, Jrange, Iaos, JaoS)
      call FILMAT (DDY, VDDY, MATlen, Iend, Jend, &
                         Iatmshl, Jatmshl, Irange, Jrange, Iaos, JaoS)
      call FILMAT (DDZ, VDDZ, MATlen, Iend, Jend, &
                         Iatmshl, Jatmshl, Irange, Jrange, Iaos, JaoS)
      end do ! Jatmshl
      end do ! Iatmshl
      end do ! Jshell
      end do ! Ishell
! End of loop over shells.
!
! End of routine VDIPOLE
      call PRG_manager ('exit', 'VDIPOLE', 'VDIPOLE%AO')
      return
      end subroutine VDIPOLE
      subroutine MDIPOLE
!***********************************************************************
!     Date last modified: November 6, 1996                 Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Calculation of R x DEL magnetic dipole moment integrals.         *
!***********************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE type_basis_set
      USE QM_objects
      USE INT_objects
      USE matrix_print

      implicit none
!
! Local scalars:
      integer :: Ishell,Jshell,Ifrst,Jfrst,Ilast,Jlast
      integer :: Istart,Jstart,Iend,Jend
      integer :: Iatmshl,Jatmshl
      integer :: LAMAX,LBMAX
      integer :: Iatom
      integer :: Irange,Jrange
      integer :: Igauss,Jgauss
      integer :: Iaos,JaoS
      integer :: IGBGN,JGBGN,IGend,JGend
      integer I,INTC,IX,IY,IZ,J,JX,JY,JZ,LIM1DS,LPMAX
      integer :: LENTQ
      double precision ABX,ABY,ABZ,ARABSQ,ARGP,AS,ASXA,ASYA,ASZA,BS,COEF,CUT1,EP,EPI,EPIO2,PEXP,PX,PY,PZ, &
                       RXDX,RXDY,RXDZ,STERM,TERMX,TERMY,TERMZ,XAP,XBP,XMOM,YAP,YBP,YMOM,ZAP,ZBP,ZMOM
      double precision :: XA,XB,YA,YB,ZA,ZB,RABSQ
!
! Local arrays:
      integer INDIX(20),INDIY(20),INDIZ(20),INDJX(20),INDJY(20),INDJZ(20)
      double precision CA(20),CB(20),CCX(192),CCY(192),CCZ(192),DDX(100),DDY(100),DDZ(100),SX(32),SY(32),SZ(32),S1C(6)
!
      parameter (CUT1=-100.0D0)
!
      DATA INDJX/1,2,1,1,3,1,1,2,2,1,4,1,1,2,3,3,2,1,1,2/,INDJY/1,1,2,1,1,3,1,2,1,2,1,4,1,3,2,1,1,2,3,2/, &
           INDJZ/1,1,1,2,1,1,3,1,2,2,1,1,4,1,1,2,3,3,2,2/
!
! Begin:
      call PRG_manager ('enter', 'MDIPOLE', 'MDIPOLE%AO')
!
      if(.not.allocated(xMDPOL))then
        allocate (xMDPOL(MATlen), yMDPOL(MATlen), zMDPOL(MATlen))
      else
        deallocate (xMDPOL, yMDPOL, zMDPOL)
        allocate (xMDPOL(MATlen), yMDPOL(MATlen), zMDPOL(MATlen))
      end if

      do I=1,20
        INDIX(I)=4*(INDJX(I)-1)
        INDIY(I)=4*(INDJY(I)-1)
        INDIZ(I)=4*(INDJZ(I)-1)
      end do ! I
!
! Loop over Ishell.
      do Ishell=1,Basis%Nshells
      LAMAX=Basis%shell(Ishell)%Xtype+1
      Istart=Basis%shell(Ishell)%Xstart
      Iend=Basis%shell(Ishell)%XEND
      Irange=Iend-Istart+1
      IGBGN=Basis%shell(Ishell)%EXPBGN
      IGEND=Basis%shell(Ishell)%EXPEND
      Ifrst=Basis%shell(Ishell)%frstSHL
!
! Loop over Jshell.
      do Jshell=1,Ishell
      LBMAX=Basis%shell(Jshell)%Xtype+1
      Jstart=Basis%shell(Jshell)%Xstart
      Jend=Basis%shell(Jshell)%XEND
      Jrange=Jend-Jstart+1
      JGBGN=Basis%shell(Jshell)%EXPBGN
      JGEND=Basis%shell(Jshell)%EXPEND
      Jfrst=Basis%shell(Jshell)%frstSHL

      Ilast= Basis%shell(Ishell)%lastSHL
      Jlast= Basis%shell(Jshell)%lastSHL
!
! Loop over Iatmshl
      do Iatmshl=Ifrst,Ilast
      XA=CARTESIAN(Basis%atmshl(Iatmshl)%ATMLST)%X
      YA=CARTESIAN(Basis%atmshl(Iatmshl)%ATMLST)%Y
      ZA=CARTESIAN(Basis%atmshl(Iatmshl)%ATMLST)%Z
      Iaos=Basis%atmshl(Iatmshl)%frstAO-1
!
! Loop over Jatmshl
      IF(Ishell.EQ.Jshell)Jlast=Iatmshl
      do Jatmshl=Jfrst,Jlast
      XB=CARTESIAN(Basis%atmshl(Jatmshl)%ATMLST)%X
      YB=CARTESIAN(Basis%atmshl(Jatmshl)%ATMLST)%Y
      ZB=CARTESIAN(Basis%atmshl(Jatmshl)%ATMLST)%Z
      Jaos=Basis%atmshl(Jatmshl)%frstAO-1
!
      LPMAX=LAMAX+LBMAX-1
      LIM1DS=(LPMAX+2)/2
      LENTQ=Irange*Jrange
      ABX=XB-XA
      ABY=YB-YA
      ABZ=ZB-ZA
      RABSQ=ABX*ABX+ABY*ABY+ABZ*ABZ
      do I=1,LENTQ
      DDX(I)=ZERO
      DDY(I)=ZERO
      DDZ(I)=ZERO
      end do ! I
!
! Begin loop over primitive Gaussians.
      do Igauss=IGBGN,IGEND
      AS=Basis%gaussian(Igauss)%exp
      ASXA=AS*XA
      ASYA=AS*YA
      ASZA=AS*ZA
      ARABSQ=AS*RABSQ
      call FILLCC (LAMAX, Basis%gaussian(Igauss)%CONTRC, CA)
      do Jgauss=JGBGN,JGEND
      BS=Basis%gaussian(Jgauss)%exp
      call FILLCC (LBMAX, Basis%gaussian(Jgauss)%CONTRC, CB)
      EP=AS+BS
      EPI=ONE/EP
      EPIO2=PT5*EPI
      ARGP=-BS*ARABSQ*EPI
      PEXP=ZERO
      IF(ARGP.GT.CUT1)PEXP=DEXP(ARGP)
      PX=(ASXA+BS*XB)*EPI
      PY=(ASYA+BS*YB)*EPI
      PZ=(ASZA+BS*ZB)*EPI
      XAP=PX-XA
      YAP=PY-YA
      ZAP=PZ-ZA
      XBP=PX-XB
      YBP=PY-YB
      ZBP=PZ-ZB
      call GETCC1 (CCX, XAP, XBP, LAMAX+1, LBMAX+1)
      call GETCC1 (CCY, YAP, YBP, LAMAX+1, LBMAX+1)
      call GETCC1 (CCZ, ZAP, ZBP, LAMAX+1, LBMAX+1)
      STERM=DSQRT(EPI*PI_VAL)
      call GET1CS (S1C, STERM, EPIO2, (LPMAX+3)/2)
      call GET2CS (SX, S1C, CCX, LAMAX+1, LBMAX+1)
      call GET2CS (SY, S1C, CCY, LAMAX+1, LBMAX+1)
      do I=1,LIM1DS
      S1C(I)=S1C(I)*PEXP
      end do ! I
      call GET2CS (SZ, S1C, CCZ, LAMAX+1, LBMAX+1)
!
! Begin loop over atomic orbitals.
      INTC=0
      do I=Istart,Iend
      IX=INDIX(I)
      IY=INDIY(I)
      IZ=INDIZ(I)
      do J=Jstart,Jend
      JX=INDJX(J)
      JY=INDJY(J)
      JZ=INDJZ(J)
      INTC=INTC+1
      COEF=CA(I)*CB(J)
! Magnetic dipole moment integrals.
      TERMX=-TWO*BS*SX(IX+JX+1)
      TERMY=-TWO*BS*SY(IY+JY+1)
      TERMZ=-TWO*BS*SZ(IZ+JZ+1)
      IF(JX.GT.1)TERMX=TERMX+DBLE(JX-1)*SX(IX+JX-1)
      IF(JY.GT.1)TERMY=TERMY+DBLE(JY-1)*SY(IY+JY-1)
      IF(JZ.GT.1)TERMZ=TERMZ+DBLE(JZ-1)*SZ(IZ+JZ-1)
      ZMOM=SZ(IZ+JZ+4)+ZA*SZ(IZ+JZ)
      YMOM=SY(IY+JY+4)+YA*SY(IY+JY)
      XMOM=SX(IX+JX+4)+XA*SX(IX+JX)
      RXDX=SX(IX+JX)*(YMOM*TERMZ-ZMOM*TERMY)
      RXDY=SY(IY+JY)*(ZMOM*TERMX-XMOM*TERMZ)
      RXDZ=SZ(IZ+JZ)*(XMOM*TERMY-YMOM*TERMX)
      DDX(INTC)=DDX(INTC)+COEF*RXDX
      DDY(INTC)=DDY(INTC)+COEF*RXDY
      DDZ(INTC)=DDZ(INTC)+COEF*RXDZ
      end do ! J
      end do ! I
! End of loop over atomic orbitals.
!
      end do ! Jgauss
      end do ! Igauss
! End of loop over Gaussians.
!
! FILMAT takes the integrals in DDX, DDY and DDZ, and stores them in the
! proper places in XMDPOL, YMDPOL and ZMDPOL.
      call FILMAT (DDX, XMDPOL, MATlen, Iend, Jend, &
                         Iatmshl, Jatmshl, Irange, Jrange, Iaos, JaoS)
      call FILMAT (DDY, YMDPOL, MATlen, Iend, Jend, &
                         Iatmshl, Jatmshl, Irange, Jrange, Iaos, JaoS)
      call FILMAT (DDZ, ZMDPOL, MATlen, Iend, Jend, &
                         Iatmshl, Jatmshl, Irange, Jrange, Iaos, JaoS)
      end do ! Jatmshl
      end do ! Iatmshl
      end do ! Jshell
      end do ! Ishell
! End of loop over shells.
!
! End of routine MDIPOLE
      call PRG_manager ('exit', 'MDIPOLE', 'MDIPOLE%AO')
      return
      end subroutine MDIPOLE
      subroutine GETCC1 (CC, AP, BP, LAMAX, LBMAX)
!***********************************************************************
!     Date last modified: June 19, 1992                    Version 1.0 *
!     Author:                                                          *
!     Description:                                                     *
!     This routine calculates the coefficients which transform         *
!     integrals over functions at center P into integrals over         *
!     functions at centers A and B.  This transformation is carried    *
!     out in routine GET3C, which should be consulted for more         *
!     details on the use of these coefficients.                        *
!                                                                      *
!     The coefficient matrix is three dimensional, although the        *
!     actual indexing is done linearly.  The coefficients are          *
!     calculated from CC(1,1,1)=1.0, and the recursion formulae:       *
!          CC(LW,LB,LA) = AP*CC(LW,LB,LA-1) + CC(LW-1,LB,LA-1)         *
!          CC(LW,LB,LA) = BP*CC(LW,LB-1,LA) + CC(LW-1,LB-1,LA)         *
!                                                                      *
!***********************************************************************
! Modules:
      USE program_constants

      implicit none
!
! Input scalars:
      integer LAMAX,LBMAX
      double precision AP,BP
!
! Input array:
      double precision CC(192)
!
! Begin:
      CC(1)=ONE
      IF(LBMAX.NE.1)then
         CC(2)=BP
         CC(3)=CC(1)
         IF(LBMAX.NE.2)then
            CC(4)=BP*CC(2)
            CC(5)=BP+CC(2)
            CC(6)=CC(3)
            IF(LBMAX.NE.3)then
               CC(7)=BP*CC(4)
               CC(8)=BP*CC(5)+CC(4)
               CC(9)=BP+CC(5)
               CC(10)=CC(6)
            end if
         end if
      end if
!
  200 IF(LAMAX.EQ.1)GO TO 900
      CC(11)=AP
      CC(12)=CC(1)
      IF(LBMAX.NE.1)then
         CC(13)=AP*CC(2)
         CC(14)=AP+CC(2)
         CC(15)=CC(3)
         IF(LBMAX.NE.2)then
            CC(16)=AP*CC(4)
            CC(17)=AP*CC(5)+CC(4)
            CC(18)=AP+CC(5)
            CC(19)=CC(6)
            IF(LBMAX.NE.3)then
               CC(20)=AP*CC(7)
               CC(21)=AP*CC(8)+CC(7)
               CC(22)=AP*CC(9)+CC(8)
               CC(23)=AP+CC(9)
               CC(24)=CC(10)
            end if
         end if
      end if
!
  300 IF(LAMAX.EQ.2)GO TO 900
      CC(25)=AP*CC(11)
      CC(26)=AP+CC(11)
      CC(27)=CC(12)
      IF(LBMAX.NE.1)then
         CC(28)=AP*CC(13)
         CC(29)=AP*CC(14)+CC(13)
         CC(30)=AP+CC(14)
         CC(31)=ONE
         IF(LBMAX.NE.2)then
            CC(32)=AP*CC(16)
            CC(33)=AP*CC(17)+CC(16)
            CC(34)=AP*CC(18)+CC(17)
            CC(35)=AP+CC(18)
            CC(36)=ONE
            IF(LBMAX.NE.3)then
               CC(37)=AP*CC(20)
               CC(38)=AP*CC(21)+CC(20)
               CC(39)=AP*CC(22)+CC(21)
               CC(40)=AP*CC(23)+CC(22)
               CC(41)=AP+CC(23)
               CC(42)=ONE
            end if
         end if
      end if
!
  400 IF(LAMAX.EQ.3)GO TO 900
      CC(43)=AP*CC(25)
      CC(44)=AP*CC(26)+CC(25)
      CC(45)=AP+CC(26)
      CC(46)=ONE
      IF(LBMAX.NE.1)then
         CC(47)=AP*CC(28)
         CC(48)=AP*CC(29)+CC(28)
         CC(49)=AP*CC(30)+CC(29)
         CC(50)=AP+CC(30)
         CC(51)=ONE
         IF(LBMAX.NE.2)then
            CC(52)=AP*CC(32)
            CC(53)=AP*CC(33)+CC(32)
            CC(54)=AP*CC(34)+CC(33)
            CC(55)=AP*CC(35)+CC(34)
            CC(56)=AP+CC(35)
            CC(57)=ONE
            IF(LBMAX.NE.3)then
               CC(58)=AP*CC(37)
               CC(59)=AP*CC(38)+CC(37)
               CC(60)=AP*CC(39)+CC(38)
               CC(61)=AP*CC(40)+CC(39)
               CC(62)=AP*CC(41)+CC(40)
               CC(63)=AP+CC(41)
               CC(64)=ONE
            end if
         end if
      end if
!
  500 IF(LAMAX.EQ.4)GO TO 900
      CC(65)=AP*CC(43)
      CC(66)=AP*CC(44)+CC(43)
      CC(67)=AP*CC(45)+CC(44)
      CC(68)=AP+CC(45)
      CC(69)=ONE
      IF(LBMAX.NE.1)then
         CC(70)=AP*CC(47)
         CC(71)=AP*CC(48)+CC(47)
         CC(72)=AP*CC(49)+CC(48)
         CC(73)=AP*CC(50)+CC(49)
         CC(74)=AP+CC(50)
         CC(75)=ONE
         IF(LBMAX.NE.2)then
            CC(76)=AP*CC(52)
            CC(77)=AP*CC(53)+CC(52)
            CC(78)=AP*CC(54)+CC(53)
            CC(79)=AP*CC(55)+CC(54)
            CC(80)=AP*CC(56)+CC(55)
            CC(81)=AP+CC(56)
            CC(82)=ONE
            IF(LBMAX.NE.3)then
               CC(83)=AP*CC(58)
               CC(84)=AP*CC(59)+CC(58)
               CC(85)=AP*CC(60)+CC(59)
               CC(86)=AP*CC(61)+CC(60)
               CC(87)=AP*CC(62)+CC(61)
               CC(88)=AP*CC(63)+CC(62)
               CC(89)=AP+CC(63)
               CC(90)=ONE
            end if
         end if
      end if
!
  600 IF(LAMAX.EQ.5)GO TO 900
      CC(91)=AP*CC(65)
      CC(92)=AP*CC(66)+CC(65)
      CC(93)=AP*CC(67)+CC(66)
      CC(94)=AP*CC(68)+CC(67)
      CC(95)=AP+CC(68)
      CC(96)=ONE
      IF(LBMAX.NE.1)then
         CC(97)=AP*CC(70)
         CC(98)=AP*CC(71)+CC(70)
         CC(99)=AP*CC(72)+CC(71)
         CC(100)=AP*CC(73)+CC(72)
         CC(101)=AP*CC(74)+CC(73)
         CC(102)=AP+CC(74)
         CC(103)=ONE
         IF(LBMAX.NE.2)then
            CC(104)=AP*CC(76)
            CC(105)=AP*CC(77)+CC(76)
            CC(106)=AP*CC(78)+CC(77)
            CC(107)=AP*CC(79)+CC(78)
            CC(108)=AP*CC(80)+CC(79)
            CC(109)=AP*CC(81)+CC(80)
            CC(110)=AP+CC(81)
            CC(111)=ONE
            IF(LBMAX.NE.3)then
               CC(112)=AP*CC(83)
               CC(113)=AP*CC(84)+CC(83)
               CC(114)=AP*CC(85)+CC(84)
               CC(115)=AP*CC(86)+CC(85)
               CC(116)=AP*CC(87)+CC(86)
               CC(117)=AP*CC(88)+CC(87)
               CC(118)=AP*CC(89)+CC(88)
               CC(119)=AP+CC(89)
               CC(120)=ONE
            end if
         end if
      end if
!
  700 IF(LAMAX.EQ.6)GO TO 900
      CC(121)=AP*CC(91)
      CC(122)=AP*CC(92)+CC(91)
      CC(123)=AP*CC(93)+CC(92)
      CC(124)=AP*CC(94)+CC(93)
      CC(125)=AP*CC(95)+CC(94)
      CC(126)=AP+CC(95)
      CC(127)=ONE
      IF(LBMAX.NE.1)then
         CC(128)=AP*CC(97)
         CC(129)=AP*CC(98)+CC(97)
         CC(130)=AP*CC(99)+CC(98)
         CC(131)=AP*CC(100)+CC(99)
         CC(132)=AP*CC(101)+CC(100)
         CC(133)=AP*CC(102)+CC(101)
         CC(134)=AP+CC(102)
         CC(135)=ONE
         IF(LBMAX.NE.2)then
            CC(136)=AP*CC(104)
            CC(137)=AP*CC(105)+CC(104)
            CC(138)=AP*CC(106)+CC(105)
            CC(139)=AP*CC(107)+CC(106)
            CC(140)=AP*CC(108)+CC(107)
            CC(141)=AP*CC(109)+CC(108)
            CC(142)=AP*CC(110)+CC(109)
            CC(143)=AP+CC(110)
            CC(144)=ONE
            IF(LBMAX.NE.3)then
               CC(145)=AP*CC(112)
               CC(146)=AP*CC(113)+CC(112)
               CC(147)=AP*CC(114)+CC(113)
               CC(148)=AP*CC(115)+CC(114)
               CC(149)=AP*CC(116)+CC(115)
               CC(150)=AP*CC(117)+CC(116)
               CC(151)=AP*CC(118)+CC(117)
               CC(152)=AP*CC(119)+CC(118)
               CC(153)=AP+CC(119)
               CC(154)=ONE
            end if
         end if
      end if
!
  800 IF(LAMAX.NE.7)then
         CC(155)=AP*CC(121)
         CC(156)=AP*CC(122)+CC(121)
         CC(157)=AP*CC(123)+CC(122)
         CC(158)=AP*CC(124)+CC(123)
         CC(159)=AP*CC(125)+CC(124)
         CC(160)=AP*CC(126)+CC(125)
         CC(161)=AP+CC(126)
         CC(162)=ONE
         IF(LBMAX.NE.1)then
            CC(163)=AP*CC(128)
            CC(164)=AP*CC(129)+CC(128)
            CC(165)=AP*CC(130)+CC(129)
            CC(166)=AP*CC(131)+CC(130)
            CC(167)=AP*CC(132)+CC(131)
            CC(168)=AP*CC(133)+CC(132)
            CC(169)=AP*CC(134)+CC(133)
            CC(170)=AP+CC(134)
            CC(171)=ONE
            IF(LBMAX.NE.2)then
               CC(172)=AP*CC(136)
               CC(173)=AP*CC(137)+CC(136)
               CC(174)=AP*CC(138)+CC(137)
               CC(175)=AP*CC(139)+CC(138)
               CC(176)=AP*CC(140)+CC(139)
               CC(177)=AP*CC(141)+CC(140)
               CC(178)=AP*CC(142)+CC(141)
               CC(179)=AP*CC(143)+CC(142)
               CC(180)=AP+CC(143)
               CC(181)=ONE
               IF(LBMAX.NE.3)then
                  CC(182)=AP*CC(145)
                  CC(183)=AP*CC(146)+CC(145)
                  CC(184)=AP*CC(147)+CC(146)
                  CC(185)=AP*CC(148)+CC(147)
                  CC(186)=AP*CC(149)+CC(148)
                  CC(187)=AP*CC(150)+CC(149)
                  CC(188)=AP*CC(151)+CC(150)
                  CC(189)=AP*CC(152)+CC(151)
                  CC(190)=AP*CC(153)+CC(152)
                  CC(191)=AP+CC(153)
                  CC(192)=ONE
               end if
            end if
         end if
      end if
!
  900 continue
!
! End of routine GETCC1
      return
      END
      subroutine GETA1 (A, CONST, INCP, LPMAX)
!***********************************************************************
!     Date last modified: June 19, 1992                    Version 1.0 *
!     Author:                                                          *
!     Description:                                                     *
!     This routine calculates the coefficients which transform the     *
!     functions G(IV) into the two center integrals.  This             *
!     transformation is carried out in routine GET2C, which should be  *
!     consulted for more details of the use of these coefficients.     *
!                                                                      *
!     The matrix is filled using A(1)=1.0 and the recursion formula:   *
!          A(LV,LP+1) = (A(LV,LP-1)*(LP-1) + A(LV-1,LP))/(2*P)         *
!***********************************************************************
! Modules:
      USE program_constants

      implicit none
!
! Input scalars:
      integer INCP,LPMAX
      double precision CONST
!
! Input array:
      double precision A(45)
!
! Local scalars:
      integer LPNEW
      double precision FIVE,SEVEN,SIX
!
      PARAMETER (FIVE=5.0D0,SIX=6.0D0,SEVEN=7.0D0)
!
! Begin:
      LPNEW=LPMAX+INCP
      IF(LPNEW.GT.1)then
         A(3)=CONST
         IF(LPNEW.NE.2)then
            A(4)=A(3)
            A(6)=A(3)*A(3)
            IF(LPNEW.NE.3)then
               A(8)=A(3)*(TWO*A(3)+A(4))
               A(10)=A(3)*A(6)
               IF(LPNEW.NE.4)then
                  A(11)=THREE*A(3)*A(4)
                  A(13)=A(3)*(THREE*A(6)+A(8))
                  A(15)=A(3)*A(10)
                  IF(LPNEW.NE.5)then
                     A(17)=A(3)*(FOUR*A(8)+A(11))
                     A(19)=A(3)*(FOUR*A(10)+A(13))
                     A(21)=A(3)*A(15)
                     IF(LPNEW.NE.6)then
                        A(22)=FIVE*A(3)*A(11)
                        A(24)=A(3)*(FIVE*A(13)+A(17))
                        A(26)=A(3)*(FIVE*A(15)+A(19))
                        A(28)=A(3)*A(21)
                        IF(LPNEW.NE.7)then
                           A(30)=A(3)*(SIX*A(17)+A(22))
                           A(32)=A(3)*(SIX*A(19)+A(24))
                           A(34)=A(3)*(SIX*A(21)+A(26))
                           A(36)=A(3)*A(28)
                           IF(LPNEW.NE.8)then
                              A(37)=SEVEN*A(3)*A(22)
                              A(39)=A(3)*(SEVEN*A(24)+A(30))
                              A(41)=A(3)*(SEVEN*A(26)+A(32))
                              A(43)=A(3)*(SEVEN*A(28)+A(34))
                              A(45)=A(3)*A(36)
                           end if
                        end if
                     end if
                  end if
               end if
            end if
         end if
      end if
!
  100 continue
!
! End of routine GETA1
      return
      END
      subroutine GET2C (TWOC, X, CONST, A, TWOPT2, INCP, LPMAX)
!***********************************************************************
!     Date last modified: June 19, 1992                    Version 1.0 *
!     Author:                                                          *
!     Description:                                                     *
!     This routine forms the two center integrals in TWOC.  The        *
!     functions G are formed from X and CONST using G(1)=CONST and     *
!     the recursion formula:                                           *
!          G(IV) = TWOPT2*(X*G(IV-1) - (IV-2)*G(IV-2))                 *
!     The two center integrals are then formed using the coefficients  *
!     in A, which were formed in routine GETA1.                        *
!                                                                      *
!***********************************************************************
! Modules:
      USE program_constants

      implicit none
!
! Input scalars:
      integer INCP,LPMAX
      double precision CONST,TWOPT2,X
! Input arrays:
      double precision A(45),TWOC(9)
!
! Local scalars:
      integer LPNEW
      double precision FIVE,SEVEN,SIX
!
! Local array:
      double precision G(9)
!
      PARAMETER (FIVE=5.0D0,SIX=6.0D0,SEVEN=7.0D0)
!
! Begin:
! Calculate the G functions.
!
      LPNEW=LPMAX+INCP
      G(1)=CONST
      IF(LPNEW.LE.1)GO TO 200
      G(2)=TWOPT2*X*G(1)
      IF(LPNEW.EQ.2)GO TO 190
      G(3)=TWOPT2*(X*G(2)-G(1))
      IF(LPNEW.EQ.3)GO TO 180
      G(4)=TWOPT2*(X*G(3)-TWO*G(2))
      IF(LPNEW.EQ.4)GO TO 170
      G(5)=TWOPT2*(X*G(4)-THREE*G(3))
      IF(LPNEW.EQ.5)GO TO 160
      G(6)=TWOPT2*(X*G(5)-FOUR*G(4))
      IF(LPNEW.EQ.6)GO TO 150
      G(7)=TWOPT2*(X*G(6)-FIVE*G(5))
      IF(LPNEW.EQ.7)GO TO 140
      G(8)=TWOPT2*(X*G(7)-SIX*G(6))
      IF(LPNEW.EQ.8)GO TO 130
      G(9)=TWOPT2*(X*G(8)-SEVEN*G(7))
!
! Now, evaluate the two center integrals using G and A.
!
      TWOC(9)=G(1)*A(37)+G(3)*A(39)+G(5)*A(41)+G(7)*A(43)+G(9)*A(45)
  130 TWOC(8)=G(2)*A(30)+G(4)*A(32)+G(6)*A(34)+G(8)*A(36)
  140 TWOC(7)=G(1)*A(22)+G(3)*A(24)+G(5)*A(26)+G(7)*A(28)
  150 TWOC(6)=G(2)*A(17)+G(4)*A(19)+G(6)*A(21)
  160 TWOC(5)=G(1)*A(11)+G(3)*A(13)+G(5)*A(15)
  170 TWOC(4)=G(2)*A(8)+G(4)*A(10)
  180 TWOC(3)=G(1)*A(4)+G(3)*A(6)
  190 TWOC(2)=G(2)*A(3)
  200 TWOC(1)=G(1)
!
! End of routine GET2C
      return
      END
      subroutine GET3C (V, IV, F, C, LAMAX, LBMAX)
!***********************************************************************
!     Date last modified: June 19, 1992                    Version 1.0 *
!     Author:                                                          *
!     Description:                                                     *
!     This routine transforms the integrals over functions at centre   *
!     P, which are now stored in F, into the integrals over functions  *
!     centered at A and B.  These are to be stored into V.             *
!     The coefficients, C, were formed in routine GETCC1 and the       *
!     integrals over functions at center P were formed in routine      *
!     GET2C.                                                           *
!                                                                      *
!***********************************************************************
      implicit none
!
! Input scalars:
      integer IV,LAMAX,LBMAX
! Input arrays:
      double precision C(192),F(9),V(80)
!
! Begin:
      GO TO (400,300,200,100),LAMAX
!
  100 GO TO (110,120,130,140),LBMAX
  140 V(IV+16)=F(1)*C(58)+F(2)*C(59)+F(3)*C(60)+F(4)*C(61)+F(5)*C(62)+F(6)*C(63)+F(7)
  130 V(IV+15)=F(1)*C(52)+F(2)*C(53)+F(3)*C(54)+F(4)*C(55)+F(5)*C(56)+F(6)
  120 V(IV+14)=F(1)*C(47)+F(2)*C(48)+F(3)*C(49)+F(4)*C(50)+F(5)
  110 V(IV+13)=F(1)*C(43)+F(2)*C(44)+F(3)*C(45)+F(4)
!
  200 GO TO (210,220,230,240),LBMAX
  240 V(IV+12)=F(1)*C(37)+F(2)*C(38)+F(3)*C(39)+F(4)*C(40)+F(5)*C(41)+F(6)
  230 V(IV+11)=F(1)*C(32)+F(2)*C(33)+F(3)*C(34)+F(4)*C(35)+F(5)
  220 V(IV+10)=F(1)*C(28)+F(2)*C(29)+F(3)*C(30)+F(4)
  210 V(IV+9)=F(1)*C(25)+F(2)*C(26)+F(3)
!
  300 GO TO (310,320,330,340),LBMAX
  340 V(IV+8)=F(1)*C(20)+F(2)*C(21)+F(3)*C(22)+F(4)*C(23)+F(5)
  330 V(IV+7)=F(1)*C(16)+F(2)*C(17)+F(3)*C(18)+F(4)
  320 V(IV+6)=F(1)*C(13)+F(2)*C(14)+F(3)
  310 V(IV+5)=F(1)*C(11)+F(2)
!
  400 GO TO (410,420,430,440),LBMAX
  440 V(IV+4)=F(1)*C(7)+F(2)*C(8)+F(3)*C(9)+F(4)
  430 V(IV+3)=F(1)*C(4)+F(2)*C(5)+F(3)
  420 V(IV+2)=F(1)*C(2)+F(2)
  410 V(IV+1)=F(1)
!
! End of routine GET3C
      return
      END
      subroutine GET1CS (F, CONST, COEF, LPNEW)
!***********************************************************************
!     Date last modified: June 19, 1992                    Version 1.0 *
!     Author:                                                          *
!     Description:                                                     *
!     This routine forms the one-dimensional, one-center overlap       *
!     integrals.  These are formed into F, and they are later          *
!     transformed into two-center integrals in routine GET2CS.         *
!                                                                      *
!***********************************************************************
      implicit none
!
! Input scalars:
      integer LPNEW
      double precision COEF,CONST
! Input array:
      double precision F(6)
! Local scalars:
      double precision F3,F5,F7,F9
!
      PARAMETER (F3=3.0D0,F5=5.0D0,F7=7.0D0,F9=9.0D0)
!
! Begin:
      F(1)=CONST
      IF(LPNEW.NE.1)then
         F(2)=COEF*F(1)
         IF(LPNEW.NE.2)then
            F(3)=COEF*F3*F(2)
            IF(LPNEW.NE.3)then
               F(4)=COEF*F5*F(3)
               IF(LPNEW.NE.4)then
                  F(5)=COEF*F7*F(4)
                  IF(LPNEW.NE.5)then
                     F(6)=COEF*F9*F(5)
                  end if
               end if
            end if
         end if
      end if
!
  100 continue
!
! End of routine GET1CS
      return
      END
      subroutine GET2CS (SS, F, C, LANEW, LBNEW)
!***********************************************************************
!     Date last modified: June 19, 1992                    Version 1.0 *
!     Author:                                                          *
!     Description:                                                     *
!     This routine calculates the two-center, one-dimensional          *
!     overlap integrals and returns them in the array SS.  It is       *
!     called with F, which contains the one-center one-dimensional     *
!     overlaps (calculated in GET1CS), and with the coefficients C.    *
!     These coefficients were calculated in routine GETCC1.            *
!                                                                      *
!***********************************************************************
      implicit none
!
! Input scalars:
      integer LANEW,LBNEW
! Input arrays:
      double precision C(192),F(6),SS(32)
!
! Begin:
      GO TO (100,200,300,400,500,600,700,800),LANEW
!
  800 GO TO (810,820,830,840),LBNEW
  840 SS(32)=F(1)*C(182)+F(2)*C(184)+F(3)*C(186)+F(4)*C(188)+F(5)*C(190)+F(6)
  830 SS(31)=F(1)*C(172)+F(2)*C(174)+F(3)*C(176)+F(4)*C(178)+F(5)*C(180)
  820 SS(30)=F(1)*C(163)+F(2)*C(165)+F(3)*C(167)+F(4)*C(169)+F(5)
  810 SS(29)=F(1)*C(155)+F(2)*C(157)+F(3)*C(159)+F(4)*C(161)
!
  700 GO TO (710,720,730,740),LBNEW
  740 SS(28)=F(1)*C(145)+F(2)*C(147)+F(3)*C(149)+F(4)*C(151)+F(5)*C(153)
  730 SS(27)=F(1)*C(136)+F(2)*C(138)+F(3)*C(140)+F(4)*C(142)+F(5)
  720 SS(26)=F(1)*C(128)+F(2)*C(130)+F(3)*C(132)+F(4)*C(134)
  710 SS(25)=F(1)*C(121)+F(2)*C(123)+F(3)*C(125)+F(4)
!
  600 GO TO (610,620,630,640),LBNEW
  640 SS(24)=F(1)*C(112)+F(2)*C(114)+F(3)*C(116)+F(4)*C(118)+F(5)
  630 SS(23)=F(1)*C(104)+F(2)*C(106)+F(3)*C(108)+F(4)*C(110)
  620 SS(22)=F(1)*C(97)+F(2)*C(99)+F(3)*C(101)+F(4)
  610 SS(21)=F(1)*C(91)+F(2)*C(93)+F(3)*C(95)
!
  500 GO TO (510,520,530,540),LBNEW
  540 SS(20)=F(1)*C(83)+F(2)*C(85)+F(3)*C(87)+F(4)*C(89)
  530 SS(19)=F(1)*C(76)+F(2)*C(78)+F(3)*C(80)+F(4)
  520 SS(18)=F(1)*C(70)+F(2)*C(72)+F(3)*C(74)
  510 SS(17)=F(1)*C(65)+F(2)*C(67)+F(3)
!
  400 GO TO (410,420,430,440),LBNEW
  440 SS(16)=F(1)*C(58)+F(2)*C(60)+F(3)*C(62)+F(4)
  430 SS(15)=F(1)*C(52)+F(2)*C(54)+F(3)*C(56)
  420 SS(14)=F(1)*C(47)+F(2)*C(49)+F(3)
  410 SS(13)=F(1)*C(43)+F(2)*C(45)
!
  300 GO TO (310,320,330,340),LBNEW
  340 SS(12)=F(1)*C(37)+F(2)*C(39)+F(3)*C(41)
  330 SS(11)=F(1)*C(32)+F(2)*C(34)+F(3)
  320 SS(10)=F(1)*C(28)+F(2)*C(30)
  310 SS(9)=F(1)*C(25)+F(2)
!
  200 GO TO (210,220,230,240),LBNEW
  240 SS(8)=F(1)*C(20)+F(2)*C(22)+F(3)
  230 SS(7)=F(1)*C(16)+F(2)*C(18)
  220 SS(6)=F(1)*C(13)+F(2)
  210 SS(5)=F(1)*C(11)
!
  100 GO TO (110,120,130,140),LBNEW
  140 SS(4)=F(1)*C(7)+F(2)*C(9)
  130 SS(3)=F(1)*C(4)+F(2)
  120 SS(2)=F(1)*C(2)
  110 SS(1)=F(1)
!
! End of routine GET2CS
      return
      END
      subroutine FILLCC (LZMAX, CONTRC, CSET)
!***********************************************************************
!     Date last modified: June 17, 1993                    Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version.                                       *
!     Routine to obtain coefficients for the primitive Gaussians.      *
!     The absolute value of the largest contraction coefficient is     *
!     returned in CMAXI.                                               *
!                                                                      *
!***********************************************************************
      implicit none
!
! Input scalars:
      integer LZMAX
!
! Input arrays:
      double precision CONTRC
!
! Output array:
      double precision CSET(20)
!
! Local scalars:
      double precision FIVE,THREE
!
      PARAMETER (THREE=3.0D0,FIVE=5.0D0)
!
! Begin:
! S Shell.
      IF(LZMAX.EQ.1)then
      CSET(1)=CONTRC
      else if(LZMAX.EQ.2)then
! P Shell.
      CSET(1)=0.0D0
      CSET(2)=CONTRC
      CSET(3)=CONTRC
      CSET(4)=CONTRC
!
      else if(LZMAX.EQ.3)then
! D Shell - Watch out for normalization.
      CSET(5)=CONTRC
      CSET(6)=CONTRC
      CSET(7)=CONTRC
      CSET(8)=CONTRC*DSQRT(THREE)
      CSET(9)=CONTRC*DSQRT(THREE)
      CSET(10)=CONTRC*DSQRT(THREE)
!
      else if(LZMAX.EQ.4)then
! F Shell - Watch out for normalization.
      CSET(11)=CONTRC
      CSET(12)=CONTRC
      CSET(13)=CONTRC
      CSET(14)=CONTRC*DSQRT(FIVE)
      CSET(15)=CONTRC*DSQRT(FIVE)
      CSET(16)=CONTRC*DSQRT(FIVE)
      CSET(17)=CONTRC*DSQRT(FIVE)
      CSET(18)=CONTRC*DSQRT(FIVE)
      CSET(19)=CONTRC*DSQRT(FIVE)
      CSET(20)=CONTRC*DSQRT(THREE)
      end if
!
! End of routine FILLCC
      return
      END
      subroutine PRT_1EINT (I1EINT)
!******************************************************************************************************
!     Date last modified:  February 29, 2008                                                          *
!     Author: R. A. Poirier                                                                           *
!     Description: This routine is called when an error occurs.  It deletes any existing files, ...   *
!******************************************************************************************************
! Modules
      USE program_files
      USE matrix_print
      USE type_basis_set
      USE QM_objects
      USE INT_objects

      implicit none
!
! Local scalars:
      character(*) I1EINT
!
! Begin:
      write(UNIout,'(/2a)')I1EINT,' Integrals'
      select case(I1EINT)
      case ('AO')
        write(UNIout,'(a)')'Hcore (Kinetic + Potential) AO integrals'
        call PRT_matrix (Hcore, MATlen, Basis%Nbasis)
        write(UNIout,'(a)')'Potential AO integrals'
        call PRT_matrix (Vint, MATlen, Basis%Nbasis)
        write(UNIout,'(a)')'Kinetic AO integrals'
        call PRT_matrix (Tint, MATlen, Basis%Nbasis)
        write(UNIout,'(a)')'Overlap AO integrals'
        call PRT_matrix (OVRLAP, MATlen, Basis%Nbasis)
      case ('DIPOLE')
        write(UNIout,'(a)')'X-dipole AO integrals'
        call PRT_matrix (Dipolex_AO, MATlen, Basis%Nbasis)
        write(UNIout,'(a)')'Y-dipole AO integrals'
        call PRT_matrix (Dipoley_AO, MATlen, Basis%Nbasis)
        write(UNIout,'(a)')'Z-dipole AO integrals'
        call PRT_matrix (Dipolez_AO, MATlen, Basis%Nbasis)
      case ('VDIPOLE')
        write(UNIout,'(a)')'Dipole X-velocity AO integrals'
        call PRT_matrix (VDDx, MATlen, Basis%Nbasis)
        write(UNIout,'(a)')'Dipole Y-velocity AO integrals'
        call PRT_matrix (VDDy, MATlen, Basis%Nbasis)
        write(UNIout,'(a)')'Dipole Z-velocity AO integrals'
        call PRT_matrix (VDDz, MATlen, Basis%Nbasis)
      case ('MDIPOLE')
        write(UNIout,'(a)')'Magnetic X-dipole AO integrals'
        call PRT_matrix (xMDpol, MATlen, Basis%Nbasis)
        write(UNIout,'(a)')'Magnetic Y-dipole AO integrals'
        call PRT_matrix (yMDpol, MATlen, Basis%Nbasis)
        write(UNIout,'(a)')'Magnetic Z-dipole AO integrals'
        call PRT_matrix (zMDpol, MATlen, Basis%Nbasis)
      end select

! end of routine PRT_1EINT
      return
      end subroutine PRT_1EINT
      subroutine FILMAT (F, A, MATlen, Iend, Jend, &
                         Iatmshl, Jatmshl, Irange, Jrange, Iaos, JaoS)
!***********************************************************************
!     Date last modified: November 6, 1996                 Version 1.0 *
!     Author:                                                          *
!     Description:  QCPE gaussIAN 80.  U. of T. Version.               *
!     This routine fills the matrix A (in linear form), with the       *
!     integrals from F.                                                *
!     For upper triangular packing, Iaos should be greater than JaoS   *
!     or equal (but with IMJ set to 0 for shell duplicate elimination).*
!     This routine receives as input the AO numbers (Iaos, JaoS        *
!     already biased by the values Istart and Jstart respectively),    *
!     the shell duplicate test variable IMJ (0 if shells identical,    *
!     1 if not identical), and limiting information, all via /C300C/.  *
!     If necessary, PUREDF is called to transform to pure D or pure F. *
!     Note that Iend and Jend may be changed by PUREDF.                *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer, intent(IN) :: MATlen
      integer :: Iend,Jend
      integer, intent(IN) :: Iatmshl,Jatmshl
      integer, intent(IN) :: Irange,Jrange
      integer, intent(IN) :: Iaos,JaoS
!
! Input arrays:
      double precision A(MATlen),F(100)
!
! Local scalars:
      integer :: I,Iends,II,IMJ,INDFM,INDX1,INTC,J,Jends,JJ
!
! Begin:
      IMJ=Iatmshl-Jatmshl
      Iends=Iend
      Jends=Jend
!
! Commence main processing loop.
! Also, shell duplicate elimination is performed here.
! Obtain correct bias for J-loop.
      INDX1=0
! Commence loop.
      do I=1,Irange
      Jend=Jrange
      IF(IMJ.EQ.0)Jend=I
      INTC=INDX1
! Obtain raw index.
      II=I+Iaos
      do J=1,Jend
      INTC=INTC+1
      JJ=J+Jaos
! Obtain full matrix index.
      INDFM=II*(II-1)/2+JJ
! Plant the value.
      A(INDFM)=F(INTC)
      end do ! J
      INDX1=INDX1+Jrange
      end do ! I
!
! Restore Iend and Jend.
      Iend=Iends
      Jend=Jends
!
! End of routine FILMAT
      return
      END
      subroutine FILMATN (F, A, MATlen, LIeqJ, Irange, Jrange, Iaos, JaoS)
!***********************************************************************
!     Date last modified: November 6, 1996                 Version 1.0 *
!     Author:                                                          *
!     Description:  QCPE gaussIAN 80.  U. of T. Version.               *
!     This routine fills the matrix A (in linear form), with the       *
!     integrals from F.                                                *
!     For upper triangular packing, Iaos should be greater than JaoS   *
!     or equal (but with IMJ set to 0 for shell duplicate elimination).*
!     This routine receives as input the AO numbers (Iaos, JaoS        *
!     already biased by the values Istart and Jstart respectively),    *
!     the shell duplicate test variable IMJ (0 if shells identical,    *
!     1 if not identical), and limiting information, all via /C300C/.  *
!     If necessary, PUREDF is called to transform to pure D or pure F. *
!     Note that Iend and Jend may be changed by PUREDF.                *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer, intent(IN) :: MATlen
      integer, intent(IN) :: Irange,Jrange
      integer, intent(IN) :: Iaos,JaoS
      logical, intent(IN) :: LIeqJ
!
! Input arrays:
      double precision A(MATlen),F(100)
!
! Local scalars:
      integer :: I,II,INDFM,INDX1,INTC,J,Jends,JJ
!
! Begin:
!
! Obtain correct bias for J-loop.
      INDX1=0
! Commence loop.
      do I=1,Irange
      Jends=Jrange
      if(LIeqJ)Jends=I
      INTC=INDX1
! Obtain raw index.
      II=I+Iaos
      do J=1,Jends
      INTC=INTC+1
      JJ=J+Jaos
! Obtain full matrix index.
      INDFM=II*(II-1)/2+JJ
! Plant the value.
      A(INDFM)=F(INTC)
      end do ! J
      INDX1=INDX1+Jrange
      end do ! I
!
! End of routine FILMATN
      return
      end

