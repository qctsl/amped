      subroutine ISPCLC
!*************************************************************************
!     Date last modified: June 28, 2002                                  *
!     Author: R. A. Poirier                                              *
!     Description:  MUN Version.                                         *
!     Gaussian two-electron integral package for S and P functions only. *
!*************************************************************************
! Modules:
      USE program_files
      USE SP_integrals
      USE type_basis_set
      USE type_molecule

      implicit none
!
! Local scalars:
      integer :: IERROR,I2EOLN,Jtype,NESHLBGN
      integer :: GET_maxint
!
! Begin:
      call PRG_manager ('enter', 'ISPCLC', 'UTILITY')
!
      call GET_object ('INT', 'INTEGRALS', 'GAMMA_FUNCTION')

      maxint=GET_maxint ()
      I2EOLN=3*maxint
      allocate (GOUT(I2EOLN), STAT=IERROR)
      if(IERROR.ne.0)then
        write(UNIout,*)'ERROR: ISPCLC> allocate failure'
        stop 'ERROR: ISPCLC> allocate failure'
      end if
      allocate (Gout_ijkl(1:maxint), Gout_ikjl(1:maxint), Gout_iljk(1:maxint))
      allocate (SHRABSQ(Natoms, Natoms))
      call I2E_RABSQ (SHRABSQ)
!
! Loop over I,J,K,Lshell.
      NESHLBGN=Basis%NFDShells+1
      do Ishell=Neshlbgn,Basis%NSHELLS
      Ifrst=Basis%shell(Ishell)%frstSHL
!
      do Jshell=Neshlbgn,Ishell
      Jfrst=Basis%shell(Jshell)%frstSHL
!
      do Kshell=Neshlbgn,Jshell
      Kfrst= Basis%shell(Kshell)%frstSHL
!
      do Lshell=Neshlbgn,Kshell
      Lfrst= Basis%shell(Lshell)%frstSHL
!
! Only 5 types are possible: 0000,0001,0011,0111,1111, specified by Jtype.
! A 6th type (0101) will be created from the JK and/or JL switches for type 0011.
      Jtype=0
      IF(Basis%shell(Ishell)%Xtype.EQ.1)Jtype=Jtype+1
      IF(Basis%shell(Jshell)%Xtype.EQ.1)Jtype=Jtype+1
      IF(Basis%shell(Kshell)%Xtype.EQ.1)Jtype=Jtype+1
      IF(Basis%shell(Lshell)%Xtype.EQ.1)Jtype=Jtype+1
!
! Irange,Jrange,... : the number of basis functions in each ashl.
      select case (Jtype)
      case(0)
        call ISP_SSSS
      case(1)
        call ISP_SSSP
      case(2)
        call ISP_SSPP
      case(3)
        call ISP_SPPP
      case(4)
        call ISP_PPPP
      end select

! End of loop over atom-ashls.
       end do ! Lshell
       end do ! Kshell
       end do ! Jshell
       end do ! Ishell
! End of loop over element-ashls.
!
      deallocate (Gout, Gout_ijkl, Gout_ikjl, Gout_iljk)
      deallocate (SHRABSQ)
!
! End of routine ISPCLC
      call PRG_manager ('exit', 'ISPCLC', 'UTILITY')
      return
      end subroutine ISPCLC
