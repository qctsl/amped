      subroutine GAMGEN
!***********************************************************************
!     Date last modified: November 7, 2006                 Version 2.0 *
!     Computes and tabulates F0(t) to F5(t)                            *
!     in range t=0.0 to 15.15, in units of t=0.05.                     *
!     Used by the SP 2-electron integral and SP force routines.        *
!     The table is generated only once                                 *
!***********************************************************************
! Modules:
      USE program_constants
      USE gamma_fn
!
      implicit none
!
! Local scalars:
      integer :: I,J,J1,K,K1,index,MAXtvalp4
      double precision :: T,T1
!
! Local parameters:
      integer :: MAXtval
      parameter (MAXtval=302)
      double precision :: PT15,PT184,SIX,TOL,DT
      parameter (PT15=0.15D0,PT184=0.184D0)
      parameter (SIX=6.0D0)
      double precision :: T_zero,Tstep
      parameter (T_zero=0.0D0,Tstep=0.05D0)
!
! Local arrays:
      double precision, dimension(:,:), allocatable :: GAMFNw1,GAMFNw2,GAMFNw3
      double precision, dimension(9) :: FM
!
! Begin:
      MAXtvalp4=MAXtval+4
      if(.not.allocated(FMT_val))then ! Only needs to be done once
        allocate (FMT_val(MAXtval),FMT_del(MAXtval),FMT_int(MAXtval))
      else
        return
      end if
!
! Allocate work arrays
      allocate (GAMFNw1(MAXtvalp4,6), GAMFNw2(MAXtval,6), GAMFNw3(MAXtval,6))
!
      call INI_BLD_Fmt
!
! If T> T_zero call BLD_FMT(), and store the values in GAMFNw1
! If T<=T_zero FMZERO value is taken as the value of FM 
      do I=1,MAXtvalp4
        T=-PT15+Tstep*DBLE(I)
        DT=DABS(T)
        if(DT.le.T_zero)then
          FM(1:6)=FMZERO(1:6) 
        else
          call BLD_FMT (T, 6, FM)
        end if
        GAMFNw1(I,1:6)=FM(1:6)
      end do !I
!
! Calculates differences Fm(t)-Fm(t-h) 
      do K=1,6
        do I=1,MAXtval
          GAMFNw2(I,K)=GAMFNw1(I+3,K)-GAMFNw1(I+2,K)
        end do ! I
      end do   ! K
!
! Use Everett's formula to compute the interpolation table
      do K=1,6
        do I=1,MAXtval
          T=GAMFNw1(I+2,K)
          T1=GAMFNw1(I+3,K)+GAMFNw1(I+1,K)
          GAMFNw3(I,K)=(T1-TWO*T-PT184*(SIX*T-FOUR*T1+GAMFNw1(I,K)+GAMFNw1(I+4,K)))/SIX
        end do !I
      end do  !K
!
! Three work arrays are copied to Gamma_function
      FMT_val(1:MAXtval)%FM0=GAMFNw1(3:MAXtval+2,1)
      FMT_val(1:MAXtval)%FM1=GAMFNw1(3:MAXtval+2,2)
      FMT_val(1:MAXtval)%FM2=GAMFNw1(3:MAXtval+2,3)
      FMT_val(1:MAXtval)%FM3=GAMFNw1(3:MAXtval+2,4)
      FMT_val(1:MAXtval)%FM4=GAMFNw1(3:MAXtval+2,5)
      FMT_val(1:MAXtval)%FM5=GAMFNw1(3:MAXtval+2,6)
!
      FMT_del(1:MAXtval)%FM0=GAMFNw2(1:MAXtval,1)
      FMT_del(1:MAXtval)%FM1=GAMFNw2(1:MAXtval,2)
      FMT_del(1:MAXtval)%FM2=GAMFNw2(1:MAXtval,3)
      FMT_del(1:MAXtval)%FM3=GAMFNw2(1:MAXtval,4)
      FMT_del(1:MAXtval)%FM4=GAMFNw2(1:MAXtval,5)
      FMT_del(1:MAXtval)%FM5=GAMFNw2(1:MAXtval,6)
!
      FMT_int(1:MAXtval)%FM0=GAMFNw3(1:MAXtval,1)
      FMT_int(1:MAXtval)%FM1=GAMFNw3(1:MAXtval,2)
      FMT_int(1:MAXtval)%FM2=GAMFNw3(1:MAXtval,3)
      FMT_int(1:MAXtval)%FM3=GAMFNw3(1:MAXtval,4)
      FMT_int(1:MAXtval)%FM4=GAMFNw3(1:MAXtval,5)
      FMT_int(1:MAXtval)%FM5=GAMFNw3(1:MAXtval,6)

      deallocate (GAMFNw1,GAMFNw2,GAMFNw3)
!
! End of subroutine GAMGEN
      end subroutine GAMGEN
      subroutine BLD_FMT (T, M, FM)
!***********************************************************************
!     Date last modified: June 14, 2006                    Version 1.0 *
!     Author:Raymond Poirier, Kushan Saputantri                        *
!     Description: This subroutine calculates the fuction FM(T) for a  *
!     given T and M values, recursively calculates the value from 1:m. *
!***********************************************************************
! Modules:
      USE program_constants
      USE gamma_fn
!
      implicit none
!
! Input scalars:
      integer, intent(IN) :: M
      double precision, intent(IN) :: T
      double precision, intent(OUT) :: FM(M)
!
! Local scalars:
      integer :: I,IX,MM1,NOTRMS,IY
      double precision :: A,APPROX,B,DT,FIMULT,FIPROP,SUM,TERM,TEXP,TX
      double precision :: T_large,T_small,TOL
      parameter (T_small=10.0D0, T_large=42.0D0,TOL=1.0D-9)
!
! Begin:
      MM1=M-1
      TEXP=DEXP(-T)
!
      DT=DABS(T)
      if(DT.ge.T_large)then  ! DT>= 42.0
        TX=DBLE(M)-PT5
        FM(M)=PT5*GA(M)/(T**TX)
!
      else if(DT.lt.T_small)then ! 0.0 < DT < 10.0
        A=DBLE(MM1)+PT5
        TERM=ONE/A
        SUM=TERM
 loop1: do IX=2,200
          A=A+ONE
          TERM=TERM*T/A
          SUM=SUM+TERM
          if(DABS(TERM/SUM).lt.TOL) exit loop1  
        end do loop1
        FM(M)=PT5*SUM*TEXP 
!
      else           ! 10.0<=DT<42.0
        A=DBLE(MM1)
        B=A+PT5
        A=A-PT5
        TX=ONE/T
        APPROX=RPITWO*DSQRT(TX)*(TX**MM1)
        if(MM1.NE.0)then
          do IX=1,MM1
            B=B-ONE
            APPROX=APPROX*B
          end do !IX
        end if
        FIMULT=PT5*TEXP*TX
        SUM=ZERO
        if(FIMULT.ne.ZERO)then
          FIPROP=FIMULT/APPROX
          TERM=ONE
          SUM=ONE
          NOTRMS=IDINT(T)+MM1
 loop2:   do IX=2,NOTRMS !XI
            TERM=TERM*A*TX
            SUM=SUM+TERM
            if(DABS(TERM*FIPROP/SUM).le.TOL)exit loop2 
            A=A-ONE
          end do loop2
        end if
        FM(M)=APPROX-FIMULT*SUM
      end if
!
! Recursively compute F(M:1)
        if(MM1.NE.0)then
          TX=T+T
          SUM=DBLE(M+M-3)
          do IX=1,MM1
            FM(M-IX)=(TX*FM(M-IX+1)+TEXP)/SUM
            SUM=SUM-TWO
          end do !IX
       end if
!
! End of routine BLD_FMT
      return
      end subroutine BLD_FMT
      subroutine INI_BLD_Fmt
!*****************************************************************************************************************
!     Date last modified: November 6, 2006                                                          Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Initialize the arrays and scalars used by BLD_Fmt                                             *
!*****************************************************************************************************************
! Modules:
      USE program_constants
      USE gamma_fn

      implicit none

! Input scalars:
! Output array:
      double precision :: TOL
! Local scalars:
      integer :: I
!
! Set values in GA = sqrt(pi)*((k-1)/2)!! = sqrt(pi)*(1/2)(3/2)(5/2)...(k-1)/2
      TOL=PT5
      GA(1)=SQRT(PI_VAL)
      RPITWO=GA(1)*PT5
      do I=2,9
        GA(I)=GA(I-1)*TOL
        TOL=TOL+ONE
      end do ! I
!
! Computes the Fm(0) = 1/(2m+1)
      TOL=ONE
      FMZERO(1)=ONE
      do I=2,9
        TOL=TOL+TWO
        FMZERO(I)=ONE/TOL
      end do !I
!
      return
      end subroutine INI_BLD_Fmt

! Temporarily placed here for compiling
      subroutine BLD_FMT_matrix (FMtmat, Norder, tval)
!*****************************************************************************************************************
!     Date last modified: November 6, 2006                                                          Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: For a Rys polynomial of order Norder compute the matrix of Fm(t) (FMtmat)                     *
!*****************************************************************************************************************
      implicit none

! Input scalars:
      integer :: Norder
      double precision :: tval
! Output array:
      double precision :: FMtmat(Norder+1,Norder+1)
      double precision :: FMt(2*Norder+1)
! Local scalars:
      integer :: I,J,Nmax
!
! Obtain Fm(t), M=0,1,2,...,2*Nmax+1.
      Nmax=Norder+1
      call INI_BLD_Fmt
      call BLD_FMT (tval, 2*Norder+1, FMt)
!
! Copy values of Fm(t) to matrix.
      do  J=1,Nmax
      do  I=1,Nmax
        FMtmat(I,J)=FMt(I+J-1)
      end do ! I
      end do ! J
      return
      end subroutine BLD_FMT_matrix

      subroutine BLD_FMT_Cmatrix (FMtmat, Cmatrix, Norder)
!*****************************************************************************************************************
!     Date last modified: November 6, 2006                                                          Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: For a Rys polynomial of order Norder compute the Cmatrix of Fm(t) (FMtmat)                    *
!*****************************************************************************************************************
! Modules:
      USE program_constants

      implicit none

! Input scalars:
      integer :: Norder
      double precision :: tval
! Output array:
      double precision :: FMtmat(Norder+1,Norder+1)
      double precision :: FMt(2*Norder+1)
      double precision :: Cmatrix(Norder+2,Norder+2)
      double precision :: A(Norder+2)
! Local scalars:
      integer :: I,ICM1,Icol,J,Jcol,Nmax,Nmax1
      double precision :: B,T,T1
!
! Obtain Fm(t), M=0,1,2,...,2*Nmax+1.
      Nmax=Norder+1
      Nmax1=Nmax+1
!
      if(Nmax1.GT.100)STOP
      if(Nmax1.LE.1)return
! Copy values of Fm(t) to matrix.
      do  I=1,Nmax1
        do  J=1,Nmax1
          Cmatrix(I,J)=ZERO
        end do ! J
        Cmatrix(I,I)=ONE
      end do ! I

!
      do ICOL=1,Nmax1
      IF(ICOL.gt.1)then
! Collect Cmatrix DOT Fmtmat terms.
      do I=1,Nmax1
        A(I)=ZERO
        do J=1,Nmax1
          A(I)=A(I)+Cmatrix(J,ICOL)*Fmtmat(J,I)
        end do ! J
      end do ! I
! Loop over columns upt to the current one
      ICM1=ICOL-1
      do JCOL=1,ICM1
! Get B.
      B=ZERO
      do I=1,Nmax1
        B=B+Cmatrix(I,JCOL)*A(I)
      end do ! I
! Update vector.
      do I=1,Nmax1
        Cmatrix(I,ICOL)=Cmatrix(I,ICOL)-B*Cmatrix(I,JCOL)
      end do ! I
      end do ! Jcol
      end if ! Icol.gt.1
! Compute normalization
   50 T=ZERO
      do I=1,Nmax1
        T1=ZERO
        do J=1,Nmax1
          T1=T1+Cmatrix(J,ICOL)*Fmtmat(J,I)
        end do ! J
        T=T+Cmatrix(I,ICOL)*T1
      end do ! I
      T=ONE/dsqrt(T)
! Update column.
      do I=1,Nmax1
        Cmatrix(I,ICOL)=Cmatrix(I,ICOL)*T
      end do ! I
      end do ! Icol

      return
      end subroutine BLD_FMT_Cmatrix

