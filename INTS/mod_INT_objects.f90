      MODULE INT_objects
!*****************************************************************************************
!     Date last modified: August 7, 2014                                                 *
!     Author: R.A. Poirier                                                               *
!     Description: Objects related to integrals of basis functions                       *
!*****************************************************************************************
      implicit none
!
      double precision, dimension(:), allocatable :: HCORE
      double precision, dimension(:), allocatable :: Vint
      double precision, dimension(:), allocatable :: Tint
      double precision, dimension(:), allocatable :: OVRLAP
      double precision, dimension(:), allocatable :: V12dr2

! Dipole moment
      double precision, dimension(:), allocatable :: DipoleX_AO
      double precision, dimension(:), allocatable :: DipoleY_AO
      double precision, dimension(:), allocatable :: DipoleZ_AO

      double precision, dimension(:), allocatable :: SecMomXX_AO
      double precision, dimension(:), allocatable :: SecMomXY_AO
      double precision, dimension(:), allocatable :: SecMomXZ_AO
      double precision, dimension(:), allocatable :: SecMomYY_AO
      double precision, dimension(:), allocatable :: SecMomYZ_AO
      double precision, dimension(:), allocatable :: SecMomZZ_AO

! Dipole velocities
      double precision, dimension(:), allocatable :: VDDX
      double precision, dimension(:), allocatable :: VDDY
      double precision, dimension(:), allocatable :: VDDZ

! Magnetic Dipole
      double precision, dimension(:), allocatable :: XMDPOL
      double precision, dimension(:), allocatable :: YMDPOL
      double precision, dimension(:), allocatable :: ZMDPOL
!
! Two electron integrals - objects
      type ints_2e_iiii
        integer :: I
        double precision :: iiii
      end type
!
      type ints_2e_ijkl
        integer :: I,J,K,L
        double precision :: IJKL
        double precision :: IKJL
        double precision :: ILJK
      end type
!
      type ints_2e_ijkj
        integer :: I,J,K
        double precision :: ijkj
        double precision :: ikjj
      end type
!
      type ints_2e_ijjl
        integer :: I,J,L
        double precision :: ijjl
        double precision :: iljj
      end type
!
      type ints_2e_iikl
        integer :: I,K,L
        double precision :: iikl
        double precision :: ikil
      end type
!
      type ints_2e_iikk
        integer :: I,K
        double precision :: iikk
        double precision :: ikik
      end type
!
      type ints_2e_iiil
        integer :: I,L
        double precision :: iiil
      end type
!
      type ints_2e_ijjj
        integer :: I,J
        double precision :: ijjj
      end type
!
      type (ints_2e_iiii), dimension(:), allocatable :: IIII_INCORE
      type (ints_2e_ijjj), dimension(:), allocatable :: IJJJ_INCORE
      type (ints_2e_iiil), dimension(:), allocatable :: IIIL_INCORE
      type (ints_2e_iikk), dimension(:), allocatable :: IIKK_INCORE
      type (ints_2e_iikl), dimension(:), allocatable :: IIKL_INCORE
      type (ints_2e_ijjl), dimension(:), allocatable :: IJJL_INCORE
      type (ints_2e_ijkj), dimension(:), allocatable :: IJKJ_INCORE
      type (ints_2e_ijkl), dimension(:), allocatable :: IJKL_INCORE
!
      integer :: NTINT(8)                              ! Integral counts for iiii ... ijkl
      integer :: IIII_unit,IJJL_unit,IJKJ_unit,IIKL_unit,IJKL_unit           ! units
! Array lengths for INCORE and Buffer rrays
      integer :: IIII_MAX,IIIL_MAX,IJJJ_MAX,IIKK_MAX,IJJL_MAX,IJKJ_MAX,IIKL_MAX,IJKL_MAX
! Current number of ints stored in INCORE arrays
      integer :: IIII_count,IIIL_count,IJJJ_count,IIKK_count,IJJL_count,IJKJ_count,IIKL_count,IJKL_count
!
      logical :: LSAVE_IIKK,LSAVE_IJJJ,LSAVE_IIIL,LSAVE_IIII ! always INCORE, do not re-save for a direct SCF
      logical :: LIJKL_ONDISK,LIIKL_ONDISK,LIJKJ_ONDISK,LIJJL_ONDISK ! If false, INCORE arrays are full, use buff arrays
      logical :: ANY_ONDISK
      logical :: Lall_INCORE
      logical :: Lall_ONDISK
      logical :: LI2E_exist ! True if files are open

      type (ints_2e_ijkl), dimension(:), allocatable :: IJKL_BUFFER
      type (ints_2e_ijkj), dimension(:), allocatable :: IJKJ_BUFFER
      type (ints_2e_ijjl), dimension(:), allocatable :: IJJL_BUFFER
      type (ints_2e_iikl), dimension(:), allocatable :: IIKL_BUFFER
!
      integer :: IJJL_Nbuffers,IJKJ_Nbuffers,IIKL_Nbuffers,IJKL_Nbuffers    ! Buffer counts.
!
! Integrals over AOs
      double precision :: omegaRS
      double precision, allocatable, dimension(:) :: eriAO, erilrAO
      double precision, allocatable, dimension(:) :: emdAO, emAO
      double precision, allocatable, dimension(:) :: amdAO, amAO
      double precision, allocatable, dimension(:) :: mbdAO, mbAO
      double precision, allocatable, dimension(:) :: ERpotAO, ERpot
      double precision, allocatable, dimension(:) :: ramAO, ramdAO
      double precision, allocatable, dimension(:) :: extAO
!
      end MODULE INT_objects
