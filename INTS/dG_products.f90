      subroutine dG_products (rx, ry, rz, dAOprodX, dAOprodY, dAOprodZ, MATlen)
!****************************************************************************************
!     Date last modified: December 3, 2015                                              *
!     Authors: Ahmad and R.A. Poirier                                                   *
!     Description: Compute d/dx, d/dy and d/dz of the product of two Gaussians          *
!                                                                                       *
!****************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE type_basis_set ! contains AOprod and dAOprodX,Y,Z
      USE matrix_print
      USE QM_defaults

      implicit none
!
! Input scalars:
      integer :: MATlen
      double precision :: rx,ry,rz
      double precision :: dAOprodX(MATlen),dAOprodY(MATlen),dAOprodZ(MATlen)
!
! Local scalars:
      integer :: Ishell,Jshell,Ifrst,Jfrst,Ilast,Jlast
      integer :: Istart,Jstart,Iend,Jend
      integer :: Iatmshl,Jatmshl
      integer :: LAMAX,LBMAX
      integer :: Iatom,Jatom
      integer :: Irange,Jrange
      integer :: Igauss,Jgauss
      integer :: IGBGN,JGBGN,IGend,JGend
      integer :: Iao,Jao,AOI,AOJ
      integer :: I,J,K,INDEX,DEFCASE,IJao
      double precision :: COEF_IJ,COEF_SXY,COEF_PXY
      double precision :: COEF_XXXY,COEF_XYXY,SQRT3,SUMDSQ,Gama,Alpha,Beta,RABSQ,EG,EG_K,EG_exp
      double precision :: Ax,Bx,Ay,By,Az,Bz,Px,Py,Pz,xA,yA,zA,xB,yB,zB,rpx,rpy,rpz
      double precision :: dEGx,dEGy,dEGz
      logical :: LIatmshl
!
! Local arrays  
      double precision :: dAOX(100),dAOY(100),dAOZ(100)     ! save derivatives

! Begin:
      SQRT3=DSQRT(THREE)
!
      MATlen=size(dAOprodX)
      dAOprodX(1:MATlen)=ZERO
      dAOprodY(1:MATlen)=ZERO
      dAOprodZ(1:MATlen)=ZERO
!
! Begin loop over shells.
!
! Loop over elemental SHELLs
! Loop over Ishell.
      do Ishell=1,Basis%Nshells
        LAMAX=Basis%shell(Ishell)%Xtype+1
        Istart=Basis%shell(Ishell)%Xstart
        Iend=Basis%shell(Ishell)%XEND
        Irange=Iend-Istart+1
        IGBGN=Basis%shell(Ishell)%EXPBGN
        IGEND=Basis%shell(Ishell)%EXPEND
        Ifrst=Basis%shell(Ishell)%frstSHL
!
! Loop over Jshell.
      do Jshell=1,Ishell
        LBMAX=Basis%shell(Jshell)%Xtype+1
        Jstart=Basis%shell(Jshell)%Xstart
        Jend=Basis%shell(Jshell)%XEND
        Jrange=Jend-Jstart+1
        JGBGN=Basis%shell(Jshell)%EXPBGN
        JGEND=Basis%shell(Jshell)%EXPEND
        Jfrst=Basis%shell(Jshell)%frstSHL
!
! Define CASE:
      if(LAMAX.lt.LBMAX)then
       DEFCASE=4*(LAMAX-1)+LBMAX
      else
       DEFCASE=4*(LBMAX-1)+LAMAX
      end if
!
      Ilast= Basis%shell(Ishell)%lastSHL
      Jlast= Basis%shell(Jshell)%lastSHL
!
      call AB_dGproducts
!
! End of loop over shells.
      
      end do ! Jshell
      end do ! Ishell
!
! End of routine dG_PRODUCTS
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine AB_dGproducts
!****************************************************************************************
!     Date last modified: December 3, 2015                                              *
!     Authors: Ahmad and R.A. Poirier                                                   *
!     Description:                                                                      *
!                                                                                       *
!****************************************************************************************
! Modules:

      implicit none
!
! Loop over all atomic shells
      do Iatmshl=Ifrst,Ilast
        Iatom=Basis%atmshl(Iatmshl)%ATMLST
!
        Ax=CARTESIAN(Iatom)%X     ! vector A={Ax,Ay,Az}     
        Ay=CARTESIAN(Iatom)%Y
        Az=CARTESIAN(Iatom)%Z
!
        AOI=Basis%atmshl(Iatmshl)%frstAO-1
        IF(Ishell.EQ.Jshell)Jlast=Iatmshl
      do Jatmshl=Jfrst,Jlast
        Jatom=Basis%atmshl(Jatmshl)%ATMLST
!
        Bx=CARTESIAN(Jatom)%X     ! vector B={Bx,By,Bz}
        By=CARTESIAN(Jatom)%Y
        Bz=CARTESIAN(Jatom)%Z
!
        AOJ=Basis%atmshl(Jatmshl)%frstAO-1
        LIatmshl=.false.
        if(Iatmshl.eq.Jatmshl)LIatmshl=.true.

        RABSQ=(Ax-Bx)*(Ax-Bx)+(Ay-By)*(Ay-By)+(Az-Bz)*(Az-Bz)     ! (A-B)**2

        dAOX(1:100)=ZERO     ! will handle up to <f|f>
        dAOY(1:100)=ZERO
        dAOZ(1:100)=ZERO
! Loop over primitive gaussians
!
      do Igauss=IGBGN,IGEND
        Alpha=Basis%gaussian(Igauss)%exp

      do Jgauss=JGBGN,JGEND
        Beta=Basis%gaussian(Jgauss)%exp
!
        Gama=Alpha+Beta

        Px=(Alpha*Ax+Beta*Bx)/Gama     ! vector P
        Py=(Alpha*Ay+Beta*By)/Gama
        Pz=(Alpha*Az+Beta*Bz)/Gama
!
        rpx = Px-rx     ! vector rp=P-r
        rpy = Py-ry
        rpz = Pz-rz

        SUMDSQ = rpx*rpx + rpy*rpy + rpz*rpz   ! (rp)**2

        EG_K=DEXP(-Alpha*Beta*RABSQ/Gama)      ! K 
        EG_exp=DEXP(-Gama*SUMDSQ)              ! exp
        EG=EG_K*EG_exp                         ! K*exp
!
        xA=rx-Ax     ! vector rA=(r-A)
        yA=ry-Ay
        zA=rz-Az
        xB=rx-Bx     ! vector rB=(r-B)
        yB=ry-By
        zB=rz-Bz
!
      dEGx=TWO*Gama*rpx*EG     ! derivative of EG with respect to x
      dEGy=TWO*Gama*rpy*EG     ! derivative of EG with respect to y
      dEGz=TWO*Gama*rpz*EG     ! derivative of EG with respect to z 

      COEF_IJ = Basis%gaussian(Igauss)%CONTRC*Basis%gaussian(Jgauss)%CONTRC
      select case (DEFCASE)
! S|S
     case (1)
        dAOX(1)=dAOX(1)+COEF_IJ*dEGx     ! COEF_IJ*EG
        dAOY(1)=dAOY(1)+COEF_IJ*dEGy
        dAOZ(1)=dAOZ(1)+COEF_IJ*dEGz
! S|P
      case (2)
        dAOX(1)=dAOX(1)+COEF_IJ*(xB*dEGx+EG)     ! xB*COEF_SP*EG
        dAOY(1)=dAOY(1)+COEF_IJ*xB*dEGy
        dAOZ(1)=dAOZ(1)+COEF_IJ*xB*dEGz

        dAOX(2)=dAOX(2)+COEF_IJ*yB*dEGx          ! yB*COEF_SP*EG   
        dAOY(2)=dAOY(2)+COEF_IJ*(yB*dEGy+EG)
        dAOZ(2)=dAOZ(2)+COEF_IJ*yB*dEGz

        dAOX(3)=dAOX(3)+COEF_IJ*zB*dEGx          ! zB*COEF_SP*EG
        dAOY(3)=dAOY(3)+COEF_IJ*zB*dEGy
        dAOZ(3)=dAOZ(3)+COEF_IJ*(zB*dEGz+EG)
!
! S|D
      case (3)
        COEF_SXY = SQRT3*COEF_IJ
        dAOX(1)=dAOX(1)+COEF_IJ*(xB*xB*dEGx+TWO*xB*EG)      ! xB*xB*COEF_IJ*EG
        dAOY(1)=dAOY(1)+COEF_IJ*xB*xB*dEGy
        dAOZ(1)=dAOZ(1)+COEF_IJ*xB*xB*dEGz

        dAOX(2)=dAOX(2)+COEF_IJ*yB*yB*dEGx                ! yB*yB*COEF_IJ*EG 
        dAOY(2)=dAOY(2)+COEF_IJ*(yB*yB*dEGy+TWO*yB*EG)
        dAOZ(2)=dAOZ(2)+COEF_IJ*yB*yB*dEGz

        dAOX(3)=dAOX(3)+COEF_IJ*zB*zB*dEGx                ! zB*zB*COEF_IJ*EG    
        dAOY(3)=dAOY(3)+COEF_IJ*zB*zB*dEGy
        dAOZ(3)=dAOZ(3)+COEF_IJ*(zB*zB*dEGz+TWO*zB*EG)

        dAOX(4)=dAOX(4)+COEF_SXY*(xB*yB*dEGx+yB*EG)        ! xB*yB*COEF_SXY*EG 
        dAOY(4)=dAOY(4)+COEF_SXY*(xB*yB*dEGy+xB*EG)
        dAOZ(4)=dAOZ(4)+COEF_SXY*xB*yB*dEGz

        dAOX(5)=dAOX(5)+COEF_SXY*(xB*zB*dEGx+zB*EG)        ! xB*zB*COEF_SXY*EG
        dAOY(5)=dAOY(5)+COEF_SXY*xB*zB*dEGy
        dAOZ(5)=dAOZ(5)+COEF_SXY*(xB*zB*dEGz+xB*EG)

        dAOX(6)=dAOX(6)+COEF_SXY*yB*zB*dEGx                ! yB*zB*COEF_SXY*EG
        dAOY(6)=dAOY(6)+COEF_SXY*(yB*zB*dEGy+zB*EG)
        dAOZ(6)=dAOZ(6)+COEF_SXY*(yB*zB*dEGz+yB*EG)

! P|P
      case (6)
        dAOX(1)=dAOX(1)+COEF_IJ*(xA*xB*dEGx+(xA+xB)*EG)    ! xA*xB*COEF_PP*EG
        dAOY(1)=dAOY(1)+COEF_IJ*xA*xB*dEGy
        dAOZ(1)=dAOZ(1)+COEF_IJ*xA*xB*dEGz

        dAOX(2)=dAOX(2)+COEF_IJ*(xA*yB*dEGx+yB*EG)         ! xA*yB*COEF_PP*EG
        dAOY(2)=dAOY(2)+COEF_IJ*(xA*yB*dEGy+xA*EG)
        dAOZ(2)=dAOZ(2)+COEF_IJ*xA*yB*dEGz

        dAOX(3)=dAOX(3)+COEF_IJ*(xA*zB*dEGx+zB*EG)         ! xA*zB*COEF_PP*EG
        dAOY(3)=dAOY(3)+COEF_IJ*xA*zB*dEGy
        dAOZ(3)=dAOZ(3)+COEF_IJ*(xA*zB*dEGz+xA*EG)

        dAOX(4)=dAOX(4)+COEF_IJ*(yA*xB*dEGx+yA*EG)         ! yA*xB*COEF_PP*EG
        dAOY(4)=dAOY(4)+COEF_IJ*(yA*xB*dEGy+xB*EG)
        dAOZ(4)=dAOZ(4)+COEF_IJ*yA*xB*dEGz

        dAOX(5)=dAOX(5)+COEF_IJ*yA*yB*dEGx                 ! yA*yB*COEF_PP*EG
        dAOY(5)=dAOY(5)+COEF_IJ*(yA*yB*dEGy+(yA+yB)*EG)
        dAOZ(5)=dAOZ(5)+COEF_IJ*yA*yB*dEGz

        dAOX(6)=dAOX(6)+COEF_IJ*yA*zB*dEGx                 ! yA*zB*COEF_PP*EG
        dAOY(6)=dAOY(6)+COEF_IJ*(yA*zB*dEGy+zB*EG)
        dAOZ(6)=dAOZ(6)+COEF_IJ*(yA*zB*dEGz+yA*EG)

        dAOX(7)=dAOX(7)+COEF_IJ*(zA*xB*dEGx+zA*EG)         ! zA*xB*COEF_PP*EG
        dAOY(7)=dAOY(7)+COEF_IJ*zA*xB*dEGy
        dAOZ(7)=dAOZ(7)+COEF_IJ*(zA*xB*dEGz+xB*EG)

        dAOX(8)=dAOX(8)+COEF_IJ*zA*yB*dEGx                 ! zA*yB*COEF_PP*EG
        dAOY(8)=dAOY(8)+COEF_IJ*(zA*yB*dEGy+zA*EG)
        dAOZ(8)=dAOZ(8)+COEF_IJ*(zA*yB*dEGz+yB*EG)

        dAOX(9)=dAOX(9)+COEF_IJ*zA*zB*dEGx                 ! zA*zB*COEF_PP*EG
        dAOY(9)=dAOY(9)+COEF_IJ*zA*zB*dEGy
        dAOZ(9)=dAOZ(9)+COEF_IJ*(zA*zB*dEGz+(zA+zB)*EG)

! P|D
      case (7)
        COEF_PXY = SQRT3*COEF_IJ
        dAOX(1)=dAOX(1)+COEF_IJ*(xA*xB*xB*dEGx+(xA*(xB+xB)+xB*xB)*EG)     ! xA*xB*xB*COEF_IJ*EG
        dAOY(1)=dAOY(1)+COEF_IJ*xA*xB*xB*dEGy
        dAOZ(1)=dAOZ(1)+COEF_IJ*xA*xB*xB*dEGz

        dAOX(2)=dAOX(2)+COEF_IJ*(xA*yB*yB*dEGx+yB*yB*EG)                  ! xA*yB*yB*COEF_IJ*EG
        dAOY(2)=dAOY(2)+COEF_IJ*(xA*yB*yB*dEGy+xA*(yB+yB)*EG)
        dAOZ(2)=dAOZ(2)+COEF_IJ*xA*yB*yB*dEGz

        dAOX(3)=dAOX(3)+COEF_IJ*(xA*zB*zB*dEGx+zB*zB*EG)                  ! xA*zB*zB*COEF_IJ*EG
        dAOY(3)=dAOY(3)+COEF_IJ*xA*zB*zB*dEGy
        dAOZ(3)=dAOZ(3)+COEF_IJ*(xA*zB*zB*dEGz+xA*(zB+zB)*EG)

        dAOX(4)=dAOX(4)+COEF_PXY*(xA*xB*yB*dEGx+yB*(xA+xB)*EG)             ! xA*xB*yB*COEF_PXY*EG
        dAOY(4)=dAOY(4)+COEF_PXY*(xA*xB*yB*dEGy+xA*xB*EG)
        dAOZ(4)=dAOZ(4)+COEF_PXY*xA*xB*yB*dEGz

        dAOX(5)=dAOX(5)+COEF_PXY*(xA*xB*zB*dEGx+(xA+xB)*zB*EG)             ! xA*xB*zB*COEF_PXY*EG
        dAOY(5)=dAOY(5)+COEF_PXY*xA*xB*zB*dEGy
        dAOZ(5)=dAOZ(5)+COEF_PXY*(xA*xB*zB*dEGz+xA*xB*EG)

        dAOX(6)=dAOX(6)+COEF_PXY*(xA*yB*zB*dEGx+yB*zB*EG)                  ! xA*yB*zB*COEF_PXY*EG
        dAOY(6)=dAOY(6)+COEF_PXY*(xA*yB*zB*dEGy+xA*zB*EG)
        dAOZ(6)=dAOZ(6)+COEF_PXY*(xA*yB*zB*dEGz+xA*yB*EG)

        dAOX(7)=dAOX(7)+COEF_IJ*(yA*xB*xB*dEGx+yA*(xB+xB)*EG)             ! yA*xB*xB*COEF_IJ*EG
        dAOY(7)=dAOY(7)+COEF_IJ*(yA*xB*xB*dEGy+xB*xB*EG)
        dAOZ(7)=dAOZ(7)+COEF_IJ*yA*xB*xB*dEGz
        
        dAOX(8)=dAOX(8)+COEF_IJ*yA*yB*yB*dEGx                             ! yA*yB*yB*COEF_IJ*EG
        dAOY(8)=dAOY(8)+COEF_IJ*(yA*yB*yB*dEGy+(yA*(yB+yB)+yB*yB)*EG)
        dAOZ(8)=dAOZ(8)+COEF_IJ*yA*yB*yB*dEGz
            
        dAOX(9)=dAOX(9)+COEF_IJ*yA*zB*zB*dEGx                             ! yA*zB*zB*COEF_IJ*EG
        dAOY(9)=dAOY(9)+COEF_IJ*(yA*zB*zB*dEGy+zB*zB*EG)
        dAOZ(9)=dAOZ(9)+COEF_IJ*(yA*zB*zB*dEGz+yA*(zB+zB)*EG)
              
        dAOX(10)=dAOX(10)+COEF_PXY*(yA*xB*yB*dEGx+yA*yB*EG)                ! yA*xB*yB*COEF_PXY*EG
        dAOY(10)=dAOY(10)+COEF_PXY*(yA*xB*yB*dEGy+(yA*xB+xB*yB)*EG)
        dAOZ(10)=dAOZ(10)+COEF_PXY*yA*xB*yB*dEGz
          
        dAOX(11)=dAOX(11)+COEF_PXY*(yA*xB*zB*dEGx+yA*zB*EG)                ! yA*xB*zB*COEF_PXY*EG
        dAOY(11)=dAOY(11)+COEF_PXY*(yA*xB*zB*dEGy+xB*zB*EG)
        dAOZ(11)=dAOZ(11)+COEF_PXY*(yA*xB*zB*dEGz+yA*xB*EG)

        dAOX(12)=dAOX(12)+COEF_PXY*(yA*yB*zB*dEGx)                         ! yA*yB*zB*COEF_PXY*EG
        dAOY(12)=dAOY(12)+COEF_PXY*(yA*yB*zB*dEGy+(yA*zB+yB*zB)*EG)
        dAOZ(12)=dAOZ(12)+COEF_PXY*(yA*yB*zB*dEGz+yA*yB*EG)

        dAOX(13)=dAOX(13)+COEF_IJ*(zA*xB*xB*dEGx+(zA*xB+zA*xB)*EG)        ! zA*xB*xB*COEF_IJ*EG
        dAOY(13)=dAOY(13)+COEF_IJ*zA*xB*xB*dEGy
        dAOZ(13)=dAOZ(13)+COEF_IJ*(zA*xB*xB*dEGz+xB*xB*EG)
        
        dAOX(14)=dAOX(14)+COEF_IJ*zA*yB*yB*dEGx                           ! zA*yB*yB*COEF_IJ*EG
        dAOY(14)=dAOY(14)+COEF_IJ*(zA*yB*yB*dEGy+(zA*yB+zA*yB)*EG)
        dAOZ(14)=dAOZ(14)+COEF_IJ*(zA*yB*yB*dEGz+yB*yB*EG)
            
        dAOX(15)=dAOX(15)+COEF_IJ*zA*zB*zB*dEGx                           ! zA*zB*zB*COEF_IJ*EG
        dAOY(15)=dAOY(15)+COEF_IJ*zA*zB*zB*dEGy
        dAOZ(15)=dAOZ(15)+COEF_IJ*(zA*zB*zB*dEGz+(zA*zB+zA*zB+zB*zB)*EG)
              
        dAOX(16)=dAOX(16)+COEF_PXY*(zA*xB*yB*dEGx+zA*yB*EG)                ! zA*xB*yB*COEF_PXY*EG
        dAOY(16)=dAOY(16)+COEF_PXY*(zA*xB*yB*dEGy+zA*xB*EG)
        dAOZ(16)=dAOZ(16)+COEF_PXY*(zA*xB*yB*dEGz+xB*yB*EG)
          
        dAOX(17)=dAOX(17)+COEF_PXY*(zA*xB*zB*dEGx+zA*zB*EG)                ! zA*xB*zB*COEF_PXY*EG 
        dAOY(17)=dAOY(17)+COEF_PXY*zA*xB*zB*dEGy
        dAOZ(17)=dAOZ(17)+COEF_PXY*(zA*xB*zB*dEGz+(zA*xB+zB*xB)*EG)

        dAOX(18)=dAOX(18)+COEF_PXY*zA*yB*zB*dEGx                           ! zA*yB*zB*COEF_PXY*EG 
        dAOY(18)=dAOY(18)+COEF_PXY*(zA*yB*zB*dEGy+zA*zB*EG)
        dAOZ(18)=dAOZ(18)+COEF_PXY*(zA*yB*zB*dEGz+(zA*yB+yB*zB)*EG)

! D|D
      case (11)
        COEF_XXXY = SQRT3*COEF_IJ
        COEF_XYXY = THREE*COEF_IJ
! xA*xA*xB*xB*COEF_IJ*EG
        dAOX(1)=dAOX(1)+COEF_IJ*(xA*xA*xB*xB*dEGx+(xA*xA*(xB+xB)+xB*xB*(xA+xA))*EG)
        dAOY(1)=dAOY(1)+COEF_IJ*xA*xA*xB*xB*dEGy                     
        dAOZ(1)=dAOZ(1)+COEF_IJ*xA*xA*xB*xB*dEgz

! xA*xA*yB*yB*COEF_IJ*EG
        dAOX(2)=dAOX(2)+COEF_IJ*(xA*xA*yB*yB*dEGx+yB*yB*(xA+xA)*EG)
        dAOY(2)=dAOY(2)+COEF_IJ*(xA*xA*yB*yB*dEGy+xA*xA*(yB+yB)*EG)
        dAOZ(2)=dAOZ(2)+COEF_IJ*xA*xA*yB*yB*dEGz

! xA*xA*zB*zB*COEF_IJ*EG
        dAOX(3)=dAOX(3)+COEF_IJ*(xA*xA*zB*zB*dEGx+zB*zB*(xA+xA)*EG) 
        dAOY(3)=dAOY(3)+COEF_IJ*xA*xA*zB*zB*dEGy
        dAOZ(3)=dAOZ(3)+COEF_IJ*(xA*xA*zB*zB*dEGz+xA*xA*(zB+zB)*EG)

! xA*xA*xB*yB*COEF_XXXY*EG
        dAOX(4)=dAOX(4)+COEF_XXXY*(xA*xA*xB*yB*dEGx+(xA*xA*yB+xB*yB*(xA+xA))*EG)
        dAOY(4)=dAOY(4)+COEF_XXXY*(xA*xA*xB*yB*dEGy+xA*xA*xB*EG)
        dAOZ(4)=dAOZ(4)+COEF_XXXY*xA*xA*xB*yB*dEGz

! xA*xA*xB*zB*COEF_XXXY*EG
        dAOX(5)=dAOX(5)+COEF_XXXY*(xA*xA*xB*zB*dEGx+(xA*xA*zB+xB*zB*(xA+xA))*EG)
        dAOY(5)=dAOY(5)+COEF_XXXY*xA*xA*xB*zB*dEGy
        dAOZ(5)=dAOZ(5)+COEF_XXXY*(xA*xA*xB*zB*dEGz+xA*xA*xB*EG)

! xA*xA*yB*zB*COEF_XXXY*EG
        dAOX(6)=dAOX(6)+COEF_XXXY*(xA*xA*yB*zB*dEGx+yB*zB*(xA+xA)*EG)
        dAOY(6)=dAOY(6)+COEF_XXXY*(xA*xA*yB*zB*dEGy+xA*xA*zB*EG)
        dAOZ(6)=dAOZ(6)+COEF_XXXY*(xA*xA*yB*zB*dEGz+xA*xA*yB*EG)

! yA*yA*xB*xB*COEF_IJ*EG 
        dAOX(7)=dAOX(7)+COEF_IJ*(yA*yA*xB*xB*dEGx+yA*yA*(xB+xB)*EG)
        dAOY(7)=dAOY(7)+COEF_IJ*(yA*yA*xB*xB*dEGy+(yA+yA)*xB*xB*EG)
        dAOZ(7)=dAOZ(7)+COEF_IJ*yA*yA*xB*xB*dEGz

! yA*yA*yB*yB*COEF_IJ*EG
        dAOX(8)=dAOX(8)+COEF_IJ*yA*yA*yB*yB*dEGx
        dAOY(8)=dAOY(8)+COEF_IJ*(yA*yA*yB*yB*dEGy+(yA*yA*(yB+yB)+(yA+yA)*yB*yB)*EG)
        dAOZ(8)=dAOZ(8)+COEF_IJ*yA*yA*yB*yB*dEGz

! yA*yA*zB*zB*COEF_IJ*EG
        dAOX(9)=dAOX(9)+COEF_IJ*yA*yA*zB*zB*dEGx
        dAOY(9)=dAOY(9)+COEF_IJ*(yA*yA*zB*zB*dEGy+zB*zB*(yA+yA)*EG)
        dAOZ(9)=dAOZ(9)+COEF_IJ*(yA*yA*zB*zB*dEGz+yA*yA*(zB+zB)*EG)

! yA*yA*xB*yB*COEF_XXXY*EG
        dAOX(10)=dAOX(10)+COEF_XXXY*(yA*yA*xB*yB*dEGx+yA*yA*yB*EG)
        dAOY(10)=dAOY(10)+COEF_XXXY*(yA*yA*xB*yB*dEGy+(yA*yA*xB+xB*yB*(yA+yA))*EG)
        dAOZ(10)=dAOZ(10)+COEF_XXXY*yA*yA*xB*yB*dEGz

! yA*yA*xB*zB*COEF_XXXY*EG
        dAOX(11)=dAOX(11)+COEF_XXXY*(yA*yA*xB*zB*dEGx+yA*yA*zB*EG)
        dAOY(11)=dAOY(11)+COEF_XXXY*(yA*yA*xB*zB*dEGy+(yA+yA)*xB*zB*EG)
        dAOZ(11)=dAOZ(11)+COEF_XXXY*(yA*yA*xB*zB*dEGz+yA*yA*xB*EG)

! yA*yA*yB*zB*COEF_XXXY*EG
        dAOX(12)=dAOX(12)+COEF_XXXY*yA*yA*yB*zB*dEGx
        dAOY(12)=dAOY(12)+COEF_XXXY*(yA*yA*yB*zB*dEGy+(yA*yA*zB+yB*zB*(yA+yA))*EG)
        dAOZ(12)=dAOZ(12)+COEF_XXXY*(yA*yA*yB*zB*dEGz+yA*yA*yB*EG)

! zA*zA*xB*xB*COEF_IJ*EG        
        dAOX(13)=dAOX(13)+COEF_IJ*(zA*zA*xB*xB*dEGx+zA*zA*(xB+xB)*EG)
        dAOY(13)=dAOY(13)+COEF_IJ*zA*zA*xB*xB*dEGy
        dAOZ(13)=dAOZ(13)+COEF_IJ*(zA*zA*xB*xB*dEGz+(zA+zA)*xB*xB*EG)

! zA*zA*yB*yB*COEF_IJ*EG      
        dAOX(14)=dAOX(14)+COEF_IJ*zA*zA*yB*yB*dEGx
        dAOY(14)=dAOY(14)+COEF_IJ*(zA*zA*yB*yB*dEGy+zA*zA*(yB+yB)*EG)
        dAOZ(14)=dAOZ(14)+COEF_IJ*(zA*zA*yB*yB*dEGz+(zA+zA)*yB*yB*EG)

! zA*zA*zB*zB*COEF_IJ*EG          
        dAOX(15)=dAOX(15)+COEF_IJ*zA*zA*zB*zB*dEGx
        dAOY(15)=dAOY(15)+COEF_IJ*zA*zA*zB*zB*dEGy
        dAOZ(15)=dAOZ(15)+COEF_IJ*(zA*zA*zB*zB*dEGz+(zA*zA*(zB+zB)+(zA+zA)*zB*zB)*EG)

! zA*zA*xB*yB*COEF_XXXY*EG              
        dAOX(16)=dAOX(16)+COEF_XXXY*(zA*zA*xB*yB*dEGx+zA*zA*yB*EG)
        dAOY(16)=dAOY(16)+COEF_XXXY*(zA*zA*xB*yB*dEGy+zA*zA*xB*EG)
        dAOZ(16)=dAOZ(16)+COEF_XXXY*(zA*zA*xB*yB*dEGz+(zA+zA)*xB*yB*EG)

! zA*zA*xB*zB*COEF_XXXY*EG
        dAOX(17)=dAOX(17)+COEF_XXXY*(zA*zA*xB*zB*dEGx+zA*zA*zB*EG)
        dAOY(17)=dAOY(17)+COEF_XXXY*zA*zA*xB*zB*dEGy
        dAOZ(17)=dAOZ(17)+COEF_XXXY*(zA*zA*xB*zB*dEGz+(zA*zA*xB+xB*zB*(zA+zA))*EG)

! zA*zA*yB*zB*COEF_XXXY*EG
        dAOX(18)=dAOX(18)+COEF_XXXY*zA*zA*yB*zB*dEGx
        dAOY(18)=dAOY(18)+COEF_XXXY*(zA*zA*yB*zB*dEGy+zA*zA*zB*EG)
        dAOZ(18)=dAOZ(18)+COEF_XXXY*(zA*zA*yB*zB*dEGz+(zA*zA*yB+yB*zB*(zA+zA))*EG)

! xA*yA*xB*xB*COEF_XXXY*EG
        dAOX(19)=dAOX(19)+COEF_XXXY*(xA*yA*xB*xB*dEGx+(xA*yA*(xB+xB)+xB*xB*yA)*EG)
        dAOY(19)=dAOY(19)+COEF_XXXY*(xA*yA*xB*xB*dEGy+xA*xB*xB*EG)
        dAOZ(19)=dAOZ(19)+COEF_XXXY*xA*yA*xB*xB*dEGz

! xA*yA*yB*yB*COEF_XXXY*EG
        dAOX(20)=dAOX(20)+COEF_XXXY*(xA*yA*yB*yB*dEGx+yA*yB*yB*EG)
        dAOY(20)=dAOY(20)+COEF_XXXY*(xA*yA*yB*yB*dEGy+(xA*yA*(yB+yB)+yB*yB*xA)*EG)
        dAOZ(20)=dAOZ(20)+COEF_XXXY*xA*yA*yB*yB*dEGz

! xA*yA*zB*zB*COEF_XXXY*EG        
        dAOX(21)=dAOX(21)+COEF_XXXY*(xA*yA*zB*zB*dEGx+yA*zB*zB*EG)
        dAOY(21)=dAOY(21)+COEF_XXXY*(xA*yA*zB*zB*dEGy+xA*zB*zB*EG)
        dAOZ(21)=dAOZ(21)+COEF_XXXY*(xA*yA*zB*zB*dEGz+xA*yA*(zB+zB)*EG)

! xA*yA*xB*yB*COEF_XYXY*EG      
        dAOX(22)=dAOX(22)+COEF_XYXY*(xA*yA*xB*yB*dEGx+(xA*yA*yB+xB*yB*yA)*EG)
        dAOY(22)=dAOY(22)+COEF_XYXY*(xA*yA*xB*yB*dEGy+(xA*yA*xB+xB*yB*xA)*EG)
        dAOZ(22)=dAOZ(22)+COEF_XYXY*xA*yA*xB*yB*dEGz

! xA*yA*xB*zB*COEF_XYXY*EG           
        dAOX(23)=dAOX(23)+COEF_XYXY*(xA*yA*xB*zB*dEGx+(xA*yA*zB+xB*zB*yA)*EG)
        dAOY(23)=dAOY(23)+COEF_XYXY*(xA*yA*xB*zB*dEGy+xB*zB*xA*EG)
        dAOZ(23)=dAOZ(23)+COEF_XYXY*(xA*yA*xB*zB*dEGz+xA*yA*xB*EG)

! xA*yA*yB*zB*COEF_XYXY*EG              
        dAOX(24)=dAOX(24)+COEF_XYXY*(xA*yA*yB*zB*dEGx+yA*yB*zB*EG)
        dAOY(24)=dAOY(24)+COEF_XYXY*(xA*yA*yB*zB*dEGy+(xA*yA*zB+yB*zB*xA)*EG)
        dAOZ(24)=dAOZ(24)+COEF_XYXY*(xA*yA*yB*zB*dEGz+xA*yA*yB*EG)

! xA*zA*xB*xB*COEF_XXXY*EG
        dAOX(25)=dAOX(25)+COEF_XXXY*(xA*zA*xB*xB*dEGx+(xA*zA*(xB+xB)+xB*xB*zA)*EG)
        dAOY(25)=dAOY(25)+COEF_XXXY*xA*zA*xB*xB*dEGy
        dAOZ(25)=dAOZ(25)+COEF_XXXY*(xA*zA*xB*xB*dEGz+xA*xB*xB*EG)

! xA*zA*yB*yB*COEF_XXXY*EG
        dAOX(26)=dAOX(26)+COEF_XXXY*(xA*zA*yB*yB*dEGx+zA*yB*yB*EG)
        dAOY(26)=dAOY(26)+COEF_XXXY*(xA*zA*yB*yB*dEGy+xA*zA*(yB+yB)*EG)
        dAOZ(26)=dAOZ(26)+COEF_XXXY*(xA*zA*yB*yB*dEGz+xA*yB*yB*EG)

! xA*zA*zB*zB*COEF_XXXY*EG
        dAOX(27)=dAOX(27)+COEF_XXXY*(xA*zA*zB*zB*dEGx+zA*zB*zB*EG)
        dAOY(27)=dAOY(27)+COEF_XXXY*xA*zA*zB*zB*dEGy
        dAOZ(27)=dAOZ(27)+COEF_XXXY*(xA*zA*zB*zB*dEGz+(xA*zA*(zB+zB)+zB*zB*xA)*EG)

! xA*zA*xB*yB*COEF_XYXY*EG
        dAOX(28)=dAOX(28)+COEF_XYXY*(xA*zA*xB*yB*dEGx+(xA*zA*yB+xB*yB*zA)*EG)
        dAOY(28)=dAOY(28)+COEF_XYXY*(xA*zA*xB*yB*dEGy+xA*zA*xB*EG)
        dAOZ(28)=dAOZ(28)+COEF_XYXY*(xA*zA*xB*yB*dEGz+xA*xB*yB*EG)

! xA*zA*xB*zB*COEF_XYXY*EG
        dAOX(29)=dAOX(29)+COEF_XYXY*(xA*zA*xB*zB*dEGx+(xA*zA*zB+xB*zB*zA)*EG)
        dAOY(29)=dAOY(29)+COEF_XYXY*xA*zA*xB*zB*dEGy
        dAOZ(29)=dAOZ(29)+COEF_XYXY*(xA*zA*xB*zB*dEGz+(xA*zA*xB+xB*zB*xA)*EG)

! xA*zA*yB*zB*COEF_XYXY*EG
        dAOX(30)=dAOX(30)+COEF_XYXY*(xA*zA*yB*zB*dEGx+zA*yB*zB*EG)
        dAOY(30)=dAOY(30)+COEF_XYXY*(xA*zA*yB*zB*dEGy+xA*zA*zB*EG)
        dAOZ(30)=dAOZ(30)+COEF_XYXY*(xA*zA*yB*zB*dEGz+(xA*zA*yB+yB*zB*xA)*EG)

! yA*zA*xB*xB*COEF_XXXY*EG
        dAOX(31)=dAOX(31)+COEF_XXXY*(yA*zA*xB*xB*dEGx+yA*zA*(xB+xB)*EG)
        dAOY(31)=dAOY(31)+COEF_XXXY*(yA*zA*xB*xB*dEGy+zA*xB*xB*EG)
        dAOZ(31)=dAOZ(31)+COEF_XXXY*(yA*zA*xB*xB*dEGz+yA*xB*xB*EG)

! yA*zA*yB*yB*COEF_XXXY*EG
        dAOX(32)=dAOX(32)+COEF_XXXY*yA*zA*yB*yB*dEGx
        dAOY(32)=dAOY(32)+COEF_XXXY*(yA*zA*yB*yB*dEGy+(yA*zA*(yB+yB)+yB*yB*zA)*EG)
        dAOZ(32)=dAOZ(32)+COEF_XXXY*(yA*zA*yB*yB*dEGz+yA*yB*yB*EG)

! yA*zA*zB*zB*COEF_XXXY*EG
        dAOX(33)=dAOX(33)+COEF_XXXY*yA*zA*zB*zB*dEGx
        dAOY(33)=dAOY(33)+COEF_XXXY*(yA*zA*zB*zB*dEGy+zA*zB*zB*EG)
        dAOZ(33)=dAOZ(33)+COEF_XXXY*(yA*zA*zB*zB*dEGz+(yA*zA*(zB+zB)+zB*zB*yA)*EG)

! yA*zA*xB*yB*COEF_XYXY*EG
        dAOX(34)=dAOX(34)+COEF_XYXY*(yA*zA*xB*yB*dEGx+yA*zA*yB*EG)
        dAOY(34)=dAOY(34)+COEF_XYXY*(yA*zA*xB*yB*dEGy+(yA*zA*xB+xB*yB*zA)*EG)
        dAOZ(34)=dAOZ(34)+COEF_XYXY*(yA*zA*xB*yB*dEGz+yA*xB*yB*EG)

! yA*zA*xB*zB*COEF_XYXY*EG
        dAOX(35)=dAOX(35)+COEF_XYXY*(yA*zA*xB*zB*dEGx+yA*zA*zB*EG)
        dAOY(35)=dAOY(35)+COEF_XYXY*(yA*zA*xB*zB*dEGy+zA*xB*zB*EG)
        dAOZ(35)=dAOZ(35)+COEF_XYXY*(yA*zA*xB*zB*dEGz+(yA*zA*xB+xB*zB*yA)*EG)

! yA*zA*yB*zB*COEF_XYXY*EG
        dAOX(36)=dAOX(36)+COEF_XYXY*yA*zA*yB*zB*dEGx
        dAOY(36)=dAOY(36)+COEF_XYXY*(yA*zA*yB*zB*dEGy+(yA*zA*zB+yB*zB*zA)*EG)
        dAOZ(36)=dAOZ(36)+COEF_XYXY*(yA*zA*yB*zB*dEGz+(yA*zA*yB+yB*zB*yA)*EG)

      case default
        write(UNIout,*)'ERROR> dG_PRODUCTS: DEFCASE type should not exist'
        stop ' ERROR> dG_PRODUCTS: DEFCASE type should not exist'
      end select
!
      end do ! Jgauss
      end do ! Igauss 
      
        IF(LIatmshl)then
          INDEX=0
          do I=1,Irange
          do J=1,Jrange
            INDEX=INDEX+1
            IF(I.GE.J)then  ! Corrected August 1,2003 had (J.ge.I)
              Iao=AOI+I
              Jao=AOJ+J
              IJao=Iao*(Iao-1)/2+Jao
              dAOprodX(IJao)=dAOX(INDEX)
              dAOprodY(IJao)=dAOY(INDEX)
              dAOprodZ(IJao)=dAOZ(INDEX)
            end if
          end do ! J   
          end do ! I
        else
              INDEX=0
          do I=1,Irange
          do J=1,Jrange
            INDEX=INDEX+1
            Iao=AOI+I
            Jao=AOJ+J
            IJao=Iao*(Iao-1)/2+Jao
            dAOprodX(IJao)=dAOX(INDEX)
            dAOprodY(IJao)=dAOY(INDEX)
            dAOprodZ(IJao)=dAOZ(INDEX)
          end do ! J
          end do ! I
        end if ! LIatmshl

      end do ! Jatmshl
      end do ! Iatmshl

      return
      end subroutine AB_dGproducts
! END CONTAINS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine dG_PRODUCTS
