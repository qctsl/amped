      subroutine EAMint(Av,Bv,Cv,Dv,a,b,c,d,A_ang,B_ang,C_ang,D_ang,eam)
! 
!   *****************************************************************
!   *                                                               *
!   *  Calculates antimomentum integrals using 5-term RR            *
!   *                                                               *
!   *       --------------------- OUTPUT ---------------------      *
!   *       eam = equimomentum integral                             *
!   *       --------------------------------------------------      *
!   *                                                               * 
!   *       --------------------- INPUT ----------------------      *
!   *       Av = cartesian coordinates of centre A                  *
!   *       Bv = cartesian coordinates of centre B                  *
!   *       Cv = cartesian coordinates of centre C                  *
!   *       Dv = cartesian coordinates of centre D                  *
!   *       a = exponent alpha (associated with centre A)           *
!   *       b = exponent beta (associated with centre B)            *
!   *       c = exponent gamma (associated with centre C)           *
!   *       d = exponent delta (associated with centre D)           *
!   *       A_ang = angular momentum of basis function on A         *
!   *       B_ang = angular momentum of basis function on B         *
!   *       C_ang = angular momentum of basis function on C         *
!   *       D_ang = angular momentum of basis function on D         * 
!   *       --------------------------------------------------      *
!   *                                                               *
!   *  For details of the equations contained in this section of    *
!   *  code, see JCP, 2021, 154, 074110.                            *
!   *                                                               *
!   *  code written by JWH (2018)                                   *
!   *                                                               *
!   *****************************************************************
!
!---------------------------------------------------------------------
! Input/Output:
!---------------------------------------------------------------------
      implicit none
      integer :: A_ang, B_ang, C_ang, D_ang
      double precision, dimension(3) :: Av, Bv, Cv, Dv
      double precision :: a, b, c, d
      double precision, dimension(1296) :: eam
!
!--------------------------------------------------------------------
! Local: 
!--------------------------------------------------------------------
!
      double precision, dimension(3) :: RRa1, RRb1, RRc1, RRd1
      double precision, dimension(3) :: Rv
      double precision :: G, Rsqd, lam
      double precision :: RRa2, RRa3, RRa4, RRa5
      double precision :: RRb2, RRb3, RRb4, RRb5
      double precision :: RRc2, RRc3, RRc4, RRc5
      double precision :: RRd2, RRd3, RRd4, RRd5
      integer :: Lcode, i, j, k, l, a_i, b_i, c_i, d_i
      integer :: ij_index, ijk_index, ijkl_index, ik_index, jk_index
      integer :: d_index, dp_index, pp_index, pd_index, dd_index, dpp_index, pdp_index, ppd_index
      integer :: ddp_index, dpd_index, pdd_index, ddpp_index, dpdp_index, dppd_index, ppp_index
      integer :: pddp_index, dppp_index, dp2_index, dp3_index, dddd_index, ddd_index, dddp_index
      integer :: ddpd_index, p1d_index, pdpd_index, pdpp_index, p2d_index, pddd_index, ppdp_index
      integer :: p1, p2, p3, p4, d1, d2, d3, d4
      integer :: Lvec_d(6,3), v_1i(3), xyz(3)
      double precision :: pi, sq3
      double precision, allocatable, dimension(:) :: Int1000, Int1100, Int1010, Int0110, Int1110
      double precision, allocatable, dimension(:) :: Int0100, Int2000, Int0200, Int0020, Int2100
      double precision, allocatable, dimension(:) :: Int2010, Int1200, Int2200, Int2020, Int2210
      double precision, allocatable, dimension(:) :: Int2120, Int2110, Int1210, Int1120, Int2211
      double precision, allocatable, dimension(:) :: Int2121, Int1220, Int2002, Int1102, Int2001
      double precision, allocatable, dimension(:) :: Int1020, Int2102, Int2101, Int2220, Int2221
      double precision, allocatable, dimension(:) :: Int0210, Int1002, Int1001, Int1221, Int0002
      double precision, allocatable, dimension(:) :: Int0010, Int0001, Int0220, Int0201, Int0102
      double precision, allocatable, dimension(:) :: Int2201, Int1201, Int0101, Int0202, Int1101
      double precision, allocatable, dimension(:) :: Int1202, Int2202, Int0011, Int0021, Int0120
      double precision, allocatable, dimension(:) :: Int0121, Int0111, Int0212, Int0211, Int0221
      double precision, allocatable, dimension(:) :: Int0122, Int0222
!
!--------------------------------------------------------------------
! Begin: 
!--------------------------------------------------------------------
!
      sq3 = dsqrt(3.0D0)
!
      pi = 3.14159265358979323846264338327950288419716939937510d0
! Set angular momentum code
      Lcode = 1000*A_ang + 100*B_ang + 10*C_ang + D_ang
! Set angular momentum vectors, for higher functions
      Lvec_d=0
      Lvec_d(1,1)=2 ! xx
      Lvec_d(2,2)=2 ! yy
      Lvec_d(3,3)=2 ! zz
      Lvec_d(4,1)=1 ! xy
      Lvec_d(4,2)=1 
      Lvec_d(5,1)=1 ! xz
      Lvec_d(5,3)=1 
      Lvec_d(6,2)=1 ! yz
      Lvec_d(6,3)=1 
! Set xyz code for p functions
      xyz(1)=1
      xyz(2)=2
      xyz(3)=3
!  
!--------------------------------------------------------------------
!  Calculate common intermediates, prefactor
!--------------------------------------------------------------------
!
      lam = a*b*c*d/(a*b*c + b*c*d + a*b*d + a*c*d)
!
      Rv = Av + Cv - Bv - Dv
!
      Rsqd = 0.0D0
      do i = 1,3
        Rsqd = Rsqd + Rv(i)*Rv(i)
      end do
!
!-------------------------------------------------------------------
! Calculate required G's
!-------------------------------------------------------------------
!
! There is only one G, which is equivalent to [0000]_L
!
      G = pi**1.5D0*dexp(-lam*Rsqd)/(8.0D0*(a*b*c + b*c*d + a*b*d + a*c*d)**1.5D0)
!      
! Calculate coefficents for RR
!
! RR1
!
      RRa1 = -lam/a*Rv                                             ! [abcd]
!
      RRb1 =  lam/b*Rv
!
      RRc1 = -lam/c*Rv
!
      RRd1 =  lam/d*Rv
!
! RRa
!
      RRa2 = (a - lam)/(2.0D0*a**2)                          ! [(a-1i)bcd]
      RRa3 = lam/(2.0D0*a*b)                                 ! [a(b-1i)cd]
      RRa4 = -lam/(2.0D0*a*c)                                ! [ab(c-1i)d]
      RRa5 = lam/(2.0D0*a*d)                                 ! [abc(d-1i)]
!
! RRb 
!
      RRb2 =  lam/(2.0D0*a*b)                                ! [(a-1i)bcd]
      RRb3 =  (b-lam)/(2.0D0*b**2)                           ! [a(b-1i)cd]
      RRb4 =  lam/(2.0D0*b*c)                                ! [ab(c-1i)d]
      RRb5 = -lam/(2.0D0*b*d)                                ! [abc(d-1i)]
!
! RRc
!   
      RRc2 =  -lam/(2.0D0*a*c)                               ! [(a-1i)bcd]
      RRc3 =  lam/(2.0D0*b*c)                                ! [a(b-1i)cd]
      RRc4 =  (c - lam)/(2.0D0*c**2)                         ! [ab(c-1i)d]
      RRc5 =  lam/(2.0D0*c*d)                                ! [abc(d-1i)]
!
! RRd
!    
      RRd2 =  lam/(2.0D0*a*d)                                ! [(a-1i)bcd]
      RRd3 = -lam/(2.0D0*b*d)                                ! [a(b-1i)cd]
      RRd4 =  lam/(2.0D0*c*d)                                ! [ab(c-1i)d]
      RRd5 =  (d - lam)/(2.0D0*d**2)                         ! [abc(d-1i)]
!
!------------------------------------------------------------------
! [ssss]
!------------------------------------------------------------------
      if(Lcode.eq.0)then
!
        eam(1) = G
!
        return
      end if
!
!------------------------------------------------------------------
! [psss]
!------------------------------------------------------------------
      if(Lcode.eq.1000)then
!
! Requires one step
!
! Step 1 :
! [1000] from [0000]
!
        do i=1,3 ! x,y,z 
          eam(i) = RRa1(i)*G
        end do !
!
! print for testing
!        write(6,'(a,I4)')'Lcode= ',Lcode
!        write(6,'(a,F16.10)')'a= ',a
!        write(6,'(a,F16.10)')'b= ',b
!        write(6,'(a,F16.10)')'c= ',c
!        write(6,'(a,F16.10)')'d= ',d
!        write(6,'(a,F16.10)')'G= ',G
!        do i=1,3
!          write(6,'(a,I1,a,F16.10)')'eam(',i,')= ',eam(i)
!        end do
!
        return
      end if
!
!------------------------------------------------------------------
! [spss]
!------------------------------------------------------------------
      if(Lcode.eq.100)then
!
! Requires one step
!
! Step 1 :
! [0100] from [0000]
!
        do i=1,3 ! x,y,z 
          eam(i) = RRb1(i)*G
        end do !
!
        return
      end if
!
!------------------------------------------------------------------
! [ssps]
!------------------------------------------------------------------
      if(Lcode.eq.10)then
!
! Requires one step
!
! Step 1 :
! [0010] from [0000]
!
        do i=1,3 ! x,y,z 
          eam(i) = RRc1(i)*G
        end do !
!
        return
      end if
!
!------------------------------------------------------------------
! [ppss]
!------------------------------------------------------------------
      if(Lcode.eq.1100)then
!
! Requires 2 steps
!
        allocate(Int1000(3))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i
!
! Step 2 :
! [1100] from [1000] and [0000]
! b+1i
        do i=1,3 ! x,y,z 
          do j=1,3 ! x,y,z
            ij_index = 3*(i-1) + j
            if(i.eq.j)then ! 2-terms
              eam(ij_index) = RRb1(j)*Int1000(i) &
                                  + RRb2*G
            else ! 1-term
              eam(ij_index) = RRb1(j)*Int1000(i)
            end if
          end do ! j
        end do ! i
!
        return
      end if
!
!------------------------------------------------------------------
! [pssp]
!------------------------------------------------------------------
      if(Lcode.eq.1001)then
!
! Requires 2 steps
!
        allocate(Int1000(3))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i
!
! Step 2 :
! [1001] from [1000] and [0000]
! d+1i
        do i=1,3 ! x,y,z 
          do j=1,3 ! x,y,z
            ij_index = 3*(i-1) + j
            if(i.eq.j)then ! 2-terms
              eam(ij_index) = RRd1(j)*Int1000(i) &
                                  + RRd2*G
            else ! 1-term 
              eam(ij_index) = RRd1(j)*Int1000(i)
            end if
          end do ! j
        end do ! i
!
        return
      end if
!
!------------------------------------------------------------------
! [psps]
!------------------------------------------------------------------
      if(Lcode.eq.1010)then
!
! Requires 2 steps
!
        allocate(Int1000(3))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i
!
! Step 2 :
! [1010] from [1000] and [0000]
! c+1i
        do i=1,3 ! x,y,z 
          do j=1,3 ! x,y,z
            ij_index = 3*(i-1) + j
            if(i.eq.j)then ! 2-terms
              eam(ij_index) = RRc1(j)*Int1000(i) &
                                  + RRc2*G
            else ! 1-term 
              eam(ij_index) = RRc1(j)*Int1000(i)
            end if
          end do ! j
        end do ! i
!
        return
      end if
!
!------------------------------------------------------------------
! [spsp]
!------------------------------------------------------------------
      if(Lcode.eq.101)then
!
! Requires 2 steps
!
        allocate(Int0100(3))
!
! Step 1 :
! [0100] from [0000]
! b+1i
!
        do i=1,3 ! x,y,z
          Int0100(i) = RRb1(i)*G
        end do ! i
!
! Step 2 :
! [0101] from [0100] and [0000]
! d+1i
        do i=1,3 ! x,y,z 
          do j=1,3 ! x,y,z
            ij_index = 3*(i-1) + j
            if(i.eq.j)then ! 2-terms
              eam(ij_index) = RRd1(j)*Int0100(i) &
                                  + RRd3*G
            else ! 1-term 
              eam(ij_index) = RRd1(j)*Int0100(i)
            end if
          end do ! j
        end do ! i
!
        return
      end if
!
!------------------------------------------------------------------
! [spps]
!------------------------------------------------------------------
      if(Lcode.eq.110)then
!
! Requires 2 steps
!
        allocate(Int0100(3))
!
! Step 1 :
! [0100] from [0000]
! b+1i
!
        do i=1,3 ! x,y,z
          Int0100(i) = RRb1(i)*G
        end do ! i
!
! Step 2 :
! [0110] from [0100] and [0000]
! c+1i
        do i=1,3 ! x,y,z 
          do j=1,3 ! x,y,z
            ij_index = 3*(i-1) + j
            if(i.eq.j)then ! 2-terms
              eam(ij_index) = RRc1(j)*Int0100(i) &
                                  + RRc3*G
            else ! 1-term 
              eam(ij_index) = RRc1(j)*Int0100(i)
            end if
          end do ! j
        end do ! i
!
        return
      end if
!
!------------------------------------------------------------------
! [ppps]
!------------------------------------------------------------------
      if(Lcode.eq.1110)then
!
! Requires 4 steps
!
        allocate(Int1000(3),Int0100(3),Int1100(9))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i
!
! Step 2 :
! [0100] from [0000]
! b+1i
!
        do i=1,3 ! x,y,z
          Int0100(i) = RRb1(i)*G
        end do ! i
!
! Step 3 :
! [1100] from [1000] and [0000]
! b+1i
!
        do i=1,3 ! x,y,z 
          do j=1,3 ! x,y,z 
            ij_index = 3*(i-1) + j
            if(i.eq.j)then ! 2-terms
              Int1100(ij_index) = RRb1(j)*Int1000(i) &
                                + RRb2*G
            else ! 1-term
              Int1100(ij_index) = RRb1(j)*Int1000(i)
            end if
          end do ! j
        end do ! i
!
! Step 4 :
! [1110] from [1100], [1000], and [0100]
! c+1i
!
        do i=1,3 ! x,y,z
          do j=1,3 ! x,y,z
            do k=1,3 ! x,y,z
              ijk_index = 9*(i-1) + 3*(j-1) + k
              ij_index = 3*(i-1) + j
              if((i.eq.k).and.(j.eq.k))then ! 3-terms
                eam(ijk_index) = RRc1(k)*Int1100(ij_index) &
                                     + RRc2*Int0100(j) + RRc3*Int1000(i)
              else if(i.eq.k)then ! 2-terms
                eam(ijk_index) = RRc1(k)*Int1100(ij_index) &
                                     + RRc2*Int0100(j)
              else if(j.eq.k)then ! 2-terms
                eam(ijk_index) = RRc1(k)*Int1100(ij_index) &
                                     + RRc3*Int1000(i)
              else ! 1-term
                eam(ijk_index) = RRc1(k)*Int1100(ij_index)
              end if
            end do ! k
          end do ! j
        end do ! i
!
        return
      end if
!
!------------------------------------------------------------------
! [ppsp]
!------------------------------------------------------------------
      if(Lcode.eq.1101)then
!
! Requires 4 steps
!
        allocate(Int1000(3),Int0100(3),Int1100(9))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i
!
! Step 2 :
! [0100] from [0000]
! b+1i
!
        do i=1,3 ! x,y,z
          Int0100(i) = RRb1(i)*G
        end do ! i
!
! Step 3 :
! [1100] from [1000] and [0000]
! b+1i
!
        do i=1,3 ! x,y,z 
          do j=1,3 ! x,y,z 
            ij_index = 3*(i-1) + j
            if(i.eq.j)then ! 2-terms
              Int1100(ij_index) = RRb1(j)*Int1000(i) &
                                + RRb2*G
            else ! 1-term
              Int1100(ij_index) = RRb1(j)*Int1000(i)
            end if
          end do ! j
        end do ! i
!
! Step 4 :
! [1101] from [1100], [1000], and [0100]
! d+1i
!
        do i=1,3 ! x,y,z
          do j=1,3 ! x,y,z
            do k=1,3 ! x,y,z
              ijk_index = 9*(i-1) + 3*(j-1) + k
              ij_index = 3*(i-1) + j
              if((i.eq.k).and.(j.eq.k))then ! 3-terms
                eam(ijk_index) = RRd1(k)*Int1100(ij_index) &
                                     + RRd2*Int0100(j) + RRd3*Int1000(i)
              else if(i.eq.k)then ! 2-terms
                eam(ijk_index) = RRd1(k)*Int1100(ij_index) &
                                     + RRd2*Int0100(j)
              else if(j.eq.k)then ! 2-terms
                eam(ijk_index) = RRd1(k)*Int1100(ij_index) &
                                     + RRd3*Int1000(i)
              else ! 1-term
                eam(ijk_index) = RRd1(k)*Int1100(ij_index)
              end if
            end do ! k
          end do ! j
        end do ! i
!
        return
      end if
!
!------------------------------------------------------------------
! [sppp]
!------------------------------------------------------------------
      if(Lcode.eq.111)then
!
! Requires 4 steps
!
        allocate(Int0010(3),Int0100(3),Int0110(9))
!
! Step 1 :
! [0010] from [0000]
! c+1i
!
        do i=1,3 ! x,y,z
          Int0010(i) = RRc1(i)*G
        end do ! i
!
! Step 2 :
! [0100] from [0000]
! b+1i
!
        do i=1,3 ! x,y,z
          Int0100(i) = RRb1(i)*G
        end do ! i
!
! Step 3 :
! [0110] from [0100] and [0000]
! c+1i
!
        do i=1,3 ! x,y,z 
          do j=1,3 ! x,y,z 
            ij_index = 3*(i-1) + j
            if(i.eq.j)then ! 2-terms
              Int0110(ij_index) = RRc1(j)*Int0100(i) &
                                + RRc3*G
            else ! 1-term
              Int0110(ij_index) = RRc1(j)*Int0100(i)
            end if
          end do ! j
        end do ! i
!
! Step 4 :
! [0111] from [0110], [0010], and [0100]
! d+1i
!
        do i=1,3 ! x,y,z
          do j=1,3 ! x,y,z
            do k=1,3 ! x,y,z
              ijk_index = 9*(i-1) + 3*(j-1) + k
              ij_index = 3*(i-1) + j
              if((i.eq.k).and.(j.eq.k))then ! 3-terms
                eam(ijk_index) = RRd1(k)*Int0110(ij_index) &
                                     + RRd3*Int0010(j) + RRd4*Int0100(i)
              else if(i.eq.k)then ! 2-terms
                eam(ijk_index) = RRd1(k)*Int0110(ij_index) &
                                     + RRd3*Int0010(j)
              else if(j.eq.k)then ! 2-terms
                eam(ijk_index) = RRd1(k)*Int0110(ij_index) &
                                     + RRd4*Int0100(i)
              else ! 1-term
                eam(ijk_index) = RRd1(k)*Int0110(ij_index)
              end if
            end do ! k
          end do ! j
        end do ! i
!
        return
      end if
!
!------------------------------------------------------------------
! [pppp]
!------------------------------------------------------------------
      if(Lcode.eq.1111)then
!
! Requires 7 steps
!
        allocate(Int1000(3),Int0100(3),Int1100(9))
        allocate(Int1010(9),Int0110(9),Int1110(27))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i
!
! Step 2 :
! [0100] from [0000]
! b+1i
!
        do i=1,3 ! x,y,z
          Int0100(i) = RRb1(i)*G
        end do ! i
!
! Step 3 :
! [1100] from [1000] and [0000]
! b+1i
!
        do i=1,3 ! x,y,z 
          do j=1,3 ! x,y,z
            ij_index = 3*(i-1) + j
            if(i.eq.j)then ! 2-terms
              Int1100(ij_index) = RRb1(j)*Int1000(i) &
                                + RRb2*G
            else ! 1-term
              Int1100(ij_index) = RRb1(j)*Int1000(i)
            end if
          end do ! j
        end do ! i
!
! Step 4 :
! [1010] from [1000] and [0000]
! c+1i
!
        do i=1,3 ! x,y,z
          do j=1,3 ! x,y,z
            ij_index = 3*(i-1) + j
            if(i.eq.j)then ! 2-terms
              Int1010(ij_index) = RRc1(j)*Int1000(i) &
                                + RRc2*G
            else ! 1-term
              Int1010(ij_index) = RRc1(j)*Int1000(i)
            end if
          end do ! j
        end do ! i
!
! Step 5 :
! [0110] from [0100] and [0000]
! c+1i
!
        do i=1,3 ! x,y,z
          do j=1,3 ! x,y,z
            ij_index = 3*(i-1) + j
            if(i.eq.j)then ! 2-terms
              Int0110(ij_index) = RRc1(j)*Int0100(i) &
                                + RRc3*G
            else ! 2-terms
              Int0110(ij_index) = RRc1(j)*Int0100(i)
            end if
          end do ! j
        end do ! i
!
! Step 6 :
! [1110] from [1100], [0100] and [1000]
! c+1i
!
        do i=1,3 ! x,y,z
          do j=1,3 ! x,y,z
            do k=1,3 ! x,y,z
              ijk_index = 9*(i-1) + 3*(j-1) + k
              ij_index = 3*(i-1) + j
              if((i.eq.k).and.(j.eq.k))then ! 3-terms
                Int1110(ijk_index) = RRc1(k)*Int1100(ij_index) &
                                   + RRc2*Int0100(j) + RRc3*Int1000(i)
              else if(i.eq.k)then ! 2-terms
                Int1110(ijk_index) = RRc1(k)*Int1100(ij_index) &
                                   + RRc2*Int0100(j)
              else if(j.eq.k)then ! 2-terms
                Int1110(ijk_index) = RRc1(k)*Int1100(ij_index) &
                                   + RRc3*Int1000(i)
              else ! 1-term
                Int1110(ijk_index) = RRc1(k)*Int1100(ij_index)
              end if
            end do ! k
          end do ! j
        end do ! i
!
! Step 7 :
! [1111] from [1110], [0110], [1010] and [1100]
! d+1i
!
        do i=1,3 ! x,y,z 
          do j=1,3 ! x,y,z 
            do k=1,3 ! x,y,z 
              do l=1,3 ! x,y,z 
                ijkl_index = 27*(i-1) + 9*(j-1) + 3*(k-1) + l
                ijk_index = 9*(i-1) + 3*(j-1) + k
                ij_index = 3*(i-1) + j
                ik_index = 3*(i-1) + k
                jk_index = 3*(j-1) + k
                if((i.eq.l).and.(j.eq.l).and.(k.eq.l))then
                  eam(ijkl_index) = RRd1(l)*Int1110(ijk_index) &
                                        + RRd2*Int0110(jk_index) + RRd3*Int1010(ik_index) &
                                        + RRd4*Int1100(ij_index)
                else if((i.eq.l).and.(j.eq.l))then
                  eam(ijkl_index) = RRd1(l)*Int1110(ijk_index) &
                                        + RRd2*Int0110(jk_index) + RRd3*Int1010(ik_index)
                else if((i.eq.l).and.(k.eq.l))then 
                  eam(ijkl_index) = RRd1(l)*Int1110(ijk_index) &
                                       + RRd2*Int0110(jk_index) &
                                       + RRd4*Int1100(ij_index)
                else if((j.eq.l).and.(k.eq.l))then 
                  eam(ijkl_index) = RRd1(l)*Int1110(ijk_index) &
                                       + RRd3*Int1010(ik_index) &
                                       + RRd4*Int1100(ij_index)
                else if(i.eq.l)then 
                  eam(ijkl_index) = RRd1(l)*Int1110(ijk_index) &
                                        + RRd2*Int0110(jk_index)
                else if(j.eq.l)then 
                  eam(ijkl_index) = RRd1(l)*Int1110(ijk_index) &
                                        + RRd3*Int1010(ik_index)
                else if(k.eq.l)then 
                  eam(ijkl_index) = RRd1(l)*Int1110(ijk_index) &
                                        + RRd4*Int1100(ij_index)
                else
                  eam(ijkl_index) = RRd1(l)*Int1110(ijk_index) 
                end if
              end do ! l
            end do ! k
          end do ! j
        end do ! l
!
        return
      end if
!
!------------------------------------------------------------------
! [dsss]
! Note: d integrals required some modification of code notation
!------------------------------------------------------------------
      if(Lcode.eq.2000)then
!
! Requires 2 steps
!
        allocate(Int1000(3))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xy-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          eam(d_index) = RRa1(i)*Int1000(p1) &
                             + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            eam(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        eam(4:6)= sq3*eam(4:6)
! 
        return
      end if
!
!------------------------------------------------------------------
! [sdss]
! Note: d integrals required some modification of code notation
!------------------------------------------------------------------
      if(Lcode.eq.200)then
!
! Requires 2 steps
!
        allocate(Int0100(3))
!
! Step 1 :
! [0100] from [0000]
! b+1i
!
        do i=1,3 ! x,y,z
          Int0100(i) = RRb1(i)*G
        end do ! i 
!
! Step 2 :
! [0200] from [0100] and [0000]
! b+1i
!
! Note: d's are numbered xy-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          eam(d_index) = RRb1(i)*Int0100(p1) &
                             + RRb3*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            eam(d_index) = RRb1(i)*Int0100(p1)
          end do ! i
        end do ! p1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        eam(4:6)= sq3*eam(4:6)
! 
        return
      end if
!
!------------------------------------------------------------------
! [ssds]
! Note: d integrals required some modification of code notation
!------------------------------------------------------------------
      if(Lcode.eq.20)then
!
! Requires 2 steps
!
        allocate(Int0010(3))
!
! Step 1 :
! [0010] from [0000]
! c+1i
!
        do i=1,3 ! x,y,z
          Int0010(i) = RRc1(i)*G
        end do ! i 
!
! Step 2 :
! [0020] from [0010] and [0000]
! c+1i
!
! Note: d's are numbered xy-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          eam(d_index) = RRc1(i)*Int0010(p1) &
                             + RRc4*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            eam(d_index) = RRc1(i)*Int0010(p1)
          end do ! i
        end do ! p1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        eam(4:6)= sq3*eam(4:6)
! 
        return
      end if
!
!------------------------------------------------------------------
! [dpss]
!------------------------------------------------------------------
      if(Lcode.eq.2100)then
!
! Requires 3 steps
!
        allocate(Int1000(3),Int2000(6))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xy-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int2000(d_index) = RRa1(i)*Int1000(p1) &
                           + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design i ne p1
            Int2000(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! Step 3:
! [2100] from [2000] and [1000]
! b+1i
!
        dp_index=0
        do d1=1,6
          do i=1,3 ! x, y, z
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i)
!
            eam(dp_index) = RRb1(i)*Int2000(d1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              eam(dp_index) = eam(dp_index) &
                                  + dble(a_i)*RRb2*Int1000(p1)
            end if
          end do ! i
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        do d1=4,6
          do p2=1,3
            dp_index=3*(d1-1)+p2
            eam(dp_index) = sq3*eam(dp_index)
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [dssp]
!------------------------------------------------------------------
      if(Lcode.eq.2001)then
!
! Requires 3 steps
!
        allocate(Int1000(3),Int2000(6))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xy-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int2000(d_index) = RRa1(i)*Int1000(p1) &
                           + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design i ne p1
            Int2000(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! Step 3:
! [2001] from [2000] and [1000]
! d+1i
!
        dp_index=0
        do d1=1,6
          do i=1,3 ! x, y, z
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i)
!
            eam(dp_index) = RRd1(i)*Int2000(d1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              eam(dp_index) = eam(dp_index) &
                                 + dble(a_i)*RRd2*Int1000(p1)
            end if
          end do ! i
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        do d1=4,6
          do p2=1,3
            dp_index=3*(d1-1)+p2
            eam(dp_index) = sq3*eam(dp_index)
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [pdss]
!------------------------------------------------------------------
      if(Lcode.eq.1200)then
!
! Requires 3 steps
!
        allocate(Int0100(3),Int0200(6))
!
! Step 1 :
! [0100] from [0000]
! b+1i
!
        do i=1,3 ! x,y,z
          Int0100(i) = RRb1(i)*G
        end do ! i 
!
! Step 2 :
! [0200] from [0100] and [0000]
! b+1i
!
! Note: d's are numbered xy-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int0200(d_index) = RRb1(i)*Int0100(p1) &
                           + RRb3*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design i ne p1
            Int0200(d_index) = RRb1(i)*Int0100(p1)
          end do ! i
        end do ! p1
!
! Step 3:
! [1200] from [0200] and [0100]
! b+1i
!
        pd_index=0
        do i=1,3 ! x, y, z
          do d1=1,6
            pd_index=pd_index+1
!
            b_i=Lvec_d(d1,i)
!
            eam(pd_index) = RRa1(i)*Int0200(d1)
            if(b_i.ne.0)then ! (b-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              eam(pd_index) = eam(pd_index) &
                                  + dble(b_i)*RRa3*Int0100(p1)
            end if
          end do ! d1
        end do ! i
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        do p1=1,3
          do d2=4,6
            pd_index=6*(p1-1)+d2
            eam(pd_index) = sq3*eam(pd_index)
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [pssd]
!------------------------------------------------------------------
      if(Lcode.eq.1002)then
!
! Requires 3 steps
!
        allocate(Int0001(3),Int0002(6))
!
! Step 1 :
! [0001] from [0000]
! d+1i
!
        do i=1,3 ! x,y,z
          Int0001(i) = RRd1(i)*G
        end do ! i 
!
! Step 2 :
! [0002] from [0001] and [0000]
! d+1i
!
! Note: d's are numbered xy-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int0002(d_index) = RRd1(i)*Int0001(p1) &
                           + RRd5*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design i ne p1
            Int0002(d_index) = RRd1(i)*Int0001(p1)
          end do ! i
        end do ! p1
!
! Step 3:
! [1002] from [0002] and [0001]
! b+1i
!
        pd_index=0
        do i=1,3 ! x, y, z
          do d1=1,6
            pd_index=pd_index+1
!
            d_i=Lvec_d(d1,i)
!
            eam(pd_index) = RRa1(i)*Int0002(d1)
            if(d_i.ne.0)then ! (d-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              eam(pd_index) = eam(pd_index) &
                                  + dble(d_i)*RRa5*Int0001(p1)
            end if
          end do ! d1
        end do ! i
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        do p1=1,3
          do d2=4,6
            pd_index=6*(p1-1)+d2
            eam(pd_index) = sq3*eam(pd_index)
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [psds]
!------------------------------------------------------------------
      if(Lcode.eq.1020)then
!
! Requires 3 steps
!
        allocate(Int0010(3),Int0020(6))
!
! Step 1 :
! [0010] from [0000]
! c+1i
!
        do i=1,3 ! x,y,z
          Int0010(i) = RRc1(i)*G
        end do ! i 
!
! Step 2 :
! [0020] from [0010] and [0000]
! c+1i
!
! Note: d's are numbered xy-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int0020(d_index) = RRc1(i)*Int0010(p1) &
                           + RRc4*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design i ne p1
            Int0020(d_index) = RRc1(i)*Int0010(p1)
          end do ! i
        end do ! p1
!
! Step 3:
! [1020] from [0020] and [0010]
! a+1i
!
        pd_index=0
        do i=1,3 ! x, y, z
          do d1=1,6
            pd_index=pd_index+1
!
            c_i=Lvec_d(d1,i)
!
            eam(pd_index) = RRa1(i)*Int0020(d1)
            if(c_i.ne.0)then ! (c-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              eam(pd_index) = eam(pd_index) &
                                  + dble(c_i)*RRa4*Int0010(p1)
            end if
          end do ! d1
        end do ! i
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        do p1=1,3
          do d2=4,6
            pd_index=6*(p1-1)+d2
            eam(pd_index) = sq3*eam(pd_index)
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [spds]
!------------------------------------------------------------------
      if(Lcode.eq.120)then
!
! Requires 3 steps
!
        allocate(Int0010(3),Int0020(6))
!
! Step 1 :
! [0010] from [0000]
! c+1i
!
        do i=1,3 ! x,y,z
          Int0010(i) = RRc1(i)*G
        end do ! i 
!
! Step 2 :
! [0020] from [0010] and [0000]
! c+1i
!
! Note: d's are numbered xy-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int0020(d_index) = RRc1(i)*Int0010(p1) &
                           + RRc4*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design i ne p1
            Int0020(d_index) = RRc1(i)*Int0010(p1)
          end do ! i
        end do ! p1
!
! Step 3:
! [0120] from [0020] and [0010]
! b+1i
!
        pd_index=0
        do i=1,3 ! x, y, z
          do d1=1,6
            pd_index=pd_index+1
!
            c_i=Lvec_d(d1,i)
!
            eam(pd_index) = RRb1(i)*Int0020(d1)
            if(c_i.ne.0)then ! (c-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              eam(pd_index) = eam(pd_index) &
                                  + dble(c_i)*RRb4*Int0010(p1)
            end if
          end do ! d1
        end do ! i
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        do p1=1,3
          do d2=4,6
            pd_index=6*(p1-1)+d2
            eam(pd_index) = sq3*eam(pd_index)
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [dsps]
!------------------------------------------------------------------
      if(Lcode.eq.2010)then
!
! Requires 3 steps
!
        allocate(Int1000(3),Int2000(6))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xy-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int2000(d_index) = RRa1(i)*Int1000(p1) &
                           + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design i ne p1
            Int2000(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! Step 3:
! [2010] from [2000] and [1000]
! c+1i
!
        dp_index=0
        do d1=1,6
          do i=1,3 ! x, y, z
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i)
!
            eam(dp_index) = RRc1(i)*Int2000(d1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              eam(dp_index) = eam(dp_index) &
                                  + dble(a_i)*RRc2*Int1000(p1)
            end if
          end do ! i
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        do d1=4,6
          do p2=1,3
            dp_index=3*(d1-1)+p2
            eam(dp_index) = sq3*eam(dp_index)
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [sdsp]
!------------------------------------------------------------------
      if(Lcode.eq.201)then
!
! Requires 3 steps
!
        allocate(Int0100(3),Int0200(6))
!
! Step 1 :
! [0100] from [0000]
! b+1i
!
        do i=1,3 ! x,y,z
          Int0100(i) = RRb1(i)*G
        end do ! i 
!
! Step 2 :
! [0200] from [1000] and [0000]
! b+1i
!
! Note: d's are numbered xy-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int0200(d_index) = RRb1(i)*Int0100(p1) &
                           + RRb3*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design i ne p1
            Int0200(d_index) = RRb1(i)*Int0100(p1)
          end do ! i
        end do ! p1
!
! Step 3:
! [0201] from [0200] and [0100]
! d+1i
!
        dp_index=0
        do d1=1,6
          do i=1,3 ! x, y, z
            dp_index=dp_index+1
!
            b_i=Lvec_d(d1,i)
!
            eam(dp_index) = RRd1(i)*Int0200(d1)
            if(b_i.ne.0)then ! (b-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              eam(dp_index) = eam(dp_index) &
                                  + dble(b_i)*RRd3*Int0100(p1)
            end if
          end do ! i
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        do d1=4,6
          do p2=1,3
            dp_index=3*(d1-1)+p2
            eam(dp_index) = sq3*eam(dp_index)
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [sdps]
!------------------------------------------------------------------
      if(Lcode.eq.210)then
!
! Requires 3 steps
!
        allocate(Int0100(3),Int0200(6))
!
! Step 1 :
! [0100] from [0000]
! b+1i
!
        do i=1,3 ! x,y,z
          Int0100(i) = RRb1(i)*G
        end do ! i 
!
! Step 2 :
! [0200] from [1000] and [0000]
! b+1i
!
! Note: d's are numbered xy-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int0200(d_index) = RRb1(i)*Int0100(p1) &
                           + RRb3*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design i ne p1
            Int0200(d_index) = RRb1(i)*Int0100(p1)
          end do ! i
        end do ! p1
!
! Step 3:
! [0210] from [0200] and [0100]
! c+1i
!
        dp_index=0
        do d1=1,6
          do i=1,3 ! x, y, z
            dp_index=dp_index+1
!
            b_i=Lvec_d(d1,i)
!
            eam(dp_index) = RRc1(i)*Int0200(d1)
            if(b_i.ne.0)then ! (b-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              eam(dp_index) = eam(dp_index) &
                                  + dble(b_i)*RRc3*Int0100(p1)
            end if
          end do ! i
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        do d1=4,6
          do p2=1,3
            dp_index=3*(d1-1)+p2
            eam(dp_index) = sq3*eam(dp_index)
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [ddss]
!------------------------------------------------------------------
      if(Lcode.eq.2200)then
!
! Requires 5 steps
!
        allocate(Int1000(3),Int2000(6),Int1100(9))
        allocate(Int2100(18))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2:
! [1100] from [1000] and [0000]
! b+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int1100(pp_index) = RRb1(i)*Int1000(p1)
            if(p1.eq.i)then
              Int1100(pp_index) = Int1100(pp_index) &
                                + RRb2*G
            end if
          end do ! i
        end do ! p1
!
! Step 3 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xy-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int2000(d_index) = RRa1(i)*Int1000(p1) &
                           + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int2000(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! Step 4:
! [2100] from [2000] and [1000]
! b+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int2100(dp_index) = RRb1(i)*Int2000(d1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              Int2100(dp_index) = Int2100(dp_index) &
                                + dble(a_i)*RRb2*Int1000(p1)
            end if
          end do ! i
        end do ! d1
!
! Step 5:
! [2200] from [2100], [2000] and [1100]
! b+1i
!
        dd_index=0
        do d1=1,6
          do p2=1,3
            i=p2 ! do xx, yy, zz first
            dd_index=dd_index+1
            dp_index=3*(d1-1)+p2
! 
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            eam(dd_index) = RRb1(i)*Int2100(dp_index) &
                               + RRb3*Int2000(d1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              eam(dd_index) = eam(dd_index) &
                                 + dble(a_i)*RRb2*Int1100(pp_index)
            end if
          end do ! p2
!
          do p2=2,3 ! y, z
            dp_index=3*(d1-1)+p2
            do i=1,p2-1 ! x, y
              dd_index=dd_index+1
!
! i ne p2, therefore no (b-1i) terms
!
              a_i=Lvec_d(d1,i) ! angular momentum vector
!
              eam(dd_index) = RRb1(i)*Int2100(dp_index)
              if(a_i.ne.0)then ! (a-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                eam(dd_index) = eam(dd_index) &
                                    + dble(a_i)*RRb2*Int1100(pp_index)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            if(d1.gt.3) eam(dd_index) = sq3*eam(dd_index)
            if(d2.gt.3) eam(dd_index) = sq3*eam(dd_index)
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [dsds]
!------------------------------------------------------------------
      if(Lcode.eq.2020)then
!
! Requires 5 steps
!
        allocate(Int1000(3),Int2000(6),Int1010(9))
        allocate(Int2010(18))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2:
! [1010] from [1000] and [0000]
! c+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int1010(pp_index) = RRc1(i)*Int1000(p1)
            if(p1.eq.i)then
              Int1010(pp_index) = Int1010(pp_index) &
                                + RRc2*G
            end if
          end do ! i
        end do ! p1
!
! Step 3 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int2000(d_index) = RRa1(i)*Int1000(p1) &
                           + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int2000(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! Step 4:
! [2010] from [2000] and [1000]
! c+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int2010(dp_index) = RRc1(i)*Int2000(d1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              Int2010(dp_index) = Int2010(dp_index) &
                                + dble(a_i)*RRc2*Int1000(p1)
            end if
          end do ! i
        end do ! d1
!
! Step 5:
! [2020] from [2010], [2000] and [1010]
! c+1i
!
        dd_index=0
        do d1=1,6
          do p2=1,3
            i=p2 ! do xy, yy, zz first
            dd_index=dd_index+1
            dp_index=3*(d1-1)+p2
! 
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            eam(dd_index) = RRc1(i)*Int2010(dp_index) &
                                + RRc4*Int2000(d1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              eam(dd_index) = eam(dd_index) &
                                  + dble(a_i)*RRc2*Int1010(pp_index)
            end if
          end do ! p2
!
          do p2=2,3 ! y, z
            dp_index=3*(d1-1)+p2
            do i=1,p2-1 ! x, y
              dd_index=dd_index+1
!
! i ne p2, therefore no (c-1i) terms
!
              a_i=Lvec_d(d1,i) ! angular momentum vector
!
              eam(dd_index) = RRc1(i)*Int2010(dp_index)
              if(a_i.ne.0)then ! (a-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                eam(dd_index) = eam(dd_index) &
                                    + dble(a_i)*RRc2*Int1010(pp_index)
              end if
            end do ! i
          end do ! p2
!
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            if(d1.gt.3) eam(dd_index) = sq3*eam(dd_index)
            if(d2.gt.3) eam(dd_index) = sq3*eam(dd_index)
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [sdds]
!------------------------------------------------------------------
      if(Lcode.eq.220)then
!
! Requires 5 steps
!
        allocate(Int0100(3),Int0200(6),Int0110(9))
        allocate(Int0210(18))
!
! Step 1 :
! [0100] from [0000]
! b+1i
!
        do i=1,3 ! x,y,z
          Int0100(i) = RRb1(i)*G
        end do ! i 
!
! Step 2:
! [0110] from [0100] and [0000]
! c+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int0110(pp_index) = RRc1(i)*Int0100(p1)
            if(p1.eq.i)then
              Int0110(pp_index) = Int0110(pp_index) &
                                + RRc3*G
            end if
          end do ! i
        end do ! p1
!
! Step 3 :
! [0200] from [0100] and [0000]
! b+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int0200(d_index) = RRb1(i)*Int0100(p1) &
                           + RRb3*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int0200(d_index) = RRb1(i)*Int0100(p1)
          end do ! i
        end do ! p1
!
! Step 4:
! [0210] from [0200] and [0100]
! c+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            b_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int0210(dp_index) = RRc1(i)*Int0200(d1)
            if(b_i.ne.0)then ! (b-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              Int0210(dp_index) = Int0210(dp_index) &
                                + dble(b_i)*RRc3*Int0100(p1)
            end if
          end do ! i
        end do ! d1
!
! Step 5:
! [0220] from [0210], [0200] and [0110]
! c+1i
!
        dd_index=0
        do d1=1,6
          do p2=1,3
            i=p2 ! do xy, yy, zz first
            dd_index=dd_index+1
            dp_index=3*(d1-1)+p2
! 
            b_i=Lvec_d(d1,i) ! angular momentum vector
!
            eam(dd_index) = RRc1(i)*Int0210(dp_index) &
                                + RRc4*Int0200(d1)
            if(b_i.ne.0)then ! (b-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! b-1_i
              p1=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              eam(dd_index) = eam(dd_index) &
                                  + dble(b_i)*RRc3*Int0110(pp_index)
            end if
          end do ! p2
!
          do p2=2,3 ! y, z
            dp_index=3*(d1-1)+p2
            do i=1,p2-1 ! x, y
              dd_index=dd_index+1
!
! i ne p2, therefore no (c-1i) terms
!
              b_i=Lvec_d(d1,i) ! angular momentum vector
!
              eam(dd_index) = RRc1(i)*Int0210(dp_index)
              if(b_i.ne.0)then ! (b-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                eam(dd_index) = eam(dd_index) &
                                    + dble(b_i)*RRc3*Int0110(pp_index)
              end if
            end do ! i
          end do ! p2
!
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            if(d1.gt.3) eam(dd_index) = sq3*eam(dd_index)
            if(d2.gt.3) eam(dd_index) = sq3*eam(dd_index)
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [dssd]
!------------------------------------------------------------------
      if(Lcode.eq.2002)then
!
! Requires 5 steps
!
        allocate(Int1000(3),Int2000(6),Int1001(9))
        allocate(Int2001(18))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2:
! [1001] from [1000] and [0000]
! d+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int1001(pp_index) = RRd1(i)*Int1000(p1)
            if(p1.eq.i)then
              Int1001(pp_index) = Int1001(pp_index) &
                                + RRd2*G
            end if
          end do ! i
        end do ! p1
!
! Step 3 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int2000(d_index) = RRa1(i)*Int1000(p1) &
                           + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
              ! by design p1 ne i
            Int2000(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! Step 4:
! [2001] from [2000] and [1000]
! d+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int2001(dp_index) = RRd1(i)*Int2000(d1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              Int2001(dp_index) = Int2001(dp_index) &
                                + dble(a_i)*RRd2*Int1000(p1)
            end if
          end do ! i
        end do ! d1
!
! Step 5:
! [2002] from [2001], [2000] and [1001]
! d+1i
!
        dd_index=0
        do d1=1,6
          do p2=1,3
            i=p2 ! do xy, yy, zz first
            dd_index=dd_index+1
            dp_index=3*(d1-1)+p2
! 
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            eam(dd_index) = RRd1(i)*Int2001(dp_index) &
                                + RRd5*Int2000(d1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              eam(dd_index) = eam(dd_index) &
                                  + dble(a_i)*RRd2*Int1001(pp_index)
            end if
          end do ! p2
!
          do p2=2,3 ! y, z
            dp_index=3*(d1-1)+p2
            do i=1,p2-1 ! x, y
              dd_index=dd_index+1
!
! i ne p2, therefore no (d-1i) terms
!
              a_i=Lvec_d(d1,i) ! angular momentum vector
!
              eam(dd_index) = RRd1(i)*Int2001(dp_index)
              if(a_i.ne.0)then ! (a-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                eam(dd_index) = eam(dd_index) &
                                    + dble(a_i)*RRd2*Int1001(pp_index)
              end if
            end do ! i
          end do ! p2
!
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            if(d1.gt.3) eam(dd_index) = sq3*eam(dd_index)
            if(d2.gt.3) eam(dd_index) = sq3*eam(dd_index)
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [sdsd]
!------------------------------------------------------------------
      if(Lcode.eq.202)then
!
! Requires 5 steps
!
        allocate(Int0100(3),Int0200(6),Int0101(9))
        allocate(Int0201(18))
!
! Step 1 :
! [0100] from [0000]
! b+1i
!
        do i=1,3 ! x,y,z
          Int0100(i) = RRb1(i)*G
        end do ! i 
!
! Step 2:
! [0101] from [0100] and [0000]
! d+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int0101(pp_index) = RRd1(i)*Int0100(p1)
            if(p1.eq.i)then
              Int0101(pp_index) = Int0101(pp_index) &
                                + RRd3*G
            end if
          end do ! i
        end do ! p1
!
! Step 3 :
! [0200] from [0100] and [0000]
! b+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int0200(d_index) = RRb1(i)*Int0100(p1) &
                           + RRb3*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
              ! by design p1 ne i
            Int0200(d_index) = RRb1(i)*Int0100(p1)
          end do ! i
        end do ! p1
!
! Step 4:
! [0201] from [0200] and [1000]
! d+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            b_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int0201(dp_index) = RRd1(i)*Int0200(d1)
            if(b_i.ne.0)then ! (b-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! b-1_i
              p1=dot_product(v_1i,xyz)
              Int0201(dp_index) = Int0201(dp_index) &
                                + dble(b_i)*RRd3*Int0100(p1)
            end if
          end do ! i
        end do ! d1
!
! Step 5:
! [0202] from [0201], [0200] and [0101]
! d+1i
!
        dd_index=0
        do d1=1,6
          do p2=1,3
            i=p2 ! do xy, yy, zz first
            dd_index=dd_index+1
            dp_index=3*(d1-1)+p2
! 
            b_i=Lvec_d(d1,i) ! angular momentum vector
!
            eam(dd_index) = RRd1(i)*Int0201(dp_index) &
                                + RRd5*Int0200(d1)
            if(b_i.ne.0)then ! (b-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! b-1_i
              p1=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              eam(dd_index) = eam(dd_index) &
                                  + dble(b_i)*RRd3*Int0101(pp_index)
            end if
          end do ! p2
!
          do p2=2,3 ! y, z
            dp_index=3*(d1-1)+p2
            do i=1,p2-1 ! x, y
              dd_index=dd_index+1
!
! i ne p2, therefore no (d-1i) terms
!
              b_i=Lvec_d(d1,i) ! angular momentum vector
!
              eam(dd_index) = RRd1(i)*Int0201(dp_index)
              if(b_i.ne.0)then ! (b-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                eam(dd_index) = eam(dd_index) &
                                    + dble(b_i)*RRd3*Int0101(pp_index)
              end if
            end do ! i
          end do ! p2
!
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            if(d1.gt.3) eam(dd_index) = sq3*eam(dd_index)
            if(d2.gt.3) eam(dd_index) = sq3*eam(dd_index)
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [dpps]
!------------------------------------------------------------------
      if(Lcode.eq.2110)then
!
! Requires 5 steps
!
        allocate(Int1000(3),Int2000(6),Int1100(9))
        allocate(Int2100(18))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2:
! [1100] from [1000] and [0000]
! b+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int1100(pp_index) = RRb1(i)*Int1000(p1)
            if(p1.eq.i)then
              Int1100(pp_index) = Int1100(pp_index) &
                                + RRb2*G
            end if
          end do ! i
        end do ! p1
!
! Step 3 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int2000(d_index) = RRa1(i)*Int1000(p1) &
                           + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int2000(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! Step 4:
! [2100] from [2000] and [1000]
! b+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int2100(dp_index) = RRb1(i)*Int2000(d1)
            if(a_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int2100(dp_index) = Int2100(dp_index) &
                                + dble(a_i)*RRb2*Int1000(p1)
            end if
          end do ! i
        end do ! d1
!
! Step 5:
! [2110] from [2100], [2000] and [1100]
! c+1i
!
        dp_index=0
        dpp_index=0
        do d1=1,6
          do p2=1,3
            dp_index=dp_index+1
            do i=1,3
              dpp_index=dpp_index+1
!
              a_i=Lvec_d(d1,i)
!             
              eam(dpp_index) = RRc1(i)*Int2100(dp_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                eam(dpp_index) = eam(dpp_index) &
                                    + dble(a_i)*RRc2*Int1100(pp_index)
              end if
              if(i.eq.p2)then
                eam(dpp_index) = eam(dpp_index) &
                                     + RRc3*Int2000(d1)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        do d1=4,6
          do p2=1,3
            do p3=1,3
              dpp_index=9*(d1-1)+3*(p2-1)+p3
              eam(dpp_index) = sq3*eam(dpp_index)
            end do
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [dpsp]
!------------------------------------------------------------------
      if(Lcode.eq.2101)then
!
! Requires 5 steps
!
        allocate(Int1000(3),Int2000(6),Int1100(9))
        allocate(Int2100(18))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2:
! [1100] from [1000] and [0000]
! b+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int1100(pp_index) = RRb1(i)*Int1000(p1)
            if(p1.eq.i)then
              Int1100(pp_index) = Int1100(pp_index) &
                                + RRb2*G
            end if
          end do ! i
        end do ! p1
!
! Step 3 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int2000(d_index) = RRa1(i)*Int1000(p1) &
                           + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int2000(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! Step 4:
! [2100] from [2000] and [1000]
! b+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int2100(dp_index) = RRb1(i)*Int2000(d1)
            if(a_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int2100(dp_index) = Int2100(dp_index) &
                                + dble(a_i)*RRb2*Int1000(p1)
            end if
          end do ! i
        end do ! d1
!
! Step 5:
! [2101] from [2100], [2000] and [1100]
! d+1i
!
        dp_index=0
        dpp_index=0
        do d1=1,6
          do p2=1,3
            dp_index=dp_index+1
            do i=1,3
              dpp_index=dpp_index+1
!
              a_i=Lvec_d(d1,i)
!             
              eam(dpp_index) = RRd1(i)*Int2100(dp_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                eam(dpp_index) = eam(dpp_index) &
                                     + dble(a_i)*RRd2*Int1100(pp_index)
              end if
              if(i.eq.p2)then
                eam(dpp_index) = eam(dpp_index) &
                                     + RRd3*Int2000(d1)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        do d1=4,6
          do p2=1,3
            do p3=1,3
              dpp_index=9*(d1-1)+3*(p2-1)+p3
              eam(dpp_index) = sq3*eam(dpp_index)
            end do
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [dspp]
!------------------------------------------------------------------
      if(Lcode.eq.2011)then
!
! Requires 5 steps
!
        allocate(Int1000(3),Int2000(6),Int1010(9))
        allocate(Int2010(18))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2:
! [1010] from [1000] and [0000]
! c+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int1010(pp_index) = RRc1(i)*Int1000(p1)
            if(i.eq.p1)then
              Int1010(pp_index) = Int1010(pp_index) &
                                + RRc2*G
            end if
          end do ! i
        end do ! p1
!
! Step 3 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int2000(d_index) = RRa1(i)*Int1000(p1) &
                           + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int2000(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! Step 4:
! [2010] from [2000] and [1000]
! c+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int2010(dp_index) = RRc1(i)*Int2000(d1)
            if(a_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int2010(dp_index) = Int2010(dp_index) &
                                + dble(a_i)*RRc2*Int1000(p1)
            end if
          end do ! i
        end do ! d1
!
! Step 5:
! [2011] from [2010], [2000] and [1010]
! d+1i
!
        dp_index=0
        dpp_index=0
        do d1=1,6
          do p2=1,3
            dp_index=dp_index+1
            do i=1,3
              dpp_index=dpp_index+1
!
              a_i=Lvec_d(d1,i)
!             
              eam(dpp_index) = RRd1(i)*Int2010(dp_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                eam(dpp_index) = eam(dpp_index) &
                                     + dble(a_i)*RRd2*Int1010(pp_index)
              end if
              if(i.eq.p2)then
                eam(dpp_index) = eam(dpp_index) &
                                     + RRd4*Int2000(d1)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        do d1=4,6
          do p2=1,3
            do p3=1,3
              dpp_index=9*(d1-1)+3*(p2-1)+p3
              eam(dpp_index) = sq3*eam(dpp_index)
            end do
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [sdpp]
!------------------------------------------------------------------
      if(Lcode.eq.211)then
!
! Requires 5 steps
!
        allocate(Int0100(3),Int0200(6),Int0110(9))
        allocate(Int0210(18))
!
! Step 1 :
! [0100] from [0000]
! b+1i
!
        do i=1,3 ! x,y,z
          Int0100(i) = RRb1(i)*G
        end do ! i 
!
! Step 2:
! [0110] from [0100] and [0000]
! c+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int0110(pp_index) = RRc1(i)*Int0100(p1)
            if(i.eq.p1)then
              Int0110(pp_index) = Int0110(pp_index) &
                                + RRc3*G
            end if
          end do ! i
        end do ! p1
!
! Step 3 :
! [0200] from [0100] and [0000]
! b+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int0200(d_index) = RRb1(i)*Int0100(p1) &
                           + RRb3*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int0200(d_index) = RRb1(i)*Int0100(p1)
          end do ! i
        end do ! p1
!
! Step 4:
! [0210] from [0200] and [0100]
! c+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            b_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int0210(dp_index) = RRc1(i)*Int0200(d1)
            if(b_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int0210(dp_index) = Int0210(dp_index) &
                                + dble(b_i)*RRc3*Int0100(p1)
            end if
          end do ! i
        end do ! d1
!
! Step 5:
! [0211] from [0210], [0200] and [0110]
! d+1i
!
        dp_index=0
        dpp_index=0
        do d1=1,6
          do p2=1,3
            dp_index=dp_index+1
            do i=1,3
              dpp_index=dpp_index+1
!
              b_i=Lvec_d(d1,i)
!             
              eam(dpp_index) = RRd1(i)*Int0210(dp_index)
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                eam(dpp_index) = eam(dpp_index) &
                                     + dble(b_i)*RRd3*Int0110(pp_index)
              end if
              if(i.eq.p2)then
                eam(dpp_index) = eam(dpp_index) &
                                     + RRd4*Int0200(d1)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        do d1=4,6
          do p2=1,3
            do p3=1,3
              dpp_index=9*(d1-1)+3*(p2-1)+p3
              eam(dpp_index) = sq3*eam(dpp_index)
            end do
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [pdsp]
!------------------------------------------------------------------
      if(Lcode.eq.1201)then
!
! Requires 5 steps
!
        allocate(Int0100(3),Int0200(6),Int1100(9))
        allocate(Int1200(18))
!
! Step 1 :
! [0100] from [0000]
! b+1i
!
        do i=1,3 ! x,y,z
          Int0100(i) = RRb1(i)*G
        end do ! i 
!
! Step 2:
! [1100] from [0100] and [0000]
! a+1i
!
        pp_index=0
        do i=1,3 ! x, y, z
          do p2=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int1100(pp_index) = RRa1(i)*Int0100(p2)
            if(p2.eq.i)then
              Int1100(pp_index) = Int1100(pp_index) &
                                + RRa3*G
            end if
          end do ! p2
        end do ! i
!
! Step 3 :
! [0200] from [0100] and [0000]
! b+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int0200(d_index) = RRb1(i)*Int0100(p1) &
                           + RRb3*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int0200(d_index) = RRb1(i)*Int0100(p1)
          end do ! i
        end do ! p1
!
! Step 4:
! [1200] from [0200] and [0100]
! a+1i
!
        pd_index=0 
        do i=1,3
          do d1=1,6
            pd_index=pd_index+1
!
            b_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int1200(pd_index) = RRa1(i)*Int0200(d1)
            if(b_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int1200(pd_index) = Int1200(pd_index) &
                                + dble(b_i)*RRa3*Int0100(p1)
            end if
          end do ! d1
        end do ! i 
!
! Step 5:
! [1201] from [1200], [0200] and [1100]
! d+1i
!
        pd_index=0
        pdp_index=0
        do p1=1,3
          do d2=1,6
            pd_index=pd_index+1
            do i=1,3
              pdp_index=pdp_index+1
!
              b_i=Lvec_d(d2,i)
!             
              eam(pdp_index) = RRd1(i)*Int1200(pd_index)
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1_i
                p2=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                eam(pdp_index) = eam(pdp_index) &
                                     + dble(b_i)*RRd3*Int1100(pp_index)
              end if
              if(i.eq.p1)then
                eam(pdp_index) = eam(pdp_index) &
                                     + RRd2*Int0200(d2)
              end if
            end do ! i
          end do ! d2
        end do ! p1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        do p1=1,3
          do d2=4,6
            do p3=1,3
              pdp_index=18*(p1-1)+3*(d2-1)+p3
              eam(pdp_index) = sq3*eam(pdp_index)
            end do
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [pdps]
!------------------------------------------------------------------
      if(Lcode.eq.1210)then
!
! Requires 5 steps
!
        allocate(Int0100(3),Int0200(6),Int1100(9))
        allocate(Int1200(18))
!
! Step 1 :
! [0100] from [0000]
! b+1i
!
        do i=1,3 ! x,y,z
          Int0100(i) = RRb1(i)*G
        end do ! i 
!
! Step 2:
! [1100] from [0100] and [0000]
! a+1i
!
        pp_index=0
        do i=1,3 ! x, y, z
          do p2=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int1100(pp_index) = RRa1(i)*Int0100(p2)
            if(p2.eq.i)then
              Int1100(pp_index) = Int1100(pp_index) &
                                + RRa3*G
            end if
          end do ! p2
        end do ! i
!
! Step 3 :
! [0200] from [0100] and [0000]
! b+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int0200(d_index) = RRb1(i)*Int0100(p1) &
                           + RRb3*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int0200(d_index) = RRb1(i)*Int0100(p1)
          end do ! i
        end do ! p1
!
! Step 4:
! [1200] from [0200] and [0100]
! a+1i
!
        pd_index=0 
        do i=1,3
          do d1=1,6
            pd_index=pd_index+1
!
            b_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int1200(pd_index) = RRa1(i)*Int0200(d1)
            if(b_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int1200(pd_index) = Int1200(pd_index) &
                                + dble(b_i)*RRa3*Int0100(p1)
            end if
          end do ! d1
        end do ! i 
!
! Step 5:
! [1210] from [1200], [0200] and [1100]
! c+1i
!
        pd_index=0
        pdp_index=0
        do p1=1,3
          do d2=1,6
            pd_index=pd_index+1
            do i=1,3
              pdp_index=pdp_index+1
!
              b_i=Lvec_d(d2,i)
!             
              eam(pdp_index) = RRc1(i)*Int1200(pd_index)
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1_i
                p2=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                eam(pdp_index) = eam(pdp_index) &
                                     + dble(b_i)*RRc3*Int1100(pp_index)
              end if
              if(i.eq.p1)then
                eam(pdp_index) = eam(pdp_index) &
                                     + RRc2*Int0200(d2)
              end if
            end do ! i
          end do ! d2
        end do ! p1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        do p1=1,3
          do d2=4,6
            do p3=1,3
              pdp_index=18*(p1-1)+3*(d2-1)+p3
              eam(pdp_index) = sq3*eam(pdp_index)
            end do
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [ppds]
!------------------------------------------------------------------
      if(Lcode.eq.1120)then
!
! Requires 5 steps
!
        allocate(Int0010(3),Int0020(6),Int1010(9))
        allocate(Int1020(18))
!
! Step 1 :
! [0010] from [0000]
! c+1i
!
        do i=1,3 ! x,y,z
          Int0010(i) = RRc1(i)*G
        end do ! i 
!
! Step 2:
! [1010] from [0010] and [0000]
! a+1i
!
        pp_index=0
        do i=1,3 ! x, y, z
          do p2=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int1010(pp_index) = RRa1(i)*Int0010(p2)
            if(p2.eq.i)then
              Int1010(pp_index) = Int1010(pp_index) &
                                + RRa4*G
            end if
          end do ! p2
        end do ! i
!
! Step 3 :
! [0020] from [0010] and [0000]
! c+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int0020(d_index) = RRc1(i)*Int0010(p1) &
                           + RRc4*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int0020(d_index) = RRc1(i)*Int0010(p1)
          end do ! i
        end do ! p1
!
! Step 4:
! [1020] from [0020] and [0010]
! a+1i
!
        pd_index=0 
        do i=1,3
          do d1=1,6
            pd_index=pd_index+1
!
            c_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int1020(pd_index) = RRa1(i)*Int0020(d1)
            if(c_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int1020(pd_index) = Int1020(pd_index) &
                                + dble(c_i)*RRa4*Int0010(p1)
            end if
          end do ! d1
        end do ! i 
!
! Step 5:
! [1120] from [1020], [0020] and [1010]
! b+1i
!
        pd_index=0
        do p1=1,3
          do d2=1,6
            pd_index=pd_index+1
            do i=1,3
              ppd_index=18*(p1-1) + 6*(i-1) + d2
!
              c_i=Lvec_d(d2,i)
!             
              eam(ppd_index) = RRb1(i)*Int1020(pd_index)
              if(c_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1_i
                p2=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                eam(ppd_index) = eam(ppd_index) &
                                     + dble(c_i)*RRb4*Int1010(pp_index)
              end if
              if(i.eq.p1)then
                eam(ppd_index) = eam(ppd_index) &
                                     + RRb2*Int0020(d2)
              end if
            end do ! i
          end do ! d2
        end do ! p1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        do p1=1,3
          do p2=1,3
            do d3=4,6
              ppd_index=18*(p1-1)+6*(p2-1)+d3
              eam(ppd_index) = sq3*eam(ppd_index)
            end do
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [spdp]
!------------------------------------------------------------------
      if(Lcode.eq.121)then
!
! Requires 5 steps
!
        allocate(Int0010(3),Int0020(6),Int0011(9))
        allocate(Int0021(18))
!
! Step 1 :
! [0010] from [0000]
! c+1i
!
        do i=1,3 ! x,y,z
          Int0010(i) = RRc1(i)*G
        end do ! i 
!
! Step 2:
! [0011] from [0010] and [0000]
! d+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int0011(pp_index) = RRd1(i)*Int0010(p1)
            if(p1.eq.i)then
              Int0011(pp_index) = Int0011(pp_index) &
                                + RRd4*G
            end if
          end do ! p2
        end do ! i
!
! Step 3 :
! [0020] from [0010] and [0000]
! c+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int0020(d_index) = RRc1(i)*Int0010(p1) &
                           + RRc4*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int0020(d_index) = RRc1(i)*Int0010(p1)
          end do ! i
        end do ! p1
!
! Step 4:
! [0021] from [0020] and [0010]
! d+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            c_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int0021(dp_index) = RRd1(i)*Int0020(d1)
            if(c_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int0021(dp_index) = Int0021(dp_index) &
                                + dble(c_i)*RRd4*Int0010(p1)
            end if
          end do ! d1
        end do ! i 
!
! Step 5:
! [0121] from [0021], [0020] and [0011]
! b+1i
!
        dp_index=0
        do d1=1,6
          do p2=1,3
            dp_index=dp_index+1
            do i=1,3
              pdp_index=18*(i-1) + 3*(d1-1) + p2
!
              c_i=Lvec_d(d1,i)
!             
              eam(pdp_index) = RRb1(i)*Int0021(dp_index)
              if(c_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                eam(pdp_index) = eam(pdp_index) &
                                     + dble(c_i)*RRb4*Int0011(pp_index)
              end if
              if(i.eq.p2)then
                eam(pdp_index) = eam(pdp_index) &
                                     + RRb5*Int0020(d1)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        do p1=1,3
          do d2=4,6
            do p3=1,3
              pdp_index=18*(p1-1)+3*(d2-1)+p3
              eam(pdp_index) = sq3*eam(pdp_index)
            end do
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [ddps]
!------------------------------------------------------------------
      if(Lcode.eq.2210)then
!
! Requires 8 steps
!
        allocate(Int1000(3),Int2000(6),Int1100(9))
        allocate(Int2100(18),Int1200(18),Int2200(36))
        allocate(Int0100(3))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2 :
! [0100] from [0000]
! b+1i
!
        do i=1,3 ! x,y,z
          Int0100(i) = RRb1(i)*G
        end do ! i 
!
! Step 3:
! [1100] from [1000] and [0000]
! b+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int1100(pp_index) = RRb1(i)*Int1000(p1)
            if(p1.eq.i)then
              Int1100(pp_index) = Int1100(pp_index) &
                                + RRb2*G
            end if
          end do ! i
        end do ! p1
!
! Step 4 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int2000(d_index) = RRa1(i)*Int1000(p1) &
                           + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int2000(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! Step 5:
! [1200] from [1100], [1000] and [0100]
! b+1i
!
        pd_index=0 
        do p1=1,3
          do p2=1,3
            i=p2
            pd_index=pd_index+1
            pp_index=3*(p1-1)+p2
!
            Int1200(pd_index) = RRb1(i)*Int1100(pp_index) &
                              + RRb3*Int1000(p1)
            if(i.eq.p1)then
              Int1200(pd_index) = Int1200(pd_index) &
                                + RRb2*Int0100(p2)
            end if
          end do ! p2
          do p2=2,3
            pp_index=3*(p1-1)+p2
            do i=1,p2-1
              pd_index=pd_index+1
!
              Int1200(pd_index) = RRb1(i)*Int1100(pp_index)
              if(i.eq.p1)then
                Int1200(pd_index) = Int1200(pd_index) &
                                  + RRb2*Int0100(p2)
              end if
            end do ! i
          end do ! p2
        end do ! p1
!
! Step 6:
! [2100] from [2000] and [1000]
! b+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int2100(dp_index) = RRb1(i)*Int2000(d1)
            if(a_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int2100(dp_index) = Int2100(dp_index) &
                                + dble(a_i)*RRb2*Int1000(p1)
            end if
          end do ! i
        end do ! d1
!
! Step 7:
! [2200] from [2100], [2000] and [1100]
! b+1i
!
        dd_index=0
        do d1=1,6
          do p2=1,3
            i=p2
            dp_index=3*(d1-1)+p2
            dd_index=dd_index+1
!
            a_i=Lvec_d(d1,i)
!
            Int2200(dd_index) = RRb1(i)*Int2100(dp_index) &
                              + RRb3*Int2000(d1)
            if(a_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1i
              p1=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              Int2200(dd_index) = Int2200(dd_index) &
                                + dble(a_i)*RRb2*Int1100(pp_index)
            end if
          end do ! p2
!
          do p2=2,3
            dp_index=3*(d1-1)+p2
            do i=1,p2-1
              dd_index=dd_index+1
!
              a_i=Lvec_d(d1,i)
!
              Int2200(dd_index) = RRb1(i)*Int2100(dp_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2200(dd_index) = Int2200(dd_index) &
                                  + dble(a_i)*RRb2*Int1100(pp_index)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 8:
! [2210] from [2200], [2100] and [1200]
! c+1i
!
        ddp_index=0
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do i=1,3
              ddp_index=ddp_index+1
!
              a_i=Lvec_d(d1,i)
              b_i=Lvec_d(d2,i)
!
              eam(ddp_index) = RRc1(i)*Int2200(dd_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pd_index=6*(p1-1)+d2
                eam(ddp_index) = eam(ddp_index) &
                                     + dble(a_i)*RRc2*Int1200(pd_index)
              end if
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p2=dot_product(v_1i,xyz)
                dp_index=3*(d1-1)+p2
                eam(ddp_index) = eam(ddp_index) &
                                     + dble(b_i)*RRc3*Int2100(dp_index)
              end if
            end do ! i
          end do ! d2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        ddp_index=0
        do d1=1,6
          do d2=1,6
            do p3=1,3
              ddp_index=ddp_index+1
              if(d1.gt.3) eam(ddp_index) = sq3*eam(ddp_index)
              if(d2.gt.3) eam(ddp_index) = sq3*eam(ddp_index)
            end do
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [ddsp]
!------------------------------------------------------------------
      if(Lcode.eq.2201)then
!
! Requires 8 steps
!
        allocate(Int1000(3),Int2000(6),Int1100(9))
        allocate(Int2100(18),Int1200(18),Int2200(36))
        allocate(Int0100(3))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2 :
! [0100] from [0000]
! b+1i
!
        do i=1,3 ! x,y,z
          Int0100(i) = RRb1(i)*G
        end do ! i 
!
! Step 3:
! [1100] from [1000] and [0000]
! b+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int1100(pp_index) = RRb1(i)*Int1000(p1)
            if(p1.eq.i)then
              Int1100(pp_index) = Int1100(pp_index) &
                                + RRb2*G
            end if
          end do ! i
        end do ! p1
!
! Step 4 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int2000(d_index) = RRa1(i)*Int1000(p1) &
                           + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int2000(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! Step 5:
! [1200] from [1100], [1000] and [0100]
! b+1i
!
        pd_index=0 
        do p1=1,3
          do p2=1,3
            i=p2
            pd_index=pd_index+1
            pp_index=3*(p1-1)+p2
!
            Int1200(pd_index) = RRb1(i)*Int1100(pp_index) &
                              + RRb3*Int1000(p1)
            if(i.eq.p1)then
              Int1200(pd_index) = Int1200(pd_index) &
                                + RRb2*Int0100(p2)
            end if
          end do ! p2
          do p2=2,3
            pp_index=3*(p1-1)+p2
            do i=1,p2-1
              pd_index=pd_index+1
!
              Int1200(pd_index) = RRb1(i)*Int1100(pp_index)
              if(i.eq.p1)then
                Int1200(pd_index) = Int1200(pd_index) &
                                  + RRb2*Int0100(p2)
              end if
            end do ! i
          end do ! p2
        end do ! p1
!
! Step 6:
! [2100] from [2000] and [1000]
! b+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int2100(dp_index) = RRb1(i)*Int2000(d1)
            if(a_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int2100(dp_index) = Int2100(dp_index) &
                                + dble(a_i)*RRb2*Int1000(p1)
            end if
          end do ! i
        end do ! d1
!
! Step 7:
! [2200] from [2100], [2000] and [1100]
! b+1i
!
        dd_index=0
        do d1=1,6
          do p2=1,3
            i=p2
            dp_index=3*(d1-1)+p2
            dd_index=dd_index+1
!
            a_i=Lvec_d(d1,i)
!
            Int2200(dd_index) = RRb1(i)*Int2100(dp_index) &
                              + RRb3*Int2000(d1)
            if(a_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1i
              p1=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              Int2200(dd_index) = Int2200(dd_index) &
                                + dble(a_i)*RRb2*Int1100(pp_index)
            end if
          end do ! p2
!
          do p2=2,3
            dp_index=3*(d1-1)+p2
            do i=1,p2-1
              dd_index=dd_index+1
!
              a_i=Lvec_d(d1,i)
!
              Int2200(dd_index) = RRb1(i)*Int2100(dp_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2200(dd_index) = Int2200(dd_index) &
                                  + dble(a_i)*RRb2*Int1100(pp_index)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 8:
! [2201] from [2200], [2100] and [1200]
! d+1i
!
        ddp_index=0
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do i=1,3
              ddp_index=ddp_index+1
!
              a_i=Lvec_d(d1,i)
              b_i=Lvec_d(d2,i)
!
              eam(ddp_index) = RRd1(i)*Int2200(dd_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pd_index=6*(p1-1)+d2
                eam(ddp_index) = eam(ddp_index) &
                                     + dble(a_i)*RRd2*Int1200(pd_index)
              end if
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p2=dot_product(v_1i,xyz)
                dp_index=3*(d1-1)+p2
                eam(ddp_index) = eam(ddp_index) &
                                     + dble(b_i)*RRd3*Int2100(dp_index)
              end if
            end do ! i
          end do ! d2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        ddp_index=0
        do d1=1,6
          do d2=1,6
            do p3=1,3
              ddp_index=ddp_index+1
              if(d1.gt.3) eam(ddp_index) = sq3*eam(ddp_index)
              if(d2.gt.3) eam(ddp_index) = sq3*eam(ddp_index)
            end do
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [dpds]
!------------------------------------------------------------------
      if(Lcode.eq.2120)then
!
! Requires 8 steps
!
        allocate(Int1000(3),Int2000(6),Int1010(9))
        allocate(Int2010(18),Int1020(18),Int2020(36))
        allocate(Int0010(3))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2 :
! [0010] from [0000]
! c+1i
!
        do i=1,3 ! x,y,z
          Int0010(i) = RRc1(i)*G
        end do ! i 
!
! Step 3:
! [1010] from [1000] and [0000]
! c+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int1010(pp_index) = RRc1(i)*Int1000(p1)
            if(i.eq.p1)then
              Int1010(pp_index) = Int1010(pp_index) &
                                + RRc2*G
            end if
          end do ! i
        end do ! p1
!
! Step 4 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xy-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int2000(d_index) = RRa1(i)*Int1000(p1) &
                           + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int2000(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! Step 5:
! [1020] from [1010], [1000] and [0010]
! c+1i
!
        pd_index=0 
        do p1=1,3
          do p2=1,3
            i=p2
            pd_index=pd_index+1
            pp_index=3*(p1-1)+p2
!
            Int1020(pd_index) = RRc1(i)*Int1010(pp_index) &
                              + RRc4*Int1000(p1)
            if(i.eq.p1)then
              Int1020(pd_index) = Int1020(pd_index) &
                                + RRc2*Int0010(p2)
            end if
          end do ! p2
          do p2=2,3
            pp_index=3*(p1-1)+p2
            do i=1,p2-1
              pd_index=pd_index+1        
!
              Int1020(pd_index) = RRc1(i)*Int1010(pp_index)
              if(i.eq.p1)then
                Int1020(pd_index) = Int1020(pd_index) &
                                  + RRc2*Int0010(p2)
              end if
            end do ! i
          end do ! p2
        end do ! p1
!
! Step 6:
! [2010] from [2000] and [1000]
! c+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int2010(dp_index) = RRc1(i)*Int2000(d1)
            if(a_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int2010(dp_index) = Int2010(dp_index) &
                                + dble(a_i)*RRc2*Int1000(p1)
            end if
          end do ! i
        end do ! d1
!
! Step 7:
! [2020] from [2010], [2000] and [1010]
! c+1i
!
        dd_index=0
        do d1=1,6
          do p2=1,3
            i=p2 ! do xx, yy, zz first
            dd_index=dd_index+1
            dp_index=3*(d1-1)+p2
!
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int2020(dd_index) = RRc1(i)*Int2010(dp_index) &
                              + RRc4*Int2000(d1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              Int2020(dd_index) = Int2020(dd_index) &
                                + dble(a_i)*RRc2*Int1010(pp_index)
            end if
          end do ! p2
!
          do p2=2,3 ! y, z
            dp_index=3*(d1-1)+p2
            do i=1,p2-1 ! x, y
              dd_index=dd_index+1
!
! i ne p2, therefore no (b-1i) terms
!
              a_i=Lvec_d(d1,i) ! angular momentum vector
!
              Int2020(dd_index) = RRc1(i)*Int2010(dp_index)
              if(a_i.ne.0)then ! (a-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2020(dd_index) = Int2020(dd_index) &
                                  + dble(a_i)*RRc2*Int1010(pp_index)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 8:
! [2120] from [2020], [2010] and [1020]
! b+1i
!
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do i=1,3
              dpd_index=18*(d1-1)+6*(i-1)+d2
!
              a_i=Lvec_d(d1,i)
              c_i=Lvec_d(d2,i)
!
              eam(dpd_index) = RRb1(i)*Int2020(dd_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pd_index=6*(p1-1)+d2
                eam(dpd_index) = eam(dpd_index) &
                                     + dble(a_i)*RRb2*Int1020(pd_index)
              end if
              if(c_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! c-1i
                p2=dot_product(v_1i,xyz)
                dp_index=3*(d1-1)+p2
                eam(dpd_index) = eam(dpd_index) &
                                     + dble(c_i)*RRb4*Int2010(dp_index)
              end if
            end do ! i
          end do ! d2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xx, xz, yz
!
        dpd_index=0
        do d1=1,6
          do p2=1,3
            do d3=1,6
              dpd_index=dpd_index+1
              if(d1.gt.3) eam(dpd_index) = sq3*eam(dpd_index)
              if(d3.gt.3) eam(dpd_index) = sq3*eam(dpd_index)
            end do
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [pdds]
!------------------------------------------------------------------
      if(Lcode.eq.1220)then
!
! Requires 8 steps
!
        allocate(Int0100(3),Int0200(6),Int0110(9))
        allocate(Int0210(18),Int0120(18),Int0220(36))
        allocate(Int0010(3))
!
! Step 1 :
! [0100] from [0000]
! b+1i
!
        do i=1,3 ! x,y,z
          Int0100(i) = RRb1(i)*G
        end do ! i 
!
! Step 2 :
! [0010] from [0000]
! c+1i
!
        do i=1,3 ! x,y,z
          Int0010(i) = RRc1(i)*G
        end do ! i 
!
! Step 3:
! [0110] from [0100] and [0000]
! c+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int0110(pp_index) = RRc1(i)*Int0100(p1)
            if(i.eq.p1)then
              Int0110(pp_index) = Int0110(pp_index) &
                                + RRc3*G
            end if
          end do ! i
        end do ! p1
!
! Step 4 :
! [0200] from [0100] and [0000]
! b+1i
!
! Note: d's are numbered xy-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int0200(d_index) = RRb1(i)*Int0100(p1) &
                           + RRb3*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int0200(d_index) = RRb1(i)*Int0100(p1)
          end do ! i
        end do ! p1
!
! Step 5:
! [0120] from [0110], [0100] and [0010]
! c+1i
!
        pd_index=0 
        do p1=1,3
          do p2=1,3
            i=p2
            pd_index=pd_index+1
            pp_index=3*(p1-1)+p2
!
            Int0120(pd_index) = RRc1(i)*Int0110(pp_index) &
                              + RRc4*Int0100(p1)
            if(i.eq.p1)then
              Int0120(pd_index) = Int0120(pd_index) &
                                + RRc3*Int0010(p2)
            end if
          end do ! p2
          do p2=2,3
            pp_index=3*(p1-1)+p2
            do i=1,p2-1
              pd_index=pd_index+1        
!
              Int0120(pd_index) = RRc1(i)*Int0110(pp_index)
              if(i.eq.p1)then
                Int0120(pd_index) = Int0120(pd_index) &
                                  + RRc3*Int0010(p2)
              end if
            end do ! i
          end do ! p2
        end do ! p1
!
! Step 6:
! [0210] from [0200] and [0100]
! c+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            b_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int0210(dp_index) = RRc1(i)*Int0200(d1)
            if(b_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int0210(dp_index) = Int0210(dp_index) &
                                + dble(b_i)*RRc3*Int0100(p1)
            end if
          end do ! i
        end do ! d1
!
! Step 7:
! [0220] from [0210], [0200] and [0110]
! c+1i
!
        dd_index=0
        do d1=1,6
          do p2=1,3
            i=p2 ! do xx, yy, zz first
            dd_index=dd_index+1
            dp_index=3*(d1-1)+p2
!
            b_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int0220(dd_index) = RRc1(i)*Int0210(dp_index) &
                              + RRc4*Int0200(d1)
            if(b_i.ne.0)then ! (b-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! b-1_i
              p1=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              Int0220(dd_index) = Int0220(dd_index) &
                                + dble(b_i)*RRc3*Int0110(pp_index)
            end if
          end do ! p2
!
          do p2=2,3 ! y, z
            dp_index=3*(d1-1)+p2
            do i=1,p2-1 ! x, y
              dd_index=dd_index+1
!
! i ne p2, therefore no (b-1i) terms
!
              b_i=Lvec_d(d1,i) ! angular momentum vector
!
              Int0220(dd_index) = RRc1(i)*Int0210(dp_index)
              if(b_i.ne.0)then ! (b-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int0220(dd_index) = Int0220(dd_index) &
                                  + dble(b_i)*RRc3*Int0110(pp_index)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 8:
! [1220] from [0220], [0210] and [0120]
! a+1i
!
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do i=1,3
              pdd_index=36*(i-1)+6*(d1-1)+d2
!
              b_i=Lvec_d(d1,i)
              c_i=Lvec_d(d2,i)
!
              eam(pdd_index) = RRa1(i)*Int0220(dd_index)
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p1=dot_product(v_1i,xyz)
                pd_index=6*(p1-1)+d2
                eam(pdd_index) = eam(pdd_index) &
                                     + dble(b_i)*RRa3*Int0120(pd_index)
              end if
              if(c_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! c-1i
                p2=dot_product(v_1i,xyz)
                dp_index=3*(d1-1)+p2
                eam(pdd_index) = eam(pdd_index) &
                                     + dble(c_i)*RRa4*Int0210(dp_index)
              end if
            end do ! i
          end do ! d2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xx, xz, yz
!
        pdd_index=0
        do p1=1,3
          do d2=1,6
            do d3=1,6
              pdd_index=pdd_index+1
              if(d2.gt.3) eam(pdd_index) = sq3*eam(pdd_index)
              if(d3.gt.3) eam(pdd_index) = sq3*eam(pdd_index)
            end do
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [sddp]
!------------------------------------------------------------------
      if(Lcode.eq.221)then
!
! Requires 8 steps
!
        allocate(Int0100(3),Int0200(6),Int0110(9))
        allocate(Int0210(18),Int0120(18),Int0220(36))
        allocate(Int0010(3))
!
! Step 1 :
! [0100] from [0000]
! b+1i
!
        do i=1,3 ! x,y,z
          Int0100(i) = RRb1(i)*G
        end do ! i 
!
! Step 2 :
! [0010] from [0000]
! c+1i
!
        do i=1,3 ! x,y,z
          Int0010(i) = RRc1(i)*G
        end do ! i 
!
! Step 3:
! [0110] from [0100] and [0000]
! c+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int0110(pp_index) = RRc1(i)*Int0100(p1)
            if(i.eq.p1)then
              Int0110(pp_index) = Int0110(pp_index) &
                                + RRc3*G
            end if
          end do ! i
        end do ! p1
!
! Step 4 :
! [0200] from [0100] and [0000]
! b+1i
!
! Note: d's are numbered xy-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int0200(d_index) = RRb1(i)*Int0100(p1) &
                           + RRb3*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int0200(d_index) = RRb1(i)*Int0100(p1)
          end do ! i
        end do ! p1
!
! Step 5:
! [0120] from [0110], [0100] and [0010]
! c+1i
!
        pd_index=0 
        do p1=1,3
          do p2=1,3
            i=p2
            pd_index=pd_index+1
            pp_index=3*(p1-1)+p2
!
            Int0120(pd_index) = RRc1(i)*Int0110(pp_index) &
                              + RRc4*Int0100(p1)
            if(i.eq.p1)then
              Int0120(pd_index) = Int0120(pd_index) &
                                + RRc3*Int0010(p2)
            end if
          end do ! p2
          do p2=2,3
            pp_index=3*(p1-1)+p2
            do i=1,p2-1
              pd_index=pd_index+1        
!
              Int0120(pd_index) = RRc1(i)*Int0110(pp_index)
              if(i.eq.p1)then
                Int0120(pd_index) = Int0120(pd_index) &
                                  + RRc3*Int0010(p2)
              end if
            end do ! i
          end do ! p2
        end do ! p1
!
! Step 6:
! [0210] from [0200] and [0100]
! c+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            b_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int0210(dp_index) = RRc1(i)*Int0200(d1)
            if(b_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int0210(dp_index) = Int0210(dp_index) &
                                + dble(b_i)*RRc3*Int0100(p1)
            end if
          end do ! i
        end do ! d1
!
! Step 7:
! [0220] from [0210], [0200] and [0110]
! c+1i
!
        dd_index=0
        do d1=1,6
          do p2=1,3
            i=p2 ! do xx, yy, zz first
            dd_index=dd_index+1
            dp_index=3*(d1-1)+p2
!
            b_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int0220(dd_index) = RRc1(i)*Int0210(dp_index) &
                              + RRc4*Int0200(d1)
            if(b_i.ne.0)then ! (b-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! b-1_i
              p1=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              Int0220(dd_index) = Int0220(dd_index) &
                                + dble(b_i)*RRc3*Int0110(pp_index)
            end if
          end do ! p2
!
          do p2=2,3 ! y, z
            dp_index=3*(d1-1)+p2
            do i=1,p2-1 ! x, y
              dd_index=dd_index+1
!
! i ne p2, therefore no (b-1i) terms
!
              b_i=Lvec_d(d1,i) ! angular momentum vector
!
              Int0220(dd_index) = RRc1(i)*Int0210(dp_index)
              if(b_i.ne.0)then ! (b-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int0220(dd_index) = Int0220(dd_index) &
                                  + dble(b_i)*RRc3*Int0110(pp_index)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 8:
! [0221] from [0220], [0210] and [0120]
! d+1i
!
        dd_index=0
        ddp_index = 0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do i=1,3
              ddp_index=ddp_index+1
!
              b_i=Lvec_d(d1,i)
              c_i=Lvec_d(d2,i)
!
              eam(ddp_index) = RRd1(i)*Int0220(dd_index)
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p1=dot_product(v_1i,xyz)
                pd_index=6*(p1-1)+d2
                eam(ddp_index) = eam(ddp_index) &
                                     + dble(b_i)*RRd3*Int0120(pd_index)
              end if
              if(c_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! c-1i
                p2=dot_product(v_1i,xyz)
                dp_index=3*(d1-1)+p2
                eam(ddp_index) = eam(ddp_index) &
                                     + dble(c_i)*RRd4*Int0210(dp_index)
              end if
            end do ! i
          end do ! d2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xx, xz, yz
!
        ddp_index=0
        do d1=1,6
          do d2=1,6
            do p3=1,3
              ddp_index=ddp_index+1
              if(d1.gt.3) eam(ddp_index) = sq3*eam(ddp_index)
              if(d2.gt.3) eam(ddp_index) = sq3*eam(ddp_index)
            end do
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [dpsd]
!------------------------------------------------------------------
      if(Lcode.eq.2102)then
!
! Requires 8 steps
!
        allocate(Int1000(3),Int2000(6),Int1001(9))
        allocate(Int2001(18),Int1002(18),Int2002(36))
        allocate(Int0001(3))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2 :
! [0001] from [0000]
! d+1i
!
        do i=1,3 ! x,y,z
          Int0001(i) = RRd1(i)*G
        end do ! i 
!
! Step 3:
! [1001] from [1000] and [0000]
! d+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int1001(pp_index) = RRd1(i)*Int1000(p1)
            if(p1.eq.i)then
              Int1001(pp_index) = Int1001(pp_index) &
                                + RRd2*G
            end if
          end do ! i
        end do ! p1
!
! Step 4 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xy-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int2000(d_index) = RRa1(i)*Int1000(p1) &
                           + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int2000(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! Step 5:
! [1002] from [1001], [1000] and [0001]
! d+1i
!
        pd_index=0 
        do p1=1,3
          do p2=1,3
            i=p2
            pd_index=pd_index+1
            pp_index=3*(p1-1)+p2
!
            Int1002(pd_index) = RRd1(i)*Int1001(pp_index) &
                              + RRd5*Int1000(p1)
            if(i.eq.p1)then
              Int1002(pd_index) = Int1002(pd_index) &
                                + RRd2*Int0001(p2)
            end if
          end do ! p2
          do p2=2,3
            pp_index=3*(p1-1)+p2
            do i=1,p2-1
              pd_index=pd_index+1        
!
              Int1002(pd_index) = RRd1(i)*Int1001(pp_index)
              if(i.eq.p1)then
                Int1002(pd_index) = Int1002(pd_index) &
                                  + RRd2*Int0001(p2)
              end if
            end do ! i
          end do ! p2
        end do ! p1
!
! Step 6:
! [2001] from [2000] and [1000]
! d+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int2001(dp_index) = RRd1(i)*Int2000(d1)
!
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              Int2001(dp_index) = Int2001(dp_index) &
                                  + dble(a_i)*RRd2*Int1000(p1)
            end if
          end do ! i
        end do ! d1
!
! Step 7:
! [2002] from [2001], [2000] and [1001]
! d+1i
!
        dd_index=0
        do d1=1,6
          do p2=1,3
            i=p2 ! do xx, yy, zz first
            dd_index=dd_index+1
            dp_index=3*(d1-1)+p2
! 
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int2002(dd_index) = RRd1(i)*Int2001(dp_index) &
                              + RRd5*Int2000(d1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              Int2002(dd_index) = Int2002(dd_index) &
                                + dble(a_i)*RRd2*Int1001(pp_index)
            end if
          end do ! p2
!
          do p2=2,3 ! y, z
            dp_index=3*(d1-1)+p2
            do i=1,p2-1 ! x, y
              dd_index=dd_index+1
!
! i ne p2, therefore no (b-1i) terms
!
              a_i=Lvec_d(d1,i) ! angular momentum vector
!
              Int2002(dd_index) = RRd1(i)*Int2001(dp_index)
              if(a_i.ne.0)then ! (a-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2002(dd_index) = Int2002(dd_index) &
                                  + dble(a_i)*RRd2*Int1001(pp_index)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 8:
! [2102] from [2002], [2001] and [1002]
! b+1i
!
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do i=1,3
              dpd_index=18*(d1-1)+6*(i-1)+d2
!
              a_i=Lvec_d(d1,i)
              d_i=Lvec_d(d2,i)
!
              eam(dpd_index) = RRb1(i)*Int2002(dd_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pd_index=6*(p1-1)+d2
                eam(dpd_index) = eam(dpd_index) &
                                     + dble(a_i)*RRb2*Int1002(pd_index)
              end if
              if(d_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! c-1i
                p2=dot_product(v_1i,xyz)
                dp_index=3*(d1-1)+p2
                eam(dpd_index) = eam(dpd_index) &
                                    + dble(d_i)*RRb5*Int2001(dp_index)
              end if
            end do ! i
          end do ! d2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        dpd_index=0
        do d1=1,6
          do p2=1,3
            do d3=1,6
              dpd_index=dpd_index+1
              if(d1.gt.3) eam(dpd_index) = sq3*eam(dpd_index)
              if(d3.gt.3) eam(dpd_index) = sq3*eam(dpd_index)
            end do
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [dspd]
!------------------------------------------------------------------
      if(Lcode.eq.2012)then
!
! Requires 8 steps
!
        allocate(Int1000(3),Int2000(6),Int1001(9))
        allocate(Int2001(18),Int1002(18),Int2002(36))
        allocate(Int0001(3))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2 :
! [0001] from [0000]
! d+1i
!
        do i=1,3 ! x,y,z
          Int0001(i) = RRd1(i)*G
        end do ! i 
!
! Step 3:
! [1001] from [1000] and [0000]
! d+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int1001(pp_index) = RRd1(i)*Int1000(p1)
            if(p1.eq.i)then
              Int1001(pp_index) = Int1001(pp_index) &
                                + RRd2*G
            end if
          end do ! i
        end do ! p1
!
! Step 4 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xy-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int2000(d_index) = RRa1(i)*Int1000(p1) &
                           + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int2000(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! Step 5:
! [1002] from [1001], [1000] and [0001]
! c+1i
!
        pd_index=0 
        do p1=1,3
          do p2=1,3
            i=p2
            pd_index=pd_index+1
            pp_index=3*(p1-1)+p2
!
            Int1002(pd_index) = RRd1(i)*Int1001(pp_index) &
                              + RRd5*Int1000(p1)
            if(i.eq.p1)then
              Int1002(pd_index) = Int1002(pd_index) &
                                + RRd2*Int0001(p2)
            end if
          end do ! p2
          do p2=2,3
            pp_index=3*(p1-1)+p2
            do i=1,p2-1
              pd_index=pd_index+1        
!
              Int1002(pd_index) = RRd1(i)*Int1001(pp_index)
              if(i.eq.p1)then
                Int1002(pd_index) = Int1002(pd_index) &
                                  + RRd2*Int0001(p2)
              end if
            end do ! i
          end do ! p2
        end do ! p1
!
! Step 6:
! [2001] from [2000] and [1000]
! c+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int2001(dp_index) = RRd1(i)*Int2000(d1)
!
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              Int2001(dp_index) = Int2001(dp_index) &
                                  + dble(a_i)*RRd2*Int1000(p1)
            end if
          end do ! i
        end do ! d1
!
! Step 7:
! [2002] from [2001], [2000] and [1001]
! c+1i
!
        dd_index=0
        do d1=1,6
          do p2=1,3
            i=p2 ! do xx, yy, zz first
            dd_index=dd_index+1
            dp_index=3*(d1-1)+p2
! 
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int2002(dd_index) = RRd1(i)*Int2001(dp_index) &
                              + RRd5*Int2000(d1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              Int2002(dd_index) = Int2002(dd_index) &
                                + dble(a_i)*RRd2*Int1001(pp_index)
            end if
          end do ! p2
!
          do p2=2,3 ! y, z
            dp_index=3*(d1-1)+p2
            do i=1,p2-1 ! x, y
              dd_index=dd_index+1
!
! i ne p2, therefore no (b-1i) terms
!
              a_i=Lvec_d(d1,i) ! angular momentum vector
!
              Int2002(dd_index) = RRd1(i)*Int2001(dp_index)
              if(a_i.ne.0)then ! (a-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2002(dd_index) = Int2002(dd_index) &
                                  + dble(a_i)*RRd2*Int1001(pp_index)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 8:
! [2012] from [2002], [2001] and [1002]
! c+1i
!
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do i=1,3
              dpd_index=18*(d1-1)+6*(i-1)+d2
!
              a_i=Lvec_d(d1,i)
              d_i=Lvec_d(d2,i)
!
              eam(dpd_index) = RRc1(i)*Int2002(dd_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pd_index=6*(p1-1)+d2
                eam(dpd_index) = eam(dpd_index) &
                                     + dble(a_i)*RRc2*Int1002(pd_index)
              end if
              if(d_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! c-1i
                p2=dot_product(v_1i,xyz)
                dp_index=3*(d1-1)+p2
                eam(dpd_index) = eam(dpd_index) &
                                     + dble(d_i)*RRc5*Int2001(dp_index)
              end if
            end do ! i
          end do ! d2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        dpd_index=0
        do d1=1,6
          do p2=1,3
            do d3=1,6
              dpd_index=dpd_index+1
              if(d1.gt.3) eam(dpd_index) = sq3*eam(dpd_index)
              if(d3.gt.3) eam(dpd_index) = sq3*eam(dpd_index)
            end do
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [pdsd]
!------------------------------------------------------------------
      if(Lcode.eq.1202)then
!
! Requires 8 steps
!
        allocate(Int0100(3),Int0200(6),Int0101(9))
        allocate(Int0201(18),Int0102(18),Int0202(36))
        allocate(Int0001(3))
!
! Step 1 :
! [0100] from [0000]
! b+1i
!
        do i=1,3 ! x,y,z
          Int0100(i) = RRb1(i)*G
        end do ! i 
!
! Step 2 :
! [0001] from [0000]
! d+1i
!
        do i=1,3 ! x,y,z
          Int0001(i) = RRd1(i)*G
        end do ! i 
!
! Step 3:
! [0101] from [0100] and [0000]
! d+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int0101(pp_index) = RRd1(i)*Int0100(p1)
            if(p1.eq.i)then
              Int0101(pp_index) = Int0101(pp_index) &
                                + RRd3*G
            end if
          end do ! i
        end do ! p1
!
! Step 4 :
! [0200] from [0100] and [0000]
! b+1i
!
! Note: d's are numbered xy-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int0200(d_index) = RRb1(i)*Int0100(p1) &
                           + RRb3*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int0200(d_index) = RRb1(i)*Int0100(p1)
          end do ! i
        end do ! p1
!
! Step 5:
! [0102] from [0101], [0100] and [0001]
! d+1i
!
        pd_index=0 
        do p1=1,3
          do p2=1,3
            i=p2
            pd_index=pd_index+1
            pp_index=3*(p1-1)+p2
!
            Int0102(pd_index) = RRd1(i)*Int0101(pp_index) &
                              + RRd5*Int0100(p1)
            if(i.eq.p1)then
              Int0102(pd_index) = Int0102(pd_index) &
                                + RRd3*Int0001(p2)
            end if
          end do ! p2
          do p2=2,3
            pp_index=3*(p1-1)+p2
            do i=1,p2-1
              pd_index=pd_index+1        
!
              Int0102(pd_index) = RRd1(i)*Int0101(pp_index)
              if(i.eq.p1)then
                Int0102(pd_index) = Int0102(pd_index) &
                                  + RRd3*Int0001(p2)
              end if
            end do ! i
          end do ! p2
        end do ! p1
!
! Step 6:
! [0201] from [0200] and [0100]
! d+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            b_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int0201(dp_index) = RRd1(i)*Int0200(d1)
!
            if(b_i.ne.0)then ! (b-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! b-1_i
              p1=dot_product(v_1i,xyz)
              Int0201(dp_index) = Int0201(dp_index) &
                                + dble(b_i)*RRd3*Int0100(p1)
            end if
          end do ! i
        end do ! d1
!
! Step 7:
! [0202] from [0201], [0200] and [0101]
! d+1i
!
        dd_index=0
        do d1=1,6
          do p2=1,3
            i=p2 ! do xx, yy, zz first
            dd_index=dd_index+1
            dp_index=3*(d1-1)+p2
! 
            b_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int0202(dd_index) = RRd1(i)*Int0201(dp_index) &
                              + RRd5*Int0200(d1)
            if(b_i.ne.0)then ! (b-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! b-1_i
              p1=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              Int0202(dd_index) = Int0202(dd_index) &
                                + dble(b_i)*RRd3*Int0101(pp_index)
            end if
          end do ! p2
!
          do p2=2,3 ! y, z
            dp_index=3*(d1-1)+p2
            do i=1,p2-1 ! x, y
              dd_index=dd_index+1
!
! i ne p2, therefore no (b-1i) terms
!
              b_i=Lvec_d(d1,i) ! angular momentum vector
!
              Int0202(dd_index) = RRd1(i)*Int0201(dp_index)
              if(b_i.ne.0)then ! (b-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int0202(dd_index) = Int0202(dd_index) &
                                  + dble(b_i)*RRd3*Int0101(pp_index)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 8:
! [1202] from [0202], [0201] and [0102]
! a+1i
!
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do i=1,3
              pdd_index=36*(i-1)+6*(d1-1)+d2
!
              b_i=Lvec_d(d1,i)
              d_i=Lvec_d(d2,i)
!
              eam(pdd_index) = RRa1(i)*Int0202(dd_index)
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p1=dot_product(v_1i,xyz)
                pd_index=6*(p1-1)+d2
                eam(pdd_index) = eam(pdd_index) &
                                     + dble(b_i)*RRa3*Int0102(pd_index)
              end if
              if(d_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! d-1i
                p2=dot_product(v_1i,xyz)
                dp_index=3*(d1-1)+p2
                eam(pdd_index) = eam(pdd_index) &
                                     + dble(d_i)*RRa5*Int0201(dp_index)
              end if
            end do ! i
          end do ! d2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        pdd_index=0
        do p1=1,3
          do d2=1,6
            do d3=1,6
              pdd_index=pdd_index+1
              if(d2.gt.3) eam(pdd_index) = sq3*eam(pdd_index)
              if(d3.gt.3) eam(pdd_index) = sq3*eam(pdd_index)
            end do
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [sdpd]
!------------------------------------------------------------------
      if(Lcode.eq.212)then
!
! Requires 8 steps
!
        allocate(Int0100(3),Int0200(6),Int0101(9))
        allocate(Int0201(18),Int0102(18),Int0202(36))
        allocate(Int0001(3))
!
! Step 1 :
! [0100] from [0000]
! b+1i
!
        do i=1,3 ! x,y,z
          Int0100(i) = RRb1(i)*G
        end do ! i 
!
! Step 2 :
! [0001] from [0000]
! d+1i
!
        do i=1,3 ! x,y,z
          Int0001(i) = RRd1(i)*G
        end do ! i 
!
! Step 3:
! [0101] from [0100] and [0000]
! d+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int0101(pp_index) = RRd1(i)*Int0100(p1)
            if(p1.eq.i)then
              Int0101(pp_index) = Int0101(pp_index) &
                                + RRd3*G
            end if
          end do ! i
        end do ! p1
!
! Step 4 :
! [0200] from [0100] and [0000]
! b+1i
!
! Note: d's are numbered xy-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int0200(d_index) = RRb1(i)*Int0100(p1) &
                           + RRb3*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int0200(d_index) = RRb1(i)*Int0100(p1)
          end do ! i
        end do ! p1
!
! Step 5:
! [0102] from [0101], [0100] and [0001]
! d+1i
!
        pd_index=0 
        do p1=1,3
          do p2=1,3
            i=p2
            pd_index=pd_index+1
            pp_index=3*(p1-1)+p2
!
            Int0102(pd_index) = RRd1(i)*Int0101(pp_index) &
                              + RRd5*Int0100(p1)
            if(i.eq.p1)then
              Int0102(pd_index) = Int0102(pd_index) &
                                + RRd3*Int0001(p2)
            end if
          end do ! p2
          do p2=2,3
            pp_index=3*(p1-1)+p2
            do i=1,p2-1
              pd_index=pd_index+1        
!
              Int0102(pd_index) = RRd1(i)*Int0101(pp_index)
              if(i.eq.p1)then
                Int0102(pd_index) = Int0102(pd_index) &
                                  + RRd3*Int0001(p2)
              end if
            end do ! i
          end do ! p2
        end do ! p1
!
! Step 6:
! [0201] from [0200] and [0100]
! d+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            b_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int0201(dp_index) = RRd1(i)*Int0200(d1)
!
            if(b_i.ne.0)then ! (b-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! b-1_i
              p1=dot_product(v_1i,xyz)
              Int0201(dp_index) = Int0201(dp_index) &
                                + dble(b_i)*RRd3*Int0100(p1)
            end if
          end do ! i
        end do ! d1
!
! Step 7:
! [0202] from [0201], [0200] and [0101]
! d+1i
!
        dd_index=0
        do d1=1,6
          do p2=1,3
            i=p2 ! do xx, yy, zz first
            dd_index=dd_index+1
            dp_index=3*(d1-1)+p2
! 
            b_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int0202(dd_index) = RRd1(i)*Int0201(dp_index) &
                              + RRd5*Int0200(d1)
            if(b_i.ne.0)then ! (b-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! b-1_i
              p1=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              Int0202(dd_index) = Int0202(dd_index) &
                                + dble(b_i)*RRd3*Int0101(pp_index)
            end if
          end do ! p2
!
          do p2=2,3 ! y, z
            dp_index=3*(d1-1)+p2
            do i=1,p2-1 ! x, y
              dd_index=dd_index+1
!
! i ne p2, therefore no (b-1i) terms
!
              b_i=Lvec_d(d1,i) ! angular momentum vector
!
              Int0202(dd_index) = RRd1(i)*Int0201(dp_index)
              if(b_i.ne.0)then ! (b-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int0202(dd_index) = Int0202(dd_index) &
                                  + dble(b_i)*RRd3*Int0101(pp_index)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 8:
! [0212] from [0202], [0201] and [0102]
! c+1i
!
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do i=1,3
              dpd_index=18*(d1-1)+6*(i-1)+d2
!
              b_i=Lvec_d(d1,i)
              d_i=Lvec_d(d2,i)
!
              eam(dpd_index) = RRc1(i)*Int0202(dd_index)
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p1=dot_product(v_1i,xyz)
                pd_index=6*(p1-1)+d2
                eam(dpd_index) = eam(dpd_index) &
                                     + dble(b_i)*RRc3*Int0102(pd_index)
              end if
              if(d_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! d-1i
                p2=dot_product(v_1i,xyz)
                dp_index=3*(d1-1)+p2
                eam(dpd_index) = eam(dpd_index) &
                                     + dble(d_i)*RRc5*Int0201(dp_index)
              end if
            end do ! i
          end do ! d2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        dpd_index=0
        do d1=1,6
          do p2=1,3
            do d3=1,6
              dpd_index=dpd_index+1
              if(d1.gt.3) eam(dpd_index) = sq3*eam(dpd_index)
              if(d3.gt.3) eam(dpd_index) = sq3*eam(dpd_index)
            end do
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [ddds]
!------------------------------------------------------------------
      if(Lcode.eq.2220)then
!
! Requires 12 steps
!
        allocate(Int1000(3),Int2000(6),Int1100(9))
        allocate(Int2100(18),Int1200(18),Int2200(36))
        allocate(Int0100(3),Int0200(6),Int1210(54))
        allocate(Int2110(54),Int2210(108))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2 :
! [0100] from [0000]
! b+1i
!
        do i=1,3 ! x,y,z
          Int0100(i) = RRb1(i)*G
        end do ! i 
!
! Step 3 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int2000(d_index) = RRa1(i)*Int1000(p1) &
                           + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int2000(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! Step 4 :
! [0200] from [0100] and [0000]
! b+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int0200(d_index) = RRb1(i)*Int0100(p1) &
                           + RRb3*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int0200(d_index) = RRb1(i)*Int0100(p1)
          end do ! i
        end do ! p1
!
! Step 5:
! [1100] from [1000] and [0000]
! b+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int1100(pp_index) = RRb1(i)*Int1000(p1)
            if(p1.eq.i)then
              Int1100(pp_index) = Int1100(pp_index) &
                                + RRb2*G
            end if
          end do ! i
        end do ! p1
!
! Step 6:
! [1200] from [1100], [1000] and [0100]
! b+1i
!
        pd_index=0
        do p1=1,3
          do p2=1,3
            i=p2
            pd_index=pd_index+1
            pp_index=3*(p1-1)+p2
!
            Int1200(pd_index) = RRb1(i)*Int1100(pp_index) &
                              + RRb3*Int1000(p1)
            if(i.eq.p1)then
              Int1200(pd_index) = Int1200(pd_index) &
                                + RRb2*Int0100(p2)
            end if
          end do ! p2
!
          do p2=2,3
            pp_index=3*(p1-1)+p2
            do i=1,p2-1
              pd_index=pd_index+1
!
              Int1200(pd_index) = RRb1(i)*Int1100(pp_index)
              if(i.eq.p1)then
                Int1200(pd_index) = Int1200(pd_index) &
                                  + RRb2*Int0100(p2)
              end if
            end do ! i 
          end do ! p2
        end do ! p1
!
! Step 7:
! [2100] from [2000] and [1000]
! b+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int2100(dp_index) = RRb1(i)*Int2000(d1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              Int2100(dp_index) = Int2100(dp_index) &
                                + dble(a_i)*RRb2*Int1000(p1)
            end if
          end do ! i
        end do ! d1
!
! Step 8:
! [1210] from [1200], [1100] and [0200]
! c+1i
!
        pd_index=0
        pdp_index=0
        do p1=1,3
          do d2=1,6
            pd_index=pd_index+1
            do i=1,3
              pdp_index=pdp_index+1
!
              b_i=Lvec_d(d2,i)
!
              Int1210(pdp_index) = RRc1(i)*Int1200(pd_index)
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p2=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int1210(pdp_index) = Int1210(pdp_index) &
                                   + dble(b_i)*RRc3*Int1100(pp_index)
              end if
              if(i.eq.p1)then
                Int1210(pdp_index) = Int1210(pdp_index) &
                                   + RRc2*Int0200(d2)
              end if
            end do ! i
          end do ! d2
        end do ! p1
!
! Step 9:
! [2110] from [2100], [2000] and [1100]
! c+1i
!
        dp_index=0
        dpp_index=0
        do d1=1,6
          do p2=1,3
            dp_index=dp_index+1
            do i=1,3
              dpp_index=dpp_index+1
!
              a_i=Lvec_d(d1,i)
!
              Int2110(dpp_index) = RRc1(i)*Int2100(dp_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2110(dpp_index) = Int2110(dpp_index) &
                                   + dble(a_i)*RRc2*Int1100(pp_index)
              end if
              if(i.eq.p2)then
                Int2110(dpp_index) = Int2110(dpp_index) &
                                   + RRc3*Int2000(d1)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 10:
! [2200] from [2100], [2000] and [1100]
! b+1i
!
        dd_index=0
        do d1=1,6
          do p2=1,3
            i=p2
            dp_index=3*(d1-1)+p2
            dd_index=dd_index+1
!
            a_i=Lvec_d(d1,i)
!
            Int2200(dd_index) = RRb1(i)*Int2100(dp_index) &
                              + RRb3*Int2000(d1)
            if(a_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1i
              p1=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              Int2200(dd_index) = Int2200(dd_index) &
                                + dble(a_i)*RRb2*Int1100(pp_index)
            end if
          end do ! p2
!
          do p2=2,3
            dp_index=3*(d1-1)+p2
            do i=1,p2-1
              dd_index=dd_index+1
!
              a_i=Lvec_d(d1,i)
!
              Int2200(dd_index) = RRb1(i)*Int2100(dp_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2200(dd_index) = Int2200(dd_index) &
                                  + dble(a_i)*RRb2*Int1100(pp_index)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 11:
! [2210] from [2200], [2100] and [1200]
! c+1i
!
        ddp_index=0
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do i=1,3
              ddp_index=ddp_index+1
!
              a_i=Lvec_d(d1,i)
              b_i=Lvec_d(d2,i)
!
              Int2210(ddp_index) = RRc1(i)*Int2200(dd_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pd_index=6*(p1-1)+d2
                Int2210(ddp_index) = Int2210(ddp_index) &
                                   + dble(a_i)*RRc2*Int1200(pd_index)
              end if
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p2=dot_product(v_1i,xyz)
                dp_index=3*(d1-1)+p2
                Int2210(ddp_index) = Int2210(ddp_index) &
                                   + dble(b_i)*RRc3*Int2100(dp_index)
              end if
            end do ! i
          end do ! d2
        end do ! d1
!
! Step 12:
! [2220] from [2210], [2200], [2110] and [1210]
! c+1i
!   
        ddd_index=0
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do p3=1,3
              i=p3
              ddp_index=18*(d1-1)+3*(d2-1)+p3
              ddd_index=ddd_index+1
!
              a_i=Lvec_d(d1,i)
              b_i=Lvec_d(d2,i)
!
              eam(ddd_index) = RRc1(i)*Int2210(ddp_index) &
                                  + RRc4*Int2200(dd_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! (a-1i)
                p1=dot_product(v_1i,xyz)
                pdp_index=18*(p1-1)+3*(d2-1)+p3
                eam(ddd_index) = eam(ddd_index) &
                                    + dble(a_i)*RRc2*Int1210(pdp_index)
              end if
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1
                p2=dot_product(v_1i,xyz)
                dpp_index=9*(d1-1)+3*(p2-1)+p3
                eam(ddd_index) = eam(ddd_index) &
                                    + dble(b_i)*RRc3*Int2110(dpp_index)
              end if
            end do ! p3
            do p3=2,3
              ddp_index=18*(d1-1)+3*(d2-1)+p3
              do i=1,p3-1
                ddd_index=ddd_index+1
!
                a_i=Lvec_d(d1,i)
                b_i=Lvec_d(d2,i)
!
                eam(ddd_index) = RRc1(i)*Int2210(ddp_index)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! (a-1i)
                  p1=dot_product(v_1i,xyz)
                  pdp_index=18*(p1-1)+3*(d2-1)+p3
                  eam(ddd_index) = eam(ddd_index) &
                                      + dble(a_i)*RRc2*Int1210(pdp_index)
                end if
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1
                  p2=dot_product(v_1i,xyz)
                  dpp_index=9*(d1-1)+3*(p2-1)+p3
                  eam(ddd_index) = eam(ddd_index) &
                                      + dble(b_i)*RRc3*Int2110(dpp_index)
                end if
              end do ! i
            end do ! p3
          end do ! d2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        ddd_index=0
        do d1=1,6
          do d2=1,6
            do d3=1,6
              ddd_index=ddd_index+1
              if(d1.gt.3) eam(ddd_index) = sq3*eam(ddd_index)
              if(d2.gt.3) eam(ddd_index) = sq3*eam(ddd_index)
              if(d3.gt.3) eam(ddd_index) = sq3*eam(ddd_index)
            end do
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [ddsd]
!------------------------------------------------------------------
      if(Lcode.eq.2202)then
!
! Requires 12 steps
!
        allocate(Int1000(3),Int2000(6),Int1100(9))
        allocate(Int2100(18),Int1200(18),Int2200(36))
        allocate(Int0100(3),Int0200(6),Int1201(54))
        allocate(Int2101(54),Int2201(108))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2 :
! [0100] from [0000]
! b+1i
!
        do i=1,3 ! x,y,z
          Int0100(i) = RRb1(i)*G
        end do ! i 
!
! Step 3 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int2000(d_index) = RRa1(i)*Int1000(p1) &
                           + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int2000(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! Step 4 :
! [0200] from [0100] and [0000]
! b+1i
!
! Note: d's are numbered xy-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int0200(d_index) = RRb1(i)*Int0100(p1) &
                           + RRb3*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int0200(d_index) = RRb1(i)*Int0100(p1)
          end do ! i
        end do ! p1
!
! Step 5:
! [1100] from [1000] and [0000]
! b+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int1100(pp_index) = RRb1(i)*Int1000(p1)
            if(p1.eq.i)then
              Int1100(pp_index) = Int1100(pp_index) &
                                + RRb2*G
            end if
          end do ! i
        end do ! p1
!
! Step 6:
! [1200] from [1100], [1000] and [0100]
! b+1i
!
        pd_index=0
        do p1=1,3
          do p2=1,3
            i=p2
            pd_index=pd_index+1
            pp_index=3*(p1-1)+p2
!
            Int1200(pd_index) = RRb1(i)*Int1100(pp_index) &
                              + RRb3*Int1000(p1)
            if(i.eq.p1)then
              Int1200(pd_index) = Int1200(pd_index) &
                                + RRb2*Int0100(p2)
            end if
          end do ! p2
!
          do p2=2,3
            pp_index=3*(p1-1)+p2
            do i=1,p2-1
              pd_index=pd_index+1
!
              Int1200(pd_index) = RRb1(i)*Int1100(pp_index)
              if(i.eq.p1)then
                Int1200(pd_index) = Int1200(pd_index) &
                                  + RRb2*Int0100(p2)
              end if
            end do ! i 
          end do ! p2
        end do ! p1
!
! Step 7:
! [2100] from [2000] and [1000]
! b+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int2100(dp_index) = RRb1(i)*Int2000(d1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              Int2100(dp_index) = Int2100(dp_index) &
                                + dble(a_i)*RRb2*Int1000(p1)
            end if
          end do ! i
        end do ! d1
!
! Step 8:
! [1201] from [1200], [1100] and [0200]
! d+1i
!
        pd_index=0
        pdp_index=0
        do p1=1,3
          do d2=1,6
            pd_index=pd_index+1
            do i=1,3
              pdp_index=pdp_index+1
!
              b_i=Lvec_d(d2,i)
!
              Int1201(pdp_index) = RRd1(i)*Int1200(pd_index)
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p2=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int1201(pdp_index) = Int1201(pdp_index) &
                                   + dble(b_i)*RRd3*Int1100(pp_index)
              end if
              if(i.eq.p1)then
                Int1201(pdp_index) = Int1201(pdp_index) &
                                   + RRd2*Int0200(d2)
              end if
            end do ! i
          end do ! d2
        end do ! p1
!
! Step 9:
! [2101] from [2100], [2000] and [1100]
! d+1i
!
        dp_index=0
        dpp_index=0
        do d1=1,6
          do p2=1,3
            dp_index=dp_index+1
            do i=1,3
              dpp_index=dpp_index+1
!
              a_i=Lvec_d(d1,i)
!
              Int2101(dpp_index) = RRd1(i)*Int2100(dp_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2101(dpp_index) = Int2101(dpp_index) &
                                   + dble(a_i)*RRd2*Int1100(pp_index)
              end if
              if(i.eq.p2)then
                Int2101(dpp_index) = Int2101(dpp_index) &
                                   + RRd3*Int2000(d1)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 10:
! [2200] from [2100], [2000] and [1100]
! b+1i
!
        dd_index=0
        do d1=1,6
          do p2=1,3
            i=p2
            dp_index=3*(d1-1)+p2
            dd_index=dd_index+1
!
            a_i=Lvec_d(d1,i)
!
            Int2200(dd_index) = RRb1(i)*Int2100(dp_index) &
                              + RRb3*Int2000(d1)
            if(a_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1i
              p1=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              Int2200(dd_index) = Int2200(dd_index) &
                                + dble(a_i)*RRb2*Int1100(pp_index)
            end if
          end do ! p2
!
          do p2=2,3
            dp_index=3*(d1-1)+p2
            do i=1,p2-1
              dd_index=dd_index+1
!
              a_i=Lvec_d(d1,i)
!
              Int2200(dd_index) = RRb1(i)*Int2100(dp_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2200(dd_index) = Int2200(dd_index) &
                                  + dble(a_i)*RRb2*Int1100(pp_index)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 11:
! [2201] from [2200], [2100] and [1200]
! d+1i
!
        ddp_index=0
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do i=1,3
              ddp_index=ddp_index+1
!
              a_i=Lvec_d(d1,i)
              b_i=Lvec_d(d2,i)
!
              Int2201(ddp_index) = RRd1(i)*Int2200(dd_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pd_index=6*(p1-1)+d2
                Int2201(ddp_index) = Int2201(ddp_index) &
                                   + dble(a_i)*RRd2*Int1200(pd_index)
              end if
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p2=dot_product(v_1i,xyz)
                dp_index=3*(d1-1)+p2
                Int2201(ddp_index) = Int2201(ddp_index) &
                                   + dble(b_i)*RRd3*Int2100(dp_index)
              end if
            end do ! i
          end do ! d2
        end do ! d1
!
! Step 12:
! [2202] from [2201], [2200], [2101] and [1201]
! d+1i
!   
        ddd_index=0
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do p3=1,3
              i=p3
              ddp_index=18*(d1-1)+3*(d2-1)+p3
              ddd_index=ddd_index+1
!
              a_i=Lvec_d(d1,i)
              b_i=Lvec_d(d2,i)
!
              eam(ddd_index) = RRd1(i)*Int2201(ddp_index) &
                                  + RRd5*Int2200(dd_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! (a-1i)
                p1=dot_product(v_1i,xyz)
                pdp_index=18*(p1-1)+3*(d2-1)+p3
                eam(ddd_index) = eam(ddd_index) &
                                    + dble(a_i)*RRd2*Int1201(pdp_index)
              end if
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1
                p2=dot_product(v_1i,xyz)
                dpp_index=9*(d1-1)+3*(p2-1)+p3
                eam(ddd_index) = eam(ddd_index) &
                                    + dble(b_i)*RRd3*Int2101(dpp_index)
              end if
            end do ! p3
            do p3=2,3
              ddp_index=18*(d1-1)+3*(d2-1)+p3
              do i=1,p3-1
                ddd_index=ddd_index+1
!
                a_i=Lvec_d(d1,i)
                b_i=Lvec_d(d2,i)
!
                eam(ddd_index) = RRd1(i)*Int2201(ddp_index)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! (a-1i)
                  p1=dot_product(v_1i,xyz)
                  pdp_index=18*(p1-1)+3*(d2-1)+p3
                  eam(ddd_index) = eam(ddd_index) &
                                      + dble(a_i)*RRd2*Int1201(pdp_index)
                end if
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1
                  p2=dot_product(v_1i,xyz)
                  dpp_index=9*(d1-1)+3*(p2-1)+p3
                  eam(ddd_index) = eam(ddd_index) &
                                      + dble(b_i)*RRd3*Int2101(dpp_index)
                end if
              end do ! i
            end do ! p3
          end do ! d2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        ddd_index=0
        do d1=1,6
          do d2=1,6
            do d3=1,6
              ddd_index=ddd_index+1
              if(d1.gt.3) eam(ddd_index) = sq3*eam(ddd_index)
              if(d2.gt.3) eam(ddd_index) = sq3*eam(ddd_index)
              if(d3.gt.3) eam(ddd_index) = sq3*eam(ddd_index)
            end do
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [sddd]
!------------------------------------------------------------------
      if(Lcode.eq.222)then
!
! Requires 12 steps
!
        allocate(Int0010(3),Int0020(6),Int0110(9))
        allocate(Int0120(18),Int0210(18),Int0220(36))
        allocate(Int0100(3),Int0200(6),Int0211(54))
        allocate(Int0121(54),Int0221(108))
!
! Step 1 :
! [0010] from [0000]
! c+1i
!
        do i=1,3 ! x,y,z
          Int0010(i) = RRc1(i)*G
        end do ! i 
!
! Step 2 :
! [0100] from [0000]
! b+1i
!
        do i=1,3 ! x,y,z
          Int0100(i) = RRb1(i)*G
        end do ! i 
!
! Step 3 :
! [0020] from [0010] and [0000]
! c+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int0020(d_index) = RRc1(i)*Int0010(p1) &
                           + RRc4*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int0020(d_index) = RRc1(i)*Int0010(p1)
          end do ! i
        end do ! p1
!
! Step 4 :
! [0200] from [0100] and [0000]
! b+1i
!
! Note: d's are numbered xy-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int0200(d_index) = RRb1(i)*Int0100(p1) &
                           + RRb3*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int0200(d_index) = RRb1(i)*Int0100(p1)
          end do ! i
        end do ! p1
!
! Step 5:
! [0110] from [0010] and [0000]
! b+1i
!
        pp_index=0
        do i=1,3 ! x, y, z
          do p1=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int0110(pp_index) = RRb1(i)*Int0010(p1)
            if(p1.eq.i)then
              Int0110(pp_index) = Int0110(pp_index) &
                                + RRb4*G
            end if
          end do ! i
        end do ! p1
!
! Step 6:
! [0210] from [0200] and [0100]
! c+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            b_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int0210(dp_index) = RRc1(i)*Int0200(d1)
            if(b_i.ne.0)then ! (b-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! b-1_i
              p1=dot_product(v_1i,xyz)
              Int0210(dp_index) = Int0210(dp_index) &
                                + dble(b_i)*RRc3*Int0100(p1)
            end if
          end do ! i
        end do ! d1
!
! Step 7:
! [0120] from [0020] and [0010]
! b+1i
!
        pd_index=0 
        do i=1,3
          do d1=1,6
            pd_index=pd_index+1
!
            c_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int0120(pd_index) = RRb1(i)*Int0020(d1)
            if(c_i.ne.0)then ! (c-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! c-1_i
              p1=dot_product(v_1i,xyz)
              Int0120(pd_index) = Int0120(pd_index) &
                                + dble(c_i)*RRb4*Int0010(p1)
            end if
          end do ! i
        end do ! d1
!
! Step 8:
! [0211] from [0210], [0110] and [0200]
! d+1i
!
        dp_index=0
        dpp_index=0
        do d1=1,6
          do p2=1,3
            dp_index=dp_index+1
            do i=1,3
              dpp_index=dpp_index+1
!
              b_i=Lvec_d(d1,i)
!
              Int0211(dpp_index) = RRd1(i)*Int0210(dp_index)
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int0211(dpp_index) = Int0211(dpp_index) &
                                   + dble(b_i)*RRd3*Int0110(pp_index)
              end if
              if(i.eq.p2)then
                Int0211(dpp_index) = Int0211(dpp_index) &
                                   + RRd4*Int0200(d1)
              end if
            end do ! i
          end do ! d2
        end do ! p1
!
! Step 9:
! [0121] from [0120], [0020] and [0110]
! d+1i
!
        pd_index=0
        pdp_index=0
        do p1=1,3
          do d2=1,6
            pd_index=pd_index+1
            do i=1,3
              pdp_index=pdp_index+1
!
              c_i=Lvec_d(d2,i)
!
              Int0121(pdp_index) = RRd1(i)*Int0120(pd_index)
              if(c_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! c-1i
                p2=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int0121(pdp_index) = Int0121(pdp_index) &
                                   + dble(c_i)*RRd4*Int0110(pp_index)
              end if
              if(i.eq.p1)then
                Int0121(pdp_index) = Int0121(pdp_index) &
                                   + RRd3*Int0020(d2)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 10:
! [0220] from [0120], [0020] and [0110]
! b+1i
!
        do d2=1,6
          d1 = 0
          do p1=1,3
            i=p1
            d1 = d1 + 1
            pd_index=6*(p1-1)+d2
            dd_index=6*(d1-1)+d2
!
            c_i=Lvec_d(d2,i)
!
            Int0220(dd_index) = RRb1(i)*Int0120(pd_index) &
                              + RRb3*Int0020(d2)
            if(c_i.ne.0)then
              v_1i(1:3)=Lvec_d(d2,1:3)
              v_1i(i)=v_1i(i)-1 ! c-1i
              p2=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              Int0220(dd_index) = Int0220(dd_index) &
                                + dble(c_i)*RRb4*Int0110(pp_index)
            end if
          end do ! p1
!
          do p1=2,3
            pd_index=6*(p1-1)+d2
            do i=1,p1-1
              d1 = d1 + 1
              dd_index=6*(d1-1)+d2
!
              c_i=Lvec_d(d2,i)
!
              Int0220(dd_index) = RRb1(i)*Int0120(pd_index)
              if(c_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! c-1i
                p2=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int0220(dd_index) = Int0220(dd_index) &
                                  + dble(c_i)*RRb4*Int0110(pp_index)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 11:
! [0221] from [0220], [0120] and [0210]
! d+1i
!
        ddp_index=0
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do i=1,3
              ddp_index=ddp_index+1
!
              b_i=Lvec_d(d1,i)
              c_i=Lvec_d(d2,i)
!
              Int0221(ddp_index) = RRd1(i)*Int0220(dd_index)
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p1=dot_product(v_1i,xyz)
                pd_index=6*(p1-1)+d2
                Int0221(ddp_index) = Int0221(ddp_index) &
                                   + dble(b_i)*RRd3*Int0120(pd_index)
              end if
              if(c_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! c-1i
                p2=dot_product(v_1i,xyz)
                dp_index=3*(d1-1)+p2
                Int0221(ddp_index) = Int0221(ddp_index) &
                                   + dble(c_i)*RRd4*Int0210(dp_index)
              end if
            end do ! i
          end do ! d2
        end do ! d1
!
! Step 12:
! [0222] from [0221], [0220], [0121] and [0211]
! d+1i
!   
        ddd_index=0
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do p3=1,3
              i=p3
              ddp_index=18*(d1-1)+3*(d2-1)+p3
              ddd_index=ddd_index+1
!
              b_i=Lvec_d(d1,i)
              c_i=Lvec_d(d2,i)
!
              eam(ddd_index) = RRd1(i)*Int0221(ddp_index) &
                                   + RRd5*Int0220(dd_index)
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! (b-1i)
                p1=dot_product(v_1i,xyz)
                pdp_index=18*(p1-1)+3*(d2-1)+p3
                eam(ddd_index) = eam(ddd_index) &
                                     + dble(b_i)*RRd3*Int0121(pdp_index)
              end if
              if(c_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1
                p2=dot_product(v_1i,xyz)
                dpp_index=9*(d1-1)+3*(p2-1)+p3
                eam(ddd_index) = eam(ddd_index) &
                                     + dble(c_i)*RRd4*Int0211(dpp_index)
              end if
            end do ! p3
            do p3=2,3
              ddp_index=18*(d1-1)+3*(d2-1)+p3
              do i=1,p3-1
                ddd_index=ddd_index+1
!
                b_i=Lvec_d(d1,i)
                c_i=Lvec_d(d2,i)
!
                eam(ddd_index) = RRd1(i)*Int0221(ddp_index)
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! (b-1i)
                  p1=dot_product(v_1i,xyz)
                  pdp_index=18*(p1-1)+3*(d2-1)+p3
                  eam(ddd_index) = eam(ddd_index) &
                                       + dble(b_i)*RRd3*Int0121(pdp_index)
                end if
                if(c_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1
                  p2=dot_product(v_1i,xyz)
                  dpp_index=9*(d1-1)+3*(p2-1)+p3
                  eam(ddd_index) = eam(ddd_index) &
                                       + dble(c_i)*RRd4*Int0211(dpp_index)
                end if
              end do ! i
            end do ! p3
          end do ! d2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        ddd_index=0
        do d1=1,6
          do d2=1,6
            do d3=1,6
              ddd_index=ddd_index+1
              if(d1.gt.3) eam(ddd_index) = sq3*eam(ddd_index)
              if(d2.gt.3) eam(ddd_index) = sq3*eam(ddd_index)
              if(d3.gt.3) eam(ddd_index) = sq3*eam(ddd_index)
            end do
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [dppp]
!------------------------------------------------------------------
      if(Lcode.eq.2111)then
!
! Requires 9 steps
!
        allocate(Int1000(3),Int2000(6),Int1100(9))
        allocate(Int2100(18),Int2010(18),Int1110(27))
        allocate(Int0100(3))
        allocate(Int2110(54))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2 :
! [0100] from [0000]
! b+1i
!
        do i=1,3 ! x,y,z
          Int0100(i) = RRb1(i)*G
        end do ! i 
!
! Step 3:
! [1100] from [1000] and [0000]
! b+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int1100(pp_index) = RRb1(i)*Int1000(p1)
            if(p1.eq.i)then
              Int1100(pp_index) = Int1100(pp_index) &
                                + RRb2*G
            end if
          end do ! i
        end do ! p1
!
! Step 4 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int2000(d_index) = RRa1(i)*Int1000(p1) &
                           + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int2000(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! Step 5 :
! [1110] from [1100], [1000] and [0100]
! c+1i
!
        ppp_index=0
        pp_index=0
        do p1=1,3
          do p2=1,3
            pp_index=pp_index+1
            do i=1,3
              ppp_index=ppp_index+1
!
              Int1110(ppp_index) = RRc1(i)*Int1100(pp_index)
              if(i.eq.p1)then
                Int1110(ppp_index) = Int1110(ppp_index) &
                                   + RRc2*Int0100(p2)
              end if
              if(i.eq.p2)then
                Int1110(ppp_index) = Int1110(ppp_index) &
                                   + RRc3*Int1000(p1)
              end if
            end do ! i
          end do ! p2
        end do ! p1
!
! Step 6:
! [2010] from [2000] and [1000]
! c+1i
!
        dp_index=0
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i)
!
            Int2010(dp_index) = RRc1(i)*Int2000(d1)
            if(a_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int2010(dp_index) = Int2010(dp_index) &
                                + dble(a_i)*RRc2*Int1000(p1)
            end if
          end do ! i
        end do ! d1
!
! Step 7:
! [2100] from [2000] and [1000]
! b+1i
!
        dp_index=0
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i)
!
            Int2100(dp_index) = RRb1(i)*Int2000(d1)
            if(a_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int2100(dp_index) = Int2100(dp_index) &
                                + dble(a_i)*RRb2*Int1000(p1)
            end if
          end do ! i
        end do ! d1
!
! Step 8:
! [2110] from [2100], [2000] and [1100]
! c+1i
!
        dp_index=0
        dpp_index=0
        do d1=1,6
          do p2=1,3
            dp_index=dp_index+1
            do i=1,3
              dpp_index=dpp_index+1
!
              a_i=Lvec_d(d1,i)
!
              Int2110(dpp_index) = RRc1(i)*Int2100(dp_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2110(dpp_index) = Int2110(dpp_index) &
                                   + dble(a_i)*RRc2*Int1100(pp_index)
              end if
              if(i.eq.p2)then
                Int2110(dpp_index) = Int2110(dpp_index) &
                                   + RRc3*Int2000(d1)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 9:
! [2111] from [2110], [2100], [2010] and [1110]
! d+1i
!
        dppp_index=0
        dpp_index=0
        do d1=1,6
          do p2=1,3
            dp2_index=3*(d1-1)+p2
            do p3=1,3
              dpp_index=dpp_index+1
              dp3_index=3*(d1-1)+p3
              do i=1,3
                dppp_index=dppp_index+1
!
                a_i=Lvec_d(d1,i)
!
                eam(dppp_index) = RRd1(i)*Int2110(dpp_index)
!
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! (a-1i)
                  p1=dot_product(v_1i,xyz)
                  ppp_index=9*(p1-1)+3*(p2-1)+p3
                  eam(dppp_index) = eam(dppp_index) &
                                       + dble(a_i)*RRd2*Int1110(ppp_index)
                end if
                if(i.eq.p2)then
                  eam(dppp_index) = eam(dppp_index) &
                                       + RRd3*Int2010(dp3_index)
                end if
                if(i.eq.p3)then
                  eam(dppp_index) = eam(dppp_index) &
                                       + RRd4*Int2100(dp2_index)
                end if
              end do ! i
            end do ! p3
          end do ! p2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        do d1=4,6
          do p2=1,3
            do p3=1,3
              do p4=1,3
                dppp_index=27*(d1-1)+9*(p2-1)+3*(p3-1)+p4
                eam(dppp_index) = sq3*eam(dppp_index)
              end do
            end do
          end do
        end do
!
        return
      end if
!
!------------------------------------------------------------------
! [pdpp]
!------------------------------------------------------------------
      if(Lcode.eq.1211)then
!
! Requires 9 steps
!
        allocate(Int0100(3),Int0200(6),Int1100(9))
        allocate(Int1200(18),Int0210(18),Int1110(27))
        allocate(Int1000(3))
        allocate(Int1210(54))
!
! Step 1 :
! [0100] from [0000]
! b+1i
!
        do i=1,3 ! x,y,z
          Int0100(i) = RRb1(i)*G
        end do ! i 
!
! Step 2 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 3:
! [1100] from [1000] and [0000]
! b+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int1100(pp_index) = RRb1(i)*Int1000(p1)
            if(p1.eq.i)then
              Int1100(pp_index) = Int1100(pp_index) &
                                + RRb2*G
            end if
          end do ! i
        end do ! p1
!
! Step 4 :
! [0200] from [0100] and [0000]
! b+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int0200(d_index) = RRb1(i)*Int0100(p1) &
                           + RRb3*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int0200(d_index) = RRb1(i)*Int0100(p1)
          end do ! i
        end do ! p1
!
! Step 5 :
! [1110] from [1100], [1000] and [0100]
! c+1i
!
        ppp_index=0
        pp_index=0
        do p1=1,3
          do p2=1,3
            pp_index=pp_index+1
            do i=1,3
              ppp_index=ppp_index+1
!
              Int1110(ppp_index) = RRc1(i)*Int1100(pp_index)
              if(i.eq.p1)then
                Int1110(ppp_index) = Int1110(ppp_index) &
                                   + RRc2*Int0100(p2)
              end if
              if(i.eq.p2)then
                Int1110(ppp_index) = Int1110(ppp_index) &
                                   + RRc3*Int1000(p1)
              end if
            end do ! i
          end do ! p2
        end do ! p1
!
! Step 6:
! [0210] from [0200] and [0100]
! c+1i
!
        dp_index=0
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            b_i=Lvec_d(d1,i)
!
            Int0210(dp_index) = RRc1(i)*Int0200(d1)
            if(b_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int0210(dp_index) = Int0210(dp_index) &
                                + dble(b_i)*RRc3*Int0100(p1)
            end if
          end do ! i
        end do ! d1
!
! Step 7:
! [1200] from [0200] and [0100]
! a+1i
!
        pd_index=0
        do i=1,3
          do d2=1,6
            pd_index=pd_index+1
!
            b_i=Lvec_d(d2,i)
!
            Int1200(pd_index) = RRa1(i)*Int0200(d2)
            if(b_i.ne.0)then
              v_1i(1:3)=Lvec_d(d2,1:3)
              v_1i(i)=v_1i(i)-1
              p2=dot_product(v_1i,xyz)
              Int1200(pd_index) = Int1200(pd_index) &
                                + dble(b_i)*RRa3*Int0100(p2)
            end if
          end do ! d2
        end do ! i
!
! Step 8:
! [1210] from [1200], [0200] and [1100]
! c+1i
!
        pd_index=0
        pdp_index=0
        do p1=1,3
          do d2=1,6
            pd_index=pd_index+1
            do i=1,3
              pdp_index=pdp_index+1
!
              b_i=Lvec_d(d2,i)
!
              Int1210(pdp_index) = RRc1(i)*Int1200(pd_index)
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p2=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int1210(pdp_index) = Int1210(pdp_index) &
                                   + dble(b_i)*RRc3*Int1100(pp_index)
              end if
              if(i.eq.p1)then
                Int1210(pdp_index) = Int1210(pdp_index) &
                                   + RRc2*Int0200(d2)
              end if
            end do ! i
          end do ! d2
        end do ! p1
!
! Step 9:
! [1211] from [1210], [1200], [0210] and [1110]
! d+1i
!
        pdpp_index=0
        pdp_index=0
        do p1=1,3
          do d2=1,6
            p1d_index=6*(p1-1)+d2
            do p3=1,3
              pdp_index=pdp_index+1
              dp3_index=3*(d2-1)+p3
              do i=1,3
                pdpp_index=pdpp_index+1
!
                b_i=Lvec_d(d2,i)
!
                eam(pdpp_index) = RRd1(i)*Int1210(pdp_index)
!
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1 ! (b-1i)
                  p2=dot_product(v_1i,xyz)
                  ppp_index=9*(p1-1)+3*(p2-1)+p3
                  eam(pdpp_index) = eam(pdpp_index) &
                                        + dble(b_i)*RRd3*Int1110(ppp_index)
                end if
                if(i.eq.p1)then
                  eam(pdpp_index) = eam(pdpp_index) &
                                        + RRd2*Int0210(dp3_index)
                end if
                if(i.eq.p3)then
                  eam(pdpp_index) = eam(pdpp_index) &
                                        + RRd4*Int1200(p1d_index)
                end if
              end do ! i
            end do ! p3
          end do ! p2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        pdpp_index = 0
        do p1=1,3
          do d2=1,6
            do p3=1,3
              do p4=1,3
                pdpp_index = pdpp_index + 1
                if(d2.gt.3) eam(pdpp_index) = sq3*eam(pdpp_index)
              end do
            end do
          end do
        end do
!
        return
      end if
!
!------------------------------------------------------------------
! [ppdp]
!------------------------------------------------------------------
      if(Lcode.eq.1121)then
!
! Requires 9 steps
!
        allocate(Int0010(3),Int0020(6),Int1010(9))
        allocate(Int1020(18),Int0120(18),Int1110(27))
        allocate(Int1000(3))
        allocate(Int1120(54))
!
! Step 1 :
! [0010] from [0000]
! c+1i
!
        do i=1,3 ! x,y,z
          Int0010(i) = RRc1(i)*G
        end do ! i 
!
! Step 2 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 3:
! [1010] from [1000] and [0000]
! c+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int1010(pp_index) = RRc1(i)*Int1000(p1)
            if(p1.eq.i)then
              Int1010(pp_index) = Int1010(pp_index) &
                                + RRc2*G
            end if
          end do ! i
        end do ! p1
!
! Step 4 :
! [0020] from [0010] and [0000]
! c+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int0020(d_index) = RRc1(i)*Int0010(p1) &
                           + RRc4*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int0020(d_index) = RRc1(i)*Int0010(p1)
          end do ! i
        end do ! p1
!
! Step 5 :
! [1110] from [1010], [1000] and [0010]
! b+1i
!
        ppp_index=0
        pp_index=0
        do p1=1,3
          do p2=1,3
            pp_index=pp_index+1
            do i=1,3
              ppp_index=9*(p1-1) + 3*(i-1) + p2
!
              Int1110(ppp_index) = RRb1(i)*Int1010(pp_index)
              if(i.eq.p1)then
                Int1110(ppp_index) = Int1110(ppp_index) &
                                   + RRb2*Int0010(p2)
              end if
              if(i.eq.p2)then
                Int1110(ppp_index) = Int1110(ppp_index) &
                                   + RRb4*Int1000(p1)
              end if
            end do ! i
          end do ! p2
        end do ! p1
!
! Step 6:
! [0120] from [0020] and [0010]
! b+1i
!
        pd_index=0
        do i=1,3
          do d1=1,6
            pd_index=pd_index+1
!
            c_i=Lvec_d(d1,i)
!
            Int0120(pd_index) = RRb1(i)*Int0020(d1)
            if(c_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int0120(pd_index) = Int0120(pd_index) &
                                + dble(c_i)*RRb4*Int0010(p1)
            end if
          end do ! i
        end do ! d1
!
! Step 7:
! [1020] from [0020] and [0010]
! a+1i
!
        pd_index=0
        do i=1,3
          do d2=1,6
            pd_index=pd_index+1
!
            c_i=Lvec_d(d2,i)
!
            Int1020(pd_index) = RRa1(i)*Int0020(d2)
            if(c_i.ne.0)then
              v_1i(1:3)=Lvec_d(d2,1:3)
              v_1i(i)=v_1i(i)-1
              p2=dot_product(v_1i,xyz)
              Int1020(pd_index) = Int1020(pd_index) &
                                + dble(c_i)*RRa4*Int0010(p2)
            end if
          end do ! d2
        end do ! i
!
! Step 8:
! [1120] from [1020], [0020] and [1010]
! b+1i
!
        pd_index=0
        ppd_index=0
        do p1=1,3
          do d2=1,6
            pd_index=pd_index+1
            do i=1,3
              ppd_index=18*(p1-1)+6*(i-1)+d2
!
              c_i=Lvec_d(d2,i)
!
              Int1120(ppd_index) = RRb1(i)*Int1020(pd_index)
              if(c_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! c-1i
                p2=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int1120(ppd_index) = Int1120(ppd_index) &
                                   + dble(c_i)*RRb4*Int1010(pp_index)
              end if
              if(i.eq.p1)then
                Int1120(ppd_index) = Int1120(ppd_index) &
                                   + RRb2*Int0020(d2)
              end if
            end do ! i
          end do ! d2
        end do ! p1
!
! Step 9:
! [1121] from [1120], [1020], [0120] and [1110]
! d+1i
!
        ppdp_index=0
        ppd_index=0
        do p1=1,3
          do p2=1,3
            do d3=1,6
              p1d_index = 6*(p1-1) + d3
              p2d_index = 6*(p2-1) + d3
              ppd_index = ppd_index+1
              do i=1,3
                ppdp_index=ppdp_index+1
!
                c_i=Lvec_d(d3,i)
!
                eam(ppdp_index) = RRd1(i)*Int1120(ppd_index)
!
                if(c_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d3,1:3)
                  v_1i(i)=v_1i(i)-1 ! (c-1i)
                  p3=dot_product(v_1i,xyz)
                  ppp_index=9*(p1-1)+3*(p2-1)+p3
                  eam(ppdp_index) = eam(ppdp_index) &
                                        + dble(c_i)*RRd4*Int1110(ppp_index)
                end if
                if(i.eq.p1)then
                  eam(ppdp_index) = eam(ppdp_index) &
                                        + RRd2*Int0120(p2d_index)
                end if
                if(i.eq.p2)then
                  eam(ppdp_index) = eam(ppdp_index) &
                                        + RRd3*Int1020(p1d_index)
                end if
              end do ! i
            end do ! p3
          end do ! p2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        ppdp_index = 0
        do p1=1,3
          do p2=1,3
            do d3=1,6
              do p4=1,3
                ppdp_index = ppdp_index + 1
                if(d3.gt.3) eam(ppdp_index) = sq3*eam(ppdp_index)
              end do
            end do
          end do
        end do
!
        return
      end if
!
!------------------------------------------------------------------
! [ddpp]
!------------------------------------------------------------------
      if(Lcode.eq.2211)then
!
! Requires 12 steps
!
        allocate(Int1000(3),Int2000(6),Int1100(9))
        allocate(Int2100(18),Int1200(18),Int1210(54))
        allocate(Int0100(3),Int0200(6),Int2200(36))
        allocate(Int2110(54),Int2210(108))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2 :
! [0100] from [0000]
! b+1i
!
        do i=1,3 ! x,y,z
          Int0100(i) = RRb1(i)*G
        end do ! i 
!
! Step 3:
! [1100] from [1000] and [0000]
! b+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int1100(pp_index) = RRb1(i)*Int1000(p1)
            if(p1.eq.i)then
              Int1100(pp_index) = Int1100(pp_index) &
                                + RRb2*G
            end if
          end do ! i
        end do ! p1
!
! Step 4 :
! [0200] from [0100] and [0000]
! b+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int0200(d_index) = RRb1(i)*Int0100(p1) &
                           + RRb3*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int0200(d_index) = RRb1(i)*Int0100(p1)
          end do ! i
        end do ! p1
!
! Step 5 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int2000(d_index) = RRa1(i)*Int1000(p1) &
                           + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int2000(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! Step 6:
! [1200] from [1100], [1000] and [0100]
! b+1i
!
        pd_index=0
        do p1=1,3
          do p2=1,3
            i=p2
            pd_index=pd_index+1
            pp_index=3*(p1-1)+p2
!
            Int1200(pd_index) = RRb1(i)*Int1100(pp_index) &
                              + RRb3*Int1000(p1)
            if(i.eq.p1)then
              Int1200(pd_index) = Int1200(pd_index) &
                                + RRb2*Int0100(p2)
            end if
          end do ! p2
!
          do p2=2,3
            pp_index=3*(p1-1)+p2
            do i=1,p2-1
              pd_index=pd_index+1
!
              Int1200(pd_index) = RRb1(i)*Int1100(pp_index)
              if(i.eq.p1)then
                Int1200(pd_index) = Int1200(pd_index) &
                                  + RRb2*Int0100(p2)
              end if
            end do ! i 
          end do ! p2
        end do ! p1
!
! Step 7:
! [2100] from [2000] and [1000]
! b+1i
!
        dp_index=0
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i)
!
            Int2100(dp_index) = RRb1(i)*Int2000(d1)
            if(a_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int2100(dp_index) = Int2100(dp_index) &
                                + dble(a_i)*RRb2*Int1000(p1)
            end if
          end do ! i
        end do ! d1 
!
! Step 8:
! [1210] from [1200], [1100] and [0200]
! c+1i
!
        pd_index=0
        pdp_index=0
        do p1=1,3
          do d2=1,6
            pd_index=pd_index+1
            do i=1,3
              pdp_index=pdp_index+1
!
              b_i=Lvec_d(d2,i)
!
              Int1210(pdp_index) = RRc1(i)*Int1200(pd_index)
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p2=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int1210(pdp_index) = Int1210(pdp_index) &
                                   + dble(b_i)*RRc3*Int1100(pp_index)
              end if
              if(i.eq.p1)then
                Int1210(pdp_index) = Int1210(pdp_index) &
                                   + RRc2*Int0200(d2)
              end if
            end do ! i
          end do ! d2
        end do ! p1
!
! Step 9:
! [2110] from [2100], [2000] and [1100]
! c+1i
!
        dp_index=0
        dpp_index=0
        do d1=1,6
          do p2=1,3
            dp_index=dp_index+1
            do i=1,3
              dpp_index=dpp_index+1
!
              a_i=Lvec_d(d1,i)
!
              Int2110(dpp_index) = RRc1(i)*Int2100(dp_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2110(dpp_index) = Int2110(dpp_index) &
                                   + dble(a_i)*RRc2*Int1100(pp_index)
              end if
              if(i.eq.p2)then
                Int2110(dpp_index) = Int2110(dpp_index) &
                                   + RRc3*Int2000(d1)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 10:
! [2200] from [2100], [2000] and [1100]
! b+1i
!
        dd_index=0
        do d1=1,6
          do p2=1,3
            i=p2
            dp_index=3*(d1-1)+p2
            dd_index=dd_index+1
!
            a_i=Lvec_d(d1,i)
!
            Int2200(dd_index) = RRb1(i)*Int2100(dp_index) &
                              + RRb3*Int2000(d1)
            if(a_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1i
              p1=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              Int2200(dd_index) = Int2200(dd_index) &
                                + dble(a_i)*RRb2*Int1100(pp_index)
            end if
          end do ! p2
!
          do p2=2,3
            dp_index=3*(d1-1)+p2
            do i=1,p2-1
              dd_index=dd_index+1
!
              a_i=Lvec_d(d1,i)
!
              Int2200(dd_index) = RRb1(i)*Int2100(dp_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2200(dd_index) = Int2200(dd_index) &
                                  + dble(a_i)*RRb2*Int1100(pp_index)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 11:
! [2210] from [2200], [2100] and [1200]
! c+1i
!
        ddp_index=0
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do i=1,3
              ddp_index=ddp_index+1
!
              a_i=Lvec_d(d1,i)
              b_i=Lvec_d(d2,i)
!
              Int2210(ddp_index) = RRc1(i)*Int2200(dd_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pd_index=6*(p1-1)+d2
                Int2210(ddp_index) = Int2210(ddp_index) &
                                   + dble(a_i)*RRc2*Int1200(pd_index)
              end if
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p2=dot_product(v_1i,xyz)
                dp_index=3*(d1-1)+p2
                Int2210(ddp_index) = Int2210(ddp_index) &
                                   + dble(b_i)*RRc3*Int2100(dp_index)
              end if
            end do ! i
          end do ! d2
        end do ! d1
!
! Step 12:
! [2211] from [2210], [2200], [2110] and [1210]
! d+1i
!
        ddpp_index=0
        ddp_index=0
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do p3=1,3
              ddp_index=ddp_index+1
              do i=1,3
                ddpp_index=ddpp_index+1
!
                a_i=Lvec_d(d1,i)
                b_i=Lvec_d(d2,i)
!
                eam(ddpp_index) = RRd1(i)*Int2210(ddp_index)
                if(i.eq.p3)then
                  eam(ddpp_index) = eam(ddpp_index) &
                                       + RRd4*Int2200(dd_index)
                end if
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1
                  p1=dot_product(v_1i,xyz)
                  pdp_index=18*(p1-1)+3*(d2-1)+p3
                  eam(ddpp_index) = eam(ddpp_index) &
                                       + dble(a_i)*RRd2*Int1210(pdp_index)
                end if
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1
                  p2=dot_product(v_1i,xyz)
                  dpp_index=9*(d1-1)+3*(p2-1)+p3
                  eam(ddpp_index) = eam(ddpp_index) &
                                       + dble(b_i)*RRd3*Int2110(dpp_index)
                end if
              end do ! i
            end do ! p3
          end do ! d2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        ddpp_index=0
        do d1=1,6
          do d2=1,6
            do p3=1,3
              do p4=1,3
                ddpp_index=ddpp_index+1
                if(d1.gt.3) eam(ddpp_index) = sq3*eam(ddpp_index)
                if(d2.gt.3) eam(ddpp_index) = sq3*eam(ddpp_index)
              end do
            end do
          end do
        end do
!
        return
      end if
!
!------------------------------------------------------------------
! [dpdp]
!------------------------------------------------------------------
      if(Lcode.eq.2121)then
!
! Requires 12 steps
!
        allocate(Int1000(3),Int2000(6),Int1010(9))
        allocate(Int2010(18),Int1020(18),Int1120(54))
        allocate(Int0010(3),Int0020(6),Int2020(36))
        allocate(Int2110(54),Int2120(108))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2 :
! [0010] from [0000]
! c+1i
!
        do i=1,3 ! x,y,z
          Int0010(i) = RRc1(i)*G
        end do ! i 
!
! Step 3 :
! [0020] from [0010] and [0000]
! c+1i
!
! Note: d's are numbered xy-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int0020(d_index) = RRc1(i)*Int0010(p1) &
                           + RRc4*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int0020(d_index) = RRc1(i)*Int0010(p1)
          end do ! i
        end do ! p1
!
! Step 4:
! [1010] from [1000] and [0000]
! c+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int1010(pp_index) = RRc1(i)*Int1000(p1)
            if(i.eq.p1)then
              Int1010(pp_index) = Int1010(pp_index) &
                                + RRc2*G
            end if
          end do ! i
        end do ! p1
!
! Step 5 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int2000(d_index) = RRa1(i)*Int1000(p1) &
                           + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int2000(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! Step 6:
! [1020] from [0020], [0010]
! a+1i
!
        do d1=1,6
          do i=1,3
            pd_index=6*(i-1)+d1
!
            c_i=Lvec_d(d1,i)
!
            Int1020(pd_index) = RRa1(i)*Int0020(d1)
            if(c_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int1020(pd_index) = Int1020(pd_index) &
                                + dble(c_i)*RRa4*Int0010(p1)
            end if
          end do ! i 
        end do ! d1
!
! Step 7:
! [2010] from [2000], [1000]
! c+1i
!
        dp_index=0
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i)
!
            Int2010(dp_index) = RRc1(i)*Int2000(d1)
            if(a_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int2010(dp_index) = Int2010(dp_index) &
                                + dble(a_i)*RRc2*Int1000(p1)
            end if
          end do ! i 
        end do ! d1
!
! Step 8:
! [1120] from [1020], [1010] and [0020]
! b+1i
!
        pd_index=0
        do p1=1,3
          do d2=1,6
            pd_index=pd_index+1
            do i=1,3
              ppd_index=18*(p1-1)+6*(i-1)+d2
!                
              c_i=Lvec_d(d2,i)
!
              Int1120(ppd_index) = RRb1(i)*Int1020(pd_index)
              if(c_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! (c-1i)
                p2=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int1120(ppd_index) = Int1120(ppd_index) &
                                   + dble(c_i)*RRb4*Int1010(pp_index)
              end if
              if(p1.eq.i)then
                Int1120(ppd_index) = Int1120(ppd_index) &
                                   + RRb2*Int0020(d2)
              end if
            end do ! i
          end do ! d2
        end do ! p1
!
! Step 9:
! [2020] from [2010], [2000] and [1010]
! c+1i
!
        dd_index=0
        do d1=1,6
          do p2=1,3
            i=p2 ! do xy, yy, zz first
            dd_index=dd_index+1
            dp_index=3*(d1-1)+p2
!
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int2020(dd_index) = RRc1(i)*Int2010(dp_index) &
                              + RRc4*Int2000(d1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              Int2020(dd_index) = Int2020(dd_index) &
                                + dble(a_i)*RRc2*Int1010(pp_index)
            end if
          end do ! p2
!
          do p2=2,3 ! y, z
            dp_index=3*(d1-1)+p2
            do i=1,p2-1 ! x, y
              dd_index=dd_index+1
!
! i ne p2, therefore no (b-1i) terms
!
              a_i=Lvec_d(d1,i) ! angular momentum vector
!
              Int2020(dd_index) = RRc1(i)*Int2010(dp_index)
              if(a_i.ne.0)then ! (a-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2020(dd_index) = Int2020(dd_index) &
                                  + dble(a_i)*RRc2*Int1010(pp_index)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 10:
! [2110] from [2010], [2000] and [1010]
! b+1i
!
        dp_index=0
        do d1=1,6
          do p2=1,3
            dp_index=dp_index+1
            do i=1,3
              dpp_index=9*(d1-1)+3*(i-1)+p2
!
              a_i=Lvec_d(d1,i)
!
              Int2110(dpp_index) = RRb1(i)*Int2010(dp_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2110(dpp_index) = Int2110(dpp_index) &
                                   + dble(a_i)*RRb2*Int1010(pp_index)
              end if
              if(i.eq.p2)then
                Int2110(dpp_index) = Int2110(dpp_index) &
                                   + RRb4*Int2000(d1)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 11:
! [2120] from [2020], [2010] and [1020]
! b+1i
!
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do i=1,3
              dpd_index=18*(d1-1)+6*(i-1)+d2
!
              a_i=Lvec_d(d1,i)
              c_i=Lvec_d(d2,i)
!
              Int2120(dpd_index) = RRb1(i)*Int2020(dd_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pd_index=6*(p1-1)+d2
                Int2120(dpd_index) = Int2120(dpd_index) &
                                   + dble(a_i)*RRb2*Int1020(pd_index)
              end if
              if(c_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! c-1i
                p2=dot_product(v_1i,xyz)
                dp_index=3*(d1-1)+p2
                Int2120(dpd_index) = Int2120(dpd_index) &
                                   + dble(c_i)*RRb4*Int2010(dp_index)
              end if
            end do ! i
          end do ! d2
        end do ! d1
!
! Step 12:
! [2121] from [2120], [2110], [2020] and [1120]
! d+1i
!
        dpdp_index=0
        dpd_index=0
        do d1=1,6
          do p2=1,3
            do d3=1,6
              dd_index=6*(d1-1)+d3
              dpd_index=dpd_index+1
              do i=1,3
                dpdp_index=dpdp_index+1
!
                a_i=Lvec_d(d1,i)
                c_i=Lvec_d(d3,i)
!
                eam(dpdp_index) = RRd1(i)*Int2120(dpd_index)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! a-1i
                  p1=dot_product(v_1i,xyz)
                  ppd_index=18*(p1-1)+6*(p2-1)+d3
                  eam(dpdp_index) = eam(dpdp_index) &
                                       + dble(a_i)*RRd2*Int1120(ppd_index)
                end if
                if(c_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d3,1:3)
                  v_1i(i)=v_1i(i)-1 ! c-1i
                  p3=dot_product(v_1i,xyz)
                  dpp_index=9*(d1-1)+3*(p2-1)+p3
                  eam(dpdp_index) = eam(dpdp_index) &
                                       + dble(c_i)*RRd4*Int2110(dpp_index)
                end if
                if(i.eq.p2)then
                  eam(dpdp_index) = eam(dpdp_index) &
                                       + RRd3*Int2020(dd_index)
                end if
              end do ! i
            end do ! d3
          end do ! p2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        dpdp_index=0
        do d1=1,6
          do p2=1,3
            do d3=1,6
              do p4=1,3
                dpdp_index=dpdp_index+1
                if(d1.gt.3) eam(dpdp_index) = sq3*eam(dpdp_index)
                if(d3.gt.3) eam(dpdp_index) = sq3*eam(dpdp_index)
              end do
            end do
          end do
        end do
!
        return
      end if
!
!------------------------------------------------------------------
! [dppd]
!------------------------------------------------------------------
      if(Lcode.eq.2112)then
!
! Requires 12 steps
!
        allocate(Int1000(3),Int2000(6),Int1001(9))
        allocate(Int2001(18),Int1002(18),Int1102(54))
        allocate(Int0001(3),Int0002(6),Int2002(36))
        allocate(Int2101(54),Int2102(108))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2 :
! [0001] from [0000]
! d+1i
!
        do i=1,3 ! x,y,z
          Int0001(i) = RRd1(i)*G
        end do ! i 
!
! Step 3 :
! [0002] from [0001] and [0000]
! d+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int0002(d_index) = RRd1(i)*Int0001(p1) &
                           + RRd5*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int0002(d_index) = RRd1(i)*Int0001(p1)
          end do ! i
        end do ! p1
!
! Step 4:
! [1001] from [1000] and [0000]
! d+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int1001(pp_index) = RRd1(i)*Int1000(p1)
            if(p1.eq.i)then
              Int1001(pp_index) = Int1001(pp_index) &
                                + RRd2*G
            end if
          end do ! i
        end do ! p1
!
! Step 5 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int2000(d_index) = RRa1(i)*Int1000(p1) &
                           + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int2000(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! Step 6:
! [1002] from [0002], [0001]
! a+1i
!
        do d1=1,6
          do i=1,3
            pd_index=6*(i-1)+d1
!
            d_i=Lvec_d(d1,i)
!
            Int1002(pd_index) = RRa1(i)*Int0002(d1)
            if(d_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int1002(pd_index) = Int1002(pd_index) &
                                + dble(d_i)*RRa5*Int0001(p1)
            end if
          end do ! i 
        end do ! d1
!
! Step 7:
! [2001] from [2000], [1000]
! d+1i
!
        dp_index=0
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i)
!
            Int2001(dp_index) = RRd1(i)*Int2000(d1)
            if(a_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int2001(dp_index) = Int2001(dp_index) &
                                + dble(a_i)*RRd2*Int1000(p1)
            end if
          end do ! i 
        end do ! d1
!
! Step 8:
! [1102] from [1002], [1001] and [0002]
! b+1i
!
        pd_index=0
        do p1=1,3
          do d2=1,6
            pd_index=pd_index+1
            do i=1,3
              ppd_index=18*(p1-1)+6*(i-1)+d2
!                
              d_i=Lvec_d(d2,i)
!
              Int1102(ppd_index) = RRb1(i)*Int1002(pd_index)
              if(d_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! (d-1i)
                p2=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int1102(ppd_index) = Int1102(ppd_index) &
                                   + dble(d_i)*RRb5*Int1001(pp_index)
              end if
              if(i.eq.p1)then
                Int1102(ppd_index) = Int1102(ppd_index) &
                                   + RRb2*Int0002(d2)
              end if
            end do ! i
          end do ! d2
        end do ! p1
!
! Step 9:
! [2002] from [2001], [2000] and [1001]
! d+1i
!
        dd_index=0
        do d1=1,6
          do p2=1,3
            i=p2 ! do xy, yy, zz first
            dd_index=dd_index+1
            dp_index=3*(d1-1)+p2
! 
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int2002(dd_index) = RRd1(i)*Int2001(dp_index) &
                              + RRd5*Int2000(d1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              Int2002(dd_index) = Int2002(dd_index) &
                                + dble(a_i)*RRd2*Int1001(pp_index)
            end if
          end do ! p2
!
          do p2=2,3 ! y, z
            dp_index=3*(d1-1)+p2
            do i=1,p2-1 ! x, y
              dd_index=dd_index+1
!
! i ne p2, therefore no (b-1i) terms
!
              a_i=Lvec_d(d1,i) ! angular momentum vector
!
              Int2002(dd_index) = RRd1(i)*Int2001(dp_index)
              if(a_i.ne.0)then ! (a-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2002(dd_index) = Int2002(dd_index) &
                                  + dble(a_i)*RRd2*Int1001(pp_index)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 10:
! [2101] from [2001], [2000] and [1001]
! b+1i
!
        dp_index=0
        do d1=1,6
          do p2=1,3
            dp_index=dp_index+1
            do i=1,3
              dpp_index=9*(d1-1)+3*(i-1)+p2 
!                
              a_i=Lvec_d(d1,i)
!
              Int2101(dpp_index) = RRb1(i)*Int2001(dp_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! (a-1i)
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2101(dpp_index) = Int2101(dpp_index) &
                                   + dble(a_i)*RRb2*Int1001(pp_index)
              end if
              if(i.eq.p2)then
                Int2101(dpp_index) = Int2101(dpp_index) &
                                   + RRb5*Int2000(d1)
              end if
            end do ! i
          end do ! d2
        end do ! p1
!
! Step 11:
! [2102] from [2002], [2001] and [1002]
! b+1i
!
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do i=1,3
              dpd_index=18*(d1-1)+6*(i-1)+d2
!
              a_i=Lvec_d(d1,i)
              d_i=Lvec_d(d2,i)
!
              Int2102(dpd_index) = RRb1(i)*Int2002(dd_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pd_index=6*(p1-1)+d2
                Int2102(dpd_index) = Int2102(dpd_index) &
                                   + dble(a_i)*RRb2*Int1002(pd_index)
              end if
              if(d_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! d-1i
                p2=dot_product(v_1i,xyz)
                dp_index=3*(d1-1)+p2
                Int2102(dpd_index) = Int2102(dpd_index) &
                                   + dble(d_i)*RRb5*Int2001(dp_index)
              end if
            end do ! i
          end do ! d2
        end do ! d1
!
! Step 12:
! [2112] from [2102], [2101], [2002] and [1102]
! c+1i
!
        dppd_index=0
        dpd_index=0
        do d1=1,6
          do p2=1,3
            do d3=1,6
              dd_index=6*(d1-1)+d3 
              dpd_index=dpd_index+1
              do i=1,3
                dppd_index=54*(d1-1)+18*(p2-1)+6*(i-1)+d3
!
                a_i=Lvec_d(d1,i)
                d_i=Lvec_d(d3,i)
!
                eam(dppd_index) = RRc1(i)*Int2102(dpd_index)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! a-1i
                  p1=dot_product(v_1i,xyz)
                  ppd_index=18*(p1-1)+6*(p2-1)+d3
                  eam(dppd_index) = eam(dppd_index) &
                                       + dble(a_i)*RRc2*Int1102(ppd_index)
                end if
                if(d_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d3,1:3)
                  v_1i(i)=v_1i(i)-1 ! d-1i
                  p3=dot_product(v_1i,xyz)
                  dpp_index=9*(d1-1)+3*(p2-1)+p3
                  eam(dppd_index) = eam(dppd_index) &
                                       + dble(d_i)*RRc5*Int2101(dpp_index)
                end if
                if(i.eq.p2)then
                  eam(dppd_index) = eam(dppd_index) &
                                       + RRc3*Int2002(dd_index)
                end if
              end do ! i
            end do ! d3
          end do ! p2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        dppd_index=0
        do d1=1,6
          do p2=1,3
            do p3=1,3
              do d4=1,6
                dppd_index=dppd_index+1
                if(d1.gt.3) eam(dppd_index) = sq3*eam(dppd_index)
                if(d4.gt.3) eam(dppd_index) = sq3*eam(dppd_index)
              end do
            end do
          end do
        end do
!
        return
      end if
!
!------------------------------------------------------------------
! [pddp]
!------------------------------------------------------------------
      if(Lcode.eq.1221)then
!
! Requires 14 steps (12, but I copied it from dddd)
!
        allocate(Int1000(3), Int0100(3))
        allocate(Int0110(9), Int1010(9), Int0200(6))
        allocate(Int1100(9), Int0210(18))
        allocate(Int1110(27), Int1200(18))
        allocate(Int0220(36), Int1120(54))
        allocate(Int1210(54))
        allocate(Int1220(108))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2 :
! [0100] from [0000]
! b+1i
!
        do i=1,3 ! x,y,z
          Int0100(i) = RRb1(i)*G
        end do ! i 
!
! Step 3 :
! [0110] from [0100] and [0000]
! c+1i
!
        pp_index=0
        do p1=1,3
          do i=1,3
            pp_index=pp_index+1
!
            Int0110(pp_index) = RRc1(i)*Int0100(p1)
            if(i.eq.p1)then
              Int0110(pp_index) = Int0110(pp_index) &
                                + RRc3*G
            end if
          end do ! i
        end do ! p1
!
! Step 4 :
! [1010] from [1000] and [0000]
! c+1i
!
        pp_index=0
        do p1=1,3
          do i=1,3
            pp_index=pp_index+1
!
            Int1010(pp_index) = RRc1(i)*Int1000(p1)
            if(i.eq.p1)then
              Int1010(pp_index) = Int1010(pp_index) &
                                + RRc2*G
            end if
          end do ! i
        end do ! p1
!
! Step 5 :
! [0200] from [0100] and [0000]
! b+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int0200(d_index) = RRb1(i)*Int0100(p1) &
                           + RRb3*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int0200(d_index) = RRb1(i)*Int0100(p1)
          end do ! i
        end do ! p1
!
! Step 6:
! [1100] from [1000] and [0000]
! b+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int1100(pp_index) = RRb1(i)*Int1000(p1)
            if(p1.eq.i)then
              Int1100(pp_index) = Int1100(pp_index) &
                                + RRb2*G
            end if
          end do ! i
        end do ! p1
!
! Step 7:
! [0210] from [0200], [0100]
! c+1i
!
        dp_index=0
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            b_i=Lvec_d(d1,i)
!
            Int0210(dp_index) = RRc1(i)*Int0200(d1)
            if(b_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int0210(dp_index) = Int0210(dp_index) &
                                + dble(b_i)*RRc3*Int0100(p1)
            end if
          end do ! i 
        end do ! d1
!
! Step 8:
! [1110] from [1100], [1000]
! c+1i
!
        ppp_index=0
        pp_index=0
        do p1=1,3
          do p2=1,3
            pp_index=pp_index+1
            do i=1,3
              ppp_index=ppp_index+1
!
              Int1110(ppp_index) = RRc1(i)*Int1100(pp_index)
              if(i.eq.p1)then
                Int1110(ppp_index) = Int1110(ppp_index) &
                                   + RRc2*Int0100(p2)
              end if
              if(i.eq.p2)then
                Int1110(ppp_index) = Int1110(ppp_index) &
                                   + RRc3*Int1000(p1)
              end if
            end do ! i
          end do ! p2
        end do ! p1
!
! Step 9:
! [1200] from [1100], [0100] and [1000]
! b+1i
!
        pd_index=0
        do p1=1,3
          do p2=1,3
            i=p2
            pd_index=pd_index+1
            pp_index=3*(p1-1)+p2
!
            Int1200(pd_index) = RRb1(i)*Int1100(pp_index) &
                              + RRb3*Int1000(p1)
            if(i.eq.p1)then
              Int1200(pd_index) = Int1200(pd_index) &
                                + RRb2*Int0100(p2)
            end if
          end do ! p2
!
          do p2=2,3
            pp_index=3*(p1-1)+p2
            do i=1,p2-1
              pd_index=pd_index+1
!
              Int1200(pd_index) = RRb1(i)*Int1100(pp_index)
              if(i.eq.p1)then
                Int1200(pd_index) = Int1200(pd_index) &
                                  + RRb2*Int0100(p2)
              end if
            end do ! i 
          end do ! p2
        end do ! p1
!
! Step 10 : 
! [0220] from [0210], [0200] and [0110]
! c+1i
!
        dd_index=0
        do d1=1,6
          do p2=1,3
            i=p2
            dd_index=dd_index+1
            dp_index=3*(d1-1)+p2
!
            b_i=Lvec_d(d1,i)
!
            Int0220(dd_index) = RRc1(i)*Int0210(dp_index) &
                              + RRc4*Int0200(d1)
            if(b_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              Int0220(dd_index) = Int0220(dd_index) &
                                + dble(b_i)*RRc3*Int0110(pp_index)
            end if
          end do ! p3
!
          do p2=2,3
            dp_index=3*(d1-1)+p2
            do i=1,p2-1
              dd_index=dd_index+1
!
              b_i=Lvec_d(d1,i)
!
              Int0220(dd_index) = RRc1(i)*Int0210(dp_index)
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int0220(dd_index) = Int0220(dd_index) &
                                  + dble(b_i)*RRc3*Int0110(pp_index)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 11:
! [1120] from [1110], [1100], [0110], [1010]
! c+1i
!
        ppd_index=0
        do p1=1,3
          do p2=1,3
            do p3=1,3
              i=p3
              ppd_index=ppd_index+1
              ppp_index=9*(p1-1)+3*(p2-1)+p3 
              pp_index=3*(p1-1)+p2
!
              Int1120(ppd_index) = RRc1(i)*Int1110(ppp_index) &
                                 + RRc4*Int1100(pp_index)
              if(i.eq.p1)then
                pp_index=3*(p2-1)+p3
                Int1120(ppd_index) = Int1120(ppd_index) &
                                   + RRc2*Int0110(pp_index)
              end if
              if(i.eq.p2)then
                pp_index=3*(p1-1)+p3
                Int1120(ppd_index) = Int1120(ppd_index) &
                                   + RRc3*Int1010(pp_index)
              end if
            end do ! p3
!
            do p3=2,3
              ppp_index=9*(p1-1)+3*(p2-1)+p3 
              do i=1,p3-1
                ppd_index=ppd_index+1
!
                Int1120(ppd_index) = RRc1(i)*Int1110(ppp_index)
                if(i.eq.p1)then
                  pp_index=3*(p2-1)+p3
                  Int1120(ppd_index) = Int1120(ppd_index) &
                                     + RRc2*Int0110(pp_index)
                end if
                if(i.eq.p2)then
                  pp_index=3*(p1-1)+p3
                  Int1120(ppd_index) = Int1120(ppd_index) &
                                     + RRc3*Int1010(pp_index)
                end if
              end do ! i
            end do ! p3
          end do ! p2
        end do ! p1
!
! Step 12:
! [1210] from [1200], [1100] and [0200]
! c+1i
!
        pd_index=0
        pdp_index=0
        do p1=1,3
          do d2=1,6
            pd_index=pd_index+1
            do i=1,3
              pdp_index=pdp_index+1
!
              b_i=Lvec_d(d2,i)
!
              Int1210(pdp_index) = RRc1(i)*Int1200(pd_index)
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p2=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int1210(pdp_index) = Int1210(pdp_index) &
                                   + dble(b_i)*RRc3*Int1100(pp_index)
              end if
              if(i.eq.p1)then
                Int1210(pdp_index) = Int1210(pdp_index) &
                                   + RRc2*Int0200(d2)
              end if
            end do ! i
          end do ! d2
        end do ! p1
!
! Step 13:
! [1220] from [1210], [1200], [1110] and [0210]
! c+1i
!
        pdd_index=0
        pd_index=0
        do p1=1,3
          do d2=1,6
            pd_index=pd_index+1
            do p3=1,3
              i=p3
              pdp_index=18*(p1-1)+3*(d2-1)+p3
              pdd_index=pdd_index+1
!
              b_i=Lvec_d(d2,i)
!
              Int1220(pdd_index) = RRc1(i)*Int1210(pdp_index) &
                                 + RRc4*Int1200(pd_index)
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p2=dot_product(v_1i,xyz)
                ppp_index=9*(p1-1)+3*(p2-1)+p3
                Int1220(pdd_index) = Int1220(pdd_index) &
                                   + dble(b_i)*RRc3*Int1110(ppp_index)
              end if
              if(i.eq.p1)then
                dp_index=3*(d2-1)+p3
                Int1220(pdd_index) = Int1220(pdd_index) &
                                   + RRc2*Int0210(dp_index)
              end if 
            end do ! p3
!
            do p3=2,3
              pdp_index=18*(p1-1)+3*(d2-1)+p3
              do i=1,p3-1
                pdd_index=pdd_index+1
!
                b_i=Lvec_d(d2,i)
!
                Int1220(pdd_index) = RRc1(i)*Int1210(pdp_index)
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1 ! b-1i
                  p2=dot_product(v_1i,xyz)
                  ppp_index=9*(p1-1)+3*(p2-1)+p3
                  Int1220(pdd_index) = Int1220(pdd_index) &
                                     + dble(b_i)*RRc3*Int1110(ppp_index)
                end if
                if(i.eq.p1)then
                  dp_index=3*(d2-1)+p3
                  Int1220(pdd_index) = Int1220(pdd_index) &
                                     + RRc2*Int0210(dp_index)
                end if 
              end do ! i
            end do ! p3
          end do ! d2
        end do ! p1 
!
! Step 14 :
! [1221] from [1220], [1210], [1120] and [0220]
! d+1i
!
        pddp_index=0
        pdd_index=0
        do p1=1,3
          do d2=1,6
            do d3=1,6
              pdd_index=pdd_index+1
              dd_index=6*(d2-1)+d3
              do i=1,3
                pddp_index=pddp_index+1
!
                b_i=Lvec_d(d2,i)
                c_i=Lvec_d(d3,i)
!
                eam(pddp_index) = RRd1(i)*Int1220(pdd_index)
                if(i.eq.p1)then
                  eam(pddp_index) = eam(pddp_index) &
                                        + RRd2*Int0220(dd_index)
                end if
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1
                  p2=dot_product(v_1i,xyz)
                  ppd_index=18*(p1-1)+6*(p2-1)+d3
                  eam(pddp_index) = eam(pddp_index) &
                                        + dble(b_i)*RRd3*Int1120(ppd_index)
                end if
                if(c_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d3,1:3)
                  v_1i(i)=v_1i(i)-1
                  p3=dot_product(v_1i,xyz)
                  pdp_index=18*(p1-1)+3*(d2-1)+p3
                  eam(pddp_index) = eam(pddp_index) &
                                        + dble(c_i)*RRd4*Int1210(pdp_index)
                end if
              end do ! i
            end do ! d3
          end do ! d2
        end do ! p1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        pddp_index=0
        do p1=1,3
          do d2=1,6
            do d3=1,6
              do p4=1,3
                pddp_index=pddp_index+1
                if(d2.gt.3) eam(pddp_index) = sq3*eam(pddp_index)
                if(d3.gt.3) eam(pddp_index) = sq3*eam(pddp_index)
              end do
            end do
          end do
        end do
!
        return
      end if
!
!------------------------------------------------------------------
! [pdpd]
!------------------------------------------------------------------
      if(Lcode.eq.1212)then
!
! Requires 12 steps
!
        allocate(Int0100(3),Int0200(6),Int0101(9))
        allocate(Int0201(18),Int0102(18),Int1102(54))
        allocate(Int0001(3),Int0002(6),Int0202(36))
        allocate(Int1201(54),Int1202(108))
!
! Step 1 :
! [0100] from [0000]
! b+1i
!
        do i=1,3 ! x,y,z
          Int0100(i) = RRb1(i)*G
        end do ! i 
!
! Step 2 :
! [0001] from [0000]
! d+1i
!
        do i=1,3 ! x,y,z
          Int0001(i) = RRd1(i)*G
        end do ! i 
!
! Step 3 :
! [0002] from [0001] and [0000]
! d+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int0002(d_index) = RRd1(i)*Int0001(p1) &
                           + RRd5*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int0002(d_index) = RRd1(i)*Int0001(p1)
          end do ! i
        end do ! p1
!
! Step 4:
! [0101] from [0100] and [0000]
! d+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int0101(pp_index) = RRd1(i)*Int0100(p1)
            if(p1.eq.i)then
              Int0101(pp_index) = Int0101(pp_index) &
                                + RRd3*G
            end if
          end do ! i
        end do ! p1
!
! Step 5 :
! [0200] from [1000] and [0000]
! b+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int0200(d_index) = RRb1(i)*Int0100(p1) &
                           + RRb3*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int0200(d_index) = RRb1(i)*Int0100(p1)
          end do ! i
        end do ! p1
!
! Step 6:
! [0102] from [0002], [0001]
! b+1i
!
        pd_index=0 
        do i=1,3
          do d1=1,6
            pd_index=pd_index+1
!
            d_i=Lvec_d(d1,i)
!
            Int0102(pd_index) = RRb1(i)*Int0002(d1)
            if(d_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int0102(pd_index) = Int0102(pd_index) &
                                + dble(d_i)*RRb5*Int0001(p1)
            end if
          end do ! i 
        end do ! d1
!
! Step 7:
! [0201] from [0200], [0100]
! d+1i
!
        dp_index=0
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            b_i=Lvec_d(d1,i)
!
            Int0201(dp_index) = RRd1(i)*Int0200(d1)
            if(b_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int0201(dp_index) = Int0201(dp_index) &
                                + dble(b_i)*RRd3*Int0100(p1)
            end if
          end do ! i 
        end do ! d1
!
! Step 8:
! [1102] from [0102], [0101] and [0002]
! a+1i
!
        ppd_index = 0
        do i=1,3
          do p1=1,3
            do d2=1,6
              pd_index=6*(p1-1) + d2
              ppd_index=ppd_index+1
!                
              d_i=Lvec_d(d2,i)
!
              Int1102(ppd_index) = RRa1(i)*Int0102(pd_index)
              if(d_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! (d-1i)
                p2=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int1102(ppd_index) = Int1102(ppd_index) &
                                   + dble(d_i)*RRa5*Int0101(pp_index)
              end if
              if(i.eq.p1)then
                Int1102(ppd_index) = Int1102(ppd_index) &
                                   + RRa3*Int0002(d2)
              end if
            end do ! i
          end do ! d2
        end do ! p1
!
! Step 9:
! [0202] from [0201], [0200] and [0101]
! d+1i
!
        dd_index=0
        do d1=1,6
          do p2=1,3
            i=p2 ! do xy, yy, zz first
            dd_index=dd_index+1
            dp_index=3*(d1-1)+p2
! 
            b_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int0202(dd_index) = RRd1(i)*Int0201(dp_index) &
                              + RRd5*Int0200(d1)
            if(b_i.ne.0)then ! (b-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              Int0202(dd_index) = Int0202(dd_index) &
                                + dble(b_i)*RRd3*Int0101(pp_index)
            end if
          end do ! p2
!
          do p2=2,3 ! y, z
            dp_index=3*(d1-1)+p2
            do i=1,p2-1 ! x, y
              dd_index=dd_index+1
!
! i ne p2, therefore no (b-1i) terms
!
              b_i=Lvec_d(d1,i) ! angular momentum vector
!
              Int0202(dd_index) = RRd1(i)*Int0201(dp_index)
              if(b_i.ne.0)then ! (b-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int0202(dd_index) = Int0202(dd_index) &
                                  + dble(b_i)*RRd3*Int0101(pp_index)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 10:
! [1201] from [0201], [0200] and [0101]
! a+1i
!
        dp_index=0
        do d1=1,6
          do p2=1,3
            dp_index=dp_index+1
            do i=1,3
              pdp_index=18*(i-1)+3*(d1-1)+p2 
!                
              b_i=Lvec_d(d1,i)
!
              Int1201(pdp_index) = RRa1(i)*Int0201(dp_index)
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! (b-1i)
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int1201(pdp_index) = Int1201(pdp_index) &
                                   + dble(b_i)*RRa3*Int0101(pp_index)
              end if
              if(i.eq.p2)then
                Int1201(pdp_index) = Int1201(pdp_index) &
                                   + RRa5*Int0200(d1)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 11:
! [1202] from [0202], [0201] and [0102]
! a+1i
!
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do i=1,3
              pdd_index=36*(i-1)+6*(d1-1)+d2
!
              b_i=Lvec_d(d1,i)
              d_i=Lvec_d(d2,i)
!
              Int1202(pdd_index) = RRa1(i)*Int0202(dd_index)
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p1=dot_product(v_1i,xyz)
                pd_index=6*(p1-1)+d2
                Int1202(pdd_index) = Int1202(pdd_index) &
                                   + dble(b_i)*RRa3*Int0102(pd_index)
              end if
              if(d_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! d-1i
                p2=dot_product(v_1i,xyz)
                dp_index=3*(d1-1)+p2
                Int1202(pdd_index) = Int1202(pdd_index) &
                                   + dble(d_i)*RRa5*Int0201(dp_index)
              end if
            end do ! i
          end do ! d2
        end do ! d1
!
! Step 12:
! [1212] from [1202], [1201], [0202] and [1102]
! c+1i
!
        pdd_index=0
        do p1=1,3
          do d2=1,6
            do d3=1,6
              dd_index=6*(d2-1)+d3 
              pdd_index=pdd_index+1
              do i=1,3
                pdpd_index=108*(p1-1)+18*(d2-1)+6*(i-1)+d3
!
                b_i=Lvec_d(d2,i)
                d_i=Lvec_d(d3,i)
!
                eam(pdpd_index) = RRc1(i)*Int1202(pdd_index)
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1 ! b-1i
                  p2=dot_product(v_1i,xyz)
                  ppd_index=18*(p1-1)+6*(p2-1)+d3
                  eam(pdpd_index) = eam(pdpd_index) &
                                        + dble(b_i)*RRc3*Int1102(ppd_index)
                end if
                if(d_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d3,1:3)
                  v_1i(i)=v_1i(i)-1 ! d-1i
                  p3=dot_product(v_1i,xyz)
                  pdp_index=18*(p1-1)+3*(d2-1)+p3
                  eam(pdpd_index) = eam(pdpd_index) &
                                        + dble(d_i)*RRc5*Int1201(pdp_index)
                end if
                if(i.eq.p1)then
                  eam(pdpd_index) = eam(pdpd_index) &
                                        + RRc2*Int0202(dd_index)
                end if
              end do ! i
            end do ! d3
          end do ! p2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        pdpd_index=0
        do p1=1,3
          do d2=1,6
            do p3=1,3
              do d4=1,6
                pdpd_index=pdpd_index+1
                if(d2.gt.3) eam(pdpd_index) = sq3*eam(pdpd_index)
                if(d4.gt.3) eam(pdpd_index) = sq3*eam(pdpd_index)
              end do
            end do
          end do
        end do
!
        return
      end if
!
!------------------------------------------------------------------
! [dddp]
!------------------------------------------------------------------
      if(Lcode.eq.2221)then
!
! Requires 18 steps
!
        allocate(Int1000(3), Int0100(3))
        allocate(Int0200(6), Int1100(9), Int2000(6))
        allocate(Int0210(18), Int1200(18), Int1110(27))
        allocate(Int2010(18), Int2100(18), Int1210(54))
        allocate(Int2110(54), Int2200(36), Int2210(108))
        allocate(Int1220(108), Int2120(108), Int2220(216))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2 :
! [0100] from [0000]
! b+1i
!
        do i=1,3 ! x,y,z
          Int0100(i) = RRb1(i)*G
        end do ! i 
!
! Step 3 :
! [0200] from [0100] and [0000]
! b+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int0200(d_index) = RRb1(i)*Int0100(p1) &
                           + RRb3*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int0200(d_index) = RRb1(i)*Int0100(p1)
          end do ! i
        end do ! p1
!
! Step 4:
! [1100] from [1000] and [0000]
! b+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int1100(pp_index) = RRb1(i)*Int1000(p1)
            if(p1.eq.i)then
              Int1100(pp_index) = Int1100(pp_index) &
                                + RRb2*G
            end if
          end do ! i
        end do ! p1
!
! Step 5 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xy-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int2000(d_index) = RRa1(i)*Int1000(p1) &
                           + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int2000(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! Step 6:
! [0210] from [0200], [0100]
! c+1i
!
        dp_index=0
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            b_i=Lvec_d(d1,i)
!
            Int0210(dp_index) = RRc1(i)*Int0200(d1)
            if(b_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int0210(dp_index) = Int0210(dp_index) &
                                + dble(b_i)*RRc3*Int0100(p1)
            end if
          end do ! i 
        end do ! d1
!
! Step 7:
! [1200] from [0200], [0100]
! a+1i
!
        pd_index=0
        do i=1,3
          do d1=1,6
            pd_index=pd_index+1
!
            b_i=Lvec_d(d1,i)
!
            Int1200(pd_index) = RRa1(i)*Int0200(d1)
            if(b_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int1200(pd_index) = Int1200(pd_index) &
                                + dble(b_i)*RRa3*Int0100(p1)
            end if
          end do ! i 
        end do ! d1
!
! Step 8:
! [1110] from [1100], [1000] and [0100]
! c+1i
!
        ppp_index=0
        pp_index=0
        do p1=1,3
          do p2=1,3
            pp_index=pp_index+1
            do i=1,3
              ppp_index=ppp_index+1
!
              Int1110(ppp_index) = RRc1(i)*Int1100(pp_index)
              if(i.eq.p1)then
                Int1110(ppp_index) = Int1110(ppp_index) &
                                   + RRc2*Int0100(p2)
              end if
              if(i.eq.p2)then
                Int1110(ppp_index) = Int1110(ppp_index) &
                                   + RRc3*Int1000(p1)
              end if
            end do ! i
          end do ! p2
        end do ! p1
!
! Step 9:
! [2010] from [2000] and [1000]
! c+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i)
!
            Int2010(dp_index) = RRc1(i)*Int2000(d1)
            if(a_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int2010(dp_index) = Int2010(dp_index) &
                                + dble(a_i)*RRc2*Int1000(p1)
            end if
          end do ! i
        end do ! d1 
!
! Step 10:
! [2100] from [2000] and [1000]
! b+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i)
!
            Int2100(dp_index) = RRb1(i)*Int2000(d1)
            if(a_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int2100(dp_index) = Int2100(dp_index) &
                                + dble(a_i)*RRb2*Int1000(p1)
            end if
          end do ! i
        end do ! d1 
!
! Step 11:
! [1210] from [1200], [1100] and [0200]
! c+1i
!
        pd_index=0
        pdp_index=0
        do p1=1,3
          do d2=1,6
            pd_index=pd_index+1
            do i=1,3
              pdp_index=pdp_index+1
!
              b_i=Lvec_d(d2,i)
!
              Int1210(pdp_index) = RRc1(i)*Int1200(pd_index)
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p2=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int1210(pdp_index) = Int1210(pdp_index) &
                                   + dble(b_i)*RRc3*Int1100(pp_index)
              end if
              if(i.eq.p1)then
                Int1210(pdp_index) = Int1210(pdp_index) &
                                   + RRc2*Int0200(d2)
              end if
            end do ! i
          end do ! d2
        end do ! p1
!
! Step 12:
! [2110] from [2100], [2000] and [1100]
! c+1i
!
        dp_index=0
        dpp_index=0
        do d1=1,6
          do p2=1,3
            dp_index=dp_index+1
            do i=1,3
              dpp_index=dpp_index+1
!
              a_i=Lvec_d(d1,i)
!
              Int2110(dpp_index) = RRc1(i)*Int2100(dp_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2110(dpp_index) = Int2110(dpp_index) &
                                   + dble(a_i)*RRc2*Int1100(pp_index)
              end if
              if(i.eq.p2)then
                Int2110(dpp_index) = Int2110(dpp_index) &
                                   + RRc3*Int2000(d1)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 13:
! [2200] from [2100], [2000] and [1100]
! b+1i
!
        dd_index=0
        do d1=1,6
          do p2=1,3
            i=p2
            dp_index=3*(d1-1)+p2
            dd_index=dd_index+1
!
            a_i=Lvec_d(d1,i)
!
            Int2200(dd_index) = RRb1(i)*Int2100(dp_index) &
                              + RRb3*Int2000(d1)
            if(a_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1i
              p1=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              Int2200(dd_index) = Int2200(dd_index) &
                                + dble(a_i)*RRb2*Int1100(pp_index)
            end if
          end do ! p2
!
          do p2=2,3
            dp_index=3*(d1-1)+p2
            do i=1,p2-1
              dd_index=dd_index+1
!
              a_i=Lvec_d(d1,i)
!
              Int2200(dd_index) = RRb1(i)*Int2100(dp_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2200(dd_index) = Int2200(dd_index) &
                                  + dble(a_i)*RRb2*Int1100(pp_index)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 14:
! [1220] from [1210], [1200], [1110] and [0210]
! c+1i
!
        pdd_index=0
        pd_index=0
        do p1=1,3
          do d2=1,6
            pd_index=pd_index+1
            do p3=1,3
              i=p3
              pdp_index=18*(p1-1)+3*(d2-1)+p3
              pdd_index=pdd_index+1
!
              b_i=Lvec_d(d2,i)
!
              Int1220(pdd_index) = RRc1(i)*Int1210(pdp_index) &
                                 + RRc4*Int1200(pd_index)
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p2=dot_product(v_1i,xyz)
                ppp_index=9*(p1-1)+3*(p2-1)+p3
                Int1220(pdd_index) = Int1220(pdd_index) &
                                   + dble(b_i)*RRc3*Int1110(ppp_index)
              end if
              if(i.eq.p1)then
                dp_index=3*(d2-1)+p3
                Int1220(pdd_index) = Int1220(pdd_index) &
                                   + RRc2*Int0210(dp_index)
              end if
            end do ! p3
!
            do p3=2,3
              pdp_index=18*(p1-1)+3*(d2-1)+p3
              do i=1,p3-1
                pdd_index=pdd_index+1
!
                b_i=Lvec_d(d2,i)
!
                Int1220(pdd_index) = RRc1(i)*Int1210(pdp_index)
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1 ! b-1i
                  p2=dot_product(v_1i,xyz)
                  ppp_index=9*(p1-1)+3*(p2-1)+p3
                  Int1220(pdd_index) = Int1220(pdd_index) &
                                     + dble(b_i)*RRc3*Int1110(ppp_index)
                end if
                if(i.eq.p1)then
                  dp_index=3*(d2-1)+p3
                  Int1220(pdd_index) = Int1220(pdd_index) &
                                     + RRc2*Int0210(dp_index)
                end if
              end do ! i
            end do ! p3
          end do ! d2
        end do ! p1
!
! Step 15:
! [2120] from [2110], [2100], [1110] and [2010]
! c+1i
!
        dpd_index=0
        dp2_index=0
        do d1=1,6
          do p2=1,3
            dp2_index=dp2_index+1
            do p3=1,3
              i=p3
              dpp_index=9*(d1-1)+3*(p2-1)+p3
              dpd_index=dpd_index+1
!
              a_i=Lvec_d(d1,i)
!
              Int2120(dpd_index) = RRc1(i)*Int2110(dpp_index) &
                                 + RRc4*Int2100(dp2_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                ppp_index=9*(p1-1)+3*(p2-1)+p3
                Int2120(dpd_index) = Int2120(dpd_index) &
                                   + dble(a_i)*RRc2*Int1110(ppp_index)
              end if
              if(i.eq.p2)then
                dp3_index=3*(d1-1)+p3
                Int2120(dpd_index) = Int2120(dpd_index) &
                                   + RRc3*Int2010(dp3_index)
              end if
            end do ! p3
!
            do p3=2,3
              dpp_index=9*(d1-1)+3*(p2-1)+p3
              do i=1,p3-1
                dpd_index=dpd_index+1
!
                a_i=Lvec_d(d1,i)
!
                Int2120(dpd_index) = RRc1(i)*Int2110(dpp_index)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! a-1i
                  p1=dot_product(v_1i,xyz)
                  ppp_index=9*(p1-1)+3*(p2-1)+p3
                  Int2120(dpd_index) = Int2120(dpd_index) &
                                     + dble(a_i)*RRc2*Int1110(ppp_index)
                end if
                if(i.eq.p2)then
                  dp3_index=3*(d1-1)+p3
                  Int2120(dpd_index) = Int2120(dpd_index) &
                                     + RRc3*Int2010(dp3_index)
                end if
              end do ! i
            end do ! p3
          end do ! d2
        end do ! p1
!
! Step 16:
! [2210] from [2200], [2100] and [1200]
! c+1i
!
        ddp_index=0
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do i=1,3
              ddp_index=ddp_index+1
!
              a_i=Lvec_d(d1,i)
              b_i=Lvec_d(d2,i)
!
              Int2210(ddp_index) = RRc1(i)*Int2200(dd_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pd_index=6*(p1-1)+d2
                Int2210(ddp_index) = Int2210(ddp_index) &
                                   + dble(a_i)*RRc2*Int1200(pd_index)
              end if
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p2=dot_product(v_1i,xyz)
                dp_index=3*(d1-1)+p2
                Int2210(ddp_index) = Int2210(ddp_index) &
                                   + dble(b_i)*RRc3*Int2100(dp_index)
              end if
            end do ! i
          end do ! d2
        end do ! d1
!
! Step 17:
! [2220] from [2210], [2200], [2110], [1210]
! c+1i
!
        ddd_index=0
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do p3=1,3
              i=p3
              ddd_index=ddd_index+1
              ddp_index=18*(d1-1)+3*(d2-1)+p3
!
              a_i=Lvec_d(d1,i)
              b_i=Lvec_d(d2,i)
!
              Int2220(ddd_index) = RRc1(i)*Int2210(ddp_index) &
                                 + RRc4*Int2200(dd_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pdp_index=18*(p1-1)+3*(d2-1)+p3
                Int2220(ddd_index) = Int2220(ddd_index) &
                                   + dble(a_i)*RRc2*Int1210(pdp_index)
              end if
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p2=dot_product(v_1i,xyz)
                dpp_index=9*(d1-1)+3*(p2-1)+p3
                Int2220(ddd_index) = Int2220(ddd_index) &
                                   + dble(b_i)*RRc3*Int2110(dpp_index)
              end if
            end do ! p3
!
            do p3=2,3
              ddp_index=18*(d1-1)+3*(d2-1)+p3
              do i=1,p3-1
                ddd_index=ddd_index+1
!
                a_i=Lvec_d(d1,i)
                b_i=Lvec_d(d2,i)
!
                Int2220(ddd_index) = RRc1(i)*Int2210(ddp_index)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! a-1i
                  p1=dot_product(v_1i,xyz)
                  pdp_index=18*(p1-1)+3*(d2-1)+p3
                  Int2220(ddd_index) = Int2220(ddd_index) &
                                     + dble(a_i)*RRc2*Int1210(pdp_index)
                end if
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1 ! b-1i
                  p2=dot_product(v_1i,xyz)
                  dpp_index=9*(d1-1)+3*(p2-1)+p3
                  Int2220(ddd_index) = Int2220(ddd_index) &
                                     + dble(b_i)*RRc3*Int2110(dpp_index)
                end if
              end do ! i
            end do ! p3
          end do ! d2
        end do ! d1
!
! Step 18:
! [2221] from [2220], [2210], [2120], [1220]
! d+1i
!
        ddd_index=0
        dddp_index=0
        do d1=1,6
          do d2=1,6
            do d3=1,6
              ddd_index=ddd_index+1
              do i=1,3
                dddp_index=dddp_index+1
!
                a_i=Lvec_d(d1,i)
                b_i=Lvec_d(d2,i)
                c_i=Lvec_d(d3,i)
!
                eam(dddp_index) = RRd1(i)*Int2220(ddd_index)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! a-1i
                  p1=dot_product(v_1i,xyz)
                  pdd_index=36*(p1-1)+6*(d2-1)+d3
                  eam(dddp_index) = eam(dddp_index) &
                                       + dble(a_i)*RRd2*Int1220(pdd_index)
                end if
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1 ! b-1i
                  p2=dot_product(v_1i,xyz)
                  dpd_index=18*(d1-1)+6*(p2-1)+d3
                  eam(dddp_index) = eam(dddp_index) &
                                       + dble(b_i)*RRd3*Int2120(dpd_index)
                end if
                if(c_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d3,1:3)
                  v_1i(i)=v_1i(i)-1 ! c-1i
                  p3=dot_product(v_1i,xyz)
                  ddp_index=18*(d1-1)+3*(d2-1)+p3
                  eam(dddp_index) = eam(dddp_index) &
                                       + dble(c_i)*RRd4*Int2210(ddp_index)
                end if
              end do ! i
            end do ! d3
          end do ! d2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        dddp_index=0
        do d1=1,6
          do d2=1,6
            do d3=1,6
              do p4=1,3
                dddp_index=dddp_index+1
                if(d1.gt.3) eam(dddp_index) = sq3*eam(dddp_index)
                if(d2.gt.3) eam(dddp_index) = sq3*eam(dddp_index)
                if(d3.gt.3) eam(dddp_index) = sq3*eam(dddp_index)
              end do
            end do
          end do
        end do
!
        return
      end if
!
!------------------------------------------------------------------
! [ddpd]
!------------------------------------------------------------------
!
      if(Lcode.eq.2212)then
!
! Requires 18 steps
!
        allocate(Int1000(3), Int0100(3))
        allocate(Int0200(6), Int1100(9), Int2000(6))
        allocate(Int0201(18), Int1200(18), Int1101(27))
        allocate(Int2001(18), Int2100(18), Int1201(54))
        allocate(Int2101(54), Int2200(36), Int2201(108))
        allocate(Int1202(108), Int2102(108), Int2202(216))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2 :
! [0100] from [0000]
! b+1i
!
        do i=1,3 ! x,y,z
          Int0100(i) = RRb1(i)*G
        end do ! i 
!
! Step 3 :
! [0200] from [0100] and [0000]
! b+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int0200(d_index) = RRb1(i)*Int0100(p1) &
                           + RRb3*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int0200(d_index) = RRb1(i)*Int0100(p1)
          end do ! i
        end do ! p1
!
! Step 4:
! [1100] from [1000] and [0000]
! b+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int1100(pp_index) = RRb1(i)*Int1000(p1)
            if(p1.eq.i)then
              Int1100(pp_index) = Int1100(pp_index) &
                                + RRb2*G
            end if
          end do ! i
        end do ! p1
!
! Step 5 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xy-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int2000(d_index) = RRa1(i)*Int1000(p1) &
                           + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int2000(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! Step 6:
! [0201] from [0200], [0100]
! d+1i
!
        dp_index=0
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            b_i=Lvec_d(d1,i)
!
            Int0201(dp_index) = RRd1(i)*Int0200(d1)
            if(b_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int0201(dp_index) = Int0201(dp_index) &
                                + dble(b_i)*RRd3*Int0100(p1)
            end if
          end do ! i 
        end do ! d1
!
! Step 7:
! [1200] from [0200], [0100]
! a+1i
!
        pd_index = 0
        do i=1,3
          do d1=1,6
            pd_index=pd_index+1
!
            b_i=Lvec_d(d1,i)
!
            Int1200(pd_index) = RRa1(i)*Int0200(d1)
            if(b_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int1200(pd_index) = Int1200(pd_index) &
                                + dble(b_i)*RRa3*Int0100(p1)
            end if
          end do ! d1 
        end do ! i
!
! Step 8:
! [1101] from [1100], [1000] and [0100]
! d+1i
!
        ppp_index=0
        pp_index=0
        do p1=1,3
          do p2=1,3
            pp_index=pp_index+1
            do i=1,3
              ppp_index=ppp_index+1
!
              Int1101(ppp_index) = RRd1(i)*Int1100(pp_index)
              if(i.eq.p1)then
                Int1101(ppp_index) = Int1101(ppp_index) &
                                   + RRd2*Int0100(p2)
              end if
              if(i.eq.p2)then
                Int1101(ppp_index) = Int1101(ppp_index) &
                                   + RRd3*Int1000(p1)
              end if
            end do ! i
          end do ! p2
        end do ! p1
!
! Step 9:
! [2001] from [2000] and [1000]
! d+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i)
!
            Int2001(dp_index) = RRd1(i)*Int2000(d1)
            if(a_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int2001(dp_index) = Int2001(dp_index) &
                                + dble(a_i)*RRd2*Int1000(p1)
            end if
          end do ! i
        end do ! d1 
!
! Step 10:
! [2100] from [2000] and [1000]
! b+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i)
!
            Int2100(dp_index) = RRb1(i)*Int2000(d1)
            if(a_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int2100(dp_index) = Int2100(dp_index) &
                                + dble(a_i)*RRb2*Int1000(p1)
            end if
          end do ! i
        end do ! d1 
!
! Step 11:
! [1201] from [1200], [1100] and [0200]
! d+1i
!
        pd_index=0
        pdp_index=0
        do p1=1,3
          do d2=1,6
            pd_index=pd_index+1
            do i=1,3
              pdp_index=pdp_index+1
!
              b_i=Lvec_d(d2,i)
!
              Int1201(pdp_index) = RRd1(i)*Int1200(pd_index)
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p2=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int1201(pdp_index) = Int1201(pdp_index) &
                                   + dble(b_i)*RRd3*Int1100(pp_index)
              end if
              if(i.eq.p1)then
                Int1201(pdp_index) = Int1201(pdp_index) &
                                   + RRd2*Int0200(d2)
              end if
            end do ! i
          end do ! d2
        end do ! p1
!
! Step 12:
! [2101] from [2100], [2000] and [1100]
! d+1i
!
        dp_index=0
        dpp_index=0
        do d1=1,6
          do p2=1,3
            dp_index=dp_index+1
            do i=1,3
              dpp_index=dpp_index+1
!
              a_i=Lvec_d(d1,i)
!
              Int2101(dpp_index) = RRd1(i)*Int2100(dp_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2101(dpp_index) = Int2101(dpp_index) &
                                   + dble(a_i)*RRd2*Int1100(pp_index)
              end if
              if(i.eq.p2)then
                Int2101(dpp_index) = Int2101(dpp_index) &
                                   + RRd3*Int2000(d1)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 13:
! [2200] from [2100], [2000] and [1100]
! b+1i
!
        dd_index=0
        do d1=1,6
          do p2=1,3
            i=p2
            dp_index=3*(d1-1)+p2
            dd_index=dd_index+1
!
            a_i=Lvec_d(d1,i)
!
            Int2200(dd_index) = RRb1(i)*Int2100(dp_index) &
                              + RRb3*Int2000(d1)
            if(a_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1i
              p1=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              Int2200(dd_index) = Int2200(dd_index) &
                                + dble(a_i)*RRb2*Int1100(pp_index)
            end if
          end do ! p2
!
          do p2=2,3
            dp_index=3*(d1-1)+p2
            do i=1,p2-1
              dd_index=dd_index+1
!
              a_i=Lvec_d(d1,i)
!
              Int2200(dd_index) = RRb1(i)*Int2100(dp_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2200(dd_index) = Int2200(dd_index) &
                                  + dble(a_i)*RRb2*Int1100(pp_index)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 14:
! [1202] from [1201], [1200], [1101] and [0201]
! d+1i
!
        pdd_index=0
        pd_index=0
        do p1=1,3
          do d2=1,6
            pd_index=pd_index+1
            do p3=1,3
              i=p3
              pdp_index=18*(p1-1)+3*(d2-1)+p3
              pdd_index=pdd_index+1
!
              b_i=Lvec_d(d2,i)
!
              Int1202(pdd_index) = RRd1(i)*Int1201(pdp_index) &
                                 + RRd5*Int1200(pd_index)
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p2=dot_product(v_1i,xyz)
                ppp_index=9*(p1-1)+3*(p2-1)+p3
                Int1202(pdd_index) = Int1202(pdd_index) &
                                   + dble(b_i)*RRd3*Int1101(ppp_index)
              end if
              if(i.eq.p1)then
                dp_index=3*(d2-1)+p3
                Int1202(pdd_index) = Int1202(pdd_index) &
                                   + RRd2*Int0201(dp_index)
              end if
            end do ! p3
!
            do p3=2,3
              pdp_index=18*(p1-1)+3*(d2-1)+p3
              do i=1,p3-1
                pdd_index=pdd_index+1
!
                b_i=Lvec_d(d2,i)
!
                Int1202(pdd_index) = RRd1(i)*Int1201(pdp_index)
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1 ! b-1i
                  p2=dot_product(v_1i,xyz)
                  ppp_index=9*(p1-1)+3*(p2-1)+p3
                  Int1202(pdd_index) = Int1202(pdd_index) &
                                     + dble(b_i)*RRd3*Int1101(ppp_index)
                end if
                if(i.eq.p1)then
                  dp_index=3*(d2-1)+p3
                  Int1202(pdd_index) = Int1202(pdd_index) &
                                     + RRd2*Int0201(dp_index)
                end if
              end do ! i
            end do ! p3
          end do ! d2
        end do ! p1
!
! Step 15:
! [2102] from [2101], [2100], [1101] and [2001]
! d+1i
!
        dpd_index=0
        dp2_index=0
        do d1=1,6
          do p2=1,3
            dp2_index=dp2_index+1
            do p3=1,3
              i=p3
              dpp_index=9*(d1-1)+3*(p2-1)+p3
              dpd_index=dpd_index+1
!
              a_i=Lvec_d(d1,i)
!
              Int2102(dpd_index) = RRd1(i)*Int2101(dpp_index) &
                                 + RRd5*Int2100(dp2_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                ppp_index=9*(p1-1)+3*(p2-1)+p3
                Int2102(dpd_index) = Int2102(dpd_index) &
                                   + dble(a_i)*RRd2*Int1101(ppp_index)
              end if
              if(i.eq.p2)then
                dp3_index=3*(d1-1)+p3
                Int2102(dpd_index) = Int2102(dpd_index) &
                                   + RRd3*Int2001(dp3_index)
              end if
            end do ! p3
!
            do p3=2,3
              dpp_index=9*(d1-1)+3*(p2-1)+p3
              do i=1,p3-1
                dpd_index=dpd_index+1
!
                a_i=Lvec_d(d1,i)
!
                Int2102(dpd_index) = RRd1(i)*Int2101(dpp_index)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! a-1i
                  p1=dot_product(v_1i,xyz)
                  ppp_index=9*(p1-1)+3*(p2-1)+p3
                  Int2102(dpd_index) = Int2102(dpd_index) &
                                     + dble(a_i)*RRd2*Int1101(ppp_index)
                end if
                if(i.eq.p2)then
                  dp3_index=3*(d1-1)+p3
                  Int2102(dpd_index) = Int2102(dpd_index) &
                                     + RRd3*Int2001(dp3_index)
                end if
              end do ! i
            end do ! p3
          end do ! d2
        end do ! p1
!
! Step 16:
! [2201] from [2200], [2100] and [1200]
! d+1i
!
        ddp_index=0
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do i=1,3
              ddp_index=ddp_index+1
!
              a_i=Lvec_d(d1,i)
              b_i=Lvec_d(d2,i)
!
              Int2201(ddp_index) = RRd1(i)*Int2200(dd_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pd_index=6*(p1-1)+d2
                Int2201(ddp_index) = Int2201(ddp_index) &
                                   + dble(a_i)*RRd2*Int1200(pd_index)
              end if
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p2=dot_product(v_1i,xyz)
                dp_index=3*(d1-1)+p2
                Int2201(ddp_index) = Int2201(ddp_index) &
                                   + dble(b_i)*RRd3*Int2100(dp_index)
              end if
            end do ! i
          end do ! d2
        end do ! d1
!
! Step 17:
! [2202] from [2201], [2200], [2101], [1201]
! d+1i
!
        ddd_index=0
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do p3=1,3
              i=p3
              ddd_index=ddd_index+1
              ddp_index=18*(d1-1)+3*(d2-1)+p3
!
              a_i=Lvec_d(d1,i)
              b_i=Lvec_d(d2,i)
!
              Int2202(ddd_index) = RRd1(i)*Int2201(ddp_index) &
                                 + RRd5*Int2200(dd_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pdp_index=18*(p1-1)+3*(d2-1)+p3
                Int2202(ddd_index) = Int2202(ddd_index) &
                                   + dble(a_i)*RRd2*Int1201(pdp_index)
              end if
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p2=dot_product(v_1i,xyz)
                dpp_index=9*(d1-1)+3*(p2-1)+p3
                Int2202(ddd_index) = Int2202(ddd_index) &
                                   + dble(b_i)*RRd3*Int2101(dpp_index)
              end if
            end do ! p3
!
            do p3=2,3
              ddp_index=18*(d1-1)+3*(d2-1)+p3
              do i=1,p3-1
                ddd_index=ddd_index+1
!
                a_i=Lvec_d(d1,i)
                b_i=Lvec_d(d2,i)
!
                Int2202(ddd_index) = RRd1(i)*Int2201(ddp_index)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! a-1i
                  p1=dot_product(v_1i,xyz)
                  pdp_index=18*(p1-1)+3*(d2-1)+p3
                  Int2202(ddd_index) = Int2202(ddd_index) &
                                     + dble(a_i)*RRd2*Int1201(pdp_index)
                end if
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1 ! b-1i
                  p2=dot_product(v_1i,xyz)
                  dpp_index=9*(d1-1)+3*(p2-1)+p3
                  Int2202(ddd_index) = Int2202(ddd_index) &
                                     + dble(b_i)*RRd3*Int2101(dpp_index)
                end if
              end do ! i
            end do ! p3
          end do ! d2
        end do ! d1
!
! Step 18:
! [2212] from [2202], [2201], [2102], [1202]
! c+1i
!
        ddd_index=0
        do d1=1,6
          do d2=1,6
            do d3=1,6
              ddd_index=ddd_index+1
              do i=1,3
                ddpd_index=108*(d1-1)+18*(d2-1)+6*(i-1)+d3
!
                a_i=Lvec_d(d1,i)
                b_i=Lvec_d(d2,i)
                d_i=Lvec_d(d3,i)
!
                eam(ddpd_index) = RRc1(i)*Int2202(ddd_index)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! a-1i
                  p1=dot_product(v_1i,xyz)
                  pdd_index=36*(p1-1)+6*(d2-1)+d3
                  eam(ddpd_index) = eam(ddpd_index) &
                                       + dble(a_i)*RRc2*Int1202(pdd_index)
                end if
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1 ! b-1i
                  p2=dot_product(v_1i,xyz)
                  dpd_index=18*(d1-1)+6*(p2-1)+d3
                  eam(ddpd_index) = eam(ddpd_index) &
                                        + dble(b_i)*RRc3*Int2102(dpd_index)
                end if
                if(d_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d3,1:3)
                  v_1i(i)=v_1i(i)-1 ! d-1i
                  p3=dot_product(v_1i,xyz)
                  ddp_index=18*(d1-1)+3*(d2-1)+p3
                  eam(ddpd_index) = eam(ddpd_index) &
                                       + dble(d_i)*RRc5*Int2201(ddp_index)
                end if
              end do ! i
            end do ! d3
          end do ! d2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        ddpd_index=0
        do d1=1,6
          do d2=1,6
            do p3=1,3
              do d4=1,6
                ddpd_index=ddpd_index+1
                if(d1.gt.3) eam(ddpd_index) = sq3*eam(ddpd_index)
                if(d2.gt.3) eam(ddpd_index) = sq3*eam(ddpd_index)
                if(d4.gt.3) eam(ddpd_index) = sq3*eam(ddpd_index)
              end do
            end do
          end do
        end do
!
        return
      end if
!
!------------------------------------------------------------------
! [pddd]
!------------------------------------------------------------------
      if(Lcode.eq.1222)then
!
! Requires 18 steps
!
        allocate(Int0010(3), Int0100(3))
        allocate(Int0200(6), Int0110(9), Int0020(6))
        allocate(Int0201(18), Int0210(18), Int0111(27))
        allocate(Int0021(18), Int0120(18), Int0211(54))
        allocate(Int0121(54), Int0220(36), Int0221(108))
        allocate(Int0212(108), Int0122(108), Int0222(216))
!
! Step 1 :
! [0010] from [0000]
! c+1i
!
        do i=1,3 ! x,y,z
          Int0010(i) = RRc1(i)*G
        end do ! i 
!
! Step 2 :
! [0100] from [0000]
! b+1i
!
        do i=1,3 ! x,y,z
          Int0100(i) = RRb1(i)*G
        end do ! i 
!
! Step 3 :
! [0200] from [0100] and [0000]
! b+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int0200(d_index) = RRb1(i)*Int0100(p1) &
                           + RRb3*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int0200(d_index) = RRb1(i)*Int0100(p1)
          end do ! i
        end do ! p1
!
! Step 4:
! [0110] from [0010] and [0000]
! b+1i
!
        pp_index=0
        do i=1,3 ! x, y, z
          do p1=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int0110(pp_index) = RRb1(i)*Int0010(p1)
            if(p1.eq.i)then
              Int0110(pp_index) = Int0110(pp_index) &
                                + RRb4*G
            end if
          end do ! i
        end do ! p1
!
! Step 5 :
! [0020] from [0010] and [0000]
! c+1i
!
! Note: d's are numbered xy-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int0020(d_index) = RRc1(i)*Int0010(p1) &
                           + RRc4*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int0020(d_index) = RRc1(i)*Int0010(p1)
          end do ! i
        end do ! p1
!
! Step 6:
! [0201] from [0200], [0100]
! d+1i
!
        dp_index=0
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            b_i=Lvec_d(d1,i)
!
            Int0201(dp_index) = RRd1(i)*Int0200(d1)
            if(b_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int0201(dp_index) = Int0201(dp_index) &
                                + dble(b_i)*RRd3*Int0100(p1)
            end if
          end do ! i 
        end do ! d1
!
! Step 7:
! [0210] from [0200], [0100]
! c+1i
!
        dp_index = 0
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            b_i=Lvec_d(d1,i)
!
            Int0210(dp_index) = RRc1(i)*Int0200(d1)
            if(b_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int0210(dp_index) = Int0210(dp_index) &
                                + dble(b_i)*RRc3*Int0100(p1)
            end if
          end do ! i 
        end do ! d1
!
! Step 8:
! [0111] from [0110], [0010] and [0100]
! d+1i
!
        ppp_index=0
        pp_index=0
        do p1=1,3
          do p2=1,3
            pp_index=pp_index+1
            do i=1,3
              ppp_index=ppp_index+1
!
              Int0111(ppp_index) = RRd1(i)*Int0110(pp_index)
              if(i.eq.p1)then
                Int0111(ppp_index) = Int0111(ppp_index) &
                                   + RRd3*Int0010(p2)
              end if
              if(i.eq.p2)then
                Int0111(ppp_index) = Int0111(ppp_index) &
                                   + RRd4*Int0100(p1)
              end if
            end do ! i
          end do ! p2
        end do ! p1
!
! Step 9:
! [0021] from [0020] and [0010]
! d+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            c_i=Lvec_d(d1,i)
!
            Int0021(dp_index) = RRd1(i)*Int0020(d1)
            if(c_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int0021(dp_index) = Int0021(dp_index) &
                                + dble(c_i)*RRd4*Int0010(p1)
            end if
          end do ! i
        end do ! d1 
!
! Step 10:
! [0120] from [0020] and [0010]
! b+1i
!
        pd_index=0 
        do i=1,3
          do d1=1,6
            pd_index=pd_index+1
!
            c_i=Lvec_d(d1,i)
!
            Int0120(pd_index) = RRb1(i)*Int0020(d1)
            if(c_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int0120(pd_index) = Int0120(pd_index) &
                                + dble(c_i)*RRb4*Int0010(p1)
            end if
          end do ! i
        end do ! d1 
!
! Step 11:
! [0211] from [0210], [0110] and [0200]
! d+1i
!
        dp_index=0
        dpp_index=0
        do d1=1,6
          do p2=1,3
            dp_index=dp_index+1
            do i=1,3
              dpp_index=dpp_index+1
!
              b_i=Lvec_d(d1,i)
!
              Int0211(dpp_index) = RRd1(i)*Int0210(dp_index)
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int0211(dpp_index) = Int0211(dpp_index) &
                                   + dble(b_i)*RRd3*Int0110(pp_index)
              end if
              if(i.eq.p2)then
                Int0211(dpp_index) = Int0211(dpp_index) &
                                   + RRd4*Int0200(d1)
              end if
            end do ! i
          end do ! d2
        end do ! p1
!
! Step 12:
! [0121] from [0120], [0020] and [0110]
! d+1i
!
        pd_index=0
        pdp_index=0
        do p1=1,3
          do d2=1,6
            pd_index=pd_index+1
            do i=1,3
              pdp_index=pdp_index+1
!
              c_i=Lvec_d(d2,i)
!
              Int0121(pdp_index) = RRd1(i)*Int0120(pd_index)
              if(c_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p2=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int0121(pdp_index) = Int0121(pdp_index) &
                                   + dble(c_i)*RRd4*Int0110(pp_index)
              end if
              if(i.eq.p1)then
                Int0121(pdp_index) = Int0121(pdp_index) &
                                   + RRd3*Int0020(d2)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 13:
! [0220] from [0120], [0020] and [0110]
! b+1i
!
        dd_index=0
        do d2=1,6
          d1=0
          do p1=1,3
            i=p1
            d1=d1+1
            pd_index=6*(p1-1)+d2
            dd_index=6*(d1-1)+d2
!
            c_i=Lvec_d(d2,i)
!
            Int0220(dd_index) = RRb1(i)*Int0120(pd_index) &
                              + RRb3*Int0020(d2)
            if(c_i.ne.0)then
              v_1i(1:3)=Lvec_d(d2,1:3)
              v_1i(i)=v_1i(i)-1 ! c-1i
              p2=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              Int0220(dd_index) = Int0220(dd_index) &
                                + dble(c_i)*RRb4*Int0110(pp_index)
            end if
          end do ! p2
!
          do p1=2,3
            pd_index=6*(p1-1)+d2
            do i=1,p1-1
              d1=d1+1
              dd_index=6*(d1-1)+d2
!
              c_i=Lvec_d(d2,i)
!
              Int0220(dd_index) = RRb1(i)*Int0120(pd_index)
              if(c_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! c-1i
                p2=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int0220(dd_index) = Int0220(dd_index) &
                                  + dble(c_i)*RRb4*Int0110(pp_index)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 14:
! [0212] from [0211], [0210], [0111] and [0201]
! d+1i
!
        dpd_index=0
        dp2_index=0
        do d1=1,6
          do p2=1,3
            dp2_index=dp2_index+1
            do p3=1,3
              i=p3
              dpp_index=9*(d1-1)+3*(p2-1)+p3
              dpd_index=dpd_index+1
!
              b_i=Lvec_d(d1,i)
!
              Int0212(dpd_index) = RRd1(i)*Int0211(dpp_index) &
                                 + RRd5*Int0210(dp2_index)
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p1=dot_product(v_1i,xyz)
                ppp_index=9*(p1-1)+3*(p2-1)+p3
                Int0212(dpd_index) = Int0212(dpd_index) &
                                   + dble(b_i)*RRd3*Int0111(ppp_index)
              end if
              if(i.eq.p2)then
                dp3_index=3*(d1-1)+p3
                Int0212(dpd_index) = Int0212(dpd_index) &
                                   + RRd4*Int0201(dp3_index)
              end if
            end do ! p3
!
            do p3=2,3
              dpp_index=9*(d1-1)+3*(p2-1)+p3
              do i=1,p3-1
                dpd_index=dpd_index+1
!
                b_i=Lvec_d(d1,i)
!
                Int0212(dpd_index) = RRd1(i)*Int0211(dpp_index)
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! b-1i
                  p1=dot_product(v_1i,xyz)
                  ppp_index=9*(p1-1)+3*(p2-1)+p3
                  Int0212(dpd_index) = Int0212(dpd_index) &
                                     + dble(b_i)*RRd3*Int0111(ppp_index)
                end if
                if(i.eq.p2)then
                  dp3_index=3*(d1-1)+p3
                  Int0212(dpd_index) = Int0212(dpd_index) &
                                     + RRd4*Int0201(dp3_index)
                end if
              end do ! i
            end do ! p3
          end do ! d2
        end do ! p1
!
! Step 15:
! [0122] from [0121], [0120], [0111] and [0021]
! d+1i
!
        pdd_index=0
        pd_index=0
        do p1=1,3
          do d2=1,6
            pd_index=pd_index+1
            do p3=1,3
              i=p3
              pdp_index=18*(p1-1)+3*(d2-1)+p3
              pdd_index=pdd_index+1
!
              c_i=Lvec_d(d2,i)
!
              Int0122(pdd_index) = RRd1(i)*Int0121(pdp_index) &
                                 + RRd5*Int0120(pd_index)
              if(c_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! c-1i
                p2=dot_product(v_1i,xyz)
                ppp_index=9*(p1-1)+3*(p2-1)+p3
                Int0122(pdd_index) = Int0122(pdd_index) &
                                   + dble(c_i)*RRd4*Int0111(ppp_index)
              end if
              if(i.eq.p1)then
                dp_index=3*(d2-1)+p3
                Int0122(pdd_index) = Int0122(pdd_index) &
                                   + RRd3*Int0021(dp_index)
              end if
            end do ! p3
!
            do p3=2,3
              pdp_index=18*(p1-1)+3*(d2-1)+p3
              do i=1,p3-1
                pdd_index=pdd_index+1
!
                c_i=Lvec_d(d2,i)
!
                Int0122(pdd_index) = RRd1(i)*Int0121(pdp_index)
                if(c_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1 ! c-1i
                  p2=dot_product(v_1i,xyz)
                  ppp_index=9*(p1-1)+3*(p2-1)+p3
                  Int0122(pdd_index) = Int0122(pdd_index) &
                                     + dble(c_i)*RRd4*Int0111(ppp_index)
                end if
                if(i.eq.p1)then
                  dp_index=3*(d2-1)+p3
                  Int0122(pdd_index) = Int0122(pdd_index) &
                                     + RRd3*Int0021(dp_index)
                end if
              end do ! i
            end do ! p3
          end do ! d2
        end do ! p1
!
! Step 16:
! [0221] from [0220], [0120] and [0210]
! d+1i
!
        ddp_index=0
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do i=1,3
              ddp_index=ddp_index+1
!
              b_i=Lvec_d(d1,i)
              c_i=Lvec_d(d2,i)
!
              Int0221(ddp_index) = RRd1(i)*Int0220(dd_index)
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p1=dot_product(v_1i,xyz)
                pd_index=6*(p1-1)+d2
                Int0221(ddp_index) = Int0221(ddp_index) &
                                   + dble(b_i)*RRd3*Int0120(pd_index)
              end if
              if(c_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! c-1i
                p2=dot_product(v_1i,xyz)
                dp_index=3*(d1-1)+p2
                Int0221(ddp_index) = Int0221(ddp_index) &
                                   + dble(c_i)*RRd4*Int0210(dp_index)
              end if
            end do ! i
          end do ! d2
        end do ! d1
!
! Step 17:
! [0222] from [0221], [0220], [0121], [0211]
! d+1i
!
        ddd_index=0
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do p3=1,3
              i=p3
              ddd_index=ddd_index+1
              ddp_index=18*(d1-1)+3*(d2-1)+p3
!
              b_i=Lvec_d(d1,i)
              c_i=Lvec_d(d2,i)
!
              Int0222(ddd_index) = RRd1(i)*Int0221(ddp_index) &
                                 + RRd5*Int0220(dd_index)
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p1=dot_product(v_1i,xyz)
                pdp_index=18*(p1-1)+3*(d2-1)+p3
                Int0222(ddd_index) = Int0222(ddd_index) &
                                   + dble(b_i)*RRd3*Int0121(pdp_index)
              end if
              if(c_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! c-1i
                p2=dot_product(v_1i,xyz)
                dpp_index=9*(d1-1)+3*(p2-1)+p3
                Int0222(ddd_index) = Int0222(ddd_index) &
                                   + dble(c_i)*RRd4*Int0211(dpp_index)
              end if
            end do ! p3
!
            do p3=2,3
              ddp_index=18*(d1-1)+3*(d2-1)+p3
              do i=1,p3-1
                ddd_index=ddd_index+1
!
                b_i=Lvec_d(d1,i)
                c_i=Lvec_d(d2,i)
!
                Int0222(ddd_index) = RRd1(i)*Int0221(ddp_index)
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! b-1i
                  p1=dot_product(v_1i,xyz)
                  pdp_index=18*(p1-1)+3*(d2-1)+p3
                  Int0222(ddd_index) = Int0222(ddd_index) &
                                     + dble(b_i)*RRd3*Int0121(pdp_index)
                end if
                if(c_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1 ! c-1i
                  p2=dot_product(v_1i,xyz)
                  dpp_index=9*(d1-1)+3*(p2-1)+p3
                  Int0222(ddd_index) = Int0222(ddd_index) &
                                     + dble(c_i)*RRd4*Int0211(dpp_index)
                end if
              end do ! i
            end do ! p3
          end do ! d2
        end do ! d1
!
! Step 18:
! [1222] from [0222], [0221], [0122], [0212]
! a+1i
!
        pddd_index=0
        do i=1,3
          ddd_index=0
          do d1=1,6
            do d2=1,6
              do d3=1,6
                ddd_index=ddd_index+1
                pddd_index=pddd_index+1
!
                b_i=Lvec_d(d1,i)
                c_i=Lvec_d(d2,i)
                d_i=Lvec_d(d3,i)
!
                eam(pddd_index) = RRa1(i)*Int0222(ddd_index)
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! b-1i
                  p1=dot_product(v_1i,xyz)
                  pdd_index=36*(p1-1)+6*(d2-1)+d3
                  eam(pddd_index) = eam(pddd_index) &
                                        + dble(b_i)*RRa3*Int0122(pdd_index)
                end if
                if(c_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1 ! c-1i
                  p2=dot_product(v_1i,xyz)
                  dpd_index=18*(d1-1)+6*(p2-1)+d3
                  eam(pddd_index) = eam(pddd_index) &
                                        + dble(c_i)*RRa4*Int0212(dpd_index)
                end if
                if(d_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d3,1:3)
                  v_1i(i)=v_1i(i)-1 ! d-1i
                  p3=dot_product(v_1i,xyz)
                  ddp_index=18*(d1-1)+3*(d2-1)+p3
                  eam(pddd_index) = eam(pddd_index) &
                                        + dble(d_i)*RRa5*Int0221(ddp_index)
                end if
              end do ! i
            end do ! d3
          end do ! d2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        pddd_index=0
        do p1=1,3
          do d2=1,6
            do d3=1,6
              do d4=1,6
                pddd_index=pddd_index+1
                if(d2.gt.3) eam(pddd_index) = sq3*eam(pddd_index)
                if(d3.gt.3) eam(pddd_index) = sq3*eam(pddd_index)
                if(d4.gt.3) eam(pddd_index) = sq3*eam(pddd_index)
              end do
            end do
          end do
        end do
!
        return
      end if
!
!------------------------------------------------------------------
! [dddd]
!------------------------------------------------------------------
      if(Lcode.eq.2222)then
!
! Requires 27 steps
!
        allocate(Int1000(3), Int0100(3))
        allocate(Int0110(9), Int1010(9), Int0200(6))
        allocate(Int1100(9), Int2000(6), Int0210(18))
        allocate(Int1110(27), Int2010(18), Int1200(18))
        allocate(Int2100(18), Int0220(36), Int1120(54))
        allocate(Int2020(36), Int1210(54), Int2110(54))
        allocate(Int2200(36), Int1220(108), Int2120(108))
        allocate(Int2210(108), Int1221(324), Int2121(324))
        allocate(Int2211(324), Int2220(216), Int2221(648))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2 :
! [0100] from [0000]
! b+1i
!
        do i=1,3 ! x,y,z
          Int0100(i) = RRb1(i)*G
        end do ! i 
!
! Step 3 :
! [0110] from [0100] and [0000]
! c+1i
!
        pp_index=0
        do p1=1,3
          do i=1,3
            pp_index=pp_index+1
!
            Int0110(pp_index) = RRc1(i)*Int0100(p1)
            if(i.eq.p1)then
              Int0110(pp_index) = Int0110(pp_index) &
                                + RRc3*G
            end if
          end do ! i
        end do ! p1
!
! Step 4 :
! [1010] from [1000] and [0000]
! c+1i
!
        pp_index=0
        do p1=1,3
          do i=1,3
            pp_index=pp_index+1
!
            Int1010(pp_index) = RRc1(i)*Int1000(p1)
            if(i.eq.p1)then
              Int1010(pp_index) = Int1010(pp_index) &
                                + RRc2*G
            end if
          end do ! i
        end do ! p1
!
! Step 5 :
! [0200] from [0100] and [0000]
! b+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int0200(d_index) = RRb1(i)*Int0100(p1) &
                           + RRb3*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int0200(d_index) = RRb1(i)*Int0100(p1)
          end do ! i
        end do ! p1
!
! Step 6:
! [1100] from [1000] and [0000]
! b+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int1100(pp_index) = RRb1(i)*Int1000(p1)
            if(p1.eq.i)then
              Int1100(pp_index) = Int1100(pp_index) &
                                + RRb2*G
            end if
          end do ! i
        end do ! p1
!
! Step 7 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int2000(d_index) = RRa1(i)*Int1000(p1) &
                           + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int2000(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! Step 8:
! [0210] from [0200], [0100]
! c+1i
!
        dp_index=0
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            b_i=Lvec_d(d1,i)
!
            Int0210(dp_index) = RRc1(i)*Int0200(d1)
            if(b_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int0210(dp_index) = Int0210(dp_index) &
                                + dble(b_i)*RRc3*Int0100(p1)
            end if
          end do ! i 
        end do ! d1
!
! Step 9:
! [1110] from [1100], [1000]
! c+1i
!
        ppp_index=0
        pp_index=0
        do p1=1,3
          do p2=1,3
            pp_index=pp_index+1
            do i=1,3
              ppp_index=ppp_index+1
!
              Int1110(ppp_index) = RRc1(i)*Int1100(pp_index)
              if(i.eq.p1)then
                Int1110(ppp_index) = Int1110(ppp_index) &
                                   + RRc2*Int0100(p2)
              end if
              if(i.eq.p2)then
                Int1110(ppp_index) = Int1110(ppp_index) &
                                   + RRc3*Int1000(p1)
              end if
            end do ! i
          end do ! p2
        end do ! p1
!
! Step 10:
! [2010] from [2000] and [1000]
! c+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i)
!
            Int2010(dp_index) = RRc1(i)*Int2000(d1)
            if(a_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int2010(dp_index) = Int2010(dp_index) &
                                + dble(a_i)*RRc2*Int1000(p1)
            end if
          end do ! i
        end do ! d1 
!
! Step 11:
! [1200] from [1100], [0100] and [1000]
! b+1i
!
        pd_index=0
        do p1=1,3
          do p2=1,3
            i=p2
            pd_index=pd_index+1
            pp_index=3*(p1-1)+p2
!
            Int1200(pd_index) = RRb1(i)*Int1100(pp_index) &
                              + RRb3*Int1000(p1)
            if(i.eq.p1)then
              Int1200(pd_index) = Int1200(pd_index) &
                                + RRb2*Int0100(p2)
            end if
          end do ! p2
!
          do p2=2,3
            pp_index=3*(p1-1)+p2
            do i=1,p2-1
              pd_index=pd_index+1
!
              Int1200(pd_index) = RRb1(i)*Int1100(pp_index)
              if(i.eq.p1)then
                Int1200(pd_index) = Int1200(pd_index) &
                                  + RRb2*Int0100(p2)
              end if
            end do ! i 
          end do ! p2
        end do ! p1
!
! Step 12:
! [2100] from [2000] and [1000]
! b+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i)
!
            Int2100(dp_index) = RRb1(i)*Int2000(d1)
            if(a_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int2100(dp_index) = Int2100(dp_index) &
                                + dble(a_i)*RRb2*Int1000(p1)
            end if
          end do ! i
        end do ! d1 
!
! Step 13 : 
! [0220] from [0210], [0200] and [0110]
! c+1i
!
        dd_index=0
        do d1=1,6
          do p2=1,3
            i=p2
            dd_index=dd_index+1
            dp_index=3*(d1-1)+p2
!
            b_i=Lvec_d(d1,i)
!
            Int0220(dd_index) = RRc1(i)*Int0210(dp_index) &
                              + RRc4*Int0200(d1)
            if(b_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              Int0220(dd_index) = Int0220(dd_index) &
                                + dble(b_i)*RRc3*Int0110(pp_index)
            end if
          end do ! p3
!
          do p2=2,3
            dp_index=3*(d1-1)+p2
            do i=1,p2-1
              dd_index=dd_index+1
!
              b_i=Lvec_d(d1,i)
!
              Int0220(dd_index) = RRc1(i)*Int0210(dp_index)
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int0220(dd_index) = Int0220(dd_index) &
                                  + dble(b_i)*RRc3*Int0110(pp_index)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 14:
! [1120] from [1110], [1100], [0110], [1010]
! c+1i
!
        ppd_index=0
        do p1=1,3
          do p2=1,3
            do p3=1,3
              i=p3
              ppd_index=ppd_index+1
              ppp_index=9*(p1-1)+3*(p2-1)+p3 
              pp_index=3*(p1-1)+p2
!
              Int1120(ppd_index) = RRc1(i)*Int1110(ppp_index) &
                                 + RRc4*Int1100(pp_index)
              if(i.eq.p1)then
                pp_index=3*(p2-1)+p3
                Int1120(ppd_index) = Int1120(ppd_index) &
                                   + RRc2*Int0110(pp_index)
              end if
              if(i.eq.p2)then
                pp_index=3*(p1-1)+p3
                Int1120(ppd_index) = Int1120(ppd_index) &
                                   + RRc3*Int1010(pp_index)
              end if
            end do ! p3
!
            do p3=2,3
              ppp_index=9*(p1-1)+3*(p2-1)+p3 
              do i=1,p3-1
                ppd_index=ppd_index+1
!
                Int1120(ppd_index) = RRc1(i)*Int1110(ppp_index)
                if(i.eq.p1)then
                  pp_index=3*(p2-1)+p3
                  Int1120(ppd_index) = Int1120(ppd_index) &
                                     + RRc2*Int0110(pp_index)
                end if
                if(i.eq.p2)then
                  pp_index=3*(p1-1)+p3
                  Int1120(ppd_index) = Int1120(ppd_index) &
                                     + RRc3*Int1010(pp_index)
                end if
              end do ! i
            end do ! p3
          end do ! p2
        end do ! p1
!
! Step 15:
! [2020] from [2010], [2000] and [1010]
! c+1i
!
        dd_index=0
        do d1=1,6
          do p2=1,3
            i=p2 ! do xy, yy, zz first
            dd_index=dd_index+1
            dp_index=3*(d1-1)+p2
!
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int2020(dd_index) = RRc1(i)*Int2010(dp_index) &
                              + RRc4*Int2000(d1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              Int2020(dd_index) = Int2020(dd_index) &
                                + dble(a_i)*RRc2*Int1010(pp_index)
            end if
          end do ! p2
!
          do p2=2,3 ! y, z
            dp_index=3*(d1-1)+p2
            do i=1,p2-1 ! x, y
              dd_index=dd_index+1
!
! i ne p2, therefore no (b-1i) terms
!
              a_i=Lvec_d(d1,i) ! angular momentum vector
!
              Int2020(dd_index) = RRc1(i)*Int2010(dp_index)
              if(a_i.ne.0)then ! (a-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2020(dd_index) = Int2020(dd_index) &
                                  + dble(a_i)*RRc2*Int1010(pp_index)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 16:
! [1210] from [1200], [1100] and [0200]
! c+1i
!
        pd_index=0
        pdp_index=0
        do p1=1,3
          do d2=1,6
            pd_index=pd_index+1
            do i=1,3
              pdp_index=pdp_index+1
!
              b_i=Lvec_d(d2,i)
!
              Int1210(pdp_index) = RRc1(i)*Int1200(pd_index)
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p2=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int1210(pdp_index) = Int1210(pdp_index) &
                                   + dble(b_i)*RRc3*Int1100(pp_index)
              end if
              if(i.eq.p1)then
                Int1210(pdp_index) = Int1210(pdp_index) &
                                   + RRc2*Int0200(d2)
              end if
            end do ! i
          end do ! d2
        end do ! p1
!
! Step 17:
! [2110] from [2100], [2000] and [1100]
! c+1i
!
        dp_index=0
        dpp_index=0
        do d1=1,6
          do p2=1,3
            dp_index=dp_index+1
            do i=1,3
              dpp_index=dpp_index+1
!
              a_i=Lvec_d(d1,i)
!
              Int2110(dpp_index) = RRc1(i)*Int2100(dp_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2110(dpp_index) = Int2110(dpp_index) &
                                   + dble(a_i)*RRc2*Int1100(pp_index)
              end if
              if(i.eq.p2)then
                Int2110(dpp_index) = Int2110(dpp_index) &
                                   + RRc3*Int2000(d1)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 18:
! [2200] from [2100], [2000] and [1100]
! b+1i
!
        dd_index=0
        do d1=1,6
          do p2=1,3
            i=p2
            dp_index=3*(d1-1)+p2
            dd_index=dd_index+1
!
            a_i=Lvec_d(d1,i)
!
            Int2200(dd_index) = RRb1(i)*Int2100(dp_index) &
                              + RRb3*Int2000(d1)
            if(a_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1i
              p1=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              Int2200(dd_index) = Int2200(dd_index) &
                                + dble(a_i)*RRb2*Int1100(pp_index)
            end if
          end do ! p2
!
          do p2=2,3
            dp_index=3*(d1-1)+p2
            do i=1,p2-1
              dd_index=dd_index+1
!
              a_i=Lvec_d(d1,i)
!
              Int2200(dd_index) = RRb1(i)*Int2100(dp_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2200(dd_index) = Int2200(dd_index) &
                                  + dble(a_i)*RRb2*Int1100(pp_index)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 19:
! [1220] from [1210], [1200], [1110] and [0210]
! c+1i
!
        pdd_index=0
        pd_index=0
        do p1=1,3
          do d2=1,6
            pd_index=pd_index+1
            do p3=1,3
              i=p3
              pdp_index=18*(p1-1)+3*(d2-1)+p3
              pdd_index=pdd_index+1
!
              b_i=Lvec_d(d2,i)
!
              Int1220(pdd_index) = RRc1(i)*Int1210(pdp_index) &
                                 + RRc4*Int1200(pd_index)
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p2=dot_product(v_1i,xyz)
                ppp_index=9*(p1-1)+3*(p2-1)+p3
                Int1220(pdd_index) = Int1220(pdd_index) &
                                   + dble(b_i)*RRc3*Int1110(ppp_index)
              end if
              if(i.eq.p1)then
                dp_index=3*(d2-1)+p3
                Int1220(pdd_index) = Int1220(pdd_index) &
                                   + RRc2*Int0210(dp_index)
              end if 
            end do ! p3
!
            do p3=2,3
              pdp_index=18*(p1-1)+3*(d2-1)+p3
              do i=1,p3-1
                pdd_index=pdd_index+1
!
                b_i=Lvec_d(d2,i)
!
                Int1220(pdd_index) = RRc1(i)*Int1210(pdp_index)
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1 ! b-1i
                  p2=dot_product(v_1i,xyz)
                  ppp_index=9*(p1-1)+3*(p2-1)+p3
                  Int1220(pdd_index) = Int1220(pdd_index) &
                                     + dble(b_i)*RRc3*Int1110(ppp_index)
                end if
                if(i.eq.p1)then
                  dp_index=3*(d2-1)+p3
                  Int1220(pdd_index) = Int1220(pdd_index) &
                                     + RRc2*Int0210(dp_index)
                end if 
              end do ! i
            end do ! p3
          end do ! d2
        end do ! p1 
!
! Step 20:
! [2120] from [2110], [2100], [1110] and [2010]
! c+1i
!
        dpd_index=0
        dp2_index=0
        do d1=1,6
          do p2=1,3
            dp2_index=dp2_index+1
            do p3=1,3
              i=p3
              dpp_index=9*(d1-1)+3*(p2-1)+p3
              dpd_index=dpd_index+1
!
              a_i=Lvec_d(d1,i)
!
              Int2120(dpd_index) = RRc1(i)*Int2110(dpp_index) &
                                 + RRc4*Int2100(dp2_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                ppp_index=9*(p1-1)+3*(p2-1)+p3
                Int2120(dpd_index) = Int2120(dpd_index) &
                                     + dble(a_i)*RRc2*Int1110(ppp_index)
              end if
              if(i.eq.p2)then
                dp3_index=3*(d1-1)+p3
                Int2120(dpd_index) = Int2120(dpd_index) &
                                   + RRc3*Int2010(dp3_index)
              end if
            end do ! p3
!
            do p3=2,3
              dpp_index=9*(d1-1)+3*(p2-1)+p3
              do i=1,p3-1
                dpd_index=dpd_index+1
!
                a_i=Lvec_d(d1,i)
!
                Int2120(dpd_index) = RRc1(i)*Int2110(dpp_index)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! a-1i
                  p1=dot_product(v_1i,xyz)
                  ppp_index=9*(p1-1)+3*(p2-1)+p3
                  Int2120(dpd_index) = Int2120(dpd_index) &
                                     + dble(a_i)*RRc2*Int1110(ppp_index)
                end if
                if(i.eq.p2)then
                  dp3_index=3*(d1-1)+p3
                  Int2120(dpd_index) = Int2120(dpd_index) &
                                     + RRc3*Int2010(dp3_index)
                end if
              end do ! i
            end do ! p3
          end do ! d2
        end do ! p1 
!
! Step 21:
! [2210] from [2200], [2100] and [1200]
! c+1i
!
        ddp_index=0
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do i=1,3
              ddp_index=ddp_index+1
!
              a_i=Lvec_d(d1,i)
              b_i=Lvec_d(d2,i)
!
              Int2210(ddp_index) = RRc1(i)*Int2200(dd_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pd_index=6*(p1-1)+d2
                Int2210(ddp_index) = Int2210(ddp_index) &
                                   + dble(a_i)*RRc2*Int1200(pd_index)
              end if
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p2=dot_product(v_1i,xyz)
                dp_index=3*(d1-1)+p2
                Int2210(ddp_index) = Int2210(ddp_index) &
                                   + dble(b_i)*RRc3*Int2100(dp_index)
              end if
            end do ! i
          end do ! d2
        end do ! d1
!
! Step 22 :
! [1221] from [1220], [1210], [1120] and [0220]
! d+1i
!
        pddp_index=0
        pdd_index=0
        do p1=1,3
          do d2=1,6
            do d3=1,6
              pdd_index=pdd_index+1
              dd_index=6*(d2-1)+d3
              do i=1,3
                pddp_index=pddp_index+1
!
                b_i=Lvec_d(d2,i)
                c_i=Lvec_d(d3,i)
!
                Int1221(pddp_index) = RRd1(i)*Int1220(pdd_index)
                if(i.eq.p1)then
                  Int1221(pddp_index) = Int1221(pddp_index) &
                                      + RRd2*Int0220(dd_index)
                end if
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1
                  p2=dot_product(v_1i,xyz)
                  ppd_index=18*(p1-1)+6*(p2-1)+d3
                  Int1221(pddp_index) = Int1221(pddp_index) &
                                      + dble(b_i)*RRd3*Int1120(ppd_index)
                end if
                if(c_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d3,1:3)
                  v_1i(i)=v_1i(i)-1
                  p3=dot_product(v_1i,xyz)
                  pdp_index=18*(p1-1)+3*(d2-1)+p3
                  Int1221(pddp_index) = Int1221(pddp_index) &
                                      + dble(c_i)*RRd4*Int1210(pdp_index)
                end if
              end do ! i
            end do ! d3
          end do ! d2
        end do ! p1
!
! Step 23 :
! [2121] from [2120], [2110], [2020] and [1120]
! d+1i
!
        dpdp_index=0
        dpd_index=0
        do d1=1,6
          do p2=1,3
            do d3=1,6
              dpd_index=dpd_index+1
              dd_index=6*(d1-1)+d3
              do i=1,3
                dpdp_index=dpdp_index+1
!
                a_i=Lvec_d(d1,i)
                c_i=Lvec_d(d3,i)
!
                Int2121(dpdp_index) = RRd1(i)*Int2120(dpd_index)
                if(i.eq.p2)then
                  Int2121(dpdp_index) = Int2121(dpdp_index) &
                                      + RRd3*Int2020(dd_index)
                end if
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1
                  p1=dot_product(v_1i,xyz)
                  ppd_index=18*(p1-1)+6*(p2-1)+d3
                  Int2121(dpdp_index) = Int2121(dpdp_index) &
                                      + dble(a_i)*RRd2*Int1120(ppd_index)
                end if
                if(c_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d3,1:3)
                  v_1i(i)=v_1i(i)-1
                  p3=dot_product(v_1i,xyz)
                  dpp_index=9*(d1-1)+3*(p2-1)+p3
                  Int2121(dpdp_index) = Int2121(dpdp_index) &
                                      + dble(c_i)*RRd4*Int2110(dpp_index)
                end if
              end do ! i
            end do ! d3
          end do ! d2
        end do ! p1
!
! Step 24 :
! [2211] from [2210], [2110], [2200] and [1210]
! d+1i
!
        ddpp_index=0
        ddp_index=0
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do p3=1,3
              ddp_index=ddp_index+1
              do i=1,3
                ddpp_index=ddpp_index+1
!
                a_i=Lvec_d(d1,i)
                b_i=Lvec_d(d2,i)
!
                Int2211(ddpp_index) = RRd1(i)*Int2210(ddp_index)
                if(i.eq.p3)then
                  Int2211(ddpp_index) = Int2211(ddpp_index) &
                                      + RRd4*Int2200(dd_index)
                end if
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1
                  p1=dot_product(v_1i,xyz)
                  pdp_index=18*(p1-1)+3*(d2-1)+p3
                  Int2211(ddpp_index) = Int2211(ddpp_index) &
                                      + dble(a_i)*RRd2*Int1210(pdp_index)
                end if
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1
                  p2=dot_product(v_1i,xyz)
                  dpp_index=9*(d1-1)+3*(p2-1)+p3
                  Int2211(ddpp_index) = Int2211(ddpp_index) &
                                      + dble(b_i)*RRd3*Int2110(dpp_index)
                end if
              end do ! i
            end do ! p3
          end do ! d2
        end do ! d1
!
! Step 25:
! [2220] from [2210], [2200], [2110], [1210]
! c+1i
!
        ddd_index=0
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do p3=1,3
              i=p3
              ddd_index=ddd_index+1
              ddp_index=18*(d1-1)+3*(d2-1)+p3
!
              a_i=Lvec_d(d1,i)
              b_i=Lvec_d(d2,i)
!
              Int2220(ddd_index) = RRc1(i)*Int2210(ddp_index) &
                                 + RRc4*Int2200(dd_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pdp_index=18*(p1-1)+3*(d2-1)+p3
                Int2220(ddd_index) = Int2220(ddd_index) &
                                   + dble(a_i)*RRc2*Int1210(pdp_index)
              end if
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p2=dot_product(v_1i,xyz)
                dpp_index=9*(d1-1)+3*(p2-1)+p3
                Int2220(ddd_index) = Int2220(ddd_index) &
                                   + dble(b_i)*RRc3*Int2110(dpp_index)
              end if
            end do ! p3
!
            do p3=2,3
              ddp_index=18*(d1-1)+3*(d2-1)+p3
              do i=1,p3-1
                ddd_index=ddd_index+1
!
                a_i=Lvec_d(d1,i)
                b_i=Lvec_d(d2,i)
!
                Int2220(ddd_index) = RRc1(i)*Int2210(ddp_index)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! a-1i
                  p1=dot_product(v_1i,xyz)
                  pdp_index=18*(p1-1)+3*(d2-1)+p3
                  Int2220(ddd_index) = Int2220(ddd_index) &
                                     + dble(a_i)*RRc2*Int1210(pdp_index)
                end if
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1 ! b-1i
                  p2=dot_product(v_1i,xyz)
                  dpp_index=9*(d1-1)+3*(p2-1)+p3
                  Int2220(ddd_index) = Int2220(ddd_index) &
                                     + dble(b_i)*RRc3*Int2110(dpp_index)
                end if
              end do ! i
            end do ! p3
          end do ! d2
        end do ! d1
!
! Step 26 :
! [2221] from [2220], [2210], [2120] and [1220]
! d+1i
!
        dddp_index=0
        ddd_index=0
        do d1=1,6
          do d2=1,6
            do d3=1,6
              ddd_index=ddd_index+1
              do i=1,3
                dddp_index=dddp_index+1
!
                a_i=Lvec_d(d1,i)
                b_i=Lvec_d(d2,i)
                c_i=Lvec_d(d3,i)
!
                Int2221(dddp_index) = RRd1(i)*Int2220(ddd_index)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1
                  p1=dot_product(v_1i,xyz)
                  pdd_index=36*(p1-1)+6*(d2-1)+d3
                  Int2221(dddp_index) = Int2221(dddp_index) &
                                      + dble(a_i)*RRd2*Int1220(pdd_index)
                end if
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1
                  p2=dot_product(v_1i,xyz)
                  dpd_index=18*(d1-1)+6*(p2-1)+d3
                  Int2221(dddp_index) = Int2221(dddp_index) &
                                      + dble(b_i)*RRd3*Int2120(dpd_index)
                end if
                if(c_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d3,1:3)
                  v_1i(i)=v_1i(i)-1
                  p3=dot_product(v_1i,xyz)
                  ddp_index=18*(d1-1)+3*(d2-1)+p3
                  Int2221(dddp_index) = Int2221(dddp_index) &
                                      + dble(c_i)*RRd4*Int2210(ddp_index)
                end if
              end do ! i
            end do ! d3
          end do ! d2
        end do ! d1
!
! Step 27 :
! [2222] from [2221], [2220], [2211], [2121] and [1221]
! d+1i
!
        dddd_index=0
        ddd_index=0
        do d1=1,6
          do d2=1,6
            do d3=1,6
              ddd_index=ddd_index+1
              do p4=1,3
                i=p4
                dddp_index=108*(d1-1)+18*(d2-1)+3*(d3-1)+p4
                dddd_index=dddd_index+1
!
                a_i=Lvec_d(d1,i)
                b_i=Lvec_d(d2,i)
                c_i=Lvec_d(d3,i)
!
                eam(dddd_index) = RRd1(i)*Int2221(dddp_index) &
                                     + RRd5*Int2220(ddd_index)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1
                  p1=dot_product(v_1i,xyz)
                  pddp_index=108*(p1-1)+18*(d2-1)+3*(d3-1)+p4
                  eam(dddd_index) = eam(dddd_index) &
                                       + dble(a_i)*RRd2*Int1221(pddp_index)
                end if
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1
                  p2=dot_product(v_1i,xyz)
                  dpdp_index=54*(d1-1)+18*(p2-1)+3*(d3-1)+p4
                  eam(dddd_index) = eam(dddd_index) &
                                       + dble(b_i)*RRd3*Int2121(dpdp_index)
                end if
                if(c_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d3,1:3)
                  v_1i(i)=v_1i(i)-1
                  p3=dot_product(v_1i,xyz)
                  ddpp_index=54*(d1-1)+9*(d2-1)+3*(p3-1)+p4
                  eam(dddd_index) = eam(dddd_index) &
                                       + dble(c_i)*RRd4*Int2211(ddpp_index)
                end if
              end do ! p4
!
              do p4=2,3
                dddp_index=108*(d1-1)+18*(d2-1)+3*(d3-1)+p4
                do i=1,p4-1
                  dddd_index=dddd_index+1
!
                  a_i=Lvec_d(d1,i)
                  b_i=Lvec_d(d2,i)
                  c_i=Lvec_d(d3,i)
!
                  eam(dddd_index) = RRd1(i)*Int2221(dddp_index)
                  if(a_i.ne.0)then
                    v_1i(1:3)=Lvec_d(d1,1:3)
                    v_1i(i)=v_1i(i)-1
                    p1=dot_product(v_1i,xyz)
                    pddp_index=108*(p1-1)+18*(d2-1)+3*(d3-1)+p4
                    eam(dddd_index) = eam(dddd_index) &
                                         + dble(a_i)*RRd2*Int1221(pddp_index)
                  end if
                  if(b_i.ne.0)then
                    v_1i(1:3)=Lvec_d(d2,1:3)
                    v_1i(i)=v_1i(i)-1
                    p2=dot_product(v_1i,xyz)
                    dpdp_index=54*(d1-1)+18*(p2-1)+3*(d3-1)+p4
                    eam(dddd_index) = eam(dddd_index) &
                                         + dble(b_i)*RRd3*Int2121(dpdp_index)
                  end if
                  if(c_i.ne.0)then
                    v_1i(1:3)=Lvec_d(d3,1:3)
                    v_1i(i)=v_1i(i)-1
                    p3=dot_product(v_1i,xyz)
                    ddpp_index=54*(d1-1)+9*(d2-1)+3*(p3-1)+p4
                    eam(dddd_index) = eam(dddd_index) &
                                         + dble(c_i)*RRd4*Int2211(ddpp_index)
                  end if
                end do ! i
              end do ! p4
            end do ! d3
          end do ! d2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        dddd_index=0
        do d1=1,6
          do d2=1,6
            do d3=1,6
              do d4=1,6
                dddd_index=dddd_index+1
                if(d1.gt.3) eam(dddd_index) = sq3*eam(dddd_index)
                if(d2.gt.3) eam(dddd_index) = sq3*eam(dddd_index)
                if(d3.gt.3) eam(dddd_index) = sq3*eam(dddd_index)
                if(d4.gt.3) eam(dddd_index) = sq3*eam(dddd_index)
              end do
            end do
          end do
        end do
!
        return
      end if
!
!--------------------------------------------------------------------
! Shouldn't be able to get here, print out an error message
!--------------------------------------------------------------------
!
        write (*,*) 'Orbital angular momentum not recognised by subroutine eqmIntegral'
        write (*,*) 'Please check that your basis set contains only s, p and d functions'
        write(*,*)' Lcode = ',Lcode 
!
      end subroutine EAMint
