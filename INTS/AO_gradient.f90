      subroutine AO_gradient (rx, ry, rz)
!******************************************************************************
!     Date last modified: August 20, 2015                        Version 3.0  *
!     Authors: JWH                                                            *
!     Description: Determine the value of the gradient of AOs at grid points. *
!******************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE type_basis_set
      USE matrix_print
      USE QM_defaults
      USE AO_values

      implicit none
!
! Output array:
!
! Local scalars:
      integer :: Ishell,Ifrst,Ilast
      integer :: Istart,Iend
      integer :: Iatmshl
      integer :: LI
      integer :: Iatom, Irange, Igauss
      integer :: IGBGN,JGBGN,IGend,JGend
      integer :: Iao,AOI,I
      double precision :: SQRT3,alpha,EG
      double precision :: rA2,Ax,Ay,Az,rx,ry,rz,rAx,rAy,rAz
!
! Local arrays
      double precision :: SHLINTS(100,3), chi(100), chim1(100)
      double precision :: grad(100,3)
!
! Begin:
      SQRT3=DSQRT(THREE)
      dAOx=0.0D0
      dAOy=0.0D0
      dAOz=0.0D0
!
! Begin loop over shells.
!
! Loop over elemental SHELLs
! Loop over Ishell.
      do Ishell=1,Basis%Nshells
        LI=Basis%shell(Ishell)%Xtype
        Istart=Basis%shell(Ishell)%Xstart
        Iend=Basis%shell(Ishell)%XEND
        Irange=Iend-Istart+1
        IGBGN=Basis%shell(Ishell)%EXPBGN
        IGEND=Basis%shell(Ishell)%EXPEND
        Ifrst=Basis%shell(Ishell)%frstSHL
        Ilast=Basis%shell(Ishell)%lastSHL
!
        select case (LI)
! s
        case (0)
          call s_grad
! p
        case (1)
          call p_grad
! d  
        case (2)
          call d_grad
!
        case default
          write(UNIout,*)'ERROR> AO_VAL: unable to determine AO val for LI:',LI
          stop ' ERROR> AO_VAL: AO type not supported'
        end select
!
! End of loop over shells.
      
      end do ! Ishell
!
! End of routine dAO
      return
      CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine s_grad
!********************************************************************************
!     Date last modified: May 22, 2015                             Version 3.0  *
!     Authors: JWH                                                              *
!     Description: Get gradient of s-type AOs at grid points.                   *
!********************************************************************************
! Modules:

      implicit none
!
! Loop over all atomic shells
      do Iatmshl=Ifrst,Ilast
        Iatom=Basis%atmshl(Iatmshl)%ATMLST
!
        Ax=CARTESIAN(Iatom)%X     ! vector A={Ax,Ay,Az}     
        Ay=CARTESIAN(Iatom)%Y
        Az=CARTESIAN(Iatom)%Z
!
        AOI=Basis%atmshl(Iatmshl)%frstAO-1
!
        SHLINTS(1:100,1:3)=ZERO ! will handle up to <f|f>
! Loop over primitive gaussians
!
        do Igauss=IGBGN,IGEND
!
          alpha=Basis%gaussian(Igauss)%exp
!
          rAx = rx - Ax     ! vector rA= r-A
          rAy = ry - Ay
          rAz = rz - Az

          rA2 = rAx*rAx + rAy*rAy + rAz*rAz   ! (rA)**2

          EG = dexp(-alpha*(rA2))             ! e^(-alpha*rA^2)
!
          grad(1,1) = -2.0D0*alpha*rAx*EG
          grad(1,2) = -2.0D0*alpha*rAy*EG
          grad(1,3) = -2.0D0*alpha*rAz*EG
!
! contract 
          SHLINTS(1,1:3) = SHLINTS(1,1:3)+Basis%gaussian(Igauss)%CONTRC*grad(1,1:3)
!
        end do ! Igauss 
!      
! Save in dAO
        do I=1,Irange
          IAO = AOI+I
          dAOx(IAO)=SHLINTS(I,1)
          dAOy(IAO)=SHLINTS(I,2)
          dAOz(IAO)=SHLINTS(I,3)
        end do ! I
!
      end do ! Iatmshl
!
      return
      end subroutine s_grad
      subroutine p_grad
!********************************************************************************
!     Date last modified: August 20, 2015                          Version 3.0  *
!     Authors: JWH                                                              *
!     Description: Get gradient of p-type AOs at grid points.                   *
!********************************************************************************
! Modules:

      implicit none
!
! Loop over all atomic shells
      do Iatmshl=Ifrst,Ilast
        Iatom=Basis%atmshl(Iatmshl)%ATMLST
!
        Ax=CARTESIAN(Iatom)%X     ! vector A={Ax,Ay,Az}     
        Ay=CARTESIAN(Iatom)%Y
        Az=CARTESIAN(Iatom)%Z
!
        AOI=Basis%atmshl(Iatmshl)%frstAO-1
!
        SHLINTS(1:100,1:3)=ZERO ! will handle up to <f|f>
! Loop over primitive gaussians
!
        do Igauss=IGBGN,IGEND
!
          alpha=Basis%gaussian(Igauss)%exp
!
          rAx = rx - Ax     ! vector rA= r-A
          rAy = ry - Ay
          rAz = rz - Az

          rA2 = rAx*rAx + rAy*rAy + rAz*rAz   ! (rA)**2

          EG = dexp(-alpha*(rA2))             ! e^(-alpha*rA^2)
!
! build p-functions
          chi(1) = rAx*EG
          chi(2) = rAy*EG
          chi(3) = rAz*EG
!
! delpx
          grad(1,1) = EG - 2.0D0*alpha*rAx*chi(1)
          grad(1,2) =     -2.0D0*alpha*rAy*chi(1)
          grad(1,3) =     -2.0D0*alpha*rAz*chi(1)
! delpy
          grad(2,1) =     -2.0D0*alpha*rAx*chi(2)
          grad(2,2) = EG - 2.0D0*alpha*rAy*chi(2)
          grad(2,3) =     -2.0D0*alpha*rAz*chi(2)
! delpz
          grad(3,1) =     -2.0D0*alpha*rAx*chi(3)
          grad(3,2) =     -2.0D0*alpha*rAy*chi(3)
          grad(3,3) = EG - 2.0D0*alpha*rAz*chi(3)
!
! contract 
          SHLINTS(1,1:3) = SHLINTS(1,1:3)+Basis%gaussian(Igauss)%CONTRC*grad(1,1:3)
          SHLINTS(2,1:3) = SHLINTS(2,1:3)+Basis%gaussian(Igauss)%CONTRC*grad(2,1:3)
          SHLINTS(3,1:3) = SHLINTS(3,1:3)+Basis%gaussian(Igauss)%CONTRC*grad(3,1:3)
!
        end do ! Igauss 
!      
! Save in dAO
        do I=1,Irange
          IAO = AOI+I
          dAOx(IAO)=SHLINTS(I,1)
          dAOy(IAO)=SHLINTS(I,2)
          dAOz(IAO)=SHLINTS(I,3)
        end do ! I
!
      end do ! Iatmshl
!
      return
      end subroutine p_grad
      subroutine d_grad
!********************************************************************************
!     Date last modified: August 20, 2015                          Version 3.0  *
!     Authors: JWH                                                              *
!     Description: Get gradient of d-type AOs at grid points.                   *
!********************************************************************************
! Modules:

      implicit none
!
! Loop over all atomic shells
      do Iatmshl=Ifrst,Ilast
        Iatom=Basis%atmshl(Iatmshl)%ATMLST
!
        Ax=CARTESIAN(Iatom)%X     ! vector A={Ax,Ay,Az}     
        Ay=CARTESIAN(Iatom)%Y
        Az=CARTESIAN(Iatom)%Z
!
        AOI=Basis%atmshl(Iatmshl)%frstAO-1
!
        SHLINTS(1:100,1:3)=ZERO ! will handle up to <f|f>
! Loop over primitive gaussians
!
        do Igauss=IGBGN,IGEND
!
          alpha=Basis%gaussian(Igauss)%exp
!
          rAx = rx - Ax     ! vector rA= r-A
          rAy = ry - Ay
          rAz = rz - Az

          rA2 = rAx*rAx + rAy*rAy + rAz*rAz   ! (rA)**2

          EG = dexp(-alpha*(rA2))             ! e^(-alpha*rA^2)
!
! build p-functions
          chim1(1) = rAx*EG
          chim1(2) = rAy*EG
          chim1(3) = rAz*EG
!
! build d-functions
          chi(1) = rAx*rAx*EG
          chi(2) = rAy*rAy*EG
          chi(3) = rAz*rAz*EG
          chi(4) = rAx*rAy*EG ! save sqrt3 factor for contraction step
          chi(5) = rAx*rAz*EG
          chi(6) = rAy*rAz*EG
!
! deldxx
          grad(1,1) = 2.0D0*chim1(1) - 2.0D0*alpha*rAx*chi(1)
          grad(1,2) =                 -2.0D0*alpha*rAy*chi(1)
          grad(1,3) =                 -2.0D0*alpha*rAz*chi(1)
! deldyy
          grad(2,1) =                 -2.0D0*alpha*rAx*chi(2)
          grad(2,2) = 2.0D0*chim1(2) - 2.0D0*alpha*rAy*chi(2)
          grad(2,3) =                 -2.0D0*alpha*rAz*chi(2)
! deldzz
          grad(3,1) =                 -2.0D0*alpha*rAx*chi(3)
          grad(3,2) =                 -2.0D0*alpha*rAy*chi(3)
          grad(3,3) = 2.0D0*chim1(3) - 2.0D0*alpha*rAz*chi(3)
! deldxy
          grad(4,1) = chim1(2) - 2.0D0*alpha*rAx*chi(4)
          grad(4,2) = chim1(1) - 2.0D0*alpha*rAy*chi(4)
          grad(4,3) =           -2.0D0*alpha*rAz*chi(4)
! deldxz
          grad(5,1) = chim1(3) - 2.0D0*alpha*rAx*chi(5)
          grad(5,2) =           -2.0D0*alpha*rAy*chi(5)
          grad(5,3) = chim1(1) - 2.0D0*alpha*rAz*chi(5)
! deldyz
          grad(6,1) =           -2.0D0*alpha*rAx*chi(6)
          grad(6,2) = chim1(3) - 2.0D0*alpha*rAy*chi(6)
          grad(6,3) = chim1(2) - 2.0D0*alpha*rAz*chi(6)
!
! contract 
          SHLINTS(1,1:3) = SHLINTS(1,1:3)+Basis%gaussian(Igauss)%CONTRC*grad(1,1:3)
          SHLINTS(2,1:3) = SHLINTS(2,1:3)+Basis%gaussian(Igauss)%CONTRC*grad(2,1:3)
          SHLINTS(3,1:3) = SHLINTS(3,1:3)+Basis%gaussian(Igauss)%CONTRC*grad(3,1:3)
          SHLINTS(4,1:3) = SHLINTS(4,1:3)+Basis%gaussian(Igauss)%CONTRC*grad(4,1:3)*SQRT3
          SHLINTS(5,1:3) = SHLINTS(5,1:3)+Basis%gaussian(Igauss)%CONTRC*grad(5,1:3)*SQRT3
          SHLINTS(6,1:3) = SHLINTS(6,1:3)+Basis%gaussian(Igauss)%CONTRC*grad(6,1:3)*SQRT3
!
        end do ! Igauss 
!      
! Save in AOval
        do I=1,Irange
          IAO = AOI+I
          dAOx(IAO)=SHLINTS(I,1)
          dAOy(IAO)=SHLINTS(I,2)
          dAOz(IAO)=SHLINTS(I,3)
        end do ! I
!
      end do ! Iatmshl
!
      return
      end subroutine d_grad
      end subroutine AO_gradient
