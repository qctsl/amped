      subroutine AO_products (rx, ry, rz, AOprod, MATlen)
!************************************************************************************************************
!     Date last modified: May 10, 2011                                                         Version 3.0  *
!     Authors: Ahmad and R.A. Poirier                                                                       *
!     Description:                                                                                          *
!                                                                                                           *
!************************************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE type_basis_set
      USE matrix_print
      USE QM_defaults

      implicit none
!
! Output array:
      integer :: MATlen
      double precision :: AOprod(MATlen)
!
! Local scalars:
      integer :: Ishell,Jshell,Ifrst,Jfrst,Ilast,Jlast
      integer :: Istart,Jstart,Iend,Jend
      integer :: Iatmshl,Jatmshl
      integer :: LAMAX,LBMAX
      integer :: Iatom,Jatom
      integer :: Irange,Jrange
      integer :: Igauss,Jgauss
      integer :: IGBGN,JGBGN,IGend,JGend
      integer :: Iao,Jao,AOI,AOJ
      integer :: I,J,K,INDEX,DEFCASE,IJao
      double precision :: COEF_SS,COEF_SP,COEF_SXX,COEF_SXY,COEF_PP,COEF_PXX,COEF_PXY,COEF_XXYY
      double precision :: COEF_XXXY,COEF_XYXY,SQRT3,SUMDSQ,Gama,Alpha,Beta,RABSQ,EG,EG_K,EG_exp
      double precision :: Ax,Bx,Ay,By,Az,Bz,rx,ry,rz,Px,Py,Pz,xA,yA,zA,xB,yB,zB,rpx,rpy,rpz
      logical LIatmshl
!
! Local arrays
      double precision :: SHLINTS(100)
!
! Begin:
      SQRT3=DSQRT(THREE)
      AOprod(1:size(AOprod))=ZERO
!
! Begin loop over shells.
!
! Loop over elemental SHELLs
! Loop over Ishell.
      do Ishell=1,Basis%Nshells
        LAMAX=Basis%shell(Ishell)%Xtype+1
        Istart=Basis%shell(Ishell)%Xstart
        Iend=Basis%shell(Ishell)%XEND
        Irange=Iend-Istart+1
        IGBGN=Basis%shell(Ishell)%EXPBGN
        IGEND=Basis%shell(Ishell)%EXPEND
        Ifrst=Basis%shell(Ishell)%frstSHL
!
! Loop over Jshell.
      do Jshell=1,Ishell
        LBMAX=Basis%shell(Jshell)%Xtype+1
        Jstart=Basis%shell(Jshell)%Xstart
        Jend=Basis%shell(Jshell)%XEND
        Jrange=Jend-Jstart+1
        JGBGN=Basis%shell(Jshell)%EXPBGN
        JGEND=Basis%shell(Jshell)%EXPEND
        Jfrst=Basis%shell(Jshell)%frstSHL
!
! Define CASE:
      if(LAMAX.lt.LBMAX)then
       DEFCASE=4*(LAMAX-1)+LBMAX
      else
       DEFCASE=4*(LBMAX-1)+LAMAX
      end if
!
      Ilast= Basis%shell(Ishell)%lastSHL
      Jlast= Basis%shell(Jshell)%lastSHL
!
      select case (DEFCASE)
! S|S
     case (1)
        call SS_products
! S|P
      case (2)
        call SP_products
! S|D
      case (3)
        call SD_products
! P|P
      case (6)
        call PP_products
! P|D
      case (7)
        call PD_products
! D|D
      case (11)
        call DD_products

      case default
        write(UNIout,*)'ERROR> AO_PRODUCTS: DEFCASE type should not exist'
        stop ' ERROR> AO_PRODUCTS: DEFCASE type should not exist'
      end select
!
! End of loop over shells.
      
      end do ! Jshell
      end do ! Ishell
!
! End of routine AO_PRODUCTS
      return
      CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine SS_products
!************************************************************************************************************
!     Date last modified: May 10, 2011                                                         Version 3.0  *
!     Authors: Ahmad and R.A. Poirier                                                                       *
!     Description:                                                                                          *
!                                                                                                           *
!************************************************************************************************************
! Modules:

      implicit none
!
! Loop over all atomic shells
      do Iatmshl=Ifrst,Ilast
        Iatom=Basis%atmshl(Iatmshl)%ATMLST
!
        Ax=CARTESIAN(Iatom)%X     ! vector A={Ax,Ay,Az}     
        Ay=CARTESIAN(Iatom)%Y
        Az=CARTESIAN(Iatom)%Z
!
        AOI=Basis%atmshl(Iatmshl)%frstAO-1
        IF(Ishell.EQ.Jshell)Jlast=Iatmshl
      do Jatmshl=Jfrst,Jlast
        Jatom=Basis%atmshl(Jatmshl)%ATMLST
!
        Bx=CARTESIAN(Jatom)%X     ! vector B={Bx,By,Bz}
        By=CARTESIAN(Jatom)%Y
        Bz=CARTESIAN(Jatom)%Z
!
        AOJ=Basis%atmshl(Jatmshl)%frstAO-1
        LIatmshl=.false.
        if(Iatmshl.eq.Jatmshl)LIatmshl=.true.

        RABSQ=(Ax-Bx)*(Ax-Bx)+(Ay-By)*(Ay-By)+(Az-Bz)*(Az-Bz)     ! (A-B)**2

        SHLINTS(1:100)=ZERO ! will handle up to <f|f>
! Loop over primitive gaussians
!
      do Igauss=IGBGN,IGEND
        Alpha=Basis%gaussian(Igauss)%exp

      do Jgauss=JGBGN,JGEND
        Beta=Basis%gaussian(Jgauss)%exp
!
        Gama=Alpha+Beta

        Px=(Alpha*Ax+Beta*Bx)/Gama     ! vector P
        Py=(Alpha*Ay+Beta*By)/Gama
        Pz=(Alpha*Az+Beta*Bz)/Gama
!
        rpx = Px-rx     ! vector rp=P-r
        rpy = Py-ry
        rpz = Pz-rz

        SUMDSQ = rpx*rpx + rpy*rpy + rpz*rpz   ! (rp)**2

        EG_K=DEXP(-Alpha*Beta*RABSQ/Gama)      ! K 
        EG_exp=DEXP(-Gama*SUMDSQ)              ! exp
        EG=EG_K*EG_exp                         ! K*exp
!
        xA=rx-Ax     ! vector rA=(r-A)
        yA=ry-Ay
        zA=rz-Az
        xB=rx-Bx     ! vector rB=(r-B)
        yB=ry-By
        zB=rz-Bz
! S|S
        COEF_SS = Basis%gaussian(Igauss)%CONTRC*Basis%gaussian(Jgauss)%CONTRC
        SHLINTS(1) = SHLINTS(1)+COEF_SS*EG
!
      end do ! Jgauss
      end do ! Igauss 
      
        IF(LIatmshl)then
          INDEX=0
          do I=1,Irange
          do J=1,Jrange
            INDEX=INDEX+1
            IF(I.GE.J)then  ! Corrected August 1,2003 had (J.ge.I)
              Iao=AOI+I
              Jao=AOJ+J
              IJao=Iao*(Iao-1)/2+Jao
              AOprod(IJao)=SHLINTS(INDEX)
            end if
          end do ! J   
          end do ! I
            else
              INDEX=0
          do I=1,Irange
          do J=1,Jrange
            INDEX=INDEX+1
            Iao=AOI+I
            Jao=AOJ+J
            IJao=Iao*(Iao-1)/2+Jao
            AOprod(IJao)=SHLINTS(INDEX)
          end do ! J
          end do ! I
        end if ! LIatmshl

      end do ! Jatmshl
      end do ! Iatmshl

      return
      end subroutine SS_products
      subroutine SP_products
!************************************************************************************************************
!     Date last modified: May 10, 2011                                                         Version 3.0  *
!     Authors: Ahmad and R.A. Poirier                                                                       *
!     Description:                                                                                          *
!                                                                                                           *
!************************************************************************************************************
! Modules:

      implicit none
!
! Loop over all atomic shells
      do Iatmshl=Ifrst,Ilast
        Iatom=Basis%atmshl(Iatmshl)%ATMLST
!
        Ax=CARTESIAN(Iatom)%X     ! vector A={Ax,Ay,Az}     
        Ay=CARTESIAN(Iatom)%Y
        Az=CARTESIAN(Iatom)%Z
!
        AOI=Basis%atmshl(Iatmshl)%frstAO-1
        IF(Ishell.EQ.Jshell)Jlast=Iatmshl
      do Jatmshl=Jfrst,Jlast
        Jatom=Basis%atmshl(Jatmshl)%ATMLST
!
        Bx=CARTESIAN(Jatom)%X     ! vector B={Bx,By,Bz}
        By=CARTESIAN(Jatom)%Y
        Bz=CARTESIAN(Jatom)%Z
!
        AOJ=Basis%atmshl(Jatmshl)%frstAO-1
        LIatmshl=.false.
        if(Iatmshl.eq.Jatmshl)LIatmshl=.true.

        RABSQ=(Ax-Bx)*(Ax-Bx)+(Ay-By)*(Ay-By)+(Az-Bz)*(Az-Bz)     ! (A-B)**2

        SHLINTS(1:100)=ZERO ! will handle up to <f|f>
! Loop over primitive gaussians
!
      do Igauss=IGBGN,IGEND
        Alpha=Basis%gaussian(Igauss)%exp

      do Jgauss=JGBGN,JGEND
        Beta=Basis%gaussian(Jgauss)%exp
!
        Gama=Alpha+Beta

        Px=(Alpha*Ax+Beta*Bx)/Gama     ! vector P
        Py=(Alpha*Ay+Beta*By)/Gama
        Pz=(Alpha*Az+Beta*Bz)/Gama
!
        rpx = Px-rx     ! vector rp=P-r
        rpy = Py-ry
        rpz = Pz-rz

        SUMDSQ = rpx*rpx + rpy*rpy + rpz*rpz   ! (rp)**2
        
        EG_K=DEXP(-Alpha*Beta*RABSQ/Gama)      ! K  
        EG_exp=DEXP(-Gama*SUMDSQ)              ! exp
        EG=EG_K*EG_exp                         ! K*exp
!
        xA=rx-Ax     ! vector rA=(r-A)
        yA=ry-Ay
        zA=rz-Az
        xB=rx-Bx     ! vector rB=(r-B)
        yB=ry-By
        zB=rz-Bz
! S|P  
        COEF_SP = Basis%gaussian(Igauss)%CONTRC*Basis%gaussian(Jgauss)%CONTRC
        SHLINTS(1)=SHLINTS(1)+xB*COEF_SP*EG
        SHLINTS(2)=SHLINTS(2)+yB*COEF_SP*EG
        SHLINTS(3)=SHLINTS(3)+zB*COEF_SP*EG

!
      end do ! Jgauss
      end do ! Igauss 
      
        IF(LIatmshl)then
          INDEX=0
          do I=1,Irange
          do J=1,Jrange
            INDEX=INDEX+1
            IF(I.GE.J)then  ! Corrected August 1,2003 had (J.ge.I)
              Iao=AOI+I
              Jao=AOJ+J
              IJao=Iao*(Iao-1)/2+Jao
              AOprod(IJao)=SHLINTS(INDEX)
            end if
          end do ! J   
          end do ! I
            else
              INDEX=0
          do I=1,Irange
          do J=1,Jrange
            INDEX=INDEX+1
            Iao=AOI+I
            Jao=AOJ+J
            IJao=Iao*(Iao-1)/2+Jao
            AOprod(IJao)=SHLINTS(INDEX)
          end do ! J
          end do ! I
        end if ! LIatmshl

      end do ! Jatmshl
      end do ! Iatmshl

      return
      end subroutine SP_products
      subroutine PP_products
!************************************************************************************************************
!     Date last modified: May 10, 2011                                                         Version 3.0  *
!     Authors: Ahmad and R.A. Poirier                                                                       *
!     Description:                                                                                          *
!                                                                                                           *
!************************************************************************************************************
! Modules:

      implicit none
!
! Loop over all atomic shells
      do Iatmshl=Ifrst,Ilast
        Iatom=Basis%atmshl(Iatmshl)%ATMLST
!
        Ax=CARTESIAN(Iatom)%X     ! vector A={Ax,Ay,Az}     
        Ay=CARTESIAN(Iatom)%Y
        Az=CARTESIAN(Iatom)%Z
!
        AOI=Basis%atmshl(Iatmshl)%frstAO-1
        IF(Ishell.EQ.Jshell)Jlast=Iatmshl
      do Jatmshl=Jfrst,Jlast
        Jatom=Basis%atmshl(Jatmshl)%ATMLST
!
        Bx=CARTESIAN(Jatom)%X     ! vector B={Bx,By,Bz}
        By=CARTESIAN(Jatom)%Y
        Bz=CARTESIAN(Jatom)%Z
!
        AOJ=Basis%atmshl(Jatmshl)%frstAO-1
        LIatmshl=.false.
        if(Iatmshl.eq.Jatmshl)LIatmshl=.true.

        RABSQ=(Ax-Bx)*(Ax-Bx)+(Ay-By)*(Ay-By)+(Az-Bz)*(Az-Bz)     ! (A-B)**2

        SHLINTS(1:100)=ZERO ! will handle up to <f|f>
! Loop over primitive gaussians
!
      do Igauss=IGBGN,IGEND
        Alpha=Basis%gaussian(Igauss)%exp

      do Jgauss=JGBGN,JGEND
        Beta=Basis%gaussian(Jgauss)%exp
!
        Gama=Alpha+Beta

        Px=(Alpha*Ax+Beta*Bx)/Gama     ! vector P
        Py=(Alpha*Ay+Beta*By)/Gama
        Pz=(Alpha*Az+Beta*Bz)/Gama
!
        rpx = Px-rx     ! vector rp=P-r
        rpy = Py-ry
        rpz = Pz-rz

        SUMDSQ = rpx*rpx + rpy*rpy + rpz*rpz   ! (rp)**2

        EG_K=DEXP(-Alpha*Beta*RABSQ/Gama)      ! K  
        EG_exp=DEXP(-Gama*SUMDSQ)              ! exp
        EG=EG_K*EG_exp                         ! K*exp
!
        xA=rx-Ax     ! vector rA=(r-A)
        yA=ry-Ay
        zA=rz-Az
        xB=rx-Bx     ! vector rB=(r-B)
        yB=ry-By
        zB=rz-Bz
! P|P          
        COEF_PP = Basis%gaussian(Igauss)%CONTRC*Basis%gaussian(Jgauss)%CONTRC
        SHLINTS(1)=SHLINTS(1)+xA*xB*COEF_PP*EG
        SHLINTS(2)=SHLINTS(2)+xA*yB*COEF_PP*EG
        SHLINTS(3)=SHLINTS(3)+xA*zB*COEF_PP*EG
        SHLINTS(4)=SHLINTS(4)+yA*xB*COEF_PP*EG
        SHLINTS(5)=SHLINTS(5)+yA*yB*COEF_PP*EG
        SHLINTS(6)=SHLINTS(6)+yA*zB*COEF_PP*EG
        SHLINTS(7)=SHLINTS(7)+zA*xB*COEF_PP*EG
        SHLINTS(8)=SHLINTS(8)+zA*yB*COEF_PP*EG
        SHLINTS(9)=SHLINTS(9)+zA*zB*COEF_PP*EG
!
      end do ! Jgauss
      end do ! Igauss 
      
        IF(LIatmshl)then
          INDEX=0
          do I=1,Irange
          do J=1,Jrange
            INDEX=INDEX+1
            IF(I.GE.J)then  ! Corrected August 1,2003 had (J.ge.I)
              Iao=AOI+I
              Jao=AOJ+J
              IJao=Iao*(Iao-1)/2+Jao
              AOprod(IJao)=SHLINTS(INDEX)
            end if
          end do ! J   
          end do ! I
            else
              INDEX=0
          do I=1,Irange
          do J=1,Jrange
            INDEX=INDEX+1
            Iao=AOI+I
            Jao=AOJ+J
            IJao=Iao*(Iao-1)/2+Jao
            AOprod(IJao)=SHLINTS(INDEX)
          end do ! J
          end do ! I
        end if ! LIatmshl

      end do ! Jatmshl
      end do ! Iatmshl

      return
      end subroutine PP_products
      subroutine SD_products
!************************************************************************************************************
!     Date last modified: May 10, 2011                                                         Version 3.0  *
!     Authors: Ahmad and R.A. Poirier                                                                       *
!     Description:                                                                                          *
!                                                                                                           *
!************************************************************************************************************
! Modules:

      implicit none
!
! Loop over all atomic shells
      do Iatmshl=Ifrst,Ilast
        Iatom=Basis%atmshl(Iatmshl)%ATMLST
!
        Ax=CARTESIAN(Iatom)%X     ! vector A={Ax,Ay,Az}     
        Ay=CARTESIAN(Iatom)%Y
        Az=CARTESIAN(Iatom)%Z
!
        AOI=Basis%atmshl(Iatmshl)%frstAO-1
        IF(Ishell.EQ.Jshell)Jlast=Iatmshl
      do Jatmshl=Jfrst,Jlast
        Jatom=Basis%atmshl(Jatmshl)%ATMLST
!
        Bx=CARTESIAN(Jatom)%X     ! vector B={Bx,By,Bz}
        By=CARTESIAN(Jatom)%Y
        Bz=CARTESIAN(Jatom)%Z
!
        AOJ=Basis%atmshl(Jatmshl)%frstAO-1
        LIatmshl=.false.
        if(Iatmshl.eq.Jatmshl)LIatmshl=.true.

        RABSQ=(Ax-Bx)*(Ax-Bx)+(Ay-By)*(Ay-By)+(Az-Bz)*(Az-Bz)     ! (A-B)**2

        SHLINTS(1:100)=ZERO ! will handle up to <f|f>
! Loop over primitive gaussians
!
      do Igauss=IGBGN,IGEND
        Alpha=Basis%gaussian(Igauss)%exp

      do Jgauss=JGBGN,JGEND
        Beta=Basis%gaussian(Jgauss)%exp
!
        Gama=Alpha+Beta

        Px=(Alpha*Ax+Beta*Bx)/Gama     ! vector P
        Py=(Alpha*Ay+Beta*By)/Gama
        Pz=(Alpha*Az+Beta*Bz)/Gama
!
        rpx = Px-rx     ! vector rp=P-r
        rpy = Py-ry
        rpz = Pz-rz

        SUMDSQ = rpx*rpx + rpy*rpy + rpz*rpz   ! (rp)**2

        EG_K=DEXP(-Alpha*Beta*RABSQ/Gama)      ! K  
        EG_exp=DEXP(-Gama*SUMDSQ)              ! exp
        EG=EG_K*EG_exp                         ! K*exp
!
        xA=rx-Ax     ! vector rA=(r-A)
        yA=ry-Ay
        zA=rz-Az
        xB=rx-Bx     ! vector rB=(r-B)
        yB=ry-By
        zB=rz-Bz
! S|D        
        COEF_SXX = Basis%gaussian(Igauss)%CONTRC*Basis%gaussian(Jgauss)%CONTRC
        COEF_SXY = SQRT3*Basis%gaussian(Igauss)%CONTRC*Basis%gaussian(Jgauss)%CONTRC
        
        SHLINTS(1)=SHLINTS(1)+xB*xB*COEF_SXX*EG
        SHLINTS(2)=SHLINTS(2)+yB*yB*COEF_SXX*EG
        SHLINTS(3)=SHLINTS(3)+zB*zB*COEF_SXX*EG
        SHLINTS(4)=SHLINTS(4)+xB*yB*COEF_SXY*EG
        SHLINTS(5)=SHLINTS(5)+xB*zB*COEF_SXY*EG
        SHLINTS(6)=SHLINTS(6)+yB*zB*COEF_SXY*EG
!
      end do ! Jgauss
      end do ! Igauss 
      
        IF(LIatmshl)then
          INDEX=0
          do I=1,Irange
          do J=1,Jrange
            INDEX=INDEX+1
            IF(I.GE.J)then  ! Corrected August 1,2003 had (J.ge.I)
              Iao=AOI+I
              Jao=AOJ+J
              IJao=Iao*(Iao-1)/2+Jao
              AOprod(IJao)=SHLINTS(INDEX)
            end if
          end do ! J   
          end do ! I
            else
              INDEX=0
          do I=1,Irange
          do J=1,Jrange
            INDEX=INDEX+1
            Iao=AOI+I
            Jao=AOJ+J
            IJao=Iao*(Iao-1)/2+Jao
            AOprod(IJao)=SHLINTS(INDEX)
          end do ! J
          end do ! I
        end if ! LIatmshl

      end do ! Jatmshl
      end do ! Iatmshl

      return
      end subroutine SD_products
      subroutine PD_products
!************************************************************************************************************
!     Date last modified: May 10, 2011                                                         Version 3.0  *
!     Authors: Ahmad and R.A. Poirier                                                                       *
!     Description:                                                                                          *
!                                                                                                           *
!************************************************************************************************************
! Modules:

      implicit none
!
! Loop over all atomic shells
      do Iatmshl=Ifrst,Ilast
        Iatom=Basis%atmshl(Iatmshl)%ATMLST
!
        Ax=CARTESIAN(Iatom)%X     ! vector A={Ax,Ay,Az}     
        Ay=CARTESIAN(Iatom)%Y
        Az=CARTESIAN(Iatom)%Z
!
        AOI=Basis%atmshl(Iatmshl)%frstAO-1
        IF(Ishell.EQ.Jshell)Jlast=Iatmshl
      do Jatmshl=Jfrst,Jlast
        Jatom=Basis%atmshl(Jatmshl)%ATMLST
!
        Bx=CARTESIAN(Jatom)%X     ! vector B={Bx,By,Bz}
        By=CARTESIAN(Jatom)%Y
        Bz=CARTESIAN(Jatom)%Z
!
        AOJ=Basis%atmshl(Jatmshl)%frstAO-1
        LIatmshl=.false.
        if(Iatmshl.eq.Jatmshl)LIatmshl=.true.

        RABSQ=(Ax-Bx)*(Ax-Bx)+(Ay-By)*(Ay-By)+(Az-Bz)*(Az-Bz)     ! (A-B)**2

        SHLINTS(1:100)=ZERO ! will handle up to <f|f>
! Loop over primitive gaussians
!
      do Igauss=IGBGN,IGEND
        Alpha=Basis%gaussian(Igauss)%exp

      do Jgauss=JGBGN,JGEND
        Beta=Basis%gaussian(Jgauss)%exp
!
        Gama=Alpha+Beta

        Px=(Alpha*Ax+Beta*Bx)/Gama     ! vector P
        Py=(Alpha*Ay+Beta*By)/Gama
        Pz=(Alpha*Az+Beta*Bz)/Gama
!
        rpx = Px-rx     ! vector rp=P-r
        rpy = Py-ry
        rpz = Pz-rz

        SUMDSQ = rpx*rpx + rpy*rpy + rpz*rpz   ! (rp)**2

        EG_K=DEXP(-Alpha*Beta*RABSQ/Gama)      ! K  
        EG_exp=DEXP(-Gama*SUMDSQ)              ! exp
        EG=EG_K*EG_exp                         ! K*exp
!
        xA=rx-Ax     ! vector rA=(r-A)
        yA=ry-Ay
        zA=rz-Az
        xB=rx-Bx     ! vector rB=(r-B)
        yB=ry-By
        zB=rz-Bz
! P|D
        COEF_PXX = Basis%gaussian(Igauss)%CONTRC*Basis%gaussian(Jgauss)%CONTRC
        COEF_PXY = SQRT3*Basis%gaussian(Igauss)%CONTRC*Basis%gaussian(Jgauss)%CONTRC

        SHLINTS(1)=SHLINTS(1)+xA*xB*xB*COEF_PXX*EG   !  X|XX
        SHLINTS(2)=SHLINTS(2)+xA*yB*yB*COEF_PXX*EG   !  X|YY
        SHLINTS(3)=SHLINTS(3)+xA*zB*zB*COEF_PXX*EG   !  X|ZZ
        SHLINTS(4)=SHLINTS(4)+xA*xB*yB*COEF_PXY*EG   !  X|XY
        SHLINTS(5)=SHLINTS(5)+xA*xB*zB*COEF_PXY*EG   !  X|XZ
        SHLINTS(6)=SHLINTS(6)+xA*yB*zB*COEF_PXY*EG   !  X|YZ

        SHLINTS(7)=SHLINTS(7)+yA*xB*xB*COEF_PXX*EG   !  Y|XX
        SHLINTS(8)=SHLINTS(8)+yA*yB*yB*COEF_PXX*EG   !  Y|YY
        SHLINTS(9)=SHLINTS(9)+yA*zB*zB*COEF_PXX*EG   !  Y|ZZ
        SHLINTS(10)=SHLINTS(10)+yA*xB*yB*COEF_PXY*EG !  Y|XY
        SHLINTS(11)=SHLINTS(11)+yA*xB*zB*COEF_PXY*EG !  Y|XZ
        SHLINTS(12)=SHLINTS(12)+yA*yB*zB*COEF_PXY*EG !  Y|YZ

        SHLINTS(13)=SHLINTS(13)+zA*xB*xB*COEF_PXX*EG !  Z|XX
        SHLINTS(14)=SHLINTS(14)+zA*yB*yB*COEF_PXX*EG !  Z|YY
        SHLINTS(15)=SHLINTS(15)+zA*zB*zB*COEF_PXX*EG !  Z|ZZ
        SHLINTS(16)=SHLINTS(16)+zA*xB*yB*COEF_PXY*EG !  Z|XY
        SHLINTS(17)=SHLINTS(17)+zA*xB*zB*COEF_PXY*EG !  Z|XZ
        SHLINTS(18)=SHLINTS(18)+zA*yB*zB*COEF_PXY*EG !  Z|YZ
!
      end do ! Jgauss
      end do ! Igauss 
      
        IF(LIatmshl)then
          INDEX=0
          do I=1,Irange
          do J=1,Jrange
            INDEX=INDEX+1
            IF(I.GE.J)then  ! Corrected August 1,2003 had (J.ge.I)
              Iao=AOI+I
              Jao=AOJ+J
              IJao=Iao*(Iao-1)/2+Jao
              AOprod(IJao)=SHLINTS(INDEX)
            end if
          end do ! J   
          end do ! I
            else
              INDEX=0
          do I=1,Irange
          do J=1,Jrange
            INDEX=INDEX+1
            Iao=AOI+I
            Jao=AOJ+J
            IJao=Iao*(Iao-1)/2+Jao
            AOprod(IJao)=SHLINTS(INDEX)
          end do ! J
          end do ! I
        end if ! LIatmshl

      end do ! Jatmshl
      end do ! Iatmshl

      return
      end subroutine PD_products
      subroutine DD_products
!************************************************************************************************************
!     Date last modified: May 10, 2011                                                         Version 3.0  *
!     Authors: Ahmad and R.A. Poirier                                                                       *
!     Description:                                                                                          *
!                                                                                                           *
!************************************************************************************************************
! Modules:

      implicit none
!
! Loop over all atomic shells
      do Iatmshl=Ifrst,Ilast
        Iatom=Basis%atmshl(Iatmshl)%ATMLST
!
        Ax=CARTESIAN(Iatom)%X     ! vector A={Ax,Ay,Az}     
        Ay=CARTESIAN(Iatom)%Y
        Az=CARTESIAN(Iatom)%Z
!
        AOI=Basis%atmshl(Iatmshl)%frstAO-1
        IF(Ishell.EQ.Jshell)Jlast=Iatmshl
      do Jatmshl=Jfrst,Jlast
        Jatom=Basis%atmshl(Jatmshl)%ATMLST
!
        Bx=CARTESIAN(Jatom)%X     ! vector B={Bx,By,Bz}
        By=CARTESIAN(Jatom)%Y
        Bz=CARTESIAN(Jatom)%Z
!
        AOJ=Basis%atmshl(Jatmshl)%frstAO-1
        LIatmshl=.false.
        if(Iatmshl.eq.Jatmshl)LIatmshl=.true.

        RABSQ=(Ax-Bx)*(Ax-Bx)+(Ay-By)*(Ay-By)+(Az-Bz)*(Az-Bz)     ! (A-B)**2

        SHLINTS(1:100)=ZERO ! will handle up to <f|f>
! Loop over primitive gaussians
!
      do Igauss=IGBGN,IGEND
        Alpha=Basis%gaussian(Igauss)%exp

      do Jgauss=JGBGN,JGEND
        Beta=Basis%gaussian(Jgauss)%exp
!
        Gama=Alpha+Beta

        Px=(Alpha*Ax+Beta*Bx)/Gama     ! vector P
        Py=(Alpha*Ay+Beta*By)/Gama
        Pz=(Alpha*Az+Beta*Bz)/Gama
!
        rpx = Px-rx     ! vector rp=P-r
        rpy = Py-ry
        rpz = Pz-rz

        SUMDSQ = rpx*rpx + rpy*rpy + rpz*rpz   ! (rp)**2

        EG_K=DEXP(-Alpha*Beta*RABSQ/Gama)      ! K  
        EG_exp=DEXP(-Gama*SUMDSQ)              ! exp
        EG=EG_K*EG_exp                         ! K*exp
!
        xA=rx-Ax     ! vector rA=(r-A)
        yA=ry-Ay
        zA=rz-Az
        xB=rx-Bx     ! vector rB=(r-B)
        yB=ry-By
        zB=rz-Bz
! D|D        
        COEF_XXYY = Basis%gaussian(Igauss)%CONTRC*Basis%gaussian(Jgauss)%CONTRC
        COEF_XXXY = SQRT3*COEF_XXYY
        COEF_XYXY = THREE*COEF_XXYY
       
        SHLINTS(1)=SHLINTS(1)+xA*xA*xB*xB*COEF_XXYY*EG   ! XX|XX
        SHLINTS(2)=SHLINTS(2)+xA*xA*yB*yB*COEF_XXYY*EG   ! XX|YY
        SHLINTS(3)=SHLINTS(3)+xA*xA*zB*zB*COEF_XXYY*EG   ! XX|ZZ
        SHLINTS(4)=SHLINTS(4)+xA*xA*xB*yB*COEF_XXXY*EG   ! XX|XY
        SHLINTS(5)=SHLINTS(5)+xA*xA*xB*zB*COEF_XXXY*EG   ! XX|XZ
        SHLINTS(6)=SHLINTS(6)+xA*xA*yB*zB*COEF_XXXY*EG   ! XX|YZ

        SHLINTS(7)=SHLINTS(7)+yA*yA*xB*xB*COEF_XXYY*EG   ! YY|XX
        SHLINTS(8)=SHLINTS(8)+yA*yA*yB*yB*COEF_XXYY*EG   ! YY|YY
        SHLINTS(9)=SHLINTS(9)+yA*yA*zB*zB*COEF_XXYY*EG   ! YY|ZZ
        SHLINTS(10)=SHLINTS(10)+yA*yA*xB*yB*COEF_XXXY*EG ! YY|XY
        SHLINTS(11)=SHLINTS(11)+yA*yA*xB*zB*COEF_XXXY*EG ! YY|XZ
        SHLINTS(12)=SHLINTS(12)+yA*yA*yB*zB*COEF_XXXY*EG ! YY|YZ

        SHLINTS(13)=SHLINTS(13)+zA*zA*xB*xB*COEF_XXYY*EG ! ZZ|XX
        SHLINTS(14)=SHLINTS(14)+zA*zA*yB*yB*COEF_XXYY*EG ! ZZ|YY
        SHLINTS(15)=SHLINTS(15)+zA*zA*zB*zB*COEF_XXYY*EG ! ZZ|ZZ
        SHLINTS(16)=SHLINTS(16)+zA*zA*xB*yB*COEF_XXXY*EG ! ZZ|XY
        SHLINTS(17)=SHLINTS(17)+zA*zA*xB*zB*COEF_XXXY*EG ! ZZ|XZ
        SHLINTS(18)=SHLINTS(18)+zA*zA*yB*zB*COEF_XXXY*EG ! ZZ|YZ

        SHLINTS(19)=SHLINTS(19)+xA*yA*xB*xB*COEF_XXXY*EG ! XY|XX
        SHLINTS(20)=SHLINTS(20)+xA*yA*yB*yB*COEF_XXXY*EG ! XY|YY
        SHLINTS(21)=SHLINTS(21)+xA*yA*zB*zB*COEF_XXXY*EG ! XY|ZZ
        SHLINTS(22)=SHLINTS(22)+xA*yA*xB*yB*COEF_XYXY*EG ! XY|XY
        SHLINTS(23)=SHLINTS(23)+xA*yA*xB*zB*COEF_XYXY*EG ! XY|XZ
        SHLINTS(24)=SHLINTS(24)+xA*yA*yB*zB*COEF_XYXY*EG ! XY|YZ

        SHLINTS(25)=SHLINTS(25)+xA*zA*xB*xB*COEF_XXXY*EG ! XZ|XX
        SHLINTS(26)=SHLINTS(26)+xA*zA*yB*yB*COEF_XXXY*EG ! XZ|YY
        SHLINTS(27)=SHLINTS(27)+xA*zA*zB*zB*COEF_XXXY*EG ! XZ|ZZ
        SHLINTS(28)=SHLINTS(28)+xA*zA*xB*yB*COEF_XYXY*EG ! XZ|XY
        SHLINTS(29)=SHLINTS(29)+xA*zA*xB*zB*COEF_XYXY*EG ! XZ|XZ
        SHLINTS(30)=SHLINTS(30)+xA*zA*yB*zB*COEF_XYXY*EG ! XZ|YZ

        SHLINTS(31)=SHLINTS(31)+yA*zA*xB*xB*COEF_XXXY*EG ! YZ|XX
        SHLINTS(32)=SHLINTS(32)+yA*zA*yB*yB*COEF_XXXY*EG ! YZ|YY
        SHLINTS(33)=SHLINTS(33)+yA*zA*zB*zB*COEF_XXXY*EG ! YZ|ZZ
        SHLINTS(34)=SHLINTS(34)+yA*zA*xB*yB*COEF_XYXY*EG ! YZ|XY
        SHLINTS(35)=SHLINTS(35)+yA*zA*xB*zB*COEF_XYXY*EG ! YZ|XZ
        SHLINTS(36)=SHLINTS(36)+yA*zA*yB*zB*COEF_XYXY*EG ! YZ|YZ
!
      end do ! Jgauss
      end do ! Igauss 
      
        IF(LIatmshl)then
          INDEX=0
          do I=1,Irange
          do J=1,Jrange
            INDEX=INDEX+1
            IF(I.GE.J)then  ! Corrected August 1,2003 had (J.ge.I)
              Iao=AOI+I
              Jao=AOJ+J
              IJao=Iao*(Iao-1)/2+Jao
              AOprod(IJao)=SHLINTS(INDEX)
            end if
          end do ! J   
          end do ! I
            else
              INDEX=0
          do I=1,Irange
          do J=1,Jrange
            INDEX=INDEX+1
            Iao=AOI+I
            Jao=AOJ+J
            IJao=Iao*(Iao-1)/2+Jao
            AOprod(IJao)=SHLINTS(INDEX)
          end do ! J
          end do ! I
        end if ! LIatmshl

      end do ! Jatmshl
      end do ! Iatmshl

      return
      end subroutine DD_products
      end subroutine AO_PRODUCTS

