      subroutine SORTALL
!***********************************************************************
!     Date last modified: June 18, 2005               Version 2.0      *
!     Author: R.A. Poirier, Kushan Saputantri                          *
!     Description:Different sorting options are called according to the*
!     Mtype                                                            *
!***********************************************************************
!Modules:
      USE mod_idfclc
!     
      implicit none
!put the error massage here
      select case(Mtype)

      case(2)
        call SORTD2
      case(3)
        call SORTD3
      case(4)
        call SORTD4
      case(5)
        call SORTD5
      case(6)
        call SORTD6
      case(7)
        call SORTD7
      case(8)
        call SORTD8
      end select
      return

CONTAINS!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine SORTD2
!***********************************************************************
!     Date last modified: May  07, 1996                                *
!     Author: Dale Keefe, R.A. Poirier                                 *
!     Description:  MUN Version                                        *
!     This routine sorts the integrals created for Mtype = 2 or 5.     *
!***********************************************************************
! Modules:

      implicit none
!
! Local scalars:
      integer I,Iao,IND,INDI,Jao,JKLRNG,JLRNG,Kao,KLRNG,Lao
!
! Work arrays:
      double precision TQ(LENTQ)
!
! Begin:
      KLRNG=Krange*Lrange
      JLRNG=Jrange*Lrange
      JKLRNG=Jrange*KLRNG
      do I=1,LENTQ
        TQ(I)=ILJKS(I)
        ILJKS(I)=ZERO
      end do ! I
!
! Have (IJ/KL), find (IK/JL) and (IL/JK) with K > L.
      do Iao=1,Irange
        INDI=(Iao-1)*JKLRNG
      do Jao=1,Jrange
      do Kao=1,Krange
      do Lao=1,Kao
        IND=INDI+(Jao-1)*KLRNG+(Kao-1)*Lrange+Lao
        ILJKS(IND)=TQ(INDI+(Kao-1)*JLRNG+(Jao-1)*Lrange+Lao)
      IF(Kao.NE.Lao)then
        IKJLS(IND)=TQ(INDI+(Lao-1)*JLRNG+(Jao-1)*Lrange+Kao)
      end if
      end do ! Lao
      end do ! Kao
      end do ! Jao
      end do ! Iao
!
! End of routine SORTD2
      return
      end subroutine SORTD2
      subroutine SORTD3
!***********************************************************************
!     Date last modified: May  07, 1996                                *
!     Author: Dale Keefe, R.A. Poirier                                 *
!     Description:  MUN Version                                        *
!     This routine sorts the integrals created for Mtype = 3 (IJJL).   *
!***********************************************************************
! Modules:

      implicit none
!
! Local scalars:
      integer I,Iao,IND,INDI,Jao,JKLRNG,JKRNG,JLRNG,Kao,KLRNG,Lao
      double precision TQW1(LENTQ),TQW2(LENTQ)
!
! Begin:
      KLRNG=Krange*Lrange
      JKRNG=Jrange*Krange
      JLRNG=Jrange*Lrange
      JKLRNG=Jrange*KLRNG
!
      do I=1,LENTQ
        TQW2(I)=IJKLS(I)
        TQW1(I)=ILJKS(I)
        IJKLS(I)=ZERO
        ILJKS(I)=ZERO
        IKJLS(I)=ZERO
      end do ! I
!
! Have (IJ/KL), find (IK/JL) and (IL/JK) with J > Kao.
      do Iao=1,Irange
        INDI=(Iao-1)*JKLRNG
      do Jao=1,Jrange
      do Kao=1,Jao
      do Lao=1,Lrange
        IND=INDI+(Jao-1)*KLRNG+(Kao-1)*Lrange+Lao
      IF(Jao.GT.Kao)then
        IJKLS(IND)=TQW2(IND)
        ILJKS(IND)=TQW2(INDI+(Kao-1)*JLRNG+(Jao-1)*Lrange+Lao)
        IKJLS(IND)=TQW1(INDI+(Lao-1)*JKRNG+(Jao-1)*Krange+Kao)
      else
        IJKLS(IND)=TQW2(IND)
        ILJKS(IND)=TQW1(INDI+(Lao-1)*JKRNG+(Jao-1)*Jrange+Kao)
      end if
      end do ! Lao
      end do ! Kao
      end do ! Jao
      end do ! Iao
!
! End of routine SORTD3
      return
      end subroutine SORTD3
      subroutine SORTD4
!***********************************************************************
!     Date last modified: July 07, 1992                                *
!     Author: Dale Keefe, R.A. Poirier                                 *
!     Description:  MUN Version                                        *
!     This routine sorts the integrals created for Mtype = 4 or 7(IJJJ)*
!***********************************************************************
! Modules:

      implicit none
!
! Local scalars:
      integer I,Iao,IND,INDI,Jao,JKLRNG,Kao,KLRNG,Lao
      double precision TQW1(LENTQ)
!
! Begin:
      KLRNG=Krange*Lrange
      JKLRNG=Jrange*KLRNG

      do I=1,LENTQ
        TQW1(I)=IJKLS(I)
        IJKLS(I)=ZERO
        ILJKS(I)=ZERO
        IKJLS(I)=ZERO
      end do ! I
!
! Have (IJ/KL), find (IK/JL) and (IL/JK) with J > K > L.
      do Iao=1,Irange
        INDI=(Iao-1)*JKLRNG
      do Jao=1,Jrange
      do Kao=1,Jao
      do Lao=1,Kao
        IND=INDI+(Jao-1)*KLRNG+(Kao-1)*Lrange+Lao
      IF(Jao.GT.Kao.and.Kao.GT.Lao)then
        IJKLS(IND)=TQW1(IND)
        ILJKS(IND)=TQW1(INDI+(Kao-1)*KLRNG+(Jao-1)*Lrange+Lao)
        IKJLS(IND)=TQW1(INDI+(Lao-1)*KLRNG+(Jao-1)*Lrange+Kao)
      else if (Jao.GT.Kao)then
        IJKLS(IND)=TQW1(IND)
        ILJKS(IND)=TQW1(INDI+(Kao-1)*KLRNG+(Jao-1)*Lrange+Lao)
      else if (Jao.GT.Lao)then
        IJKLS(IND)=TQW1(IND)
        ILJKS(IND)=TQW1(INDI+(Lao-1)*KLRNG+(Jao-1)*Lrange+Kao)
      else
        IJKLS(IND)=TQW1(IND)
      end if
      end do ! Lao
      end do ! Kao
      end do ! Jao
      end do ! Iao
!
! End of routine SORTD4
      return
      end subroutine SORTD4
      subroutine SORTD5
!***********************************************************************
!     Date last modified: May  20, 1996                                *
!     Author: Dale Keefe, R.A. Poirier                                 *
!     Description:  MUN Version                                        *
!     This routine sorts the integrals created for Mtype = 2 or 5.     *
!     Can be improved: This is a special case to avoid shell switching *
!***********************************************************************
! Modules:

      implicit none
!
! Local scalars:
      integer I,Iao,IND,INDI,Jao,JKLRNG,JKRNG,Kao,KLRNG,Lao
! Work arrays:
      double precision TQ(LENTQ)
!
! Begin:
      KLRNG=Krange*Lrange
      JKRNG=Jrange*Krange
      JKLRNG=Jrange*KLRNG

      do I=1,LENTQ
      TQ(I)=ILJKS(I)
      ILJKS(I)=ZERO
      IKJLS(I)=ZERO
      end do ! I
!
! Have (IJ/KL), find (IK/JL) and (IL/JK) with K > L.
      do Iao=1,Irange
        INDI=(Iao-1)*JKLRNG
      do Jao=1,Iao
      do Kao=1,Krange
      do Lao=1,Lrange
        IND=INDI+(Jao-1)*KLRNG+(Kao-1)*Lrange+Lao
      if(Iao.eq.Jao)then
        ILJKS(IND)=TQ(INDI+(Lao-1)*JKRNG+(Jao-1)*Krange+Kao)
      else IF(Iao.GT.Jao)then
        ILJKS(IND)=TQ((Jao-1)*JKLRNG+(Lao-1)*JKRNG+(Iao-1)*Krange+Kao)
        IKJLS(IND)=TQ(INDI+(Lao-1)*JKRNG+(Jao-1)*Krange+Kao)
      end if
      end do ! Lao
      end do ! Kao
      end do ! Jao
      end do ! Iao
!
! End of routine SORTD5
      return
      end subroutine SORTD5
      subroutine SORTD6
!***********************************************************************
!     Date last modified: May  07, 1996                                *
!     Author: Dale Keefe, R.A. Poirier                                 *
!     Description:  MUN Version                                        *
!     This routine sorts the integrals created for Mtype = 6 (IIKK)    *
!***********************************************************************
! Modules:

      implicit none
!
! Local scalars:
      integer I,Iao,IND,INDI,Jao,JKLRNG,JLRNG,Kao,KLRNG,Lao
      double precision TQW1(LENTQ)
!
! Begin:
      KLRNG=Krange*Lrange
      JLRNG=Jrange*Lrange
      JKLRNG=Jrange*KLRNG
!
      do I=1,LENTQ
      TQW1(I)=ILJKS(I)
      ILJKS(I)=ZERO
      IKJLS(I)=ZERO
      end do ! I
!
! Have (IJ/KL), find (IK/JL) and (IL/JK) with I > J and K > L.
      do Iao=1,Irange
        INDI=(Iao-1)*JKLRNG
      do Jao=1,Iao
      do Kao=1,Krange
      do Lao=1,Kao
        IND=INDI+(Jao-1)*KLRNG+(Kao-1)*Lrange+Lao
      IF(Iao.GT.Jao.and.Kao.GT.Lao)then
        ILJKS(IND)=TQW1(INDI+(Kao-1)*JLRNG+(Jao-1)*Lrange+Lao)
        IKJLS(IND)=TQW1(INDI+(Lao-1)*JLRNG+(Jao-1)*Lrange+Kao)
      else if (Iao.GE.Jao)then
        ILJKS(IND)=TQW1(INDI+(Kao-1)*JLRNG+(Jao-1)*Lrange+Lao)
      else if (Kao.GT.Lao)then
        ILJKS(IND)=TQW1(INDI+(Kao-1)*JLRNG+(Jao-1)*Lrange+Lao)
      end if
      end do ! Lao
      end do ! Kao
      end do ! Jao
      end do ! Iao
!
! End of routine SORTD6
      return
      end subroutine SORTD6
      subroutine SORTD7
!***********************************************************************
!     Date last modified: May  08, 199                                *
!     Author: Dale Keefe, R.A. Poirier                                 *
!     Description:  MUN Version                                        *
!     This routine sorts the integrals created for Mtype = 7(IIIL)     *
!***********************************************************************
! Modules:

      implicit none
!
! Local scalars:
      integer I,Iao,IND,INDI,Jao,JKLRNG,Kao,KLRNG,Lao
      double precision TQW1(LENTQ)
!
! Begin:
      KLRNG=Krange*Lrange
      JKLRNG=Jrange*KLRNG

      do I=1,LENTQ
      TQW1(I)=IJKLS(I)
      IJKLS(I)=ZERO
      ILJKS(I)=ZERO
      IKJLS(I)=ZERO
      end do ! I
!
! Have (IJ/KL), find (IK/JL) and (IL/JK) with J > K > L.
      do Iao=1,Irange
        INDI=(Iao-1)*JKLRNG
      do Jao=1,Iao
      do Kao=1,Jao
      do Lao=1,Lrange
        IND=INDI+(Jao-1)*KLRNG+(Kao-1)*Lrange+Lao
      IF(Iao.GT.Jao.and.Jao.GT.Kao)then
        IJKLS(IND)=TQW1(IND)
        ILJKS(IND)= &
        TQW1(INDI+(Kao-1)*KLRNG+(Jao-1)*Lrange+Lao)
        IKJLS(IND)= &
        TQW1((Jao-1)*jklrng+(Kao-1)*KLRNG+(Iao-1)*Lrange+Lao)
      else if (Iao.GT.Jao)then
        IJKLS(IND)=TQW1(IND)
        ILJKS(IND)= &
        TQW1((Jao-1)*jklrng+(Kao-1)*KLRNG+(Iao-1)*Lrange+Lao)
      else if (Jao.GT.Kao)then
        IJKLS(IND)=TQW1(IND)
        ILJKS(IND)=TQW1(INDI+(Kao-1)*KLRNG+(Jao-1)*Lrange+Lao)
      else
        IJKLS(IND)=TQW1(IND)
      end if
      end do ! Lao
      end do ! Kao
      end do ! Jao
      end do ! Iao
!
! End of routine sortd7
      return
      end subroutine SORTD7
      subroutine SORTD8
!***********************************************************************
!     Date last modified: May  07, 1996                                *
!     Author: Dale Keefe, R.A. Poirier                                 *
!     Description:  MUN Version                                        *
!     This routine sorts the integrals created for Mtype = 8 (IIII)    *
!***********************************************************************
! Modules:

      implicit none
!
! Local scalars:
      integer I,Iao,IND,INDI,Jao,JKLRNG,Kao,KLRNG,Lao
      double precision TQW1(LENTQ)
!
! Begin:
      KLRNG=Krange*Lrange
      JKLRNG=Jrange*KLRNG
!
      do I=1,LENTQ
      TQW1(I)=IJKLS(I)
      IJKLS(I)=ZERO
      ILJKS(I)=ZERO
      IKJLS(I)=ZERO
      end do ! I
!
! Have (IJ/KL), find (IK/JL) and (IL/JK) with I > J > K > L.
      do Iao=1,Irange
        INDI=(Iao-1)*JKLRNG
      do Jao=1,Iao
      do Kao=1,Jao
      do Lao=1,Kao
        IND=INDI+(Jao-1)*KLRNG+(Kao-1)*Lrange+Lao
      IF(Iao.GT.Jao.and.Jao.GT.Kao.and.Kao.GT.Lao)then            ! ijkl
        IJKLS(IND)=TQW1(IND)
        ILJKS(IND)=TQW1(INDI+(Kao-1)*KLRNG+(Jao-1)*Lrange+Lao)
        IKJLS(IND)=TQW1(INDI+(Lao-1)*KLRNG+(Jao-1)*Lrange+Kao)
      else if(Iao.GT.Jao.and.Jao.GT.Kao)then                  ! ijkk
        IJKLS(IND)=TQW1(IND)
        ILJKS(IND)=TQW1(INDI+(Kao-1)*KLRNG+(Jao-1)*Lrange+Lao)
      else if(Iao.GT.Jao.and.Jao.GT.Lao)then                  ! ijjl
        IJKLS(IND)=TQW1(IND)
        ILJKS(IND)=TQW1(INDI+(Lao-1)*KLRNG+(Jao-1)*Lrange+Kao)
      else if(Iao.GT.Kao.and.Kao.GT.Lao)then                  ! iikl
        IJKLS(IND)=TQW1(IND)
        ILJKS(IND)=TQW1(INDI+(Kao-1)*KLRNG+(Jao-1)*Lrange+Lao)
      else if(Iao.GT.Kao.and.Jao.GT.Lao)then                  ! iikk
        IJKLS(IND)=TQW1(IND)
        ILJKS(IND)=TQW1(INDI+(Kao-1)*KLRNG+(Jao-1)*Lrange+Lao)
      else                                            ! ijjj, iiil, iiii
        IJKLS(IND)=TQW1(IND)
      end if
      end do ! Lao
      end do ! Kao
      end do ! Jao
      end do ! Iao
!
! End of routine SORTD8
      return
      end subroutine SORTD8
      end subroutine SORTALL
