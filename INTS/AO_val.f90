      subroutine AO_val (rx, ry, rz)
!******************************************************************************
!     Date last modified: May 22, 2015                           Version 3.0  *
!     Authors: JWH                                                            *
!     Description: Determine the value of AOs at grid points.                 *
!******************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE type_basis_set
      USE matrix_print
      USE QM_defaults
      USE AO_values

      implicit none
!
! Output array:
!
! Local scalars:
      integer :: Ishell,Ifrst,Ilast
      integer :: Istart,Iend
      integer :: Iatmshl
      integer :: LI
      integer :: Iatom, Irange, Igauss
      integer :: IGBGN,JGBGN,IGend,JGend
      integer :: Iao,AOI,I
      double precision :: SQRT3,alpha,EG
      double precision :: rA2,Ax,Ay,Az,rx,ry,rz,rAx,rAy,rAz
!
! Local arrays
      double precision :: SHLINTS(100)
!
! Begin:
      SQRT3=DSQRT(THREE)
      AOval=0.0D0
!
! Begin loop over shells.
!
! Loop over elemental SHELLs
! Loop over Ishell.
      do Ishell=1,Basis%Nshells
        LI=Basis%shell(Ishell)%Xtype
        Istart=Basis%shell(Ishell)%Xstart
        Iend=Basis%shell(Ishell)%XEND
        Irange=Iend-Istart+1
        IGBGN=Basis%shell(Ishell)%EXPBGN
        IGEND=Basis%shell(Ishell)%EXPEND
        Ifrst=Basis%shell(Ishell)%frstSHL
        Ilast=Basis%shell(Ishell)%lastSHL
!
        select case (LI)
! s
        case (0)
          call s_val
! p
        case (1)
          call p_val
! d  
        case (2)
          call d_val
!
        case default
          write(UNIout,*)'ERROR> AO_VAL: unable to determine AO val for LI:',LI
          stop ' ERROR> AO_VAL: AO type not supported'
        end select
!
! End of loop over shells.
      
      end do ! Ishell
!
! End of routine AO_val
      return
      CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine s_val
!********************************************************************************
!     Date last modified: May 22, 2015                             Version 3.0  *
!     Authors: JWH                                                              *
!     Description: Get value of s-type AOs at grid points.                      *
!********************************************************************************
! Modules:

      implicit none
!
! Loop over all atomic shells
      do Iatmshl=Ifrst,Ilast
        Iatom=Basis%atmshl(Iatmshl)%ATMLST
!
        Ax=CARTESIAN(Iatom)%X     ! vector A={Ax,Ay,Az}     
        Ay=CARTESIAN(Iatom)%Y
        Az=CARTESIAN(Iatom)%Z
!
        AOI=Basis%atmshl(Iatmshl)%frstAO-1
!
        SHLINTS(1:100)=ZERO ! will handle up to <f|f>
! Loop over primitive gaussians
!
        do Igauss=IGBGN,IGEND
!
          alpha=Basis%gaussian(Igauss)%exp
!
          rAx = rx - Ax     ! vector rA= r-A
          rAy = ry - Ay
          rAz = rz - Az

          rA2 = rAx*rAx + rAy*rAy + rAz*rAz   ! (rA)**2

          EG = dexp(-alpha*(rA2))             ! e^(-alpha*rA^2)
!
! contract 
          SHLINTS(1) = SHLINTS(1)+Basis%gaussian(Igauss)%CONTRC*EG
!
        end do ! Igauss 
!      
! Save in AOval
        do I=1,Irange
          IAO = AOI+I
          AOval(IAO)=SHLINTS(I)
        end do ! I
!
      end do ! Iatmshl
!
      return
      end subroutine s_val
      subroutine p_val
!********************************************************************************
!     Date last modified: May 22, 2015                             Version 3.0  *
!     Authors: JWH                                                              *
!     Description: Get value of p-type AOs at grid points.                      *
!********************************************************************************
! Modules:

      implicit none
!
! Loop over all atomic shells
      do Iatmshl=Ifrst,Ilast
        Iatom=Basis%atmshl(Iatmshl)%ATMLST
!
        Ax=CARTESIAN(Iatom)%X     ! vector A={Ax,Ay,Az}     
        Ay=CARTESIAN(Iatom)%Y
        Az=CARTESIAN(Iatom)%Z
!
        AOI=Basis%atmshl(Iatmshl)%frstAO-1
!
        SHLINTS(1:100)=ZERO ! will handle up to <f|f>
! Loop over primitive gaussians
!
        do Igauss=IGBGN,IGEND
!
          alpha=Basis%gaussian(Igauss)%exp
!
          rAx = rx - Ax     ! vector rA= r-A
          rAy = ry - Ay
          rAz = rz - Az

          rA2 = rAx*rAx + rAy*rAy + rAz*rAz   ! (rA)**2

          EG = dexp(-alpha*(rA2))             ! e^(-alpha*rA^2)
!
! contract 
          SHLINTS(1) = SHLINTS(1)+Basis%gaussian(Igauss)%CONTRC*rAx*EG
          SHLINTS(2) = SHLINTS(2)+Basis%gaussian(Igauss)%CONTRC*rAy*EG
          SHLINTS(3) = SHLINTS(3)+Basis%gaussian(Igauss)%CONTRC*rAz*EG
!
        end do ! Igauss 
!      
! Save in AOval
        do I=1,Irange
          IAO = AOI+I
          AOval(IAO)=SHLINTS(I)
        end do ! I
!
      end do ! Iatmshl
!
      return
      end subroutine p_val
      subroutine d_val
!********************************************************************************
!     Date last modified: May 22, 2015                             Version 3.0  *
!     Authors: JWH                                                              *
!     Description: Get value of d-type AOs at grid points.                      *
!********************************************************************************
! Modules:

      implicit none
!
! Loop over all atomic shells
      do Iatmshl=Ifrst,Ilast
        Iatom=Basis%atmshl(Iatmshl)%ATMLST
!
        Ax=CARTESIAN(Iatom)%X     ! vector A={Ax,Ay,Az}     
        Ay=CARTESIAN(Iatom)%Y
        Az=CARTESIAN(Iatom)%Z
!
        AOI=Basis%atmshl(Iatmshl)%frstAO-1
!
        SHLINTS(1:100)=ZERO ! will handle up to <f|f>
! Loop over primitive gaussians
!
        do Igauss=IGBGN,IGEND
!
          alpha=Basis%gaussian(Igauss)%exp
!
          rAx = rx - Ax     ! vector rA= r-A
          rAy = ry - Ay
          rAz = rz - Az

          rA2 = rAx*rAx + rAy*rAy + rAz*rAz   ! (rA)**2

          EG = dexp(-alpha*(rA2))             ! e^(-alpha*rA^2)
!
! contract 
          SHLINTS(1) = SHLINTS(1)+Basis%gaussian(Igauss)%CONTRC*rAx*rAx*EG
          SHLINTS(2) = SHLINTS(2)+Basis%gaussian(Igauss)%CONTRC*rAy*rAy*EG
          SHLINTS(3) = SHLINTS(3)+Basis%gaussian(Igauss)%CONTRC*rAz*rAz*EG
          SHLINTS(4) = SHLINTS(4)+Basis%gaussian(Igauss)%CONTRC*rAx*rAy*EG*SQRT3
          SHLINTS(5) = SHLINTS(5)+Basis%gaussian(Igauss)%CONTRC*rAx*rAz*EG*SQRT3
          SHLINTS(6) = SHLINTS(6)+Basis%gaussian(Igauss)%CONTRC*rAy*rAz*EG*SQRT3
!
        end do ! Igauss 
!      
! Save in AOval
        do I=1,Irange
          IAO = AOI+I
          AOval(IAO)=SHLINTS(I)
        end do ! I
!
      end do ! Iatmshl
!
      return
      end subroutine d_val
      end subroutine AO_val
