      subroutine GET_MO_point(rv, Nact, MO, phi)
!*******************************************************************
!   Date last modified: May 26, 2021                               *
!   Author: JWH                                                    *
!   Description: Calculate the value of each MO at a point, where  *
!                Nact is the number of active MOs.                 * 
!*******************************************************************
! Modules:
      USE QM_objects
      USE AO_values
      USE module_grid_points

      implicit none
!
! Input scalars:
      integer :: Nact
!
! Input arrays:
      double precision :: MO(Nbasis,Nbasis), rv(3)
!
! Output arrays:
      double precision :: phi(Nbasis)
!
! Local scalars:
      integer   :: aMO, iAO
!
! Begin:
!
      allocate(AOval(1:Nbasis))
!
      phi = 0.0D0
!
      call AO_val(rv(1), rv(2), rv(3))
!
      do aMO = 1,Nact
        do iAO = 1,Nbasis
          phi(aMO) = phi(aMO) + MO(iAO,aMO)*AOval(iAO)
        end do ! iAO
      end do ! aMO
!
      deallocate(AOval)
!
      end subroutine GET_MO_point
      subroutine GET_MOgrad_point(rv, Nact, MO, dphi)
!*******************************************************************
!   Date last modified: December 13, 2021                          *
!   Author: JWH                                                    *
!   Description: Calculate the gradient of each MO at a point,     *
!                where Nact is the number of active MOs.           * 
!*******************************************************************
! Modules:
      USE QM_objects
      USE AO_values
      USE module_grid_points

      implicit none
!
! Input scalars:
      integer :: Nact
!
! Input arrays:
      double precision :: MO(Nbasis,Nbasis), rv(3)
!
! Output arrays:
      double precision :: dphi(Nbasis,3)
!
! Local scalars:
      integer   :: aMO, iAO
!
! Begin:
!
      allocate(dAOx(1:Nbasis),dAOy(1:Nbasis),dAOz(1:Nbasis))
!
      dphi = 0.0D0
!
      call AO_gradient(rv(1), rv(2), rv(3))
!
      do aMO = 1,Nact
        do iAO = 1,Nbasis
          dphi(aMO,1) = dphi(aMO,1) + MO(iAO,aMO)*dAOx(iAO)
          dphi(aMO,2) = dphi(aMO,2) + MO(iAO,aMO)*dAOy(iAO)
          dphi(aMO,3) = dphi(aMO,3) + MO(iAO,aMO)*dAOz(iAO)
        end do ! iAO
      end do ! aMO
!
      deallocate(dAOx,dAOy,dAOz)
!
      end subroutine GET_MOgrad_point
      subroutine GET_MOlap_point(rv, Nact, MO, d2phi)
!*******************************************************************
!   Date last modified: December 13, 2021                          *
!   Author: JWH                                                    *
!   Description: Calculate the Laplacian of each MO at a point,    *
!                where Nact is the number of active MOs.           * 
!*******************************************************************
! Modules:
      USE QM_objects
      USE AO_values
      USE module_grid_points

      implicit none
!
! Input scalars:
      integer :: Nact
!
! Input arrays:
      double precision :: MO(Nbasis,Nbasis), rv(3)
!
! Output arrays:
      double precision :: d2phi(Nbasis)
!
! Local scalars:
      integer   :: aMO, iAO
!
! Begin:
!
      allocate(d2AO(1:Nbasis))
!
      d2phi = 0.0D0
!
      call AO_Laplacian(rv(1), rv(2), rv(3))
!
      do aMO = 1,Nact
        do iAO = 1,Nbasis
          d2phi(aMO) = d2phi(aMO) + MO(iAO,aMO)*d2AO(iAO)
        end do ! iAO
      end do ! aMO
!
      deallocate(d2AO)
!
      end subroutine GET_MOlap_point
      subroutine BLD_MO4prod(rv, tp, MO)
!*******************************************************************
!   Date last modified: May 26, 2021                               *
!   Author: JWH                                                    *
!   Description: Calculate the value of the general 4-MO product.  *
!*******************************************************************
! Modules:
      USE AO_values
      USE module_grid_points
      USE QM_objects

      implicit none
!
! Input arrays:
      double precision :: rv(3), tp(3)
      double precision :: MO(Nbasis,Nbasis)
!
! Local scalars:
      integer   :: pMO, qMO, rMO, sMO, pqrs_index
!
! Local arrays:
      double precision, allocatable, dimension(:) :: phi_rv, phi_tp
!
! Begin:
!
! rv - grid point
! tp - test point
!
      allocate(phi_rv(Nbasis),phi_tp(Nbasis))

      MO4prod = 0.0D0
!
      call GET_MO_point(rv, Nbasis, MO, phi_rv)
      call GET_MO_point(tp, Nbasis, MO, phi_tp)
!
      pqrs_index = 0
      do pMO = 1, Nbasis
        do qMO = 1, Nbasis
          do rMO = 1, Nbasis
            do sMO = 1, Nbasis
              pqrs_index = pqrs_index + 1
              MO4prod(pqrs_index) = phi_rv(pMO)*phi_tp(qMO)*phi_rv(rMO)*phi_tp(sMO)
            end do ! sMO
          end do ! rMO
        end do ! qMO
      end do ! pMO
!
      deallocate(phi_rv,phi_tp)
!
      end subroutine BLD_MO4prod
      subroutine BLD_MO4prodJK(rv, tp, Nact, MO)
!*******************************************************************
!   Date last modified: October 18, 2021                           *
!   Author: JWH                                                    *
!   Description: Calculate the value of the 2-index 4-MO product,  *
!                for a set of active MOs.                          *
!*******************************************************************
! Modules:
      USE AO_values
      USE module_grid_points
      USE QM_objects

      implicit none
!
! Input scalars:
      integer :: Nact
!
! Input arrays:
      double precision :: rv(3), tp(3)
      double precision :: MO(Nbasis,Nbasis)
!
! Local scalars:
      integer   :: pMO, qMO
!
! Local arrays:
      double precision, allocatable, dimension(:) :: phi_rv, phi_tp
!
! Begin:
!
! rv - grid point
! tp - test point
!
      allocate(phi_rv(Nbasis),phi_tp(Nbasis))

      MO4prodJ = 0.0D0
      MO4prodK = 0.0D0
!
      call GET_MO_point(rv, Nact, MO, phi_rv)
      call GET_MO_point(tp, Nact, MO, phi_tp)
!
      do pMO = 1, Nact
        do qMO = 1, Nact
          MO4prodJ(pMO,qMO) = phi_rv(pMO)**2*phi_tp(qMO)**2
          MO4prodK(pMO,qMO) = phi_rv(pMO)*phi_tp(qMO)*phi_rv(qMO)*phi_tp(pMO)
        end do ! qMO
      end do ! pMO
!
      deallocate(phi_rv,phi_tp)
!
      end subroutine BLD_MO4prodJK
      subroutine BLD_MO4prodVGL_JL(rv, tp, Nact, MO)
!*******************************************************************
!   Date last modified: December 14, 2021                          *
!   Author: JWH                                                    *
!   Description: Calculate the value, gradient and Laplacian of    *
!                the 2-index 4-MO product for a set of active MOs. *
!                (kinetic energy style, sandwiched derivative)     *
!*******************************************************************
! Modules:
      USE AO_values
      USE module_grid_points
      USE QM_objects

      implicit none
!
! Input scalars:
      integer :: Nact
!
! Input arrays:
      double precision :: rv(3), tp(3)
      double precision :: MO(Nbasis,Nbasis)
!
! Local scalars:
      integer   :: pMO, qMO
!
! Local arrays:
      double precision, allocatable, dimension(:) :: phi_rv, phi_tp
      double precision, allocatable, dimension(:,:) :: dphi_rv, dphi_tp
      double precision, allocatable, dimension(:) :: d2phi_rv, d2phi_tp
!
! Begin:
!
! rv - grid point
! tp - test point
!
      allocate(phi_rv(Nbasis),phi_tp(Nbasis))
      allocate(dphi_rv(Nbasis,3),dphi_tp(Nbasis,3))
      allocate(d2phi_rv(Nbasis),d2phi_tp(Nbasis))
!
      MO4prodJ = 0.0D0
      MO4prodK = 0.0D0
!
      MO4ugradJ = 0.0D0
      MO4ugradL = 0.0D0
      MO4ulapJ = 0.0D0
      MO4ulapL = 0.0D0
!
      MO4RgradJ = 0.0D0
      MO4RgradL = 0.0D0
      MO4RlapJ = 0.0D0
      MO4RlapL = 0.0D0
!
      call GET_MO_point(rv, Nact, MO, phi_rv)
      call GET_MO_point(tp, Nact, MO, phi_tp)
!
      call GET_MOgrad_point(rv, Nact, MO, dphi_rv)
      call GET_MOgrad_point(tp, Nact, MO, dphi_tp)
!
      call GET_MOlap_point(rv, Nact, MO, d2phi_rv)
      call GET_MOlap_point(tp, Nact, MO, d2phi_tp)
!
      do pMO = 1, Nact
        do qMO = 1, Nact
          ! value
          MO4prodJ(pMO,qMO) = phi_rv(pMO)**2*phi_tp(qMO)**2
          MO4prodK(pMO,qMO) = phi_rv(pMO)*phi_tp(qMO)*phi_rv(qMO)*phi_tp(pMO)
!
! u derivatives
          ! gradient
          MO4ugradJ(pMO,qMO,1:3) = 0.5D0*phi_rv(pMO)*phi_tp(qMO)&
                                 *(dphi_rv(pMO,1:3)*phi_tp(qMO) - phi_rv(pMO)*dphi_tp(qMO,1:3))
          MO4ugradL(pMO,qMO,1:3) = 0.5D0*phi_rv(pMO)*phi_tp(pMO)&
                                 *(dphi_rv(qMO,1:3)*phi_tp(qMO) - phi_rv(qMO)*dphi_tp(qMO,1:3))
!
          ! Laplacian
          MO4ulapJ(pMO,qMO) = 0.25D0*phi_rv(pMO)*phi_tp(qMO)&
                             *(d2phi_rv(pMO)*phi_tp(qMO) + phi_rv(pMO)*d2phi_tp(qMO)&
                            - 2.0D0*dot_product(dphi_rv(pMO,1:3),dphi_tp(qMO,1:3))) 
          MO4ulapL(pMO,qMO) = 0.25D0*phi_rv(pMO)*phi_tp(pMO)&
                             *(d2phi_rv(qMO)*phi_tp(qMO) + phi_rv(qMO)*d2phi_tp(qMO)&
                            - 2.0D0*dot_product(dphi_rv(qMO,1:3),dphi_tp(qMO,1:3))) 
!
! R derivatives
          ! gradient
          MO4RgradJ(pMO,qMO,1:3) = phi_rv(pMO)*phi_tp(qMO)&
                                  *(dphi_rv(pMO,1:3)*phi_tp(qMO) + phi_rv(pMO)*dphi_tp(qMO,1:3))
          MO4RgradL(pMO,qMO,1:3) = phi_rv(pMO)*phi_tp(pMO)&
                                  *(dphi_rv(qMO,1:3)*phi_tp(qMO) + phi_rv(qMO)*dphi_tp(qMO,1:3))
!
          ! Laplacian
          MO4RlapJ(pMO,qMO) = phi_rv(pMO)*phi_tp(qMO)&
                             *(d2phi_rv(pMO)*phi_tp(qMO) + phi_rv(pMO)*d2phi_tp(qMO)&
                            + 2.0D0*dot_product(dphi_rv(pMO,1:3),dphi_tp(qMO,1:3))) 
          MO4RlapL(pMO,qMO) = phi_rv(pMO)*phi_tp(pMO)&
                             *(d2phi_rv(qMO)*phi_tp(qMO) + phi_rv(qMO)*d2phi_tp(qMO)&
                            + 2.0D0*dot_product(dphi_rv(qMO,1:3),dphi_tp(qMO,1:3))) 
!
! Careful K and L derivatives are not equivalent
! 
!
        end do ! qMO
      end do ! pMO
!
      deallocate(phi_rv,phi_tp,dphi_rv,dphi_tp,d2phi_rv,d2phi_tp)
!
      end subroutine BLD_MO4prodVGL_JL
      subroutine BLD_MO4prodVGLRF_JL(rv, tp, Nact, MO)
!*******************************************************************
!   Date last modified: December 20, 2021                          *
!   Author: JWH                                                    *
!   Description: Calculate the value, gradient and Laplacian of    *
!                the 2-index 4-MO product for a set of active MOs. *
!                (For the renormalization function)                *
!*******************************************************************
! Modules:
      USE AO_values
      USE module_grid_points
      USE QM_objects

      implicit none
!
! Input scalars:
      integer :: Nact
!
! Input arrays:
      double precision :: rv(3), tp(3)
      double precision :: MO(Nbasis,Nbasis)
!
! Local scalars:
      integer   :: pMO, qMO
!
! Local arrays:
      double precision :: v1(3), v2(3)
      double precision, allocatable, dimension(:) :: phi_rv, phi_tp
      double precision, allocatable, dimension(:,:) :: dphi_rv, dphi_tp
      double precision, allocatable, dimension(:) :: d2phi_rv, d2phi_tp
!
! Begin:
!
! rv - grid point
! tp - test point
!
      allocate(phi_rv(Nbasis),phi_tp(Nbasis))
      allocate(dphi_rv(Nbasis,3),dphi_tp(Nbasis,3))
      allocate(d2phi_rv(Nbasis),d2phi_tp(Nbasis))
!
      MO4prodJ = 0.0D0
      MO4prodK = 0.0D0
!
      MO4rfgradJ = 0.0D0
      MO4rfgradL = 0.0D0
      MO4rflapJ = 0.0D0
      MO4rflapL = 0.0D0
!
      call GET_MO_point(rv, Nact, MO, phi_rv)
      call GET_MO_point(tp, Nact, MO, phi_tp)
!
      call GET_MOgrad_point(rv, Nact, MO, dphi_rv)
      call GET_MOgrad_point(tp, Nact, MO, dphi_tp)
!
      call GET_MOlap_point(rv, Nact, MO, d2phi_rv)
      call GET_MOlap_point(tp, Nact, MO, d2phi_tp)
!
      do pMO = 1, Nact
        do qMO = 1, Nact
          ! value
          MO4prodJ(pMO,qMO) = phi_rv(pMO)**2*phi_tp(qMO)**2
          MO4prodK(pMO,qMO) = phi_rv(pMO)*phi_tp(qMO)*phi_rv(qMO)*phi_tp(pMO)
!
! R derivatives
          ! gradient
          MO4rfgradJ(pMO,qMO,1:3) = dphi_rv(pMO,1:3)*phi_tp(qMO)*phi_rv(qMO)*phi_tp(pMO)&
                                  + phi_rv(pMO)*dphi_tp(qMO,1:3)*phi_rv(qMO)*phi_tp(pMO)&
                                  + phi_rv(pMO)*phi_tp(qMO)*dphi_rv(qMO,1:3)*phi_tp(pMO)&
                                  + phi_rv(pMO)*phi_tp(qMO)*phi_rv(qMO)*dphi_tp(pMO,1:3)
!
          MO4rfgradL(pMO,qMO,1:3) = dphi_rv(pMO,1:3)*phi_tp(pMO)*phi_rv(qMO)*phi_tp(qMO)&
                                  + phi_rv(pMO)*dphi_tp(pMO,1:3)*phi_rv(qMO)*phi_tp(qMO)&
                                  + phi_rv(pMO)*phi_tp(pMO)*dphi_rv(qMO,1:3)*phi_tp(qMO)&
                                  + phi_rv(pMO)*phi_tp(pMO)*phi_rv(qMO)*dphi_tp(qMO,1:3)
!
          ! Laplacian
          MO4rflapJ(pMO,qMO) = 8.0D0*phi_rv(pMO)*phi_tp(qMO)&
                              *dot_product(dphi_rv(pMO,1:3),dphi_tp(qMO,1:3))&
                             + 2.0D0*phi_rv(pMO)**2*(dot_product(dphi_tp(qMO,1:3),dphi_tp(qMO,1:3))&
                                                     + phi_tp(qMO)*d2phi_tp(qMO))&
                             + 2.0D0*phi_tp(qMO)**2*(dot_product(dphi_rv(pMO,1:3),dphi_rv(pMO,1:3))&
                                                     + phi_rv(pMO)*d2phi_rv(pMO))
!
         v1(1:3) = phi_rv(pMO)*dphi_tp(pMO,1:3) + dphi_rv(pMO,1:3)*phi_tp(pMO)
         v2(1:3) = phi_rv(qMO)*dphi_tp(qMO,1:3) + dphi_rv(qMO,1:3)*phi_tp(qMO)
         MO4rflapL(pMO,qMO) = 2.0D0*dot_product(v1,v2)&
                 + phi_rv(pMO)*phi_tp(pMO)*(2.0D0*dot_product(dphi_rv(qMO,1:3),dphi_tp(qMO,1:3))&
                                           + phi_rv(qMO)*d2phi_tp(qMO) + d2phi_rv(qMO)*phi_tp(qMO))&
                 + phi_rv(qMO)*phi_tp(qMO)*(2.0D0*dot_product(dphi_rv(pMO,1:3),dphi_tp(pMO,1:3))&
                                           + phi_rv(pMO)*d2phi_tp(pMO) + d2phi_rv(pMO)*phi_tp(pMO))
!
        end do ! qMO
      end do ! pMO
!
      deallocate(phi_rv,phi_tp,dphi_rv,dphi_tp,d2phi_rv,d2phi_tp)
!
      end subroutine BLD_MO4prodVGLRF_JL
      subroutine GET_den_VGL_point(rv, Nact, MO, rho, drho, d2rho)
!*******************************************************************
!   Date last modified: December 23, 2021                          *
!   Author: JWH                                                    *
!   Description: Calculate the value, gradient and Laplacian of    *
!                the one-electron density.                         *
!*******************************************************************
! Modules:
      USE AO_values
      USE module_grid_points
      USE QM_objects

      implicit none
!
! Input scalars:
      integer, intent(IN) :: Nact
!
! Input arrays:
      double precision, intent(IN) :: rv(3)
      double precision, intent(IN) :: MO(Nbasis,Nbasis)
!
! Output scalars:
      double precision, intent(OUT) :: rho, d2rho
!
! Output arrays:
      double precision, intent(OUT) :: drho(3)
!
! Local scalars:
      integer   :: pMO
!
! Local arrays:
      double precision, allocatable, dimension(:) :: phi_rv
      double precision, allocatable, dimension(:,:) :: dphi_rv
      double precision, allocatable, dimension(:) :: d2phi_rv
!
! Begin:
!
! rv - grid point
!
      allocate(phi_rv(Nbasis))
      allocate(dphi_rv(Nbasis,3))
      allocate(d2phi_rv(Nbasis))
!
      rho   = 0.0D0
      drho  = 0.0D0
      d2rho = 0.0D0
!
      call GET_MO_point(rv, Nact, MO, phi_rv)
      call GET_MOgrad_point(rv, Nact, MO, dphi_rv)
      call GET_MOlap_point(rv, Nact, MO, d2phi_rv)
!
      do pMO = 1, Nact
        ! value
        rho = rho + on(pMO)*phi_rv(pMO)**2
!
        ! gradient
        drho(1:3) = drho(1:3) + 2.0D0*on(pMO)*phi_rv(pMO)*dphi_rv(pMO,1:3)
!
        ! Laplacian
        d2rho = d2rho + 2.0D0*on(pMO)*(phi_rv(pMO)*d2phi_rv(pMO)&
                                       + dot_product(dphi_rv(pMO,1:3),dphi_rv(pMO,1:3)))
!
      end do ! pMO
!
! assuming closed-shell (for now)
      rho = 2.0D0*rho
      drho = 2.0D0*drho
      d2rho = 2.0D0*d2rho
!
      deallocate(phi_rv,dphi_rv,d2phi_rv)
!
      end subroutine GET_den_VGL_point
      subroutine BLD_MOd2prod(rv, Nact, MO)
!*******************************************************************
!   Date last modified: February 15, 2022                          *
!   Author: JWH                                                    *
!   Description: Calculate the Laplacian of the 1-index            *
!                2-MO product for a set of active MOs.             *
!*******************************************************************
! Modules:
      USE AO_values
      USE module_grid_points
      USE QM_objects

      implicit none
!
! Input scalars:
      integer, intent(IN) :: Nact
!
! Input arrays:
      double precision, intent(IN) :: rv(3)
      double precision, intent(IN) :: MO(Nbasis,Nbasis)
!
! Local scalars:
      integer   :: pMO
!
! Local arrays:
      double precision, allocatable, dimension(:) :: phi_rv
      double precision, allocatable, dimension(:) :: d2phi_rv
!
! Begin:
!
! rv - grid point
!
      allocate(phi_rv(Nbasis))
      allocate(d2phi_rv(Nbasis))
!
      call GET_MO_point(rv, Nact, MO, phi_rv)
      call GET_MOlap_point(rv, Nact, MO, d2phi_rv)
!
      do pMO = 1, Nact
!
        MOd2prod(pMO) = phi_rv(pMO)*d2phi_rv(pMO)
!
      end do ! pMO
!
      deallocate(phi_rv,d2phi_rv)
!
      end subroutine BLD_MOd2prod
      subroutine BLD_ERpot4AO(rv)
!*******************************************************************
!   Date last modified: September 9, 2022                          *
!   Author: JWH                                                    *
!   Description: Calculate electron-repulsion potential over AOs   *
!*******************************************************************
! Modules:
      USE AO_values
      USE module_grid_points
      USE QM_objects
      USE INT_objects

      implicit none
!
! Input arrays:
      double precision :: rv(3)
!
! Local scalars:
      integer   :: ijkl_index, iAO, jAO, kAO, lAO
!
! Local arrays:
      double precision, allocatable, dimension(:,:) :: ERpot2AO
!
! Begin:
!
! rv - grid point
!
      allocate(AOval(Nbasis),ERpot2AO(Nbasis,Nbasis))
      if(allocated(ERpotAO))then
        deallocate(ERpotAO)
      end if
      allocate(ERpotAO(Nbasis**4))
      if(allocated(ERpot))then
        deallocate(ERpot)
      end if
      allocate(ERpot(MATlen))
!
      ERpotAO = 0.0D0
!
      call AO_val(rv(1), rv(2), rv(3))
      call I1E_ERpot_val(rv(1), rv(2), rv(3))
      call UPT_to_SQS (ERpot, MATlen, ERpot2AO, Nbasis)
!
      ijkl_index = 0
      do iAO = 1,Nbasis
        do jAO = 1,Nbasis
          do kAO = 1,Nbasis
            do lAO = 1,Nbasis
              ijkl_index = ijkl_index + 1
              ! Switching from chemist to physicist notation [11|22] -> <12|12>
              ERpotAO(ijkl_index) = AOval(iAO)*AOval(jAO)*ERpot2AO(kAO,lAO)
            end do ! lAO
          end do ! kAO
        end do ! jAO
      end do ! iAO
!
      deallocate(AOval,ERpot2AO)
!
      end subroutine BLD_ERpot4AO
      subroutine BLD_ERpotMO(rv, Nact, MO, ERpotMO)
!*******************************************************************
!   Date last modified: August 29, 2023                            *
!   Author: JWH                                                    *
!   Description: Calculate electron-repulsion potential over MOs   *
!*******************************************************************
! Modules:
      USE module_grid_points
      USE QM_objects
      USE INT_objects

      implicit none
!
! Input arrays:
      double precision, intent(IN) :: rv(3)
      double precision, intent(IN) :: MO(Nbasis,Nbasis)
!
! Input scalars: 
      integer, intent(IN) :: Nact
!
! Output arrays:
      double precision, intent(OUT) :: ERpotMO(Nact,Nact)
!
! Local scalars:
      integer   :: iAO, jAO, aMO, bMO
!
! Local arrays: 
      double precision, allocatable, dimension(:,:) :: temp, ERpot2AO
!
! Begin:
!
! rv - grid point
!
      allocate(ERpot2AO(Nbasis,Nbasis))
      if(allocated(ERpot))then
        deallocate(ERpot)
      end if
      allocate(ERpot(MATlen))
!
      call I1E_ERpot_val(rv(1), rv(2), rv(3))
      call UPT_to_SQS (ERpot, MATlen, ERpot2AO, Nbasis)
!
      allocate(temp(Nact,Nbasis))
      temp = 0.0D0
!
      do jAO = 1, Nbasis
        do aMO = 1, Nact
          do iAO = 1, Nbasis
            temp(aMO,jAO) = temp(aMO,jAO) + MO(iAO,aMO)*ERpot2AO(iAO,jAO)
          end do ! iAO
        end do ! aMO
      end do ! jAO
!
     ERpotMO = 0.0D0 
!
      do aMO = 1, Nact
        do bMO = 1, Nact
          do jAO = 1, Nbasis
            ERpotMO(aMO,bMO) = ERpotMO(aMO,bMO) + MO(jAO,bMO)*temp(aMO,jAO)
          end do ! jAO
        end do ! bMO
      end do ! aMO
!
      deallocate(temp,ERpot2AO)
!
      end subroutine BLD_ERpotMO
