      subroutine Gaussian_products (rx, ry, rz, AOprod, MATlen)
!****************************************************************************************
!     Date last modified: May 6, 2017 by Ibrahim Awad                                              *
!     Authors: Ahmad and R.A. Poirier                                                   *
!     Description: Compute the product of all the basis functions and return the values *
!     in the array AO_prod.  No need to used the Gaussian product theorem, just find    *
!     g_s(rx,ry,rz)*g_t(rx,ry,rz)                                                       *
!****************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE type_basis_set
      USE matrix_print
      USE QM_defaults

      implicit none
!
! Input scalars:
      integer :: MATlen
      double precision :: AOprod(MATlen)
!
! Local scalars:
      integer :: Ishell,Jshell,Ifrst,Jfrst,Ilast,Jlast
      integer :: Istart,Jstart,Iend,Jend
      integer :: Iatmshl,Jatmshl
      integer :: LAMAX,LBMAX
      integer :: Iatom,Jatom,LS,LL,LNUM
      integer :: Irange,Jrange,MINrange,MAXrange, MINtype, MAXtype
      integer :: Igauss,Jgauss
      integer :: IGBGN,JGBGN,IGend,JGend
      integer :: Iao,Jao,AOI,AOJ
      integer :: I,J,K,INDEX,DEFCASE,IJao
      double precision :: COEF_IJ,COEF_SXY,COEF_PXY
      double precision :: COEF_XXXY,COEF_XYXY,SQRT3,SQRT5,SQRT7,SUMDSQ,Gama,Alpha,Beta,RABSQ,EG,EG_K,EG_exp
      double precision :: Ax,Bx,Ay,By,Az,Bz,rx,ry,rz,Px,Py,Pz,xA,yA,zA,xB,yB,zB,rpx,rpy,rpz
      double precision :: PAx,PAy,PAz,PBx,PBy,PBz
      logical :: LIatmshl

!
! Local arrays
      double precision :: SHLINTS(441)
      double precision :: GFA(21), GFB(21)

! Parameters:
      double precision FIVE
      parameter (FIVE=5.0D0)
      double precision SEVEN
      parameter (SEVEN=7.0D0)
!
! Begin:
      SQRT3=DSQRT(THREE) 
      SQRT5=DSQRT(FIVE)
      SQRT7=DSQRT(SEVEN)
      AOprod(1:size(AOprod))=ZERO
!
! Begin loop over shells.
!
! Loop over elemental SHELLs
! Loop over Ishell.
      do Ishell=1,Basis%Nshells
        LAMAX=Basis%shell(Ishell)%Xtype+1
        Istart=Basis%shell(Ishell)%Xstart
        Iend=Basis%shell(Ishell)%XEND
        Irange=Iend-Istart+1
        IGBGN=Basis%shell(Ishell)%EXPBGN
        IGEND=Basis%shell(Ishell)%EXPEND
        Ifrst=Basis%shell(Ishell)%frstSHL
!
! Loop over Jshell.
      do Jshell=1,Ishell
        LBMAX=Basis%shell(Jshell)%Xtype+1
        Jstart=Basis%shell(Jshell)%Xstart
        Jend=Basis%shell(Jshell)%XEND
        Jrange=Jend-Jstart+1
        JGBGN=Basis%shell(Jshell)%EXPBGN
        JGEND=Basis%shell(Jshell)%EXPEND
        Jfrst=Basis%shell(Jshell)%frstSHL
!
! Define CASE:
      if(LAMAX.lt.LBMAX)then
       MINrange=Irange
       MAXrange=Jrange
       MINtype=LAMAX
       MAXtype=LBMAX
      else
       MINrange=Jrange
       MAXrange=Irange
       MINtype=LBMAX
       MAXtype=LAMAX
      end if
!
      Ilast= Basis%shell(Ishell)%lastSHL
      Jlast= Basis%shell(Jshell)%lastSHL
!
      call AB_products
!
! End of loop over shells.
      end do ! Jshell
      end do ! Ishell
!
! End of routine AO_PRODUCTS
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine AB_products
!****************************************************************************************
!     Date last modified: May 6, 2017 by Ibrahim Awad                                              *
!     Authors: Ahmad and R.A. Poirier                                                   *
!     Description: Loop over gaussians and compute the product                          *
!****************************************************************************************
! Modules:

      implicit none
!
! Loop over all atomic shells
      do Iatmshl=Ifrst,Ilast
        Iatom=Basis%atmshl(Iatmshl)%ATMLST
!
        Ax=CARTESIAN(Iatom)%X     ! vector A={Ax,Ay,Az}     
        Ay=CARTESIAN(Iatom)%Y
        Az=CARTESIAN(Iatom)%Z
!
        AOI=Basis%atmshl(Iatmshl)%frstAO-1
        IF(Ishell.EQ.Jshell)Jlast=Iatmshl
      do Jatmshl=Jfrst,Jlast
        Jatom=Basis%atmshl(Jatmshl)%ATMLST
!
        Bx=CARTESIAN(Jatom)%X     ! vector B={Bx,By,Bz}
        By=CARTESIAN(Jatom)%Y
        Bz=CARTESIAN(Jatom)%Z
!
        AOJ=Basis%atmshl(Jatmshl)%frstAO-1
        LIatmshl=.false.
        if(Iatmshl.eq.Jatmshl)LIatmshl=.true.

        RABSQ=(Ax-Bx)*(Ax-Bx)+(Ay-By)*(Ay-By)+(Az-Bz)*(Az-Bz)     ! (A-B)**2

        SHLINTS(1:441)=ZERO ! will handle up to <h|h> 21*21=441
! Loop over primitive gaussians
!
      do Igauss=IGBGN,IGEND
        Alpha=Basis%gaussian(Igauss)%exp

      do Jgauss=JGBGN,JGEND
        Beta=Basis%gaussian(Jgauss)%exp
!
        Gama=Alpha+Beta

        Px=(Alpha*Ax+Beta*Bx)/Gama     ! vector P
        Py=(Alpha*Ay+Beta*By)/Gama
        Pz=(Alpha*Az+Beta*Bz)/Gama
!
! rpx=Xp in equations
        rpx = rx-Px
        rpy = ry-Py
        rpz = rz-Pz

        SUMDSQ = rpx*rpx + rpy*rpy + rpz*rpz   ! (rp)**2

        EG_K=DEXP(-Alpha*Beta*RABSQ/Gama)      ! K 
        EG_exp=DEXP(-Gama*SUMDSQ)              ! exp
        EG=EG_K*EG_exp                         ! K*exp

        xA=rx-Ax     ! vector rA=(r-A)
        yA=ry-Ay
        zA=rz-Az
        xB=rx-Bx     ! vector rB=(r-B)
        yB=ry-By
        zB=rz-Bz
!
        COEF_IJ = Basis%gaussian(Igauss)%CONTRC*Basis%gaussian(Jgauss)%CONTRC*EG

        call Gaussian_function (xA, yA, zA, GFA, MINtype)
        call Gaussian_function (xB, yB, zB, GFB, MAXtype)
        LNUM=0
        do LS=1,MINrange
          do LL=1,MAXrange
            LNUM=LNUM+1
            SHLINTS(LNUM)=SHLINTS(LNUM)+GFA(LS)*GFB(LL)*COEF_IJ
          end do
        end do
!
      end do ! Jgauss
      end do ! Igauss 
      
      IF(LIatmshl)then
        INDEX=0
        do I=1,Irange
        do J=1,Jrange
          INDEX=INDEX+1
          IF(I.GE.J)then  ! Corrected August 1,2003 had (J.ge.I)
            Iao=AOI+I
            Jao=AOJ+J
            IJao=Iao*(Iao-1)/2+Jao
            AOprod(IJao)=SHLINTS(INDEX)
          end if
        end do ! J   
        end do ! I
      else
        INDEX=0
        do I=1,Irange
        do J=1,Jrange
          INDEX=INDEX+1
          Iao=AOI+I
          Jao=AOJ+J
          IJao=Iao*(Iao-1)/2+Jao
          AOprod(IJao)=SHLINTS(INDEX)
        end do ! J
        end do ! I
      end if ! LIatmshl

      end do ! Jatmshl
      end do ! Iatmshl

      return
      end subroutine AB_products
! END CONTAINS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine Gaussian_products
      subroutine Gaussian_function (x, y, z, GF, GF_type)
!****************************************************************************************
!     Date last modified: May 6, 2017                                        *
!     Authors: Ibrahim Awad                                                   *
!     Description:                                                        *
!****************************************************************************************
! Modules:

      USE program_constants

      implicit none
     double precision, intent(in) :: x, y, z
     double precision, intent(out) :: GF(21)
     integer, intent(in) :: GF_type

      double precision :: SQRT3, SQRT5, SQRT7
! Parameters:
      double precision FIVE
      parameter (FIVE=5.0D0)
      double precision SEVEN
      parameter (SEVEN=7.0D0)

!
! Begin:
      SQRT3=DSQRT(THREE) 
      SQRT5=DSQRT(FIVE)
      SQRT7=DSQRT(SEVEN)

     GF=ZERO

     select case (GF_type)
     case (1) ! S
     GF(1)=ONE

     case (2) ! P
     GF(1)=x
     GF(2)=y
     GF(3)=z

     case (3) ! D   
     GF(1)=x*x
     GF(2)=y*y
     GF(3)=z*z
     GF(4)=x*y
     GF(5)=x*z
     GF(6)=y*z

     GF(4:6)=GF(4:6)*SQRT3

     case (4) ! F
     GF(1)=x*x*x
     GF(2)=y*y*y
     GF(3)=z*z*z
     GF(4)=x*y*y
     GF(5)=x*x*y
     GF(6)=x*x*z
     GF(7)=x*z*z
     GF(8)=y*z*z
     GF(9)=y*y*z
     GF(10)=x*y*z

      GF(4:9)=Gf(4:9)*SQRT5
      Gf(10)=GF(10)*SQRT3*SQRT5

     case (5) ! G      
     GF(1)=x*x*x*x
     GF(2)=y*y*y*y
     GF(3)=z*z*z*z
     GF(4)=x*x*x*y
     GF(5)=x*x*x*z
     GF(6)=x*y*y*y
     GF(7)=x*z*z*z
     GF(8)=y*z*z*z
     GF(9)=y*y*y*z
     GF(10)=x*x*y*y
     GF(11)=x*x*z*z
     GF(12)=y*y*z*z
     GF(13)=x*x*y*z
     GF(14)=x*y*y*z
     GF(15)=x*y*z*z

      GF(4:9)=GF(4:9)*SQRT7
      GF(10:12)=GF(10:12)*SQRT5*SQRT7/SQRT3
      GF(13:15)=GF(13:15)*SQRT5*SQRT7

     case (6) ! H
     GF(1)=x*x*x*x*x
     GF(2)=y*y*y*y*y
     GF(3)=z*z*z*z*z
     GF(4)=x*x*x*x*y
     GF(5)=x*x*x*x*z
     GF(6)=x*y*y*y*y
     GF(7)=x*z*z*z*z
     GF(8)=y*y*y*y*z
     GF(9)=y*z*z*z*z
     GF(10)=x*x*x*y*y
     GF(11)=x*x*x*z*z
     GF(12)=x*x*y*y*y
     GF(13)=x*x*z*z*z
     GF(14)=y*y*y*z*z
     GF(15)=y*y*z*z*z
     GF(16)=x*x*x*y*z
     GF(17)=x*y*y*y*z
     GF(18)=x*y*z*z*z
     GF(19)=x*x*y*y*z
     GF(20)=x*x*y*z*z
     GF(21)=x*y*y*z*z

      GF(4:9)=GF(4:9)*Three
      GF(10:15)=GF(10:15)*sqrt3*sqrt7
      GF(16:18)=GF(16:18)*three*sqrt7
      GF(19:21)=GF(19:21)*sqrt3*sqrt5*sqrt7

     case default
         write(*,*)'ERROR> AO: type should not exist'
         stop ' ERROR> AO: type should not exist'
     end select

     end subroutine Gaussian_function
      subroutine DelGaussian_function (x, y, z, dGF, GF_type)
!****************************************************************************************
!     Date last modified: May 6, 2017                                        *
!     Authors: Ibrahim Awad                                                   *
!     Description:                                                        *
!****************************************************************************************
! Modules:

      USE program_constants

      implicit none
     double precision, intent(in) :: x, y, z
     double precision, intent(out) :: dGF(21,3)! dx,dy,dz
     integer, intent(in) :: GF_type

      double precision :: SQRT3, SQRT5, SQRT7
! Parameters:
      double precision FIVE
      parameter (FIVE=5.0D0)
      double precision SEVEN
      parameter (SEVEN=7.0D0)
!
! Begin:
      SQRT3=DSQRT(THREE) 
      SQRT5=DSQRT(FIVE)
      SQRT7=DSQRT(SEVEN)

     dGF=ZERO

     select case (GF_type)
     case (1) ! S
      dGF(1,1)=ZERO
      dGF(1,2)=ZERO
      dGF(1,3)=ZERO

     case (2) ! P
!     px
      dGF(1,1)=ONE       !dx     
      dGF(1,2)=ZERO      !dy
      dGF(1,3)=ZERO      !dz
!     py
      dGF(2,1)=ZERO      !dx
      dGF(2,2)=ONE       !dy
      dGF(2,3)=ZERO      !dz
!     pz
      dGF(3,1)=ZERO      !dx
      dGF(3,2)=ZERO      !dy
      dGF(3,3)=ONE       !dz

     case (3) ! D   
!     dxx
      dGF(1,1)=TWO*x    !dx
      dGF(1,2)=ZERO      !dy
      dGF(1,3)=ZERO      !dz
!     dyy
      dGF(2,1)=ZERO      !dx
      dGF(2,2)=TWO*y    !dy
      dGF(2,3)=ZERO      !dz
!     dzz
      dGF(3,1)=ZERO      !dx
      dGF(3,2)=ZERO      !dy
      dGF(3,3)=TWO*z    !dz
!     dxy
      dGF(4,1)=y        !dx
      dGF(4,2)=x        !dy
      dGF(4,3)=ZERO      !dz
!     dxz
      dGF(5,1)=z        !dx
      dGF(5,2)=ZERO      !dy
      dGF(5,3)=x        !dz
!     dyz
      dGF(6,1)=ZERO      !dx
      dGF(6,2)=z        !dy
      dGF(6,3)=y        !dz

     dGF(4:6,1:3)=dGF(4:6,1:3)*SQRT3

     case (4) ! F
!     Fxxx
      dGF(1,1)=THREE*x*x          !dx
      dGF(1,2)=ZERO                 !dy
      dGF(1,3)=ZERO                 !dz
!     Fyyy
      dGF(2,1)=ZERO                 !dx
      dGF(2,2)=THREE*y*y          !dy
      dGF(2,3)=ZERO                 !dz
!     Fzzz
      dGF(3,1)=ZERO                 !dx
      dGF(3,2)=ZERO                 !dy
      dGF(3,3)=THREE*z*z          !dz
!     Fxyy
      dGF(4,1)=y*y                !dx  
      dGF(4,2)=x*(TWO*y)          !dy
      dGF(4,3)=ZERO                 !dz  
!     Fxxy
      dGF(5,1)=(TWO*x)*y          !dx
      dGF(5,2)=x*x                !dy
      dGF(5,3)=ZERO                 !dz
!     Fxxz
      dGF(6,1)=(TWO*x)*z          !dx
      dGF(6,2)=ZERO                 !dy
      dGF(6,3)=x*x                !dz
!     Fxzz
      dGF(7,1)=z*z                !dx
      dGF(7,2)=ZERO                 !dy
      dGF(7,3)=x*(TWO*z)          !dz
!     Fyzz
      dGF(8,1)=ZERO                 !dx  
      dGF(8,2)=z*z                !dy
      dGF(8,3)=y*(TWO*z)          !dz  
!     Fyyz
      dGF(9,1)=ZERO                 !dx
      dGF(9,2)=(TWO*y)*z          !dy
      dGF(9,3)=y*y                !dz
!     Fxyz
      dGF(10,1)=y*z               !dx
      dGF(10,2)=x*z               !dy
      dGF(10,3)=x*y               !dz

      dGF(4:9,1:3)=dGf(4:9,1:3)*SQRT5
      dGf(10,1:3)=dGF(10,1:3)*SQRT3*SQRT5


     case default
         write(*,*)'ERROR> del AO: type should not exist'
         stop ' ERROR> del AO: type should not exist'
     end select

     end subroutine DelGaussian_function
      subroutine Del2Gaussian_function (x, y, z, d2GF, GF_type)
!****************************************************************************************
!     Date last modified: May 6, 2017                                        *
!     Authors: Ibrahim Awad                                                   *
!     Description:                                                        *
!****************************************************************************************
! Modules:

      USE program_constants

      implicit none

      double precision, intent(in) :: x, y, z
      double precision, intent(out) :: d2GF(21,3,3)! dxx,dyy,dzz,dxy,dxz,dyx
      integer, intent(in) :: GF_type

      double precision :: SQRT3, SQRT5, SQRT7
! Parameters:
      double precision FIVE
      parameter (FIVE=5.0D0)
      double precision SIX
      parameter (SIX=6.0D0)
      double precision SEVEN
      parameter (SEVEN=7.0D0)
!
! Begin:
      SQRT3=DSQRT(THREE) 
      SQRT5=DSQRT(FIVE)
      SQRT7=DSQRT(SEVEN)

      d2GF=ZERO

      select case (GF_type)
      case (1) ! S
      d2GF(1,1:3,1:3)=ZERO

      case (2) ! P
      d2GF(1:3,1:3,1:3)=ZERO !px,py,pz

      case (3) ! D   
!     dxxA 
      d2GF(1,1,1)=TWO         !dxx
!     dyyA
      d2GF(2,2,2)=TWO         !dyy
!     dzzA
      d2GF(3,3,3)=TWO         !dzz
!     dxyA
      d2GF(4,1,2)=ONE         !dxy
      d2GF(4,2,1)=ONE         !dyx
!     dxzA
      d2GF(5,1,3)=ONE         !dxz
      d2GF(5,3,1)=ONE         !dzx
!     dyzA
      d2GF(6,2,3)=ONE         !dyz
      d2GF(6,3,2)=ONE         !dzy

      d2GF(4:6,1:3,1:3)=d2GF(4:6,1:3,1:3)*SQRT3

      case (4) ! F
!     FxxxA
      d2GF(1,1,1)=SIX*x             !dxx
!     FyyyA
      d2GF(2,2,2)=SIX*y             !dyy
!     FzzzA
      d2GF(3,3,3)=SIX*z             !dzz
!     FxyyA
      d2GF(4,1,2)=TWO*y             !dxy  
      d2GF(4,2,1)=d2GF(4,1,2)         !dyx
      d2GF(4,2,2)=TWO*x             !dyy  
!     FxxyA
      d2GF(5,1,1)=TWO*y             !dxx  
      d2GF(5,1,2)=TWO*x             !dxy
      d2GF(5,2,1)=d2GF(5,1,2)       !dyx 
!     FxxzA
      d2GF(6,1,1)=TWO*z             !dxy  
      d2GF(6,1,3)=TWO*x             !dyx
      d2GF(6,3,1)=d2GF(6,1,3)         !dyy
!     FxzzA
      d2GF(7,1,3)=TWO*z             !dxy  
      d2GF(7,3,1)=d2GF(7,1,3)         !dyx
      d2GF(7,3,3)=TWO*x             !dyy 
!     FyzzA
      d2GF(8,2,3)=TWO*z             !dxy  
      d2GF(8,3,2)=d2GF(8,2,3)         !dyx
      d2GF(8,3,3)=TWO*y             !dyy  
!     FyyzA
      d2GF(9,2,2)=TWO*z             !dxy  
      d2GF(9,2,3)=TWO*y             !dyx
      d2GF(9,3,2)=d2GF(9,2,3)         !dyy
!     FxyzA
      d2GF(10,1,2)=z                  !dxx
      d2GF(10,1,3)=y                  !dyy
      d2GF(10,2,1)=d2GF(10,1,2)         !dzz
      d2GF(10,2,3)=x                  !dxy
      d2GF(10,3,1)=d2GF(10,1,3)         !dxz
      d2GF(10,3,2)=d2GF(10,2,3)         !dyz

      d2GF(4:9,1:3,1:3)=d2Gf(4:9,1:3,1:3)*SQRT5
      d2Gf(10,1:3,1:3)=d2GF(10,1:3,1:3)*SQRT3*SQRT5

      case default
         write(*,*)'ERROR> del2 AO: type should not exist'
         stop ' ERROR> del2 AO: type should not exist'
      end select

      end subroutine Del2Gaussian_function
      subroutine DelExpGaussian_function (x, y, z, alpha, dEG)
!****************************************************************************************
!     Date last modified: May 6, 2017                                        *
!     Authors: Ibrahim Awad                                                   *
!     Description:                                                        *
!****************************************************************************************
! Modules:

      USE program_constants

      implicit none

      double precision, intent(in) :: x, y, z, alpha
      double precision, intent(out) :: dEG(3)! dx,dy,dz

!
! Begin:

      dEG(1)=-TWO*alpha*x     ! derivative of EG with respect to x
      dEG(2)=-TWO*alpha*y     ! derivative of EG with respect to y
      dEG(3)=-TWO*alpha*z     ! derivative of EG with respect to z 

      return
      end subroutine DelExpGaussian_function
      subroutine Del2ExpGaussian_function (x, y, z, alpha, d2EG)
!****************************************************************************************
!     Date last modified: May 6, 2017                                        *
!     Authors: Ibrahim Awad                                                   *
!     Description:                                                        *
!****************************************************************************************
! Modules:

      USE program_constants

      implicit none

      double precision, intent(in) :: x, y, z, alpha
      double precision, intent(out) :: d2EG(3,3)! dxx,dy,dxz, ....etc

!   the Second derivtive of E
      d2EG(1,1)=-TWO*Alpha*(ONE-TWO*Alpha*x*x)      ! derivtive of EG with respect to xx
      d2EG(1,2)=TWO*Alpha*x*TWO*Alpha*y             ! derivtive of EG with respect to xy
      d2EG(1,3)=TWO*Alpha*x*TWO*Alpha*z             ! derivtive of EG with respect to xz

      d2EG(2,1)=d2EG(1,2)                           ! derivtive of EG with respect to yx
      d2EG(2,2)=-TWO*Alpha*(ONE-TWO*Alpha*y*y)      ! derivtive of EG with respect to yy
      d2EG(2,3)=TWO*Alpha*y*TWO*Alpha*z             ! derivtive of EG with respect to yz

      d2EG(3,1)=d2EG(1,3)                           ! derivtive of EG with respect to zx
      d2EG(3,2)=d2EG(2,3)                           ! derivtive of EG with respect to zy
      d2EG(3,3)=-TWO*Alpha*(ONE-TWO*Alpha*z*z)      ! derivtive of EG with respect to zz

      return
      end subroutine Del2ExpGaussian_function
