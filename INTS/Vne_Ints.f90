      subroutine Vne_Ints
!***********************************************************************
!     Date last modified: November 6, 1996                 Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!***********************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE type_basis_set
      USE QM_defaults
      USE QM_objects
      USE INT_objects
      USE matrix_print

      implicit none
!
! Local scalars:
      integer :: Ishell,Jshell,Ifrst,Jfrst,Ilast,Jlast
      integer :: Istart,Jstart,Iend,Jend
      integer :: Iatmshl,Jatmshl
      integer :: LAMAX,LBMAX
      integer :: Iatom
      integer :: Irange,Jrange
      integer :: Igauss,Jgauss
      integer :: Iaos,JaoS
      integer :: IGBGN,JGBGN,IGend,JGend
      integer :: LPMAX,I,IA,IJX,IJY,IJZ,INTC,IX,IXP,IY,IYP,IZ,IZERO,IZP,J,JX,JY,JZ,NZERO
      integer :: LENTQ
      double precision :: ABX,ABY,ABZ,ARABSQ,ARG,AS,ASXA,ASYA,ASZA,BS,COEF,CUT1,EP,EPI,EPIO2,PCX,PCY,PCZ,PEXP, &
                       PX,PY,PZ,RPCSQ,STERM,SYZ,TK,TWOASQ,TWOP,TWOPI,TWOPT2,XAP,XBP, &
                       YAP,YBP,YIIM1,ZAP,ZBP,ZCONST,ZT,ZTEMP
      double precision :: XA,XB,XC,YA,YB,YC,ZA,ZB,ZC,RABSQ
      double precision :: EVtotal
      double precision :: TRACLO
!
! Local arrays:
      integer INDIX(20),INDIY(20),INDIZ(20),INDJX(20),INDJY(20),INDJZ(20)
      double precision :: TP(7),WP(7)
      double precision :: A(45),CA(20),CB(20),CCX(192),CCY(192),CCZ(192)
      double precision :: SX(32),SY(32),SZ(32),S1C(6),TWOCX(9),TWOCY(9),TWOCZ(9),XIP(80),YIP(80),ZIP(80)
      double precision :: EEPB(100),EEPV(100)
      double precision, allocatable :: Vne_Atoms(:,:),Vint_temp(:)
      double precision, allocatable :: EEPBA(:,:)
      double precision, allocatable :: EEPVA(:,:)
      double precision, dimension(:), allocatable :: EV_Atom
!
      parameter (TWOPI=TWO*PI_VAL,CUT1=-75.0D0)
!
      DATA INDJX/1,2,1,1,3,1,1,2,2,1,4,1,1,2,3,3,2,1,1,2/,INDJY/1,1,2,1,1,3,1,2,1,2,1,4,1,3,2,1,1,2,3,2/, &
           INDJZ/1,1,1,2,1,1,3,1,2,2,1,1,4,1,1,2,3,3,2,2/
!
! Begin:
      call PRG_manager ('enter', 'VNE_INTS', '1EINT%VNE')
!
      call GET_object ('QM', 'CMO', Wavefunction)
! Object:
      if(.not.allocated(Vint))then
        allocate (Vint(MATlen))
      else
        if(Basis%Nbasis.ne.size(Vint,1))then
          deallocate (Vint)
          allocate (Vint(MATlen))
        end if
      end if

      allocate (Vint_temp(MATlen))
      allocate (Vne_Atoms(MATlen,Natoms))
      allocate (EEPVA(100,Natoms))
      allocate (EEPBA(100,Natoms))
      allocate (EV_Atom(Natoms))
      Vint(1:MATlen)=ZERO
      Vne_Atoms(1:MATlen,1:Natoms)=ZERO
      EEPBA(1:100,1:Natoms)=ZERO
      EV_Atom(1:Natoms)=ZERO
!
      call RYSSET
!
! Fill INDIX.
      do I=1,20
        INDIX(I)=4*(INDJX(I)-1)
        INDIY(I)=4*(INDJY(I)-1)
        INDIZ(I)=4*(INDJZ(I)-1)
      end do ! I
!
! Loop over shells.
! Loop over Ishell.
      do Ishell=1,Basis%Nshells
      LAMAX=Basis%shell(Ishell)%Xtype+1
      Istart=Basis%shell(Ishell)%Xstart
      Iend=Basis%shell(Ishell)%XEND
      Irange=Iend-Istart+1
      IGBGN=Basis%shell(Ishell)%EXPBGN
      IGEND=Basis%shell(Ishell)%EXPEND
      Ifrst=Basis%shell(Ishell)%frstSHL
!
! Loop over Jshell.
      do Jshell=1,Ishell
      LBMAX=Basis%shell(Jshell)%Xtype+1
      Jstart=Basis%shell(Jshell)%Xstart
      Jend=Basis%shell(Jshell)%XEND
      Jrange=Jend-Jstart+1
      JGBGN=Basis%shell(Jshell)%EXPBGN
      JGEND=Basis%shell(Jshell)%EXPEND
      Jfrst=Basis%shell(Jshell)%frstSHL
!
      Ilast= Basis%shell(Ishell)%lastSHL
      Jlast= Basis%shell(Jshell)%lastSHL
!
! Loop over Iatmshl
      do Iatmshl=Ifrst,Ilast
      XA=CARTESIAN(Basis%atmshl(Iatmshl)%ATMLST)%X
      YA=CARTESIAN(Basis%atmshl(Iatmshl)%ATMLST)%Y
      ZA=CARTESIAN(Basis%atmshl(Iatmshl)%ATMLST)%Z
      Iaos=Basis%atmshl(Iatmshl)%frstAO-1
!
! Loop over Jatmshl
      IF(Ishell.EQ.Jshell)Jlast=Iatmshl
      do Jatmshl=Jfrst,Jlast
      XB=CARTESIAN(Basis%atmshl(Jatmshl)%ATMLST)%X
      YB=CARTESIAN(Basis%atmshl(Jatmshl)%ATMLST)%Y
      ZB=CARTESIAN(Basis%atmshl(Jatmshl)%ATMLST)%Z
      Jaos=Basis%atmshl(Jatmshl)%frstAO-1
!
      LPMAX=LAMAX+LBMAX-1
      LENTQ=Irange*Jrange
      NZERO=(LAMAX+LBMAX-2)/2+1
      ABX=XB-XA
      ABY=YB-YA
      ABZ=ZB-ZA
      RABSQ=ABX*ABX+ABY*ABY+ABZ*ABZ
!
      EEPVA(1:LENTQ,1:NAtoms)=ZERO
!
! Loop over primitive Gaussians.
      do Igauss=IGBGN,IGEND
      AS=Basis%gaussian(Igauss)%exp
      TWOASQ=TWO*AS*AS
      ASXA=AS*XA
      ASYA=AS*YA
      ASZA=AS*ZA
      ARABSQ=AS*RABSQ
      call FILLCC (LAMAX, Basis%gaussian(Igauss)%CONTRC, CA)

      do Jgauss=JGBGN,JGEND
      BS=Basis%gaussian(Jgauss)%exp
      call FILLCC (LBMAX, Basis%gaussian(Jgauss)%CONTRC, CB)
      EP=AS+BS
      EPI=ONE/EP
      EPIO2=PT5*EPI
      TWOP=EP+EP
      ARG=-BS*ARABSQ*EPI
      PEXP=ZERO
      IF(ARG.GT.CUT1)PEXP=DEXP(ARG)
      ZTEMP=TWOPI*EPI*PEXP
      PX=(ASXA+BS*XB)*EPI
      PY=(ASYA+BS*YB)*EPI
      PZ=(ASZA+BS*ZB)*EPI
      XAP=PX-XA
      XBP=PX-XB
      YAP=PY-YA
      YBP=PY-YB
      ZAP=PZ-ZA
      ZBP=PZ-ZB
      call GETCC1 (CCX, XAP, XBP, LAMAX+2, LBMAX+2)
      call GETCC1 (CCY, YAP, YBP, LAMAX+2, LBMAX+2)
      call GETCC1 (CCZ, ZAP, ZBP, LAMAX+2, LBMAX+2)
!
! Zero accumulation area.
!
      EEPBA(1:LENTQ,1:Natoms)=ZERO
! Loop over atoms.
      do Iatom=1,Natoms
      IA=CARTESIAN(Iatom)%Atomic_number
      if(IA.le.0)cycle
      EEPB(1:LENTQ)=ZERO
      XC=CARTESIAN(Iatom)%X
      YC=CARTESIAN(Iatom)%Y
      ZC=CARTESIAN(Iatom)%Z
      ZT=ZTEMP*DBLE(IA)
      PCX=XC-PX
      PCY=YC-PY
      PCZ=ZC-PZ
      RPCSQ=PCX*PCX+PCY*PCY+PCZ*PCZ
      ARG=EP*RPCSQ
      call RPOLX (NZERO, ARG, TP, WP)
      call GETA1 (A, EPIO2, 0, LPMAX)
!
! Loop over zeroes of Rys polynomial.
      do IZERO=1,NZERO
      TWOPT2=TWOP*TP(IZERO)
      ZCONST=ZT*WP(IZERO)
      call GET2C (TWOCX, PCX, ONE, A, TWOPT2, 0, LPMAX)
      call GET2C (TWOCY, PCY, ONE, A, TWOPT2, 0, LPMAX)
      call GET2C (TWOCZ, PCZ, ZCONST, A, TWOPT2, 0, LPMAX)
      call GET3C (XIP, 0, TWOCX, CCX, LAMAX, LBMAX)
      call GET3C (YIP, 0, TWOCY, CCY, LAMAX, LBMAX)
      call GET3C (ZIP, 0, TWOCZ, CCZ, LAMAX, LBMAX)
!
! Loop over atomic orbitals.
      INTC=0
      do I=Istart,Iend
      IX=INDIX(I)
      IY=INDIY(I)
      IZ=INDIZ(I)
!
      do J=Jstart,Jend
      JX=INDJX(J)
      JY=INDJY(J)
      JZ=INDJZ(J)
      INTC=INTC+1
      EEPB(INTC)=EEPB(INTC)+XIP(IX+JX)*YIP(IY+JY)*ZIP(IZ+JZ)
      end do ! J
      end do ! I
! End loop over primitives.
      end do ! IZERO
! End of loop over Rys zeroes.
      EEPBA(1:LENTQ,Iatom)=EEPB(1:LENTQ)
      end do ! Iatom
! End of loop over atoms.
!
! Apply the contraction coefficients.
      INTC=0
      do I=Istart,Iend
      do J=Jstart,Jend
      INTC=INTC+1
      COEF=CA(I)*CB(J)
      do Iatom=1,Natoms
      IA=CARTESIAN(Iatom)%Atomic_number
      if(IA.le.0)cycle
      EEPVA(INTC,Iatom)=EEPVA(INTC,Iatom)-EEPBA(INTC,Iatom)*COEF
      end do ! Iatom
      end do ! J
      end do ! I
!
      end do ! Jgauss
      end do ! Igauss
! End of loop over Gaussians.
!
      EEPV(1:LENTQ)=ZERO
      Vint_temp(1:MATlen)=ZERO
      do Iatom=1,Natoms
        IA=CARTESIAN(Iatom)%Atomic_number
        if(IA.le.0)cycle
!       EEPV(1:LENTQ)=EEPV(1:LENTQ)+EEPVA(1:LENTQ,Iatom)
        EEPV(1:LENTQ)=EEPVA(1:LENTQ,Iatom)
        call FILMAT (EEPV, Vint_temp, MATlen, Iend, Jend, Iatmshl, Jatmshl, Irange, Jrange, Iaos, JaoS)
        Vne_Atoms(1:MATlen,Iatom)=Vne_Atoms(1:MATlen,Iatom)+Vint_temp(1:MATlen)
      end do ! Iatom
!
      end do ! Jatmshl
      end do ! Iatmshl
      end do ! Jshell
      end do ! Ishell

      Vint(1:MATlen)=ZERO
      do Iatom=1,Natoms
        IA=CARTESIAN(Iatom)%Atomic_number
        if(IA.le.0)cycle
        Vint(1:MATlen)=Vint(1:MATlen)+Vne_Atoms(1:MATlen,Iatom)
      end do ! Iatom
!
! End of loop over shells.
      write(UNIout,*)'NUCLEAR ATTRACTION (V)'
      call PRT_matrix (Vint, MATlen, Nbasis)
!     EVtotal = TRACLO (Vint, PM0, Nbasis, MATlen)
!    write(UNIout,*)'Total Potential Energy: ',EVtotal  ! Correct!

      EVtotal=ZERO
      do Iatom=1,Natoms
        IA=CARTESIAN(Iatom)%Atomic_number
        if(IA.le.0)cycle
        Vint(1:MATlen)=Vne_Atoms(1:MATlen,Iatom)
        EV_Atom(Iatom) = TRACLO (Vint, PM0, Nbasis, MATlen)
        EVtotal = EVtotal + EV_Atom(Iatom)
       write(UNIout,*)'Atomic Potential Energy for atom ',CARTESIAN(Iatom)%ELEMENT, &
                      'label',Iatom,' : ',EV_Atom(Iatom)
      end do ! Iatom

     write(UNIout,*)'Total Potential Energy: ',EVtotal
!
! End of routine Vne_Ints
      call PRG_manager ('exit', 'VNE_INTS', '1EINT%VNE')
      return
      end subroutine Vne_Ints
