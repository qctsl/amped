      subroutine Velocity_Gaussian_prod (rx, ry, rz, dAOprodX, dAOprodY, dAOprodZ, MATlen)
!************************************************************************************************************
!     Date last modified: May 10, 2011                                                         Version 3.0  *
!     Authors: Ahmad and R.A. Poirier                                                                       *
!     Description:                                                                                          *
!                                                                                                           *
!************************************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE type_basis_set
      USE matrix_print
      USE QM_defaults

      implicit none
!
! Input scalars:
      integer :: MATlen
      double precision :: rx,ry,rz
      double precision :: dAOprodX(MATlen),dAOprodY(MATlen),dAOprodZ(MATlen)
!
! Local scalars:
      integer :: Ishell,Jshell,Ifrst,Jfrst,Ilast,Jlast
      integer :: Istart,Jstart,Iend,Jend
      integer :: Iatmshl,Jatmshl
      integer :: LAMAX,LBMAX
      integer :: Iatom,Jatom
      integer :: Irange,Jrange
      integer :: Igauss,Jgauss
      integer :: IGBGN,JGBGN,IGend,JGend
      integer :: Iao,Jao,AOI,AOJ
      integer :: I,J,K,INDEX,DEFCASE,IJao
      double precision :: COEF_SS,COEF_SP,COEF_SXX,COEF_SXY,COEF_PP,COEF_PXX,COEF_PXY,COEF_XXYY
      double precision :: COEF_XXXY,COEF_XYXY,SQRT3,SUMDSQ,Gama,Alpha,Beta,RABSQ,EG,EG_K,EG_exp
      double precision :: Ax,Bx,Ay,By,Az,Bz,Px,Py,Pz,xA,yA,zA,xB,yB,zB,rpx,rpy,rpz
      double precision :: dEGx,dEGy,dEGz
      logical :: LIatmshl
!
! Local arrays  
      double precision :: dAOX(100),dAOY(100),dAOZ(100)     ! save derivatives

! Begin:
      SQRT3=DSQRT(THREE)
!
      MATlen=size(dAOprodX)
      dAOprodX(1:MATlen)=ZERO
      dAOprodY(1:MATlen)=ZERO
      dAOprodZ(1:MATlen)=ZERO
!
! Begin loop over shells.
!
! Loop over elemental SHELLs
! Loop over Ishell.
      do Ishell=1,Basis%Nshells
        LAMAX=Basis%shell(Ishell)%Xtype+1
        Istart=Basis%shell(Ishell)%Xstart
        Iend=Basis%shell(Ishell)%XEND
        Irange=Iend-Istart+1
        IGBGN=Basis%shell(Ishell)%EXPBGN
        IGEND=Basis%shell(Ishell)%EXPEND
        Ifrst=Basis%shell(Ishell)%frstSHL
!
! Loop over Jshell.
      do Jshell=1,Ishell
        LBMAX=Basis%shell(Jshell)%Xtype+1
        Jstart=Basis%shell(Jshell)%Xstart
        Jend=Basis%shell(Jshell)%XEND
        Jrange=Jend-Jstart+1
        JGBGN=Basis%shell(Jshell)%EXPBGN
        JGEND=Basis%shell(Jshell)%EXPEND
        Jfrst=Basis%shell(Jshell)%frstSHL
!
! Define CASE:
      if(LAMAX.lt.LBMAX)then
       DEFCASE=4*(LAMAX-1)+LBMAX
      else
       DEFCASE=4*(LBMAX-1)+LAMAX
      end if
!
      Ilast= Basis%shell(Ishell)%lastSHL
      Jlast= Basis%shell(Jshell)%lastSHL
!
      select case (DEFCASE)
! S|S
     case (1)
        call SS_dproducts
! S|P
      case (2)
        call SP_dproducts
! S|D
      case (3)
        call SD_dproducts
! P|P
      case (6)
        call PP_dproducts
! P|D
      case (7)
        call PD_dproducts
! D|D
      case (11)
        call DD_dproducts

      case default
        write(UNIout,*)'ERROR> dAO_PRODUCTS: DEFCASE type should not exist'
        stop ' ERROR> dAO_PRODUCTS: DEFCASE type should not exist'
      end select
!
! End of loop over shells.
      
      end do ! Jshell
      end do ! Ishell
!
! End of routine dAO_PRODUCTS
      return
      CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine SS_dproducts
!************************************************************************************************************
!     Date last modified: May 10, 2011                                                         Version 3.0  *
!     Authors: Ahmad and R.A. Poirier                                                                       *
!     Description:                                                                                          *
!                                                                                                           *
!************************************************************************************************************
! Modules:

      implicit none
!
! Loop over all atomic shells
      do Iatmshl=Ifrst,Ilast
        Iatom=Basis%atmshl(Iatmshl)%ATMLST
!
        Ax=CARTESIAN(Iatom)%X     ! vector A={Ax,Ay,Az}     
        Ay=CARTESIAN(Iatom)%Y
        Az=CARTESIAN(Iatom)%Z
!
        AOI=Basis%atmshl(Iatmshl)%frstAO-1
        IF(Ishell.EQ.Jshell)Jlast=Iatmshl
      do Jatmshl=Jfrst,Jlast
        Jatom=Basis%atmshl(Jatmshl)%ATMLST
!
        Bx=CARTESIAN(Jatom)%X     ! vector B={Bx,By,Bz}
        By=CARTESIAN(Jatom)%Y
        Bz=CARTESIAN(Jatom)%Z
!
        AOJ=Basis%atmshl(Jatmshl)%frstAO-1
        LIatmshl=.false.
        if(Iatmshl.eq.Jatmshl)LIatmshl=.true.

        RABSQ=(Ax-Bx)*(Ax-Bx)+(Ay-By)*(Ay-By)+(Az-Bz)*(Az-Bz)     ! (A-B)**2

        dAOX(1:100)=ZERO     ! will handle up to <f|f>
        dAOY(1:100)=ZERO
        dAOZ(1:100)=ZERO
! Loop over primitive gaussians
!
      do Igauss=IGBGN,IGEND
        Alpha=Basis%gaussian(Igauss)%exp

      do Jgauss=JGBGN,JGEND
        Beta=Basis%gaussian(Jgauss)%exp
!
        Gama=Alpha+Beta

        Px=(Alpha*Ax+Beta*Bx)/Gama     ! vector P
        Py=(Alpha*Ay+Beta*By)/Gama
        Pz=(Alpha*Az+Beta*Bz)/Gama
!
        rpx = Px-rx     ! vector rp=P-r
        rpy = Py-ry
        rpz = Pz-rz

        SUMDSQ = rpx*rpx + rpy*rpy + rpz*rpz   ! (rp)**2

        EG_K=DEXP(-Alpha*Beta*RABSQ/Gama)      ! K 
        EG_exp=DEXP(-Gama*SUMDSQ)              ! exp
        EG=EG_K*EG_exp                         ! K*exp
!
        xA=rx-Ax     ! vector rA=(r-A)
        yA=ry-Ay
        zA=rz-Az
        xB=rx-Bx     ! vector rB=(r-B)
        yB=ry-By
        zB=rz-Bz
! Done
      dEGx=-two*Beta*xB*EG     ! derivative of EG with respect to x
      dEGy=-two*Beta*yB*EG     ! derivative of EG with respect to y
      dEGz=-two*Beta*zB*EG     ! derivative of EG with respect to z 
! S|S
        COEF_SS = Basis%gaussian(Igauss)%CONTRC*Basis%gaussian(Jgauss)%CONTRC

! derivative:
        dAOX(1)=dAOX(1)+COEF_SS*dEGx     ! COEF_SS*EG
        dAOY(1)=dAOY(1)+COEF_SS*dEGy
        dAOZ(1)=dAOZ(1)+COEF_SS*dEGz
      end do ! Jgauss
      end do ! Igauss 
      
        IF(LIatmshl)then
          INDEX=0
          do I=1,Irange
          do J=1,Jrange
            INDEX=INDEX+1
            IF(I.GE.J)then  ! Corrected August 1,2003 had (J.ge.I)
              Iao=AOI+I
              Jao=AOJ+J
              IJao=Iao*(Iao-1)/2+Jao
              dAOprodX(IJao)=dAOX(INDEX)
              dAOprodY(IJao)=dAOY(INDEX)
              dAOprodZ(IJao)=dAOZ(INDEX)
            end if
          end do ! J   
          end do ! I
            else
              INDEX=0
          do I=1,Irange
          do J=1,Jrange
            INDEX=INDEX+1
            Iao=AOI+I
            Jao=AOJ+J
            IJao=Iao*(Iao-1)/2+Jao
            dAOprodX(IJao)=dAOX(INDEX)
            dAOprodY(IJao)=dAOY(INDEX)
            dAOprodZ(IJao)=dAOZ(INDEX)

          end do ! J
          end do ! I
        end if ! LIatmshl

      end do ! Jatmshl
      end do ! Iatmshl

      return
      end subroutine SS_dproducts
      subroutine SP_dproducts
!************************************************************************************************************
!     Date last modified: May 10, 2011                                                         Version 3.0  *
!     Authors: Ahmad and R.A. Poirier                                                                       *
!     Description:                                                                                          *
!                                                                                                           *
!************************************************************************************************************
! Modules:

      implicit none
!
! Loop over all atomic shells
      do Iatmshl=Ifrst,Ilast
        Iatom=Basis%atmshl(Iatmshl)%ATMLST
!
        Ax=CARTESIAN(Iatom)%X     ! vector A={Ax,Ay,Az}     
        Ay=CARTESIAN(Iatom)%Y
        Az=CARTESIAN(Iatom)%Z
!
        AOI=Basis%atmshl(Iatmshl)%frstAO-1
        IF(Ishell.EQ.Jshell)Jlast=Iatmshl
      do Jatmshl=Jfrst,Jlast
        Jatom=Basis%atmshl(Jatmshl)%ATMLST
!
        Bx=CARTESIAN(Jatom)%X     ! vector B={Bx,By,Bz}
        By=CARTESIAN(Jatom)%Y
        Bz=CARTESIAN(Jatom)%Z
!
        AOJ=Basis%atmshl(Jatmshl)%frstAO-1
        LIatmshl=.false.
        if(Iatmshl.eq.Jatmshl)LIatmshl=.true.

        RABSQ=(Ax-Bx)*(Ax-Bx)+(Ay-By)*(Ay-By)+(Az-Bz)*(Az-Bz)     ! (A-B)**2

        dAOX(1:100)=ZERO     ! will handle up to <f|f>
        dAOY(1:100)=ZERO
        dAOZ(1:100)=ZERO

! Loop over primitive gaussians
!
      do Igauss=IGBGN,IGEND
        Alpha=Basis%gaussian(Igauss)%exp

      do Jgauss=JGBGN,JGEND
        Beta=Basis%gaussian(Jgauss)%exp
!
        Gama=Alpha+Beta

        Px=(Alpha*Ax+Beta*Bx)/Gama     ! vector P
        Py=(Alpha*Ay+Beta*By)/Gama
        Pz=(Alpha*Az+Beta*Bz)/Gama
!
        rpx = Px-rx     ! vector rp=P-r
        rpy = Py-ry
        rpz = Pz-rz

        SUMDSQ = rpx*rpx + rpy*rpy + rpz*rpz   ! (rp)**2
        
        EG_K=DEXP(-Alpha*Beta*RABSQ/Gama)      ! K  
        EG_exp=DEXP(-Gama*SUMDSQ)              ! exp
        EG=EG_K*EG_exp                         ! K*exp
!
        xA=rx-Ax     ! vector rA=(r-A)
        yA=ry-Ay
        zA=rz-Az
        xB=rx-Bx     ! vector rB=(r-B)
        yB=ry-By
        zB=rz-Bz
!
      dEGx=-two*Beta*xB*EG     ! derivative of EG with respect to x
      dEGy=-two*Beta*yB*EG     ! derivative of EG with respect to y
      dEGz=-two*Beta*zB*EG     ! derivative of EG with respect to z 

! S|P  
        COEF_SP = Basis%gaussian(Igauss)%CONTRC*Basis%gaussian(Jgauss)%CONTRC
!
! derivative:
        dAOX(1)=dAOX(1)+COEF_SP*(xB*dEGx+EG)     ! S!Px
        dAOY(1)=dAOY(1)+COEF_SP*dEGy
        dAOZ(1)=dAOZ(1)+COEF_SP*dEGz

        dAOX(2)=dAOX(2)+COEF_SP*dEGx          ! S!Py
        dAOY(2)=dAOY(2)+COEF_SP*(yB*dEGy+EG)
        dAOZ(2)=dAOZ(2)+COEF_SP*dEGz

        dAOX(3)=dAOX(3)+COEF_SP*dEGx          ! S!Pz
        dAOY(3)=dAOY(3)+COEF_SP*dEGy
        dAOZ(3)=dAOZ(3)+COEF_SP*(zB*dEGz+EG)
      end do ! Jgauss
      end do ! Igauss 
      
        IF(LIatmshl)then
          INDEX=0
          do I=1,Irange
          do J=1,Jrange
            INDEX=INDEX+1
            IF(I.GE.J)then  ! Corrected August 1,2003 had (J.ge.I)
              Iao=AOI+I
              Jao=AOJ+J
              IJao=Iao*(Iao-1)/2+Jao
              dAOprodX(IJao)=dAOX(INDEX)
              dAOprodY(IJao)=dAOY(INDEX)
              dAOprodZ(IJao)=dAOZ(INDEX)
            end if
          end do ! J   
          end do ! I
            else
              INDEX=0
          do I=1,Irange
          do J=1,Jrange
            INDEX=INDEX+1
            Iao=AOI+I
            Jao=AOJ+J
            IJao=Iao*(Iao-1)/2+Jao
            dAOprodX(IJao)=dAOX(INDEX)
            dAOprodY(IJao)=dAOY(INDEX)
            dAOprodZ(IJao)=dAOZ(INDEX)
          end do ! J
          end do ! I
        end if ! LIatmshl

      end do ! Jatmshl
      end do ! Iatmshl

      return
      end subroutine SP_dproducts
      subroutine PP_dproducts
!************************************************************************************************************
!     Date last modified: May 10, 2011                                                         Version 3.0  *
!     Authors: Ahmad and R.A. Poirier                                                                       *
!     Description:                                                                                          *
!                                                                                                           *
!************************************************************************************************************
! Modules:

      implicit none
!
! Loop over all atomic shells
      do Iatmshl=Ifrst,Ilast
        Iatom=Basis%atmshl(Iatmshl)%ATMLST
!
        Ax=CARTESIAN(Iatom)%X     ! vector A={Ax,Ay,Az}     
        Ay=CARTESIAN(Iatom)%Y
        Az=CARTESIAN(Iatom)%Z
!
        AOI=Basis%atmshl(Iatmshl)%frstAO-1
        IF(Ishell.EQ.Jshell)Jlast=Iatmshl
      do Jatmshl=Jfrst,Jlast
        Jatom=Basis%atmshl(Jatmshl)%ATMLST
!
        Bx=CARTESIAN(Jatom)%X     ! vector B={Bx,By,Bz}
        By=CARTESIAN(Jatom)%Y
        Bz=CARTESIAN(Jatom)%Z
!
        AOJ=Basis%atmshl(Jatmshl)%frstAO-1
        LIatmshl=.false.
        if(Iatmshl.eq.Jatmshl)LIatmshl=.true.

        RABSQ=(Ax-Bx)*(Ax-Bx)+(Ay-By)*(Ay-By)+(Az-Bz)*(Az-Bz)     ! (A-B)**2

        dAOX(1:100)=ZERO     ! will handle up to <f|f>
        dAOY(1:100)=ZERO
        dAOZ(1:100)=ZERO

! Loop over primitive gaussians
!
      do Igauss=IGBGN,IGEND
        Alpha=Basis%gaussian(Igauss)%exp

      do Jgauss=JGBGN,JGEND
        Beta=Basis%gaussian(Jgauss)%exp
!
        Gama=Alpha+Beta

        Px=(Alpha*Ax+Beta*Bx)/Gama     ! vector P
        Py=(Alpha*Ay+Beta*By)/Gama
        Pz=(Alpha*Az+Beta*Bz)/Gama
!
        rpx = Px-rx     ! vector rp=P-r
        rpy = Py-ry
        rpz = Pz-rz

        SUMDSQ = rpx*rpx + rpy*rpy + rpz*rpz   ! (rp)**2

        EG_K=DEXP(-Alpha*Beta*RABSQ/Gama)      ! K  
        EG_exp=DEXP(-Gama*SUMDSQ)              ! exp
        EG=EG_K*EG_exp                         ! K*exp
!
        xA=rx-Ax     ! vector rA=(r-A)
        yA=ry-Ay
        zA=rz-Az
        xB=rx-Bx     ! vector rB=(r-B)
        yB=ry-By
        zB=rz-Bz
!
      dEGx=-two*Beta*xB*EG     ! derivative of EG with respect to x
      dEGy=-two*Beta*yB*EG     ! derivative of EG with respect to y
      dEGz=-two*Beta*zB*EG     ! derivative of EG with respect to z 

! P|P          
        COEF_PP = Basis%gaussian(Igauss)%CONTRC*Basis%gaussian(Jgauss)%CONTRC
!
! derivative: Px|Px
        dAOX(1)=dAOX(1)+COEF_PP*(xA*xB*dEGx+xA*EG)    ! xA*xB*COEF_PP*EG
        dAOY(1)=dAOY(1)+COEF_PP*xA*xB*dEGy
        dAOZ(1)=dAOZ(1)+COEF_PP*xA*xB*dEGz
! Px Py
        dAOX(2)=dAOX(2)+COEF_PP*xA*yB*dEGx         ! xA*yB*COEF_PP*EG
        dAOY(2)=dAOY(2)+COEF_PP*(xA*yB*dEGy+xA*EG)
        dAOZ(2)=dAOZ(2)+COEF_PP*xA*yB*dEGz
! Px Pz
        dAOX(3)=dAOX(3)+COEF_PP*xA*zB*dEGx         ! xA*zB*COEF_PP*EG
        dAOY(3)=dAOY(3)+COEF_PP*xA*zB*dEGy
        dAOZ(3)=dAOZ(3)+COEF_PP*(xA*zB*dEGz+xA*EG)
! Py Px
        dAOX(4)=dAOX(4)+COEF_PP*(yA*xB*dEGx+yA*EG)         ! yA*xB*COEF_PP*EG
        dAOY(4)=dAOY(4)+COEF_PP*yA*xB*dEGy
        dAOZ(4)=dAOZ(4)+COEF_PP*yA*xB*dEGz
! Py Py
        dAOX(5)=dAOX(5)+COEF_PP*yA*yB*dEGx                 ! yA*yB*COEF_PP*EG
        dAOY(5)=dAOY(5)+COEF_PP*(yA*yB*dEGy+yA*EG)
        dAOZ(5)=dAOZ(5)+COEF_PP*yA*yB*dEGz
! Py Pz
        dAOX(6)=dAOX(6)+COEF_PP*yA*zB*dEGx                 ! yA*zB*COEF_PP*EG
        dAOY(6)=dAOY(6)+COEF_PP*yA*zB*dEGy
        dAOZ(6)=dAOZ(6)+COEF_PP*(yA*zB*dEGz+yA*EG)
! Pz Px
        dAOX(7)=dAOX(7)+COEF_PP*(zA*xB*dEGx+zA*EG)         ! zA*xB*COEF_PP*EG
        dAOY(7)=dAOY(7)+COEF_PP*zA*xB*dEGy
        dAOZ(7)=dAOZ(7)+COEF_PP*zA*xB*dEGz
! Pz Py
        dAOX(8)=dAOX(8)+COEF_PP*zA*yB*dEGx                 ! zA*yB*COEF_PP*EG
        dAOY(8)=dAOY(8)+COEF_PP*(zA*yB*dEGy+zA*EG)
        dAOZ(8)=dAOZ(8)+COEF_PP*zA*yB*dEGz
! Pz Pz
        dAOX(9)=dAOX(9)+COEF_PP*zA*zB*dEGx                 ! zA*zB*COEF_PP*EG
        dAOY(9)=dAOY(9)+COEF_PP*zA*zB*dEGy
        dAOZ(9)=dAOZ(9)+COEF_PP*(zA*zB*dEGz+zA*EG)
      end do ! Jgauss
      end do ! Igauss 
      
        IF(LIatmshl)then
          INDEX=0
          do I=1,Irange
          do J=1,Jrange
            INDEX=INDEX+1
            IF(I.GE.J)then  ! Corrected August 1,2003 had (J.ge.I)
              Iao=AOI+I
              Jao=AOJ+J
              IJao=Iao*(Iao-1)/2+Jao
              dAOprodX(IJao)=dAOX(INDEX)
              dAOprodY(IJao)=dAOY(INDEX)
              dAOprodZ(IJao)=dAOZ(INDEX)
            end if
          end do ! J   
          end do ! I
            else
              INDEX=0
          do I=1,Irange
          do J=1,Jrange
            INDEX=INDEX+1
            Iao=AOI+I
            Jao=AOJ+J
            IJao=Iao*(Iao-1)/2+Jao
            dAOprodX(IJao)=dAOX(INDEX)
            dAOprodY(IJao)=dAOY(INDEX)
            dAOprodZ(IJao)=dAOZ(INDEX)
          end do ! J
          end do ! I
        end if ! LIatmshl

      end do ! Jatmshl
      end do ! Iatmshl

      return
      end subroutine PP_dproducts
      subroutine SD_dproducts
!************************************************************************************************************
!     Date last modified: May 10, 2011                                                         Version 3.0  *
!     Authors: Ahmad and R.A. Poirier                                                                       *
!     Description:                                                                                          *
!                                                                                                           *
!************************************************************************************************************
! Modules:

      implicit none
!
! Loop over all atomic shells
      do Iatmshl=Ifrst,Ilast
        Iatom=Basis%atmshl(Iatmshl)%ATMLST
!
        Ax=CARTESIAN(Iatom)%X     ! vector A={Ax,Ay,Az}     
        Ay=CARTESIAN(Iatom)%Y
        Az=CARTESIAN(Iatom)%Z
!
        AOI=Basis%atmshl(Iatmshl)%frstAO-1
        IF(Ishell.EQ.Jshell)Jlast=Iatmshl
      do Jatmshl=Jfrst,Jlast
        Jatom=Basis%atmshl(Jatmshl)%ATMLST
!
        Bx=CARTESIAN(Jatom)%X     ! vector B={Bx,By,Bz}
        By=CARTESIAN(Jatom)%Y
        Bz=CARTESIAN(Jatom)%Z
!
        AOJ=Basis%atmshl(Jatmshl)%frstAO-1
        LIatmshl=.false.
        if(Iatmshl.eq.Jatmshl)LIatmshl=.true.

        RABSQ=(Ax-Bx)*(Ax-Bx)+(Ay-By)*(Ay-By)+(Az-Bz)*(Az-Bz)     ! (A-B)**2

        dAOX(1:100)=ZERO     ! will handle up to <f|f>
        dAOY(1:100)=ZERO
        dAOZ(1:100)=ZERO

! Loop over primitive gaussians
!
      do Igauss=IGBGN,IGEND
        Alpha=Basis%gaussian(Igauss)%exp

      do Jgauss=JGBGN,JGEND
        Beta=Basis%gaussian(Jgauss)%exp
!
        Gama=Alpha+Beta

        Px=(Alpha*Ax+Beta*Bx)/Gama     ! vector P
        Py=(Alpha*Ay+Beta*By)/Gama
        Pz=(Alpha*Az+Beta*Bz)/Gama
!
        rpx = Px-rx     ! vector rp=P-r
        rpy = Py-ry
        rpz = Pz-rz

        SUMDSQ = rpx*rpx + rpy*rpy + rpz*rpz   ! (rp)**2

        EG_K=DEXP(-Alpha*Beta*RABSQ/Gama)      ! K  
        EG_exp=DEXP(-Gama*SUMDSQ)              ! exp
        EG=EG_K*EG_exp                         ! K*exp
!
        xA=rx-Ax     ! vector rA=(r-A)
        yA=ry-Ay
        zA=rz-Az
        xB=rx-Bx     ! vector rB=(r-B)
        yB=ry-By
        zB=rz-Bz
!
      dEGx=-two*Beta*xB*EG     ! derivative of EG with respect to x
      dEGy=-two*Beta*yB*EG     ! derivative of EG with respect to y
      dEGz=-two*Beta*zB*EG     ! derivative of EG with respect to z 
! S|D        
        COEF_SXX = Basis%gaussian(Igauss)%CONTRC*Basis%gaussian(Jgauss)%CONTRC
        COEF_SXY = SQRT3*Basis%gaussian(Igauss)%CONTRC*Basis%gaussian(Jgauss)%CONTRC
!
! derivative: S XX
        dAOX(1)=dAOX(1)+COEF_SXX*(xB*xB*dEGx+two*xB*EG)      ! xB*xB*COEF_SXX*EG
        dAOY(1)=dAOY(1)+COEF_SXX*xB*xB*dEGy
        dAOZ(1)=dAOZ(1)+COEF_SXX*xB*xB*dEGz
! S YY
        dAOX(2)=dAOX(2)+COEF_SXX*yB*yB*dEGx                ! yB*yB*COEF_SXX*EG 
        dAOY(2)=dAOY(2)+COEF_SXX*(yB*yB*dEGy+two*yB*EG)
        dAOZ(2)=dAOZ(2)+COEF_SXX*yB*yB*dEGz
! S ZZ
        dAOX(3)=dAOX(3)+COEF_SXX*zB*zB*dEGx                ! zB*zB*COEF_SXX*EG    
        dAOY(3)=dAOY(3)+COEF_SXX*zB*zB*dEGy
        dAOZ(3)=dAOZ(3)+COEF_SXX*(zB*zB*dEGz+two*zB*EG)
! S XY
        dAOX(4)=dAOX(4)+COEF_SXY*(xB*yB*dEGx+yB*EG)        ! xB*yB*COEF_SXY*EG 
        dAOY(4)=dAOY(4)+COEF_SXY*(xB*yB*dEGy+xB*EG)
        dAOZ(4)=dAOZ(4)+COEF_SXY*xB*yB*dEGz
! S XZ
        dAOX(5)=dAOX(5)+COEF_SXY*(xB*zB*dEGx+zB*EG)        ! xB*zB*COEF_SXY*EG
        dAOY(5)=dAOY(5)+COEF_SXY*xB*zB*dEGy
        dAOZ(5)=dAOZ(5)+COEF_SXY*(xB*zB*dEGz+xB*EG)
! S YZ
        dAOX(6)=dAOX(6)+COEF_SXY*yB*zB*dEGx                ! yB*zB*COEF_SXY*EG
        dAOY(6)=dAOY(6)+COEF_SXY*(yB*zB*dEGy+zB*EG)
        dAOZ(6)=dAOZ(6)+COEF_SXY*(yB*zB*dEGz+yB*EG)
      end do ! Jgauss
      end do ! Igauss 
      
        IF(LIatmshl)then
          INDEX=0
          do I=1,Irange
          do J=1,Jrange
            INDEX=INDEX+1
            IF(I.GE.J)then  ! Corrected August 1,2003 had (J.ge.I)
              Iao=AOI+I
              Jao=AOJ+J
              IJao=Iao*(Iao-1)/2+Jao
              dAOprodX(IJao)=dAOX(INDEX)
              dAOprodY(IJao)=dAOY(INDEX)
              dAOprodZ(IJao)=dAOZ(INDEX)
            end if
          end do ! J   
          end do ! I
            else
              INDEX=0
          do I=1,Irange
          do J=1,Jrange
            INDEX=INDEX+1
            Iao=AOI+I
            Jao=AOJ+J
            IJao=Iao*(Iao-1)/2+Jao
            dAOprodX(IJao)=dAOX(INDEX)
            dAOprodY(IJao)=dAOY(INDEX)
            dAOprodZ(IJao)=dAOZ(INDEX)
          end do ! J
          end do ! I
        end if ! LIatmshl

      end do ! Jatmshl
      end do ! Iatmshl

      return
      end subroutine SD_dproducts
      subroutine PD_dproducts
!************************************************************************************************************
!     Date last modified: May 10, 2011                                                         Version 3.0  *
!     Authors: Ahmad and R.A. Poirier                                                                       *
!     Description:                                                                                          *
!                                                                                                           *
!************************************************************************************************************
! Modules:

      implicit none
!
! Loop over all atomic shells
      do Iatmshl=Ifrst,Ilast
        Iatom=Basis%atmshl(Iatmshl)%ATMLST
!
        Ax=CARTESIAN(Iatom)%X     ! vector A={Ax,Ay,Az}     
        Ay=CARTESIAN(Iatom)%Y
        Az=CARTESIAN(Iatom)%Z
!
        AOI=Basis%atmshl(Iatmshl)%frstAO-1
        IF(Ishell.EQ.Jshell)Jlast=Iatmshl
      do Jatmshl=Jfrst,Jlast
        Jatom=Basis%atmshl(Jatmshl)%ATMLST
!
        Bx=CARTESIAN(Jatom)%X     ! vector B={Bx,By,Bz}
        By=CARTESIAN(Jatom)%Y
        Bz=CARTESIAN(Jatom)%Z
!
        AOJ=Basis%atmshl(Jatmshl)%frstAO-1
        LIatmshl=.false.
        if(Iatmshl.eq.Jatmshl)LIatmshl=.true.

        RABSQ=(Ax-Bx)*(Ax-Bx)+(Ay-By)*(Ay-By)+(Az-Bz)*(Az-Bz)     ! (A-B)**2

        dAOX(1:100)=ZERO     ! will handle up to <f|f>
        dAOY(1:100)=ZERO
        dAOZ(1:100)=ZERO

! Loop over primitive gaussians
!
      do Igauss=IGBGN,IGEND
        Alpha=Basis%gaussian(Igauss)%exp

      do Jgauss=JGBGN,JGEND
        Beta=Basis%gaussian(Jgauss)%exp
!
        Gama=Alpha+Beta

        Px=(Alpha*Ax+Beta*Bx)/Gama     ! vector P
        Py=(Alpha*Ay+Beta*By)/Gama
        Pz=(Alpha*Az+Beta*Bz)/Gama
!
        rpx = Px-rx     ! vector rp=P-r
        rpy = Py-ry
        rpz = Pz-rz

        SUMDSQ = rpx*rpx + rpy*rpy + rpz*rpz   ! (rp)**2

        EG_K=DEXP(-Alpha*Beta*RABSQ/Gama)      ! K  
        EG_exp=DEXP(-Gama*SUMDSQ)              ! exp
        EG=EG_K*EG_exp                         ! K*exp
!
        xA=rx-Ax     ! vector rA=(r-A)
        yA=ry-Ay
        zA=rz-Az
        xB=rx-Bx     ! vector rB=(r-B)
        yB=ry-By
        zB=rz-Bz
!
      dEGx=-two*Beta*xB*EG     ! derivative of EG with respect to x
      dEGy=-two*Beta*yB*EG     ! derivative of EG with respect to y
      dEGz=-two*Beta*zB*EG     ! derivative of EG with respect to z 

! P|D
        COEF_PXX = Basis%gaussian(Igauss)%CONTRC*Basis%gaussian(Jgauss)%CONTRC
        COEF_PXY = SQRT3*Basis%gaussian(Igauss)%CONTRC*Basis%gaussian(Jgauss)%CONTRC
!
! derivative:
        dAOX(1)=dAOX(1)+COEF_PXX*(xA*xB*xB*dEGx+two*xA*xB*EG)     ! OK
        dAOY(1)=dAOY(1)+COEF_PXX*xA*xB*xB*dEGy
        dAOZ(1)=dAOZ(1)+COEF_PXX*xA*xB*xB*dEGz

        dAOX(2)=dAOX(2)+COEF_PXX*xA*yB*yB*dEGx                  ! OK
        dAOY(2)=dAOY(2)+COEF_PXX*(xA*yB*yB*dEGy+two*xA*yB*EG)
        dAOZ(2)=dAOZ(2)+COEF_PXX*xA*yB*yB*dEGz

        dAOX(3)=dAOX(3)+COEF_PXX*(xA*zB*zB*dEGx)                  ! OK
        dAOY(3)=dAOY(3)+COEF_PXX*xA*zB*zB*dEGy
        dAOZ(3)=dAOZ(3)+COEF_PXX*(xA*zB*zB*dEGz+two*xA*zB*EG)

        dAOX(4)=dAOX(4)+COEF_PXY*(xA*xB*yB*dEGx+xA*yB*EG)             ! OK
        dAOY(4)=dAOY(4)+COEF_PXY*(xA*xB*yB*dEGy+xA*xB*EG)
        dAOZ(4)=dAOZ(4)+COEF_PXY*xA*xB*yB*dEGz

        dAOX(5)=dAOX(5)+COEF_PXY*(xA*xB*zB*dEGx+xA*zB*EG)             ! OK
        dAOY(5)=dAOY(5)+COEF_PXY*xA*xB*zB*dEGy
        dAOZ(5)=dAOZ(5)+COEF_PXY*(xA*xB*zB*dEGz+xA*xB*EG)

        dAOX(6)=dAOX(6)+COEF_PXY*(xA*yB*zB*dEGx)                  ! OK
        dAOY(6)=dAOY(6)+COEF_PXY*(xA*yB*zB*dEGy+xA*zB*EG)
        dAOZ(6)=dAOZ(6)+COEF_PXY*(xA*yB*zB*dEGz+xA*yB*EG)

        dAOX(7)=dAOX(7)+COEF_PXX*(yA*xB*xB*dEGx+two*yA*xB*EG)             ! OK
        dAOY(7)=dAOY(7)+COEF_PXX*(yA*xB*xB*dEGy)
        dAOZ(7)=dAOZ(7)+COEF_PXX*yA*xB*xB*dEGz
        
        dAOX(8)=dAOX(8)+COEF_PXX*yA*yB*yB*dEGx                             ! OK
        dAOY(8)=dAOY(8)+COEF_PXX*(yA*yB*yB*dEGy+two*yA*yB*EG)
        dAOZ(8)=dAOZ(8)+COEF_PXX*yA*yB*yB*dEGz
            
        dAOX(9)=dAOX(9)+COEF_PXX*yA*zB*zB*dEGx                             ! OK
        dAOY(9)=dAOY(9)+COEF_PXX*(yA*zB*zB*dEGy)
        dAOZ(9)=dAOZ(9)+COEF_PXX*(yA*zB*zB*dEGz+two*yA*zB*EG)
              
        dAOX(10)=dAOX(10)+COEF_PXY*(yA*xB*yB*dEGx+yA*yB*EG)                ! OK
        dAOY(10)=dAOY(10)+COEF_PXY*(yA*xB*yB*dEGy+yA*xB*EG)
        dAOZ(10)=dAOZ(10)+COEF_PXY*yA*xB*yB*dEGz
          
        dAOX(11)=dAOX(11)+COEF_PXY*(yA*xB*zB*dEGx+yA*zB*EG)                ! OK
        dAOY(11)=dAOY(11)+COEF_PXY*(yA*xB*zB*dEGy)
        dAOZ(11)=dAOZ(11)+COEF_PXY*(yA*xB*zB*dEGz+yA*xB*EG)

        dAOX(12)=dAOX(12)+COEF_PXY*(yA*yB*zB*dEGx)                         ! OK
        dAOY(12)=dAOY(12)+COEF_PXY*(yA*yB*zB*dEGy+yA*zB*EG)
        dAOZ(12)=dAOZ(12)+COEF_PXY*(yA*yB*zB*dEGz+yA*yB*EG)

        dAOX(13)=dAOX(13)+COEF_PXX*(zA*xB*xB*dEGx+two*zA*xB*EG)        ! OK
        dAOY(13)=dAOY(13)+COEF_PXX*zA*xB*xB*dEGy
        dAOZ(13)=dAOZ(13)+COEF_PXX*(zA*xB*xB*dEGz)
        
        dAOX(14)=dAOX(14)+COEF_PXX*zA*yB*yB*dEGx                           ! OK
        dAOY(14)=dAOY(14)+COEF_PXX*(zA*yB*yB*dEGy+two*zA*yB*EG)
        dAOZ(14)=dAOZ(14)+COEF_PXX*(zA*yB*yB*dEGz)
            
        dAOX(15)=dAOX(15)+COEF_PXX*zA*zB*zB*dEGx                           ! OK
        dAOY(15)=dAOY(15)+COEF_PXX*zA*zB*zB*dEGy
        dAOZ(15)=dAOZ(15)+COEF_PXX*(zA*zB*zB*dEGz+two*zA*zB*EG)
              
        dAOX(16)=dAOX(16)+COEF_PXY*(zA*xB*yB*dEGx+zA*yB*EG)                ! OK
        dAOY(16)=dAOY(16)+COEF_PXY*(zA*xB*yB*dEGy+zA*xB*EG)
        dAOZ(16)=dAOZ(16)+COEF_PXY*(zA*xB*yB*dEGz)
          
        dAOX(17)=dAOX(17)+COEF_PXY*(zA*xB*zB*dEGx+zA*zB*EG)                ! OK
        dAOY(17)=dAOY(17)+COEF_PXY*zA*xB*zB*dEGy
        dAOZ(17)=dAOZ(17)+COEF_PXY*(zA*xB*zB*dEGz+zA*xB*EG)

        dAOX(18)=dAOX(18)+COEF_PXY*zA*yB*zB*dEGx                           ! OK
        dAOY(18)=dAOY(18)+COEF_PXY*(zA*yB*zB*dEGy+zA*zB*EG)
        dAOZ(18)=dAOZ(18)+COEF_PXY*(zA*yB*zB*dEGz+zA*yB*EG)
      end do ! Jgauss
      end do ! Igauss 
      
        IF(LIatmshl)then
          INDEX=0
          do I=1,Irange
          do J=1,Jrange
            INDEX=INDEX+1
            IF(I.GE.J)then  ! Corrected August 1,2003 had (J.ge.I)
              Iao=AOI+I
              Jao=AOJ+J
              IJao=Iao*(Iao-1)/2+Jao
              dAOprodX(IJao)=dAOX(INDEX)
              dAOprodY(IJao)=dAOY(INDEX)
              dAOprodZ(IJao)=dAOZ(INDEX)
            end if
          end do ! J   
          end do ! I
            else
              INDEX=0
          do I=1,Irange
          do J=1,Jrange
            INDEX=INDEX+1
            Iao=AOI+I
            Jao=AOJ+J
            IJao=Iao*(Iao-1)/2+Jao
            dAOprodX(IJao)=dAOX(INDEX)
            dAOprodY(IJao)=dAOY(INDEX)
            dAOprodZ(IJao)=dAOZ(INDEX)
          end do ! J
          end do ! I
        end if ! LIatmshl

      end do ! Jatmshl
      end do ! Iatmshl

      return
      end subroutine PD_dproducts
      subroutine DD_dproducts
!************************************************************************************************************
!     Date last modified: May 10, 2011                                                         Version 3.0  *
!     Authors: Ahmad and R.A. Poirier                                                                       *
!     Description:                                                                                          *
!                                                                                                           *
!************************************************************************************************************
! Modules:

      implicit none
!
! Loop over all atomic shells
      do Iatmshl=Ifrst,Ilast
        Iatom=Basis%atmshl(Iatmshl)%ATMLST
!
        Ax=CARTESIAN(Iatom)%X     ! vector A={Ax,Ay,Az}     
        Ay=CARTESIAN(Iatom)%Y
        Az=CARTESIAN(Iatom)%Z
!
        AOI=Basis%atmshl(Iatmshl)%frstAO-1
        IF(Ishell.EQ.Jshell)Jlast=Iatmshl
      do Jatmshl=Jfrst,Jlast
        Jatom=Basis%atmshl(Jatmshl)%ATMLST
!
        Bx=CARTESIAN(Jatom)%X     ! vector B={Bx,By,Bz}
        By=CARTESIAN(Jatom)%Y
        Bz=CARTESIAN(Jatom)%Z
!
        AOJ=Basis%atmshl(Jatmshl)%frstAO-1
        LIatmshl=.false.
        if(Iatmshl.eq.Jatmshl)LIatmshl=.true.

        RABSQ=(Ax-Bx)*(Ax-Bx)+(Ay-By)*(Ay-By)+(Az-Bz)*(Az-Bz)     ! (A-B)**2

        dAOX(1:100)=ZERO     ! will handle up to <f|f>
        dAOY(1:100)=ZERO
        dAOZ(1:100)=ZERO

! Loop over primitive gaussians
!
      do Igauss=IGBGN,IGEND
        Alpha=Basis%gaussian(Igauss)%exp

      do Jgauss=JGBGN,JGEND
        Beta=Basis%gaussian(Jgauss)%exp
!
        Gama=Alpha+Beta

        Px=(Alpha*Ax+Beta*Bx)/Gama     ! vector P
        Py=(Alpha*Ay+Beta*By)/Gama
        Pz=(Alpha*Az+Beta*Bz)/Gama
!
        rpx = Px-rx     ! vector rp=P-r
        rpy = Py-ry
        rpz = Pz-rz

        SUMDSQ = rpx*rpx + rpy*rpy + rpz*rpz   ! (rp)**2

        EG_K=DEXP(-Alpha*Beta*RABSQ/Gama)      ! K  
        EG_exp=DEXP(-Gama*SUMDSQ)              ! exp
        EG=EG_K*EG_exp                         ! K*exp
!
        xA=rx-Ax     ! vector rA=(r-A)
        yA=ry-Ay
        zA=rz-Az
        xB=rx-Bx     ! vector rB=(r-B)
        yB=ry-By
        zB=rz-Bz
!
      dEGx=-two*Beta*xB*EG     ! derivative of EG with respect to x
      dEGy=-two*Beta*yB*EG     ! derivative of EG with respect to y
      dEGz=-two*Beta*zB*EG     ! derivative of EG with respect to z 

! D|D        
        COEF_XXYY = Basis%gaussian(Igauss)%CONTRC*Basis%gaussian(Jgauss)%CONTRC
        COEF_XXXY = SQRT3*COEF_XXYY
        COEF_XYXY = THREE*COEF_XXYY
!
! derivative:
! xA*xA*xB*xB*COEF_XXYY*EG
        dAOX(1)=dAOX(1)+COEF_XXYY*(xA*xA*xB*xB*dEGx+two*xA*xA*xB*EG) ! OK
        dAOY(1)=dAOY(1)+COEF_XXYY*xA*xA*xB*xB*dEGy                     
        dAOZ(1)=dAOZ(1)+COEF_XXYY*xA*xA*xB*xB*dEgz

! xA*xA*yB*yB*COEF_XXYY*EG
        dAOX(2)=dAOX(2)+COEF_XXYY*(xA*xA*yB*yB*dEGx)                 ! OK
        dAOY(2)=dAOY(2)+COEF_XXYY*(xA*xA*yB*yB*dEGy+two*xA*xA*yB*EG)
        dAOZ(2)=dAOZ(2)+COEF_XXYY*xA*xA*yB*yB*dEGz

! xA*xA*zB*zB*COEF_XXYY*EG
        dAOX(3)=dAOX(3)+COEF_XXYY*xA*xA*zB*zB*dEGx                  ! OK
        dAOY(3)=dAOY(3)+COEF_XXYY*xA*xA*zB*zB*dEGy
        dAOZ(3)=dAOZ(3)+COEF_XXYY*(xA*xA*zB*zB*dEGz+two*xA*xA*zB*EG)

! xA*xA*xB*yB*COEF_XXXY*EG
        dAOX(4)=dAOX(4)+COEF_XXXY*(xA*xA*xB*yB*dEGx+xA*xA*yB*EG)                 ! OK
        dAOY(4)=dAOY(4)+COEF_XXXY*(xA*xA*xB*yB*dEGy+xA*xA*xB*EG)
        dAOZ(4)=dAOZ(4)+COEF_XXXY*xA*xA*xB*yB*dEGz

! xA*xA*xB*zB*COEF_XXXY*EG
        dAOX(5)=dAOX(5)+COEF_XXXY*(xA*xA*xB*zB*dEGx+xA*xA*zB*EG)                 ! OK
        dAOY(5)=dAOY(5)+COEF_XXXY*xA*xA*xB*zB*dEGy
        dAOZ(5)=dAOZ(5)+COEF_XXXY*(xA*xA*xB*zB*dEGz+xA*xA*xB*EG)

! xA*xA*yB*zB*COEF_XXXY*EG OK
        dAOX(6)=dAOX(6)+COEF_XXXY*xA*xA*yB*zB*dEGx                 ! OK
        dAOY(6)=dAOY(6)+COEF_XXXY*(xA*xA*yB*zB*dEGy+xA*xA*zB*EG)
        dAOZ(6)=dAOZ(6)+COEF_XXXY*(xA*xA*yB*zB*dEGz+xA*xA*yB*EG)

! yA*yA*xB*xB*COEF_XXYY*EG 
        dAOX(7)=dAOX(7)+COEF_XXYY*(yA*yA*xB*xB*dEGx+two*yA*yA*xB*EG)
        dAOY(7)=dAOY(7)+COEF_XXYY*yA*yA*xB*xB*dEGy
        dAOZ(7)=dAOZ(7)+COEF_XXYY*yA*yA*xB*xB*dEGz

! yA*yA*yB*yB*COEF_XXYY*EG
        dAOX(8)=dAOX(8)+COEF_XXYY*yA*yA*yB*yB*dEGx
        dAOY(8)=dAOY(8)+COEF_XXYY*(yA*yA*yB*yB*dEGy+two*yA*yA*yB*EG)
        dAOZ(8)=dAOZ(8)+COEF_XXYY*yA*yA*yB*yB*dEGz

! yA*yA*zB*zB*COEF_XXYY*EG
        dAOX(9)=dAOX(9)+COEF_XXYY*yA*yA*zB*zB*dEGx
        dAOY(9)=dAOY(9)+COEF_XXYY*yA*yA*zB*zB*dEGy
        dAOZ(9)=dAOZ(9)+COEF_XXYY*(yA*yA*zB*zB*dEGz+two*yA*yA*zB*EG)

! yA*yA*xB*yB*COEF_XXXY*EG
        dAOX(10)=dAOX(10)+COEF_XXXY*(yA*yA*xB*yB*dEGx+yA*yA*yB*EG)
        dAOY(10)=dAOY(10)+COEF_XXXY*(yA*yA*xB*yB*dEGy+yA*yA*xB*EG)
        dAOZ(10)=dAOZ(10)+COEF_XXXY*yA*yA*xB*yB*dEGz

! yA*yA*xB*zB*COEF_XXXY*EG
        dAOX(11)=dAOX(11)+COEF_XXXY*(yA*yA*xB*zB*dEGx+yA*yA*zB*EG)
        dAOY(11)=dAOY(11)+COEF_XXXY*yA*yA*xB*zB*dEGy
        dAOZ(11)=dAOZ(11)+COEF_XXXY*(yA*yA*xB*zB*dEGz+yA*yA*xB*EG)

! yA*yA*yB*zB*COEF_XXXY*EG
        dAOX(12)=dAOX(12)+COEF_XXXY*yA*yA*yB*zB*dEGx
        dAOY(12)=dAOY(12)+COEF_XXXY*(yA*yA*yB*zB*dEGy+yA*yA*zB*EG)
        dAOZ(12)=dAOZ(12)+COEF_XXXY*(yA*yA*yB*zB*dEGz+yA*yA*yB*EG)

! zA*zA*xB*xB*COEF_XXYY*EG        
        dAOX(13)=dAOX(13)+COEF_XXYY*(zA*zA*xB*xB*dEGx+two*zA*zA*xB*EG)
        dAOY(13)=dAOY(13)+COEF_XXYY*zA*zA*xB*xB*dEGy
        dAOZ(13)=dAOZ(13)+COEF_XXYY*zA*zA*xB*xB*dEGz

! zA*zA*yB*yB*COEF_XXYY*EG      
        dAOX(14)=dAOX(14)+COEF_XXYY*zA*zA*yB*yB*dEGx
        dAOY(14)=dAOY(14)+COEF_XXYY*(zA*zA*yB*yB*dEGy+two*zA*zA*yB*EG)
        dAOZ(14)=dAOZ(14)+COEF_XXYY*zA*zA*yB*yB*dEGz

! zA*zA*zB*zB*COEF_XXYY*EG          
        dAOX(15)=dAOX(15)+COEF_XXYY*zA*zA*zB*zB*dEGx
        dAOY(15)=dAOY(15)+COEF_XXYY*zA*zA*zB*zB*dEGy
        dAOZ(15)=dAOZ(15)+COEF_XXYY*(zA*zA*zB*zB*dEGz+two*zA*zA*zB*EG)

! zA*zA*xB*yB*COEF_XXXY*EG              
        dAOX(16)=dAOX(16)+COEF_XXXY*(zA*zA*xB*yB*dEGx+zA*zA*yB*EG)
        dAOY(16)=dAOY(16)+COEF_XXXY*(zA*zA*xB*yB*dEGy+zA*zA*xB*EG)
        dAOZ(16)=dAOZ(16)+COEF_XXXY*zA*zA*xB*yB*dEGz

! zA*zA*xB*zB*COEF_XXXY*EG
        dAOX(17)=dAOX(17)+COEF_XXXY*(zA*zA*xB*zB*dEGx+zA*zA*zB*EG)
        dAOY(17)=dAOY(17)+COEF_XXXY*zA*zA*xB*zB*dEGy
        dAOZ(17)=dAOZ(17)+COEF_XXXY*(zA*zA*xB*zB*dEGz+zA*zA*xB*EG)

! zA*zA*yB*zB*COEF_XXXY*EG
        dAOX(18)=dAOX(18)+COEF_XXXY*zA*zA*yB*zB*dEGx
        dAOY(18)=dAOY(18)+COEF_XXXY*(zA*zA*yB*zB*dEGy+zA*zA*zB*EG)
        dAOZ(18)=dAOZ(18)+COEF_XXXY*(zA*zA*yB*zB*dEGz+zA*zA*yB*EG)

! xA*yA*xB*xB*COEF_XXXY*EG
        dAOX(19)=dAOX(19)+COEF_XXXY*(xA*yA*xB*xB*dEGx+two*xA*yA*xB*EG)
        dAOY(19)=dAOY(19)+COEF_XXXY*xA*yA*xB*xB*dEGy
        dAOZ(19)=dAOZ(19)+COEF_XXXY*xA*yA*xB*xB*dEGz

! xA*yA*yB*yB*COEF_XXXY*EG
        dAOX(20)=dAOX(20)+COEF_XXXY*xA*yA*yB*yB*dEGx
        dAOY(20)=dAOY(20)+COEF_XXXY*(xA*yA*yB*yB*dEGy+two*xA*yA*yB*EG)
        dAOZ(20)=dAOZ(20)+COEF_XXXY*xA*yA*yB*yB*dEGz

! xA*yA*zB*zB*COEF_XXXY*EG        
        dAOX(21)=dAOX(21)+COEF_XXXY*xA*yA*zB*zB*dEGx
        dAOY(21)=dAOY(21)+COEF_XXXY*xA*yA*zB*zB*dEGy
        dAOZ(21)=dAOZ(21)+COEF_XXXY*(xA*yA*zB*zB*dEGz+two*xA*yA*zB*EG)

! xA*yA*xB*yB*COEF_XYXY*EG      
        dAOX(22)=dAOX(22)+COEF_XYXY*(xA*yA*xB*yB*dEGx+xA*yA*yB*EG)
        dAOY(22)=dAOY(22)+COEF_XYXY*(xA*yA*xB*yB*dEGy+xA*yA*xB*EG)
        dAOZ(22)=dAOZ(22)+COEF_XYXY*xA*yA*xB*yB*dEGz

! xA*yA*xB*zB*COEF_XYXY*EG           
        dAOX(23)=dAOX(23)+COEF_XYXY*(xA*yA*xB*zB*dEGx+xA*yA*zB*EG)
        dAOY(23)=dAOY(23)+COEF_XYXY*xA*yA*xB*zB*dEGy
        dAOZ(23)=dAOZ(23)+COEF_XYXY*(xA*yA*xB*zB*dEGz+xA*yA*xB*EG)

! xA*yA*yB*zB*COEF_XYXY*EG              
        dAOX(24)=dAOX(24)+COEF_XYXY*xA*yA*yB*zB*dEGx
        dAOY(24)=dAOY(24)+COEF_XYXY*(xA*yA*yB*zB*dEGy+xA*yA*zB*EG)
        dAOZ(24)=dAOZ(24)+COEF_XYXY*(xA*yA*yB*zB*dEGz+xA*yA*yB*EG)

! xA*zA*xB*xB*COEF_XXXY*EG
        dAOX(25)=dAOX(25)+COEF_XXXY*(xA*zA*xB*xB*dEGx+two*xA*zA*xB*EG)
        dAOY(25)=dAOY(25)+COEF_XXXY*xA*zA*xB*xB*dEGy
        dAOZ(25)=dAOZ(25)+COEF_XXXY*xA*zA*xB*xB*dEGz

! xA*zA*yB*yB*COEF_XXXY*EG
        dAOX(26)=dAOX(26)+COEF_XXXY*xA*zA*yB*yB*dEGx
        dAOY(26)=dAOY(26)+COEF_XXXY*(xA*zA*yB*yB*dEGy+two*xA*zA*yB*EG)
        dAOZ(26)=dAOZ(26)+COEF_XXXY*xA*zA*yB*yB*dEGz

! xA*zA*zB*zB*COEF_XXXY*EG
        dAOX(27)=dAOX(27)+COEF_XXXY*xA*zA*zB*zB*dEGx
        dAOY(27)=dAOY(27)+COEF_XXXY*xA*zA*zB*zB*dEGy
        dAOZ(27)=dAOZ(27)+COEF_XXXY*(xA*zA*zB*zB*dEGz+two*xA*zA*zB*EG)

! xA*zA*xB*yB*COEF_XYXY*EG
        dAOX(28)=dAOX(28)+COEF_XYXY*(xA*zA*xB*yB*dEGx+xA*zA*yB*EG)
        dAOY(28)=dAOY(28)+COEF_XYXY*(xA*zA*xB*yB*dEGy+xA*zA*xB*EG)
        dAOZ(28)=dAOZ(28)+COEF_XYXY*xA*zA*xB*yB*dEGz

! xA*zA*xB*zB*COEF_XYXY*EG
        dAOX(29)=dAOX(29)+COEF_XYXY*(xA*zA*xB*zB*dEGx+xA*zA*zB*EG)
        dAOY(29)=dAOY(29)+COEF_XYXY*xA*zA*xB*zB*dEGy
        dAOZ(29)=dAOZ(29)+COEF_XYXY*(xA*zA*xB*zB*dEGz+xA*zA*xB*EG)

! xA*zA*yB*zB*COEF_XYXY*EG
        dAOX(30)=dAOX(30)+COEF_XYXY*xA*zA*yB*zB*dEGx
        dAOY(30)=dAOY(30)+COEF_XYXY*(xA*zA*yB*zB*dEGy+xA*zA*zB*EG)
        dAOZ(30)=dAOZ(30)+COEF_XYXY*(xA*zA*yB*zB*dEGz+xA*zA*yB*EG)

! yA*zA*xB*xB*COEF_XXXY*EG
        dAOX(31)=dAOX(31)+COEF_XXXY*(yA*zA*xB*xB*dEGx+two*yA*zA*xB*EG)
        dAOY(31)=dAOY(31)+COEF_XXXY*yA*zA*xB*xB*dEGy
        dAOZ(31)=dAOZ(31)+COEF_XXXY*yA*zA*xB*xB*dEGz

! yA*zA*yB*yB*COEF_XXXY*EG
        dAOX(32)=dAOX(32)+COEF_XXXY*yA*zA*yB*yB*dEGx
        dAOY(32)=dAOY(32)+COEF_XXXY*(yA*zA*yB*yB*dEGy+two*yA*zA*yB*EG)
        dAOZ(32)=dAOZ(32)+COEF_XXXY*yA*zA*yB*yB*dEGz

! yA*zA*zB*zB*COEF_XXXY*EG
        dAOX(33)=dAOX(33)+COEF_XXXY*yA*zA*zB*zB*dEGx
        dAOY(33)=dAOY(33)+COEF_XXXY*yA*zA*zB*zB*dEGy
        dAOZ(33)=dAOZ(33)+COEF_XXXY*(yA*zA*zB*zB*dEGz+two*yA*zA*zB*EG)

! yA*zA*xB*yB*COEF_XYXY*EG
        dAOX(34)=dAOX(34)+COEF_XYXY*(yA*zA*xB*yB*dEGx+yA*zA*yB*EG)
        dAOY(34)=dAOY(34)+COEF_XYXY*(yA*zA*xB*yB*dEGy+yA*zA*xB*EG)
        dAOZ(34)=dAOZ(34)+COEF_XYXY*yA*zA*xB*yB*dEGz

! yA*zA*xB*zB*COEF_XYXY*EG
        dAOX(35)=dAOX(35)+COEF_XYXY*(yA*zA*xB*zB*dEGx+yA*zA*zB*EG)
        dAOY(35)=dAOY(35)+COEF_XYXY*yA*zA*xB*zB*dEGy
        dAOZ(35)=dAOZ(35)+COEF_XYXY*(yA*zA*xB*zB*dEGz+yA*zA*xB*EG)

! yA*zA*yB*zB*COEF_XYXY*EG
        dAOX(36)=dAOX(36)+COEF_XYXY*yA*zA*yB*zB*dEGx
        dAOY(36)=dAOY(36)+COEF_XYXY*(yA*zA*yB*zB*dEGy+yA*zA*zB*EG)
        dAOZ(36)=dAOZ(36)+COEF_XYXY*(yA*zA*yB*zB*dEGz+yA*zA*yB*EG)
      end do ! Jgauss
      end do ! Igauss 
      
        IF(LIatmshl)then
          INDEX=0
          do I=1,Irange
          do J=1,Jrange
            INDEX=INDEX+1
            IF(I.GE.J)then  ! Corrected August 1,2003 had (J.ge.I)
              Iao=AOI+I
              Jao=AOJ+J
              IJao=Iao*(Iao-1)/2+Jao
              dAOprodX(IJao)=dAOX(INDEX)
              dAOprodY(IJao)=dAOY(INDEX)
              dAOprodZ(IJao)=dAOZ(INDEX)
            end if
          end do ! J   
          end do ! I
            else
              INDEX=0
          do I=1,Irange
          do J=1,Jrange
            INDEX=INDEX+1
            Iao=AOI+I
            Jao=AOJ+J
            IJao=Iao*(Iao-1)/2+Jao
            dAOprodX(IJao)=dAOX(INDEX)
            dAOprodY(IJao)=dAOY(INDEX)
            dAOprodZ(IJao)=dAOZ(INDEX)
          end do ! J
          end do ! I
        end if ! LIatmshl

      end do ! Jatmshl
      end do ! Iatmshl

      return
      end subroutine DD_dproducts
      end subroutine Velocity_Gaussian_prod

