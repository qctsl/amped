      MODULE RAM_RR
!**************************************************************************************************
!     Date last modified: May 22, 2024                                                            *
!     Author: JWH                                                                                 *
!     Description:  Intermediates for 18-term/20-term recurrence relation for RAM/RAM density     *
!                   integrals.
!**************************************************************************************************
      implicit none
!
! RR coefficients
      double precision, dimension(3) :: RRa1, RRa2, RRa3, RRa4
      double precision, dimension(3) :: RRb1, RRb2, RRb3, RRb4
      double precision, dimension(3) :: RRc1, RRc2, RRc3, RRc4
      double precision, dimension(3) :: RRd1, RRd2, RRd3, RRd4
      double precision :: RRa5, RRa6, RRa7, RRa8, RRa9, RRa10, RRa11, RRa12
      double precision :: RRa13, RRa14, RRa15, RRa16, RRa17, RRa18, RRa19, RRa20
      double precision :: RRb5, RRb6, RRb7, RRb8, RRb9, RRb10, RRb11, RRb12
      double precision :: RRb13, RRb14, RRb15, RRb16, RRb17, RRb18, RRb19, RRb20
      double precision :: RRc5, RRc6, RRc7, RRc8, RRc9, RRc10, RRc11, RRc12
      double precision :: RRc13, RRc14, RRc15, RRc16, RRc17, RRc18, RRc19, RRc20
      double precision :: RRd5, RRd6, RRd7, RRd8, RRd9, RRd10, RRd11, RRd12
      double precision :: RRd13, RRd14, RRd15, RRd16, RRd17, RRd18, RRd19, RRd20
!
! Integrals
      double precision, allocatable, dimension(:) :: G
      double precision, allocatable, dimension(:,:) :: Int1000, Int1100, Int1010, Int0110, Int1110
      double precision, allocatable, dimension(:,:) :: Int0101, Int0111, Int1111, Int1101
      double precision, allocatable, dimension(:,:) :: Int0100, Int2000, Int0200, Int0020, Int2100
      double precision, allocatable, dimension(:,:) :: Int2010, Int1200, Int2200, Int2020, Int2210
      double precision, allocatable, dimension(:,:) :: Int2120, Int2110, Int1210, Int1120, Int2211
      double precision, allocatable, dimension(:,:) :: Int2121, Int1220, Int2002, Int1102, Int2001
      double precision, allocatable, dimension(:,:) :: Int1020, Int2102, Int2101, Int2220, Int2221
      double precision, allocatable, dimension(:,:) :: Int0210, Int1002, Int1001, Int1221, Int0002
      double precision, allocatable, dimension(:,:) :: Int0010, Int0001, Int0220, Int0000
      double precision, allocatable, dimension(:,:) :: Int0201, Int2011, Int0211, Int1201, Int2111
      double precision, allocatable, dimension(:,:) :: Int1211, Int0202, Int2201, Int2012, Int2112
      double precision, allocatable, dimension(:,:) :: Int1212, Int2202, Int0102, Int1202, Int0120
      double precision, allocatable, dimension(:,:) :: Int2212, Int2222 
!
! Others
      integer :: Lvec_d(6,3), xyz(3), v_1i(3), imat(3,3)
      double precision, parameter :: sq3 = dsqrt(3.0D0)
      double precision, parameter :: pi  = 3.14159265358979323846264338327950288419716939937510D0
!
      end MODULE RAM_RR
