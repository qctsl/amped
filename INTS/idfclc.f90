      subroutine IDFCLC
!***********************************************************************
!     Date last modified: February 8, 2006                 Version 2.0 *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!     Two-electron integral package for SPDF functions, using the      *
!     method of Rys polynomials for the following functions:           *
!                                                                      *
!      1      S                                                        *
!      2      X                                                        *
!      3      Y                                                        *
!      4      Z                                                        *
!      5      X**2      D(0)   3*Z**2-R**2                             *
!      6      Y**2      D(1,+) X*Z                                     *
!      7      Z**2      D(1,-) Y*Z                                     *
!      8      X*Y       D(2,+) X**2-Y**2                               *
!      9      X*Z       D(2,-) X*Y                                     *
!     10      Y*Z                                                      *
!     11      X**3                           F(0)   Z*(5*Z**2-3*R**2)  *
!     12      Y**3                           F(1,+) X*(5*Z**2-R**2)    *
!     13      Z**3                           F(1,-) Y*(5*Z**2-R**2)    *
!     14      X*Y**2                         F(2,+) Z*(X**2-Y**2)      *
!     15      X**2*Y                         F(2,-) Z*(X*Y)            *
!     16      X**2*Z                         F(3,+) X*(X**2-3*Y**2)    *
!     17      X*Z**2                         F(3,-) Y*(3*X**2-Y**2)    *
!     18      Y*Z**2                                                   *
!     19      Y**2*Z                                                   *
!     20      X*Y*Z                                                    *
!***********************************************************************
! Modules:
      USE program_constants
      USE type_basis_set
      USE mod_idfclc

      implicit none
!
! Local scalars:
      integer :: ICASE,IERROR,LendL,Neshlend
      integer :: Icc,Jcc,Kcc,Lcc
      integer :: Iao,Jao,Kao,Lao
      integer :: Igauss,Jgauss,Kgauss,Lgauss
!     integer :: CCA,CCB,CCC,CCD
      integer :: DEFCASE
      integer :: GET_maxint
!
! Local functions:
!
! Begin:
      call PRG_manager ('enter', 'IDFCLC', 'UTILITY')
!
      maxint=GET_maxint ()
      allocate (IJKLS(MAXINT), IKJLS(MAXINT), ILJKS(MAXINT), TQ(MAXint), STAT=IERROR)
      allocate (TQprim(10000), STAT=IERROR)
      allocate (SHRABSQ(Natoms, Natoms))
      call I2E_RABSQ (SHRABSQ)

      if(IERROR.ne.0)then
        write(UNIout,'(a)')'ERROR> IDFCLC: allocate failure'
        write(UNIout,'(a,i10)')'ERROR> IDFCLC: MAXINT= ',MAXINT
        stop 'ERROR> IDFCLC: allocate failure'
      end if
!
      PIconst=(PI_VAL+PI_VAL)*PI_VAL*DSQRT(PI_VAL) ! Initialize program_constants.

      NESHLend=Basis%Natmshls
      IF(LI2ESP)then
        NESHLend=Basis%NFDShells
      end if
!
! Commence loops over shells.
      FIRST_loop=.true.
! Loop over Ishell.
      do Ishell=Ishell_start,Basis%Nshells
      LAMAX=Basis%shell(Ishell)%Xtype+1
      Istart=Basis%shell(Ishell)%Xstart
      Iend=Basis%shell(Ishell)%XEND
      Irange=Iend-Istart+1
      IGBGN=Basis%shell(Ishell)%EXPBGN
      IGEND=Basis%shell(Ishell)%EXPEND
      Ifrst=Basis%shell(Ishell)%frstSHL

      Icc=0
      CCA=BASIS%ccpntr(Ishell)
      NgaussA=0
      do Igauss=IGBGN,IGEND
      NgaussA=NgaussA+1
      GexpA(NgaussA)=BASIS%gaussian(Igauss)%exp
      do Iao=1,Irange
        Icc=Icc+1
        GccA(Icc)=BASIS%ccbyao(CCA+Iao-1)
      end do
      CCA=CCA+Irange
      end do

! Loop over Jshell.
      Jshell_start=1
      if(FIRST_LOOP)Jshell_start=Jshell_save
      do Jshell=Jshell_start,Ishell
      LBMAX0=Basis%shell(Jshell)%Xtype+1
      Jstart0=Basis%shell(Jshell)%Xstart
      Jend0=Basis%shell(Jshell)%XEND
      Jrange=Jend0-Jstart0+1
      JGBGN=Basis%shell(Jshell)%EXPBGN
      JGEND=Basis%shell(Jshell)%EXPEND
      Jcc=0
      CCB=BASIS%ccpntr(Jshell)
      NgaussB0=0
      do Jgauss=JGBGN,JGEND
        NgaussB0=NgaussB0+1
        GexpB0(NgaussB0)=BASIS%gaussian(Jgauss)%exp
      do Jao=1,Jrange
        Jcc=Jcc+1
        GccB0(Jcc)=BASIS%ccbyao(CCB+Jao-1)
      end do
      CCB=CCB+Jrange
      end do

! Loop over Kshell.
      Kshell_start=1
      if(FIRST_LOOP)Kshell_start=Kshell_save
      do Kshell=Kshell_start,Jshell
      LCMAX0=basis%shell(Kshell)%Xtype+1
      Kstart0=basis%shell(Kshell)%Xstart
      Kend0=basis%shell(Kshell)%XEND
      Krange=Kend0-Kstart0+1
      KGBGN=basis%shell(Kshell)%EXPBGN
      KGEND= basis%shell(Kshell)%EXPEND
      Kcc=0
      CCC=BASIS%ccpntr(Kshell)
      NgaussC0=0
      do Kgauss=KGBGN,KGEND
        NgaussC0=NgaussC0+1
        GexpC0(NgaussC0)=BASIS%gaussian(Kgauss)%exp
      do Kao=1,Krange
        Kcc=Kcc+1
        GccC0(Kcc)=BASIS%ccbyao(CCC+Kao-1)
      end do
      CCC=CCC+Krange
      end do

! Loop over Lshell.
      LendL=Kshell
      if(LI2ESP)LendL=min0(Neshlend, Kshell)
      Lshell_start=1
      if(FIRST_LOOP)Lshell_start=Lshell_save
      do Lshell=Lshell_start,LendL
      LDMAX0=basis%shell(Lshell)%Xtype+1
      Lstart0=basis%shell(Lshell)%Xstart
      Lend0=basis%shell(Lshell)%XEND
      Lrange=Lend0-Lstart0+1
      LGBGN=basis%shell(Lshell)%EXPBGN
      LGEND= basis%shell(Lshell)%EXPEND
      Lcc=0
      CCD=BASIS%ccpntr(Lshell)
      NgaussD0=0
      do Lgauss=LGBGN,LGEND
        NgaussD0=NgaussD0+1
        GexpD0(NgaussD0)=BASIS%gaussian(Lgauss)%exp
      do Lao=1,Lrange
        Lcc=Lcc+1
        GccD0(Lcc)=BASIS%ccbyao(CCD+Lao-1)
      end do
      CCD=CCD+Lrange
      end do
!
      LENTQ=Irange*Jrange*Krange*Lrange
      NZERO=(LAMAX+LBMAX0+LCMAX0+LDMAX0-4)/2+1

      EABPmin0=Basis%minEABP(Ishell,Jshell)
      ECDPmin0=Basis%minEABP(Kshell,Lshell)
      EADPmin=Basis%minEABP(Ishell,Lshell)
      EBCPmin=Basis%minEABP(Jshell,Kshell)
      EACPmin=Basis%minEABP(Ishell,Kshell)
      EBDPmin=Basis%minEABP(Jshell,Lshell)

! NOTE: These must be re-initialized everytime!!
      Ilast= Basis%shell(Ishell)%lastSHL

      ICASE=DEFCASE(LAMAX, LBMAX0, LCMAX0, LDMAX0)
!
      select case (ICASE)
      case (1)
      call I2ER_SSSS
      case (2)
      call I2ER_SSSP
      case (3)
      call I2ER_SSPP
      case (4)
      call I2ER_SPPP
      case (5)
      call I2ER_PPPP
      case (6)
      call I2ER_SSSD
      case (10)
      call I2ER_SSDD
      case (13)
      call I2ER_SDDD
      case (15)
      call I2ER_DDDD
      case (7,8,9,11,12,14,16:)
      call I2ER_SPDF
      end select
!
      FIRST_loop=.false.
      end do ! Lshell
      end do ! Kshell
      end do ! Jshell
      end do ! Ishell
! End of loop over locations.
!
      deallocate (IJKLS, IKJLS, ILJKS, STAT=IERROR)
      deallocate (TQprim, TQ, STAT=IERROR)
      deallocate (SHRABSQ, STAT=IERROR)
      if(IERROR.ne.0)then
        write(UNIout,'(a)')'ERROR> IDFCLC: deallocate failure'
        stop 'ERROR> IDFCLC: deallocate failure'
      end if
!
! End of routine IDFCLC
      call PRG_manager ('exit', 'IDFCLC', 'UTILITY')
      return
end subroutine IDFCLC
!CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
     integer function DEFCASE (LA, LB, LC, LD)
!***********************************************************************
!     Date last modified: December 11, 1996                            *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!     Fortran function that determines the type of integral, i.e.      *
!     ssss, sssp, sspp, sppp, pppp, ..., ffff                          *
!***********************************************************************
      implicit none
!
! Input scalars:
      integer LA,LB,LC,LD
!
! Begin:
!
      IF(LD.EQ.1)then
        DEFCASE=1
      else if(LD.EQ.2)then
        IF(LC.EQ.1)then
          DEFCASE=2
        else if(LC.EQ.2)then
          IF(LB.EQ.1)then
            DEFCASE=3
          else if(LB.EQ.2)then
            IF(LA.EQ.1)then
              DEFCASE=4
            else if(LA.EQ.2)then
              DEFCASE=5
            end if ! LA
          end if ! LB
        end if ! LC

      else if(LD.EQ.3)then
        IF(LC.EQ.1)then
          DEFCASE=6
        else if(LC.EQ.2)then
          IF(LB.EQ.1)then
            DEFCASE=7
          else if(LB.EQ.2)then
            IF(LA.EQ.1)then
              DEFCASE=8
            else if(LA.EQ.2)then
              DEFCASE=9
            end if ! LA
          end if ! LB
        else if(LC.EQ.3)then
          IF(LB.EQ.1)then
            DEFCASE=10
          else if(LB.EQ.2)then
            IF(LA.EQ.1)then
              DEFCASE=11
            else if(LA.EQ.2)then
              DEFCASE=12
            end if ! LA
          else if(LB.EQ.3)then
            IF(LA.EQ.1)then
              DEFCASE=13
            else if(LA.EQ.2)then
              DEFCASE=14
            else if(LA.EQ.3)then
              DEFCASE=15
            end if ! LA
          end if ! LB
        end if ! LC

      else if(LD.EQ.4)then
        IF(LC.EQ.1)then
          DEFCASE=16
        else if(LC.EQ.2)then
          IF(LB.EQ.1)then
            DEFCASE=17
          else if(LB.EQ.2)then
            IF(LA.EQ.1)then
              DEFCASE=18
            else if(LA.EQ.2)then
              DEFCASE=19
            end if ! LA
          end if ! LB
        else if(LC.EQ.3)then
          IF(LB.EQ.1)then
            DEFCASE=20
          else if(LB.EQ.2)then
            IF(LA.EQ.1)then
              DEFCASE=21
            else if(LA.EQ.2)then
              DEFCASE=22
            end if ! LA
          else if(LB.EQ.3)then
            IF(LA.EQ.1)then
              DEFCASE=23
            else if(LA.EQ.2)then
              DEFCASE=24
            else if(LA.EQ.3)then
              DEFCASE=25
            end if ! LA
          end if ! LB
        else if(LC.EQ.4)then
          IF(LB.EQ.1)then
            DEFCASE=26
          else if(LB.EQ.2)then
            IF(LA.EQ.1)then
              DEFCASE=27
            else if(LA.EQ.2)then
              DEFCASE=28
            end if ! LA
          else if(LB.EQ.3)then
            IF(LA.EQ.1)then
              DEFCASE=29
            else if(LA.EQ.2)then
              DEFCASE=30
            else if(LA.EQ.3)then
              DEFCASE=31
            end if ! LA
          else if(LB.EQ.4)then
            IF(LA.EQ.1)then
              DEFCASE=32
            else if(LA.EQ.2)then
              DEFCASE=33
            else if(LA.EQ.3)then
              DEFCASE=34
            else if(LA.EQ.4)then
              DEFCASE=35
            end if ! LA
          end if ! LB
        end if ! LC
      end if ! LD
      return
      end function DEFCASE
      subroutine I2E_PFockOP
!***********************************************************************
!     Date last modified: February 8, 2006                 Version 2.0 *
!     Author: R.A. Poirier                                             *
!     Description: See IDFCLC Uses mixed basis sets                    *
!***********************************************************************
! Modules:
      USE program_constants
      USE type_basis_set
      USE mod_idfclc

      implicit none
!
! Local scalars:
      integer ICASE,IERROR,LendL,Neshlend
      integer :: DEFCASE
!
! Local functions:
!
! Begin:
      call PRG_manager ('enter', 'I2E_PFockOP', 'UTILITY')
!
      allocate (IJKLS(MAXINT), IKJLS(MAXINT), ILJKS(MAXINT), TQ(MAXint), STAT=IERROR)
      allocate (TQprim(10000), STAT=IERROR)

      if(IERROR.ne.0)then
        write(UNIout,'(a)')'ERROR> I2E_PFockOP: allocate failure'
        write(UNIout,'(a,i10)')'ERROR> I2E_PFockOP: MAXINT= ',MAXINT
        stop 'ERROR> I2E_PFockOP: allocate failure'
      end if
!
      PIconst=(PI_VAL+PI_VAL)*PI_VAL*DSQRT(PI_VAL) ! Initialize program_constants.

      NESHLend=Basis%Natmshls
      IF(LI2ESP)then
        NESHLend=Basis%NFDShells
      end if
!
! Commence loops over shells.
      FIRST_loop=.true.
! Loop over Ishell.
      do Ishell=Ishell_start,Basis%Nshells
      LAMAX=Basis%shell(Ishell)%Xtype+1
      Istart=Basis%shell(Ishell)%Xstart
      Iend=Basis%shell(Ishell)%XEND
      Irange=Iend-Istart+1
      IGBGN=Basis%shell(Ishell)%EXPBGN
      IGEND=Basis%shell(Ishell)%EXPEND
      Ifrst=Basis%shell(Ishell)%frstSHL
! Loop over Jshell.
      Jshell_start=1
      if(FIRST_LOOP)Jshell_start=Jshell_save
      do Jshell=Jshell_start,Ishell
! Change basis set
      Basis=>Guess_basis
! Loop over Kshell.
      Kshell_start=1
      if(FIRST_LOOP)Kshell_start=Kshell_save
      do Kshell=Kshell_start,Basis%Nshells
! Loop over Lshell.
      Lshell_start=1
      if(FIRST_LOOP)Lshell_start=Lshell_save
      do Lshell=Lshell_start,Kshell
!
! NOTE: These must be re-initialized everytime!!
      Ilast= Basis%shell(Ishell)%lastSHL

      LBMAX=Basis%shell(Jshell)%Xtype+1
      LCMAX=basis%shell(Kshell)%Xtype+1
      LDMAX=basis%shell(Lshell)%Xtype+1

      if(LBmax.gt.LCmax)cycle
      ICASE=DEFCASE(LAMAX, LBMAX, LCMAX, LDMAX)
!
      select case (ICASE)
      case (1)
      call I2ER_SSSS
      case (2)
      call I2ER_SSSP
      case (3)
      call I2ER_SSPP
      case (4)
      call I2ER_SPPP
      case (5)
      call I2ER_PPPP
      case (6)
      call I2ER_SSSD
      case (10)
      call I2ER_SSDD
      case (13)
      call I2ER_SDDD
      case (15)
      call I2ER_DDDD
      case (7,8,9,11,12,14,16:)
      call I2ER_SPDF
      end select
!
      FIRST_loop=.false.
      end do ! Lshell
      end do ! Kshell
      Basis=>Current_basis
      end do ! Jshell
      end do ! Ishell
! End of loop over locations.
!
      deallocate (IJKLS, IKJLS, ILJKS, STAT=IERROR)
      deallocate (TQprim, TQ, STAT=IERROR)
      if(IERROR.ne.0)then
        write(UNIout,'(a)')'ERROR> I2E_PFockOP: deallocate failure'
        stop 'ERROR> I2E_PFockOP: deallocate failure'
      end if
!
! End of routine I2E_PFockOP
      call PRG_manager ('exit', 'I2E_PFockOP', 'UTILITY')
      return
end subroutine I2E_PFockOP
