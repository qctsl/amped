      subroutine Kinetic_Atomic
!***********************************************************************
!     Date last modified: November 6, 1996                 Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!***********************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE type_basis_set
      USE QM_defaults
      USE QM_objects
      USE INT_objects
      USE matrix_print

      implicit none
!
! Local scalars:
      integer :: Ishell,Jshell,Ifrst,Jfrst,Ilast,Jlast
      integer :: Istart,Jstart,Iend,Jend
      integer :: Iatmshl,Jatmshl
      integer :: LAMAX,LBMAX
      integer :: Iatom,Jatom
      integer :: Irange,Jrange
      integer :: Igauss,Jgauss
      integer :: Iaos,JaoS
      integer :: IGBGN,JGBGN,IGend,JGend
      integer :: LPMAX,I,IA,IJX,IJY,IJZ,INTC,IX,IXP,IY,IYP,IZ,IZP,J,JX,JY,JZ,LIM1DS
      integer :: LENTQ
      double precision ABX,ABY,ABZ,ARABSQ,ARG,AS,ASXA,ASYA,ASZA,BS,COEF,CUT1,EP,EPI,EPIO2,PCX,PCY,PCZ,PEXP, &
                       PX,PY,PZ,RPCSQ,SYZ,TEMPK,TEMPS,TK,TWOASQ,TWOP,TWOPI,TWOPT2,XIIM1, &
                       XK,YIIM1,ZIIM1
      double precision :: XA,XB,XC,YA,YB,YC,ZA,ZB,RABSQ
      double precision :: TRACLO
      double precision :: ETtotal
!
! Local arrays:
      integer INDIX(20),INDIY(20),INDIZ(20),INDJX(20),INDJY(20),INDJZ(20)
      double precision :: TP(7),WP(7)
      double precision :: A(45),CA(20),CB(20),EEPT(100),SS(100),SX(32),SY(32),SZ(32)
!
      parameter (TWOPI=TWO*PI_VAL,CUT1=-75.0D0)
!
      DATA INDJX/1,2,1,1,3,1,1,2,2,1,4,1,1,2,3,3,2,1,1,2/,INDJY/1,1,2,1,1,3,1,2,1,2,1,4,1,3,2,1,1,2,3,2/, &
           INDJZ/1,1,1,2,1,1,3,1,2,2,1,1,4,1,1,2,3,3,2,2/
!
! Begin:
      call PRG_manager ('enter', 'KINETIC_ATOMIC', 'ENERGY%KINETIC_ATOMIC')
!
      call GET_object ('QM', 'CMO', Wavefunction)
! Object:
      if(.not.allocated(Tint))then
        allocate (Tint((Basis%Nbasis*(Basis%Nbasis+1))/2))
      else
        if(Basis%Nbasis.ne.size(HCORE,1))then
          deallocate (Tint)
          allocate (Tint((Basis%Nbasis*(Basis%Nbasis+1))/2))
        end if
      end if
!
      call RYSSET
!
! Fill INDIX.
      do I=1,20
        INDIX(I)=4*(INDJX(I)-1)
        INDIY(I)=4*(INDJY(I)-1)
        INDIZ(I)=4*(INDJZ(I)-1)
      end do ! I
!
! Loop over shells.
! Loop over Ishell.
      do Ishell=1,Basis%Nshells
      LAMAX=Basis%shell(Ishell)%Xtype+1
      Istart=Basis%shell(Ishell)%Xstart
      Iend=Basis%shell(Ishell)%XEND
      Irange=Iend-Istart+1
      IGBGN=Basis%shell(Ishell)%EXPBGN
      IGEND=Basis%shell(Ishell)%EXPEND
      Ifrst=Basis%shell(Ishell)%frstSHL
!
! Loop over Jshell.
      do Jshell=1,Ishell
      LBMAX=Basis%shell(Jshell)%Xtype+1
      Jstart=Basis%shell(Jshell)%Xstart
      Jend=Basis%shell(Jshell)%XEND
      Jrange=Jend-Jstart+1
      JGBGN=Basis%shell(Jshell)%EXPBGN
      JGEND=Basis%shell(Jshell)%EXPEND
      Jfrst=Basis%shell(Jshell)%frstSHL
!
      Ilast= Basis%shell(Ishell)%lastSHL
      Jlast= Basis%shell(Jshell)%lastSHL
!
! Loop over Iatmshl
      do Iatmshl=Ifrst,Ilast
      XA=CARTESIAN(Basis%atmshl(Iatmshl)%ATMLST)%X
      YA=CARTESIAN(Basis%atmshl(Iatmshl)%ATMLST)%Y
      ZA=CARTESIAN(Basis%atmshl(Iatmshl)%ATMLST)%Z
      Iaos=Basis%atmshl(Iatmshl)%frstAO-1
!
! Loop over Jatmshl
      IF(Ishell.EQ.Jshell)Jlast=Iatmshl
      do Jatmshl=Jfrst,Jlast
      XB=CARTESIAN(Basis%atmshl(Jatmshl)%ATMLST)%X
      YB=CARTESIAN(Basis%atmshl(Jatmshl)%ATMLST)%Y
      ZB=CARTESIAN(Basis%atmshl(Jatmshl)%ATMLST)%Z
      Jaos=Basis%atmshl(Jatmshl)%frstAO-1
!
      LPMAX=LAMAX+LBMAX-1
      LIM1DS=(LPMAX+3)/2
      LENTQ=Irange*Jrange
      ABX=XB-XA
      ABY=YB-YA
      ABZ=ZB-ZA
      RABSQ=ABX*ABX+ABY*ABY+ABZ*ABZ
!
      EEPT(1:LENTQ)=ZERO
!
! Loop over primitive Gaussians.
      do Igauss=IGBGN,IGEND
      AS=Basis%gaussian(Igauss)%exp
      TWOASQ=TWO*AS*AS
      ASXA=AS*XA
      ASYA=AS*YA
      ASZA=AS*ZA
      ARABSQ=AS*RABSQ
      call FILLCC (LAMAX, Basis%gaussian(Igauss)%CONTRC, CA)

      do Jgauss=JGBGN,JGEND
      BS=Basis%gaussian(Jgauss)%exp
      call FILLCC (LBMAX, Basis%gaussian(Jgauss)%CONTRC, CB)
      EP=AS+BS
      EPI=ONE/EP
      EPIO2=PT5*EPI
      TWOP=EP+EP
      ARG=-BS*ARABSQ*EPI
      PEXP=ZERO
      IF(ARG.GT.CUT1)PEXP=DEXP(ARG)
      PX=(ASXA+BS*XB)*EPI
      PY=(ASYA+BS*YB)*EPI
      PZ=(ASZA+BS*ZB)*EPI
!
! Begin loop over atomic orbitals for kinetic energy integrals.
      INTC=0
      do I=Istart,Iend
      IX=INDIX(I)
      IY=INDIY(I)
      IZ=INDIZ(I)
      IXP=INDJX(I)
      IYP=INDJY(I)
      IZP=INDJZ(I)
      XK=DBLE(2*(IXP+IYP+IZP)-3)*AS
      XIIM1=DBLE((IXP-1)*(IXP-2))
      YIIM1=DBLE((IYP-1)*(IYP-2))
      ZIIM1=DBLE((IZP-1)*(IZP-2))
      do J=Jstart,Jend
      JX=INDJX(J)
      JY=INDJY(J)
      JZ=INDJZ(J)
      IJX=IX+JX
      IJY=IY+JY
      IJZ=IZ+JZ
      INTC=INTC+1
      COEF=CA(I)*CB(J)
      SYZ=SY(IJY)*SZ(IJZ)
      TEMPS=SX(IJX)*SYZ
      SS(INTC)=SS(INTC)+TEMPS*COEF
      TEMPK=TEMPS*XK
      TEMPK=TEMPK-TWOASQ*(SX(IJX+8)*SYZ+SX(IJX)*(SY(IJY+8)*SZ(IJZ)+SY(IJY)*SZ(IJZ+8)))
      TK=ZERO
      IF(IXP.GT.2)TK=XIIM1*SX(IJX-8)*SYZ
      IF(IYP.GT.2)TK=TK+YIIM1*SX(IJX)*SY(IJY-8)*SZ(IJZ)
      IF(IZP.GT.2)TK=TK+ZIIM1*SX(IJX)*SY(IJY)*SZ(IJZ-8)
      TEMPK=TEMPK-PT5*TK
      EEPT(INTC)=EEPT(INTC)+TEMPK*COEF     ! new
      end do ! J
      end do ! I
!
      end do ! Jgauss
      end do ! Igauss
! End of loop over Gaussians.
!
! FILMAT takes the integrals in EEPT and stores them in their proper places
      call FILMAT (EEPT, Tint, MATlen, Iend, Jend, &
                         Iatmshl, Jatmshl, Irange, Jrange, Iaos, JaoS)
!
      end do ! Jatmshl
      end do ! Iatmshl
      end do ! Jshell
      end do ! Ishell
!
! End of loop over shells.
      ETtotal = TRACLO (Tint, PM0, Nbasis, MATlen)
      write(UNIout,*)'Total Kinetic Energy: ',ETtotal
!        write(UNIout,*)'KINETIC (T)'
!        call PRT_matrix (TINT, MATlen, Basis%Nbasis)
!
! End of routine Kinetic_Atomic
      call PRG_manager ('exit', 'KINETIC_ATOMIC', 'ENERGY%KINETIC_ATOMIC')
      return
      end subroutine Kinetic_Atomic
