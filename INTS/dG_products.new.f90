      subroutine dG_products (rx, ry, rz, dAOprodX, dAOprodY, dAOprodZ, MATlen)
!****************************************************************************************
!     Date last modified: May 7, 2017 by Ibrahim Awad                                   *
!     Authors: Ahmad and R.A. Poirier                                                   *
!     Description: Compute d/dx, d/dy and d/dz of the product of two Gaussians          *
!                                                                                       *
!****************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE type_basis_set ! contains AOprod and dAOprodX,Y,Z
      USE matrix_print
      USE QM_defaults

      implicit none
!
! Input scalars:
      integer :: MATlen
      double precision :: rx,ry,rz
      double precision :: dAOprodX(MATlen),dAOprodY(MATlen),dAOprodZ(MATlen)
!
! Local scalars:
      integer :: Ishell,Jshell,Ifrst,Jfrst,Ilast,Jlast
      integer :: Istart,Jstart,Iend,Jend,AONUM
      integer :: Iatmshl,Jatmshl
      integer :: LAMAX,LBMAX
      integer :: Iatom,Jatom,LS,LL,LNUM
      integer :: Irange,Jrange,MINrange,MAXrange,MINtype,MAXtype
      integer :: Igauss,Jgauss
      integer :: IGBGN,JGBGN,IGend,JGend
      integer :: Iao,Jao,AOI,AOJ
      integer :: I,J,K,INDEX,DEFCASE,IJao
      double precision :: COEF_IJ,COEF_SXY,COEF_PXY
      double precision :: COEF_XXXY,COEF_XYXY,SQRT3,SQRT5,SUMDSQ,Gama,Alpha,Beta,RABSQ,EG,EG_K,EG_exp
      double precision :: Ax,Bx,Ay,By,Az,Bz,Px,Py,Pz,xA,yA,zA,xB,yB,zB,rpx,rpy,rpz
      double precision :: dEG(3), GFA(21),GFB(21),dGFA(21,3),dGFB(21,3)
      logical :: LIatmshl
!
! Local arrays  
      double precision :: dAO(441,3)     ! save derivatives

! Parameters:
      double precision FIVE
      parameter (FIVE=5.0D0)
!
! Begin:
      SQRT3=DSQRT(THREE) 
      SQRT5=DSQRT(FIVE)
!
      MATlen=size(dAOprodX)
      dAOprodX(1:MATlen)=ZERO
      dAOprodY(1:MATlen)=ZERO
      dAOprodZ(1:MATlen)=ZERO
!
! Begin loop over shells.
!
! Loop over elemental SHELLs
! Loop over Ishell.
      do Ishell=1,Basis%Nshells
        LAMAX=Basis%shell(Ishell)%Xtype+1
        Istart=Basis%shell(Ishell)%Xstart
        Iend=Basis%shell(Ishell)%XEND
        Irange=Iend-Istart+1
        IGBGN=Basis%shell(Ishell)%EXPBGN
        IGEND=Basis%shell(Ishell)%EXPEND
        Ifrst=Basis%shell(Ishell)%frstSHL
!
! Loop over Jshell.
      do Jshell=1,Ishell
        LBMAX=Basis%shell(Jshell)%Xtype+1
        Jstart=Basis%shell(Jshell)%Xstart
        Jend=Basis%shell(Jshell)%XEND
        Jrange=Jend-Jstart+1
        JGBGN=Basis%shell(Jshell)%EXPBGN
        JGEND=Basis%shell(Jshell)%EXPEND
        Jfrst=Basis%shell(Jshell)%frstSHL
!
! Define CASE:
      if(LAMAX.lt.LBMAX)then
       MINrange=Irange
       MAXrange=Jrange
       MINtype=LAMAX
       MAXtype=LBMAX
      else
       MINrange=Jrange
       MAXrange=Irange
       MINtype=LBMAX
       MAXtype=LAMAX
      end if

!
      Ilast= Basis%shell(Ishell)%lastSHL
      Jlast= Basis%shell(Jshell)%lastSHL
!
      call AB_dGproducts
!
! End of loop over shells.
      
      end do ! Jshell
      end do ! Ishell
!
! End of routine dG_PRODUCTS
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine AB_dGproducts
!****************************************************************************************
!     Date last modified: May 7, 2017 by Ibrahim Awad                                   *
!     Authors: Ahmad and R.A. Poirier                                                   *
!     Description:                                                                      *
!                                                                                       *
!****************************************************************************************
! Modules:

      implicit none
!
! Loop over all atomic shells
      do Iatmshl=Ifrst,Ilast
        Iatom=Basis%atmshl(Iatmshl)%ATMLST
!
        Ax=CARTESIAN(Iatom)%X     ! vector A={Ax,Ay,Az}     
        Ay=CARTESIAN(Iatom)%Y
        Az=CARTESIAN(Iatom)%Z
!
        AOI=Basis%atmshl(Iatmshl)%frstAO-1
        IF(Ishell.EQ.Jshell)Jlast=Iatmshl
      do Jatmshl=Jfrst,Jlast
        Jatom=Basis%atmshl(Jatmshl)%ATMLST
!
        Bx=CARTESIAN(Jatom)%X     ! vector B={Bx,By,Bz}
        By=CARTESIAN(Jatom)%Y
        Bz=CARTESIAN(Jatom)%Z
!
        AOJ=Basis%atmshl(Jatmshl)%frstAO-1
        LIatmshl=.false.
        if(Iatmshl.eq.Jatmshl)LIatmshl=.true.

        RABSQ=(Ax-Bx)*(Ax-Bx)+(Ay-By)*(Ay-By)+(Az-Bz)*(Az-Bz)     ! (A-B)**2

        dAO(1:441,1:3)=ZERO     ! will handle up to <h|h>, 21*21=441, 1:3 -> x, y, z
! Loop over primitive gaussians
!
      do Igauss=IGBGN,IGEND
        Alpha=Basis%gaussian(Igauss)%exp

      do Jgauss=JGBGN,JGEND
        Beta=Basis%gaussian(Jgauss)%exp
!
        Gama=Alpha+Beta

        Px=(Alpha*Ax+Beta*Bx)/Gama     ! vector P
        Py=(Alpha*Ay+Beta*By)/Gama
        Pz=(Alpha*Az+Beta*Bz)/Gama
!
        rpx = rx-Px    ! vector rp=P-r
        rpy = ry-Py
        rpz = rz-Pz

        SUMDSQ = rpx*rpx + rpy*rpy + rpz*rpz   ! (rp)**2

        EG_K=DEXP(-Alpha*Beta*RABSQ/Gama)      ! K 
        EG_exp=DEXP(-Gama*SUMDSQ)              ! exp
        EG=EG_K*EG_exp                         ! K*exp
!
        xA=rx-Ax     ! vector rA=(r-A)
        yA=ry-Ay
        zA=rz-Az
        xB=rx-Bx     ! vector rB=(r-B)
        yB=ry-By
        zB=rz-Bz


      COEF_IJ = Basis%gaussian(Igauss)%CONTRC*Basis%gaussian(Jgauss)%CONTRC*EG

      call Gaussian_function (xA, yA, zA, GFA, MINtype)
      call Gaussian_function (xB, yB, zB, GFB, MAXtype)
      call DELGaussian_function (xA, yA, zA, dGFA, MINtype)
      call DELGaussian_function (xB, yB, zB, dGFB, MAXtype)
      call DELExpGaussian_function (rpx, rpy, rpz, Gama, dEG)

      LNUM=0
        do LS=1,MINrange
           do LL=1,MAXrange
           LNUM=LNUM+1
               do AONUM=1, 3 ! dx, dy, dz

!              **** THE RULE >>>>>>>
!              [pA*dpB+dpA*pB+pA*pB*dEG]*EG

               dAO(LNUM,AONUM)=dAO(LNUM,AONUM)+ &
                             & COEF_IJ*(GFA(LS)*dGFB(LL,AONUM)+ &
                             & dGFA(LS,AONUM)*GFB(LL)+          &
                             & GFA(LS)*GFB(LL)*dEG(AONUM))

              end do
           end do
        end do
!
      end do ! Jgauss
      end do ! Igauss 
      
        IF(LIatmshl)then
          INDEX=0
          do I=1,Irange
          do J=1,Jrange
            INDEX=INDEX+1
            IF(I.GE.J)then  ! Corrected August 1,2003 had (J.ge.I)
              Iao=AOI+I
              Jao=AOJ+J
              IJao=Iao*(Iao-1)/2+Jao
              dAOprodX(IJao)=dAO(INDEX,1)
              dAOprodY(IJao)=dAO(INDEX,2)
              dAOprodZ(IJao)=dAO(INDEX,3)
            end if
          end do ! J   
          end do ! I
        else
              INDEX=0
          do I=1,Irange
          do J=1,Jrange
            INDEX=INDEX+1
            Iao=AOI+I
            Jao=AOJ+J
            IJao=Iao*(Iao-1)/2+Jao
            dAOprodX(IJao)=dAO(INDEX,1)
            dAOprodY(IJao)=dAO(INDEX,2)
            dAOprodZ(IJao)=dAO(INDEX,3)
          end do ! J
          end do ! I
        end if ! LIatmshl

      end do ! Jatmshl
      end do ! Iatmshl

      return
      end subroutine AB_dGproducts
! END CONTAINS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine dG_PRODUCTS
