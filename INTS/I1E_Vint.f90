      subroutine I1E_Vint (Vint, G_prod, length, XC, YC, ZC)
!***********************************************************************
!     Date last modified: April 20, 2009                   Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: Compute the Vint for grid point IApoint             *
!***********************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE type_basis_set
      USE QM_defaults
      USE QM_objects
!
      implicit none
!
! Input scalars/arrays:
      integer :: length
      double precision :: XC,YC,ZC
      double precision :: Vint(length),G_prod(length)
!
! Local scalars:
      integer :: Icase,Iatom,Jatom
      integer :: Ishell,Jshell,Ifrst,Jfrst,Ilast,Jlast
      integer :: Istart,Jstart,Iend,Jend
      integer :: Iatmshl,Jatmshl
      integer :: LAMAX,LBMAX
      integer :: Irange,Jrange
      integer :: Igauss,Jgauss
      integer :: IAOS,JAOS
      integer :: IGBGN,JGBGN,IGend,JGend
      integer :: LPMAX,I,J,LIM1DS,NZERO
      integer :: LENTQ
      integer :: INTC,IZERO
      integer :: II,JJ,INDX,INDX1,Jends
      logical :: LIeqJ
      double precision :: XA,XB,YA,YB,ZA,ZB,RABSQ
      double precision :: XAC,YAC,ZAC,XBC,YBC,ZBC
      double precision :: ABX,ABY,ABZ,ARABSQ,ARG,AS,ASXA,ASYA,ASZA,BS
      double precision :: EPAB,EPABI,EPIO2,PCX,PCY,PCZ,PEXP,EG
      double precision :: PX,PY,PZ,RPCSQ,TWOASQ,TWOP,TWOPT2,XAP,XBP
      double precision :: XK,YAP,YBP,ZAP,ZBP,ZCONST,ZT,ZTEMP
      double precision :: TP(7),WP(7)
      double precision :: A(45),CA(20),CB(20),CCX(192),CCY(192),CCZ(192)
      double precision :: G(5)
      double precision :: EEPB(100),EEPV(100)
      double precision :: SHLINTS(100)
      double precision :: TWOCX(9),TWOCY(9),TWOCZ(9),XIP(80),YIP(80),ZIP(80)
!
! Local scalars:
      double precision CUT1,TWOPI
      parameter (TWOPI=TWO*PI_VAL,CUT1=-25.0D0)

! Begin:
! Calculate the potential at grid point
! Loop over ISHELL.
      do ISHELL=1,Basis%Nshells
        LAMAX=Basis%shell(ISHELL)%XTYPE+1
        ISTART=Basis%shell(ISHELL)%XSTART
        IEND=Basis%shell(ISHELL)%XEND
        IRANGE=IEND-ISTART+1
        IGBGN=Basis%shell(ISHELL)%EXPBGN
        IGEND=Basis%shell(ISHELL)%EXPEND
        ifRST=Basis%shell(ISHELL)%FRSTSHL
!
! Loop over JSHELL.
        do JSHELL=1,ISHELL
          LBMAX=Basis%shell(JSHELL)%XTYPE+1
          JSTART=Basis%shell(JSHELL)%XSTART
          JEND=Basis%shell(JSHELL)%XEND
          JRANGE=JEND-JSTART+1
          JGBGN=Basis%shell(JSHELL)%EXPBGN
          JGEND=Basis%shell(JSHELL)%EXPEND
          JFRST=Basis%shell(JSHELL)%FRSTSHL
!
          LPMAX=LAMAX+LBMAX-1
          LIM1DS=(LPMAX+3)/2
          LENTQ=IRANGE*JRANGE
          NZERO=(LAMAX+LBMAX-2)/2+1
      ILAST= Basis%shell(Ishell)%LASTSHL
      JLAST= Basis%shell(Jshell)%LASTSHL


      ICASE=LAMAX*(LAMAX-1)+LBMAX
      select case (ICASE)
      case (1)
        call I1ER_SS
      case (2)
        call I1ER_SP
      case (3)
        call I1ER_SD
      case (4)
        call I1ER_PP
      case (5)
        call I1ER_PD
      case (9)
        call I1ER_DD
      case default
        write(6,*)'Incorrect case'
        stop
      end select
!
    end do ! Jshell
  end do ! Ishell
! End of loop over shells.
!
! End of routine I1E_Vint
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine I1ER_SS
!***********************************************************************
!     Date last modified: February 9, 2006                 Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: Attempt at calculating the integral over r2 numerically.    *
!      NOTE must loop over grid points
!***********************************************************************
! Modules:
!
  implicit none
!
! Local scalars:
 
! Loop over atom shells

      include "mungauss_atmshl_IJ"
!
      ABX=XB-XA
      ABY=YB-YA
      ABZ=ZB-ZA
      RABSQ=ABX*ABX+ABY*ABY+ABZ*ABZ ! Only required once for I,Jshell
!
      EEPV(1:LENTQ)=ZERO
      SHLINTS(1:LENTQ)=ZERO
!
! Loop over primitive Gaussians.
      do Igauss=IGBGN,IGEND
        AS=Basis%gaussian(Igauss)%exp
        TWOASQ=TWO*AS*AS
        ASXA=AS*XA
        ASYA=AS*YA
        ASZA=AS*ZA
        ARABSQ=AS*RABSQ
        call FILLCC (LAMAX, Basis%gaussian(Igauss)%CONTRC, CA)
!
        do Jgauss=JGBGN,JGEND
          BS=Basis%gaussian(Jgauss)%exp
          call FILLCC (LBMAX, Basis%gaussian(Jgauss)%CONTRC, CB)
          EPAB=AS+BS
          EPABI=ONE/EPAB
          EPIO2=PT5*EPABI
          TWOP=EPAB+EPAB
          ARG=-BS*ARABSQ*EPABI
          if(ARG.le.CUT1)cycle
          PEXP=DEXP(ARG)

          ZTEMP=TWOPI*EPABI*PEXP
          PX=(ASXA+BS*XB)*EPABI
          PY=(ASYA+BS*YB)*EPABI
          PZ=(ASZA+BS*ZB)*EPABI
!
! Calculate the potential at grid point
          PCX=XC-PX
          PCY=YC-PY
          PCZ=ZC-PZ
          RPCSQ=PCX*PCX+PCY*PCY+PCZ*PCZ
          EG=PEXP*DEXP(-EPAB*RPCSQ)
          ARG=EPAB*RPCSQ
          call RPOLX (NZERO, ARG, TP, WP)

          ZCONST=ZTEMP*WP(1)
          EEPV(1)=EEPV(1)-ZCONST*CA(1)*CB(1)
          SHLINTS(1)=SHLINTS(1)+EG*CA(1)*CB(1)
          end do ! Jgauss
          end do ! Igauss

          INDX=IAOS*(IAOS+1)/2+JAOS+1
          Vint(INDX)=EEPV(1)
          G_prod(INDX)=SHLINTS(1)

        end do ! JATMSHL
      end do ! IATMSHL
!
! End of routine I1ER_SS
      return
      end subroutine I1ER_SS
      subroutine I1ER_SP
!***********************************************************************
!     Date last modified: February 9, 2006                 Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: Attempt at calculating the integral over r2 numerically.    *
!      NOTE must loop over grid points
!***********************************************************************
! Modules:
!
      implicit none
!
! Local scalars:
 
! Loop over atom shells
      include "mungauss_atmshl_IJ"
!
      ABX=XB-XA
      ABY=YB-YA
      ABZ=ZB-ZA
      RABSQ=ABX*ABX+ABY*ABY+ABZ*ABZ ! Only required once for I,Jshell
!
      EEPV(1:LENTQ)=ZERO
      SHLINTS(1:LENTQ)=ZERO
!
! Loop over primitive Gaussians.
      do Igauss=IGBGN,IGEND
        AS=Basis%gaussian(Igauss)%exp
        TWOASQ=TWO*AS*AS
        ASXA=AS*XA
        ASYA=AS*YA
        ASZA=AS*ZA
        ARABSQ=AS*RABSQ
        call FILLCC (LAMAX, Basis%gaussian(Igauss)%CONTRC, CA)
!
        do Jgauss=JGBGN,JGEND
          BS=Basis%gaussian(Jgauss)%exp
          call FILLCC (LBMAX, Basis%gaussian(Jgauss)%CONTRC, CB)
          EPAB=AS+BS
          EPABI=ONE/EPAB
          EPIO2=PT5*EPABI
          TWOP=EPAB+EPAB
          ARG=-BS*ARABSQ*EPABI
          PEXP=ZERO
          if(ARG.le.CUT1)cycle
          PEXP=DEXP(ARG)

          ZTEMP=TWOPI*EPABI*PEXP
          PX=(ASXA+BS*XB)*EPABI
          PY=(ASYA+BS*YB)*EPABI
          PZ=(ASZA+BS*ZB)*EPABI
          XBP=PX-XB
          YBP=PY-YB
          ZBP=PZ-ZB
!
! Calculate the potential at grid point
          ZT=ZTEMP
          PCX=XC-PX
          PCY=YC-PY
          PCZ=ZC-PZ
          RPCSQ=PCX*PCX+PCY*PCY+PCZ*PCZ
          EG=PEXP*DEXP(-EPAB*RPCSQ)
          ARG=EPAB*RPCSQ
          call RPOLX (NZERO, ARG, TP, WP)

          TWOPT2=TWOP*TP(1)
          ZCONST=ZTEMP*WP(1)
          EEPB(1)=(XBP+TWOPT2*PCX*EPIO2)*ZCONST
          EEPB(2)=(YBP+TWOPT2*PCY*EPIO2)*ZCONST
          EEPB(3)=(ZBP+TWOPT2*PCZ*EPIO2)*ZCONST

          EEPV(1)=EEPV(1)-EEPB(1)*CA(1)*CB(2)
          EEPV(2)=EEPV(2)-EEPB(2)*CA(1)*CB(3)
          EEPV(3)=EEPV(3)-EEPB(3)*CA(1)*CB(4)
          XBC=XC-XB
          YBC=YC-YB
          ZBC=ZC-ZB
          SHLINTS(1)=SHLINTS(1)+XBC*EG*CA(1)*CB(2)
          SHLINTS(2)=SHLINTS(2)+YBC*EG*CA(1)*CB(3)
          SHLINTS(3)=SHLINTS(3)+ZBC*EG*CA(1)*CB(4)
          end do ! Jgauss
          end do ! Igauss
!
          INDX=IAOS*(IAOS+1)/2+JAOS+1
          Vint(INDX)=EEPV(1)
          Vint(INDX+1)=EEPV(2)
          Vint(INDX+2)=EEPV(3)
          G_prod(INDX)=SHLINTS(1)
          G_prod(INDX+1)=SHLINTS(2)
          G_prod(INDX+2)=SHLINTS(3)

        end do ! JATMSHL
      end do ! IATMSHL
!
! End of routine I1ER_SP
      return
      end subroutine I1ER_SP
      subroutine I1ER_SD
!***********************************************************************
!     Date last modified: February 9, 2006                 Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: Attempt at calculating the integral over r2 numerically.    *
!      NOTE must loop over grid points
!***********************************************************************
! Modules:
!
      implicit none
!
! Local scalars:
 
! Loop over atom shells
      include "mungauss_atmshl_IJ"
!
      ABX=XB-XA
      ABY=YB-YA
      ABZ=ZB-ZA
      RABSQ=ABX*ABX+ABY*ABY+ABZ*ABZ ! Only required once for I,Jshell
!
      EEPV(1:LENTQ)=ZERO
      SHLINTS(1:LENTQ)=ZERO
!
! Loop over primitive Gaussians.
      do Igauss=IGBGN,IGEND
        AS=Basis%gaussian(Igauss)%exp
        TWOASQ=TWO*AS*AS
        ASXA=AS*XA
        ASYA=AS*YA
        ASZA=AS*ZA
        ARABSQ=AS*RABSQ
        call FILLCC (LAMAX, Basis%gaussian(Igauss)%CONTRC, CA)
!
        do Jgauss=JGBGN,JGEND
          BS=Basis%gaussian(Jgauss)%exp
          call FILLCC (LBMAX, Basis%gaussian(Jgauss)%CONTRC, CB)
          EPAB=AS+BS
          EPABI=ONE/EPAB
          EPIO2=PT5*EPABI
          TWOP=EPAB+EPAB
          ARG=-BS*ARABSQ*EPABI
          PEXP=ZERO
          if(ARG.le.CUT1)cycle
          PEXP=DEXP(ARG)

          ZTEMP=TWOPI*EPABI*PEXP
          PX=(ASXA+BS*XB)*EPABI
          PY=(ASYA+BS*YB)*EPABI
          PZ=(ASZA+BS*ZB)*EPABI
          XBP=PX-XB
          YBP=PY-YB
          ZBP=PZ-ZB
!
! Zero accumulation area.
          EEPB(1:LENTQ)=ZERO
!
! Calculate the potential at grid point
          ZT=ZTEMP
          PCX=XC-PX
          PCY=YC-PY
          PCZ=ZC-PZ
          RPCSQ=PCX*PCX+PCY*PCY+PCZ*PCZ
          EG=PEXP*DEXP(-EPAB*RPCSQ)
          ARG=EPAB*RPCSQ
          call RPOLX (NZERO, ARG, TP, WP)
! I1E_SD code:
          CCX(4)=XBP*XBP
          CCX(5)=XBP+XBP
          CCY(4)=YBP*YBP
          CCY(5)=YBP+YBP
          CCZ(4)=ZBP*ZBP
          CCZ(5)=ZBP+ZBP
          A(6)=EPIO2*EPIO2
          do IZERO=1,2
          TWOPT2=TWOP*TP(IZERO)
          ZCONST=ZTEMP*WP(IZERO)
          G(2)=TWOPT2*PCX
          G(3)=TWOPT2*(PCX*G(2)-ONE)
          TWOCX(3)=EPIO2+G(3)*A(6)
          TWOCX(2)=G(2)*EPIO2
          XIP(3)=CCX(4)+TWOCX(2)*CCX(5)+TWOCX(3)
          XIP(2)=XBP+TWOCX(2)
          G(2)=TWOPT2*PCY
          G(3)=TWOPT2*(PCY*G(2)-ONE)
          TWOCY(3)=EPIO2+G(3)*A(6)
          TWOCY(2)=G(2)*EPIO2
          YIP(3)=CCY(4)+TWOCY(2)*CCY(5)+TWOCY(3)
          YIP(2)=YBP+TWOCY(2)
          G(2)=TWOPT2*PCZ*ZCONST
          G(3)=TWOPT2*(PCZ*G(2)-ZCONST)
          TWOCZ(3)=ZCONST*EPIO2+G(3)*A(6)
          TWOCZ(2)=G(2)*EPIO2
          ZIP(3)=ZCONST*CCZ(4)+TWOCZ(2)*CCZ(5)+TWOCZ(3)
          ZIP(2)=ZCONST*ZBP+TWOCZ(2)
          EEPB(1)=EEPB(1)+XIP(3)*ZCONST
          EEPB(2)=EEPB(2)+YIP(3)*ZCONST
          EEPB(3)=EEPB(3)+ZIP(3)
          EEPB(4)=EEPB(4)+XIP(2)*YIP(2)*ZCONST
          EEPB(5)=EEPB(5)+XIP(2)*ZIP(2)
          EEPB(6)=EEPB(6)+YIP(2)*ZIP(2)
          end do ! IZERO=1,NZERO

          EEPV(1)=EEPV(1)-EEPB(1)*CA(1)*CB(5)
          EEPV(2)=EEPV(2)-EEPB(2)*CA(1)*CB(6)
          EEPV(3)=EEPV(3)-EEPB(3)*CA(1)*CB(7)
          EEPV(4)=EEPV(4)-EEPB(4)*CA(1)*CB(8)
          EEPV(5)=EEPV(5)-EEPB(5)*CA(1)*CB(9)
          EEPV(6)=EEPV(6)-EEPB(6)*CA(1)*CB(10)
          XBC=XC-XB
          YBC=YC-YB
          ZBC=ZC-ZB
          SHLINTS(1)=SHLINTS(1)+XBC*XBC*EG*CA(1)*CB(5)
          SHLINTS(2)=SHLINTS(2)+YBC*YBC*EG*CA(1)*CB(6)
          SHLINTS(3)=SHLINTS(3)+ZBC*ZBC*EG*CA(1)*CB(7)
          SHLINTS(4)=SHLINTS(4)+XBC*YBC*EG*CA(1)*CB(8)
          SHLINTS(5)=SHLINTS(5)+XBC*ZBC*EG*CA(1)*CB(9)
          SHLINTS(6)=SHLINTS(6)+YBC*ZBC*EG*CA(1)*CB(10)
          end do ! Jgauss
          end do ! Igauss

          INDX=IAOS*(IAOS+1)/2+JAOS
          do I=1,6
            Vint(INDX+I)=EEPV(I)
            G_prod(INDX+I)=SHLINTS(I)
          end do

        end do ! JATMSHL
      end do ! IATMSHL
! End of routine I1ER_SD
      return
      end subroutine I1ER_SD
      subroutine I1ER_PP
!***********************************************************************
!     Date last modified: February 9, 2006                 Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: Attempt at calculating the integral over r2 numerically.    *
!      NOTE must loop over grid points
!***********************************************************************
! Modules:
!
  implicit none
!
! Local scalars:
 
! Loop over atom shells
      include "mungauss_atmshl_IJ"
!
      ABX=XB-XA
      ABY=YB-YA
      ABZ=ZB-ZA
      RABSQ=ABX*ABX+ABY*ABY+ABZ*ABZ ! Only required once for I,Jshell
!
      EEPV(1:LENTQ)=ZERO
      SHLINTS(1:LENTQ)=ZERO
!
! Loop over primitive Gaussians.
      do Igauss=IGBGN,IGEND
        AS=Basis%gaussian(Igauss)%exp
        TWOASQ=TWO*AS*AS
        ASXA=AS*XA
        ASYA=AS*YA
        ASZA=AS*ZA
        ARABSQ=AS*RABSQ
        call FILLCC (LAMAX, Basis%gaussian(Igauss)%CONTRC, CA)
!
        do Jgauss=JGBGN,JGEND
          BS=Basis%gaussian(Jgauss)%exp
          call FILLCC (LBMAX, Basis%gaussian(Jgauss)%CONTRC, CB)
          EPAB=AS+BS
          EPABI=ONE/EPAB
          EPIO2=PT5*EPABI
          TWOP=EPAB+EPAB
          ARG=-BS*ARABSQ*EPABI
          if(ARG.le.CUT1)cycle
          PEXP=DEXP(ARG)

          ZTEMP=TWOPI*EPABI*PEXP
          PX=(ASXA+BS*XB)*EPABI
          PY=(ASYA+BS*YB)*EPABI
          PZ=(ASZA+BS*ZB)*EPABI
          XAP=PX-XA
          XBP=PX-XB
          YAP=PY-YA
          YBP=PY-YB
          ZAP=PZ-ZA
          ZBP=PZ-ZB
!
! Zero accumulation area.
          EEPB(1:LENTQ)=ZERO
!
! Calculate the potential at grid point
          ZT=ZTEMP
          PCX=XC-PX
          PCY=YC-PY
          PCZ=ZC-PZ
          RPCSQ=PCX*PCX+PCY*PCY+PCZ*PCZ
          EG=PEXP*DEXP(-EPAB*RPCSQ)
          ARG=EPAB*RPCSQ
          call RPOLX (NZERO, ARG, TP, WP)
! I1E_PP code:
          CCX(11)=XAP
          CCX(13)=XAP*XBP
          CCX(14)=XAP+XBP
          CCY(11)=YAP
          CCY(13)=YAP*YBP
          CCY(14)=YAP+YBP
          CCZ(11)=ZAP
          CCZ(13)=ZAP*ZBP
          CCZ(14)=ZAP+ZBP
          A(6)=EPIO2*EPIO2
          do IZERO=1,2
          TWOPT2=TWOP*TP(IZERO)
          ZCONST=ZTEMP*WP(IZERO)
          G(2)=TWOPT2*PCX
          G(3)=TWOPT2*(PCX*G(2)-ONE)
          TWOCX(3)=EPIO2+G(3)*A(6)
          TWOCX(2)=G(2)*EPIO2
          XIP(6)=CCX(13)+TWOCX(2)*CCX(14)+TWOCX(3)
          XIP(5)=CCX(11)+TWOCX(2)
          XIP(2)=XBP+TWOCX(2)
          G(2)=TWOPT2*PCY
          G(3)=TWOPT2*(PCY*G(2)-ONE)
          TWOCY(3)=EPIO2+G(3)*A(6)
          TWOCY(2)=G(2)*EPIO2
          YIP(6)=CCY(13)+TWOCY(2)*CCY(14)+TWOCY(3)
          YIP(5)=CCY(11)+TWOCY(2)
          YIP(2)=YBP+TWOCY(2)
          G(2)=TWOPT2*PCZ*ZCONST
          G(3)=TWOPT2*(PCZ*G(2)-ZCONST)
          TWOCZ(3)=ZCONST*EPIO2+G(3)*A(6)
          TWOCZ(2)=G(2)*EPIO2
          ZIP(6)=ZCONST*CCZ(13)+TWOCZ(2)*CCZ(14)+TWOCZ(3)
          ZIP(5)=ZCONST*CCZ(11)+TWOCZ(2)
          ZIP(2)=ZCONST*ZBP+TWOCZ(2)
          EEPB(1)=EEPB(1)+XIP(6)*ZCONST
          EEPB(2)=EEPB(2)+XIP(5)*YIP(2)*ZCONST
          EEPB(3)=EEPB(3)+XIP(5)*ZIP(2)
          EEPB(4)=EEPB(4)+XIP(2)*YIP(5)*ZCONST
          EEPB(5)=EEPB(5)+YIP(6)*ZCONST 
          EEPB(6)=EEPB(6)+YIP(5)*ZIP(2)
          EEPB(7)=EEPB(7)+XIP(2)*ZIP(5)
          EEPB(8)=EEPB(8)+YIP(2)*ZIP(5)
          EEPB(9)=EEPB(9)+ZIP(6)
          end do ! IZERO=1,NZERO

          EEPV(1)=EEPV(1)-EEPB(1)*CA(2)*CB(2)
          EEPV(2)=EEPV(2)-EEPB(2)*CA(2)*CB(3)
          EEPV(3)=EEPV(3)-EEPB(3)*CA(2)*CB(4)
          EEPV(4)=EEPV(4)-EEPB(4)*CA(3)*CB(2)
          EEPV(5)=EEPV(5)-EEPB(5)*CA(3)*CB(3)
          EEPV(6)=EEPV(6)-EEPB(6)*CA(3)*CB(4)
          EEPV(7)=EEPV(7)-EEPB(7)*CA(4)*CB(2)
          EEPV(8)=EEPV(8)-EEPB(8)*CA(4)*CB(3)
          EEPV(9)=EEPV(9)-EEPB(9)*CA(4)*CB(4)
          XAC=XC-XA
          YAC=YC-YA
          ZAC=ZC-ZA
          XBC=XC-XB
          YBC=YC-YB
          ZBC=ZC-ZB
          SHLINTS(1)=SHLINTS(1)+XAC*XBC*EG*CA(2)*CB(2)
          SHLINTS(2)=SHLINTS(2)+XAC*YBC*EG*CA(2)*CB(3)
          SHLINTS(3)=SHLINTS(3)+XAC*ZBC*EG*CA(2)*CB(4)
          SHLINTS(4)=SHLINTS(4)+YAC*XBC*EG*CA(3)*CB(2)
          SHLINTS(5)=SHLINTS(5)+YAC*YBC*EG*CA(3)*CB(3)
          SHLINTS(6)=SHLINTS(6)+YAC*ZBC*EG*CA(3)*CB(4)
          SHLINTS(7)=SHLINTS(7)+ZAC*XBC*EG*CA(4)*CB(2)
          SHLINTS(8)=SHLINTS(8)+ZAC*YBC*EG*CA(4)*CB(3)
          SHLINTS(9)=SHLINTS(9)+ZAC*ZBC*EG*CA(4)*CB(4)
          end do ! Jgauss
          end do ! Igauss

          LIeqJ=.false.
          if(Iatmshl.eq.Jatmshl)LIeqJ=.true.
          INTC=0
          INDX1=0
          do I=1,Irange
            Jends=Jrange
            if(LIeqJ)Jends=I
            II=Iaos+I
            INTC=INDX1
            do J=1,Jends
              INTC=INTC+1
              JJ=Jaos+J
              INDX=II*(II-1)/2+JJ
              Vint(INDX)=EEPV(INTC)
              G_prod(INDX)=SHLINTS(INTC)
            end do ! J
            INDX1=INDX1+Jrange
          end do ! I

        end do ! JATMSHL
      end do ! IATMSHL
!
! End of routine I1ER_PP
      return
      end subroutine I1ER_PP
      subroutine I1ER_PD
!***********************************************************************
!     Date last modified: February 9, 2006                 Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: Attempt at calculating the integral over r2 numerically.    *
!      NOTE must loop over grid points
!***********************************************************************
! Modules:
!
  implicit none
!
! Local scalars:
 
! Loop over atom shells
      include "mungauss_atmshl_IJ"
!
      ABX=XB-XA
      ABY=YB-YA
      ABZ=ZB-ZA
      RABSQ=ABX*ABX+ABY*ABY+ABZ*ABZ ! Only required once for I,Jshell
!
      EEPV(1:LENTQ)=ZERO
      SHLINTS(1:LENTQ)=ZERO
!
! Loop over primitive Gaussians.
      do Igauss=IGBGN,IGEND
        AS=Basis%gaussian(Igauss)%exp
        TWOASQ=TWO*AS*AS
        ASXA=AS*XA
        ASYA=AS*YA
        ASZA=AS*ZA
        ARABSQ=AS*RABSQ
        call FILLCC (LAMAX, Basis%gaussian(Igauss)%CONTRC, CA)
!
        do Jgauss=JGBGN,JGEND
          BS=Basis%gaussian(Jgauss)%exp
          call FILLCC (LBMAX, Basis%gaussian(Jgauss)%CONTRC, CB)
          EPAB=AS+BS
          EPABI=ONE/EPAB
          EPIO2=PT5*EPABI
          TWOP=EPAB+EPAB
          ARG=-BS*ARABSQ*EPABI
          PEXP=ZERO
          if(ARG.le.CUT1)cycle
          PEXP=DEXP(ARG)

          ZTEMP=TWOPI*EPABI*PEXP
          PX=(ASXA+BS*XB)*EPABI
          PY=(ASYA+BS*YB)*EPABI
          PZ=(ASZA+BS*ZB)*EPABI
          XAP=PX-XA
          XBP=PX-XB
          YAP=PY-YA
          YBP=PY-YB
          ZAP=PZ-ZA
          ZBP=PZ-ZB
!
! Zero accumulation area.
          EEPB(1:LENTQ)=ZERO
!
! Calculate the potential at grid point
          ZT=ZTEMP
          PCX=XC-PX
          PCY=YC-PY
          PCZ=ZC-PZ
          RPCSQ=PCX*PCX+PCY*PCY+PCZ*PCZ
          EG=PEXP*DEXP(-EPAB*RPCSQ)
          ARG=EPAB*RPCSQ
          call RPOLX (NZERO, ARG, TP, WP)
! I1E_PD code:
          CCX(4)=XBP*XBP
          CCX(5)=XBP+XBP
          CCX(11)=XAP
          CCX(13)=XAP*XBP
          CCX(14)=XAP+XBP
          CCX(16)=XAP*CCX(4)
          CCX(17)=XAP*CCX(5)+CCX(4)
          CCX(18)=XAP+CCX(5)
          CCY(4)=YBP*YBP
          CCY(5)=YBP+YBP
          CCY(11)=YAP
          CCY(13)=YAP*YBP
          CCY(14)=YAP+YBP
          CCY(16)=YAP*CCY(4)
          CCY(17)=YAP*CCY(5)+CCY(4)
          CCY(18)=YAP+CCY(5)
          CCZ(4)=ZBP*ZBP
          CCZ(5)=ZBP+ZBP
          CCZ(11)=ZAP
          CCZ(13)=ZAP*ZBP
          CCZ(14)=ZAP+ZBP
          CCZ(16)=ZAP*CCZ(4)
          CCZ(17)=ZAP*CCZ(5)+CCZ(4)
          CCZ(18)=ZAP+CCZ(5)
          A(6)=EPIO2*EPIO2
          A(8)=EPIO2*(TWO*EPIO2+EPIO2)
          A(10)=EPIO2*A(6)
          do IZERO=1,2
          TWOPT2=TWOP*TP(IZERO)
          ZCONST=ZTEMP*WP(IZERO)
          G(2)=TWOPT2*PCX
          G(3)=TWOPT2*(PCX*G(2)-ONE)
          G(4)=TWOPT2*(PCX*G(3)-TWO*G(2))
          TWOCX(4)=G(2)*A(8)+G(4)*A(10)
          TWOCX(3)=EPIO2+G(3)*A(6)
          TWOCX(2)=G(2)*EPIO2
          XIP(7)=CCX(16)+TWOCX(2)*CCX(17)+TWOCX(3)*CCX(18)+TWOCX(4)
          XIP(6)=CCX(13)+TWOCX(2)*CCX(14)+TWOCX(3)
          XIP(5)=CCX(11)+TWOCX(2)
          XIP(3)=CCX(4)+TWOCX(2)*CCX(5)+TWOCX(3)
          XIP(2)=XBP+TWOCX(2)
          XIP(1)=TWOCX(1)
          G(2)=TWOPT2*PCY
          G(3)=TWOPT2*(PCY*G(2)-ONE)
          G(4)=TWOPT2*(PCY*G(3)-TWO*G(2))
          TWOCY(4)=G(2)*A(8)+G(4)*A(10)
          TWOCY(3)=EPIO2+G(3)*A(6)
          TWOCY(2)=G(2)*EPIO2
          YIP(7)=CCY(16)+TWOCY(2)*CCY(17)+TWOCY(3)*CCY(18)+TWOCY(4)
          YIP(6)=CCY(13)+TWOCY(2)*CCY(14)+TWOCY(3)
          YIP(5)=CCY(11)+TWOCY(2)
          YIP(3)=CCY(4)+TWOCY(2)*CCY(5)+TWOCY(3)
          YIP(2)=YBP+TWOCY(2)
          YIP(1)=TWOCY(1)
          G(2)=TWOPT2*PCZ*ZCONST
          G(3)=TWOPT2*(PCZ*G(2)-ZCONST)
          G(4)=TWOPT2*(PCZ*G(3)-TWO*G(2))
          TWOCZ(4)=G(2)*A(8)+G(4)*A(10)
          TWOCZ(3)=ZCONST*EPIO2+G(3)*A(6)
          TWOCZ(2)=G(2)*EPIO2
          ZIP(7)=ZCONST*CCZ(16)+TWOCZ(2)*CCZ(17)+TWOCZ(3)*CCZ(18)+TWOCZ(4)
          ZIP(6)=ZCONST*CCZ(13)+TWOCZ(2)*CCZ(14)+TWOCZ(3)
          ZIP(5)=ZCONST*CCZ(11)+TWOCZ(2)
          ZIP(3)=ZCONST*CCZ(4)+TWOCZ(2)*CCZ(5)+TWOCZ(3)
          ZIP(2)=ZCONST*ZBP+TWOCZ(2)
          EEPB(1)=EEPB(1)+XIP(7)*ZCONST
          EEPB(2)=EEPB(2)+XIP(5)*YIP(3)*ZCONST
          EEPB(3)=EEPB(3)+XIP(5)*ZIP(3)
          EEPB(4)=EEPB(4)+XIP(6)*YIP(2)*ZCONST
          EEPB(5)=EEPB(5)+XIP(6)*ZIP(2)
          EEPB(6)=EEPB(6)+XIP(5)*YIP(2)*ZIP(2)
          EEPB(7)=EEPB(7)+XIP(3)*YIP(5)*ZCONST
          EEPB(8)=EEPB(8)+YIP(7)*ZCONST
          EEPB(9)=EEPB(9)+YIP(5)*ZIP(3)
          EEPB(10)=EEPB(10)+XIP(2)*YIP(6)*ZCONST
          EEPB(11)=EEPB(11)+XIP(2)*YIP(5)*ZIP(2)
          EEPB(12)=EEPB(12)+YIP(6)*ZIP(2)
          EEPB(13)=EEPB(13)+XIP(3)*ZIP(5)
          EEPB(14)=EEPB(14)+YIP(3)*ZIP(5)
          EEPB(15)=EEPB(15)+ZIP(7)
          EEPB(16)=EEPB(16)+XIP(2)*YIP(2)*ZIP(5)
          EEPB(17)=EEPB(17)+XIP(2)*ZIP(6)
          EEPB(18)=EEPB(18)+YIP(2)*ZIP(6)
          end do ! IZERO=1,NZERO

          EEPV(1)=EEPV(1)-EEPB(1)*CA(2)*CB(5)
          EEPV(2)=EEPV(2)-EEPB(2)*CA(2)*CB(6)
          EEPV(3)=EEPV(3)-EEPB(3)*CA(2)*CB(7)
          EEPV(4)=EEPV(4)-EEPB(4)*CA(2)*CB(8)
          EEPV(5)=EEPV(5)-EEPB(5)*CA(2)*CB(9)
          EEPV(6)=EEPV(6)-EEPB(6)*CA(2)*CB(10)
          EEPV(7)=EEPV(7)-EEPB(7)*CA(3)*CB(5)
          EEPV(8)=EEPV(8)-EEPB(8)*CA(3)*CB(6)
          EEPV(9)=EEPV(9)-EEPB(9)*CA(3)*CB(7)
          EEPV(10)=EEPV(10)-EEPB(10)*CA(3)*CB(8)
          EEPV(11)=EEPV(11)-EEPB(11)*CA(3)*CB(9)
          EEPV(12)=EEPV(12)-EEPB(12)*CA(3)*CB(10)
          EEPV(13)=EEPV(13)-EEPB(13)*CA(4)*CB(5)
          EEPV(14)=EEPV(14)-EEPB(14)*CA(4)*CB(6)
          EEPV(15)=EEPV(15)-EEPB(15)*CA(4)*CB(7)
          EEPV(16)=EEPV(16)-EEPB(16)*CA(4)*CB(8)
          EEPV(17)=EEPV(17)-EEPB(17)*CA(4)*CB(9)
          EEPV(18)=EEPV(18)-EEPB(18)*CA(4)*CB(10)
          XAC=XC-XA
          YAC=YC-YA
          ZAC=ZC-ZA
          XBC=XC-XB
          YBC=YC-YB
          ZBC=ZC-ZB
          SHLINTS(1)=SHLINTS(1)+XAC*XBC*XBC*EG*CA(2)*CB(5)
          SHLINTS(2)=SHLINTS(2)+XAC*YBC*YBC*EG*CA(2)*CB(6)
          SHLINTS(3)=SHLINTS(3)+XAC*ZBC*ZBC*EG*CA(2)*CB(7)
          SHLINTS(4)=SHLINTS(4)+XAC*XBC*YBC*EG*CA(2)*CB(8)
          SHLINTS(5)=SHLINTS(5)+XAC*XBC*ZBC*EG*CA(2)*CB(9)
          SHLINTS(6)=SHLINTS(6)+XAC*YBC*ZBC*EG*CA(2)*CB(10)

          SHLINTS(7)=SHLINTS(7)+YAC*XBC*XBC*EG*CA(3)*CB(5)
          SHLINTS(8)=SHLINTS(8)+YAC*YBC*YBC*EG*CA(3)*CB(6)
          SHLINTS(9)=SHLINTS(9)+YAC*ZBC*ZBC*EG*CA(3)*CB(7)
          SHLINTS(10)=SHLINTS(10)+YAC*XBC*YBC*EG*CA(3)*CB(8)
          SHLINTS(11)=SHLINTS(11)+YAC*XBC*ZBC*EG*CA(3)*CB(9)
          SHLINTS(12)=SHLINTS(12)+YAC*YBC*ZBC*EG*CA(3)*CB(10)

          SHLINTS(13)=SHLINTS(13)+ZAC*XBC*XBC*EG*CA(4)*CB(5)
          SHLINTS(14)=SHLINTS(14)+ZAC*YBC*YBC*EG*CA(4)*CB(6)
          SHLINTS(15)=SHLINTS(15)+ZAC*ZBC*ZBC*EG*CA(4)*CB(7)
          SHLINTS(16)=SHLINTS(16)+ZAC*XBC*YBC*EG*CA(4)*CB(8)
          SHLINTS(17)=SHLINTS(17)+ZAC*XBC*ZBC*EG*CA(4)*CB(9)
          SHLINTS(18)=SHLINTS(18)+ZAC*YBC*ZBC*EG*CA(4)*CB(10)
          end do ! Jgauss
          end do ! Igauss

          INTC=0
          do I=1,Irange
            II=Iaos+I
            do J=1,Jrange
              INTC=INTC+1
              JJ=Jaos+J
              INDX=II*(II-1)/2+JJ
              Vint(INDX)=EEPV(INTC)
              G_prod(INDX)=SHLINTS(INTC)
            end do ! J
          end do ! I

        end do ! JATMSHL
      end do ! IATMSHL

!
! End of routine I1ER_PD
      return
      end subroutine I1ER_PD
      subroutine I1ER_DD
!***********************************************************************
!     Date last modified: February 9, 2006                 Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: Attempt at calculating the integral over r2 numerically.    *
!      NOTE must loop over grid points
!***********************************************************************
! Modules:
!
  implicit none
!
! Local scalars:
 
! Loop over atom shells
      include "mungauss_atmshl_IJ"
!
      ABX=XB-XA
      ABY=YB-YA
      ABZ=ZB-ZA
      RABSQ=ABX*ABX+ABY*ABY+ABZ*ABZ ! Only required once for I,Jshell
!
      EEPV(1:LENTQ)=ZERO
      SHLINTS(1:LENTQ)=ZERO
!
! Loop over primitive Gaussians.
      do Igauss=IGBGN,IGEND
        AS=Basis%gaussian(Igauss)%exp
        TWOASQ=TWO*AS*AS
        ASXA=AS*XA
        ASYA=AS*YA
        ASZA=AS*ZA
        ARABSQ=AS*RABSQ
        call FILLCC (LAMAX, Basis%gaussian(Igauss)%CONTRC, CA)
!
        do Jgauss=JGBGN,JGEND
          BS=Basis%gaussian(Jgauss)%exp
          call FILLCC (LBMAX, Basis%gaussian(Jgauss)%CONTRC, CB)
          EPAB=AS+BS
          EPABI=ONE/EPAB
          EPIO2=PT5*EPABI
          TWOP=EPAB+EPAB
          ARG=-BS*ARABSQ*EPABI
          PEXP=ZERO
          if(ARG.le.CUT1)cycle
          PEXP=DEXP(ARG)

          ZTEMP=TWOPI*EPABI*PEXP
          PX=(ASXA+BS*XB)*EPABI
          PY=(ASYA+BS*YB)*EPABI
          PZ=(ASZA+BS*ZB)*EPABI
          XAP=PX-XA
          XBP=PX-XB
          YAP=PY-YA
          YBP=PY-YB
          ZAP=PZ-ZA
          ZBP=PZ-ZB
!
! Zero accumulation area.
          EEPB(1:LENTQ)=ZERO
!
! Calculate the potential at grid point
          ZT=ZTEMP
          PCX=XC-PX
          PCY=YC-PY
          PCZ=ZC-PZ
          RPCSQ=PCX*PCX+PCY*PCY+PCZ*PCZ
          EG=PEXP*DEXP(-EPAB*RPCSQ)
          ARG=EPAB*RPCSQ
          call RPOLX (NZERO, ARG, TP, WP)
! I1E_DD code:
          CCX(4)=XBP*XBP
          CCX(5)=XBP+XBP
          CCX(11)=XAP
          CCX(13)=XAP*XBP
          CCX(14)=XAP+XBP
          CCX(16)=XAP*CCX(4)
          CCX(17)=XAP*CCX(5)+CCX(4)
          CCX(18)=XAP+CCX(5)
          CCX(25)=XAP*CCX(11)
          CCX(26)=XAP+CCX(11)
          CCX(28)=XAP*CCX(13)
          CCX(29)=XAP*CCX(14)+CCX(13)
          CCX(30)=XAP+CCX(14)
          CCX(32)=XAP*CCX(16)
          CCX(33)=XAP*CCX(17)+CCX(16)
          CCX(34)=XAP*CCX(18)+CCX(17)
          CCX(35)=XAP+CCX(18)
          CCY(4)=YBP*YBP
          CCY(5)=YBP+YBP
          CCY(11)=YAP
          CCY(13)=YAP*YBP
          CCY(14)=YAP+YBP
          CCY(16)=YAP*CCY(4)
          CCY(17)=YAP*CCY(5)+CCY(4)
          CCY(18)=YAP+CCY(5)
          CCY(25)=YAP*CCY(11)
          CCY(26)=YAP+CCY(11)
          CCY(28)=YAP*CCY(13)
          CCY(29)=YAP*CCY(14)+CCY(13)
          CCY(30)=YAP+CCY(14)
          CCY(32)=YAP*CCY(16)
          CCY(33)=YAP*CCY(17)+CCY(16)
          CCY(34)=YAP*CCY(18)+CCY(17)
          CCY(35)=YAP+CCY(18)
          CCZ(4)=ZBP*ZBP
          CCZ(5)=ZBP+ZBP
          CCZ(11)=ZAP
          CCZ(13)=ZAP*ZBP
          CCZ(14)=ZAP+ZBP
          CCZ(16)=ZAP*CCZ(4)
          CCZ(17)=ZAP*CCZ(5)+CCZ(4)
          CCZ(18)=ZAP+CCZ(5)
          CCZ(25)=ZAP*CCZ(11)
          CCZ(26)=ZAP+CCZ(11)
          CCZ(28)=ZAP*CCZ(13)
          CCZ(29)=ZAP*CCZ(14)+CCZ(13)
          CCZ(30)=ZAP+CCZ(14)
          CCZ(32)=ZAP*CCZ(16)
          CCZ(33)=ZAP*CCZ(17)+CCZ(16)
          CCZ(34)=ZAP*CCZ(18)+CCZ(17)
          CCZ(35)=ZAP+CCZ(18)
          A(6)=EPIO2*EPIO2
          A(8)=EPIO2*(TWO*EPIO2+EPIO2)
          A(10)=EPIO2*A(6)
          A(11)=THREE*EPIO2*EPIO2
          A(13)=EPIO2*(THREE*A(6)+A(8))
          A(15)=EPIO2*A(10)
          do IZERO=1,3
          TWOPT2=TWOP*TP(IZERO)
          ZCONST=ZTEMP*WP(IZERO)
          G(2)=TWOPT2*PCX
          G(3)=TWOPT2*(PCX*G(2)-ONE)
          G(4)=TWOPT2*(PCX*G(3)-TWO*G(2))
          G(5)=TWOPT2*(PCX*G(4)-THREE*G(3))
          TWOCX(5)=A(11)+G(3)*A(13)+G(5)*A(15)
          TWOCX(4)=G(2)*A(8)+G(4)*A(10)
          TWOCX(3)=EPIO2+G(3)*A(6)
          TWOCX(2)=G(2)*EPIO2
          XIP(11)=CCX(32)+TWOCX(2)*CCX(33)+TWOCX(3)*CCX(34)+TWOCX(4)*CCX(35)+TWOCX(5)
          XIP(10)=CCX(28)+TWOCX(2)*CCX(29)+TWOCX(3)*CCX(30)+TWOCX(4)
          XIP(9)=CCX(25)+TWOCX(2)*CCX(26)+TWOCX(3)
          XIP(7)=CCX(16)+TWOCX(2)*CCX(17)+TWOCX(3)*CCX(18)+TWOCX(4)
          XIP(6)=CCX(13)+TWOCX(2)*CCX(14)+TWOCX(3)
          XIP(5)=CCX(11)+TWOCX(2)
          XIP(3)=CCX(4)+TWOCX(2)*CCX(5)+TWOCX(3)
          XIP(2)=XBP+TWOCX(2)
          G(2)=TWOPT2*PCY
          G(3)=TWOPT2*(PCY*G(2)-ONE)
          G(4)=TWOPT2*(PCY*G(3)-TWO*G(2))
          G(5)=TWOPT2*(PCY*G(4)-THREE*G(3))
          TWOCY(5)=A(11)+G(3)*A(13)+G(5)*A(15)
          TWOCY(4)=G(2)*A(8)+G(4)*A(10)
          TWOCY(3)=EPIO2+G(3)*A(6)
          TWOCY(2)=G(2)*EPIO2
          YIP(11)=CCY(32)+TWOCY(2)*CCY(33)+TWOCY(3)*CCY(34)+TWOCY(4)*CCY(35)+TWOCY(5)
          YIP(10)=CCY(28)+TWOCY(2)*CCY(29)+TWOCY(3)*CCY(30)+TWOCY(4)
          YIP(9)=CCY(25)+TWOCY(2)*CCY(26)+TWOCY(3)
          YIP(7)=CCY(16)+TWOCY(2)*CCY(17)+TWOCY(3)*CCY(18)+TWOCY(4)
          YIP(6)=CCY(13)+TWOCY(2)*CCY(14)+TWOCY(3)
          YIP(5)=CCY(11)+TWOCY(2)
          YIP(3)=CCY(4)+TWOCY(2)*CCY(5)+TWOCY(3)
          YIP(2)=YBP+TWOCY(2)
          G(2)=TWOPT2*PCZ*ZCONST
          G(3)=TWOPT2*(PCZ*G(2)-ZCONST)
          G(4)=TWOPT2*(PCZ*G(3)-TWO*G(2))
          G(5)=TWOPT2*(PCZ*G(4)-THREE*G(3))
          TWOCZ(5)=ZCONST*A(11)+G(3)*A(13)+G(5)*A(15)
          TWOCZ(4)=G(2)*A(8)+G(4)*A(10)
          TWOCZ(3)=ZCONST*EPIO2+G(3)*A(6)
          TWOCZ(2)=G(2)*EPIO2
          ZIP(11)=ZCONST*CCZ(32)+TWOCZ(2)*CCZ(33)+TWOCZ(3)*CCZ(34)+TWOCZ(4)*CCZ(35)+TWOCZ(5)
          ZIP(10)=ZCONST*CCZ(28)+TWOCZ(2)*CCZ(29)+TWOCZ(3)*CCZ(30)+TWOCZ(4)
          ZIP(9)=ZCONST*CCZ(25)+TWOCZ(2)*CCZ(26)+TWOCZ(3)
          ZIP(7)=ZCONST*CCZ(16)+TWOCZ(2)*CCZ(17)+TWOCZ(3)*CCZ(18)+TWOCZ(4)
          ZIP(6)=ZCONST*CCZ(13)+TWOCZ(2)*CCZ(14)+TWOCZ(3)
          ZIP(5)=ZCONST*CCZ(11)+TWOCZ(2)
          ZIP(3)=ZCONST*CCZ(4)+TWOCZ(2)*CCZ(5)+TWOCZ(3)
          ZIP(2)=ZCONST*ZBP+TWOCZ(2)
          EEPB(1)=EEPB(1)+XIP(11)*ZCONST
          EEPB(2)=EEPB(2)+XIP(9)*YIP(3)*ZCONST
          EEPB(3)=EEPB(3)+XIP(9)*ZIP(3)
          EEPB(4)=EEPB(4)+XIP(10)*YIP(2)*ZCONST
          EEPB(5)=EEPB(5)+XIP(10)*ZIP(2)
          EEPB(6)=EEPB(6)+XIP(9)*YIP(2)*ZIP(2)
          EEPB(7)=EEPB(7)+XIP(3)*YIP(9)*ZCONST
          EEPB(8)=EEPB(8)+YIP(11)*ZCONST
          EEPB(9)=EEPB(9)+YIP(9)*ZIP(3)
          EEPB(10)=EEPB(10)+XIP(2)*YIP(10)*ZCONST
          EEPB(11)=EEPB(11)+XIP(2)*YIP(9)*ZIP(2)
          EEPB(12)=EEPB(12)+YIP(10)*ZIP(2)
          EEPB(13)=EEPB(13)+XIP(3)*ZIP(9)
          EEPB(14)=EEPB(14)+YIP(3)*ZIP(9)
          EEPB(15)=EEPB(15)+ZIP(11)
          EEPB(16)=EEPB(16)+XIP(2)*YIP(2)*ZIP(9)
          EEPB(17)=EEPB(17)+XIP(2)*ZIP(10)
          EEPB(18)=EEPB(18)+YIP(2)*ZIP(10)
          EEPB(19)=EEPB(19)+XIP(7)*YIP(5)*ZCONST
          EEPB(20)=EEPB(20)+XIP(5)*YIP(7)*ZCONST
          EEPB(21)=EEPB(21)+XIP(5)*YIP(5)*ZIP(3)
          EEPB(22)=EEPB(22)+XIP(6)*YIP(6)*ZCONST
          EEPB(23)=EEPB(23)+XIP(6)*YIP(5)*ZIP(2)
          EEPB(24)=EEPB(24)+XIP(5)*YIP(6)*ZIP(2)
          EEPB(25)=EEPB(25)+XIP(7)*ZIP(5)
          EEPB(26)=EEPB(26)+XIP(5)*YIP(3)*ZIP(5)
          EEPB(27)=EEPB(27)+XIP(5)*ZIP(7)
          EEPB(28)=EEPB(28)+XIP(6)*YIP(2)*ZIP(5)
          EEPB(29)=EEPB(29)+XIP(6)*ZIP(6)
          EEPB(30)=EEPB(30)+XIP(5)*YIP(2)*ZIP(6)
          EEPB(31)=EEPB(31)+XIP(3)*YIP(5)*ZIP(5)
          EEPB(32)=EEPB(32)+YIP(7)*ZIP(5)
          EEPB(33)=EEPB(33)+YIP(5)*ZIP(7)
          EEPB(34)=EEPB(34)+XIP(2)*YIP(6)*ZIP(5)
          EEPB(35)=EEPB(35)+XIP(2)*YIP(5)*ZIP(6)
          EEPB(36)=EEPB(36)+YIP(6)*ZIP(6)
          end do ! IZERO=1,NZERO

          EEPV( 1)=EEPV( 1)-EEPB( 1)*CA(5)*CB(5)
          EEPV( 2)=EEPV( 2)-EEPB( 2)*CA(5)*CB(6)
          EEPV( 3)=EEPV( 3)-EEPB( 3)*CA(5)*CB(7)
          EEPV( 4)=EEPV( 4)-EEPB( 4)*CA(5)*CB(8)
          EEPV( 5)=EEPV( 5)-EEPB( 5)*CA(5)*CB(9)
          EEPV( 6)=EEPV( 6)-EEPB( 6)*CA(5)*CB(10)
          EEPV( 7)=EEPV( 7)-EEPB( 7)*CA(6)*CB(5)
          EEPV( 8)=EEPV( 8)-EEPB( 8)*CA(6)*CB(6)
          EEPV( 9)=EEPV( 9)-EEPB( 9)*CA(6)*CB(7)
          EEPV(10)=EEPV(10)-EEPB(10)*CA(6)*CB(8)
          EEPV(11)=EEPV(11)-EEPB(11)*CA(6)*CB(9)
          EEPV(12)=EEPV(12)-EEPB(12)*CA(6)*CB(10)
          EEPV(13)=EEPV(13)-EEPB(13)*CA(7)*CB(5)
          EEPV(14)=EEPV(14)-EEPB(14)*CA(7)*CB(6)
          EEPV(15)=EEPV(15)-EEPB(15)*CA(7)*CB(7)
          EEPV(16)=EEPV(16)-EEPB(16)*CA(7)*CB(8)
          EEPV(17)=EEPV(17)-EEPB(17)*CA(7)*CB(9)
          EEPV(18)=EEPV(18)-EEPB(18)*CA(7)*CB(10)
          EEPV(19)=EEPV(19)-EEPB(19)*CA(8)*CB(5)
          EEPV(20)=EEPV(20)-EEPB(20)*CA(8)*CB(6)
          EEPV(21)=EEPV(21)-EEPB(21)*CA(8)*CB(7)
          EEPV(22)=EEPV(22)-EEPB(22)*CA(8)*CB(8)
          EEPV(23)=EEPV(23)-EEPB(23)*CA(8)*CB(9)
          EEPV(24)=EEPV(24)-EEPB(24)*CA(8)*CB(10)
          EEPV(25)=EEPV(25)-EEPB(25)*CA(9)*CB(5)
          EEPV(26)=EEPV(26)-EEPB(26)*CA(9)*CB(6)
          EEPV(27)=EEPV(27)-EEPB(27)*CA(9)*CB(7)
          EEPV(28)=EEPV(28)-EEPB(28)*CA(9)*CB(8)
          EEPV(29)=EEPV(29)-EEPB(29)*CA(9)*CB(9)
          EEPV(30)=EEPV(30)-EEPB(30)*CA(9)*CB(10)
          EEPV(31)=EEPV(31)-EEPB(31)*CA(10)*CB(5)
          EEPV(32)=EEPV(32)-EEPB(32)*CA(10)*CB(6)
          EEPV(33)=EEPV(33)-EEPB(33)*CA(10)*CB(7)
          EEPV(34)=EEPV(34)-EEPB(34)*CA(10)*CB(8)
          EEPV(35)=EEPV(35)-EEPB(35)*CA(10)*CB(9)
          EEPV(36)=EEPV(36)-EEPB(36)*CA(10)*CB(10)
          XAC=XC-XA
          YAC=YC-YA
          ZAC=ZC-ZA
          XBC=XC-XB
          YBC=YC-YB
          ZBC=ZC-ZB
          SHLINTS(1)=SHLINTS(1)+XAC*XAC*XBC*XBC*EG*CA(5)*CB(5)
          SHLINTS(2)=SHLINTS(2)+XAC*XAC*YBC*YBC*EG*CA(5)*CB(6)
          SHLINTS(3)=SHLINTS(3)+XAC*XAC*ZBC*ZBC*EG*CA(5)*CB(7)
          SHLINTS(4)=SHLINTS(4)+XAC*XAC*XBC*YBC*EG*CA(5)*CB(8)
          SHLINTS(5)=SHLINTS(5)+XAC*XAC*XBC*ZBC*EG*CA(5)*CB(9)
          SHLINTS(6)=SHLINTS(6)+XAC*XAC*YBC*ZBC*EG*CA(5)*CB(10)

          SHLINTS(7)=SHLINTS(7)+YAC*YAC*XBC*XBC*EG*CA(6)*CB(5)
          SHLINTS(8)=SHLINTS(8)+YAC*YAC*YBC*YBC*EG*CA(6)*CB(6)
          SHLINTS(9)=SHLINTS(9)+YAC*YAC*ZBC*ZBC*EG*CA(6)*CB(7)
          SHLINTS(10)=SHLINTS(10)+YAC*YAC*XBC*YBC*EG*CA(6)*CB(8)
          SHLINTS(11)=SHLINTS(11)+YAC*YAC*XBC*ZBC*EG*CA(6)*CB(9)
          SHLINTS(12)=SHLINTS(12)+YAC*YAC*YBC*ZBC*EG*CA(6)*CB(10)

          SHLINTS(13)=SHLINTS(13)+ZAC*ZAC*XBC*XBC*EG*CA(7)*CB(5)
          SHLINTS(14)=SHLINTS(14)+ZAC*ZAC*YBC*YBC*EG*CA(7)*CB(6)
          SHLINTS(15)=SHLINTS(15)+ZAC*ZAC*ZBC*ZBC*EG*CA(7)*CB(7)
          SHLINTS(16)=SHLINTS(16)+ZAC*ZAC*XBC*YBC*EG*CA(7)*CB(8)
          SHLINTS(17)=SHLINTS(17)+ZAC*ZAC*XBC*ZBC*EG*CA(7)*CB(9)
          SHLINTS(18)=SHLINTS(18)+ZAC*ZAC*YBC*ZBC*EG*CA(7)*CB(10)

          SHLINTS(19)=SHLINTS(19)+XAC*YAC*XBC*XBC*EG*CA(8)*CB(5)
          SHLINTS(20)=SHLINTS(20)+XAC*YAC*YBC*YBC*EG*CA(8)*CB(6)
          SHLINTS(21)=SHLINTS(21)+XAC*YAC*ZBC*ZBC*EG*CA(8)*CB(7)
          SHLINTS(22)=SHLINTS(22)+XAC*YAC*XBC*YBC*EG*CA(8)*CB(8)
          SHLINTS(23)=SHLINTS(23)+XAC*YAC*XBC*ZBC*EG*CA(8)*CB(9)
          SHLINTS(24)=SHLINTS(24)+XAC*YAC*YBC*ZBC*EG*CA(8)*CB(10)

          SHLINTS(25)=SHLINTS(25)+XAC*ZAC*XBC*XBC*EG*CA(9)*CB(5)
          SHLINTS(26)=SHLINTS(26)+XAC*ZAC*YBC*YBC*EG*CA(9)*CB(6)
          SHLINTS(27)=SHLINTS(27)+XAC*ZAC*ZBC*ZBC*EG*CA(9)*CB(7)
          SHLINTS(28)=SHLINTS(28)+XAC*ZAC*XBC*YBC*EG*CA(9)*CB(8)
          SHLINTS(29)=SHLINTS(29)+XAC*ZAC*XBC*ZBC*EG*CA(9)*CB(9)
          SHLINTS(30)=SHLINTS(30)+XAC*ZAC*YBC*ZBC*EG*CA(9)*CB(10)

          SHLINTS(31)=SHLINTS(31)+YAC*ZAC*XBC*XBC*EG*CA(10)*CB(5)
          SHLINTS(32)=SHLINTS(32)+YAC*ZAC*YBC*YBC*EG*CA(10)*CB(6)
          SHLINTS(33)=SHLINTS(33)+YAC*ZAC*ZBC*ZBC*EG*CA(10)*CB(7)
          SHLINTS(34)=SHLINTS(34)+YAC*ZAC*XBC*YBC*EG*CA(10)*CB(8)
          SHLINTS(35)=SHLINTS(35)+YAC*ZAC*XBC*ZBC*EG*CA(10)*CB(9)
          SHLINTS(36)=SHLINTS(36)+YAC*ZAC*YBC*ZBC*EG*CA(10)*CB(10)
          end do ! Jgauss
          end do ! Igauss

          LIeqJ=.false.
          if(Iatmshl.eq.Jatmshl)LIeqJ=.true.
          INTC=0
          INDX1=0
          do I=1,Irange
            Jends=Jrange
            if(LIeqJ)Jends=I
            II=Iaos+I
            INTC=INDX1
            do J=1,Jends
              INTC=INTC+1
              JJ=Jaos+J
              INDX=II*(II-1)/2+JJ
              Vint(INDX)=EEPV(INTC)
              G_prod(INDX)=SHLINTS(INTC)
            end do ! J
            INDX1=INDX1+Jrange
          end do ! I

        end do ! JATMSHL
      end do ! IATMSHL
!
! End of routine I1ER_DD
      return
      end subroutine I1ER_DD
! END CONTAINS
      end subroutine I1E_Vint
