      MODULE mod_idfclc
!*****************************************************************************************************************
!     Date last modified:                                                                           Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Desciption: module used by the DF two-electron integral package                                            *
!*****************************************************************************************************************
!Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE type_basis_set
      USE QM_defaults

      implicit none
!
      integer :: MAXINT  ! Maximum number of integrals for for any 4 shells (NAOMAX**4)
      integer :: Ishell_start,Jshell_start,Kshell_start,Lshell_start
      integer :: Ishell_save,Jshell_save,Kshell_save,Lshell_save
      logical :: FIRST_loop
!
      integer Ifrst,Ilast,Ishell
      integer Jfrst,Jlast,Jshell
      integer Kfrst,Klast,Kshell
      integer Lfrst,Llast,Lshell
!
! Work arrays:
      double precision, dimension(:,:), allocatable :: SHRABSQ

! Global variables from module_df_integrals
      integer, public :: LAMAX,LBMAX,LCMAX,LDMAX
      integer, public :: LBMAX0,LCMAX0,LDMAX0
      integer, public :: Mtype,LENTQ
      integer, public :: IGEND,JGEND,KGEND,LGEND
      integer, public :: IGBGN,JGBGN,KGBGN,LGBGN
      integer, public :: AOI,AOJ,AOK,AOL !Used in IDFCLC1 and 2
      logical, public :: LTWOINT !Used in IDFCLC
      logical, public :: Lsort
! Arrays of contraction coefficients
      double precision :: MAXGexp
      double precision, public, dimension(20) :: GexpA,GexpB,GexpC,GexpD
      double precision, public, dimension(60) :: GccA,GccB,GccC,GccD
      double precision, dimension(20) :: GexpB0,GexpC0,GexpD0
      double precision, dimension(60) :: GccB0,GccC0,GccD0
      double precision,public,dimension(:),target,allocatable :: IJKLS
      double precision,public,dimension(:),target,allocatable :: IKJLS
      double precision,public,dimension(:),target,allocatable :: ILJKS
      double precision,public,dimension(:),pointer:: Int_pointer 
       
! public variables used in I2E_SPDF,def and specialized subroutines
      double precision, public :: XA,YA,ZA,XB,YB,ZB,XC,YC,ZC,XD,YD,ZD
      integer,public :: Irange,Jrange,Krange,Lrange
      integer,public :: Iatom,Jatom,Katom,Latom
      integer,public :: Jatom0,Katom0,Latom0
      integer,public :: Iatmshl,Jatmshl,Katmshl,Latmshl
      integer, public :: NgaussA,NgaussB,NgaussC,NgaussD
      integer, public :: NgaussB0,NgaussC0,NgaussD0
      double precision,public :: PIconst, EXPARG !Global
      double precision :: ABEXP,CDEXP
      
! public variables used in def and specialized subroutine         
      integer, public :: LABMAX,LCDMAX,LPQMAX
      integer, public :: NZERO
      integer, public :: Iend,Kend,Lend,Jend
      integer, public :: Kend0,Lend0,Jend0
      integer, public :: Istart,Jstart,Kstart,Lstart
      integer, public :: Jstart0,Kstart0,Lstart0
      logical, public :: LRABCD,LRAB,LRCD
      logical,public :: IIKK,Iatmshl_EQ_Katmshl,Jatmshl_EQ_Latmshl,Iatmshl_EQ_Jatmshl,IJIJ,Katmshl_EQ_Latmshl
      double precision, public :: RABSQ,RCDSQ
      double precision, public :: EABPmin,ECDPmin,EADPmin,EBCPmin,EACPmin,EBDPmin
      double precision, public :: EABPmin0,ECDPmin0

! private variables
      integer :: Case
!     integer, private :: IV,INTC,JendM,KendM,LendM,NJKL,NKL
!     integer, private :: Igauss,Jgauss,Kgauss,Lgauss
      integer, private :: IERROR
      integer :: CCA,CCB,CCC,CCD
!     integer, private :: IZERO
      double precision, public, dimension(:),allocatable:: TQprim
      double precision, public, dimension(:),allocatable:: TQ
      double precision :: TP(7),WP(7)!private        
!     double precision, private :: CC1,CC2,CC3 !moved from the module_DF_int
!     double precision, private :: AS,BS,CS,DS !moved from module_DF_int
!     double precision, private :: A2DF(174),CCPX(48),CCPY(48),CCPZ(48),CCQX(48),CCQY(48),CCQZ(48)

      parameter (MAXGexp=10)

      end module mod_idfclc
