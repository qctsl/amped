      subroutine INT_PRINT_manager (ObjName, Modality)
!********************************************************************************
!     Date last modified: April 24, 2008                           Version 2.0 *
!     Author: R.A. Poirier                                                      *
!     Description: Calls the appropriate routine to "Command" the object nameIN *
!     Command can be "PRINT" or "KILL"                                          *
!     Pwhat can be "OBJECT", "DOCUMENTATION" or "TABLE"                         *
!********************************************************************************
! Modules:
      USE program_files
      USE program_objects

      implicit none
!
! Input scalars:
      character*(*), intent(IN) :: ObjName,Modality
!
! Local scalars:
!
! Begin:
      call PRG_manager ('enter', 'INT_PRINT_manager', 'UTILITY')

      SelectObject:   select case (ObjName)
        case ('1EINT')
          select case (Modality)
            case ('AO')
              call PRT_1EINT ('AO')
            case ('MO')
            case ('VNE')
            case ('DIPOLE')
              call PRT_1EINT ('DIPOLE')
            case default
            write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                           '" for Modality "',Modality(1:len_trim(Modality)),'"'
            stop 'No such object'
          end select

        case ('VDIPOLE')
          select case (Modality)
            case ('AO')
              call PRT_1EINT ('VDIPOLE')
            case ('MO')
!             call PRT_1EINT ('VDIPOLE')
            case default
            write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                           '" for Modality "',Modality(1:len_trim(Modality)),'"'
            stop 'No such object'
          end select

        case ('MDIPOLE')
          select case (Modality)
            case ('AO')
              call PRT_1EINT ('MDIPOLE')
            case ('MO')
!             call PRT_1EINT ('MDIPOLE')
            case default
            write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                           '" for Modality "',Modality(1:len_trim(Modality)),'"'
            stop 'No such object'
          end select

        case ('2EINT')
          select case (Modality)
            case ('MO')
          end select

      case default
        write(UNIout,*)'INT_PRINT_manager>'
        write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                       '" for Modality "',Modality(1:len_trim(Modality)),'"'
      end select SelectObject
!
! end of function INT_PRINT_manager
      call PRG_manager ('exit', 'INT_PRINT_manager', 'UTILITY')
      return
      end subroutine INT_PRINT_manager
