      MODULE gamma_fn

      implicit none
!
      double precision :: RPITWO ! sqrt(pi)/2 = GA(2)
      double precision :: FMZERO(9) ! Fm(0) = 1/(2m+1), for t=0
      double precision :: GA(9) ! sqrt(pi)*((k-1)/2)!! = sqrt(pi)*(1/2)(3/2)(5/2)...(k-1)/2

! Derived type used as a storage, to avoid double indexing
      type:: FMT_type
        double precision :: FM0
        double precision :: FM1
        double precision :: FM2
        double precision :: FM3
        double precision :: FM4
        double precision :: FM5
        double precision :: FM6
      end type FMT_type
!
! Array with derived types
      type(FMT_type), dimension(:), allocatable :: FMT_val ! Values of FM(t)
      type(FMT_type), dimension(:), allocatable :: FMT_del ! Values of deltaFM(t)
      type(FMT_type), dimension(:), allocatable :: FMT_int ! Interpolation values

!
      end MODULE gamma_fn
