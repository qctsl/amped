      subroutine I2ECLC (ModalityIN)
!*******************************************************************************************************
!     Date last modified: June 6, 2000                                                                 *
!     Author: R.A. Poirier                                                                             *
!     Desciption:                                                                                      *
!*******************************************************************************************************
! Modules:
      USE program_files
      USE program_objects
      USE QM_defaults
      USE QM_objects
      USE INT_objects

      implicit none
!
! Input scalars:
      character*(*), intent(IN) :: ModalityIN
!
! Local scalars:
      integer(kind=8) :: IDFCNT,ISPCNT,I2ECNT  ! Total integrals calculted in IDFCLC, ISPCLC and total
!
! Begin:
      call PRG_manager ('enter', 'I2ECLC', 'UTILITY')
!
      ISPCNT=0
      IDFCNT=0
!     if(PRG_cycles.eq.0)call INI_I2E ! Only needs to be done once, improve - how????
      if(.not.LI2E_exist)call INI_I2E ! Only needs to be done once

      if(index(ModalityIN,'RAW').ne.0)then
        I2E_type='RAW'
        integral_combinations=.false.
      else if(index(ModalityIN,'COMBINATIONS').ne.0)then
        I2E_type='COMBINATIONS'
        integral_combinations=.true.
      else
        stop 'ERROR> I2ECLC: no modality provided, must %RAW or %COMBINATIONS'
      end if
!
      call I2EINI      ! must be done at every function evaluation
!
      IF(MUN_PRTLEV.GT.0)then
        write(UNIout,'(a,1PE9.2,a)') &
        'NOTE: INTEGRALS .LE. ',I2E_ACC,' (I2EACC) WERE NOT KEPT'
        write(UNIout,'(a,1PE9.2,a,1PE9.2)') &
        'Exponent cutoff used: ',I2E_EXPCUT,' PQCUT2 cutoff used: ',I2E_PQCUT2
      end if

      if(LDandC)then
        NTINT(1:8)=0
        write(UNIout,'(a)')'I2E_DandC not available yet'
!       call I2E_DandC
        if(MUN_PRTLEV.GT.0)call PRT_I2E_details ('IDFCLC', IDFCNT)
      else if(LPFockOP)then
        NTINT(1:8)=0
        call I2E_PFockOP
        if(MUN_PRTLEV.GT.0)call PRT_I2E_details ('I2E_PFockOP', IDFCNT)
      else

      IF(LI2EDF)then
        NTINT(1:8)=0
        call IDFCLC
        if(MUN_PRTLEV.GT.0)call PRT_I2E_details ('IDFCLC', IDFCNT)
      end if ! LI2EDF

      IF(LI2ESP)then
        NTINT(1:8)=0
        call ISPCLC
        if(MUN_PRTLEV.GT.0)call PRT_I2E_details ('ISPCLC', ISPCNT)
      end if ! LI2ESP

      end if ! LDandC

! Dump the last buffers
      if(integral_combinations)call I2E_COMB ! for INCORE integrals only
      if(Lall_ONDISK)then
        call I2E_PURGE
      end if

      IF(MUN_PRTLEV.GT.0)then
        I2ECNT=ISPCNT+IDFCNT
        write(UNIout,1010)I2ECNT,I2e_type(1:len_trim(I2e_type))
      end if
!
! Always prevent these INCORE integrals from being re-saved (only applies to a direct SCF)
      LSAVE_IIKK=.false.
      LSAVE_IJJJ=.false.
      LSAVE_IIIL=.false.
      LSAVE_IIII=.false.
!
 1010 FORMAT(' TOTAL OF ',I10,' TWO-ELECTRON INTEGRALS CALCULATED (',a,')'//)
!
! End of routine I2ECLC
      call PRG_manager ('exit', 'I2ECLC', 'UTILITY')
      return
      end
      subroutine INI_I2E
!*******************************************************************************************************
!     Date last modified: March 31, 2000                                                               *
!     Author: R.A. Poirier                                                                             *
!     Description: Set code defaults.                                                                  *
!*******************************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_basis_set
      USE QM_defaults
      USE QM_objects
      USE INT_objects

      implicit none
!
! Local scalars:
      integer IERROR
      logical :: Lerror
!
! Begin:
      call PRG_manager ('enter', 'INI_I2E', 'UTILITY')
!
! First determine if all integrals are done by IDFCLC or both IDFCLC and ISPCLC... (must be done first)
      if(Basis%NFDShells.gt.0)LI2EDF=.true.

      if(LDirect_SCF)then ! use IDFCLC only (for now)
        if(.not.associated(PM0))then ! Must have been associated before IDFCLC is called
          write(UNIOut,'(a)')'INI_I2E> For a direct SCF must not request integrals'
          call PRG_stop ('For a direct SCF must not request integrals')
        end if
      end if
!
      if(LDandC)then ! Use I2E_DandC
        LI2EDF=.false.
        LI2ESP=.false.
      end if

! Load lengths:
      call GET_I2E_Buffer_sizes
! Build file name (always??)
      call BLD_FileID
      I2I_BasicFileID(1:)=Basic_FileID(1:len_trim(Basic_FileID))
      if(index(tempdir, 'localscratch').ne.0)then
        I2I_BasicFileID(1:)=tempdir(1:len_trim(tempdir))//'/'//I2I_BasicFileID(1:len_trim(I2I_BasicFileID))
        write(6,*)'I2I_BasicFileID: ',I2I_BasicFileID(1:len_trim(I2I_BasicFileID))
        call flush(6)
       end if
      
      if (.not.Name_set) RUN_name(1:)=I2I_BasicFileID(1:len_trim(I2I_BasicFileID))
!
! Always allocate buffers for now: ONLY used by the SCF????
      if(Lall_ONDISK)then
        call I2E_FILE_OPEN ('I2E_IIKL', IIKL_MAX, IIKL_unit)
        call I2E_FILE_OPEN ('I2E_IJKJ', IJKJ_MAX, IJKJ_unit)
        call I2E_FILE_OPEN ('I2E_IJJL', IJJL_MAX, IJJL_unit)
        call I2E_FILE_OPEN ('I2E_IJKL', IJKL_MAX, IJKL_unit)
        call I2E_FILE_OPEN ('I2E_IIII', IIII_MAX, IIII_unit)
        if(allocated (IIKL_BUFFER))deallocate (IIKL_BUFFER)
        allocate (IIKL_BUFFER(IIKL_MAX))
        if(allocated (IJKJ_BUFFER))deallocate (IJKJ_BUFFER)
        allocate (IJKJ_BUFFER(IJKJ_MAX))
        if(allocated (IJJL_BUFFER))deallocate (IJJL_BUFFER)
        allocate (IJJL_BUFFER(IJJL_MAX))
        if(allocated (IJKL_BUFFER))deallocate (IJKL_BUFFER)
        allocate (IJKL_BUFFER(IJKL_MAX))
      else if(Lall_INCORE)then ! Do nothing
!
      else if(LDirect_SCF)then ! Only allocate buffers
        if(allocated (IIKL_BUFFER))deallocate (IIKL_BUFFER)
        allocate (IIKL_BUFFER(IIKL_MAX))
        if(allocated (IJKJ_BUFFER))deallocate (IJKJ_BUFFER)
        allocate (IJKJ_BUFFER(IJKJ_MAX))
        if(allocated (IJJL_BUFFER))deallocate (IJJL_BUFFER)
        allocate (IJJL_BUFFER(IJJL_MAX))
        if(allocated (IJKL_BUFFER))deallocate (IJKL_BUFFER)
        allocate (IJKL_BUFFER(IJKL_MAX))
      else ! Some ONDISK
      if(IIKL_store(1:6).EQ.'ONDISK')then
        call I2E_FILE_OPEN ('I2E_IIKL', IIKL_MAX, IIKL_unit)
        if(allocated (IIKL_BUFFER))deallocate (IIKL_BUFFER)
        allocate (IIKL_BUFFER(IIKL_MAX))
      end if
      if(IJKJ_store(1:6).EQ.'ONDISK')then
        call I2E_FILE_OPEN ('I2E_IJKJ', IJKJ_MAX, IJKJ_unit)
        if(allocated (IJKJ_BUFFER))deallocate (IJKJ_BUFFER)
        allocate (IJKJ_BUFFER(IJKJ_MAX))
      end if
      if(IJJL_store(1:6).EQ.'ONDISK')then
        call I2E_FILE_OPEN ('I2E_IJJL', IJJL_MAX, IJJL_unit)
        if(allocated (IJJL_BUFFER))deallocate (IJJL_BUFFER)
        allocate (IJJL_BUFFER(IJJL_MAX))
      end if
      if(IJKL_store(1:6).EQ.'ONDISK')then
        call I2E_FILE_OPEN ('I2E_IJKL', IJKL_MAX, IJKL_unit)
        if(allocated (IJKL_BUFFER))deallocate (IJKL_BUFFER)
        allocate (IJKL_BUFFER(IJKL_MAX))
      end if
      end if ! Lall_ONDISK
!
! Allocate the INCORE buffers
      if(allocated(IIII_INCORE))then !   All INCORE cases
        deallocate (IIII_INCORE, IIIL_INCORE, IJJJ_INCORE, IIKK_INCORE, stat=IERROR)
        deallocate (IJKJ_INCORE, IIKL_INCORE, IJJL_INCORE, stat=IERROR)
        deallocate (IJKL_INCORE, stat=IERROR)
      end if
     
      allocate (IIII_INCORE(IIII_MAX), IIIL_INCORE(IIIL_MAX), &
                IJJJ_INCORE(IJJJ_MAX), IIKK_INCORE(IIKK_MAX), stat=IERROR)
!
      if(IERROR.NE.0)then
        write(UNIout,'(a)')'ERROR> INI_I2E: IIII-IKIK_INCORE allocation error'
        write(UNIout,'(a)')'Maybe caused by BufferSize being too large - reduce and try again'
        stop 'ERROR> INI_I2E: _INCORE allocation error'
      end if

      allocate (IJKJ_INCORE(IJKJ_MAX), IIKL_INCORE(IIKL_MAX), IJJL_INCORE(IJJL_MAX), stat=IERROR)
      if(IERROR.NE.0)then
        write(UNIout,'(a)')'ERROR> INI_I2E: IIKL-ILJJ_INCORE allocation error'
        write(UNIout,'(a)')'Maybe caused by BufferSize being too large - reduce and try again'
        stop 'ERROR> INI_I2E: _INCORE allocation error'
      end if
!
      allocate (IJKL_INCORE(IJKL_MAX), stat=IERROR)
      if(IERROR.NE.0)then
        write(UNIout,'(a)')'ERROR> INI_I2E: IJKL_INCORE allocation error'
        write(UNIout,'(a)')'Maybe caused by BufferSize being too large - reduce and try again'
        stop 'ERROR> INI_I2E: _INCORE allocation error'
      end if
!
      LI2E_exist=.true.
      call RYSSET
!
      call PRG_manager ('exit', 'INI_I2E', 'UTILITY')
      return
      end
      subroutine GET_I2E_Buffer_sizes
!***********************************************************************
!     Date last modified: December 8, 1994                 Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: Defines the method used for handling the            *
!                  two-electron integrals, i.e. INCORE, ONDISK, ...    *
!***********************************************************************
! Modules:
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE INT_objects

      implicit none
!
! Local scalars:
      integer(kind=8) LENGTH1,LENGTH2,LENGTH3
!
! Begin:
      call PRG_manager ('enter', 'GET_I2E_Buffer_sizes', 'UTILITY')
!
      if(LDirect_SCF.and.Lall_ONDISK)then
        write(UNIout,'(a)')'WARNING> For a direct SCF the integrals cannot be all ONDISK'
        write(UNIout,'(a)')'WARNING> Lall_ONDISK set to false'
        Lall_ONDISK=.false.
      end if ! LDirect_SCF
!
! Determine the maximum number of integrals of each type:
      if(Nbasis.gt.3)then
        LENGTH1=Nbasis*(Nbasis-1)/2
        LENGTH2=LENGTH1*(Nbasis-2)/3
        LENGTH3=LENGTH2*(Nbasis-3)/4
      else
        LENGTH1=Nbasis
        LENGTH2=1
        LENGTH3=1
      end if
!
      if(Lall_ONDISK)then
        IIII_store='ONDISK'
        IIKL_store='ONDISK'
        IJKJ_store='ONDISK'
        IJJL_store='ONDISK'
        IJKL_store='ONDISK'
        IIKL_MAX=I2EBUF
        IJJL_MAX=I2EBUF
        IJKJ_MAX=I2EBUF
        IJKL_MAX=I2EBUF ! ????
        IIII_MAX=Nbasis
        IIIL_MAX=LENGTH1
        IJJJ_MAX=LENGTH1
        IIKK_MAX=LENGTH1
        if(mun_prtlev.gt.0)write(UNIout,'(a)')'All integrals will be kept ONDISK'
        Lall_INCORE=.false.
      else ! Check if can keep all INCORE
      if(LENGTH3.le.I2EBUF)then
        Lall_INCORE=.true.
        IJKJ_MAX=LENGTH2
        IIKL_MAX=LENGTH2
        IJJL_MAX=LENGTH2
        IJKL_MAX=LENGTH3 ! ????
        IIII_MAX=Nbasis
        IIIL_MAX=LENGTH1
        IJJJ_MAX=LENGTH1
        IIKK_MAX=LENGTH1
        if(mun_prtlev.gt.0.and.Lall_INCORE)write(UNIout,'(a)')'All integrals will be kept INCORE'
      else if(LDirect_SCF)then  ! Allocate buffers only
        Lall_INCORE=.false.
        IJKJ_MAX=I2EBUF
        IIKL_MAX=I2EBUF
        IJJL_MAX=I2EBUF
        IJKL_MAX=I2EBUF
        IIII_MAX=Nbasis
        IIIL_MAX=LENGTH1
        IJJJ_MAX=LENGTH1
        IIKK_MAX=LENGTH1
      else  ! Some INCORE and some ONDISK
        Lall_INCORE=.false.
        IJKJ_MAX=I2EBUF
        IIKL_MAX=I2EBUF
        IJJL_MAX=I2EBUF
        IJKL_MAX=I2EBUF
        IIII_MAX=Nbasis
        IIIL_MAX=LENGTH1
        IJJJ_MAX=LENGTH1
        IIKK_MAX=LENGTH1
        IIKL_store='ONDISK'
        IJKJ_store='ONDISK'
        IJJL_store='ONDISK'
        IJKL_store='ONDISK'
        LIJKL_ONDISK=.true.
        LIJKJ_ONDISK=.true.
        LIJJL_ONDISK=.true.
        LIIKL_ONDISK=.true.
      end if
      end if ! Lall_ONDISK
!
! End of routine GET_I2E_Buffer_sizes
      call PRG_manager ('exit', 'GET_I2E_Buffer_sizes', 'UTILITY')
      return
      end
      integer function GET_maxint ()
!*****************************************************************************************************************
!     Date last modified: February 16, 2000                                                         Version 2.0  *
!     Author: R. A. Poirier                                                                                      *
!     Description: Set up Basis.                                                                                 *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE type_basis_set

      implicit none
!
! Local scalars:
      integer Ibasis,Ishell,NAOMAX
!
! Begin:
      call PRG_manager ('enter', 'GET_maxint', 'UTILITY')
!
! Define NAOMAX
      NAOMAX=0
      do Ishell=1,Basis%Nshells
      IF(Basis%shell(Ishell)%Xtype.EQ.0)then
        Ibasis=1
      else if(Basis%shell(Ishell)%Xtype.EQ.1)then
        Ibasis=3
      else if(Basis%shell(Ishell)%Xtype.EQ.2)then
        Ibasis=Basis%NDORBS
      else if(Basis%shell(Ishell)%Xtype.EQ.3)then
        Ibasis=Basis%NFORBS
      end if ! Xtype
        NAOMAX=max0(NAOMAX, Ibasis)
      end do ! Ishell
      if(NAOMAX.eq.3)NAOMAX=4 ! for now
!
      if(NAOMAX.le.0)then
        write(UNIout,'(a,i8)')'ERROR> GET_maxint: NAOMAX = ',NAOMAX
        stop 'ERROR> GET_maxint'
      end if

      GET_MAXINT=NAOMAX*NAOMAX*NAOMAX*NAOMAX
!
! End of routine GET_maxint
      call PRG_manager ('exit', 'GET_maxint', 'UTILITY')
      return
      end function GET_maxint
      subroutine i2e_comb
!***********************************************************************
!     Date last modified: July 24, 1998                    Version 1.0 *
!     Author:                                                          *
!     Description: Form two-electron integral combinations.            *
!***********************************************************************
! Modules:
      USE QM_objects
      USE INT_objects

      implicit none
!
! Local scalars:
!
! Begin:
      call PRG_manager ('enter', 'I2E_COMB', 'UTILITY')
!
! INCORE buffers:
      IF(IJKL_count.GT.0)then
        call I2E_COMB_IJKL (IJKL_INCORE, IJKL_count)
      end if !IF(IJKL_count.GT.0)

      IF(IJKJ_count.GT.0)then
        call I2E_COMB_IJKJ (IJKJ_INCORE, IJKJ_count)
      end if !IF(IJKJ_count.GT.0)

      IF(IJJL_count.GT.0)then
        call I2E_COMB_IJJL (IJJL_INCORE, IJJL_count)
      end if !IF(IJJL_count.GT.0)

      IF(IIKL_count.GT.0)then
        call I2E_COMB_IIKL (IIKL_INCORE, IIKL_count)
      end if !IF(IIKL_count.GT.0)

      IF(IIKK_count.GT.0)then
        call I2E_COMB_IIKK (IIKK_INCORE, IIKK_count)
      end if !IF(IIKK_count.GT.0)
!
! End of routine i2e_comb
      call PRG_manager ('exit', 'I2E_COMB', 'UTILITY')
      return
      end
      subroutine I2EINI
!***********************************************************************
!     Date last modified: June 24, 1992                    Version 1.0 *
!     Author: F. Colonna and R.A. Poirier                              *
!     Description: Initialize two-electron integrals calculation       *
!***********************************************************************
! Modules:
      USE QM_defaults
      USE type_basis_set
      USE type_molecule
      USE QM_objects
      USE INT_objects
      USE mod_idfclc ! improve

      implicit none
!
! Local scalar:
!
! Begin:
      call PRG_manager ('enter', 'I2EINI', 'UTILITY')
!
! Initialize program_constants.
! These integrals are always saved INCORE (only applies to a direct SCF)
      LSAVE_IIKK=.true.
      LSAVE_IJJJ=.true.
      LSAVE_IIIL=.true.
      LSAVE_IIII=.true.
! Initialize starting points of loops (used by direct SCF, to skip INCORE integrals)
      Ishell_start=1
      Jshell_start=1
      Kshell_start=1
      Lshell_start=1
      Ishell_save=1
      Jshell_save=1
      Kshell_save=1
      Lshell_save=1
!
      LIJKL_ONDISK=.false.
      LIJKJ_ONDISK=.false.
      LIJJL_ONDISK=.false.
      LIIKL_ONDISK=.false.
      ANY_ONDISK=.false.
!     Lall_INCORE=.true.
!
! Rewind files
      if(IIII_store(1:6).eq.'ONDISK')rewind IIII_unit
      IF(IIKL_store(1:6).EQ.'ONDISK')rewind IIKL_unit
      IF(IJKJ_store(1:6).EQ.'ONDISK')rewind IJKJ_unit
      IF(IJJL_store(1:6).EQ.'ONDISK')rewind IJJL_unit
      IF(IJKL_store(1:6).EQ.'ONDISK')rewind IJKL_unit
!
! Initialize INCORE buffer counters.
      IIII_count=0
      IIIL_count=0
      IJJJ_count=0
      IIKK_count=0
      IJJL_count=0
      IJKJ_count=0
      IIKL_count=0
      IJKL_count=0
! Initialize buffer numbers.
      IJJL_Nbuffers=0
      IJKJ_Nbuffers=0
      IIKL_Nbuffers=0
      IJKL_Nbuffers=0
! Count the number of two-electron integrals.
!
! End of routine I2EINI
      call PRG_manager ('exit', 'I2EINI', 'UTILITY')
      return
      end
      subroutine I2E_COMB2 (ARRAY1, ARRAY2, type, LENGTH)
!***********************************************************************
!     Date last modified: August 3, 1998                 Version1 1.0  *
!     Author: R. A. Poirier                                            *
!     Description:                                                     *
!***********************************************************************
! Modules:
      USE program_constants

      implicit none
!
! Input scalars:
      integer, intent(IN) ::  LENGTH,type
!
! Input arrays:
      double precision ARRAY1(LENGTH),ARRAY2(LENGTH)
!
! Local scalars:
      integer I
      double precision A1,A2,C1,C2,C3,C4
!
! Begin:
      call PRG_manager ('enter', 'I2E_COMB2', 'UTILITY')
!
      IF(type.EQ.2.or.type.EQ.3)then
        C1=THREE
        C2=-ONE
        C3=-TWO
        C4=FOUR
      else if(type.EQ.4)then
        C1=TWO
        C2=-ONE
        C3=-ONE
        C4=THREE
      else if(type.EQ.1)then
        C1=FOUR
        C2=-TWO
        C3=-ONE
        C4=THREE
      end if
!
      do I=1,LENGTH
        A1=ARRAY1(I)
        A2=ARRAY2(I)
        ARRAY1(I)=C1*A1+C2*A2
        ARRAY2(I)=C3*A1+C4*A2
      end do
!
! End of routine I2E_COMB2
      call PRG_manager ('exit', 'I2E_COMB2', 'UTILITY')
      return
      end
      subroutine I2E_COMB3 (ARRAY1, ARRAY2, ARRAY3, LENGTH)
!***********************************************************************
!     Date last modified: August 3, 1998                 Version1 1.0  *
!     Author: R. A. Poirier                                            *
!     Description:                                                     *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalar:
      integer, intent(IN) :: LENGTH
!
! Input array:
      double precision ARRAY1(LENGTH),ARRAY2(LENGTH),ARRAY3(LENGTH)
!
! Local scalars:
      integer I
      double precision A1,A2,A3,FOUR
!
      parameter (FOUR=4.0D0)
!
! Begin:
      call PRG_manager ('enter', 'I2E_COMB3', 'UTILITY')
!
      do I=1,LENGTH
        A1=ARRAY1(I)
        A2=ARRAY2(I)
        A3=ARRAY3(I)
        ARRAY1(I)=FOUR*A1-(A2+A3)
        ARRAY2(I)=FOUR*A2-(A1+A3)
        ARRAY3(I)=FOUR*A3-(A1+A2)
      end do
!
! End of routine I2E_COMB3
      call PRG_manager ('exit', 'I2E_COMB3', 'UTILITY')
      return
      end
      subroutine I2E_PURGE
!***********************************************************************
!     Date last modified: July 24, 1998                    Version 1.0 *
!     Author:                                                          *
!     Description: Purge two-electron integral buffers.                *
!***********************************************************************
! Modules:
      USE QM_defaults
      USE QM_objects
      USE INT_objects

      implicit none
!
!
! Begin:
      call PRG_manager ('enter', 'I2E_PURGE', 'UTILITY')
!
      if(IJKL_count.gt.0)call WRI_ijkl (IJKL_INCORE, IJKL_MAX, IJKL_unit, IJKL_count)
      if(IIKL_count.gt.0)call WRI_iikl (IIKL_INCORE, IIKL_MAX, IIKL_unit, IIKL_count)
      if(IJKJ_count.gt.0)call WRI_ijkj (IJKJ_INCORE, IJKJ_MAX, IJKJ_unit, IJKJ_count)
      if(IJJL_count.gt.0)call WRI_ijjl (IJJL_INCORE, IJJL_MAX, IJJL_unit, IJJL_count)
      call WRI_iiii
!
! End of routine I2E_PURGE
      call PRG_manager ('exit', 'I2E_PURGE', 'UTILITY')
      return
      end
      subroutine I2E_DEALLOCATES
!***********************************************************************
!     Date last modified: May 11, 1995                     Version 1.0 *
!     Author: R. A. Poirier                                            *
!     Description: I2E_DEALLOCATES the arrays.                         *
!***********************************************************************
! Modules:
      USE QM_objects
      USE INT_objects

      implicit none
!
! Local scalar:
      integer IERROR
!
! Begin:
      call PRG_manager ('enter', 'I2E_DEALLOCATES', 'UTILITY')
!
! Deallocate integral buffers if allocated
      if(allocated(IIII_INCORE))then
        deallocate (IIII_INCORE, IIIL_INCORE, IJJJ_INCORE, IIKK_INCORE, IIKL_INCORE, IJJL_INCORE)
        deallocate (IJKL_INCORE, IJKJ_INCORE)
      end if
!
        if(allocated(IIKL_BUFFER))then
          deallocate (IIKL_BUFFER)
        end if
        if(allocated(IJKJ_BUFFER))then
          deallocate (IJKJ_BUFFER)
        end if
        if(allocated(IJJL_BUFFER))then
          deallocate (IJJL_BUFFER)
        end if
        if(allocated(IJKL_BUFFER))then
          deallocate (IJKL_BUFFER)
        end if
!
! End of routine I2E_DEALLOCATES
      call PRG_manager ('exit', 'I2E_DEALLOCATES', 'UTILITY')
      return
      end
      subroutine I2E_FILE_OPEN (I2ENAM,   & ! I2E name
                                Buffer_size,   & ! Buffer size
                                UNIT)    ! Output file unit
!***********************************************************************
!     Date last modified: April 18, 1995                   Version 1.0 *
!     Author: F. Colonna and R.A. Poirier                              *
!     Description: Open unit to store two-electron integrals and       *
!                  indices                                             *
!***********************************************************************
! Modules:
      USE program_files
      USE type_basis_set
      USE QM_objects
      USE INT_objects

      implicit none
!
! Input scalar:
      integer Buffer_size
      character*(*), intent(IN) :: I2ENAM
!
! Output scalars:
      integer, intent(OUT) :: UNIT
!
! Local scalars:
      logical LERROR,LEXIST
      integer lennam,lenfil,lenrun,lenstr
      character(len=MAX_namelen) FILNAM
!
! Begin:
      call PRG_manager ('enter', 'I2E_FILE_OPEN', 'UTILITY')
!
      lenrun=len_trim(I2I_BasicFileID)
!
      FILNAM=I2I_BasicFileID(1:lenrun)//'_'//I2ENAM(1:len_trim(I2ENAM))
      call GET_unit (FILNAM, unit, Lerror)
      PRG_file(unit)%name=FILNAM
      PRG_file(unit)%form='UNFORMATTED'
      PRG_file(unit)%unit=unit
      PRG_file(unit)%type='BINARY'
      PRG_file(unit)%status='NEW'
      PRG_file(unit)%action='READWRITE'
      call FILE_open (unit)
!
      rewind unit
!
! End of routine I2E_FILE_OPEN
      call PRG_manager ('exit', 'I2E_FILE_OPEN', 'UTILITY')
      return
      end
      subroutine I2E_FILE_CLOSE
!***********************************************************************
!     Date last modified: April 18, 1995                   Version 1.0 *
!     Author: F. Colonna and R.A. Poirier                              *
!     Description: Open unit to store two-electron integrals and       *
!                  indices                                             *
!***********************************************************************
! Modules:
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE INT_objects

      implicit none
!
! Local scalars:
      integer lennam,lenrun
      logical LERROR
      character(len=MAX_namelen) FILNAM
!
! Begin:
      call PRG_manager ('enter', 'I2E_FILE_CLOSE', 'UTILITY')
!
      lenrun=len_trim(I2I_BasicFileID)
      FILNAM=I2I_BasicFileID(1:lenrun)//'_I2E_IIKL'
      call FILE_delete (FILNAM, lerror)

      FILNAM=I2I_BasicFileID(1:lenrun)//'_I2E_IJKJ'
      call FILE_delete (FILNAM, lerror)

      FILNAM=I2I_BasicFileID(1:lenrun)//'_I2E_IJJL'
      call FILE_delete (FILNAM, lerror)

      FILNAM=I2I_BasicFileID(1:lenrun)//'_I2E_IJKL'
      call FILE_delete (FILNAM, lerror)

      FILNAM=I2I_BasicFileID(1:lenrun)//'_I2E_IIII'
      call FILE_delete (FILNAM, lerror)
!
! End of routine I2E_FILE_CLOSE
      call PRG_manager ('exit', 'I2E_FILE_CLOSE', 'UTILITY')
      return
      end
      subroutine I2E_FILE_DELETE
!***********************************************************************
!     Date last modified: April 18, 1995                   Version 1.0 *
!     Author: F. Colonna and R.A. Poirier                              *
!     Description: Open unit to store two-electron integrals and       *
!                  indices                                             *
!***********************************************************************
! Modules:
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE INT_objects

      implicit none
!
! Local scalars:
      integer lennam,lenrun
      logical LERROR
      character(len=MAX_namelen) FILNAM
!
! Begin:
      call PRG_manager ('enter', 'I2E_FILE_DELETE', 'UTILITY')
!
      if(IIKL_store(1:6).eq.'ONDISK')then
      lenrun=len_trim(I2I_BasicFileID)
      FILNAM=I2I_BasicFileID(1:lenrun)//'_I2E_IIKL'
      call FILE_delete (FILNAM, lerror)
      end if

      if(IJKJ_store(1:6).eq.'ONDISK')then
      FILNAM=I2I_BasicFileID(1:lenrun)//'_I2E_IJKJ'
      call FILE_delete (FILNAM, lerror)
      end if

      if(IJJL_store(1:6).eq.'ONDISK')then
      FILNAM=I2I_BasicFileID(1:lenrun)//'_I2E_IJJL'
      call FILE_delete (FILNAM, lerror)
      end if

      if(IJKL_store(1:6).eq.'ONDISK')then
      FILNAM=I2I_BasicFileID(1:lenrun)//'_I2E_IJKL'
      call FILE_delete (FILNAM, lerror)
      end if

      if(IIII_store(1:6).eq.'ONDISK')then
      FILNAM=I2I_BasicFileID(1:lenrun)//'_I2E_IIII'
      call FILE_delete (FILNAM, lerror)
      end if
!
! End of routine I2E_FILE_DELETE
      call PRG_manager ('exit', 'I2E_FILE_DELETE', 'UTILITY')
      return
      end
      subroutine I2E_FILES
!***********************************************************************
!     Date last modified: April 18, 1995                   Version 1.0 *
!     Author: F. Colonna and R.A. Poirier                              *
!     Description: Open unit to store two-electron integrals and       *
!                  indices                                             *
!***********************************************************************
! Modules:
      USE program_files
      USE INT_objects

      implicit none
!
! Local scalars:
!
! Begin:
      call PRG_manager ('enter', 'I2E_FILES', 'UTILITY')
!
      if(Lall_ONDISK)then ! Do not delete the files if all all the integrals are kept on disk
        write(UNIout,'(a)')'Two-electron integrals are kept since Lall_ONDISK=true.'
      else
        call I2E_FILE_DELETE
      end if
!
! End of routine I2E_FILES
      call PRG_manager ('exit', 'I2E_FILES', 'UTILITY')
      return
      end subroutine I2E_FILES
      subroutine I2E_COMB_ijkl (IJKL_IN, LENGTH)
!***********************************************************************
!     Date last modified: August 3, 1998                 Version1 1.0  *
!     Author: R. A. Poirier                                            *
!     Description:                                                     *
!***********************************************************************
! Modules:
      USE QM_objects
      USE INT_objects

      implicit none
!
! Input scalar:
      integer, intent(IN) :: LENGTH
!
! Input array:
      type (ints_2e_ijkl) IJKL_IN(LENGTH)
!
! Local scalars:
      integer :: I
      double precision :: A1,A2,A3,FOUR
!
      parameter (FOUR=4.0D0)
!
! Begin:
      call PRG_manager ('enter', 'I2E_COMB_ijkl', 'UTILITY')
!
      do I=1,LENGTH
        A1=IJKL_IN(I)%IJKL
        A2=IJKL_IN(I)%IKJL
        A3=IJKL_IN(I)%ILJK
        IJKL_IN(I)%IJKL=FOUR*A1-(A2+A3)
        IJKL_IN(I)%IKJL=FOUR*A2-(A1+A3)
        IJKL_IN(I)%ILJK=FOUR*A3-(A1+A2)
      end do
!
! End of routine I2E_COMB_ijkl
      call PRG_manager ('exit', 'I2E_COMB_ijkl', 'UTILITY')
      return
      end
      subroutine I2E_COMB_IJKJ (ARRAY, LENGTH)
!***********************************************************************
!     Date last modified: August 3, 1998                 Version1 1.0  *
!     Author: R. A. Poirier                                            *
!     Description:                                                     *
!***********************************************************************
! Modules:
      USE program_constants
      USE QM_objects
      USE INT_objects

      implicit none
!
! Input scalars:
      integer, intent(IN) :: LENGTH
!
! Input arrays:
      type (ints_2e_ijkj) ARRAY(LENGTH)
!
! Local scalars:
      integer I
      double precision A1,A2,C1,C2,C3,C4
!
! Begin:
      call PRG_manager ('enter', 'I2E_COMB_IJKJ', 'UTILITY')
!
      C1=THREE
      C2=-ONE
      C3=-TWO
      C4=FOUR
!
      do I=1,LENGTH
        A1=ARRAY(I)%ijkj
        A2=ARRAY(I)%ikjj
        ARRAY(I)%ijkj=C1*A1+C2*A2
        ARRAY(I)%ikjj=C3*A1+C4*A2
      end do
!
! End of routine I2E_COMB_IJKJ
      call PRG_manager ('exit', 'I2E_COMB_IJKJ', 'UTILITY')
      return
      end
      subroutine I2E_COMB_IJJL (ARRAY, LENGTH)
!***********************************************************************
!     Date last modified: August 3, 1998                 Version1 1.0  *
!     Author: R. A. Poirier                                            *
!     Description:                                                     *
!***********************************************************************
! Modules:
      USE program_constants
      USE QM_objects
      USE INT_objects

      implicit none
!
! Input scalars:
      integer, intent(IN) :: LENGTH
!
! Input arrays:
      type (ints_2e_ijjl) ARRAY(LENGTH)
!
! Local scalars:
      integer I
      double precision A1,A2,C1,C2,C3,C4
!
! Begin:
      call PRG_manager ('enter', 'I2E_COMB_IJJL', 'UTILITY')
!
      C1=THREE
      C2=-ONE
      C3=-TWO
      C4=FOUR
!
      do I=1,LENGTH
        A1=ARRAY(I)%ijjl
        A2=ARRAY(I)%iljj
        ARRAY(I)%ijjl=C1*A1+C2*A2
        ARRAY(I)%iljj=C3*A1+C4*A2
      end do
!
! End of routine I2E_COMB_IJJL
      call PRG_manager ('exit', 'I2E_COMB_IJJL', 'UTILITY')
      return
      end
      subroutine I2E_COMB_IIKK (ARRAY, LENGTH)
!***********************************************************************
!     Date last modified: August 3, 1998                 Version1 1.0  *
!     Author: R. A. Poirier                                            *
!     Description:                                                     *
!***********************************************************************
! Modules:
      USE program_constants
      USE QM_objects
      USE INT_objects

      implicit none
!
! Input scalars:
      integer, intent(IN) :: LENGTH
!
! Input arrays:
      type (ints_2e_iikk) ARRAY(LENGTH)
!
! Local scalars:
      integer I
      double precision A1,A2,C1,C2,C3,C4
!
! Begin:
      call PRG_manager ('enter', 'I2E_COMB_IIKK', 'UTILITY')
!
      C1=TWO
      C2=-ONE
      C3=-ONE
      C4=THREE
!
      do I=1,LENGTH
        A1=ARRAY(I)%iikk
        A2=ARRAY(I)%ikik
        ARRAY(I)%iikk=C1*A1+C2*A2
        ARRAY(I)%ikik=C3*A1+C4*A2
      end do
!
! End of routine I2E_COMB_IIKK
      call PRG_manager ('exit', 'I2E_COMB_IIKK', 'UTILITY')
      return
      end
      subroutine I2E_COMB_IIKL (ARRAY, LENGTH)
!***********************************************************************
!     Date last modified: August 3, 1998                 Version1 1.0  *
!     Author: R. A. Poirier                                            *
!     Description:                                                     *
!***********************************************************************
! Modules:
      USE program_constants
      USE QM_objects
      USE INT_objects

      implicit none
!
! Input scalars:
      integer, intent(IN) :: LENGTH
!
! Input arrays:
      type (ints_2e_iikl) ARRAY(LENGTH)
!
! Local scalars:
      integer I
      double precision A1,A2,C1,C2,C3,C4
!
! Begin:
      call PRG_manager ('enter', 'I2E_COMB_IIKL', 'UTILITY')
!
      C1=FOUR
      C2=-TWO
      C3=-ONE
      C4=THREE
!
      do I=1,LENGTH
        A1=ARRAY(I)%iikl
        A2=ARRAY(I)%ikil
        ARRAY(I)%iikl=C1*A1+C2*A2
        ARRAY(I)%ikil=C3*A1+C4*A2
      end do
!
! End of routine I2E_COMB_IIKL
      call PRG_manager ('exit', 'I2E_COMB_IIKL', 'UTILITY')
      return
      end
      subroutine WRI_iiii
!***********************************************************************
!     Date last modified: April 12, 2004                   Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: Write the integrals normally kept INCORE, IIII-IIKK *
!***********************************************************************
! Modules:
      USE program_files
      USE QM_objects
      USE INT_objects

      implicit none
!
! Input scalars:
!
! Input array:
!
! Begin:
      call PRG_manager ('enter', 'WRI_iiii', 'UTILITY')
!
      if(IIII_unit.gt.0)then
        write(IIII_unit)IIII_count
        write(IIII_unit)IIII_INCORE
        write(IIII_unit)IIIL_count
        write(IIII_unit)IIIL_INCORE
        write(IIII_unit)IJJJ_count
        write(IIII_unit)IJJJ_INCORE
        write(IIII_unit)IIKK_count
        write(IIII_unit)IIKK_INCORE
      else
        write(UNIout,'(a,i10)')'ERROR> WRI_iiii: invalid unit, UNIT= ',IIII_unit
        stop 'ERROR> WRI_iiii: invalid unit'
      end if
!
! End of routine WRI_iiii
      call PRG_manager ('exit', 'WRI_iiii', 'UTILITY')
      return
      end
      subroutine READ_iiii
!***********************************************************************
!     Date last modified: April 13, 2004                   Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: Read an array of length LENGTH from unit UNIT.      *
!                  UNFORMATTED file.                                   *
!***********************************************************************
! Modules:
      USE program_files
      USE QM_objects
      USE INT_objects

      implicit none
!
! Input array:
!
! Output scalars:
      logical :: EOF
!
! Local scalars:
      integer :: IOSTAT,IOstatus1,IOstatus2
      logical :: Lerror
!
! Begin:
      call PRG_manager ('enter', 'READ_iiii', 'UTILITY')
!
      Lerror=.false.
      read(IIII_unit, IOSTAT=IOstatus1)IIII_count
      if(IOstatus1.eq.0)then
        read(IIII_unit, IOSTAT=IOstatus2)IIII_INCORE
        if(IOstatus2.ne.0)then
          Lerror=.true.
        end if
      else if(IOstatus1.lt.0)then
        EOF=.true.
      end if
      read(IIII_unit, IOSTAT=IOstatus1)IIIL_count
      if(IOstatus1.eq.0)then
        read(IIII_unit, IOSTAT=IOstatus2)IIIL_INCORE
        if(IOstatus2.ne.0)then
          Lerror=.true.
        end if
      else if(IOstatus1.lt.0)then
        EOF=.true.
      end if
      read(IIII_unit, IOSTAT=IOstatus1)IJJJ_count
      if(IOstatus1.eq.0)then
        read(IIII_unit, IOSTAT=IOstatus2)IJJJ_INCORE
        if(IOstatus2.ne.0)then
          Lerror=.true.
        end if
      else if(IOstatus1.lt.0)then
        EOF=.true.
      end if
      read(IIII_unit, IOSTAT=IOstatus1)IIKK_count
      if(IOstatus1.eq.0)then
        read(IIII_unit, IOSTAT=IOstatus2)IIKK_INCORE
        if(IOstatus2.ne.0)then
          Lerror=.true.
        end if
      else if(IOstatus1.lt.0)then
        EOF=.true.
      end if
!
      if(Lerror)then
        write(UNIout,'(a)')'ERROR> READ_iiii:'
        write(UNIout,'(a,l2)')'end_of_file =',EOF
        write(UNIout,'(a,l2)')'error =',Lerror
        write(UNIout,'(a)')'> end_of_file reset to TRUE'
        EOF=.TRUE.
        write(UNIout,'(a,I10)')'unit =',IIII_unit
        stop 'ERROR> READ_iiii: Error has occured while reading this file'
      end if
!
! End of routine READ_iiii
      call PRG_manager ('exit', 'READ_iiii', 'UTILITY')
      return
      end
      subroutine WRI_ijkl (ARRAY, &
                           BUFSIZ,  & ! is the array dimension
                           UNIT,    & ! File unit
                           LENGTH) ! Number of elements
!***********************************************************************
!     Date last modified: January 17, 2002                 Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: Write an array of length LENGTH on unit UNIT.       *
!                  UNFORMATTED file.                                   *
!***********************************************************************
! Modules:
      USE program_files
      USE QM_objects
      USE INT_objects

      implicit none
!
! Input scalars:
      integer, intent(IN) :: BUFSIZ,LENGTH,UNIT
!
! Input array:
      type (ints_2e_ijkl) ARRAY(BUFSIZ)
!
! Local scalars:
      integer :: Iiostat
!
! Begin:
      call PRG_manager ('enter', 'WRI_ijkl', 'UTILITY')
!
      if(UNIT.gt.0)then
!!$OMP CRITICAL I_O
        write(UNIT,iostat=Iiostat)LENGTH
        write(UNIT,iostat=Iiostat)ARRAY
        if(Iiostat.ne.0)then
          write(UNIout,'(a,i10)')'ERROR> WRI_ijkl iostat= ',Iiostat
         stop 'ERROR> WRI_ijkl: iostat error'
        end if
!!$OMP end CRITICAL I_O
      else
        write(UNIout,'(a,i10)')'ERROR> WRI_ijkl: invalid unit, UNIT= ',UNIT
        stop 'ERROR> WRI_ijkl: invalid unit'
      end if
!
! End of routine WRI_ijkl
      call PRG_manager ('exit', 'WRI_ijkl', 'UTILITY')
      return
      end
      subroutine READ_ijkl (ARRAY, &
                            BUFSIZ,  & ! is the array dimension
                            UNIT,    & !
                            LENGTH,  & ! Number of elements
                            EOF)    ! End of File
!***********************************************************************
!     Date last modified: January 17, 2002                 Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: Read an array of length LENGTH from unit UNIT.      *
!                  UNFORMATTED file.                                   *
!***********************************************************************
! Modules:
      USE program_files
      USE QM_objects
      USE INT_objects

      implicit none
!
! Input scalars:
      integer, intent(IN) :: BUFSIZ,UNIT
!
! Input array:
      type (ints_2e_ijkl) ARRAY(BUFSIZ)
!
! Output scalars:
      integer,intent(OUT) :: LENGTH
      logical :: EOF
!
! Local scalars:
      integer :: IOSTAT,IOstatus1,IOstatus2
!
! Begin:
      call PRG_manager ('enter', 'READ_ijkl', 'UTILITY')
!
      read(UNIT,IOSTAT=IOstatus1)LENGTH
      if(IOstatus1.eq.0)then
        read(UNIT,IOSTAT=IOstatus2)ARRAY
        if(IOstatus2.ne.0)then
          write(UNIout,'(a)')'ERROR> READ_ijkl:'
          write(UNIout,'(a,l2)')'end_of_file =',EOF
          write(UNIout,'(a,l2)')'error =',IOstatus2
          write(UNIout,'(a)')'> end_of_file reset to TRUE'
          EOF=.TRUE.
          write(UNIout,'(a,I10)')'unit =',unit
          write(UNIout,'(a,I10)')'array length =',length
          stop 'ERROR> READ_ijkl: Error has occured while reading this file'
        end if
      else if(IOstatus1.lt.0)then
        EOF=.true.
      else if(IOstatus1.gt.0)then
        write(UNIout,'(a,I10)')'unit =',unit
        write(UNIout,'(a,I10)')'array length =',length
        stop 'ERROR> READ_ijkl: Error has occured while reading length'
      end if ! (IOstatus1.eq.0)
!
! End of routine READ_ijkl
      call PRG_manager ('exit', 'READ_ijkl', 'UTILITY')
      return
      end
      subroutine WRI_iikl (ARRAY, &
                           BUFSIZ,  & ! is the array dimension
                           UNIT, &
                           LENGTH) ! Number of elements
!***********************************************************************
!     Date last modified: January 17, 2002                 Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: Write an array of length LENGTH on unit UNIT.       *
!                  UNFORMATTED file.                                   *
!***********************************************************************
! Modules:
      USE program_files
      USE QM_objects
      USE INT_objects

      implicit none
!
! Input scalars:
      integer, intent(IN) :: BUFSIZ,LENGTH,UNIT
!
! Input array:
      type (ints_2e_iikl) ARRAY(BUFSIZ)
!
! Local scalars:
      integer :: Iiostat
!
! Begin:
      call PRG_manager ('enter', 'WRI_iikl', 'UTILITY')
!
      if(UNIT.gt.0)then
        write(UNIT,iostat=Iiostat)LENGTH
        write(UNIT,iostat=Iiostat)ARRAY
        if(Iiostat.ne.0)then
          write(UNIout,'(a,i10)')'ERROR> WRI_ijkl iostat= ',Iiostat
         stop 'ERROR> WRI_ijkl: iostat error'
        end if
      else
        write(UNIout,'(a,i10)')'ERROR> WRI_iikl: invalid unit, UNIT= ',UNIT
        stop 'ERROR> WRI_iikl: invalid unit'
      end if
!
! End of routine WRI_iikl
      call PRG_manager ('exit', 'WRI_iikl', 'UTILITY')
      return
      end
      subroutine WRI_ijkj (ARRAY, &
                           BUFSIZ,  & ! is the array dimension
                           UNIT, &
                           LENGTH) ! Number of elements
!***********************************************************************
!     Date last modified: January 17, 2002                 Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: Write an array of length LENGTH on unit UNIT.       *
!                  UNFORMATTED file.                                   *
!***********************************************************************
! Modules:
      USE program_files
      USE QM_objects
      USE INT_objects

      implicit none
!
! Input scalars:
      integer, intent(IN) :: BUFSIZ,LENGTH,UNIT
!
! Input array:
      type (ints_2e_ijkj) ARRAY(BUFSIZ)
!
! Local scalars:
      integer :: Iiostat
!
! Begin:
      call PRG_manager ('enter', 'WRI_ijkj', 'UTILITY')
!
      if(UNIT.gt.0)then
        write(UNIT,iostat=Iiostat)LENGTH
        write(UNIT,iostat=Iiostat)ARRAY
        if(Iiostat.ne.0)then
          write(UNIout,'(a,i10)')'ERROR> WRI_ijkl iostat= ',Iiostat
         stop 'ERROR> WRI_ijkl: iostat error'
        end if
      else
        write(UNIout,'(a,i10)')'ERROR> WRI_ijkj: invalid unit, UNIT= ',UNIT
        stop 'ERROR> WRI_ijkj: invalid unit'
      end if
!
! End of routine WRI_ijkj
      call PRG_manager ('exit', 'WRI_ijkj', 'UTILITY')
      return
      end
      subroutine WRI_ijjl (ARRAY, &
                           BUFSIZ,  & ! is the array dimension
                           UNIT, &
                           LENGTH) ! Number of elements
!***********************************************************************
!     Author: R.A. Poirier                                             *
!     Description: Write an array of length LENGTH on unit UNIT.       *
!                  UNFORMATTED file.                                   *
!***********************************************************************
! Modules:
      USE program_files
      USE QM_objects
      USE INT_objects

      implicit none
!
! Input scalars:
      integer, intent(IN) :: BUFSIZ,LENGTH,UNIT
!
! Input array:
      type (ints_2e_ijjl) ARRAY(BUFSIZ)
!
! Local scalars:
      integer :: Iiostat
!
! Begin:
      call PRG_manager ('enter', 'WRI_ijjl', 'UTILITY')
!
      if(UNIT.gt.0)then
        write(UNIT,iostat=Iiostat)LENGTH
        write(UNIT,iostat=Iiostat)ARRAY
        if(Iiostat.ne.0)then
          write(UNIout,'(a,i10)')'ERROR> WRI_ijkl iostat= ',Iiostat
         stop 'ERROR> WRI_ijkl: iostat error'
        end if
      else
        write(UNIout,'(a,i10)')'ERROR> WRI_ijjl: invalid unit, UNIT= ',UNIT
        stop 'ERROR> WRI_ijjl: invalid unit'
      end if
!
! End of routine WRI_ijjl
      call PRG_manager ('exit', 'WRI_ijjl', 'UTILITY')
      return
      end
      subroutine READ_iikl (ARRAY, &
                            BUFSIZ,  & ! is the array dimension
                            UNIT,    & !
                            LENGTH,  & ! Number of elements
                            EOF)
!***********************************************************************
!     Date last modified: January 17, 2002                 Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: Read an array of length LENGTH from unit UNIT.      *
!                  UNFORMATTED file.                                   *
!***********************************************************************
! Modules:
      USE program_files
      USE QM_objects
      USE INT_objects

      implicit none
!
! Input scalars:
      integer, intent(IN) :: BUFSIZ,UNIT
!
! Input array:
      type (ints_2e_iikl) ARRAY(BUFSIZ)
!
! Output scalars:
      integer, intent(OUT) :: LENGTH
      logical EOF
!
! Local scalars:
      integer :: IOSTAT,IOstatus1,IOstatus2
!
! Begin:
      call PRG_manager ('enter', 'READ_iikl', 'UTILITY')
!
      read(UNIT,IOSTAT=IOstatus1)LENGTH
      if(IOstatus1.eq.0)then
        read(UNIT,IOSTAT=IOstatus2)ARRAY
        if(IOstatus2.ne.0)then
        write(UNIout,'(a)')'ERROR> READ_iikl:'
        write(UNIout,'(a,l2)')'end_of_file =',EOF
        write(UNIout,'(a,l2)')'error =',IOstatus2
        write(UNIout,'(a)')'> end_of_file reset to TRUE'
        EOF=.TRUE.
        write(UNIout,'(a,I10)')'unit =',unit
        write(UNIout,'(a,I10)')'array length =',length
        stop 'ERROR> READ_iikl: Error has occured while reading this file'
        end if
      else if(IOstatus1.lt.0)then
        EOF=.true.
      else if(IOstatus1.gt.0)then
        write(UNIout,'(a,I10)')'unit =',unit
        write(UNIout,'(a,I10)')'array length =',length
        stop 'ERROR> READ_iikl: Error has occured while reading length'
      end if ! (IOstatus1.eq.0)
!
! End of routine READ_iikl
      call PRG_manager ('exit', 'READ_iikl', 'UTILITY')
      return
      end
      subroutine READ_ijkj (ARRAY, &
                            BUFSIZ,  & ! is the array dimension
                            UNIT,    & !
                            LENGTH,  & ! Number of elements
                            EOF)
!***********************************************************************
!     Date last modified: January 17, 2002                 Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: Read an array of length LENGTH from unit UNIT.      *
!                  UNFORMATTED file.                                   *
!***********************************************************************
! Modules:
      USE program_files
      USE QM_objects
      USE INT_objects

      implicit none
!
! Input scalars:
      integer, intent(IN) :: BUFSIZ,UNIT
!
! Input array:
      type (ints_2e_ijkj) ARRAY(BUFSIZ)
!
! Output scalars:
      integer,intent(OUT) :: LENGTH
      logical EOF
!
! Local scalars:
      integer :: IOSTAT,IOstatus1,IOstatus2
!
! Begin:
      call PRG_manager ('enter', 'READ_ijkj', 'UTILITY')
!
      read(UNIT,IOSTAT=IOstatus1)LENGTH
      if(IOstatus1.eq.0)then
        read(UNIT,IOSTAT=IOstatus2)ARRAY
        if(IOstatus2.ne.0)then
        write(UNIout,'(a)')'ERROR> READ_ijkj:'
        write(UNIout,'(a,l2)')'end_of_file =',EOF
        write(UNIout,'(a,l2)')'error =',IOstatus2
        write(UNIout,'(a)')'> end_of_file reset to TRUE'
        EOF=.TRUE.
        write(UNIout,'(a,I10)')'unit =',unit
        write(UNIout,'(a,I10)')'array length =',length
        stop 'ERROR> READ_ijkj: Error has occured while reading this file'
        end if
      else if(IOstatus1.lt.0)then
        EOF=.true.
      else if(IOstatus1.gt.0)then
        write(UNIout,'(a,I10)')'unit =',unit
        write(UNIout,'(a,I10)')'array length =',length
        stop 'ERROR> READ_ijkj: Error has occured while reading length'
      end if ! (IOstatus1.eq.0)
!
! End of routine READ_ijkj
      call PRG_manager ('exit', 'READ_ijkj', 'UTILITY')
      return
      end
      subroutine READ_ijjl (ARRAY, &
                            BUFSIZ,  & ! is the array dimension
                            UNIT,    & !
                            LENGTH,  & ! Number of elements
                            EOF)
!***********************************************************************
!     Date last modified: January 17, 2002                 Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: Read an array of length LENGTH from unit UNIT.      *
!                  UNFORMATTED file.                                   *
!***********************************************************************
! Modules:
      USE program_files
      USE QM_objects
      USE INT_objects

      implicit none
!
! Input scalars:
      integer, intent(IN) :: BUFSIZ,UNIT
!
! Input array:
      type (ints_2e_ijjl) ARRAY(BUFSIZ)
!
! Output scalars:
      integer,intent(OUT) :: LENGTH
      logical EOF
!
! Local scalars:
      integer :: IOSTAT,IOstatus1,IOstatus2
!
! Begin:
      call PRG_manager ('enter', 'READ_ijjl', 'UTILITY')
!
      read(UNIT,IOSTAT=IOstatus1)LENGTH
      if(IOstatus1.eq.0)then
        read(UNIT,IOSTAT=IOstatus2)ARRAY
        if(IOstatus2.ne.0)then
        write(UNIout,'(a)')'ERROR> READ_ijjl:'
        write(UNIout,'(a,l2)')'end_of_file =',EOF
        write(UNIout,'(a,l2)')'error =',IOstatus2
        write(UNIout,'(a)')'> end_of_file reset to TRUE'
        EOF=.TRUE.
        write(UNIout,'(a,I10)')'unit =',unit
        write(UNIout,'(a,I10)')'array length =',length
        stop 'ERROR> READ_ijjl: Error has occured while reading this file'
        end if
      else if(IOstatus1.lt.0)then
        EOF=.true.
      else if(IOstatus1.gt.0)then
        write(UNIout,'(a,I10)')'unit =',unit
        write(UNIout,'(a,I10)')'array length =',length
        stop 'ERROR> READ_ijjl: Error has occured while reading length'
      end if ! (IOstatus1.eq.0)
!
! End of routine READ_ijjl
      call PRG_manager ('exit', 'READ_ijjl', 'UTILITY')
      return
      end
      subroutine PRT_I2E_details (Routine, i2ecnt)
!***********************************************************************
!     Date last modified: April 13, 2006                   Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: Print the number of integrals calculated by integral*
!                  routine "Routine"                                   *
!***********************************************************************
! Modules:
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE INT_objects

      implicit none
!
! Input scalars:
      integer(kind=8), intent(OUT) :: i2ecnt
      character(*), intent(IN) :: Routine
!
! Local scalars:
!
! Begin:
        IF(MUN_PRTLEV.GT.0)then ! Print the total number of integrals calculated in ISPCLC.
          i2ecnt=NTINT(1)+NTINT(2)+NTINT(3)+NTINT(4)+NTINT(5)+NTINT(6)+NTINT(7)+NTINT(8)
          write(UNIout,1001)i2ecnt,Routine(1:6),I2e_type(1:len_trim(I2e_type))
          write(UNIout,1040)NTINT(1:8)
          write(UNIout,'(a)')'Number of integrals in INCORE buffers:'
          write(UNIout,'(3(a,i10))')'IIKK: ',IIKK_count,' IJJL: ',IJJL_count,' IJKJ: ',IJKJ_count
          write(UNIout,'(2(a,i10))')'IIKL: ',IIKL_count,' IJKL: ',IJKL_count
          if(ANY_ONDISK)then
            write(UNIout,'(a)')'Number of buffers:'
            write(UNIout,'(2(a,i10))')'IJJL: ',IJJL_Nbuffers,' IJKJ: ',IJKJ_Nbuffers
            write(UNIout,'(2(a,i10))')'IIKL: ',IIKL_Nbuffers,' IJKL: ',IJKL_Nbuffers
          end if
        end if

 1001 FORMAT(/1X,I10,' TWO-ELECTRON INTEGRALS CALCULATED IN ',a,' (',a,')')
 1040 FORMAT(' TOTAL NUMBER OF EACH OF THE 8 TYPES OF INTEGRALS SAVED:' &
       /1X,'IIKL: ',I10,1X,'IJKJ: ',I10,1X,'IJJL: ',I10,1X,'IIKK: ',I10 &
       /1X,'IJJJ: ',I10,1X,'IIIL: ',I10,1X,'IIII: ',I10,1X,'IJKL: ',I10)

      return
      end subroutine PRT_I2E_details
