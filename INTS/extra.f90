      subroutine extraint(Av,Bv,Cv,Dv,a,b,c,d,A_ang,B_ang,C_ang,D_ang,Rv,ext)
! 
!   *****************************************************************
!   *                                                               *
!   *  Calculates 3D extracule density integrals using a 5-term RR. *
!   *                                                               *
!   *       --------------------- OUTPUT ---------------------      *
!   *       ext = extracule density integrals                       *
!   *       --------------------------------------------------      *
!   *                                                               * 
!   *       --------------------- INPUT ----------------------      *
!   *       Av = cartesian coordinates of centre A                  *
!   *       Bv = cartesian coordinates of centre B                  *
!   *       Cv = cartesian coordinates of centre C                  *
!   *       Dv = cartesian coordinates of centre D                  *
!   *       a = exponent alpha (associated with centre A)           *
!   *       b = exponent beta (associated with centre B)            *
!   *       c = exponent gamma (associated with centre C)           *
!   *       d = exponent delta (associated with centre D)           *
!   *       A_ang = angular momentum of basis function on A         *
!   *       B_ang = angular momentum of basis function on B         *
!   *       C_ang = angular momentum of basis function on C         *
!   *       D_ang = angular momentum of basis function on D         * 
!   *       Rv = extracular (centre-of-mass) coordinate             *
!   *       --------------------------------------------------      *
!   *                                                               *
!   *  For details of the equations contained in this section of    *
!   *  code, see Hollett 2024??                                     *
!   *                                                               *
!   *  code written by JWH (2024)                                   *
!   *                                                               *
!   *****************************************************************
!
! Modules:
      use extra_RR
!
      implicit none
!
!---------------------------------------------------------------------
! Input/Output:
!---------------------------------------------------------------------
      integer :: A_ang, B_ang, C_ang, D_ang
      double precision, dimension(3) :: Av, Bv, Cv, Dv, Rv
      double precision :: a, b, c, d
      double precision, dimension(1296) :: ext 
!
!--------------------------------------------------------------------
! Local: 
!--------------------------------------------------------------------
!
      double precision, dimension(3) :: P
      double precision :: Eabcd, lam
      integer :: Lcode, i, j
      integer :: d1, d2, d3, d4, p1, p2, p3, p4
      integer :: dd_index, ddd_index, dddd_index
      integer :: dddp_index, ddpd_index, ddpp_index, dpdp_index, dppd_index, dppp_index, pdpd_index
      integer :: pdpp_index
      integer :: ddp_index, dpd_index, pdd_index, dpp_index, pdp_index
      integer :: dp_index, pd_index
!
!--------------------------------------------------------------------
! Begin: 
!--------------------------------------------------------------------
!
! Set angular momentum code
      Lcode = 1000*A_ang + 100*B_ang + 10*C_ang + D_ang
! Set angular momentum vectors, for higher functions
      Lvec_d=0
      Lvec_d(1,1)=2 ! xx
      Lvec_d(2,2)=2 ! yy
      Lvec_d(3,3)=2 ! zz
      Lvec_d(4,1)=1 ! xy
      Lvec_d(4,2)=1 
      Lvec_d(5,1)=1 ! xz
      Lvec_d(5,3)=1 
      Lvec_d(6,2)=1 ! yz
      Lvec_d(6,3)=1 
! Set xyz code for p functions
      xyz(1)=1
      xyz(2)=2
      xyz(3)=3
! Identity matrix
      imat = 0
      imat(1,1) = 1
      imat(2,2) = 1
      imat(3,3) = 1
!  
!--------------------------------------------------------------------
!  Calculate common intermediates, prefactor
!--------------------------------------------------------------------
!
      lam = (a+b)*(c+d)/(a+b+c+d)
      P   = 0.0D0
      do i=1,3 
        P(i) = (a*(Av(i) - Rv(i)) + b*(Bv(i) - Rv(i)))/(a+b)&
             + (c*(Cv(i) - Rv(i)) + d*(Dv(i) - Rv(i)))/(c+d)
      end do
!
! calculate exponent first
      Eabcd = 0.0D0
      do i=1,3
        Eabcd = Eabcd - lam*P(i)**2&
                      - a*b*(Av(i)-Bv(i))**2/(a+b) - c*d*(Cv(i)-Dv(i))**2/(c+d)
      end do
      Eabcd = 8.0D0*pi**1.5D0*exp(Eabcd)/(a+b+c+d)**1.5D0
!      
!-------------------------------------------------------------------
! Calculate required G's
!-------------------------------------------------------------------
!
! No G's required for 3D extracule integrals
!
      G = 1.0D0
!
! Combine with prefactors
!
      G = Eabcd*G
!
! Calculate coefficents for RR
      do i=1,3 ! x,y,z 
        RRa1(i) = (b*(Bv(i) - Av(i)) - lam*P(i))/(a+b)
        RRb1(i) = (a*(Av(i) - Bv(i)) - lam*P(i))/(a+b)
        RRc1(i) = (d*(Dv(i) - Cv(i)) - lam*P(i))/(c+d)
        RRd1(i) = (c*(Cv(i) - Dv(i)) - lam*P(i))/(c+d)
      end do ! i
!
!                                        Corresponding Integral
! RRa
      RRa2  = (a+b-lam)/(2.0D0*(a+b)**2)   ! [(a-1i)bcd]
      RRa3  = RRa2                         ! [a(b-1i)cd]
      RRa4  = -lam/(2.0D0*(a+b)*(c+d))     ! [ab(c-1i)d]
      RRa5  = RRa4                         ! [ab(c-1i)d]
!
! RRb
      RRb2  = (a+b-lam)/(2.0D0*(a+b)**2)   ! [(a-1i)bcd]
      RRb3  = RRb2                         ! [a(b-1i)cd]
      RRb4  = -lam/(2.0D0*(a+b)*(c+d))     ! [ab(c-1i)d]
      RRb5  = RRb4                         ! [abc(d-1i)]
!
! RRc
      RRc2  = -lam/(2.0D0*(a+b)*(c+d))     ! [(a-1i)bcd]
      RRc3  = RRc2                         ! [a(b-1i)cd]
      RRc4  = (c+d-lam)/(2.0D0*(c+d)**2)   ! [ab(c-1i)d]
      RRc5  = RRc4                         ! [abc(d-1i)]
!
! RRd
      RRd2  = -lam/(2.0D0*(a+b)*(c+d))     ! [(a-1i)bcd]
      RRd3  = RRd2                         ! [a(b-1i)cd]
      RRd4  = (c+d-lam)/(2.0D0*(c+d)**2)   ! [ab(c-1i)d]
      RRd5  = RRd4                         ! [abc(d-1i)]
!
!------------------------------------------------------------------
! [ssss]
!------------------------------------------------------------------
      if(Lcode.eq.0)then
!
        ext(1) = G
!
        return
      end if
!
!------------------------------------------------------------------
! [psss]
!------------------------------------------------------------------
      if(Lcode.eq.1000)then
!
! Requires one step
        allocate(Int1000(3)) 
!
! Step 1 :
! [1000]^(0) from [0000]^(1)
!
        call calcextra1000
!
        ext(1:3) = Int1000(1:3)
!
        deallocate(Int1000)
        return
      end if
!
!------------------------------------------------------------------
! [ppss]
!------------------------------------------------------------------
      if(Lcode.eq.1100)then
!
! Requires 2 steps
!
      allocate(Int1000(3),Int1100(9))
      Int1000 = 0.0D0
      Int1100 = 0.0D0
!
! Step 1 :
! [1000]^(1) from [0000]^(2)
! a+1i
!
      call calcextra1000
!
! Step 2 :
! [1100]^(0) from [1000]^(1) and [0000]^(1)
! b+1i
!      
      call calcextra1100
!
      ext(1:9) = Int1100(1:9) 
!
        deallocate(Int1000,Int1100)
        return
      end if
!
!------------------------------------------------------------------
! [psps]
!------------------------------------------------------------------
      if(Lcode.eq.1010)then
!
! Requires 2 steps
!
      allocate(Int1000(3),Int1010(9))
      Int1000 = 0.0D0
      Int1010 = 0.0D0
!
! Step 1 :
! [1000]^(1) from [0000]^(2)
! a+1i
!
      call calcextra1000
!
! Step 2 :
! [1010]^(0) from [1000]^(1) and [0000]^(1)
! c+1i
!      
      call calcextra1010
!
      ext(1:9) = Int1010(1:9) 
!
        deallocate(Int1000,Int1010)
        return
      end if
!
!------------------------------------------------------------------
! [ppps]
!------------------------------------------------------------------
      if(Lcode.eq.1110)then
!
! Requires 4 steps
!
      allocate(Int1000(3),Int0100(3),Int1100(9),Int1110(27))
      Int1000 = 0.0D0
      Int0100 = 0.0D0
      Int1100 = 0.0D0
      Int1110 = 0.0D0
!
! Step 1 :
! [1000]^(2) from [0000]^(3)
! a+1i
!
      call calcextra1000
!
! Step 2 :
! [0100]^(1) from [0000]^(2)
! b+1i
!
      call calcextra0100
!
! Step 3 :
! [1100]^(1) from [1000]^(2) and [0000]^(2)
! b+1i
!
      call calcextra1100
!
! Step 4 :
! [1110]^(0) from [1100]^(1), [1000]^(1), and [0100]^(1)
! c+1i
!
      call calcextra1110
!
      ext(1:27) = Int1110(1:27)
!
        deallocate(Int1000,Int0100,Int1100,Int1110)
        return
      end if
!
!------------------------------------------------------------------
! [pppp]
!------------------------------------------------------------------
      if(Lcode.eq.1111)then
!
! Requires 7 steps
      allocate(Int1000(3),Int0100(3),Int1100(9),Int1010(9))
      allocate(Int0110(9),Int1110(27),Int1111(81))
      Int1000 = 0.0D0
      Int0100 = 0.0D0
      Int1100 = 0.0D0
      Int1010 = 0.0D0
      Int0110 = 0.0D0
      Int1110 = 0.0D0
      Int1111 = 0.0D0
!
! Step 1 :
! [1000]^(3) from [0000]^(4)
! a+1i
!
      call calcextra1000
!
! Step 2 :
! [0100]^(2) from [0000]^(3) 
! b+1i
!
      call calcextra0100
!
! Step 3 :
! [1100]^(2) from [1000]^(3) and [0000]^(3)
! b+1i
!
      call calcextra1100
!
! Step 4 :
! [1010]^(1) from [1000]^(2) and [0000]^(2)
! c+1i
!
      call calcextra1010
!
! Step 5 :
! [0110]^(1) from [0100]^(2) and [0000]^(2)
! c+1i
!
      call calcextra0110
!
! Step 6 :
! [1110]^(1) from [1100]^(2), [0100]^(2) and [1000]^(2)
! c+1i
!
      call calcextra1110
!
! Step 7 :
! [1111]^(0) from [1110]^(1), [0110]^(1), [1010]^(1) and [1100]^(1)
! d+1i
!
        call calcextra1111
!
        ext(1:81) = Int1111(1:81)
!
        deallocate(Int1000,Int0100,Int1100,Int1010)
        deallocate(Int0110,Int1110,Int1111)
        return
      end if
!
!------------------------------------------------------------------
! [dsss]
!------------------------------------------------------------------
      if(Lcode.eq.2000)then
!
! Requires 2 steps
!
! Step 1 :
! [1000]^(1) from [0000]^(2)
! a+1i
!
      allocate(Int1000(3))
      Int1000 = 0.0D0
!
      call calcextra1000
!
! Step 2 :
! [2000]^(0) from [1000]^(1)
! a+1i
!
      allocate(Int2000(6))
      Int2000 = 0.0D0
!
      call calcextra2000
!
      ext(1:3) = Int2000(1:3)
      ext(4:6) = sq3*Int2000(4:6)
!
      deallocate(Int2000,Int1000)
      return
    end if
!
!------------------------------------------------------------------
! [dpss]
!------------------------------------------------------------------
      if(Lcode.eq.2100)then
!
! Requires 3 steps
!
! Step 1 :
! [1000]^(2) from [0000]^(3)
! a+1i
!
      allocate(Int1000(3))
      Int1000 = 0.0D0
!
      call calcextra1000
!
! Step 2:
! [2000]^(1) from [1000]^(2) and [0000]^(2)
! a+1i
!
      allocate(Int2000(6))
      Int2000 = 0.0D0
!
      call calcextra2000
!
! Step 3:
! [2100]^(0) from [2000]^(1) and [1000]^(1)
! b+1i
!
      allocate(Int2100(18))
      Int2100 = 0.0D0
!
      call calcextra2100
!
      ext(1:18) = Int2100(1:18)
!
      do d1=4,6
        do p2=1,3
          dp_index=3*(d1-1)+p2
          ext(dp_index) = sq3*ext(dp_index)
        end do
      end do
!
      deallocate(Int1000,Int2000,Int2100)
      return
      end if
!
!------------------------------------------------------------------
! [dsps]
!------------------------------------------------------------------
      if(Lcode.eq.2010)then
!
! Requires 3 steps
!
! Step 1 :
! [1000]^(2) from [0000]^(3)
! a+1i
!
      allocate(Int1000(3))
      Int1000 = 0.0D0
!
      call calcextra1000
!
! Step 2:
! [2000]^(1) from [1000]^(2) and [0000]^(2)
! a+1i
!
      allocate(Int2000(6))
      Int2000 = 0.0D0
!
      call calcextra2000
!
! Step 3:
! [2010]^(0) from [2000]^(1) and [1000]^(1)
! c+1i
!
      allocate(Int2010(18))
      Int2010 = 0.0D0
!
      call calcextra2010
!
      ext(1:18) = Int2010(1:18)
!
      do d1=4,6
        do p2=1,3
          dp_index=3*(d1-1)+p2
          ext(dp_index) = sq3*ext(dp_index)
        end do
      end do
!
      deallocate(Int1000,Int2000,Int2010)
      return
      end if
!
!------------------------------------------------------------------
! [dpps]
!------------------------------------------------------------------
      if(Lcode.eq.2110)then
!
! Requires 5 steps
!
! Step 1 :
! [1000]^(3) from [0000]^(4)
! a+1i
!
      allocate(Int1000(3))
      Int1000 = 0.0D0
!
      call calcextra1000
!
! Step 2:  
! [1100]^(1) from [1000]^(2) and [0000]^(2)
! b+1i
!
      allocate(Int1100(9))
      Int1100 = 0.0D0
!
      call calcextra1100
!
! Step 3 :
! [2000]^(2) from [1000]^(3) and [0000]^(3)
! a+1i
!
      allocate(Int2000(6))
      Int2000 = 0.0D0
!
      call calcextra2000
!
! Step 4:
! [2100]^(1) from [2000]^(2) and [1000]^(2)
! b+1i
!
      allocate(Int2100(18))
      Int2100 = 0.0D0
!
      call calcextra2100
!
! Step 5:
! [2110]^(0) from [2100]^(1), [2000]^(1) and [1100]^(1)
! c+1i
!
      allocate(Int2110(54))
      Int2110 = 0.0D0
!
      call calcextra2110c
!
      ext(1:54) = Int2110(1:54)
!
      do d1=4,6
        do p2=1,3
          do p3=1,3
            dpp_index=9*(d1-1)+3*(p2-1)+p3
            ext(dpp_index) = sq3*ext(dpp_index)
          end do
        end do
      end do
!
      deallocate(Int1000,Int1100,Int2000,Int2100,Int2110)
      return
      end if
!
!------------------------------------------------------------------
! [dspp]
!------------------------------------------------------------------
      if(Lcode.eq.2011)then
!
! Requires 5 steps
!
! Step 1 :
! [1000]^(3) from [0000]^(4)
! a+1i
!
      allocate(Int1000(3))
      Int1000 = 0.0D0
!
      call calcextra1000
!
! Step 2:  
! [1010]^(1) from [1000]^(2) and [0000]^(2)
! c+1i
!
      allocate(Int1010(9))
      Int1010 = 0.0D0
!
      call calcextra1010
!
! Step 3 :
! [2000]^(2) from [1000]^(3) and [0000]^(3)
! a+1i
!
      allocate(Int2000(6))
      Int2000 = 0.0D0
!
      call calcextra2000
!
! Step 4:
! [2010]^(1) from [2000]^(2) and [1000]^(2)
! c+1i
!
      allocate(Int2010(18))
      Int2010 = 0.0D0
!
      call calcextra2010
!
! Step 5:
! [2011]^(0) from [2010]^(1), [2000]^(1) and [1010]^(1)
! d+1i
!
      allocate(Int2011(54))
      Int2011 = 0.0D0
!
      call calcextra2011
!
      ext(1:54) = Int2011(1:54)
!
      do d1=4,6
        do p2=1,3
          do p3=1,3
            dpp_index=9*(d1-1)+3*(p2-1)+p3
            ext(dpp_index) = sq3*ext(dpp_index)
          end do
        end do
      end do
!
      deallocate(Int1000,Int1010,Int2000,Int2010,Int2011)
      return
      end if
!
!------------------------------------------------------------------
! [dppp]
!------------------------------------------------------------------
      if(Lcode.eq.2111)then
!
! Requires 9 steps
!
! Step 1 :
! [1000]^(4) from [0000]^(5)
! a+1i
!
      allocate(Int1000(3))
      Int1000 = 0.0D0
!
      call calcextra1000
!
! Step 2 :
! [0100]^(2) from [0000]^(3)
! b+1i
!
      allocate(Int0100(3))
      Int0100 = 0.0D0
!
      call calcextra0100
!
! Step 3:
! [1100]^(2) from [1000]^(3) and [0000]^(3)
! b+1i
!
      allocate(Int1100(9))
      Int1100 = 0.0D0
!
      call calcextra1100
!
! Step 4 :
! [2000]^(3) from [1000]^(4) and [0000]^(4)
! a+1i
!
      allocate(Int2000(6))
      Int2000 = 0.0D0
!
      call calcextra2000
!
! Step 5 :
! [1110]^(1) from [1100]^(2), [1000]^(2) and [0100]^(2)
! c+1i
!
      allocate(Int1110(27))
      Int1110 = 0.0D0
!
      call calcextra1110
!
! Step 6:
! [2010]^(1) from [2000]^(2) and [1000]^(2)
! c+1i
!
      allocate(Int2010(18))
      Int2010 = 0.0D0
!
      call calcextra2010
!
! Step 7:
! [2100]^(2) from [2000]^(3) and [1000]^(3)
! b+1i
!
      allocate(Int2100(18))
      Int2100 = 0.0D0
!
      call calcextra2100
!
! Step 8:
! [2110]^(1) from [2100]^(2), [2000]^(2) and [1100]^(2)
! c+1i
!
      allocate(Int2110(54))
      Int2110 = 0.0D0
!
      call calcextra2110c
!
! Step 9:
! [2111]^(0) from [2110]^(1), [2100]^(1), [2010]^(1) and [1110]^(1)
! d+1i
!
      allocate(Int2111(162))
      Int2111 = 0.0D0
!
      call calcextra2111
!
      ext(1:162) = Int2111(1:162)
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        do d1=4,6
          do p2=1,3
            do p3=1,3
              do p4=1,3
                dppp_index=27*(d1-1)+9*(p2-1)+3*(p3-1)+p4
                ext(dppp_index) = sq3*ext(dppp_index)
              end do
            end do
          end do
        end do
!
      deallocate(Int1000,Int0100,Int2000,Int1100,Int2100,Int2010)
      deallocate(Int1110,Int2110,Int2111)
      return
      end if
!
!------------------------------------------------------------------
! [ddss]
!------------------------------------------------------------------
      if(Lcode.eq.2200)then
!
! Requires 5 steps
!
! Step 1 :
! [1000]^(3) from [0000]^(4)
! a+1i
!
      allocate(Int1000(3))
      Int1000 = 0.0D0
!
      call calcextra1000
!
! Step 2:
! [1100]^(1) from [1000]^(2) and [0000]^(2)
! b+1i
!
      allocate(Int1100(9))
      Int1100 = 0.0D0
!
      call calcextra1100
!
! Step 3 :
! [2000]^(2) from [1000]^(3) and [0000]^(3)
! a+1i
!
      allocate(Int2000(6))
      Int2000 = 0.0D0
!
      call calcextra2000
!
! Step 4:
! [2100]^(1) from [2000]^(2) and [1000]^(2)
! b+1i
!
      allocate(Int2100(18))
      Int2100 = 0.0D0
!
      call calcextra2100
!
! Step 5:
! [2200]^(0) from [2100]^(1), [2000]^(1) and [1100]^(1)
! b+1i
!
      allocate(Int2200(36))
      Int2200 = 0.0D0
!
      call calcextra2200
!
      ext(1:36) = Int2200(1:36)
!
! multiply by sqrt(3) for each d-function xy, xz, yz
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            if(d1.gt.3) ext(dd_index) = sq3*ext(dd_index)
            if(d2.gt.3) ext(dd_index) = sq3*ext(dd_index)
          end do
        end do
!
      deallocate(Int1000,Int1100,Int2000,Int2100,Int2200)
      return
      end if
!
!------------------------------------------------------------------
! [dsds]
!------------------------------------------------------------------
      if(Lcode.eq.2020)then
!
! Requires 5 steps
!
! Step 1 :
! [1000]^(3) from [0000]^(4)
! a+1i
!
      allocate(Int1000(3))       
      Int1000 = 0.0D0
!
      call calcextra1000
!
! Step 2:
! [1010]^(1) from [1000]^(2) and [0000]^(2)
! c+1i
!
      allocate(Int1010(9))       
      Int1010 = 0.0D0
!
      call calcextra1010
!
! Step 3 :
! [2000]^(2) from [1000]^(3) and [0000]^(3)
! a+1i
!
      allocate(Int2000(6))       
      Int2000 = 0.0D0
!
      call calcextra2000
!
! Step 4:
! [2010]^(1) from [2000]^(2) and [1000]^(2)
! c+1i
!
      allocate(Int2010(18))       
      Int2010 = 0.0D0
!
      call calcextra2010
!
! Step 5:
! [2020]^(0) from [2010]^(1), [2000]^(1) and [1100]^(1)
! c+1i
!
      allocate(Int2020(36))       
      Int2020 = 0.0D0
!
      call calcextra2020
!
      ext(1:36) = Int2020(1:36)
!
! multiply by sqrt(3) for each d-function xy, xz, yz
      dd_index=0
      do d1=1,6
        do d2=1,6
          dd_index=dd_index+1
          if(d1.gt.3) ext(dd_index) = sq3*ext(dd_index)
          if(d2.gt.3) ext(dd_index) = sq3*ext(dd_index)
        end do
      end do
!
      deallocate(Int1000,Int1010,Int2000,Int2010,Int2020)
      return
      end if
!
!------------------------------------------------------------------
! [ddps]
!------------------------------------------------------------------
      if(Lcode.eq.2210)then
!
! Requires 9 steps
!
! Step 1:
! [1000]^(4) from [0000]^(5)
! a+1i
!
      allocate(Int1000(3))
      Int1000 = 0.0D0
!
      call calcextra1000
!
! Step 2:
! [0100]^(2) from [0000]^(3)
! b+1i
!
      allocate(Int0100(3))
      Int0100 = 0.0D0
!
      call calcextra0100
!
! Step 3:
! [1100]^(2) from [1000]^(3) and [0000]^(3)
! b+1i
!
      allocate(Int1100(9))
      Int1100 = 0.0D0
!
      call calcextra1100
!
! Step 4:
! [2000]^(3) from [1000]^(4) and [0000]^(4)
! a+1i
!
      allocate(Int2000(6))
      Int2000 = 0.0D0
!
      call calcextra2000
!
! Step 5:
! [0200]^(3) from [0100]^(4) and [0000]^(4)
! b+1i
!
      allocate(Int0200(6))
      Int0200 = 0.0D0
!
      call calcextra0200
!
! Step 6:
! [1200]^(1) from [0200]^(2) and [0100]^(2)
! a+1i
!
      allocate(Int1200(18))
      Int1200 = 0.0D0
!
      call calcextra1200
!
! Step 7:
! [2100]^(2) from [2000]^(3) and [1000]^(3)
! b+1i
!
      allocate(Int2100(18))
      Int2100 = 0.0D0
!
      call calcextra2100
!
! Step 8:
! [2200]^(1) from [2100]^(2), [1100]^(2), and [2000]^(2)
! b+1i
!
      allocate(Int2200(36))
      Int2200 = 0.0D0
!
      call calcextra2200
!
! Step 9:
! [2210]^(0) from [2200]^(1), [2100]^(1), [1200]^(1)
! c+1i
!
      allocate(Int2210(108))
      Int2210 = 0.0D0
!
      call calcextra2210
!
      ext(1:108) = Int2210(1:108)
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
      ddp_index=0
      do d1=1,6
        do d2=1,6
          do p3=1,3
            ddp_index=ddp_index+1
            if(d1.gt.3) ext(ddp_index) = sq3*ext(ddp_index)
            if(d2.gt.3) ext(ddp_index) = sq3*ext(ddp_index)
          end do
        end do
      end do
!
      deallocate(Int1000,Int0100,Int0200,Int2000,Int1100,Int2100)
      deallocate(Int1200,Int2200,Int2210)
      return
      end if
!
!------------------------------------------------------------------
! [dpds]
!------------------------------------------------------------------
      if(Lcode.eq.2120)then
!
! Requires 9 steps
!
! Step 1:
! [1000]^(4) from [0000]^(5)
! a+1i
!
      allocate(Int1000(3))
      Int1000 = 0.0D0
!
      call calcextra1000
!
! Step 2:
! [0010]^(2) from [0000]^(3)
! c+1i
!
      allocate(Int0010(3))
      Int0010 = 0.0D0
!
      call calcextra0010
!
! Step 3:
! [1010]^(2) from [1000]^(3) and [0000]^(3)
! c+1i
!
      allocate(Int1010(9))
      Int1010 = 0.0D0
!
      call calcextra1010
!
! Step 4:
! [2000]^(3) from [1000]^(4) and [0000]^(4)
! a+1i
!
      allocate(Int2000(6))
      Int2000 = 0.0D0
!
      call calcextra2000
!
! Step 5:
! [0020]^(3) from [0010]^(4) and [0000]^(4)
! c+1i
!
      allocate(Int0020(6))
      Int0020 = 0.0D0
!
      call calcextra0020
!
! Step 6:
! [1020]^(1) from [0020]^(2) and [0010]^(2)
! a+1i
!
      allocate(Int1020(18))
      Int1020 = 0.0D0
!
      call calcextra1020
!
! Step 6:
! [2010]^(2) from [2000]^(3) and [1000]^(3)
! c+1i
!
      allocate(Int2010(18))
      Int2010 = 0.0D0
!
      call calcextra2010
!
! Step 7:
! [2020]^(1) from [2010]^(2), [1010]^(2), and [2000]^(2)
! c+1i
!
      allocate(Int2020(36))
      Int2020 = 0.0D0
!
      call calcextra2020
!
! Step 8:
! [2120]^(0) from [2020]^(1), [2010]^(1), [1020]^(1)
! b+1i
!
      allocate(Int2120(108))
      Int2120 = 0.0D0
!
      call calcextra2120
!
      ext(1:108) = Int2120(1:108)
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
      dpd_index=0
      do d1=1,6
        do p2=1,3
          do d3=1,6
            dpd_index=dpd_index+1
            if(d1.gt.3) ext(dpd_index) = sq3*ext(dpd_index)
            if(d3.gt.3) ext(dpd_index) = sq3*ext(dpd_index)
          end do
        end do
      end do
!
      deallocate(Int1000,Int0010,Int2000,Int0020,Int1010)
      deallocate(Int1020,Int2010,Int2020,Int2120)
      return
      end if
!
!------------------------------------------------------------------
! [ddpp]
!------------------------------------------------------------------
      if(Lcode.eq.2211)then
!
! Requires 12 steps
!
! Step 1 :
! [1000]^(5) from [0000]^(6)
! a+1i
!
      allocate(Int1000(3))
      Int1000 = 0.0D0
!
      call calcextra1000
!
! Step 2 :        
! [0100]^(3) from [0000]^(4)
! b+1i            
!
      allocate(Int0100(3))
      Int0100 = 0.0D0
!
      call calcextra0100
!
! Step 3:
! [1100]^(3) from [1000]^(4) and [0000]^(4)
! b+1i
!
      allocate(Int1100(9))
      Int1100 = 0.0D0
!
      call calcextra1100
!
! Step 4:
! [0200]^(2) from [0100]^(3) and [0000]^(3)
! b+1i
!
      allocate(Int0200(6))
      Int0200 = 0.0D0
!
      call calcextra0200
!
! Step 5:
! [2000]^(4) from [1000]^(5) and [0000]^(5)
! a+1i
!
      allocate(Int2000(6))
      Int2000 = 0.0D0
!
      call calcextra2000
!
! Step 6:
! [1200]^(2) from [0200]^(3) and [0100]^(3)
! a+1i
!
      allocate(Int1200(18))
      Int1200 = 0.0D0
!
      call calcextra1200
!
! Step 7:
! [2100]^(3) from [2000]^(4) and [1000]^(4)
! b+1i
!
      allocate(Int2100(18))
      Int2100 = 0.0D0
!
      call calcextra2100
!
! Step 8:
! [2200]^(3) from [2100]^(4) and [1200]^(4)
! b+1i
!
      allocate(Int2200(36))
      Int2200 = 0.0D0
!
      call calcextra2200
!
! Step 9:
! [2110]^(3) from [2100]^(4), [1100]^(4), [2000]^(4)
! c+1i
!
      allocate(Int2110(54))
      Int2110 = 0.0D0
!
      call calcextra2110c
!
! Step 10:
! [1210]^(1) from [1200]^(2), [1100]^(2) and [0200]^(2)
! c+1i
!
      allocate(Int1210(54))
      Int1210 = 0.0D0
!
      call calcextra1210
!
! Step 11:
! [2210]^(1) from [2200]^(2), [2100]^(2) and [1200]^(2)
! c+1i
!
      allocate(Int2210(108))
      Int2210 = 0.0D0
!
      call calcextra2210
!
! Step 12:
! [2211]^(1) from [2210]^(1), [1210]^(1), [2110]^(1), [2200]^(1)
! d+1i
!
      allocate(Int2211(324))
      Int2211 = 0.0D0
!
      call calcextra2211
!
      ext(1:324) = Int2211(1:324)
!
      ddpp_index=0
      do d1=1,6
        do d2=1,6
          do p3=1,3
            do p4=1,3
              ddpp_index=ddpp_index+1
              if(d1.gt.3) ext(ddpp_index) = sq3*ext(ddpp_index)
              if(d2.gt.3) ext(ddpp_index) = sq3*ext(ddpp_index)
            end do
          end do
        end do
      end do
!
      deallocate(Int1000,Int0100,Int1100,Int0200,Int2000)
      deallocate(Int2100,Int1200,Int2200,Int2110,Int1210)
      deallocate(Int2210,Int2211)
!
      return 
      end if 
!
!--------------------------------------------------------------------
! [dpdp]
!--------------------------------------------------------------------
      if(Lcode.eq.2121)then
!
! Requires 12 steps
!
! Step 1:
! [1000]^(5) from [0000]^(6)
! a+1i
!
      allocate(Int1000(3))
      Int1000 = 0.0D0
!
      call calcextra1000
!
! Step 2:
! [0010]^(4) from [0000]^(5)
! c+1i
!
      allocate(Int0010(3))
      Int0010 = 0.0D0
!
      call calcextra0010
!
! Step 3:
! [0020]^(3) from [0010]^(4) and [0000]^(4)
! c+1i
!
      allocate(Int0020(6))
      Int0020 = 0.0D0
!
      call calcextra0020
!
! Step 4:
! [1010]^(3) from [1000]^(4) and [0000]^(4)
! c+1i
!
      allocate(Int1010(9))
      Int1010 = 0.0D0
!
      call calcextra1010
!
! Step 5:
! [2000]^(3) from [1000]^(4) and [0000]^(4)
! a+1i
!
      allocate(Int2000(6))
      Int2000 = 0.0D0
!
      call calcextra2000
!
! Step 6:
! [1020]^(3) from [0020]^(4) and [0010]^(4)
! a+1i
!
      allocate(Int1020(18))
      Int1020 = 0.0D0
!
      call calcextra1020
!
! Step 7:
! [2010]^(3) from [2000]^(4) and [1000]^(4)
! c+1i
!
      allocate(Int2010(18))
      Int2010 = 0.0D0
!
      call calcextra2010
!
! Step 8:
! [1120]^(3) from [1020]^(4), [0020]^(4) and [1010]^(4)
! b+1i
!
      allocate(Int1120(54))
      Int1120 = 0.0D0
!
      call calcextra1120
!
! Step 9:
! [2020]^(3) from [2010]^(4), [2000]^(4) and [1010]^(4)
! c+1i
!
      allocate(Int2020(36))
      Int2020 = 0.0D0
!
      call calcextra2020
!
! Step 10:
! [2110]^(3) from [2010]^(4), [2000]^(4) and [1010]^(4)
! b+1i
!
      allocate(Int2110(54))
      Int2110 = 0.0D0
!
      call calcextra2110b
!
! Step 11:
! [2120]^(3) from [2020]^(4), [1020]^(4) and [2010]^(4)
! b+1i
!
      allocate(Int2120(108))
      Int2120 = 0.0D0
!
      call calcextra2120
!
! Step 12:
! [2121]^(0) from [2120]^(1), [1120]^(1), [2020]^(1), and [2110]^(1)
! d+1i
!
      allocate(Int2121(324))
      Int2121 = 0.0D0
!
      call calcextra2121
!
      ext(1:324) = Int2121(1:324)
!
        dpdp_index=0
        do d1=1,6
          do p2=1,3
            do d3=1,6
              do p4=1,3
                dpdp_index=dpdp_index+1
                if(d1.gt.3) ext(dpdp_index) = sq3*ext(dpdp_index)
                if(d3.gt.3) ext(dpdp_index) = sq3*ext(dpdp_index)
              end do
            end do
          end do
        end do
!
      deallocate(Int1000,Int0010,Int1010,Int0020,Int2000)
      deallocate(Int2010,Int1020,Int2020,Int2110,Int1120)
      deallocate(Int2120,Int2121)
!
      return
      end if
!
!--------------------------------------------------------------------
! [ddds]
!--------------------------------------------------------------------
      if(Lcode.eq.2220)then
!
! Requires 12 steps
!
! Step 1:
! [1000]^(4) from [0000]^(5)
! a+1i
!
      allocate(Int1000(3))
      Int1000 = 0.0D0
!
      call calcextra1000
!
! Step 2:
! [0100]^(4) from [0000]^(5)
! b+1i
!
      allocate(Int0100(3))
      Int0100 = 0.0D0
!
      call calcextra0100
!
! Step 3:
! [1100]^(4) from [1000]^(5) and [0000]^(5)
! b+1i
!
      allocate(Int1100(9))
      Int1100 = 0.0D0
!
      call calcextra1100
!
! Step 4:
! [2000]^(4) from [1000]^(5) and [0000]^(5)
! a+1i
!
      allocate(Int2000(6))
      Int2000 = 0.0D0
!
      call calcextra2000
!
! Step 5:
! [0200]^(4) from [0100]^(5) and [0000]^(5)
! b+1i
!
      allocate(Int0200(6))
      Int0200 = 0.0D0
!
      call calcextra0200
!
! Step 6:
! [2100]^(4) from [2000]^(5) and [1000]^(5)
! b+1i
!
      allocate(Int2100(18))
      Int2100 = 0.0D0
!
      call calcextra2100
!
! Step 7:
! [1200]^(4) from [0200]^(5) and [0100]^(5)
! a+1i
!
      allocate(Int1200(18))
      Int1200 = 0.0D0
!
      call calcextra1200
!
! Step 8:
! [2200]^(4) from [2100]^(5), [1100]^(5) and [2000]^(5)
! b+1i
!
      allocate(Int2200(36))
      Int2200 = 0.0D0
!
      call calcextra2200
!
! Step 9:
! [2110]^(4) from [2100]^(5), [1100]^(5) and [2000]^(5)
! c+1i
!
      allocate(Int2110(54))
      Int2110 = 0.0D0
!
      call calcextra2110c
!
! Step 10:
! [1210]^(4) from [1200]^(5), [0200]^(5) and [1100]^(5)
! c+1i
!
      allocate(Int1210(54))
      Int1210 = 0.0D0
!
      call calcextra1210
!
! Step 11:
! [2210]^(4) from [2200]^(5), [1200]^(5) and [2100]^(5)
! c+1i
!
      allocate(Int2210(108))
      Int2210 = 0.0D0
!
      call calcextra2210
!
! Step 12:
! [2220]^(4) from [2210]^(5), [1210]^(5), [2110]^(5) and [2200]^(5)
! c+1i
!
      allocate(Int2220(216))
      Int2220 = 0.0D0
!
      call calcextra2220
!
      ext(1:216) = Int2220(1:216)
!
      ddd_index = 0
      do d1 = 1,6
        do d2 = 1,6
          do d3 = 1,6
            ddd_index = ddd_index + 1
            if(d1.gt.3) ext(ddd_index) = sq3*ext(ddd_index)
            if(d2.gt.3) ext(ddd_index) = sq3*ext(ddd_index)
            if(d3.gt.3) ext(ddd_index) = sq3*ext(ddd_index)
          end do
        end do
      end do
!
      deallocate(Int1000,Int0100,Int1100,Int2000,Int0200)
      deallocate(Int1200,Int2100,Int2110,Int1210,Int2200)
      deallocate(Int2210,Int2220)
!
      return
      end if
!
!--------------------------------------------------------------------
! [dddp]
!--------------------------------------------------------------------
      if(Lcode.eq.2221)then
!
! Requires 25 steps
!
! Step 1:
! [1000]^(4) from [0000]^(5)
! a+1i
!
      allocate(Int1000(3))
      Int1000 = 0.0D0
!
      call calcextra1000
!
! Step 2:
! [0100]^(4) from [0000]^(5)
! b+1i
!
      allocate(Int0100(3))
      Int0100 = 0.0D0
!
      call calcextra0100
!
! Step 3:
! [0010]^(4) from [0000]^(5)
! c+1i
!
      allocate(Int0010(3))
      Int0010 = 0.0D0
!
      call calcextra0010
!
! Step 4:
! [1100]^(4) from [1000]^(5) and [0000]^(5)
! b+1i
!
      allocate(Int1100(9))
      Int1100 = 0.0D0
!
      call calcextra1100
!
! Step 5:
! [1010]^(4) from [1000]^(5) and [0000]^(5)
! c+1i
!
      allocate(Int1010(9))
      Int1010 = 0.0D0
!
      call calcextra1010
!
! Step 6:
! [0110]^(4) from [0100]^(5) and [0000]^(5)
! c+1i
!
      allocate(Int0110(9))
      Int0110 = 0.0D0
!
      call calcextra0110
!
! Step 7:
! [2000]^(4) from [1000]^(5) and [0000]^(5)
! a+1i
!
      allocate(Int2000(6))
      Int2000 = 0.0D0
!
      call calcextra2000
!
! Step 8:
! [0200]^(4) from [0100]^(5) and [0000]^(5)
! b+1i
!
      allocate(Int0200(6))
      Int0200 = 0.0D0
!
      call calcextra0200
!
! Step 9:
! [0020]^(4) from [0010]^(5) and [0000]^(5)
! c+1i
!
      allocate(Int0020(6))
      Int0020 = 0.0D0
!
      call calcextra0020
!
! Step 10:
! [2100]^(4) from [2000]^(5) and [1000]^(5)
! b+1i
!
      allocate(Int2100(18))
      Int2100 = 0.0D0
!
      call calcextra2100
!
! Step 11:
! [1200]^(4) from [0200]^(5) and [0100]^(5)
! a+1i
!
      allocate(Int1200(18))
      Int1200 = 0.0D0
!
      call calcextra1200
!
! Step 12:
! [2010]^(4) from [2000]^(5) and [1000]^(5)
! c+1i
!
      allocate(Int2010(18))
      Int2010 = 0.0D0
!
      call calcextra2010
!
! Step 13:
! [1020]^(4) from [0020]^(5) and [1000]^(5)
! a+1i
!
      allocate(Int1020(18))
      Int1020 = 0.0D0
!
      call calcextra1020
!
! Step 14:
! [0210]^(4) from [0200]^(5) and [0100]^(5)
! c+1i
!
      allocate(Int0210(18))
      Int0210 = 0.0D0
!
      call calcextra0210
!
! Step 15:
! [0120]^(4) from [0020]^(5) and [0010]^(5)
! b+1i
!
      allocate(Int0120(18))
      Int0120 = 0.0D0
!
      call calcextra0120
!
! Step 16:
! [2200]^(4) from [2100]^(5), [1100]^(5) and [2000]^(5)
! b+1i
!
      allocate(Int2200(36))
      Int2200 = 0.0D0
!
      call calcextra2200
!
! Step 17:
! [2020]^(4) from [2010]^(5), [1010]^(5) and [2000]^(5)
! c+1i
!
      allocate(Int2020(36))
      Int2020 = 0.0D0
!
      call calcextra2020
!
! Step 18:
! [0220]^(4) from [0210]^(5), [0110]^(5) and [0200]^(5)
! c+1i
!
      allocate(Int0220(36))
      Int0220 = 0.0D0
!
      call calcextra0220
!
! Step 19:
! [2110]^(4) from [2100]^(5), [1100]^(5) and [2000]^(5)
! c+1i
!
      allocate(Int2110(54))
      Int2110 = 0.0D0
!
      call calcextra2110c
!
! Step 20:
! [1210]^(4) from [1200]^(5), [0200]^(5) and [1100]^(5)
! c+1i
!
      allocate(Int1210(54))
      Int1210 = 0.0D0
!
      call calcextra1210
!
! Step 21:
! [2210]^(4) from [2200]^(5), [1200]^(5) and [2100]^(5)
! c+1i
!
      allocate(Int2210(108))
      Int2210 = 0.0D0
!
      call calcextra2210
!
! Step 22:
! [2120]^(4) from [2020]^(5), [1020]^(5) and [2010]^(5)
! b+1i
!
      allocate(Int2120(108))
      Int2120 = 0.0D0
!
      call calcextra2120
!
! Step 23:
! [1220]^(4) from [0220]^(5), [0120]^(5) and [0210]^(5)
! a+1i
!
      allocate(Int1220(108))
      Int1220 = 0.0D0
!
      call calcextra1220
!
! Step 24:
! [2220]^(4) from [2210]^(5), [1210]^(5), [2110]^(5) and [2200]^(5)
! c+1i
!
      allocate(Int2220(216))
      Int2220 = 0.0D0
!
      call calcextra2220
!
! Step 25:
! [2221]^(4) from [2220]^(5), [1220]^(5), [2120]^(5) and [2210]^(5)
! d+1i
!
      allocate(Int2221(648))
      Int2221 = 0.0D0
!
      call calcextra2221
!
      ext(1:648) = Int2221(1:648)
!
      dddp_index = 0
      do d1 = 1,6
        do d2 = 1,6
          do d3 = 1,6
            do p4 = 1,3
              dddp_index = dddp_index + 1
              if(d1.gt.3) ext(dddp_index) = sq3*ext(dddp_index)
              if(d2.gt.3) ext(dddp_index) = sq3*ext(dddp_index)
              if(d3.gt.3) ext(dddp_index) = sq3*ext(dddp_index)
            end do
          end do
        end do
      end do
!
      deallocate(Int1000,Int0100,Int0010,Int1100,Int1010,Int0110)
      deallocate(Int2000,Int0200,Int0020,Int2100,Int1200)
      deallocate(Int2010,Int1020,Int0210,Int0120,Int2200,Int2020,Int0220)
      deallocate(Int2110,Int1210,Int2210,Int2120,Int1220)
      deallocate(Int2220,Int2221)
!
      return
      end if
!
!--------------------------------------------------------------------
! [dddd]
!--------------------------------------------------------------------
      if(Lcode.eq.2222)then
!
! Requires 30 steps
!
! Step 1:
! [1000]^(4) from [0000]^(5)
! a+1i
!
      allocate(Int1000(3))
      Int1000 = 0.0D0
!
      call calcextra1000
!
! Step 2:
! [0100]^(4) from [0000]^(5)
! b+1i
!
      allocate(Int0100(3))
      Int0100 = 0.0D0
!
      call calcextra0100
!
! Step 3:
! [0010]^(4) from [0000]^(5)
! c+1i
!
      allocate(Int0010(3))
      Int0010 = 0.0D0
!
      call calcextra0010
!
! Step 4:
! [1100]^(4) from [1000]^(5) and [0000]^(5)
! b+1i
!
      allocate(Int1100(9))
      Int1100 = 0.0D0
!
      call calcextra1100
!
! Step 5:
! [1010]^(4) from [1000]^(5) and [0000]^(5)
! c+1i
!
      allocate(Int1010(9))
      Int1010 = 0.0D0
!
      call calcextra1010
!
! Step 6:
! [0110]^(4) from [0100]^(5) and [0000]^(5)
! c+1i
!
      allocate(Int0110(9))
      Int0110 = 0.0D0
!
      call calcextra0110
!
! Step 7:
! [2000]^(4) from [1000]^(5) and [0000]^(5)
! a+1i
!
      allocate(Int2000(6))
      Int2000 = 0.0D0
!
      call calcextra2000
!
! Step 8:
! [0200]^(4) from [0100]^(5) and [0000]^(5)
! b+1i
!
      allocate(Int0200(6))
      Int0200 = 0.0D0
!
      call calcextra0200
!
! Step 9:
! [0020]^(4) from [0010]^(5) and [0000]^(5)
! c+1i
!
      allocate(Int0020(6))
      Int0020 = 0.0D0
!
      call calcextra0020
!
! Step 10:
! [2100]^(4) from [2000]^(5) and [1000]^(5)
! b+1i
!
      allocate(Int2100(18))
      Int2100 = 0.0D0
!
      call calcextra2100
!
! Step 11:
! [1200]^(4) from [0200]^(5) and [0100]^(5)
! a+1i
!
      allocate(Int1200(18))
      Int1200 = 0.0D0
!
      call calcextra1200
!
! Step 12:
! [2010]^(4) from [2000]^(5) and [1000]^(5)
! c+1i
!
      allocate(Int2010(18))
      Int2010 = 0.0D0
!
      call calcextra2010
!
! Step 13:
! [1020]^(4) from [0020]^(5) and [1000]^(5)
! a+1i
!
      allocate(Int1020(18))
      Int1020 = 0.0D0
!
      call calcextra1020
!
! Step 14:
! [0210]^(4) from [0200]^(5) and [0100]^(5)
! c+1i
!
      allocate(Int0210(18))
      Int0210 = 0.0D0
!
      call calcextra0210
!
! Step 15:
! [0120]^(4) from [0020]^(5) and [0010]^(5)
! b+1i
!
      allocate(Int0120(18))
      Int0120 = 0.0D0
!
      call calcextra0120
!
! Step 16:
! [2200]^(4) from [2100]^(5), [1100]^(5) and [2000]^(5)
! b+1i
!
      allocate(Int2200(36))
      Int2200 = 0.0D0
!
      call calcextra2200
!
! Step 17:
! [2020]^(4) from [2010]^(5), [1010]^(5) and [2000]^(5)
! c+1i
!
      allocate(Int2020(36))
      Int2020 = 0.0D0
!
      call calcextra2020
!
! Step 18:
! [0220]^(4) from [0210]^(5), [0110]^(5) and [0200]^(5)
! c+1i
!
      allocate(Int0220(36))
      Int0220 = 0.0D0
!
      call calcextra0220
!
! Step 19:
! [2110]^(4) from [2100]^(5), [1100]^(5) and [2000]^(5)
! c+1i
!
      allocate(Int2110(54))
      Int2110 = 0.0D0
!
      call calcextra2110c
!
! Step 20:
! [1210]^(4) from [1200]^(5), [0200]^(5) and [1100]^(5)
! c+1i
!
      allocate(Int1210(54))
      Int1210 = 0.0D0
!
      call calcextra1210
!
! Step 21:
! [1120]^(4) from [1020]^(5), [0020]^(5) and [1010]^(5)
! b+1i
!
      allocate(Int1120(54))
      Int1120 = 0.0D0
!
      call calcextra1120
!
! Step 22:
! [2210]^(4) from [2200]^(5), [1200]^(5) and [2100]^(5)
! c+1i
!
      allocate(Int2210(108))
      Int2210 = 0.0D0
!
      call calcextra2210
!
! Step 23:
! [2120]^(4) from [2020]^(5), [1020]^(5) and [2010]^(5)
! b+1i
!
      allocate(Int2120(108))
      Int2120 = 0.0D0
!
      call calcextra2120
!
! Step 24:
! [1220]^(4) from [0220]^(5), [0120]^(5) and [0210]^(5)
! a+1i
!
      allocate(Int1220(108))
      Int1220 = 0.0D0
!
      call calcextra1220
!
! Step 25:
! [2220]^(4) from [2210]^(5), [1210]^(5), [2110]^(5) and [2200]^(5)
! c+1i
!
      allocate(Int2220(216))
      Int2220 = 0.0D0
!
      call calcextra2220
!
! Step 26:
! [2211]^(4) from [2210]^(5), [1210]^(5), [2110]^(5) and [2200]^(5)
! d+1i
!
      allocate(Int2211(324))
      Int2211 = 0.0D0
!
      call calcextra2211
!
! Step 27:
! [2121]^(4) from [2120]^(5), [1120]^(5), [2020]^(5) and [2110]^(5)
! d+1i
!
      allocate(Int2121(324))
      Int2121 = 0.0D0
!
      call calcextra2121
!
! Step 28:
! [1221]^(4) from [1220]^(5), [0220]^(5), [1120]^(5) and [1210]^(5)
! d+1i
!
      allocate(Int1221(324))
      Int1221 = 0.0D0
!
      call calcextra1221
!
! Step 29:
! [2221]^(4) from [2220]^(5), [1220]^(5), [2120]^(5) and [2210]^(5)
! d+1i
!
      allocate(Int2221(648))
      Int2221 = 0.0D0
!
      call calcextra2221
!
! Step 30:
! [2222]^(4) from [2221]^(5), [1221]^(5), [2121]^(5), [2211]^(5) and [2220]^(5)
! d+1i
!
      allocate(Int2222(1296))
      Int2222 = 0.0D0
!
      call calcextra2222
!
      ext(1:1296) = Int2222(1:1296)
!
      dddd_index = 0
      do d1 = 1,6
        do d2 = 1,6
          do d3 = 1,6
            do d4 = 1,6
              dddd_index = dddd_index + 1
              if(d1.gt.3) ext(dddd_index) = sq3*ext(dddd_index)
              if(d2.gt.3) ext(dddd_index) = sq3*ext(dddd_index)
              if(d3.gt.3) ext(dddd_index) = sq3*ext(dddd_index)
              if(d4.gt.3) ext(dddd_index) = sq3*ext(dddd_index)
            end do
          end do
        end do
      end do
!
      deallocate(Int1000,Int0100,Int0010,Int1100,Int1010,Int0110)
      deallocate(Int2000,Int0200,Int0020,Int2100,Int1200)
      deallocate(Int2010,Int1020,Int0210,Int0120,Int2200,Int2020,Int0220)
      deallocate(Int2110,Int1210,Int1120,Int2210,Int2120,Int1220,Int2220)
      deallocate(Int2211,Int2121,Int1221,Int2221,Int2222)
!
      return
      end if
!
!--------------------------------------------------------------------
! Shouldn't be able to get here, print out an error message
!--------------------------------------------------------------------
!
        write (*,*) 'Orbital angular momentum not recognised by subroutine extraint'
        write (*,*) 'Please check that your basis set contains only s, p and d functions'
!
      end subroutine extraint
!
      subroutine calcextra1000
!******************************************************!
!     [1000]                                           !
!******************************************************!
!
      use extra_RR
!
      implicit none
!      
      integer :: i
!
      do i = 1,3 ! x,y,z
        Int1000(i) = RRa1(i)*G
      end do ! i
!
      end subroutine calcextra1000 
      subroutine calcextra0100
!******************************************************!
!     [0100]                                           !
!******************************************************!
!
      use extra_RR
!
      implicit none
!      
      integer :: i
!
      do i = 1,3 ! x,y,z
        Int0100(i) = RRb1(i)*G
      end do ! i
!
      end subroutine calcextra0100 
      subroutine calcextra0010
!******************************************************!
!     [0010]                                           !
!******************************************************!
!
      use extra_RR
!
      implicit none
!      
      integer :: i
!
      do i = 1,3 ! x,y,z
        Int0010(i) = RRc1(i)*G
      end do ! i
!
      end subroutine calcextra0010 
      subroutine calcextra1100
!******************************************************!
!     [1100]                                           !
!******************************************************!
! b+1i
!
      use extra_RR
!
      implicit none
!      
      integer :: i, p1, pp_index
!
      pp_index = 0
      do p1 = 1,3 ! x,y,z
        do i = 1,3 ! x,y,z
          pp_index = pp_index + 1           
!
          Int1100(pp_index) = RRb1(i)*Int1000(p1)
          if(p1.eq.i)then
            Int1100(pp_index) = Int1100(pp_index)&
                              + RRb2*G
          end if
!
        end do ! i
      end do ! p1
!
      end subroutine calcextra1100 
      subroutine calcextra1010
!******************************************************!
!     [1010]                                           !
!******************************************************!
! c+1i
!
      use extra_RR
!
      implicit none
!      
      integer :: i, p1, pp_index
!
      pp_index = 0
      do p1 = 1,3 ! x,y,z
        do i = 1,3 ! x,y,z
          pp_index = pp_index + 1           
!
          Int1010(pp_index) = RRc1(i)*Int1000(p1)
          if(p1.eq.i)then
            Int1010(pp_index) = Int1010(pp_index) &
                              + RRc2*G
          end if
!
        end do ! i
      end do ! p1
!
      end subroutine calcextra1010 
      subroutine calcextra0110
!******************************************************!
!     [0110]                                           !
!******************************************************!
! c+1i
!
      use extra_RR
!
      implicit none
!      
      integer :: i, p1, pp_index
!
      pp_index = 0
      do p1 = 1,3 ! x,y,z
        do i = 1,3 ! x,y,z
          pp_index = pp_index + 1           
!
          Int0110(pp_index) = RRc1(i)*Int0100(p1)
          if(p1.eq.i)then
            Int0110(pp_index) = Int0110(pp_index) &
                              + RRc3*G
          end if
!
        end do ! i
      end do ! p1
!
      end subroutine calcextra0110 
      subroutine calcextra1110
!******************************************************!
!     [1110]                                           !
!******************************************************!
! c+1i
!
      use extra_RR
!
      implicit none
!      
      integer :: i, p1, p2, pp_index, ppp_index
!
      ppp_index = 0
      pp_index = 0
      do p1 = 1,3 ! x,y,z
        do p2 = 1,3 ! x,y,z
          pp_index = pp_index + 1           
          do i = 1,3 ! x,y,z
            ppp_index = ppp_index + 1           
!
            Int1110(ppp_index) = RRc1(i)*Int1100(pp_index)
            if(i.eq.p1)then
              Int1110(ppp_index) = Int1110(ppp_index) &
                                 + RRc2*Int0100(p2)
            end if
            if(i.eq.p2)then
              Int1110(ppp_index) = Int1110(ppp_index) &
                                 + RRc3*Int1000(p1)
            end if 
!
          end do ! i
        end do ! p2
      end do ! p1
!
      end subroutine calcextra1110 
      subroutine calcextra1111
!******************************************************!
!     [1111]                                           !
!******************************************************!
! d+1i
!
      use extra_RR
!
      implicit none
!      
      integer :: i, p1, p2, p3, pp_index, ppp_index
      integer :: pppp_index
!
      pppp_index = 0
      ppp_index = 0
      pp_index = 0
      do p1 = 1,3 ! x,y,z
        do p2 = 1,3 ! x,y,z
          do p3 = 1,3 ! x,y,z
            ppp_index = ppp_index + 1           
            do i = 1,3 ! x,y,z
              pppp_index = pppp_index + 1           
!
              Int1111(pppp_index) = RRd1(i)*Int1110(ppp_index)
              if(i.eq.p1)then
                pp_index = 3*(p2-1) + p3
                Int1111(pppp_index) = Int1111(pppp_index) &
                                    + RRd2*Int0110(pp_index)
              end if
              if(i.eq.p2)then
                pp_index = 3*(p1-1) + p3
                Int1111(pppp_index) = Int1111(pppp_index) &
                                    + RRd3*Int1010(pp_index)
              end if 
              if(i.eq.p3)then
                pp_index = 3*(p1-1) + p2
                Int1111(pppp_index) = Int1111(pppp_index) &
                                    + RRd4*Int1100(pp_index)
              end if 
!
            end do ! i
          end do ! p3
        end do ! p2
      end do ! p1
!
      end subroutine calcextra1111
      subroutine calcextra2000
!******************************************************!
!     [2000]                                           !
!******************************************************!
! a+1i
!
      use extra_RR
!
      implicit none
!      
      integer :: i, p1, d_index
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! first three d's
      d_index = 0
      do p1 = 1,3 ! x,y,z
        i = p1
        d_index = d_index + 1
!
        Int2000(d_index) = RRa1(i)*Int1000(p1)&
                         + RRa2*G
!
      end do ! p1
! last three d's
      do p1 = 2,3 ! y,z
        do i = 1,p1-1
          d_index = d_index + 1
!
          Int2000(d_index) = RRa1(i)*Int1000(p1)
!
        end do ! i
      end do ! p1
!
      end subroutine calcextra2000
      subroutine calcextra0200
!******************************************************!
!     [0200]                                           !
!******************************************************!
! b+1i
!
      use extra_RR
!
      implicit none
!      
      integer :: i, p1, d_index
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! first three d's
      d_index = 0
      do p1 = 1,3 ! x,y,z
        i = p1
        d_index = d_index + 1
!
        Int0200(d_index) = RRb1(i)*Int0100(p1)&
                         + RRb3*G
!
      end do ! p1
! last three d's
      do p1 = 2,3 ! y,z
        do i = 1,p1-1
          d_index = d_index + 1
!
          Int0200(d_index) = RRb1(i)*Int0100(p1)
!
        end do ! i
      end do ! p1
!
      end subroutine calcextra0200
      subroutine calcextra0020
!******************************************************!
!     [0020]                                           !
!******************************************************!
! c+1i
!
      use extra_RR
!
      implicit none
!      
      integer :: i, p1, d_index
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! first three d's
      d_index = 0
      do p1 = 1,3 ! x,y,z
        i = p1
        d_index = d_index + 1
!
        Int0020(d_index) = RRc1(i)*Int0010(p1)&
                         + RRc4*G
!
      end do ! p1
! last three d's
      do p1 = 2,3 ! y,z
        do i = 1,p1-1
          d_index = d_index + 1
!
          Int0020(d_index) = RRc1(i)*Int0010(p1)
!
        end do ! i
      end do ! p1
!
      end subroutine calcextra0020
      subroutine calcextra2100
!******************************************************!
!     [2100]                                           !
!******************************************************!
! b+1i
!
      use extra_RR
!
      implicit none
!      
      integer :: i, d1, p1, dp_index, a_i
!
      dp_index = 0
      do d1 = 1,6
        do i = 1,3 ! x,y,z
          dp_index = dp_index + 1
!
          a_i = Lvec_d(d1,i)
!
          Int2100(dp_index) = RRb1(i)*Int2000(d1)
!
          if(a_i.ne.0)then ! (a-1i) terms
            v_1i(1:3) = Lvec_d(d1,1:3)
            v_1i(i) = v_1i(i) - 1
            p1 = dot_product(v_1i,xyz)
            Int2100(dp_index) = Int2100(dp_index)&
                              + dble(a_i)*RRb2*Int1000(p1)
          end if
!
        end do ! i
      end do ! d1
!
      end subroutine calcextra2100
      subroutine calcextra2010
!******************************************************!
!     [2010]                                           !
!******************************************************!
! c+1i
!
      use extra_RR
!
      implicit none
!      
      integer :: i, d1, p1, dp_index, a_i
!
      dp_index = 0
      do d1 = 1,6
        do i = 1,3 ! x,y,z
          dp_index = dp_index + 1
!
          a_i = Lvec_d(d1,i)
!
          Int2010(dp_index) = RRc1(i)*Int2000(d1)
!
          if(a_i.ne.0)then ! (a-1i) terms
            v_1i(1:3) = Lvec_d(d1,1:3)
            v_1i(i) = v_1i(i) - 1
            p1 = dot_product(v_1i,xyz)
            Int2010(dp_index) = Int2010(dp_index)&
                              + dble(a_i)*RRc2*Int1000(p1)
          end if
!
        end do ! i
      end do ! d1
!
      end subroutine calcextra2010
      subroutine calcextra1200
!******************************************************!
!     [1200]                                           !
!******************************************************!
! a+1i
!
      use extra_RR
!
      implicit none
!      
      integer :: i, d1, p1, pd_index, b_i
!
      pd_index = 0
      do i = 1,3 ! x,y,z
        do d1 = 1,6
          pd_index = pd_index + 1
!
          b_i = Lvec_d(d1,i)
!
          Int1200(pd_index) = RRa1(i)*Int0200(d1)
!
          if(b_i.ne.0)then ! (b-1i) terms
            v_1i(1:3) = Lvec_d(d1,1:3)
            v_1i(i) = v_1i(i) - 1
            p1 = dot_product(v_1i,xyz)
            Int1200(pd_index) = Int1200(pd_index)&
                              + dble(b_i)*RRa3*Int0100(p1)
          end if
!
        end do ! d1
      end do ! i
!
      end subroutine calcextra1200
      subroutine calcextra1020
!******************************************************!
!     [1020]                                           !
!******************************************************!
! a+1i
!
      use extra_RR
!
      implicit none
!      
      integer :: i, d1, p1, pd_index, c_i
!
      pd_index = 0
      do i = 1,3
        do d1 = 1,6
          pd_index = pd_index + 1
!
          c_i = Lvec_d(d1,i)
!
          Int1020(pd_index) = RRa1(i)*Int0020(d1)
!
          if(c_i.ne.0)then
            v_1i(1:3) = Lvec_d(d1,1:3)
            v_1i(i) = v_1i(i) - 1
            p1 = dot_product(v_1i,xyz)
            Int1020(pd_index) = Int1020(pd_index)&
                              + dble(c_i)*RRa4*Int0010(p1)
          end if
        end do ! d1
      end do ! i
!
      end subroutine calcextra1020
      subroutine calcextra0120
!******************************************************!
!     [0120]                                           !
!******************************************************!
! b+1i
!
      use extra_RR
!
      implicit none
!      
      integer :: i, d1, p1, pd_index, c_i
!
      pd_index = 0
      do i = 1,3
        do d1 = 1,6
          pd_index = pd_index + 1
!
          c_i = Lvec_d(d1,i)
!
          Int0120(pd_index) = RRb1(i)*Int0020(d1)
!
          if(c_i.ne.0)then
            v_1i(1:3) = Lvec_d(d1,1:3)
            v_1i(i) = v_1i(i) - 1
            p1 = dot_product(v_1i,xyz)
            Int0120(pd_index) = Int0120(pd_index)&
                              + dble(c_i)*RRb4*Int0010(p1)
          end if
        end do ! d1
      end do ! i
!
      end subroutine calcextra0120
      subroutine calcextra0210
!******************************************************!
!     [0210]                                           !
!******************************************************!
! c+1i
!
      use extra_RR
!
      implicit none
!      
      integer :: i, d1, p1, dp_index, b_i
!
      dp_index = 0
      do d1 = 1,6
        do i = 1,3 ! x,y,z
          dp_index = dp_index + 1
!
          b_i = Lvec_d(d1,i)
!
          Int0210(dp_index) = RRc1(i)*Int0200(d1)
!
          if(b_i.ne.0)then ! (b-1i) terms
            v_1i(1:3) = Lvec_d(d1,1:3)
            v_1i(i) = v_1i(i) - 1
            p1 = dot_product(v_1i,xyz)
            Int0210(dp_index) = Int0210(dp_index)&
                              + dble(b_i)*RRc3*Int0100(p1)
          end if
!
        end do ! i
      end do ! d1
!
      end subroutine calcextra0210
      subroutine calcextra2110c
!******************************************************!
!     [2110]                                           !
!******************************************************!
! c+1i
!
      use extra_RR
!
      implicit none
!      
      integer :: i, d1, p1, p2, dp_index, dpp_index, a_i
      integer :: pp_index
!
      dp_index = 0
      dpp_index = 0
      do d1 = 1,6
        do p2 = 1,3
          dp_index = dp_index + 1
          do i = 1,3 
            dpp_index = dpp_index + 1
!
            a_i = Lvec_d(d1,i)
!
            Int2110(dpp_index) = RRc1(i)*Int2100(dp_index)
!
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3) = Lvec_d(d1,1:3)
              v_1i(i) = v_1i(i) - 1
              p1 = dot_product(v_1i,xyz)
              pp_index = 3*(p1-1) + p2
              Int2110(dpp_index) = Int2110(dpp_index)&
                                 + dble(a_i)*RRc2*Int1100(pp_index)
            end if
            if(i.eq.p2)then
              Int2110(dpp_index) = Int2110(dpp_index)&
                                 + RRc3*Int2000(d1)
            end if
          end do ! i
        end do ! p2
      end do ! d1
!
      end subroutine calcextra2110c
      subroutine calcextra2110b
!******************************************************!
!     [2110]                                           !
!******************************************************!
! b+1i
!
      use extra_RR
!
      implicit none
!      
      integer :: i, d1, p1, p2, dp_index, dpp_index, a_i
      integer :: pp_index
!
      dp_index = 0
      do d1 = 1,6
        do p2 = 1,3
          dp_index = dp_index + 1
          do i = 1,3 
            dpp_index = 9*(d1-1) + 3*(i-1) + p2
!
            a_i = Lvec_d(d1,i)
!
            Int2110(dpp_index) = RRb1(i)*Int2010(dp_index)
!
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3) = Lvec_d(d1,1:3)
              v_1i(i) = v_1i(i) - 1
              p1 = dot_product(v_1i,xyz)
              pp_index = 3*(p1-1) + p2
              Int2110(dpp_index) = Int2110(dpp_index)&
                                 + dble(a_i)*RRb2*Int1010(pp_index)
            end if
            if(i.eq.p2)then
              Int2110(dpp_index) = Int2110(dpp_index)&
                                 + RRb4*Int2000(d1)
            end if
          end do ! i
        end do ! p2
      end do ! d1
!
      end subroutine calcextra2110b
      subroutine calcextra2011
!******************************************************!
!     [2011]                                           !
!******************************************************!
! d+1i
!
      use extra_RR
!
      implicit none
!      
      integer :: i, d1, p1, p2, dp_index, dpp_index, a_i
      integer :: pp_index
!
      dp_index = 0
      dpp_index = 0
      do d1 = 1,6
        do p2 = 1,3
          dp_index = dp_index + 1
          do i = 1,3 
            dpp_index = dpp_index + 1
!
            a_i = Lvec_d(d1,i)
!
            Int2011(dpp_index) = RRd1(i)*Int2010(dp_index)
!
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3) = Lvec_d(d1,1:3)
              v_1i(i) = v_1i(i) - 1
              p1 = dot_product(v_1i,xyz)
              pp_index = 3*(p1-1) + p2
              Int2011(dpp_index) = Int2011(dpp_index)&
                                 + dble(a_i)*RRd2*Int1010(pp_index)
            end if
            if(i.eq.p2)then
              Int2011(dpp_index) = Int2011(dpp_index)&
                                 + RRd4*Int2000(d1)
            end if
          end do ! i
        end do ! p2
      end do ! d1
!
      end subroutine calcextra2011
      subroutine calcextra1210
!******************************************************!
!     [1210]                                           !
!******************************************************!
! c+1i
!
      use extra_RR
!      
      implicit none
!
      integer :: i, d2, p1, p2, pd_index, pdp_index, b_i
      integer :: pp_index
!
      pdp_index = 0
      pd_index = 0
      do p1 = 1,3 
        do d2 = 1,6
          pd_index = pd_index + 1
          do i = 1,3
            pdp_index = pdp_index + 1
!
            b_i = Lvec_d(d2,i)
!
            Int1210(pdp_index) = RRc1(i)*Int1200(pd_index)
!
            if(i.eq.p1)then
              Int1210(pdp_index) = Int1210(pdp_index)&
                                 + RRc2*Int0200(d2)
            end if
            if(b_i.ne.0)then ! (b-1i) terms
              v_1i(1:3) = Lvec_d(d2,1:3)
              v_1i(i) = v_1i(i) - 1
              p2 = dot_product(v_1i,xyz)
              pp_index = 3*(p1-1) + p2
              Int1210(pdp_index) = Int1210(pdp_index)&
                                 + dble(b_i)*RRc3*Int1100(pp_index)
            end if
          end do ! i
        end do ! d2
      end do ! p1
!
      end subroutine calcextra1210
      subroutine calcextra1120
!******************************************************!
!     [1120]                                           !
!******************************************************!
! b+1i
!
      use extra_RR
!      
      implicit none
!
      integer :: i, p1, d2, p2, c_i
      integer :: ppd_index, pd_index, pp_index
!
      ppd_index = 0
      do p1 = 1,3
        do i = 1,3
          do d2 = 1,6
            ppd_index = ppd_index + 1
            pd_index = 6*(p1-1) + d2
!
            c_i = Lvec_d(d2,i)
!
            Int1120(ppd_index) = RRb1(i)*Int1020(pd_index)
!
            if(i.eq.p1)then
              Int1120(ppd_index) = Int1120(ppd_index)&
                                 + RRb2*Int0020(d2)
            end if
            if(c_i.ne.0)then ! (c-1i) terms
              v_1i(1:3) = Lvec_d(d2,1:3)
              v_1i(i) = v_1i(i) - 1
              p2 = dot_product(v_1i,xyz)
              pp_index = 3*(p1-1) + p2
              Int1120(ppd_index) = Int1120(ppd_index)&
                                 + dble(c_i)*RRb4*Int1010(pp_index)
            end if
          end do ! d2
        end do ! i
      end do ! p1
!
      end subroutine calcextra1120
      subroutine calcextra2111
!******************************************************!
!     [2111]^(m,n,p)                                   !
!******************************************************!
! d+1i
!
      use extra_RR
!      
      implicit none
!
      integer :: i, d1, p1, p2, p3, a_i
      integer :: dp2_index, dp3_index, dpp_index, dppp_index, ppp_index
!
      dppp_index = 0
      dpp_index = 0
      dp2_index = 0
      do d1 = 1,6 
        do p2 = 1,3
          dp2_index = dp2_index + 1
          do p3 = 1,3
            dpp_index = dpp_index + 1
            dp3_index = 3*(d1-1) + p3
            do i = 1,3
              dppp_index = dppp_index + 1
!
              a_i = Lvec_d(d1,i)
!
              Int2111(dppp_index) = RRd1(i)*Int2110(dpp_index)
!
              if(a_i.ne.0)then
                v_1i(1:3) = Lvec_d(d1,1:3)
                v_1i(i) = v_1i(i) - 1 ! (a-1i)
                p1 = dot_product(v_1i,xyz)
                ppp_index = 9*(p1-1) + 3*(p2-1) + p3
                Int2111(dppp_index) = Int2111(dppp_index)&
                                    + dble(a_i)*RRd2*Int1110(ppp_index)
              end if
              if(i.eq.p2)then
                Int2111(dppp_index) = Int2111(dppp_index)&
                                    + RRd3*Int2010(dp3_index)
              end if
              if(i.eq.p3)then
                Int2111(dppp_index) = Int2111(dppp_index)&
                                    + RRd4*Int2100(dp2_index)
              end if
            end do ! i
          end do ! p3
        end do ! p2
      end do ! d1
!
      end subroutine calcextra2111
      subroutine calcextra2200
!******************************************************!
!     [2200]^(m,n,p)                                   !
!******************************************************!
! b+1i
!
      use extra_RR
!      
      implicit none
!
      integer :: i, d1, p1, p2, dp_index, dd_index, a_i
      integer :: pp_index
!
      dd_index = 0
      do d1 = 1,6
        do p2 = 1,3
          i = p2 ! do xx, yy, zz first
          dd_index = dd_index + 1
          dp_index = 3*(d1-1) + p2
!
          a_i = Lvec_d(d1,i) ! angular momentum vector
!
          Int2200(dd_index) = RRb1(i)*Int2100(dp_index)&
                            + RRb3*Int2000(d1)
!
          if(a_i.ne.0)then ! (a-1i) terms
            v_1i(1:3) = Lvec_d(d1,1:3)
            v_1i(i) = v_1i(i) - 1 ! a-1_i
            p1 = dot_product(v_1i,xyz)
            pp_index = 3*(p1-1) + p2
            Int2200(dd_index) = Int2200(dd_index) &
                              + dble(a_i)*RRb2*Int1100(pp_index)
          end if
        end do ! p2
!
        do p2 = 2,3 ! y, z
          dp_index = 3*(d1-1) + p2
          do i = 1,p2-1 ! x, y
            dd_index = dd_index + 1
!
! i ne p2, therefore no (b-1i) terms
!
            a_i = Lvec_d(d1,i) ! angular momentum vector
!
            Int2200(dd_index) = RRb1(i)*Int2100(dp_index)
!
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3) = Lvec_d(d1,1:3)
              v_1i(i) = v_1i(i) - 1 ! a-1_i
              p1 = dot_product(v_1i,xyz)
              pp_index = 3*(p1-1) + p2
              Int2200(dd_index) = Int2200(dd_index) &
                                + dble(a_i)*RRb2*Int1100(pp_index)
            end if
          end do ! i
        end do ! p2
      end do ! d1
!
      end subroutine calcextra2200
      subroutine calcextra2020
!******************************************************!
!     [2020]                                           !
!******************************************************!
! c+1i
!
      use extra_RR
!      
      implicit none
!
      integer :: i, d1, p1, p2, dp_index, dd_index, a_i
      integer :: pp_index
!
      dd_index = 0
      do d1 = 1,6
        do p2 = 1,3
          i = p2 ! do xx, yy, zz first
          dd_index = dd_index + 1
          dp_index = 3*(d1-1) + p2
!
          a_i = Lvec_d(d1,i) ! angular momentum vector
!
          Int2020(dd_index) = RRc1(i)*Int2010(dp_index)&
                            + RRc4*Int2000(d1)
!
          if(a_i.ne.0)then ! (a-1i) terms
            v_1i(1:3) = Lvec_d(d1,1:3)
            v_1i(i) = v_1i(i) - 1 ! a-1_i
            p1 = dot_product(v_1i,xyz)
            pp_index = 3*(p1-1) + p2
            Int2020(dd_index) = Int2020(dd_index) &
                              + dble(a_i)*RRc2*Int1010(pp_index)
          end if
        end do ! p2
!
        do p2 = 2,3 ! y, z
          dp_index = 3*(d1-1) + p2
          do i = 1,p2-1 ! x, y
            dd_index = dd_index + 1
!
! i ne p2, therefore no (b-1i) terms
!
            a_i = Lvec_d(d1,i) ! angular momentum vector
!
            Int2020(dd_index) = RRc1(i)*Int2010(dp_index)
!
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3) = Lvec_d(d1,1:3)
              v_1i(i) = v_1i(i) - 1 ! a-1_i
              p1 = dot_product(v_1i,xyz)
              pp_index = 3*(p1-1) + p2
              Int2020(dd_index) = Int2020(dd_index) &
                                + dble(a_i)*RRc2*Int1010(pp_index)
            end if
          end do ! i
        end do ! p2
      end do ! d1
!
      end subroutine calcextra2020
      subroutine calcextra0220
!******************************************************!
!     [0220]                                           !
!******************************************************!
! c+1i
!
      use extra_RR
!      
      implicit none
!
      integer :: i, d1, p1, p2, dp_index, dd_index, b_i
      integer :: pp_index
!
      dd_index = 0
      do d1 = 1,6
        do p2 = 1,3
          i = p2 ! do xx, yy, zz first
          dd_index = dd_index + 1
          dp_index = 3*(d1-1) + p2
!
          b_i = Lvec_d(d1,i) ! angular momentum vector
!
          Int0220(dd_index) = RRc1(i)*Int0210(dp_index)&
                            + RRc4*Int0200(d1)
!
          if(b_i.ne.0)then ! (b-1i) terms
            v_1i(1:3) = Lvec_d(d1,1:3)
            v_1i(i) = v_1i(i) - 1 ! b-1_i
            p1 = dot_product(v_1i,xyz)
            pp_index = 3*(p1-1) + p2
            Int0220(dd_index) = Int0220(dd_index) &
                              + dble(b_i)*RRc3*Int0110(pp_index)
          end if
        end do ! p2
!
        do p2 = 2,3 ! y, z
          dp_index = 3*(d1-1) + p2
          do i = 1,p2-1 ! x, y
            dd_index = dd_index + 1
!
! i ne p2, therefore no (b-1i) terms
!
            b_i = Lvec_d(d1,i) ! angular momentum vector
!
            Int0220(dd_index) = RRc1(i)*Int0210(dp_index)
!
            if(b_i.ne.0)then ! (b-1i) terms
              v_1i(1:3) = Lvec_d(d1,1:3)
              v_1i(i) = v_1i(i) - 1 ! b-1_i
              p1 = dot_product(v_1i,xyz)
              pp_index = 3*(p1-1) + p2
              Int0220(dd_index) = Int0220(dd_index) &
                                + dble(b_i)*RRc3*Int0110(pp_index)
            end if
          end do ! i
        end do ! p2
      end do ! d1
!
      end subroutine calcextra0220
      subroutine calcextra2210
!******************************************************!
!     [2210]                                           !
!******************************************************!
! c+1i
!
      use extra_RR
!      
      implicit none
!
      integer :: i, d1, d2, p1, p2, dd_index, a_i, b_i
      integer :: ddp_index, dp_index, pd_index
!
      ddp_index = 0
      dd_index = 0
      do d1 = 1,6
        do d2 = 1,6
          dd_index = dd_index + 1
          do i = 1,3
            ddp_index=ddp_index+1
!
            a_i = Lvec_d(d1,i)
            b_i = Lvec_d(d2,i)
!
            Int2210(ddp_index) = RRc1(i)*Int2200(dd_index)
!
            if(a_i.ne.0)then
              v_1i(1:3) = Lvec_d(d1,1:3)
              v_1i(i) = v_1i(i)-1
              p1 = dot_product(v_1i,xyz)
              pd_index = 6*(p1-1) + d2
              Int2210(ddp_index) = Int2210(ddp_index) &
                                 + dble(a_i)*RRc2*Int1200(pd_index)
            end if
            if(b_i.ne.0)then
              v_1i(1:3) = Lvec_d(d2,1:3)
              v_1i(i) = v_1i(i)-1
              p2 = dot_product(v_1i,xyz)
              dp_index = 3*(d1-1) + p2
              Int2210(ddp_index) = Int2210(ddp_index) &
                                 + dble(b_i)*RRc3*Int2100(dp_index)
            end if
          end do ! i
        end do ! d2
      end do ! d1
!
      end subroutine calcextra2210
      subroutine calcextra2120
!******************************************************!
!     [2120]                                           !
!******************************************************!
! b+1i
!
      use extra_RR
!      
      implicit none
!
      integer :: i, d1, p1, d2, p2, a_i, c_i
      integer :: dpd_index, dd_index, pd_index, dp_index
!
      dd_index = 0
      do d1 = 1,6
        do d2 = 1,6
          dd_index = dd_index + 1
          do i = 1,3
            dpd_index = 18*(d1-1) + 6*(i-1) + d2
!
            a_i = Lvec_d(d1,i)
            c_i = Lvec_d(d2,i)
!
            Int2120(dpd_index) = RRb1(i)*Int2020(dd_index)
!
            if(a_i.ne.0)then
              v_1i(1:3) = Lvec_d(d1,1:3)
              v_1i(i) = v_1i(i)-1
              p1 = dot_product(v_1i,xyz)
              pd_index = 6*(p1-1) + d2
              Int2120(dpd_index) = Int2120(dpd_index) &
                                 + dble(a_i)*RRb2*Int1020(pd_index)
              end if
              if(c_i.ne.0)then
                v_1i(1:3) = Lvec_d(d2,1:3)
                v_1i(i) = v_1i(i)-1
                p2 = dot_product(v_1i,xyz)
                dp_index = 3*(d1-1) + p2
                Int2120(dpd_index) = Int2120(dpd_index) &
                                   + dble(c_i)*RRb4*Int2010(dp_index)
              end if
            end do ! i
          end do ! d2
        end do ! d1
!
      end subroutine calcextra2120
      subroutine calcextra1220
!******************************************************!
!     [1220]                                           !
!******************************************************!
! a+1i
!
      use extra_RR
!      
      implicit none
!
      integer :: i, d1, d2, p1, p2, dd_index, b_i, c_i
      integer :: pdd_index, dp_index, pd_index
!
      dd_index = 0
      do d1 = 1,6
        do d2 = 1,6
          dd_index = dd_index + 1
          do i = 1,3
            pdd_index = 36*(i-1) + 6*(d1-1) + d2
!
            b_i = Lvec_d(d1,i)
            c_i = Lvec_d(d2,i)
!
            Int1220(pdd_index) = RRa1(i)*Int0220(dd_index)
!
            if(b_i.ne.0)then
              v_1i(1:3) = Lvec_d(d1,1:3)
              v_1i(i) = v_1i(i)-1
              p1 = dot_product(v_1i,xyz)
              pd_index = 6*(p1-1) + d2
              Int1220(pdd_index) = Int1220(pdd_index) &
                                 + dble(b_i)*RRa3*Int0120(pd_index)
              end if
              if(c_i.ne.0)then
                v_1i(1:3) = Lvec_d(d2,1:3)
                v_1i(i) = v_1i(i)-1
                p2 = dot_product(v_1i,xyz)
                dp_index = 3*(d1-1) + p2
                Int1220(pdd_index) = Int1220(pdd_index) &
                                   + dble(c_i)*RRa4*Int0210(dp_index)
              end if
            end do ! i
          end do ! d2
        end do ! d1
!
      end subroutine calcextra1220
      subroutine calcextra2211
!******************************************************!
!     [2211]                                           !
!******************************************************!
! d+1i
!
      use extra_RR
!      
      implicit none
!
      integer :: i, d1, d2, p1, p2, p3, a_i, b_i
      integer :: ddpp_index, ddp_index, pdp_index, dpp_index, dd_index
!
      ddpp_index = 0
      ddp_index = 0
      dd_index = 0
      do d1 = 1,6
        do d2 = 1,6
          dd_index = dd_index + 1
          do p3 = 1,3
            ddp_index = ddp_index + 1
            do i = 1,3
              ddpp_index = ddpp_index + 1
!
              a_i = Lvec_d(d1,i)
              b_i = Lvec_d(d2,i)
!
              Int2211(ddpp_index) = RRd1(i)*Int2210(ddp_index)
!
              if(a_i.ne.0)then
                v_1i(1:3) = Lvec_d(d1,1:3)
                v_1i(i) = v_1i(i)-1
                p1 = dot_product(v_1i,xyz)
                pdp_index = 18*(p1-1) + 3*(d2-1) + p3
                Int2211(ddpp_index) = Int2211(ddpp_index) &
                                    + dble(a_i)*RRd2*Int1210(pdp_index)
              end if
              if(b_i.ne.0)then
                v_1i(1:3) = Lvec_d(d2,1:3)
                v_1i(i) = v_1i(i)-1
                p2 = dot_product(v_1i,xyz)
                dpp_index = 9*(d1-1) + 3*(p2-1) + p3
                Int2211(ddpp_index) = Int2211(ddpp_index) &
                                    + dble(b_i)*RRd3*Int2110(dpp_index)
              end if
              if(i.eq.p3)then
                Int2211(ddpp_index) = Int2211(ddpp_index) &
                                    + RRd4*Int2200(dd_index)
              end if
            end do ! i
          end do ! p3
        end do ! d2
      end do ! d1
!
      end subroutine calcextra2211
      subroutine calcextra2121
!******************************************************!
!     [2121]                                           !
!******************************************************!
! d+1i
!
      use extra_RR
!      
      implicit none
!
      integer :: i, d1, p1, p2, d3, p3, a_i, c_i
      integer :: dpdp_index, dpd_index, ppd_index, dpp_index, dd_index
!
      dpdp_index = 0
      dpd_index = 0
      do d1 = 1,6
        do p2 = 1,3
          do d3 = 1,6
            dpd_index = dpd_index + 1
            do i = 1,3
              dpdp_index = dpdp_index + 1
!
              a_i = Lvec_d(d1,i)
              c_i = Lvec_d(d3,i)
!
              Int2121(dpdp_index) = RRd1(i)*Int2120(dpd_index)
!
              if(a_i.ne.0)then
                v_1i(1:3) = Lvec_d(d1,1:3)
                v_1i(i) = v_1i(i)-1
                p1 = dot_product(v_1i,xyz)
                ppd_index = 18*(p1-1) + 6*(p2-1) + d3
                Int2121(dpdp_index) = Int2121(dpdp_index) &
                                    + dble(a_i)*RRd2*Int1120(ppd_index)
              end if
              if(i.eq.p2)then
                dd_index = 6*(d1-1) + d3
                Int2121(dpdp_index) = Int2121(dpdp_index) &
                                    + RRd3*Int2020(dd_index)
              end if
              if(c_i.ne.0)then
                v_1i(1:3) = Lvec_d(d3,1:3)
                v_1i(i) = v_1i(i)-1
                p3 = dot_product(v_1i,xyz)
                dpp_index = 9*(d1-1) + 3*(p2-1) + p3
                Int2121(dpdp_index) = Int2121(dpdp_index) &
                                    + dble(c_i)*RRd4*Int2110(dpp_index)
              end if
            end do ! i
          end do ! d3
        end do ! p2
      end do ! d1
!
      end subroutine calcextra2121
      subroutine calcextra1221
!******************************************************!
!     [1221]                                           !
!******************************************************!
! d+1i
!
      use extra_RR
!      
      implicit none
!
      integer :: i, p1, d2, p2, d3, p3, b_i, c_i
      integer :: pddp_index, pdd_index, ppd_index, pdp_index, dd_index
!
      pddp_index = 0
      pdd_index = 0
      do p1 = 1,3
        do d2 = 1,6
          do d3 = 1,6
            pdd_index = pdd_index + 1
            do i = 1,3
              pddp_index = pddp_index + 1
!
              b_i = Lvec_d(d2,i)
              c_i = Lvec_d(d3,i)
!
             Int1221(pddp_index) = RRd1(i)*Int1220(pdd_index)
!
              if(i.eq.p1)then
                dd_index = 6*(d2-1) + d3
                Int1221(pddp_index) = Int1221(pddp_index) &
                                    + RRd2*Int0220(dd_index)
              end if
              if(b_i.ne.0)then
                v_1i(1:3) = Lvec_d(d2,1:3)
                v_1i(i) = v_1i(i)-1
                p2 = dot_product(v_1i,xyz)
                ppd_index = 18*(p1-1) + 6*(p2-1) + d3
                Int1221(pddp_index) = Int1221(pddp_index) &
                                    + dble(b_i)*RRd3*Int1120(ppd_index)
              end if
              if(c_i.ne.0)then
                v_1i(1:3) = Lvec_d(d3,1:3)
                v_1i(i) = v_1i(i)-1
                p3 = dot_product(v_1i,xyz)
                pdp_index = 18*(p1-1) + 3*(d2-1) + p3
                Int1221(pddp_index) = Int1221(pddp_index) &
                                    + dble(c_i)*RRd4*Int1210(pdp_index)
              end if
            end do ! i
          end do ! d3
        end do ! p2
      end do ! d1
!
      end subroutine calcextra1221
      subroutine calcextra2220
!******************************************************!
!     [2220]                                           !
!******************************************************!
! c+1i
!
      use extra_RR
!      
      implicit none
!
      integer :: i, d1, p1, d2, p2, p3, a_i, b_i
      integer :: ddd_index, ddp_index, pdp_index, dpp_index, dd_index
!
      ddd_index = 0
      dd_index = 0
      do d1 = 1,6
        do d2 = 1,6
          dd_index = dd_index + 1
          do p3 = 1,3
            i = p3
            ddp_index = 18*(d1-1) + 3*(d2-1) + p3
            ddd_index = ddd_index + 1
!
            a_i = Lvec_d(d1,i)
            b_i = Lvec_d(d2,i)
!
            Int2220(ddd_index) = RRc1(i)*Int2210(ddp_index)&
                               + RRc4*Int2200(dd_index)
!
            if(a_i.ne.0)then
              v_1i(1:3) = Lvec_d(d1,1:3)
              v_1i(i) = v_1i(i)-1
              p1 = dot_product(v_1i,xyz)
              pdp_index = 18*(p1-1) + 3*(d2-1) + p3
              Int2220(ddd_index) = Int2220(ddd_index) &
                                 + dble(a_i)*RRc2*Int1210(pdp_index)
            end if
            if(b_i.ne.0)then
              v_1i(1:3) = Lvec_d(d2,1:3)
              v_1i(i) = v_1i(i)-1
              p2 = dot_product(v_1i,xyz)
              dpp_index = 9*(d1-1) + 3*(p2-1) + p3
              Int2220(ddd_index) = Int2220(ddd_index) &
                                 + dble(b_i)*RRc3*Int2110(dpp_index)
            end if
          end do ! p3
          do p3 = 2,3
            ddp_index = 18*(d1-1) + 3*(d2-1) + p3
            do i = 1,p3-1
              ddd_index = ddd_index + 1
!
              a_i = Lvec_d(d1,i)
              b_i = Lvec_d(d2,i)
!
              Int2220(ddd_index) = RRc1(i)*Int2210(ddp_index)
!
              if(a_i.ne.0)then
                v_1i(1:3) = Lvec_d(d1,1:3)
                v_1i(i) = v_1i(i)-1
                p1 = dot_product(v_1i,xyz)
                pdp_index = 18*(p1-1) + 3*(d2-1) + p3
                Int2220(ddd_index) = Int2220(ddd_index) &
                                   + dble(a_i)*RRc2*Int1210(pdp_index)
              end if
              if(b_i.ne.0)then
                v_1i(1:3) = Lvec_d(d2,1:3)
                v_1i(i) = v_1i(i)-1
                p2 = dot_product(v_1i,xyz)
                dpp_index = 9*(d1-1) + 3*(p2-1) + p3
                Int2220(ddd_index) = Int2220(ddd_index) &
                                   + dble(b_i)*RRc3*Int2110(dpp_index)
              end if
            end do ! i   
          end do ! p3
        end do ! d2 
      end do ! d1
!
      end subroutine calcextra2220
      subroutine calcextra2221
!******************************************************!
!     [2221]                                           !
!******************************************************!
! d+1i
!
      use extra_RR
!      
      implicit none
!
      integer :: i, d1, p1, d2, p2, d3, p3, a_i, b_i, c_i
      integer :: dddp_index, ddd_index, pdd_index, dpd_index, ddp_index
!
      dddp_index = 0
      ddd_index = 0
      do d1 = 1,6
        do d2 = 1,6
          do d3 = 1,6
            ddd_index = ddd_index + 1
            do i = 1,3
              dddp_index = dddp_index + 1
!
              a_i = Lvec_d(d1,i)
              b_i = Lvec_d(d2,i)
              c_i = Lvec_d(d3,i)
!
              Int2221(dddp_index) = RRd1(i)*Int2220(ddd_index)
!
              if(a_i.ne.0)then
                v_1i(1:3) = Lvec_d(d1,1:3)
                v_1i(i) = v_1i(i)-1
                p1 = dot_product(v_1i,xyz)
                pdd_index = 36*(p1-1) + 6*(d2-1) + d3
                Int2221(dddp_index) = Int2221(dddp_index) &
                                    + dble(a_i)*RRd2*Int1220(pdd_index)
              end if
              if(b_i.ne.0)then
                v_1i(1:3) = Lvec_d(d2,1:3)
                v_1i(i) = v_1i(i)-1
                p2 = dot_product(v_1i,xyz)
                dpd_index = 18*(d1-1) + 6*(p2-1) + d3
                Int2221(dddp_index) = Int2221(dddp_index) &
                                    + dble(b_i)*RRd3*Int2120(dpd_index)
              end if
              if(c_i.ne.0)then
                v_1i(1:3) = Lvec_d(d3,1:3)
                v_1i(i) = v_1i(i)-1
                p3 = dot_product(v_1i,xyz)
                ddp_index = 18*(d1-1) + 3*(d2-1) + p3
                Int2221(dddp_index) = Int2221(dddp_index) &
                                    + dble(c_i)*RRd4*Int2210(ddp_index)
              end if
            end do ! i
          end do ! d3
        end do ! d2 
      end do ! d1
!
      end subroutine calcextra2221
      subroutine calcextra2222
!******************************************************!
!     [2222]                                           !
!******************************************************!
! d+1i
!
      use extra_RR
!      
      implicit none
!
      integer :: i, d1, p1, d2, p2, d3, p3, p4, a_i, b_i, c_i
      integer :: dddd_index, dddp_index, pddp_index, dpdp_index, ddpp_index, ddd_index
!
      dddd_index = 0
      ddd_index = 0
      do d1 = 1,6
        do d2 = 1,6
          do d3 = 1,6
            ddd_index = ddd_index + 1
            do p4 = 1,3
              i = p4
              dddp_index = 108*(d1-1) + 18*(d2-1) + 3*(d3-1) + p4
              dddd_index = dddd_index + 1
!
              a_i = Lvec_d(d1,i)
              b_i = Lvec_d(d2,i)
              c_i = Lvec_d(d3,i)
!
              Int2222(dddd_index) = RRd1(i)*Int2221(dddp_index)&
                                  + RRd5*Int2220(ddd_index)
!
              if(a_i.ne.0)then
                v_1i(1:3) = Lvec_d(d1,1:3)
                v_1i(i) = v_1i(i)-1
                p1 = dot_product(v_1i,xyz)
                pddp_index = 108*(p1-1) + 18*(d2-1) + 3*(d3-1) + p4
                Int2222(dddd_index) = Int2222(dddd_index) &
                                    + dble(a_i)*RRd2*Int1221(pddp_index)
              end if
              if(b_i.ne.0)then
                v_1i(1:3) = Lvec_d(d2,1:3)
                v_1i(i) = v_1i(i)-1
                p2 = dot_product(v_1i,xyz)
                dpdp_index = 54*(d1-1) + 18*(p2-1) + 3*(d3-1) + p4
                Int2222(dddd_index) = Int2222(dddd_index) &
                                    + dble(b_i)*RRd3*Int2121(dpdp_index)
              end if
              if(c_i.ne.0)then
                v_1i(1:3) = Lvec_d(d3,1:3)
                v_1i(i) = v_1i(i)-1
                p3 = dot_product(v_1i,xyz)
                ddpp_index = 54*(d1-1) + 9*(d2-1) + 3*(p3-1) + p4
                Int2222(dddd_index) = Int2222(dddd_index) &
                                    + dble(c_i)*RRd4*Int2211(ddpp_index)
              end if
            end do ! p4
!
            do p4 = 2,3
              dddp_index = 108*(d1-1) + 18*(d2-1) + 3*(d3-1) + p4
              do i = 1,p4-1
                dddd_index = dddd_index + 1
!
                a_i = Lvec_d(d1,i)
                b_i = Lvec_d(d2,i)
                c_i = Lvec_d(d3,i)
!
                Int2222(dddd_index) = RRd1(i)*Int2221(dddp_index)
!
                if(a_i.ne.0)then
                  v_1i(1:3) = Lvec_d(d1,1:3)
                  v_1i(i) = v_1i(i)-1
                  p1 = dot_product(v_1i,xyz)
                  pddp_index = 108*(p1-1) + 18*(d2-1) + 3*(d3-1) + p4
                  Int2222(dddd_index) = Int2222(dddd_index) &
                                      + dble(a_i)*RRd2*Int1221(pddp_index)
                end if
                if(b_i.ne.0)then
                  v_1i(1:3) = Lvec_d(d2,1:3)
                  v_1i(i) = v_1i(i)-1
                  p2 = dot_product(v_1i,xyz)
                  dpdp_index = 54*(d1-1) + 18*(p2-1) + 3*(d3-1) + p4
                  Int2222(dddd_index) = Int2222(dddd_index) &
                                      + dble(b_i)*RRd3*Int2121(dpdp_index)
                end if
                if(c_i.ne.0)then
                  v_1i(1:3) = Lvec_d(d3,1:3)
                  v_1i(i) = v_1i(i)-1
                  p3 = dot_product(v_1i,xyz)
                  ddpp_index = 54*(d1-1) + 9*(d2-1) + 3*(p3-1) + p4
                  Int2222(dddd_index) = Int2222(dddd_index) &
                                      + dble(c_i)*RRd4*Int2211(ddpp_index)
                end if
              end do ! i
            end do ! p4
          end do ! d3
        end do ! d2 
      end do ! d1
!
      end subroutine calcextra2222
!
