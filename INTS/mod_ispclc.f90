      module SP_integrals
!*****************************************************************************************************************
!     Date last modified: March 27, 2003                                                            Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Desciption: Local module used by the SP two-electron integral package                                      *
!*****************************************************************************************************************
! Modules:
      USE program_constants
      USE type_basis_set
      USE QM_defaults
      USE INT_objects
      USE gamma_fn

      implicit none
!
      integer :: MAXINT  ! Maximum number of integrals for for any 4 shells (NAOMAX**4)
      integer :: Ishell_save,Jshell_save,Kshell_save,Lshell_save
      logical :: FIRST_loop
!
      integer Ifrst,Ilast,Ishell
      integer Jfrst,Jlast,Jshell
      integer Kfrst,Klast,Kshell
      integer Lfrst,Llast,Lshell
!
! Work arrays:
      double precision, dimension(:,:), allocatable :: SHRABSQ

! do I,J,K,L LOOPs
      integer :: Iao,Iatmshl,Iatom,Irange,Igauss,IGBGN,IGEND
      integer :: Jao,Jatmshl,Jatom,Jrange,Jgauss,JGBGN,JGEND
      integer :: Kao,Katmshl,Katom,Krange,Kgauss,KGBGN,KGEND
      integer :: Lao,Latmshl,Latom,Lrange,Lgauss,LGBGN,LGEND
      integer :: AOI,AOJ,AOK,AOL
      integer :: Mtype
!
! Scalars:
      integer Iashl,Jashl,Kashl,Lashl
!
      double precision RAB,RCD,P11,P12,P13,P21,P22,P23,P31,P32,P33,Q11,Q12,Q13,Q21,Q22,Q23,Q31,Q32,Q33, &
                       ACX,ACY,ACZ,ACY2,COSG,SING,AP,BP,PX,PY,PZ,QX,QY,QZ,RPQ,RPQSQ,PQ1,PQ2,PQ3, &
                       C11,C12,C13,C21,C22,C23,C31,C32,C33
      double precision :: R13,R14,R33,R34,SINP
      double precision :: XA,XB,XC,XD,YA,YB,YC,YD,ZA,ZB,ZC,ZD,RABSQ,RCDSQ
! R13 = Component of CQ along penultimate X-axis.
! R14 = Component of DQ along penultimate X-axis.
! R33 = Component of CQ along penultimate Z-axis.
! R34 = Component of DQ along penultimate Z-axis.
!
      double precision, parameter :: THRTY4=34.9868366552497D0  ! 2*PI_val**2.5
      double precision, parameter :: PT7853=PI_val/4.0D0  ! PI_val/4.0
      double precision, parameter :: TWENTY=20.0D0 ! = 1.0/increment in used by gamgen (0.05)
      double precision, parameter :: TENM12=1.0D-12
      double precision, parameter :: ONEPT5=1.5D0
      double precision, parameter :: TWOPT5=2.5D0
      double precision, parameter :: THRPT5=3.5D0
      double precision, parameter :: PT25=0.25D0
!
      double precision, dimension(:), target, allocatable :: GOUT ! work array to store ijkl/ikjl/iljk integrals
      double precision, dimension(:), allocatable :: Gout_ijkl
      double precision, dimension(:), allocatable :: Gout_ikjl
      double precision, dimension(:), allocatable :: Gout_iljk
!
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine ISP_SSSS
!***********************************************************************
!     Date last modified: May 24, 2002                     Version 1.0 *
!     Author: R. A. Poirier                                            *
!     Description: Special fast routine for SSSS integrals.            *
!***********************************************************************
! Modules:

      implicit none
!
! Local scalars:
      integer I,IGOUT,Iset,NUMINT
!
! Begin:
      IGOUT=1
      include 'mungauss_do_IJKL'
      NUMINT=0
!
      GOUT_ijkl(1)=ZERO
      GOUT_ikjl(1)=ZERO
      GOUT_iljk(1)=ZERO
! Set 1
      Iashl=Iatmshl
      Jashl=Jatmshl
      Kashl=Katmshl
      Lashl=Latmshl
      call SET_shells (Ishell, Jshell, Kshell, Lshell)
      call SP0000 (Gout_ijkl)
      IF(DABS(GOUT_ijkl(1)).GT.I2E_ACC)then
        NUMINT=NUMINT+1
      else
        GOUT_ijkl(1)=ZERO
      end IF
!
! Set 2
      if(Mtype.eq.3)then
        Iashl=Iatmshl
        Jashl=Latmshl
        Kashl=Jatmshl
        Lashl=Katmshl
        call SET_shells (Ishell, Lshell, Jshell, Kshell)
        call SP0000 (Gout_ikjl)
      else if(Mtype.lt.6)then
        Iashl=Iatmshl
        Jashl=Katmshl
        Kashl=Jatmshl
        Lashl=Latmshl
        call SET_shells (Ishell, Kshell, Jshell, Lshell)
        call SP0000 (Gout_ikjl)
      end if ! Mtype
        IF(DABS(GOUT_ikjl(1)).GT.I2E_ACC)then
          NUMINT=NUMINT+1
        else
          GOUT_ikjl(1)=ZERO
        end IF
!
! Set 3
      if(Mtype.eq.1)then
        Iashl=Iatmshl
        Jashl=Latmshl
        Kashl=Jatmshl
        Lashl=Katmshl
        call SET_shells (Ishell, Lshell, Jshell, Kshell)
        call SP0000 (Gout_iljk)
        IF(DABS(GOUT_iljk(1)).GT.I2E_ACC)then
          NUMINT=NUMINT+1
        else
          GOUT_iljk(1)=ZERO
        end IF
      end if ! Mtype.eq.1
!
      IF(NUMINT.NE.0)call I2E_SHL_SSSS (NUMINT)
!
      include 'mungauss_endo_IJKL'
!
! End of routine ISP_SSSS
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine SP0000 (INTS_out)
!***********************************************************************
!     Date last modified: May 24, 2002                                 *
!     Author:                                                          *
!     Description: Special fast routine for P loop for SSSS.           *
!***********************************************************************
! Modules:

      implicit none
!
! Output array:
      double precision INTS_out(1)
!
! Local scalars:
      integer I,ISML,ISMLP,ISMLQ,N
      double precision AQX,AQZ,AUXVAR,CQ,EQCDI,F0,Gijkl,CS,EQCD,APP,BPP,DP00P,DS,EKLD,Hkl,QPERP2,QQ,THETA,THETA2, &
                       THETA3,THETA4,TERM
      double precision EPABI,AS,EPAB,BS,EPABB,Expij,Expkl,EXPARG,ExpCCij,ExpCCkl
!
! Begin:
      Gijkl=ZERO

! Preliminary Q loop.
      do Kgauss=KGBGN,KGend
      CS=Basis%gaussian(Kgauss)%exp
      do Lgauss=LGBGN,LGend
      DS=Basis%gaussian(Lgauss)%exp
      EQCD=CS+DS
      EQCDI=ONE/EQCD
      EKLD=DS*EQCDI
      EXPARG=EKLD*CS*RCDSQ
      IF(EXPARG.LE.I2E_expcut)then
      Expkl=DEXP(-EXPARG)*EQCDI
      ExpCCkl=Expkl*Basis%gaussian(Kgauss)%CONTRC*Basis%gaussian(Lgauss)%CONTRC
      IF(dabs(ExpCCkl).GT.ERROR1)then
        ISMLQ=0
      else
        ISMLQ=1
      end IF
      IF(dabs(ExpCCkl).GT.ERROR2)then
      CQ=EKLD*RCD
      AQX=ACX+SING*CQ
      AQZ=ACZ+COSG*CQ
      QPERP2=AQX*AQX+ACY2

      Hkl=ZERO
      do Igauss=IGBGN,IGend
      AS=Basis%gaussian(Igauss)%exp
      do Jgauss=JGBGN,JGend
      BS=Basis%gaussian(Jgauss)%exp
      EPAB=AS+BS
      EPABI=ONE/EPAB
      EPABB=BS*EPABI
      APP=EPABB*RAB
      BPP=APP-RAB
      EXPARG=AS*EPABB*RABSQ
      IF(EXPARG.GT.I2E_expcut)then
        ISMLP=2
        DP00P=ZERO
      else
        Expij=DEXP(-EXPARG)*EPABI
        ExpCCij=Expij*Basis%gaussian(Igauss)%CONTRC*Basis%gaussian(Jgauss)%CONTRC
        IF(dabs(ExpCCij).GT.ERROR1)then
          ISMLP=0
        else
          IF(dabs(ExpCCij).GT.ERROR2)then
            ISMLP=1
          else
            ISMLP=2
          end IF
        end IF
        DP00P=THRTY4*ExpCCij
      end IF
!****MUST CHANGE VARIABLES HERE*****
      ISML=ISMLQ+ISMLP
!* --------------------------------------------------------
      IF(ISML.LT.2)then
      IF(ISML.LT.1)then
        AUXVAR=VAR1
      else
        AUXVAR=VAR2
      end IF ! ISML.LT.1
      TERM=((AQZ-APP)**2+QPERP2)/(EPABI+EQCDI)
      IF(TERM.GT.AUXVAR)then
        Hkl=Hkl+DP00P*DSQRT(PT7853/(TERM*(EPAB+EQCD)))
      else
        QQ=TERM*TWENTY
        N=INT(QQ)
        THETA=QQ-N
        THETA2=THETA*(THETA-ONE)
        THETA3=THETA2*(THETA-TWO)
        THETA4=THETA2*(THETA+ONE)
        F0=FMT_val(N+1)%FM0+THETA*FMT_del(N+1)%FM0-THETA3*FMT_int(N+1)%FM0+THETA4*FMT_int(N+2)%FM0
        Hkl=Hkl+DP00P*F0/DSQRT(EPAB+EQCD)
      end IF ! TERM.GT.AUXVAR
      end IF ! ISML.LT.2
      end do ! Jgauss
      end do ! Igauss
      Gijkl=Gijkl+Hkl*ExpCCkl
      end IF ! ExpCCkl
      end IF ! EXPARG.GT.I2E_expcut
      end do ! Lgauss
      end do ! Kgauss
      INTS_out(1)=Gijkl
!
! End of routine SP0000
      return
      end subroutine SP0000
      subroutine I2E_SHL_SSSS (NUMINT)
!***********************************************************************
!     Date last modified: May 24, 2002                     Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version.                                       *
!     Two-electron integral output routine.                            *
!     Note that each block contains integrals of only one type.        *
!***********************************************************************
! Modules:
      USE program_files
      USE QM_objects

      implicit none
!
! Input scalars:
      integer :: NUMINT
!
! Local scalars:
      integer nint,I,J,K,KL,L,NCNT1
!
! Begin:
!      call PRG_manager ('enter', 'I2E_SHL_SSSS', 'UTILITY')
!
      NCNT1=1
! Integral over atomic orbitals (IJ/KL).
      Iao=AOI+1
      Jao=AOJ+1
      Kao=AOK+1
      Lao=AOL+1
!
! The following code determines the integral type
      IF(Iao.NE.Jao.and.Iao.NE.Kao.and.Jao.NE.Kao.and.Kao.NE.Lao.and.Jao.NE.Lao)then
        NTINT(8)=NTINT(8)+NUMINT
        IJKL_count=IJKL_count+1
        IF(IJKL_count.gt.IJKL_MAX)then
          IJKL_Nbuffers=IJKL_Nbuffers+1
          if(integral_combinations)call I2E_COMB_ijkl (IJKL_INCORE, IJKL_MAX)
          call WRI_ijkl (IJKL_INCORE, IJKL_MAX, IJKL_unit, IJKL_MAX)
          Lijkl_ONDISK=.TRUE.
          ANY_ONDISK=.TRUE.
          IJKL_count=1
        end IF
        IJKL_INCORE(IJKL_count)%I=Iao
        IJKL_INCORE(IJKL_count)%J=Jao
        IJKL_INCORE(IJKL_count)%K=Kao
        IJKL_INCORE(IJKL_count)%L=Lao
        IJKL_INCORE(IJKL_count)%IJKL=Gout_ijkl(ncnt1)
        IJKL_INCORE(IJKL_count)%IKJL=Gout_ikjl(ncnt1)
        IJKL_INCORE(IJKL_count)%ILJK=Gout_iljk(ncnt1)
!
      else if(Iao.EQ.Jao.and.Iao.NE.Kao.and.Kao.NE.Lao)then
! (II,KL).
      NTINT(1)=NTINT(1)+NUMINT
        IIKL_count=IIKL_count+1
        IF(IIKL_count.gt.IIKL_MAX)then
          IIKL_Nbuffers=IIKL_Nbuffers+1
          if(integral_combinations)call I2E_COMB_iikl (IIKL_INCORE, IIKL_MAX)
          call WRI_iikl (IIKL_INCORE, IIKL_MAX, IIKL_unit, IIKL_MAX)
          Liikl_ONDISK=.TRUE.
          ANY_ONDISK=.TRUE.
          IIKL_count=1
        end IF
        IIKL_INCORE(IIKL_count)%I=Iao
        IIKL_INCORE(IIKL_count)%K=Kao
        IIKL_INCORE(IIKL_count)%L=Lao
        IIKL_INCORE(IIKL_count)%iikl=Gout_ijkl(ncnt1)
        IIKL_INCORE(IIKL_count)%ikil=Gout_ikjl(ncnt1)
!
      else if(Iao.NE.Jao.and.Jao.GT.Kao.and.Kao.EQ.Lao)then
! (IJ,KK) converted to (IK,JK).
      NTINT(2)=NTINT(2)+NUMINT
        IJKJ_count=IJKJ_count+1
        IF(IJKJ_count.gt.IJKJ_MAX)then
          IJKJ_Nbuffers=IJKJ_Nbuffers+1
          if(integral_combinations)call I2E_COMB_ijkj (IJKJ_INCORE, IJKJ_MAX)
          call WRI_ijkj (IJKJ_INCORE,  IJKJ_MAX, IJKJ_unit, IJKJ_MAX)
          Lijkj_ONDISK=.TRUE.
          ANY_ONDISK=.TRUE.
          IJKJ_count=1
        end IF
        IJKJ_INCORE(IJKJ_count)%I=Iao
        IJKJ_INCORE(IJKJ_count)%J=Kao
        IJKJ_INCORE(IJKJ_count)%K=Jao
        IJKJ_INCORE(IJKJ_count)%ijkj=Gout_ikjl(ncnt1)
        IJKJ_INCORE(IJKJ_count)%ikjj=Gout_ijkl(ncnt1)
!
      else if(Iao.NE.Jao.and.Jao.LT.Kao.and.Kao.EQ.Lao)then
! (IJ,KK) converted to (IK,KJ) = (IJ,JL)
      NTINT(3)=NTINT(3)+NUMINT
        IJJL_count=IJJL_count+1
        IF(IJJL_count.gt.IJJL_MAX)then
          IJJL_Nbuffers=IJJL_Nbuffers+1
          if(integral_combinations)call I2E_COMB_ijjl (IJJL_INCORE, IJJL_MAX)
          call WRI_ijjl (IJJL_INCORE,  IJJL_MAX, IJJL_unit, IJJL_MAX)
          Lijjl_ONDISK=.TRUE.
          ANY_ONDISK=.TRUE.
          IJJL_count=1
        end IF
        IJJL_INCORE(IJJL_count)%I=Iao
        IJJL_INCORE(IJJL_count)%J=Kao
        IJJL_INCORE(IJJL_count)%L=Jao
        IJJL_INCORE(IJJL_count)%ijjl=Gout_ikjl(ncnt1)
        IJJL_INCORE(IJJL_count)%iljj=Gout_ijkl(ncnt1)
!
      else if(Iao.NE.Jao.and.Iao.NE.Kao.and.Jao.NE.Kao.and.Kao.NE.Lao)then
! (IJ,KJ).
      NTINT(2)=NTINT(2)+NUMINT
        IJKJ_count=IJKJ_count+1
        IF(IJKJ_count.gt.IJKJ_MAX)then
          IJKJ_Nbuffers=IJKJ_Nbuffers+1
          if(integral_combinations)call I2E_COMB_ijkj (IJKJ_INCORE, IJKJ_MAX)
          call WRI_ijkj (IJKJ_INCORE,  IJKJ_MAX, IJKJ_unit, IJKJ_MAX)
          Lijkj_ONDISK=.TRUE.
          ANY_ONDISK=.TRUE.
          IJKJ_count=1
        end IF
        IJKJ_INCORE(IJKJ_count)%I=Iao
        IJKJ_INCORE(IJKJ_count)%J=Jao
        IJKJ_INCORE(IJKJ_count)%K=Kao
        IJKJ_INCORE(IJKJ_count)%ijkj=Gout_ijkl(ncnt1)
        IJKJ_INCORE(IJKJ_count)%ikjj=Gout_ikjl(ncnt1)
!
      else if(Iao.NE.Jao.and.Iao.EQ.Kao.and.Jao.NE.Lao)then
! (IJ,IL) converted to (II,JK) = (II,KL).
      NTINT(1)=NTINT(1)+NUMINT
        IIKL_count=IIKL_count+1
        IF(IIKL_count.gt.IIKL_MAX)then
          IIKL_Nbuffers=IIKL_Nbuffers+1
          if(integral_combinations)call I2E_COMB_iikl (IIKL_INCORE, IIKL_MAX)
          call WRI_iikl (IIKL_INCORE, IIKL_MAX, IIKL_unit, IIKL_MAX)
          Liikl_ONDISK=.TRUE.
          ANY_ONDISK=.TRUE.
          IIKL_count=1
        end IF
        IIKL_INCORE(IIKL_count)%I=Iao
        IIKL_INCORE(IIKL_count)%K=Jao
        IIKL_INCORE(IIKL_count)%L=Lao
        IIKL_INCORE(IIKL_count)%iikl=Gout_ikjl(ncnt1)
        IIKL_INCORE(IIKL_count)%ikil=Gout_ijkl(ncnt1)
!
      else if(Iao.NE.Jao.and.Jao.EQ.Kao.and.Kao.NE.Lao)then
! (IJ,JL).
      NTINT(3)=NTINT(3)+NUMINT
        IJJL_count=IJJL_count+1
        IF(IJJL_count.gt.IJJL_MAX)then
          IJJL_Nbuffers=IJJL_Nbuffers+1
          if(integral_combinations)call I2E_COMB_ijjl (IJJL_INCORE, IJJL_MAX)
          call WRI_ijjl (IJJL_INCORE, IJJL_MAX, IJJL_unit, IJJL_MAX)
          Lijjl_ONDISK=.TRUE.
          ANY_ONDISK=.TRUE.
          IJJL_count=1
        end IF
        IJJL_INCORE(IJJL_count)%I=Iao
        IJJL_INCORE(IJJL_count)%J=Jao
        IJJL_INCORE(IJJL_count)%L=Lao
        IJJL_INCORE(IJJL_count)%ijjl=Gout_ijkl(ncnt1)
        IJJL_INCORE(IJJL_count)%iljj=Gout_ikjl(ncnt1)
!
      else if(Iao.EQ.Jao.and.Iao.NE.Kao.and.Kao.EQ.Lao)then
! (II,KK).
      NTINT(4)=NTINT(4)+NUMINT
      IIKK_count=IIKK_count+1
      IF(IIKK_count.GT.IIKK_MAX)then
        write(UNIout,*)' error_I2E_SHL_SSSS> too many IIKK integrals'
        stop ' error_I2E_SHL_SSSS> too many IIKK integrals'
      end IF
      IIKK_INCORE(IIKK_count)%I=Iao
      IIKK_INCORE(IIKK_count)%K=Kao
      IIKK_INCORE(IIKK_count)%iikk=Gout_ijkl(ncnt1)
      IIKK_INCORE(IIKK_count)%ikik=Gout_ikjl(ncnt1)
!
      else if(Iao.NE.Jao.and.Iao.EQ.Kao.and.Jao.EQ.Lao)then
!
! (IJ,IJ) converted to (II,KK).
      NTINT(4)=NTINT(4)+NUMINT
      IIKK_count=IIKK_count+1
      IF(IIKK_count.GT.IIKK_MAX)then
        write(UNIout,*)' error_I2E_SHL_SSSS> too many IIKK integrals'
        stop ' error_I2E_SHL_SSSS> too many IIKK integrals'
      end IF
      IIKK_INCORE(IIKK_count)%I=Iao
      IIKK_INCORE(IIKK_count)%K=Jao
      IIKK_INCORE(IIKK_count)%iikk=Gout_ikjl(ncnt1)
      IIKK_INCORE(IIKK_count)%ikik=Gout_ijkl(ncnt1)
!
      else if(Iao.NE.Jao.and.Jao.EQ.Kao.and.Kao.EQ.Lao)then
!
! (IJ,JJ).
      NTINT(5)=NTINT(5)+NUMINT
      IJJJ_count=IJJJ_count+1
      IF(IJJJ_count.GT.IJJJ_MAX)then
        write(UNIout,*)' error_I2E_SHL_SSSS> too many IJJJ integrals'
        stop ' error_I2E_SHL_SSSS> too many IJJJ integrals'
      end IF
      IJJJ_INCORE(IJJJ_count)%I=Iao
      IJJJ_INCORE(IJJJ_count)%J=Jao
      IJJJ_INCORE(IJJJ_count)%ijjj=Gout_ijkl(ncnt1)
!
      else if(Iao.EQ.Jao.and.Iao.EQ.Kao.and.Iao.NE.Lao)then
!
! (II,IL).
      NTINT(6)=NTINT(6)+NUMINT
      IIIL_count=IIIL_count+1
      IF(IIIL_count.GT.IIIL_MAX)then
        write(UNIout,*)' error_I2E_SHL_SSSS> too many IIIL integrals'
        stop
      end IF
      IIIL_INCORE(IIIL_count)%I=Iao
      IIIL_INCORE(IIIL_count)%L=Lao
      IIIL_INCORE(IIIL_count)%iiil=Gout_ijkl(ncnt1)
!
      else if(Iao.EQ.Jao.and.Iao.EQ.Kao.and.Iao.EQ.Lao)then
!
! (II,II).
      NTINT(7)=NTINT(7)+NUMINT
      IIII_count=IIII_count+1
      IF(IIII_count.GT.IIII_MAX)then
        write(UNIout,*)' error_I2E_SHL_SSSS> too many IIII integrals'
        stop ' error_I2E_SHL_SSSS> too many IIII integrals'
      end IF
      IIII_INCORE(IIII_count)%I=Iao
      IIII_INCORE(IIII_count)%iiii=Gout_ijkl(ncnt1)
!
      else
        stop 'I2E_SHL_SSSS: illegal integral type found'
      end IF
!
! End of routine I2E_SHL_SSSS
!      call PRG_manager ('exit', 'I2E_SHL_SSSS', 'UTILITY')
      return
      end subroutine I2E_SHL_SSSS
      end subroutine ISP_SSSS
      subroutine ISP_SSSP
!***********************************************************************
!     Date last modified: May 24, 2002                     Version 1.0 *
!     Author: R. A. Poirier                                            *
!     Description: Special fast routine for SSSS integrals.            *
!***********************************************************************
! Modules:

      implicit none
!
! Local scalars:
      integer I,IGOUT,Iset,NUMINT
!
! Begin:
      Irange=1
      Jrange=1
      Krange=1
      Lrange=3
!
      IGOUT=3
      include 'mungauss_do_IJKL'
      NUMINT=0
!
      GOUT_ijkl(1:3)=ZERO
      GOUT_ikjl(1:3)=ZERO
      GOUT_iljk(1:3)=ZERO
! Set 1
      Iashl=Iatmshl
      Jashl=Jatmshl
      Kashl=Katmshl
      Lashl=Latmshl
      call SET_shells (Ishell, Jshell, Kshell, Lshell)
      call SP0001 (Gout_ijkl)
!
! Set 2
      if(Mtype.eq.3)then
        Iashl=Jatmshl
        Jashl=Katmshl
        Kashl=Iatmshl
        Lashl=Latmshl
        call SET_shells (Jshell, Kshell, Ishell, Lshell)
        call SP0001 (Gout_ikjl)
      else if(Mtype.lt.6)then
        Iashl=Iatmshl
        Jashl=Katmshl
        Kashl=Jatmshl
        Lashl=Latmshl
        call SET_shells (Ishell, Kshell, Jshell, Lshell)
        call SP0001 (Gout_ikjl)
      end if ! Mtype
!
! Set 3
      if(Mtype.eq.1)then
        Iashl=Jatmshl
        Jashl=Katmshl
        Kashl=Iatmshl
        Lashl=Latmshl
        call SET_shells (Jshell, Kshell, Ishell, Lshell)
        call SP0001 (Gout_iljk)
      end IF
!
      call I2E_SHL_SSSP (NUMINT)
      include 'mungauss_endo_IJKL'
!
! End of routine ISP_SSSP
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine SP0001 (INTS_out)
!***********************************************************************
!     Date last modified: May 24, 2002                                 *
!     Author:                                                          *
!     Description: Special fast routine for sssp type integrals.       *
!***********************************************************************
! Modules:

      implicit none
!
! Output array:
      double precision INTS_out(3)
!
! Local scalars:
      integer I,ISML,ISMLP,ISMLQ,N
      double precision AQX,AQZ,AUXVAR,COSP,CQ,DQ,EQCDI,CS,EQCD,DS,G0,APP,BPP,DP00P,PQAB,QPERP,QPERP2,QQ, &
                       THETA,THETA2,THETA3,THETA4,G0Y1,TERM,T1,T2,T3,Yterm,Y0,Y1
      double precision EPABI,AS,EPAB,BS,EPABB,Expij,Expkl,EXPARG,ExpCCij,ExpCCkl
!
! Local arrays:
      double precision Gijkl(3),Hkl(3)
!
! Begin:
! Begin Q loop.
      do Kgauss=KGBGN,KGend
      CS=Basis%gaussian(Kgauss)%exp
      do Lgauss=LGBGN,LGend
      DS=Basis%gaussian(Lgauss)%exp
      EQCD=CS+DS
      EQCDI=ONE/EQCD
      CQ=DS*EQCDI*RCD
      DQ=CQ-RCD
      EXPARG=CQ*DQ*EQCD
!     IF(EXPARG.GE.-I2E_expcut)then
      if(EXPARG.le.I2E_expcut)then
      Expkl=DEXP(EXPARG)*EQCDI
      ExpCCkl=Expkl*Basis%gaussian(Kgauss)%CONTRC*Basis%gaussian(Lgauss)%CONTRC
      IF(dabs(ExpCCkl).GT.ERROR1)then
        ISMLQ=0
      else
        ISMLQ=1
      end IF
!
      IF(dabs(ExpCCkl).GT.ERROR2)then
! Find coordinates of Q relative to axes at A.
! QPERP is perpendicular from Q to AB.
      AQX=ACX+SING*CQ
      AQZ=ACZ+COSG*CQ
      QPERP2=AQX*AQX+ACY2
      QPERP=DSQRT(QPERP2)
! PHI is 180 - Azimuthal angle for AQ in AB local axis system.
      IF(QPERP.GT.TENM12)then
        COSP=-AQX/QPERP
        SINP=-ACY/QPERP
      else
        COSP=ONE
        SINP=ZERO
      end IF
      Hkl(1)=ZERO
      Hkl(2)=ZERO
      Hkl(3)=ZERO

      do Igauss=IGBGN,IGend
      AS=Basis%gaussian(Igauss)%exp
      do Jgauss=JGBGN,JGend
      BS=Basis%gaussian(Jgauss)%exp
      EPAB=AS+BS
      EPABI=ONE/EPAB
      EPABB=BS*EPABI
      APP=EPABB*RAB
      BPP=APP-RAB
      EXPARG=AS*EPABB*RABSQ
      IF(EXPARG.GT.I2E_expcut)then
        ISMLP=2
        DP00P=ZERO
      else
        Expij=DEXP(-EXPARG)*EPABI
        ExpCCij=Expij*Basis%gaussian(Igauss)%CONTRC*Basis%gaussian(Jgauss)%CONTRC
        IF(dabs(ExpCCij).GT.ERROR1)then
          ISMLP=0
        else
          IF(dabs(ExpCCij).GT.ERROR2)then
            ISMLP=1
          else
            ISMLP=2
          end IF
        end IF
        DP00P=THRTY4*ExpCCij
      end IF

      ISML=ISMLQ+ISMLP
      IF(ISML.LT.2)then
      IF(ISML.LT.1)then
        AUXVAR=VAR1
      else
        AUXVAR=VAR2
      end IF

      PQAB=AQZ-APP
      G0=ONE/(EPABI+EQCDI)
      TERM=(PQAB*PQAB+QPERP2)*G0

      IF(TERM.GT.AUXVAR)then
        Y0=DP00P*DSQRT(PT7853/(TERM*(EPAB+EQCD)))
        Y1=PT5*Y0/TERM
      else
        Yterm=DP00P/DSQRT(EPAB+EQCD)
        QQ=TERM*TWENTY
        N=INT(QQ)
        THETA=QQ-N
        THETA2=THETA*(THETA-ONE)
        THETA3=THETA2*(THETA-TWO)
        THETA4=THETA2*(THETA+ONE)
        Y0=(FMT_val(N+1)%FM0+THETA*FMT_del(N+1)%FM0-THETA3*FMT_int(N+1)%FM0+THETA4*FMT_int(N+2)%FM0)*Yterm
        Y1=(FMT_val(N+1)%FM1+THETA*FMT_del(N+1)%FM1-THETA3*FMT_int(N+1)%FM1+THETA4*FMT_int(N+2)%FM1)*Yterm
      end IF

      G0Y1=G0*Y1
      Hkl(1)=Hkl(1)+Y0
      Hkl(2)=Hkl(2)+G0Y1
      Hkl(3)=Hkl(3)-G0Y1*PQAB
      end IF ! ISML.LT.2
      end do ! Jgauss
      end do ! Igauss
!
      Hkl(2)=Hkl(2)*EQCDI*QPERP
      Hkl(3)=Hkl(3)*EQCDI
      TERM=DQ*Hkl(1)
      Gijkl(1)=Hkl(2)*COSP+TERM*SING
      Gijkl(2)=Hkl(2)*SINP
      Gijkl(3)=Hkl(3)+TERM*COSG

      INTS_out(1)=INTS_out(1)+Gijkl(1)*ExpCCkl
      INTS_out(2)=INTS_out(2)+Gijkl(2)*ExpCCkl
      INTS_out(3)=INTS_out(3)+Gijkl(3)*ExpCCkl
      end IF ! ExpCCkl
      end IF ! EXPARG
      end do ! Lgauss
      end do ! Kgauss
!
      T1=INTS_out(1)
      T2=INTS_out(2)
      T3=INTS_out(3)
      INTS_out(1)=P11*T1+P21*T2+P31*T3
      INTS_out(2)=P12*T1+P22*T2+P32*T3
      INTS_out(3)=P13*T1+P23*T2+P33*T3
!
! End of routine SP0001
      return
      end subroutine SP0001
      subroutine I2E_SHL_SSSP (NUMINT)
!***********************************************************************
!     Date last modified: May 24, 2002                     Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version.                                       *
!     Two-electron integral output routine.                            *
!     Note that each block contains integrals of only one type.        *
!***********************************************************************
! Modules:
      USE program_files
      USE QM_objects

      implicit none
!
! Local scalars:
      integer nint,I,J,K,KL,L,NCNT1,NN,NUMINT
!
! Begin:
!      call PRG_manager ('enter', 'I2E_SHL_SSSP', 'UTILITY')
!
      Iao=AOI+1
      Jao=AOJ+1
      Kao=AOK+1
      NCNT1=0
! Loop over set of integrals defined by ashls Iashl, Jashl, etc.
      do L=1,Lrange
      NCNT1=NCNT1+1
      NUMINT=0
      IF(DABS(GOUT_ijkl(NCNT1)).GT.I2E_ACC)then
        NUMINT=NUMINT+1
      else
        GOUT_ijkl(NCNT1)=ZERO
      end IF
      IF(DABS(GOUT_ikjl(NCNT1)).GT.I2E_ACC)then
        NUMINT=NUMINT+1
      else
        GOUT_ikjl(NCNT1)=ZERO
      end IF
      IF(DABS(GOUT_iljk(NCNT1)).GT.I2E_ACC)then
        NUMINT=NUMINT+1
      else
        GOUT_iljk(NCNT1)=ZERO
      end IF
      IF(NUMINT.NE.0)then
! Integral over atomic orbitals (IJ/KL).
      Lao=AOL+L
!
! The following code determines the integral type
      IF(Iao.NE.Jao.and.Iao.NE.Kao.and.Jao.NE.Kao.and.Kao.NE.Lao.and.Jao.NE.Lao)then
!
      NTINT(8)=NTINT(8)+NUMINT
        IJKL_count=IJKL_count+1
        IF(IJKL_count.gt.IJKL_MAX)then
          IJKL_Nbuffers=IJKL_Nbuffers+1
          if(integral_combinations)call I2E_COMB_ijkl (IJKL_INCORE, IJKL_MAX)
          call WRI_ijkl (IJKL_INCORE, IJKL_MAX, IJKL_unit, IJKL_MAX)
          Lijkl_ONDISK=.TRUE.
          ANY_ONDISK=.TRUE.
          IJKL_count=1
        end IF
        IJKL_INCORE(IJKL_count)%I=Iao
        IJKL_INCORE(IJKL_count)%J=Jao
        IJKL_INCORE(IJKL_count)%K=Kao
        IJKL_INCORE(IJKL_count)%L=Lao
        IJKL_INCORE(IJKL_count)%IJKL=Gout_ijkl(ncnt1)
        IJKL_INCORE(IJKL_count)%IKJL=Gout_ikjl(ncnt1)
        IJKL_INCORE(IJKL_count)%ILJK=Gout_iljk(ncnt1)
!
      else if(Iao.EQ.Jao.and.Iao.NE.Kao.and.Kao.NE.Lao)then
! (II,KL).
      NTINT(1)=NTINT(1)+NUMINT
        IIKL_count=IIKL_count+1
        IF(IIKL_count.gt.IIKL_MAX)then
          IIKL_Nbuffers=IIKL_Nbuffers+1
          if(integral_combinations)call I2E_COMB_iikl (IIKL_INCORE, IIKL_MAX)
          call WRI_iikl (IIKL_INCORE, IIKL_MAX, IIKL_unit, IIKL_MAX)
          Liikl_ONDISK=.TRUE.
          ANY_ONDISK=.TRUE.
          IIKL_count=1
        end IF
        IIKL_INCORE(IIKL_count)%I=Iao
        IIKL_INCORE(IIKL_count)%K=Kao
        IIKL_INCORE(IIKL_count)%L=Lao
        IIKL_INCORE(IIKL_count)%iikl=Gout_ijkl(ncnt1)
        IIKL_INCORE(IIKL_count)%ikil=Gout_ikjl(ncnt1)
!
      else if(Iao.NE.Jao.and.Jao.GT.Kao.and.Kao.EQ.Lao)then
! (IJ,KK) converted to (IK,JK).
        IJKJ_count=IJKJ_count+1
        IF(IJKJ_count.gt.IJKJ_MAX)then
          IJKJ_Nbuffers=IJKJ_Nbuffers+1
          if(integral_combinations)call I2E_COMB_ijkj (IJKJ_INCORE, IJKJ_MAX)
          call WRI_ijkj (IJKJ_INCORE,  IJKJ_MAX, IJKJ_unit, IJKJ_MAX)
          Lijkj_ONDISK=.TRUE.
          ANY_ONDISK=.TRUE.
          IJKJ_count=1
        end IF
        IJKJ_INCORE(IJKJ_count)%I=Iao
        IJKJ_INCORE(IJKJ_count)%J=Kao
        IJKJ_INCORE(IJKJ_count)%K=Jao
        IJKJ_INCORE(IJKJ_count)%ijkj=Gout_ikjl(ncnt1)
        IJKJ_INCORE(IJKJ_count)%ikjj=Gout_ijkl(ncnt1)
!
      else if(Iao.NE.Jao.and.Jao.LT.Kao.and.Kao.EQ.Lao)then
! (IJ,KK) converted to (IK,KJ) = (IJ,JL)
      NTINT(3)=NTINT(3)+NUMINT
        IJJL_count=IJJL_count+1
        IF(IJJL_count.gt.IJJL_MAX)then
          IJJL_Nbuffers=IJJL_Nbuffers+1
          if(integral_combinations)call I2E_COMB_ijjl (IJJL_INCORE, IJJL_MAX)
          call WRI_ijjl (IJJL_INCORE,  IJJL_MAX, IJJL_unit, IJJL_MAX)
          Lijjl_ONDISK=.TRUE.
          ANY_ONDISK=.TRUE.
          IJJL_count=1
        end IF
        IJJL_INCORE(IJJL_count)%I=Iao
        IJJL_INCORE(IJJL_count)%J=Kao
        IJJL_INCORE(IJJL_count)%L=Jao
        IJJL_INCORE(IJJL_count)%ijjl=Gout_ikjl(ncnt1)
        IJJL_INCORE(IJJL_count)%iljj=Gout_ijkl(ncnt1)
!
      else if(Iao.NE.Jao.and.Iao.NE.Kao.and.Jao.NE.Kao.and.Kao.NE.Lao)then
! (IJ,KJ).
      NTINT(2)=NTINT(2)+NUMINT
        IJKJ_count=IJKJ_count+1
        IF(IJKJ_count.gt.IJKJ_MAX)then
          IJKJ_Nbuffers=IJKJ_Nbuffers+1
          if(integral_combinations)call I2E_COMB_ijkj (IJKJ_INCORE, IJKJ_MAX)
          call WRI_ijkj (IJKJ_INCORE,  IJKJ_MAX, IJKJ_unit, IJKJ_MAX)
          Lijkj_ONDISK=.TRUE.
          ANY_ONDISK=.TRUE.
          IJKJ_count=1
        end IF
        IJKJ_INCORE(IJKJ_count)%I=Iao
        IJKJ_INCORE(IJKJ_count)%J=Jao
        IJKJ_INCORE(IJKJ_count)%K=Kao
        IJKJ_INCORE(IJKJ_count)%ijkj=Gout_ijkl(ncnt1)
        IJKJ_INCORE(IJKJ_count)%ikjj=Gout_ikjl(ncnt1)
!
      else if(Iao.NE.Jao.and.Iao.EQ.Kao.and.Jao.NE.Lao)then
! (IJ,IL) converted to (II,JK) = (II,KL).
      NTINT(1)=NTINT(1)+NUMINT
        IIKL_count=IIKL_count+1
        IF(IIKL_count.gt.IIKL_MAX)then
          IIKL_Nbuffers=IIKL_Nbuffers+1
          if(integral_combinations)call I2E_COMB_iikl (IIKL_INCORE, IIKL_MAX)
          call WRI_iikl (IIKL_INCORE, IIKL_MAX, IIKL_unit, IIKL_MAX)
          Liikl_ONDISK=.TRUE.
          ANY_ONDISK=.TRUE.
          IIKL_count=1
        end IF
        IIKL_INCORE(IIKL_count)%I=Iao
        IIKL_INCORE(IIKL_count)%K=Jao
        IIKL_INCORE(IIKL_count)%L=Lao
        IIKL_INCORE(IIKL_count)%iikl=Gout_ikjl(ncnt1)
        IIKL_INCORE(IIKL_count)%ikil=Gout_ijkl(ncnt1)
!
      else if(Iao.NE.Jao.and.Jao.EQ.Kao.and.Kao.NE.Lao)then
! (IJ,JL).
      NTINT(3)=NTINT(3)+NUMINT
        IJJL_count=IJJL_count+1
        IF(IJJL_count.gt.IJJL_MAX)then
          IJJL_Nbuffers=IJJL_Nbuffers+1
          if(integral_combinations)call I2E_COMB_ijjl (IJJL_INCORE, IJJL_MAX)
          call WRI_ijjl (IJJL_INCORE, IJJL_MAX, IJJL_unit, IJJL_MAX)
          Lijjl_ONDISK=.TRUE.
          ANY_ONDISK=.TRUE.
          IJJL_count=1
        end IF
        IJJL_INCORE(IJJL_count)%I=Iao
        IJJL_INCORE(IJJL_count)%J=Jao
        IJJL_INCORE(IJJL_count)%L=Lao
        IJJL_INCORE(IJJL_count)%ijjl=Gout_ijkl(ncnt1)
        IJJL_INCORE(IJJL_count)%iljj=Gout_ikjl(ncnt1)
!
      else if(Iao.EQ.Jao.and.Iao.NE.Kao.and.Kao.EQ.Lao)then
! (II,KK).
      NTINT(4)=NTINT(4)+NUMINT
      IIKK_count=IIKK_count+1
      IF(IIKK_count.GT.IIKK_MAX)then
        write(UNIout,*)' error_I2E_SHL_SSSP> too many IIKK integrals'
        stop ' error_I2E_SHL_SSSP> too many IIKK integrals'
      end IF
      IIKK_INCORE(IIKK_count)%I=Iao
      IIKK_INCORE(IIKK_count)%K=Kao
      IIKK_INCORE(IIKK_count)%iikk=Gout_ijkl(ncnt1)
      IIKK_INCORE(IIKK_count)%ikik=Gout_ikjl(ncnt1)
!
      else if(Iao.NE.Jao.and.Iao.EQ.Kao.and.Jao.EQ.Lao)then
!
! (IJ,IJ) converted to (II,KK).
      NTINT(4)=NTINT(4)+NUMINT
      IIKK_count=IIKK_count+1
      IF(IIKK_count.GT.IIKK_MAX)then
        write(UNIout,*)' error_I2E_SHL_SSSP> too many IIKK integrals'
        stop ' error_I2E_SHL_SSSP> too many IIKK integrals'
      end IF
      IIKK_INCORE(IIKK_count)%I=Iao
      IIKK_INCORE(IIKK_count)%K=Jao
      IIKK_INCORE(IIKK_count)%iikk=Gout_ikjl(ncnt1)
      IIKK_INCORE(IIKK_count)%ikik=Gout_ijkl(ncnt1)
!
      else if(Iao.NE.Jao.and.Jao.EQ.Kao.and.Kao.EQ.Lao)then
!
! (IJ,JJ).
      NTINT(5)=NTINT(5)+NUMINT
      IJJJ_count=IJJJ_count+1
      IF(IJJJ_count.GT.IJJJ_MAX)then
        write(UNIout,*)' error_I2E_SHL_SSSP> too many IJJJ integrals'
        stop ' error_I2E_SHL_SSSP> too many IJJJ integrals'
      end IF
      IJJJ_INCORE(IJJJ_count)%I=Iao
      IJJJ_INCORE(IJJJ_count)%J=Jao
      IJJJ_INCORE(IJJJ_count)%ijjj=Gout_ijkl(ncnt1)
!
      else if(Iao.EQ.Jao.and.Iao.EQ.Kao.and.Iao.NE.Lao)then
!
! (II,IL).
      NTINT(6)=NTINT(6)+NUMINT
      IIIL_count=IIIL_count+1
      IF(IIIL_count.GT.IIIL_MAX)then
        write(UNIout,*)' error_I2E_SHL_SSSP> too many IIIL integrals'
        stop
      end IF
      IIIL_INCORE(IIIL_count)%I=Iao
      IIIL_INCORE(IIIL_count)%L=Lao
      IIIL_INCORE(IIIL_count)%iiil=Gout_ijkl(ncnt1)
!
      else if(Iao.EQ.Jao.and.Iao.EQ.Kao.and.Iao.EQ.Lao)then
!
! (II,II).
      NTINT(7)=NTINT(7)+NUMINT
      IIII_count=IIII_count+1
      IF(IIII_count.GT.IIII_MAX)then
        write(UNIout,*)' error_I2E_SHL_SSSP> too many IIII integrals'
        stop ' error_I2E_SHL_SSSP> too many IIII integrals'
      end IF
      IIII_INCORE(IIII_count)%I=Iao
      IIII_INCORE(IIII_count)%iiii=Gout_ijkl(ncnt1)
!
      else
        stop 'I2E_SHL_SSSP: illegal integral type found'
      end IF
      end IF ! NUMINT.NE.0
      end do ! L
!
! End of routine I2E_SHL_SSSP
      return
      end subroutine I2E_SHL_SSSP
      end subroutine ISP_SSSP
      subroutine ISP_SSPP
!***********************************************************************
!     Date last modified: May 24, 2002                     Version 1.0 *
!     Author: R. A. Poirier                                            *
!     Description: Special fast routine for SSSS integrals.            *
!***********************************************************************
! Modules:

      implicit none
!
! Local scalars:
      integer I,IGOUT,NUMINT
      logical DOZERO
!
! Local arrays:
      double precision, dimension(:), pointer :: INT_out => null()
!
! Begin:
      Irange=1
      Jrange=1
      Krange=3
      Lrange=3

      IGOUT=9
      include 'mungauss_do_IJKL'
      NUMINT=0
      GOUT_ijkl(1:9)=ZERO
      GOUT_ikjl(1:9)=ZERO
      GOUT_iljk(1:9)=ZERO
! Set 1
      Iashl=Iatmshl
      Jashl=Jatmshl
      Kashl=Katmshl
      Lashl=Latmshl
      call SET_shells (Ishell, Jshell, Kshell, Lshell)
      DOZERO=.false.
      IF(Kashl.EQ.Lashl)DOZERO=.TRUE.
      call SP0011 (Gout_ijkl, DOZERO)

! Set 2
      Iashl=Iatmshl
      Jashl=Katmshl
      Kashl=Jatmshl
      Lashl=Latmshl
      call SET_shells (Ishell, Kshell, Jshell, Lshell)
      DOZERO=.false.
      IF(Kashl.EQ.Iashl.and.Lashl.EQ.Jashl)DOZERO=.TRUE.       ! use Mtype instead
!      IF(Mtype.eq.4)DOZERO=.TRUE.
      call SP0101 (Gout_ikjl, DOZERO)
      if(Mtype.eq.2)then
        Gout_iljk(4)=Gout_ikjl(2)
        Gout_ikjl(2)=ZERO
        Gout_iljk(7)=Gout_ikjl(3)
        Gout_ikjl(3)=ZERO
        Gout_iljk(8)=Gout_ikjl(6)
        Gout_ikjl(6)=ZERO
      end if ! Mtype.eq.2
!
! Set 3
      if(Mtype.eq.1)then
      Iashl=Jatmshl
      Jashl=Katmshl
      Kashl=Iatmshl
      Lashl=Latmshl
      call SET_shells (Jshell, Kshell, Ishell, Lshell)
      DOZERO=.false.
      call SP0101 (Gout_iljk, DOZERO)
      end if
!
      call I2E_SHL_SSPP (NUMINT)
      include 'mungauss_endo_IJKL'
!
! End of routine ISP_SSPP
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine SP0011 (INTS_out, &
                         DOZERO)
!***********************************************************************
!     Date last modified: June 28, 2002                                *
!     Author:                                                          *
!     Description: Special fast routine for sspp type integrals.       *
!***********************************************************************
! Modules:

      implicit none
!
! Input logical:
      logical DOZERO
!
! Output array:
      double precision INTS_out(9)
!
! Local scalars:
      integer I,ISML,ISMLP,ISMLQ,N
      double precision AQX,AQZ,AUXVAR,COSP,COSP2,COSSIN,CQ,DQ,EQCDI,F0,APP,BPP,DP00P,F1,F2,CS,EQCD,DS,GGY,GTX,GY,G0,MCP2P1, &
                       PQAB,PQAB2,QPERP2,QPERP,QQ,THETA,THETA2,THETA3,THETA4,TERM,Yterm
      double precision EPABI,AS,EPAB,BS,EPABB,Expij,Expkl,EXPARG,ExpCCij,ExpCCkl
!
! Local arrays:
      double precision Gijkl(16),Hkl(16)
!
! Begin:
! Begin Q loop.
      do Kgauss=KGBGN,KGend
      CS=Basis%gaussian(Kgauss)%exp
      do Lgauss=LGBGN,LGend
      DS=Basis%gaussian(Lgauss)%exp
      EQCD=CS+DS
      EQCDI=ONE/EQCD
      CQ=DS*EQCDI*RCD
      DQ=CQ-RCD
      EXPARG=CQ*DQ*EQCD
!     IF(EXPARG.GE.-I2E_expcut)then
      if(EXPARG.le.I2E_expcut)then
      Expkl=DEXP(EXPARG)*EQCDI
      ExpCCkl=Expkl*Basis%gaussian(Kgauss)%CONTRC*Basis%gaussian(Lgauss)%CONTRC
      IF(dabs(ExpCCkl).GT.ERROR1)then
        ISMLQ=0
      else
         ISMLQ=1
      end IF
      IF(dabs(ExpCCkl).GT.ERROR2)then
! Find coordinates of Q relative to axes at A.
! QPERP is perpendicular from Q to AB.
      AQX=ACX+SING*CQ
      AQZ=ACZ+COSG*CQ
      QPERP2=AQX*AQX+ACY2
      QPERP=DSQRT(QPERP2)
! PHI is 180 - Azimuthal angle for AQ in AB local axis system.
      IF(QPERP.GT.TENM12)then
        COSP=-AQX/QPERP
        SINP=-ACY/QPERP
      else
        COSP=ONE
        SINP=ZERO
      end IF
      Hkl(1)=ZERO
      Hkl(2)=ZERO
      Hkl(4)=ZERO
      Hkl(6)=ZERO
      Hkl(8)=ZERO
      Hkl(16)=ZERO
!
      do Igauss=IGBGN,IGend
      AS=Basis%gaussian(Igauss)%exp
      do Jgauss=JGBGN,JGend
      BS=Basis%gaussian(Jgauss)%exp
      EPAB=AS+BS
      EPABI=ONE/EPAB
      EPABB=BS*EPABI
      APP=EPABB*RAB
      BPP=APP-RAB
      EXPARG=AS*EPABB*RABSQ
      IF(EXPARG.GT.I2E_expcut)then
        ISMLP=2
        DP00P=ZERO
      else
        Expij=DEXP(-EXPARG)*EPABI
        ExpCCij=Expij*Basis%gaussian(Igauss)%CONTRC*Basis%gaussian(Jgauss)%CONTRC
        IF(dabs(ExpCCij).GT.ERROR1)then
          ISMLP=0
        else
          IF(dabs(ExpCCij).GT.ERROR2)then
            ISMLP=1
          else
            ISMLP=2
          end IF
        end IF
        DP00P=THRTY4*ExpCCij
      end IF

      ISML=ISMLQ+ISMLP
      IF(ISML.LT.2)then
      IF(ISML.LT.1)then
        AUXVAR=VAR1
      else
        AUXVAR=VAR2
      end IF

      PQAB=AQZ-APP
      PQAB2=PQAB*PQAB
      G0=ONE/(EPABI+EQCDI)
      TERM=G0*(PQAB2+QPERP2)
      G0=G0*EQCDI

      IF(TERM.GT.AUXVAR)then
        F0=DP00P*DSQRT(PT7853/(TERM*(EPAB+EQCD)))
        GTX=G0/TERM
        F1=PT5*F0*GTX
        F2=ONEPT5*F1*GTX
      else
        Yterm=DP00P/DSQRT(EPAB+EQCD)
        GY=G0*Yterm
        GGY=G0*GY
        QQ=TERM*TWENTY
        N=INT(QQ)
        THETA=QQ-N
        THETA2=THETA*(THETA-ONE)
        THETA3=THETA2*(THETA-TWO)
        THETA4=THETA2*(THETA+ONE)
        F0=(FMT_val(N+1)%FM0+THETA*FMT_del(N+1)%FM0-THETA3*FMT_int(N+1)%FM0+THETA4*FMT_int(N+2)%FM0)*Yterm
        F1=(FMT_val(N+1)%FM1+THETA*FMT_del(N+1)%FM1-THETA3*FMT_int(N+1)%FM1+THETA4*FMT_int(N+2)%FM1)*GY
        F2=(FMT_val(N+1)%FM2+THETA*FMT_del(N+1)%FM2-THETA3*FMT_int(N+1)%FM2+THETA4*FMT_int(N+2)%FM2)*GGY
      end IF

      Hkl(1)=Hkl(1)+F0
      Hkl(2)=Hkl(2)+F1
      Hkl(4)=Hkl(4)-F1*PQAB
      Hkl(6)=Hkl(6)+F2
      Hkl(8)=Hkl(8)-F2*PQAB
      Hkl(16)=Hkl(16)+F2*PQAB2
      end IF ! ISML.LT.2
      end do ! Jgauss
      end do ! Igauss
!
      Hkl(11)=PT5*EQCDI*(Hkl(1)-Hkl(2))
      Hkl(2)=Hkl(2)*QPERP
      Hkl(6)=Hkl(6)*QPERP2+Hkl(11)
      Hkl(8)=Hkl(8)*QPERP
      Hkl(16)=Hkl(16)+Hkl(11)
!
      Gijkl(1)=Hkl(1)
      Gijkl(4)=Hkl(4)
      Gijkl(16)=Hkl(16)
! No rotation if ABCD coplanar.
      IF(SINP.EQ.ZERO.and.COSP.GT.ZERO)then
        Gijkl(2)=Hkl(2)
        Gijkl(3)=ZERO
        Gijkl(6)=Hkl(6)
        Gijkl(7)=ZERO
        Gijkl(8)=Hkl(8)
        Gijkl(11)=Hkl(11)
        Gijkl(12)=ZERO
      else if(SINP.EQ.ZERO.and.COSP.LT.ZERO)then
        Gijkl(2)=-Hkl(2)
        Gijkl(3)=ZERO
        Gijkl(6)=Hkl(6)
        Gijkl(7)=ZERO
        Gijkl(8)=-Hkl(8)
        Gijkl(11)=Hkl(11)
        Gijkl(12)=ZERO
      else
        COSP2=COSP*COSP
        MCP2P1=ONE-COSP2
        COSSIN=COSP*SINP
        Gijkl(6)=COSP2*Hkl(6)+MCP2P1*Hkl(11)
        Gijkl(7)=COSSIN*Hkl(6)-COSSIN*Hkl(11)
        Gijkl(11)=MCP2P1*Hkl(6)+COSP2*Hkl(11)
        Gijkl(8)=COSP*Hkl(8)
        Gijkl(12)=SINP*Hkl(8)
        Gijkl(2)=COSP*Hkl(2)
        Gijkl(3)=SINP*Hkl(2)
        Hkl(6)=Gijkl(6)
!        Hkl(7)=Gijkl(7)
        Hkl(8)=Gijkl(8)
        Hkl(11)=Gijkl(11)
!        Hkl(12)=Gijkl(12)
        Hkl(2)=Gijkl(2)
!        Hkl(3)=Gijkl(3)
      end IF
!
! Translates up to 160 integrals on A, B and Q to up to 256 integrals on A, B, C and D.
      Gijkl(5)=Gijkl(2)
      Gijkl(10)=Gijkl(7)
      Gijkl(13)=Gijkl(4)
      Gijkl(14)=Gijkl(8)
      Gijkl(15)=Gijkl(12)
      IF(RCDSQ.GT.ZERO)then
        R13=CQ*SING
        R33=CQ*COSG
        R14=DQ*SING
        R34=DQ*COSG
        Gijkl(5)=Gijkl(5)+R13*Gijkl(1)
        Gijkl(6)=Gijkl(6)+R13*Gijkl(2)+R14*Gijkl(5)
        Gijkl(7)=Gijkl(7)+R13*Gijkl(3)
        Gijkl(8)=Gijkl(8)+R13*Gijkl(4)+R34*Gijkl(5)
        Gijkl(10)=Gijkl(10)+R14*Gijkl(3) ! 9->3
        Gijkl(12)=Gijkl(12)+R34*Gijkl(3) ! 9->3
        Gijkl(13)=Gijkl(13)+R33*Gijkl(1)
        Gijkl(14)=Gijkl(14)+R33*Gijkl(2)+R14*Gijkl(13)
        Gijkl(15)=Gijkl(15)+R33*Gijkl(3)
        Gijkl(16)=Gijkl(16)+R33*Gijkl(4)+R34*Gijkl(13)
      end IF ! RCDSQ.GT.ZERO
!
! SSPP only (9 ints)
      INTS_out(1)=INTS_out(1)+Gijkl(6)*ExpCCkl
      INTS_out(2)=INTS_out(2)+Gijkl(7)*ExpCCkl
      INTS_out(3)=INTS_out(3)+Gijkl(8)*ExpCCkl
      INTS_out(4)=INTS_out(4)+Gijkl(10)*ExpCCkl
      INTS_out(5)=INTS_out(5)+Gijkl(11)*ExpCCkl
      INTS_out(6)=INTS_out(6)+Gijkl(12)*ExpCCkl
      INTS_out(7)=INTS_out(7)+Gijkl(14)*ExpCCkl
      INTS_out(8)=INTS_out(8)+Gijkl(15)*ExpCCkl
      INTS_out(9)=INTS_out(9)+Gijkl(16)*ExpCCkl
      end IF ! ExpCCkl
      end IF ! EXPARG
      end do ! L
      end do ! K

      call R30011 (INTS_out)
      IF(DOZERO)then
        INTS_out(2)=ZERO
        INTS_out(3)=ZERO
        INTS_out(6)=ZERO
      end IF
!
! End of routine SP0011
      return
      end subroutine SP0011
      subroutine SP0101 (INTS_out, &
                         DOZERO)
!***********************************************************************
!     Date last modified: May 24, 2002                                 *
!     Author:                                                          *
!     Description: Special fast routine for P loop for 0101.           *
!                  MUN Version.                                        *
!***********************************************************************
! Modules:

      implicit none
!
! Input logical:
      logical DOZERO
!
! Output array:
      double precision INTS_out(9)
!
! Local scalars:
      integer I,ISML,ISMLP,ISMLQ,N
      double precision EPABI,AS,EPAB,BS,EPABB,APP,BPP,CONP,DP01P
      double precision AQX,AQZ,AUXVAR,COSP,CQ,DQ,EQCDI,F0,F1,F2,CS,EQCD,DS,GGY,GTX,GY,G0,G03,PQAB, &
                       PQAB2,QPERP,QPERP2,QQ,THETA,THETA2,THETA3,THETA4,TERM,Yterm
      double precision EXPARG,Expij,Expkl,ExpCCij,ExpCCkl
!
! Local arrays:
      double precision Gijkl(16),Hkl(16)
!
! Begin:
      Gijkl(1:16)=ZERO
      Hkl(1:16)=ZERO
!
! Begin K,L (Q) loop.
      do Kgauss=KGBGN,KGend
      CS=Basis%gaussian(Kgauss)%exp
      do Lgauss=LGBGN,LGend
      DS=Basis%gaussian(Lgauss)%exp
      EQCD=CS+DS
      EQCDI=ONE/EQCD
      CQ=DS*EQCDI*RCD
      DQ=CQ-RCD
      EXPARG=CQ*DQ*EQCD
!     IF(EXPARG.GE.-I2E_expcut)then
      if(EXPARG.le.I2E_expcut)then
      Expkl=DEXP(EXPARG)*EQCDI
      ExpCCkl=Expkl*Basis%gaussian(Kgauss)%CONTRC*Basis%gaussian(Lgauss)%CONTRC
      IF(dabs(ExpCCkl).GT.ERROR1)then
        ISMLQ=0
      else
        ISMLQ=1
      end IF
      IF(dabs(ExpCCkl).GT.ERROR2)then
! Find coordinates of Q relative to axes at A.
! QPERP is perpendicular from Q to AB.
      AQX=ACX+SING*CQ
      AQZ=ACZ+COSG*CQ
      QPERP2=AQX*AQX+ACY2
      QPERP=DSQRT(QPERP2)
! PHI is 180 - Azimuthal angle for AQ in AB local axis system.
      IF(QPERP.GT.TENM12)then
        COSP=-AQX/QPERP
        SINP=-ACY/QPERP
      else
        COSP=ONE
        SINP=ZERO
      end IF
      Hkl(5)=ZERO
      Hkl(6)=ZERO
      Hkl(8)=ZERO
      Hkl(13)=ZERO
      Hkl(14)=ZERO
      Hkl(16)=ZERO
!
! Begin I,J (P) loop.
      do Igauss=IGBGN,IGend
      AS=Basis%gaussian(Igauss)%exp
      do Jgauss=JGBGN,JGend
      BS=Basis%gaussian(Jgauss)%exp
      EPAB=AS+BS
      EPABI=ONE/EPAB
      EPABB=BS*EPABI
      APP=EPABB*RAB
      BPP=APP-RAB
      EXPARG=AS*EPABB*RABSQ
      IF(EXPARG.GT.I2E_expcut)then
        ISMLP=2
        DP01P=ZERO
        CONP=ZERO
        BPP=BPP*EPAB
      else
        Expij=DEXP(-EXPARG)*EPABI
        ExpCCij=Expij*Basis%gaussian(Igauss)%CONTRC*Basis%gaussian(Jgauss)%CONTRC
        IF(dabs(ExpCCij).GT.ERROR1)then
          ISMLP=0
        else
          IF(dabs(ExpCCij).GT.ERROR2)then
            ISMLP=1
          else
            ISMLP=2
          end IF
        end IF
        DP01P=THRTY4*ExpCCij
        CONP=DP01P*EPABI
        BPP=BPP*EPAB
      end IF ! EXPARG.GT.I2E_expcut
!
      ISML=ISMLQ+ISMLP
      IF(ISML.LT.2)then
      IF(ISML.LT.1)then
        AUXVAR=VAR1
      else
        AUXVAR=VAR2
      end IF

      BP=BPP
      PQAB=AQZ-APP
      PQAB2=PQAB*PQAB
      G0=ONE/(EPABI+EQCDI)
      TERM=(PQAB2+QPERP2)*G0

      IF(TERM.GT.AUXVAR)then
        F0=CONP*DSQRT(PT7853/(TERM*(EPAB+EQCD)))
        GTX=G0/TERM
        F1=PT5*F0*GTX
        F2=ONEPT5*F1*GTX
      else
        Yterm=CONP/DSQRT(EPAB+EQCD)
        GY=G0*Yterm
        GGY=G0*GY
        QQ=TERM*TWENTY
        N=INT(QQ)
        THETA=QQ-N
        THETA2=THETA*(THETA-ONE)
        THETA3=THETA2*(THETA-TWO)
        THETA4=THETA2*(THETA+ONE)
        F0=(FMT_val(N+1)%FM0+THETA*FMT_del(N+1)%FM0-THETA3*FMT_int(N+1)%FM0+THETA4*FMT_int(N+2)%FM0)*Yterm
        F1=(FMT_val(N+1)%FM1+THETA*FMT_del(N+1)%FM1-THETA3*FMT_int(N+1)%FM1+THETA4*FMT_int(N+2)%FM1)*GY
        F2=(FMT_val(N+1)%FM2+THETA*FMT_del(N+1)%FM2-THETA3*FMT_int(N+1)%FM2+THETA4*FMT_int(N+2)%FM2)*GGY
      end IF

      G03=-PQAB*F1
      Hkl(5)=Hkl(5)-F1
      Hkl(6)=Hkl(6)-F2
      Hkl(8)=Hkl(8)+PQAB*F2
      Hkl(13)=Hkl(13)-G03+BP*F0
      Hkl(14)=Hkl(14)+BP*F1
      Hkl(16)=Hkl(16)-PQAB2*F2+BP*G03
      end IF ! ISML.LT.2
      end do ! Jgauss
      end do ! Igauss

      TERM=QPERP*EQCDI
      Hkl(11)=-PT5*EQCDI*Hkl(5)
      Hkl(5)=Hkl(5)*QPERP
      Hkl(6)=Hkl(6)*QPERP2*EQCDI
      Hkl(8)=Hkl(8)*TERM
      Hkl(14)=Hkl(14)*TERM
      Hkl(16)=Hkl(16)*EQCDI
      Hkl(14)=Hkl(14)+Hkl(8)
      Hkl(6)=Hkl(6)+Hkl(11)
      Hkl(16)=Hkl(16)+Hkl(11)
!
! No rotation if ABCD coplanar.
      IF(SINP.EQ.ZERO.and.COSP.GT.ZERO)then
        Gijkl(5)=Hkl(5)
        Gijkl(6)=Hkl(6)
        Gijkl(7)=ZERO
        Gijkl(8)=Hkl(8)
        Gijkl(9)=ZERO
        Gijkl(10)=ZERO
        Gijkl(11)=Hkl(11)
!        Gijkl(12)=ZERO
        Gijkl(13)=Hkl(13)
        Gijkl(14)=Hkl(14)
        Gijkl(15)=ZERO
        Gijkl(16)=Hkl(16)
      else if(SINP.EQ.ZERO.and.COSP.LT.ZERO)then
        Gijkl(5)=-Hkl(5)
        Gijkl(6)=Hkl(6)
        Gijkl(7)=ZERO
        Gijkl(8)=-Hkl(8)
        Gijkl(9)=ZERO
        Gijkl(10)=ZERO
        Gijkl(11)=Hkl(11)
!        Gijkl(12)=ZERO
        Gijkl(13)=Hkl(13)
        Gijkl(14)=-Hkl(14)
        Gijkl(15)=ZERO
        Gijkl(16)=Hkl(16)
      else
        Gijkl(6)=COSP*Hkl(6)
        Gijkl(7)=SINP*Hkl(6)
        Gijkl(8)=Hkl(8)
        Gijkl(10)=-SINP*Hkl(11)
        Gijkl(11)=COSP*Hkl(11)
!        Gijkl(12)=ZERO
        Gijkl(14)=COSP*Hkl(14)
        Gijkl(15)=SINP*Hkl(14)
        Gijkl(16)=Hkl(16)
        Gijkl(5)=Hkl(5)
        Gijkl(9)=ZERO
        Gijkl(13)=Hkl(13)
        Hkl(6)=Gijkl(6)
        Hkl(7)=Gijkl(7)
        Hkl(10)=Gijkl(10)
        Hkl(11)=Gijkl(11)
!        Hkl(12)=Gijkl(12)
        Hkl(12)=ZERO
        Hkl(14)=Gijkl(14)
!        Hkl(15)=Gijkl(15)
        Hkl(9)=Gijkl(9)
        Gijkl(6)=COSP*Hkl(6)-SINP*Hkl(10)
        Gijkl(7)=COSP*Hkl(7)-SINP*Hkl(11)
        Gijkl(8)=COSP*Hkl(8) ! -SINP*Hkl(12)
        Gijkl(10)=SINP*Hkl(6)+COSP*Hkl(10)
        Gijkl(11)=SINP*Hkl(7)+COSP*Hkl(11)
        Gijkl(12)=SINP*Hkl(8) ! +COSP*Hkl(12)
        Gijkl(5)=COSP*Hkl(5)-SINP*Hkl(9)
        Gijkl(9)=SINP*Hkl(5)+COSP*Hkl(9)
      end IF
!
! Translates up to 160 integrals on A, B and Q to up to 256 integrals on A, B, C and D.
      IF(RCDSQ.GT.ZERO)then
        R14=DQ*SING
        R34=DQ*COSG
        Gijkl(6)=Gijkl(6)+R14*Gijkl(5)
        Gijkl(10)=Gijkl(10)+R14*Gijkl(9)
        Gijkl(14)=Gijkl(14)+R14*Gijkl(13)

        Gijkl(8)=Gijkl(8)+R34*Gijkl(5)
        Gijkl(12)=Gijkl(12)+R34*Gijkl(9)
        Gijkl(16)=Gijkl(16)+R34*Gijkl(13)
      end IF
! SPSP only (9 ints)
      INTS_out(1)=INTS_out(1)+Gijkl(6)*ExpCCkl
      INTS_out(2)=INTS_out(2)+Gijkl(7)*ExpCCkl
      INTS_out(3)=INTS_out(3)+Gijkl(8)*ExpCCkl
      INTS_out(4)=INTS_out(4)+Gijkl(10)*ExpCCkl
      INTS_out(5)=INTS_out(5)+Gijkl(11)*ExpCCkl
      INTS_out(6)=INTS_out(6)+Gijkl(12)*ExpCCkl
      INTS_out(7)=INTS_out(7)+Gijkl(14)*ExpCCkl
      INTS_out(8)=INTS_out(8)+Gijkl(15)*ExpCCkl
      INTS_out(9)=INTS_out(9)+Gijkl(16)*ExpCCkl
      end IF ! ExpCCkl
      end IF ! EXPARG
      end do ! Lgauss
      end do ! Kgauss

      call R30011 (INTS_out)

      IF(DOZERO)then
        INTS_out(2)=ZERO
        INTS_out(3)=ZERO
        INTS_out(6)=ZERO
      end IF
!
! End of routine SP0101
      return
      end subroutine SP0101
      subroutine R30011 (INTS_out)
!*****************************************************************************
!     Date last modified: May 24, 2002                                       *
!     Author: R.A. Poirier                                                   *
!     Description:  Based QCPE Version.                                      *
!     Rotate up to96 integrals to space fixed axes.                          *
!     P11,... are direction cosines of space fixed axes wrt axes at P.       *
!     Q11,... are direction cosines of space fixed axes wrt axes at Q.       *
!     Applies to 0011 and 0101.                                              *
!*****************************************************************************
! Modules:

      implicit none
!
! Output array:
      double precision INTS_out(9)
!
! Local scalars:
      double precision T1,T2,T3
!
! Begin:
      T1=INTS_out(1)
      T2=INTS_out(4)
      T3=INTS_out(7)
      INTS_out(1)=P11*T1+P21*T2+P31*T3
      INTS_out(4)=P12*T1+P22*T2+P32*T3
      INTS_out(7)=P13*T1+P23*T2+P33*T3
      T1=INTS_out(2)
      T2=INTS_out(5)
      T3=INTS_out(8)
      INTS_out(2)=P11*T1+P21*T2+P31*T3
      INTS_out(5)=P12*T1+P22*T2+P32*T3
      INTS_out(8)=P13*T1+P23*T2+P33*T3
      T1=INTS_out(3)
      T2=INTS_out(6)
      T3=INTS_out(9)
      INTS_out(3)=P11*T1+P21*T2+P31*T3
      INTS_out(6)=P12*T1+P22*T2+P32*T3
      INTS_out(9)=P13*T1+P23*T2+P33*T3
      T1=INTS_out(1)
      T2=INTS_out(2)
      T3=INTS_out(3)
      INTS_out(1)=P11*T1+P21*T2+P31*T3
      INTS_out(2)=P12*T1+P22*T2+P32*T3
      INTS_out(3)=P13*T1+P23*T2+P33*T3
      T1=INTS_out(4)
      T2=INTS_out(5)
      T3=INTS_out(6)
      INTS_out(4)=P11*T1+P21*T2+P31*T3
      INTS_out(5)=P12*T1+P22*T2+P32*T3
      INTS_out(6)=P13*T1+P23*T2+P33*T3
      T1=INTS_out(7)
      T2=INTS_out(8)
      T3=INTS_out(9)
      INTS_out(7)=P11*T1+P21*T2+P31*T3
      INTS_out(8)=P12*T1+P22*T2+P32*T3
      INTS_out(9)=P13*T1+P23*T2+P33*T3
!
! End of routine R30011
      return
      end subroutine R30011
      subroutine I2E_SHL_SSPP (NUMINT)
!***********************************************************************
!     Date last modified: May 24, 2002                     Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version.                                       *
!     Two-electron integral output routine.                            *
!     Note that each block contains integrals of only one type.        *
!***********************************************************************
! Modules:
      USE program_files
      USE QM_objects

      implicit none
!
! Input scalars:
      integer NUMINT
!
! Local scalars:
      integer nint,I,J,K,KL,L,NCNT1,NN,SWITCH
      double precision INT1,INT2,INT3
!
! Begin:
!      call PRG_manager ('enter', 'I2E_SHL_SSPP', 'UTILITY')
!
      NCNT1=0
      Iao=AOI+1
      Jao=AOJ+1
! Loop over set of integrals defined by ashls Iashl, Jashl, etc.
      do K=1,Krange
      do L=1,Lrange
      NCNT1=NCNT1+1
      NUMINT=0
      IF(DABS(GOUT_ijkl(NCNT1)).GT.I2E_ACC)then
        NUMINT=NUMINT+1
      else
        GOUT_ijkl(NCNT1)=ZERO
      end IF
      IF(DABS(GOUT_ikjl(NCNT1)).GT.I2E_ACC)then
        NUMINT=NUMINT+1
      else
        GOUT_ikjl(NCNT1)=ZERO
      end IF
      IF(DABS(GOUT_iljk(NCNT1)).GT.I2E_ACC)then
        NUMINT=NUMINT+1
      else
        GOUT_iljk(NCNT1)=ZERO
      end IF
      IF(NUMINT.NE.0)then

      Kao=AOK+K
      Lao=AOL+L
!
      SWITCH=-1
      IF(Kao.LT.Lao)then
      NN=Kao
      Kao=Lao
      Lao=NN
      SWITCH=-SWITCH
      end IF
!
! The following code determines the integral type
      IF(Iao.NE.Jao.and.Iao.NE.Kao.and.Jao.NE.Kao.and.Kao.NE.Lao.and.Jao.NE.Lao)then
      IF(SWITCH.LT.0)then
        INT1=Gout_ijkl(ncnt1)
        INT2=Gout_ikjl(ncnt1)
        INT3=Gout_iljk(ncnt1)
      else
        INT1=Gout_ijkl(ncnt1)
        INT3=Gout_ikjl(ncnt1)
        INT2=Gout_iljk(ncnt1)
      end IF
!
! Pack label.
      NTINT(8)=NTINT(8)+NUMINT
        IJKL_count=IJKL_count+1
        IF(IJKL_count.gt.IJKL_MAX)then
          IJKL_Nbuffers=IJKL_Nbuffers+1
          if(integral_combinations)call I2E_COMB_ijkl (IJKL_INCORE, IJKL_MAX)
          call WRI_ijkl (IJKL_INCORE, IJKL_MAX, IJKL_unit, IJKL_MAX)
          Lijkl_ONDISK=.TRUE.
          ANY_ONDISK=.TRUE.
          IJKL_count=1
        end IF
        IJKL_INCORE(IJKL_count)%I=Iao
        IJKL_INCORE(IJKL_count)%J=Jao
        IJKL_INCORE(IJKL_count)%K=Kao
        IJKL_INCORE(IJKL_count)%L=Lao
        IJKL_INCORE(IJKL_count)%IJKL=INT1
        IJKL_INCORE(IJKL_count)%IKJL=INT2
        IJKL_INCORE(IJKL_count)%ILJK=INT3
!
      else if(Iao.EQ.Jao.and.Iao.NE.Kao.and.Kao.NE.Lao)then
! (II,KL).
      NTINT(1)=NTINT(1)+NUMINT
        IIKL_count=IIKL_count+1
        IF(IIKL_count.gt.IIKL_MAX)then
          IIKL_Nbuffers=IIKL_Nbuffers+1
          if(integral_combinations)call I2E_COMB_iikl (IIKL_INCORE, IIKL_MAX)
          call WRI_iikl (IIKL_INCORE, IIKL_MAX, IIKL_unit, IIKL_MAX)
          Liikl_ONDISK=.TRUE.
          ANY_ONDISK=.TRUE.
          IIKL_count=1
        end IF
        IIKL_INCORE(IIKL_count)%I=Iao
        IIKL_INCORE(IIKL_count)%K=Kao
        IIKL_INCORE(IIKL_count)%L=Lao
        IIKL_INCORE(IIKL_count)%iikl=Gout_ijkl(ncnt1)
        IIKL_INCORE(IIKL_count)%ikil=Gout_ikjl(ncnt1)
!
      else if(Iao.NE.Jao.and.Jao.GT.Kao.and.Kao.EQ.Lao)then
! (IJ,KK) converted to (IK,JK).
      NTINT(2)=NTINT(2)+NUMINT
        IJKJ_count=IJKJ_count+1
        IF(IJKJ_count.gt.IJKJ_MAX)then
          IJKJ_Nbuffers=IJKJ_Nbuffers+1
          if(integral_combinations)call I2E_COMB_ijkj (IJKJ_INCORE, IJKJ_MAX)
          call WRI_ijkj (IJKJ_INCORE,  IJKJ_MAX, IJKJ_unit, IJKJ_MAX)
          Lijkj_ONDISK=.TRUE.
          ANY_ONDISK=.TRUE.
          IJKJ_count=1
        end IF
        IJKJ_INCORE(IJKJ_count)%I=Iao
        IJKJ_INCORE(IJKJ_count)%J=Kao
        IJKJ_INCORE(IJKJ_count)%K=Jao
        IJKJ_INCORE(IJKJ_count)%ijkj=Gout_ikjl(ncnt1)
        IJKJ_INCORE(IJKJ_count)%ikjj=Gout_ijkl(ncnt1)
!
      else if(Iao.NE.Jao.and.Jao.LT.Kao.and.Kao.EQ.Lao)then
! (IJ,KK) converted to (IK,KJ) = (IJ,JL)
      NTINT(3)=NTINT(3)+NUMINT
        IJJL_count=IJJL_count+1
        IF(IJJL_count.gt.IJJL_MAX)then
          IJJL_Nbuffers=IJJL_Nbuffers+1
          if(integral_combinations)call I2E_COMB_ijjl (IJJL_INCORE, IJJL_MAX)
          call WRI_ijjl (IJJL_INCORE,  IJJL_MAX, IJJL_unit, IJJL_MAX)
          Lijjl_ONDISK=.TRUE.
          ANY_ONDISK=.TRUE.
          IJJL_count=1
        end IF
        IJJL_INCORE(IJJL_count)%I=Iao
        IJJL_INCORE(IJJL_count)%J=Kao
        IJJL_INCORE(IJJL_count)%L=Jao
        IJJL_INCORE(IJJL_count)%ijjl=Gout_ikjl(ncnt1)
        IJJL_INCORE(IJJL_count)%iljj=Gout_ijkl(ncnt1)
!
      else if(Iao.NE.Jao.and.Iao.NE.Kao.and.Jao.NE.Kao.and.Kao.NE.Lao)then
! (IJ,KJ).
      NTINT(2)=NTINT(2)+NUMINT
        IJKJ_count=IJKJ_count+1
        IF(IJKJ_count.gt.IJKJ_MAX)then
          IJKJ_Nbuffers=IJKJ_Nbuffers+1
          if(integral_combinations)call I2E_COMB_ijkj (IJKJ_INCORE, IJKJ_MAX)
          call WRI_ijkj (IJKJ_INCORE,  IJKJ_MAX, IJKJ_unit, IJKJ_MAX)
          Lijkj_ONDISK=.TRUE.
          ANY_ONDISK=.TRUE.
          IJKJ_count=1
        end IF
        IJKJ_INCORE(IJKJ_count)%I=Iao
        IJKJ_INCORE(IJKJ_count)%J=Jao
        IJKJ_INCORE(IJKJ_count)%K=Kao
        IJKJ_INCORE(IJKJ_count)%ijkj=Gout_ijkl(ncnt1)
        IJKJ_INCORE(IJKJ_count)%ikjj=Gout_ikjl(ncnt1)

      else if(Iao.NE.Jao.and.Iao.EQ.Kao.and.Jao.NE.Lao)then
! (IJ,IL) converted to (II,JK) = (II,KL).
      NTINT(1)=NTINT(1)+NUMINT
        IIKL_count=IIKL_count+1
        IF(IIKL_count.gt.IIKL_MAX)then
          IIKL_Nbuffers=IIKL_Nbuffers+1
          if(integral_combinations)call I2E_COMB_iikl (IIKL_INCORE, IIKL_MAX)
          call WRI_iikl (IIKL_INCORE, IIKL_MAX, IIKL_unit, IIKL_MAX)
          Liikl_ONDISK=.TRUE.
          ANY_ONDISK=.TRUE.
          IIKL_count=1
        end IF
        IIKL_INCORE(IIKL_count)%I=Iao
        IIKL_INCORE(IIKL_count)%K=Jao
        IIKL_INCORE(IIKL_count)%L=Lao
        IIKL_INCORE(IIKL_count)%iikl=Gout_ikjl(ncnt1)
        IIKL_INCORE(IIKL_count)%ikil=Gout_ijkl(ncnt1)
!
      else if(Iao.NE.Jao.and.Jao.EQ.Kao.and.Kao.NE.Lao)then
! (IJ,JL).
      NTINT(3)=NTINT(3)+NUMINT
        IJJL_count=IJJL_count+1
        IF(IJJL_count.gt.IJJL_MAX)then
          IJJL_Nbuffers=IJJL_Nbuffers+1
          if(integral_combinations)call I2E_COMB_ijjl (IJJL_INCORE, IJJL_MAX)
          call WRI_ijjl (IJJL_INCORE, IJJL_MAX, IJJL_unit, IJJL_MAX)
          Lijjl_ONDISK=.TRUE.
          ANY_ONDISK=.TRUE.
          IJJL_count=1
        end IF
        IJJL_INCORE(IJJL_count)%I=Iao
        IJJL_INCORE(IJJL_count)%J=Jao
        IJJL_INCORE(IJJL_count)%L=Lao
        IJJL_INCORE(IJJL_count)%ijjl=Gout_ijkl(ncnt1)
        IJJL_INCORE(IJJL_count)%iljj=Gout_ikjl(ncnt1)
!
      else if(Iao.EQ.Jao.and.Iao.NE.Kao.and.Kao.EQ.Lao)then
! (II,KK).
      NTINT(4)=NTINT(4)+NUMINT
      IIKK_count=IIKK_count+1
      IF(IIKK_count.GT.IIKK_MAX)then
        write(UNIout,*)' error_I2E_SHL_SSPP> too many IIKK integrals'
        stop ' error_I2E_SHL_SSPP> too many IIKK integrals'
      end IF
      IIKK_INCORE(IIKK_count)%I=Iao
      IIKK_INCORE(IIKK_count)%K=Kao
      IIKK_INCORE(IIKK_count)%iikk=Gout_ijkl(ncnt1)
      IIKK_INCORE(IIKK_count)%ikik=Gout_ikjl(ncnt1)
!
      else if(Iao.NE.Jao.and.Iao.EQ.Kao.and.Jao.EQ.Lao)then
!
! (IJ,IJ) converted to (II,KK).
      NTINT(4)=NTINT(4)+NUMINT
      IIKK_count=IIKK_count+1
      IF(IIKK_count.GT.IIKK_MAX)then
        write(UNIout,*)' error_I2E_SHL_SSPP> too many IIKK integrals'
        stop ' error_I2E_SHL_SSPP> too many IIKK integrals'
      end IF
      IIKK_INCORE(IIKK_count)%I=Iao
      IIKK_INCORE(IIKK_count)%K=Jao
      IIKK_INCORE(IIKK_count)%iikk=Gout_ikjl(ncnt1)
      IIKK_INCORE(IIKK_count)%ikik=Gout_ijkl(ncnt1)
!
      else if(Iao.NE.Jao.and.Jao.EQ.Kao.and.Kao.EQ.Lao)then
!
! (IJ,JJ).
      NTINT(5)=NTINT(5)+NUMINT
      IJJJ_count=IJJJ_count+1
      IF(IJJJ_count.GT.IJJJ_MAX)then
        write(UNIout,*)' error_I2E_SHL_SSPP> too many IJJJ integrals'
        stop ' error_I2E_SHL_SSPP> too many IJJJ integrals'
      end IF
      IJJJ_INCORE(IJJJ_count)%I=Iao
      IJJJ_INCORE(IJJJ_count)%J=Jao
      IJJJ_INCORE(IJJJ_count)%ijjj=Gout_ijkl(ncnt1)
!
      else if(Iao.EQ.Jao.and.Iao.EQ.Kao.and.Iao.NE.Lao)then
!
! (II,IL).
      NTINT(6)=NTINT(6)+NUMINT
      IIIL_count=IIIL_count+1
      IF(IIIL_count.GT.IIIL_MAX)then
        write(UNIout,*)' error_I2E_SHL_SSPP> too many IIIL integrals'
        stop
      end IF
      IIIL_INCORE(IIIL_count)%I=Iao
      IIIL_INCORE(IIIL_count)%L=Lao
      IIIL_INCORE(IIIL_count)%iiil=Gout_ijkl(ncnt1)
!
      else if(Iao.EQ.Jao.and.Iao.EQ.Kao.and.Iao.EQ.Lao)then
!
! (II,II).
      NTINT(7)=NTINT(7)+NUMINT
      IIII_count=IIII_count+1
      IF(IIII_count.GT.IIII_MAX)then
        write(UNIout,*)' error_I2E_SHL_SSPP> too many IIII integrals'
        stop ' error_I2E_SHL_SSPP> too many IIII integrals'
      end IF
      IIII_INCORE(IIII_count)%I=Iao
      IIII_INCORE(IIII_count)%iiii=Gout_ijkl(ncnt1)
!
      else
        stop 'I2E_SHL_SSPP: illegal integral type found'
      end IF
      end IF ! NUMINT.NE.0
      end do ! L
      end do ! K
!
! End of routine I2E_SHL_SSPP
!      call PRG_manager ('exit', 'I2E_SHL_SSPP', 'UTILITY')
      return
      end subroutine I2E_SHL_SSPP
      end subroutine ISP_SSPP
      subroutine ISP_SPPP
!***********************************************************************
!     Date last modified: May 24, 2002                     Version 1.0 *
!     Author: R. A. Poirier                                            *
!     Description: Special fast routine for SSSS integrals.            *
!***********************************************************************
! Modules:

      implicit none
!
! Local scalars:
      integer I,IGOUT
      logical DOZERO
!
! Local arrays:
      double precision, dimension(:), pointer :: INT_out => null()
!
! Begin:
      Irange=1
      Jrange=3
      Krange=3
      Lrange=3
!
      IGOUT=27
      include 'mungauss_do_IJKL'
!
      GOUT_ijkl(1:27)=ZERO
      GOUT_ikjl(1:27)=ZERO
      GOUT_iljk(1:27)=ZERO
! Set 1
      Iashl=Iatmshl
      Jashl=Jatmshl
      Kashl=Katmshl
      Lashl=Latmshl
      call SET_shells (Ishell, Jshell, Kshell, Lshell)
      DOZERO=.false.
      IF(Kashl.EQ.Lashl)DOZERO=.TRUE.
      INT_out => GOUT(1:Igout)
      call SP0111 (INT_out, DOZERO)
!
! Set 2
      if(Mtype.eq.3)then
      Iashl=Iatmshl
      Jashl=Latmshl
      Kashl=Jatmshl
      Lashl=Katmshl
      call SET_shells (Ishell, Lshell, Jshell, Kshell)
      DOZERO=.false.
      IF(Kashl.EQ.Lashl)DOZERO=.TRUE.
      INT_out => GOUT(Igout+1:2*Igout)
      call SP0111 (INT_out, DOZERO)
      else if(Mtype.lt.6)then
      Iashl=Iatmshl
      Jashl=Katmshl
      Kashl=Jatmshl
      Lashl=Latmshl
      call SET_shells (Ishell, Kshell, Jshell, Lshell)
      DOZERO=.false.
      IF(Kashl.EQ.Lashl)DOZERO=.TRUE.
      INT_out => GOUT(Igout+1:2*Igout)
      call SP0111 (INT_out, DOZERO)
      end if
!
! Set 3
      if(Mtype.eq.1)then
      Iashl=Iatmshl
      Jashl=Latmshl
      Kashl=Jatmshl
      Lashl=Katmshl
      call SET_shells (Ishell, Lshell, Jshell, Kshell)
      INT_out => GOUT(2*Igout+1:3*Igout)
      call SP0111 (INT_out, DOZERO)
      end if

      IF(Mtype.EQ.1)then
        call SORT1N (Igout)
      else if(Mtype.EQ.2)then
        call SORT2LN (Igout)
      else if(Mtype.EQ.3)then
        call SORT3N (Igout)
      else
        call SORT4N (Igout)
      end IF
!
      if(Mtype.eq.1)then
        call I2E_SHL_SPPP ! for now
      else
        call I2E_SHL_SPPP
      end if
!
      include 'mungauss_endo_IJKL'
!
! End of routine ISP_SPPP
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine SP0111 (INTS_out, &
                         DOZERO)
!***********************************************************************
!     Date last modified: May 24, 2002                                 *
!     Author:                                                          *
!     Description: Special fast routine for P loop for 0111.           *
!***********************************************************************
! Modules:

      implicit none
!
! Input logical:
      logical DOZERO
!
! Output array:
      double precision INTS_out(27)
!
! Local scalars:
      integer I,ISML,ISMLP,ISMLQ,J,N
      double precision AS,EPAB,BS,EPABB
      double precision AQX,AQZ,AUXVAR,COSP,COSP2,COSSIN,CQ,DQ,APP,BPP,CONP,DP01P,EPABI, &
                       EQCDI,ECD2,F0,F1,F1PQAB,F2,F2PQAB,F2PQA2,F3,F3PQAB,CS,EQCD,DS,GGGY,GGY,GTX,GY,G0,HECD,MCP2P1,MTCPSP, &
                       PQAB,PQAB2,QECD,QECD2,QPERP,QPERP2,QQ,Q2ECD,Q2ECD2, &
                       TCP2M1,THETA,THETA2,THETA3,THETA4,TERM, &
                       Yterm,Y1,Y2,Y3,Y4,Y5,Y6,Z1,Z2,Z3,Z4,Z5,Z6,Z7,Z8,Z9
      double precision Expij,Expkl,EXPARG,ExpCCij,ExpCCkl
! Local arrays:
      double precision Gijkl(64),Hkl(27)
!
! Begin:
      Gijkl(1:64)=ZERO
      Hkl(1:27)=ZERO
! Begin Q loop.
      do Kgauss=KGBGN,KGend
      CS=Basis%gaussian(Kgauss)%exp
      do Lgauss=LGBGN,LGend
      DS=Basis%gaussian(Lgauss)%exp
      EQCD=CS+DS
      EQCDI=ONE/EQCD
      CQ=DS*EQCDI*RCD
      DQ=CQ-RCD
      EXPARG=CQ*DQ*EQCD
!     IF(EXPARG.GE.-I2E_expcut)then
      if(EXPARG.le.I2E_expcut)then
      Expkl=DEXP(EXPARG)*EQCDI
      ExpCCkl=Expkl*Basis%gaussian(Kgauss)%CONTRC*Basis%gaussian(Lgauss)%CONTRC
      IF(dabs(ExpCCkl).GT.ERROR1)then
        ISMLQ=0
      else
        ISMLQ=1
      end IF
      IF(dabs(ExpCCkl).GT.ERROR2)then
! Find coordinates of Q relative to axes at A.
! QPERP is perpendicular from Q to AB.
      AQX=ACX+SING*CQ
      AQZ=ACZ+COSG*CQ
      QPERP2=AQX*AQX+ACY2
      QPERP=DSQRT(QPERP2)
! PHI is 180 - Azimuthal angle for AQ in AB local axis system.
      IF(QPERP.GT.TENM12)then
        COSP=-AQX/QPERP
        SINP=-ACY/QPERP
      else
        COSP=ONE
        SINP=ZERO
      end IF
      Y1=ZERO
      Y2=ZERO
      Y3=ZERO
      Y4=ZERO
      Y5=ZERO
      Y6=ZERO
      Z1=ZERO
      Z2=ZERO
      Z3=ZERO
      Z4=ZERO
      Z5=ZERO
      Z6=ZERO
      Z7=ZERO
      Z8=ZERO
      Z9=ZERO
! Begin P loop.
      do Igauss=IGBGN,IGend
      AS=Basis%gaussian(Igauss)%exp
      do Jgauss=JGBGN,JGend
      BS=Basis%gaussian(Jgauss)%exp
      EPAB=AS+BS
      EPABI=ONE/EPAB
      EPABB=BS*EPABI
      APP=EPABB*RAB
      BPP=APP-RAB
      EXPARG=AS*EPABB*RABSQ
      IF(EXPARG.GT.I2E_expcut)then
        ISMLP=2
        DP01P=ZERO
        CONP=ZERO
        BPP=BPP*EPAB
      else
        Expij=DEXP(-EXPARG)*EPABI
        ExpCCij=Expij*Basis%gaussian(Igauss)%CONTRC*Basis%gaussian(Jgauss)%CONTRC
        IF(dabs(ExpCCij).GT.ERROR1)then
          ISMLP=0
        else
          IF(dabs(ExpCCij).GT.ERROR2)then
            ISMLP=1
          else
            ISMLP=2
          end IF
        end IF
! For type 00111 only DP01P needed.
        DP01P=THRTY4*ExpCCij
        CONP=DP01P*EPABI
        BPP=BPP*EPAB
      end IF
      ISML=ISMLQ+ISMLP
      IF(ISML.LT.2)then
      IF(ISML.LT.1)then
        AUXVAR=VAR1
      else
        AUXVAR=VAR2
      end IF
      EPABI=EPABI
      BP=BPP
      PQAB=AQZ-APP
      PQAB2=PQAB*PQAB
      G0=ONE/(EPABI+EQCDI)
      TERM=G0*(PQAB2+QPERP2)
      IF(TERM.GT.AUXVAR)then
        F0=CONP*DSQRT(PT7853/(TERM*(EPAB+EQCD)))
        GTX=G0/TERM
        F1=PT5*F0*GTX
        F2=ONEPT5*F1*GTX
        F3=TWOPT5*F2*GTX
      else
        Yterm=CONP/DSQRT(EPAB+EQCD)
        GY=G0*Yterm
        GGY=G0*GY
        GGGY=G0*GGY
        QQ=TERM*TWENTY
        N=INT(QQ)
        THETA=QQ-N
        THETA2=THETA*(THETA-ONE)
        THETA3=THETA2*(THETA-TWO)
        THETA4=THETA2*(THETA+ONE)
        F0=(FMT_val(N+1)%FM0+THETA*FMT_del(N+1)%FM0-THETA3*FMT_int(N+1)%FM0+THETA4*FMT_int(N+2)%FM0)*Yterm
        F1=(FMT_val(N+1)%FM1+THETA*FMT_del(N+1)%FM1-THETA3*FMT_int(N+1)%FM1+THETA4*FMT_int(N+2)%FM1)*GY
        F2=(FMT_val(N+1)%FM2+THETA*FMT_del(N+1)%FM2-THETA3*FMT_int(N+1)%FM2+THETA4*FMT_int(N+2)%FM2)*GGY
        F3=(FMT_val(N+1)%FM3+THETA*FMT_del(N+1)%FM3-THETA3*FMT_int(N+1)%FM3+THETA4*FMT_int(N+2)%FM3)*GGGY
      end IF
      F1PQAB=F1*PQAB
      F2PQAB=F2*PQAB
      F3PQAB=F3*PQAB
      F2PQA2=F2*PQAB2
      Y1=Y1+F0*BP
      Y2=Y2+F1*BP
      Y3=Y3+F2*BP
      Y4=Y4+F1PQAB*BP
      Y5=Y5+F2PQAB*BP
      Y6=Y6+F2PQA2*BP
      Z1=Z1+F1
      Z2=Z2+F2
      Z3=Z3+F3
      Z4=Z4+F1PQAB
      Z5=Z5+F2PQAB
      Z6=Z6+F3PQAB
      Z7=Z7+F2PQA2
      Z8=Z8+F3*PQAB2
      Z9=Z9+F3PQAB*PQAB2
      end IF ! ISML.LT.2
      end do ! Jgauss
      end do ! Igauss

      HECD=PT5*EQCDI
      ECD2=EQCDI*EQCDI
      QECD=QPERP*EQCDI
      QECD2=QPERP*ECD2
      Q2ECD=QPERP2*EQCDI
      Q2ECD2=QPERP2*ECD2
      Hkl(1)=-QPERP*Z1
      Hkl(21)=Z4+Y1
      Hkl(13)=HECD*Z1
      Hkl(2)=Hkl(13)-Q2ECD*Z2
      Hkl(4)=QECD*Z5
      Hkl(22)=Hkl(4)+QECD*Y2
      Hkl(23)=Hkl(13)-EQCDI*Z7-EQCDI*Y4
      Hkl(16)=PT5*QECD2*Z2
      Hkl(19)=-PT5*ECD2*Z5
      Hkl(8)=Hkl(16)-QPERP*Hkl(13)
      Hkl(26)=Hkl(19)+HECD*(Hkl(21)-EQCDI*Y2)
      Hkl(7)=Hkl(19)+Q2ECD2*Z6
      Hkl(25)=Hkl(16)-QECD2*(Z8+Y5)
      Hkl(5)=Hkl(8)+Hkl(16)+Hkl(16)-Q2ECD2*QPERP*Z3
      Hkl(10)=Hkl(8)-QECD2*Z8
      Hkl(24)=Hkl(26)+Q2ECD2*(Z6+Y3)
      Hkl(27)=Hkl(26)+Hkl(19)+Hkl(19)+ECD2*(Z9+Y6)
!
! No rotation if ABCD coplanar.
      IF(SINP.EQ.ZERO.and.COSP.GT.ZERO)then
        Gijkl(17)=Hkl(1)
        Gijkl(18)=Hkl(2)
        Gijkl(19)=ZERO
        Gijkl(20)=Hkl(4)
        Gijkl(22)=Hkl(5)          ! used
        Gijkl(23)=ZERO
        Gijkl(24)=Hkl(7)          ! used
        Gijkl(27)=Hkl(8)
        Gijkl(28)=ZERO
        Gijkl(32)=Hkl(10)
        Gijkl(33)=ZERO
        Gijkl(34)=ZERO
        Gijkl(35)=Hkl(13)
        Gijkl(36)=ZERO
        Gijkl(38)=ZERO
        Gijkl(39)=Hkl(16)
        Gijkl(40)=ZERO
        Gijkl(43)=ZERO
        Gijkl(44)=Hkl(19)
        Gijkl(48)=ZERO
        Gijkl(49)=Hkl(21)
        Gijkl(50)=Hkl(22)
        Gijkl(51)=ZERO
        Gijkl(52)=Hkl(23)
        Gijkl(54)=Hkl(24)
        Gijkl(55)=ZERO
        Gijkl(56)=Hkl(25)
        Gijkl(59)=Hkl(26)
        Gijkl(60)=ZERO
        Gijkl(64)=Hkl(27)
      else if(SINP.EQ.ZERO.and.COSP.LT.ZERO)then
        Gijkl(17)=-Hkl(1)
        Gijkl(18)=Hkl(2)
        Gijkl(19)=ZERO
        Gijkl(20)=-Hkl(4)
        Gijkl(22)=-Hkl(5)       ! used
        Gijkl(23)=ZERO
        Gijkl(24)=Hkl(7)        ! used
        Gijkl(27)=-Hkl(8)
        Gijkl(28)=ZERO
        Gijkl(32)=-Hkl(10)
        Gijkl(33)=ZERO
        Gijkl(34)=ZERO
        Gijkl(35)=Hkl(13)
        Gijkl(36)=ZERO
        Gijkl(38)=ZERO
        Gijkl(39)=-Hkl(16)
        Gijkl(40)=ZERO
        Gijkl(43)=ZERO
        Gijkl(44)=Hkl(19)
        Gijkl(48)=ZERO
        Gijkl(49)=Hkl(21)
        Gijkl(50)=-Hkl(22)
        Gijkl(51)=ZERO
        Gijkl(52)=Hkl(23)
        Gijkl(54)=Hkl(24)
        Gijkl(55)=ZERO
        Gijkl(56)=-Hkl(25)
        Gijkl(59)=Hkl(26)
        Gijkl(60)=ZERO
        Gijkl(64)=Hkl(27)
      else
        COSP2=COSP*COSP
        MCP2P1=ONE-COSP2
        COSSIN=COSP*SINP
        MTCPSP=-COSSIN-COSSIN
        TCP2M1=COSP2-MCP2P1
!
        Gijkl(22)=COSP2*Hkl(5)+MCP2P1*Hkl(8)    ! used
        Gijkl(23)=COSSIN*Hkl(5)-COSSIN*Hkl(8)   ! used
        Gijkl(27)=MCP2P1*Hkl(5)+COSP2*Hkl(8)
        Gijkl(24)=COSP*Hkl(7)                    ! used
        Gijkl(28)=SINP*Hkl(7)
        Gijkl(32)=Hkl(10)
        Gijkl(38)=MTCPSP*Hkl(16)
        Gijkl(39)=TCP2M1*Hkl(16)
        Gijkl(43)=-Gijkl(38)
        Gijkl(40)=-SINP*Hkl(19)
        Gijkl(44)=COSP*Hkl(19)
        Gijkl(48)=ZERO
        Gijkl(54)=COSP2*Hkl(24)+MCP2P1*Hkl(26)
        Gijkl(55)=COSSIN*Hkl(24)-COSSIN*Hkl(26)
        Gijkl(59)=MCP2P1*Hkl(24)+COSP2*Hkl(26)
        Gijkl(56)=COSP*Hkl(25)
        Gijkl(60)=SINP*Hkl(25)
        Gijkl(64)=Hkl(27)
        Gijkl(18)=COSP*Hkl(2)
        Gijkl(19)=SINP*Hkl(2)
        Gijkl(20)=Hkl(4)
        Gijkl(34)=-SINP*Hkl(13)
        Gijkl(35)=COSP*Hkl(13)
        Gijkl(36)=ZERO
        Gijkl(50)=COSP*Hkl(22)
        Gijkl(51)=SINP*Hkl(22)
        Gijkl(52)=Hkl(23)
        Gijkl(17)=Hkl(1)
        Gijkl(33)=ZERO
        Gijkl(49)=Hkl(21)
        Hkl(5)=Gijkl(22)
        Hkl(6)=Gijkl(23)
        Hkl(7)=Gijkl(24)       ! used
        Hkl(8)=Gijkl(27)
        Hkl(9)=Gijkl(28)
        Hkl(15)=Gijkl(38)
        Hkl(16)=Gijkl(39)
        Hkl(17)=Gijkl(40)
        Hkl(18)=Gijkl(43)
        Hkl(19)=Gijkl(44)
        Hkl(20)=Gijkl(48)
        Hkl(24)=Gijkl(54)
!        Hkl(55)=Gijkl(55)
        Hkl(25)=Gijkl(56)
        Hkl(26)=Gijkl(59)
        Hkl(2)=Gijkl(18)
        Hkl(3)=Gijkl(19)
        Hkl(12)=Gijkl(34)
        Hkl(13)=Gijkl(35)
        Hkl(14)=Gijkl(36)
        Hkl(22)=Gijkl(50)
!        Hkl(51)=Gijkl(51)
        Hkl(11)=Gijkl(33)
        Gijkl(22)=COSP*Hkl(5)-SINP*Hkl(15)    ! used
        Gijkl(23)=COSP*Hkl(6)-SINP*Hkl(16)    ! used
        Gijkl(24)=COSP*Hkl(7)-SINP*Hkl(17)    ! used
        Gijkl(27)=COSP*Hkl(8)-SINP*Hkl(18)
        Gijkl(28)=COSP*Hkl(9)-SINP*Hkl(19)
        Gijkl(32)=COSP*Hkl(10)-SINP*Hkl(20)
        Gijkl(38)=SINP*Hkl(5)+COSP*Hkl(15)
        Gijkl(39)=SINP*Hkl(6)+COSP*Hkl(16)
        Gijkl(40)=SINP*Hkl(7)+COSP*Hkl(17)
        Gijkl(43)=SINP*Hkl(8)+COSP*Hkl(18)
        Gijkl(44)=SINP*Hkl(9)+COSP*Hkl(19)
        Gijkl(48)=SINP*Hkl(10)+COSP*Hkl(20)
        Gijkl(18)=COSP*Hkl(2)-SINP*Hkl(12)
        Gijkl(19)=COSP*Hkl(3)-SINP*Hkl(13)
        Gijkl(20)=COSP*Hkl(4)-SINP*Hkl(14)
        Gijkl(34)=SINP*Hkl(2)+COSP*Hkl(12)
        Gijkl(35)=SINP*Hkl(3)+COSP*Hkl(13)
        Gijkl(36)=SINP*Hkl(4)+COSP*Hkl(14)
        Gijkl(17)=COSP*Hkl(1)-SINP*Hkl(11)
        Gijkl(33)=SINP*Hkl(1)+COSP*Hkl(11)
      end IF
!
! Translates up to 160 integrals on A, B and Q to up to 256 integrals on A, B, C and D.
      Gijkl(21)=Gijkl(18)
      Gijkl(25)=Gijkl(19)
      Gijkl(26)=Gijkl(23)  !used
      Gijkl(29)=Gijkl(20)
      Gijkl(30)=Gijkl(24) ! used
      Gijkl(31)=Gijkl(28)
      Gijkl(37)=Gijkl(34)
      Gijkl(41)=Gijkl(35)
      Gijkl(42)=Gijkl(39)
      Gijkl(45)=Gijkl(36)
      Gijkl(46)=Gijkl(40)
      Gijkl(47)=Gijkl(44)
      Gijkl(53)=Gijkl(50)
      Gijkl(57)=Gijkl(51)
      Gijkl(58)=Gijkl(55)
      Gijkl(61)=Gijkl(52)
      Gijkl(62)=Gijkl(56)
      Gijkl(63)=Gijkl(60)

      IF(RCDSQ.GT.ZERO)then
        R13=CQ*SING
        R33=CQ*COSG
        R14=DQ*SING
        R34=DQ*COSG

        Gijkl(21)=Gijkl(21)+R13*Gijkl(17)
        Gijkl(22)=Gijkl(22)+R13*Gijkl(18) ! used
        Gijkl(23)=Gijkl(23)+R13*Gijkl(19) ! used
        Gijkl(24)=Gijkl(24)+R13*Gijkl(20) ! used

        Gijkl(37)=Gijkl(37)+R13*Gijkl(33)
        Gijkl(38)=Gijkl(38)+R13*Gijkl(34)
        Gijkl(39)=Gijkl(39)+R13*Gijkl(35)
        Gijkl(40)=Gijkl(40)+R13*Gijkl(36)

        Gijkl(53)=Gijkl(53)+R13*Gijkl(49)
        Gijkl(54)=Gijkl(54)+R13*Gijkl(50)
        Gijkl(55)=Gijkl(55)+R13*Gijkl(51)
        Gijkl(56)=Gijkl(56)+R13*Gijkl(52)

        Gijkl(29)=Gijkl(29)+R33*Gijkl(17)
        Gijkl(30)=Gijkl(30)+R33*Gijkl(18)  ! used
        Gijkl(31)=Gijkl(31)+R33*Gijkl(19)
        Gijkl(32)=Gijkl(32)+R33*Gijkl(20)

        Gijkl(45)=Gijkl(45)+R33*Gijkl(33)
        Gijkl(46)=Gijkl(46)+R33*Gijkl(34)
        Gijkl(47)=Gijkl(47)+R33*Gijkl(35)
        Gijkl(48)=Gijkl(48)+R33*Gijkl(36)

        Gijkl(61)=Gijkl(61)+R33*Gijkl(49)
        Gijkl(62)=Gijkl(62)+R33*Gijkl(50)
        Gijkl(63)=Gijkl(63)+R33*Gijkl(51)
        Gijkl(64)=Gijkl(64)+R33*Gijkl(52)

        Gijkl(18)=Gijkl(18)+R14*Gijkl(17)
        Gijkl(22)=Gijkl(22)+R14*Gijkl(21) ! used
        Gijkl(26)=Gijkl(26)+R14*Gijkl(25)
        Gijkl(30)=Gijkl(30)+R14*Gijkl(29) ! used
        Gijkl(34)=Gijkl(34)+R14*Gijkl(33)
        Gijkl(38)=Gijkl(38)+R14*Gijkl(37)
        Gijkl(42)=Gijkl(42)+R14*Gijkl(41)
        Gijkl(46)=Gijkl(46)+R14*Gijkl(45)
        Gijkl(50)=Gijkl(50)+R14*Gijkl(49)
        Gijkl(54)=Gijkl(54)+R14*Gijkl(53)
        Gijkl(58)=Gijkl(58)+R14*Gijkl(57)
        Gijkl(62)=Gijkl(62)+R14*Gijkl(61)

        Gijkl(20)=Gijkl(20)+R34*Gijkl(17)
        Gijkl(24)=Gijkl(24)+R34*Gijkl(21)  ! used
        Gijkl(28)=Gijkl(28)+R34*Gijkl(25)
        Gijkl(32)=Gijkl(32)+R34*Gijkl(29)
        Gijkl(36)=Gijkl(36)+R34*Gijkl(33)
        Gijkl(40)=Gijkl(40)+R34*Gijkl(37)
        Gijkl(44)=Gijkl(44)+R34*Gijkl(41)
        Gijkl(48)=Gijkl(48)+R34*Gijkl(45)
        Gijkl(52)=Gijkl(52)+R34*Gijkl(49)
        Gijkl(56)=Gijkl(56)+R34*Gijkl(53)
        Gijkl(60)=Gijkl(60)+R34*Gijkl(57)
        Gijkl(64)=Gijkl(64)+R34*Gijkl(61)
      end IF

      Gijkl(1:21)=ZERO
      Gijkl(25)=ZERO
      Gijkl(29)=ZERO
      Gijkl(33:37)=ZERO
      Gijkl(41)=ZERO
      Gijkl(45)=ZERO
      Gijkl(49:53)=ZERO
      Gijkl(57)=ZERO
      Gijkl(61)=ZERO
!
! Total of 27 non-zero
      INTS_out(1)=INTS_out(1)+Gijkl(22)*ExpCCkl
      INTS_out(2)=INTS_out(2)+Gijkl(23)*ExpCCkl
      INTS_out(3)=INTS_out(3)+Gijkl(24)*ExpCCkl
      INTS_out(4)=INTS_out(4)+Gijkl(26)*ExpCCkl
      INTS_out(5)=INTS_out(5)+Gijkl(27)*ExpCCkl
      INTS_out(6)=INTS_out(6)+Gijkl(28)*ExpCCkl
      INTS_out(7)=INTS_out(7)+Gijkl(30)*ExpCCkl
      INTS_out(8)=INTS_out(8)+Gijkl(31)*ExpCCkl
      INTS_out(9)=INTS_out(9)+Gijkl(32)*ExpCCkl
      INTS_out(10)=INTS_out(10)+Gijkl(38)*ExpCCkl
      INTS_out(11)=INTS_out(11)+Gijkl(39)*ExpCCkl
      INTS_out(12)=INTS_out(12)+Gijkl(40)*ExpCCkl
      INTS_out(13)=INTS_out(13)+Gijkl(42)*ExpCCkl
      INTS_out(14)=INTS_out(14)+Gijkl(43)*ExpCCkl
      INTS_out(15)=INTS_out(15)+Gijkl(44)*ExpCCkl
      INTS_out(16)=INTS_out(16)+Gijkl(46)*ExpCCkl
      INTS_out(17)=INTS_out(17)+Gijkl(47)*ExpCCkl
      INTS_out(18)=INTS_out(18)+Gijkl(48)*ExpCCkl
      INTS_out(19)=INTS_out(19)+Gijkl(54)*ExpCCkl
      INTS_out(20)=INTS_out(20)+Gijkl(55)*ExpCCkl
      INTS_out(21)=INTS_out(21)+Gijkl(56)*ExpCCkl
      INTS_out(22)=INTS_out(22)+Gijkl(58)*ExpCCkl
      INTS_out(23)=INTS_out(23)+Gijkl(59)*ExpCCkl
      INTS_out(24)=INTS_out(24)+Gijkl(60)*ExpCCkl
      INTS_out(25)=INTS_out(25)+Gijkl(62)*ExpCCkl
      INTS_out(26)=INTS_out(26)+Gijkl(63)*ExpCCkl
      INTS_out(27)=INTS_out(27)+Gijkl(64)*ExpCCkl
      end IF ! ExpCCkl
      end IF ! EXPARG
      end do ! Lgauss
      end do ! Kgauss
      call R30111 (INTS_out)
      IF(DOZERO)then
        INTS_out(2)=ZERO
        INTS_out(3)=ZERO
        INTS_out(6)=ZERO
        INTS_out(11)=ZERO
        INTS_out(12)=ZERO
        INTS_out(15)=ZERO
        INTS_out(20)=ZERO
        INTS_out(21)=ZERO
        INTS_out(24)=ZERO
      end IF
!
! End of routine SP0111
      return
      end subroutine SP0111
      subroutine R30111 (INTS_out)
!*****************************************************************************
!     Date last modified: May 24, 2002                                       *
!     Author: R.A. Poirier                                                   *
!     Description:  Based QCPE Version.                                      *
!     Rotate up to 27 integrals to space fixed axes.                         *
!*****************************************************************************
! Modules:

      implicit none
!
! Output array:
      double precision INTS_out(27)
!
! Local scalars:
      double precision T1,T2,T3
!
! Begin:
      T1=INTS_out(1)
      T2=INTS_out(10)
      T3=INTS_out(19)
      INTS_out(1)=P11*T1+P21*T2+P31*T3
      INTS_out(10)=P12*T1+P22*T2+P32*T3
      INTS_out(19)=P13*T1+P23*T2+P33*T3
      T1=INTS_out(2)
      T2=INTS_out(11)
      T3=INTS_out(20)
      INTS_out(2)=P11*T1+P21*T2+P31*T3
      INTS_out(11)=P12*T1+P22*T2+P32*T3
      INTS_out(20)=P13*T1+P23*T2+P33*T3
      T1=INTS_out(3)
      T2=INTS_out(12)
      T3=INTS_out(21)
      INTS_out(3)=P11*T1+P21*T2+P31*T3
      INTS_out(12)=P12*T1+P22*T2+P32*T3
      INTS_out(21)=P13*T1+P23*T2+P33*T3
      T1=INTS_out(4)
      T2=INTS_out(13)
      T3=INTS_out(22)
      INTS_out(4)=P11*T1+P21*T2+P31*T3
      INTS_out(13)=P12*T1+P22*T2+P32*T3
      INTS_out(22)=P13*T1+P23*T2+P33*T3
      T1=INTS_out(5)
      T2=INTS_out(14)
      T3=INTS_out(23)
      INTS_out(5)=P11*T1+P21*T2+P31*T3
      INTS_out(14)=P12*T1+P22*T2+P32*T3
      INTS_out(23)=P13*T1+P23*T2+P33*T3
      T1=INTS_out(6)
      T2=INTS_out(15)
      T3=INTS_out(24)
      INTS_out(6)=P11*T1+P21*T2+P31*T3
      INTS_out(15)=P12*T1+P22*T2+P32*T3
      INTS_out(24)=P13*T1+P23*T2+P33*T3
      T1=INTS_out(7)
      T2=INTS_out(16)
      T3=INTS_out(25)
      INTS_out(7)=P11*T1+P21*T2+P31*T3
      INTS_out(16)=P12*T1+P22*T2+P32*T3
      INTS_out(25)=P13*T1+P23*T2+P33*T3
      T1=INTS_out(8)
      T2=INTS_out(17)
      T3=INTS_out(26)
      INTS_out(8)=P11*T1+P21*T2+P31*T3
      INTS_out(17)=P12*T1+P22*T2+P32*T3
      INTS_out(26)=P13*T1+P23*T2+P33*T3
      T1=INTS_out(9)
      T2=INTS_out(18)
      T3=INTS_out(27)
      INTS_out(9)=P11*T1+P21*T2+P31*T3
      INTS_out(18)=P12*T1+P22*T2+P32*T3
      INTS_out(27)=P13*T1+P23*T2+P33*T3
      T1=INTS_out(1)
      T2=INTS_out(4)
      T3=INTS_out(7)
      INTS_out(1)=P11*T1+P21*T2+P31*T3
      INTS_out(4)=P12*T1+P22*T2+P32*T3
      INTS_out(7)=P13*T1+P23*T2+P33*T3
      T1=INTS_out(2)
      T2=INTS_out(5)
      T3=INTS_out(8)
      INTS_out(2)=P11*T1+P21*T2+P31*T3
      INTS_out(5)=P12*T1+P22*T2+P32*T3
      INTS_out(8)=P13*T1+P23*T2+P33*T3
      T1=INTS_out(3)
      T2=INTS_out(6)
      T3=INTS_out(9)
      INTS_out(3)=P11*T1+P21*T2+P31*T3
      INTS_out(6)=P12*T1+P22*T2+P32*T3
      INTS_out(9)=P13*T1+P23*T2+P33*T3
      T1=INTS_out(10)
      T2=INTS_out(13)
      T3=INTS_out(16)
      INTS_out(10)=P11*T1+P21*T2+P31*T3
      INTS_out(13)=P12*T1+P22*T2+P32*T3
      INTS_out(16)=P13*T1+P23*T2+P33*T3
      T1=INTS_out(11)
      T2=INTS_out(14)
      T3=INTS_out(17)
      INTS_out(11)=P11*T1+P21*T2+P31*T3
      INTS_out(14)=P12*T1+P22*T2+P32*T3
      INTS_out(17)=P13*T1+P23*T2+P33*T3
      T1=INTS_out(12)
      T2=INTS_out(15)
      T3=INTS_out(18)
      INTS_out(12)=P11*T1+P21*T2+P31*T3
      INTS_out(15)=P12*T1+P22*T2+P32*T3
      INTS_out(18)=P13*T1+P23*T2+P33*T3
      T1=INTS_out(19)
      T2=INTS_out(22)
      T3=INTS_out(25)
      INTS_out(19)=P11*T1+P21*T2+P31*T3
      INTS_out(22)=P12*T1+P22*T2+P32*T3
      INTS_out(25)=P13*T1+P23*T2+P33*T3
      T1=INTS_out(20)
      T2=INTS_out(23)
      T3=INTS_out(26)
      INTS_out(20)=P11*T1+P21*T2+P31*T3
      INTS_out(23)=P12*T1+P22*T2+P32*T3
      INTS_out(26)=P13*T1+P23*T2+P33*T3
      T1=INTS_out(21)
      T2=INTS_out(24)
      T3=INTS_out(27)
      INTS_out(21)=P11*T1+P21*T2+P31*T3
      INTS_out(24)=P12*T1+P22*T2+P32*T3
      INTS_out(27)=P13*T1+P23*T2+P33*T3
      T1=INTS_out(1)
      T2=INTS_out(2)
      T3=INTS_out(3)
      INTS_out(1)=P11*T1+P21*T2+P31*T3
      INTS_out(2)=P12*T1+P22*T2+P32*T3
      INTS_out(3)=P13*T1+P23*T2+P33*T3
      T1=INTS_out(4)
      T2=INTS_out(5)
      T3=INTS_out(6)
      INTS_out(4)=P11*T1+P21*T2+P31*T3
      INTS_out(5)=P12*T1+P22*T2+P32*T3
      INTS_out(6)=P13*T1+P23*T2+P33*T3
      T1=INTS_out(7)
      T2=INTS_out(8)
      T3=INTS_out(9)
      INTS_out(7)=P11*T1+P21*T2+P31*T3
      INTS_out(8)=P12*T1+P22*T2+P32*T3
      INTS_out(9)=P13*T1+P23*T2+P33*T3
      T1=INTS_out(10)
      T2=INTS_out(11)
      T3=INTS_out(12)
      INTS_out(10)=P11*T1+P21*T2+P31*T3
      INTS_out(11)=P12*T1+P22*T2+P32*T3
      INTS_out(12)=P13*T1+P23*T2+P33*T3
      T1=INTS_out(13)
      T2=INTS_out(14)
      T3=INTS_out(15)
      INTS_out(13)=P11*T1+P21*T2+P31*T3
      INTS_out(14)=P12*T1+P22*T2+P32*T3
      INTS_out(15)=P13*T1+P23*T2+P33*T3
      T1=INTS_out(16)
      T2=INTS_out(17)
      T3=INTS_out(18)
      INTS_out(16)=P11*T1+P21*T2+P31*T3
      INTS_out(17)=P12*T1+P22*T2+P32*T3
      INTS_out(18)=P13*T1+P23*T2+P33*T3
      T1=INTS_out(19)
      T2=INTS_out(20)
      T3=INTS_out(21)
      INTS_out(19)=P11*T1+P21*T2+P31*T3
      INTS_out(20)=P12*T1+P22*T2+P32*T3
      INTS_out(21)=P13*T1+P23*T2+P33*T3
      T1=INTS_out(22)
      T2=INTS_out(23)
      T3=INTS_out(24)
      INTS_out(22)=P11*T1+P21*T2+P31*T3
      INTS_out(23)=P12*T1+P22*T2+P32*T3
      INTS_out(24)=P13*T1+P23*T2+P33*T3
      T1=INTS_out(25)
      T2=INTS_out(26)
      T3=INTS_out(27)
      INTS_out(25)=P11*T1+P21*T2+P31*T3
      INTS_out(26)=P12*T1+P22*T2+P32*T3
      INTS_out(27)=P13*T1+P23*T2+P33*T3
!
! End of routine R30111
      return
      end subroutine R30111
      subroutine I2E_SHL_SPPP
!***********************************************************************
!     Date last modified: May 24, 2002                     Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version.                                       *
!     Two-electron integral output routine.                            *
!     Note that each block contains integrals of only one type.        *
!***********************************************************************
! Modules:
      USE program_files
      USE QM_objects

      implicit none
!
! Local scalars:
      integer nint,I,J,K,KL,L,NCNT1,NN,NUMINT
      double precision INT1,INT2,INT3
!
! Begin:
!      call PRG_manager ('enter', 'I2E_SHL_SPPP', 'UTILITY')
!
      NCNT1=0
      Iao=AOI+1
! Loop over set of integrals defined by ashls Iashl, Jashl, etc.
      do J=1,Jrange
      do K=1,Krange
      do L=1,Lrange
      NCNT1=NCNT1+1
      NUMINT=0
      IF(DABS(GOUT_ijkl(NCNT1)).GT.I2E_ACC)then
        NUMINT=NUMINT+1
      else
        GOUT_ijkl(NCNT1)=ZERO
      end IF
      IF(DABS(GOUT_ikjl(NCNT1)).GT.I2E_ACC)then
        NUMINT=NUMINT+1
      else
        GOUT_ikjl(NCNT1)=ZERO
      end IF
      IF(DABS(GOUT_iljk(NCNT1)).GT.I2E_ACC)then
        NUMINT=NUMINT+1
      else
        GOUT_iljk(NCNT1)=ZERO
      end IF
      IF(NUMINT.NE.0)then
! Integral over atomic orbitals (IJ/KL).
      Jao=AOJ+J
      Kao=AOK+K
      Lao=AOL+L
!
! The following code determines the integral type
      IF(Iao.NE.Jao.and.Iao.NE.Kao.and.Jao.NE.Kao.and.Kao.NE.Lao.and.Jao.NE.Lao)then
!
      INT1=Gout_ijkl(ncnt1)
      INT2=Gout_ikjl(ncnt1)
      INT3=Gout_iljk(ncnt1)
!
! Pack label.
      NTINT(8)=NTINT(8)+NUMINT
        IJKL_count=IJKL_count+1
        IF(IJKL_count.gt.IJKL_MAX)then
          IJKL_Nbuffers=IJKL_Nbuffers+1
          if(integral_combinations)call I2E_COMB_ijkl (IJKL_INCORE, IJKL_MAX)
          call WRI_ijkl (IJKL_INCORE, IJKL_MAX, IJKL_unit, IJKL_MAX)
          Lijkl_ONDISK=.TRUE.
          ANY_ONDISK=.TRUE.
          IJKL_count=1
        end IF
        IJKL_INCORE(IJKL_count)%I=Iao
        IJKL_INCORE(IJKL_count)%J=Jao
        IJKL_INCORE(IJKL_count)%K=Kao
        IJKL_INCORE(IJKL_count)%L=Lao
        IJKL_INCORE(IJKL_count)%IJKL=INT1
        IJKL_INCORE(IJKL_count)%IKJL=INT2
        IJKL_INCORE(IJKL_count)%ILJK=INT3
!
      else if(Iao.EQ.Jao.and.Iao.NE.Kao.and.Kao.NE.Lao)then
! (II,KL).
      NTINT(1)=NTINT(1)+NUMINT
        IIKL_count=IIKL_count+1
        IF(IIKL_count.gt.IIKL_MAX)then
          IIKL_Nbuffers=IIKL_Nbuffers+1
          if(integral_combinations)call I2E_COMB_iikl (IIKL_INCORE, IIKL_MAX)
          call WRI_iikl (IIKL_INCORE, IIKL_MAX, IIKL_unit, IIKL_MAX)
          Liikl_ONDISK=.TRUE.
          ANY_ONDISK=.TRUE.
          IIKL_count=1
        end IF
        IIKL_INCORE(IIKL_count)%I=Iao
        IIKL_INCORE(IIKL_count)%K=Kao
        IIKL_INCORE(IIKL_count)%L=Lao
        IIKL_INCORE(IIKL_count)%iikl=Gout_ijkl(ncnt1)
        IIKL_INCORE(IIKL_count)%ikil=Gout_ikjl(ncnt1)
!
      else if(Iao.NE.Jao.and.Jao.GT.Kao.and.Kao.EQ.Lao)then
! (IJ,KK) converted to (IK,JK).
      NTINT(2)=NTINT(2)+NUMINT
        IJKJ_count=IJKJ_count+1
        IF(IJKJ_count.gt.IJKJ_MAX)then
          IJKJ_Nbuffers=IJKJ_Nbuffers+1
          if(integral_combinations)call I2E_COMB_ijkj (IJKJ_INCORE, IJKJ_MAX)
          call WRI_ijkj (IJKJ_INCORE,  IJKJ_MAX, IJKJ_unit, IJKJ_MAX)
          Lijkj_ONDISK=.TRUE.
          ANY_ONDISK=.TRUE.
          IJKJ_count=1
        end IF
        IJKJ_INCORE(IJKJ_count)%I=Iao
        IJKJ_INCORE(IJKJ_count)%J=Kao
        IJKJ_INCORE(IJKJ_count)%K=Jao
        IJKJ_INCORE(IJKJ_count)%ijkj=Gout_ikjl(ncnt1)
        IJKJ_INCORE(IJKJ_count)%ikjj=Gout_ijkl(ncnt1)
!
      else if(Iao.NE.Jao.and.Jao.LT.Kao.and.Kao.EQ.Lao)then
! (IJ,KK) converted to (IK,KJ) = (IJ,JL)
      NTINT(3)=NTINT(3)+NUMINT
        IJJL_count=IJJL_count+1
        IF(IJJL_count.gt.IJJL_MAX)then
          IJJL_Nbuffers=IJJL_Nbuffers+1
          if(integral_combinations)call I2E_COMB_ijjl (IJJL_INCORE, IJJL_MAX)
          call WRI_ijjl (IJJL_INCORE,  IJJL_MAX, IJJL_unit, IJJL_MAX)
          Lijjl_ONDISK=.TRUE.
          ANY_ONDISK=.TRUE.
          IJJL_count=1
        end IF
        IJJL_INCORE(IJJL_count)%I=Iao
        IJJL_INCORE(IJJL_count)%J=Kao
        IJJL_INCORE(IJJL_count)%L=Jao
        IJJL_INCORE(IJJL_count)%ijjl=Gout_ikjl(ncnt1)
        IJJL_INCORE(IJJL_count)%iljj=Gout_ijkl(ncnt1)
!
      else if(Iao.NE.Jao.and.Iao.NE.Kao.and.Jao.NE.Kao.and.Kao.NE.Lao)then
! (IJ,KJ).
      NTINT(2)=NTINT(2)+NUMINT
        IJKJ_count=IJKJ_count+1
        IF(IJKJ_count.gt.IJKJ_MAX)then
          IJKJ_Nbuffers=IJKJ_Nbuffers+1
          if(integral_combinations)call I2E_COMB_ijkj (IJKJ_INCORE, IJKJ_MAX)
          call WRI_ijkj (IJKJ_INCORE,  IJKJ_MAX, IJKJ_unit, IJKJ_MAX)
          Lijkj_ONDISK=.TRUE.
          ANY_ONDISK=.TRUE.
          IJKJ_count=1
        end IF
        IJKJ_INCORE(IJKJ_count)%I=Iao
        IJKJ_INCORE(IJKJ_count)%J=Jao
        IJKJ_INCORE(IJKJ_count)%K=Kao
        IJKJ_INCORE(IJKJ_count)%ijkj=Gout_ijkl(ncnt1)
        IJKJ_INCORE(IJKJ_count)%ikjj=Gout_ikjl(ncnt1)
!
      else if(Iao.NE.Jao.and.Iao.EQ.Kao.and.Jao.NE.Lao)then
! (IJ,IL) converted to (II,JK) = (II,KL).
      NTINT(1)=NTINT(1)+NUMINT
        IIKL_count=IIKL_count+1
        IF(IIKL_count.gt.IIKL_MAX)then
          IIKL_Nbuffers=IIKL_Nbuffers+1
          if(integral_combinations)call I2E_COMB_iikl (IIKL_INCORE, IIKL_MAX)
          call WRI_iikl (IIKL_INCORE, IIKL_MAX, IIKL_unit, IIKL_MAX)
          Liikl_ONDISK=.TRUE.
          ANY_ONDISK=.TRUE.
          IIKL_count=1
        end IF
        IIKL_INCORE(IIKL_count)%I=Iao
        IIKL_INCORE(IIKL_count)%K=Jao
        IIKL_INCORE(IIKL_count)%L=Lao
        IIKL_INCORE(IIKL_count)%iikl=Gout_ikjl(ncnt1)
        IIKL_INCORE(IIKL_count)%ikil=Gout_ijkl(ncnt1)
!
      else if(Iao.NE.Jao.and.Jao.EQ.Kao.and.Kao.NE.Lao)then
! (IJ,JL).
      NTINT(3)=NTINT(3)+NUMINT
        IJJL_count=IJJL_count+1
        IF(IJJL_count.gt.IJJL_MAX)then
          IJJL_Nbuffers=IJJL_Nbuffers+1
          if(integral_combinations)call I2E_COMB_ijjl (IJJL_INCORE, IJJL_MAX)
          call WRI_ijjl (IJJL_INCORE, IJJL_MAX, IJJL_unit, IJJL_MAX)
          Lijjl_ONDISK=.TRUE.
          ANY_ONDISK=.TRUE.
          IJJL_count=1
        end IF
        IJJL_INCORE(IJJL_count)%I=Iao
        IJJL_INCORE(IJJL_count)%J=Jao
        IJJL_INCORE(IJJL_count)%L=Lao
        IJJL_INCORE(IJJL_count)%ijjl=Gout_ijkl(ncnt1)
        IJJL_INCORE(IJJL_count)%iljj=Gout_ikjl(ncnt1)
!
      else if(Iao.EQ.Jao.and.Iao.NE.Kao.and.Kao.EQ.Lao)then
! (II,KK).
      NTINT(4)=NTINT(4)+NUMINT
      IIKK_count=IIKK_count+1
      IF(IIKK_count.GT.IIKK_MAX)then
        write(UNIout,*)' error_I2E_SHL_SPPP> too many IIKK integrals'
        stop ' error_I2E_SHL_SPPP> too many IIKK integrals'
      end IF
      IIKK_INCORE(IIKK_count)%I=Iao
      IIKK_INCORE(IIKK_count)%K=Kao
      IIKK_INCORE(IIKK_count)%iikk=Gout_ijkl(ncnt1)
      IIKK_INCORE(IIKK_count)%ikik=Gout_ikjl(ncnt1)
!
      else if(Iao.NE.Jao.and.Iao.EQ.Kao.and.Jao.EQ.Lao)then
!
! (IJ,IJ) converted to (II,KK).
      NTINT(4)=NTINT(4)+NUMINT
      IIKK_count=IIKK_count+1
      IF(IIKK_count.GT.IIKK_MAX)then
        write(UNIout,*)' error_I2E_SHL_SPPP> too many IIKK integrals'
        stop ' error_I2E_SHL_SPPP> too many IIKK integrals'
      end IF
      IIKK_INCORE(IIKK_count)%I=Iao
      IIKK_INCORE(IIKK_count)%K=Jao
      IIKK_INCORE(IIKK_count)%iikk=Gout_ikjl(ncnt1)
      IIKK_INCORE(IIKK_count)%ikik=Gout_ijkl(ncnt1)
!
      else if(Iao.NE.Jao.and.Jao.EQ.Kao.and.Kao.EQ.Lao)then
!
! (IJ,JJ).
      NTINT(5)=NTINT(5)+NUMINT
      IJJJ_count=IJJJ_count+1
      IF(IJJJ_count.GT.IJJJ_MAX)then
        write(UNIout,*)' error_I2E_SHL_SPPP> too many IJJJ integrals'
        stop ' error_I2E_SHL_SPPP> too many IJJJ integrals'
      end IF
      IJJJ_INCORE(IJJJ_count)%I=Iao
      IJJJ_INCORE(IJJJ_count)%J=Jao
      IJJJ_INCORE(IJJJ_count)%ijjj=Gout_ijkl(ncnt1)
!
      else if(Iao.EQ.Jao.and.Iao.EQ.Kao.and.Iao.NE.Lao)then
!
! (II,IL).
      NTINT(6)=NTINT(6)+NUMINT
      IIIL_count=IIIL_count+1
      IF(IIIL_count.GT.IIIL_MAX)then
        write(UNIout,*)' error_I2E_SHL_SPPP> too many IIIL integrals'
        stop
      end IF
      IIIL_INCORE(IIIL_count)%I=Iao
      IIIL_INCORE(IIIL_count)%L=Lao
      IIIL_INCORE(IIIL_count)%iiil=Gout_ijkl(ncnt1)
!
      else if(Iao.EQ.Jao.and.Iao.EQ.Kao.and.Iao.EQ.Lao)then
!
! (II,II).
      NTINT(7)=NTINT(7)+NUMINT
      IIII_count=IIII_count+1
      IF(IIII_count.GT.IIII_MAX)then
        write(UNIout,*)' error_I2E_SHL_SPPP> too many IIII integrals'
        stop ' error_I2E_SHL_SPPP> too many IIII integrals'
      end IF
      IIII_INCORE(IIII_count)%I=Iao
      IIII_INCORE(IIII_count)%iiii=Gout_ijkl(ncnt1)
!
      else
        stop 'I2E_SHL_SPPP: illegal integral type found'
      end IF
      end IF ! NUMINT.NE.0
      end do ! L
      end do ! K
      end do ! J
!
! End of routine I2E_SHL_SPPP
!      call PRG_manager ('exit', 'I2E_SHL_SPPP', 'UTILITY')
      return
      end subroutine I2E_SHL_SPPP
      end subroutine ISP_SPPP
      subroutine ISP_PPPP
!***********************************************************************
!     Date last modified: May 24, 2002                     Version 1.0 *
!     Author: R. A. Poirier                                            *
!     Description: Special fast routine for SSSS integrals.            *
!***********************************************************************
! Modules:

      implicit none
!
! Local scalars:
      integer I,IGOUT
      logical DOZERO
!
! Local arrays:
      double precision, dimension(:), pointer :: INT_out => null()
!
! Begin:
      Irange=3
      Jrange=3
      Krange=3
      Lrange=3
      IGOUT=81
      include 'mungauss_do_IJKL'
!
      GOUT_ijkl(1:81)=ZERO
      GOUT_ikjl(1:81)=ZERO
      GOUT_iljk(1:81)=ZERO
! Set 1
      Iashl=Iatmshl
      Jashl=Jatmshl
      Kashl=Katmshl
      Lashl=Latmshl
      call SET_shells (Ishell, Jshell, Kshell, Lshell)
      DOZERO=.false.
      IF(Kashl.EQ.Lashl)DOZERO=.TRUE.
      INT_out => GOUT(1:Igout)
      call SP1111 (INT_out, DOZERO)
!
! Set 2
      if(Mtype.eq.3)then
        Iashl=Iatmshl
        Jashl=Latmshl
        Kashl=Jatmshl
        Lashl=Katmshl
        call SET_shells (Ishell, Lshell, Jshell, Kshell)
        DOZERO=.false.
        IF(Kashl.EQ.Lashl)DOZERO=.TRUE.
        INT_out => GOUT(Igout+1:2*Igout)
        call SP1111 (INT_out, DOZERO)
      else if(Mtype.lt.6)then
        Iashl=Iatmshl
        Jashl=Katmshl
        Kashl=Jatmshl
        Lashl=Latmshl
        call SET_shells (Ishell, Kshell, Jshell, Lshell)
        DOZERO=.false.
        IF(Kashl.EQ.Lashl)DOZERO=.TRUE.
        INT_out => GOUT(Igout+1:2*Igout)
        call SP1111 (INT_out, DOZERO)
      end if
!
! Set 3
      if(Mtype.eq.1)then
        Iashl=Iatmshl
        Jashl=Latmshl
        Kashl=Jatmshl
        Lashl=Katmshl
        call SET_shells (Ishell, Lshell, Jshell, Kshell)
        DOZERO=.false.
        IF(Kashl.EQ.Lashl)DOZERO=.TRUE.
        INT_out => GOUT(2*Igout+1:3*Igout)
        call SP1111 (INT_out, DOZERO)
      end if

      IF(Mtype.EQ.1)then
        call SORT1N (Igout)
        else if(Mtype.EQ.2)then
        call SORT2LN (Igout)
        else if(Mtype.EQ.3)then
        call SORT3N (Igout)
        else if(Mtype.EQ.6)then
        call SORT4N (Igout)
        else if(Mtype.EQ.5)then
        call SORT5N
        else if(Mtype.EQ.4)then
        call SORT6N
        else if(Mtype.EQ.7)then
        call SORT7N (GOUT(1), GOUT(82), GOUT(163))
        else
        call SORT8N
      end IF ! Mtype
!
      if(Mtype.eq.1)then
        call I2E_SHL_ABCD
      else
        call I2E_SHL_PPPP
      end if
      include 'mungauss_endo_IJKL'
!
! End of routine ISP_PPPP
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine SP1111 (INTS_out, &
                         DOZERO)
!***********************************************************************
!     Date last modified: May 24, 2002                                 *
!     Author:                                                          *
!     Description: Special fast routine for P loop for 1111.           *
!***********************************************************************
! Modules:

      implicit none
!
! Input logical:
      logical DOZERO
!
! Output array:
      double precision INTS_out(81)
!
! Local scalars:
      integer I,IND,ISML,ISMLP,ISMLQ,J,K,L,N
      double precision AS,EPAB,BS,EPABB,APP,BPP,CONP,DP11P
      double precision APBP,AQX,AQZ,AUXVAR,A1,A2,A3,A4,A5,A6,A8,A9,A10,BQZ,B1,B2,B3,B4,B5,B6,B8,B9,B10, &
                       COSP,COSP2,COSSIN,CQ,C1,C2,C3,C4,C5,C6,DQ,EPABI,EAB2,EQCDI,ECD2,F0,F1, &
                       F1PQAB,F1PQA2,F2,F2PQAB,F2PQA2,F2PQA3,F3,F3PQAB,F3PQA2,F3PQA3,F4,F4PQAB,F4PQA2,F4PQA3,CS,EQCD,DS, &
                       GGGY,GGY,GTX,GY,G0,HECD,HECD2,HQECD,HQECD2,HXXYY,MCP2P1,MTCPSP,PQAB,PQAB2, &
                       QECD,QECD2,QPERP2,QPERP,QQ,Q2ECD,Q3ECD,Q2ECD2,Q3ECD2, &
                       S1,S10,S11,S12,S13,S14,S2,S3,S4,S6,S7,S8,S9,TCP2M1,TEMP,THETA,THETA2,THETA3,THETA4, &
                       T2,T3,T4,T5,T6,T7,T8,T9,T10,T11,T12,T13,T14,TERM,Yterm
      double precision Expij,Expkl,EXPARG,ExpCCij,ExpCCkl
!
! Local arrays:
      double precision Gijkl(256),Hkl(87)
!
! Begin:
      Gijkl(1:256)=ZERO
      Hkl(1:87)=ZERO
!
! Begin KL(CD) loop (Q).
      do Kgauss=KGBGN,KGend
      CS=Basis%gaussian(Kgauss)%exp
      do Lgauss=LGBGN,LGend
      DS=Basis%gaussian(Lgauss)%exp
      EQCD=CS+DS
      EQCDI=ONE/EQCD
      CQ=DS*EQCDI*RCD
      DQ=CQ-RCD
      EXPARG=CQ*DQ*EQCD
!     IF(EXPARG.GE.-I2E_expcut)then
      if(EXPARG.le.I2E_expcut)then
      Expkl=DEXP(EXPARG)*EQCDI
      ExpCCkl=Expkl*Basis%gaussian(Kgauss)%CONTRC*Basis%gaussian(Lgauss)%CONTRC
      IF(dabs(ExpCCkl).GT.ERROR1)then
        ISMLQ=0
      else
         ISMLQ=1
      end IF
      IF(dabs(ExpCCkl).GT.ERROR2)then
! Find coordinates of Q relative to axes at A.
! QPERP is perpendicular from Q to AB.
      AQX=ACX+SING*CQ
      AQZ=ACZ+COSG*CQ
      QPERP2=AQX*AQX+ACY2
      QPERP=DSQRT(QPERP2)
! PHI is 180 - Azimuthal angle for AQ in AB local axis system.
      IF(QPERP.GT.TENM12)then
        COSP=-AQX/QPERP
        SINP=-ACY/QPERP
      else
        COSP=ONE
        SINP=ZERO
      end IF

      S1=ZERO
      S2=ZERO
      S3=ZERO
      S4=ZERO
      S6=ZERO
      S7=ZERO
      S8=ZERO
      S9=ZERO
      S10=ZERO
      S11=ZERO
      S12=ZERO
      S13=ZERO
      S14=ZERO
!
      T2=ZERO
      T3=ZERO
      T4=ZERO
      T5=ZERO
      T6=ZERO
      T7=ZERO
      T8=ZERO
      T9=ZERO
      T10=ZERO
      T11=ZERO
      T12=ZERO
      T13=ZERO
      T14=ZERO

      C1=ZERO
      C2=ZERO
      C3=ZERO
      C4=ZERO
      C5=ZERO
      C6=ZERO
      do Igauss=IGBGN,IGend
      AS=Basis%gaussian(Igauss)%exp
      do Jgauss=JGBGN,JGend
      BS=Basis%gaussian(Jgauss)%exp
      EPAB=AS+BS
      EPABI=ONE/EPAB
      EPABB=BS*EPABI
      APP=EPABB*RAB
      BPP=APP-RAB
      EXPARG=AS*EPABB*RABSQ
      IF(EXPARG.GT.I2E_expcut)then
        ISMLP=2
        CONP=ZERO
        DP11P=ZERO
      else
        Expij=DEXP(-EXPARG)*EPABI
        ExpCCij=Expij*Basis%gaussian(Igauss)%CONTRC*Basis%gaussian(Jgauss)%CONTRC
        IF(dabs(ExpCCij).GT.ERROR1)then
          ISMLP=0
        else
          IF(dabs(ExpCCij).GT.ERROR2)then
            ISMLP=1
          else
            ISMLP=2
          end IF
        end IF
! For types 0000,0001,0011 only needed.
        DP11P=THRTY4*ExpCCij
        CONP=DP11P
      end IF
      ISML=ISMLQ+ISMLP
      IF(ISML.LT.2)then
      IF(ISML.LT.1)then
        AUXVAR=VAR1
      else
        AUXVAR=VAR2
      end IF
      EPABI=EPABI
      AP=APP
      BP=BPP
      PQAB=AQZ-AP
      PQAB2=PQAB*PQAB
      G0=ONE/(EPABI+EQCDI)
      TERM=G0*(QPERP2+PQAB2)
      IF(TERM.GT.AUXVAR)then
        F0=DSQRT(PT7853/(TERM*(EPAB+EQCD)))*CONP
        GTX=G0/TERM
        F1=PT5*F0*GTX
        F2=ONEPT5*F1*GTX
        F3=TWOPT5*F2*GTX
        F4=THRPT5*F3*GTX
      else
        Yterm=CONP/DSQRT(EPAB+EQCD)
        GY=G0*Yterm
        GGY=G0*GY
        GGGY=G0*GGY
        QQ=TERM*TWENTY
        N=INT(QQ)
        THETA=QQ-N
        THETA2=THETA*(THETA-ONE)
        THETA3=THETA2*(THETA-TWO)
        THETA4=THETA2*(THETA+ONE)
        F0=(FMT_val(N+1)%FM0+THETA*FMT_del(N+1)%FM0-THETA3*FMT_int(N+1)%FM0+THETA4*FMT_int(N+2)%FM0)*Yterm
        F1=(FMT_val(N+1)%FM1+THETA*FMT_del(N+1)%FM1-THETA3*FMT_int(N+1)%FM1+THETA4*FMT_int(N+2)%FM1)*GY
        F2=(FMT_val(N+1)%FM2+THETA*FMT_del(N+1)%FM2-THETA3*FMT_int(N+1)%FM2+THETA4*FMT_int(N+2)%FM2)*GGY
        F3=(FMT_val(N+1)%FM3+THETA*FMT_del(N+1)%FM3-THETA3*FMT_int(N+1)%FM3+THETA4*FMT_int(N+2)%FM3)*GGGY
        F4=(FMT_val(N+1)%FM4+THETA*FMT_del(N+1)%FM4-THETA3*FMT_int(N+1)%FM4+THETA4*FMT_int(N+2)%FM4)*GGGY*G0
      end IF
      APBP=AP*BP
      EAB2=EPABI*EPABI
      F1PQAB=F1*PQAB
      F2PQAB=F2*PQAB
      F3PQAB=F3*PQAB
      F4PQAB=F4*PQAB
      F1PQA2=F1*PQAB2
      F2PQA2=F2*PQAB2
      F3PQA2=F3*PQAB2
      F4PQA2=F4*PQAB2
      F2PQA3=F2PQA2*PQAB
      F3PQA3=F3PQA2*PQAB
      F4PQA3=F4PQA2*PQAB

      S1=S1+F0*EPABI
      S2=S2+F1*EPABI
      S3=S3+F2*EPABI
      S4=S4+F3*EPABI
      S6=S6+F1PQAB*EPABI
      S7=S7+F2PQAB*EPABI
      S8=S8+F3PQAB*EPABI
      S9=S9+F1PQA2*EPABI
      S10=S10+F2PQA2*EPABI
      S11=S11+F3PQA2*EPABI
      S12=S12+F2PQA3*EPABI
      S13=S13+F3PQA3*EPABI
      S14=S14+F3PQA3*PQAB*EPABI

      T2=T2+F1*EAB2
      T3=T3+F2*EAB2
      T4=T4+F3*EAB2
      T5=T5+F4*EAB2
      T6=T6+F2PQAB*EAB2
      T7=T7+F3PQAB*EAB2
      T8=T8+F4PQAB*EAB2
      T9=T9+F2PQA2*EAB2
      T10=T10+F3PQA2*EAB2
      T11=T11+F4PQA2*EAB2
      T12=T12+F3PQA3*EAB2
      T13=T13+F4PQA3*EAB2
      T14=T14+F4PQA3*PQAB*EAB2
      IF(RABSQ.NE.ZERO)then
        C1=C1+F0*APBP
        C2=C2+F1*APBP
        C3=C3+F2*APBP
        C4=C4+F1PQAB*APBP
        C5=C5+F2PQAB*APBP
        C6=C6+F2PQA2*APBP
      end IF
      end IF !ISML.LT.2
      end do ! Jgauss
      end do ! Igauss

      A1=AQZ*S2-S6
      A2=AQZ*S3-S7
      A3=AQZ*S4-S8
      A4=AQZ*S6-S9
      A5=AQZ*S7-S10
      A6=AQZ*S8-S11
      A8=AQZ*S10-S12
      A9=AQZ*S11-S13
      A10=AQZ*S13-S14
      BQZ=AQZ-RAB
      B1=BQZ*S2-S6
      B2=BQZ*S3-S7
      B3=BQZ*S4-S8
      B4=BQZ*S6-S9
      B5=BQZ*S7-S10
      B6=BQZ*S8-S11
      B8=BQZ*S10-S12
      B9=BQZ*S11-S13
      B10=BQZ*S13-S14
      HECD=PT5*EQCDI
      ECD2=EQCDI*EQCDI
      HECD2=PT5*ECD2
      QECD=QPERP*EQCDI
      HQECD=PT5*QECD
      QECD2=QPERP*ECD2
      HQECD2=PT5*QECD2
      Q2ECD=QPERP2*EQCDI
      Q3ECD=QPERP*Q2ECD
      Q2ECD2=QPERP2*ECD2
      Q3ECD2=Q2ECD2*QPERP
      Hkl(41)=PT5*(S1-T2)
      Hkl(1)=Hkl(41)+QPERP2*T3
      Hkl(21)=-QPERP*(T6+B1)
      Hkl(61)=-QPERP*(T6+A1)
      Hkl(81)=Hkl(41)+T9+A4+B4+C1
      Hkl(42)=HQECD*(S2-T3)
      Hkl(2)=Hkl(42)-QECD*T3+Q3ECD*T4
      TEMP =HECD*T6-Q2ECD*T7
      Hkl(22)=TEMP+HECD*B1-Q2ECD*B2
      Hkl(62)=TEMP+HECD*A1-Q2ECD*A2
      Hkl(82)=Hkl(42)+QECD*(T10+A5+B5+C2)
      Hkl(13)=-HQECD*T3
      Hkl(33)=Hkl(13)
      Hkl(53)=HECD*(T6+B1)
      Hkl(73)=HECD*(T6+A1)
      Hkl(44)=HECD*(T6-S6)
      Hkl(4)=Hkl(44)-Q2ECD*T7
      TEMP =-HQECD*T3+QECD*T10
      Hkl(24)=TEMP+QECD*B5
      Hkl(64)=TEMP+QECD*A5
      Hkl(83)=Hkl(44)+EQCDI*(T6-T12-A8-B8-C4)+HECD*(A1+B1)
      Hkl(16)=PT25*ECD2*T3-PT5*Q2ECD2*T4
      Hkl(36)=Hkl(16)
      Hkl(56)=HQECD2*(T7+B2)
      Hkl(76)=HQECD2*(T7+A2)
      Hkl(19)=HQECD2*T7
      Hkl(39)=Hkl(19)
      Hkl(59)=HECD2*(PT5*T3-T10-B5)
      Hkl(79)=HECD2*(PT5*T3-T10-A5)
      HXXYY=PT25*(EQCDI*(S1-T2)-ECD2*(S2-T3))
      Hkl(48)=HXXYY+HECD2*T3
      Hkl(8)=HXXYY+PT5*(Q2ECD*T3-Q2ECD2*T4)
      TEMP =HQECD*(EQCDI*T7-T6)
      Hkl(28)=TEMP+HQECD*(EQCDI*B2-B1)
      Hkl(68)=TEMP+HQECD*(EQCDI*A2-A1)
      Hkl(86)=HXXYY+HECD*(T9+A4+B4+C1)-HECD2*(T10+A5+B5+C2)
      Hkl(45)=HXXYY+PT5*Q2ECD2*(S3-T4)
      Hkl(5)=HXXYY+(HECD2+PT5*Q2ECD)*T3+Q2ECD2*(-THREE*T4+PT5*S3+QPERP2*T5)
      Hkl(25)=ONEPT5*QECD2*(T7+B2)-HQECD*(T6+B1)-Q3ECD2*(B3+T8)
      Hkl(65)=ONEPT5*QECD2*(T7+A2)-HQECD*(T6+A1)-Q3ECD2*(A3+T8)
      Hkl(84)=HXXYY-HECD2*(QPERP2*T4+T10+A5+B5)+HECD*(T9+A4+B4+C1-EQCDI*C2)+Q2ECD2*(T11+PT5*S3+A6+B6+C3)
      Hkl(47)=HQECD2*(T7-S7)
      Hkl(7)=ONEPT5*QECD2*T7-HQECD2*S7-Q3ECD2*T8
      TEMP =HECD2*(PT5*T3-T10)+Q2ECD2*(T11-PT5*T4)
      Hkl(27)=TEMP-HECD2*B5+Q2ECD2*B6
      Hkl(67)=TEMP-HECD2*A5+Q2ECD2*A6
      Hkl(85)=QECD2*(ONEPT5*T7-T13-A9-B9-C5)-HQECD2*(S7-A2-B2)
      Hkl(50)=HXXYY+HECD2*(S10-T10)
      Hkl(10)=HXXYY-HECD2*(QPERP2*T4+T10-S10)+PT5*Q2ECD*T3+Q2ECD2*T11
      Hkl(30)=QECD2*(ONEPT5*T7-T13-B9)-HQECD*(T6+B1)+HQECD2*B2
      Hkl(70)=QECD2*(ONEPT5*T7-T13-A9)-HQECD*(T6+A1)+HQECD2*A2
      Hkl(87)=HXXYY+HECD2*(-THREE*(A5+B5)+T3+S10-C2)+ECD2*(-THREE*T10+T14+A10+B10+C6)+HECD*(T9+A4+B4+C1)
!
! No rotation if ABCD coplanar.
      IF(SINP.EQ.ZERO.and.COSP.GT.ZERO)then
        Gijkl(81)=Hkl(1)
        Gijkl(82)=Hkl(2)
        Gijkl(83)=ZERO
        Gijkl(84)=Hkl(4)
        Gijkl(86)=Hkl(5)
        Gijkl(87)=ZERO
        Gijkl(88)=Hkl(7)
        Gijkl(91)=Hkl(8)
        Gijkl(92)=ZERO
        Gijkl(96)=Hkl(10)
        Gijkl(97)=ZERO
        Gijkl(98)=ZERO
        Gijkl(99)=Hkl(13)
        Gijkl(100)=ZERO
        Gijkl(102)=ZERO
        Gijkl(103)=Hkl(16)
        Gijkl(104)=ZERO
        Gijkl(107)=ZERO
        Gijkl(108)=Hkl(19)
        Gijkl(112)=ZERO
        Gijkl(113)=Hkl(21)
        Gijkl(114)=Hkl(22)
        Gijkl(115)=ZERO
        Gijkl(116)=Hkl(24)
        Gijkl(118)=Hkl(25)
        Gijkl(119)=ZERO
        Gijkl(120)=Hkl(27)
        Gijkl(123)=Hkl(28)
        Gijkl(124)=ZERO
        Gijkl(128)=Hkl(30)
        Gijkl(145)=ZERO
        Gijkl(146)=ZERO
        Gijkl(147)=Hkl(33)
        Gijkl(148)=ZERO
        Gijkl(150)=ZERO
        Gijkl(151)=Hkl(36)
        Gijkl(152)=ZERO
        Gijkl(155)=ZERO
        Gijkl(156)=Hkl(39)
        Gijkl(160)=ZERO
        Gijkl(161)=Hkl(41)
        Gijkl(162)=Hkl(42)
        Gijkl(163)=ZERO
        Gijkl(164)=Hkl(44)
        Gijkl(166)=Hkl(45)
        Gijkl(167)=ZERO
        Gijkl(168)=Hkl(47)
        Gijkl(171)=Hkl(48)
        Gijkl(172)=ZERO
        Gijkl(176)=Hkl(50)
        Gijkl(177)=ZERO
        Gijkl(178)=ZERO
        Gijkl(179)=Hkl(53)
        Gijkl(180)=ZERO
        Gijkl(182)=ZERO
        Gijkl(183)=Hkl(56)
        Gijkl(184)=ZERO
        Gijkl(187)=ZERO
        Gijkl(188)=Hkl(59)
        Gijkl(192)=ZERO
        Gijkl(209)=Hkl(61)
        Gijkl(210)=Hkl(62)
        Gijkl(211)=ZERO
        Gijkl(212)=Hkl(64)
        Gijkl(214)=Hkl(65)
        Gijkl(215)=ZERO
        Gijkl(216)=Hkl(67)
        Gijkl(219)=Hkl(68)
        Gijkl(220)=ZERO
        Gijkl(224)=Hkl(70)
        Gijkl(225)=ZERO
        Gijkl(226)=ZERO
        Gijkl(227)=Hkl(73)
        Gijkl(228)=ZERO
        Gijkl(230)=ZERO
        Gijkl(231)=Hkl(76)
        Gijkl(232)=ZERO
        Gijkl(235)=ZERO
        Gijkl(236)=Hkl(79)
        Gijkl(240)=ZERO
        Gijkl(241)=Hkl(81)
        Gijkl(242)=Hkl(82)
        Gijkl(243)=ZERO
        Gijkl(244)=Hkl(83)
        Gijkl(246)=Hkl(84)
        Gijkl(247)=ZERO
        Gijkl(248)=Hkl(85)
        Gijkl(251)=Hkl(86)
        Gijkl(252)=ZERO
        Gijkl(256)=Hkl(87)
      else if(SINP.EQ.ZERO.and.COSP.LT.ZERO)then
        Gijkl(81)=Hkl(1)
        Gijkl(82)=-Hkl(2)
        Gijkl(83)=ZERO
        Gijkl(84)=Hkl(4)
        Gijkl(86)=Hkl(5)
        Gijkl(87)=ZERO
        Gijkl(88)=-Hkl(7)
        Gijkl(91)=Hkl(8)
        Gijkl(92)=ZERO
        Gijkl(96)=Hkl(10)
        Gijkl(97)=ZERO
        Gijkl(98)=ZERO
        Gijkl(99)=-Hkl(13)
        Gijkl(100)=ZERO
        Gijkl(102)=ZERO
        Gijkl(103)=Hkl(16)
        Gijkl(104)=ZERO
        Gijkl(107)=ZERO
        Gijkl(108)=-Hkl(19)
        Gijkl(112)=ZERO
        Gijkl(113)=-Hkl(21)
        Gijkl(114)=Hkl(22)
        Gijkl(115)=ZERO
        Gijkl(116)=-Hkl(24)
        Gijkl(118)=-Hkl(25)
        Gijkl(119)=ZERO
        Gijkl(120)=Hkl(27)
        Gijkl(123)=-Hkl(28)
        Gijkl(124)=ZERO
        Gijkl(128)=-Hkl(30)
        Gijkl(145)=ZERO
        Gijkl(146)=ZERO
        Gijkl(147)=-Hkl(33)
        Gijkl(148)=ZERO
        Gijkl(150)=ZERO
        Gijkl(151)=Hkl(36)
        Gijkl(152)=ZERO
        Gijkl(155)=ZERO
        Gijkl(156)=-Hkl(39)
        Gijkl(160)=ZERO
        Gijkl(161)=Hkl(41)
        Gijkl(162)=-Hkl(42)
        Gijkl(163)=ZERO
        Gijkl(164)=Hkl(44)
        Gijkl(166)=Hkl(45)
        Gijkl(167)=ZERO
        Gijkl(168)=-Hkl(47)
        Gijkl(171)=Hkl(48)
        Gijkl(172)=ZERO
        Gijkl(176)=Hkl(50)
        Gijkl(177)=ZERO
        Gijkl(178)=ZERO
        Gijkl(179)=Hkl(53)
        Gijkl(180)=ZERO
        Gijkl(182)=ZERO
        Gijkl(183)=-Hkl(56)
        Gijkl(184)=ZERO
        Gijkl(187)=ZERO
        Gijkl(188)=Hkl(59)
        Gijkl(192)=ZERO
        Gijkl(209)=-Hkl(61)
        Gijkl(210)=Hkl(62)
        Gijkl(211)=ZERO
        Gijkl(212)=-Hkl(64)
        Gijkl(214)=-Hkl(65)
        Gijkl(215)=ZERO
        Gijkl(216)=Hkl(67)
        Gijkl(219)=-Hkl(68)
        Gijkl(220)=ZERO
        Gijkl(224)=-Hkl(70)
        Gijkl(225)=ZERO
        Gijkl(226)=ZERO
        Gijkl(227)=Hkl(73)
        Gijkl(228)=ZERO
        Gijkl(230)=ZERO
        Gijkl(231)=-Hkl(76)
        Gijkl(232)=ZERO
        Gijkl(235)=ZERO
        Gijkl(236)=Hkl(79)
        Gijkl(240)=ZERO
        Gijkl(241)=Hkl(81)
        Gijkl(242)=-Hkl(82)
        Gijkl(243)=ZERO
        Gijkl(244)=Hkl(83)
        Gijkl(246)=Hkl(84)
        Gijkl(247)=ZERO
        Gijkl(248)=-Hkl(85)
        Gijkl(251)=Hkl(86)
        Gijkl(252)=ZERO
        Gijkl(256)=Hkl(87)
      else
        COSP2=COSP*COSP
        MCP2P1=ONE-COSP2
        COSSIN=COSP*SINP
        MTCPSP=-COSSIN-COSSIN
        TCP2M1=COSP2-MCP2P1
!
        Gijkl(86)=COSP2*Hkl(5)+MCP2P1*Hkl(8)
        Gijkl(87)=COSSIN*Hkl(5)-COSSIN*Hkl(8)
        Gijkl(91)=MCP2P1*Hkl(5)+COSP2*Hkl(8)
        Gijkl(88)=COSP*Hkl(7)
        Gijkl(92)=SINP*Hkl(7)
        Gijkl(96)=Hkl(10)
        Gijkl(102)=MTCPSP*Hkl(16)
        Gijkl(103)=TCP2M1*Hkl(16)
        Gijkl(107)=-Gijkl(102)
        Gijkl(104)=-SINP*Hkl(19)
        Gijkl(108)=COSP*Hkl(19)
        Gijkl(112)=ZERO
        Gijkl(118)=COSP2*Hkl(25)+MCP2P1*Hkl(28)
        Gijkl(119)=COSSIN*Hkl(25)-COSSIN*Hkl(28)
        Gijkl(123)=MCP2P1*Hkl(25)+COSP2*Hkl(28)
        Gijkl(120)=COSP*Hkl(27)
        Gijkl(124)=SINP*Hkl(27)
        Gijkl(128)=Hkl(30)
        Gijkl(150)=MTCPSP*Hkl(36)
        Gijkl(151)=TCP2M1*Hkl(36)
        Gijkl(155)=-Gijkl(150)
        Gijkl(152)=-SINP*Hkl(39)
        Gijkl(156)=COSP*Hkl(39)
        Gijkl(160)=ZERO
        Gijkl(166)=COSP2*Hkl(45)+MCP2P1*Hkl(48)
        Gijkl(167)=COSSIN*Hkl(45)-COSSIN*Hkl(48)
        Gijkl(171)=MCP2P1*Hkl(45)+COSP2*Hkl(48)
        Gijkl(168)=COSP*Hkl(47)
        Gijkl(172)=SINP*Hkl(47)
        Gijkl(176)=Hkl(50)
        Gijkl(182)=MTCPSP*Hkl(56)
        Gijkl(183)=TCP2M1*Hkl(56)
        Gijkl(187)=-Gijkl(182)
        Gijkl(184)=-SINP*Hkl(59)
        Gijkl(188)=COSP*Hkl(59)
        Gijkl(192)=ZERO
        Gijkl(214)=COSP2*Hkl(65)+MCP2P1*Hkl(68)
        Gijkl(215)=COSSIN*Hkl(65)-COSSIN*Hkl(68)
        Gijkl(219)=MCP2P1*Hkl(65)+COSP2*Hkl(68)
        Gijkl(216)=COSP*Hkl(67)
        Gijkl(220)=SINP*Hkl(67)
        Gijkl(224)=Hkl(70)
        Gijkl(230)=MTCPSP*Hkl(76)
        Gijkl(231)=TCP2M1*Hkl(76)
        Gijkl(235)=-Gijkl(230)
        Gijkl(232)=-SINP*Hkl(79)
        Gijkl(236)=COSP*Hkl(79)
        Gijkl(240)=ZERO
        Gijkl(246)=COSP2*Hkl(84)+MCP2P1*Hkl(86)
        Gijkl(247)=COSSIN*Hkl(84)-COSSIN*Hkl(86)
        Gijkl(251)=MCP2P1*Hkl(84)+COSP2*Hkl(86)
        Gijkl(248)=COSP*Hkl(85)
        Gijkl(252)=SINP*Hkl(85)
        Gijkl(256)=Hkl(87)
        Gijkl(82)=COSP*Hkl(2)
        Gijkl(83)=SINP*Hkl(2)
        Gijkl(84)=Hkl(4)
        Gijkl(98)=-SINP*Hkl(13)
        Gijkl(99)=COSP*Hkl(13)
        Gijkl(100)=ZERO
        Gijkl(114)=COSP*Hkl(22)
        Gijkl(115)=SINP*Hkl(22)
        Gijkl(116)=Hkl(24)
        Gijkl(146)=-SINP*Hkl(33)
        Gijkl(147)=COSP*Hkl(33)
        Gijkl(148)=ZERO
        Gijkl(162)=COSP*Hkl(42)
        Gijkl(163)=SINP*Hkl(42)
        Gijkl(164)=Hkl(44)
        Gijkl(178)=-SINP*Hkl(53)
        Gijkl(179)=COSP*Hkl(53)
        Gijkl(180)=ZERO
        Gijkl(210)=COSP*Hkl(62)
        Gijkl(211)=SINP*Hkl(62)
        Gijkl(212)=Hkl(64)
        Gijkl(226)=-SINP*Hkl(73)
        Gijkl(227)=COSP*Hkl(73)
        Gijkl(228)=ZERO
        Gijkl(242)=COSP*Hkl(82)
        Gijkl(243)=SINP*Hkl(82)
        Gijkl(244)=Hkl(83)
        Gijkl(81)=Hkl(1)
        Gijkl(97)=ZERO
        Gijkl(113)=Hkl(21)
        Gijkl(145)=ZERO
        Gijkl(161)=Hkl(41)
        Gijkl(177)=ZERO
        Gijkl(209)=Hkl(61)
        Gijkl(225)=ZERO
        Gijkl(241)=Hkl(81)
!
        Hkl(5)=COSP*Gijkl(86)-SINP*Gijkl(150)
        Hkl(6)=COSP*Gijkl(87)-SINP*Gijkl(151)
        Hkl(7)=COSP*Gijkl(88)-SINP*Gijkl(152)
        Hkl(8)=COSP*Gijkl(91)-SINP*Gijkl(155)
        Hkl(9)=COSP*Gijkl(92)-SINP*Gijkl(156)
        Hkl(10)=COSP*Gijkl(96)-SINP*Gijkl(160)
        Hkl(15)=COSP*Gijkl(102)-SINP*Gijkl(166)
        Hkl(16)=COSP*Gijkl(103)-SINP*Gijkl(167)
        Hkl(17)=COSP*Gijkl(104)-SINP*Gijkl(168)
        Hkl(18)=COSP*Gijkl(107)-SINP*Gijkl(171)
        Hkl(19)=COSP*Gijkl(108)-SINP*Gijkl(172)
        Hkl(20)=COSP*Gijkl(112)-SINP*Gijkl(176)
        Hkl(25)=COSP*Gijkl(118)-SINP*Gijkl(182)
        Hkl(26)=COSP*Gijkl(119)-SINP*Gijkl(183)
        Hkl(27)=COSP*Gijkl(120)-SINP*Gijkl(184)
        Hkl(28)=COSP*Gijkl(123)-SINP*Gijkl(187)
        Hkl(29)=COSP*Gijkl(124)-SINP*Gijkl(188)
        Hkl(30)=COSP*Gijkl(128)-SINP*Gijkl(192)
        Hkl(35)=SINP*Gijkl(86)+COSP*Gijkl(150)
        Hkl(36)=SINP*Gijkl(87)+COSP*Gijkl(151)
        Hkl(37)=SINP*Gijkl(88)+COSP*Gijkl(152)
        Hkl(38)=SINP*Gijkl(91)+COSP*Gijkl(155)
        Hkl(39)=SINP*Gijkl(92)+COSP*Gijkl(156)
        Hkl(40)=SINP*Gijkl(96)+COSP*Gijkl(160)
        Hkl(45)=SINP*Gijkl(102)+COSP*Gijkl(166)
        Hkl(46)=SINP*Gijkl(103)+COSP*Gijkl(167)
        Hkl(47)=SINP*Gijkl(104)+COSP*Gijkl(168)
        Hkl(48)=SINP*Gijkl(107)+COSP*Gijkl(171)
        Hkl(49)=SINP*Gijkl(108)+COSP*Gijkl(172)
        Hkl(50)=SINP*Gijkl(112)+COSP*Gijkl(176)
        Hkl(55)=SINP*Gijkl(118)+COSP*Gijkl(182)
        Hkl(56)=SINP*Gijkl(119)+COSP*Gijkl(183)
        Hkl(57)=SINP*Gijkl(120)+COSP*Gijkl(184)
        Hkl(58)=SINP*Gijkl(123)+COSP*Gijkl(187)
        Hkl(59)=SINP*Gijkl(124)+COSP*Gijkl(188)
        Hkl(60)=SINP*Gijkl(128)+COSP*Gijkl(192)
        Hkl(65)=Gijkl(214)
        Hkl(66)=Gijkl(215)
        Hkl(67)=Gijkl(216)
        Hkl(68)=Gijkl(219)
        Hkl(69)=Gijkl(220)
        Hkl(75)=Gijkl(230)
        Hkl(76)=Gijkl(231)
        Hkl(77)=Gijkl(232)
        Hkl(78)=Gijkl(235)
        Hkl(79)=Gijkl(236)
        Hkl(80)=Gijkl(240)
        Hkl(84)=Gijkl(246)
        Hkl(85)=Gijkl(248)
        Hkl(86)=Gijkl(251)
        Hkl(2)=COSP*Gijkl(82)-SINP*Gijkl(146)
        Hkl(3)=COSP*Gijkl(83)-SINP*Gijkl(147)
        Hkl(4)=COSP*Gijkl(84)-SINP*Gijkl(148)
        Hkl(12)=COSP*Gijkl(98)-SINP*Gijkl(162)
        Hkl(13)=COSP*Gijkl(99)-SINP*Gijkl(163)
        Hkl(14)=COSP*Gijkl(100)-SINP*Gijkl(164)
        Hkl(22)=COSP*Gijkl(114)-SINP*Gijkl(178)
        Hkl(23)=COSP*Gijkl(115)-SINP*Gijkl(179)
        Hkl(24)=COSP*Gijkl(116)-SINP*Gijkl(180)
        Hkl(32)=SINP*Gijkl(82)+COSP*Gijkl(146)
        Hkl(33)=SINP*Gijkl(83)+COSP*Gijkl(147)
        Hkl(34)=SINP*Gijkl(84)+COSP*Gijkl(148)
        Hkl(42)=SINP*Gijkl(98)+COSP*Gijkl(162)
        Hkl(43)=SINP*Gijkl(99)+COSP*Gijkl(163)
        Hkl(44)=SINP*Gijkl(100)+COSP*Gijkl(164)
        Hkl(52)=SINP*Gijkl(114)+COSP*Gijkl(178)
        Hkl(53)=SINP*Gijkl(115)+COSP*Gijkl(179)
        Hkl(54)=SINP*Gijkl(116)+COSP*Gijkl(180)
        Hkl(62)=Gijkl(210)
        Hkl(63)=Gijkl(211)
        Hkl(72)=Gijkl(226)
        Hkl(73)=Gijkl(227)
        Hkl(74)=Gijkl(228)
        Hkl(82)=Gijkl(242)
        Hkl(1)=COSP*Gijkl(81)-SINP*Gijkl(145)
        Hkl(11)=COSP*Gijkl(97)-SINP*Gijkl(161)
        Hkl(21)=COSP*Gijkl(113)-SINP*Gijkl(177)
        Hkl(31)=SINP*Gijkl(81)+COSP*Gijkl(145)
        Hkl(41)=SINP*Gijkl(97)+COSP*Gijkl(161)
        Hkl(51)=SINP*Gijkl(113)+COSP*Gijkl(177)
        Hkl(71)=Gijkl(225)
!
        Gijkl(86)=COSP*Hkl(5)-SINP*Hkl(15)
        Gijkl(87)=COSP*Hkl(6)-SINP*Hkl(16)
        Gijkl(88)=COSP*Hkl(7)-SINP*Hkl(17)
        Gijkl(91)=COSP*Hkl(8)-SINP*Hkl(18)
        Gijkl(92)=COSP*Hkl(9)-SINP*Hkl(19)
        Gijkl(96)=COSP*Hkl(10)-SINP*Hkl(20)
        Gijkl(102)=SINP*Hkl(5)+COSP*Hkl(15)
        Gijkl(103)=SINP*Hkl(6)+COSP*Hkl(16)
        Gijkl(104)=SINP*Hkl(7)+COSP*Hkl(17)
        Gijkl(107)=SINP*Hkl(8)+COSP*Hkl(18)
        Gijkl(108)=SINP*Hkl(9)+COSP*Hkl(19)
        Gijkl(112)=SINP*Hkl(10)+COSP*Hkl(20)
        Gijkl(118)=Hkl(25)
        Gijkl(119)=Hkl(26)
        Gijkl(120)=Hkl(27)
        Gijkl(123)=Hkl(28)
        Gijkl(124)=Hkl(29)
        Gijkl(128)=Hkl(30)
        Gijkl(150)=COSP*Hkl(35)-SINP*Hkl(45)
        Gijkl(151)=COSP*Hkl(36)-SINP*Hkl(46)
        Gijkl(152)=COSP*Hkl(37)-SINP*Hkl(47)
        Gijkl(155)=COSP*Hkl(38)-SINP*Hkl(48)
        Gijkl(156)=COSP*Hkl(39)-SINP*Hkl(49)
        Gijkl(160)=COSP*Hkl(40)-SINP*Hkl(50)
        Gijkl(166)=SINP*Hkl(35)+COSP*Hkl(45)
        Gijkl(167)=SINP*Hkl(36)+COSP*Hkl(46)
        Gijkl(168)=SINP*Hkl(37)+COSP*Hkl(47)
        Gijkl(171)=SINP*Hkl(38)+COSP*Hkl(48)
        Gijkl(172)=SINP*Hkl(39)+COSP*Hkl(49)
        Gijkl(176)=SINP*Hkl(40)+COSP*Hkl(50)
        Gijkl(182)=Hkl(55)
        Gijkl(183)=Hkl(56)
        Gijkl(184)=Hkl(57)
        Gijkl(187)=Hkl(58)
        Gijkl(188)=Hkl(59)
        Gijkl(192)=Hkl(60)
        Gijkl(214)=COSP*Hkl(65)-SINP*Hkl(75)
        Gijkl(215)=COSP*Hkl(66)-SINP*Hkl(76)
        Gijkl(216)=COSP*Hkl(67)-SINP*Hkl(77)
        Gijkl(219)=COSP*Hkl(68)-SINP*Hkl(78)
        Gijkl(220)=COSP*Hkl(69)-SINP*Hkl(79)
        Gijkl(224)=COSP*Hkl(70)-SINP*Hkl(80)
        Gijkl(230)=SINP*Hkl(65)+COSP*Hkl(75)
        Gijkl(231)=SINP*Hkl(66)+COSP*Hkl(76)
        Gijkl(232)=SINP*Hkl(67)+COSP*Hkl(77)
        Gijkl(235)=SINP*Hkl(68)+COSP*Hkl(78)
        Gijkl(236)=SINP*Hkl(69)+COSP*Hkl(79)
        Gijkl(240)=SINP*Hkl(70)+COSP*Hkl(80)
        Gijkl(82)=COSP*Hkl(2)-SINP*Hkl(12)
        Gijkl(83)=COSP*Hkl(3)-SINP*Hkl(13)
        Gijkl(84)=COSP*Hkl(4)-SINP*Hkl(14)
        Gijkl(98)=SINP*Hkl(2)+COSP*Hkl(12)
        Gijkl(99)=SINP*Hkl(3)+COSP*Hkl(13)
        Gijkl(100)=SINP*Hkl(4)+COSP*Hkl(14)
        Gijkl(114)=Hkl(22)
        Gijkl(115)=Hkl(23)
        Gijkl(116)=Hkl(24)
        Gijkl(146)=COSP*Hkl(32)-SINP*Hkl(42)
        Gijkl(147)=COSP*Hkl(33)-SINP*Hkl(43)
        Gijkl(148)=COSP*Hkl(34)-SINP*Hkl(44)
        Gijkl(162)=SINP*Hkl(32)+COSP*Hkl(42)
        Gijkl(163)=SINP*Hkl(33)+COSP*Hkl(43)
        Gijkl(164)=SINP*Hkl(34)+COSP*Hkl(44)
        Gijkl(178)=Hkl(52)
        Gijkl(179)=Hkl(53)
        Gijkl(180)=Hkl(54)
        Gijkl(210)=COSP*Hkl(62)-SINP*Hkl(72)
        Gijkl(211)=COSP*Hkl(63)-SINP*Hkl(73)
        Gijkl(212)=COSP*Hkl(64)-SINP*Hkl(74)
        Gijkl(226)=SINP*Hkl(62)+COSP*Hkl(72)
        Gijkl(227)=SINP*Hkl(63)+COSP*Hkl(73)
        Gijkl(228)=SINP*Hkl(64)+COSP*Hkl(74)
        Gijkl(81)=COSP*Hkl(1)-SINP*Hkl(11)
        Gijkl(97)=SINP*Hkl(1)+COSP*Hkl(11)
        Gijkl(113)=Hkl(21)
        Gijkl(145)=COSP*Hkl(31)-SINP*Hkl(41)
        Gijkl(161)=SINP*Hkl(31)+COSP*Hkl(41)
        Gijkl(177)=Hkl(51)
        Gijkl(209)=COSP*Hkl(61)-SINP*Hkl(71)
        Gijkl(225)=SINP*Hkl(61)+COSP*Hkl(71)
      end IF
!
! Translates up to 160 integrals on A, B and Q to up to 256 integrals on A, B, C and D.
      Gijkl(85)=Gijkl(82)
      Gijkl(89)=Gijkl(83)
      Gijkl(90)=Gijkl(87)
      Gijkl(93)=Gijkl(84)
      Gijkl(94)=Gijkl(88)
      Gijkl(95)=Gijkl(92)
      Gijkl(101)=Gijkl(98)
      Gijkl(105)=Gijkl(99)
      Gijkl(106)=Gijkl(103)
      Gijkl(109)=Gijkl(100)
      Gijkl(110)=Gijkl(104)
      Gijkl(111)=Gijkl(108)
      Gijkl(117)=Gijkl(114)
      Gijkl(121)=Gijkl(115)
      Gijkl(122)=Gijkl(119)
      Gijkl(125)=Gijkl(116)
      Gijkl(126)=Gijkl(120)
      Gijkl(127)=Gijkl(124)
      Gijkl(149)=Gijkl(146)
      Gijkl(153)=Gijkl(147)
      Gijkl(154)=Gijkl(151)
      Gijkl(157)=Gijkl(148)
      Gijkl(158)=Gijkl(152)
      Gijkl(159)=Gijkl(156)
      Gijkl(165)=Gijkl(162)
      Gijkl(169)=Gijkl(163)
      Gijkl(170)=Gijkl(167)
      Gijkl(173)=Gijkl(164)
      Gijkl(174)=Gijkl(168)
      Gijkl(175)=Gijkl(172)
      Gijkl(181)=Gijkl(178)
      Gijkl(185)=Gijkl(179)
      Gijkl(186)=Gijkl(183)
      Gijkl(189)=Gijkl(180)
      Gijkl(190)=Gijkl(184)
      Gijkl(191)=Gijkl(188)
      Gijkl(213)=Gijkl(210)
      Gijkl(217)=Gijkl(211)
      Gijkl(218)=Gijkl(215)
      Gijkl(221)=Gijkl(212)
      Gijkl(222)=Gijkl(216)
      Gijkl(223)=Gijkl(220)
      Gijkl(229)=Gijkl(226)
      Gijkl(233)=Gijkl(227)
      Gijkl(234)=Gijkl(231)
      Gijkl(237)=Gijkl(228)
      Gijkl(238)=Gijkl(232)
      Gijkl(239)=Gijkl(236)
      Gijkl(245)=Gijkl(242)
      Gijkl(249)=Gijkl(243)
      Gijkl(250)=Gijkl(247)
      Gijkl(253)=Gijkl(244)
      Gijkl(254)=Gijkl(248)
      Gijkl(255)=Gijkl(252)
      IF(RCDSQ.GT.ZERO)then
        R13=CQ*SING
        R33=CQ*COSG
        R14=DQ*SING
        R34=DQ*COSG
      Gijkl( 85)=Gijkl( 85)+R13*Gijkl( 81)
      Gijkl( 86)=Gijkl( 86)+R13*Gijkl( 82)
      Gijkl( 87)=Gijkl( 87)+R13*Gijkl( 83)
      Gijkl( 88)=Gijkl( 88)+R13*Gijkl( 84)
      Gijkl(101)=Gijkl(101)+R13*Gijkl( 97)
      Gijkl(102)=Gijkl(102)+R13*Gijkl( 98)
      Gijkl(103)=Gijkl(103)+R13*Gijkl( 99)
      Gijkl(104)=Gijkl(104)+R13*Gijkl(100)
      Gijkl(117)=Gijkl(117)+R13*Gijkl(113)
      Gijkl(118)=Gijkl(118)+R13*Gijkl(114)
      Gijkl(119)=Gijkl(119)+R13*Gijkl(115)
      Gijkl(120)=Gijkl(120)+R13*Gijkl(116)
      Gijkl(133)=Gijkl(133)+R13*Gijkl(129)
      Gijkl(134)=Gijkl(134)+R13*Gijkl(130)
      Gijkl(149)=Gijkl(149)+R13*Gijkl(145)
      Gijkl(150)=Gijkl(150)+R13*Gijkl(146)
      Gijkl(151)=Gijkl(151)+R13*Gijkl(147)
      Gijkl(152)=Gijkl(152)+R13*Gijkl(148)
      Gijkl(165)=Gijkl(165)+R13*Gijkl(161)
      Gijkl(166)=Gijkl(166)+R13*Gijkl(162)
      Gijkl(167)=Gijkl(167)+R13*Gijkl(163)
      Gijkl(168)=Gijkl(168)+R13*Gijkl(164)
      Gijkl(181)=Gijkl(181)+R13*Gijkl(177)
      Gijkl(182)=Gijkl(182)+R13*Gijkl(178)
      Gijkl(183)=Gijkl(183)+R13*Gijkl(179)
      Gijkl(184)=Gijkl(184)+R13*Gijkl(180)
      Gijkl(197)=Gijkl(197)+R13*Gijkl(193)
      Gijkl(198)=Gijkl(198)+R13*Gijkl(194)
      Gijkl(213)=Gijkl(213)+R13*Gijkl(209)
      Gijkl(214)=Gijkl(214)+R13*Gijkl(210)
      Gijkl(215)=Gijkl(215)+R13*Gijkl(211)
      Gijkl(216)=Gijkl(216)+R13*Gijkl(212)
      Gijkl(229)=Gijkl(229)+R13*Gijkl(225)
      Gijkl(230)=Gijkl(230)+R13*Gijkl(226)
      Gijkl(231)=Gijkl(231)+R13*Gijkl(227)
      Gijkl(232)=Gijkl(232)+R13*Gijkl(228)
      Gijkl(245)=Gijkl(245)+R13*Gijkl(241)
      Gijkl(246)=Gijkl(246)+R13*Gijkl(242)
      Gijkl(247)=Gijkl(247)+R13*Gijkl(243)
      Gijkl(248)=Gijkl(248)+R13*Gijkl(244)

      Gijkl( 93)=Gijkl( 93)+R33*Gijkl( 81)
      Gijkl( 94)=Gijkl( 94)+R33*Gijkl( 82)
      Gijkl( 95)=Gijkl( 95)+R33*Gijkl( 83)
      Gijkl( 96)=Gijkl( 96)+R33*Gijkl( 84)
      Gijkl(109)=Gijkl(109)+R33*Gijkl( 97)
      Gijkl(110)=Gijkl(110)+R33*Gijkl( 98)
      Gijkl(111)=Gijkl(111)+R33*Gijkl( 99)
      Gijkl(112)=Gijkl(112)+R33*Gijkl(100)
      Gijkl(125)=Gijkl(125)+R33*Gijkl(113)
      Gijkl(126)=Gijkl(126)+R33*Gijkl(114)
      Gijkl(127)=Gijkl(127)+R33*Gijkl(115)
      Gijkl(128)=Gijkl(128)+R33*Gijkl(116)
      Gijkl(141)=Gijkl(141)+R33*Gijkl(129)
      Gijkl(144)=Gijkl(144)+R33*Gijkl(132)
      Gijkl(157)=Gijkl(157)+R33*Gijkl(145)
      Gijkl(158)=Gijkl(158)+R33*Gijkl(146)
      Gijkl(159)=Gijkl(159)+R33*Gijkl(147)
      Gijkl(160)=Gijkl(160)+R33*Gijkl(148)
      Gijkl(173)=Gijkl(173)+R33*Gijkl(161)
      Gijkl(174)=Gijkl(174)+R33*Gijkl(162)
      Gijkl(175)=Gijkl(175)+R33*Gijkl(163)
      Gijkl(176)=Gijkl(176)+R33*Gijkl(164)
      Gijkl(189)=Gijkl(189)+R33*Gijkl(177)
      Gijkl(190)=Gijkl(190)+R33*Gijkl(178)
      Gijkl(191)=Gijkl(191)+R33*Gijkl(179)
      Gijkl(192)=Gijkl(192)+R33*Gijkl(180)
      Gijkl(205)=Gijkl(205)+R33*Gijkl(193)
      Gijkl(208)=Gijkl(208)+R33*Gijkl(196)
      Gijkl(221)=Gijkl(221)+R33*Gijkl(209)
      Gijkl(222)=Gijkl(222)+R33*Gijkl(210)
      Gijkl(223)=Gijkl(223)+R33*Gijkl(211)
      Gijkl(224)=Gijkl(224)+R33*Gijkl(212)
      Gijkl(237)=Gijkl(237)+R33*Gijkl(225)
      Gijkl(238)=Gijkl(238)+R33*Gijkl(226)
      Gijkl(239)=Gijkl(239)+R33*Gijkl(227)
      Gijkl(240)=Gijkl(240)+R33*Gijkl(228)
      Gijkl(253)=Gijkl(253)+R33*Gijkl(241)
      Gijkl(254)=Gijkl(254)+R33*Gijkl(242)
      Gijkl(255)=Gijkl(255)+R33*Gijkl(243)
      Gijkl(256)=Gijkl(256)+R33*Gijkl(244)

      Gijkl( 86)=Gijkl( 86)+R14*Gijkl( 85)
      Gijkl( 90)=Gijkl( 90)+R14*Gijkl( 89)
      Gijkl( 94)=Gijkl( 94)+R14*Gijkl( 93)
      Gijkl( 98)=Gijkl( 98)+R14*Gijkl( 97)
      Gijkl(102)=Gijkl(102)+R14*Gijkl(101)
      Gijkl(106)=Gijkl(106)+R14*Gijkl(105)
      Gijkl(110)=Gijkl(110)+R14*Gijkl(109)
      Gijkl(114)=Gijkl(114)+R14*Gijkl(113)
      Gijkl(118)=Gijkl(118)+R14*Gijkl(117)
      Gijkl(122)=Gijkl(122)+R14*Gijkl(121)
      Gijkl(126)=Gijkl(126)+R14*Gijkl(125)
      Gijkl(130)=Gijkl(130)+R14*Gijkl(129)
      Gijkl(134)=Gijkl(134)+R14*Gijkl(133)
      Gijkl(146)=Gijkl(146)+R14*Gijkl(145)
      Gijkl(150)=Gijkl(150)+R14*Gijkl(149)
      Gijkl(154)=Gijkl(154)+R14*Gijkl(153)
      Gijkl(158)=Gijkl(158)+R14*Gijkl(157)
      Gijkl(162)=Gijkl(162)+R14*Gijkl(161)
      Gijkl(166)=Gijkl(166)+R14*Gijkl(165)
      Gijkl(170)=Gijkl(170)+R14*Gijkl(169)
      Gijkl(174)=Gijkl(174)+R14*Gijkl(173)
      Gijkl(178)=Gijkl(178)+R14*Gijkl(177)
      Gijkl(182)=Gijkl(182)+R14*Gijkl(181)
      Gijkl(186)=Gijkl(186)+R14*Gijkl(185)
      Gijkl(190)=Gijkl(190)+R14*Gijkl(189)
      Gijkl(194)=Gijkl(194)+R14*Gijkl(193)
      Gijkl(198)=Gijkl(198)+R14*Gijkl(197)
      Gijkl(210)=Gijkl(210)+R14*Gijkl(209)
      Gijkl(214)=Gijkl(214)+R14*Gijkl(213)
      Gijkl(218)=Gijkl(218)+R14*Gijkl(217)
      Gijkl(222)=Gijkl(222)+R14*Gijkl(221)
      Gijkl(226)=Gijkl(226)+R14*Gijkl(225)
      Gijkl(230)=Gijkl(230)+R14*Gijkl(229)
      Gijkl(234)=Gijkl(234)+R14*Gijkl(233)
      Gijkl(238)=Gijkl(238)+R14*Gijkl(237)
      Gijkl(242)=Gijkl(242)+R14*Gijkl(241)
      Gijkl(246)=Gijkl(246)+R14*Gijkl(245)
      Gijkl(250)=Gijkl(250)+R14*Gijkl(249)
      Gijkl(254)=Gijkl(254)+R14*Gijkl(253)

      Gijkl( 88)=Gijkl( 88)+R34*Gijkl( 85)
      Gijkl( 92)=Gijkl( 92)+R34*Gijkl( 89)
      Gijkl( 96)=Gijkl( 96)+R34*Gijkl( 93)
      Gijkl(100)=Gijkl(100)+R34*Gijkl( 97)
      Gijkl(104)=Gijkl(104)+R34*Gijkl(101)
      Gijkl(108)=Gijkl(108)+R34*Gijkl(105)
      Gijkl(112)=Gijkl(112)+R34*Gijkl(109)
      Gijkl(116)=Gijkl(116)+R34*Gijkl(113)
      Gijkl(120)=Gijkl(120)+R34*Gijkl(117)
      Gijkl(124)=Gijkl(124)+R34*Gijkl(121)
      Gijkl(128)=Gijkl(128)+R34*Gijkl(125)
      Gijkl(132)=Gijkl(132)+R34*Gijkl(129)
      Gijkl(144)=Gijkl(144)+R34*Gijkl(141)
      Gijkl(148)=Gijkl(148)+R34*Gijkl(145)
      Gijkl(152)=Gijkl(152)+R34*Gijkl(149)
      Gijkl(156)=Gijkl(156)+R34*Gijkl(153)
      Gijkl(160)=Gijkl(160)+R34*Gijkl(157)
      Gijkl(164)=Gijkl(164)+R34*Gijkl(161)
      Gijkl(168)=Gijkl(168)+R34*Gijkl(165)
      Gijkl(172)=Gijkl(172)+R34*Gijkl(169)
      Gijkl(176)=Gijkl(176)+R34*Gijkl(173)
      Gijkl(180)=Gijkl(180)+R34*Gijkl(177)
      Gijkl(184)=Gijkl(184)+R34*Gijkl(181)
      Gijkl(188)=Gijkl(188)+R34*Gijkl(185)
      Gijkl(192)=Gijkl(192)+R34*Gijkl(189)
      Gijkl(196)=Gijkl(196)+R34*Gijkl(193)
      Gijkl(208)=Gijkl(208)+R34*Gijkl(205)
      Gijkl(212)=Gijkl(212)+R34*Gijkl(209)
      Gijkl(216)=Gijkl(216)+R34*Gijkl(213)
      Gijkl(220)=Gijkl(220)+R34*Gijkl(217)
      Gijkl(224)=Gijkl(224)+R34*Gijkl(221)
      Gijkl(228)=Gijkl(228)+R34*Gijkl(225)
      Gijkl(232)=Gijkl(232)+R34*Gijkl(229)
      Gijkl(236)=Gijkl(236)+R34*Gijkl(233)
      Gijkl(240)=Gijkl(240)+R34*Gijkl(237)
      Gijkl(244)=Gijkl(244)+R34*Gijkl(241)
      Gijkl(248)=Gijkl(248)+R34*Gijkl(245)
      Gijkl(252)=Gijkl(252)+R34*Gijkl(249)
      Gijkl(256)=Gijkl(256)+R34*Gijkl(253)
      end IF

      Gijkl(1:85)=ZERO
      Gijkl(89)=ZERO
      Gijkl(93)=ZERO
      Gijkl(97:101)=ZERO
      Gijkl(105)=ZERO
      Gijkl(109)=ZERO
      Gijkl(113:117)=ZERO
      Gijkl(121)=ZERO
      Gijkl(125)=ZERO
      Gijkl(129:149)=ZERO
      Gijkl(153)=ZERO
      Gijkl(157)=ZERO
      Gijkl(161:165)=ZERO
      Gijkl(169)=ZERO
      Gijkl(173)=ZERO
      Gijkl(177:181)=ZERO
      Gijkl(185)=ZERO
      Gijkl(189)=ZERO
      Gijkl(193:213)=ZERO
      Gijkl(217)=ZERO
      Gijkl(221)=ZERO
      Gijkl(225:229)=ZERO
      Gijkl(233)=ZERO
      Gijkl(237)=ZERO
      Gijkl(241:245)=ZERO
      Gijkl(249)=ZERO
      Gijkl(253)=ZERO
!
! Total of 81 integrals:
      INTS_out(1)=INTS_out(1)+Gijkl( 86)*ExpCCkl
      INTS_out(2)=INTS_out(2)+Gijkl( 87)*ExpCCkl
      INTS_out(3)=INTS_out(3)+Gijkl( 88)*ExpCCkl
      INTS_out(4)=INTS_out(4)+Gijkl( 90)*ExpCCkl
      INTS_out(5)=INTS_out(5)+Gijkl( 91)*ExpCCkl
      INTS_out(6)=INTS_out(6)+Gijkl( 92)*ExpCCkl
      INTS_out(7)=INTS_out(7)+Gijkl( 94)*ExpCCkl
      INTS_out(8)=INTS_out(8)+Gijkl( 95)*ExpCCkl
      INTS_out(9)=INTS_out(9)+Gijkl( 96)*ExpCCkl
      INTS_out(10)=INTS_out(10)+Gijkl(102)*ExpCCkl
      INTS_out(11)=INTS_out(11)+Gijkl(103)*ExpCCkl
      INTS_out(12)=INTS_out(12)+Gijkl(104)*ExpCCkl
      INTS_out(13)=INTS_out(13)+Gijkl(106)*ExpCCkl
      INTS_out(14)=INTS_out(14)+Gijkl(107)*ExpCCkl
      INTS_out(15)=INTS_out(15)+Gijkl(108)*ExpCCkl
      INTS_out(16)=INTS_out(16)+Gijkl(110)*ExpCCkl
      INTS_out(17)=INTS_out(17)+Gijkl(111)*ExpCCkl
      INTS_out(18)=INTS_out(18)+Gijkl(112)*ExpCCkl
      INTS_out(19)=INTS_out(19)+Gijkl(118)*ExpCCkl
      INTS_out(20)=INTS_out(20)+Gijkl(119)*ExpCCkl
      INTS_out(21)=INTS_out(21)+Gijkl(120)*ExpCCkl
      INTS_out(22)=INTS_out(22)+Gijkl(122)*ExpCCkl
      INTS_out(23)=INTS_out(23)+Gijkl(123)*ExpCCkl
      INTS_out(24)=INTS_out(24)+Gijkl(124)*ExpCCkl
      INTS_out(25)=INTS_out(25)+Gijkl(126)*ExpCCkl
      INTS_out(26)=INTS_out(26)+Gijkl(127)*ExpCCkl
      INTS_out(27)=INTS_out(27)+Gijkl(128)*ExpCCkl
      INTS_out(28)=INTS_out(28)+Gijkl(150)*ExpCCkl
      INTS_out(29)=INTS_out(29)+Gijkl(151)*ExpCCkl
      INTS_out(30)=INTS_out(30)+Gijkl(152)*ExpCCkl
      INTS_out(31)=INTS_out(31)+Gijkl(154)*ExpCCkl
      INTS_out(32)=INTS_out(32)+Gijkl(155)*ExpCCkl
      INTS_out(33)=INTS_out(33)+Gijkl(156)*ExpCCkl
      INTS_out(34)=INTS_out(34)+Gijkl(158)*ExpCCkl
      INTS_out(35)=INTS_out(35)+Gijkl(159)*ExpCCkl
      INTS_out(36)=INTS_out(36)+Gijkl(160)*ExpCCkl
      INTS_out(37)=INTS_out(37)+Gijkl(166)*ExpCCkl
      INTS_out(38)=INTS_out(38)+Gijkl(167)*ExpCCkl
      INTS_out(39)=INTS_out(39)+Gijkl(168)*ExpCCkl
      INTS_out(40)=INTS_out(40)+Gijkl(170)*ExpCCkl
      INTS_out(41)=INTS_out(41)+Gijkl(171)*ExpCCkl
      INTS_out(42)=INTS_out(42)+Gijkl(172)*ExpCCkl
      INTS_out(43)=INTS_out(43)+Gijkl(174)*ExpCCkl
      INTS_out(44)=INTS_out(44)+Gijkl(175)*ExpCCkl
      INTS_out(45)=INTS_out(45)+Gijkl(176)*ExpCCkl
      INTS_out(46)=INTS_out(46)+Gijkl(182)*ExpCCkl
      INTS_out(47)=INTS_out(47)+Gijkl(183)*ExpCCkl
      INTS_out(48)=INTS_out(48)+Gijkl(184)*ExpCCkl
      INTS_out(49)=INTS_out(49)+Gijkl(186)*ExpCCkl
      INTS_out(50)=INTS_out(50)+Gijkl(187)*ExpCCkl
      INTS_out(51)=INTS_out(51)+Gijkl(188)*ExpCCkl
      INTS_out(52)=INTS_out(52)+Gijkl(190)*ExpCCkl
      INTS_out(53)=INTS_out(53)+Gijkl(191)*ExpCCkl
      INTS_out(54)=INTS_out(54)+Gijkl(192)*ExpCCkl
      INTS_out(55)=INTS_out(55)+Gijkl(214)*ExpCCkl
      INTS_out(56)=INTS_out(56)+Gijkl(215)*ExpCCkl
      INTS_out(57)=INTS_out(57)+Gijkl(216)*ExpCCkl
      INTS_out(58)=INTS_out(58)+Gijkl(218)*ExpCCkl
      INTS_out(59)=INTS_out(59)+Gijkl(219)*ExpCCkl
      INTS_out(60)=INTS_out(60)+Gijkl(220)*ExpCCkl
      INTS_out(61)=INTS_out(61)+Gijkl(222)*ExpCCkl
      INTS_out(62)=INTS_out(62)+Gijkl(223)*ExpCCkl
      INTS_out(63)=INTS_out(63)+Gijkl(224)*ExpCCkl
      INTS_out(64)=INTS_out(64)+Gijkl(230)*ExpCCkl
      INTS_out(65)=INTS_out(65)+Gijkl(231)*ExpCCkl
      INTS_out(66)=INTS_out(66)+Gijkl(232)*ExpCCkl
      INTS_out(67)=INTS_out(67)+Gijkl(234)*ExpCCkl
      INTS_out(68)=INTS_out(68)+Gijkl(235)*ExpCCkl
      INTS_out(69)=INTS_out(69)+Gijkl(236)*ExpCCkl
      INTS_out(70)=INTS_out(70)+Gijkl(238)*ExpCCkl
      INTS_out(71)=INTS_out(71)+Gijkl(239)*ExpCCkl
      INTS_out(72)=INTS_out(72)+Gijkl(240)*ExpCCkl
      INTS_out(73)=INTS_out(73)+Gijkl(246)*ExpCCkl
      INTS_out(74)=INTS_out(74)+Gijkl(247)*ExpCCkl
      INTS_out(75)=INTS_out(75)+Gijkl(248)*ExpCCkl
      INTS_out(76)=INTS_out(76)+Gijkl(250)*ExpCCkl
      INTS_out(77)=INTS_out(77)+Gijkl(251)*ExpCCkl
      INTS_out(78)=INTS_out(78)+Gijkl(252)*ExpCCkl
      INTS_out(79)=INTS_out(79)+Gijkl(254)*ExpCCkl
      INTS_out(80)=INTS_out(80)+Gijkl(255)*ExpCCkl
      INTS_out(81)=INTS_out(81)+Gijkl(256)*ExpCCkl

      end IF ! ExpCCkl
      end IF ! EXPARG
      end do ! Lgauss
      end do ! Kgauss
      call R31111 (INTS_out)
      IF(Jashl.EQ.Iashl)then
        do I=10,27
        INTS_out(I)=ZERO
        end DO
        do I=46,54
        INTS_out(I)=ZERO
        end DO
      end IF
      IF(DOZERO)then
        INTS_out(2)=ZERO
        INTS_out(3)=ZERO
        INTS_out(6)=ZERO
        INTS_out(11)=ZERO
        INTS_out(12)=ZERO
        INTS_out(15)=ZERO
        INTS_out(20)=ZERO
        INTS_out(21)=ZERO
        INTS_out(24)=ZERO
        INTS_out(29)=ZERO
        INTS_out(30)=ZERO
        INTS_out(33)=ZERO
        INTS_out(38)=ZERO
        INTS_out(39)=ZERO
        INTS_out(42)=ZERO
        INTS_out(47)=ZERO
        INTS_out(48)=ZERO
        INTS_out(51)=ZERO
        INTS_out(56)=ZERO
        INTS_out(57)=ZERO
        INTS_out(60)=ZERO
        INTS_out(65)=ZERO
        INTS_out(66)=ZERO
        INTS_out(69)=ZERO
        INTS_out(74)=ZERO
        INTS_out(75)=ZERO
        INTS_out(78)=ZERO
      end IF
      IF(Kashl.EQ.Iashl)then
        IF(Lashl.EQ.Jashl)then
          INTS_out(2)=ZERO
          INTS_out(3)=ZERO
          INTS_out(4)=ZERO
          INTS_out(5)=ZERO
          INTS_out(6)=ZERO
          INTS_out(7)=ZERO
          INTS_out(8)=ZERO
          INTS_out(9)=ZERO
          INTS_out(12)=ZERO
          INTS_out(13)=ZERO
          INTS_out(14)=ZERO
          INTS_out(15)=ZERO
          INTS_out(16)=ZERO
          INTS_out(17)=ZERO
          INTS_out(18)=ZERO
          INTS_out(22)=ZERO
          INTS_out(23)=ZERO
          INTS_out(24)=ZERO
          INTS_out(25)=ZERO
          INTS_out(26)=ZERO
          INTS_out(27)=ZERO
          INTS_out(32)=ZERO
          INTS_out(33)=ZERO
          INTS_out(34)=ZERO
          INTS_out(35)=ZERO
          INTS_out(36)=ZERO
          INTS_out(42)=ZERO
          INTS_out(43)=ZERO
          INTS_out(44)=ZERO
          INTS_out(45)=ZERO
          INTS_out(52)=ZERO
          INTS_out(53)=ZERO
          INTS_out(54)=ZERO
          INTS_out(62)=ZERO
          INTS_out(63)=ZERO
          INTS_out(72)=ZERO
        end IF
      end IF
!
! End of routine SP1111
      return
      end subroutine SP1111
      subroutine R31111 (INTS_out)
!*****************************************************************************
!     Date last modified: May 24, 2002                                       *
!     Author: R.A. Poirier                                                   *
!     Description:  Based QCPE Version.                                      *
!     Rotate up to 81 integrals to space fixed axes.                        *
!*****************************************************************************
! Modules:

      implicit none
!
! Output array:
      double precision INTS_out(81)
!
! Local scalars:
      integer I1
      double precision T1,T2,T3
!
! Begin:
! SET 1:
      do I1=1,27
      T1=INTS_out(I1)
      T2=INTS_out(I1+27)
      T3=INTS_out(I1+54)
      INTS_out(I1)   =P11*T1+P21*T2+P31*T3
      INTS_out(I1+27)=P12*T1+P22*T2+P32*T3
      INTS_out(I1+54)=P13*T1+P23*T2+P33*T3
      end do ! I1


! SET 2:
      do I1=1,9
      T1=INTS_out(I1)
      T2=INTS_out(I1+9)
      T3=INTS_out(I1+18)
      INTS_out(I1)   =P11*T1+P21*T2+P31*T3
      INTS_out(I1+9) =P12*T1+P22*T2+P32*T3
      INTS_out(I1+18)=P13*T1+P23*T2+P33*T3

      T1=INTS_out(I1+27)
      T2=INTS_out(I1+36)
      T3=INTS_out(I1+45)
      INTS_out(I1+27)=P11*T1+P21*T2+P31*T3
      INTS_out(I1+36)=P12*T1+P22*T2+P32*T3
      INTS_out(I1+45)=P13*T1+P23*T2+P33*T3

      T1=INTS_out(I1+54)
      T2=INTS_out(I1+63)
      T3=INTS_out(I1+72)
      INTS_out(I1+54)=P11*T1+P21*T2+P31*T3
      INTS_out(I1+63)=P12*T1+P22*T2+P32*T3
      INTS_out(I1+72)=P13*T1+P23*T2+P33*T3
      end do ! I1

! SET 3:
      do I1=1,73,9
      T1=INTS_out(I1)
      T2=INTS_out(I1+3)
      T3=INTS_out(I1+6)
      INTS_out(I1)  =P11*T1+P21*T2+P31*T3
      INTS_out(I1+3)=P12*T1+P22*T2+P32*T3
      INTS_out(I1+6)=P13*T1+P23*T2+P33*T3
      T1=INTS_out(I1+1)
      T2=INTS_out(I1+4)
      T3=INTS_out(I1+7)
      INTS_out(I1+1)=P11*T1+P21*T2+P31*T3
      INTS_out(I1+4)=P12*T1+P22*T2+P32*T3
      INTS_out(I1+7)=P13*T1+P23*T2+P33*T3
      T1=INTS_out(I1+2)
      T2=INTS_out(I1+5)
      T3=INTS_out(I1+8)
      INTS_out(I1+2)=P11*T1+P21*T2+P31*T3
      INTS_out(I1+5)=P12*T1+P22*T2+P32*T3
      INTS_out(I1+8)=P13*T1+P23*T2+P33*T3
      end do ! I1

! SET 4:
      do i1=1,79,3
      T1=INTS_out(I1)
      T2=INTS_out(I1+1)
      T3=INTS_out(I1+2)
      INTS_out(I1)  =P11*T1+P21*T2+P31*T3
      INTS_out(I1+1)=P12*T1+P22*T2+P32*T3
      INTS_out(I1+2)=P13*T1+P23*T2+P33*T3
      end do ! I1
!
! End of routine R31111
      return
      end subroutine R31111
      subroutine I2E_SHL_PPPP
!***********************************************************************
!     Date last modified: May 24, 2002                     Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version.                                       *
!     Two-electron integral output routine.                            *
!     Note that each block contains integrals of only one type.        *
!***********************************************************************
! Modules:
      USE program_files
      USE QM_objects

      implicit none
!
! Local scalars:
      integer nint,I,J,K,KL,L,NCNT1,NN,NUMINT
      double precision INT1,INT2,INT3
!
! Begin:
!      call PRG_manager ('enter', 'I2E_SHL_PPPP', 'UTILITY')
!
      NCNT1=0
! Loop over set of integrals defined by ashls Iashl, Jashl, etc.
      do I=1,Irange
      do J=1,Jrange
      do K=1,Krange
      do L=1,Lrange
      NCNT1=NCNT1+1
      NUMINT=0
      IF(DABS(GOUT_ijkl(NCNT1)).GT.I2E_ACC)then
        NUMINT=NUMINT+1
      else
        GOUT_ijkl(NCNT1)=ZERO
      end IF
      IF(DABS(GOUT_ikjl(NCNT1)).GT.I2E_ACC)then
        NUMINT=NUMINT+1
      else
        GOUT_ikjl(NCNT1)=ZERO
      end IF
      IF(DABS(GOUT_iljk(NCNT1)).GT.I2E_ACC)then
        NUMINT=NUMINT+1
      else
        GOUT_iljk(NCNT1)=ZERO
      end IF
      IF(NUMINT.NE.0)then
! Integral over atomic orbitals (IJ/KL).
      Iao=AOI+I
      Jao=AOJ+J
      Kao=AOK+K
      Lao=AOL+L
!
! The following code determines the integral type
      IF(Iao.NE.Jao.and.Iao.NE.Kao.and.Jao.NE.Kao.and.Kao.NE.Lao.and.Jao.NE.Lao)then
!
      INT1=Gout_ijkl(ncnt1)
      INT2=Gout_ikjl(ncnt1)
      INT3=Gout_iljk(ncnt1)
!
! Pack label.
      NTINT(8)=NTINT(8)+NUMINT
        IJKL_count=IJKL_count+1
        IF(IJKL_count.gt.IJKL_MAX)then
          IJKL_Nbuffers=IJKL_Nbuffers+1
          if(integral_combinations)call I2E_COMB_ijkl (IJKL_INCORE, IJKL_MAX)
          call WRI_ijkl (IJKL_INCORE, IJKL_MAX, IJKL_unit, IJKL_MAX)
          Lijkl_ONDISK=.TRUE.
          ANY_ONDISK=.TRUE.
          IJKL_count=1
        end IF
        IJKL_INCORE(IJKL_count)%I=Iao
        IJKL_INCORE(IJKL_count)%J=Jao
        IJKL_INCORE(IJKL_count)%K=Kao
        IJKL_INCORE(IJKL_count)%L=Lao
        IJKL_INCORE(IJKL_count)%IJKL=INT1
        IJKL_INCORE(IJKL_count)%IKJL=INT2
        IJKL_INCORE(IJKL_count)%ILJK=INT3
!
      else if(Iao.EQ.Jao.and.Iao.NE.Kao.and.Kao.NE.Lao)then
! (II,KL).
      NTINT(1)=NTINT(1)+NUMINT
        IIKL_count=IIKL_count+1
        IF(IIKL_count.gt.IIKL_MAX)then
          IIKL_Nbuffers=IIKL_Nbuffers+1
          if(integral_combinations)call I2E_COMB_iikl (IIKL_INCORE, IIKL_MAX)
          call WRI_iikl (IIKL_INCORE, IIKL_MAX, IIKL_unit, IIKL_MAX)
          Liikl_ONDISK=.TRUE.
          ANY_ONDISK=.TRUE.
          IIKL_count=1
        end IF
        IIKL_INCORE(IIKL_count)%I=Iao
        IIKL_INCORE(IIKL_count)%K=Kao
        IIKL_INCORE(IIKL_count)%L=Lao
        IIKL_INCORE(IIKL_count)%iikl=Gout_ijkl(ncnt1)
        IIKL_INCORE(IIKL_count)%ikil=Gout_ikjl(ncnt1)
!
      else if(Iao.NE.Jao.and.Jao.GT.Kao.and.Kao.EQ.Lao)then
! (IJ,KK) converted to (IK,JK).
      NTINT(2)=NTINT(2)+NUMINT
        IJKJ_count=IJKJ_count+1
        IF(IJKJ_count.gt.IJKJ_MAX)then
          IJKJ_Nbuffers=IJKJ_Nbuffers+1
          if(integral_combinations)call I2E_COMB_ijkj (IJKJ_INCORE, IJKJ_MAX)
          call WRI_ijkj (IJKJ_INCORE,  IJKJ_MAX, IJKJ_unit, IJKJ_MAX)
          Lijkj_ONDISK=.TRUE.
          ANY_ONDISK=.TRUE.
          IJKJ_count=1
        end IF
        IJKJ_INCORE(IJKJ_count)%I=Iao
        IJKJ_INCORE(IJKJ_count)%J=Kao
        IJKJ_INCORE(IJKJ_count)%K=Jao
        IJKJ_INCORE(IJKJ_count)%ijkj=Gout_ikjl(ncnt1)
        IJKJ_INCORE(IJKJ_count)%ikjj=Gout_ijkl(ncnt1)
!
      else if(Iao.NE.Jao.and.Jao.LT.Kao.and.Kao.EQ.Lao)then
! (IJ,KK) converted to (IK,KJ) = (IJ,JL)
      NTINT(3)=NTINT(3)+NUMINT
        IJJL_count=IJJL_count+1
        IF(IJJL_count.gt.IJJL_MAX)then
          IJJL_Nbuffers=IJJL_Nbuffers+1
          if(integral_combinations)call I2E_COMB_ijjl (IJJL_INCORE, IJJL_MAX)
          call WRI_ijjl (IJJL_INCORE,  IJJL_MAX, IJJL_unit, IJJL_MAX)
          Lijjl_ONDISK=.TRUE.
          ANY_ONDISK=.TRUE.
          IJJL_count=1
        end IF
        IJJL_INCORE(IJJL_count)%I=Iao
        IJJL_INCORE(IJJL_count)%J=Kao
        IJJL_INCORE(IJJL_count)%L=Jao
        IJJL_INCORE(IJJL_count)%ijjl=Gout_ikjl(ncnt1)
        IJJL_INCORE(IJJL_count)%iljj=Gout_ijkl(ncnt1)
!
      else if(Iao.NE.Jao.and.Iao.NE.Kao.and.Jao.NE.Kao.and.Kao.NE.Lao)then
! (IJ,KJ).
      NTINT(2)=NTINT(2)+NUMINT
        IJKJ_count=IJKJ_count+1
        IF(IJKJ_count.gt.IJKJ_MAX)then
          IJKJ_Nbuffers=IJKJ_Nbuffers+1
          if(integral_combinations)call I2E_COMB_ijkj (IJKJ_INCORE, IJKJ_MAX)
          call WRI_ijkj (IJKJ_INCORE,  IJKJ_MAX, IJKJ_unit, IJKJ_MAX)
          Lijkj_ONDISK=.TRUE.
          ANY_ONDISK=.TRUE.
          IJKJ_count=1
        end IF
        IJKJ_INCORE(IJKJ_count)%I=Iao
        IJKJ_INCORE(IJKJ_count)%J=Jao
        IJKJ_INCORE(IJKJ_count)%K=Kao
        IJKJ_INCORE(IJKJ_count)%ijkj=Gout_ijkl(ncnt1)
        IJKJ_INCORE(IJKJ_count)%ikjj=Gout_ikjl(ncnt1)
!
      else if(Iao.NE.Jao.and.Iao.EQ.Kao.and.Jao.NE.Lao)then
! (IJ,IL) converted to (II,JK) = (II,KL).
      NTINT(1)=NTINT(1)+NUMINT
        IIKL_count=IIKL_count+1
        IF(IIKL_count.gt.IIKL_MAX)then
          IIKL_Nbuffers=IIKL_Nbuffers+1
          if(integral_combinations)call I2E_COMB_iikl (IIKL_INCORE, IIKL_MAX)
          call WRI_iikl (IIKL_INCORE, IIKL_MAX, IIKL_unit, IIKL_MAX)
          Liikl_ONDISK=.TRUE.
          ANY_ONDISK=.TRUE.
          IIKL_count=1
        end IF
        IIKL_INCORE(IIKL_count)%I=Iao
        IIKL_INCORE(IIKL_count)%K=Jao
        IIKL_INCORE(IIKL_count)%L=Lao
        IIKL_INCORE(IIKL_count)%iikl=Gout_ikjl(ncnt1)
        IIKL_INCORE(IIKL_count)%ikil=Gout_ijkl(ncnt1)
!
      else if(Iao.NE.Jao.and.Jao.EQ.Kao.and.Kao.NE.Lao)then
! (IJ,JL).
      NTINT(3)=NTINT(3)+NUMINT
        IJJL_count=IJJL_count+1
        IF(IJJL_count.gt.IJJL_MAX)then
          IJJL_Nbuffers=IJJL_Nbuffers+1
          if(integral_combinations)call I2E_COMB_ijjl (IJJL_INCORE, IJJL_MAX)
          call WRI_ijjl (IJJL_INCORE, IJJL_MAX, IJJL_unit, IJJL_MAX)
          Lijjl_ONDISK=.TRUE.
          ANY_ONDISK=.TRUE.
          IJJL_count=1
        end IF
        IJJL_INCORE(IJJL_count)%I=Iao
        IJJL_INCORE(IJJL_count)%J=Jao
        IJJL_INCORE(IJJL_count)%L=Lao
        IJJL_INCORE(IJJL_count)%ijjl=Gout_ijkl(ncnt1)
        IJJL_INCORE(IJJL_count)%iljj=Gout_ikjl(ncnt1)
!
      else if(Iao.EQ.Jao.and.Iao.NE.Kao.and.Kao.EQ.Lao)then
! (II,KK).
      NTINT(4)=NTINT(4)+NUMINT
      IIKK_count=IIKK_count+1
      IF(IIKK_count.GT.IIKK_MAX)then
        write(UNIout,*)' error_I2E_SHL_PPPP> too many IIKK integrals'
        stop ' error_I2E_SHL_PPPP> too many IIKK integrals'
      end IF
      IIKK_INCORE(IIKK_count)%I=Iao
      IIKK_INCORE(IIKK_count)%K=Kao
      IIKK_INCORE(IIKK_count)%iikk=Gout_ijkl(ncnt1)
      IIKK_INCORE(IIKK_count)%ikik=Gout_ikjl(ncnt1)
!
      else if(Iao.NE.Jao.and.Iao.EQ.Kao.and.Jao.EQ.Lao)then
!
! (IJ,IJ) converted to (II,KK).
      NTINT(4)=NTINT(4)+NUMINT
      IIKK_count=IIKK_count+1
      IF(IIKK_count.GT.IIKK_MAX)then
        write(UNIout,*)' error_I2E_SHL_PPPP> too many IIKK integrals'
        stop ' error_I2E_SHL_PPPP> too many IIKK integrals'
      end IF
      IIKK_INCORE(IIKK_count)%I=Iao
      IIKK_INCORE(IIKK_count)%K=Jao
      IIKK_INCORE(IIKK_count)%iikk=Gout_ikjl(ncnt1)
      IIKK_INCORE(IIKK_count)%ikik=Gout_ijkl(ncnt1)
!
      else if(Iao.NE.Jao.and.Jao.EQ.Kao.and.Kao.EQ.Lao)then
!
! (IJ,JJ).
      NTINT(5)=NTINT(5)+NUMINT
      IJJJ_count=IJJJ_count+1
      IF(IJJJ_count.GT.IJJJ_MAX)then
        write(UNIout,*)' error_I2E_SHL_PPPP> too many IJJJ integrals'
        stop ' error_I2E_SHL_PPPP> too many IJJJ integrals'
      end IF
      IJJJ_INCORE(IJJJ_count)%I=Iao
      IJJJ_INCORE(IJJJ_count)%J=Jao
      IJJJ_INCORE(IJJJ_count)%ijjj=Gout_ijkl(ncnt1)
!
      else if(Iao.EQ.Jao.and.Iao.EQ.Kao.and.Iao.NE.Lao)then
!
! (II,IL).
      NTINT(6)=NTINT(6)+NUMINT
      IIIL_count=IIIL_count+1
      IF(IIIL_count.GT.IIIL_MAX)then
        write(UNIout,*)' error_I2E_SHL_PPPP> too many IIIL integrals'
        stop
      end IF
      IIIL_INCORE(IIIL_count)%I=Iao
      IIIL_INCORE(IIIL_count)%L=Lao
      IIIL_INCORE(IIIL_count)%iiil=Gout_ijkl(ncnt1)
!
      else if(Iao.EQ.Jao.and.Iao.EQ.Kao.and.Iao.EQ.Lao)then
!
! (II,II).
      NTINT(7)=NTINT(7)+NUMINT
      IIII_count=IIII_count+1
      IF(IIII_count.GT.IIII_MAX)then
        write(UNIout,*)' error_I2E_SHL_PPPP> too many IIII integrals'
        stop ' error_I2E_SHL_PPPP> too many IIII integrals'
      end IF
      IIII_INCORE(IIII_count)%I=Iao
      IIII_INCORE(IIII_count)%iiii=Gout_ijkl(ncnt1)
!
      else
        stop 'I2E_SHL_PPPP: illegal integral type found'
      end IF
      end IF ! NUMINT.NE.0
      end do ! L
      end do ! K
      end do ! J
      end do ! I
!
! End of routine I2E_SHL_PPPP
!      call PRG_manager ('exit', 'I2E_SHL_PPPP', 'UTILITY')
      return
      end subroutine I2E_SHL_PPPP
      subroutine I2E_SHL_ABCD
!***********************************************************************
!     Date last modified: May 24, 2002                     Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version.                                       *
!     Two-electron integral output routine.                            *
!     Note that each block contains integrals of only one type.        *
!***********************************************************************
! Modules:
      USE QM_objects

      implicit none
!
! Local scalars:
      integer nint,I,J,K,L,NCNT1,NUMINT
      double precision INT1,INT2,INT3
!
! Begin:
!      call PRG_manager ('enter', 'I2E_SHL_ABCD', 'UTILITY')
!
      NCNT1=0
! Loop over set of integrals defined by ashls Iashl, Jashl, etc.
      do I=1,Irange
      do J=1,Jrange
      do K=1,Krange
      do L=1,Lrange
      NCNT1=NCNT1+1
      NUMINT=0
      IF(DABS(GOUT_ijkl(NCNT1)).GT.I2E_ACC)then
        NUMINT=NUMINT+1
      else
        GOUT_ijkl(NCNT1)=ZERO
      end IF
      IF(DABS(GOUT_ikjl(NCNT1)).GT.I2E_ACC)then
        NUMINT=NUMINT+1
      else
        GOUT_ikjl(NCNT1)=ZERO
      end IF
      IF(DABS(GOUT_iljk(NCNT1)).GT.I2E_ACC)then
        NUMINT=NUMINT+1
      else
        GOUT_iljk(NCNT1)=ZERO
      end IF
      IF(NUMINT.NE.0)then
! Integral over atomic orbitals (IJ/KL).
      Iao=AOI+I
      Jao=AOJ+J
      Kao=AOK+K
      Lao=AOL+L
!
      INT1=Gout_ijkl(ncnt1)
      INT2=Gout_ikjl(ncnt1)
      INT3=Gout_iljk(ncnt1)
!
! Pack label.
      NTINT(8)=NTINT(8)+NUMINT
        IJKL_count=IJKL_count+1
        IF(IJKL_count.gt.IJKL_MAX)then
          IJKL_Nbuffers=IJKL_Nbuffers+1
          if(integral_combinations)call I2E_COMB_ijkl (IJKL_INCORE, IJKL_MAX)
          call WRI_ijkl (IJKL_INCORE, IJKL_MAX, IJKL_unit, IJKL_MAX)
          Lijkl_ONDISK=.TRUE.
          ANY_ONDISK=.TRUE.
          IJKL_count=1
        end IF
        IJKL_INCORE(IJKL_count)%I=Iao
        IJKL_INCORE(IJKL_count)%J=Jao
        IJKL_INCORE(IJKL_count)%K=Kao
        IJKL_INCORE(IJKL_count)%L=Lao
        IJKL_INCORE(IJKL_count)%IJKL=INT1
        IJKL_INCORE(IJKL_count)%IKJL=INT2
        IJKL_INCORE(IJKL_count)%ILJK=INT3
      end IF ! NUMINT.NE.0
      end do ! L
      end do ! K
      end do ! J
      end do ! I
!
! End of routine I2E_SHL_ABCD
!      call PRG_manager ('exit', 'I2E_SHL_ABCD', 'UTILITY')
      return
      end subroutine I2E_SHL_ABCD
      end subroutine ISP_PPPP
!
      subroutine SORT1N (Igout)
!***********************************************************************
!     Date last modified: May 24, 2002                                 *
!     Author: Dale Keefe                                               *
!     Description:                                                     *
!     Sort the integrals created for Mtype=1 and ISET=2 or 3 so that   *
!     they are in the same order as the ones created on ISET=1.        *
!***********************************************************************

      implicit none
!
! Input scalars:
      integer Igout
!
! Local scalars:
      integer I,J,K,L,NCNT
!
! Begin:
      GOUT_ijkl(1:Igout)=GOUT(1:IGOUT)
      NCNT=0
      do I=0,Irange-1
      do J=0,Jrange-1
      do K=0,Krange-1
      do L=0,Lrange-1
      NCNT=NCNT+1
      GOUT_ikjl(NCNT)=GOUT(I*27+K*9+J*3+L+1+IGOUT)
      GOUT_iljk(NCNT)=GOUT(I*27+L*9+J*3+K+1+2*IGOUT)
      end do ! L
      end do ! K
      end do ! J
      end do ! I
!
! End of routine SORT1N
      return
      end subroutine SORT1N
      subroutine SORT2LN (Igout)
!***********************************************************************
!     Date last modified: May 24, 2002                                 *
!     Author: Dale Keefe                                               *
!     Description:                                                     *
!     Sort the integrals created for 0111 and 1111 (Mtype=2) so that   *
!     they are in the same order as the ones created on ISET=1.        *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer Igout
!
! Local scalars:
      integer I,J,K,L
!
! Begin:
      GOUT_ijkl(1:Igout)=GOUT(1:IGOUT)

      do I=0,Irange-1
      do J=0,2
      do K=0,2
      do L=0,K
      GOUT_ikjl(I*27+J*9+K*3+L+1)=GOUT(IGOUT+I*27+K*9+J*3+L+1)
      end do ! L
      end do ! K
      end do ! J
      end do ! I

      do I=0,Irange-1
      do J=0,2
      do K=1,2
      do L=0,K-1
      GOUT_iljk(I*27+J*9+K*3+L+1)=GOUT(IGOUT+I*27+L*9+J*3+K+1)
      end do ! L
      end do ! K
      end do ! J
      end do ! I
!
! End of routine SORT2LN
      return
      end subroutine SORT2LN
      subroutine SORT3N (Igout)
!***********************************************************************
!     Date last modified: May 24, 2002                                 *
!     Author: Dale Keefe                                               *
!     Description:                                                     *
!     Sort the integrals created for 0111 and 1111 (Mtype=2) so that   *
!     they are in the same order as the ones created on ISET=1.        *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer Igout
!
! Local scalars:
      integer I,J,K,L
!
! Begin:
      GOUT_ijkl(1:Igout)=GOUT(1:IGOUT)
      GOUT_iljk(1:Igout)=GOUT(2*Igout+1:3*IGOUT)
!
      do I=0,Irange-1
      do J=1,2
      do K=0,J-1
      do L=0,2
      GOUT_ikjl(I*27+J*9+K*3+L+1)=GOUT(I*27+K*9+J*3+L+1)
      GOUT_ijkl(I*27+K*9+J*3+L+1)=ZERO
      GOUT_iljk(I*27+J*9+K*3+L+1)=GOUT(IGOUT+I*27+L*9+J*3+K+1)
      end do ! L
      end do ! K
      end do ! J
      end do ! I
!
      do I=0,Irange-1
      do J=0,2
      do L=0,2
      GOUT_ikjl(I*27+J*12+L+1)=GOUT(IGOUT+I*27+L*9+4*J+1) ! ???
      end do ! L
      end do ! J
      end do ! I
!
! End of routine SORT3N
      return
      end subroutine SORT3N
      subroutine SORT4N (Igout)
!***********************************************************************
!     Date last modified: May 24, 2002                                 *
!     Author: Dale Keefe                                               *
!     Description:                                                     *
!     Sort the integrals created for 0111 and 1111 (Mtype=2).          *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer Igout
!
! Local scalars:
      integer I,J,K,L
!
! Begin:
      GOUT_ijkl(1:Igout)=GOUT(1:IGOUT)
      GOUT_ikjl(1:Igout)=GOUT(Igout+1:2*IGOUT)
!
      do I=0,Irange-1
      do J=1,2
      do K=0,J-1
      do L=0,K
      GOUT_ikjl(I*27+J*9+K*3+L+1)=GOUT(I*27+K*9+J*3+L+1)
      GOUT_ijkl(I*27+K*9+J*3+L+1)=ZERO
      GOUT(I*27+K*9+J*3+L+1)=ZERO
      end do ! L
      end do ! K
      end do ! J
      end do ! I

      do I=0,Irange-1
      do J=2,2
      do K=1,J-1
      do L=0,K-1
      GOUT_iljk(I*27+J*9+K*3+L+1)=GOUT(I*27+L*9+J*3+K+1)
      GOUT_ijkl(I*27+L*9+J*3+K+1)=ZERO
      GOUT(I*27+L*9+J*3+K+1)=ZERO
      end do ! L
      end do ! K
      end do ! J
      end do ! I
!
      do I=0,Irange-1
      do J=1,2
      do L=0,J-1
      GOUT_ikjl(I*27+J*12+L+1)=GOUT(I*27+L*9+4*J+1) ! ??
      GOUT_ijkl(I*27+L*9+4*J+1)=ZERO ! ??
      end do ! L
      end do ! J
      end do ! I
!
! End of routine SORT4N
      return
      end subroutine SORT4N
      subroutine SORT5N
!***********************************************************************
!     Date last modified: May 24, 2002                                 *
!     Author: Dale Keefe                                               *
!     Description:                                                     *
!     Sort the integrals created for 1111 (Mtype=5).                   *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalar:
!
! Local scalars:
      integer Igout,I,J,K,L
!
! Begin:
      Igout=81
      GOUT_ijkl(1:Igout)=GOUT(1:IGOUT)
!
      do I=1,2
      do J=0,I-1
      do K=0,2
      do L=0,2
      GOUT_ikjl(I*27+J*9+K*3+L+1)=GOUT(I*27+K*9+J*3+L+82)
      GOUT_iljk(I*27+J*9+K*3+L+1)=GOUT(J*27+K*9+I*3+L+82)
      end do ! L
      end do ! K
      end do ! J
      end do ! I

      do I=0,2
      do K=0,2
      do L=0,2
      GOUT_ikjl(I*36+K*3+L+1)=GOUT(I*30+K*9+L+82)
      end do ! L
      end do ! K
      end do ! I
!
! End of routine SORT5N
      return
      end subroutine SORT5N
      subroutine SORT6N
!***********************************************************************
!     Date last modified: May 24, 2002                                 *
!     Author: Dale Keefe                                               *
!     Description:                                                     *
!     Sort the integrals created for 1111 (Mtype=4).                   *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalar:
!
! Local scalars:
      integer Igout,I,J,K,L
!
! Begin:
      Igout=81
      GOUT_ijkl(1:Igout)=GOUT(1:IGOUT)

      do I=1,2
      do J=0,I-1
      do K=1,2
      do L=0,K-1
      GOUT_ikjl(I*27+J*9+K*3+L+1)=GOUT(I*27+K*9+J*3+L+82)
      GOUT_iljk(I*27+J*9+K*3+L+1)=GOUT(I*27+L*9+J*3+K+82)
      end do ! L
      end do ! K
      end do ! J
      end do ! I

      do I=0,2
      do K=0,2
      do L=0,K
      GOUT_ikjl(I*36+K*3+L+1)=GOUT(I*30+K*9+L+82)
      end do ! L
      end do ! K
      end do ! L

      do I=0,2
      do J=0,I-1
      do K=0,2
      GOUT_ikjl(I*27+J*9+K*4+1)=GOUT(I*27+K*10+J*3+82)
      end do ! K
      end do ! J
      end do ! I
!
! End of routine SORT6N
      return
      end subroutine SORT6N
      subroutine SORT7N (IJKLS, ILJKS, IKJLS)
!***********************************************************************
!     Date last modified: May 24, 2002                                 *
!     Author: Dale Keefe                                               *
!     Description:                                                     *
!     This routine sorts the integrals created for Mtype = 7(IIIL)     *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalar:
!
! Input array:
      double precision IJKLS(81),ILJKS(81),IKJLS(81)
!
! Local scalars:
      integer I,Igout,IND,INDI,JKLRNG,KLRNG
!
! Begin:
      Igout=81
      KLRNG=Krange*Lrange
      JKLRNG=Jrange*KLRNG
!
! Have (IJ/KL), find (IK/JL) and (IL/JK) with J > K > L.
      do Iao=1,Irange
        INDI=(Iao-1)*JKLRNG
      do Jao=1,Iao
      do Kao=1,Jao
      do Lao=1,Lrange
        IND=INDI+(Jao-1)*KLRNG+(Kao-1)*Lrange+Lao
      IF(Iao.GT.Jao.and.Jao.GT.Kao)then
        GOUT_ijkl(IND)=IJKLS(IND)
        Gout_ikjl(IND)=IJKLS(INDI+(Kao-1)*KLRNG+(Jao-1)*Lrange+Lao)
        Gout_iljk(IND)=IJKLS((Jao-1)*jklrng+(Kao-1)*KLRNG+(Iao-1)*Lrange+Lao)
      else if (Iao.GT.Jao)then
        Gout_ijkl(IND)=IJKLS(IND)
        Gout_ikjl(IND)=IJKLS((Jao-1)*jklrng+(Kao-1)*KLRNG+(Iao-1)*Lrange+Lao)
      else if (Jao.GT.Kao)then
        Gout_ijkl(IND)=IJKLS(IND)
        Gout_ikjl(IND)=IJKLS(INDI+(Kao-1)*KLRNG+(Jao-1)*Lrange+Lao)
      else
        Gout_ijkl(IND)=IJKLS(IND)
      end IF
      end do ! Lao
      end do ! Kao
      end do ! Jao
      end do ! Iao
!
! End of routine SORT7N
      return
      end subroutine SORT7N
      subroutine SORT8N
!***********************************************************************
!     Date last modified: May 24, 2002                                 *
!     Author: Dale Keefe                                               *
!     Description:                                                     *
!     Sort the integrals created for 1111 (Mtype=8).                   *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalar:
!
! Local scalars:
      integer Igout,I,J,K,L
!
! Begin:
      Igout=81
      GOUT_ijkl(1:Igout)=GOUT(1:IGOUT)

      do I=1,2
      do J=1,I
      do K=0,J-1
      do L=0,K
      GOUT_ikjl(I*27+J*9+K*3+L+1)=GOUT(I*27+K*9+J*3+L+1)
      GOUT_ijkl(I*27+K*9+J*3+L+1)=ZERO
      GOUT(I*27+K*9+J*3+L+1)=ZERO
      end do ! L
      end do ! K
      end do ! J
      end do ! I

      I=2
      do J=1,I-1
      do L=0,J-1
      GOUT_ikjl(I*27+J*12+L+1)=GOUT(I*27+L*9+J*4+1)
      GOUT_ijkl(I*27+L*9+J*4+1)=ZERO
      end do ! L
      end do ! J	
!
! End of routine SORT8N
      return
      end subroutine SORT8N
      subroutine SET_shells (Ishll, Jshll, Kshll, Lshll)
!***********************************************************************
!     Date last modified: February 21, 2002                            *
!     Author: R. A. Poirier                                            *
!     Description:  Prepare shells                                     *
!     Obtains geometrical information about the four centers,          *
!     finds two sets of local axes:                                    *
!     for centers  A and B,  P Set                                     *
!     for centers  C and D,  Q Set;                                    *
!     obtains information about Gaussian functions connected with      *
!     the P set of axes.                                               *
!***********************************************************************
! Modules:
      USE type_molecule

      implicit none
!
! Input scalars:
      integer Ishll,Jshll,Kshll,Lshll
!
! Local scalars:
      double precision PMQ1,PMQ2,PMQ3,PPQ1,PPQ2,PPQ3,P2,P3131,P3333,TEMP
!
! Local parameters:
      double precision PT0001,PT7,PT9
      parameter (PT0001=1.0D-4,PT7=0.7D0,PT9=0.9D0)
!
      IGBGN=Basis%shell(Ishll)%EXPBGN
      IGend=Basis%shell(Ishll)%EXPend
      JGBGN=Basis%shell(Jshll)%EXPBGN
      JGend=Basis%shell(Jshll)%EXPend
      KGBGN=Basis%shell(Kshll)%EXPBGN
      KGend=Basis%shell(Kshll)%EXPend
      LGBGN=Basis%shell(Lshll)%EXPBGN
      LGend=Basis%shell(Lshll)%EXPend
!
! Coordinates of atoms associated with I, J, K and Lashl.
! Obtain cartesian coordinates:
      Iatom=Basis%atmshl(Iashl)%ATMLST
      Jatom=Basis%atmshl(Jashl)%ATMLST
      Katom=Basis%atmshl(Kashl)%ATMLST
      Latom=Basis%atmshl(Lashl)%ATMLST
      XA=CARTESIAN(Iatom)%X
      YA=CARTESIAN(Iatom)%Y
      ZA=CARTESIAN(Iatom)%Z
      XB=CARTESIAN(Jatom)%X
      YB=CARTESIAN(Jatom)%Y
      ZB=CARTESIAN(Jatom)%Z
      XC=CARTESIAN(Katom)%X
      YC=CARTESIAN(Katom)%Y
      ZC=CARTESIAN(Katom)%Z
      XD=CARTESIAN(Latom)%X
      YD=CARTESIAN(Latom)%Y
      ZD=CARTESIAN(Latom)%Z
      RABSQ=SHRABSQ(Iatom,Jatom)
      RCDSQ=SHRABSQ(Katom,Latom)
! Find RAB, etc.
      RAB=DSQRT(RABSQ)
      RCD=DSQRT(RCDSQ)
! Find direction cosines of AB and CD.  These are local Z-axes.
! If indeterminate, take along space Z-axis.
      IF(RAB.NE.ZERO)then
        P31=(XB-XA)/RAB
        P32=(YB-YA)/RAB
        P33=(ZB-ZA)/RAB
      else
        P31=ZERO
        P32=ZERO
        P33=ONE
      end IF
      IF(RCD.NE.ZERO)then
        Q31=(XD-XC)/RCD
        Q32=(YD-YC)/RCD
        Q33=(ZD-ZC)/RCD
      else
        Q31=ZERO
        Q32=ZERO
        Q33=ONE
      end IF
! Find local Y-axis as common perpendicular to AB and CD.
! If indeterminate, take perpendicular to AB and space Z-axis.
! If still indeterminate, take perpendicular to AB and space X-axis.
      COSG=P31*Q31+P32*Q32+P33*Q33
      COSG=DMIN1(ONE,COSG)
      COSG=DMAX1(-ONE,COSG)
      SING=DSQRT(ONE-COSG*COSG)
! Modified rotation testing - Code from gaussIAN 76.
      IF(DABS(COSG).LE.PT9)then
        P21=(P32*Q33-P33*Q32)/SING
        P22=(P33*Q31-P31*Q33)/SING
        P23=(P31*Q32-P32*Q31)/SING
      else
! Calculate (P-Q)*(P+Q), etc.
        PPQ1=P31+Q31
        PPQ2=P32+Q32
        PPQ3=P33+Q33
        PMQ1=P31-Q31
        PMQ2=P32-Q32
        PMQ3=P33-Q33
        P21=PMQ2*PPQ3-PPQ2*PMQ3
        P22=PMQ3*PPQ1-PPQ3*PMQ1
        P23=PMQ1*PPQ2-PPQ1*PMQ2
        P2=DSQRT(P21*P21+P22*P22+P23*P23)
        SING=PT5*P2
        IF(SING.GE.TENM12)then
          TEMP=ONE/P2
          P21=P21*TEMP
          P22=P22*TEMP
          P23=P23*TEMP
        else if(DABS(P31).LE.PT7)then
          P3131=P31*P31
          P3131=DMIN1(ONE,P3131)
          SINP=DSQRT(ONE-P3131)
          P21=ZERO
          P22=P33/SINP
          P23=-P32/SINP
        else
          P3333=P33*P33
          P3333=DMIN1(ONE,P3333)
          SINP=DSQRT(ONE-P3333)
          P21=P32/SINP
          P22=-P31/SINP
          P23=ZERO
        end IF
      end IF
      Q21=P21
      Q22=P22
      Q23=P23
! Find direction cosines of local X-axes.
      P11=P22*P33-P23*P32
      P12=P23*P31-P21*P33
      P13=P21*P32-P22*P31
      Q11=Q22*Q33-Q23*Q32
      Q12=Q23*Q31-Q21*Q33
      Q13=Q21*Q32-Q22*Q31
! Find coordinates of C relative to local axes at A.
      ACX=(XC-XA)*P11+(YC-YA)*P12+(ZC-ZA)*P13
      ACY=(XC-XA)*P21+(YC-YA)*P22+(ZC-ZA)*P23
! Set ACY=0, if close.
      IF(DABS(ACY).LE.PT0001)ACY=ZERO
      ACZ=(XC-XA)*P31+(YC-YA)*P32+(ZC-ZA)*P33
      ACY2=ACY*ACY
!     ******************************************************************
!     Direction cosines of CD local axes with respect to AB local axes.
!     (COSG,0,-SING)  (0,1,0)  (SING,0,COSG)
!     ******************************************************************
! End of routine SET_shells
      return
      end subroutine SET_shells
      end module SP_integrals
