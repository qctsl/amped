      subroutine BLD_INT_objects
!****************************************************************************************************************
!       Date last modified June 23, 2000                                                                        *
!       Author: Darryl Reid and R.A. Poirier                                                                    *
!       Description:  Builds a complete list of all the objects which can be created in MUNgauss                *
!       When adding a class or object within a class ensure it is done in alphabetical order                    *
!****************************************************************************************************************
! Modules:
      USE program_objects

      implicit none

! Local scalar:
      integer :: Iobject
!
! Begin:
      OBJ_INT(1:Max_objects)%modality = 'other'
      OBJ_INT(1:Max_objects)%class = 'INT'
      OBJ_INT(1:Max_objects)%depend = .true.
      NINTobjects = 0
!
! Class INTegrals (INT)
      NINTobjects = NINTobjects + 1
      OBJ_INT(NINTobjects)%name = '1EINT'
      OBJ_INT(NINTobjects)%modality = 'AO'
      OBJ_INT(NINTobjects)%routine = 'I1ECLC'
!
      NINTobjects = NINTobjects + 1
      OBJ_INT(NINTobjects)%name = '1EINT'
      OBJ_INT(NINTobjects)%modality = 'MO'
      OBJ_INT(NINTobjects)%routine = 'I1E_AOtoMO'
!
      NINTobjects = NINTobjects + 1
      OBJ_INT(NINTobjects)%name = '1EINT'
      OBJ_INT(NINTobjects)%modality = 'VNE'
      OBJ_INT(NINTobjects)%routine = 'VNE_INTS'
!
      NINTobjects = NINTobjects + 1
      OBJ_INT(NINTobjects)%name = '1EINT'
      OBJ_INT(NINTobjects)%modality = 'DIPOLE'
      OBJ_INT(NINTobjects)%routine = 'DIPOLE_1e'
!
      NINTobjects = NINTobjects + 1
      OBJ_INT(NINTobjects)%name = 'MDIPOLE'
      OBJ_INT(NINTobjects)%modality = 'AO'
      OBJ_INT(NINTobjects)%routine = 'MDIPOLE'
!
      NINTobjects = NINTobjects + 1
      OBJ_INT(NINTobjects)%name = 'MDIPOLE'
      OBJ_INT(NINTobjects)%modality = 'MO'
      OBJ_INT(NINTobjects)%routine = 'MDIPOLE'
!
      NINTobjects = NINTobjects + 1
      OBJ_INT(NINTobjects)%name = 'VDIPOLE'
      OBJ_INT(NINTobjects)%modality = 'AO'
      OBJ_INT(NINTobjects)%routine = 'VDIPOLE'
!
      NINTobjects = NINTobjects + 1
      OBJ_INT(NINTobjects)%name = 'VDIPOLE'
      OBJ_INT(NINTobjects)%modality = 'MO'
      OBJ_INT(NINTobjects)%routine = 'VDIPOLE_MO'
!
      NINTobjects = NINTobjects + 1
      OBJ_INT(NINTobjects)%name = 'MOMENT1'
      OBJ_INT(NINTobjects)%modality = 'AO'
      OBJ_INT(NINTobjects)%routine = 'MOMENT12_AO'
!
      NINTobjects = NINTobjects + 1
      OBJ_INT(NINTobjects)%name = 'MOMENT2'
      OBJ_INT(NINTobjects)%modality = 'AO'
      OBJ_INT(NINTobjects)%routine = 'MOMENT12_AO'
!
      NINTobjects = NINTobjects + 1
      OBJ_INT(NINTobjects)%name = 'MOMENT1'
      OBJ_INT(NINTobjects)%modality = 'MO'
      OBJ_INT(NINTobjects)%routine = 'FIRST_MOMENT_MO'
!
      NINTobjects = NINTobjects + 1
      OBJ_INT(NINTobjects)%name = 'MOMENT2'
      OBJ_INT(NINTobjects)%modality = 'MO'
      OBJ_INT(NINTobjects)%routine = 'SECOND_MOMENT_MO'
!
      NINTobjects = NINTobjects + 1
      OBJ_INT(NINTobjects)%name = '1EINT'
      OBJ_INT(NINTobjects)%modality = 'OVLAP_AO_A_B_BLOCK'
      OBJ_INT(NINTobjects)%routine = 'SABCHA'
!
      NINTobjects = NINTobjects + 1
      OBJ_INT(NINTobjects)%name = '1EINT'
      OBJ_INT(NINTobjects)%modality = 'SM1TV_AO_AB'
      OBJ_INT(NINTobjects)%routine = 'SM1HAB'
!
      NINTobjects = NINTobjects + 1
      OBJ_INT(NINTobjects)%name = '1EINT'
      OBJ_INT(NINTobjects)%modality = 'SSM1_AO_AB_BLOCK'
      OBJ_INT(NINTobjects)%routine = 'SSM1AB'
!
      NINTobjects = NINTobjects + 1
      OBJ_INT(NINTobjects)%name = '1EINT'
      OBJ_INT(NINTobjects)%modality = 'TV_AO_AB'
      OBJ_INT(NINTobjects)%routine = 'HABBLD'
!
      NINTobjects = NINTobjects + 1
      OBJ_INT(NINTobjects)%name = '1EINT'
      OBJ_INT(NINTobjects)%modality = 'TV_AO_MONOMER_A'
      OBJ_INT(NINTobjects)%routine = 'I1TVAB'
!
      NINTobjects = NINTobjects + 1
      OBJ_INT(NINTobjects)%name = '1EINT'
      OBJ_INT(NINTobjects)%modality = 'TV_AO_MONOMER_B'
      OBJ_INT(NINTobjects)%routine = 'I1TVAB'
!
      NINTobjects = NINTobjects + 1
      OBJ_INT(NINTobjects)%name = '1EINT'
      OBJ_INT(NINTobjects)%modality = 'V_AO_AB'
      OBJ_INT(NINTobjects)%routine = 'VABBLD'
!
      NINTobjects = NINTobjects + 1
      OBJ_INT(NINTobjects)%name = '1EINT'
      OBJ_INT(NINTobjects)%modality = 'V_AO_MONOMER_A'
      OBJ_INT(NINTobjects)%routine = 'HAHB'
!
      NINTobjects = NINTobjects + 1
      OBJ_INT(NINTobjects)%name = '1EINT'
      OBJ_INT(NINTobjects)%modality = 'V_AO_MONOMER_B'
      OBJ_INT(NINTobjects)%routine = 'HAHB'
!
      NINTobjects = NINTobjects + 1
      OBJ_INT(NINTobjects)%name = '2EINT'
      OBJ_INT(NINTobjects)%modality = 'COMBINATIONS'
      OBJ_INT(NINTobjects)%routine = 'I2ECLC'
!
      NINTobjects = NINTobjects + 1
      OBJ_INT(NINTobjects)%name = '2EINT'
      OBJ_INT(NINTobjects)%modality = 'RAW'
      OBJ_INT(NINTobjects)%routine = 'I2ECLC'
!
      NINTobjects = NINTobjects + 1
      OBJ_INT(NINTobjects)%name = '2EINT'
      OBJ_INT(NINTobjects)%modality = 'MO'
      OBJ_INT(NINTobjects)%routine = 'I2E_AOtoMO'
!
      NINTobjects = NINTobjects + 1
      OBJ_INT(NINTobjects)%name = '2EINT'
      OBJ_INT(NINTobjects)%modality = 'MO_OVOV'
      OBJ_INT(NINTobjects)%routine = 'I2E_OVOV'
!
      NINTobjects = NINTobjects + 1
      OBJ_INT(NINTobjects)%name = '2EINT'
      OBJ_INT(NINTobjects)%modality = 'OS'
      OBJ_INT(NINTobjects)%routine = 'BLD_2eint'
!
      NINTobjects = NINTobjects + 1
      OBJ_INT(NINTobjects)%name = '2EINTlr'
      OBJ_INT(NINTobjects)%modality = 'OS'
      OBJ_INT(NINTobjects)%routine = 'BLD_lr2eint'
!
      NINTobjects = NINTobjects + 1
      OBJ_INT(NINTobjects)%name = 'INTEGRALS'
      OBJ_INT(NINTobjects)%modality = 'GAMMA_FUNCTION'
      OBJ_INT(NINTobjects)%routine = 'GAMGEN'
!
      NINTobjects = NINTobjects + 1
      OBJ_INT(NINTobjects)%name = 'EAMINT'
      OBJ_INT(NINTobjects)%modality = 'AO'
      OBJ_INT(NINTobjects)%routine = 'BLD_EAMint'
!
      NINTobjects = NINTobjects + 1
      OBJ_INT(NINTobjects)%name = 'RAMINT'
      OBJ_INT(NINTobjects)%modality = 'AO'
      OBJ_INT(NINTobjects)%routine = 'BLD_RAMint'
!
! Dummy Class
      NINTobjects = NINTobjects + 1
      OBJ_INT(NINTobjects)%name = '?'
      OBJ_INT(NINTobjects)%modality = '?'
      OBJ_INT(NINTobjects)%routine = '?'
!
      do Iobject=1,NINTobjects
        OBJ_INT(Iobject)%exist=.false.
        OBJ_INT(Iobject)%Current=.false.
      end do

      return
      end subroutine BLD_INT_objects
