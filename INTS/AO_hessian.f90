      subroutine AO_hessian (rx, ry, rz)
!******************************************************************************
!     Date last modified: February 16, 2018                      Version 3.0  *
!     Authors: JWH                                                            *
!     Description: Determine the value of the hessian of AOs at grid points.  *
!******************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE type_basis_set
      USE matrix_print
      USE QM_defaults
      USE AO_values

      implicit none
!
! Output array:
!
! Local scalars:
      integer :: Ishell,Ifrst,Ilast
      integer :: Istart,Iend
      integer :: Iatmshl
      integer :: LI
      integer :: Iatom, Irange, Igauss
      integer :: IGBGN,JGBGN,IGend,JGend
      integer :: Iao,AOI,I
      double precision :: SQRT3,alpha,EG
      double precision :: rA2,Ax,Ay,Az,rx,ry,rz,rAx,rAy,rAz
!
! Local arrays
      double precision :: SHLINTS(100,6), hess(100,6), chi(100)
!
! Begin:
      SQRT3=DSQRT(THREE)
      AOhess=0.0D0
!
! Begin loop over shells.
!
! Loop over elemental SHELLs
! Loop over Ishell.
      do Ishell=1,Basis%Nshells
        LI=Basis%shell(Ishell)%Xtype
        Istart=Basis%shell(Ishell)%Xstart
        Iend=Basis%shell(Ishell)%XEND
        Irange=Iend-Istart+1
        IGBGN=Basis%shell(Ishell)%EXPBGN
        IGEND=Basis%shell(Ishell)%EXPEND
        Ifrst=Basis%shell(Ishell)%frstSHL
        Ilast=Basis%shell(Ishell)%lastSHL
!
        select case (LI)
! s
        case (0)
          call s_hess
! p
        case (1)
          call p_hess
! d  
        case (2)
          call d_hess
!
        case default
          write(UNIout,*)'ERROR> AO_VAL: unable to determine AO val for LI:',LI
          stop ' ERROR> AO_VAL: AO type not supported'
        end select
!
! End of loop over shells.
      
      end do ! Ishell
!
! End of routine AOhess
      return
      CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine s_hess
!********************************************************************************
!     Date last modified: February 16, 2018                        Version 3.0  *
!     Authors: JWH                                                              *
!     Description: Get hessian of s-type AOs at grid points.                    *
!********************************************************************************
! Modules:

      implicit none
!
! Loop over all atomic shells
      do Iatmshl=Ifrst,Ilast
        Iatom=Basis%atmshl(Iatmshl)%ATMLST
!
        Ax=CARTESIAN(Iatom)%X     ! vector A={Ax,Ay,Az}     
        Ay=CARTESIAN(Iatom)%Y
        Az=CARTESIAN(Iatom)%Z
!
        AOI=Basis%atmshl(Iatmshl)%frstAO-1
!
        SHLINTS(1:100,1:6)=ZERO ! will handle up to <f|f>
! Loop over primitive gaussians
!
        do Igauss=IGBGN,IGEND
!
          alpha=Basis%gaussian(Igauss)%exp
!
          rAx = rx - Ax     ! vector rA= r-A
          rAy = ry - Ay
          rAz = rz - Az

          rA2 = rAx*rAx + rAy*rAy + rAz*rAz   ! (rA)**2

          EG = dexp(-alpha*(rA2))             ! e^(-alpha*rA^2)
!
          hess(1,1) = 2.0D0*alpha*(2.0D0*alpha*rAx**2 - 1.0D0)*EG ! d2/dx2
          hess(1,2) = 2.0D0*alpha*(2.0D0*alpha*rAy**2 - 1.0D0)*EG ! d2/dy2
          hess(1,3) = 2.0D0*alpha*(2.0D0*alpha*rAz**2 - 1.0D0)*EG ! d2/dz2
          hess(1,4) = 2.0D0*alpha*(2.0D0*alpha*rAx*rAy)*EG        ! d2/dxdy
          hess(1,5) = 2.0D0*alpha*(2.0D0*alpha*rAx*rAz)*EG        ! d2/dxdz
          hess(1,6) = 2.0D0*alpha*(2.0D0*alpha*rAy*rAz)*EG        ! d2/dydz
!
! contract 
          SHLINTS(1,1:6) = SHLINTS(1,1:6)+Basis%gaussian(Igauss)%CONTRC*hess(1,1:6)
!
        end do ! Igauss 
!      
! Save in AOhess
        do I=1,Irange
          IAO = AOI+I
          AOhess(IAO,1:6)=SHLINTS(I,1:6)
        end do ! I
!
      end do ! Iatmshl
!
      return
      end subroutine s_hess
      subroutine p_hess
!********************************************************************************
!     Date last modified: February 16, 2018                        Version 3.0  *
!     Authors: JWH                                                              *
!     Description: Get hessian of p-type AOs at grid points.                    *
!********************************************************************************
! Modules:

      implicit none
!
! Loop over all atomic shells
      do Iatmshl=Ifrst,Ilast
        Iatom=Basis%atmshl(Iatmshl)%ATMLST
!
        Ax=CARTESIAN(Iatom)%X     ! vector A={Ax,Ay,Az}     
        Ay=CARTESIAN(Iatom)%Y
        Az=CARTESIAN(Iatom)%Z
!
        AOI=Basis%atmshl(Iatmshl)%frstAO-1
!
        SHLINTS(1:100,1:6)=ZERO ! will handle up to <f|f>
! Loop over primitive gaussians
!
        do Igauss=IGBGN,IGEND
!
          alpha=Basis%gaussian(Igauss)%exp
!
          rAx = rx - Ax     ! vector rA= r-A
          rAy = ry - Ay
          rAz = rz - Az

          rA2 = rAx*rAx + rAy*rAy + rAz*rAz   ! (rA)**2

          EG = dexp(-alpha*(rA2))             ! e^(-alpha*rA^2)
!
! build p-functions
          chi(1) = rAx*EG
          chi(2) = rAy*EG
          chi(3) = rAz*EG
! px
          hess(1,1) = 2.0D0*alpha*(2.0D0*alpha*rAx**2 - 3.0D0)*chi(1)   ! d2/dx2
          hess(1,2) = 2.0D0*alpha*(2.0D0*alpha*rAy**2 - 1.0D0)*chi(1)   ! d2/dy2
          hess(1,3) = 2.0D0*alpha*(2.0D0*alpha*rAz**2 - 1.0D0)*chi(1)   ! d2/dz2
          hess(1,4) = 2.0D0*alpha*(2.0D0*alpha*rAx*rAy*chi(1) - chi(2)) ! d2/dxdy
          hess(1,5) = 2.0D0*alpha*(2.0D0*alpha*rAx*rAz*chi(1) - chi(3)) ! d2/dxdz
          hess(1,6) = 2.0D0*alpha*(2.0D0*alpha*rAy*rAz*chi(1))          ! d2/dydz
! py
          hess(2,1) = 2.0D0*alpha*(2.0D0*alpha*rAx**2 - 1.0D0)*chi(2)   ! d2/dx2
          hess(2,2) = 2.0D0*alpha*(2.0D0*alpha*rAy**2 - 3.0D0)*chi(2)   ! d2/dy2
          hess(2,3) = 2.0D0*alpha*(2.0D0*alpha*rAz**2 - 1.0D0)*chi(2)   ! d2/dz2
          hess(2,4) = 2.0D0*alpha*(2.0D0*alpha*rAx*rAy*chi(2) - chi(1)) ! d2/dxdy
          hess(2,5) = 2.0D0*alpha*(2.0D0*alpha*rAx*rAz*chi(2))          ! d2/dxdz
          hess(2,6) = 2.0D0*alpha*(2.0D0*alpha*rAy*rAz*chi(2) - chi(3)) ! d2/dydz
! pz
          hess(3,1) = 2.0D0*alpha*(2.0D0*alpha*rAx**2 - 1.0D0)*chi(3)   ! d2/dx2
          hess(3,2) = 2.0D0*alpha*(2.0D0*alpha*rAy**2 - 1.0D0)*chi(3)   ! d2/dy2
          hess(3,3) = 2.0D0*alpha*(2.0D0*alpha*rAz**2 - 3.0D0)*chi(3)   ! d2/dz2
          hess(3,4) = 2.0D0*alpha*(2.0D0*alpha*rAx*rAy*chi(3))          ! d2/dxdy
          hess(3,5) = 2.0D0*alpha*(2.0D0*alpha*rAx*rAz*chi(3) - chi(1)) ! d2/dxdz
          hess(3,6) = 2.0D0*alpha*(2.0D0*alpha*rAy*rAz*chi(3) - chi(2)) ! d2/dydz
!
! contract 
          SHLINTS(1:3,1:6) = SHLINTS(1:3,1:6)+Basis%gaussian(Igauss)%CONTRC*hess(1:3,1:6)
!
        end do ! Igauss 
!      
! Save in dAO
        do I=1,Irange
          IAO = AOI+I
          AOhess(IAO,1:6)=SHLINTS(I,1:6)
        end do ! I
!
      end do ! Iatmshl
!
      return
      end subroutine p_hess
      subroutine d_hess
!********************************************************************************
!     Date last modified: February 16, 2018                        Version 3.0  *
!     Authors: JWH                                                              *
!     Description: Get hessian of d-type AOs at grid points.                    *
!********************************************************************************
! Modules:

      implicit none
!
! Loop over all atomic shells
      do Iatmshl=Ifrst,Ilast
        Iatom=Basis%atmshl(Iatmshl)%ATMLST
!
        Ax=CARTESIAN(Iatom)%X     ! vector A={Ax,Ay,Az}     
        Ay=CARTESIAN(Iatom)%Y
        Az=CARTESIAN(Iatom)%Z
!
        AOI=Basis%atmshl(Iatmshl)%frstAO-1
!
        SHLINTS(1:100,1:6)=ZERO ! will handle up to <f|f>
! Loop over primitive gaussians
!
        do Igauss=IGBGN,IGEND
!
          alpha=Basis%gaussian(Igauss)%exp
!
          rAx = rx - Ax     ! vector rA= r-A
          rAy = ry - Ay
          rAz = rz - Az

          rA2 = rAx*rAx + rAy*rAy + rAz*rAz   ! (rA)**2

          EG = dexp(-alpha*(rA2))             ! e^(-alpha*rA^2)
!
! build d-functions
          chi(1) = rAx*rAx*EG
          chi(2) = rAy*rAy*EG
          chi(3) = rAz*rAz*EG
          chi(4) = rAx*rAy*EG ! save sqrt3 factor for contraction step
          chi(5) = rAx*rAz*EG
          chi(6) = rAy*rAz*EG
!
! del2dxx
          hess(1,1) = 2.0D0*alpha*(2.0D0*alpha*rAx**2 - 5.0D0)*chi(1) + 2.0D0*EG ! d2/dx2
          hess(1,2) = 2.0D0*alpha*(2.0D0*alpha*rAy**2 - 1.0D0)*chi(1)            ! d2/dy2
          hess(1,3) = 2.0D0*alpha*(2.0D0*alpha*rAz**2 - 1.0D0)*chi(1)            ! d2/dz2
          hess(1,4) = 2.0D0*alpha*((2.0D0*alpha*rAx*rAy)*chi(1) - 2.0D0*chi(4))  ! d2/dxdy
          hess(1,5) = 2.0D0*alpha*((2.0D0*alpha*rAx*rAz)*chi(1) - 2.0D0*chi(5))  ! d2/dxdz
          hess(1,6) = 2.0D0*alpha*(2.0D0*alpha*rAy*rAz)*chi(1)                   ! d2/dydz
! del2dyy
          hess(2,1) = 2.0D0*alpha*(2.0D0*alpha*rAx**2 - 1.0D0)*chi(2)            ! d2/dx2
          hess(2,2) = 2.0D0*alpha*(2.0D0*alpha*rAy**2 - 5.0D0)*chi(2) + 2.0D0*EG ! d2/dy2
          hess(2,3) = 2.0D0*alpha*(2.0D0*alpha*rAz**2 - 1.0D0)*chi(2)            ! d2/dz2
          hess(2,4) = 2.0D0*alpha*((2.0D0*alpha*rAx*rAy)*chi(2) - 2.0D0*chi(4))  ! d2/dxdy
          hess(2,5) = 2.0D0*alpha*(2.0D0*alpha*rAx*rAz)*chi(2)                   ! d2/dxdz
          hess(2,6) = 2.0D0*alpha*((2.0D0*alpha*rAy*rAz)*chi(2) - 2.0D0*chi(6))  ! d2/dydz
! del2dzz
          hess(3,1) = 2.0D0*alpha*(2.0D0*alpha*rAx**2 - 1.0D0)*chi(3)            ! d2/dx2
          hess(3,2) = 2.0D0*alpha*(2.0D0*alpha*rAy**2 - 1.0D0)*chi(3)            ! d2/dy2
          hess(3,3) = 2.0D0*alpha*(2.0D0*alpha*rAz**2 - 5.0D0)*chi(3) + 2.0D0*EG ! d2/dz2
          hess(3,4) = 2.0D0*alpha*(2.0D0*alpha*rAx*rAy)*chi(3)                   ! d2/dxdy
          hess(3,5) = 2.0D0*alpha*((2.0D0*alpha*rAx*rAz)*chi(3) - 2.0D0*chi(5))  ! d2/dxdz
          hess(3,6) = 2.0D0*alpha*((2.0D0*alpha*rAy*rAz)*chi(3) - 2.0D0*chi(6))  ! d2/dydz
!
! del2dxy
          hess(4,1) = 2.0D0*alpha*(2.0D0*alpha*rAx**2 - 3.0D0)*chi(4)                 ! d2/dx2
          hess(4,2) = 2.0D0*alpha*(2.0D0*alpha*rAy**2 - 3.0D0)*chi(4)                 ! d2/dy2
          hess(4,3) = 2.0D0*alpha*(2.0D0*alpha*rAz**2 - 1.0D0)*chi(4)                 ! d2/dy2
          hess(4,4) = 2.0D0*alpha*(2.0D0*alpha*rAx*rAy*chi(4) - chi(1) - chi(2)) + EG ! d2/dxy
          hess(4,5) = 2.0D0*alpha*(2.0D0*alpha*rAx*rAz*chi(4) - chi(6))               ! d2/dxz
          hess(4,6) = 2.0D0*alpha*(2.0D0*alpha*rAy*rAz*chi(4) - chi(5))               ! d2/dyz
! del2dxz
          hess(5,1) = 2.0D0*alpha*(2.0D0*alpha*rAx**2 - 3.0D0)*chi(5)                 ! d2/dx2
          hess(5,2) = 2.0D0*alpha*(2.0D0*alpha*rAy**2 - 1.0D0)*chi(5)                 ! d2/dy2
          hess(5,3) = 2.0D0*alpha*(2.0D0*alpha*rAz**2 - 3.0D0)*chi(5)                 ! d2/dy2
          hess(5,4) = 2.0D0*alpha*(2.0D0*alpha*rAx*rAy*chi(5) - chi(6))               ! d2/dxy
          hess(5,5) = 2.0D0*alpha*(2.0D0*alpha*rAx*rAz*chi(5) - chi(1) - chi(3)) + EG ! d2/dxz
          hess(5,6) = 2.0D0*alpha*(2.0D0*alpha*rAy*rAz*chi(5) - chi(4))               ! d2/dyz
! del2dyz
          hess(6,1) = 2.0D0*alpha*(2.0D0*alpha*rAx**2 - 1.0D0)*chi(6)                 ! d2/dx2
          hess(6,2) = 2.0D0*alpha*(2.0D0*alpha*rAy**2 - 3.0D0)*chi(6)                 ! d2/dy2
          hess(6,3) = 2.0D0*alpha*(2.0D0*alpha*rAz**2 - 3.0D0)*chi(6)                 ! d2/dy2
          hess(6,4) = 2.0D0*alpha*(2.0D0*alpha*rAx*rAy*chi(6) - chi(5))               ! d2/dxy
          hess(6,5) = 2.0D0*alpha*(2.0D0*alpha*rAx*rAz*chi(6) - chi(4))               ! d2/dxz
          hess(6,6) = 2.0D0*alpha*(2.0D0*alpha*rAy*rAz*chi(6) - chi(2) - chi(3)) + EG ! d2/dyz
!
! contract 
          SHLINTS(1,1:6) = SHLINTS(1,1:6)+Basis%gaussian(Igauss)%CONTRC*hess(1,1:6)
          SHLINTS(2,1:6) = SHLINTS(2,1:6)+Basis%gaussian(Igauss)%CONTRC*hess(2,1:6)
          SHLINTS(3,1:6) = SHLINTS(3,1:6)+Basis%gaussian(Igauss)%CONTRC*hess(3,1:6)
          SHLINTS(4,1:6) = SHLINTS(4,1:6)+Basis%gaussian(Igauss)%CONTRC*hess(4,1:6)*SQRT3
          SHLINTS(5,1:6) = SHLINTS(5,1:6)+Basis%gaussian(Igauss)%CONTRC*hess(5,1:6)*SQRT3
          SHLINTS(6,1:6) = SHLINTS(6,1:6)+Basis%gaussian(Igauss)%CONTRC*hess(6,1:6)*SQRT3
!
        end do ! Igauss 
!      
! Save in AOhess
        do I=1,Irange
          IAO = AOI+I
          AOhess(IAO,1:6)=SHLINTS(I,1:6)
        end do ! I
!
      end do ! Iatmshl
!
      return
      end subroutine d_hess
      end subroutine AO_hessian
