      MODULE AO_values
!**************************************************************************************************
!     Date last modified: April 12, 2016                                                          *
!     Author: JWH                                                                                 *
!     Description:  AO values and derivatives at grid points                                      *
!**************************************************************************************************
      implicit none
!
      double precision, allocatable, dimension(:) :: AOval, dAOx, dAOy, dAOz, d2AO
      double precision, allocatable, dimension(:,:) :: AOhess
!
      end MODULE AO_values
