      subroutine GET_INT_object (class, Objname, Modality)
!**************************************************************************
!     Date last modified: June 21, 2001                       Version 2.0 *
!     Author: R.A. Poirier                                                *
!     Description: Objects belonging to the class INT (Integrals)         *
!**************************************************************************
! Modules:
      USE program_files
      USE program_objects

      implicit none
!
! Input scalars:
      character*(*) :: class
      character*(*) :: Objname
      character*(*) :: Modality
!
! Local scalars:
      integer :: Object_number
!
! Begin:
      call PRG_manager ('enter', 'GET_INT_object', 'UTILITY')

      call GET_object_number (OBJ_INT, NINTobjects, FirstINT, Objname, Modality, Object_number)

      if(OBJ_INT(Object_number)%current)then
        call PRG_manager ('exit', 'GET_INT_object', 'UTILITY')
        return
      end if

      OBJ_INT(Object_number)%exist=.true.
      OBJ_INT(Object_number)%Current=.true.
      Object(ObjNum)%exist=.true.
      Object(ObjNum)%Current=.true.

      select_Object: select case (Objname)

      case ('1EINT')
        select case (Modality)
        case ('AO')
          call i1eclc
        case ('MO')
          call I1E_AOtoMO
        case ('DIPOLE')
          call Dipole_1e
        case ('VNE')
          call Vne_Ints
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select

      case ('VDIPOLE')
        select case (Modality)
        case ('AO')
          call VDipole
        case ('MO')
          call VDipole_MO
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select

      case ('MDIPOLE')
        select case (Modality)
        case ('AO')
          call MDipole
        case ('MO')
          call MDipole_MO
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select

      case ('MOMENT1','MOMENT2')
        select case (Modality)
          case ('AO')
          call MOMENT12_AO
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select

      case ('2EINT') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
        case ('COMBINATIONS', 'RAW')
          call I2ECLC (Modality)
        case ('MO')
          call I2E_AOtoMO
        case ('MO_OVOV')
          call I2E_OVOV
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select

      case ('INTEGRALS') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
        case ('GAMMA_FUNCTION')
          call gamgen
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select

      case ('EAMINT') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
        case ('AO')
          call BLD_EAMint
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select

      case ('RAMINT') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
        case ('AO')
          call BLD_RAMint
        case default
          write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
          stop 'No such object'
        end select

      end select select_Object

      call PRG_manager ('exit', 'GET_INT_object', 'UTILITY')
      return
      end subroutine GET_INT_object
