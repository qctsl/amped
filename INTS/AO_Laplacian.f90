      subroutine AO_Laplacian (rx, ry, rz)
!******************************************************************************
!     Date last modified: August 20, 2015                        Version 3.0  *
!     Authors: JWH                                                            *
!     Description: Determine the value of the Laplacian of AOs at grid points.*
!******************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE type_basis_set
      USE matrix_print
      USE QM_defaults
      USE AO_values

      implicit none
!
! Output array:
!
! Local scalars:
      integer :: Ishell,Ifrst,Ilast
      integer :: Istart,Iend
      integer :: Iatmshl
      integer :: LI
      integer :: Iatom, Irange, Igauss
      integer :: IGBGN,JGBGN,IGend,JGend
      integer :: Iao,AOI,I
      double precision :: SQRT3,alpha,EG
      double precision :: rA2,Ax,Ay,Az,rx,ry,rz,rAx,rAy,rAz
!
! Local arrays
      double precision :: SHLINTS(100), lap(100), chi(100)
!
! Begin:
      SQRT3=DSQRT(THREE)
      d2AO=0.0D0
!
! Begin loop over shells.
!
! Loop over elemental SHELLs
! Loop over Ishell.
      do Ishell=1,Basis%Nshells
        LI=Basis%shell(Ishell)%Xtype
        Istart=Basis%shell(Ishell)%Xstart
        Iend=Basis%shell(Ishell)%XEND
        Irange=Iend-Istart+1
        IGBGN=Basis%shell(Ishell)%EXPBGN
        IGEND=Basis%shell(Ishell)%EXPEND
        Ifrst=Basis%shell(Ishell)%frstSHL
        Ilast=Basis%shell(Ishell)%lastSHL
!
        select case (LI)
! s
        case (0)
          call s_lap
! p
        case (1)
          call p_lap
! d  
        case (2)
          call d_lap
!
        case default
          write(UNIout,*)'ERROR> AO_VAL: unable to determine AO val for LI:',LI
          stop ' ERROR> AO_VAL: AO type not supported'
        end select
!
! End of loop over shells.
      
      end do ! Ishell
!
! End of routine dAO
      return
      CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine s_lap 
!********************************************************************************
!     Date last modified: August 20,2015                           Version 3.0  *
!     Authors: JWH                                                              *
!     Description: Get Laplacian of s-type AOs at grid points.                  *
!********************************************************************************
! Modules:

      implicit none
!
! Loop over all atomic shells
      do Iatmshl=Ifrst,Ilast
        Iatom=Basis%atmshl(Iatmshl)%ATMLST
!
        Ax=CARTESIAN(Iatom)%X     ! vector A={Ax,Ay,Az}     
        Ay=CARTESIAN(Iatom)%Y
        Az=CARTESIAN(Iatom)%Z
!
        AOI=Basis%atmshl(Iatmshl)%frstAO-1
!
        SHLINTS(1:100)=ZERO ! will handle up to <f|f>
! Loop over primitive gaussians
!
        do Igauss=IGBGN,IGEND
!
          alpha=Basis%gaussian(Igauss)%exp
!
          rAx = rx - Ax     ! vector rA= r-A
          rAy = ry - Ay
          rAz = rz - Az

          rA2 = rAx*rAx + rAy*rAy + rAz*rAz   ! (rA)**2

          EG = dexp(-alpha*(rA2))             ! e^(-alpha*rA^2)
!
          lap(1) = 2.0D0*alpha*(2.0D0*alpha*rAx**2 - 1.0D0)*EG& ! d2/dx2
                 + 2.0D0*alpha*(2.0D0*alpha*rAy**2 - 1.0D0)*EG& ! d2/dy2
                 + 2.0D0*alpha*(2.0D0*alpha*rAz**2 - 1.0D0)*EG  ! d2/dz2
!
! contract 
          SHLINTS(1) = SHLINTS(1)+Basis%gaussian(Igauss)%CONTRC*lap(1)
!
        end do ! Igauss 
!      
! Save in dAO
        do I=1,Irange
          IAO = AOI+I
          d2AO(IAO)=SHLINTS(I)
        end do ! I
!
      end do ! Iatmshl
!
      return
      end subroutine s_lap
      subroutine p_lap
!********************************************************************************
!     Date last modified: August 20, 2015                          Version 3.0  *
!     Authors: JWH                                                              *
!     Description: Get Laplacian of p-type AOs at grid points.                  *
!********************************************************************************
! Modules:

      implicit none
!
! Loop over all atomic shells
      do Iatmshl=Ifrst,Ilast
        Iatom=Basis%atmshl(Iatmshl)%ATMLST
!
        Ax=CARTESIAN(Iatom)%X     ! vector A={Ax,Ay,Az}     
        Ay=CARTESIAN(Iatom)%Y
        Az=CARTESIAN(Iatom)%Z
!
        AOI=Basis%atmshl(Iatmshl)%frstAO-1
!
        SHLINTS(1:100)=ZERO ! will handle up to <f|f>
! Loop over primitive gaussians
!
        do Igauss=IGBGN,IGEND
!
          alpha=Basis%gaussian(Igauss)%exp
!
          rAx = rx - Ax     ! vector rA= r-A
          rAy = ry - Ay
          rAz = rz - Az

          rA2 = rAx*rAx + rAy*rAy + rAz*rAz   ! (rA)**2

          EG = dexp(-alpha*(rA2))             ! e^(-alpha*rA^2)
!
! build p-functions
          chi(1) = rAx*EG
          chi(2) = rAy*EG
          chi(3) = rAz*EG
!
          lap(1) = 2.0D0*alpha*(2.0D0*alpha*rAx**2 - 3.0D0)*chi(1)& ! d2/dx2
                 + 2.0D0*alpha*(2.0D0*alpha*rAy**2 - 1.0D0)*chi(1)& ! d2/dy2
                 + 2.0D0*alpha*(2.0D0*alpha*rAz**2 - 1.0D0)*chi(1)  ! d2/dz2
!
          lap(2) = 2.0D0*alpha*(2.0D0*alpha*rAx**2 - 1.0D0)*chi(2)& ! d2/dx2
                 + 2.0D0*alpha*(2.0D0*alpha*rAy**2 - 3.0D0)*chi(2)& ! d2/dy2
                 + 2.0D0*alpha*(2.0D0*alpha*rAz**2 - 1.0D0)*chi(2)  ! d2/dz2
!
          lap(3) = 2.0D0*alpha*(2.0D0*alpha*rAx**2 - 1.0D0)*chi(3)& ! d2/dx2
                 + 2.0D0*alpha*(2.0D0*alpha*rAy**2 - 1.0D0)*chi(3)& ! d2/dy2
                 + 2.0D0*alpha*(2.0D0*alpha*rAz**2 - 3.0D0)*chi(3)  ! d2/dz2
!
! contract 
          SHLINTS(1:3) = SHLINTS(1:3)+Basis%gaussian(Igauss)%CONTRC*lap(1:3)
!
        end do ! Igauss 
!      
! Save in dAO
        do I=1,Irange
          IAO = AOI+I
          d2AO(IAO)=SHLINTS(I)
        end do ! I
!
      end do ! Iatmshl
!
      return
      end subroutine p_lap
      subroutine d_lap
!********************************************************************************
!     Date last modified: August 20, 2015                          Version 3.0  *
!     Authors: JWH                                                              *
!     Description: Get Laplacian of d-type AOs at grid points.                  *
!********************************************************************************
! Modules:

      implicit none
!
! Loop over all atomic shells
      do Iatmshl=Ifrst,Ilast
        Iatom=Basis%atmshl(Iatmshl)%ATMLST
!
        Ax=CARTESIAN(Iatom)%X     ! vector A={Ax,Ay,Az}     
        Ay=CARTESIAN(Iatom)%Y
        Az=CARTESIAN(Iatom)%Z
!
        AOI=Basis%atmshl(Iatmshl)%frstAO-1
!
        SHLINTS(1:100)=ZERO ! will handle up to <f|f>
! Loop over primitive gaussians
!
        do Igauss=IGBGN,IGEND
!
          alpha=Basis%gaussian(Igauss)%exp
!
          rAx = rx - Ax     ! vector rA= r-A
          rAy = ry - Ay
          rAz = rz - Az

          rA2 = rAx*rAx + rAy*rAy + rAz*rAz   ! (rA)**2

          EG = dexp(-alpha*(rA2))             ! e^(-alpha*rA^2)
!
! build d-functions
          chi(1) = rAx*rAx*EG
          chi(2) = rAy*rAy*EG
          chi(3) = rAz*rAz*EG
          chi(4) = rAx*rAy*EG ! save sqrt3 factor for contraction step
          chi(5) = rAx*rAz*EG
          chi(6) = rAy*rAz*EG
!
! del2dxx
          lap(1) = &
          2.0D0*EG + 2.0D0*alpha*(2.0D0*alpha*rAx**2 - 5.0D0)*chi(1)& ! d2/dx2
                   + 2.0D0*alpha*(2.0D0*alpha*rAy**2 - 1.0D0)*chi(1)& ! d2/dy2
                   + 2.0D0*alpha*(2.0D0*alpha*rAz**2 - 1.0D0)*chi(1)  ! d2/dz2
! del2dyy
          lap(2) = &
                     2.0D0*alpha*(2.0D0*alpha*rAx**2 - 1.0D0)*chi(2)& ! d2/dx2
        + 2.0D0*EG + 2.0D0*alpha*(2.0D0*alpha*rAy**2 - 5.0D0)*chi(2)& ! d2/dy2
                   + 2.0D0*alpha*(2.0D0*alpha*rAz**2 - 1.0D0)*chi(2)  ! d2/dz2
! del2dzz
          lap(3) = &
                     2.0D0*alpha*(2.0D0*alpha*rAx**2 - 1.0D0)*chi(3)& ! d2/dx2
                   + 2.0D0*alpha*(2.0D0*alpha*rAy**2 - 1.0D0)*chi(3)& ! d2/dy2
        + 2.0D0*EG + 2.0D0*alpha*(2.0D0*alpha*rAz**2 - 5.0D0)*chi(3)  ! d2/dz2
! del2dxy
          lap(4) = &
                     2.0D0*alpha*(2.0D0*alpha*rAx**2 - 3.0D0)*chi(4)& ! d2/dx2
                   + 2.0D0*alpha*(2.0D0*alpha*rAy**2 - 3.0D0)*chi(4)& ! d2/dy2
                   + 2.0D0*alpha*(2.0D0*alpha*rAz**2 - 1.0D0)*chi(4)  ! d2/dz2
! del2dxz
          lap(5) = &
                     2.0D0*alpha*(2.0D0*alpha*rAx**2 - 3.0D0)*chi(5)& ! d2/dx2
                   + 2.0D0*alpha*(2.0D0*alpha*rAy**2 - 1.0D0)*chi(5)& ! d2/dy2
                   + 2.0D0*alpha*(2.0D0*alpha*rAz**2 - 3.0D0)*chi(5)  ! d2/dz2
! del2dyz
          lap(6) = &
                     2.0D0*alpha*(2.0D0*alpha*rAx**2 - 1.0D0)*chi(6)& ! d2/dx2
                   + 2.0D0*alpha*(2.0D0*alpha*rAy**2 - 3.0D0)*chi(6)& ! d2/dy2
                   + 2.0D0*alpha*(2.0D0*alpha*rAz**2 - 3.0D0)*chi(6)  ! d2/dz2
!
! contract 
          SHLINTS(1) = SHLINTS(1)+Basis%gaussian(Igauss)%CONTRC*lap(1)
          SHLINTS(2) = SHLINTS(2)+Basis%gaussian(Igauss)%CONTRC*lap(2)
          SHLINTS(3) = SHLINTS(3)+Basis%gaussian(Igauss)%CONTRC*lap(3)
          SHLINTS(4) = SHLINTS(4)+Basis%gaussian(Igauss)%CONTRC*lap(4)*SQRT3
          SHLINTS(5) = SHLINTS(5)+Basis%gaussian(Igauss)%CONTRC*lap(5)*SQRT3
          SHLINTS(6) = SHLINTS(6)+Basis%gaussian(Igauss)%CONTRC*lap(6)*SQRT3
!
        end do ! Igauss 
!      
! Save in AOval
        do I=1,Irange
          IAO = AOI+I
          d2AO(IAO)=SHLINTS(I)
        end do ! I
!
      end do ! Iatmshl
!
      return
      end subroutine d_lap
      end subroutine AO_laplacian
