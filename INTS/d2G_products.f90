      subroutine d2G_products (rx, ry, rz)
!**************************************************************************************************
!     Date last modified: December 7, 2015                                                        *
!     Authors: Jessica Besaw, Ahmad and R.A. Poirier                                              *
!     Description: Note about derivatives:                                                        *
!     When we take d2Gderivative in terms of x, y, and z, we are taking it in                    *
!     terms of the 'rx, ry, and rz'. Note the following derivatives                               *      
!      d/dx [ rpx ] = d/dx [ Px-rx ] =  -1  --> Px = constant, rx = 'x' variable                  *
!      d/dx [ xA  ] = d/dx [ rx-Ax ] =   1  --> Ax = constant, rx = 'x' variable                  *
!**************************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE type_basis_set
      USE matrix_print
      USE QM_defaults

      implicit none
!
! Output array:
!
! Local scalars:
      integer :: MATlen
      integer :: Ishell,Jshell,Ifrst,Jfrst,Ilast,Jlast
      integer :: Istart,Jstart,Iend,Jend
      integer :: Iatmshl,Jatmshl
      integer :: LAMAX,LBMAX
      integer :: Iatom,Jatom
      integer :: Irange,Jrange
      integer :: Igauss,Jgauss
      integer :: IGBGN,JGBGN,IGend,JGend
      integer :: Iao,Jao,AOI,AOJ
      integer :: I,J,K,INDEX,DEFCASE,IJao
      double precision :: COEF_IJ,COEF_SXY,COEF_PXY
      double precision :: COEF_XXXY,COEF_XYXY,SQRT3,SUMDSQ,Gama,Alpha,Beta,RABSQ,EG,EG_K,EG_exp
      double precision :: Ax,Bx,Ay,By,Az,Bz,rx,ry,rz,Px,Py,Pz,xA,yA,zA,xB,yB,zB,rpx,rpy,rpz
      double precision :: dEGx,dEGy,dEGz
      double precision :: d2EGxx,d2EGyy,d2EGzz,d2EGxy,d2EGxz,d2EGyz
      logical :: LIatmshl
!
! Local arrays  
      double precision :: d2AOXX(100),d2AOYY(100),d2AOZZ(100), &
                        & d2AOXY(100),d2AOXZ(100),d2AOYZ(100)      ! save derivatives
      double precision, dimension(:), allocatable  :: d2AOprodXX,d2AOprodYY,d2AOprodZZ,d2AOprodXY,d2AOprodXZ,d2AOprodYZ
!
! Begin:
      SQRT3=DSQRT(THREE)
!
      MATlen=size(d2AOprodXX)
      d2AOprodXX(1:MATlen)=ZERO
      d2AOprodYY(1:MATlen)=ZERO
      d2AOprodZZ(1:MATlen)=ZERO
      d2AOprodXY(1:MATlen)=ZERO
      d2AOprodXZ(1:MATlen)=ZERO
      d2AOprodYZ(1:MATlen)=ZERO
!
! Begin loop over shells.
! Loop over elemental SHELLs
! Loop over Ishell.
      do Ishell=1,Basis%Nshells
        LAMAX=Basis%shell(Ishell)%Xtype+1
        Istart=Basis%shell(Ishell)%Xstart
        Iend=Basis%shell(Ishell)%XEND
        Irange=Iend-Istart+1
        IGBGN=Basis%shell(Ishell)%EXPBGN
        IGEND=Basis%shell(Ishell)%EXPEND
        Ifrst=Basis%shell(Ishell)%frstSHL
!
! Loop over Jshell.
      do Jshell=1,Ishell
        LBMAX=Basis%shell(Jshell)%Xtype+1
        Jstart=Basis%shell(Jshell)%Xstart
        Jend=Basis%shell(Jshell)%XEND
        Jrange=Jend-Jstart+1
        JGBGN=Basis%shell(Jshell)%EXPBGN
        JGEND=Basis%shell(Jshell)%EXPEND
        Jfrst=Basis%shell(Jshell)%frstSHL
!
! Define CASE:
      if(LAMAX.lt.LBMAX)then
       DEFCASE=4*(LAMAX-1)+LBMAX
      else
       DEFCASE=4*(LBMAX-1)+LAMAX
      end if
!
      Ilast= Basis%shell(Ishell)%lastSHL
      Jlast= Basis%shell(Jshell)%lastSHL
!
      call AB_d2products
!
! End of loop over shells.
      
      end do ! Jshell
      end do ! Ishell
!
! End of routine dAO_PRODUCTS
      return
      CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine AB_d2products
!************************************************************************************************************
!     Date last modified: July 31, 2012                                                        Version 3.0  *
!     Authors: Jessica Besaw, Ahmad and R.A. Poirier                                                        *
!     Description: Calculate the seconded derivative of the product of two S-orbitals                       *
!                                                                                                           *
!************************************************************************************************************
! Modules:

      implicit none
!
! Loop over all atomic shells
      do Iatmshl=Ifrst,Ilast
        Iatom=Basis%atmshl(Iatmshl)%ATMLST
!
        Ax=CARTESIAN(Iatom)%X     ! vector A={Ax,Ay,Az}     
        Ay=CARTESIAN(Iatom)%Y
        Az=CARTESIAN(Iatom)%Z
!
        AOI=Basis%atmshl(Iatmshl)%frstAO-1
        IF(Ishell.EQ.Jshell)Jlast=Iatmshl
      do Jatmshl=Jfrst,Jlast
        Jatom=Basis%atmshl(Jatmshl)%ATMLST
!
        Bx=CARTESIAN(Jatom)%X     ! vector B={Bx,By,Bz}
        By=CARTESIAN(Jatom)%Y
        Bz=CARTESIAN(Jatom)%Z
!
        AOJ=Basis%atmshl(Jatmshl)%frstAO-1
        LIatmshl=.false.
        if(Iatmshl.eq.Jatmshl)LIatmshl=.true.

        RABSQ=(Ax-Bx)*(Ax-Bx)+(Ay-By)*(Ay-By)+(Az-Bz)*(Az-Bz)     ! (A-B)**2

        d2AOXX(1:100)=ZERO     ! will handle up to <f|f>
        d2AOYY(1:100)=ZERO
        d2AOZZ(1:100)=ZERO
        d2AOXY(1:100)=ZERO    
        d2AOXZ(1:100)=ZERO
        d2AOYZ(1:100)=ZERO

! Loop over primitive gaussians
!
      do Igauss=IGBGN,IGEND
        Alpha=Basis%gaussian(Igauss)%exp

      do Jgauss=JGBGN,JGEND
        Beta=Basis%gaussian(Jgauss)%exp
!
        Gama=Alpha+Beta

        Px=(Alpha*Ax+Beta*Bx)/Gama     ! vector P
        Py=(Alpha*Ay+Beta*By)/Gama
        Pz=(Alpha*Az+Beta*Bz)/Gama
!
        rpx = Px-rx     ! vector rp=P-r
        rpy = Py-ry
        rpz = Pz-rz

        SUMDSQ = rpx*rpx + rpy*rpy + rpz*rpz   ! (rp)**2

        EG_K=DEXP(-Alpha*Beta*RABSQ/Gama)      ! K 
        EG_exp=DEXP(-Gama*SUMDSQ)              ! exp
        EG=EG_K*EG_exp                         ! K*exp
!
        xA=rx-Ax     ! vector rA=(r-A)
        yA=ry-Ay
        zA=rz-Az
        xB=rx-Bx     ! vector rB=(r-B)
        yB=ry-By
        zB=rz-Bz
!
      dEGx=TWO*Gama*rpx*EG     ! derivative of EG with respect to x
      dEGy=TWO*Gama*rpy*EG     ! derivative of EG with respect to y
      dEGz=TWO*Gama*rpz*EG     ! derivative of EG with respect to z 
  
      d2EGxx=TWO*Gama*(rpx*dEGx-EG) ! second derivative of EG with respect to x & x
      d2EGyy=TWO*Gama*(rpy*dEGy-EG) ! second derivative of EG with respect to y & y
      d2EGzz=TWO*Gama*(rpz*dEGz-EG) ! second derivative of EG with respect to z & z
      d2EGxy=TWO*Gama*rpx*dEGy      ! second derivative of EG with respect to x & y
      d2EGxz=TWO*Gama*rpx*dEGz      ! second derivative of EG with respect to x & z
      d2EGyz=TWO*Gama*rpy*dEGz      ! second derivative of EG with respect to y & z
  
      COEF_IJ = Basis%gaussian(Igauss)%CONTRC*Basis%gaussian(Jgauss)%CONTRC

! Second derivatives:
      select case (DEFCASE)
     case (1) ! S|S
        d2AOXX(1)=d2AOXX(1)+COEF_IJ*d2EGxx     ! COEF_IJ*EG <-- WHY IS THIS HERE
        d2AOYY(1)=d2AOYY(1)+COEF_IJ*d2EGyy
        d2AOZZ(1)=d2AOZZ(1)+COEF_IJ*d2EGzz
        d2AOXY(1)=d2AOXY(1)+COEF_IJ*d2EGxy     
        d2AOXZ(1)=d2AOXZ(1)+COEF_IJ*d2EGxz
        d2AOYZ(1)=d2AOYZ(1)+COEF_IJ*d2EGyz
      case (2) ! S|P
        d2AOXX(1)=d2AOXX(1)+COEF_IJ*(TWO*dEGx+xB*d2EGxx)
        d2AOXY(1)=d2AOXY(1)+COEF_IJ*(xB*d2EGxy+dEGy)
        d2AOXZ(1)=d2AOXZ(1)+COEF_IJ*(xB*d2EGxz+dEGz)
        d2AOYY(1)=d2AOYY(1)+COEF_IJ*xB*d2EGyy
        d2AOYZ(1)=d2AOYZ(1)+COEF_IJ*xB*d2EGyz
        d2AOZZ(1)=d2AOZZ(1)+COEF_IJ*xB*d2EGzz

        d2AOXX(2)=d2AOXX(2)+COEF_IJ*yB*d2EGxx            
        d2AOXY(2)=d2AOXY(2)+COEF_IJ*(yB*d2EGxy+dEGx)
        d2AOXZ(2)=d2AOXZ(2)+COEF_IJ*yB*d2EGxz 
        d2AOYY(2)=d2AOYY(2)+COEF_IJ*(TWO*dEGy+yB*d2EGyy)
        d2AOYZ(2)=d2AOYZ(2)+COEF_IJ*(yB*d2EGyz+dEGz)
        d2AOZZ(2)=d2AOZZ(2)+COEF_IJ*yB*d2EGzz

        d2AOXX(3)=d2AOXX(3)+COEF_IJ*zB*d2EGxx          
        d2AOXY(3)=d2AOXY(3)+COEF_IJ*zB*d2EGxy
        d2AOXZ(3)=d2AOXZ(3)+COEF_IJ*(zB*d2EGxz+dEGx)
        d2AOYY(3)=d2AOYY(3)+COEF_IJ*zB*d2EGyy
        d2AOYZ(3)=d2AOYZ(3)+COEF_IJ*(zB*d2EGyz+dEGy)
        d2AOZZ(3)=d2AOZZ(3)+COEF_IJ*(TWO*dEGz+zB*d2EGzz)
!
      case (3) ! S|D
        COEF_SXY = SQRT3*COEF_IJ
        d2AOXX(1)=d2AOXX(1)+COEF_IJ*(xB*xB*d2EGxx+FOUR*xB*dEGx+TWO*EG)
        d2AOXY(1)=d2AOXY(1)+COEF_IJ*(xB*xB*d2EGxy+TWO*xB*dEGy)
        d2AOXZ(1)=d2AOXZ(1)+COEF_IJ*(xB*xB*d2EGxz+TWO*xB*dEGz)
        d2AOYY(1)=d2AOYY(1)+COEF_IJ*xB*xB*d2EGyy
        d2AOYZ(1)=d2AOYZ(1)+COEF_IJ*xB*xB*d2EGyz
        d2AOZZ(1)=d2AOZZ(1)+COEF_IJ*xB*xB*d2EGzz
              
        d2AOXX(2)=d2AOXX(2)+COEF_IJ*yB*yB*d2EGxx 
        d2AOXY(2)=d2AOXY(2)+COEF_IJ*(yB*yB*d2EGxy+TWO*yB*dEGx) 
        d2AOXZ(2)=d2AOXZ(2)+COEF_IJ*yB*yB*d2EGxz 
        d2AOYY(2)=d2AOYY(2)+COEF_IJ*(yB*yB*d2EGyy+FOUR*yB*dEGy+TWO*EG)
        d2AOYZ(2)=d2AOYZ(2)+COEF_IJ*(yB*yB*d2EGyz+TWO*yB*dEGz)
        d2AOZZ(2)=d2AOZZ(2)+COEF_IJ*yB*yB*d2EGzz
                 
        d2AOXX(3)=d2AOXX(3)+COEF_IJ*zB*zB*d2EGxx
        d2AOXY(3)=d2AOXY(3)+COEF_IJ*zB*zB*d2EGxy
        d2AOXZ(3)=d2AOXZ(3)+COEF_IJ*(zB*zB*d2EGxz+TWO*zB*dEGx)
        d2AOYY(3)=d2AOYY(3)+COEF_IJ*zB*zB*d2EGyy
        d2AOYZ(3)=d2AOYZ(3)+COEF_IJ*(zB*zB*d2EGyz+TWO*zB*dEGy)
        d2AOZZ(3)=d2AOZZ(3)+COEF_IJ*(zB*zB*d2EGzz+FOUR*zB*dEGz+TWO*EG)
     
        d2AOXX(4)=d2AOXX(4)+COEF_SXY*(TWO*yB*dEGx+xB*yB*d2EGxx)
        d2AOXY(4)=d2AOXY(4)+COEF_SXY*(xB*dEGx+xB*yB*d2EGxy+EG+yB*dEGy)
        d2AOXZ(4)=d2AOXZ(4)+COEF_SXY*(xB*yB*d2EGxz+yB*dEGz)
        d2AOYY(4)=d2AOYY(4)+COEF_SXY*(TWO*xB*dEGy+xB*yB*d2EGyy)
        d2AOYZ(4)=d2AOYZ(4)+COEF_SXY*(xB*yB*d2EGyz+xB*dEGz)
        d2AOZZ(4)=d2AOZZ(4)+COEF_SXY*xB*yB*d2EGzz
    
        d2AOXX(5)=d2AOXX(5)+COEF_SXY*(TWO*zB*dEGx+xB*zB*d2EGxx)
        d2AOXY(5)=d2AOXY(5)+COEF_SXY*(xB*zB*d2EGxy+zB*dEGy)
        d2AOXZ(5)=d2AOXZ(5)+COEF_SXY*(xB*dEGx+xB*zB*d2EGxz+EG+zB*dEGz)
        d2AOYY(5)=d2AOYY(5)+COEF_SXY*xB*zB*d2EGyy
        d2AOYZ(5)=d2AOYZ(5)+COEF_SXY*(xB*dEGy+xB*zB*d2EGyz)
        d2AOZZ(5)=d2AOZZ(5)+COEF_SXY*(TWO*xB*dEGz+xB*zB*d2EGzz)
         
        d2AOXX(6)=d2AOXX(6)+COEF_SXY*yB*zB*d2EGxx 
        d2AOXY(6)=d2AOXY(6)+COEF_SXY*(zB*dEGx+yB*zB*d2EGxy) 
        d2AOXZ(6)=d2AOXZ(6)+COEF_SXY*(yB*dEGx+yB*zB*d2EGxz)
        d2AOYY(6)=d2AOYY(6)+COEF_SXY*(TWO*zB*dEGy+yB*zB*d2EGyy)
        d2AOYZ(6)=d2AOYZ(6)+COEF_SXY*(yB*dEGy+yB*zB*d2EGyz+EG+zB*dEGz)
        d2AOZZ(6)=d2AOZZ(6)+COEF_SXY*(TWO*yB*dEGz+yB*zB*d2EGzz)
      case (6) ! P|P
        d2AOXX(1)=d2AOXX(1)+COEF_IJ*(xA*xB*d2EGxx+TWO*(xA+xB)*dEGx+TWO*EG) 
        d2AOXY(1)=d2AOXY(1)+COEF_IJ*(xA*xB*d2EGxy+(xA+xB)*dEGy)
        d2AOXZ(1)=d2AOXZ(1)+COEF_IJ*(xA*xB*d2EGxz+(xA+xB)*dEGz)
        d2AOYY(1)=d2AOYY(1)+COEF_IJ*xA*xB*d2EGyy
        d2AOYZ(1)=d2AOYZ(1)+COEF_IJ*xA*xB*d2EGyz
        d2AOZZ(1)=d2AOZZ(1)+COEF_IJ*xA*xB*d2EGzz

        d2AOXX(2)=d2AOXX(2)+COEF_IJ*(TWO*yB*dEGx+xA*yB*d2EGxx) 
        d2AOXY(2)=d2AOXY(2)+COEF_IJ*(xA*dEGx+xA*yB*d2EGxy+EG+yB*dEGy) 
        d2AOXZ(2)=d2AOXZ(2)+COEF_IJ*(xA*yB*d2EGxz+yB*dEGz) 
        d2AOYY(2)=d2AOYY(2)+COEF_IJ*(TWO*xA*dEGy+xA*yB*d2EGyy)
        d2AOYZ(2)=d2AOYZ(2)+COEF_IJ*(xA*yB*d2EGyz+xA*dEGz)
        d2AOZZ(2)=d2AOZZ(2)+COEF_IJ*xA*yB*d2EGzz

        d2AOXX(3)=d2AOXX(3)+COEF_IJ*(TWO*zB*dEGx+xA*zB*d2EGxx)
        d2AOXY(3)=d2AOXY(3)+COEF_IJ*(xA*zB*d2EGxy+zB*dEGy)
        d2AOXZ(3)=d2AOXZ(3)+COEF_IJ*(xA*dEGx+xA*zB*d2EGxz+EG+zB*dEGz)
        d2AOYY(3)=d2AOYY(3)+COEF_IJ*xA*zB*d2EGyy
        d2AOYZ(3)=d2AOYZ(3)+COEF_IJ*(xA*dEGy+xA*zB*d2EGyz)
        d2AOZZ(3)=d2AOZZ(3)+COEF_IJ*(TWO*xA*dEGz+xA*zB*d2EGzz)

        d2AOXX(4)=d2AOXX(4)+COEF_IJ*(TWO*yA*dEGx+yA*xB*d2EGxx) 
        d2AOXY(4)=d2AOXY(4)+COEF_IJ*(xB*dEGx+yA*xB*d2EGxy+EG+yA*dEGy) 
        d2AOXZ(4)=d2AOXZ(4)+COEF_IJ*(yA*xB*d2EGxz+yA*dEGz) 
        d2AOYY(4)=d2AOYY(4)+COEF_IJ*(TWO*xB*dEGy+yA*xB*d2EGyy)
        d2AOYZ(4)=d2AOYZ(4)+COEF_IJ*(yA*xB*d2EGyz+xB*dEGz)
        d2AOZZ(4)=d2AOZZ(4)+COEF_IJ*yA*xB*d2EGzz

        d2AOXX(5)=d2AOXX(5)+COEF_IJ*yA*yB*d2EGxx     
        d2AOXY(5)=d2AOXY(5)+COEF_IJ*(yA*yB*d2EGxy+(yA+yB)*dEGx)
        d2AOXZ(5)=d2AOXZ(5)+COEF_IJ*yA*yB*d2EGxz 
        d2AOYY(5)=d2AOYY(5)+COEF_IJ*(yA*yB*d2EGyy+TWO*(yA+yB)*dEGy+TWO*EG)
        d2AOYZ(5)=d2AOYZ(5)+COEF_IJ*(yA*yB*d2EGyz+(yA+yB)*dEGz)
        d2AOZZ(5)=d2AOZZ(5)+COEF_IJ*yA*yB*d2EGzz

        d2AOXX(6)=d2AOXX(6)+COEF_IJ*yA*zB*d2EGxx 
        d2AOXY(6)=d2AOXY(6)+COEF_IJ*(zB*dEGx+yA*zB*d2EGxy) 
        d2AOXZ(6)=d2AOXZ(6)+COEF_IJ*(yA*dEGx+yA*zB*d2EGxz) 
        d2AOYY(6)=d2AOYY(6)+COEF_IJ*(TWO*zB*dEGy+yA*zB*d2EGyy)
        d2AOYZ(6)=d2AOYZ(6)+COEF_IJ*(yA*dEGy+yA*zB*d2EGyz+EG+zB*dEGz)
        d2AOZZ(6)=d2AOZZ(6)+COEF_IJ*(TWO*yA*dEGz+yA*zB*d2EGzz)
      
        d2AOXX(7)=d2AOXX(7)+COEF_IJ*(TWO*zA*dEGx+zA*xB*d2EGxx)
        d2AOXY(7)=d2AOXY(7)+COEF_IJ*(zA*xB*d2EGxy+zA*dEGy)
        d2AOXZ(7)=d2AOXZ(7)+COEF_IJ*(xB*dEGx+zA*xB*d2EGxz+EG+zA*dEGz)
        d2AOYY(7)=d2AOYY(7)+COEF_IJ*zA*xB*d2EGyy
        d2AOYZ(7)=d2AOYZ(7)+COEF_IJ*(xB*dEGy+zA*xB*d2EGyz)
        d2AOZZ(7)=d2AOZZ(7)+COEF_IJ*(TWO*xB*dEGz+zA*xB*d2EGzz)
              
        d2AOXX(8)=d2AOXX(8)+COEF_IJ*zA*yB*d2EGxx  
        d2AOXY(8)=d2AOXY(8)+COEF_IJ*(zA*dEGx+zA*yB*d2EGxy)  
        d2AOXZ(8)=d2AOXZ(8)+COEF_IJ*(yB*dEGx+zA*yB*d2EGxz) 
        d2AOYY(8)=d2AOYY(8)+COEF_IJ*(TWO*zA*dEGy+zA*yB*d2EGyy)
        d2AOYZ(8)=d2AOYZ(8)+COEF_IJ*(yB*dEGy+zA*yB*d2EGyz+EG+zA*dEGz)
        d2AOZZ(8)=d2AOZZ(8)+COEF_IJ*(TWO*yB*dEGz+zA*yB*d2EGzz)
         
        d2AOXX(9)=d2AOXX(9)+COEF_IJ*zA*zB*d2EGxx 
        d2AOXY(9)=d2AOXY(9)+COEF_IJ*zA*zB*d2EGxy 
        d2AOXZ(9)=d2AOXZ(9)+COEF_IJ*(zA*zB*d2EGxz+(zA+zB)*dEGx)  
        d2AOYY(9)=d2AOYY(9)+COEF_IJ*zA*zB*d2EGyy
        d2AOYZ(9)=d2AOYZ(9)+COEF_IJ*(zA*zB*d2EGyz+(zA+zB)*dEGy)
        d2AOZZ(9)=d2AOZZ(9)+COEF_IJ*(zA*zB*d2EGzz+TWO*(zA+zB)*dEGz+TWO*EG)
      case (7) ! P|D
        COEF_PXY = SQRT3*COEF_IJ
        d2AOXX(1)=d2AOXX(1)+COEF_IJ*(xA*xB*xB*d2EGxx+TWO*(xB*xB+TWO*xA*xB)*dEGx+TWO*(xA+TWO*xB)*EG)
        d2AOXY(1)=d2AOXY(1)+COEF_IJ*(xA*xB*xB*d2EGxy+(TWO*xA*xB+xB*xB)*dEGy)
        d2AOXZ(1)=d2AOXZ(1)+COEF_IJ*(xA*xB*xB*d2EGxz+(TWO*xA*xB+xB*xB)*dEGz)
        d2AOYY(1)=d2AOYY(1)+COEF_IJ*xA*xB*xB*d2EGyy
        d2AOYZ(1)=d2AOYZ(1)+COEF_IJ*xA*xB*xB*d2EGyz
        d2AOZZ(1)=d2AOZZ(1)+COEF_IJ*xA*xB*xB*d2EGzz
         
        d2AOXX(2)=d2AOXX(2)+COEF_IJ*(xA*yB*yB*d2EGxx+TWO*yB*yB*dEGx)
        d2AOXY(2)=d2AOXY(2)+COEF_IJ*(xA*yB*yB*d2EGxy+TWO*xA*yB*dEGx+yB*yB*dEGy+TWO*yB*EG)
        d2AOXZ(2)=d2AOXZ(2)+COEF_IJ*(xA*yB*yB*d2EGxz+yB*yB*dEGz)
        d2AOYY(2)=d2AOYY(2)+COEF_IJ*(xA*yB*yB*d2EGyy+FOUR*xA*yB*dEGy+TWO*xA*EG)
        d2AOYZ(2)=d2AOYZ(2)+COEF_IJ*(xA*yB*yB*d2EGyz+TWO*xA*yB*dEGz)
        d2AOZZ(2)=d2AOZZ(2)+COEF_IJ*xA*yB*yB*d2EGzz
 
        d2AOXX(3)=d2AOXX(3)+COEF_IJ*(xA*zB*zB*d2EGxx+TWO*zB*zB*dEGx)
        d2AOXY(3)=d2AOXY(3)+COEF_IJ*(xA*zB*zB*d2EGxy+zB*zB*dEGy)
        d2AOXZ(3)=d2AOXZ(3)+COEF_IJ*(xA*zB*zB*d2EGxz+TWO*xA*zB*dEGx+zB*zB*dEGz+TWO*zB*EG)
        d2AOYY(3)=d2AOYY(3)+COEF_IJ*xA*zB*zB*d2EGyy
        d2AOYZ(3)=d2AOYZ(3)+COEF_IJ*(xA*zB*zB*d2EGyz+TWO*xA*zB*dEGy)
        d2AOZZ(3)=d2AOZZ(3)+COEF_IJ*(xA*zB*zB*d2EGzz+FOUR*xA*zB*dEGz+TWO*xA*EG)
   
        d2AOXX(4)=d2AOXX(4)+COEF_PXY*(xA*xB*yB*d2EGxx+TWO*(xA+xB)*yB*dEGx+TWO*yB*EG)     
        d2AOXY(4)=d2AOXY(4)+COEF_PXY*(xA*xB*yB*d2EGxy+xA*xB*dEGx+yB*(xA+xB)*dEGy+(xA+xB)*EG)     
        d2AOXZ(4)=d2AOXZ(4)+COEF_PXY*(xA*xB*yB*d2EGxz+yB*(xA+xB)*dEGz)     
        d2AOYY(4)=d2AOYY(4)+COEF_PXY*(xA*xB*yB*d2EGyy+TWO*xA*xB*dEGy)
        d2AOYZ(4)=d2AOYZ(4)+COEF_PXY*(xA*xB*yB*d2EGyz+xA*xB*dEGz)
        d2AOZZ(4)=d2AOZZ(4)+COEF_PXY*xA*xB*yB*d2EGzz

        d2AOXX(5)=d2AOXX(5)+COEF_PXY*(xA*xB*zB*d2EGxx+TWO*(xA+xB)*zB*dEGx+TWO*zB*EG)
        d2AOXY(5)=d2AOXY(5)+COEF_PXY*(xA*xB*zB*d2EGxy+(xA+xB)*zB*dEGy)
        d2AOXZ(5)=d2AOXZ(5)+COEF_PXY*(xA*xB*zB*d2EGxz+xA*xB*dEGx+(xA+xB)*zB*dEGz+(xA+xB)*EG)
        d2AOYY(5)=d2AOYY(5)+COEF_PXY*xA*xB*zB*d2EGyy
        d2AOYZ(5)=d2AOYZ(5)+COEF_PXY*(xA*xB*zB*d2EGyz+xA*xB*dEGy)
        d2AOZZ(5)=d2AOZZ(5)+COEF_PXY*(xA*xB*zB*d2EGzz+TWO*xA*xB*dEGz)
        
        d2AOXX(6)=d2AOXX(6)+COEF_PXY*(xA*yB*zB*d2EGxx+TWO*yB*zB*dEGx) 
        d2AOXY(6)=d2AOXY(6)+COEF_PXY*(xA*yB*zB*d2EGxy+xA*zB*dEGx+yB*zB*dEGy+zB*EG)
        d2AOXZ(6)=d2AOXZ(6)+COEF_PXY*(xA*yB*zB*d2EGxz+xA*yB*dEGx+yB*zB*dEGz+yB*EG) 
        d2AOYY(6)=d2AOYY(6)+COEF_PXY*(xA*yB*zB*d2EGyy+TWO*xA*zB*dEGy)
        d2AOYZ(6)=d2AOYZ(6)+COEF_PXY*(xA*yB*zB*d2EGyz+xA*yB*dEGy+xA*zB*dEGz+xA*EG)
        d2AOZZ(6)=d2AOZZ(6)+COEF_PXY*(xA*yB*zB*d2EGzz+TWO*xA*yB*dEGz)
 
        d2AOXX(7)=d2AOXX(7)+COEF_IJ*(yA*xB*xB*d2EGxx+FOUR*yA*xB*dEGx+TWO*yA*EG)
        d2AOXY(7)=d2AOXY(7)+COEF_IJ*(yA*xB*xB*d2EGxy+xB*xB*dEGx+TWO*yA*xB*dEGy+TWO*xB*EG)
        d2AOXZ(7)=d2AOXZ(7)+COEF_IJ*(yA*xB*xB*d2EGxz+TWO*yA*xB*dEGz)
        d2AOYY(7)=d2AOYY(7)+COEF_IJ*(yA*xB*xB*d2EGyy+TWO*xB*xB*dEGy)
        d2AOYZ(7)=d2AOYZ(7)+COEF_IJ*(yA*xB*xB*d2EGyz+xB*xB*dEGz)
        d2AOZZ(7)=d2AOZZ(7)+COEF_IJ*yA*xB*xB*d2EGzz
              
        d2AOXX(8)=d2AOXX(8)+COEF_IJ*yA*yB*yB*d2EGxx 
        d2AOXY(8)=d2AOXY(8)+COEF_IJ*(yA*yB*yB*d2EGxy+(yB*yB+TWO*yA*yB)*dEGx) 
        d2AOXZ(8)=d2AOXZ(8)+COEF_IJ*yA*yB*yB*d2EGxz 
        d2AOYY(8)=d2AOYY(8)+COEF_IJ*(yA*yB*yB*d2EGyy+TWO*(yB*yB+TWO*yA*yB)*dEGy+TWO*(yA+TWO*yB)*EG)
        d2AOYZ(8)=d2AOYZ(8)+COEF_IJ*(yA*yB*yB*d2EGyz+(yB*yB+TWO*yA*yB)*dEGz)
        d2AOZZ(8)=d2AOZZ(8)+COEF_IJ*yA*yB*yB*d2EGzz
                    
        d2AOXX(9)=d2AOXX(9)+COEF_IJ*yA*zB*zB*d2EGxx
        d2AOXY(9)=d2AOXY(9)+COEF_IJ*(yA*zB*zB*d2EGxy+zB*zB*dEGx)
        d2AOXZ(9)=d2AOXZ(9)+COEF_IJ*(yA*zB*zB*d2EGxz+TWO*yA*zB*dEGx)
        d2AOYY(9)=d2AOYY(9)+COEF_IJ*(yA*zB*zB*d2EGyy+TWO*zB*zB*dEGy)
        d2AOYZ(9)=d2AOYZ(9)+COEF_IJ*(yA*zB*zB*d2EGyz+TWO*yA*zB*dEGy+zB*zB*dEGz+TWO*zB*EG)
        d2AOZZ(9)=d2AOZZ(9)+COEF_IJ*(yA*zB*zB*d2EGzz+FOUR*yA*zB*dEGz+TWO*yA*EG)          

        d2AOXX(10)=d2AOXX(10)+COEF_PXY*(yA*xB*yB*d2EGxx+TWO*yA*yB*dEGx)
        d2AOXY(10)=d2AOXY(10)+COEF_PXY*(yA*xB*yB*d2EGxy+(yA+yB)*xB*dEGx+yA*yB*dEGy+(yA+yB)*EG)
        d2AOXZ(10)=d2AOXZ(10)+COEF_PXY*(yA*xB*yB*d2EGxz+yA*yB*dEGz)
        d2AOYY(10)=d2AOYY(10)+COEF_PXY*(yA*xB*yB*d2EGyy+TWO*(yA+yB)*xB*dEGy+TWO*xB*EG)
        d2AOYZ(10)=d2AOYZ(10)+COEF_PXY*(yA*xB*yB*d2EGyz+(yA+yB)*xB*dEGz)
        d2AOZZ(10)=d2AOZZ(10)+COEF_PXY*yA*xB*yB*d2EGzz
          
        d2AOXX(11)=d2AOXX(11)+COEF_PXY*(yA*xB*zB*d2EGxx+TWO*yA*zB*dEGx)
        d2AOXY(11)=d2AOXY(11)+COEF_PXY*(yA*xB*zB*d2EGxy+xB*zB*dEGx+yA*zB*dEGy+zB*EG)
        d2AOXZ(11)=d2AOXZ(11)+COEF_PXY*(yA*xB*zB*d2EGxz+yA*xB*dEGx+yA*zB*dEGz+yA*EG)
        d2AOYY(11)=d2AOYY(11)+COEF_PXY*(yA*xB*zB*d2EGyy+TWO*xB*zB*dEGy)
        d2AOYZ(11)=d2AOYZ(11)+COEF_PXY*(yA*xB*zB*d2EGyz+yA*xB*dEGy+xB*zB*dEGz+xB*EG)
        d2AOZZ(11)=d2AOZZ(11)+COEF_PXY*(yA*xB*zB*d2EGzz+TWO*yA*xB*dEGz)
 
        d2AOXX(12)=d2AOXX(12)+COEF_PXY*(yA*yB*zB*d2EGxx)     
        d2AOXY(12)=d2AOXY(12)+COEF_PXY*(yA*yB*zB*d2EGxy+(yA+yB)*zB*dEGx)
        d2AOXZ(12)=d2AOXZ(12)+COEF_PXY*(yA*yB*zB*d2EGxz+yA*yB*dEGx)
        d2AOYY(12)=d2AOYY(12)+COEF_PXY*(yA*yB*zB*d2EGyy+TWO*(yA+yB)*zB*dEGy+TWO*zB*EG)
        d2AOYZ(12)=d2AOYZ(12)+COEF_PXY*(yA*yB*zB*d2EGyz+yA*yB*dEGy+(yA+yB)*zB*dEGz+(yA+yB)*EG)
        d2AOZZ(12)=d2AOZZ(12)+COEF_PXY*(yA*yB*zB*d2EGzz+TWO*yA*yB*dEGz)
       
        d2AOXX(13)=d2AOXX(13)+COEF_IJ*(zA*xB*xB*d2EGxx+FOUR*zA*xB*dEGx+TWO*zA*EG) 
        d2AOXY(13)=d2AOXY(13)+COEF_IJ*(zA*xB*xB*d2EGxy+TWO*zA*xB*dEGy) 
        d2AOXZ(13)=d2AOXZ(13)+COEF_IJ*(zA*xB*xB*d2EGxz+xB*xB*dEGx+TWO*zA*xB*dEGz+TWO*xB*EG) 
        d2AOYY(13)=d2AOYY(13)+COEF_IJ*zA*xB*xB*d2EGyy
        d2AOYZ(13)=d2AOYZ(13)+COEF_IJ*(zA*xB*xB*d2EGyz+xB*xB*dEGy)
        d2AOZZ(13)=d2AOZZ(13)+COEF_IJ*(zA*xB*xB*d2EGzz+TWO*xB*xB*dEGz)

        d2AOXX(14)=d2AOXX(14)+COEF_IJ*zA*yB*yB*d2EGxx
        d2AOXY(14)=d2AOXY(14)+COEF_IJ*(zA*yB*yB*d2EGxy+TWO*zA*yB*dEGx)
        d2AOXZ(14)=d2AOXZ(14)+COEF_IJ*(zA*yB*yB*d2EGxz+yB*yB*dEGx)
        d2AOYY(14)=d2AOYY(14)+COEF_IJ*(zA*yB*yB*d2EGyy+FOUR*zA*yB*dEGy+TWO*zA*EG)
        d2AOYZ(14)=d2AOYZ(14)+COEF_IJ*(zA*yB*yB*d2EGyz+yB*yB*dEGy+TWO*zA*yB*dEGz+TWO*yB*EG)
        d2AOZZ(14)=d2AOZZ(14)+COEF_IJ*(zA*yB*yB*d2EGzz+TWO*yB*yB*dEGz)
            
        d2AOXX(15)=d2AOXX(15)+COEF_IJ*zA*zB*zB*d2EGxx
        d2AOXY(15)=d2AOXY(15)+COEF_IJ*zA*zB*zB*d2EGxy
        d2AOXZ(15)=d2AOXZ(15)+COEF_IJ*(zA*zB*zB*d2EGxz+(zB*zB+TWO*zA*zB)*dEGx)
        d2AOYY(15)=d2AOYY(15)+COEF_IJ*zA*zB*zB*d2EGyy
        d2AOYZ(15)=d2AOYZ(15)+COEF_IJ*(zA*zB*zB*d2EGyz+(zB*zB+TWO*zA*zB)*dEGy)
        d2AOZZ(15)=d2AOZZ(15)+COEF_IJ*(zA*zB*zB*d2EGzz+TWO*(zB*zB+TWO*zA*zB)*dEGz+TWO*(zA+TWO*zB)*EG)
            
        d2AOXX(16)=d2AOXX(16)+COEF_PXY*(zA*xB*yB*d2EGxx+TWO*zA*yB*dEGx) 
        d2AOXY(16)=d2AOXY(16)+COEF_PXY*(zA*xB*yB*d2EGxy+zA*xB*dEGx+zA*yB*dEGy+zA*EG) 
        d2AOXZ(16)=d2AOXZ(16)+COEF_PXY*(zA*xB*yB*d2EGxz+xB*yB*dEGx+zA*yB*dEGz+yB*EG) 
        d2AOYY(16)=d2AOYY(16)+COEF_PXY*(zA*xB*yB*d2EGyy+TWO*zA*xB*dEGy)
        d2AOYZ(16)=d2AOYZ(16)+COEF_PXY*(zA*xB*yB*d2EGyz+xB*yB*dEGy+zA*xB*dEGz+xB*EG)
        d2AOZZ(16)=d2AOZZ(16)+COEF_PXY*(zA*xB*yB*d2EGzz+TWO*xB*yB*dEGz)
            
        d2AOXX(17)=d2AOXX(17)+COEF_PXY*(zA*xB*zB*d2EGxx+TWO*zA*zB*dEGx) 
        d2AOXY(17)=d2AOXY(17)+COEF_PXY*(zA*xB*zB*d2EGxy+zA*zB*dEGy) 
        d2AOXZ(17)=d2AOXZ(17)+COEF_PXY*(zA*xB*zB*d2EGxz+(zA+zB)*xB*dEGx+zA*zB*dEGz+(zA+zB)*EG) 
        d2AOYY(17)=d2AOYY(17)+COEF_PXY*zA*xB*zB*d2EGyy
        d2AOYZ(17)=d2AOYZ(17)+COEF_PXY*(zA*xB*zB*d2EGyz+(zA+zB)*xB*dEGy)
        d2AOZZ(17)=d2AOZZ(17)+COEF_PXY*(zA*xB*zB*d2EGzz+TWO*(zA+zB)*xB*dEGz+TWO*xB*EG)
                             
        d2AOXX(18)=d2AOXX(18)+COEF_PXY*zA*yB*zB*d2EGxx 
        d2AOXY(18)=d2AOXY(18)+COEF_PXY*(zA*yB*zB*d2EGxy+zA*zB*dEGx) 
        d2AOXZ(18)=d2AOXZ(18)+COEF_PXY*(zA*yB*zB*d2EGxz+(zA+zB)*yB*dEGx) 
        d2AOYY(18)=d2AOYY(18)+COEF_PXY*(zA*yB*zB*d2EGyy+TWO*zA*zB*dEGy)
        d2AOYZ(18)=d2AOYZ(18)+COEF_PXY*(zA*yB*zB*d2EGyz+(zA+zB)*yB*dEGy+zA*zB*dEGz+(zA+zB)*EG) 
        d2AOZZ(18)=d2AOZZ(18)+COEF_PXY*(zA*yB*zB*d2EGzz+TWO*(zA+zB)*yB*dEGz+TWO*yB*EG)
      case (11) ! D|D
        COEF_XXXY = SQRT3*COEF_IJ
        COEF_XYXY = THREE*COEF_IJ
        d2AOXX(1)=d2AOXX(1)+COEF_IJ*(xA*xA*xB*xB*d2EGxx+FOUR*(xA*xB)*(xA+xB)*dEGx+TWO*(xA*xA+xB*xB+FOUR*xA*xB)*EG)
        d2AOXY(1)=d2AOXY(1)+COEF_IJ*(xA*xA*xB*xB*d2EGxy+TWO*(xA*xB)*(xA+xB)*dEGy)
        d2AOXZ(1)=d2AOXZ(1)+COEF_IJ*(xA*xA*xB*xB*d2EGxz+TWO*(xA*xB)*(xA+xB)*dEGz)
        d2AOYY(1)=d2AOYY(1)+COEF_IJ*xA*xA*xB*xB*d2EGyy
        d2AOYZ(1)=d2AOYZ(1)+COEF_IJ*xA*xA*xB*xB*d2EGyz
        d2AOZZ(1)=d2AOZZ(1)+COEF_IJ*xA*xA*xB*xB*d2EGzz

        d2AOXX(2)=d2AOXX(2)+COEF_IJ*(xA*xA*yB*yB*d2EGxx+FOUR*xA*yB*yB*dEGx+TWO*yB*yB*EG)
        d2AOXY(2)=d2AOXY(2)+COEF_IJ*(xA*xA*yB*yB*d2EGxy+TWO*xA*xA*yB*dEGx+TWO*yB*yB*xA*dEGy+FOUR*yB*xA*EG)
        d2AOXZ(2)=d2AOXZ(2)+COEF_IJ*(xA*xA*yB*yB*d2EGxz+TWO*yB*yB*xA*dEGz)
        d2AOYY(2)=d2AOYY(2)+COEF_IJ*(xA*xA*yB*yB*d2EGyy+FOUR*xA*xA*yB*dEGy+TWO*xA*xA*EG)
        d2AOYZ(2)=d2AOYZ(2)+COEF_IJ*(xA*xA*yB*yB*d2EGyz+TWO*xA*xA*yB*dEGz)
        d2AOZZ(2)=d2AOZZ(2)+COEF_IJ*xA*xA*yB*yB*d2EGzz

        d2AOXX(3)=d2AOXX(3)+COEF_IJ*(xA*xA*zB*zB*d2EGxx+FOUR*xA*zB*zB*dEGx+TWO*zB*zB*EG) 
        d2AOXY(3)=d2AOXY(3)+COEF_IJ*(xA*xA*zB*zB*d2EGxy+TWO*zB*zB*xA*dEGy) 
        d2AOXZ(3)=d2AOXZ(3)+COEF_IJ*(xA*xA*zB*zB*d2EGxz+TWO*xA*xA*zB*dEGx+TWO*zB*zB*xA*dEGz+FOUR*zB*xA*EG) 
        d2AOYY(3)=d2AOYY(3)+COEF_IJ*xA*xA*zB*zB*d2EGyy
        d2AOYZ(3)=d2AOYZ(3)+COEF_IJ*(xA*xA*zB*zB*d2EGyz+TWO*xA*xA*zB*dEGy)
        d2AOZZ(3)=d2AOZZ(3)+COEF_IJ*(xA*xA*zB*zB*d2EGzz+FOUR*xA*xA*zB*dEGz+TWO*xA*xA*EG)

        d2AOXX(4)=d2AOXX(4)+COEF_XXXY*(xA*xA*xB*yB*d2EGxx+TWO*(xA*xA+TWO*xA*xB)*yB*dEGx+TWO*(TWO*xA+xB)*yB*EG)
        d2AOXY(4)=d2AOXY(4)+COEF_XXXY*(xA*xA*xB*yB*d2EGxy+xA*xA*xB*dEGx+(xA*xA+TWO*xA*xB)*(yB*dEGy+EG))
        d2AOXZ(4)=d2AOXZ(4)+COEF_XXXY*(xA*xA*xB*yB*d2EGxz+(xA*xA+TWO*xA*xB)*yB*dEGz)
        d2AOYY(4)=d2AOYY(4)+COEF_XXXY*(xA*xA*xB*yB*d2EGyy+TWO*xA*xA*xB*dEGy)
        d2AOYZ(4)=d2AOYZ(4)+COEF_XXXY*(xA*xA*xB*yB*d2EGyz+xA*xA*xB*dEGz)
        d2AOZZ(4)=d2AOZZ(4)+COEF_XXXY*xA*xA*xB*yB*d2EGzz

        d2AOXX(5)=d2AOXX(5)+COEF_XXXY*(xA*xA*xB*zB*d2EGxx+TWO*(xA*xA+TWO*xA*xB)*zB*dEGx+TWO*(TWO*xA+xB)*zB*EG)
        d2AOXY(5)=d2AOXY(5)+COEF_XXXY*(xA*xA*xB*zB*d2EGxy+(xA*xA+TWO*xA*xB)*zB*dEGy)
        d2AOXZ(5)=d2AOXZ(5)+COEF_XXXY*(xA*xA*xB*(zB*d2EGxz+dEGx)+(xA*xA+TWO*xA*xB)*(zB*dEGz+EG))
        d2AOYY(5)=d2AOYY(5)+COEF_XXXY*xA*xA*xB*zB*d2EGyy
        d2AOYZ(5)=d2AOYZ(5)+COEF_XXXY*xA*xA*xB*(zB*d2EGyz+dEGy)
        d2AOZZ(5)=d2AOZZ(5)+COEF_XXXY*(xA*xA*xB*zB*d2EGzz+TWO*xA*xA*xB*dEGz)

        d2AOXX(6)=d2AOXX(6)+COEF_XXXY*(xA*xA*yB*zB*d2EGxx+FOUR*xA*yB*zB*dEGx+TWO*yB*zB*EG)
        d2AOXY(6)=d2AOXY(6)+COEF_XXXY*(xA*xA*zB*(yB*d2EGxy+dEGx)+TWO*xA*zB*(yB*dEGy+EG))
        d2AOXZ(6)=d2AOXZ(6)+COEF_XXXY*(xA*xA*yB*(zB*d2EGxz+dEGx)+TWO*xA*yB*(zB*dEGz+EG))
        d2AOYY(6)=d2AOYY(6)+COEF_XXXY*(xA*xA*yB*zB*d2EGyy+TWO*xA*xA*zB*dEGy)
        d2AOYZ(6)=d2AOYZ(6)+COEF_XXXY*(xA*xA*yB*(zB*d2EGyz+dEGy)+xA*xA*(zB*dEGz+EG))
        d2AOZZ(6)=d2AOZZ(6)+COEF_XXXY*(xA*xA*yB*zB*d2EGzz+TWO*xA*xA*yB*dEGz)

        d2AOXX(7)=d2AOXX(7)+COEF_IJ*(yA*yA*xB*xB*d2EGxx+FOUR*yA*yA*xB*dEGx+TWO*yA*yA*EG)
        d2AOXY(7)=d2AOXY(7)+COEF_IJ*(yA*yA*xB*xB*d2EGxy+TWO*yA*xB*xB*dEGx+TWO*yA*yA*xB*dEGy+FOUR*yA*xB*EG)
        d2AOXZ(7)=d2AOXZ(7)+COEF_IJ*(yA*yA*xB*xB*d2EGxz+TWO*yA*yA*xB*dEGz)
        d2AOYY(7)=d2AOYY(7)+COEF_IJ*(yA*yA*xB*xB*d2EGyy+FOUR*yA*xB*xB*dEGy+TWO*xB*xB*EG)
        d2AOYZ(7)=d2AOYZ(7)+COEF_IJ*(yA*yA*xB*xB*d2EGyz+TWO*yA*xB*xB*dEGz)
        d2AOZZ(7)=d2AOZZ(7)+COEF_IJ*yA*yA*xB*xB*d2EGzz

        d2AOXX(8)=d2AOXX(8)+COEF_IJ*yA*yA*yB*yB*d2EGxx
        d2AOXY(8)=d2AOXY(8)+COEF_IJ*(yA*yA*yB*yB*d2EGxy+TWO*(yA*yB)*(yA+yB)*dEGx)
        d2AOXZ(8)=d2AOXZ(8)+COEF_IJ*yA*yA*yB*yB*d2EGxz
        d2AOYY(8)=d2AOYY(8)+COEF_IJ*(yA*yA*yB*yB*d2EGyy+FOUR*(yA*yB)*(yA+yB)*dEGy+TWO*(yA*yA+yB*yB+FOUR*yA*yB)*EG)
        d2AOYZ(8)=d2AOYZ(8)+COEF_IJ*(yA*yA*yB*yB*d2EGyz+TWO*(yA*yB)*(yA+yB)*dEGz)
        d2AOZZ(8)=d2AOZZ(8)+COEF_IJ*yA*yA*yB*yB*d2EGzz

        d2AOXX(9)=d2AOXX(9)+COEF_IJ*yA*yA*zB*zB*d2EGxx
        d2AOXY(9)=d2AOXY(9)+COEF_IJ*(yA*yA*zB*zB*d2EGxy+TWO*yA*zB*zB*dEGx)
        d2AOXZ(9)=d2AOXZ(9)+COEF_IJ*(yA*yA*zB*zB*d2EGxz+TWO*yA*yA*zB*dEGx)
        d2AOYY(9)=d2AOYY(9)+COEF_IJ*(yA*yA*zB*zB*d2EGyy+FOUR*yA*zB*zB*dEGy+TWO*zB*zB*EG)
        d2AOYZ(9)=d2AOYZ(9)+COEF_IJ*(yA*yA*zB*zB*d2EGyz+TWO*yA*yA*zB*dEGy+TWO*zB*zB*yA*dEGz+FOUR*zB*yA*EG)
        d2AOZZ(9)=d2AOZZ(9)+COEF_IJ*(yA*yA*zB*zB*d2EGzz+FOUR*yA*yA*zB*dEGz+TWO*yA*yA*EG)

        d2AOXX(10)=d2AOXX(10)+COEF_XXXY*(yA*yA*yB*(xB*d2EGxx+TWO*dEGx))
        d2AOXY(10)=d2AOXY(10)+COEF_XXXY*(yA*yA*xB*yB*d2EGxy+(yA*yA+TWO*yA*yB)*(xB*dEGx+EG)+yA*yA*yB*dEGy)
        d2AOXZ(10)=d2AOXZ(10)+COEF_XXXY*(yA*yA*yB*(xB*d2EGxz+dEGz))
        d2AOYY(10)=d2AOYY(10)+COEF_XXXY*(yA*yA*xB*yB*d2EGyy+TWO*(yA*yA+TWO*yA*yB)*xB*dEGy+TWO*(TWO*yA+yB)*xB*EG)
        d2AOYZ(10)=d2AOYZ(10)+COEF_XXXY*(yA*yA*xB*yB*d2EGyz+(yA*yA+TWO*yB*yA)*xB*dEGz)
        d2AOZZ(10)=d2AOZZ(10)+COEF_XXXY*yA*yA*xB*yB*d2EGzz

        d2AOXX(11)=d2AOXX(11)+COEF_XXXY*(yA*yA*zB*(xB*d2EGxx+TWO*dEGx))
        d2AOXY(11)=d2AOXY(11)+COEF_XXXY*(yA*xB*zB*(yA*d2EGxy+TWO*dEGx)+yA*zB*(yA*dEGy+TWO*EG))
        d2AOXZ(11)=d2AOXZ(11)+COEF_XXXY*(yA*yA*xB*(zB*d2EGxz+dEGx)+yA*yA*(zB*dEGz+EG))
        d2AOYY(11)=d2AOYY(11)+COEF_XXXY*(yA*yA*xB*zB*d2EGyy+FOUR*yA*xB*zB*dEGy+TWO*xB*zB*EG)
        d2AOYZ(11)=d2AOYZ(11)+COEF_XXXY*(yA*yA*xB*(zB*d2EGyz+dEGy)+TWO*yA*xB*(zB*dEGz+EG))
        d2AOZZ(11)=d2AOZZ(11)+COEF_XXXY*(yA*yA*xB*(zB*d2EGzz+TWO*dEGz))

        d2AOXX(12)=d2AOXX(12)+COEF_XXXY*yA*yA*yB*zB*d2EGxx
        d2AOXY(12)=d2AOXY(12)+COEF_XXXY*(yA*yA*yB*zB*d2EGxy+(yA*yA+TWO*yA*yB)*zB*dEGx)
        d2AOXZ(12)=d2AOXZ(12)+COEF_XXXY*(yA*yA*yB*(zB*d2EGxz+dEGx))
        d2AOYY(12)=d2AOYY(12)+COEF_XXXY*(yA*yA*yB*zB*d2EGyy+TWO*(yA*yA+TWO*yA*yB)*zB*dEGy+TWO*(TWO*yA+yB)*zB*EG)
        d2AOYZ(12)=d2AOYZ(12)+COEF_XXXY*(yA*yA*yB*(zB*d2EGyz+dEGy)+(yA*yA+TWO*yA*yB)*(zB*dEGz+EG))
        d2AOZZ(12)=d2AOZZ(12)+COEF_XXXY*(yA*yA*yB*(zB*d2EGzz+TWO*dEGz))

        d2AOXX(13)=d2AOXX(13)+COEF_IJ*(zA*zA*xB*xB*d2EGxx+FOUR*zA*zA*xB*dEGx+TWO*zA*zA*EG)
        d2AOXY(13)=d2AOXY(13)+COEF_IJ*(zA*zA*xB*xB*d2EGxy+TWO*zA*zA*xB*dEGy)
        d2AOXZ(13)=d2AOXZ(13)+COEF_IJ*(zA*zA*xB*xB*d2EGxz+TWO*zA*xB*xB*dEGx+TWO*zA*zA*xB*dEGz+FOUR*zA*xB*EG)
        d2AOYY(13)=d2AOYY(13)+COEF_IJ*zA*zA*xB*xB*d2EGyy
        d2AOYZ(13)=d2AOYZ(13)+COEF_IJ*(zA*zA*xB*xB*d2EGyz+TWO*zA*xB*xB*dEGy)
        d2AOZZ(13)=d2AOZZ(13)+COEF_IJ*(zA*zA*xB*xB*d2EGzz+FOUR*zA*xB*xB*dEGz+TWO*xB*xB*EG)

        d2AOXX(14)=d2AOXX(14)+COEF_IJ*zA*zA*yB*yB*d2EGxx
        d2AOXY(14)=d2AOXY(14)+COEF_IJ*(zA*zA*yB*yB*d2EGxy+TWO*zA*zA*yB*dEGx)
        d2AOXZ(14)=d2AOXZ(14)+COEF_IJ*(zA*zA*yB*yB*d2EGxz+TWO*zA*yB*yB*dEGx)
        d2AOYY(14)=d2AOYY(14)+COEF_IJ*(zA*zA*yB*yB*d2EGyy+FOUR*zA*zA*yB*dEGy+TWO*zA*zA*EG)
        d2AOYZ(14)=d2AOYZ(14)+COEF_IJ*(zA*zA*yB*yB*d2EGyz+TWO*zA*yB*yB*dEGy+TWO*zA*zA*yB*dEGz+FOUR*zA*yB*EG)
        d2AOZZ(14)=d2AOZZ(14)+COEF_IJ*(zA*zA*yB*yB*d2EGzz+FOUR*zA*yB*yB*dEGz+TWO*yB*yB*EG)
       
        d2AOXX(15)=d2AOXX(15)+COEF_IJ*zA*zA*zB*zB*d2EGxx
        d2AOXY(15)=d2AOXY(15)+COEF_IJ*zA*zA*zB*zB*d2EGxy
        d2AOXZ(15)=d2AOXZ(15)+COEF_IJ*(zA*zA*zB*zB*d2EGxz+TWO*(zA*zB)*(zA+zB)*dEGx)
        d2AOYY(15)=d2AOYY(15)+COEF_IJ*zA*zA*zB*zB*d2EGyy
        d2AOYZ(15)=d2AOYZ(15)+COEF_IJ*(zA*zA*zB*zB*d2EGyz+TWO*(zA*zB)*(zA+zB)*dEGy)
        d2AOZZ(15)=d2AOZZ(15)+COEF_IJ*(zA*zA*zB*zB*d2EGzz+FOUR*(zA*zB)*(zA+zB)*dEGz+TWO*(zA*zA+zB*zB+FOUR*zA*zB)*EG)

        d2AOXX(16)=d2AOXX(16)+COEF_XXXY*(zA*zA*yB*(xB*d2EGxx+TWO*dEGx))
        d2AOXY(16)=d2AOXY(16)+COEF_XXXY*(zA*zA*xB*(yB*d2EGxy+dEGx)+zA*zA*(yB*dEGy+EG))
        d2AOXZ(16)=d2AOXZ(16)+COEF_XXXY*(zA*xB*yB*(zA*d2EGxz+TWO*dEGx)+zA*yB*(zA*dEGz+TWO*EG))
        d2AOYY(16)=d2AOYY(16)+COEF_XXXY*(zA*zA*xB*(yB*d2EGyy+TWO*dEGy))
        d2AOYZ(16)=d2AOYZ(16)+COEF_XXXY*(zA*xB*yB*(zA*d2EGyz+TWO*dEGy)+zA*xB*(zA*dEGz+TWO*EG))
        d2AOZZ(16)=d2AOZZ(16)+COEF_XXXY*(zA*xB*yB*(zA*d2EGzz+FOUR*dEGz)+TWO*xB*yB*EG)
        
        d2AOXX(17)=d2AOXX(17)+COEF_XXXY*(zA*zA*zB*(xB*d2EGxx+TWO*dEGx))
        d2AOXY(17)=d2AOXY(17)+COEF_XXXY*(zA*zA*zB*(xB*d2EGxy+dEGy))
        d2AOXZ(17)=d2AOXZ(17)+COEF_XXXY*(zA*zA*zB*(xB*d2EGxz+dEGz)+(zA*zA+TWO*zA*zB)*(xB*dEGx+EG))
        d2AOYY(17)=d2AOYY(17)+COEF_XXXY*zA*zA*xB*zB*d2EGyy
        d2AOYZ(17)=d2AOYZ(17)+COEF_XXXY*(zA*zA*xB*zB*d2EGyz+(zA*zA+TWO*zA*zB)*xB*dEGy)
        d2AOZZ(17)=d2AOZZ(17)+COEF_XXXY*(zA*zA*xB*zB*d2EGzz+TWO*(zA*zA+TWO*zA*zB)*xB*dEGz+TWO*(TWO*zA+zB)*xB*EG)

        d2AOXX(18)=d2AOXX(18)+COEF_XXXY*zA*zA*yB*zB*d2EGxx
        d2AOXY(18)=d2AOXY(18)+COEF_XXXY*zA*zA*zB*(yB*d2EGxy+dEGx)
        d2AOXZ(18)=d2AOXZ(18)+COEF_XXXY*(zA*zA*yB*zB*d2EGxz+(zA*zA+TWO*zA*zB)*yB*dEGx)
        d2AOYY(18)=d2AOYY(18)+COEF_XXXY*(zA*zA*zB*(yB*d2EGyy+TWO*dEGy))
        d2AOYZ(18)=d2AOYZ(18)+COEF_XXXY*(zA*zA*zB*(yB*d2EGyz+dEGz)+(zA*zA+TWO*zA*zB)*(yB*dEGy+EG))
        d2AOZZ(18)=d2AOZZ(18)+COEF_XXXY*(zA*zA*yB*zB*d2EGzz+TWO*(zA*zA+TWO*zA*zB)*yB*dEGz+TWO*(TWO*zA+zB)*yB*EG)

        d2AOXX(19)=d2AOXX(19)+COEF_XXXY*(xA*yA*xB*xB*d2EGxx+TWO*(xB*xB+TWO*xA*xB)*yA*dEGx+TWO*(TWO*xB+xA)*yA*EG)
        d2AOXY(19)=d2AOXY(19)+COEF_XXXY*(xA*xB*xB*(yA*d2EGxy+dEGx)+(xB*xB+TWO*xA*xB)*(yA*dEGy+EG))
        d2AOXZ(19)=d2AOXZ(19)+COEF_XXXY*(xA*yA*xB*xB*d2EGxz+(xB*xB+TWO*xA*xB)*yA*dEGz)
        d2AOYY(19)=d2AOYY(19)+COEF_XXXY*(xA*xB*xB*(yA*d2EGyy+TWO*dEGy))
        d2AOYZ(19)=d2AOYZ(19)+COEF_XXXY*(xA*xB*xB*(yA*d2EGyz+dEGz))
        d2AOZZ(19)=d2AOZZ(19)+COEF_XXXY*xA*yA*xB*xB*d2EGzz

        d2AOXX(20)=d2AOXX(20)+COEF_XXXY*(yA*yB*yB*(xA*d2EGxx+TWO*dEGx))
        d2AOXY(20)=d2AOXY(20)+COEF_XXXY*(yA*yB*yB*(xA*d2EGxy+dEGy)+(yB*yB+TWO*yA*yB)*(xA*dEGx+EG))
        d2AOXZ(20)=d2AOXZ(20)+COEF_XXXY*(yA*yB*yB*(xA*d2EGxz+dEGz))
        d2AOYY(20)=d2AOYY(20)+COEF_XXXY*(xA*yA*yB*yB*d2EGyy+TWO*(yB*yB+TWO*yA*yB)*xA*dEGy+TWO*(TWO*yB+yA)*xA*EG)
        d2AOYZ(20)=d2AOYZ(20)+COEF_XXXY*(xA*yA*yB*yB*d2EGyz+(yB*yB+TWO*yA*yB)*xA*dEGz)
        d2AOZZ(20)=d2AOZZ(20)+COEF_XXXY*xA*yA*yB*yB*d2EGzz

        d2AOXX(21)=d2AOXX(21)+COEF_XXXY*(yA*zB*zB*(xA*d2EGxx+TWO*dEGx))
        d2AOXY(21)=d2AOXY(21)+COEF_XXXY*(xA*zB*zB*(yA*d2EGxy+dEGx)+zB*zB*(yA*dEGy+EG))
        d2AOXZ(21)=d2AOXZ(21)+COEF_XXXY*(xA*yA*zB*(zB*d2EGxz+TWO*dEGx)+yA*zB*(zB*dEGz+TWO*EG))
        d2AOYY(21)=d2AOYY(21)+COEF_XXXY*(xA*zB*zB*(yA*d2EGyy+TWO*dEGy))
        d2AOYZ(21)=d2AOYZ(21)+COEF_XXXY*(xA*yA*zB*(zB*d2EGyz+TWO*dEGy)+xA*zB*(zB*dEGz+TWO*EG))
        d2AOZZ(21)=d2AOZZ(21)+COEF_XXXY*(xA*yA*zB*(zB*d2EGzz+FOUR*dEGz))
        
        d2AOXX(22)=d2AOXX(22)+COEF_XYXY*(yA*yB*(xA*xB*d2EGxx+TWO*(xA+xB)*dEGx+TWO*EG))
        d2AOXY(22)=d2AOXY(22)+COEF_XYXY*(xA*xB*(yA*yB*d2EGxy+(yA+yB)*dEGx)+(xA+xB)*(yA*yB*dEGy+(yA+yB)*EG))
        d2AOXZ(22)=d2AOXZ(22)+COEF_XYXY*(xA*yA*xB*yB*d2EGxz+(xA+xB)*yA*yB*dEGz)
        d2AOYY(22)=d2AOYY(22)+COEF_XYXY*(xA*xB*(yA*yB*d2EGyy+TWO*(yA+yB)*dEGy+TWO*EG))
        d2AOYZ(22)=d2AOYZ(22)+COEF_XYXY*(xA*xB*(yA*yB*d2EGyz+(yA+yB)*dEGz))
        d2AOZZ(22)=d2AOZZ(22)+COEF_XYXY*xA*yA*xB*yB*d2EGzz

        d2AOXX(23)=d2AOXX(23)+COEF_XYXY*(yA*zB*(xA*xB*d2EGxx+TWO*(xA+xB)*dEGx+TWO*EG))
        d2AOXY(23)=d2AOXY(23)+COEF_XYXY*(xA*xB*zB*(yA*d2EGxy+dEGx)+(xA+xB)*zB*(yA*dEGy+EG))
        d2AOXZ(23)=d2AOXZ(23)+COEF_XYXY*(xA*yA*xB*(zB*d2EGxz+dEGx)+(xA+xB)*yA*(zB*dEGz+EG))
        d2AOYY(23)=d2AOYY(23)+COEF_XYXY*(xA*xB*zB*(yA*d2EGyy+TWO*dEGy))
        d2AOYZ(23)=d2AOYZ(23)+COEF_XYXY*(xA*yA*xB*(zB*d2EGyz+dEGy)+xA*xB*(zB*dEGz+EG))
        d2AOZZ(23)=d2AOZZ(23)+COEF_XYXY*(xA*yA*xB*(zB*d2EGzz+TWO*dEGz))

        d2AOXX(24)=d2AOXX(24)+COEF_XYXY*(yA*yB*zB*(xA*d2EGxx+TWO*dEGx))
        d2AOXY(24)=d2AOXY(24)+COEF_XYXY*(xA*zB*(yA*yB*d2EGxy+(yA+yB)*dEGx)+zB*(yA*yB*dEGy+(yA+yB)*EG))
        d2AOXZ(24)=d2AOXZ(24)+COEF_XYXY*(xA*yA*yB*(zB*d2EGxz+dEGx)+yA*yB*(zB*dEGz+EG))
        d2AOYY(24)=d2AOYY(24)+COEF_XYXY*(xA*zB*(yA*yB*d2EGyy+TWO*(yA+yB)*dEGy+TWO*EG))
        d2AOYZ(24)=d2AOYZ(24)+COEF_XYXY*(xA*yA*yB*(zB*d2EGyz+dEGy)+(yA+yB)*xA*(zB*dEGz+EG))
        d2AOZZ(24)=d2AOZZ(24)+COEF_XYXY*(xA*yA*yB*(zB*d2EGzz+TWO*dEGz))

        d2AOXX(25)=d2AOXX(25)+COEF_XXXY*(xA*zA*xB*xB*d2EGxx+TWO*(xB*xB+TWO*xA*xB)*zA*dEGx+TWO*(TWO*xB+xA)*zA*EG)
        d2AOXY(25)=d2AOXY(25)+COEF_XXXY*(xA*zA*xB*xB*d2EGxy+(xB*xB+TWO*xA*xB)*zA*dEGy)
        d2AOXZ(25)=d2AOXZ(25)+COEF_XXXY*(xA*xB*xB*(zA*d2EGxz+dEGx)+(xB*xB+TWO*xA*xB)*(zA*dEGz+EG))
        d2AOYY(25)=d2AOYY(25)+COEF_XXXY*xA*zA*xB*xB*d2EGyy
        d2AOYZ(25)=d2AOYZ(25)+COEF_XXXY*xA*xB*xB*(zA*d2EGyz+dEGy)
        d2AOZZ(25)=d2AOZZ(25)+COEF_XXXY*(xA*xB*xB*(zA*d2EGzz+TWO*dEGz))

        d2AOXX(26)=d2AOXX(26)+COEF_XXXY*(zA*yB*yB*(xA*d2EGxx+TWO*dEGx))
        d2AOXY(26)=d2AOXY(26)+COEF_XXXY*(xA*zA*yB*(yB*d2EGxy+TWO*dEGx)+zA*yB*(yB*dEGy+TWO*EG))
        d2AOXZ(26)=d2AOXZ(26)+COEF_XXXY*(xA*yB*yB*(zA*d2EGxz+dEGx)+yB*yB*(zA*dEGz+EG))
        d2AOYY(26)=d2AOYY(26)+COEF_XXXY*(xA*zA*yB*(yB*d2EGyy+FOUR*dEGy))
        d2AOYZ(26)=d2AOYZ(26)+COEF_XXXY*(xA*yB*yB*(zA*d2EGyz+dEGy)+TWO*xA*yB*(zA*dEGz+EG))
        d2AOZZ(26)=d2AOZZ(26)+COEF_XXXY*(xA*yB*yB*(zA*d2EGzz+TWO*dEGz))

        d2AOXX(27)=d2AOXX(27)+COEF_XXXY*(zA*zB*zB*(xA*d2EGxx+TWO*dEGx))
        d2AOXY(27)=d2AOXY(27)+COEF_XXXY*(zA*zB*zB*(xA*d2EGxy+EG))
        d2AOXZ(27)=d2AOXZ(27)+COEF_XXXY*(zA*zB*zB*(xA*d2EGxz+dEGz)+(zB*zB+TWO*zA*zB)*(xA*dEGx+EG))
        d2AOYY(27)=d2AOYY(27)+COEF_XXXY*xA*zA*zB*zB*d2EGyy
        d2AOYZ(27)=d2AOYZ(27)+COEF_XXXY*(xA*zA*zB*zB*d2EGyz+(zB*zB+TWO*zA*zB)*xA*dEGy)
        d2AOZZ(27)=d2AOZZ(27)+COEF_XXXY*(xA*zA*zB*zB*d2EGzz+TWO*(zB*zB+TWO*zA*zB)*xA*dEGz+TWO*(TWO*zB+zA)*xA*EG)

        d2AOXX(28)=d2AOXX(28)+COEF_XYXY*(zA*yB*(xA*xB*d2EGxx+TWO*(xA+xB)*dEGx+TWO*EG))
        d2AOXY(28)=d2AOXY(28)+COEF_XYXY*(xA*zA*xB*(yB*d2EGxy+dEGx)+(xA+xB)*zA*(yB*dEGy+EG))
        d2AOXZ(28)=d2AOXZ(28)+COEF_XYXY*(xA*xB*yB*(zA*d2EGxz+dEGx)+(xA+xB)*yB*(zA*dEGz+EG))
        d2AOYY(28)=d2AOYY(28)+COEF_XYXY*(xA*zA*xB*(yB*d2EGyy+TWO*dEGy))
        d2AOYZ(28)=d2AOYZ(28)+COEF_XYXY*(xA*xB*yB*(zA*d2EGyz+dEGy)+xA*xB*(zA*dEGz+EG))
        d2AOZZ(28)=d2AOZZ(28)+COEF_XYXY*(xA*xB*yB*(zA*d2EGzz+TWO*dEGz))

        d2AOXX(29)=d2AOXX(29)+COEF_XYXY*(zA*zB*(xA*xB*d2EGxx+TWO*(xA+xB)*dEGx+TWO*EG))
        d2AOXY(29)=d2AOXY(29)+COEF_XYXY*(xA*zA*xB*zB*d2EGxy+(xA+xB)*zA*zB*dEGy)
        d2AOXZ(29)=d2AOXZ(29)+COEF_XYXY*(xA*xB*(zA*zB*d2EGxz+(zA+zB)*dEGx)+(xA+xB)*(zA*zB*dEGz+(zA+zB)*EG))
        d2AOYY(29)=d2AOYY(29)+COEF_XYXY*xA*zA*xB*zB*d2EGyy
        d2AOYZ(29)=d2AOYZ(29)+COEF_XYXY*xA*xB*(zA*zB*d2EGyz+(zA+zB)*dEGy)
        d2AOZZ(29)=d2AOZZ(29)+COEF_XYXY*(xA*xB*(zA*zB*d2EGzz+TWO*(zA+zB)*dEGz+TWO*EG))

        d2AOXX(30)=d2AOXX(30)+COEF_XYXY*(zA*yB*zB*(xA*d2EGxx+TWO*dEGx))
        d2AOXY(30)=d2AOXY(30)+COEF_XYXY*(xA*zA*zB*(yB*d2EGxy+dEGx)+zA*zB*(yB*dEGy+EG))
        d2AOXZ(30)=d2AOXZ(30)+COEF_XYXY*(xA*yB*(zA*zB*d2EGxz+(zA+zB)*dEGx)+yB*(zA*zB*dEGz+(zA+zB)*EG))
        d2AOYY(30)=d2AOYY(30)+COEF_XYXY*(xA*zA*zB*(yB*d2EGyy+TWO*dEGy))
        d2AOYZ(30)=d2AOYZ(30)+COEF_XYXY*(xA*yB*(zA*zB*d2EGyz+(zA+zB)*dEGy)+xA*(zA*zB*dEGz+(zA+zB)*EG))
        d2AOZZ(30)=d2AOZZ(30)+COEF_XYXY*(xA*yB*(zA*zB*d2EGzz+TWO*(zA+zB)*dEGz+TWO*EG))

        d2AOXX(31)=d2AOXX(31)+COEF_XXXY*yA*zA*(xB*xB*d2EGxx+FOUR*xB*dEGx+TWO*EG)
        d2AOXY(31)=d2AOXY(31)+COEF_XXXY*(xB*xB*zA*(yA*d2EGxy+dEGx)+TWO*xB*zA*(yA*dEGy+EG))
        d2AOXZ(31)=d2AOXZ(31)+COEF_XXXY*(xB*xB*yA*(zA*d2EGxz+dEGx)+TWO*xB*yA*(zA*dEGz+EG))
        d2AOYY(31)=d2AOYY(31)+COEF_XXXY*(xB*xB*zA*(yA*d2EGyy+TWO*dEGy))
        d2AOYZ(31)=d2AOYZ(31)+COEF_XXXY*(xB*xB*yA*(zA*d2EGyz+dEGy)+xB*xB*(zA*dEGz+EG))
        d2AOZZ(31)=d2AOZZ(31)+COEF_XXXY*(xB*xB*yA*(zA*d2EGzz+TWO*dEGz))

        d2AOXX(32)=d2AOXX(32)+COEF_XXXY*yB*yB*yA*zA*d2EGxx
        d2AOXY(32)=d2AOXY(32)+COEF_XXXY*(yB*yB*yA*zA*d2EGxy+(yB*yB+TWO*yB*yA)*zA*dEGx)
        d2AOXZ(32)=d2AOXZ(32)+COEF_XXXY*(yB*yB*yA*(zA*d2EGxz+dEGx))
        d2AOYY(32)=d2AOYY(32)+COEF_XXXY*(yB*yB*yA*zA*d2EGyy+TWO*(yB*yB+TWO*yB*yA)*zA*dEGy+TWO*(TWO*yB+yA)*zA*EG)
        d2AOYZ(32)=d2AOYZ(32)+COEF_XXXY*(yB*yB*yA*(zA*d2EGyz+dEGy)+(yB*yB+TWO*yB*yA)*(zA*dEGz+EG))
        d2AOZZ(32)=d2AOZZ(32)+COEF_XXXY*(yB*yB*yA*(zA*d2EGzz+TWO*dEGz))

        d2AOXX(32)=d2AOXX(32)+COEF_XXXY*yB*yB*yA*zA*d2EGxx
        d2AOXY(32)=d2AOXY(32)+COEF_XXXY*(yB*yB*yA*zA*d2EGxy+(yB*yB+TWO*yB*yA)*zA*dEGx)
        d2AOXZ(32)=d2AOXZ(32)+COEF_XXXY*(yB*yB*yA*(zA*d2EGxz+dEGx))
        d2AOYY(32)=d2AOYY(32)+COEF_XXXY*(yB*yB*yA*zA*d2EGyy+TWO*(yB*yB+TWO*yB*yA)*zA*dEGy+TWO*(TWO*yB+yA)*zA*EG)
        d2AOYZ(32)=d2AOYZ(32)+COEF_XXXY*(yB*yB*yA*(zA*d2EGyz+dEGy)+(yB*yB+TWO*yB*yA)*(zA*dEGz+EG))
        d2AOZZ(32)=d2AOZZ(32)+COEF_XXXY*(yB*yB*yA*(zA*d2EGzz+TWO*dEGz))

        d2AOXX(33)=d2AOXX(33)+COEF_XXXY*zB*zB*yA*zA*d2EGxx
        d2AOXY(33)=d2AOXY(33)+COEF_XXXY*zB*zB*zA*(yA*d2EGxy+dEGx)
        d2AOXZ(33)=d2AOXZ(33)+COEF_XXXY*(zB*zB*yA*zA*d2EGxz+(zB*zB+TWO*zB*zA)*yA*dEGx)
        d2AOYY(33)=d2AOYY(33)+COEF_XXXY*(zB*zB*zA*(yA*d2EGyy+TWO*dEGy))
        d2AOYZ(33)=d2AOYZ(33)+COEF_XXXY*(zB*zB*zA*(yA*d2EGyz+dEGz)+(zB*zB+TWO*zB*zA)*(yA*dEGy+EG))
        d2AOZZ(33)=d2AOZZ(33)+COEF_XXXY*(zB*zB*yA*zA*d2EGzz+TWO*(zB*zB+TWO*zB*zA)*yA*dEGz+TWO*(TWO*zB+zA)*yA*EG)

        d2AOXX(34)=d2AOXX(34)+COEF_XYXY*(yB*yA*zA*(xB*d2EGxx+TWO*dEGx))
        d2AOXY(34)=d2AOXY(34)+COEF_XYXY*(xB*zA*(yB*yA*d2EGxy+(yB+yA)*dEGx)+zA*(yB*yA*dEGy+(yB+yA)*EG))
        d2AOXZ(34)=d2AOXZ(34)+COEF_XYXY*(xB*yB*yA*(zA*d2EGxz+dEGx)+yB*yA*(zA*dEGz+EG))
        d2AOYY(34)=d2AOYY(34)+COEF_XYXY*(xB*zA*(yB*yA*d2EGyy+TWO*(yB+yA)*dEGy+TWO*EG))
        d2AOYZ(34)=d2AOYZ(34)+COEF_XYXY*(xB*yB*yA*(zA*d2EGyz+dEGy)+(yB+yA)*xB*(zA*dEGz+EG))
        d2AOZZ(34)=d2AOZZ(34)+COEF_XYXY*(xB*yB*yA*(zA*d2EGzz+TWO*dEGz))

        d2AOXX(35)=d2AOXX(35)+COEF_XYXY*(zB*yA*zA*(xB*d2EGxx+TWO*dEGx))
        d2AOXY(35)=d2AOXY(35)+COEF_XYXY*(xB*zB*zA*(yA*d2EGxy+dEGx)+zB*zA*(yA*dEGy+EG))
        d2AOXZ(35)=d2AOXZ(35)+COEF_XYXY*(xB*yA*(zB*zA*d2EGxz+(zB+zA)*dEGx)+yA*(zB*zA*dEGz+(zB+zA)*EG))
        d2AOYY(35)=d2AOYY(35)+COEF_XYXY*(xB*zB*zA*(yA*d2EGyy+TWO*dEGy))
        d2AOYZ(35)=d2AOYZ(35)+COEF_XYXY*(xB*yA*(zB*zA*d2EGyz+(zB+zA)*dEGy)+xB*(zB*zA*dEGz+(zB+zA)*EG))
        d2AOZZ(35)=d2AOZZ(35)+COEF_XYXY*(xB*yA*(zB*zA*d2EGzz+TWO*(zB+zA)*dEGz+TWO*EG))

        d2AOXX(36)=d2AOXX(36)+COEF_XYXY*yA*zA*yB*zB*d2EGxx
        d2AOXY(36)=d2AOXY(36)+COEF_XYXY*zA*zB*(yA*yB*d2EGxy+(yA+yB)*dEGx)
        d2AOXZ(36)=d2AOXZ(36)+COEF_XYXY*yA*yB*(zA*zB*d2EGxz+(zA+zB)*dEGx)
        d2AOYY(36)=d2AOYY(36)+COEF_XYXY*(zA*zB*(yA*yB*d2EGyy+TWO*(yA+yB)*dEGy+TWO*EG))
        d2AOYZ(36)=d2AOYZ(36)+COEF_XYXY*(yA*yB*(zA*zB*d2EGyz+(zA+zB)*dEGy)+(yA+yB)*(zA*zB*dEGz+(zA+zB)*EG))
        d2AOZZ(36)=d2AOZZ(36)+COEF_XYXY*(yA*yB*(zA*zB*d2EGzz+TWO*(zA+zB)*dEGz+TWO*EG))

      case default
        write(UNIout,*)'ERROR> dAO_PRODUCTS: DEFCASE type should not exist'
        stop ' ERROR> d2AO_PRODUCTS: DEFCASE type should not exist'
      end select
!
      end do ! Jgauss
      end do ! Igauss 
      
        IF(LIatmshl)then
          INDEX=0
          do I=1,Irange
          do J=1,Jrange
            INDEX=INDEX+1
            IF(I.GE.J)then  ! Corrected August 1,2003 had (J.ge.I)
              Iao=AOI+I
              Jao=AOJ+J
              IJao=Iao*(Iao-1)/2+Jao ! Not sure if this formula still works for storing stuff
              d2AOprodXX(IJao)=d2AOXX(INDEX)
              d2AOprodYY(IJao)=d2AOYY(INDEX)
              d2AOprodZZ(IJao)=d2AOZZ(INDEX)
              d2AOprodXY(IJao)=d2AOXY(INDEX)
              d2AOprodXZ(IJao)=d2AOXZ(INDEX)
              d2AOprodYZ(IJao)=d2AOYZ(INDEX)
            end if
          end do ! J   
          end do ! I
        else
          INDEX=0
          do I=1,Irange
          do J=1,Jrange
            INDEX=INDEX+1
            Iao=AOI+I
            Jao=AOJ+J
            IJao=Iao*(Iao-1)/2+Jao
              d2AOprodXX(IJao)=d2AOXX(INDEX)
              d2AOprodYY(IJao)=d2AOYY(INDEX)
              d2AOprodZZ(IJao)=d2AOZZ(INDEX)
              d2AOprodXY(IJao)=d2AOXY(INDEX)
              d2AOprodXZ(IJao)=d2AOXZ(INDEX)
              d2AOprodYZ(IJao)=d2AOYZ(INDEX)
          end do ! J
          end do ! I
        end if ! LIatmshl

      end do ! Jatmshl
      end do ! Iatmshl

      return
      end subroutine AB_d2products
      end subroutine d2G_PRODUCTS
