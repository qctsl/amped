! CHECK d loops for 1,6 not 1,3
      subroutine RAMint(Av,Bv,Cv,Dv,a,b,c,d,A_ang,B_ang,C_ang,D_ang,ram)
! 
!   *****************************************************************
!   *                                                               *
!   *  Calculates relative-angular momentum integrals using a       *
!   *  18-term RR.  
!   *                                                               *
!   *       --------------------- OUTPUT ---------------------      *
!   *       ram = relative-angular momentum integrals               *
!   *       --------------------------------------------------      *
!   *                                                               * 
!   *       --------------------- INPUT ----------------------      *
!   *       Av = cartesian coordinates of centre A                  *
!   *       Bv = cartesian coordinates of centre B                  *
!   *       Cv = cartesian coordinates of centre C                  *
!   *       Dv = cartesian coordinates of centre D                  *
!   *       a = exponent alpha (associated with centre A)           *
!   *       b = exponent beta (associated with centre B)            *
!   *       c = exponent gamma (associated with centre C)           *
!   *       d = exponent delta (associated with centre D)           *
!   *       A_ang = angular momentum of basis function on A         *
!   *       B_ang = angular momentum of basis function on B         *
!   *       C_ang = angular momentum of basis function on C         *
!   *       D_ang = angular momentum of basis function on D         * 
!   *       --------------------------------------------------      *
!   *                                                               *
!   *  For details of the equations contained in this section of    *
!   *  code, see Hollett 2024??                                     *
!   *                                                               *
!   *  code written by JWH (2024)                                   *
!   *                                                               *
!   *****************************************************************
!
! Modules:
      use RAM_RR
!
      implicit none
!
!---------------------------------------------------------------------
! Input/Output:
!---------------------------------------------------------------------
      integer :: A_ang, B_ang, C_ang, D_ang
      double precision, dimension(3) :: Av, Bv, Cv, Dv
      double precision :: a, b, c, d
      double precision, dimension(1296) :: ram 
!
!--------------------------------------------------------------------
! Local: 
!--------------------------------------------------------------------
!
      double precision, dimension(3) :: S, V
      double precision, dimension(3) :: S2, V2, SV
      double precision :: Eabcd
      integer :: mv(3), nv(3), pv(3)
      integer :: Lcode, i, j, mnp
      integer :: d1, d2, d3, d4, p1, p2, p3, p4
      integer :: dd_index, ddd_index, dddd_index
      integer :: dddp_index, ddpd_index, ddpp_index, dpdp_index, dppd_index, dppp_index, pdpd_index
      integer :: pdpp_index
      integer :: ddp_index, dpd_index, pdd_index, dpp_index, pdp_index
      integer :: dp_index, pd_index
!
! Function:
      integer :: D3index
!
!--------------------------------------------------------------------
! Begin: 
!--------------------------------------------------------------------
!
! Set angular momentum code
      Lcode = 1000*A_ang + 100*B_ang + 10*C_ang + D_ang
! Set angular momentum vectors, for higher functions
      Lvec_d=0
      Lvec_d(1,1)=2 ! xx
      Lvec_d(2,2)=2 ! yy
      Lvec_d(3,3)=2 ! zz
      Lvec_d(4,1)=1 ! xy
      Lvec_d(4,2)=1 
      Lvec_d(5,1)=1 ! xz
      Lvec_d(5,3)=1 
      Lvec_d(6,2)=1 ! yz
      Lvec_d(6,3)=1 
! Set xyz code for p functions
      xyz(1)=1
      xyz(2)=2
      xyz(3)=3
! Identity matrix
      imat = 0
      imat(1,1) = 1
      imat(2,2) = 1
      imat(3,3) = 1
!  
!--------------------------------------------------------------------
!  Calculate common intermediates, prefactor
!--------------------------------------------------------------------
!
      S  = 0.0D0
      S2 = 0.0D0
      V  = 0.0D0
      V2 = 0.0D0
      SV = 0.0D0
      do i=1,3 
        S(i)  = a*b*(Av(i) - Bv(i))/(a+b) - c*d*(Cv(i) - Dv(i))/(c+d)
        S2(i) = S(i)**2
        V(i)  = (a*Av(i) + b*Bv(i))/(a+b) - (c*Cv(i) + d*Dv(i))/(c+d)
        V2(i) = V(i)**2
        SV(i) = S(i)*V(i)
      end do
!
! calculate exponent first
      Eabcd = 0.0D0
      do i=1,3
        Eabcd = Eabcd - a*b*(Av(i)-Bv(i))**2/(a+b) - c*d*(Cv(i)-Dv(i))**2/(c+d)
      end do
      Eabcd = pi**3*exp(Eabcd)/(2.0D0*((a+b)*(c+d))**1.5D0)
!      
!-------------------------------------------------------------------
! Calculate required G's
!-------------------------------------------------------------------
!
! 2^9 = 512
!
      allocate(G(513))
!
      G = 0.0D0
!
! G(0)
      mv = 0
      nv = 0
      pv = 0
      mnp = D3index(mv,nv,pv)
!
! xyz
      G(mnp) = G(mnp) + (a*b/(a+b) + c*d/(c+d) - 2.0D0*S2(1))*((a+b+c+d)/((a+b)*(c+d)) + V2(2) + V2(3))
! yxz
      G(mnp) = G(mnp) + (a*b/(a+b) + c*d/(c+d) - 2.0D0*S2(2))*((a+b+c+d)/((a+b)*(c+d)) + V2(1) + V2(3))
! zxy
      G(mnp) = G(mnp) + (a*b/(a+b) + c*d/(c+d) - 2.0D0*S2(3))*((a+b+c+d)/((a+b)*(c+d)) + V2(1) + V2(2))
! xy
      G(mnp) = G(mnp) + (b/(a+b) + d/(c+d) + 2.0D0*SV(1))*(b/(a+b) + d/(c+d) + 2.0D0*SV(2))
! xz
      G(mnp) = G(mnp) + (b/(a+b) + d/(c+d) + 2.0D0*SV(1))*(b/(a+b) + d/(c+d) + 2.0D0*SV(3))
! yz
      G(mnp) = G(mnp) + (b/(a+b) + d/(c+d) + 2.0D0*SV(2))*(b/(a+b) + d/(c+d) + 2.0D0*SV(3))
! x
      G(mnp) = G(mnp) - 2.0D0*(b/(a+b) + d/(c+d) + 2.0D0*SV(1))
! y
      G(mnp) = G(mnp) - 2.0D0*(b/(a+b) + d/(c+d) + 2.0D0*SV(2))
! z
      G(mnp) = G(mnp) - 2.0D0*(b/(a+b) + d/(c+d) + 2.0D0*SV(3))
!
! first derivatives
!
! S2 -> m
! mx = 1
      mv(1) = 1
      mnp = D3index(mv,nv,pv)
!
      G(mnp) = -2.0D0*((a+b+c+d)/((a+b)*(c+d)) + V2(2) + V2(3))
!
! my = 1
      mv(1) = 0
      mv(2) = 1
      mnp = D3index(mv,nv,pv)
!
      G(mnp) = -2.0D0*((a+b+c+d)/((a+b)*(c+d)) + V2(1) + V2(3))
!
! mz = 1
      mv(2) = 0
      mv(3) = 1
      mnp = D3index(mv,nv,pv)
!
      G(mnp) = -2.0D0*((a+b+c+d)/((a+b)*(c+d)) + V2(1) + V2(2))
!
! V2 -> n
      mv = 0
! nx = 1
      nv(1) = 1
      mnp = D3index(mv,nv,pv)
!
      G(mnp) = 2.0D0*(a*b/(a+b) + c*d/(c+d) - S2(2) - S2(3))
!
! ny = 1
      nv(1) = 0
      nv(2) = 1
      mnp = D3index(mv,nv,pv)
!
      G(mnp) = 2.0D0*(a*b/(a+b) + c*d/(c+d) - S2(1) - S2(3))
!
! nz = 1
      nv(2) = 0
      nv(3) = 1
      mnp = D3index(mv,nv,pv)
!
      G(mnp) = 2.0D0*(a*b/(a+b) + c*d/(c+d) - S2(1) - S2(2))
!
! SV -> p 
      nv = 0
! px = 1
      pv(1) = 1
      mnp = D3index(mv,nv,pv)
!
      G(mnp) = 4.0D0*(b/(a+b) + d/(c+d) + SV(2) + SV(3)) - 4.0D0
!
! py = 1
      pv(1) = 0
      pv(2) = 1
      mnp = D3index(mv,nv,pv)
!
      G(mnp) = 4.0D0*(b/(a+b) + d/(c+d) + SV(1) + SV(3)) - 4.0D0
!
! pz = 1
      pv(2) = 0
      pv(3) = 1
      mnp = D3index(mv,nv,pv)
!
      G(mnp) = 4.0D0*(b/(a+b) + d/(c+d) + SV(1) + SV(2)) - 4.0D0
!
! second derivatives
      pv = 0
!
! dS2dV2 (mi = 1, nj = 1, i.ne.j)
! mx = 1, ny = 1      
      mv(1) = 1
      nv(2) = 1
      mnp = D3index(mv,nv,pv)
!
      G(mnp) = -2.0D0
!
! my = 1, nx = 1      
      mv = 0
      nv = 0
      mv(2) = 1
      nv(1) = 1
      mnp = D3index(mv,nv,pv)
!
      G(mnp) = -2.0D0
!
! mx = 1, nz = 1      
      mv = 0
      nv = 0
      mv(1) = 1
      nv(3) = 1
      mnp = D3index(mv,nv,pv)
!
      G(mnp) = -2.0D0
!
! mz = 1, nx = 1      
      mv = 0
      nv = 0
      mv(3) = 1
      nv(1) = 1
      mnp = D3index(mv,nv,pv)
!
      G(mnp) = -2.0D0
!
! my = 1, nz = 1      
      mv = 0
      nv = 0
      mv(2) = 1
      nv(3) = 1
      mnp = D3index(mv,nv,pv)
!
      G(mnp) = -2.0D0
!
! mz = 1, ny = 1      
      mv = 0
      nv = 0
      mv(3) = 1
      nv(2) = 1
      mnp = D3index(mv,nv,pv)
!
      G(mnp) = -2.0D0
!
! dSVdSV (pi = 1, pj = 1, i.ne.j)
      mv = 0
      nv = 0
!
! px = 1, py = 1      
      pv(1) = 1
      pv(2) = 1
      pv(3) = 0
      mnp = D3index(mv,nv,pv)
!
      G(mnp) = 4.0D0
!
! px = 1, pz = 1      
      pv(1) = 1
      pv(2) = 0
      pv(3) = 1
      mnp = D3index(mv,nv,pv)
!
      G(mnp) = 4.0D0
!
! py = 1, pz = 1      
      pv(1) = 0
      pv(2) = 1
      pv(3) = 1
      mnp = D3index(mv,nv,pv)
!
      G(mnp) = 4.0D0
!
! Combine with prefactors
!
      G = Eabcd*G
!
! Calculate coefficents for RR
      do i=1,3 ! x,y,z 
        RRa1(i) = b*(Bv(i) - Av(i))/(a+b)
        RRa2(i) = b*S(i)/(a+b)
        RRa3(i) = V(i)/(a+b)
        RRa4(i) = (S(i) + b*V(i))/(2.0D0*(a+b))
        RRb1(i) = a*(Av(i) - Bv(i))/(a+b)
        RRb2(i) = -a*S(i)/(a+b)
        RRb3(i) = V(i)/(a+b)
        RRb4(i) = (S(i) - a*V(i))/(2.0D0*(a+b))
        RRc1(i) = d*(Dv(i) - Cv(i))/(c+d)
        RRc2(i) = -d*S(i)/(c+d)
        RRc3(i) = -V(i)/(c+d)
        RRc4(i) = -(S(i) + d*V(i))/(2.0D0*(c+d))
        RRd1(i) = c*(Cv(i) - Dv(i))/(c+d)
        RRd2(i) = c*S(i)/(c+d)
        RRd3(i) = -V(i)/(c+d)
        RRd4(i) = (-S(i) + c*V(i))/(2.0D0*(c+d))
      end do ! i
!
!                                        Corresponding Integral
! RRa
      RRa5  = 1.0D0/(2.0D0*(a+b))         ! [(a-1i)bcd]^(m,n,p)
      RRa6  = b**2/(2.0D0*(a+b)**2)       ! [(a-1i)bcd]^(m+1,n,p)
      RRa7  = 1.0D0/(2.0D0*(a+b)**2)      ! [(a-1i)bcd]^(m,n+1,p)
      RRa8  = b/(2.0D0*(a+b)**2)          ! [(a-1i)bcd]^(m,n,p+1)
      RRa9  = 1.0D0/(2.0D0*(a+b))         ! [a(b-1i)cd]^(m,n,p)
      RRa10 = -a*b/(2.0D0*(a+b)**2)       ! [a(b-1i)cd]^(m+1,n,p)
      RRa11 = 1.0D0/(2.0D0*(a+b)**2)      ! [a(b-1i)cd]^(m,n+1,p)
      RRa12 = (b-a)/(4.0D0*(a+b)**2)      ! [a(b-1i)cd]^(m,n,p+1)
      RRa13 = -b*d/(2.0D0*(a+b)*(c+d))    ! [ab(c-1i)d]^(m+1,n,p)
      RRa14 = -1.0D0/(2.0D0*(a+b)*(c+d))  ! [ab(c-1i)d]^(m,n+1,p)
      RRa15 = -(b+d)/(4.0D0*(a+b)*(c+d))  ! [ab(c-1i)d]^(m,n,p+1)
      RRa16 = b*c/(2.0D0*(a+b)*(c+d))     ! [abc(d-1i)]^(m+1,n,p)
      RRa17 = -1.0D0/(2.0D0*(a+b)*(c+d))  ! [abc(d-1i)]^(m,n+1,p)
      RRa18 = (c-b)/(4.0D0*(a+b)*(c+d))   ! [abc(d-1i)]^(m,n,p+1)
!
! RRb
      RRb5  = RRa5                        ! [(a-1i)bcd]^(m,n,p)
      RRb6  = RRa10                       ! [(a-1i)bcd]^(m+1,n,p)
      RRb7  = RRa7                        ! [(a-1i)bcd]^(m,n+1,p)
      RRb8  = RRa12                       ! [(a-1i)bcd]^(m,n,p+1)
      RRb9  = RRa9                        ! [a(b-1i)cd]^(m,n,p)
      RRb10 = a**2/(2.0D0*(a+b)**2)       ! [a(b-1i)cd]^(m+1,n,p)
      RRb11 = RRa11                       ! [a(b-1i)cd]^(m,n+1,p)
      RRb12 = -a/(2.0D0*(a+b)**2)         ! [a(b-1i)cd]^(m,n,p+1)
      RRb13 = a*d/(2.0D0*(a+b)*(c+d))     ! [ab(c-1i)d]^(m+1,n,p)
      RRb14 = RRa14                       ! [ab(c-1i)d]^(m,n+1,p)
      RRb15 = (a-d)/(4.0D0*(a+b)*(c+d))   ! [ab(c-1i)d]^(m,n,p+1)
      RRb16 = -a*c/(2.0D0*(a+b)*(c+d))    ! [abc(d-1i)]^(m+1,n,p)
      RRb17 = RRa17                       ! [abc(d-1i)]^(m,n+1,p)
      RRb18 = (a+c)/(4.0D0*(a+b)*(c+d))   ! [abc(d-1i)]^(m,n,p+1)
!
! RRc
      RRc5  = RRa13                       ! [(a-1i)bcd]^(m+1,n,p)
      RRc6  = RRa14                       ! [(a-1i)bcd]^(m,n+1,p)
      RRc7  = RRa15                       ! [(a-1i)bcd]^(m,n,p+1)
      RRc8  = RRb13                       ! [a(b-1i)cd]^(m+1,n,p)
      RRc9  = RRb14                       ! [a(b-1i)cd]^(m,n+1,p)
      RRc10 = RRb15                       ! [a(b-1i)cd]^(m,n+1,p)
      RRc11 = 1.0D0/(2.0D0*(c+d))         ! [ab(c-1i)d]^(m,n,p)
      RRc12 = d**2/(2.0D0*(c+d)**2)       ! [ab(c-1i)d]^(m+1,n,p)
      RRc13 = 1.0D0/(2.0D0*(c+d)**2)      ! [ab(c-1i)d]^(m,n+1,p)
      RRc14 = d/(2.0D0*(c+d)**2)          ! [ab(c-1i)d]^(m,n,p+1)
      RRc15 = RRc11                       ! [abc(d-1i)]^(m,n,p)
      RRc16 = -c*d/(2.0D0*(c+d)**2)       ! [abc(d-1i)]^(m+1,n,p)
      RRc17 = RRc13                       ! [abc(d-1i)]^(m,n+1,p)
      RRc18 = (d-c)/(4.0D0*(c+d)**2)      ! [abc(d-1i)]^(m,n,p+1)
!
! RRd
      RRd5  = RRa16                       ! [(a-1i)bcd]^(m+1,n,p)
      RRd6  = RRa17                       ! [(a-1i)bcd]^(m,n+1,p)
      RRd7  = RRa18                       ! [(a-1i)bcd]^(m,n,p+1)
      RRd8  = RRb16                       ! [a(b-1i)cd]^(m+1,n,p)
      RRd9  = RRb17                       ! [a(b-1i)cd]^(m,n+1,p)
      RRd10 = RRb18                       ! [a(b-1i)cd]^(m,n,p+1)
      RRd11 = RRc11                       ! [ab(c-1i)d]^(m,n,p)
      RRd12 = RRc16                       ! [ab(c-1i)d]^(m+1,n,p)
      RRd13 = RRc13                       ! [ab(c-1i)d]^(m,n+1,p)
      RRd14 = RRc18                       ! [ab(c-1i)d]^(m,n,p+1)
      RRd15 = RRc15                       ! [abc(d-1i)]^(m,n,p)
      RRd16 = c**2/(2.0D0*(c+d)**2)       ! [abc(d-1i)]^(m+1,n,p)
      RRd17 = RRc17                       ! [abc(d-1i)]^(m,n+1,p)
      RRd18 = -c/(2.0D0*(c+d)**2)         ! [abc(d-1i)]^(m,n,p+1)
!
!------------------------------------------------------------------
! [ssss]
!------------------------------------------------------------------
      if(Lcode.eq.0)then
!
        ram(1) = G(1)
!
        deallocate(G)
        return
      end if
!
!------------------------------------------------------------------
! [psss]
!------------------------------------------------------------------
      if(Lcode.eq.1000)then
!
! Requires one step
        allocate(Int1000(3,1)) 
!
        mv = 0 
        nv = 0 
        pv = 0 
!
! Step 1 :
! [1000]^(0) from [0000]^(1)
!
        call calcRAM1000(mv,nv,pv) 
!
        ram(1:3) = Int1000(1:3,1)
!
        deallocate(G,Int1000)
        return
      end if
!
!------------------------------------------------------------------
! [spss]
!------------------------------------------------------------------
      if(Lcode.eq.100)then
!
! Requires one step
        allocate(Int0100(3,1)) 
!
        mv = 0 
        nv = 0 
        pv = 0 
!
! Step 1 :
! [0100]^(0) from [0000]^(1)
!
        call calcRAM0100(mv,nv,pv) 
!
        ram(1:3) = Int0100(1:3,1)
!
        deallocate(G,Int0100)
        return
      end if
!
!------------------------------------------------------------------
! [ssps]
!------------------------------------------------------------------
      if(Lcode.eq.10)then
!
! Requires one step
        allocate(Int0010(3,1)) 
!
        mv = 0 
        nv = 0 
        pv = 0 
!
! Step 1 :
! [0010]^(0) from [0000]^(1)
!
        call calcRAM0010(mv,nv,pv) 
!
        ram(1:3) = Int0010(1:3,1)
!
        deallocate(G,Int0010)
        return
      end if
!
!------------------------------------------------------------------
! [sssp]
!------------------------------------------------------------------
      if(Lcode.eq.1)then
!
! Requires one step
        allocate(Int0001(3,1)) 
!
! Step 1 :
! [0001]^(0) from [0000]^(1)
!
        mv = 0 
        nv = 0 
        pv = 0 
        call calcRAM0001(mv,nv,pv) 
!
        ram(1:3) = Int0001(1:3,1)
!
        deallocate(G,Int0001)
        return
      end if
!
!------------------------------------------------------------------
! [ppss]
!------------------------------------------------------------------
      if(Lcode.eq.1100)then
!
! Requires 2 steps
!
      allocate(Int1000(3,513),Int1100(9,1))
      Int1000 = 0.0D0
      Int1100 = 0.0D0
!
! Step 1 :
! [1000]^(1) from [0000]^(2)
! a+1i
!
      mv = 0 
      nv = 0 
      pv = 0 
      call calcRAM1000(mv,nv,pv)
      do i = 1,3
        call calcRAM1000(mv+imat(1:3,i),nv,pv)
        call calcRAM1000(mv,nv+imat(1:3,i),pv)
        call calcRAM1000(mv,nv,pv+imat(1:3,i))
      end do
!
! Step 2 :
! [1100]^(0) from [1000]^(1) and [0000]^(1)
! b+1i
!      
      call calcRAM1100(mv,nv,pv) 
!
      ram(1:9) = Int1100(1:9,1) 
!
        deallocate(G,Int1000,Int1100)
        return
      end if
!
!------------------------------------------------------------------
! [psps]
!------------------------------------------------------------------
      if(Lcode.eq.1010)then
!
! Requires 2 steps
!
      allocate(Int1000(3,513),Int1010(9,1))
      Int1000 = 0.0D0
      Int1010 = 0.0D0
!
! Step 1 :
! [1000]^(1) from [0000]^(2)
! a+1i
!
      mv = 0 
      nv = 0 
      pv = 0 
      call calcRAM1000(mv,nv,pv)
      do i = 1,3
        call calcRAM1000(mv+imat(1:3,i),nv,pv)
        call calcRAM1000(mv,nv+imat(1:3,i),pv)
        call calcRAM1000(mv,nv,pv+imat(1:3,i))
      end do
!
! Step 2 :
! [1010]^(0) from [1000]^(1) and [0000]^(1)
! c+1i
!      
      call calcRAM1010(mv,nv,pv) 
!
      ram(1:9) = Int1010(1:9,1) 
!
        deallocate(G,Int1000,Int1010)
        return
      end if
!
!------------------------------------------------------------------
! [pssp]
!------------------------------------------------------------------
      if(Lcode.eq.1001)then
!
! Requires 2 steps
!
      allocate(Int1000(3,513),Int1001(9,1))
      Int1000 = 0.0D0
      Int1001 = 0.0D0
!
! Step 1 :
! [1000]^(1) from [0000]^(2)
! a+1i
!
      mv = 0 
      nv = 0 
      pv = 0 
      call calcRAM1000(mv,nv,pv)
      do i = 1,3
        call calcRAM1000(mv+imat(1:3,i),nv,pv)
        call calcRAM1000(mv,nv+imat(1:3,i),pv)
        call calcRAM1000(mv,nv,pv+imat(1:3,i))
      end do
!
! Step 2 :
! [1001]^(0) from [1000]^(1) and [0000]^(1)
! d+1i
!
      call calcRAM1001(mv,nv,pv) 
!
      ram(1:9) = Int1001(1:9,1) 
!
        deallocate(G,Int1000,Int1001)
        return
      end if
!
!------------------------------------------------------------------
! [spsp]
!------------------------------------------------------------------
      if(Lcode.eq.101)then
!
! Requires 2 steps
!
      allocate(Int0100(3,513),Int0101(9,1))
      Int0100 = 0.0D0
      Int0101 = 0.0D0
!
! Step 1 :
! [0100]^(1) from [0000]^(2)
! b+1i
!
      mv = 0 
      nv = 0 
      pv = 0 
      call calcRAM0100(mv,nv,pv)
      do i = 1,3
        call calcRAM0100(mv+imat(1:3,i),nv,pv)
        call calcRAM0100(mv,nv+imat(1:3,i),pv)
        call calcRAM0100(mv,nv,pv+imat(1:3,i))
      end do
!
! Step 2 :
! [0101]^(0) from [0100]^(1) and [0000]^(1)
! d+1i
!
      call calcRAM0101(mv,nv,pv) 
!
      ram(1:9) = Int0101(1:9,1) 
!
        deallocate(G,Int0100,Int0101)
        return
      end if
!
!------------------------------------------------------------------
! [ppps]
!------------------------------------------------------------------
      if(Lcode.eq.1110)then
!
! Requires 4 steps
!
      allocate(Int1000(3,513),Int0100(3,513),Int1100(9,513),Int1110(27,1))
      Int1000 = 0.0D0
      Int0100 = 0.0D0
      Int1100 = 0.0D0
      Int1110 = 0.0D0
!
      mv = 0
      nv = 0
      pv = 0
!
! Step 1 :
! [1000]^(2) from [0000]^(3)
! a+1i
!
      call calcRAM1000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1000(mv+imat(1:3,i),nv,pv)
        call calcRAM1000(mv,nv+imat(1:3,i),pv)
        call calcRAM1000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 2 :
! [0100]^(1) from [0000]^(2)
! b+1i
!
      call calcRAM0100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0100(mv+imat(1:3,i),nv,pv)
        call calcRAM0100(mv,nv+imat(1:3,i),pv)
        call calcRAM0100(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 3 :
! [1100]^(1) from [1000]^(2) and [0000]^(2)
! b+1i
!
      call calcRAM1100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1100(mv+imat(1:3,i),nv,pv)
        call calcRAM1100(mv,nv+imat(1:3,i),pv)
        call calcRAM1100(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 4 :
! [1110]^(0) from [1100]^(1), [1000]^(1), and [0100]^(1)
! c+1i
!
      call calcRAM1110(mv,nv,pv) 
!
      ram(1:27) = Int1110(1:27,1)
!
        deallocate(G,Int1000,Int0100,Int1100,Int1110)
        return
      end if
!
!------------------------------------------------------------------
! [ppsp]
!------------------------------------------------------------------
      if(Lcode.eq.1101)then
!
! Requires 4 steps
!
      allocate(Int1000(3,513),Int0100(3,513),Int1100(9,513),Int1101(27,1))
      Int1000 = 0.0D0
      Int0100 = 0.0D0
      Int1100 = 0.0D0
      Int1101 = 0.0D0
!
      mv = 0
      nv = 0
      pv = 0
!
! Step 1 :
! [1000]^(2) from [0000]^(3)
! a+1i
!
      call calcRAM1000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1000(mv+imat(1:3,i),nv,pv)
        call calcRAM1000(mv,nv+imat(1:3,i),pv)
        call calcRAM1000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 2 :
! [0100]^(1) from [0000]^(2)
! b+1i
!
      call calcRAM0100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0100(mv+imat(1:3,i),nv,pv)
        call calcRAM0100(mv,nv+imat(1:3,i),pv)
        call calcRAM0100(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 3 :
! [1100]^(1) from [1000]^(2) and [0000]^(2)
! b+1i
!
      call calcRAM1100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1100(mv+imat(1:3,i),nv,pv)
        call calcRAM1100(mv,nv+imat(1:3,i),pv)
        call calcRAM1100(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 4 :
! [1101]^(0) from [1100]^(1), [1000]^(1), and [0100]^(1)
! d+1i
!
      call calcRAM1101(mv,nv,pv) 
!
      ram(1:27) = Int1101(1:27,1)
!
        deallocate(G,Int1000,Int0100,Int1100,Int1101)
        return
      end if
!
!------------------------------------------------------------------
! [sppp]
!------------------------------------------------------------------
      if(Lcode.eq.111)then
!
! Requires 4 steps
!
      allocate(Int0100(3,513),Int0010(3,513),Int0110(9,513),Int0111(27,1))
      Int0100 = 0.0D0
      Int0010 = 0.0D0
      Int0110 = 0.0D0
      Int0111 = 0.0D0
!
      mv = 0
      nv = 0
      pv = 0
!
! Step 1 :
! [0100]^(2) from [0000]^(3)
! b+1i
!
      call calcRAM0100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0100(mv+imat(1:3,i),nv,pv)
        call calcRAM0100(mv,nv+imat(1:3,i),pv)
        call calcRAM0100(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0100(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0100(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0100(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 2 :
! [0010]^(1) from [0000]^(2)
! c+1i
!
      call calcRAM0010(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0010(mv+imat(1:3,i),nv,pv)
        call calcRAM0010(mv,nv+imat(1:3,i),pv)
        call calcRAM0010(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 3 :
! [0110]^(1) from [0100]^(2) and [0000]^(2)
! c+1i
!
      call calcRAM0110(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0110(mv+imat(1:3,i),nv,pv)
        call calcRAM0110(mv,nv+imat(1:3,i),pv)
        call calcRAM0110(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 4 :
! [0111]^(0) from [0110]^(1), [0100]^(1), and [0010]^(1)
! d+1i
!
      call calcRAM0111(mv,nv,pv) 
!
      ram(1:27) = Int0111(1:27,1)
!
        deallocate(G,Int0100,Int0010,Int0110,Int0111)
        return
      end if
!
!------------------------------------------------------------------
! [pppp]
!------------------------------------------------------------------
      if(Lcode.eq.1111)then
!
! Requires 7 steps
      allocate(Int1000(3,513),Int0100(3,513),Int1100(9,513),Int1010(9,513))
      allocate(Int0110(9,513),Int1110(27,513),Int1111(81,1))
      Int1000 = 0.0D0
      Int0100 = 0.0D0
      Int1100 = 0.0D0
      Int1010 = 0.0D0
      Int0110 = 0.0D0
      Int1110 = 0.0D0
      Int1111 = 0.0D0
!
      mv = 0
      nv = 0
      pv = 0
!
! Step 1 :
! [1000]^(3) from [0000]^(4)
! a+1i
!
      call calcRAM1000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1000(mv+imat(1:3,i),nv,pv)
        call calcRAM1000(mv,nv+imat(1:3,i),pv)
        call calcRAM1000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 2 :
! [0100]^(2) from [0000]^(3) 
! b+1i
!
      call calcRAM0100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0100(mv+imat(1:3,i),nv,pv)
        call calcRAM0100(mv,nv+imat(1:3,i),pv)
        call calcRAM0100(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0100(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0100(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0100(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 3 :
! [1100]^(2) from [1000]^(3) and [0000]^(3)
! b+1i
!
      call calcRAM1100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1100(mv+imat(1:3,i),nv,pv)
        call calcRAM1100(mv,nv+imat(1:3,i),pv)
        call calcRAM1100(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1100(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1100(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1100(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 4 :
! [1010]^(1) from [1000]^(2) and [0000]^(2)
! c+1i
!
      call calcRAM1010(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1010(mv+imat(1:3,i),nv,pv)
        call calcRAM1010(mv,nv+imat(1:3,i),pv)
        call calcRAM1010(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 5 :
! [0110]^(1) from [0100]^(2) and [0000]^(2)
! c+1i
!
      call calcRAM0110(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0110(mv+imat(1:3,i),nv,pv)
        call calcRAM0110(mv,nv+imat(1:3,i),pv)
        call calcRAM0110(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 6 :
! [1110]^(1) from [1100]^(2), [0100]^(2) and [1000]^(2)
! c+1i
!
      call calcRAM1110(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1110(mv+imat(1:3,i),nv,pv)
        call calcRAM1110(mv,nv+imat(1:3,i),pv)
        call calcRAM1110(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 7 :
! [1111]^(0) from [1110]^(1), [0110]^(1), [1010]^(1) and [1100]^(1)
! d+1i
!
        call calcRAM1111(mv,nv,pv)
!
        ram(1:81) = Int1111(1:81,1)
!
        deallocate(G,Int1000,Int0100,Int1100,Int1010)
        deallocate(Int0110,Int1110,Int1111)
        return
      end if
!
!------------------------------------------------------------------
! [dsss]
!------------------------------------------------------------------
      if(Lcode.eq.2000)then
!
! Requires 2 steps
!
      mv = 0
      nv = 0
      pv = 0
!
! Step 1 :
! [1000]^(1) from [0000]^(2)
! a+1i
!
      allocate(Int1000(3,513))
      Int1000 = 0.0D0
!
      call calcRAM1000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1000(mv+imat(1:3,i),nv,pv)
        call calcRAM1000(mv,nv+imat(1:3,i),pv)
        call calcRAM1000(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 2 :
! [2000]^(0) from [1000]^(1)
! a+1i
!
      allocate(Int2000(6,1))
      Int2000 = 0.0D0
!
      call calcRAM2000(mv,nv,pv)
!
      ram(1:3) = Int2000(1:3,1)
      ram(4:6) = sq3*Int2000(4:6,1)
!
      deallocate(G,Int2000,Int1000)
      return
    end if
!
!------------------------------------------------------------------
! [sdss]
!------------------------------------------------------------------
      if(Lcode.eq.200)then
!
! Requires 2 steps
!
      mv = 0
      nv = 0
      pv = 0
!
! Step 1 :
! [0100]^(1) from [0000]^(2)
! b+1i
!
      allocate(Int0100(3,513))
      Int0100 = 0.0D0
!
      call calcRAM0100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0100(mv+imat(1:3,i),nv,pv)
        call calcRAM0100(mv,nv+imat(1:3,i),pv)
        call calcRAM0100(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 2 :
! [0200]^(0) from [0100]^(1)
! b+1i
!
      allocate(Int0200(6,1))
      Int0200 = 0.0D0
!
      call calcRAM0200(mv,nv,pv)
!
      ram(1:3) = Int0200(1:3,1)
      ram(4:6) = sq3*Int0200(4:6,1)
!
      deallocate(G,Int0200,Int0100)
      return
    end if
!
!------------------------------------------------------------------
! [dpss]
!------------------------------------------------------------------
      if(Lcode.eq.2100)then
!
! Requires 3 steps
!
      mv = 0
      nv = 0
      pv = 0
!
! Step 1 :
! [1000]^(2) from [0000]^(3)
! a+1i
!
      allocate(Int1000(3,513))
      Int1000 = 0.0D0
!
      call calcRAM1000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1000(mv+imat(1:3,i),nv,pv)
        call calcRAM1000(mv,nv+imat(1:3,i),pv)
        call calcRAM1000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 2:
! [2000]^(1) from [1000]^(2) and [0000]^(2)
! a+1i
!
      allocate(Int2000(6,513))
      Int2000 = 0.0D0
!
      call calcRAM2000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2000(mv+imat(1:3,i),nv,pv)
        call calcRAM2000(mv,nv+imat(1:3,i),pv)
        call calcRAM2000(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 3:
! [2100]^(0) from [2000]^(1) and [1000]^(1)
! b+1i
!
      allocate(Int2100(18,1))
      Int2100 = 0.0D0
!
      call calcRAM2100(mv,nv,pv)
!
      ram(1:18) = Int2100(1:18,1)
!
      do d1=4,6
        do p2=1,3
          dp_index=3*(d1-1)+p2
          ram(dp_index) = sq3*ram(dp_index)
        end do
      end do
!
      deallocate(G,Int1000,Int2000,Int2100)
      return
      end if
!
!------------------------------------------------------------------
! [dsps]
!------------------------------------------------------------------
      if(Lcode.eq.2010)then
!
! Requires 3 steps
!
      mv = 0
      nv = 0
      pv = 0
!
! Step 1 :
! [1000]^(2) from [0000]^(3)
! a+1i
!
      allocate(Int1000(3,513))
      Int1000 = 0.0D0
!
      call calcRAM1000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1000(mv+imat(1:3,i),nv,pv)
        call calcRAM1000(mv,nv+imat(1:3,i),pv)
        call calcRAM1000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 2:
! [2000]^(1) from [1000]^(2) and [0000]^(2)
! a+1i
!
      allocate(Int2000(6,513))
      Int2000 = 0.0D0
!
      call calcRAM2000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2000(mv+imat(1:3,i),nv,pv)
        call calcRAM2000(mv,nv+imat(1:3,i),pv)
        call calcRAM2000(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 3:
! [2010]^(0) from [2000]^(1) and [1000]^(1)
! c+1i
!
      allocate(Int2010(18,1))
      Int2010 = 0.0D0
!
      call calcRAM2010(mv,nv,pv)
!
      ram(1:18) = Int2010(1:18,1)
!
      do d1=4,6
        do p2=1,3
          dp_index=3*(d1-1)+p2
          ram(dp_index) = sq3*ram(dp_index)
        end do
      end do
!
      deallocate(G,Int1000,Int2000,Int2010)
      return
      end if
!
!------------------------------------------------------------------
! [dssp]
!------------------------------------------------------------------
      if(Lcode.eq.2001)then
!
! Requires 3 steps
!
      mv = 0
      nv = 0
      pv = 0
!
! Step 1 :
! [1000]^(2) from [0000]^(3)
! a+1i
!
      allocate(Int1000(3,513))
      Int1000 = 0.0D0
!
      call calcRAM1000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1000(mv+imat(1:3,i),nv,pv)
        call calcRAM1000(mv,nv+imat(1:3,i),pv)
        call calcRAM1000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 2:
! [2000]^(1) from [1000]^(2) and [0000]^(2)
! a+1i
!
      allocate(Int2000(6,513))
      Int2000 = 0.0D0
!
      call calcRAM2000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2000(mv+imat(1:3,i),nv,pv)
        call calcRAM2000(mv,nv+imat(1:3,i),pv)
        call calcRAM2000(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 3:
! [2001]^(0) from [2000]^(1) and [1000]^(1)
! d+1i
!
      allocate(Int2001(18,1))
      Int2001 = 0.0D0
!
      call calcRAM2001(mv,nv,pv)
!
      ram(1:18) = Int2001(1:18,1)
!
      do d1=4,6
        do p2=1,3
          dp_index=3*(d1-1)+p2
          ram(dp_index) = sq3*ram(dp_index)
        end do
      end do
!
      deallocate(G,Int1000,Int2000,Int2001)
      return
      end if
!
!------------------------------------------------------------------
! [pdss]
!------------------------------------------------------------------
      if(Lcode.eq.1200)then
!
! Requires 3 steps
!
      mv = 0
      nv = 0
      pv = 0
!
! Step 1 :
! [0100]^(2) from [0000]^(3)
! b+1i
!
      allocate(Int0100(3,513))
      Int0100 = 0.0D0
!
      call calcRAM0100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0100(mv+imat(1:3,i),nv,pv)
        call calcRAM0100(mv,nv+imat(1:3,i),pv)
        call calcRAM0100(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0100(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0100(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0100(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 2:
! [0200]^(1) from [0100]^(2) and [0000]^(2)
! b+1i
!
      allocate(Int0200(6,513))
      Int0200 = 0.0D0
!
      call calcRAM0200(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0200(mv+imat(1:3,i),nv,pv)
        call calcRAM0200(mv,nv+imat(1:3,i),pv)
        call calcRAM0200(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 3:
! [1200]^(0) from [0200]^(1) and [0100]^(1)
! a+1i
!
      allocate(Int1200(18,1))
      Int1200 = 0.0D0
!
      call calcRAM1200(mv,nv,pv)
!
      ram(1:18) = Int1200(1:18,1)
!
      do p1=1,3
        do d2=4,6
          pd_index=6*(p1-1)+d2
          ram(pd_index) = sq3*ram(pd_index)
        end do
      end do
!
      deallocate(G,Int0100,Int0200,Int1200)
      return
      end if
!
!------------------------------------------------------------------
! [sdps]
!------------------------------------------------------------------
      if(Lcode.eq.210)then
!
! Requires 3 steps
!
      mv = 0
      nv = 0
      pv = 0
!
! Step 1 :
! [0100]^(2) from [0000]^(3)
! b+1i
!
      allocate(Int0100(3,513))
      Int0100 = 0.0D0
!
      call calcRAM0100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0100(mv+imat(1:3,i),nv,pv)
        call calcRAM0100(mv,nv+imat(1:3,i),pv)
        call calcRAM0100(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0100(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0100(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0100(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 2:
! [0200]^(1) from [0100]^(2) and [0000]^(2)
! b+1i
!
      allocate(Int0200(6,513))
      Int0200 = 0.0D0
!
      call calcRAM0200(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0200(mv+imat(1:3,i),nv,pv)
        call calcRAM0200(mv,nv+imat(1:3,i),pv)
        call calcRAM0200(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 3:
! [0210]^(0) from [0200]^(1) and [0100]^(1)
! c+1i
!
      allocate(Int0210(18,1))
      Int0210 = 0.0D0
!
      call calcRAM0210(mv,nv,pv)
!
      ram(1:18) = Int0210(1:18,1)
!
      do d1=4,6
        do p2=1,3
          dp_index=3*(d1-1)+p2
          ram(dp_index) = sq3*ram(dp_index)
        end do
      end do
!
      deallocate(G,Int0100,Int0200,Int0210)
      return
      end if
!
!------------------------------------------------------------------
! [sdsp]
!------------------------------------------------------------------
      if(Lcode.eq.201)then
!
! Requires 3 steps
!
      mv = 0
      nv = 0
      pv = 0
!
! Step 1 :
! [0100]^(2) from [0000]^(3)
! b+1i
!
      allocate(Int0100(3,513))
      Int0100 = 0.0D0
!
      call calcRAM0100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0100(mv+imat(1:3,i),nv,pv)
        call calcRAM0100(mv,nv+imat(1:3,i),pv)
        call calcRAM0100(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0100(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0100(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0100(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 2:
! [0200]^(1) from [0100]^(2) and [0000]^(2)
! b+1i
!
      allocate(Int0200(6,513))
      Int0200 = 0.0D0
!
      call calcRAM0200(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0200(mv+imat(1:3,i),nv,pv)
        call calcRAM0200(mv,nv+imat(1:3,i),pv)
        call calcRAM0200(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 3:
! [0201]^(0) from [0200]^(1) and [0100]^(1)
! d+1i
!
      allocate(Int0201(18,513))
      Int0201 = 0.0D0
!
      call calcRAM0201(mv,nv,pv)
!
      ram(1:18) = Int0201(1:18,1)
!
      do d1=4,6
        do p2=1,3
          dp_index=3*(d1-1)+p2
          ram(dp_index) = sq3*ram(dp_index)
        end do
      end do
!
      deallocate(G,Int0100,Int0200,Int0201)
      return
      end if
!
!------------------------------------------------------------------
! [dpps]
!------------------------------------------------------------------
      if(Lcode.eq.2110)then
!
! Requires 5 steps
!
      mv = 0
      nv = 0
      pv = 0
!
! Step 1 :
! [1000]^(3) from [0000]^(4)
! a+1i
!
      allocate(Int1000(3,513))
      Int1000 = 0.0D0
!
      call calcRAM1000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1000(mv+imat(1:3,i),nv,pv)
        call calcRAM1000(mv,nv+imat(1:3,i),pv)
        call calcRAM1000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 2:  
! [1100]^(1) from [1000]^(2) and [0000]^(2)
! b+1i
!
      allocate(Int1100(9,513))
      Int1100 = 0.0D0
!
      call calcRAM1100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1100(mv+imat(1:3,i),nv,pv)
        call calcRAM1100(mv,nv+imat(1:3,i),pv)
        call calcRAM1100(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 3 :
! [2000]^(2) from [1000]^(3) and [0000]^(3)
! a+1i
!
      allocate(Int2000(6,513))
      Int2000 = 0.0D0
!
      call calcRAM2000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2000(mv+imat(1:3,i),nv,pv)
        call calcRAM2000(mv,nv+imat(1:3,i),pv)
        call calcRAM2000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 4:
! [2100]^(1) from [2000]^(2) and [1000]^(2)
! b+1i
!
      allocate(Int2100(18,513))
      Int2100 = 0.0D0
!
      call calcRAM2100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2100(mv+imat(1:3,i),nv,pv)
        call calcRAM2100(mv,nv+imat(1:3,i),pv)
        call calcRAM2100(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 5:
! [2110]^(0) from [2100]^(1), [2000]^(1) and [1100]^(1)
! c+1i
!
      allocate(Int2110(54,1))
      Int2110 = 0.0D0
!
      call calcRAM2110c(mv,nv,pv)
!
      ram(1:54) = Int2110(1:54,1)
!
      do d1=4,6
        do p2=1,3
          do p3=1,3
            dpp_index=9*(d1-1)+3*(p2-1)+p3
            ram(dpp_index) = sq3*ram(dpp_index)
          end do
        end do
      end do
!
      deallocate(G,Int1000,Int1100,Int2000,Int2100,Int2110)
      return
      end if
!
!------------------------------------------------------------------
! [dpsp]
!------------------------------------------------------------------
      if(Lcode.eq.2101)then
!
! Requires 5 steps
!
      mv = 0
      nv = 0
      pv = 0
!
! Step 1 :
! [1000]^(3) from [0000]^(4)
! a+1i
!
      allocate(Int1000(3,513))
      Int1000 = 0.0D0
!
      call calcRAM1000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1000(mv+imat(1:3,i),nv,pv)
        call calcRAM1000(mv,nv+imat(1:3,i),pv)
        call calcRAM1000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 2:  
! [1001]^(1) from [1000]^(2) and [0000]^(2)
! d+1i
!
      allocate(Int1001(9,513))
      Int1001 = 0.0D0
!
      call calcRAM1001(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1001(mv+imat(1:3,i),nv,pv)
        call calcRAM1001(mv,nv+imat(1:3,i),pv)
        call calcRAM1001(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 3 :
! [2000]^(2) from [1000]^(3) and [0000]^(3)
! a+1i
!
      allocate(Int2000(6,513))
      Int2000 = 0.0D0
!
      call calcRAM2000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2000(mv+imat(1:3,i),nv,pv)
        call calcRAM2000(mv,nv+imat(1:3,i),pv)
        call calcRAM2000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 4:
! [2001]^(1) from [2000]^(2) and [1000]^(2)
! d+1i
!
      allocate(Int2001(18,513))
      Int2001 = 0.0D0
!
      call calcRAM2001(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2001(mv+imat(1:3,i),nv,pv)
        call calcRAM2001(mv,nv+imat(1:3,i),pv)
        call calcRAM2001(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 5:
! [2101]^(0) from [2001]^(1), [2000]^(1) and [1001]^(1)
! b+1i
!
      allocate(Int2101(54,1))
      Int2101 = 0.0D0
!
      call calcRAM2101b(mv,nv,pv)
!
      ram(1:54) = Int2101(1:54,1)
!
      do d1=4,6
        do p2=1,3
          do p3=1,3
            dpp_index=9*(d1-1)+3*(p2-1)+p3
            ram(dpp_index) = sq3*ram(dpp_index)
          end do
        end do
      end do
!
      deallocate(G,Int1000,Int1001,Int2000,Int2001,Int2101)
      return
      end if
!
!------------------------------------------------------------------
! [dspp]
!------------------------------------------------------------------
      if(Lcode.eq.2011)then
!
! Requires 5 steps
!
      mv = 0
      nv = 0
      pv = 0
!
! Step 1 :
! [1000]^(3) from [0000]^(4)
! a+1i
!
      allocate(Int1000(3,513))
      Int1000 = 0.0D0
!
      call calcRAM1000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1000(mv+imat(1:3,i),nv,pv)
        call calcRAM1000(mv,nv+imat(1:3,i),pv)
        call calcRAM1000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 2:  
! [1010]^(1) from [1000]^(2) and [0000]^(2)
! c+1i
!
      allocate(Int1010(9,513))
      Int1010 = 0.0D0
!
      call calcRAM1010(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1010(mv+imat(1:3,i),nv,pv)
        call calcRAM1010(mv,nv+imat(1:3,i),pv)
        call calcRAM1010(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 3 :
! [2000]^(2) from [1000]^(3) and [0000]^(3)
! a+1i
!
      allocate(Int2000(6,513))
      Int2000 = 0.0D0
!
      call calcRAM2000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2000(mv+imat(1:3,i),nv,pv)
        call calcRAM2000(mv,nv+imat(1:3,i),pv)
        call calcRAM2000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 4:
! [2010]^(1) from [2000]^(2) and [1000]^(2)
! c+1i
!
      allocate(Int2010(18,513))
      Int2010 = 0.0D0
!
      call calcRAM2010(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2010(mv+imat(1:3,i),nv,pv)
        call calcRAM2010(mv,nv+imat(1:3,i),pv)
        call calcRAM2010(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 5:
! [2011]^(0) from [2010]^(1), [2000]^(1) and [1010]^(1)
! d+1i
!
      allocate(Int2011(54,1))
      Int2011 = 0.0D0
!
      call calcRAM2011(mv,nv,pv)
!
      ram(1:54) = Int2011(1:54,1)
!
      do d1=4,6
        do p2=1,3
          do p3=1,3
            dpp_index=9*(d1-1)+3*(p2-1)+p3
            ram(dpp_index) = sq3*ram(dpp_index)
          end do
        end do
      end do
!
      deallocate(G,Int1000,Int1010,Int2000,Int2010,Int2011)
      return
      end if
!
!------------------------------------------------------------------
! [sdpp]
!------------------------------------------------------------------
      if(Lcode.eq.211)then
!
! Requires 5 steps
!
      mv = 0
      nv = 0
      pv = 0
!
! Step 1 :
! [0100]^(3) from [0000]^(4)
! b+1i
!
      allocate(Int0100(3,513))
      Int0100 = 0.0D0
!
      call calcRAM0100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0100(mv+imat(1:3,i),nv,pv)
        call calcRAM0100(mv,nv+imat(1:3,i),pv)
        call calcRAM0100(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0100(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0100(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0100(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 2:  
! [0110]^(1) from [0100]^(2) and [0000]^(2)
! c+1i
!
      allocate(Int0110(9,513))
      Int0110 = 0.0D0
!
      call calcRAM0110(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0110(mv+imat(1:3,i),nv,pv)
        call calcRAM0110(mv,nv+imat(1:3,i),pv)
        call calcRAM0110(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 3 :
! [0200]^(2) from [0100]^(3) and [0000]^(3)
! b+1i
!
      allocate(Int0200(6,513))
      Int0200 = 0.0D0
!
      call calcRAM0200(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0200(mv+imat(1:3,i),nv,pv)
        call calcRAM0200(mv,nv+imat(1:3,i),pv)
        call calcRAM0200(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0200(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0200(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0200(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 4:
! [0210]^(1) from [0200]^(2) and [0100]^(2)
! c+1i
!
      allocate(Int0210(18,513))
      Int0210 = 0.0D0
!
      call calcRAM0210(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0210(mv+imat(1:3,i),nv,pv)
        call calcRAM0210(mv,nv+imat(1:3,i),pv)
        call calcRAM0210(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 5:
! [0211]^(0) from [0210]^(1), [0200]^(1) and [0110]^(1)
! d+1i
!
      allocate(Int0211(54,1))
      Int0211 = 0.0D0
!
      call calcRAM0211(mv,nv,pv)
!
      ram(1:54) = Int0211(1:54,1)
!
      do d1=4,6
        do p2=1,3
          do p3=1,3
            dpp_index=9*(d1-1)+3*(p2-1)+p3
            ram(dpp_index) = sq3*ram(dpp_index)
          end do
        end do
      end do
!
      deallocate(G,Int0100,Int0110,Int0200,Int0210,Int0211) 
      return
      end if
!
!------------------------------------------------------------------
! [pdps]
!------------------------------------------------------------------
      if(Lcode.eq.1210)then
!
! Requires 6 steps
!
      mv = 0
      nv = 0
      pv = 0
!
! Step 1 :
! [1000]^(3) from [0000]^(4)
! a+1i
!
      allocate(Int1000(3,513))
      Int1000 = 0.0D0
!
      call calcRAM1000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1000(mv+imat(1:3,i),nv,pv)
        call calcRAM1000(mv,nv+imat(1:3,i),pv)
        call calcRAM1000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 2:  
! [0100]^(3) from [0000]^(4)
! b+1i
!
      allocate(Int0100(3,513))
      Int0100 = 0.0D0
!
      call calcRAM0100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0100(mv+imat(1:3,i),nv,pv)
        call calcRAM0100(mv,nv+imat(1:3,i),pv)
        call calcRAM0100(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0100(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0100(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0100(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 3 :
! [1100]^(1) from [1000]^(2) and [0000]^(2)
! b+1i
!
      allocate(Int1100(9,513))
      Int1100 = 0.0D0
!
      call calcRAM1100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1100(mv+imat(1:3,i),nv,pv)
        call calcRAM1100(mv,nv+imat(1:3,i),pv)
        call calcRAM1100(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 4:
! [0200]^(2) from [0100]^(3) and [0100]^(3)
! b+1i
!
      allocate(Int0200(6,513))
      Int0200 = 0.0D0
!
      call calcRAM0200(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0200(mv+imat(1:3,i),nv,pv)
        call calcRAM0200(mv,nv+imat(1:3,i),pv)
        call calcRAM0200(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0200(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0200(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0200(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 5:
! [1200]^(1) from [0200]^(2) and [0100]^(2)
! a+1i
!
      allocate(Int1200(18,513))
      Int1200 = 0.0D0
!
      call calcRAM1200(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1200(mv+imat(1:3,i),nv,pv)
        call calcRAM1200(mv,nv+imat(1:3,i),pv)
        call calcRAM1200(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 6:
! [1210]^(0) from [1200]^(1), [0200]^(1) and [1100]^(1)
!
      allocate(Int1210(54,1))
      Int1210 = 0.0D0
!
      call calcRAM1210(mv,nv,pv)
!
      ram(1:54) = Int1210(1:54,1)
!
      do p1=1,3
        do d2=4,6
          do p3=1,3
            pdp_index=18*(p1-1)+3*(d2-1)+p3
            ram(pdp_index) = sq3*ram(pdp_index)
          end do
        end do
      end do
!
      deallocate(G,Int1000,Int0100,Int1100,Int0200,Int1200,Int1210)
      return
      end if
!
!------------------------------------------------------------------
! [pdsp]
!------------------------------------------------------------------
      if(Lcode.eq.1201)then
!
! Requires 5 steps
!
      mv = 0
      nv = 0
      pv = 0
!
! Step 1 :
! [0100]^(3) from [0000]^(4)
! b+1i
!
      allocate(Int0100(3,513))
      Int0100 = 0.0D0
!
      call calcRAM0100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0100(mv+imat(1:3,i),nv,pv)
        call calcRAM0100(mv,nv+imat(1:3,i),pv)
        call calcRAM0100(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0100(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0100(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0100(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 2:  
! [0101]^(1) from [0100]^(2) and [0000]^(2)
! d+1i
!
      allocate(Int0101(9,513))
      Int0101 = 0.0D0
!
      call calcRAM0101(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0101(mv+imat(1:3,i),nv,pv)
        call calcRAM0101(mv,nv+imat(1:3,i),pv)
        call calcRAM0101(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 3 :
! [0200]^(2) from [0100]^(3) and [0000]^(3)
! b+1i
!
      allocate(Int0200(6,513))
      Int0200 = 0.0D0
!
      call calcRAM0200(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0200(mv+imat(1:3,i),nv,pv)
        call calcRAM0200(mv,nv+imat(1:3,i),pv)
        call calcRAM0200(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0200(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0200(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0200(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 4:
! [0201]^(1) from [0200]^(2) and [0100]^(2)
! d+1i
!
      allocate(Int0201(18,513))
      Int0201 = 0.0D0
!
      call calcRAM0201(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0201(mv+imat(1:3,i),nv,pv)
        call calcRAM0201(mv,nv+imat(1:3,i),pv)
        call calcRAM0201(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 5:
! [1201]^(0) from [0201]^(1), [0200]^(1) and [0101]^(1)
! a+1i
!
      allocate(Int1201(54,1))
      Int1201 = 0.0D0
!
      call calcRAM1201a(mv,nv,pv)
!
      ram(1:54) = Int1201(1:54,1)
!
      do p1=1,3
        do d2=4,6
          do p3=1,3
            pdp_index=18*(p1-1)+3*(d2-1)+p3
            ram(pdp_index) = sq3*ram(pdp_index)
          end do
        end do
      end do
!
      deallocate(G,Int0100,Int0101,Int0200,Int0201,Int1201)
      return
      end if
!
!------------------------------------------------------------------
! [dppp]
!------------------------------------------------------------------
      if(Lcode.eq.2111)then
!
! Requires 9 steps
!
      mv = 0
      nv = 0
      pv = 0
!
! Step 1 :
! [1000]^(4) from [0000]^(5)
! a+1i
!
      allocate(Int1000(3,513))
      Int1000 = 0.0D0
!
      call calcRAM1000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1000(mv+imat(1:3,i),nv,pv)
        call calcRAM1000(mv,nv+imat(1:3,i),pv)
        call calcRAM1000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 2 :
! [0100]^(2) from [0000]^(3)
! b+1i
!
      allocate(Int0100(3,513))
      Int0100 = 0.0D0
!
      call calcRAM0100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0100(mv+imat(1:3,i),nv,pv)
        call calcRAM0100(mv,nv+imat(1:3,i),pv)
        call calcRAM0100(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0100(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0100(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0100(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 3:
! [1100]^(2) from [1000]^(3) and [0000]^(3)
! b+1i
!
      allocate(Int1100(9,513))
      Int1100 = 0.0D0
!
      call calcRAM1100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1100(mv+imat(1:3,i),nv,pv)
        call calcRAM1100(mv,nv+imat(1:3,i),pv)
        call calcRAM1100(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1100(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1100(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1100(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 4 :
! [2000]^(3) from [1000]^(4) and [0000]^(4)
! a+1i
!
      allocate(Int2000(6,513))
      Int2000 = 0.0D0
!
      call calcRAM2000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2000(mv+imat(1:3,i),nv,pv)
        call calcRAM2000(mv,nv+imat(1:3,i),pv)
        call calcRAM2000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 5 :
! [1110]^(1) from [1100]^(2), [1000]^(2) and [0100]^(2)
! c+1i
!
      allocate(Int1110(27,513))
      Int1110 = 0.0D0
!
      call calcRAM1110(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1110(mv+imat(1:3,i),nv,pv)
        call calcRAM1110(mv,nv+imat(1:3,i),pv)
        call calcRAM1110(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1110(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1110(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1110(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 6:
! [2010]^(1) from [2000]^(2) and [1000]^(2)
! c+1i
!
      allocate(Int2010(18,513))
      Int2010 = 0.0D0
!
      call calcRAM2010(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2010(mv+imat(1:3,i),nv,pv)
        call calcRAM2010(mv,nv+imat(1:3,i),pv)
        call calcRAM2010(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 7:
! [2100]^(2) from [2000]^(3) and [1000]^(3)
! b+1i
!
      allocate(Int2100(18,513))
      Int2100 = 0.0D0
!
      call calcRAM2100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2100(mv+imat(1:3,i),nv,pv)
        call calcRAM2100(mv,nv+imat(1:3,i),pv)
        call calcRAM2100(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2100(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2100(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2100(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 8:
! [2110]^(1) from [2100]^(2), [2000]^(2) and [1100]^(2)
! c+1i
!
      allocate(Int2110(54,513))
      Int2110 = 0.0D0
!
      call calcRAM2110c(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2110c(mv+imat(1:3,i),nv,pv)
        call calcRAM2110c(mv,nv+imat(1:3,i),pv)
        call calcRAM2110c(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 9:
! [2111]^(0) from [2110]^(1), [2100]^(1), [2010]^(1) and [1110]^(1)
! d+1i
!
      allocate(Int2111(162,1))
      Int2111 = 0.0D0
!
      call calcRAM2111(mv,nv,pv)
!
      ram(1:162) = Int2111(1:162,1)
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        do d1=4,6
          do p2=1,3
            do p3=1,3
              do p4=1,3
                dppp_index=27*(d1-1)+9*(p2-1)+3*(p3-1)+p4
                ram(dppp_index) = sq3*ram(dppp_index)
              end do
            end do
          end do
        end do
!
      deallocate(G,Int1000,Int0100,Int2000,Int1100,Int2100,Int2010)
      deallocate(Int1110,Int2110,Int2111)
      return
      end if
!
!------------------------------------------------------------------
! [pdpp]
!------------------------------------------------------------------
      if(Lcode.eq.1211)then
!
! Requires 9 steps
!
      mv = 0
      nv = 0
      pv = 0
!
! Step 1 :
! [1000]^(4) from [0000]^(5)
! a+1i
!
      allocate(Int1000(3,513))
      Int1000 = 0.0D0
!
      call calcRAM1000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1000(mv+imat(1:3,i),nv,pv)
        call calcRAM1000(mv,nv+imat(1:3,i),pv)
        call calcRAM1000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 2 :
! [0100]^(2) from [0000]^(3)
! b+1i
!
      allocate(Int0100(3,513))
      Int0100 = 0.0D0
!
      call calcRAM0100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0100(mv+imat(1:3,i),nv,pv)
        call calcRAM0100(mv,nv+imat(1:3,i),pv)
        call calcRAM0100(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0100(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0100(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0100(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 3:
! [1100]^(2) from [1000]^(3) and [0000]^(3)
! b+1i
!
      allocate(Int1100(9,513))
      Int1100 = 0.0D0
!
      call calcRAM1100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1100(mv+imat(1:3,i),nv,pv)
        call calcRAM1100(mv,nv+imat(1:3,i),pv)
        call calcRAM1100(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1100(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1100(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1100(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 4 :
! [0200]^(3) from [0100]^(4) and [0000]^(4)
! b+1i
!
      allocate(Int0200(6,513))
      Int0200 = 0.0D0
!
      call calcRAM0200(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0200(mv+imat(1:3,i),nv,pv)
        call calcRAM0200(mv,nv+imat(1:3,i),pv)
        call calcRAM0200(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0200(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0200(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0200(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 5 :
! [1110]^(1) from [1100]^(2), [1000]^(2) and [0100]^(2)
! c+1i
!
      allocate(Int1110(27,513))
      Int1110 = 0.0D0
!
      call calcRAM1110(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1110(mv+imat(1:3,i),nv,pv)
        call calcRAM1110(mv,nv+imat(1:3,i),pv)
        call calcRAM1110(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1110(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1110(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1110(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 6:
! [0210]^(1) from [0200]^(2) and [0100]^(2)
! c+1i
!
      allocate(Int0210(18,513))
      Int0210 = 0.0D0
!
      call calcRAM0210(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0210(mv+imat(1:3,i),nv,pv)
        call calcRAM0210(mv,nv+imat(1:3,i),pv)
        call calcRAM0210(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 7:
! [1200]^(2) from [0200]^(3) and [0100]^(3)
! a+1i
!
      allocate(Int1200(18,513))
      Int1200 = 0.0D0
!
      call calcRAM1200(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1200(mv+imat(1:3,i),nv,pv)
        call calcRAM1200(mv,nv+imat(1:3,i),pv)
        call calcRAM1200(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1200(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1200(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1200(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 8:
! [1210]^(1) from [1200]^(2), [0200]^(2) and [1100]^(2)
! c+1i
!
      allocate(Int1210(54,513))
      Int1210 = 0.0D0
!
      call calcRAM1210(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1210(mv+imat(1:3,i),nv,pv)
        call calcRAM1210(mv,nv+imat(1:3,i),pv)
        call calcRAM1210(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 9:
! [1211]^(0) from [1210]^(1), [1200]^(1), [0210]^(1) and [1110]^(1)
! d+1i
!
      allocate(Int1211(162,1))
      Int1211 = 0.0D0
!
      call calcRAM1211(mv,nv,pv)
!
      ram(1:162) = Int1211(1:162,1)
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        do p1=1,3
          do d2=4,6
            do p3=1,3
              do p4=1,3
                pdpp_index=54*(p1-1)+9*(d2-1)+3*(p3-1)+p4
                ram(pdpp_index) = sq3*ram(pdpp_index)
              end do
            end do
          end do
        end do
!
      deallocate(G,Int0100,Int1000,Int1100,Int0200,Int1200,Int1110,Int0210,Int1210)
      deallocate(Int1211)
      return
      end if
!
!------------------------------------------------------------------
! [ddss]
!------------------------------------------------------------------
      if(Lcode.eq.2200)then
!
! Requires 5 steps
!
      mv = 0
      nv = 0
      pv = 0
!
! Step 1 :
! [1000]^(3) from [0000]^(4)
! a+1i
!
      allocate(Int1000(3,513))
      Int1000 = 0.0D0
!
      call calcRAM1000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1000(mv+imat(1:3,i),nv,pv)
        call calcRAM1000(mv,nv+imat(1:3,i),pv)
        call calcRAM1000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 2:
! [1100]^(1) from [1000]^(2) and [0000]^(2)
! b+1i
!
      allocate(Int1100(9,513))
      Int1100 = 0.0D0
!
      call calcRAM1100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1100(mv+imat(1:3,i),nv,pv)
        call calcRAM1100(mv,nv+imat(1:3,i),pv)
        call calcRAM1100(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 3 :
! [2000]^(2) from [1000]^(3) and [0000]^(3)
! a+1i
!
      allocate(Int2000(6,513))
      Int2000 = 0.0D0
!
      call calcRAM2000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2000(mv+imat(1:3,i),nv,pv)
        call calcRAM2000(mv,nv+imat(1:3,i),pv)
        call calcRAM2000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 4:
! [2100]^(1) from [2000]^(2) and [1000]^(2)
! b+1i
!
      allocate(Int2100(18,513))
      Int2100 = 0.0D0
!
      call calcRAM2100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2100(mv+imat(1:3,i),nv,pv)
        call calcRAM2100(mv,nv+imat(1:3,i),pv)
        call calcRAM2100(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 5:
! [2200]^(0) from [2100]^(1), [2000]^(1) and [1100]^(1)
! b+1i
!
      allocate(Int2200(36,1))
      Int2200 = 0.0D0
!
      call calcRAM2200(mv,nv,pv)
!
      ram(1:36) = Int2200(1:36,1)
!
! multiply by sqrt(3) for each d-function xy, xz, yz
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            if(d1.gt.3) ram(dd_index) = sq3*ram(dd_index)
            if(d2.gt.3) ram(dd_index) = sq3*ram(dd_index)
          end do
        end do
!
      deallocate(G,Int1000,Int1100,Int2000,Int2100,Int2200)
      return
      end if
!
!------------------------------------------------------------------
! [dsds]
!------------------------------------------------------------------
      if(Lcode.eq.2020)then
!
! Requires 5 steps
!
      mv = 0
      nv = 0
      pv = 0
!
! Step 1 :
! [1000]^(3) from [0000]^(4)
! a+1i
!
      allocate(Int1000(3,513))       
      Int1000 = 0.0D0
!
      call calcRAM1000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1000(mv+imat(1:3,i),nv,pv)
        call calcRAM1000(mv,nv+imat(1:3,i),pv)
        call calcRAM1000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 2:
! [1010]^(1) from [1000]^(2) and [0000]^(2)
! c+1i
!
      allocate(Int1010(9,513))       
      Int1010 = 0.0D0
!
      call calcRAM1010(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1010(mv+imat(1:3,i),nv,pv)
        call calcRAM1010(mv,nv+imat(1:3,i),pv)
        call calcRAM1010(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 3 :
! [2000]^(2) from [1000]^(3) and [0000]^(3)
! a+1i
!
      allocate(Int2000(6,513))       
      Int2000 = 0.0D0
!
      call calcRAM2000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2000(mv+imat(1:3,i),nv,pv)
        call calcRAM2000(mv,nv+imat(1:3,i),pv)
        call calcRAM2000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 4:
! [2010]^(1) from [2000]^(2) and [1000]^(2)
! c+1i
!
      allocate(Int2010(18,513))       
      Int2010 = 0.0D0
!
      call calcRAM2010(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2010(mv+imat(1:3,i),nv,pv)
        call calcRAM2010(mv,nv+imat(1:3,i),pv)
        call calcRAM2010(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 5:
! [2020]^(0) from [2010]^(1), [2000]^(1) and [1100]^(1)
! c+1i
!
      allocate(Int2020(36,1))       
      Int2020 = 0.0D0
!
      call calcRAM2020(mv,nv,pv)
!
      ram(1:36) = Int2020(1:36,1)
!
! multiply by sqrt(3) for each d-function xy, xz, yz
      dd_index=0
      do d1=1,6
        do d2=1,6
          dd_index=dd_index+1
          if(d1.gt.3) ram(dd_index) = sq3*ram(dd_index)
          if(d2.gt.3) ram(dd_index) = sq3*ram(dd_index)
        end do
      end do
!
      deallocate(G,Int1000,Int1010,Int2000,Int2010,Int2020)
      return
      end if
!
!------------------------------------------------------------------
! [dssd]
!------------------------------------------------------------------
      if(Lcode.eq.2002)then
!
! Requires 5 steps
!
      mv = 0
      nv = 0
      pv = 0
!
! Step 1 :
! [1000]^(3) from [0000]^(4)
! a+1i
!
      allocate(Int1000(3,513))
      Int1000 = 0.0D0
!
      call calcRAM1000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1000(mv+imat(1:3,i),nv,pv)
        call calcRAM1000(mv,nv+imat(1:3,i),pv)
        call calcRAM1000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 2:
! [1001]^(1) from [1000]^(2) and [0000]^(2)
! d+1i
!
      allocate(Int1001(9,513))
      Int1001 = 0.0D0
!
      call calcRAM1001(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1001(mv+imat(1:3,i),nv,pv)
        call calcRAM1001(mv,nv+imat(1:3,i),pv)
        call calcRAM1001(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 3 :
! [2000]^(2) from [1000]^(3) and [0000]^(3)
! a+1i
!
      allocate(Int2000(6,513))
      Int2000 = 0.0D0
!
      call calcRAM2000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2000(mv+imat(1:3,i),nv,pv)
        call calcRAM2000(mv,nv+imat(1:3,i),pv)
        call calcRAM2000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 4:
! [2001]^(1) from [2000]^(2) and [1000]^(2)
! d+1i
!
      allocate(Int2001(18,513))
      Int2001 = 0.0D0
!
      call calcRAM2001(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2001(mv+imat(1:3,i),nv,pv)
        call calcRAM2001(mv,nv+imat(1:3,i),pv)
        call calcRAM2001(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 5:
! [2002]^(0) from [2001]^(1), [2000]^(1) and [1001]^(1)
! c+1i
!
      allocate(Int2002(36,1))
      Int2002 = 0.0D0
!
      call calcRAM2002(mv,nv,pv)
!
      ram(1:36) = Int2002(1:36,1)
!
! multiply by sqrt(3) for each d-function xy, xz, yz
      dd_index=0
      do d1=1,6
        do d2=1,6
          dd_index=dd_index+1
          if(d1.gt.3) ram(dd_index) = sq3*ram(dd_index)
          if(d2.gt.3) ram(dd_index) = sq3*ram(dd_index)
        end do
      end do
!
      deallocate(G,Int1000,Int1001,Int2000,Int2001,Int2002)
      return
      end if
!
!------------------------------------------------------------------
! [sdsd]
!------------------------------------------------------------------
      if(Lcode.eq.202)then
!
! Requires 5 steps
!
      mv = 0
      nv = 0
      pv = 0
!
! Step 1 :
! [0100]^(3) from [0000]^(4)
! b+1i
!
      allocate(Int0100(3,513))
      Int0100 = 0.0D0
!
      call calcRAM0100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0100(mv+imat(1:3,i),nv,pv)
        call calcRAM0100(mv,nv+imat(1:3,i),pv)
        call calcRAM0100(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0100(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0100(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0100(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 2:
! [0101]^(1) from [0100]^(2) and [0000]^(2)
! d+1i
!
      allocate(Int0101(9,513))
      Int0101 = 0.0D0
!
      call calcRAM0101(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0101(mv+imat(1:3,i),nv,pv)
        call calcRAM0101(mv,nv+imat(1:3,i),pv)
        call calcRAM0101(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 3 :
! [0200]^(2) from [0100]^(3) and [0000]^(3)
! b+1i
!
      allocate(Int0200(6,513))
      Int0200 = 0.0D0
!
      call calcRAM0200(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0200(mv+imat(1:3,i),nv,pv)
        call calcRAM0200(mv,nv+imat(1:3,i),pv)
        call calcRAM0200(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0200(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0200(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0200(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 4:
! [0201]^(1) from [0200]^(2) and [0100]^(2)
! d+1i
!
      allocate(Int0201(18,513))
      Int0201 = 0.0D0
!
      call calcRAM0201(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0201(mv+imat(1:3,i),nv,pv)
        call calcRAM0201(mv,nv+imat(1:3,i),pv)
        call calcRAM0201(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 5:
! [0202]^(0) from [0201]^(1), [0200]^(1) and [0101]^(1)
! d+1i
!
      allocate(Int0202(36,1))
      Int0202 = 0.0D0
!
      call calcRAM0202(mv,nv,pv)
!
      ram(1:36) = Int0202(1:36,1)
!
! multiply by sqrt(3) for each d-function xy, xz, yz
      dd_index=0
      do d1=1,6
        do d2=1,6
          dd_index=dd_index+1
          if(d1.gt.3) ram(dd_index) = sq3*ram(dd_index)
          if(d2.gt.3) ram(dd_index) = sq3*ram(dd_index)
        end do
      end do
!
      deallocate(G,Int0100,Int0101,Int0200,Int0201,Int0202)
      return
      end if
!
!------------------------------------------------------------------
! [ddps]
!------------------------------------------------------------------
      if(Lcode.eq.2210)then
!
! Requires 9 steps
!
      mv = 0
      nv = 0
      pv = 0
!
! Step 1:
! [1000]^(4) from [0000]^(5)
! a+1i
!
      allocate(Int1000(3,513))
      Int1000 = 0.0D0
!
      call calcRAM1000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1000(mv+imat(1:3,i),nv,pv)
        call calcRAM1000(mv,nv+imat(1:3,i),pv)
        call calcRAM1000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 2:
! [0100]^(2) from [0000]^(3)
! b+1i
!
      allocate(Int0100(3,513))
      Int0100 = 0.0D0
!
      call calcRAM0100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0100(mv+imat(1:3,i),nv,pv)
        call calcRAM0100(mv,nv+imat(1:3,i),pv)
        call calcRAM0100(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0100(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0100(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0100(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 3:
! [1100]^(2) from [1000]^(3) and [0000]^(3)
! b+1i
!
      allocate(Int1100(9,513))
      Int1100 = 0.0D0
!
      call calcRAM1100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1100(mv+imat(1:3,i),nv,pv)
        call calcRAM1100(mv,nv+imat(1:3,i),pv)
        call calcRAM1100(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1100(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1100(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1100(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 4:
! [2000]^(3) from [1000]^(4) and [0000]^(4)
! a+1i
!
      allocate(Int2000(6,513))
      Int2000 = 0.0D0
!
      call calcRAM2000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2000(mv+imat(1:3,i),nv,pv)
        call calcRAM2000(mv,nv+imat(1:3,i),pv)
        call calcRAM2000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 5:
! [0200]^(3) from [0100]^(4) and [0000]^(4)
! b+1i
!
      allocate(Int0200(6,513))
      Int0200 = 0.0D0
!
      call calcRAM0200(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0200(mv+imat(1:3,i),nv,pv)
        call calcRAM0200(mv,nv+imat(1:3,i),pv)
        call calcRAM0200(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0200(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0200(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0200(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 6:
! [1200]^(1) from [0200]^(2) and [0100]^(2)
! a+1i
!
      allocate(Int1200(18,513))
      Int1200 = 0.0D0
!
      call calcRAM1200(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1200(mv+imat(1:3,i),nv,pv)
        call calcRAM1200(mv,nv+imat(1:3,i),pv)
        call calcRAM1200(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 7:
! [2100]^(2) from [2000]^(3) and [1000]^(3)
! b+1i
!
      allocate(Int2100(18,513))
      Int2100 = 0.0D0
!
      call calcRAM2100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2100(mv+imat(1:3,i),nv,pv)
        call calcRAM2100(mv,nv+imat(1:3,i),pv)
        call calcRAM2100(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2100(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2100(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2100(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 8:
! [2200]^(1) from [2100]^(2), [1100]^(2), and [2000]^(2)
! b+1i
!
      allocate(Int2200(36,513))
      Int2200 = 0.0D0
!
      call calcRAM2200(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2200(mv+imat(1:3,i),nv,pv)
        call calcRAM2200(mv,nv+imat(1:3,i),pv)
        call calcRAM2200(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 9:
! [2210]^(0) from [2200]^(1), [2100]^(1), [1200]^(1)
! c+1i
!
      allocate(Int2210(108,1))
      Int2210 = 0.0D0
!
      call calcRAM2210(mv,nv,pv)
!
      ram(1:108) = Int2210(1:108,1)
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
      ddp_index=0
      do d1=1,6
        do d2=1,6
          do p3=1,3
            ddp_index=ddp_index+1
            if(d1.gt.3) ram(ddp_index) = sq3*ram(ddp_index)
            if(d2.gt.3) ram(ddp_index) = sq3*ram(ddp_index)
          end do
        end do
      end do
!
      deallocate(G,Int1000,Int0100,Int0200,Int2000,Int1100,Int2100)
      deallocate(Int1200,Int2200,Int2210)
      return
      end if
!
!------------------------------------------------------------------
! [dpds]
!------------------------------------------------------------------
      if(Lcode.eq.2120)then
!
! Requires 9 steps
!
      mv = 0
      nv = 0
      pv = 0
!
! Step 1:
! [1000]^(4) from [0000]^(5)
! a+1i
!
      allocate(Int1000(3,513))
      Int1000 = 0.0D0
!
      call calcRAM1000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1000(mv+imat(1:3,i),nv,pv)
        call calcRAM1000(mv,nv+imat(1:3,i),pv)
        call calcRAM1000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 2:
! [0010]^(2) from [0000]^(3)
! c+1i
!
      allocate(Int0010(3,513))
      Int0010 = 0.0D0
!
      call calcRAM0010(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0010(mv+imat(1:3,i),nv,pv)
        call calcRAM0010(mv,nv+imat(1:3,i),pv)
        call calcRAM0010(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0010(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0010(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0010(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 3:
! [1010]^(2) from [1000]^(3) and [0000]^(3)
! c+1i
!
      allocate(Int1010(9,513))
      Int1010 = 0.0D0
!
      call calcRAM1010(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1010(mv+imat(1:3,i),nv,pv)
        call calcRAM1010(mv,nv+imat(1:3,i),pv)
        call calcRAM1010(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1010(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1010(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1010(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 4:
! [2000]^(3) from [1000]^(4) and [0000]^(4)
! a+1i
!
      allocate(Int2000(6,513))
      Int2000 = 0.0D0
!
      call calcRAM2000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2000(mv+imat(1:3,i),nv,pv)
        call calcRAM2000(mv,nv+imat(1:3,i),pv)
        call calcRAM2000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 5:
! [0020]^(3) from [0010]^(4) and [0000]^(4)
! c+1i
!
      allocate(Int0020(6,513))
      Int0020 = 0.0D0
!
      call calcRAM0020(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0020(mv+imat(1:3,i),nv,pv)
        call calcRAM0020(mv,nv+imat(1:3,i),pv)
        call calcRAM0020(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0020(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0020(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0020(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 6:
! [1020]^(1) from [0020]^(2) and [0010]^(2)
! a+1i
!
      allocate(Int1020(18,513))
      Int1020 = 0.0D0
!
      call calcRAM1020(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1020(mv+imat(1:3,i),nv,pv)
        call calcRAM1020(mv,nv+imat(1:3,i),pv)
        call calcRAM1020(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1020(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1020(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1020(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 6:
! [2010]^(2) from [2000]^(3) and [1000]^(3)
! c+1i
!
      allocate(Int2010(18,513))
      Int2010 = 0.0D0
!
      call calcRAM2010(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2010(mv+imat(1:3,i),nv,pv)
        call calcRAM2010(mv,nv+imat(1:3,i),pv)
        call calcRAM2010(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2010(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2010(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2010(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 7:
! [2020]^(1) from [2010]^(2), [1010]^(2), and [2000]^(2)
! c+1i
!
      allocate(Int2020(36,513))
      Int2020 = 0.0D0
!
      call calcRAM2020(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2020(mv+imat(1:3,i),nv,pv)
        call calcRAM2020(mv,nv+imat(1:3,i),pv)
        call calcRAM2020(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 8:
! [2120]^(0) from [2020]^(1), [2010]^(1), [1020]^(1)
! b+1i
!
      allocate(Int2120(108,1))
      Int2120 = 0.0D0
!
      call calcRAM2120(mv,nv,pv)
!
      ram(1:108) = Int2120(1:108,1)
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
      dpd_index=0
      do d1=1,6
        do p2=1,3
          do d3=1,6
            dpd_index=dpd_index+1
            if(d1.gt.3) ram(dpd_index) = sq3*ram(dpd_index)
            if(d3.gt.3) ram(dpd_index) = sq3*ram(dpd_index)
          end do
        end do
      end do
!
      deallocate(G,Int1000,Int0010,Int2000,Int0020,Int1010)
      deallocate(Int1020,Int2010,Int2020,Int2120)
      return
      end if
!
!------------------------------------------------------------------
! [ddsp]
!------------------------------------------------------------------
      if(Lcode.eq.2201)then
!
! Requires 9 steps
!
      mv = 0
      nv = 0
      pv = 0
!
! Step 1:
! [1000]^(4) from [0000]^(5)
! a+1i
!
      allocate(Int1000(3,513))
      Int1000 = 0.0D0
!
      call calcRAM1000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1000(mv+imat(1:3,i),nv,pv)
        call calcRAM1000(mv,nv+imat(1:3,i),pv)
        call calcRAM1000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 2:
! [0100]^(2) from [0000]^(3)
! b+1i
!
      allocate(Int0100(3,513))
      Int0100 = 0.0D0
!
      call calcRAM0100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0100(mv+imat(1:3,i),nv,pv)
        call calcRAM0100(mv,nv+imat(1:3,i),pv)
        call calcRAM0100(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0100(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0100(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0100(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 3:
! [1100]^(2) from [1000]^(3) and [0000]^(3)
! b+1i
!
      allocate(Int1100(9,513))
      Int1100 = 0.0D0
!
      call calcRAM1100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1100(mv+imat(1:3,i),nv,pv)
        call calcRAM1100(mv,nv+imat(1:3,i),pv)
        call calcRAM1100(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1100(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1100(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1100(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 4:
! [2000]^(3) from [1000]^(4) and [0000]^(4)
! a+1i
!
      allocate(Int2000(6,513))
      Int2000 = 0.0D0
!
      call calcRAM2000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2000(mv+imat(1:3,i),nv,pv)
        call calcRAM2000(mv,nv+imat(1:3,i),pv)
        call calcRAM2000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 5:
! [0200]^(3) from [0100]^(4) and [0000]^(4)
! b+1i
!
      allocate(Int0200(6,513))
      Int0200 = 0.0D0
!
      call calcRAM0200(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0200(mv+imat(1:3,i),nv,pv)
        call calcRAM0200(mv,nv+imat(1:3,i),pv)
        call calcRAM0200(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0200(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0200(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0200(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 6:
! [1200]^(1) from [0200]^(2) and [0100]^(2)
! a+1i
!
      allocate(Int1200(18,513))
      Int1200 = 0.0D0
!
      call calcRAM1200(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1200(mv+imat(1:3,i),nv,pv)
        call calcRAM1200(mv,nv+imat(1:3,i),pv)
        call calcRAM1200(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 7:
! [2100]^(2) from [2000]^(3) and [1000]^(3)
! b+1i
!
      allocate(Int2100(18,513))
      Int2100 = 0.0D0
!
      call calcRAM2100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2100(mv+imat(1:3,i),nv,pv)
        call calcRAM2100(mv,nv+imat(1:3,i),pv)
        call calcRAM2100(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2100(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2100(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2100(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 8:
! [2200]^(1) from [2100]^(2), [1100]^(2), and [2000]^(2)
! b+1i
!
      allocate(Int2200(36,513))
      Int2200 = 0.0D0
!
      call calcRAM2200(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2200(mv+imat(1:3,i),nv,pv)
        call calcRAM2200(mv,nv+imat(1:3,i),pv)
        call calcRAM2200(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 8:
! [2201]^(0) from [2200]^(1), [2100]^(1), [1200]^(1)
! d+1i
!
      allocate(Int2201(108,1))
      Int2201 = 0.0D0
!
      call calcRAM2201(mv,nv,pv)
!
      ram(1:108) = Int2201(1:108,1)
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
      ddp_index=0
      do d1=1,6
        do d2=1,6
          do p3=1,3
            ddp_index=ddp_index+1
            if(d1.gt.3) ram(ddp_index) = sq3*ram(ddp_index)
            if(d2.gt.3) ram(ddp_index) = sq3*ram(ddp_index)
          end do
        end do
      end do
!
      deallocate(G,Int1000,Int0100,Int1100,Int2000,Int0200)
      deallocate(Int2100,Int1200,Int2200,Int2201)
      return
      end if
!
!------------------------------------------------------------------
! [dpsd]
!------------------------------------------------------------------
      if(Lcode.eq.2102)then
!
! Requires 9 steps
!
      mv = 0
      nv = 0
      pv = 0
!
! Step 1:
! [1000]^(4) from [0000]^(5)
! a+1i
!
      allocate(Int1000(3,513))
      Int1000 = 0.0D0
!
      call calcRAM1000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1000(mv+imat(1:3,i),nv,pv)
        call calcRAM1000(mv,nv+imat(1:3,i),pv)
        call calcRAM1000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 2:
! [0001]^(2) from [0000]^(3)
! d+1i
!
      allocate(Int0001(3,513))
      Int0001 = 0.0D0
!
      call calcRAM0001(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0001(mv+imat(1:3,i),nv,pv)
        call calcRAM0001(mv,nv+imat(1:3,i),pv)
        call calcRAM0001(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0001(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0001(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0001(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 3:
! [1001]^(2) from [1000]^(3) and [0000]^(3)
! d+1i
!
      allocate(Int1001(9,513))
      Int1001 = 0.0D0
!
      call calcRAM1001(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1001(mv+imat(1:3,i),nv,pv)
        call calcRAM1001(mv,nv+imat(1:3,i),pv)
        call calcRAM1001(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1001(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1001(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1001(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 4:
! [2000]^(3) from [1000]^(4) and [0000]^(4)
! a+1i
!
      allocate(Int2000(6,513))
      Int2000 = 0.0D0
!
      call calcRAM2000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2000(mv+imat(1:3,i),nv,pv)
        call calcRAM2000(mv,nv+imat(1:3,i),pv)
        call calcRAM2000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 5:
! [0002]^(3) from [0001]^(4) and [0000]^(4)
! d+1i
!
      allocate(Int0002(6,513))
      Int0002 = 0.0D0
!
      call calcRAM0002(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0002(mv+imat(1:3,i),nv,pv)
        call calcRAM0002(mv,nv+imat(1:3,i),pv)
        call calcRAM0002(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0002(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0002(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0002(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 6:
! [2001]^(2) from [2000]^(3) and [1000]^(3)
! d+1i
!
      allocate(Int2001(18,513))
      Int2001 = 0.0D0
!
      call calcRAM2001(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2001(mv+imat(1:3,i),nv,pv)
        call calcRAM2001(mv,nv+imat(1:3,i),pv)
        call calcRAM2001(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2001(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2001(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2001(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 7:
! [1002]^(2) from [0002]^(3) and [0001]^(3)
! a+1i
!
      allocate(Int1002(18,513))
      Int1002 = 0.0D0
!
      call calcRAM1002(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1002(mv+imat(1:3,i),nv,pv)
        call calcRAM1002(mv,nv+imat(1:3,i),pv)
        call calcRAM1002(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1002(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1002(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1002(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 8:
! [2002]^(1) from [2001]^(2), [1001]^(2), and [2000]^(2)
! d+1i
!
      allocate(Int2002(36,513))
      Int2002 = 0.0D0
!
      call calcRAM2002(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2002(mv+imat(1:3,i),nv,pv)
        call calcRAM2002(mv,nv+imat(1:3,i),pv)
        call calcRAM2002(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 9:
! [2102]^(0) from [2002]^(1), [2001]^(1), [1002]^(1)
! b+1i
!
      allocate(Int2102(108,1))
      Int2102 = 0.0D0
!
      call calcRAM2102(mv,nv,pv)
!
      ram(1:108) = Int2102(1:108,1)
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
      dpd_index=0
      do d1=1,6
        do p2=1,3
          do d3=1,6
            dpd_index=dpd_index+1
            if(d1.gt.3) ram(dpd_index) = sq3*ram(dpd_index)
            if(d3.gt.3) ram(dpd_index) = sq3*ram(dpd_index)
          end do
        end do
      end do
!
      deallocate(G,Int1000,Int0001,Int1001,Int2000,Int0002)
      deallocate(Int2001,Int1002,Int2002,Int2102)
      return
      end if
!
!------------------------------------------------------------------
! [dspd]
!------------------------------------------------------------------
      if(Lcode.eq.2012)then
!
! Requires 9 steps
!
      mv = 0
      nv = 0
      pv = 0
!
! Step 1:
! [1000]^(4) from [0000]^(5)
! a+1i
!
      allocate(Int1000(3,513))
      Int1000 = 0.0D0
!
      call calcRAM1000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1000(mv+imat(1:3,i),nv,pv)
        call calcRAM1000(mv,nv+imat(1:3,i),pv)
        call calcRAM1000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 2:
! [0001]^(2) from [0000]^(3)
! d+1i
!
      allocate(Int0001(3,513))
      Int0001 = 0.0D0
!
      call calcRAM0001(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0001(mv+imat(1:3,i),nv,pv)
        call calcRAM0001(mv,nv+imat(1:3,i),pv)
        call calcRAM0001(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0001(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0001(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0001(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 3:
! [1001]^(2) from [1000]^(3) and [0000]^(3)
! d+1i
!
      allocate(Int1001(9,513))
      Int1001 = 0.0D0
!
      call calcRAM1001(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1001(mv+imat(1:3,i),nv,pv)
        call calcRAM1001(mv,nv+imat(1:3,i),pv)
        call calcRAM1001(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1001(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1001(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1001(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 4:
! [2000]^(3) from [1000]^(4) and [0000]^(4)
! a+1i
!
      allocate(Int2000(6,513))
      Int2000 = 0.0D0
!
      call calcRAM2000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2000(mv+imat(1:3,i),nv,pv)
        call calcRAM2000(mv,nv+imat(1:3,i),pv)
        call calcRAM2000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 5:
! [0002]^(3) from [0001]^(4) and [0000]^(4)
! d+1i
!
      allocate(Int0002(6,513))
      Int0002 = 0.0D0
!
      call calcRAM0002(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0002(mv+imat(1:3,i),nv,pv)
        call calcRAM0002(mv,nv+imat(1:3,i),pv)
        call calcRAM0002(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0002(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0002(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0002(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 6:
! [2001]^(2) from [2000]^(3) and [1000]^(3)
! d+1i
!
      allocate(Int2001(18,513))
      Int2001 = 0.0D0
!
      call calcRAM2001(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2001(mv+imat(1:3,i),nv,pv)
        call calcRAM2001(mv,nv+imat(1:3,i),pv)
        call calcRAM2001(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2001(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2001(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2001(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 7:
! [1002]^(1) from [0002]^(2) and [0001]^(2)
! a+1i
!
      allocate(Int1002(18,513))
      Int1002 = 0.0D0
!
      call calcRAM1002(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1002(mv+imat(1:3,i),nv,pv)
        call calcRAM1002(mv,nv+imat(1:3,i),pv)
        call calcRAM1002(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1002(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1002(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1002(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 8:
! [2002]^(1) from [2001]^(2), [1001]^(2), and [2000]^(2)
! d+1i
!
      allocate(Int2002(36,513))
      Int2002 = 0.0D0
!
      call calcRAM2002(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2002(mv+imat(1:3,i),nv,pv)
        call calcRAM2002(mv,nv+imat(1:3,i),pv)
        call calcRAM2002(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 9:
! [2012]^(0) from [2002]^(1), [2001]^(1), [1002]^(1)
! c+1i
!
      allocate(Int2012(108,1))
      Int2012 = 0.0D0
!
      call calcRAM2012(mv,nv,pv)
!
      ram(1:108) = Int2012(1:108,1)
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
      dpd_index=0
      do d1=1,6
        do p2=1,3
          do d3=1,6
            dpd_index=dpd_index+1
            if(d1.gt.3) ram(dpd_index) = sq3*ram(dpd_index)
            if(d3.gt.3) ram(dpd_index) = sq3*ram(dpd_index)
          end do
        end do
      end do
!
      deallocate(G,Int1000,Int0001,Int1001,Int2000,Int0002)
      deallocate(Int2001,Int1002,Int2002,Int2012)
!      
      return
      end if
!
!------------------------------------------------------------------
! [pdsd]
!------------------------------------------------------------------
      if(Lcode.eq.1202)then
!
! Requires 9 steps
!
      mv = 0
      nv = 0
      pv = 0
!
! Step 1:
! [0100]^(4) from [0000]^(5)
! b+1i
!
      allocate(Int0100(3,513)) 
      Int0100 = 0.0D0
!
      call calcRAM0100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0100(mv+imat(1:3,i),nv,pv)
        call calcRAM0100(mv,nv+imat(1:3,i),pv)
        call calcRAM0100(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0100(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0100(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0100(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 2:
! [0001]^(2) from [0000]^(3)
! d+1i
!
      allocate(Int0001(3,513)) 
      Int0001 = 0.0D0
!
      call calcRAM0001(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0001(mv+imat(1:3,i),nv,pv)
        call calcRAM0001(mv,nv+imat(1:3,i),pv)
        call calcRAM0001(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0001(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0001(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0001(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 3:
! [0101]^(2) from [0100]^(3) and [0000]^(3)
! d+1i
!
      allocate(Int0101(9,513)) 
      Int0101 = 0.0D0
!
      call calcRAM0101(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0101(mv+imat(1:3,i),nv,pv)
        call calcRAM0101(mv,nv+imat(1:3,i),pv)
        call calcRAM0101(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0101(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0101(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0101(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 4:
! [0200]^(3) from [0100]^(4) and [0000]^(4)
! b+1i
!
      allocate(Int0200(6,513)) 
      Int0200 = 0.0D0
!
      call calcRAM0200(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0200(mv+imat(1:3,i),nv,pv)
        call calcRAM0200(mv,nv+imat(1:3,i),pv)
        call calcRAM0200(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0200(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0200(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0200(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 5:
! [0002]^(3) from [0001]^(4) and [0000]^(4)
! d+1i
!
      allocate(Int0002(6,513)) 
      Int0002 = 0.0D0
!
      call calcRAM0002(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0002(mv+imat(1:3,i),nv,pv)
        call calcRAM0002(mv,nv+imat(1:3,i),pv)
        call calcRAM0002(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0002(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0002(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0002(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 6:
! [0102]^(1) from [0002]^(2) and [0001]^(2)
! b+1i
!
      allocate(Int0102(18,513)) 
      Int0102 = 0.0D0
!
      call calcRAM0102(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0102(mv+imat(1:3,i),nv,pv)
        call calcRAM0102(mv,nv+imat(1:3,i),pv)
        call calcRAM0102(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 7:
! [0201]^(2) from [0200]^(3) and [1000]^(3)
! d+1i
!
      allocate(Int0201(18,513)) 
      Int0201 = 0.0D0
!
      call calcRAM0201(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0201(mv+imat(1:3,i),nv,pv)
        call calcRAM0201(mv,nv+imat(1:3,i),pv)
        call calcRAM0201(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0201(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0201(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0201(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 8:
! [0202]^(1) from [0201]^(2), [0101]^(2), and [0200]^(2)
! d+1i
!
      allocate(Int0202(36,513)) 
      Int0202 = 0.0D0
!
      call calcRAM0202(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0202(mv+imat(1:3,i),nv,pv)
        call calcRAM0202(mv,nv+imat(1:3,i),pv)
        call calcRAM0202(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 9:
! [1202]^(0) from [0202]^(1), [0201]^(1), [0102]^(1)
! a+1i
!
      allocate(Int1202(108,1)) 
      Int1202 = 0.0D0
!
      call calcRAM1202(mv,nv,pv)
!
      ram(1:108) = Int1202(1:108,1)
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
      pdd_index=0
      do p1=1,3
        do d2=1,6
          do d3=1,6
            pdd_index=pdd_index+1
            if(d2.gt.3) ram(pdd_index) = sq3*ram(pdd_index)
            if(d3.gt.3) ram(pdd_index) = sq3*ram(pdd_index)
          end do
        end do
      end do
!
      deallocate(G,Int0100,Int0001,Int0101,Int0200,Int0002)
      deallocate(Int0201,Int0102,Int0202,Int1202)
!
      return
      end if
!
!------------------------------------------------------------------
! [ddpp]
!------------------------------------------------------------------
      if(Lcode.eq.2211)then
!
! Requires 12 steps
!
      mv = 0
      nv = 0
      pv = 0
!
! Step 1 :
! [1000]^(5) from [0000]^(6)
! a+1i
!
      allocate(Int1000(3,513))
      Int1000 = 0.0D0
!
      call calcRAM1000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1000(mv+imat(1:3,i),nv,pv)
        call calcRAM1000(mv,nv+imat(1:3,i),pv)
        call calcRAM1000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 2 :        
! [0100]^(3) from [0000]^(4)
! b+1i            
!
      allocate(Int0100(3,513))
      Int0100 = 0.0D0
!
      call calcRAM0100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0100(mv+imat(1:3,i),nv,pv)
        call calcRAM0100(mv,nv+imat(1:3,i),pv)
        call calcRAM0100(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0100(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0100(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0100(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 3:
! [1100]^(3) from [1000]^(4) and [0000]^(4)
! b+1i
!
      allocate(Int1100(9,513))
      Int1100 = 0.0D0
!
      call calcRAM1100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1100(mv+imat(1:3,i),nv,pv)
        call calcRAM1100(mv,nv+imat(1:3,i),pv)
        call calcRAM1100(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1100(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1100(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1100(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 4:
! [0200]^(2) from [0100]^(3) and [0000]^(3)
! b+1i
!
      allocate(Int0200(6,513))
      Int0200 = 0.0D0
!
      call calcRAM0200(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0200(mv+imat(1:3,i),nv,pv)
        call calcRAM0200(mv,nv+imat(1:3,i),pv)
        call calcRAM0200(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0200(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0200(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0200(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 5:
! [2000]^(4) from [1000]^(5) and [0000]^(5)
! a+1i
!
      allocate(Int2000(6,513))
      Int2000 = 0.0D0
!
      call calcRAM2000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2000(mv+imat(1:3,i),nv,pv)
        call calcRAM2000(mv,nv+imat(1:3,i),pv)
        call calcRAM2000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 6:
! [1200]^(2) from [0200]^(3) and [0100]^(3)
! a+1i
!
      allocate(Int1200(18,513))
      Int1200 = 0.0D0
!
      call calcRAM1200(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1200(mv+imat(1:3,i),nv,pv)
        call calcRAM1200(mv,nv+imat(1:3,i),pv)
        call calcRAM1200(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1200(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1200(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1200(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 7:
! [2100]^(3) from [2000]^(4) and [1000]^(4)
! b+1i
!
      allocate(Int2100(18,513))
      Int2100 = 0.0D0
!
      call calcRAM2100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2100(mv+imat(1:3,i),nv,pv)
        call calcRAM2100(mv,nv+imat(1:3,i),pv)
        call calcRAM2100(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2100(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2100(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2100(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 8:
! [2200]^(3) from [2100]^(4) and [1200]^(4)
! b+1i
!
      allocate(Int2200(36,513))
      Int2200 = 0.0D0
!
      call calcRAM2200(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2200(mv+imat(1:3,i),nv,pv)
        call calcRAM2200(mv,nv+imat(1:3,i),pv)
        call calcRAM2200(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2200(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2200(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2200(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 9:
! [2110]^(3) from [2100]^(4), [1100]^(4), [2000]^(4)
! c+1i
!
      allocate(Int2110(54,513))
      Int2110 = 0.0D0
!
      call calcRAM2110c(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2110c(mv+imat(1:3,i),nv,pv)
        call calcRAM2110c(mv,nv+imat(1:3,i),pv)
        call calcRAM2110c(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 10:
! [1210]^(1) from [1200]^(2), [1100]^(2) and [0200]^(2)
! c+1i
!
      allocate(Int1210(54,513))
      Int1210 = 0.0D0
!
      call calcRAM1210(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1210(mv+imat(1:3,i),nv,pv)
        call calcRAM1210(mv,nv+imat(1:3,i),pv)
        call calcRAM1210(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 11:
! [2210]^(1) from [2200]^(2), [2100]^(2) and [1200]^(2)
! c+1i
!
      allocate(Int2210(108,513))
      Int2210 = 0.0D0
!
      call calcRAM2210(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2210(mv+imat(1:3,i),nv,pv)
        call calcRAM2210(mv,nv+imat(1:3,i),pv)
        call calcRAM2210(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 12:
! [2211]^(1) from [2210]^(1), [1210]^(1), [2110]^(1), [2200]^(1)
! d+1i
!
      allocate(Int2211(324,1))
      Int2211 = 0.0D0
!
      call calcRAM2211(mv,nv,pv)
!
      ram(1:324) = Int2211(1:324,1)
!
      ddpp_index=0
      do d1=1,6
        do d2=1,6
          do p3=1,3
            do p4=1,3
              ddpp_index=ddpp_index+1
              if(d1.gt.3) ram(ddpp_index) = sq3*ram(ddpp_index)
              if(d2.gt.3) ram(ddpp_index) = sq3*ram(ddpp_index)
            end do
          end do
        end do
      end do
!
      deallocate(G,Int1000,Int0100,Int1100,Int0200,Int2000)
      deallocate(Int2100,Int1200,Int2200,Int2110,Int1210)
      deallocate(Int2210,Int2211)
!
      return 
      end if 
!
!--------------------------------------------------------------------
! [dpdp]
!--------------------------------------------------------------------
      if(Lcode.eq.2121)then
!
! Requires 12 steps
!
      mv = 0
      nv = 0
      pv = 0
!
! Step 1:
! [1000]^(5) from [0000]^(6)
! a+1i
!
      allocate(Int1000(3,513))
      Int1000 = 0.0D0
!
      call calcRAM1000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1000(mv+imat(1:3,i),nv,pv)
        call calcRAM1000(mv,nv+imat(1:3,i),pv)
        call calcRAM1000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 2:
! [0010]^(4) from [0000]^(5)
! c+1i
!
      allocate(Int0010(3,513))
      Int0010 = 0.0D0
!
      call calcRAM0010(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0010(mv+imat(1:3,i),nv,pv)
        call calcRAM0010(mv,nv+imat(1:3,i),pv)
        call calcRAM0010(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0010(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0010(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0010(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 3:
! [0020]^(3) from [0010]^(4) and [0000]^(4)
! c+1i
!
      allocate(Int0020(6,513))
      Int0020 = 0.0D0
!
      call calcRAM0020(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0020(mv+imat(1:3,i),nv,pv)
        call calcRAM0020(mv,nv+imat(1:3,i),pv)
        call calcRAM0020(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0020(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0020(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0020(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 4:
! [1010]^(3) from [1000]^(4) and [0000]^(4)
! c+1i
!
      allocate(Int1010(9,513))
      Int1010 = 0.0D0
!
      call calcRAM1010(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1010(mv+imat(1:3,i),nv,pv)
        call calcRAM1010(mv,nv+imat(1:3,i),pv)
        call calcRAM1010(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1010(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1010(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1010(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 5:
! [2000]^(3) from [1000]^(4) and [0000]^(4)
! a+1i
!
      allocate(Int2000(6,513))
      Int2000 = 0.0D0
!
      call calcRAM2000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2000(mv+imat(1:3,i),nv,pv)
        call calcRAM2000(mv,nv+imat(1:3,i),pv)
        call calcRAM2000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 6:
! [1020]^(3) from [0020]^(4) and [0010]^(4)
! a+1i
!
      allocate(Int1020(18,513))
      Int1020 = 0.0D0
!
      call calcRAM1020(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1020(mv+imat(1:3,i),nv,pv)
        call calcRAM1020(mv,nv+imat(1:3,i),pv)
        call calcRAM1020(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1020(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1020(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1020(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 7:
! [2010]^(3) from [2000]^(4) and [1000]^(4)
! c+1i
!
      allocate(Int2010(18,513))
      Int2010 = 0.0D0
!
      call calcRAM2010(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2010(mv+imat(1:3,i),nv,pv)
        call calcRAM2010(mv,nv+imat(1:3,i),pv)
        call calcRAM2010(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2010(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2010(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2010(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 8:
! [1120]^(3) from [1020]^(4), [0020]^(4) and [1010]^(4)
! b+1i
!
      allocate(Int1120(54,513))
      Int1120 = 0.0D0
!
      call calcRAM1120(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1120(mv+imat(1:3,i),nv,pv)
        call calcRAM1120(mv,nv+imat(1:3,i),pv)
        call calcRAM1120(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 9:
! [2020]^(3) from [2010]^(4), [2000]^(4) and [1010]^(4)
! c+1i
!
      allocate(Int2020(36,513))
      Int2020 = 0.0D0
!
      call calcRAM2020(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2020(mv+imat(1:3,i),nv,pv)
        call calcRAM2020(mv,nv+imat(1:3,i),pv)
        call calcRAM2020(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2020(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2020(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2020(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 10:
! [2110]^(3) from [2010]^(4), [2000]^(4) and [1010]^(4)
! b+1i
!
      allocate(Int2110(54,513))
      Int2110 = 0.0D0
!
      call calcRAM2110b(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2110b(mv+imat(1:3,i),nv,pv)
        call calcRAM2110b(mv,nv+imat(1:3,i),pv)
        call calcRAM2110b(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 11:
! [2120]^(3) from [2020]^(4), [1020]^(4) and [2010]^(4)
! b+1i
!
      allocate(Int2120(108,513))
      Int2120 = 0.0D0
!
      call calcRAM2120(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2120(mv+imat(1:3,i),nv,pv)
        call calcRAM2120(mv,nv+imat(1:3,i),pv)
        call calcRAM2120(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 12:
! [2121]^(0) from [2120]^(1), [1120]^(1), [2020]^(1), and [2110]^(1)
! d+1i
!
      allocate(Int2121(324,1))
      Int2121 = 0.0D0
!
      call calcRAM2121(mv,nv,pv)
!
      ram(1:324) = Int2121(1:324,1)
!
        dpdp_index=0
        do d1=1,6
          do p2=1,3
            do d3=1,6
              do p4=1,3
                dpdp_index=dpdp_index+1
                if(d1.gt.3) ram(dpdp_index) = sq3*ram(dpdp_index)
                if(d3.gt.3) ram(dpdp_index) = sq3*ram(dpdp_index)
              end do
            end do
          end do
        end do
!
      deallocate(G,Int1000,Int0010,Int1010,Int0020,Int2000)
      deallocate(Int2010,Int1020,Int2020,Int2110,Int1120)
      deallocate(Int2120,Int2121)
!
      return
      end if
!
!--------------------------------------------------------------------
! [dppd]
!--------------------------------------------------------------------
      if(Lcode.eq.2112)then
!
! Requires 12 steps
!
      mv = 0
      nv = 0
      pv = 0
!
! Step 1:
! [1000]^(5) from [0000]^(6)
! a+1i
!
      allocate(Int1000(3,513))
      Int1000 = 0.0D0
!
      call calcRAM1000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1000(mv+imat(1:3,i),nv,pv)
        call calcRAM1000(mv,nv+imat(1:3,i),pv)
        call calcRAM1000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 2:
! [0001]^(4) from [0000]^(5)
! d+1i
!
      allocate(Int0001(3,513))
      Int0001 = 0.0D0
!
      call calcRAM0001(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0001(mv+imat(1:3,i),nv,pv)
        call calcRAM0001(mv,nv+imat(1:3,i),pv)
        call calcRAM0001(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0001(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0001(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0001(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 3:
! [0002]^(3) from [0001]^(4) and [0000]^(4)
! d+1i
!
      allocate(Int0002(6,513))
      Int0002 = 0.0D0
!
      call calcRAM0002(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0002(mv+imat(1:3,i),nv,pv)
        call calcRAM0002(mv,nv+imat(1:3,i),pv)
        call calcRAM0002(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0002(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0002(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0002(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 4:
! [1001]^(3) from [1000]^(4) and [0000]^(4)
! d+1i
!
      allocate(Int1001(9,513))
      Int1001 = 0.0D0
!
      call calcRAM1001(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1001(mv+imat(1:3,i),nv,pv)
        call calcRAM1001(mv,nv+imat(1:3,i),pv)
        call calcRAM1001(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1001(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1001(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1001(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 5:
! [2000]^(3) from [1000]^(4) and [0000]^(4)
! a+1i
!
      allocate(Int2000(6,513))
      Int2000 = 0.0D0
!
      call calcRAM2000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2000(mv+imat(1:3,i),nv,pv)
        call calcRAM2000(mv,nv+imat(1:3,i),pv)
        call calcRAM2000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 6:
! [1002]^(3) from [0002]^(4) and [0001]^(4)
! a+1i
!
      allocate(Int1002(18,513))
      Int1002 = 0.0D0
!
      call calcRAM1002(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1002(mv+imat(1:3,i),nv,pv)
        call calcRAM1002(mv,nv+imat(1:3,i),pv)
        call calcRAM1002(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1002(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1002(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1002(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 7:
! [2001]^(3) from [2000]^(4) and [1000]^(4)
! d+1i
!
      allocate(Int2001(18,513))
      Int2001 = 0.0D0
!
      call calcRAM2001(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2001(mv+imat(1:3,i),nv,pv)
        call calcRAM2001(mv,nv+imat(1:3,i),pv)
        call calcRAM2001(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2001(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2001(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2001(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 8:
! [1102]^(3) from [1002]^(4), [0002]^(4) and [1001]^(4)
! b+1i
!
      allocate(Int1102(54,513))
      Int1102 = 0.0D0
!
      call calcRAM1102b(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1102b(mv+imat(1:3,i),nv,pv)
        call calcRAM1102b(mv,nv+imat(1:3,i),pv)
        call calcRAM1102b(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 9:
! [2002]^(3) from [2001]^(4), [2000]^(4) and [1001]^(4)
! d+1i
!
      allocate(Int2002(36,513))
      Int2002 = 0.0D0
!
      call calcRAM2002(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2002(mv+imat(1:3,i),nv,pv)
        call calcRAM2002(mv,nv+imat(1:3,i),pv)
        call calcRAM2002(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2002(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2002(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2002(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 10:
! [2101]^(3) from [2001]^(4), [2000]^(4) and [1001]^(4)
! b+1i
!
      allocate(Int2101(54,513))
      Int2101 = 0.0D0
!
      call calcRAM2101b(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2101b(mv+imat(1:3,i),nv,pv)
        call calcRAM2101b(mv,nv+imat(1:3,i),pv)
        call calcRAM2101b(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 11:
! [2102]^(3) from [2002]^(4), [1002]^(4) and [2001]^(4)
! b+1i
!
      allocate(Int2102(108,513))
      Int2102 = 0.0D0
!
      call calcRAM2102(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2102(mv+imat(1:3,i),nv,pv)
        call calcRAM2102(mv,nv+imat(1:3,i),pv)
        call calcRAM2102(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 12:
! [2112]^(0) from [2102]^(1), [1102]^(1), [2002]^(1), and [2101]^(1)
! c+1i
!
      allocate(Int2112(324,1))
      Int2112 = 0.0D0
!
      call calcRAM2112(mv,nv,pv)
!
      ram(1:324) = Int2112(1:324,1)
!
        dppd_index=0
        do d1=1,6
          do p2=1,3
            do p3=1,3
              do d4=1,6
                dppd_index=dppd_index+1
                if(d1.gt.3) ram(dppd_index) = sq3*ram(dppd_index)
                if(d4.gt.3) ram(dppd_index) = sq3*ram(dppd_index)
              end do
            end do
          end do
        end do
!
        deallocate(G,Int1000,Int0001,Int1001,Int2000,Int0002)
        deallocate(Int2001,Int1002,Int2101,Int1102,Int2002)
        deallocate(Int2102,Int2112)
!
        return
        end if
!
!--------------------------------------------------------------------
! [pdpd]
!--------------------------------------------------------------------
      if(Lcode.eq.1212)then
!
! Requires 12 steps
!
      mv = 0
      nv = 0
      pv = 0
!
! Step 1:
! [0001]^(4) from [0000]^(5)
! d+1i
!
      allocate(Int0001(3,513))
      Int0001 = 0.0D0
!
      call calcRAM0001(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0001(mv+imat(1:3,i),nv,pv)
        call calcRAM0001(mv,nv+imat(1:3,i),pv)
        call calcRAM0001(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0001(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0001(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0001(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 2:
! [0100]^(4) from [0000]^(5)
! b+1i
!
      allocate(Int0100(3,513))
      Int0100 = 0.0D0
!
      call calcRAM0100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0100(mv+imat(1:3,i),nv,pv)
        call calcRAM0100(mv,nv+imat(1:3,i),pv)
        call calcRAM0100(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0100(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0100(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0100(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 3:
! [0101]^(4) from [0100]^(5) and [0000]^(5)
! d+1i
!
      allocate(Int0101(9,513))
      Int0101 = 0.0D0
!
      call calcRAM0101(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0101(mv+imat(1:3,i),nv,pv)
        call calcRAM0101(mv,nv+imat(1:3,i),pv)
        call calcRAM0101(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0101(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0101(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0101(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 4:
! [0200]^(4) from [0100]^(5) and [0000]^(5)
! b+1i
!
      allocate(Int0200(6,513))
      Int0200 = 0.0D0
!
      call calcRAM0200(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0200(mv+imat(1:3,i),nv,pv)
        call calcRAM0200(mv,nv+imat(1:3,i),pv)
        call calcRAM0200(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0200(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0200(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0200(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 5:
! [0002]^(4) from [0001]^(5) and [0000]^(5)
! d+1i
!
      allocate(Int0002(6,513))
      Int0002 = 0.0D0
!
      call calcRAM0002(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0002(mv+imat(1:3,i),nv,pv)
        call calcRAM0002(mv,nv+imat(1:3,i),pv)
        call calcRAM0002(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0002(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0002(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0002(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 6:
! [0201]^(4) from [0200]^(5) and [0100]^(5)
! d+1i
!
      allocate(Int0201(18,513))
      Int0201 = 0.0D0
!
      call calcRAM0201(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0201(mv+imat(1:3,i),nv,pv)
        call calcRAM0201(mv,nv+imat(1:3,i),pv)
        call calcRAM0201(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0201(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0201(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0201(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 7:
! [0102]^(4) from [0002]^(5) and [0001]^(5)
! b+1i
!
      allocate(Int0102(18,513))
      Int0102 = 0.0D0
!
      call calcRAM0102(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0102(mv+imat(1:3,i),nv,pv)
        call calcRAM0102(mv,nv+imat(1:3,i),pv)
        call calcRAM0102(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0102(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0102(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0102(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 8:
! [0202]^(4) from [0201]^(5), [0101]^(5) and [0200]^(5)
! d+1i
!
      allocate(Int0202(36,513))
      Int0202 = 0.0D0
!
      call calcRAM0202(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0202(mv+imat(1:3,i),nv,pv)
        call calcRAM0202(mv,nv+imat(1:3,i),pv)
        call calcRAM0202(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0202(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0202(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0202(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 9:
! [1201]^(4) from [0201]^(5), [0101]^(5) and [0200]^(5)
! a+1i
!
      allocate(Int1201(54,513))
      Int1201 = 0.0D0
!
      call calcRAM1201a(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1201a(mv+imat(1:3,i),nv,pv)
        call calcRAM1201a(mv,nv+imat(1:3,i),pv)
        call calcRAM1201a(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 10:
! [1102]^(4) from [0102]^(5), [0002]^(5) and [0101]^(5)
! a+1i
!
      allocate(Int1102(54,513))
      Int1102 = 0.0D0
!
      call calcRAM1102a(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1102a(mv+imat(1:3,i),nv,pv)
        call calcRAM1102a(mv,nv+imat(1:3,i),pv)
        call calcRAM1102a(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 11:
! [1202]^(4) from [0202]^(5), [0102]^(5) and [0201]^(5)
! a+1i
!
      allocate(Int1202(108,513))
      Int1202 = 0.0D0
!
      call calcRAM1202(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1202(mv+imat(1:3,i),nv,pv)
        call calcRAM1202(mv,nv+imat(1:3,i),pv)
        call calcRAM1202(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 12:
! [1212]^(4) from [1202]^(5), [0202]^(5), [1102]^(5) and [1201]^(5)
! c+1i
!
      allocate(Int1212(324,1))
      Int1212 = 0.0D0
!
      call calcRAM1212(mv,nv,pv)
!
      ram(1:324) = Int1212(1:324,1)
!
      pdpd_index = 0
      do p1 = 1,3
        do d2 = 1,6
          do p3 = 1,3
            do d4 = 1,6
              pdpd_index = pdpd_index + 1
              if(d2.gt.3) ram(pdpd_index) = sq3*ram(pdpd_index)
              if(d4.gt.3) ram(pdpd_index) = sq3*ram(pdpd_index)
            end do
          end do
        end do
      end do
!
      deallocate(G,Int0100,Int0001,Int0200,Int0002,Int0101)
      deallocate(Int0201,Int0102,Int0202,Int1201,Int1102)
      deallocate(Int1202,Int1212)
!
      return
      end if
!
!--------------------------------------------------------------------
! [ddds]
!--------------------------------------------------------------------
      if(Lcode.eq.2220)then
!
! Requires 12 steps
!
      mv = 0
      nv = 0
      pv = 0
!
! Step 1:
! [1000]^(4) from [0000]^(5)
! a+1i
!
      allocate(Int1000(3,513))
      Int1000 = 0.0D0
!
      call calcRAM1000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1000(mv+imat(1:3,i),nv,pv)
        call calcRAM1000(mv,nv+imat(1:3,i),pv)
        call calcRAM1000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 2:
! [0100]^(4) from [0000]^(5)
! b+1i
!
      allocate(Int0100(3,513))
      Int0100 = 0.0D0
!
      call calcRAM0100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0100(mv+imat(1:3,i),nv,pv)
        call calcRAM0100(mv,nv+imat(1:3,i),pv)
        call calcRAM0100(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0100(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0100(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0100(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 3:
! [1100]^(4) from [1000]^(5) and [0000]^(5)
! b+1i
!
      allocate(Int1100(9,513))
      Int1100 = 0.0D0
!
      call calcRAM1100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1100(mv+imat(1:3,i),nv,pv)
        call calcRAM1100(mv,nv+imat(1:3,i),pv)
        call calcRAM1100(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1100(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1100(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1100(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 4:
! [2000]^(4) from [1000]^(5) and [0000]^(5)
! a+1i
!
      allocate(Int2000(6,513))
      Int2000 = 0.0D0
!
      call calcRAM2000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2000(mv+imat(1:3,i),nv,pv)
        call calcRAM2000(mv,nv+imat(1:3,i),pv)
        call calcRAM2000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 5:
! [0200]^(4) from [0100]^(5) and [0000]^(5)
! b+1i
!
      allocate(Int0200(6,513))
      Int0200 = 0.0D0
!
      call calcRAM0200(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0200(mv+imat(1:3,i),nv,pv)
        call calcRAM0200(mv,nv+imat(1:3,i),pv)
        call calcRAM0200(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0200(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0200(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0200(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 6:
! [2100]^(4) from [2000]^(5) and [1000]^(5)
! b+1i
!
      allocate(Int2100(18,513))
      Int2100 = 0.0D0
!
      call calcRAM2100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2100(mv+imat(1:3,i),nv,pv)
        call calcRAM2100(mv,nv+imat(1:3,i),pv)
        call calcRAM2100(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2100(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2100(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2100(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 7:
! [1200]^(4) from [0200]^(5) and [0100]^(5)
! a+1i
!
      allocate(Int1200(18,513))
      Int1200 = 0.0D0
!
      call calcRAM1200(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1200(mv+imat(1:3,i),nv,pv)
        call calcRAM1200(mv,nv+imat(1:3,i),pv)
        call calcRAM1200(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1200(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1200(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1200(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 8:
! [2200]^(4) from [2100]^(5), [1100]^(5) and [2000]^(5)
! b+1i
!
      allocate(Int2200(36,513))
      Int2200 = 0.0D0
!
      call calcRAM2200(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2200(mv+imat(1:3,i),nv,pv)
        call calcRAM2200(mv,nv+imat(1:3,i),pv)
        call calcRAM2200(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2200(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2200(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2200(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 9:
! [2110]^(4) from [2100]^(5), [1100]^(5) and [2000]^(5)
! c+1i
!
      allocate(Int2110(54,513))
      Int2110 = 0.0D0
!
      call calcRAM2110c(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2110c(mv+imat(1:3,i),nv,pv)
        call calcRAM2110c(mv,nv+imat(1:3,i),pv)
        call calcRAM2110c(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 10:
! [1210]^(4) from [1200]^(5), [0200]^(5) and [1100]^(5)
! c+1i
!
      allocate(Int1210(54,513))
      Int1210 = 0.0D0
!
      call calcRAM1210(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1210(mv+imat(1:3,i),nv,pv)
        call calcRAM1210(mv,nv+imat(1:3,i),pv)
        call calcRAM1210(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 11:
! [2210]^(4) from [2200]^(5), [1200]^(5) and [2100]^(5)
! c+1i
!
      allocate(Int2210(108,513))
      Int2210 = 0.0D0
!
      call calcRAM2210(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2210(mv+imat(1:3,i),nv,pv)
        call calcRAM2210(mv,nv+imat(1:3,i),pv)
        call calcRAM2210(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 12:
! [2220]^(4) from [2210]^(5), [1210]^(5), [2110]^(5) and [2200]^(5)
! c+1i
!
      allocate(Int2220(216,1))
      Int2220 = 0.0D0
!
      call calcRAM2220(mv,nv,pv)
!
      ram(1:216) = Int2220(1:216,1)
!
      ddd_index = 0
      do d1 = 1,6
        do d2 = 1,6
          do d3 = 1,6
            ddd_index = ddd_index + 1
            if(d1.gt.3) ram(ddd_index) = sq3*ram(ddd_index)
            if(d2.gt.3) ram(ddd_index) = sq3*ram(ddd_index)
            if(d3.gt.3) ram(ddd_index) = sq3*ram(ddd_index)
          end do
        end do
      end do
!
      deallocate(G,Int1000,Int0100,Int1100,Int2000,Int0200)
      deallocate(Int1200,Int2100,Int2110,Int1210,Int2200)
      deallocate(Int2210,Int2220)
!
      return
      end if
!
!--------------------------------------------------------------------
! [ddsd]
!--------------------------------------------------------------------
      if(Lcode.eq.2202)then
!
! Requires 12 steps
!
      mv = 0
      nv = 0
      pv = 0
!
! Step 1:
! [1000]^(4) from [0000]^(5)
! a+1i
!
      allocate(Int1000(3,513))
      Int1000 = 0.0D0
!
      call calcRAM1000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1000(mv+imat(1:3,i),nv,pv)
        call calcRAM1000(mv,nv+imat(1:3,i),pv)
        call calcRAM1000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 2:
! [0100]^(4) from [0000]^(5)
! b+1i
!
      allocate(Int0100(3,513))
      Int0100 = 0.0D0
!
      call calcRAM0100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0100(mv+imat(1:3,i),nv,pv)
        call calcRAM0100(mv,nv+imat(1:3,i),pv)
        call calcRAM0100(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0100(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0100(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0100(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 3:
! [1100]^(4) from [1000]^(5) and [0000]^(5)
! b+1i
!
      allocate(Int1100(9,513))
      Int1100 = 0.0D0
!
      call calcRAM1100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1100(mv+imat(1:3,i),nv,pv)
        call calcRAM1100(mv,nv+imat(1:3,i),pv)
        call calcRAM1100(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1100(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1100(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1100(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 4:
! [2000]^(4) from [1000]^(5) and [0000]^(5)
! a+1i
!
      allocate(Int2000(6,513))
      Int2000 = 0.0D0
!
      call calcRAM2000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2000(mv+imat(1:3,i),nv,pv)
        call calcRAM2000(mv,nv+imat(1:3,i),pv)
        call calcRAM2000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 5:
! [0200]^(4) from [0100]^(5) and [0000]^(5)
! b+1i
!
      allocate(Int0200(6,513))
      Int0200 = 0.0D0
!
      call calcRAM0200(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0200(mv+imat(1:3,i),nv,pv)
        call calcRAM0200(mv,nv+imat(1:3,i),pv)
        call calcRAM0200(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0200(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0200(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0200(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 6:
! [2100]^(4) from [2000]^(5) and [1000]^(5)
! b+1i
!
      allocate(Int2100(18,513))
      Int2100 = 0.0D0
!
      call calcRAM2100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2100(mv+imat(1:3,i),nv,pv)
        call calcRAM2100(mv,nv+imat(1:3,i),pv)
        call calcRAM2100(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2100(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2100(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2100(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 7:
! [1200]^(4) from [0200]^(5) and [0100]^(5)
! a+1i
!
      allocate(Int1200(18,513))
      Int1200 = 0.0D0
!
      call calcRAM1200(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1200(mv+imat(1:3,i),nv,pv)
        call calcRAM1200(mv,nv+imat(1:3,i),pv)
        call calcRAM1200(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1200(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1200(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1200(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 8:
! [2200]^(4) from [2100]^(5), [1100]^(5) and [2000]^(5)
! b+1i
!
      allocate(Int2200(36,513))
      Int2200 = 0.0D0
!
      call calcRAM2200(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2200(mv+imat(1:3,i),nv,pv)
        call calcRAM2200(mv,nv+imat(1:3,i),pv)
        call calcRAM2200(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2200(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2200(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2200(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 9:
! [2101]^(4) from [2100]^(5), [1100]^(5) and [2000]^(5)
! d+1i
!
      allocate(Int2101(54,513))
      Int2101 = 0.0D0
!
      call calcRAM2101d(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2101d(mv+imat(1:3,i),nv,pv)
        call calcRAM2101d(mv,nv+imat(1:3,i),pv)
        call calcRAM2101d(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 10:
! [1201]^(4) from [1200]^(5), [0200]^(5) and [1100]^(5)
! d+1i
!
      allocate(Int1201(54,513))
      Int1201 = 0.0D0
!
      call calcRAM1201d(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1201d(mv+imat(1:3,i),nv,pv)
        call calcRAM1201d(mv,nv+imat(1:3,i),pv)
        call calcRAM1201d(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 11:
! [2201]^(4) from [2200]^(5), [1200]^(5) and [2100]^(5)
! d+1i
!
      allocate(Int2201(108,513))
      Int2201 = 0.0D0
!
      call calcRAM2201(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2201(mv+imat(1:3,i),nv,pv)
        call calcRAM2201(mv,nv+imat(1:3,i),pv)
        call calcRAM2201(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 12:
! [2202]^(4) from [2201]^(5), [1201]^(5), [2101]^(5) and [2200]^(5)
! d+1i
!
      allocate(Int2202(216,1))
      Int2202 = 0.0D0
!
      call calcRAM2202(mv,nv,pv)
!
      ram(1:216) = Int2202(1:216,1)
!
      ddd_index = 0
      do d1 = 1,6
        do d2 = 1,6
          do d3 = 1,6
            ddd_index = ddd_index + 1
            if(d1.gt.3) ram(ddd_index) = sq3*ram(ddd_index)
            if(d2.gt.3) ram(ddd_index) = sq3*ram(ddd_index)
            if(d3.gt.3) ram(ddd_index) = sq3*ram(ddd_index)
          end do
        end do
      end do
!
      deallocate(G,Int1000,Int0100,Int1100,Int2000,Int0200)
      deallocate(Int2100,Int1200,Int2200,Int2101,Int1201)
      deallocate(Int2201,Int2202)
!
      return
      end if
!
!--------------------------------------------------------------------
! [dddp]
!--------------------------------------------------------------------
      if(Lcode.eq.2221)then
!
! Requires 25 steps
!
      mv = 0
      nv = 0
      pv = 0
!
! Step 1:
! [1000]^(4) from [0000]^(5)
! a+1i
!
      allocate(Int1000(3,513))
      Int1000 = 0.0D0
!
      call calcRAM1000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1000(mv+imat(1:3,i),nv,pv)
        call calcRAM1000(mv,nv+imat(1:3,i),pv)
        call calcRAM1000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 2:
! [0100]^(4) from [0000]^(5)
! b+1i
!
      allocate(Int0100(3,513))
      Int0100 = 0.0D0
!
      call calcRAM0100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0100(mv+imat(1:3,i),nv,pv)
        call calcRAM0100(mv,nv+imat(1:3,i),pv)
        call calcRAM0100(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0100(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0100(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0100(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 3:
! [0010]^(4) from [0000]^(5)
! c+1i
!
      allocate(Int0010(3,513))
      Int0010 = 0.0D0
!
      call calcRAM0010(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0010(mv+imat(1:3,i),nv,pv)
        call calcRAM0010(mv,nv+imat(1:3,i),pv)
        call calcRAM0010(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0010(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0010(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0010(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 4:
! [1100]^(4) from [1000]^(5) and [0000]^(5)
! b+1i
!
      allocate(Int1100(9,513))
      Int1100 = 0.0D0
!
      call calcRAM1100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1100(mv+imat(1:3,i),nv,pv)
        call calcRAM1100(mv,nv+imat(1:3,i),pv)
        call calcRAM1100(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1100(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1100(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1100(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 5:
! [1010]^(4) from [1000]^(5) and [0000]^(5)
! c+1i
!
      allocate(Int1010(9,513))
      Int1010 = 0.0D0
!
      call calcRAM1010(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1010(mv+imat(1:3,i),nv,pv)
        call calcRAM1010(mv,nv+imat(1:3,i),pv)
        call calcRAM1010(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1010(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1010(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1010(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 6:
! [0110]^(4) from [0100]^(5) and [0000]^(5)
! c+1i
!
      allocate(Int0110(9,513))
      Int0110 = 0.0D0
!
      call calcRAM0110(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0110(mv+imat(1:3,i),nv,pv)
        call calcRAM0110(mv,nv+imat(1:3,i),pv)
        call calcRAM0110(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0110(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0110(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0110(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 7:
! [2000]^(4) from [1000]^(5) and [0000]^(5)
! a+1i
!
      allocate(Int2000(6,513))
      Int2000 = 0.0D0
!
      call calcRAM2000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2000(mv+imat(1:3,i),nv,pv)
        call calcRAM2000(mv,nv+imat(1:3,i),pv)
        call calcRAM2000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 8:
! [0200]^(4) from [0100]^(5) and [0000]^(5)
! b+1i
!
      allocate(Int0200(6,513))
      Int0200 = 0.0D0
!
      call calcRAM0200(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0200(mv+imat(1:3,i),nv,pv)
        call calcRAM0200(mv,nv+imat(1:3,i),pv)
        call calcRAM0200(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0200(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0200(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0200(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 9:
! [0020]^(4) from [0010]^(5) and [0000]^(5)
! c+1i
!
      allocate(Int0020(6,513))
      Int0020 = 0.0D0
!
      call calcRAM0020(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0020(mv+imat(1:3,i),nv,pv)
        call calcRAM0020(mv,nv+imat(1:3,i),pv)
        call calcRAM0020(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0020(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0020(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0020(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 10:
! [2100]^(4) from [2000]^(5) and [1000]^(5)
! b+1i
!
      allocate(Int2100(18,513))
      Int2100 = 0.0D0
!
      call calcRAM2100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2100(mv+imat(1:3,i),nv,pv)
        call calcRAM2100(mv,nv+imat(1:3,i),pv)
        call calcRAM2100(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2100(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2100(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2100(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 11:
! [1200]^(4) from [0200]^(5) and [0100]^(5)
! a+1i
!
      allocate(Int1200(18,513))
      Int1200 = 0.0D0
!
      call calcRAM1200(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1200(mv+imat(1:3,i),nv,pv)
        call calcRAM1200(mv,nv+imat(1:3,i),pv)
        call calcRAM1200(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1200(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1200(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1200(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 12:
! [2010]^(4) from [2000]^(5) and [1000]^(5)
! c+1i
!
      allocate(Int2010(18,513))
      Int2010 = 0.0D0
!
      call calcRAM2010(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2010(mv+imat(1:3,i),nv,pv)
        call calcRAM2010(mv,nv+imat(1:3,i),pv)
        call calcRAM2010(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2010(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2010(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2010(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 13:
! [1020]^(4) from [0020]^(5) and [1000]^(5)
! a+1i
!
      allocate(Int1020(18,513))
      Int1020 = 0.0D0
!
      call calcRAM1020(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1020(mv+imat(1:3,i),nv,pv)
        call calcRAM1020(mv,nv+imat(1:3,i),pv)
        call calcRAM1020(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1020(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1020(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1020(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 14:
! [0210]^(4) from [0200]^(5) and [0100]^(5)
! c+1i
!
      allocate(Int0210(18,513))
      Int0210 = 0.0D0
!
      call calcRAM0210(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0210(mv+imat(1:3,i),nv,pv)
        call calcRAM0210(mv,nv+imat(1:3,i),pv)
        call calcRAM0210(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0210(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0210(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0210(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 15:
! [0120]^(4) from [0020]^(5) and [0010]^(5)
! b+1i
!
      allocate(Int0120(18,513))
      Int0120 = 0.0D0
!
      call calcRAM0120(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0120(mv+imat(1:3,i),nv,pv)
        call calcRAM0120(mv,nv+imat(1:3,i),pv)
        call calcRAM0120(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0120(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0120(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0120(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 16:
! [2200]^(4) from [2100]^(5), [1100]^(5) and [2000]^(5)
! b+1i
!
      allocate(Int2200(36,513))
      Int2200 = 0.0D0
!
      call calcRAM2200(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2200(mv+imat(1:3,i),nv,pv)
        call calcRAM2200(mv,nv+imat(1:3,i),pv)
        call calcRAM2200(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2200(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2200(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2200(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 17:
! [2020]^(4) from [2010]^(5), [1010]^(5) and [2000]^(5)
! c+1i
!
      allocate(Int2020(36,513))
      Int2020 = 0.0D0
!
      call calcRAM2020(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2020(mv+imat(1:3,i),nv,pv)
        call calcRAM2020(mv,nv+imat(1:3,i),pv)
        call calcRAM2020(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2020(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2020(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2020(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 18:
! [0220]^(4) from [0210]^(5), [0110]^(5) and [0200]^(5)
! c+1i
!
      allocate(Int0220(36,513))
      Int0220 = 0.0D0
!
      call calcRAM0220(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0220(mv+imat(1:3,i),nv,pv)
        call calcRAM0220(mv,nv+imat(1:3,i),pv)
        call calcRAM0220(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0220(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0220(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0220(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 19:
! [2110]^(4) from [2100]^(5), [1100]^(5) and [2000]^(5)
! c+1i
!
      allocate(Int2110(54,513))
      Int2110 = 0.0D0
!
      call calcRAM2110c(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2110c(mv+imat(1:3,i),nv,pv)
        call calcRAM2110c(mv,nv+imat(1:3,i),pv)
        call calcRAM2110c(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2110c(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2110c(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2110c(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 20:
! [1210]^(4) from [1200]^(5), [0200]^(5) and [1100]^(5)
! c+1i
!
      allocate(Int1210(54,513))
      Int1210 = 0.0D0
!
      call calcRAM1210(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1210(mv+imat(1:3,i),nv,pv)
        call calcRAM1210(mv,nv+imat(1:3,i),pv)
        call calcRAM1210(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1210(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1210(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1210(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 21:
! [2210]^(4) from [2200]^(5), [1200]^(5) and [2100]^(5)
! c+1i
!
      allocate(Int2210(108,513))
      Int2210 = 0.0D0
!
      call calcRAM2210(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2210(mv+imat(1:3,i),nv,pv)
        call calcRAM2210(mv,nv+imat(1:3,i),pv)
        call calcRAM2210(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2210(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2210(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2210(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 22:
! [2120]^(4) from [2020]^(5), [1020]^(5) and [2010]^(5)
! b+1i
!
      allocate(Int2120(108,513))
      Int2120 = 0.0D0
!
      call calcRAM2120(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2120(mv+imat(1:3,i),nv,pv)
        call calcRAM2120(mv,nv+imat(1:3,i),pv)
        call calcRAM2120(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 23:
! [1220]^(4) from [0220]^(5), [0120]^(5) and [0210]^(5)
! a+1i
!
      allocate(Int1220(108,513))
      Int1220 = 0.0D0
!
      call calcRAM1220(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1220(mv+imat(1:3,i),nv,pv)
        call calcRAM1220(mv,nv+imat(1:3,i),pv)
        call calcRAM1220(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 24:
! [2220]^(4) from [2210]^(5), [1210]^(5), [2110]^(5) and [2200]^(5)
! c+1i
!
      allocate(Int2220(216,513))
      Int2220 = 0.0D0
!
      call calcRAM2220(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2220(mv+imat(1:3,i),nv,pv)
        call calcRAM2220(mv,nv+imat(1:3,i),pv)
        call calcRAM2220(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 25:
! [2221]^(4) from [2220]^(5), [1220]^(5), [2120]^(5) and [2210]^(5)
! d+1i
!
      allocate(Int2221(648,1))
      Int2221 = 0.0D0
!
      call calcRAM2221(mv,nv,pv)
!
      ram(1:648) = Int2221(1:648,1)
!
      dddp_index = 0
      do d1 = 1,6
        do d2 = 1,6
          do d3 = 1,6
            do p4 = 1,3
              dddp_index = dddp_index + 1
              if(d1.gt.3) ram(dddp_index) = sq3*ram(dddp_index)
              if(d2.gt.3) ram(dddp_index) = sq3*ram(dddp_index)
              if(d3.gt.3) ram(dddp_index) = sq3*ram(dddp_index)
            end do
          end do
        end do
      end do
!
      deallocate(G,Int1000,Int0100,Int0010,Int1100,Int1010,Int0110)
      deallocate(Int2000,Int0200,Int0020,Int2100,Int1200)
      deallocate(Int2010,Int1020,Int0210,Int0120,Int2200,Int2020,Int0220)
      deallocate(Int2110,Int1210,Int2210,Int2120,Int1220)
      deallocate(Int2220,Int2221)
!
      return
      end if
!
!--------------------------------------------------------------------
! [ddpd]
!--------------------------------------------------------------------
      if(Lcode.eq.2212)then
!
! Requires 25 steps
!
      mv = 0
      nv = 0
      pv = 0
!
! Step 1:
! [1000]^(4) from [0000]^(5)
! a+1i
!
      allocate(Int1000(3,513))
      Int1000 = 0.0D0
!
      call calcRAM1000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1000(mv+imat(1:3,i),nv,pv)
        call calcRAM1000(mv,nv+imat(1:3,i),pv)
        call calcRAM1000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 2:
! [0100]^(4) from [0000]^(5)
! b+1i
!
      allocate(Int0100(3,513))
      Int0100 = 0.0D0
!
      call calcRAM0100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0100(mv+imat(1:3,i),nv,pv)
        call calcRAM0100(mv,nv+imat(1:3,i),pv)
        call calcRAM0100(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0100(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0100(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0100(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 3:
! [0001]^(4) from [0000]^(5)
! d+1i
!
      allocate(Int0001(3,513))
      Int0001 = 0.0D0
!
      call calcRAM0001(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0001(mv+imat(1:3,i),nv,pv)
        call calcRAM0001(mv,nv+imat(1:3,i),pv)
        call calcRAM0001(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0001(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0001(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0001(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 4:
! [1100]^(4) from [1000]^(5) and [0000]^(5)
! b+1i
!
      allocate(Int1100(9,513))
      Int1100 = 0.0D0
!
      call calcRAM1100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1100(mv+imat(1:3,i),nv,pv)
        call calcRAM1100(mv,nv+imat(1:3,i),pv)
        call calcRAM1100(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1100(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1100(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1100(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 5:
! [1001]^(4) from [1000]^(5) and [0000]^(5)
! d+1i
!
      allocate(Int1001(9,513))
      Int1001 = 0.0D0
!
      call calcRAM1001(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1001(mv+imat(1:3,i),nv,pv)
        call calcRAM1001(mv,nv+imat(1:3,i),pv)
        call calcRAM1001(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1001(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1001(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1001(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 6:
! [0101]^(4) from [0100]^(5) and [0000]^(5)
! d+1i
!
      allocate(Int0101(9,513))
      Int0101 = 0.0D0
!
      call calcRAM0101(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0101(mv+imat(1:3,i),nv,pv)
        call calcRAM0101(mv,nv+imat(1:3,i),pv)
        call calcRAM0101(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0101(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0101(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0101(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 7:
! [2000]^(4) from [1000]^(5) and [0000]^(5)
! a+1i
!
      allocate(Int2000(6,513))
      Int2000 = 0.0D0
!
      call calcRAM2000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2000(mv+imat(1:3,i),nv,pv)
        call calcRAM2000(mv,nv+imat(1:3,i),pv)
        call calcRAM2000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 8:
! [0200]^(4) from [0100]^(5) and [0000]^(5)
! b+1i
!
      allocate(Int0200(6,513))
      Int0200 = 0.0D0
!
      call calcRAM0200(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0200(mv+imat(1:3,i),nv,pv)
        call calcRAM0200(mv,nv+imat(1:3,i),pv)
        call calcRAM0200(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0200(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0200(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0200(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 9:
! [0002]^(4) from [0001]^(5) and [0000]^(5)
! d+1i
!
      allocate(Int0002(6,513))
      Int0002 = 0.0D0
!
      call calcRAM0002(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0002(mv+imat(1:3,i),nv,pv)
        call calcRAM0002(mv,nv+imat(1:3,i),pv)
        call calcRAM0002(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0002(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0002(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0002(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 10:
! [2100]^(4) from [2000]^(5) and [1000]^(5)
! b+1i
!
      allocate(Int2100(18,513))
      Int2100 = 0.0D0
!
      call calcRAM2100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2100(mv+imat(1:3,i),nv,pv)
        call calcRAM2100(mv,nv+imat(1:3,i),pv)
        call calcRAM2100(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2100(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2100(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2100(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 11:
! [1200]^(4) from [0200]^(5) and [0100]^(5)
! a+1i
!
      allocate(Int1200(18,513))
      Int1200 = 0.0D0
!
      call calcRAM1200(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1200(mv+imat(1:3,i),nv,pv)
        call calcRAM1200(mv,nv+imat(1:3,i),pv)
        call calcRAM1200(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1200(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1200(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1200(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 12:
! [2001]^(4) from [2000]^(5) and [1000]^(5)
! d+1i
!
      allocate(Int2001(18,513))
      Int2001 = 0.0D0
!
      call calcRAM2001(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2001(mv+imat(1:3,i),nv,pv)
        call calcRAM2001(mv,nv+imat(1:3,i),pv)
        call calcRAM2001(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2001(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2001(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2001(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 13:
! [1002]^(4) from [0020]^(5) and [1000]^(5)
! a+1i
!
      allocate(Int1002(18,513))
      Int1002 = 0.0D0
!
      call calcRAM1002(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1002(mv+imat(1:3,i),nv,pv)
        call calcRAM1002(mv,nv+imat(1:3,i),pv)
        call calcRAM1002(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1002(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1002(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1002(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 14:
! [0201]^(4) from [0200]^(5) and [0100]^(5)
! d+1i
!
      allocate(Int0201(18,513))
      Int0201 = 0.0D0
!
      call calcRAM0201(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0201(mv+imat(1:3,i),nv,pv)
        call calcRAM0201(mv,nv+imat(1:3,i),pv)
        call calcRAM0201(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0201(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0201(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0201(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 15:
! [0102]^(4) from [0020]^(5) and [0010]^(5)
! b+1i
!
      allocate(Int0102(18,513))
      Int0102 = 0.0D0
!
      call calcRAM0102(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0102(mv+imat(1:3,i),nv,pv)
        call calcRAM0102(mv,nv+imat(1:3,i),pv)
        call calcRAM0102(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0102(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0102(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0102(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 16:
! [2200]^(4) from [2100]^(5), [1100]^(5) and [2000]^(5)
! b+1i
!
      allocate(Int2200(36,513))
      Int2200 = 0.0D0
!
      call calcRAM2200(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2200(mv+imat(1:3,i),nv,pv)
        call calcRAM2200(mv,nv+imat(1:3,i),pv)
        call calcRAM2200(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2200(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2200(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2200(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 17:
! [2002]^(4) from [2001]^(5), [1001]^(5) and [2000]^(5)
! d+1i
!
      allocate(Int2002(36,513))
      Int2002 = 0.0D0
!
      call calcRAM2002(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2002(mv+imat(1:3,i),nv,pv)
        call calcRAM2002(mv,nv+imat(1:3,i),pv)
        call calcRAM2002(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2002(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2002(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2002(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 18:
! [0202]^(4) from [0201]^(5), [0101]^(5) and [0200]^(5)
! d+1i
!
      allocate(Int0202(36,513))
      Int0202 = 0.0D0
!
      call calcRAM0202(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0202(mv+imat(1:3,i),nv,pv)
        call calcRAM0202(mv,nv+imat(1:3,i),pv)
        call calcRAM0202(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0202(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0202(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0202(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 19:
! [2101]^(4) from [2100]^(5), [1100]^(5) and [2000]^(5)
! d+1i
!
      allocate(Int2101(54,513))
      Int2101 = 0.0D0
!
      call calcRAM2101d(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2101d(mv+imat(1:3,i),nv,pv)
        call calcRAM2101d(mv,nv+imat(1:3,i),pv)
        call calcRAM2101d(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2101d(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2101d(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2101d(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 20:
! [1201]^(4) from [1200]^(5), [0200]^(5) and [1100]^(5)
! d+1i
!
      allocate(Int1201(54,513))
      Int1201 = 0.0D0
!
      call calcRAM1201d(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1201d(mv+imat(1:3,i),nv,pv)
        call calcRAM1201d(mv,nv+imat(1:3,i),pv)
        call calcRAM1201d(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1201d(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1201d(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1201d(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 21:
! [2201]^(4) from [2200]^(5), [1200]^(5) and [2100]^(5)
! d+1i
!
      allocate(Int2201(108,513))
      Int2201 = 0.0D0
!
      call calcRAM2201(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2201(mv+imat(1:3,i),nv,pv)
        call calcRAM2201(mv,nv+imat(1:3,i),pv)
        call calcRAM2201(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2201(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2201(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2201(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 22:
! [2102]^(4) from [2002]^(5), [1002]^(5) and [2001]^(5)
! b+1i
!
      allocate(Int2102(108,513))
      Int2102 = 0.0D0
!
      call calcRAM2102(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2102(mv+imat(1:3,i),nv,pv)
        call calcRAM2102(mv,nv+imat(1:3,i),pv)
        call calcRAM2102(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 23:
! [1202]^(4) from [0202]^(5), [0102]^(5) and [0201]^(5)
! a+1i
!
      allocate(Int1202(108,513))
      Int1202 = 0.0D0
!
      call calcRAM1202(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1202(mv+imat(1:3,i),nv,pv)
        call calcRAM1202(mv,nv+imat(1:3,i),pv)
        call calcRAM1202(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 24:
! [2202]^(4) from [2201]^(5), [1201]^(5), [2101]^(5) and [2200]^(5)
! d+1i
!
      allocate(Int2202(216,513))
      Int2202 = 0.0D0
!
      call calcRAM2202(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2202(mv+imat(1:3,i),nv,pv)
        call calcRAM2202(mv,nv+imat(1:3,i),pv)
        call calcRAM2202(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 25:
! [2212]^(4) from [2202]^(5), [1202]^(5), [2102]^(5) and [2201]^(5)
! c+1i
!
      allocate(Int2212(648,1))
      Int2212 = 0.0D0
!
      call calcRAM2212(mv,nv,pv)
!
      ram(1:648) = Int2212(1:648,1)
!
      ddpd_index = 0
      do d1 = 1,6
        do d2 = 1,6
          do p3 = 1,3
            do d4 = 1,6
              ddpd_index = ddpd_index + 1
              if(d1.gt.3) ram(ddpd_index) = sq3*ram(ddpd_index)
              if(d2.gt.3) ram(ddpd_index) = sq3*ram(ddpd_index)
              if(d4.gt.3) ram(ddpd_index) = sq3*ram(ddpd_index)
            end do
          end do
        end do
      end do
!
      deallocate(G,Int1000,Int0100,Int0001,Int1100,Int0101,Int1001)
      deallocate(Int2000,Int0200,Int0002,Int2100,Int1200,Int2001,Int1002)
      deallocate(Int0102,Int0201,Int2200,Int2002,Int0202,Int2101,Int1201)
      deallocate(Int2201,Int2102,Int1202,Int2202,Int2212)
!
      return
      end if
!
!--------------------------------------------------------------------
! [dddd]
!--------------------------------------------------------------------
      if(Lcode.eq.2222)then
!
! Requires 30 steps
!
      mv = 0
      nv = 0
      pv = 0
!
! Step 1:
! [1000]^(4) from [0000]^(5)
! a+1i
!
      allocate(Int1000(3,513))
      Int1000 = 0.0D0
!
      call calcRAM1000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1000(mv+imat(1:3,i),nv,pv)
        call calcRAM1000(mv,nv+imat(1:3,i),pv)
        call calcRAM1000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 2:
! [0100]^(4) from [0000]^(5)
! b+1i
!
      allocate(Int0100(3,513))
      Int0100 = 0.0D0
!
      call calcRAM0100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0100(mv+imat(1:3,i),nv,pv)
        call calcRAM0100(mv,nv+imat(1:3,i),pv)
        call calcRAM0100(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0100(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0100(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0100(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 3:
! [0010]^(4) from [0000]^(5)
! c+1i
!
      allocate(Int0010(3,513))
      Int0010 = 0.0D0
!
      call calcRAM0010(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0010(mv+imat(1:3,i),nv,pv)
        call calcRAM0010(mv,nv+imat(1:3,i),pv)
        call calcRAM0010(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0010(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0010(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0010(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 4:
! [1100]^(4) from [1000]^(5) and [0000]^(5)
! b+1i
!
      allocate(Int1100(9,513))
      Int1100 = 0.0D0
!
      call calcRAM1100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1100(mv+imat(1:3,i),nv,pv)
        call calcRAM1100(mv,nv+imat(1:3,i),pv)
        call calcRAM1100(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1100(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1100(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1100(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 5:
! [1010]^(4) from [1000]^(5) and [0000]^(5)
! c+1i
!
      allocate(Int1010(9,513))
      Int1010 = 0.0D0
!
      call calcRAM1010(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1010(mv+imat(1:3,i),nv,pv)
        call calcRAM1010(mv,nv+imat(1:3,i),pv)
        call calcRAM1010(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1010(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1010(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1010(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 6:
! [0110]^(4) from [0100]^(5) and [0000]^(5)
! c+1i
!
      allocate(Int0110(9,513))
      Int0110 = 0.0D0
!
      call calcRAM0110(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0110(mv+imat(1:3,i),nv,pv)
        call calcRAM0110(mv,nv+imat(1:3,i),pv)
        call calcRAM0110(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0110(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0110(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0110(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 7:
! [2000]^(4) from [1000]^(5) and [0000]^(5)
! a+1i
!
      allocate(Int2000(6,513))
      Int2000 = 0.0D0
!
      call calcRAM2000(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2000(mv+imat(1:3,i),nv,pv)
        call calcRAM2000(mv,nv+imat(1:3,i),pv)
        call calcRAM2000(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2000(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2000(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2000(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 8:
! [0200]^(4) from [0100]^(5) and [0000]^(5)
! b+1i
!
      allocate(Int0200(6,513))
      Int0200 = 0.0D0
!
      call calcRAM0200(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0200(mv+imat(1:3,i),nv,pv)
        call calcRAM0200(mv,nv+imat(1:3,i),pv)
        call calcRAM0200(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0200(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0200(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0200(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 9:
! [0020]^(4) from [0010]^(5) and [0000]^(5)
! c+1i
!
      allocate(Int0020(6,513))
      Int0020 = 0.0D0
!
      call calcRAM0020(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0020(mv+imat(1:3,i),nv,pv)
        call calcRAM0020(mv,nv+imat(1:3,i),pv)
        call calcRAM0020(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0020(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0020(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0020(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 10:
! [2100]^(4) from [2000]^(5) and [1000]^(5)
! b+1i
!
      allocate(Int2100(18,513))
      Int2100 = 0.0D0
!
      call calcRAM2100(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2100(mv+imat(1:3,i),nv,pv)
        call calcRAM2100(mv,nv+imat(1:3,i),pv)
        call calcRAM2100(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2100(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2100(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2100(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 11:
! [1200]^(4) from [0200]^(5) and [0100]^(5)
! a+1i
!
      allocate(Int1200(18,513))
      Int1200 = 0.0D0
!
      call calcRAM1200(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1200(mv+imat(1:3,i),nv,pv)
        call calcRAM1200(mv,nv+imat(1:3,i),pv)
        call calcRAM1200(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1200(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1200(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1200(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 12:
! [2010]^(4) from [2000]^(5) and [1000]^(5)
! c+1i
!
      allocate(Int2010(18,513))
      Int2010 = 0.0D0
!
      call calcRAM2010(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2010(mv+imat(1:3,i),nv,pv)
        call calcRAM2010(mv,nv+imat(1:3,i),pv)
        call calcRAM2010(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2010(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2010(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2010(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 13:
! [1020]^(4) from [0020]^(5) and [1000]^(5)
! a+1i
!
      allocate(Int1020(18,513))
      Int1020 = 0.0D0
!
      call calcRAM1020(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1020(mv+imat(1:3,i),nv,pv)
        call calcRAM1020(mv,nv+imat(1:3,i),pv)
        call calcRAM1020(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1020(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1020(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1020(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 14:
! [0210]^(4) from [0200]^(5) and [0100]^(5)
! c+1i
!
      allocate(Int0210(18,513))
      Int0210 = 0.0D0
!
      call calcRAM0210(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0210(mv+imat(1:3,i),nv,pv)
        call calcRAM0210(mv,nv+imat(1:3,i),pv)
        call calcRAM0210(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0210(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0210(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0210(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 15:
! [0120]^(4) from [0020]^(5) and [0010]^(5)
! b+1i
!
      allocate(Int0120(18,513))
      Int0120 = 0.0D0
!
      call calcRAM0120(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0120(mv+imat(1:3,i),nv,pv)
        call calcRAM0120(mv,nv+imat(1:3,i),pv)
        call calcRAM0120(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0120(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0120(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0120(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 16:
! [2200]^(4) from [2100]^(5), [1100]^(5) and [2000]^(5)
! b+1i
!
      allocate(Int2200(36,513))
      Int2200 = 0.0D0
!
      call calcRAM2200(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2200(mv+imat(1:3,i),nv,pv)
        call calcRAM2200(mv,nv+imat(1:3,i),pv)
        call calcRAM2200(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2200(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2200(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2200(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 17:
! [2020]^(4) from [2010]^(5), [1010]^(5) and [2000]^(5)
! c+1i
!
      allocate(Int2020(36,513))
      Int2020 = 0.0D0
!
      call calcRAM2020(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2020(mv+imat(1:3,i),nv,pv)
        call calcRAM2020(mv,nv+imat(1:3,i),pv)
        call calcRAM2020(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2020(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2020(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2020(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 18:
! [0220]^(4) from [0210]^(5), [0110]^(5) and [0200]^(5)
! c+1i
!
      allocate(Int0220(36,513))
      Int0220 = 0.0D0
!
      call calcRAM0220(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM0220(mv+imat(1:3,i),nv,pv)
        call calcRAM0220(mv,nv+imat(1:3,i),pv)
        call calcRAM0220(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM0220(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM0220(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM0220(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 19:
! [2110]^(4) from [2100]^(5), [1100]^(5) and [2000]^(5)
! c+1i
!
      allocate(Int2110(54,513))
      Int2110 = 0.0D0
!
      call calcRAM2110c(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2110c(mv+imat(1:3,i),nv,pv)
        call calcRAM2110c(mv,nv+imat(1:3,i),pv)
        call calcRAM2110c(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2110c(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2110c(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2110c(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 20:
! [1210]^(4) from [1200]^(5), [0200]^(5) and [1100]^(5)
! c+1i
!
      allocate(Int1210(54,513))
      Int1210 = 0.0D0
!
      call calcRAM1210(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1210(mv+imat(1:3,i),nv,pv)
        call calcRAM1210(mv,nv+imat(1:3,i),pv)
        call calcRAM1210(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1210(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1210(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1210(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 21:
! [1120]^(4) from [1020]^(5), [0020]^(5) and [1010]^(5)
! b+1i
!
      allocate(Int1120(54,513))
      Int1120 = 0.0D0
!
      call calcRAM1120(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1120(mv+imat(1:3,i),nv,pv)
        call calcRAM1120(mv,nv+imat(1:3,i),pv)
        call calcRAM1120(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1120(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1120(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1120(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 22:
! [2210]^(4) from [2200]^(5), [1200]^(5) and [2100]^(5)
! c+1i
!
      allocate(Int2210(108,513))
      Int2210 = 0.0D0
!
      call calcRAM2210(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2210(mv+imat(1:3,i),nv,pv)
        call calcRAM2210(mv,nv+imat(1:3,i),pv)
        call calcRAM2210(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2210(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2210(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2210(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 23:
! [2120]^(4) from [2020]^(5), [1020]^(5) and [2010]^(5)
! b+1i
!
      allocate(Int2120(108,513))
      Int2120 = 0.0D0
!
      call calcRAM2120(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2120(mv+imat(1:3,i),nv,pv)
        call calcRAM2120(mv,nv+imat(1:3,i),pv)
        call calcRAM2120(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2120(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2120(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2120(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 24:
! [1220]^(4) from [0220]^(5), [0120]^(5) and [0210]^(5)
! a+1i
!
      allocate(Int1220(108,513))
      Int1220 = 0.0D0
!
      call calcRAM1220(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1220(mv+imat(1:3,i),nv,pv)
        call calcRAM1220(mv,nv+imat(1:3,i),pv)
        call calcRAM1220(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM1220(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM1220(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM1220(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 25:
! [2220]^(4) from [2210]^(5), [1210]^(5), [2110]^(5) and [2200]^(5)
! c+1i
!
      allocate(Int2220(216,513))
      Int2220 = 0.0D0
!
      call calcRAM2220(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2220(mv+imat(1:3,i),nv,pv)
        call calcRAM2220(mv,nv+imat(1:3,i),pv)
        call calcRAM2220(mv,nv,pv+imat(1:3,i))
      end do ! i
      do i=2,3 ! x,y,z
        do j=1,i-1 ! x,y,z
          call calcRAM2220(mv+imat(1:3,i),nv+imat(1:3,j),pv)
          call calcRAM2220(mv+imat(1:3,j),nv+imat(1:3,i),pv)
          call calcRAM2220(mv,nv,pv+imat(1:3,i)+imat(1:3,j))
        end do ! j
      end do ! i
!
! Step 26:
! [2211]^(4) from [2210]^(5), [1210]^(5), [2110]^(5) and [2200]^(5)
! d+1i
!
      allocate(Int2211(324,513))
      Int2211 = 0.0D0
!
      call calcRAM2211(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2211(mv+imat(1:3,i),nv,pv)
        call calcRAM2211(mv,nv+imat(1:3,i),pv)
        call calcRAM2211(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 27:
! [2121]^(4) from [2120]^(5), [1120]^(5), [2020]^(5) and [2110]^(5)
! d+1i
!
      allocate(Int2121(324,513))
      Int2121 = 0.0D0
!
      call calcRAM2121(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2121(mv+imat(1:3,i),nv,pv)
        call calcRAM2121(mv,nv+imat(1:3,i),pv)
        call calcRAM2121(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 28:
! [1221]^(4) from [1220]^(5), [0220]^(5), [1120]^(5) and [1210]^(5)
! d+1i
!
      allocate(Int1221(324,513))
      Int1221 = 0.0D0
!
      call calcRAM1221(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM1221(mv+imat(1:3,i),nv,pv)
        call calcRAM1221(mv,nv+imat(1:3,i),pv)
        call calcRAM1221(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 29:
! [2221]^(4) from [2220]^(5), [1220]^(5), [2120]^(5) and [2210]^(5)
! d+1i
!
      allocate(Int2221(648,513))
      Int2221 = 0.0D0
!
      call calcRAM2221(mv,nv,pv)
      do i=1,3 ! x,y,z
        call calcRAM2221(mv+imat(1:3,i),nv,pv)
        call calcRAM2221(mv,nv+imat(1:3,i),pv)
        call calcRAM2221(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Step 30:
! [2222]^(4) from [2221]^(5), [1221]^(5), [2121]^(5), [2211]^(5) and [2220]^(5)
! d+1i
!
      allocate(Int2222(1296,1))
      Int2222 = 0.0D0
!
      call calcRAM2222(mv,nv,pv)
!
      ram(1:1296) = Int2222(1:1296,1)
!
      dddd_index = 0
      do d1 = 1,6
        do d2 = 1,6
          do d3 = 1,6
            do d4 = 1,6
              dddd_index = dddd_index + 1
              if(d1.gt.3) ram(dddd_index) = sq3*ram(dddd_index)
              if(d2.gt.3) ram(dddd_index) = sq3*ram(dddd_index)
              if(d3.gt.3) ram(dddd_index) = sq3*ram(dddd_index)
              if(d4.gt.3) ram(dddd_index) = sq3*ram(dddd_index)
            end do
          end do
        end do
      end do
!
      deallocate(G,Int1000,Int0100,Int0010,Int1100,Int1010,Int0110)
      deallocate(Int2000,Int0200,Int0020,Int2100,Int1200)
      deallocate(Int2010,Int1020,Int0210,Int0120,Int2200,Int2020,Int0220)
      deallocate(Int2110,Int1210,Int1120,Int2210,Int2120,Int1220,Int2220)
      deallocate(Int2211,Int2121,Int1221,Int2221,Int2222)
!
      return
      end if
!
!--------------------------------------------------------------------
! Shouldn't be able to get here, print out an error message
!--------------------------------------------------------------------
!
        write (*,*) 'Orbital angular momentum not recognised by subroutine RAMint'
        write (*,*) 'Please check that your basis set contains only s, p and d functions'
!
      end subroutine RAMint
!
      subroutine calcRAM1000(mv,nv,pv)   
!******************************************************!
!     [1000]^(m,n,p)                                   !
!******************************************************!
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      do i = 1,3 ! x,y,z
        Int1000(i,mnp) = RRa1(i)*G(mnp) + RRa2(i)*G(m1np(i)) + RRa3(i)*G(mn1p(i)) + RRa4(i)*G(mnp1(i))
      end do ! i
!
      end subroutine calcRAM1000 
      subroutine calcRAM0100(mv,nv,pv)   
!******************************************************!
!     [0100]^(m,n,p)                                   !
!******************************************************!
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      do i = 1,3 ! x,y,z
        Int0100(i,mnp) = RRb1(i)*G(mnp) + RRb2(i)*G(m1np(i)) + RRb3(i)*G(mn1p(i)) + RRb4(i)*G(mnp1(i))
      end do ! i
!
      end subroutine calcRAM0100 
      subroutine calcRAM0010(mv,nv,pv)   
!******************************************************!
!     [0010]^(m,n,p)                                   !
!******************************************************!
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      do i = 1,3 ! x,y,z
        Int0010(i,mnp) = RRc1(i)*G(mnp) + RRc2(i)*G(m1np(i)) + RRc3(i)*G(mn1p(i)) + RRc4(i)*G(mnp1(i))
      end do ! i
!
      end subroutine calcRAM0010 
      subroutine calcRAM0001(mv,nv,pv)   
!******************************************************!
!     [0001]^(m,n,p)                                   !
!******************************************************!
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      do i = 1,3 ! x,y,z
        Int0001(i,mnp) = RRd1(i)*G(mnp) + RRd2(i)*G(m1np(i)) + RRd3(i)*G(mn1p(i)) + RRd4(i)*G(mnp1(i))
      end do ! i
!
      end subroutine calcRAM0001 
      subroutine calcRAM1100(mv,nv,pv)   
!******************************************************!
!     [1100]^(m,n,p)                                   !
!******************************************************!
! b+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, p1, pp_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      pp_index = 0
      do p1 = 1,3 ! x,y,z
        do i = 1,3 ! x,y,z
          pp_index = pp_index + 1           
!
          Int1100(pp_index,mnp) = RRb1(i)*Int1000(p1,mnp)     + RRb2(i)*Int1000(p1,m1np(i)) &
                                + RRb3(i)*Int1000(p1,mn1p(i)) + RRb4(i)*Int1000(p1,mnp1(i))
          if(p1.eq.i)then
            Int1100(pp_index,mnp) = Int1100(pp_index,mnp) &
                                  + RRb5*G(mnp) + RRb6*G(m1np(i)) + RRb7*G(mn1p(i)) + RRb8*G(mnp1(i))
          end if
!
        end do ! i
      end do ! p1
!
      end subroutine calcRAM1100 
      subroutine calcRAM1010(mv,nv,pv)   
!******************************************************!
!     [1010]^(m,n,p)                                   !
!******************************************************!
! c+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, p1, pp_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      pp_index = 0
      do p1 = 1,3 ! x,y,z
        do i = 1,3 ! x,y,z
          pp_index = pp_index + 1           
!
          Int1010(pp_index,mnp) = RRc1(i)*Int1000(p1,mnp)     + RRc2(i)*Int1000(p1,m1np(i)) &
                                + RRc3(i)*Int1000(p1,mn1p(i)) + RRc4(i)*Int1000(p1,mnp1(i))
          if(p1.eq.i)then
            Int1010(pp_index,mnp) = Int1010(pp_index,mnp) &
                                  + RRc5*G(m1np(i)) + RRc6*G(mn1p(i)) + RRc7*G(mnp1(i))
          end if
!
        end do ! i
      end do ! p1
!
      end subroutine calcRAM1010 
      subroutine calcRAM1001(mv,nv,pv)   
!******************************************************!
!     [1001]^(m,n,p)                                   !
!******************************************************!
! d+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, p1, pp_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      pp_index = 0
      do p1 = 1,3 ! x,y,z
        do i = 1,3 ! x,y,z
          pp_index = pp_index + 1           
!
          Int1001(pp_index,mnp) = RRd1(i)*Int1000(p1,mnp)     + RRd2(i)*Int1000(p1,m1np(i)) &
                                + RRd3(i)*Int1000(p1,mn1p(i)) + RRd4(i)*Int1000(p1,mnp1(i))
          if(p1.eq.i)then
            Int1001(pp_index,mnp) = Int1001(pp_index,mnp) &
                                  + RRd5*G(m1np(i)) + RRd6*G(mn1p(i)) + RRd7*G(mnp1(i))
          end if
!
        end do ! i
      end do ! p1
!
      end subroutine calcRAM1001 
      subroutine calcRAM0101(mv,nv,pv)   
!******************************************************!
!     [0101]^(m,n,p)                                   !
!******************************************************!
! d+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, p1, pp_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      pp_index = 0
      do p1 = 1,3 ! x,y,z
        do i = 1,3 ! x,y,z
          pp_index = pp_index + 1           
!
          Int0101(pp_index,mnp) = RRd1(i)*Int0100(p1,mnp)     + RRd2(i)*Int0100(p1,m1np(i)) &
                                + RRd3(i)*Int0100(p1,mn1p(i)) + RRd4(i)*Int0100(p1,mnp1(i))
          if(p1.eq.i)then
            Int0101(pp_index,mnp) = Int0101(pp_index,mnp) &
                                  + RRd8*G(m1np(i)) + RRd9*G(mn1p(i)) + RRd10*G(mnp1(i))
          end if
!
        end do ! i
      end do ! p1
!
      end subroutine calcRAM0101 
      subroutine calcRAM0110(mv,nv,pv)   
!******************************************************!
!     [0110]^(m,n,p)                                   !
!******************************************************!
! c+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, p1, pp_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      pp_index = 0
      do p1 = 1,3 ! x,y,z
        do i = 1,3 ! x,y,z
          pp_index = pp_index + 1           
!
          Int0110(pp_index,mnp) = RRc1(i)*Int0100(p1,mnp)     + RRc2(i)*Int0100(p1,m1np(i)) &
                                + RRc3(i)*Int0100(p1,mn1p(i)) + RRc4(i)*Int0100(p1,mnp1(i))
          if(p1.eq.i)then
            Int0110(pp_index,mnp) = Int0110(pp_index,mnp) &
                                  + RRc8*G(m1np(i)) + RRc9*G(mn1p(i)) + RRc10*G(mnp1(i))
          end if
!
        end do ! i
      end do ! p1
!
      end subroutine calcRAM0110 
      subroutine calcRAM1110(mv,nv,pv)   
!******************************************************!
!     [1110]^(m,n,p)                                   !
!******************************************************!
! c+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, p1, p2, pp_index, ppp_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      ppp_index = 0
      pp_index = 0
      do p1 = 1,3 ! x,y,z
        do p2 = 1,3 ! x,y,z
          pp_index = pp_index + 1           
          do i = 1,3 ! x,y,z
            ppp_index = ppp_index + 1           
!
            Int1110(ppp_index,mnp) = RRc1(i)*Int1100(pp_index,mnp) + RRc2(i)*Int1100(pp_index,m1np(i)) &
                               + RRc3(i)*Int1100(pp_index,mn1p(i)) + RRc4(i)*Int1100(pp_index,mnp1(i))
            if(i.eq.p1)then
              Int1110(ppp_index,mnp) = Int1110(ppp_index,mnp) &
                                     + RRc5*Int0100(p2,m1np(i)) + RRc6*Int0100(p2,mn1p(i))&
                                     + RRc7*Int0100(p2,mnp1(i)) 
            end if
            if(i.eq.p2)then
              Int1110(ppp_index,mnp) = Int1110(ppp_index,mnp) &
                                     + RRc8*Int1000(p1,m1np(i)) + RRc9*Int1000(p1,mn1p(i))&
                                     + RRc10*Int1000(p1,mnp1(i)) 
            end if 
!
          end do ! i
        end do ! p2
      end do ! p1
!
      end subroutine calcRAM1110 
      subroutine calcRAM1101(mv,nv,pv)   
!******************************************************!
!     [1101]^(m,n,p)                                   !
!******************************************************!
! d+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, p1, p2, pp_index, ppp_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      ppp_index = 0
      pp_index = 0
      do p1 = 1,3 ! x,y,z
        do p2 = 1,3 ! x,y,z
          pp_index = pp_index + 1           
          do i = 1,3 ! x,y,z
            ppp_index = ppp_index + 1           
!
            Int1101(ppp_index,mnp) = RRd1(i)*Int1100(pp_index,mnp) + RRd2(i)*Int1100(pp_index,m1np(i)) &
                               + RRd3(i)*Int1100(pp_index,mn1p(i)) + RRd4(i)*Int1100(pp_index,mnp1(i))
            if(i.eq.p1)then
              Int1101(ppp_index,mnp) = Int1101(ppp_index,mnp) &
                                     + RRd5*Int0100(p2,m1np(i)) + RRd6*Int0100(p2,mn1p(i))&
                                     + RRd7*Int0100(p2,mnp1(i)) 
            end if
            if(i.eq.p2)then
              Int1101(ppp_index,mnp) = Int1101(ppp_index,mnp) &
                                     + RRd8*Int1000(p1,m1np(i)) + RRd9*Int1000(p1,mn1p(i))&
                                     + RRd10*Int1000(p1,mnp1(i)) 
            end if 
!
          end do ! i
        end do ! p2
      end do ! p1
!
      end subroutine calcRAM1101 
      subroutine calcRAM0111(mv,nv,pv)   
!******************************************************!
!     [0111]^(m,n,p)                                   !
!******************************************************!
! d+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, p1, p2, pp_index, ppp_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      ppp_index = 0
      pp_index = 0
      do p1 = 1,3 ! x,y,z
        do p2 = 1,3 ! x,y,z
          pp_index = pp_index + 1           
          do i = 1,3 ! x,y,z
            ppp_index = ppp_index + 1           
!
            Int0111(ppp_index,mnp) = RRd1(i)*Int0110(pp_index,mnp) + RRd2(i)*Int0110(pp_index,m1np(i)) &
                               + RRd3(i)*Int0110(pp_index,mn1p(i)) + RRd4(i)*Int0110(pp_index,mnp1(i))
            if(i.eq.p1)then
              Int0111(ppp_index,mnp) = Int0111(ppp_index,mnp) &
                                     + RRd8*Int0010(p2,m1np(i)) + RRd9*Int0010(p2,mn1p(i))&
                                     + RRd10*Int0010(p2,mnp1(i)) 
            end if
            if(i.eq.p2)then
              Int0111(ppp_index,mnp) = Int0111(ppp_index,mnp) &
                                     + RRd11*Int0100(p1,mnp) + RRd12*Int0100(p1,m1np(i))&
                                     + RRd13*Int0100(p1,mn1p(i)) + RRd14*Int0100(p1,mnp1(i))
            end if 
!
          end do ! i
        end do ! p2
      end do ! p1
!
      end subroutine calcRAM0111
      subroutine calcRAM1111(mv,nv,pv)   
!******************************************************!
!     [1111]^(m,n,p)                                   !
!******************************************************!
! d+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, p1, p2, p3, pp_index, ppp_index
      integer :: pppp_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      pppp_index = 0
      ppp_index = 0
      pp_index = 0
      do p1 = 1,3 ! x,y,z
        do p2 = 1,3 ! x,y,z
          do p3 = 1,3 ! x,y,z
            ppp_index = ppp_index + 1           
            do i = 1,3 ! x,y,z
              pppp_index = pppp_index + 1           
!
      Int1111(pppp_index,mnp) = RRd1(i)*Int1110(ppp_index,mnp) + RRd2(i)*Int1110(ppp_index,m1np(i)) &
                              + RRd3(i)*Int1110(ppp_index,mn1p(i)) + RRd4(i)*Int1110(ppp_index,mnp1(i))
            if(i.eq.p1)then
              pp_index = 3*(p2-1) + p3
              Int1111(pppp_index,mnp) = Int1111(pppp_index,mnp) &
                                      + RRd5*Int0110(pp_index,m1np(i)) + RRd6*Int0110(pp_index,mn1p(i))&
                                      + RRd7*Int0110(pp_index,mnp1(i)) 
            end if
            if(i.eq.p2)then
              pp_index = 3*(p1-1) + p3
              Int1111(pppp_index,mnp) = Int1111(pppp_index,mnp) &
                                      + RRd8*Int1010(pp_index,m1np(i)) + RRd9*Int1010(pp_index,mn1p(i))&
                                      + RRd10*Int1010(pp_index,mnp1(i))
            end if 
            if(i.eq.p3)then
              pp_index = 3*(p1-1) + p2
              Int1111(pppp_index,mnp) = Int1111(pppp_index,mnp) &
                                      + RRd11*Int1100(pp_index,mnp) + RRd12*Int1100(pp_index,m1np(i))&
                                    + RRd13*Int1100(pp_index,mn1p(i)) + RRd14*Int1100(pp_index,mnp1(i))
            end if 
!
            end do ! i
          end do ! p3
        end do ! p2
      end do ! p1
!
      end subroutine calcRAM1111
      subroutine calcRAM2000(mv,nv,pv)   
!******************************************************!
!     [2000]^(m,n,p)                                   !
!******************************************************!
! a+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, p1, d_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! first three d's
      d_index = 0
      do p1 = 1,3 ! x,y,z
        i = p1
        d_index = d_index + 1
!
        Int2000(d_index,mnp) = RRa1(i)*Int1000(p1,mnp) + RRa2(i)*Int1000(p1,m1np(i)) &
                             + RRa3(i)*Int1000(p1,mn1p(i)) + RRa4(i)*Int1000(p1,mnp1(i))&
                             + RRa5*G(mnp) + RRa6*G(m1np(i)) + RRa7*G(mn1p(i)) + RRa8*G(mnp1(i))
!
      end do ! p1
! last three d's
      do p1 = 2,3 ! y,z
        do i = 1,p1-1
          d_index = d_index + 1
!
          Int2000(d_index,mnp) = RRa1(i)*Int1000(p1,mnp) + RRa2(i)*Int1000(p1,m1np(i)) &
                               + RRa3(i)*Int1000(p1,mn1p(i)) + RRa4(i)*Int1000(p1,mnp1(i))
!
        end do ! i
      end do ! p1
!
      end subroutine calcRAM2000
      subroutine calcRAM0200(mv,nv,pv)   
!******************************************************!
!     [0200]^(m,n,p)                                   !
!******************************************************!
! b+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, p1, d_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! first three d's
      d_index = 0
      do p1 = 1,3 ! x,y,z
        i = p1
        d_index = d_index + 1
!
        Int0200(d_index,mnp) = RRb1(i)*Int0100(p1,mnp) + RRb2(i)*Int0100(p1,m1np(i)) &
                             + RRb3(i)*Int0100(p1,mn1p(i)) + RRb4(i)*Int0100(p1,mnp1(i))&
                             + RRb9*G(mnp) + RRb10*G(m1np(i)) + RRb11*G(mn1p(i)) + RRb12*G(mnp1(i))
!
      end do ! p1
! last three d's
      do p1 = 2,3 ! y,z
        do i = 1,p1-1
          d_index = d_index + 1
!
          Int0200(d_index,mnp) = RRb1(i)*Int0100(p1,mnp) + RRb2(i)*Int0100(p1,m1np(i)) &
                               + RRb3(i)*Int0100(p1,mn1p(i)) + RRb4(i)*Int0100(p1,mnp1(i))
!
        end do ! i
      end do ! p1
!
      end subroutine calcRAM0200
      subroutine calcRAM0020(mv,nv,pv)   
!******************************************************!
!     [0020]^(m,n,p)                                   !
!******************************************************!
! c+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, p1, d_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! first three d's
      d_index = 0
      do p1 = 1,3 ! x,y,z
        i = p1
        d_index = d_index + 1
!
        Int0020(d_index,mnp) = RRc1(i)*Int0010(p1,mnp) + RRc2(i)*Int0010(p1,m1np(i)) &
                             + RRc3(i)*Int0010(p1,mn1p(i)) + RRc4(i)*Int0010(p1,mnp1(i))&
                             + RRc11*G(mnp) + RRc12*G(m1np(i)) + RRc13*G(mn1p(i)) + RRc14*G(mnp1(i))
!
      end do ! p1
! last three d's
      do p1 = 2,3 ! y,z
        do i = 1,p1-1
          d_index = d_index + 1
!
          Int0020(d_index,mnp) = RRc1(i)*Int0010(p1,mnp) + RRc2(i)*Int0010(p1,m1np(i)) &
                               + RRc3(i)*Int0010(p1,mn1p(i)) + RRc4(i)*Int0010(p1,mnp1(i))
!
        end do ! i
      end do ! p1
!
      end subroutine calcRAM0020
      subroutine calcRAM0002(mv,nv,pv)   
!******************************************************!
!     [0002]^(m,n,p)                                   !
!******************************************************!
! d+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, p1, d_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! first three d's
      d_index = 0
      do p1 = 1,3 ! x,y,z
        i = p1
        d_index = d_index + 1
!
        Int0002(d_index,mnp) = RRd1(i)*Int0001(p1,mnp) + RRd2(i)*Int0001(p1,m1np(i)) &
                             + RRd3(i)*Int0001(p1,mn1p(i)) + RRd4(i)*Int0001(p1,mnp1(i))&
                             + RRd15*G(mnp) + RRd16*G(m1np(i)) + RRd17*G(mn1p(i)) + RRd18*G(mnp1(i))
!
      end do ! p1
! last three d's
      do p1 = 2,3 ! y,z
        do i = 1,p1-1
          d_index = d_index + 1
!
          Int0002(d_index,mnp) = RRd1(i)*Int0001(p1,mnp) + RRd2(i)*Int0001(p1,m1np(i)) &
                               + RRd3(i)*Int0001(p1,mn1p(i)) + RRd4(i)*Int0001(p1,mnp1(i))
!
        end do ! i
      end do ! p1
!
      end subroutine calcRAM0002
      subroutine calcRAM2100(mv,nv,pv)   
!******************************************************!
!     [2100]^(m,n,p)                                   !
!******************************************************!
! b+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, d1, dp_index, a_i, p1
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      dp_index = 0
      do d1 = 1,6
        do i = 1,3 ! x,y,z
          dp_index = dp_index + 1
!
          a_i = Lvec_d(d1,i)
!
          Int2100(dp_index,mnp) = RRb1(i)*Int2000(d1,mnp)     + RRb2(i)*Int2000(d1,m1np(i))&
                                + RRb3(i)*Int2000(d1,mn1p(i)) + RRb4(i)*Int2000(d1,mnp1(i))
          if(a_i.ne.0)then ! (a-1i) terms
            v_1i(1:3) = Lvec_d(d1,1:3)
            v_1i(i) = v_1i(i) - 1
            p1 = dot_product(v_1i,xyz)
            Int2100(dp_index,mnp) = Int2100(dp_index,mnp)&
                                  + dble(a_i)*(RRb5*Int1000(p1,mnp)     + RRb6*Int1000(p1,m1np(i))&
                                             + RRb7*Int1000(p1,mn1p(i)) + RRb8*Int1000(p1,mnp1(i)))
          end if
!
        end do ! i
      end do ! d1
!
      end subroutine calcRAM2100
      subroutine calcRAM2010(mv,nv,pv)   
!******************************************************!
!     [2010]^(m,n,p)                                   !
!******************************************************!
! c+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, d1, dp_index, a_i, p1
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      dp_index = 0
      do d1 = 1,6
        do i = 1,3 ! x,y,z
          dp_index = dp_index + 1
!
          a_i = Lvec_d(d1,i)
!
          Int2010(dp_index,mnp) = RRc1(i)*Int2000(d1,mnp) + RRc2(i)*Int2000(d1,m1np(i)) &
                                + RRc3(i)*Int2000(d1,mn1p(i)) + RRc4(i)*Int2000(d1,mnp1(i))
          if(a_i.ne.0)then ! (a-1i) terms
            v_1i(1:3) = Lvec_d(d1,1:3)
            v_1i(i) = v_1i(i) - 1
            p1 = dot_product(v_1i,xyz)
            Int2010(dp_index,mnp) = Int2010(dp_index,mnp)&
                                  + dble(a_i)*(RRc5*Int1000(p1,m1np(i))&
                                             + RRc6*Int1000(p1,mn1p(i)) + RRc7*Int1000(p1,mnp1(i)))
          end if
!
        end do ! i
      end do ! d1
!
      end subroutine calcRAM2010
      subroutine calcRAM2001(mv,nv,pv)   
!******************************************************!
!     [2001]^(m,n,p)                                   !
!******************************************************!
! d+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, d1, dp_index, a_i, p1
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      dp_index = 0
      do d1 = 1,6
        do i = 1,3 ! x,y,z
          dp_index = dp_index + 1
!
          a_i = Lvec_d(d1,i)
!
          Int2001(dp_index,mnp) = RRd1(i)*Int2000(d1,mnp)     + RRd2(i)*Int2000(d1,m1np(i)) &
                                + RRd3(i)*Int2000(d1,mn1p(i)) + RRd4(i)*Int2000(d1,mnp1(i))
          if(a_i.ne.0)then ! (a-1i) terms
            v_1i(1:3) = Lvec_d(d1,1:3)
            v_1i(i) = v_1i(i) - 1
            p1 = dot_product(v_1i,xyz)
            Int2001(dp_index,mnp) = Int2001(dp_index,mnp)&
                                  + dble(a_i)*(RRd5*Int1000(p1,m1np(i))&
                                             + RRd6*Int1000(p1,mn1p(i)) + RRd7*Int1000(p1,mnp1(i)))
          end if
!
        end do ! i
      end do ! d1
!
      end subroutine calcRAM2001
      subroutine calcRAM1200(mv,nv,pv)   
!******************************************************!
!     [1200]^(m,n,p)                                   !
!******************************************************!
! a+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, d1, pd_index, b_i, p1
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      pd_index = 0
      do i = 1,3 ! x,y,z
        do d1 = 1,6
          pd_index = pd_index + 1
!
          b_i = Lvec_d(d1,i)
!
          Int1200(pd_index,mnp) = RRa1(i)*Int0200(d1,mnp)     + RRa2(i)*Int0200(d1,m1np(i))&
                                + RRa3(i)*Int0200(d1,mn1p(i)) + RRa4(i)*Int0200(d1,mnp1(i))
          if(b_i.ne.0)then ! (b-1i) terms
            v_1i(1:3) = Lvec_d(d1,1:3)
            v_1i(i) = v_1i(i) - 1
            p1 = dot_product(v_1i,xyz)
            Int1200(pd_index,mnp) = Int1200(pd_index,mnp)&
                                  + dble(b_i)*(RRa9*Int0100(p1,mnp)      + RRa10*Int0100(p1,m1np(i))&
                                             + RRa11*Int0100(p1,mn1p(i)) + RRa12*Int0100(p1,mnp1(i)))
          end if
!
        end do ! d1
      end do ! i
!
      end subroutine calcRAM1200
      subroutine calcRAM1020(mv,nv,pv)   
!******************************************************!
!     [1020]^(m,n,p)                                   !
!******************************************************!
! a+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, d1, pd_index, c_i, p1
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      pd_index = 0
      do i = 1,3
        do d1 = 1,6
          pd_index = pd_index + 1
!
          c_i = Lvec_d(d1,i)
!
          Int1020(pd_index,mnp) = RRa1(i)*Int0020(d1,mnp)     + RRa2(i)*Int0020(d1,m1np(i))&
                                + RRa3(i)*Int0020(d1,mn1p(i)) + RRa4(i)*Int0020(d1,mnp1(i))
          if(c_i.ne.0)then
            v_1i(1:3)=Lvec_d(d1,1:3)
            v_1i(i)=v_1i(i)-1
            p1=dot_product(v_1i,xyz)
            Int1020(pd_index,mnp) = Int1020(pd_index,mnp)&
                                  + dble(c_i)*(RRa13*Int0010(p1,m1np(i)) + RRa14*Int0010(p1,mn1p(i))&
                                             + RRa15*Int0010(p1,mnp1(i)))
          end if
        end do ! d1
      end do ! i
!
      end subroutine calcRAM1020
      subroutine calcRAM0102(mv,nv,pv)   
!******************************************************!
!     [0102]^(m,n,p)                                   !
!******************************************************!
! b+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, d1, pd_index, d_i, p1
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      pd_index = 0
      do i = 1,3
        do d1 = 1,6
          pd_index = pd_index + 1
!
          d_i = Lvec_d(d1,i)
!
          Int0102(pd_index,mnp) = RRb1(i)*Int0002(d1,mnp)     + RRb2(i)*Int0002(d1,m1np(i))&
                                + RRb3(i)*Int0002(d1,mn1p(i)) + RRb4(i)*Int0002(d1,mnp1(i))
          if(d_i.ne.0)then
            v_1i(1:3)=Lvec_d(d1,1:3)
            v_1i(i)=v_1i(i)-1
            p1=dot_product(v_1i,xyz)                   
            Int0102(pd_index,mnp) = Int0102(pd_index,mnp)&
                                  + dble(d_i)*(RRb16*Int0001(p1,m1np(i)) + RRb17*Int0001(p1,mn1p(i))&
                                             + RRb18*Int0001(p1,mnp1(i)))
          end if
        end do ! d1
      end do ! i
!
      end subroutine calcRAM0102
      subroutine calcRAM1002(mv,nv,pv)   
!******************************************************!
!     [1002]^(m,n,p)                                   !
!******************************************************!
! a+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, d1, pd_index, d_i, p1
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      pd_index = 0
      do i = 1,3
        do d1 = 1,6
          pd_index = pd_index + 1
!
          d_i = Lvec_d(d1,i)
!
          Int1002(pd_index,mnp) = RRa1(i)*Int0002(d1,mnp)     + RRa2(i)*Int0002(d1,m1np(i))&
                                + RRa3(i)*Int0002(d1,mn1p(i)) + RRa4(i)*Int0002(d1,mnp1(i))
          if(d_i.ne.0)then
            v_1i(1:3)=Lvec_d(d1,1:3)
            v_1i(i)=v_1i(i)-1
            p1=dot_product(v_1i,xyz)                   
            Int1002(pd_index,mnp) = Int1002(pd_index,mnp)&
                                  + dble(d_i)*(RRa16*Int0001(p1,m1np(i)) + RRa17*Int0001(p1,mn1p(i))&
                                             + RRa18*Int0001(p1,mnp1(i)))
          end if
        end do ! d1
      end do ! i
!
      end subroutine calcRAM1002
      subroutine calcRAM0120(mv,nv,pv)   
!******************************************************!
!     [0120]^(m,n,p)                                   !
!******************************************************!
! b+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, d1, pd_index, c_i, p1
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      pd_index = 0
      do i = 1,3
        do d1 = 1,6
          pd_index = pd_index + 1
!
          c_i = Lvec_d(d1,i)
!
          Int0120(pd_index,mnp) = RRb1(i)*Int0020(d1,mnp) + RRb2(i)*Int0020(d1,m1np(i))&
                                + RRb3(i)*Int0020(d1,mn1p(i)) + RRb4(i)*Int0020(d1,mnp1(i))
          if(c_i.ne.0)then
            v_1i(1:3)=Lvec_d(d1,1:3)
            v_1i(i)=v_1i(i)-1
            p1=dot_product(v_1i,xyz)
            Int0120(pd_index,mnp) = Int0120(pd_index,mnp)&
                                  + dble(c_i)*(RRb13*Int0010(p1,m1np(i)) + RRb14*Int0010(p1,mn1p(i))&
                                             + RRb15*Int0010(p1,mnp1(i)))
          end if
        end do ! d1
      end do ! i
!
      end subroutine calcRAM0120
      subroutine calcRAM0210(mv,nv,pv)   
!******************************************************!
!     [0210]^(m,n,p)                                   !
!******************************************************!
! c+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, d1, p1, dp_index, b_i
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      dp_index = 0
      do d1 = 1,6
        do i = 1,3 ! x,y,z
          dp_index = dp_index + 1
!
          b_i = Lvec_d(d1,i)
!
          Int0210(dp_index,mnp) = RRc1(i)*Int0200(d1,mnp)     + RRc2(i)*Int0200(d1,m1np(i)) &
                                + RRc3(i)*Int0200(d1,mn1p(i)) + RRc4(i)*Int0200(d1,mnp1(i))
          if(b_i.ne.0)then ! (b-1i) terms
            v_1i(1:3) = Lvec_d(d1,1:3)
            v_1i(i) = v_1i(i) - 1
            p1 = dot_product(v_1i,xyz)
            Int0210(dp_index,mnp) = Int0210(dp_index,mnp)&
                                  + dble(b_i)*(RRc8*Int0100(p1,m1np(i))&
                                             + RRc9*Int0100(p1,mn1p(i)) + RRc10*Int0100(p1,mnp1(i)))
          end if
!
        end do ! i
      end do ! d1
!
      end subroutine calcRAM0210
      subroutine calcRAM0201(mv,nv,pv)   
!******************************************************!
!     [0201]^(m,n,p)                                   !
!******************************************************!
! d+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, d1, p1, dp_index, b_i
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      dp_index = 0
      do d1 = 1,6
        do i = 1,3 ! x,y,z
          dp_index = dp_index + 1
!
          b_i = Lvec_d(d1,i)
!
          Int0201(dp_index,mnp) = RRd1(i)*Int0200(d1,mnp)     + RRd2(i)*Int0200(d1,m1np(i)) &
                                + RRd3(i)*Int0200(d1,mn1p(i)) + RRd4(i)*Int0200(d1,mnp1(i))
          if(b_i.ne.0)then ! (b-1i) terms
            v_1i(1:3) = Lvec_d(d1,1:3)
            v_1i(i) = v_1i(i) - 1
            p1 = dot_product(v_1i,xyz)
            Int0201(dp_index,mnp) = Int0201(dp_index,mnp)&
                                  + dble(b_i)*(RRd8*Int0100(p1,m1np(i))&
                                             + RRd9*Int0100(p1,mn1p(i)) + RRd10*Int0100(p1,mnp1(i)))
          end if
!
        end do ! i
      end do ! d1
!
      end subroutine calcRAM0201
      subroutine calcRAM2110c(mv,nv,pv)   
!******************************************************!
!     [2110]^(m,n,p)                                   !
!******************************************************!
! c+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, d1, p1, p2, dp_index, dpp_index, a_i
      integer :: pp_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      dp_index = 0
      dpp_index = 0
      do d1 = 1,6
        do p2 = 1,3
          dp_index = dp_index + 1
          do i = 1,3 
            dpp_index = dpp_index + 1
!
            a_i = Lvec_d(d1,i)
!
            Int2110(dpp_index,mnp) = RRc1(i)*Int2100(dp_index,mnp)     + RRc2(i)*Int2100(dp_index,m1np(i))&
                                   + RRc3(i)*Int2100(dp_index,mn1p(i)) + RRc4(i)*Int2100(dp_index,mnp1(i))
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3) = Lvec_d(d1,1:3)
              v_1i(i) = v_1i(i) - 1
              p1 = dot_product(v_1i,xyz)
              pp_index = 3*(p1-1) + p2
              Int2110(dpp_index,mnp) = Int2110(dpp_index,mnp)&
                          + dble(a_i)*(RRc5*Int1100(pp_index,m1np(i))&
                                     + RRc6*Int1100(pp_index,mn1p(i)) + RRc7*Int1100(pp_index,mnp1(i)))
            end if
            if(i.eq.p2)then
              Int2110(dpp_index,mnp) = Int2110(dpp_index,mnp)&
                                     + RRc8*Int2000(d1,m1np(i)) + RRc9*Int2000(d1,mn1p(i))&
                                     + RRc10*Int2000(d1,mnp1(i))
            end if
          end do ! i
        end do ! p2
      end do ! d1
!
      end subroutine calcRAM2110c
      subroutine calcRAM2110b(mv,nv,pv)   
!******************************************************!
!     [2110]^(m,n,p)                                   !
!******************************************************!
! b+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, d1, p1, p2, a_i
      integer :: dpp_index, dp_index, pp_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      dp_index = 0
      do d1 = 1,6
        do p2 = 1,3
          dp_index = dp_index + 1
          do i = 1,3 
            dpp_index = 9*(d1-1) + 3*(i-1) + p2
!
            a_i = Lvec_d(d1,i)
!
            Int2110(dpp_index,mnp) = RRb1(i)*Int2010(dp_index,mnp)     + RRb2(i)*Int2010(dp_index,m1np(i))&
                                   + RRb3(i)*Int2010(dp_index,mn1p(i)) + RRb4(i)*Int2010(dp_index,mnp1(i))
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3) = Lvec_d(d1,1:3)
              v_1i(i) = v_1i(i) - 1
              p1 = dot_product(v_1i,xyz)
              pp_index = 3*(p1-1) + p2
              Int2110(dpp_index,mnp) = Int2110(dpp_index,mnp)&
                          + dble(a_i)*(RRb5*Int1010(pp_index,mnp)     + RRb6*Int1010(pp_index,m1np(i))&
                                     + RRb7*Int1010(pp_index,mn1p(i)) + RRb8*Int1010(pp_index,mnp1(i)))
            end if
            if(i.eq.p2)then
              Int2110(dpp_index,mnp) = Int2110(dpp_index,mnp)&
                                     + RRb13*Int2000(d1,m1np(i)) + RRb14*Int2000(d1,mn1p(i))&
                                     + RRb15*Int2000(d1,mnp1(i))
            end if
          end do ! i
        end do ! p2
      end do ! d1
!
      end subroutine calcRAM2110b
      subroutine calcRAM2101b(mv,nv,pv)   
!******************************************************!
!     [2101]^(m,n,p)                                   !
!******************************************************!
! b+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, d1, p1, p2, dp_index, dpp_index, a_i
      integer :: pp_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      dp_index = 0
      dpp_index = 0
      do d1 = 1,6
        do p2 = 1,3 
          dp_index = dp_index + 1
          do i = 1,3
            dpp_index = 9*(d1-1) + 3*(i-1) + p2
!
            a_i = Lvec_d(d1,i)
!
            Int2101(dpp_index,mnp) = RRb1(i)*Int2001(dp_index,mnp)     + RRb2(i)*Int2001(dp_index,m1np(i))&
                                   + RRb3(i)*Int2001(dp_index,mn1p(i)) + RRb4(i)*Int2001(dp_index,mnp1(i))
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3) = Lvec_d(d1,1:3)
              v_1i(i) = v_1i(i) - 1
              p1 = dot_product(v_1i,xyz)
              pp_index = 3*(p1-1) + p2
              Int2101(dpp_index,mnp) = Int2101(dpp_index,mnp)&
                          + dble(a_i)*(RRb5*Int1001(pp_index,mnp)     + RRb6*Int1001(pp_index,m1np(i))&
                                     + RRb7*Int1001(pp_index,mn1p(i)) + RRb8*Int1001(pp_index,mnp1(i)))
            end if
            if(i.eq.p2)then
              Int2101(dpp_index,mnp) = Int2101(dpp_index,mnp)&
                                     + RRb16*Int2000(d1,m1np(i)) + RRb17*Int2000(d1,mn1p(i))&
                                     + RRb18*Int2000(d1,mnp1(i))
            end if
          end do ! i
        end do ! p2
      end do ! d1
!
      end subroutine calcRAM2101b
      subroutine calcRAM2101d(mv,nv,pv)   
!******************************************************!
!     [2101]^(m,n,p)                                   !
!******************************************************!
! d+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, d1, p1, p2, a_i
      integer :: dpp_index, dp_index, pp_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      dp_index = 0
      dpp_index = 0
      do d1 = 1,6
        do p2 = 1,3 
          dp_index = dp_index + 1
          do i = 1,3
            dpp_index = dpp_index + 1
!
            a_i = Lvec_d(d1,i)
!
            Int2101(dpp_index,mnp) = RRd1(i)*Int2100(dp_index,mnp)     + RRd2(i)*Int2100(dp_index,m1np(i))&
                                   + RRd3(i)*Int2100(dp_index,mn1p(i)) + RRd4(i)*Int2100(dp_index,mnp1(i))
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3) = Lvec_d(d1,1:3)
              v_1i(i) = v_1i(i) - 1
              p1 = dot_product(v_1i,xyz)
              pp_index = 3*(p1-1) + p2
              Int2101(dpp_index,mnp) = Int2101(dpp_index,mnp)&
                          + dble(a_i)*(RRd5*Int1100(pp_index,m1np(i))&
                                     + RRd6*Int1100(pp_index,mn1p(i)) + RRd7*Int1100(pp_index,mnp1(i)))
            end if
            if(i.eq.p2)then
              Int2101(dpp_index,mnp) = Int2101(dpp_index,mnp)&
                                     + RRd8*Int2000(d1,m1np(i)) + RRd9*Int2000(d1,mn1p(i))&
                                     + RRd10*Int2000(d1,mnp1(i))
            end if
          end do ! i
        end do ! p2
      end do ! d1
!
      end subroutine calcRAM2101d
      subroutine calcRAM2011(mv,nv,pv)   
!******************************************************!
!     [2011]^(m,n,p)                                   !
!******************************************************!
! d+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, d1, p1, p2, dp_index, dpp_index, a_i
      integer :: pp_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      dp_index = 0
      dpp_index = 0
      do d1 = 1,6
        do p2 = 1,3
          dp_index = dp_index + 1
          do i = 1,3 
            dpp_index = dpp_index + 1
!
            a_i = Lvec_d(d1,i)
!
            Int2011(dpp_index,mnp) = RRd1(i)*Int2010(dp_index,mnp)     + RRd2(i)*Int2010(dp_index,m1np(i))&
                                   + RRd3(i)*Int2010(dp_index,mn1p(i)) + RRd4(i)*Int2010(dp_index,mnp1(i))
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3) = Lvec_d(d1,1:3)
              v_1i(i) = v_1i(i) - 1
              p1 = dot_product(v_1i,xyz)
              pp_index = 3*(p1-1) + p2
              Int2011(dpp_index,mnp) = Int2011(dpp_index,mnp)&
                          + dble(a_i)*(RRd5*Int1010(pp_index,m1np(i))&
                                     + RRd6*Int1010(pp_index,mn1p(i)) + RRd7*Int1010(pp_index,mnp1(i)))
            end if
            if(i.eq.p2)then
              Int2011(dpp_index,mnp) = Int2011(dpp_index,mnp)&
                                     + RRd11*Int2000(d1,mnp)     + RRd12*Int2000(d1,m1np(i))&
                                     + RRd13*Int2000(d1,mn1p(i)) + RRd14*Int2000(d1,mnp1(i))
            end if
          end do ! i
        end do ! p2
      end do ! d1
!
      end subroutine calcRAM2011
      subroutine calcRAM0211(mv,nv,pv)   
!******************************************************!
!     [0211]^(m,n,p)                                   !
!******************************************************!
! d+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, d1, p1, p2, dp_index, dpp_index, b_i
      integer :: pp_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      dp_index = 0
      dpp_index = 0
      do d1 = 1,6
        do p2 = 1,3
          dp_index = dp_index + 1
          do i = 1,3 
            dpp_index = dpp_index + 1
!
            b_i = Lvec_d(d1,i)
!
            Int0211(dpp_index,mnp) = RRd1(i)*Int0210(dp_index,mnp)     + RRd2(i)*Int0210(dp_index,m1np(i))&
                                   + RRd3(i)*Int0210(dp_index,mn1p(i)) + RRd4(i)*Int0210(dp_index,mnp1(i))
            if(b_i.ne.0)then ! (b-1i) terms
              v_1i(1:3) = Lvec_d(d1,1:3)
              v_1i(i) = v_1i(i) - 1
              p1 = dot_product(v_1i,xyz)
              pp_index = 3*(p1-1) + p2
              Int0211(dpp_index,mnp) = Int0211(dpp_index,mnp)&
                          + dble(b_i)*(RRd8*Int0110(pp_index,m1np(i))&
                                     + RRd9*Int0110(pp_index,mn1p(i)) + RRd10*Int0110(pp_index,mnp1(i)))
            end if
            if(i.eq.p2)then
              Int0211(dpp_index,mnp) = Int0211(dpp_index,mnp)&
                                     + RRd11*Int0200(d1,mnp)     + RRd12*Int0200(d1,m1np(i))&
                                     + RRd13*Int0200(d1,mn1p(i)) + RRd14*Int0200(d1,mnp1(i))
            end if
          end do ! i
        end do ! p2
      end do ! d1
!
      end subroutine calcRAM0211
      subroutine calcRAM1210(mv,nv,pv)   
!******************************************************!
!     [1210]^(m,n,p)                                   !
!******************************************************!
! c+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, d2, p1, p2, pd_index, pdp_index, b_i
      integer :: pp_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      pdp_index = 0
      pd_index = 0
      do p1 = 1,3 
        do d2 = 1,6
          pd_index = pd_index + 1
          do i = 1,3
            pdp_index = pdp_index + 1
!
            b_i = Lvec_d(d2,i)
!
            Int1210(pdp_index,mnp) = RRc1(i)*Int1200(pd_index,mnp)     + RRc2(i)*Int1200(pd_index,m1np(i))&
                                   + RRc3(i)*Int1200(pd_index,mn1p(i)) + RRc4(i)*Int1200(pd_index,mnp1(i))
!
            if(i.eq.p1)then
              Int1210(pdp_index,mnp) = Int1210(pdp_index,mnp)&
                                     + RRc5*Int0200(d2,m1np(i))&
                                     + RRc6*Int0200(d2,mn1p(i)) + RRc7*Int0200(d2,mnp1(i))
            end if
            if(b_i.ne.0)then ! (b-1i) terms
              v_1i(1:3) = Lvec_d(d2,1:3)
              v_1i(i) = v_1i(i) - 1
              p2 = dot_product(v_1i,xyz)
              pp_index = 3*(p1-1) + p2
              Int1210(pdp_index,mnp) = Int1210(pdp_index,mnp)&
                          + dble(b_i)*(RRc8*Int1100(pp_index, m1np(i))&
                                     + RRc9*Int1100(pp_index,mn1p(i)) + RRc10*Int1100(pp_index,mnp1(i)))
            end if
          end do ! i
        end do ! d2
      end do ! p1
!
      end subroutine calcRAM1210
      subroutine calcRAM1201a(mv,nv,pv)   
!******************************************************!
!     [1201]^(m,n,p)                                   !
!******************************************************!
! a+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, d1, p1, p2, pdp_index, dp_index, b_i
      integer :: pp_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      pdp_index = 0
      do i = 1,3 
        dp_index = 0
        do d1 = 1,6
          do p2 = 1,3
            dp_index = dp_index + 1
            pdp_index = pdp_index + 1
!
            b_i = Lvec_d(d1,i)
!
            Int1201(pdp_index,mnp) = RRa1(i)*Int0201(dp_index,mnp)     + RRa2(i)*Int0201(dp_index,m1np(i))&
                                   + RRa3(i)*Int0201(dp_index,mn1p(i)) + RRa4(i)*Int0201(dp_index,mnp1(i))
            if(b_i.ne.0)then ! (b-1i) terms
              v_1i(1:3) = Lvec_d(d1,1:3)
              v_1i(i) = v_1i(i) - 1
              p1 = dot_product(v_1i,xyz)
              pp_index = 3*(p1-1) + p2
              Int1201(pdp_index,mnp) = Int1201(pdp_index,mnp)&
                          + dble(b_i)*(RRa9*Int0101(pp_index,mnp)      + RRa10*Int0101(pp_index,m1np(i))&
                                     + RRa11*Int0101(pp_index,mn1p(i)) + RRa12*Int0101(pp_index,mnp1(i)))
            end if
            if(i.eq.p2)then
              Int1201(pdp_index,mnp) = Int1201(pdp_index,mnp)&
                                     + RRa16*Int0200(d1,m1np(i))&
                                     + RRa17*Int0200(d1,mn1p(i)) + RRa18*Int0200(d1,mnp1(i))
            end if
          end do ! i
        end do ! p2
      end do ! d1
!
      end subroutine calcRAM1201a
      subroutine calcRAM1201d(mv,nv,pv)   
!******************************************************!
!     [1201]^(m,n,p)                                   !
!******************************************************!
! d+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, p1, d2, p2, b_i
      integer :: pdp_index, pd_index, pp_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      pdp_index = 0
      pd_index = 0
      do p1 = 1,3
        do d2 = 1,6 
          pd_index = pd_index + 1 
          do i = 1,3 
            pdp_index = pdp_index + 1
!
            b_i = Lvec_d(d2,i)
!
            Int1201(pdp_index,mnp) = RRd1(i)*Int1200(pd_index,mnp)     + RRd2(i)*Int1200(pd_index,m1np(i))&
                                   + RRd3(i)*Int1200(pd_index,mn1p(i)) + RRd4(i)*Int1200(pd_index,mnp1(i))
            if(i.eq.p1)then
              Int1201(pdp_index,mnp) = Int1201(pdp_index,mnp)&
                                     + RRd5*Int0200(d2,m1np(i))&
                                     + RRd6*Int0200(d2,mn1p(i)) + RRd7*Int0200(d2,mnp1(i))
            end if
            if(b_i.ne.0)then ! (b-1i) terms
              v_1i(1:3) = Lvec_d(d2,1:3)
              v_1i(i) = v_1i(i) - 1
              p2 = dot_product(v_1i,xyz)
              pp_index = 3*(p1-1) + p2
              Int1201(pdp_index,mnp) = Int1201(pdp_index,mnp)&
                          + dble(b_i)*(RRd8*Int1100(pp_index,m1np(i))&
                                     + RRd9*Int1100(pp_index,mn1p(i)) + RRd10*Int1100(pp_index,mnp1(i)))
            end if
          end do ! i
        end do ! d2
      end do ! p1
!
      end subroutine calcRAM1201d
      subroutine calcRAM1120(mv,nv,pv)   
!******************************************************!
!     [1120]^(m,n,p)                                   !
!******************************************************!
! b+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, p1, d2, p2, c_i
      integer :: ppd_index, pd_index, pp_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      ppd_index = 0
      do p1 = 1,3
        do i = 1,3
          do d2 = 1,6
            ppd_index = ppd_index + 1
            pd_index = 6*(p1-1) + d2
!
            c_i = Lvec_d(d2,i)
!
            Int1120(ppd_index,mnp) = RRb1(i)*Int1020(pd_index,mnp)     + RRb2(i)*Int1020(pd_index,m1np(i))&
                                   + RRb3(i)*Int1020(pd_index,mn1p(i)) + RRb4(i)*Int1020(pd_index,mnp1(i))
            if(i.eq.p1)then
              Int1120(ppd_index,mnp) = Int1120(ppd_index,mnp)&
                                     + RRb5*Int0020(d2,mnp)     + RRb6*Int0020(d2,m1np(i))&
                                     + RRb7*Int0020(d2,mn1p(i)) + RRb8*Int0020(d2,mnp1(i))
            end if
            if(c_i.ne.0)then ! (c-1i) terms
              v_1i(1:3) = Lvec_d(d2,1:3)
              v_1i(i) = v_1i(i) - 1
              p2 = dot_product(v_1i,xyz)
              pp_index = 3*(p1-1) + p2
              Int1120(ppd_index,mnp) = Int1120(ppd_index,mnp)&
                          + dble(c_i)*(RRb13*Int1010(pp_index,m1np(i))&
                                     + RRb14*Int1010(pp_index,mn1p(i)) + RRb15*Int1010(pp_index,mnp1(i)))
            end if
          end do ! d2
        end do ! i
      end do ! p1
!
      end subroutine calcRAM1120
      subroutine calcRAM1102b(mv,nv,pv)   
!******************************************************!
!     [1102]^(m,n,p)                                   !
!******************************************************!
! b+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, p1, d2, p2, d_i
      integer :: ppd_index, pd_index, pp_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      ppd_index = 0
      do p1 = 1,3
        do i = 1,3
          do d2 = 1,6
            ppd_index = ppd_index + 1
            pd_index = 6*(p1-1) + d2
!
            d_i = Lvec_d(d2,i)
!
            Int1102(ppd_index,mnp) = RRb1(i)*Int1002(pd_index,mnp)     + RRb2(i)*Int1002(pd_index,m1np(i))&
                                   + RRb3(i)*Int1002(pd_index,mn1p(i)) + RRb4(i)*Int1002(pd_index,mnp1(i))
            if(i.eq.p1)then
              Int1102(ppd_index,mnp) = Int1102(ppd_index,mnp)&
                                     + RRb5*Int0002(d2,mnp)     + RRb6*Int0002(d2,m1np(i))&
                                     + RRb7*Int0002(d2,mn1p(i)) + RRb8*Int0002(d2,mnp1(i))
            end if
            if(d_i.ne.0)then ! (d-1i) terms
              v_1i(1:3) = Lvec_d(d2,1:3)
              v_1i(i) = v_1i(i) - 1
              p2 = dot_product(v_1i,xyz)
              pp_index = 3*(p1-1) + p2
              Int1102(ppd_index,mnp) = Int1102(ppd_index,mnp)&
                          + dble(d_i)*(RRb16*Int1001(pp_index,m1np(i))&
                                     + RRb17*Int1001(pp_index,mn1p(i)) + RRb18*Int1001(pp_index,mnp1(i)))
            end if
          end do ! d2
        end do ! i
      end do ! p1
!
      end subroutine calcRAM1102b
      subroutine calcRAM1102a(mv,nv,pv)   
!******************************************************!
!     [1102]^(m,n,p)                                   !
!******************************************************!
! a+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, p1, d2, p2, d_i
      integer :: ppd_index, pd_index, pp_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      ppd_index = 0
      do i = 1,3
        pd_index = 0
        do p1 = 1,3
          do d2 = 1,6
            ppd_index = ppd_index + 1
            pd_index = pd_index + 1
!
            d_i = Lvec_d(d2,i)
!
            Int1102(ppd_index,mnp) = RRa1(i)*Int0102(pd_index,mnp)     + RRa2(i)*Int0102(pd_index,m1np(i))&
                                   + RRa3(i)*Int0102(pd_index,mn1p(i)) + RRa4(i)*Int0102(pd_index,mnp1(i))
            if(i.eq.p1)then
              Int1102(ppd_index,mnp) = Int1102(ppd_index,mnp)&
                                     + RRa9*Int0002(d2,mnp)      + RRa10*Int0002(d2,m1np(i))&
                                     + RRa11*Int0002(d2,mn1p(i)) + RRa12*Int0002(d2,mnp1(i))
            end if
            if(d_i.ne.0)then ! (d-1i) terms
              v_1i(1:3) = Lvec_d(d2,1:3)
              v_1i(i) = v_1i(i) - 1
              p2 = dot_product(v_1i,xyz)
              pp_index = 3*(p1-1) + p2
              Int1102(ppd_index,mnp) = Int1102(ppd_index,mnp)&
                          + dble(d_i)*(RRa16*Int0101(pp_index,m1np(i))&
                                     + RRa17*Int0101(pp_index,mn1p(i)) + RRa18*Int0101(pp_index,mnp1(i)))
            end if
          end do ! d2
        end do ! i
      end do ! p1
!
      end subroutine calcRAM1102a
      subroutine calcRAM2111(mv,nv,pv)   
!******************************************************!
!     [2111]^(m,n,p)                                   !
!******************************************************!
! d+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, d1, p1, p2, p3, a_i
      integer :: dp2_index, dp3_index, dpp_index, dppp_index, ppp_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      dppp_index = 0
      dpp_index = 0
      dp2_index = 0
      do d1 = 1,6 
        do p2 = 1,3
          dp2_index = dp2_index + 1
          do p3 = 1,3
            dpp_index = dpp_index + 1
            dp3_index = 3*(d1-1) + p3
            do i = 1,3
              dppp_index = dppp_index + 1
!
              a_i = Lvec_d(d1,i)
!
         Int2111(dppp_index,mnp) = RRd1(i)*Int2110(dpp_index,mnp)     + RRd2(i)*Int2110(dpp_index,m1np(i))&
                                 + RRd3(i)*Int2110(dpp_index,mn1p(i)) + RRd4(i)*Int2110(dpp_index,mnp1(i))
!
              if(a_i.ne.0)then
                v_1i(1:3) = Lvec_d(d1,1:3)
                v_1i(i) = v_1i(i) - 1 ! (a-1i)
                p1 = dot_product(v_1i,xyz)
                ppp_index = 9*(p1-1) + 3*(p2-1) + p3
                Int2111(dppp_index,mnp) = Int2111(dppp_index,mnp)&
                           + dble(a_i)*(RRd5*Int1110(ppp_index, m1np(i))&
                                      + RRd6*Int1110(ppp_index,mn1p(i)) + RRd7*Int1110(ppp_index,mnp1(i)))
              end if
              if(i.eq.p2)then
                Int2111(dppp_index,mnp) = Int2111(dppp_index,mnp)&
                                      + RRd8*Int2010(dp3_index,m1np(i))&
                                      + RRd9*Int2010(dp3_index,mn1p(i)) + RRd10*Int2010(dp3_index,mnp1(i))
              end if
              if(i.eq.p3)then
                Int2111(dppp_index,mnp) = Int2111(dppp_index,mnp)&
                                     + RRd11*Int2100(dp2_index,mnp)     + RRd12*Int2100(dp2_index,m1np(i))&
                                     + RRd13*Int2100(dp2_index,mn1p(i)) + RRd14*Int2100(dp2_index,mnp1(i))
              end if
            end do ! i
          end do ! p3
        end do ! p2
      end do ! d1
!
      end subroutine calcRAM2111
      subroutine calcRAM1211(mv,nv,pv)   
!******************************************************!
!     [1211]^(m,n,p)                                   !
!******************************************************!
! d+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, p1, d2, p2, p3, b_i
      integer :: pdpp_index, pdp_index, dp_index, ppp_index, pd_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      pdpp_index = 0
      pdp_index = 0
      pd_index = 0
      do p1 = 1,3
        dp_index = 0
        do d2 = 1,6 
          pd_index = pd_index + 1
          do p3 = 1,3
            pdp_index = pdp_index + 1
            dp_index = dp_index + 1
            do i = 1,3
              pdpp_index = pdpp_index + 1
!
              b_i = Lvec_d(d2,i)
!
         Int1211(pdpp_index,mnp) = RRd1(i)*Int1210(pdp_index,mnp)     + RRd2(i)*Int1210(pdp_index,m1np(i))&
                                 + RRd3(i)*Int1210(pdp_index,mn1p(i)) + RRd4(i)*Int1210(pdp_index,mnp1(i))
!
              if(i.eq.p1)then
                Int1211(pdpp_index,mnp) = Int1211(pdpp_index,mnp)&
                                      + RRd5*Int0210(dp_index,m1np(i))&
                                      + RRd6*Int0210(dp_index,mn1p(i)) + RRd7*Int0210(dp_index,mnp1(i))
              end if
              if(b_i.ne.0)then
                v_1i(1:3) = Lvec_d(d2,1:3)
                v_1i(i) = v_1i(i) - 1 ! (b-1i)
                p2 = dot_product(v_1i,xyz)
                ppp_index = 9*(p1-1) + 3*(p2-1) + p3
                Int1211(pdpp_index,mnp) = Int1211(pdpp_index,mnp)&
                          + dble(b_i)*(RRd8*Int1110(ppp_index,m1np(i))&
                                     + RRd9*Int1110(ppp_index,mn1p(i)) + RRd10*Int1110(ppp_index,mnp1(i)))
              end if
              if(i.eq.p3)then
                Int1211(pdpp_index,mnp) = Int1211(pdpp_index,mnp)&
                                     + RRd11*Int1200(pd_index,mnp)     + RRd12*Int1200(pd_index,m1np(i))&
                                     + RRd13*Int1200(pd_index,mn1p(i)) + RRd14*Int1200(pd_index,mnp1(i))
              end if
            end do ! i
          end do ! p3
        end do ! d2
      end do ! p1
!
      end subroutine calcRAM1211
      subroutine calcRAM2200(mv,nv,pv)   
!******************************************************!
!     [2200]^(m,n,p)                                   !
!******************************************************!
! b+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, d1, p1, p2, dp_index, dd_index, a_i
      integer :: pp_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      dd_index = 0
      do d1 = 1,6
        do p2 = 1,3
          i = p2 ! do xx, yy, zz first
          dd_index = dd_index + 1
          dp_index = 3*(d1-1) + p2
!
          a_i = Lvec_d(d1,i) ! angular momentum vector
!
          Int2200(dd_index,mnp) = RRb1(i)*Int2100(dp_index,mnp)     + RRb2(i)*Int2100(dp_index,m1np(i))&
                                + RRb3(i)*Int2100(dp_index,mn1p(i)) + RRb4(i)*Int2100(dp_index,mnp1(i))&
                                +  RRb9*Int2000(d1,mnp)     + RRb10*Int2000(d1,m1np(i))&
                                + RRb11*Int2000(d1,mn1p(i)) + RRb12*Int2000(d1,mnp1(i))
!
          if(a_i.ne.0)then ! (a-1i) terms
            v_1i(1:3) = Lvec_d(d1,1:3)
            v_1i(i) = v_1i(i) - 1 ! a-1_i
            p1 = dot_product(v_1i,xyz)
            pp_index = 3*(p1-1) + p2
            Int2200(dd_index,mnp) = Int2200(dd_index,mnp) &
                             + dble(a_i)*(RRb5*Int1100(pp_index,mnp)     + RRb6*Int1100(pp_index,m1np(i))&
                                        + RRb7*Int1100(pp_index,mn1p(i)) + RRb8*Int1100(pp_index,mnp1(i)))
          end if
        end do ! p2
!
        do p2 = 2,3 ! y, z
          dp_index = 3*(d1-1) + p2
          do i = 1,p2-1 ! x, y
            dd_index = dd_index + 1
!
! i ne p2, therefore no (b-1i) terms
!
            a_i = Lvec_d(d1,i) ! angular momentum vector
!
            Int2200(dd_index,mnp) = RRb1(i)*Int2100(dp_index,mnp)     + RRb2(i)*Int2100(dp_index,m1np(i))&
                                  + RRb3(i)*Int2100(dp_index,mn1p(i)) + RRb4(i)*Int2100(dp_index,mnp1(i))
!
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3) = Lvec_d(d1,1:3)
              v_1i(i) = v_1i(i) - 1 ! a-1_i
              p1 = dot_product(v_1i,xyz)
              pp_index = 3*(p1-1) + p2
              Int2200(dd_index,mnp) = Int2200(dd_index,mnp) &
                               + dble(a_i)*(RRb5*Int1100(pp_index,mnp)     + RRb6*Int1100(pp_index,m1np(i))&
                                          + RRb7*Int1100(pp_index,mn1p(i)) + RRb8*Int1100(pp_index,mnp1(i)))
            end if
          end do ! i
        end do ! p2
      end do ! d1
!
      end subroutine calcRAM2200
      subroutine calcRAM2020(mv,nv,pv)   
!******************************************************!
!     [2020]^(m,n,p)                                   !
!******************************************************!
! c+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, d1, p1, p2, dp_index, dd_index, a_i
      integer :: pp_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      dd_index = 0
      do d1 = 1,6
        do p2 = 1,3
          i = p2 ! do xx, yy, zz first
          dd_index = dd_index + 1
          dp_index = 3*(d1-1) + p2
!
          a_i = Lvec_d(d1,i) ! angular momentum vector
!
          Int2020(dd_index,mnp) = RRc1(i)*Int2010(dp_index,mnp)     + RRc2(i)*Int2010(dp_index,m1np(i))&
                                + RRc3(i)*Int2010(dp_index,mn1p(i)) + RRc4(i)*Int2010(dp_index,mnp1(i))&
                                + RRc11*Int2000(d1,mnp)     + RRc12*Int2000(d1,m1np(i))&
                                + RRc13*Int2000(d1,mn1p(i)) + RRc14*Int2000(d1,mnp1(i))
!
          if(a_i.ne.0)then ! (a-1i) terms
            v_1i(1:3) = Lvec_d(d1,1:3)
            v_1i(i) = v_1i(i) - 1 ! a-1_i
            p1 = dot_product(v_1i,xyz)
            pp_index = 3*(p1-1) + p2
            Int2020(dd_index,mnp) = Int2020(dd_index,mnp) &
                           + dble(a_i)*(RRc5*Int1010(pp_index,m1np(i))&
                                      + RRc6*Int1010(pp_index,mn1p(i)) + RRc7*Int1010(pp_index,mnp1(i)))
          end if
        end do ! p2
!
        do p2 = 2,3 ! y, z
          dp_index = 3*(d1-1) + p2
          do i = 1,p2-1 ! x, y
            dd_index = dd_index + 1
!
! i ne p2, therefore no (b-1i) terms
!
            a_i = Lvec_d(d1,i) ! angular momentum vector
!
          Int2020(dd_index,mnp) = RRc1(i)*Int2010(dp_index,mnp)     + RRc2(i)*Int2010(dp_index,m1np(i))&
                                + RRc3(i)*Int2010(dp_index,mn1p(i)) + RRc4(i)*Int2010(dp_index,mnp1(i))
!
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3) = Lvec_d(d1,1:3)
              v_1i(i) = v_1i(i) - 1 ! a-1_i
              p1 = dot_product(v_1i,xyz)
              pp_index = 3*(p1-1) + p2
              Int2020(dd_index,mnp) = Int2020(dd_index,mnp) &
                           + dble(a_i)*(RRc5*Int1010(pp_index,m1np(i))&
                                      + RRc6*Int1010(pp_index,mn1p(i)) + RRc7*Int1010(pp_index,mnp1(i)))
            end if
          end do ! i
        end do ! p2
      end do ! d1
!
      end subroutine calcRAM2020
      subroutine calcRAM0220(mv,nv,pv)   
!******************************************************!
!     [0220]^(m,n,p)                                   !
!******************************************************!
! c+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, d1, p1, p2, dp_index, dd_index, b_i
      integer :: pp_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      dd_index = 0
      do d1 = 1,6
        do p2 = 1,3
          i = p2 ! do xx, yy, zz first
          dd_index = dd_index + 1
          dp_index = 3*(d1-1) + p2
!
          b_i = Lvec_d(d1,i) ! angular momentum vector
!
          Int0220(dd_index,mnp) = RRc1(i)*Int0210(dp_index,mnp)     + RRc2(i)*Int0210(dp_index,m1np(i))&
                                + RRc3(i)*Int0210(dp_index,mn1p(i)) + RRc4(i)*Int0210(dp_index,mnp1(i))&
                                + RRc11*Int0200(d1,mnp)     + RRc12*Int0200(d1,m1np(i))&
                                + RRc13*Int0200(d1,mn1p(i)) + RRc14*Int0200(d1,mnp1(i))
!
          if(b_i.ne.0)then ! (b-1i) terms
            v_1i(1:3) = Lvec_d(d1,1:3)
            v_1i(i) = v_1i(i) - 1 ! b-1_i
            p1 = dot_product(v_1i,xyz)
            pp_index = 3*(p1-1) + p2
            Int0220(dd_index,mnp) = Int0220(dd_index,mnp) &
                             + dble(b_i)*(RRc8*Int0110(pp_index,m1np(i))&
                                        + RRc9*Int0110(pp_index,mn1p(i)) + RRc10*Int0110(pp_index,mnp1(i)))
          end if
        end do ! p2
!
        do p2 = 2,3 ! y, z
          dp_index = 3*(d1-1) + p2
          do i = 1,p2-1 ! x, y
            dd_index = dd_index + 1
!
! i ne p2, therefore no (b-1i) terms
!
            b_i = Lvec_d(d1,i) ! angular momentum vector
!
          Int0220(dd_index,mnp) = RRc1(i)*Int0210(dp_index,mnp)     + RRc2(i)*Int0210(dp_index,m1np(i))&
                                + RRc3(i)*Int0210(dp_index,mn1p(i)) + RRc4(i)*Int0210(dp_index,mnp1(i))
!
            if(b_i.ne.0)then ! (b-1i) terms
              v_1i(1:3) = Lvec_d(d1,1:3)
              v_1i(i) = v_1i(i) - 1 ! b-1_i
              p1 = dot_product(v_1i,xyz)
              pp_index = 3*(p1-1) + p2
            Int0220(dd_index,mnp) = Int0220(dd_index,mnp) &
                             + dble(b_i)*(RRc8*Int0110(pp_index,m1np(i))&
                                        + RRc9*Int0110(pp_index,mn1p(i)) + RRc10*Int0110(pp_index,mnp1(i)))
            end if
          end do ! i
        end do ! p2
      end do ! d1
!
      end subroutine calcRAM0220
      subroutine calcRAM2002(mv,nv,pv)   
!******************************************************!
!     [2002]^(m,n,p)                                   !
!******************************************************!
! d+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, d1, p1, p2, a_i
      integer :: dd_index, dp_index, pp_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      dd_index = 0
      do d1 = 1,6
        do p2 = 1,3
          i = p2 ! do xx, yy, zz first
          dd_index = dd_index + 1
          dp_index = 3*(d1-1) + p2
!
          a_i = Lvec_d(d1,i) ! angular momentum vector
!
          Int2002(dd_index,mnp) = RRd1(i)*Int2001(dp_index,mnp)     + RRd2(i)*Int2001(dp_index,m1np(i))&
                                + RRd3(i)*Int2001(dp_index,mn1p(i)) + RRd4(i)*Int2001(dp_index,mnp1(i))&
                                + RRd15*Int2000(d1,mnp)     + RRd16*Int2000(d1,m1np(i))&
                                + RRd17*Int2000(d1,mn1p(i)) + RRd18*Int2000(d1,mnp1(i))
!
          if(a_i.ne.0)then ! (a-1i) terms
            v_1i(1:3) = Lvec_d(d1,1:3)
            v_1i(i) = v_1i(i) - 1 ! a-1_i
            p1 = dot_product(v_1i,xyz)
            pp_index = 3*(p1-1) + p2
            Int2002(dd_index,mnp) = Int2002(dd_index,mnp) &
                             + dble(a_i)*(RRd5*Int1001(pp_index,m1np(i))&
                                        + RRd6*Int1001(pp_index,mn1p(i)) + RRd7*Int1001(pp_index,mnp1(i)))
          end if
        end do ! p2
!
        do p2 = 2,3 ! y, z
          dp_index = 3*(d1-1) + p2
          do i = 1,p2-1 ! x, y
            dd_index = dd_index + 1
!
! i ne p2, therefore no (b-1i) terms
!
            a_i = Lvec_d(d1,i) ! angular momentum vector
!
          Int2002(dd_index,mnp) = RRd1(i)*Int2001(dp_index,mnp)     + RRd2(i)*Int2001(dp_index,m1np(i))&
                                + RRd3(i)*Int2001(dp_index,mn1p(i)) + RRd4(i)*Int2001(dp_index,mnp1(i))
!
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3) = Lvec_d(d1,1:3)
              v_1i(i) = v_1i(i) - 1 ! a-1_i
              p1 = dot_product(v_1i,xyz)
              pp_index = 3*(p1-1) + p2
              Int2002(dd_index,mnp) = Int2002(dd_index,mnp) &
                               + dble(a_i)*(RRd5*Int1001(pp_index,m1np(i))&
                                          + RRd6*Int1001(pp_index,mn1p(i)) + RRd7*Int1001(pp_index,mnp1(i)))
            end if
          end do ! i
        end do ! p2
      end do ! d1
!
      end subroutine calcRAM2002
      subroutine calcRAM0202(mv,nv,pv)   
!******************************************************!
!     [0202]^(m,n,p)                                   !
!******************************************************!
! d+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, d1, p1, p2, dp_index, dd_index, b_i
      integer :: pp_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      dd_index = 0
      do d1 = 1,6
        do p2 = 1,3
          i = p2 ! do xx, yy, zz first
          dd_index = dd_index + 1
          dp_index = 3*(d1-1) + p2
!
          b_i = Lvec_d(d1,i) ! angular momentum vector
!
          Int0202(dd_index,mnp) = RRd1(i)*Int0201(dp_index,mnp)     + RRd2(i)*Int0201(dp_index,m1np(i))&
                                + RRd3(i)*Int0201(dp_index,mn1p(i)) + RRd4(i)*Int0201(dp_index,mnp1(i))&
                                + RRd15*Int0200(d1,mnp)     + RRd16*Int0200(d1,m1np(i))&
                                + RRd17*Int0200(d1,mn1p(i)) + RRd18*Int0200(d1,mnp1(i))
!
          if(b_i.ne.0)then ! (b-1i) terms
            v_1i(1:3) = Lvec_d(d1,1:3)
            v_1i(i) = v_1i(i) - 1 ! b-1_i
            p1 = dot_product(v_1i,xyz)
            pp_index = 3*(p1-1) + p2
            Int0202(dd_index,mnp) = Int0202(dd_index,mnp) &
                             + dble(b_i)*(RRd8*Int0101(pp_index,m1np(i))&
                                        + RRd9*Int0101(pp_index,mn1p(i)) + RRd10*Int0101(pp_index,mnp1(i)))
          end if
        end do ! p2
!
        do p2 = 2,3 ! y, z
          dp_index = 3*(d1-1) + p2
          do i = 1,p2-1 ! x, y
            dd_index = dd_index + 1
!
! i ne p2, therefore no (b-1i) terms
!
            b_i = Lvec_d(d1,i) ! angular momentum vector
!
            Int0202(dd_index,mnp) = RRd1(i)*Int0201(dp_index,mnp)     + RRd2(i)*Int0201(dp_index,m1np(i))&
                                  + RRd3(i)*Int0201(dp_index,mn1p(i)) + RRd4(i)*Int0201(dp_index,mnp1(i))
!
            if(b_i.ne.0)then ! (b-1i) terms
              v_1i(1:3) = Lvec_d(d1,1:3)
              v_1i(i) = v_1i(i) - 1 ! b-1_i
              p1 = dot_product(v_1i,xyz)
              pp_index = 3*(p1-1) + p2
              Int0202(dd_index,mnp) = Int0202(dd_index,mnp) &
                               + dble(b_i)*(RRd8*Int0101(pp_index,m1np(i))&
                                          + RRd9*Int0101(pp_index,mn1p(i)) + RRd10*Int0101(pp_index,mnp1(i)))
            end if
          end do ! i
        end do ! p2
      end do ! d1
!
      end subroutine calcRAM0202
      subroutine calcRAM2210(mv,nv,pv)   
!******************************************************!
!     [2210]^(m,n,p)                                   !
!******************************************************!
! c+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, d1, d2, p1, p2, dd_index, a_i, b_i
      integer :: ddp_index, dp_index, pd_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      ddp_index = 0
      dd_index = 0
      do d1 = 1,6
        do d2 = 1,6
          dd_index = dd_index + 1
          do i = 1,3
            ddp_index=ddp_index+1
!
            a_i = Lvec_d(d1,i)
            b_i = Lvec_d(d2,i)
!
            Int2210(ddp_index,mnp) = RRc1(i)*Int2200(dd_index,mnp)     + RRc2(i)*Int2200(dd_index,m1np(i))&
                                   + RRc3(i)*Int2200(dd_index,mn1p(i)) + RRc4(i)*Int2200(dd_index,mnp1(i))
!
            if(a_i.ne.0)then
              v_1i(1:3) = Lvec_d(d1,1:3)
              v_1i(i) = v_1i(i)-1
              p1 = dot_product(v_1i,xyz)
              pd_index = 6*(p1-1) + d2
              Int2210(ddp_index,mnp) = Int2210(ddp_index,mnp) &
                          + dble(a_i)*(RRc5*Int1200(pd_index,m1np(i)) + RRc6*Int1200(pd_index,mn1p(i))&
                                     + RRc7*Int1200(pd_index,mnp1(i)))
              end if
              if(b_i.ne.0)then
                v_1i(1:3) = Lvec_d(d2,1:3)
                v_1i(i) = v_1i(i)-1
                p2 = dot_product(v_1i,xyz)
                dp_index = 3*(d1-1) + p2
                Int2210(ddp_index,mnp) = Int2210(ddp_index,mnp) &
                          + dble(b_i)*(RRc8*Int2100(dp_index,m1np(i)) + RRc9*Int2100(dp_index,mn1p(i))&
                                     + RRc10*Int2100(dp_index,mnp1(i))) 
              end if
            end do ! i
          end do ! d2
        end do ! d1
!
      end subroutine calcRAM2210
      subroutine calcRAM2120(mv,nv,pv)   
!******************************************************!
!     [2120]^(m,n,p)                                   !
!******************************************************!
! b+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, d1, p1, d2, p2, a_i, c_i
      integer :: dpd_index, dd_index, pd_index, dp_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      dd_index = 0
      do d1 = 1,6
        do d2 = 1,6
          dd_index = dd_index + 1
          do i = 1,3
            dpd_index = 18*(d1-1) + 6*(i-1) + d2
!
            a_i = Lvec_d(d1,i)
            c_i = Lvec_d(d2,i)
!
            Int2120(dpd_index,mnp) = RRb1(i)*Int2020(dd_index,mnp)     + RRb2(i)*Int2020(dd_index,m1np(i))&
                                   + RRb3(i)*Int2020(dd_index,mn1p(i)) + RRb4(i)*Int2020(dd_index,mnp1(i))
!
            if(a_i.ne.0)then
              v_1i(1:3) = Lvec_d(d1,1:3)
              v_1i(i) = v_1i(i)-1
              p1 = dot_product(v_1i,xyz)
              pd_index = 6*(p1-1) + d2
              Int2120(dpd_index,mnp) = Int2120(dpd_index,mnp) &
                          + dble(a_i)*(RRb5*Int1020(pd_index,mnp)     + RRb6*Int1020(pd_index,m1np(i))&
                                     + RRb7*Int1020(pd_index,mn1p(i)) + RRb8*Int1020(pd_index,mnp1(i)))
              end if
              if(c_i.ne.0)then
                v_1i(1:3) = Lvec_d(d2,1:3)
                v_1i(i) = v_1i(i)-1
                p2 = dot_product(v_1i,xyz)
                dp_index = 3*(d1-1) + p2
                Int2120(dpd_index,mnp) = Int2120(dpd_index,mnp) &
                          + dble(c_i)*(RRb13*Int2010(dp_index,m1np(i)) + RRb14*Int2010(dp_index,mn1p(i))&
                                     + RRb15*Int2010(dp_index,mnp1(i))) 
              end if
            end do ! i
          end do ! d2
        end do ! d1
!
      end subroutine calcRAM2120
      subroutine calcRAM1220(mv,nv,pv)   
!******************************************************!
!     [1220]^(m,n,p)                                   !
!******************************************************!
! a+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, d1, d2, p1, p2, dd_index, b_i, c_i
      integer :: pdd_index, dp_index, pd_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      dd_index = 0
      do d1 = 1,6
        do d2 = 1,6
          dd_index = dd_index + 1
          do i = 1,3
            pdd_index = 36*(i-1) + 6*(d1-1) + d2
!
            b_i = Lvec_d(d1,i)
            c_i = Lvec_d(d2,i)
!
            Int1220(pdd_index,mnp) = RRa1(i)*Int0220(dd_index,mnp)     + RRa2(i)*Int0220(dd_index,m1np(i))&
                                   + RRa3(i)*Int0220(dd_index,mn1p(i)) + RRa4(i)*Int0220(dd_index,mnp1(i))
!
            if(b_i.ne.0)then
              v_1i(1:3) = Lvec_d(d1,1:3)
              v_1i(i) = v_1i(i)-1
              p1 = dot_product(v_1i,xyz)
              pd_index = 6*(p1-1) + d2
              Int1220(pdd_index,mnp) = Int1220(pdd_index,mnp) &
                          + dble(b_i)*(RRa9*Int0120(pd_index,mnp)      + RRa10*Int0120(pd_index,m1np(i))&
                                     + RRa11*Int0120(pd_index,mn1p(i)) + RRa12*Int0120(pd_index,mnp1(i)))
              end if
              if(c_i.ne.0)then
                v_1i(1:3) = Lvec_d(d2,1:3)
                v_1i(i) = v_1i(i)-1
                p2 = dot_product(v_1i,xyz)
                dp_index = 3*(d1-1) + p2
                Int1220(pdd_index,mnp) = Int1220(pdd_index,mnp) &
                          + dble(c_i)*(RRa13*Int0210(dp_index,m1np(i)) + RRa14*Int0210(dp_index,mn1p(i))&
                                     + RRa15*Int0210(dp_index,mnp1(i))) 
              end if
            end do ! i
          end do ! d2
        end do ! d1
!
      end subroutine calcRAM1220
      subroutine calcRAM2201(mv,nv,pv)   
!******************************************************!
!     [2201]^(m,n,p)                                   !
!******************************************************!
! d+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, d1, d2, p1, p2, dd_index, a_i, b_i
      integer :: ddp_index, dp_index, pd_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      ddp_index = 0
      dd_index = 0
      do d1 = 1,6
        do d2 = 1,6
          dd_index = dd_index + 1
          do i = 1,3
            ddp_index=ddp_index+1
!
            a_i = Lvec_d(d1,i)
            b_i = Lvec_d(d2,i)
!
            Int2201(ddp_index,mnp) = RRd1(i)*Int2200(dd_index,mnp)     + RRd2(i)*Int2200(dd_index,m1np(i))&
                                   + RRd3(i)*Int2200(dd_index,mn1p(i)) + RRd4(i)*Int2200(dd_index,mnp1(i))
!
            if(a_i.ne.0)then
              v_1i(1:3) = Lvec_d(d1,1:3)
              v_1i(i) = v_1i(i)-1
              p1 = dot_product(v_1i,xyz)
              pd_index = 6*(p1-1) + d2
              Int2201(ddp_index,mnp) = Int2201(ddp_index,mnp) &
                          + dble(a_i)*(RRd5*Int1200(pd_index,m1np(i)) + RRd6*Int1200(pd_index,mn1p(i))&
                                     + RRd7*Int1200(pd_index,mnp1(i)))
              end if
              if(b_i.ne.0)then
                v_1i(1:3) = Lvec_d(d2,1:3)
                v_1i(i) = v_1i(i)-1
                p2 = dot_product(v_1i,xyz)
                dp_index = 3*(d1-1) + p2
                Int2201(ddp_index,mnp) = Int2201(ddp_index,mnp) &
                          + dble(b_i)*(RRd8*Int2100(dp_index,m1np(i)) + RRd9*Int2100(dp_index,mn1p(i))&
                                     + RRd10*Int2100(dp_index,mnp1(i)))
              end if
            end do ! i
          end do ! d2
        end do ! d1
!
      end subroutine calcRAM2201
      subroutine calcRAM2102(mv,nv,pv)   
!******************************************************!
!     [2102]^(m,n,p)                                   !
!******************************************************!
! b+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, d1, d2, p1, p2, dd_index, a_i, d_i
      integer :: dpd_index, dp_index, pd_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      dd_index = 0
      do d1 = 1,6
        do d2 = 1,6
          dd_index = dd_index + 1
          do i = 1,3
            dpd_index = 18*(d1-1) + 6*(i-1) + d2
!
            a_i = Lvec_d(d1,i)
            d_i = Lvec_d(d2,i)
!
            Int2102(dpd_index,mnp) = RRb1(i)*Int2002(dd_index,mnp)     + RRb2(i)*Int2002(dd_index,m1np(i))&
                                   + RRb3(i)*Int2002(dd_index,mn1p(i)) + RRb4(i)*Int2002(dd_index,mnp1(i))
!
            if(a_i.ne.0)then
              v_1i(1:3) = Lvec_d(d1,1:3)
              v_1i(i) = v_1i(i)-1
              p1 = dot_product(v_1i,xyz)
              pd_index = 6*(p1-1) + d2
              Int2102(dpd_index,mnp) = Int2102(dpd_index,mnp) &
                          + dble(a_i)*(RRb5*Int1002(pd_index,mnp)     + RRb6*Int1002(pd_index,m1np(i))&
                                     + RRb7*Int1002(pd_index,mn1p(i)) + RRb8*Int1002(pd_index,mnp1(i)))
            end if
            if(d_i.ne.0)then
              v_1i(1:3) = Lvec_d(d2,1:3)
              v_1i(i) = v_1i(i)-1
              p2 = dot_product(v_1i,xyz)
              dp_index = 3*(d1-1) + p2
              Int2102(dpd_index,mnp) = Int2102(dpd_index,mnp) &
                        + dble(d_i)*(RRb16*Int2001(dp_index,m1np(i)) + RRb17*Int2001(dp_index,mn1p(i))&
                                   + RRb18*Int2001(dp_index,mnp1(i))) 
            end if
          end do ! i
        end do ! d2
      end do ! d1
!
      end subroutine calcRAM2102
      subroutine calcRAM2012(mv,nv,pv)   
!******************************************************!
!     [2012]^(m,n,p)                                   !
!******************************************************!
! c+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, d1, p1, d2, p2, a_i, d_i
      integer :: dpd_index, dd_index, pd_index, dp_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      dd_index = 0
      do d1 = 1,6
        do d2 = 1,6
          dd_index = dd_index + 1
          do i = 1,3
            dpd_index = 18*(d1-1) + 6*(i-1) + d2
!
            a_i = Lvec_d(d1,i)
            d_i = Lvec_d(d2,i)
!
            Int2012(dpd_index,mnp) = RRc1(i)*Int2002(dd_index,mnp)     + RRc2(i)*Int2002(dd_index,m1np(i))&
                                   + RRc3(i)*Int2002(dd_index,mn1p(i)) + RRc4(i)*Int2002(dd_index,mnp1(i))
!
            if(a_i.ne.0)then
              v_1i(1:3) = Lvec_d(d1,1:3)
              v_1i(i) = v_1i(i)-1
              p1 = dot_product(v_1i,xyz)
              pd_index = 6*(p1-1) + d2
              Int2012(dpd_index,mnp) = Int2012(dpd_index,mnp) &
                          + dble(a_i)*(RRc5*Int1002(pd_index,m1np(i))&
                                     + RRc6*Int1002(pd_index,mn1p(i)) + RRc7*Int1002(pd_index,mnp1(i)))
            end if
            if(d_i.ne.0)then
              v_1i(1:3) = Lvec_d(d2,1:3)
              v_1i(i) = v_1i(i)-1
              p2 = dot_product(v_1i,xyz)
              dp_index = 3*(d1-1) + p2
              Int2012(dpd_index,mnp) = Int2012(dpd_index,mnp) &
                          + dble(d_i)*(RRc15*Int2001(dp_index,mnp)     + RRc16*Int2001(dp_index,m1np(i))&
                                     + RRc17*Int2001(dp_index,mn1p(i)) + RRc18*Int2001(dp_index,mnp1(i))) 
            end if
!
          end do ! i
        end do ! d2
      end do ! d1
!
      end subroutine calcRAM2012
      subroutine calcRAM1202(mv,nv,pv)   
!******************************************************!
!     [1202]^(m,n,p)                                   !
!******************************************************!
! a+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, d1, p1, d2, p2, b_i, d_i
      integer :: pdd_index, dd_index, dp_index, pd_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      dd_index = 0
      do d1 = 1,6
        do d2 = 1,6
          dd_index = dd_index + 1
          do i = 1,3
            pdd_index = 36*(i-1) + 6*(d1-1) + d2
!
            b_i = Lvec_d(d1,i)
            d_i = Lvec_d(d2,i)
!
            Int1202(pdd_index,mnp) = RRa1(i)*Int0202(dd_index,mnp)     + RRa2(i)*Int0202(dd_index,m1np(i))&
                                   + RRa3(i)*Int0202(dd_index,mn1p(i)) + RRa4(i)*Int0202(dd_index,mnp1(i))
!
            if(b_i.ne.0)then
              v_1i(1:3) = Lvec_d(d1,1:3)
              v_1i(i) = v_1i(i)-1
              p1 = dot_product(v_1i,xyz)
              pd_index = 6*(p1-1) + d2
              Int1202(pdd_index,mnp) = Int1202(pdd_index,mnp) &
                          + dble(b_i)*(RRa9*Int0102(pd_index,mnp)      + RRa10*Int0102(pd_index,m1np(i))&
                                     + RRa11*Int0102(pd_index,mn1p(i)) + RRa12*Int0102(pd_index,mnp1(i)))
            end if
            if(d_i.ne.0)then
              v_1i(1:3) = Lvec_d(d2,1:3)
              v_1i(i) = v_1i(i)-1
              p2 = dot_product(v_1i,xyz)
              dp_index = 3*(d1-1) + p2
              Int1202(pdd_index,mnp) = Int1202(pdd_index,mnp) &
                          + dble(d_i)*(RRa16*Int0201(dp_index,m1np(i)) + RRa17*Int0201(dp_index,mn1p(i))&
                                     + RRa18*Int0201(dp_index,mnp1(i))) 
            end if
          end do ! i
        end do ! d2
      end do ! d1
!
      end subroutine calcRAM1202
      subroutine calcRAM2211(mv,nv,pv)   
!******************************************************!
!     [2211]^(m,n,p)                                   !
!******************************************************!
! d+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, d1, d2, p1, p2, p3, a_i, b_i
      integer :: ddpp_index, ddp_index, pdp_index, dpp_index, dd_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      ddpp_index = 0
      ddp_index = 0
      dd_index = 0
      do d1 = 1,6
        do d2 = 1,6
          dd_index = dd_index + 1
          do p3 = 1,3
            ddp_index = ddp_index + 1
            do i = 1,3
              ddpp_index = ddpp_index + 1
!
              a_i = Lvec_d(d1,i)
              b_i = Lvec_d(d2,i)
!
          Int2211(ddpp_index,mnp) = RRd1(i)*Int2210(ddp_index,mnp)     + RRd2(i)*Int2210(ddp_index,m1np(i))&
                                  + RRd3(i)*Int2210(ddp_index,mn1p(i)) + RRd4(i)*Int2210(ddp_index,mnp1(i))
!
              if(a_i.ne.0)then
                v_1i(1:3) = Lvec_d(d1,1:3)
                v_1i(i) = v_1i(i)-1
                p1 = dot_product(v_1i,xyz)
                pdp_index = 18*(p1-1) + 3*(d2-1) + p3
                Int2211(ddpp_index,mnp) = Int2211(ddpp_index,mnp) &
                             + dble(a_i)*(RRd5*Int1210(pdp_index,m1np(i))&
                                        + RRd6*Int1210(pdp_index,mn1p(i)) + RRd7*Int1210(pdp_index,mnp1(i)))
              end if
              if(b_i.ne.0)then
                v_1i(1:3) = Lvec_d(d2,1:3)
                v_1i(i) = v_1i(i)-1
                p2 = dot_product(v_1i,xyz)
                dpp_index = 9*(d1-1) + 3*(p2-1) + p3
                Int2211(ddpp_index,mnp) = Int2211(ddpp_index,mnp) &
                            + dble(b_i)*(RRd8*Int2110(dpp_index,m1np(i))&
                                       + RRd9*Int2110(dpp_index,mn1p(i)) + RRd10*Int2110(dpp_index,mnp1(i)))
              end if
              if(i.eq.p3)then
                Int2211(ddpp_index,mnp) = Int2211(ddpp_index,mnp) &
                                        + RRd11*Int2200(dd_index,mnp)     + RRd12*Int2200(dd_index,m1np(i))&
                                        + RRd13*Int2200(dd_index,mn1p(i)) + RRd14*Int2200(dd_index,mnp1(i))
              end if
            end do ! i
          end do ! p3
        end do ! d2
      end do ! d1
!
      end subroutine calcRAM2211
      subroutine calcRAM2121(mv,nv,pv)   
!******************************************************!
!     [2121]^(m,n,p)                                   !
!******************************************************!
! d+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, d1, p1, p2, d3, p3, a_i, c_i
      integer :: dpdp_index, dpd_index, ppd_index, dpp_index, dd_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      dpdp_index = 0
      dpd_index = 0
      do d1 = 1,6
        do p2 = 1,3
          do d3 = 1,6
            dpd_index = dpd_index + 1
            do i = 1,3
              dpdp_index = dpdp_index + 1
!
              a_i = Lvec_d(d1,i)
              c_i = Lvec_d(d3,i)
!
          Int2121(dpdp_index,mnp) = RRd1(i)*Int2120(dpd_index,mnp)     + RRd2(i)*Int2120(dpd_index,m1np(i))&
                                  + RRd3(i)*Int2120(dpd_index,mn1p(i)) + RRd4(i)*Int2120(dpd_index,mnp1(i))
!
              if(a_i.ne.0)then
                v_1i(1:3) = Lvec_d(d1,1:3)
                v_1i(i) = v_1i(i)-1
                p1 = dot_product(v_1i,xyz)
                ppd_index = 18*(p1-1) + 6*(p2-1) + d3
                Int2121(dpdp_index,mnp) = Int2121(dpdp_index,mnp) &
                             + dble(a_i)*(RRd5*Int1120(ppd_index,m1np(i))&
                                        + RRd6*Int1120(ppd_index,mn1p(i)) + RRd7*Int1120(ppd_index,mnp1(i)))
              end if
              if(i.eq.p2)then
                dd_index = 6*(d1-1) + d3
                Int2121(dpdp_index,mnp) = Int2121(dpdp_index,mnp) &
                                        + RRd8*Int2020(dd_index,m1np(i))&
                                        + RRd9*Int2020(dd_index,mn1p(i)) + RRd10*Int2020(dd_index,mnp1(i))
              end if
              if(c_i.ne.0)then
                v_1i(1:3) = Lvec_d(d3,1:3)
                v_1i(i) = v_1i(i)-1
                p3 = dot_product(v_1i,xyz)
                dpp_index = 9*(d1-1) + 3*(p2-1) + p3
                Int2121(dpdp_index,mnp) = Int2121(dpdp_index,mnp) &
                          + dble(c_i)*(RRd11*Int2110(dpp_index,mnp)     + RRd12*Int2110(dpp_index,m1np(i))&
                                     + RRd13*Int2110(dpp_index,mn1p(i)) + RRd14*Int2110(dpp_index,mnp1(i)))
              end if
            end do ! i
          end do ! d3
        end do ! p2
      end do ! d1
!
      end subroutine calcRAM2121
      subroutine calcRAM1221(mv,nv,pv)   
!******************************************************!
!     [1221]^(m,n,p)                                   !
!******************************************************!
! d+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, p1, d2, p2, d3, p3, b_i, c_i
      integer :: pddp_index, pdd_index, ppd_index, pdp_index, dd_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      pddp_index = 0
      pdd_index = 0
      do p1 = 1,3
        do d2 = 1,6
          do d3 = 1,6
            pdd_index = pdd_index + 1
            do i = 1,3
              pddp_index = pddp_index + 1
!
              b_i = Lvec_d(d2,i)
              c_i = Lvec_d(d3,i)
!
          Int1221(pddp_index,mnp) = RRd1(i)*Int1220(pdd_index,mnp)     + RRd2(i)*Int1220(pdd_index,m1np(i))&
                                  + RRd3(i)*Int1220(pdd_index,mn1p(i)) + RRd4(i)*Int1220(pdd_index,mnp1(i))
!
              if(i.eq.p1)then
                dd_index = 6*(d2-1) + d3
                Int1221(pddp_index,mnp) = Int1221(pddp_index,mnp) &
                                        + RRd5*Int0220(dd_index,m1np(i))&
                                        + RRd6*Int0220(dd_index,mn1p(i)) + RRd7*Int0220(dd_index,mnp1(i))
              end if
              if(b_i.ne.0)then
                v_1i(1:3) = Lvec_d(d2,1:3)
                v_1i(i) = v_1i(i)-1
                p2 = dot_product(v_1i,xyz)
                ppd_index = 18*(p1-1) + 6*(p2-1) + d3
                Int1221(pddp_index,mnp) = Int1221(pddp_index,mnp) &
                            + dble(b_i)*(RRd8*Int1120(ppd_index,m1np(i))&
                                       + RRd9*Int1120(ppd_index,mn1p(i)) + RRd10*Int1120(ppd_index,mnp1(i)))
              end if
              if(c_i.ne.0)then
                v_1i(1:3) = Lvec_d(d3,1:3)
                v_1i(i) = v_1i(i)-1
                p3 = dot_product(v_1i,xyz)
                pdp_index = 18*(p1-1) + 3*(d2-1) + p3
                Int1221(pddp_index,mnp) = Int1221(pddp_index,mnp) &
                          + dble(c_i)*(RRd11*Int1210(pdp_index,mnp)     + RRd12*Int1210(pdp_index,m1np(i))&
                                     + RRd13*Int1210(pdp_index,mn1p(i)) + RRd14*Int1210(pdp_index,mnp1(i)))
              end if
            end do ! i
          end do ! d3
        end do ! p2
      end do ! d1
!
      end subroutine calcRAM1221
      subroutine calcRAM2112(mv,nv,pv)   
!******************************************************!
!     [2112]^(m,n,p)                                   !
!******************************************************!
! c+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, d1, p1, p2, d3, p3, a_i, d_i
      integer :: dppd_index, dpd_index, ppd_index, dd_index, dpp_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      dppd_index = 0
      dpd_index = 0
      do d1 = 1,6
        do p2 = 1,3
          do d3 = 1,6
            dpd_index = dpd_index + 1
            do i = 1,3
              dppd_index = 54*(d1-1) + 18*(p2-1) + 6*(i-1) + d3
!
              a_i = Lvec_d(d1,i)
              d_i = Lvec_d(d3,i)
!
          Int2112(dppd_index,mnp) = RRc1(i)*Int2102(dpd_index,mnp)     + RRc2(i)*Int2102(dpd_index,m1np(i))&
                                  + RRc3(i)*Int2102(dpd_index,mn1p(i)) + RRc4(i)*Int2102(dpd_index,mnp1(i))
!
              if(a_i.ne.0)then
                v_1i(1:3) = Lvec_d(d1,1:3)
                v_1i(i) = v_1i(i)-1
                p1 = dot_product(v_1i,xyz)
                ppd_index = 18*(p1-1) + 6*(p2-1) + d3
                Int2112(dppd_index,mnp) = Int2112(dppd_index,mnp) &
                            + dble(a_i)*(RRc5*Int1102(ppd_index,m1np(i))&
                                       + RRc6*Int1102(ppd_index,mn1p(i)) + RRc7*Int1102(ppd_index,mnp1(i)))
              end if
              if(i.eq.p2)then
                dd_index = 6*(d1-1) + d3
                Int2112(dppd_index,mnp) = Int2112(dppd_index,mnp) &
                                        + RRc8*Int2002(dd_index,m1np(i))&
                                        + RRc9*Int2002(dd_index,mn1p(i)) + RRc10*Int2002(dd_index,mnp1(i))
              end if
              if(d_i.ne.0)then
                v_1i(1:3) = Lvec_d(d3,1:3)
                v_1i(i) = v_1i(i)-1
                p3 = dot_product(v_1i,xyz)
                dpp_index = 9*(d1-1) + 3*(p2-1) + p3
                Int2112(dppd_index,mnp) = Int2112(dppd_index,mnp) &
                          + dble(d_i)*(RRc15*Int2101(dpp_index,mnp)     + RRc16*Int2101(dpp_index,m1np(i))&
                                     + RRc17*Int2101(dpp_index,mn1p(i)) + RRc18*Int2101(dpp_index,mnp1(i)))
              end if
            end do ! i
          end do ! d3
        end do ! p2
      end do ! d1
!
      end subroutine calcRAM2112
      subroutine calcRAM1212(mv,nv,pv)   
!******************************************************!
!     [1212]^(m,n,p)                                   !
!******************************************************!
! c+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, p1, d2, p2, d3, p3, b_i, d_i
      integer :: pdpd_index, pdd_index, dd_index, ppd_index, pdp_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      pdd_index = 0  
      do p1 = 1,3
        do d2 = 1,6
          do d3 = 1,6
            pdd_index = pdd_index + 1
            do i = 1,3
              pdpd_index = 108*(p1-1) + 18*(d2-1) + 6*(i-1) + d3
!
              b_i = Lvec_d(d2,i)
              d_i = Lvec_d(d3,i)
!
          Int1212(pdpd_index,mnp) = RRc1(i)*Int1202(pdd_index,mnp)     + RRc2(i)*Int1202(pdd_index,m1np(i))&
                                  + RRc3(i)*Int1202(pdd_index,mn1p(i)) + RRc4(i)*Int1202(pdd_index,mnp1(i))
!
              if(i.eq.p1)then
                dd_index = 6*(d2-1) + d3
                Int1212(pdpd_index,mnp) = Int1212(pdpd_index,mnp) &
                                        + RRc5*Int0202(dd_index,m1np(i))&
                                        + RRc6*Int0202(dd_index,mn1p(i)) + RRc7*Int0202(dd_index,mnp1(i))
              end if
              if(b_i.ne.0)then
                v_1i(1:3) = Lvec_d(d2,1:3)
                v_1i(i) = v_1i(i)-1
                p2 = dot_product(v_1i,xyz)
                ppd_index = 18*(p1-1) + 6*(p2-1) + d3
                Int1212(pdpd_index,mnp) = Int1212(pdpd_index,mnp) &
                            + dble(b_i)*(RRc8*Int1102(ppd_index,m1np(i))&
                                       + RRc9*Int1102(ppd_index,mn1p(i)) + RRc10*Int1102(ppd_index,mnp1(i)))
              end if
              if(d_i.ne.0)then
                v_1i(1:3) = Lvec_d(d3,1:3)
                v_1i(i) = v_1i(i)-1
                p3 = dot_product(v_1i,xyz)
                pdp_index = 18*(p1-1) + 3*(d2-1) + p3
                Int1212(pdpd_index,mnp) = Int1212(pdpd_index,mnp) &
                          + dble(d_i)*(RRc15*Int1201(pdp_index,mnp)     + RRc16*Int1201(pdp_index,m1np(i))&
                                     + RRc17*Int1201(pdp_index,mn1p(i)) + RRc18*Int1201(pdp_index,mnp1(i)))
              end if
            end do ! i
          end do ! d3
        end do ! d2
      end do ! p1
!
      end subroutine calcRAM1212
      subroutine calcRAM2220(mv,nv,pv)   
!******************************************************!
!     [2220]^(m,n,p)                                   !
!******************************************************!
! c+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, d1, p1, d2, p2, p3, a_i, b_i
      integer :: ddd_index, ddp_index, pdp_index, dpp_index, dd_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      ddd_index = 0
      dd_index = 0
      do d1 = 1,6
        do d2 = 1,6
          dd_index = dd_index + 1
          do p3 = 1,3
            i = p3
            ddp_index = 18*(d1-1) + 3*(d2-1) + p3
            ddd_index = ddd_index + 1
!
            a_i = Lvec_d(d1,i)
            b_i = Lvec_d(d2,i)
!
          Int2220(ddd_index,mnp) = RRc1(i)*Int2210(ddp_index,mnp)     + RRc2(i)*Int2210(ddp_index,m1np(i))&
                                 + RRc3(i)*Int2210(ddp_index,mn1p(i)) + RRc4(i)*Int2210(ddp_index,mnp1(i))&
                                 + RRc11*Int2200(dd_index,mnp)        + RRc12*Int2200(dd_index,m1np(i))&
                                 + RRc13*Int2200(dd_index,mn1p(i))    + RRc14*Int2200(dd_index,mnp1(i))
!
            if(a_i.ne.0)then
              v_1i(1:3) = Lvec_d(d1,1:3)
              v_1i(i) = v_1i(i)-1
              p1 = dot_product(v_1i,xyz)
              pdp_index = 18*(p1-1) + 3*(d2-1) + p3
              Int2220(ddd_index,mnp) = Int2220(ddd_index,mnp) &
                          + dble(a_i)*(RRc5*Int1210(pdp_index,m1np(i))&
                                     + RRc6*Int1210(pdp_index,mn1p(i)) + RRc7*Int1210(pdp_index,mnp1(i)))
            end if
            if(b_i.ne.0)then
              v_1i(1:3) = Lvec_d(d2,1:3)
              v_1i(i) = v_1i(i)-1
              p2 = dot_product(v_1i,xyz)
              dpp_index = 9*(d1-1) + 3*(p2-1) + p3
              Int2220(ddd_index,mnp) = Int2220(ddd_index,mnp) &
                          + dble(b_i)*(RRc8*Int2110(dpp_index,m1np(i))&
                                     + RRc9*Int2110(dpp_index,mn1p(i)) + RRc10*Int2110(dpp_index,mnp1(i)))
            end if
          end do ! p3
          do p3 = 2,3
            ddp_index = 18*(d1-1) + 3*(d2-1) + p3
            do i = 1,p3-1
              ddd_index = ddd_index + 1
!
              a_i = Lvec_d(d1,i)
              b_i = Lvec_d(d2,i)
!
          Int2220(ddd_index,mnp) = RRc1(i)*Int2210(ddp_index,mnp)     + RRc2(i)*Int2210(ddp_index,m1np(i))&
                                 + RRc3(i)*Int2210(ddp_index,mn1p(i)) + RRc4(i)*Int2210(ddp_index,mnp1(i))
!
              if(a_i.ne.0)then
                v_1i(1:3) = Lvec_d(d1,1:3)
                v_1i(i) = v_1i(i)-1
                p1 = dot_product(v_1i,xyz)
                pdp_index = 18*(p1-1) + 3*(d2-1) + p3
                Int2220(ddd_index,mnp) = Int2220(ddd_index,mnp) &
                            + dble(a_i)*(RRc5*Int1210(pdp_index,m1np(i))&
                                       + RRc6*Int1210(pdp_index,mn1p(i)) + RRc7*Int1210(pdp_index,mnp1(i)))
              end if
              if(b_i.ne.0)then
                v_1i(1:3) = Lvec_d(d2,1:3)
                v_1i(i) = v_1i(i)-1
                p2 = dot_product(v_1i,xyz)
                dpp_index = 9*(d1-1) + 3*(p2-1) + p3
                Int2220(ddd_index,mnp) = Int2220(ddd_index,mnp) &
                            + dble(b_i)*(RRc8*Int2110(dpp_index,m1np(i))&
                                       + RRc9*Int2110(dpp_index,mn1p(i)) + RRc10*Int2110(dpp_index,mnp1(i)))
              end if
            end do ! i   
          end do ! p3
        end do ! d2 
      end do ! d1
!
      end subroutine calcRAM2220
      subroutine calcRAM2202(mv,nv,pv)   
!******************************************************!
!     [2202]^(m,n,p)                                   !
!******************************************************!
! d+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, d1, d2, p1, p2, p3, a_i, b_i
      integer :: ddd_index, ddp_index, pdp_index, dpp_index, dd_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      ddd_index = 0
      dd_index = 0
      do d1 = 1,6
        do d2 = 1,6
          dd_index = dd_index + 1
          do p3 = 1,3
            i = p3
            ddp_index = 18*(d1-1) + 3*(d2-1) + p3
            ddd_index = ddd_index + 1
!
            a_i = Lvec_d(d1,i)
            b_i = Lvec_d(d2,i)
!
          Int2202(ddd_index,mnp) = RRd1(i)*Int2201(ddp_index,mnp)     + RRd2(i)*Int2201(ddp_index,m1np(i))&
                                 + RRd3(i)*Int2201(ddp_index,mn1p(i)) + RRd4(i)*Int2201(ddp_index,mnp1(i))&
                                 + RRd15*Int2200(dd_index,mnp)     + RRd16*Int2200(dd_index,m1np(i))&
                                 + RRd17*Int2200(dd_index,mn1p(i)) + RRd18*Int2200(dd_index,mnp1(i))
!
            if(a_i.ne.0)then
              v_1i(1:3) = Lvec_d(d1,1:3)
              v_1i(i) = v_1i(i)-1
              p1 = dot_product(v_1i,xyz)
              pdp_index = 18*(p1-1) + 3*(d2-1) + p3
              Int2202(ddd_index,mnp) = Int2202(ddd_index,mnp) &
                          + dble(a_i)*(RRd5*Int1201(pdp_index,m1np(i))&
                                     + RRd6*Int1201(pdp_index,mn1p(i)) + RRd7*Int1201(pdp_index,mnp1(i)))
            end if
            if(b_i.ne.0)then
              v_1i(1:3) = Lvec_d(d2,1:3)
              v_1i(i) = v_1i(i)-1
              p2 = dot_product(v_1i,xyz)
              dpp_index = 9*(d1-1) + 3*(p2-1) + p3
              Int2202(ddd_index,mnp) = Int2202(ddd_index,mnp) &
                        + dble(b_i)*(RRd8*Int2101(dpp_index,m1np(i))&
                                   + RRd9*Int2101(dpp_index,mn1p(i)) + RRd10*Int2101(dpp_index,mnp1(i)))
            end if
          end do ! p3
          do p3 = 2,3
            ddp_index = 18*(d1-1) + 3*(d2-1) + p3
            do i = 1,p3-1
              ddd_index = ddd_index + 1
!
              a_i = Lvec_d(d1,i)
              b_i = Lvec_d(d2,i)
!
          Int2202(ddd_index,mnp) = RRd1(i)*Int2201(ddp_index,mnp)     + RRd2(i)*Int2201(ddp_index,m1np(i))&
                                 + RRd3(i)*Int2201(ddp_index,mn1p(i)) + RRd4(i)*Int2201(ddp_index,mnp1(i))
!
              if(a_i.ne.0)then
                v_1i(1:3) = Lvec_d(d1,1:3)
                v_1i(i) = v_1i(i)-1
                p1 = dot_product(v_1i,xyz)
                pdp_index = 18*(p1-1) + 3*(d2-1) + p3
                Int2202(ddd_index,mnp) = Int2202(ddd_index,mnp) &
                            + dble(a_i)*(RRd5*Int1201(pdp_index,m1np(i))&
                                       + RRd6*Int1201(pdp_index,mn1p(i)) + RRd7*Int1201(pdp_index,mnp1(i)))
              end if
              if(b_i.ne.0)then
                v_1i(1:3) = Lvec_d(d2,1:3)
                v_1i(i) = v_1i(i)-1
                p2 = dot_product(v_1i,xyz)
                dpp_index = 9*(d1-1) + 3*(p2-1) + p3
                Int2202(ddd_index,mnp) = Int2202(ddd_index,mnp) &
                          + dble(b_i)*(RRd8*Int2101(dpp_index,m1np(i))&
                                     + RRd9*Int2101(dpp_index,mn1p(i)) + RRd10*Int2101(dpp_index,mnp1(i)))
              end if
            end do ! i   
          end do ! p3
        end do ! d2 
      end do ! d1
!
      end subroutine calcRAM2202
      subroutine calcRAM2221(mv,nv,pv)   
!******************************************************!
!     [2221]^(m,n,p)                                   !
!******************************************************!
! d+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, d1, p1, d2, p2, d3, p3, a_i, b_i, c_i
      integer :: dddp_index, ddd_index, pdd_index, dpd_index, ddp_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      dddp_index = 0
      ddd_index = 0
      do d1 = 1,6
        do d2 = 1,6
          do d3 = 1,6
            ddd_index = ddd_index + 1
            do i = 1,3
              dddp_index = dddp_index + 1
!
              a_i = Lvec_d(d1,i)
              b_i = Lvec_d(d2,i)
              c_i = Lvec_d(d3,i)
!
          Int2221(dddp_index,mnp) = RRd1(i)*Int2220(ddd_index,mnp)     + RRd2(i)*Int2220(ddd_index,m1np(i))&
                                  + RRd3(i)*Int2220(ddd_index,mn1p(i)) + RRd4(i)*Int2220(ddd_index,mnp1(i))
!
              if(a_i.ne.0)then
                v_1i(1:3) = Lvec_d(d1,1:3)
                v_1i(i) = v_1i(i)-1
                p1 = dot_product(v_1i,xyz)
                pdd_index = 36*(p1-1) + 6*(d2-1) + d3
                Int2221(dddp_index,mnp) = Int2221(dddp_index,mnp) &
                             + dble(a_i)*(RRd5*Int1220(pdd_index,m1np(i))&
                                        + RRd6*Int1220(pdd_index,mn1p(i)) + RRd7*Int1220(pdd_index,mnp1(i)))
              end if
              if(b_i.ne.0)then
                v_1i(1:3) = Lvec_d(d2,1:3)
                v_1i(i) = v_1i(i)-1
                p2 = dot_product(v_1i,xyz)
                dpd_index = 18*(d1-1) + 6*(p2-1) + d3
                Int2221(dddp_index,mnp) = Int2221(dddp_index,mnp) &
                            + dble(b_i)*(RRd8*Int2120(dpd_index,m1np(i))&
                                       + RRd9*Int2120(dpd_index,mn1p(i)) + RRd10*Int2120(dpd_index,mnp1(i)))
              end if
              if(c_i.ne.0)then
                v_1i(1:3) = Lvec_d(d3,1:3)
                v_1i(i) = v_1i(i)-1
                p3 = dot_product(v_1i,xyz)
                ddp_index = 18*(d1-1) + 3*(d2-1) + p3
                Int2221(dddp_index,mnp) = Int2221(dddp_index,mnp) &
                           + dble(c_i)*(RRd11*Int2210(ddp_index,mnp)     + RRd12*Int2210(ddp_index,m1np(i))&
                                      + RRd13*Int2210(ddp_index,mn1p(i)) + RRd14*Int2210(ddp_index,mnp1(i)))
              end if
            end do ! i
          end do ! d3
        end do ! d2 
      end do ! d1
!
      end subroutine calcRAM2221
      subroutine calcRAM2212(mv,nv,pv)   
!******************************************************!
!     [2212]^(m,n,p)                                   !
!******************************************************!
! c+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, d1, p1, d2, p2, d3, p3, a_i, b_i, d_i
      integer :: ddpd_index, ddd_index, pdd_index, dpd_index, ddp_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      ddpd_index = 0
      ddd_index = 0
      do d1 = 1,6
        do d2 = 1,6
          do d3 = 1,6
            ddd_index = ddd_index + 1
            do i = 1,3
              ddpd_index = 108*(d1-1) + 18*(d2-1) + 6*(i-1) + d3
!
              a_i = Lvec_d(d1,i)
              b_i = Lvec_d(d2,i)
              d_i = Lvec_d(d3,i)
!
          Int2212(ddpd_index,mnp) = RRc1(i)*Int2202(ddd_index,mnp)     + RRc2(i)*Int2202(ddd_index,m1np(i))&
                                  + RRc3(i)*Int2202(ddd_index,mn1p(i)) + RRc4(i)*Int2202(ddd_index,mnp1(i))
!
              if(a_i.ne.0)then
                v_1i(1:3) = Lvec_d(d1,1:3)
                v_1i(i) = v_1i(i)-1
                p1 = dot_product(v_1i,xyz)
                pdd_index = 36*(p1-1) + 6*(d2-1) + d3
                Int2212(ddpd_index,mnp) = Int2212(ddpd_index,mnp) &
                             + dble(a_i)*(RRc5*Int1202(pdd_index,m1np(i))&
                                        + RRc6*Int1202(pdd_index,mn1p(i)) + RRc7*Int1202(pdd_index,mnp1(i)))
              end if
              if(b_i.ne.0)then
                v_1i(1:3) = Lvec_d(d2,1:3)
                v_1i(i) = v_1i(i)-1
                p2 = dot_product(v_1i,xyz)
                dpd_index = 18*(d1-1) + 6*(p2-1) + d3
                Int2212(ddpd_index,mnp) = Int2212(ddpd_index,mnp) &
                            + dble(b_i)*(RRc8*Int2102(dpd_index,m1np(i))&
                                       + RRc9*Int2102(dpd_index,mn1p(i)) + RRc10*Int2102(dpd_index,mnp1(i)))
              end if
              if(d_i.ne.0)then
                v_1i(1:3) = Lvec_d(d3,1:3)
                v_1i(i) = v_1i(i)-1
                p3 = dot_product(v_1i,xyz)
                ddp_index = 18*(d1-1) + 3*(d2-1) + p3
                Int2212(ddpd_index,mnp) = Int2212(ddpd_index,mnp) &
                           + dble(d_i)*(RRc15*Int2201(ddp_index,mnp)     + RRc16*Int2201(ddp_index,m1np(i))&
                                      + RRc17*Int2201(ddp_index,mn1p(i)) + RRc18*Int2201(ddp_index,mnp1(i)))
              end if
            end do ! i
          end do ! d3
        end do ! d2 
      end do ! d1
!
      end subroutine calcRAM2212
      subroutine calcRAM2222(mv,nv,pv)   
!******************************************************!
!     [2222]^(m,n,p)                                   !
!******************************************************!
! d+1i
!
      use RAM_RR
!      
      implicit none
!
      integer, dimension(3) :: mv, nv, pv, m1np, mn1p, mnp1
      integer :: mnp, i, d1, p1, d2, p2, d3, p3, p4, a_i, b_i, c_i
      integer :: dddd_index, dddp_index, pddp_index, dpdp_index, ddpp_index, ddd_index
      integer :: D3index
!
      mnp = D3index(mv,nv,pv)
      ! precalculate incremented mnp indices
      do i = 1,3 ! x,y,z
        m1np(i) = D3index(mv+imat(1:3,i),nv,pv)
        mn1p(i) = D3index(mv,nv+imat(1:3,i),pv)
        mnp1(i) = D3index(mv,nv,pv+imat(1:3,i))
      end do ! i
!
      dddd_index = 0
      ddd_index = 0
      do d1 = 1,6
        do d2 = 1,6
          do d3 = 1,6
            ddd_index = ddd_index + 1
            do p4 = 1,3
              i = p4
              dddp_index = 108*(d1-1) + 18*(d2-1) + 3*(d3-1) + p4
              dddd_index = dddd_index + 1
!
              a_i = Lvec_d(d1,i)
              b_i = Lvec_d(d2,i)
              c_i = Lvec_d(d3,i)
!
      Int2222(dddd_index,mnp) = RRd1(i)*Int2221(dddp_index,mnp)     + RRd2(i)*Int2221(dddp_index,m1np(i))&
                              + RRd3(i)*Int2221(dddp_index,mn1p(i)) + RRd4(i)*Int2221(dddp_index,mnp1(i))&
                              + RRd15*Int2220(ddd_index,mnp)     + RRd16*Int2220(ddd_index,m1np(i))&
                              + RRd17*Int2220(ddd_index,mn1p(i)) + RRd18*Int2220(ddd_index,mnp1(i))
!
              if(a_i.ne.0)then
                v_1i(1:3) = Lvec_d(d1,1:3)
                v_1i(i) = v_1i(i)-1
                p1 = dot_product(v_1i,xyz)
                pddp_index = 108*(p1-1) + 18*(d2-1) + 3*(d3-1) + p4
                Int2222(dddd_index,mnp) = Int2222(dddd_index,mnp) &
                           + dble(a_i)*(RRd5*Int1221(pddp_index,m1np(i))&
                                      + RRd6*Int1221(pddp_index,mn1p(i)) + RRd7*Int1221(pddp_index,mnp1(i)))
              end if
              if(b_i.ne.0)then
                v_1i(1:3) = Lvec_d(d2,1:3)
                v_1i(i) = v_1i(i)-1
                p2 = dot_product(v_1i,xyz)
                dpdp_index = 54*(d1-1) + 18*(p2-1) + 3*(d3-1) + p4
                Int2222(dddd_index,mnp) = Int2222(dddd_index,mnp) &
                          + dble(b_i)*(RRd8*Int2121(dpdp_index,m1np(i))&
                                     + RRd9*Int2121(dpdp_index,mn1p(i)) + RRd10*Int2121(dpdp_index,mnp1(i)))
              end if
              if(c_i.ne.0)then
                v_1i(1:3) = Lvec_d(d3,1:3)
                v_1i(i) = v_1i(i)-1
                p3 = dot_product(v_1i,xyz)
                ddpp_index = 54*(d1-1) + 9*(d2-1) + 3*(p3-1) + p4
                Int2222(dddd_index,mnp) = Int2222(dddd_index,mnp) &
                         + dble(c_i)*(RRd11*Int2211(ddpp_index,mnp)     + RRd12*Int2211(ddpp_index,m1np(i))&
                                    + RRd13*Int2211(ddpp_index,mn1p(i)) + RRd14*Int2211(ddpp_index,mnp1(i)))
              end if
            end do ! p4
!
            do p4 = 2,3
              dddp_index = 108*(d1-1) + 18*(d2-1) + 3*(d3-1) + p4
              do i = 1,p4-1
                dddd_index = dddd_index + 1
!
                a_i = Lvec_d(d1,i)
                b_i = Lvec_d(d2,i)
                c_i = Lvec_d(d3,i)
!
      Int2222(dddd_index,mnp) = RRd1(i)*Int2221(dddp_index,mnp)     + RRd2(i)*Int2221(dddp_index,m1np(i))&
                              + RRd3(i)*Int2221(dddp_index,mn1p(i)) + RRd4(i)*Int2221(dddp_index,mnp1(i))
!
                if(a_i.ne.0)then
                  v_1i(1:3) = Lvec_d(d1,1:3)
                  v_1i(i) = v_1i(i)-1
                  p1 = dot_product(v_1i,xyz)
                  pddp_index = 108*(p1-1) + 18*(d2-1) + 3*(d3-1) + p4
                  Int2222(dddd_index,mnp) = Int2222(dddd_index,mnp) &
                           + dble(a_i)*(RRd5*Int1221(pddp_index,m1np(i))&
                                      + RRd6*Int1221(pddp_index,mn1p(i)) + RRd7*Int1221(pddp_index,mnp1(i)))
                end if
                if(b_i.ne.0)then
                  v_1i(1:3) = Lvec_d(d2,1:3)
                  v_1i(i) = v_1i(i)-1
                  p2 = dot_product(v_1i,xyz)
                  dpdp_index = 54*(d1-1) + 18*(p2-1) + 3*(d3-1) + p4
                  Int2222(dddd_index,mnp) = Int2222(dddd_index,mnp) &
                          + dble(b_i)*(RRd8*Int2121(dpdp_index,m1np(i))&
                                     + RRd9*Int2121(dpdp_index,mn1p(i)) + RRd10*Int2121(dpdp_index,mnp1(i)))
                end if
                if(c_i.ne.0)then
                  v_1i(1:3) = Lvec_d(d3,1:3)
                  v_1i(i) = v_1i(i)-1
                  p3 = dot_product(v_1i,xyz)
                  ddpp_index = 54*(d1-1) + 9*(d2-1) + 3*(p3-1) + p4
                  Int2222(dddd_index,mnp) = Int2222(dddd_index,mnp) &
                         + dble(c_i)*(RRd11*Int2211(ddpp_index,mnp)     + RRd12*Int2211(ddpp_index,m1np(i))&
                                    + RRd13*Int2211(ddpp_index,mn1p(i)) + RRd14*Int2211(ddpp_index,mnp1(i)))
                end if
              end do ! i
            end do ! p4
          end do ! d3
        end do ! d2 
      end do ! d1
!
      end subroutine calcRAM2222
!******************************************************!
! Indexing functions                                   !
!******************************************************!
!
      function Dindex(m)
      ! indexing for 3D vector with 2 options in each dimension (0 and 1)

      integer    :: Dindex
      integer, dimension(3) :: m 
!
      Dindex = 2**2*m(1) + 2*m(2) + m(3) + 1
!
      end function Dindex
!
      function D3index(m,n,p)
      ! indexing for 3 3D vectors with 2 options in each dimension (0 and 1)

      integer, dimension(3) :: m,n,p 
      integer    :: m_ind, n_ind, p_ind
      integer    :: Dindex
      integer    :: D3index
!
      if((sum(m)+sum(n)+sum(p).gt.2).or.(maxval(m).gt.1).or.(maxval(n).gt.1).or.(maxval(p).gt.1))then
        D3index = 513
      else
        m_ind = Dindex(m)
        n_ind = Dindex(n)
        p_ind = Dindex(p)
        D3index = 8**2*(m_ind-1) + 8*(n_ind-1) + p_ind
      end if 
!
      end function D3index
!
