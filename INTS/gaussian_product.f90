      subroutine Gaussian_products (rx, ry, rz, AOprod, MATlen)
!****************************************************************************************
!     Date last modified: December 2, 2012                                              *
!     Authors: Ahmad and R.A. Poirier                                                   *
!     Description: Compute the product of all the basis functions and return the values *
!     in the array AO_prod.  No need to used the Gaussian product theorem, just find    *
!     g_s(rx,ry,rz)*g_t(rx,ry,rz)                                                       *
!****************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE type_basis_set
      USE matrix_print
      USE QM_defaults

      implicit none
!
! Input scalars:
      integer :: MATlen
      double precision :: AOprod(MATlen)
!
! Local scalars:
      integer :: Ishell,Jshell,Ifrst,Jfrst,Ilast,Jlast
      integer :: Istart,Jstart,Iend,Jend
      integer :: Iatmshl,Jatmshl
      integer :: LAMAX,LBMAX
      integer :: Iatom,Jatom
      integer :: Irange,Jrange
      integer :: Igauss,Jgauss
      integer :: IGBGN,JGBGN,IGend,JGend
      integer :: Iao,Jao,AOI,AOJ
      integer :: I,J,K,INDEX,DEFCASE,IJao
      double precision :: COEF_IJ,COEF_SXY,COEF_PXY
      double precision :: COEF_XXXY,COEF_XYXY,SQRT3,SUMDSQ,Gama,Alpha,Beta,RABSQ,EG,EG_K,EG_exp
      double precision :: Ax,Bx,Ay,By,Az,Bz,rx,ry,rz,Px,Py,Pz,xA,yA,zA,xB,yB,zB,rpx,rpy,rpz
      double precision :: PAx,PAy,PAz,PBx,PBy,PBz
      logical :: LIatmshl
!
! Local arrays
      double precision :: SHLINTS(100)
!
! Begin:
      SQRT3=DSQRT(THREE)
      AOprod(1:size(AOprod))=ZERO
!
! Begin loop over shells.
!
! Loop over elemental SHELLs
! Loop over Ishell.
      do Ishell=1,Basis%Nshells
        LAMAX=Basis%shell(Ishell)%Xtype+1
        Istart=Basis%shell(Ishell)%Xstart
        Iend=Basis%shell(Ishell)%XEND
        Irange=Iend-Istart+1
        IGBGN=Basis%shell(Ishell)%EXPBGN
        IGEND=Basis%shell(Ishell)%EXPEND
        Ifrst=Basis%shell(Ishell)%frstSHL
!
! Loop over Jshell.
      do Jshell=1,Ishell
        LBMAX=Basis%shell(Jshell)%Xtype+1
        Jstart=Basis%shell(Jshell)%Xstart
        Jend=Basis%shell(Jshell)%XEND
        Jrange=Jend-Jstart+1
        JGBGN=Basis%shell(Jshell)%EXPBGN
        JGEND=Basis%shell(Jshell)%EXPEND
        Jfrst=Basis%shell(Jshell)%frstSHL
!
! Define CASE:
      if(LAMAX.lt.LBMAX)then
       DEFCASE=4*(LAMAX-1)+LBMAX
      else
       DEFCASE=4*(LBMAX-1)+LAMAX
      end if
!
      Ilast= Basis%shell(Ishell)%lastSHL
      Jlast= Basis%shell(Jshell)%lastSHL
!
      call AB_products
!
! End of loop over shells.
      end do ! Jshell
      end do ! Ishell
!
! End of routine AO_PRODUCTS
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine AB_products
!****************************************************************************************
!     Date last modified: December 2, 2015                                              *
!     Authors: Ahmad and R.A. Poirier                                                   *
!     Description: Loop over gaussians and compute the product                          *
!****************************************************************************************
! Modules:

      implicit none
!
! Loop over all atomic shells
      do Iatmshl=Ifrst,Ilast
        Iatom=Basis%atmshl(Iatmshl)%ATMLST
!
        Ax=CARTESIAN(Iatom)%X     ! vector A={Ax,Ay,Az}     
        Ay=CARTESIAN(Iatom)%Y
        Az=CARTESIAN(Iatom)%Z
!
        AOI=Basis%atmshl(Iatmshl)%frstAO-1
        IF(Ishell.EQ.Jshell)Jlast=Iatmshl
      do Jatmshl=Jfrst,Jlast
        Jatom=Basis%atmshl(Jatmshl)%ATMLST
!
        Bx=CARTESIAN(Jatom)%X     ! vector B={Bx,By,Bz}
        By=CARTESIAN(Jatom)%Y
        Bz=CARTESIAN(Jatom)%Z
!
        AOJ=Basis%atmshl(Jatmshl)%frstAO-1
        LIatmshl=.false.
        if(Iatmshl.eq.Jatmshl)LIatmshl=.true.

        RABSQ=(Ax-Bx)*(Ax-Bx)+(Ay-By)*(Ay-By)+(Az-Bz)*(Az-Bz)     ! (A-B)**2

        SHLINTS(1:100)=ZERO ! will handle up to <f|f>
! Loop over primitive gaussians
!
      do Igauss=IGBGN,IGEND
        Alpha=Basis%gaussian(Igauss)%exp

      do Jgauss=JGBGN,JGEND
        Beta=Basis%gaussian(Jgauss)%exp
!
        Gama=Alpha+Beta

        Px=(Alpha*Ax+Beta*Bx)/Gama     ! vector P
        Py=(Alpha*Ay+Beta*By)/Gama
        Pz=(Alpha*Az+Beta*Bz)/Gama
!
! rpx=Xp in equations
        rpx = rx-Px
        rpy = ry-Py
        rpz = rz-Pz

        SUMDSQ = rpx*rpx + rpy*rpy + rpz*rpz   ! (rp)**2

        EG_K=DEXP(-Alpha*Beta*RABSQ/Gama)      ! K 
        EG_exp=DEXP(-Gama*SUMDSQ)              ! exp
        EG=EG_K*EG_exp                         ! K*exp

        xA=rx-Ax     ! vector rA=(r-A)
        yA=ry-Ay
        zA=rz-Az
        xB=rx-Bx     ! vector rB=(r-B)
        yB=ry-By
        zB=rz-Bz
!
     COEF_IJ = Basis%gaussian(Igauss)%CONTRC*Basis%gaussian(Jgauss)%CONTRC
     select case (DEFCASE)
     case (1) ! S|S
        SHLINTS(1) = SHLINTS(1)+COEF_IJ*EG
     case (2) ! S|P
        SHLINTS(1)=SHLINTS(1)+xB*COEF_IJ*EG
        SHLINTS(2)=SHLINTS(2)+yB*COEF_IJ*EG
        SHLINTS(3)=SHLINTS(3)+zB*COEF_IJ*EG

     case (3) ! S|D
        COEF_SXY = SQRT3*COEF_IJ
        SHLINTS(1)=SHLINTS(1)+xB*xB*COEF_IJ*EG
        SHLINTS(2)=SHLINTS(2)+yB*yB*COEF_IJ*EG
        SHLINTS(3)=SHLINTS(3)+zB*zB*COEF_IJ*EG
        SHLINTS(4)=SHLINTS(4)+xB*yB*COEF_SXY*EG
        SHLINTS(5)=SHLINTS(5)+xB*zB*COEF_SXY*EG
        SHLINTS(6)=SHLINTS(6)+yB*zB*COEF_SXY*EG

     case (6) ! P|P
        SHLINTS(1)=SHLINTS(1)+xA*xB*COEF_IJ*EG
        SHLINTS(2)=SHLINTS(2)+xA*yB*COEF_IJ*EG
        SHLINTS(3)=SHLINTS(3)+xA*zB*COEF_IJ*EG
        SHLINTS(4)=SHLINTS(4)+yA*xB*COEF_IJ*EG
        SHLINTS(5)=SHLINTS(5)+yA*yB*COEF_IJ*EG
        SHLINTS(6)=SHLINTS(6)+yA*zB*COEF_IJ*EG
        SHLINTS(7)=SHLINTS(7)+zA*xB*COEF_IJ*EG
        SHLINTS(8)=SHLINTS(8)+zA*yB*COEF_IJ*EG
        SHLINTS(9)=SHLINTS(9)+zA*zB*COEF_IJ*EG
     case (7) ! P|D
        COEF_PXY = SQRT3*COEF_IJ
        SHLINTS(1)=SHLINTS(1)+xA*xB*xB*COEF_IJ*EG   !  X|XX
        SHLINTS(2)=SHLINTS(2)+xA*yB*yB*COEF_IJ*EG   !  X|YY
        SHLINTS(3)=SHLINTS(3)+xA*zB*zB*COEF_IJ*EG   !  X|ZZ
        SHLINTS(4)=SHLINTS(4)+xA*xB*yB*COEF_PXY*EG   !  X|XY
        SHLINTS(5)=SHLINTS(5)+xA*xB*zB*COEF_PXY*EG   !  X|XZ
        SHLINTS(6)=SHLINTS(6)+xA*yB*zB*COEF_PXY*EG   !  X|YZ

        SHLINTS(7)=SHLINTS(7)+yA*xB*xB*COEF_IJ*EG   !  Y|XX
        SHLINTS(8)=SHLINTS(8)+yA*yB*yB*COEF_IJ*EG   !  Y|YY
        SHLINTS(9)=SHLINTS(9)+yA*zB*zB*COEF_IJ*EG   !  Y|ZZ
        SHLINTS(10)=SHLINTS(10)+yA*xB*yB*COEF_PXY*EG !  Y|XY
        SHLINTS(11)=SHLINTS(11)+yA*xB*zB*COEF_PXY*EG !  Y|XZ
        SHLINTS(12)=SHLINTS(12)+yA*yB*zB*COEF_PXY*EG !  Y|YZ

        SHLINTS(13)=SHLINTS(13)+zA*xB*xB*COEF_IJ*EG !  Z|XX
        SHLINTS(14)=SHLINTS(14)+zA*yB*yB*COEF_IJ*EG !  Z|YY
        SHLINTS(15)=SHLINTS(15)+zA*zB*zB*COEF_IJ*EG !  Z|ZZ
        SHLINTS(16)=SHLINTS(16)+zA*xB*yB*COEF_PXY*EG !  Z|XY
        SHLINTS(17)=SHLINTS(17)+zA*xB*zB*COEF_PXY*EG !  Z|XZ
        SHLINTS(18)=SHLINTS(18)+zA*yB*zB*COEF_PXY*EG !  Z|YZ
     case (11) ! D|D
        COEF_XXXY = SQRT3*COEF_IJ
        COEF_XYXY = THREE*COEF_IJ
        SHLINTS(1)=SHLINTS(1)+xA*xA*xB*xB*COEF_IJ*EG   ! XX|XX
        SHLINTS(2)=SHLINTS(2)+xA*xA*yB*yB*COEF_IJ*EG   ! XX|YY
        SHLINTS(3)=SHLINTS(3)+xA*xA*zB*zB*COEF_IJ*EG   ! XX|ZZ
        SHLINTS(4)=SHLINTS(4)+xA*xA*xB*yB*COEF_XXXY*EG   ! XX|XY
        SHLINTS(5)=SHLINTS(5)+xA*xA*xB*zB*COEF_XXXY*EG   ! XX|XZ
        SHLINTS(6)=SHLINTS(6)+xA*xA*yB*zB*COEF_XXXY*EG   ! XX|YZ

        SHLINTS(7)=SHLINTS(7)+yA*yA*xB*xB*COEF_IJ*EG   ! YY|XX
        SHLINTS(8)=SHLINTS(8)+yA*yA*yB*yB*COEF_IJ*EG   ! YY|YY
        SHLINTS(9)=SHLINTS(9)+yA*yA*zB*zB*COEF_IJ*EG   ! YY|ZZ
        SHLINTS(10)=SHLINTS(10)+yA*yA*xB*yB*COEF_XXXY*EG ! YY|XY
        SHLINTS(11)=SHLINTS(11)+yA*yA*xB*zB*COEF_XXXY*EG ! YY|XZ
        SHLINTS(12)=SHLINTS(12)+yA*yA*yB*zB*COEF_XXXY*EG ! YY|YZ

        SHLINTS(13)=SHLINTS(13)+zA*zA*xB*xB*COEF_IJ*EG ! ZZ|XX
        SHLINTS(14)=SHLINTS(14)+zA*zA*yB*yB*COEF_IJ*EG ! ZZ|YY
        SHLINTS(15)=SHLINTS(15)+zA*zA*zB*zB*COEF_IJ*EG ! ZZ|ZZ
        SHLINTS(16)=SHLINTS(16)+zA*zA*xB*yB*COEF_XXXY*EG ! ZZ|XY
        SHLINTS(17)=SHLINTS(17)+zA*zA*xB*zB*COEF_XXXY*EG ! ZZ|XZ
        SHLINTS(18)=SHLINTS(18)+zA*zA*yB*zB*COEF_XXXY*EG ! ZZ|YZ

        SHLINTS(19)=SHLINTS(19)+xA*yA*xB*xB*COEF_XXXY*EG ! XY|XX
        SHLINTS(20)=SHLINTS(20)+xA*yA*yB*yB*COEF_XXXY*EG ! XY|YY
        SHLINTS(21)=SHLINTS(21)+xA*yA*zB*zB*COEF_XXXY*EG ! XY|ZZ
        SHLINTS(22)=SHLINTS(22)+xA*yA*xB*yB*COEF_XYXY*EG ! XY|XY
        SHLINTS(23)=SHLINTS(23)+xA*yA*xB*zB*COEF_XYXY*EG ! XY|XZ
        SHLINTS(24)=SHLINTS(24)+xA*yA*yB*zB*COEF_XYXY*EG ! XY|YZ

        SHLINTS(25)=SHLINTS(25)+xA*zA*xB*xB*COEF_XXXY*EG ! XZ|XX
        SHLINTS(26)=SHLINTS(26)+xA*zA*yB*yB*COEF_XXXY*EG ! XZ|YY
        SHLINTS(27)=SHLINTS(27)+xA*zA*zB*zB*COEF_XXXY*EG ! XZ|ZZ
        SHLINTS(28)=SHLINTS(28)+xA*zA*xB*yB*COEF_XYXY*EG ! XZ|XY
        SHLINTS(29)=SHLINTS(29)+xA*zA*xB*zB*COEF_XYXY*EG ! XZ|XZ
        SHLINTS(30)=SHLINTS(30)+xA*zA*yB*zB*COEF_XYXY*EG ! XZ|YZ

        SHLINTS(31)=SHLINTS(31)+yA*zA*xB*xB*COEF_XXXY*EG ! YZ|XX
        SHLINTS(32)=SHLINTS(32)+yA*zA*yB*yB*COEF_XXXY*EG ! YZ|YY
        SHLINTS(33)=SHLINTS(33)+yA*zA*zB*zB*COEF_XXXY*EG ! YZ|ZZ
        SHLINTS(34)=SHLINTS(34)+yA*zA*xB*yB*COEF_XYXY*EG ! YZ|XY
        SHLINTS(35)=SHLINTS(35)+yA*zA*xB*zB*COEF_XYXY*EG ! YZ|XZ
        SHLINTS(36)=SHLINTS(36)+yA*zA*yB*zB*COEF_XYXY*EG ! YZ|YZ

     case default
       write(UNIout,*)'ERROR> AO_PRODUCTS: DEFCASE type should not exist'
       stop ' ERROR> AO_PRODUCTS: DEFCASE type should not exist'
     end select
!
      end do ! Jgauss
      end do ! Igauss 
      
      IF(LIatmshl)then
        INDEX=0
        do I=1,Irange
        do J=1,Jrange
          INDEX=INDEX+1
          IF(I.GE.J)then  ! Corrected August 1,2003 had (J.ge.I)
            Iao=AOI+I
            Jao=AOJ+J
            IJao=Iao*(Iao-1)/2+Jao
            AOprod(IJao)=SHLINTS(INDEX)
          end if
        end do ! J   
        end do ! I
      else
        INDEX=0
        do I=1,Irange
        do J=1,Jrange
          INDEX=INDEX+1
          Iao=AOI+I
          Jao=AOJ+J
          IJao=Iao*(Iao-1)/2+Jao
          AOprod(IJao)=SHLINTS(INDEX)
        end do ! J
        end do ! I
      end if ! LIatmshl

      end do ! Jatmshl
      end do ! Iatmshl

      return
      end subroutine AB_products
! END CONTAINS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine Gaussian_products
