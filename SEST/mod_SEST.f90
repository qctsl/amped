      module SEST_tools
!***********************************************************************
!     Date last modified: October  29, 2007                            *
!     Author: Joshua Hollett                                           *
!     Description: Scalars and arrays used in the SEST code.            *
!***********************************************************************
!
      implicit none
!
      type bond_definition
        character(len=10) :: type
        double precision :: distance
        integer :: connect
      end type bond_definition
!
      type(bond_definition), allocatable, dimension(:,:) :: SEST_BOND
!
      type emm_energy_definition
        double precision :: V, V_ee_A, V_Ne_A, V_NN, V_ee_AB, V_Ne_AB, V_Ne_Alp, V_ee_Alp, V_ee_lplp
      end type emm_energy_definition
!
      type(emm_energy_definition) :: SEST_ENERGY, SEST_def, SEST_und
!
      type lp_definition
        character(len=5) :: atom_type
        double precision :: X, Y, Z
        integer :: atom
      end type lp_definition
!
      type(lp_definition), allocatable, dimension(:) :: LP
!
! Parameter arrays:
      double precision :: Energy_total_SEST
      double precision, allocatable, dimension(:) :: V_Ne_ABa, V_Ne_BAa, V_Ne_ABi, V_Ne_BAi
      double precision, allocatable, dimension(:) :: r_Ne_ABa, r_Ne_BAa, r_Ne_ABi, r_Ne_BAi
      double precision, allocatable, dimension(:,:) :: V_ee_AaBb, V_ee_AaBi, V_ee_BbAi, V_ee_AiBj
      double precision, allocatable, dimension(:,:) :: r_ee_AaBb, r_ee_AaBi, r_ee_BbAi, r_ee_AiBj
      double precision, allocatable, dimension(:) :: V_ee_Aalp, V_ee_Ailp
      double precision, allocatable, dimension(:) :: r_ee_Aalp, r_ee_Ailp
      double precision  :: V_Ne_Alp, V_ee_alpblp
      double precision  :: r_Ne_Alp, r_ee_alpblp
!
! Number of lone pairs:
      integer :: Nlp
!
!  Logical arrays indicating whether the parameter is to be optimized or held constant, and gives index in PARSET:
      logical, allocatable, dimension(:) :: r_Ne_ABa_opt, r_Ne_BAa_opt, r_Ne_ABi_opt, r_Ne_BAi_opt
      logical, allocatable, dimension(:,:) :: r_ee_AaBb_opt, r_ee_AaBi_opt, r_ee_BbAi_opt, r_ee_AiBj_opt
      logical, allocatable, dimension(:) :: r_ee_Aalp_opt, r_ee_Ailp_opt
      logical :: r_Ne_Alp_opt, r_ee_alpblp_opt
!
! Array containing information about AB and AlpB interactions to be optimized
      type emm_opt
        character(len=5) :: Aatom_type, Batom_type
        integer :: connect ! 0 or 1 for connectivity
        integer :: I_und ! where parameters for this interaction begin in PARSET 
        logical :: set
      end type emm_opt
!
      type(emm_opt), allocatable, dimension(:) :: SEST_OPT_AB, SEST_OPT_AlpB
!
! Number of AB and AlpB parameters
      integer :: N_AB_und, N_AlpB_und
!
! Hartree-Fock energy of system which is having its gradient fit
      double precision :: E_HF
!
      end module SEST_tools
