! SESC code
! Created July 12, 2007
      subroutine E_SEST
!***********************************************************************
!     Date last modified: July 12, 2007                                *
!     Author: Joshua Hollett                                           *
!     Description: The Simulated Electronic Structure Calculation(SESC)*
!                  program.                                            *
!***********************************************************************
! Modules:
      USE program_manager
      USE program_files
      USE SEST_tools
      USE type_molecule
      USE program_constants
      USE matrix_print
      USE type_elements
!                                                                      
      implicit none
!
! Scalars:
      integer :: I,J, Aatom, alp
      double precision, allocatable :: g(:), H(:,:)
!
! Begin:
!
      call PRG_manager('enter', 'E_SEST', 'MM:ENERGY%SEST')
!
      molecule%name=MOL_TITLE(1:len_trim(MOL_TITLE))
!
! Copy Cartesian to molecule, the rest of this code uses type molecule(should change this to cartesian eventually)
      do Aatom=1,molecule%Natoms
        MOL_atoms(Aatom)%symbol=trim(ELEMENT_SYMBOLS(MOL_atoms(Aatom)%Atomic_number))
        MOL_atoms(Aatom)%X=CARTESIAN(Aatom)%X
        MOL_atoms(Aatom)%Y=CARTESIAN(Aatom)%Y
        MOL_atoms(Aatom)%Z=CARTESIAN(Aatom)%Z
      end do
!
      if(MUN_PRTLEV.gt.0)then
        write(6,'(a)')'***********************************************' 
        write(6,'(a)')'SESC MM version 1.1'
        write(6,'(a)')'created October 29, 2007' 
        write(6,'(a/)')'***********************************************' 
!
        write(6,'(3a)')'Initial Cartesian coordinates of ',trim(molecule%name),' (angstroms)'
        write(6,'(a)')''
        do Aatom=1,molecule%Natoms
          write(6,'(a2,x,3F12.6)')MOL_atoms(Aatom)%symbol,&
                                 &MOL_atoms(Aatom)%X*Bohr_to_Angstrom,&
                                 &MOL_atoms(Aatom)%Y*Bohr_to_Angstrom,&
                                 &MOL_atoms(Aatom)%Z*Bohr_to_Angstrom
        end do
        write(6,'(a)')''
      end if ! prtlev
!
! Define connectivity matrix (also distance matrix)
      call SEST_DEFINE_BONDS
! Determine number of lone pairs and their positions
      if(.not.allocated(LP))then
        call SEST_COUNT_LONE_PAIRS
        if(Nlp.gt.0)then
          call SEST_PUT_LONE_PAIRS
        end if
      end if
!
      if(MUN_PRTLEV.gt.0)then
        if(Nlp.gt.0)then
          write(6,'(a)')'Coordinates of Lone Pairs (angstroms)'
          do alp=1,Nlp
            write(6,'(3F14.8)')lp(alp)%X, lp(alp)%Y, lp(alp)%Z
          end do
          write(6,'(a)')''
        end if
      end if ! prtlev
!
!
! Print distance and connectivity matrices
      if(MUN_PRTLEV.gt.0)then
        write(6,'(a)')'Distance Matrix in Bohr'
        write(6,'(8x,10I12)')(J,J=1,molecule%Natoms)
        do I=1,molecule%Natoms
          write(6,'(a,I3,10F12.6)')MOL_atoms(I)%symbol,I,(SEST_BOND(I,J)%distance,J=1,molecule%Natoms)
        end do
!
        write(6,'(a)')''
!
        write(6,'(a)')'Connectivity Matrix'
        write(6,'(12x,10I12)')(J,J=1,molecule%Natoms)
        do I=1,molecule%Natoms
          write(6,'(a,I3,10I0)')MOL_atoms(I)%symbol,I,(SEST_BOND(I,J)%connect,J=1,molecule%Natoms)
        end do
!
        write(6,'(a)')''
!
        write(6,'(a)')'Bond Type Matrix'
        write(6,'(5x,10I12)')(J,J=1,molecule%Natoms)
        do I=1,molecule%Natoms
          write(6,'(a,I3,7x,10a12)')MOL_atoms(I)%symbol,I,(SEST_BOND(I,J)%type,J=1,molecule%Natoms)
        end do
      end if ! prtlev
!
      call SEST_ENERGY_CALC
!
      if(MUN_PRTLEV.gt.0)then
        write(6,'(a)')''
        write(6,'(a,F16.8,a)')'Energy of initial structure: ',SEST_ENERGY%V/2.0D0,' hartrees'
        write(6,'(a)')''
!
        write(6,'(a,F16.8,a)')'Energy Components'
        write(6,'(a,F14.8)')'V_Ne_A= ',SEST_ENERGY%V_Ne_A
        write(6,'(a,F14.8)')'V_ee_A= ',SEST_ENERGY%V_ee_A
        write(6,'(a,F14.8)')'V_Ne_AB= ',SEST_ENERGY%V_Ne_AB
        write(6,'(a,F14.8)')'V_ee_AB= ',SEST_ENERGY%V_ee_AB
        write(6,'(a,F14.8)')'V_NN= ',SEST_ENERGY%V_NN
        write(6,'(a,F14.8)')'V_Ne_Alp= ',SEST_ENERGY%V_Ne_Alp
        write(6,'(a,F14.8)')'V_ee_Alp= ',SEST_ENERGY%V_ee_Alp
        write(6,'(a,F14.8)')'V_ee_lplp= ',SEST_ENERGY%V_ee_lplp
!
!      call GEOM_OPT
!
!      call SEST_ENERGY_CALC
!      write(6,'(a)')''
!      write(6,'(a,F16.8,a)')'Energy of Final structure: ',SEST_ENERGY%V/2.0D0,' hartrees'
!      write(6,'(a)')''
!
!      write(6,'(3a)')'Final Cartesian coordinates of ',trim(molecule%name),' (angstroms)'
!      write(6,'(a)')''
!      do I=1,molecule%Natoms
!        write(6,'(a2,x,3F12.6)')MOL_atoms(I)%symbol,MOL_atoms(I)%X/Angstrom_to_Bohr,&
!                                MOL_atoms(I)%Y/Angstrom_to_Bohr,MOL_atoms(I)%Z/Angstrom_to_Bohr
!      end do
!
      allocate(g(3*(molecule%Natoms+Nlp)),H(3*(molecule%Natoms+Nlp),3*(molecule%Natoms+Nlp)))
      call SEST_CALC_DERIVS(g,H)
!
      write(6,'(a)')''
      write(6,'(a)')'g'
      do I=1,3*(molecule%Natoms+Nlp)
        write(6,'(F16.8)')g(I)
      end do 
!
      write(6,'(a)')''
      write(6,'(a)')'H'
      do I=1,3*(molecule%Natoms+Nlp)
        write(6,'(10F16.8)')(H(I,J)/2.0D0,J=1,3*(molecule%Natoms+Nlp))
      end do
      write(6,'(a)')''
      deallocate(g,H)
!
!      if(molecule%Natoms.eq.2)then
!        call SEST_PES_PLOT
!      end if
!
      write(6,'(a)')''
      write(6,'(3a)')'Final Cartesian coordinates of ',trim(molecule%name),' (angstroms)'
      write(6,'(a)')''
      do I=1,molecule%Natoms
        write(6,'(a2,x,3F12.6)')MOL_atoms(I)%symbol,MOL_atoms(I)%X/Angstrom_to_Bohr,&
                                MOL_atoms(I)%Y/Angstrom_to_Bohr,MOL_atoms(I)%Z/Angstrom_to_Bohr
      end do
        write(6,'(a)')''
        write(6,'(a)')'********************************************************************'
        write(6,'(a)')'********************************************************************'
      end if ! prtlev
      call PRG_manager('exit','E_SEST','MM:ENERGY%SEST')
!
      end subroutine E_SEST
!
      subroutine SEST_COUNT_LONE_PAIRS
!***********************************************************************
!     Date last modified: April 23, 2007                               *
!     Author: Joshua Hollett                                           *
!     Description: Count lone pairs and allocate LP array.             *
!***********************************************************************
!Modules:
      USE program_manager
      USE SEST_tools
      USE type_molecule
!
      implicit none
!
! Scalars:
      integer :: Aatom
!
! Begin:
!
      call PRG_manager('enter','SEST_COUNT_LONE_PAIRS','UTILITY')
!
      Nlp=0
      do Aatom=1,molecule%Natoms
        select case(trim(MOL_atoms(Aatom)%symbol))
          case('N')
            Nlp=Nlp+1 
          case('O')
            Nlp=Nlp+2
          case('F')
            Nlp=Nlp+3
          case('P')
            Nlp=Nlp+1 
          case('S')
            Nlp=Nlp+2
          case('Cl')
            Nlp=Nlp+3
          case('Br')
            Nlp=Nlp+3
          case('I')
            Nlp=Nlp+3
          case default
            cycle
        end select
      end do ! Aatom
!
      if(Nlp.gt.0)then
        if(.not.allocated(LP))then
          allocate(lp(Nlp))
        end if 
      end if
!
      call PRG_manager('exit','SEST_COUNT_LONE_PAIRS','UTILITY')
!
      end subroutine SEST_COUNT_LONE_PAIRS
!
      subroutine SEST_DEFINE_BONDS
!***********************************************************************
!     Date last modified: April 23, 2007                               *
!     Author: Joshua Hollett                                           *
!     Description: Calculate distance matrix and determine             *
!                  connectivity matrix.                                *
!***********************************************************************
! Modules:
      USE program_manager
      USE SEST_tools
      USE type_molecule
      USE GRAPH_objects
!
      implicit none
!
! Local:
      integer :: I,J,Aatom, Batom, valencyA, valencyB
      character(len=5) :: nameA,nameB
!
! Begin:
!
      call PRG_manager('enter','SEST_DEFINE_BONDS','UTILITY')
!
! Calculate distances and define connectivity matrix
      if(.not.allocated(SEST_BOND))then
        allocate(SEST_BOND(molecule%Natoms,molecule%Natoms))
      end if
!      call GET_object('MOLECULE:ATOMIC_DISTANCES')
      call ATOM_DISTANCES
      SEST_BOND(1:molecule%Natoms,1:molecule%Natoms)%distance=DISMAT(1:molecule%Natoms,1:molecule%Natoms)
!      call GET_object('GRAPH:CONNECT')
      call GRAPH_CONN_BS
      SEST_BOND(1:molecule%Natoms,1:molecule%Natoms)%connect=CONNECT(1:molecule%Natoms,1:molecule%Natoms)
!
! Manual connectivity for testing
      SEST_BOND(2,1)%connect=1
      SEST_BOND(1,2)%connect=1
!      SEST_BOND(3,1)%connect=1
!      SEST_BOND(1,3)%connect=1
!      SEST_BOND(1,4)%connect=1
!      SEST_BOND(4,1)%connect=1
!      SEST_BOND(1,5)%connect=1
!      SEST_BOND(5,1)%connect=1
!      SEST_BOND(2,6)%connect=1
!      SEST_BOND(6,2)%connect=1
!      SEST_BOND(2,7)%connect=1
!      SEST_BOND(7,2)%connect=1
!      SEST_BOND(2,8)%connect=1
!      SEST_BOND(8,2)%connect=1
!      SEST_BOND(3,2)%connect=0
!      SEST_BOND(2,3)%connect=0
!      SEST_BOND(3,4)%connect=0
!      SEST_BOND(4,3)%connect=0
!      SEST_BOND(4,2)%connect=0
!      SEST_BOND(2,4)%connect=0
!      SEST_BOND(5,2)%connect=0
!      SEST_BOND(2,5)%connect=0
!      SEST_BOND(5,3)%connect=0
!      SEST_BOND(3,5)%connect=0
!      SEST_BOND(5,4)%connect=0
!      SEST_BOND(4,5)%connect=0
!
! Determine bond types
      do Aatom=1,molecule%Natoms
        nameA=trim(MOL_atoms(Aatom)%symbol)
!
        valencyA=sum(SEST_BOND(Aatom,1:molecule%Natoms)%connect)
!
        if(trim(MOL_atoms(Aatom)%symbol).eq.'C')then
          if(valencyA.eq.4)nameA='sp3C'
          if(valencyA.eq.3)nameA='sp2C'
          if(valencyA.eq.2)nameA='spC'
        end if
!
        if(trim(MOL_atoms(Aatom)%symbol).eq.'N')then
          if(valencyA.eq.3)nameA='sp3N'
          if(valencyA.eq.2)nameA='sp2N'
          if(valencyA.eq.1)nameA='spN'
        end if
!
        if(trim(MOL_atoms(Aatom)%symbol).eq.'O')then
          if(valencyA.eq.2)nameA='sp3O'
          if(valencyA.eq.1)nameA='sp2O'
        end if
!
        MOL_atoms(Aatom)%type=trim(nameA)
!
        do Batom=1,Aatom
          nameB=MOL_atoms(Batom)%type
!
          if(SEST_BOND(Aatom,Batom)%connect.eq.0)then
            SEST_BOND(Aatom,Batom)%type='none'
            SEST_BOND(Batom,Aatom)%type='none'
            cycle
          end if 
!
          valencyB=sum(SEST_BOND(Batom,1:molecule%Natoms)%connect)
!
          if(MOL_atoms(Aatom)%Atomic_number.eq.MOL_atoms(Batom)%Atomic_number)then
            if(valencyA.lt.valencyB)then
              SEST_BOND(Aatom,Batom)%type=trim(nameA)//trim(nameB)
            else
              SEST_BOND(Aatom,Batom)%type=trim(nameB)//trim(nameA)
            end if
          else if(MOL_atoms(Aatom)%Atomic_number.lt.MOL_atoms(Batom)%Atomic_number)then
            SEST_BOND(Aatom,Batom)%type=trim(nameA)//trim(nameB)
          else
            SEST_BOND(Aatom,Batom)%type=trim(nameB)//trim(nameA)
          end if
          SEST_BOND(Batom,Aatom)%type=SEST_BOND(Aatom,Batom)%type
        end do 
      end do
!
      call PRG_manager('exit','SEST_DEFINE_BONDS','UTILITY')
!
      end subroutine SEST_DEFINE_BONDS
!
      subroutine SEST_PUT_LONE_PAIRS
!***********************************************************************
!     Date last modified: September 5, 2007                            *
!     Author: Joshua Hollett                                           *
!     Description: Place lone pairs in initial guess positions.         *
!***********************************************************************
! Modules:
      USE program_manager
      USE SEST_tools
      USE type_molecule
!
      implicit none
!
! Local:
      integer :: Aatom, alp,I
      double precision, dimension(:,:) :: A(3,3), A_inv(3,3)
      double precision, dimension(:) :: NORMAL(3), CNC(3), CNC_to_ATOM(3), X_to_Y(3), R_P(3), atom_A(3)
      double precision, dimension(:) :: R_lp1(3), R_lp2(3), R_lp3(3)
      double precision :: TOLER, length_R_Alp, length_XY, length_CNC_to_ATOM, length_R_AP, length_R_lplp, length_R_Plp
      logical :: last_atom_O, last_atom_F
!
! Begin:
!
      call PRG_manager('enter','SEST_PUT_LONE_PAIRS','UTILITY')
!
      TOLER=0.000001D0
! Define and label lone pair types
      alp=1
      do Aatom=1,molecule%Natoms
        if(MOL_atoms(Aatom)%Atomic_number.eq.7)then ! N
          lp(alp)%atom=Aatom
          lp(alp)%atom_type=MOL_atoms(Aatom)%type
          alp=alp+1
        end if
        if(MOL_atoms(Aatom)%Atomic_number.eq.8)then ! O
          lp(alp)%atom=Aatom
          lp(alp+1)%atom=Aatom
          lp(alp)%atom_type=MOL_atoms(Aatom)%type
          lp(alp+1)%atom_type=MOL_atoms(Aatom)%type
          alp=alp+2
        end if
        if(MOL_atoms(Aatom)%Atomic_number.eq.9)then ! F
          lp(alp)%atom=Aatom
          lp(alp+1)%atom=Aatom
          lp(alp+2)%atom=Aatom
          lp(alp)%atom_type=MOL_atoms(Aatom)%type
          lp(alp+1)%atom_type=MOL_atoms(Aatom)%type
          lp(alp+2)%atom_type=MOL_atoms(Aatom)%type
          alp=alp+3
        end if
      end do
!
! Put lone pairs according to their type
      last_atom_O=.false.
      last_atom_F=.false.
      I=0
      do alp=1,Nlp
!
        if(last_atom_O)then ! skip a lone pair if O was last atom
          last_atom_O=.false.
          cycle
        end if
!
        if(last_atom_F)then ! skip two lone pairs if F was last atom
          I=I+1
          if(I.eq.2)then
            last_atom_F=.false.
            I=0
            cycle
          end if
          cycle
        end if
!
        select case(trim(lp(alp)%atom_type))
          case('sp3N')
            length_R_Alp=0.67750762D0
            call SEST_NORMAL_VECTOR(lp(alp)%atom, NORMAL)
            call SEST_CNC_OF_CONNECTED(lp(alp)%atom, CNC)
            CNC_to_ATOM(1)=MOL_atoms(lp(alp)%atom)%X-CNC(1)
            CNC_to_ATOM(2)=MOL_atoms(lp(alp)%atom)%Y-CNC(2)
            CNC_to_ATOM(3)=MOL_atoms(lp(alp)%atom)%Z-CNC(3)
! Assign postion of lone pair depending on geometry
            if(dabs(dot_product(NORMAL(1:3),CNC_to_ATOM(1:3))).lt.TOLER)then !  planar geometry
              lp(alp)%X=MOL_atoms(lp(alp)%atom)%X+length_R_Alp*NORMAL(1)
              lp(alp)%Y=MOL_atoms(lp(alp)%atom)%Y+length_R_Alp*NORMAL(2)
              lp(alp)%Z=MOL_atoms(lp(alp)%atom)%Z+length_R_Alp*NORMAL(3)
            else                                                             !  nonplanar
              CNC_to_ATOM(1:3)=CNC_to_ATOM(1:3)/dsqrt(CNC_to_ATOM(1)**2+CNC_to_ATOM(2)**2+CNC_to_ATOM(3)**2) !normalize CNC_to_ATOM
              lp(alp)%X=MOL_atoms(lp(alp)%atom)%X+length_R_Alp*CNC_to_ATOM(1)
              lp(alp)%Y=MOL_atoms(lp(alp)%atom)%Y+length_R_Alp*CNC_to_ATOM(2)
              lp(alp)%Z=MOL_atoms(lp(alp)%atom)%Z+length_R_Alp*CNC_to_ATOM(3)
            end if
          case('sp3O')
            length_R_AP=0.29309671D0
            length_R_lplp=1.00022604D0
            call SEST_CNC_OF_CONNECTED(lp(alp)%atom, CNC)
            CNC_to_ATOM(1)=MOL_atoms(lp(alp)%atom)%X-CNC(1)
            CNC_to_ATOM(2)=MOL_atoms(lp(alp)%atom)%Y-CNC(2)
            CNC_to_ATOM(3)=MOL_atoms(lp(alp)%atom)%Z-CNC(3)
!            write(6,'(a,3F12.8)')'CNC_to_ATOM= ',CNC_to_ATOM(1:3)
            call SEST_X_TO_Y_VECTOR(lp(alp)%atom, X_to_Y)
!            write(6,'(a,3F12.8)')'X_to_Y= ',X_to_Y(1:3)
            length_XY=dsqrt(X_to_Y(1)**2+X_to_Y(2)**2+X_to_Y(3)**2)
            length_CNC_to_ATOM=dsqrt(CNC_to_ATOM(1)**2+CNC_to_ATOM(2)**2+CNC_to_ATOM(3)**2)
! Assign postion of lone pair depending on geometry
            if(dabs(dabs(dot_product(X_to_Y(1:3),CNC_to_ATOM(1:3)))-length_XY*length_cnc_to_atom).lt.TOLER)then !  linear geometry
              X_to_Y(1:3)=X_to_Y(1:3)/length_XY ! normalize X_to_Y
              ! now use abitrary vector which is normal to X_to_Y i.e. (-y,x,0)
              if(dabs(X_to_Y(3)).lt.TOLER)then ! use (-y,x,0) for R_AP
                R_P(1)=MOL_atoms(lp(alp)%atom)%X-length_R_AP*X_to_Y(2)
                R_P(2)=MOL_atoms(lp(alp)%atom)%Y+length_R_AP*X_to_Y(1)
                R_P(3)=MOL_atoms(lp(alp)%atom)%Z
                ! Find R_XY X R_AP to get normal vector
                NORMAL(1)=-X_to_Y(1)*X_to_Y(3)
                NORMAL(2)=-X_to_Y(2)*X_to_Y(3)
                NORMAL(3)=X_to_Y(1)**2+X_to_Y(2)**2
              else                             ! use (0,-z,y) for R_AP
                R_P(1)=MOL_atoms(lp(alp)%atom)%X
                R_P(2)=MOL_atoms(lp(alp)%atom)%Y-length_R_AP*X_to_Y(3)
                R_P(3)=MOL_atoms(lp(alp)%atom)%Z+length_R_AP*X_to_Y(2)
                ! Find R_XY X R_AP to get normal vector
                NORMAL(1)=X_to_Y(2)**2+X_to_Y(3)**2
                NORMAL(2)=-X_to_Y(1)*X_to_Y(2)
                NORMAL(3)=-X_to_Y(1)*X_to_Y(3)
              end if 
            else                                                             !  nonlinear
! Use Vector normal to the OXY plane to put lone pairs
              call SEST_NORMAL_VECTOR(lp(alp)%atom, NORMAL)
              CNC_to_ATOM(1:3)=CNC_to_ATOM(1:3)/dsqrt(CNC_to_ATOM(1)**2+CNC_to_ATOM(2)**2+CNC_to_ATOM(3)**2)
              R_P(1)=MOL_atoms(lp(alp)%atom)%X+length_R_AP*CNC_to_ATOM(1)
              R_P(2)=MOL_atoms(lp(alp)%atom)%Y+length_R_AP*CNC_to_ATOM(2)
              R_P(3)=MOL_atoms(lp(alp)%atom)%Z+length_R_AP*CNC_to_ATOM(3)
            end if
!            write(6,'(a,3F12.8)')'R_P= ',R_P(1:3)
!            write(6,'(a,3F12.8)')'NORMAL= ',NORMAL(1:3)
            ! First lone pair 
            lp(alp)%X=R_P(1)+length_R_lplp/2.0D0*NORMAL(1)
            lp(alp)%Y=R_P(2)+length_R_lplp/2.0D0*NORMAL(2)
            lp(alp)%Z=R_P(3)+length_R_lplp/2.0D0*NORMAL(3)
            ! Second lone pair 
            lp(alp+1)%X=R_P(1)-length_R_lplp/2.0D0*NORMAL(1)
            lp(alp+1)%Y=R_P(2)-length_R_lplp/2.0D0*NORMAL(2)
            lp(alp+1)%Z=R_P(3)-length_R_lplp/2.0D0*NORMAL(3)
            last_atom_O=.true.
          case('F')
            length_R_AP=0.12811055D0
            length_R_Plp=0.49187569D0
! X_to_Y will be X_to_F
            call SEST_X_TO_F_VECTOR(lp(alp)%atom, X_to_Y)
            if((dabs(X_to_Y(1)).gt.0.000001).or.(dabs(X_to_Y(2)).gt.0.000001))then ! not parallel to z-axis
! normalize X_to_Y            
              length_XY=dsqrt(X_to_Y(1)**2+X_to_Y(2)**2+X_to_Y(3)**2)
              X_to_Y(1:3)=X_to_Y(1:3)/length_XY
! Construct matrix that transforms X_to_Y so that it is parallel with the z-axis, matrix A
              A(1,1)=-X_to_Y(2) !-y
              A(1,2)=X_to_Y(1)  !x 
              A(1,3)=0.0D0
              A(2,1)=-X_to_Y(1)*X_to_Y(3) !-xz
              A(2,2)=-X_to_Y(2)*X_to_Y(3) !-yz
              A(2,3)=X_to_Y(1)**2+X_to_Y(2)**2 !x^2+y^2
              A(3,1:3)=X_to_Y(1:3) !x,y,z
!
              atom_A(1)=MOL_atoms(lp(alp)%atom)%X
              atom_A(2)=MOL_atoms(lp(alp)%atom)%Y
              atom_A(3)=MOL_atoms(lp(alp)%atom)%Z
              R_P(1:3)=matmul(A(1:3,1:3),atom_A(1:3))
              R_P(3)=R_P(3)+length_R_AP ! point P', transformed point between the lone pairs
              R_lp1(1)=R_P(1)+length_R_Plp
              R_lp1(2)=R_P(2)
              R_lp1(3)=R_P(3)
              R_lp2(1)=R_P(1)-0.5D0*length_R_Plp
              R_lp2(2)=R_P(2)+dsqrt(3.0D0)/2.0D0*length_R_Plp
              R_lp2(3)=R_P(3)
              R_lp3(1)=R_P(1)-0.5D0*length_R_Plp
              R_lp3(2)=R_P(2)-dsqrt(3.0D0)/2.0D0*length_R_Plp
              R_lp3(3)=R_P(3)
! now transform lp coordinates back to original coordinate system
              A_inv(1:3,1:3)=transpose(A(1:3,1:3))
              R_lp1(1:3)=matmul(A_inv(1:3,1:3),R_lp1(1:3))
              R_lp2(1:3)=matmul(A_inv(1:3,1:3),R_lp2(1:3))
              R_lp3(1:3)=matmul(A_inv(1:3,1:3),R_lp3(1:3))
            else ! X_to_y is parallel to z-axis
              R_P(1)=MOL_atoms(lp(alp)%atom)%X
              R_P(2)=MOL_atoms(lp(alp)%atom)%Y
              R_P(3)=MOL_atoms(lp(alp)%atom)%Z
              R_P(3)=R_P(3)+sign(1.0D0,X_to_Y(3))*length_R_AP ! point P, between lone pairs
              R_lp1(1)=R_P(1)+length_R_Plp
              R_lp1(2)=R_P(2)
              R_lp1(3)=R_P(3)
              R_lp2(1)=R_P(1)-0.5D0*length_R_Plp
              R_lp2(2)=R_P(2)+dsqrt(3.0D0)/2.0D0*length_R_Plp
              R_lp2(3)=R_P(3)
              R_lp3(1)=R_P(1)-0.5D0*length_R_Plp
              R_lp3(2)=R_P(2)-dsqrt(3.0D0)/2.0D0*length_R_Plp
              R_lp3(3)=R_P(3)
            end if
            lp(alp)%X=R_lp1(1)
            lp(alp)%Y=R_lp1(2)
            lp(alp)%Z=R_lp1(3)
            lp(alp+1)%X=R_lp2(1)
            lp(alp+1)%Y=R_lp2(2)
            lp(alp+1)%Z=R_lp2(3)
            lp(alp+2)%X=R_lp3(1)
            lp(alp+2)%Y=R_lp3(2)
            lp(alp+2)%Z=R_lp3(3)
            last_atom_F=.true.
          case default
            stop 'Error, lone pairs on this atom type not supported'
        end select
      end do
!
! Hard code coordinates of lone pairs of HF dimer 
!      lp(1)%X=-0.27392479D0
!      lp(1)%Y=0.42548422D0
!      lp(1)%Z=-0.05343993D0
!      lp(2)%X=0.42989233D0
!      lp(2)%Y=0.00000000D0
!      lp(2)%Z=-0.27010036D0
!      lp(3)%X=-0.27392479D0
!      lp(3)%Y=-0.42548422D0
!      lp(3)%Z=-0.05343993D0
!      lp(4)%X=0.15828968D0
!      lp(4)%Y=0.42413413D0
!      lp(4)%Z=5.34203723D0
!      lp(5)%X=0.15828968D0
!      lp(5)%Y=-0.42413413D0
!      lp(5)%Z=5.34203723D0
!      lp(6)%X=0.07636527D0
!      lp(6)%Y=0.00000000D0
!      lp(6)%Z=4.58256426D0
!
      call PRG_manager('exit','SEST_PUT_LONE_PAIRS','UTILITY')
!
      end subroutine SEST_PUT_LONE_PAIRS
!
      subroutine SEST_NORMAL_VECTOR(Xatom, NORMAL)
!***********************************************************************
!     Date last modified: September 20, 2007                           *
!     Author: Joshua Hollett                                           *
!     Description: Find normal vector to plane formed by the three     *
!                  atoms attached to atom X. In the case of            *
!                  bivalent atoms it finds the normal vector to the    *
!                  plane containing atom X and the two atoms connected *
!                  to it.                                              *
!***********************************************************************
! Modules:
      USE program_manager
      USE SEST_tools
      USE type_molecule
!
      implicit none
!
! Input: 
      integer, intent(IN) :: Xatom
! Output:
      double precision, dimension(:), intent(OUT) :: normal(3)
! Local: 
      integer :: Iatom, Aatom, Batom, Catom
      double precision, dimension(:) :: vector_AB(3), vector_AC(3)
!
! Begin:
!
      call PRG_manager('enter','SEST_NORMAL_VECTOR','UTILITY')
!
      Aatom=0
      Batom=0
      Catom=0
! Find atoms connected to atom X
      do Iatom=1, molecule%Natoms
        if(SEST_BOND(Iatom,Xatom)%connect.eq.1)then
          if(Aatom.eq.0)then
            Aatom=Iatom
          else if(Batom.eq.0)then
            Batom=Iatom
          else if(Catom.eq.0)then 
            Catom=Iatom
            exit
          end if
        end if
      end do
      if(Catom.eq.0)then ! sp2N or sp3O
        Catom=Batom
        Batom=Aatom
        Aatom=Xatom
      end if
!
! Define vectors AB and AC
      vector_AB(1)=MOL_atoms(Batom)%X-MOL_atoms(Aatom)%X
      vector_AB(2)=MOL_atoms(Batom)%Y-MOL_atoms(Aatom)%Y
      vector_AB(3)=MOL_atoms(Batom)%Z-MOL_atoms(Aatom)%Z
      vector_AC(1)=MOL_atoms(Catom)%X-MOL_atoms(Aatom)%X
      vector_AC(2)=MOL_atoms(Catom)%Y-MOL_atoms(Aatom)%Y
      vector_AC(3)=MOL_atoms(Catom)%Z-MOL_atoms(Aatom)%Z
!
! Calculate normal vector (AB X AC)
      NORMAL(1)=vector_AB(2)*vector_AC(3)-vector_AB(3)*vector_AC(2)
      NORMAL(2)=vector_AB(3)*vector_AC(1)-vector_AB(1)*vector_AC(3)
      NORMAL(3)=vector_AB(1)*vector_AC(2)-vector_AB(2)*vector_AC(1)
! Normalize the normal vector
      NORMAL(1:3)=NORMAL(1:3)/dsqrt(NORMAL(1)**2+NORMAL(2)**2+NORMAL(3)**2) 
!
      call PRG_manager('exit','SEST_NORMAL_VECTOR','UTILITY')
!
      end subroutine SEST_NORMAL_VECTOR
!
      subroutine SEST_CNC_OF_CONNECTED(Xatom, CNC)
!***********************************************************************
!     Date last modified: September 6, 2007                            *
!     Author: Joshua Hollett                                           *
!     Description: Find the center of nuclear charge of the atoms      *
!                  attached to atom X.                                 *
!***********************************************************************
! Modules:
      USE program_manager
      USE SEST_tools
      USE type_molecule
!
      implicit none
!
! Input: 
      integer, intent(IN) :: Xatom
! Output:
      double precision, dimension(:), intent(OUT) :: CNC(3)
! Local: 
      integer :: Aatom
      double precision, dimension(:) :: TOP(3),BOTTOM(3)
!
! Begin:
!
      call PRG_manager('enter','SEST_CNC_OF_CONNECTED','UTILITY')
!
! Find atoms connected to atom X
      TOP(1:3)=0.0D0
      BOTTOM(1:3)=0.0D0
      do Aatom=1, molecule%Natoms
        if(SEST_BOND(Aatom,Xatom)%connect.eq.1)then
          TOP(1)=TOP(1)+dble(MOL_atoms(Aatom)%Atomic_number)*MOL_atoms(Aatom)%X
          TOP(2)=TOP(2)+dble(MOL_atoms(Aatom)%Atomic_number)*MOL_atoms(Aatom)%Y
          TOP(3)=TOP(3)+dble(MOL_atoms(Aatom)%Atomic_number)*MOL_atoms(Aatom)%Z
          BOTTOM(1:3)=BOTTOM(1:3)+dble(MOL_atoms(Aatom)%Atomic_number)
        end if
      end do
      CNC(1:3)=TOP(1:3)/BOTTOM(1:3)
!
      call PRG_manager('exit','SEST_CNC_OF_CONNECTED','UTILITY')
!
      end subroutine SEST_CNC_OF_CONNECTED
!
      subroutine SEST_X_TO_Y_VECTOR(Aatom, X_to_Y)
!***********************************************************************
!     Date last modified: September 20, 2007                           *
!     Author: Joshua Hollett                                           *
!     Description: Find the vector connecting atoms X and Y which are  *
!                  attached to atom A.                                 *
!***********************************************************************
! Modules:
      USE program_manager
      USE SEST_tools
      USE type_molecule
!
      implicit none
!
! Input: 
      integer, intent(IN) :: Aatom
! Output:
      double precision, dimension(:), intent(OUT) :: X_to_Y(3)
! Local: 
      integer :: Iatom,Xatom,Yatom
!
! Begin:
!
      call PRG_manager('enter','SEST_X_TO_Y_VECTOR','UTILITY')
!
! Find atoms connected to atom X
      Xatom=0
      Yatom=0
      do Iatom=1, molecule%Natoms
        if(SEST_BOND(Aatom,Iatom)%connect.eq.1)then
          if(Xatom.eq.0)then
            Xatom=Iatom
          else if(Yatom.eq.0)then
            Yatom=Iatom
            exit 
          end if
        end if
      end do
      X_to_Y(1)=MOL_atoms(Yatom)%X-MOL_atoms(Xatom)%X
      X_to_Y(2)=MOL_atoms(Yatom)%Y-MOL_atoms(Xatom)%Y
      X_to_Y(3)=MOL_atoms(Yatom)%Z-MOL_atoms(Xatom)%Z
!
      call PRG_manager('exit','SEST_X_TO_Y_VECTOR','UTILITY')
!
      end subroutine SEST_X_TO_Y_VECTOR
!
      subroutine SEST_X_TO_F_VECTOR(Aatom, X_to_Y)
!***********************************************************************
!     Date last modified: September 20, 2007                           *
!     Author: Joshua Hollett                                           *
!     Description: Find the vector of the XF bond.                     *
!***********************************************************************
! Modules:
      USE program_manager
      USE SEST_tools
      USE type_molecule
!
      implicit none
!
! Input: 
      integer, intent(IN) :: Aatom
! Output:
      double precision, dimension(:), intent(OUT) :: X_to_Y(3)
! Local: 
      integer :: Iatom,Xatom
!
! Begin:
!
      call PRG_manager('enter','SEST_X_TO_F_VECTOR','UTILITY')
!
! Find atoms connected to atom X
      Xatom=0
      do Iatom=1, molecule%Natoms
        if(SEST_BOND(Aatom,Iatom)%connect.eq.1)then
          Xatom=Iatom
          exit 
        end if
      end do
      X_to_Y(1)=MOL_atoms(Aatom)%X-MOL_atoms(Xatom)%X
      X_to_Y(2)=MOL_atoms(Aatom)%Y-MOL_atoms(Xatom)%Y
      X_to_Y(3)=MOL_atoms(Aatom)%Z-MOL_atoms(Xatom)%Z
!
      call PRG_manager('exit','SEST_X_TO_F_VECTOR','UTILITY')
!
      end subroutine SEST_X_TO_F_VECTOR
!
      subroutine SEST_ENERGY_CALC
!***********************************************************************
!     Date last modified: July 20, 2007                                *
!     Author: Joshua Hollett                                           *
!     Description: Calculate the energy of the molecule.               *
!***********************************************************************
! Modules:
      USE program_manager
      USE SEST_tools
      USE QM_objects
!
      implicit none
!
! Local scalars:
      double precision :: c_v
!
! Begin:
!
      call PRG_manager('enter','SEST_ENERGY_CALC','UTILITY')
!
      call SEST_V_CONSTANT_CALC
!
      call SEST_V_NN_CALC
!
      call SEST_V_AB_CALC
!
      SEST_ENERGY%V=SEST_ENERGY%V_Ne_A+SEST_ENERGY%V_ee_A+SEST_ENERGY%V_Ne_AB+SEST_ENERGY%V_ee_AB&
                  +SEST_ENERGY%V_NN+SEST_ENERGY%V_Ne_Alp+SEST_ENERGY%V_ee_Alp+SEST_ENERGY%V_ee_lplp
!
      c_v=2.000362789D0
      ENERGY_total_SEST=SEST_ENERGY%V*(c_v-1.0D0)/c_v
!
      call PRG_manager('exit','SEST_ENERGY_CALC','UTILITY')
!
      end subroutine SEST_ENERGY_CALC
!
      subroutine SEST_V_CONSTANT_CALC
!***********************************************************************
!     Date last modified: July 20, 2007                                *
!     Author: Joshua Hollett                                           *
!     Description: Calculate the constant energy of the molecule.      *
!***********************************************************************
! Modules:
      USE program_manager
      USE SEST_tools
      USE type_molecule
!
      implicit none
!
! Local:
      integer :: Aatom
      double precision :: V_ee_A, V_Ne_A
!
! V_A values are determined from potential energy of electrons associated with
! nucleus A only, therefore independent of R_AB
!
! Begin:
!
      call PRG_manager('enter','SEST_V_CONSTANT_CALC','UTILITY')
!
      SEST_ENERGY%V_ee_A=0.00000000D0
      SEST_ENERGY%V_Ne_A=0.00000000D0
      do Aatom=1,molecule%Natoms
        select case(trim(MOL_atoms(Aatom)%type))
          case('H')
            V_Ne_A=-1.00810481D0
            V_ee_A=0.000000000D0
!          case('Li')
          case('Be')
            V_Ne_A=-33.37896528D0
            V_ee_A=4.42004059D0
          case('B')
            V_Ne_A=-55.97567807D0
            V_ee_A=7.73523554D0
          case('sp3C')
            V_Ne_A=-85.89556536D0
            V_ee_A=12.59244603D0
!          case('sp2C')
!          case('spC')
          case('sp3N')
            V_Ne_A=-114.41020415D0
            V_ee_A=12.36567908D0
!          case('sp2N')
!          case('spN')
          case('sp3O')
            V_Ne_A=-143.32479957D0
            V_ee_A=11.80778579D0
!          case('sp2O')
          case('F')
            V_Ne_A=-171.20068535D0
            V_ee_A=11.14754160D0
!          case('Cl')
!          case('Br')
!          case('I')
          case default
            write(6,'(a)')MOL_atoms(Aatom)%type
            stop 'Error, atom type not supported'
        end select
        SEST_ENERGY%V_ee_A=SEST_ENERGY%V_ee_A+V_ee_A
        SEST_ENERGY%V_Ne_A=SEST_ENERGY%V_Ne_A+V_Ne_A
      end do
!
      call PRG_manager('exit','SEST_V_CONSTANT_CALC','UTILITY')
!
      end subroutine SEST_V_CONSTANT_CALC
!
      subroutine SEST_V_AB_CALC
!***********************************************************************
!     Date last modified: April 24, 2007                               *
!     Author: Joshua Hollett                                           *
!     Description: Calculate the energy associated with bonding in     *
!                  the molecule.                                       *
!***********************************************************************
! Modules:
      USE program_manager
      USE SEST_tools
      USE type_molecule
!
      implicit none
!
! Local:
      integer :: Aatom, Batom, N_pair_A, N_pair_B, N_i_A, N_i_B, a, b, i, j, alp, blp
      integer :: N_A, N_B, BOND_ORDER, valency_A, valency_B, temp_pair, temp_i
      double precision :: R_e, R, max_r, delta
!
! Begin:
!
      call PRG_manager('enter','SEST_V_AB_CALC','UTILITY')
!
! Set delta value for piecewise functions
      delta=0.02
!
      SEST_ENERGY%V_ee_AB=0.00000000D0
      SEST_ENERGY%V_Ne_AB=0.00000000D0
      SEST_ENERGY%V_Ne_Alp=0.00000000D0
      SEST_ENERGY%V_ee_Alp=0.00000000D0
      SEST_ENERGY%V_ee_lplp=0.00000000D0
      do Aatom=2,molecule%Natoms
        do Batom=1,Aatom-1
!
          call SEST_COUNT_ELECTRONS(Aatom, N_pair_A, N_i_A)
          call SEST_COUNT_ELECTRONS(Batom, N_pair_B, N_i_B)
! Order atom A and B
          valency_A=sum(SEST_BOND(Aatom,1:molecule%Natoms)%connect)
          valency_B=sum(SEST_BOND(Batom,1:molecule%Natoms)%connect)
          if(MOL_atoms(Aatom)%Atomic_number.eq.MOL_atoms(Batom)%Atomic_number)then 
            if(valency_B.lt.valency_A)then
              temp_pair=N_pair_A
              temp_i=N_i_A
              N_pair_A=N_pair_B
              N_i_A=N_i_B
              N_pair_B=temp_pair
              N_i_B=temp_i
            end if
          else if(MOL_atoms(Batom)%Atomic_number.lt.MOL_atoms(Aatom)%Atomic_number)then
            temp_pair=N_pair_A
            temp_i=N_i_A
            N_pair_A=N_pair_B
            N_i_A=N_i_B
            N_pair_B=temp_pair
            N_i_B=temp_i
          end if
!
! Allocate parameter arrays
          if((N_pair_A.eq.0).and.(N_pair_B.eq.0))then
            N_A=1
            N_B=1
          else if(N_pair_A.eq.0)then
            N_A=1
            N_B=N_pair_B
          else if(N_pair_B.eq.0)then
            N_A=N_pair_A
            N_B=1
          else
            N_A=N_pair_A
            N_B=N_pair_B
          end if
!
          allocate(V_Ne_ABa(1:N_B),V_Ne_BAa(1:N_A),V_Ne_ABi(1:N_i_B),V_Ne_BAi(1:N_i_A)) 
          allocate(r_Ne_ABa(1:N_B),r_Ne_BAa(1:N_A),r_Ne_ABi(1:N_i_B),r_Ne_BAi(1:N_i_A)) 
          allocate(V_ee_AaBb(1:N_A,1:N_B),V_ee_AaBi(1:N_A,1:N_i_B),V_ee_BbAi(1:N_B,1:N_i_A)) 
          allocate(r_ee_AaBb(1:N_A,1:N_B),r_ee_AaBi(1:N_A,1:N_i_B),r_ee_BbAi(1:N_B,1:N_i_A)) 
          allocate(V_ee_AiBj(1:N_i_A,1:N_i_B),r_ee_AiBj(1:N_i_A,1:N_i_B))
!
          if(SEST_BOND(Aatom,Batom)%connect.eq.1)then
            call SEST_BOND_PARAMS(SEST_BOND(Aatom,Batom)%type,BOND_ORDER,R_e)
          else
            call SEST_NON_BOND_PARAMS(Aatom,Batom,R_e)
            BOND_ORDER=0
          end if
!
          R=SEST_BOND(Aatom,Batom)%distance
!
! To remove singularities, energy functions are now piecewise
! Find maximum r
          max_r=0.00000000D0
! r_Ne_BAa
          if(N_pair_A.gt.0)then
            do a=1,N_pair_A  
              if(r_Ne_BAa(a).gt.max_r)then 
                max_r=r_Ne_BAa(a)
              end if
            end do
          end if
! r_Ne_ABa
          if(N_pair_B.gt.0)then
            do a=1,N_pair_B  
              if(r_Ne_ABa(a).gt.max_r)then 
                max_r=r_Ne_ABa(a)
              end if
            end do
          end if
! r_Ne_BAi
          do i=1,N_i_A
            if(r_Ne_BAi(i).gt.max_r)then 
              max_r=r_Ne_BAi(i)
            end if
          end do
! r_Ne_ABi
          do i=1,N_i_B
            if(r_Ne_ABi(i).gt.max_r)then 
              max_r=r_Ne_ABi(i)
            end if
          end do
! r_ee_AaBb
          if(N_pair_A.gt.0)then 
            do a=1,N_pair_A
              if(N_pair_B.gt.0)then ! r_ee_AaBb
                do b=1,N_pair_B
                  if(r_ee_AaBb(a,b).gt.max_r)then
                    max_r=r_ee_AaBb(a,b)
                  end if
                end do
              end if
! r_ee_AaBi
              do i=1,N_i_B ! r_ee_AaBi
                if(r_ee_AaBi(a,i).gt.max_r)then
                  max_r=r_ee_AaBi(a,i)
                end if
              end do
            end do
          end if
! r_ee_BbAi
          if(N_pair_B.gt.0)then
            do b=1,N_pair_B
              do i=1,N_i_A ! r_ee_BbAi
                if(r_ee_BbAi(b,i).gt.max_r)then
                  max_r=r_ee_BbAi(b,i)
                end if
              end do
            end do
          end if
! r_ee_AiBj
          do i=1,N_i_A
            do j=1,N_i_B ! r_ee_AiBj
              if(r_ee_AiBj(i,j).gt.max_r)then
                max_r=r_ee_AiBj(i,j)
              end if
            end do
          end do
!
          if(R.le.(max_r+delta))then
            R=max_r+delta
          end if
!
! Calculate electron-nuclear attraction potential energy (V_Ne_AB)
          if(N_pair_A.gt.0)then ! V_Ne for pairs on A with B
            do a=1,N_pair_A
              SEST_ENERGY%V_Ne_AB=SEST_ENERGY%V_Ne_AB+2.0D0*V_Ne_BAa(a)*(R_e-r_Ne_BAa(a))/(R-r_Ne_BAa(a))
            end do
          end if
          if(N_pair_B.gt.0)then ! V_Ne for pairs on B with A
            do a=1,N_pair_B
              SEST_ENERGY%V_Ne_AB=SEST_ENERGY%V_Ne_AB+2.0D0*V_Ne_ABa(a)*(R_e-r_Ne_ABa(a))/(R-r_Ne_ABa(a))
            end do
          end if
          do i=1,N_i_A ! V_Ne for bonding electrons on A with B
            SEST_ENERGY%V_Ne_AB=SEST_ENERGY%V_Ne_AB+V_Ne_BAi(i)*(R_e-r_Ne_BAi(i))/(R-r_Ne_BAi(i))
          end do
          do i=1,N_i_B ! V_Ne for bonding electrons on B with A
            SEST_ENERGY%V_Ne_AB=SEST_ENERGY%V_Ne_AB+V_Ne_ABi(i)*(R_e-r_Ne_ABi(i))/(R-r_Ne_ABi(i))
          end do
!
! Calculate electron-electron potential energy (V_ee_AB)
          if(N_pair_A.gt.0)then 
            do a=1,N_pair_A
              if(N_pair_B.gt.0)then ! V_ee_AaBb
                do b=1,N_pair_B
                  SEST_ENERGY%V_ee_AB=SEST_ENERGY%V_ee_AB+2.0D0*V_ee_AaBb(a,b)*(R_e-r_ee_AaBb(a,b))/(R-r_ee_AaBb(a,b))
                end do
              end if
              do i=1,N_i_B ! V_ee_AaBi
                SEST_ENERGY%V_ee_AB=SEST_ENERGY%V_ee_AB+V_ee_AaBi(a,i)*(R_e-r_ee_AaBi(a,i))/(R-r_ee_AaBi(a,i))
              end do
            end do
          end if
          if(N_pair_B.gt.0)then
            do b=1,N_pair_B
              do i=1,N_i_A ! V_ee_BbAi
                SEST_ENERGY%V_ee_AB=SEST_ENERGY%V_ee_AB+V_ee_BbAi(b,i)*(R_e-r_ee_BbAi(b,i))/(R-r_ee_BbAi(b,i))
              end do
            end do
          end if
          do i=1,N_i_A
            do j=1,N_i_B
              if((i.le.BOND_ORDER).and.(j.le.BOND_ORDER).and.(i.eq.j))then
                ! i and j belong to the same bond
                SEST_ENERGY%V_ee_AB=SEST_ENERGY%V_ee_AB+V_ee_AiBj(i,j)*(R_e-r_ee_AiBj(i,j))/(R-r_ee_AiBj(i,j))
              else
                SEST_ENERGY%V_ee_AB=SEST_ENERGY%V_ee_AB+0.5D0*V_ee_AiBj(i,j)*(R_e-r_ee_AiBj(i,j))/(R-r_ee_AiBj(i,j))
              end if
            end do
          end do
!
          deallocate(V_Ne_ABa, V_Ne_BAa, V_Ne_ABi, V_Ne_BAi)
          deallocate(r_Ne_ABa, r_Ne_BAa, r_Ne_ABi, r_Ne_BAi)
          deallocate(V_ee_AaBb, V_ee_AaBi, V_ee_BbAi, V_ee_AiBj)
          deallocate(r_ee_AaBb, r_ee_AaBi, r_ee_BbAi, r_ee_AiBj)
        end do ! Batom
      end do ! Aatom
! Lone pairs and atoms
      if(Nlp.gt.0)then
        do Aatom=1,molecule%Natoms
!
          call SEST_COUNT_ELECTRONS(Aatom, N_pair_A, N_i_A)
!
          if(N_pair_A.eq.0)then
            N_A=1
          else
            N_A=N_pair_A
          end if
!
          allocate(V_ee_Aalp(1:N_A),V_ee_Ailp(1:N_i_A)) 
          allocate(r_ee_Aalp(1:N_A),r_ee_Ailp(1:N_i_A)) 
!
          do alp=1,Nlp
!
            R=dsqrt((MOL_atoms(Aatom)%X-lp(alp)%X)**2&
                   +(MOL_atoms(Aatom)%Y-lp(alp)%Y)**2&
                   +(MOL_atoms(Aatom)%Z-lp(alp)%Z)**2)
!
            if(lp(alp)%atom.eq.Aatom)then
              call SEST_LP_onA_PARAMS(Aatom, R_e)
            else
              call SEST_LP_notonA_PARAMS(alp, Aatom, R_e)
            end if
!
! Energy functions are piecewise to remove singularities
! Find maximum r parameter
            max_r=0.00000000D0
            !r_Ne_Alp(B)
            if(r_Ne_Alp.gt.max_r)then
               max_r=r_Ne_Alp
            end if
            !r_ee_Aalp(B)
            if(N_pair_A.gt.0)then
              do a=1,N_pair_A
                if(r_ee_Aalp(a).gt.max_r)then
                  max_r=r_ee_Aalp(a)
                end if
              end do
            end if
            !r_ee_Ailp(B)
            do i=1,N_i_A
              if(r_ee_Ailp(i).gt.max_r)then
                max_r=r_ee_Ailp(i)
              end if
            end do
!
            if(R.le.max_r+delta)then
              !V_ee_Aalp(B)
              if(N_pair_A.gt.0)then
                do a=1,N_pair_A
                  SEST_ENERGY%V_ee_Alp=SEST_ENERGY%V_ee_Alp+2.0D0*V_ee_Aalp(a)*(R_e-r_ee_Aalp(a))/R
                end do
              end if
              !V_ee_Ailp(B)
              do i=1,N_i_A
                SEST_ENERGY%V_ee_Alp=SEST_ENERGY%V_ee_Alp+V_ee_Ailp(i)*(R_e-r_ee_Ailp(i))/R
              end do
              R=max_r+delta
            end if
!
            !V_Ne_Alp(B)
            SEST_ENERGY%V_Ne_Alp=SEST_ENERGY%V_Ne_Alp+2.0D0*V_Ne_Alp*(R_e-r_Ne_Alp)/(R-r_Ne_Alp)
            !V_ee_Aalp(B)
            if(N_pair_A.gt.0)then
              do a=1,N_pair_A
                SEST_ENERGY%V_ee_Alp=SEST_ENERGY%V_ee_Alp+2.0D0*V_ee_Aalp(a)*(R_e-r_ee_Aalp(a))/(R-r_ee_Aalp(a))
              end do
            end if
            !V_ee_Ailp(B)
            do i=1,N_i_A
              SEST_ENERGY%V_ee_Alp=SEST_ENERGY%V_ee_Alp+V_ee_Ailp(i)*(R_e-r_ee_Ailp(i))/(R-r_ee_Ailp(i))
            end do
!
          end do ! alp
!
          deallocate(V_ee_Aalp,V_ee_Ailp) 
          deallocate(r_ee_Aalp,r_ee_Ailp) 
!
        end do  ! Aatom
      end if! Nlp
!
! Lone pairs with lone pairs
      if(Nlp.gt.1)then
        do alp=2,Nlp
          do blp=1,alp-1
!
            R=dsqrt((lp(alp)%X-lp(blp)%X)**2&
                  +(lp(alp)%Y-lp(blp)%Y)**2&
                  +(lp(alp)%Z-lp(blp)%Z)**2)
!
            if(lp(alp)%atom.eq.lp(blp)%atom)then
              call SEST_LP_A_LP_A_PARAMS(alp, R_e)
            else
              call SEST_LP_A_LP_B_PARAMS(alp, blp, R_e)
            end if
!
            ! V_ee_lp(A)_lp(B)
            SEST_ENERGY%V_ee_lplp=SEST_ENERGY%V_ee_lplp+2.0D0*V_ee_alpblp*(R_e-r_ee_alpblp)/(R-r_ee_alpblp)
!
          end do
        end do
      end if
!
      call PRG_manager('exit','SEST_V_AB_CALC','UTILITY')
!
      end subroutine SEST_V_AB_CALC
!
      subroutine SEST_V_NN_CALC
!***********************************************************************
!     Date last modified: May 3, 2007                                  *
!     Author: Joshua Hollett                                           *
!     Description: Calculate the nuclear repulsion energy.             *
!***********************************************************************
! Modules:
      USE program_manager
      USE SEST_tools
      USE type_molecule
!
      implicit none
!
! Local:
      integer :: Aatom, Batom
      double precision :: V_NN_A
!
! Begin:
!
      call PRG_manager('enter','SEST_V_NN_CALC','UTILITY')
!
      SEST_ENERGY%V_NN=0.00000000D0
      do Aatom=2,molecule%Natoms
        V_NN_A=0.00000000D0
        do Batom=1,Aatom-1
          V_NN_A=V_NN_A+dble(MOL_atoms(Batom)%Atomic_number)/SEST_BOND(Aatom,Batom)%distance
        end do
        SEST_ENERGY%V_NN=SEST_ENERGY%V_NN+dble(MOL_atoms(Aatom)%Atomic_number)*V_NN_A
      end do
!
      call PRG_manager('exit','SEST_V_NN_CALC','UTILITY')
!
      end subroutine SEST_V_NN_CALC
!
      subroutine SEST_SOLVE_LIN_SYS(A,b,x,N)
!***********************************************************************
!     Date last modified: May 8, 2007                                  *
!     Author: Joshua Hollett                                           *
!     Description: Solve Ax=b for symmetric A using LU decomposition.  *
! NOT TESTED                                                           *
!***********************************************************************
! Modules:
      USE program_manager
!
      implicit none
!
! Output:
      double precision, dimension(:), intent(OUT) :: x(N)
!
! Input: 
      double precision, dimension(:,:), intent(IN) :: A(N,N)
      double precision, dimension(:), intent(IN) :: b(N)
      integer, intent(IN) ::  N
!
! Local:
      integer :: I,J,ncol
      double precision :: TOLER
      double precision, dimension(:), allocatable :: temp_row,y,bwork
      logical :: zeroed
      double precision, dimension(:,:), allocatable :: L,Awork,Test
!
! Begin:
!
      call PRG_manager('enter','SEST_SOLVE_LIN_SYS','UTILITY')
!
      TOLER=0.00000001D0
      allocate(Awork(N,N))
      Awork(1:N,1:N)=A(1:N,1:N)
!      write(6,'(a)')'A'
!      do I=1,N
!        write(6,'(10F16.8)')(A(I,J),J=1,N)
!      end do
!      write(6,'(a)')'b'
!      do I=1,N
!        write(6,'(F16.8)')b(I)
!      end do
! First reduce A to an upper triangular matrix using elementary row operations
      allocate(temp_row(N))
      ncol=1
      do I=ncol,N
        if(dabs(Awork(I,ncol)).lt.TOLER)then !ncolth entry is zero, must rearrange rows 
          zeroed=.true.
          do J=ncol+1,N
            if(dabs(Awork(J,ncol)).gt.TOLER)then
              ! switch rows, cannot switch rows must add non-zero to zero row
              Awork(I,1:N)=Awork(I,1:N)+Awork(J,1:N)
!              temp_row(1:N)=Awork(I,1:N)
!              Awork(I,1:N)=Awork(J,1:N)
!              Awork(J,1:N)=temp_row(1:N)
              zeroed=.false.
              exit
            end if
          end do
        else ! ncolth entry is not zero, therefore use it to make ncolth entry of preceding rows zero
          do J=I+1,N
            Awork(J,1:N)=Awork(J,1:N)-Awork(I,1:N)*Awork(J,ncol)/Awork(I,ncol)
          end do
          zeroed=.true.
        end if !
        if(zeroed)ncol=ncol+1
        if(ncol.eq.N)exit
      end do
!      write(6,'(a)')'A upper triangular'
!      do I=1,N
!        write(6,'(10F16.8)')(Awork(I,J),J=1,N)
!      end do
!
! A is now in upper triangular form, A'. For symmetric A, A=LDU, where L and U have 1's
! in their diagonals. D is a diagonal matrix with diagonals equal to the upper
! triangular form of A we have just created. Also L is equal to U transpose.
! So LA'x=b. 
! Define L
      allocate(L(N,N)) 
      L(1:N,1:N)=0.0D0
      L(1,1)=1.0D0
      do I=2,N
        L(I,I)=1.0D0
        do J=1,I-1
          if(dabs(Awork(J,J)).lt.TOLER)then
            L(I,J)=Awork(J,I)
          else
            L(I,J)=Awork(J,I)/Awork(J,J)
          end if  
        end do
      end do
!      write(6,'(a)')'L'
!      do I=1,N
!        write(6,'(10F16.8)')(L(I,J),J=1,N)
!      end do
!
! multiply LA' and check if it is equal to A
      allocate(Test(N,N))
      Test(1:N,1:N)=matmul(L(1:N,1:N),Awork(1:N,1:N))
!      write(6,'(a)')'LA'
!      do I=1,N
!        write(6,'(10F16.8)')(Test(I,J),J=1,N)
!      end do
!
! Set A'x=y, then Ly=b. Solve for y. 
!
      allocate(y(N),bwork(N)) 
      bwork(1:N)=b(1:N)
      do I=1,N
        y(I)=bwork(I)
        bwork(I+1:N)=bwork(I+1:N)-y(I)*L(I+1:N,I)
      end do
!
!      write(6,'(a)')'y'
!      do I=1,N
!        write(6,'(F16.8)')y(I)
!      end do
!
! Solve A'x=y for x.
      do I=1,N
!        if(dabs(Awork(N+1-I,N+1-I)).lt.TOLER)then
!          x(N+1-I)=0.0D0
!        else
          x(N+1-I)=y(N+1-I)/Awork(N+1-I,N+1-I) ! With 3N-6(or 5) matrix should not have a singularity
!        end if
        y(1:N-I)=y(1:N-I)-x(N+1-I)*Awork(1:N-I,N+1-I)
      end do
!
!      write(6,'(a)')'x'
!      do I=1,N
!        write(6,'(F16.8)')x(I)
!      end do
!
! For debugging purposes, calculate Ax-b.  If not zero, then there is an
! error
      bwork(1:N)=matmul(A(1:N,1:N),x(1:N))
!      write(6,'(a)')'bnew-b'
      do I=1,N
!        write(6,'(F16.8)')bwork(I)-b(I)
        if(dabs(bwork(I)-b(I)).gt.TOLER)then
          write(6,'(a)')'SOLVE_LIN_SYS not working properly'
          STOP
        end if
      end do 
!
      deallocate(y,L,temp_row)
!
      call PRG_manager('exit','SEST_SOLVE_LIN_SYS','UTILITY')
!
      end subroutine SEST_SOLVE_LIN_SYS
!
      subroutine GEOM_OPT
!***********************************************************************
!     Date last modified: May 8, 2007                                  *
!     Author: Joshua Hollett                                           *
!     Description: Find energy minimum of a given molecule using       *
!                  using Newton-Raphson method.                        *
!***********************************************************************
!
! Modules:
      USE program_manager
      USE SEST_tools
      USE type_molecule

      implicit none
!
! Local:
      double precision, allocatable, dimension(:) :: g,q,g_all,q_all,y
      double precision, allocatable, dimension(:,:) :: H,H_all
      integer :: Iiter,Maxcycle,ThreeN,Nqs,I,J,Iq,Jq, atom_three, alp
      double precision :: TOLER,grad_length,max_step,max_g
      logical :: converged
!
! Begin:
!
      call PRG_manager('enter','GEOM_OPT','UTILITY')
!
      TOLER=0.00000001D0
!
      write(6,'(a)')''
      write(6,'(a)')'**** Beginning SESC geometry optimization ****'
      write(6,'(a)')''
! Transform cartesian coordinates to reduce to 3N-6(or 5) degrees of freedom
      call SEST_COORD_TRANS(atom_three)
!
      ThreeN=3*molecule%Natoms
      if(atom_three.gt.0)then
        Nqs=ThreeN-6
      else
        Nqs=ThreeN-5 ! molecule is linear
      end if
!
! Add lone pair coordinates
      Nqs=Nqs+3*Nlp
      ThreeN=ThreeN+3*Nlp
!
      allocate(g_all(ThreeN),H_all(ThreeN,ThreeN),q_all(ThreeN))
      allocate(g(Nqs),H(Nqs,Nqs),q(Nqs))
! Define q_all(3N)
! Atoms
      do I=1,molecule%Natoms
        q_all(3*I-2)=MOL_atoms(I)%X
        q_all(3*I-1)=MOL_atoms(I)%Y
        q_all(3*I)=MOL_atoms(I)%Z
      end do
! Lone pairs
      J=1
      do I=molecule%Natoms+1,molecule%Natoms+Nlp
        q_all(3*I-2)=lp(J)%X
        q_all(3*I-1)=lp(J)%Y
        q_all(3*I)=lp(J)%Z
        J=J+1
      end do
!
      if(atom_three.eq.0)then
        q(1:Nqs)=q_all(6:ThreeN) ! molecule is linear
      else
        q(1:3*atom_three-7)=q_all(6:3*atom_three-2)
        q(3*atom_three-6:Nqs)=q_all(3*atom_three:ThreeN)
      end if 
      write(6,'(a)')''
      write(6,'(a,F16.8)')'Initial q'
      do I=1,Nqs
        write(6,'(F16.8)')q(I)
      end do
      write(6,'(a)')''
!
      Maxcycle=1000
      converged=.false.
      do Iiter=1,Maxcycle
! Get first and second derivatives
        call SEST_CALC_DERIVS(g_all,H_all)
!
! Remove 6 or 5 parameters from g and H
        if(atom_three.eq.0)then
          g(1:Nqs)=g_all(6:ThreeN)
          H(1:Nqs,1:Nqs)=H_all(6:ThreeN,6:ThreeN)
        else
          g(1:3*atom_three-7)=g_all(6:3*atom_three-2)
          g(3*atom_three-6:Nqs)=g_all(3*atom_three:ThreeN)
          H(1:3*atom_three-7,1:3*atom_three-7)=H_all(6:3*atom_three-2,6:3*atom_three-2)
          H(3*atom_three-6:Nqs,1:3*atom_three-7)=H_all(3*atom_three:ThreeN,6:3*atom_three-2)
          H(1:3*atom_three-7,3*atom_three-6:Nqs)=H_all(6:3*atom_three-2,3*atom_three:ThreeN)
          H(3*atom_three-6:Nqs,3*atom_three-6:Nqs)=H_all(3*atom_three:ThreeN,3*atom_three:ThreeN)
        end if 
!
        write(6,'(a,F16.8)')'q'
        do I=1,Nqs
          write(6,'(F16.8)')q(I)
        end do
        write(6,'(a)')''
        write(6,'(a,F16.8)')'g'
        do I=1,Nqs
          write(6,'(F16.8)')g(I)
        end do
      write(6,'(a)')'H'
      do I=1,Nqs
        write(6,'(10F16.8)')(H(I,J),J=1,Nqs)
      end do
        write(6,'(a)')''
! Check gradient length
        grad_length=0.0D0
        do I=1,Nqs
          grad_length=grad_length+g(I)**2
        end do
        grad_length=dsqrt(grad_length)
        write(6,'(a)')''
        write(6,'(a,I0)')'Iteration ',Iiter
        write(6,'(a,F16.8)')'Gradient length= ',grad_length
        write(6,'(a)')''
        if(grad_length.lt.TOLER)then
          converged=.true.
          exit
        end if
! Find H^-1g by solving Hy=g for y
        allocate(y(Nqs))
        call SEST_SOLVE_LIN_SYS(H,g,y,Nqs)
!
! Sharene's code in case mine has an error
!        call sys_solve(H,g,Nqs,y)
!
! Check size of step(eg. inflection point etc.), if too large use steepest descent.
!
! Atoms
!        max_step=0.1D0
!        do I=1,Nqs-3*Nlp
!          if((y(I)*g(I).lt.0.0D0))then
!            write(6,'(a,I2)')'H<0 for y ',I 
!            y(I)=min(max_step,dabs(g(I)))
!            y(I)=sign(y(I),g(I))
!             y(I)=g(I)
!          else if(dabs(y(I)).gt.max_step)then
!            write(6,'(a,I2)')'H>0 but y>max_step for y ',I 
!            y(I)=min(max_step,dabs(g(I)))
!            y(I)=sign(y(I),g(I))
!          end if  
!        end do
! Lone pairs
!        max_step=0.001D0
!        do I=Nqs-3*Nlp+1,Nqs
!          if((y(I)*g(I).lt.0.0D0).or.(dabs(y(I)).gt.max_step))then
!            write(6,'(a,I2)')'H<0 for y ',I 
!            y(I)=min(max_step,g(I))
!          end if
!        end do
! Now take an optimization step
        do I=1,Nqs
          q(I)=q(I)-y(I)
        end do
        deallocate(y)
! Copy nuclear coordinates back
        MOL_atoms(2)%Z=q(1)
        Iq=2
        do I=3,molecule%Natoms
          if(I.eq.atom_three)then
            MOL_atoms(I)%X=q(Iq)
            MOL_atoms(I)%Z=q(Iq+1)
            Iq=Iq+2
          else
            MOL_atoms(I)%X=q(Iq)
            MOL_atoms(I)%Y=q(Iq+1)
            MOL_atoms(I)%Z=q(Iq+2)
            Iq=Iq+3
          end if
        end do
! Lone pairs
        Iq=Nqs-3*Nlp+1
        do alp=1,Nlp
          lp(alp)%X=q(Iq)
          lp(alp)%Y=q(Iq+1)
          lp(alp)%Z=q(Iq+2)
          Iq=Iq+3
        end do
!
      write(6,'(3a)')'Cartesian coordinates of ',trim(molecule%name),' (bohr)'
      write(6,'(a)')''
      do I=1,molecule%Natoms
        write(6,'(a2,x,3F12.6)')MOL_atoms(I)%symbol,MOL_atoms(I)%X,&
                                MOL_atoms(I)%Y,MOL_atoms(I)%Z
      end do
      write(6,'(a)')''
!
!
      write(6,'(a)')''
      write(6,'(a)')'Coordinates of Lone Pairs'
      do I=1,Nlp
        write(6,'(3F14.8)')lp(I)%X, lp(I)%Y, lp(I)%Z
      end do
      write(6,'(a)')''
!
!
        call SEST_DEFINE_BONDS
        call SEST_ENERGY_CALC
!
        write(6,'(a)')''
        write(6,'(a,F16.8,a)')'Energy of structure: ',SEST_ENERGY%V/2.0D0,' hartrees'
        write(6,'(a)')''
!
      end do ! end of optimization step
!
      write(6,'(a)')''
      write(6,'(a,F16.8)')'Final gradient length= ',grad_length
      write(6,'(a)')''
      write(6,'(a,F16.8)')'Final gradient components'
      do I=1,Nqs
        write(6,'(F16.8)')g(I)
      end do
      write(6,'(a)')''
      write(6,'(a)')'Hessian matrix'
      do I=1,ThreeN
        write(6,'(10F16.8)')(H_all(I,J)/2.0D0,J=1,ThreeN)
      end do
      write(6,'(a)')''
      write(6,'(a,F16.8)')'Final bond distances'
      do I=2,molecule%Natoms
        do J=1,I-1
          write(6,'(F16.8)')SEST_BOND(I,J)%distance
        end do
      end do
      write(6,'(a)')''
      if(.not.converged)then
        stop 'Stucture has not converged after maximum number of iterations'
      end if 
!
      write(6,'(a)')''
      write(6,'(a)')'*** Convergence criteria met ***'
      write(6,'(I4,a)')Iiter-1, ' iterations required'
      write(6,'(a)')''
!
      deallocate(H,g,q)
!
      call PRG_manager('exit','GEOM_OPT','UTILITY')
!
      end subroutine GEOM_OPT
!
      subroutine SEST_CALC_DERIVS(g,H)
!***********************************************************************
!     Date last modified: May 8, 2007                                  *
!     Author: Joshua Hollett                                           *
!     Description: Calculate first and second derivatives of q         *
!***********************************************************************
!
! Modules:
      USE program_manager
      USE SEST_tools
      USE type_molecule 
!
      implicit none
!
! Output:
      double precision, dimension(:), intent(OUT) :: g(3*(molecule%Natoms+Nlp))
      double precision, dimension(:,:), intent(OUT) :: H(3*(molecule%Natoms+Nlp),3*(molecule%Natoms+Nlp))
! Local:
      integer :: Batom,Aatom,I_Ax,I_Ay,I_Az,I_Bx,I_By,I_Bz
      double precision :: R_e,R,dV,ddV
      double precision :: DX_AB,DY_AB,DZ_AB
      integer :: N_pair_A, N_pair_B, N_i_A, N_i_B, N_A, N_B, a, b, i, j, alp, blp, BOND_ORDER
      integer :: I_alpx,I_alpy,I_alpz,I_blpx,I_blpy,I_blpz
      double precision :: DX_Aalp,DY_Aalp,DZ_Aalp
      double precision :: DX_alpA,DY_alpA,DZ_alpA
      double precision :: DX_alpblp,DY_alpblp,DZ_alpblp
      integer :: valency_A, valency_B, temp_pair, temp_i
      double precision :: delta, max_r 
!
! Begin:
!
      call PRG_manager('enter','SEST_CALC_DERIVS','UTILITY')
!
! Set delta value for piecewise functions
      delta=0.02
!
! Calculate first and second derivatives
      g(1:3*(molecule%Natoms+Nlp))=0.0D0
      H(1:3*(molecule%Natoms+Nlp),1:3*(molecule%Natoms+Nlp))=0.0D0
      do Aatom=1,molecule%Natoms
        I_Ax=3*Aatom-2
        I_Ay=3*Aatom-1
        I_Az=3*Aatom
        do Batom=1,molecule%Natoms
          if(Aatom.eq.Batom)cycle ! same atom
!          write(6,'(a,I2)')'Aatom= ',Aatom
!          write(6,'(a,I2)')'Batom= ',Batom
          R=SEST_BOND(Aatom,Batom)%distance
!          write(6,'(a,F12.8)')'R_AB= ',R
          dV=-dble(MOL_atoms(Aatom)%Atomic_number)*dble(MOL_atoms(Batom)%Atomic_number)/R**2 ! Nuclear repulsion terms
          ddV=2.0*dble(MOL_atoms(Aatom)%Atomic_number)*dble(MOL_atoms(Batom)%Atomic_number)/R**3
!          write(6,'(a,F16.8)')'dV=  ',dV
!          write(6,'(a,F16.8)')'ddV= ',ddV
!
          call SEST_COUNT_ELECTRONS(Aatom, N_pair_A, N_i_A)
          call SEST_COUNT_ELECTRONS(Batom, N_pair_B, N_i_B)
! Order atom A and B
          valency_A=sum(SEST_BOND(Aatom,1:molecule%Natoms)%connect)
          valency_B=sum(SEST_BOND(Batom,1:molecule%Natoms)%connect)
          if(MOL_atoms(Aatom)%Atomic_number.eq.MOL_atoms(Batom)%Atomic_number)then 
            if(valency_B.lt.valency_A)then
              temp_pair=N_pair_A
              temp_i=N_i_A
              N_pair_A=N_pair_B
              N_i_A=N_i_B
              N_pair_B=temp_pair
              N_i_B=temp_i
            end if
          else if(MOL_atoms(Batom)%Atomic_number.lt.MOL_atoms(Aatom)%Atomic_number)then
            temp_pair=N_pair_A
            temp_i=N_i_A
            N_pair_A=N_pair_B
            N_i_A=N_i_B
            N_pair_B=temp_pair
            N_i_B=temp_i
          end if
!
! Allocate parameter arrays
          if((N_pair_A.eq.0).and.(N_pair_B.eq.0))then
            N_A=1
            N_B=1
          else if(N_pair_A.eq.0)then
            N_A=1
            N_B=N_pair_B
          else if(N_pair_B.eq.0)then
            N_A=N_pair_A
            N_B=1
          else
            N_A=N_pair_A
            N_B=N_pair_B
          end if
!
          allocate(V_Ne_ABa(1:N_B),V_Ne_BAa(1:N_A),V_Ne_ABi(1:N_i_B),V_Ne_BAi(1:N_i_A)) 
          allocate(r_Ne_ABa(1:N_B),r_Ne_BAa(1:N_A),r_Ne_ABi(1:N_i_B),r_Ne_BAi(1:N_i_A)) 
          allocate(V_ee_AaBb(1:N_A,1:N_B),V_ee_AaBi(1:N_A,1:N_i_B),V_ee_BbAi(1:N_B,1:N_i_A)) 
          allocate(r_ee_AaBb(1:N_A,1:N_B),r_ee_AaBi(1:N_A,1:N_i_B),r_ee_BbAi(1:N_B,1:N_i_A)) 
          allocate(V_ee_AiBj(1:N_i_A,1:N_i_B),r_ee_AiBj(1:N_i_A,1:N_i_B))
!
          if(SEST_BOND(Aatom,Batom)%connect.eq.1)then
!            write(6,'(a)')'Atoms are bonded'
            call SEST_BOND_PARAMS(SEST_BOND(Aatom,Batom)%type, BOND_ORDER, R_e)
          else
            call SEST_NON_BOND_PARAMS(Aatom, Batom, R_e)
            BOND_ORDER=0
          end if
!
! To remove singularities, energy functions are now piecewise
! Find maximum r
          max_r=0.00000000D0
! r_Ne_BAa
          if(N_pair_A.gt.0)then
            do a=1,N_pair_A  
              if(r_Ne_BAa(a).gt.max_r)then 
                max_r=r_Ne_BAa(a)
              end if
            end do
          end if
! r_Ne_ABa
          if(N_pair_B.gt.0)then
            do a=1,N_pair_B  
              if(r_Ne_ABa(a).gt.max_r)then 
                max_r=r_Ne_ABa(a)
              end if
            end do
          end if
! r_Ne_BAi
          do i=1,N_i_A
            if(r_Ne_BAi(i).gt.max_r)then 
              max_r=r_Ne_BAi(i)
            end if
          end do
! r_Ne_ABi
          do i=1,N_i_B
            if(r_Ne_ABi(i).gt.max_r)then 
              max_r=r_Ne_ABi(i)
            end if
          end do
! r_ee_AaBb
          if(N_pair_A.gt.0)then 
            do a=1,N_pair_A
              if(N_pair_B.gt.0)then ! r_ee_AaBb
                do b=1,N_pair_B
                  if(r_ee_AaBb(a,b).gt.max_r)then
                    max_r=r_ee_AaBb(a,b)
                  end if
                end do
              end if
! r_ee_AaBi
              do i=1,N_i_B ! r_ee_AaBi
                if(r_ee_AaBi(a,i).gt.max_r)then
                  max_r=r_ee_AaBi(a,i)
                end if
              end do
            end do
          end if
! r_ee_BbAi
          if(N_pair_B.gt.0)then
            do b=1,N_pair_B
              do i=1,N_i_A ! r_ee_BbAi
                if(r_ee_BbAi(b,i).gt.max_r)then
                  max_r=r_ee_BbAi(b,i)
                end if
              end do
            end do
          end if
! r_ee_AiBj
          do i=1,N_i_A
            do j=1,N_i_B ! r_ee_AiBj
              if(r_ee_AiBj(i,j).gt.max_r)then
                max_r=r_ee_AiBj(i,j)
              end if
            end do
          end do
!
          if(R.le.(max_r+delta))then
            R=max_r+delta
          end if
!
! Calculate electron-nuclear attraction potential energy derivatives(dV_Ne_AB and ddV_Ne_AB)
          if(N_pair_A.gt.0)then ! V_Ne for pairs on A with B
            do a=1,N_pair_A
              dV=dV-2.0D0*V_Ne_BAa(a)*(R_e-r_Ne_BAa(a))/(R-r_Ne_BAa(a))**2
              ddV=ddV+4.0D0*V_Ne_BAa(a)*(R_e-r_Ne_BAa(a))/(R-r_Ne_BAa(a))**3
            end do
          end if
          if(N_pair_B.gt.0)then ! V_Ne for pairs on B with A
            do a=1,N_pair_B
              dV=dV-2.0D0*V_Ne_ABa(a)*(R_e-r_Ne_ABa(a))/(R-r_Ne_ABa(a))**2
              ddV=ddV+4.0D0*V_Ne_ABa(a)*(R_e-r_Ne_ABa(a))/(R-r_Ne_ABa(a))**3
            end do
          end if
          do i=1,N_i_A ! V_Ne for bonding electrons on A with B
            dV=dV-V_Ne_BAi(i)*(R_e-r_Ne_BAi(i))/(R-r_Ne_BAi(i))**2
            ddV=ddV+2.0D0*V_Ne_BAi(i)*(R_e-r_Ne_BAi(i))/(R-r_Ne_BAi(i))**3
          end do
          do i=1,N_i_B ! V_Ne for bonding electrons on B with A
            dV=dV-V_Ne_ABi(i)*(R_e-r_Ne_ABi(i))/(R-r_Ne_ABi(i))**2
            ddV=ddV+2.0D0*V_Ne_ABi(i)*(R_e-r_Ne_ABi(i))/(R-r_Ne_ABi(i))**3
          end do
!   
! Calculate electron-electron potential energy derivatives(dV_ee_AB and ddV_ee_AB)
          if(N_pair_A.gt.0)then 
            do a=1,N_pair_A
              if(N_pair_B.gt.0)then ! V_ee_AaBb
                do b=1,N_pair_B
                  dV=dV-2.0D0*V_ee_AaBb(a,b)*(R_e-r_ee_AaBb(a,b))/(R-r_ee_AaBb(a,b))**2
                  ddV=ddV+4.0D0*V_ee_AaBb(a,b)*(R_e-r_ee_AaBb(a,b))/(R-r_ee_AaBb(a,b))**3
                end do
              end if
              do i=1,N_i_B ! V_ee_AaBi
                dV=dV-V_ee_AaBi(a,i)*(R_e-r_ee_AaBi(a,i))/(R-r_ee_AaBi(a,i))**2
                ddV=ddV+2.0D0*V_ee_AaBi(a,i)*(R_e-r_ee_AaBi(a,i))/(R-r_ee_AaBi(a,i))**3
              end do
            end do
          end if
          if(N_pair_B.gt.0)then
            do b=1,N_pair_B
              do i=1,N_i_A ! V_ee_BbAi
                dV=dV-V_ee_BbAi(b,i)*(R_e-r_ee_BbAi(b,i))/(R-r_ee_BbAi(b,i))**2
                ddV=ddV+2.0D0*V_ee_BbAi(b,i)*(R_e-r_ee_BbAi(b,i))/(R-r_ee_BbAi(b,i))**3
              end do
            end do
          end if
          do i=1,N_i_A
            do j=1,N_i_B
              if((i.le.BOND_ORDER).and.(j.le.BOND_ORDER).and.(i.eq.j))then ! i and j belong to same bond
                dV=dV-V_ee_AiBj(i,j)*(R_e-r_ee_AiBj(i,j))/(R-r_ee_AiBj(i,j))**2
                ddV=ddV+2.0D0*V_ee_AiBj(i,j)*(R_e-r_ee_AiBj(i,j))/(R-r_ee_AiBj(i,j))**3
              else
                dV=dV-0.5D0*V_ee_AiBj(i,j)*(R_e-r_ee_AiBj(i,j))/(R-r_ee_AiBj(i,j))**2
                ddV=ddV+V_ee_AiBj(i,j)*(R_e-r_ee_AiBj(i,j))/(R-r_ee_AiBj(i,j))**3
              end if
            end do
          end do
!
          deallocate(V_Ne_ABa, V_Ne_BAa, V_Ne_ABi, V_Ne_BAi)
          deallocate(r_Ne_ABa, r_Ne_BAa, r_Ne_ABi, r_Ne_BAi)
          deallocate(V_ee_AaBb, V_ee_AaBi, V_ee_BbAi, V_ee_AiBj)
          deallocate(r_ee_AaBb, r_ee_AaBi, r_ee_BbAi, r_ee_AiBj)
!
          DX_AB=MOL_atoms(Aatom)%X-MOL_atoms(Batom)%X
          DY_AB=MOL_atoms(Aatom)%Y-MOL_atoms(Batom)%Y
          DZ_AB=MOL_atoms(Aatom)%Z-MOL_atoms(Batom)%Z
!          write(6,'(a)')''
!          write(6,'(a,I2)')'Aatom= ',Aatom
!          write(6,'(a,I2)')'Batom= ',Batom
!          write(6,'(a,F12.8)')'dV= ',dV
!          write(6,'(a,F12.8)')'ddV= ',ddV
!          write(6,'(a,F12.8)')'DX_AB= ',DX_AB
!          write(6,'(a,F12.8)')'DY_AB= ',DY_AB
!          write(6,'(a,F12.8)')'DZ_AB= ',DZ_AB
!          write(6,'(a)')''
          I_Bx=3*Batom-2
          I_By=3*Batom-1
          I_Bz=3*Batom
! Calculate gradient
          g(I_Ax)=g(I_Ax)+dV*DX_AB/R !x
          g(I_Ay)=g(I_Ay)+dV*DY_AB/R !y
          g(I_Az)=g(I_Az)+dV*DZ_AB/R !z
! Calculate Hessian
          H(I_Ax,I_Ax)=H(I_Ax,I_Ax)+ddV*(DX_AB/R)**2+dV*(1.0D0/R-DX_AB**2/R**3) ! xAxA
          H(I_Ay,I_Ay)=H(I_Ay,I_Ay)+ddV*(DY_AB/R)**2+dV*(1.0D0/R-DY_AB**2/R**3) ! yAyA
          H(I_Az,I_Az)=H(I_Az,I_Az)+ddV*(DZ_AB/R)**2+dV*(1.0D0/R-DZ_AB**2/R**3) ! zAzA
!
          H(I_Ay,I_Ax)=H(I_Ay,I_Ax)+ddV*(DY_AB*DX_AB/R**2)+dV*(-DY_AB*DX_AB/R**3) ! yAxA
          H(I_Az,I_Ax)=H(I_Az,I_Ax)+ddV*(DZ_AB*DX_AB/R**2)+dV*(-DZ_AB*DX_AB/R**3) ! zAxA
          H(I_Az,I_Ay)=H(I_Az,I_Ay)+ddV*(DZ_AB*DY_AB/R**2)+dV*(-DZ_AB*DY_AB/R**3) ! zAyA
!
          if(Batom.gt.Aatom)then
            H(I_Bx,I_Ax)=-ddV*(DX_AB/R)**2+dV*(DX_AB**2/R**3-1.0D0/R) ! xBxA
            H(I_By,I_Ay)=-ddV*(DY_AB/R)**2+dV*(DY_AB**2/R**3-1.0D0/R) ! yByA
            H(I_Bz,I_Az)=-ddV*(DZ_AB/R)**2+dV*(DZ_AB**2/R**3-1.0D0/R) ! zBzA
!
            H(I_Bx,I_Ay)=ddV*(-DX_AB*DY_AB/R**2)+dV*(DX_AB*DY_AB/R**3) ! xByA
            H(I_Bx,I_Az)=ddV*(-DX_AB*DZ_AB/R**2)+dV*(DX_AB*DZ_AB/R**3) ! xBzA
            H(I_By,I_Az)=ddV*(-DY_AB*DZ_AB/R**2)+dV*(DY_AB*DZ_AB/R**3) ! yBzA
! For this case vBuA=uBvA
            H(I_By,I_Ax)=H(I_Bx,I_Ay) ! yBxA=xByA
            H(I_Bz,I_Ax)=H(I_Bx,I_Az) ! zBxA=xBzA
            H(I_Bz,I_Ay)=H(I_By,I_Az) ! zByA=yBzA
! And of course uAvB=vBuA
            H(I_Ax:I_Az,I_Bx:I_Bz)=H(I_Bx:I_Bz,I_Ax:I_Az)
          end if
!
        end do !Batom
!
! Lone pairs and Aatom
        if(Nlp.gt.0)then
          call SEST_COUNT_ELECTRONS(Aatom, N_pair_A, N_i_A)
!
          if(N_pair_A.eq.0)then
            N_A=1
          else
            N_A=N_pair_A
          end if
!
          allocate(V_ee_Aalp(1:N_A),V_ee_Ailp(1:N_i_A)) 
          allocate(r_ee_Aalp(1:N_A),r_ee_Ailp(1:N_i_A)) 
!
          do alp=1,Nlp
!
            dV=0.0D0
            ddV=0.0D0
!
            I_alpx=3*(molecule%Natoms+alp)-2
            I_alpy=3*(molecule%Natoms+alp)-1
            I_alpz=3*(molecule%Natoms+alp)
!
            R=dsqrt((MOL_atoms(Aatom)%X-lp(alp)%X)**2&
                   +(MOL_atoms(Aatom)%Y-lp(alp)%Y)**2&
                   +(MOL_atoms(Aatom)%Z-lp(alp)%Z)**2)
!
            if(lp(alp)%atom.eq.Aatom)then
              call SEST_LP_onA_PARAMS(Aatom, R_e)
            else
              call SEST_LP_notonA_PARAMS(alp, Aatom, R_e)
            end if
!
! Energy functions are piecewise to remove singularities
! Find maximum r parameter
            max_r=0.00000000D0
            !r_Ne_Alp(B)
            if(r_Ne_Alp.gt.max_r)then
               max_r=r_Ne_Alp
            end if
            !r_ee_Aalp(B)
            if(N_pair_A.gt.0)then
              do a=1,N_pair_A
                if(r_ee_Aalp(a).gt.max_r)then
                  max_r=r_ee_Aalp(a)
                end if
              end do
            end if
            !r_ee_Ailp(B)
            do i=1,N_i_A
              if(r_ee_Ailp(i).gt.max_r)then
                max_r=r_ee_Ailp(i)
              end if
            end do
!
            if(R.le.max_r+delta)then
              !V_ee_Aalp(B)
              if(N_pair_A.gt.0)then
                do a=1,N_pair_A
                  dV=dV-2.0D0*V_ee_Aalp(a)*(R_e-r_ee_Aalp(a))/R**2
                  ddV=ddV+4.0D0*V_ee_Aalp(a)*(R_e-r_ee_Aalp(a))/R**3
                end do
              end if
              !V_ee_Ailp(B)
              do i=1,N_i_A
                dV=dV-V_ee_Ailp(i)*(R_e-r_ee_Ailp(i))/R**2
                ddV=ddV+2.0D0*V_ee_Ailp(i)*(R_e-r_ee_Ailp(i))/R**3
              end do
              R=max_r+delta
            end if
!
          !V_Ne_Alp(B)
            dV=dV-2.0D0*V_Ne_Alp*(R_e-r_Ne_Alp)/(R-r_Ne_Alp)**2
            ddV=ddV+4.0D0*V_Ne_Alp*(R_e-r_Ne_Alp)/(R-r_Ne_Alp)**3
          !V_ee_Aalp(B)
            if(N_pair_A.gt.0)then
              do a=1,N_pair_A
                dV=dV-2.0D0*V_ee_Aalp(a)*(R_e-r_ee_Aalp(a))/(R-r_ee_Aalp(a))**2
                ddV=ddV+4.0D0*V_ee_Aalp(a)*(R_e-r_ee_Aalp(a))/(R-r_ee_Aalp(a))**3
              end do
            end if
          !V_ee_Ailp(B)
            do i=1,N_i_A
              dV=dV-V_ee_Ailp(i)*(R_e-r_ee_Ailp(i))/(R-r_ee_Ailp(i))**2
              ddV=ddV+2.0D0*V_ee_Ailp(i)*(R_e-r_ee_Ailp(i))/(R-r_ee_Ailp(i))**3
            end do
!
!            write(6,'(a)')''
!            write(6,'(a,I2,a,I2)')'Aatom= ', Aatom,' alp= ',alp
!            write(6,'(a,F12.8)')'dV= ',dV
!            write(6,'(a,F12.8)')'ddV= ',ddV
!            write(6,'(a)')''
!
            DX_Aalp=MOL_atoms(Aatom)%X-lp(alp)%X
            DY_Aalp=MOL_atoms(Aatom)%Y-lp(alp)%Y
            DZ_Aalp=MOL_atoms(Aatom)%Z-lp(alp)%Z
!
!            write(6,'(a,F14.8)')'DX_Aalp= ',DX_Aalp
!            write(6,'(a,F14.8)')'DY_Aalp= ',DY_Aalp
!            write(6,'(a,F14.8)')'DZ_Aalp= ',DZ_Aalp
!            write(6,'(a)')''
!
            DX_alpA=-DX_Aalp
            DY_alpA=-DY_Aalp
            DZ_alpA=-DZ_Aalp
!
! Calculate lone pair contribution to gradient
!
            g(I_Ax)=g(I_Ax)+dV*DX_Aalp/R
            g(I_Ay)=g(I_Ay)+dV*DY_Aalp/R
            g(I_Az)=g(I_Az)+dV*DZ_Aalp/R
!
            g(I_alpx)=g(I_alpx)+dV*DX_alpA/R
            g(I_alpy)=g(I_alpy)+dV*DY_alpA/R
            g(I_alpz)=g(I_alpz)+dV*DZ_alpA/R
!
! Calculate lone pair contribution to Hessian
            H(I_Ax,I_Ax)=H(I_Ax,I_Ax)+ddV*(DX_Aalp/R)**2+dV*(1.0D0/R-DX_Aalp**2/R**3) ! xAxA
            H(I_Ay,I_Ay)=H(I_Ay,I_Ay)+ddV*(DY_Aalp/R)**2+dV*(1.0D0/R-DY_Aalp**2/R**3) ! yAyA
            H(I_Az,I_Az)=H(I_Az,I_Az)+ddV*(DZ_Aalp/R)**2+dV*(1.0D0/R-DZ_Aalp**2/R**3) ! zAzA
!
            H(I_Ay,I_Ax)=H(I_Ay,I_Ax)+ddV*(DY_Aalp*DX_Aalp/R**2)+dV*(-DY_Aalp*DX_Aalp/R**3) ! yAxA
            H(I_Az,I_Ax)=H(I_Az,I_Ax)+ddV*(DZ_Aalp*DX_Aalp/R**2)+dV*(-DZ_Aalp*DX_Aalp/R**3) ! zAxA
            H(I_Az,I_Ay)=H(I_Az,I_Ay)+ddV*(DZ_Aalp*DY_Aalp/R**2)+dV*(-DZ_Aalp*DY_Aalp/R**3) ! zAyA
!
            H(I_alpx,I_Ax)=-ddV*(DX_Aalp/R)**2+dV*(DX_Aalp**2/R**3-1.0D0/R) ! xalpxA
            H(I_alpy,I_Ay)=-ddV*(DY_Aalp/R)**2+dV*(DY_Aalp**2/R**3-1.0D0/R) ! yalpyA
            H(I_alpz,I_Az)=-ddV*(DZ_Aalp/R)**2+dV*(DZ_Aalp**2/R**3-1.0D0/R) ! zalpzA
!
            H(I_alpx,I_Ay)=ddV*(-DX_Aalp*DY_Aalp/R**2)+dV*(DX_Aalp*DY_Aalp/R**3) ! xalpyA
            H(I_alpx,I_Az)=ddV*(-DX_Aalp*DZ_Aalp/R**2)+dV*(DX_Aalp*DZ_Aalp/R**3) ! xalpzA
            H(I_alpy,I_Az)=ddV*(-DY_Aalp*DZ_Aalp/R**2)+dV*(DY_Aalp*DZ_Aalp/R**3) ! yalpzA
!
            H(I_alpx,I_alpx)=H(I_alpx,I_alpx)+ddV*(DX_alpA/R)**2+dV*(1.0D0/R-DX_alpA**2/R**3) ! xalpxalp
            H(I_alpy,I_alpy)=H(I_alpy,I_alpy)+ddV*(DY_alpA/R)**2+dV*(1.0D0/R-DY_alpA**2/R**3) ! yalpyalp
            H(I_alpz,I_alpz)=H(I_alpz,I_alpz)+ddV*(DZ_alpA/R)**2+dV*(1.0D0/R-DZ_alpA**2/R**3) ! zalpzalp
!
            H(I_alpy,I_alpx)=H(I_alpy,I_alpx)+ddV*(DY_alpA*DX_alpA/R**2)+dV*(-DY_alpA*DX_alpA/R**3) ! yalpxalp
            H(I_alpz,I_alpx)=H(I_alpz,I_alpx)+ddV*(DZ_alpA*DX_alpA/R**2)+dV*(-DZ_alpA*DX_alpA/R**3) ! zalpxalp
            H(I_alpz,I_alpy)=H(I_alpz,I_alpy)+ddV*(DZ_alpA*DY_alpA/R**2)+dV*(-DZ_alpA*DY_alpA/R**3) ! zalpyalp
!
! For this case valpuA=ualpvA
            H(I_alpy,I_Ax)=H(I_alpx,I_Ay) ! yalpxA=xalpyA
            H(I_alpz,I_Ax)=H(I_alpx,I_Az) ! zalpxA=xalpzA
            H(I_alpz,I_Ay)=H(I_alpy,I_Az) ! zalpyA=yalpzA
! And of course uAvalp=valpuA
            H(I_Ax:I_Az,I_alpx:I_alpz)=H(I_alpx:I_alpz,I_Ax:I_Az)
! again ualpvalp=valpualp
            H(I_alpx,I_alpy)=H(I_alpy,I_alpx) ! xalpyalp=yalpxalp
            H(I_alpx,I_alpz)=H(I_alpz,I_alpx) ! xalpzalp=zalpxalp
            H(I_alpy,I_alpz)=H(I_alpz,I_alpy) ! yalpzalp=zalpyalp
!
        end do ! alp
!
        deallocate(V_ee_Aalp,V_ee_Ailp) 
        deallocate(r_ee_Aalp,r_ee_Ailp) 
!
      end if ! Nlp.gt.0
!
! again uAvA=vAuA
        H(I_Ax,I_Ay)=H(I_Ay,I_Ax) ! xAyA=yAxA
        H(I_Ax,I_Az)=H(I_Az,I_Ax) ! xAzA=zAxA
        H(I_Ay,I_Az)=H(I_Az,I_Ay) ! yAzA=zAyA
!
      end do ! Aatom
!
! Calculate lone pair-lone pair components of gradient and Hessian
!
      if(Nlp.gt.1)then
        do alp=1,Nlp
!
          I_alpx=3*(molecule%Natoms+alp)-2
          I_alpy=3*(molecule%Natoms+alp)-1
          I_alpz=3*(molecule%Natoms+alp)
!
          do blp=1,Nlp
            if(alp.eq.blp)cycle
!
            R=dsqrt((lp(alp)%X-lp(blp)%X)**2&
                  +(lp(alp)%Y-lp(blp)%Y)**2&
                  +(lp(alp)%Z-lp(blp)%Z)**2)
!
            if(lp(alp)%atom.eq.lp(blp)%atom)then
              call SEST_LP_A_LP_A_PARAMS(alp, R_e)
            else
              call SEST_LP_A_LP_B_PARAMS(alp, blp, R_e)
            end if
!
            ! V_ee_alp_blp
            dV=-2.0D0*V_ee_alpblp*(R_e-r_ee_alpblp)/(R-r_ee_alpblp)**2
            ddV=4.0D0*V_ee_alpblp*(R_e-r_ee_alpblp)/(R-r_ee_alpblp)**3
!
            DX_alpblp=lp(alp)%X-lp(blp)%X
            DY_alpblp=lp(alp)%Y-lp(blp)%Y
            DZ_alpblp=lp(alp)%Z-lp(blp)%Z
!
            I_blpx=3*(molecule%Natoms+blp)-2
            I_blpy=3*(molecule%Natoms+blp)-1
            I_blpz=3*(molecule%Natoms+blp)
! Calculate gradient
            g(I_alpx)=g(I_alpx)+dV*DX_alpblp/R
            g(I_alpy)=g(I_alpy)+dV*DY_alpblp/R
            g(I_alpz)=g(I_alpz)+dV*DZ_alpblp/R
!
! Calculate Hessian
            H(I_alpx,I_alpx)=H(I_alpx,I_alpx)+ddV*(DX_alpblp/R)**2+dV*(1.0D0/R-DX_alpblp**2/R**3) ! xalpxalp
            H(I_alpy,I_alpy)=H(I_alpy,I_alpy)+ddV*(DY_alpblp/R)**2+dV*(1.0D0/R-DY_alpblp**2/R**3) ! yalpyalp
            H(I_alpz,I_alpz)=H(I_alpz,I_alpz)+ddV*(DZ_alpblp/R)**2+dV*(1.0D0/R-DZ_alpblp**2/R**3) ! zalpzalp
!
            H(I_alpy,I_alpx)=H(I_alpy,I_alpx)+ddV*(DY_alpblp*DX_alpblp/R**2)+dV*(-DY_alpblp*DX_alpblp/R**3) ! yalpxalp
            H(I_alpz,I_alpx)=H(I_alpz,I_alpx)+ddV*(DZ_alpblp*DX_alpblp/R**2)+dV*(-DZ_alpblp*DX_alpblp/R**3) ! zalpxalp
            H(I_alpz,I_alpy)=H(I_alpz,I_alpy)+ddV*(DZ_alpblp*DY_alpblp/R**2)+dV*(-DZ_alpblp*DY_alpblp/R**3) ! zalpyalp
!
            if(blp.gt.alp)then
              H(I_blpx,I_alpx)=-ddV*(DX_alpblp/R)**2+dV*(DX_alpblp**2/R**3-1.0D0/R) ! xblpxalp
              H(I_blpy,I_alpy)=-ddV*(DY_alpblp/R)**2+dV*(DY_alpblp**2/R**3-1.0D0/R) ! yblpyalp
              H(I_blpz,I_alpz)=-ddV*(DZ_alpblp/R)**2+dV*(DZ_alpblp**2/R**3-1.0D0/R) ! zblpzalp
!
              H(I_blpx,I_alpy)=ddV*(-DX_alpblp*DY_alpblp/R**2)+dV*(DX_alpblp*DY_alpblp/R**3) ! xblpyalp
              H(I_blpx,I_alpz)=ddV*(-DX_alpblp*DZ_alpblp/R**2)+dV*(DX_alpblp*DZ_alpblp/R**3) ! xblpzalp
              H(I_blpy,I_alpz)=ddV*(-DY_alpblp*DZ_alpblp/R**2)+dV*(DY_alpblp*DZ_alpblp/R**3) ! yblpzalp
! For this case vblpualp=ublpvalp
              H(I_blpy,I_alpx)=H(I_blpx,I_alpy) ! yblpxalp=xblpyalp
              H(I_blpz,I_alpx)=H(I_blpx,I_alpz) ! zblpxalp=xblpzalp
              H(I_blpz,I_alpy)=H(I_blpy,I_alpz) ! zblpyalp=yblpzalp
! And of course ualpvblp=vblpualp
              H(I_alpx:I_alpz,I_blpx:I_blpz)=H(I_blpx:I_blpz,I_alpx:I_alpz)
            end if
!
          end do
!
! again ualpvalp=valpualp
          H(I_alpx,I_alpy)=H(I_alpy,I_alpx) ! xalpyalp=yalpxalp
          H(I_alpx,I_alpz)=H(I_alpz,I_alpx) ! xalpzalp=zalpxalp
          H(I_alpy,I_alpz)=H(I_alpz,I_alpy) ! yalpzalp=zalpyalp
!
        end do
      end if
!
      call PRG_manager('exit','SEST_CALC_DERIVS','UTILITY')
!
      end subroutine SEST_CALC_DERIVS
!
      subroutine SEST_COORD_TRANS(atom_three)
!***********************************************************************
!     Date last modified: May 8, 2007                                  *
!     Author: Joshua Hollett                                           *
!     Description: Places atom one on the origin, atom two on the      *
!                  z-axis, and a third atom in the xz-plane.           *
!***********************************************************************
!
! Modules:
      USE program_manager
      USE type_molecule
      USE SEST_tools
      USE type_elements
!
      implicit none
!
! Output:
      integer, intent(OUT) :: atom_three
!
! Local:
      double precision, allocatable, dimension(:,:) :: T,XYZ,XYZ_lp
      double precision, allocatable, dimension(:) :: i_prime,j_prime,k_prime
      double precision :: TOLER
      integer :: I,J, Aatom
!
! Begin:
!
      call PRG_manager('enter','SEST_COORD_TRANS','UTILITY')
!
! Copy Cartesian to molecule, the rest of this code uses type molecule(should change this to cartesian eventually)
      do Aatom=1,molecule%Natoms
        MOL_atoms(Aatom)%symbol=trim(ELEMENT_SYMBOLS(MOL_atoms(Aatom)%Atomic_number))
        MOL_atoms(Aatom)%X=CARTESIAN(Aatom)%X
        MOL_atoms(Aatom)%Y=CARTESIAN(Aatom)%Y
        MOL_atoms(Aatom)%Z=CARTESIAN(Aatom)%Z
      end do
!
! Put Atom 1 at origin
      allocate(XYZ(molecule%Natoms,3))
      if(Nlp.gt.0)then
        allocate(XYZ_lp(Nlp,3))
      end if
      do I=1,molecule%Natoms
        XYZ(I,1)=MOL_atoms(I)%X-MOL_atoms(1)%X
        XYZ(I,2)=MOL_atoms(I)%Y-MOL_atoms(1)%Y
        XYZ(I,3)=MOL_atoms(I)%Z-MOL_atoms(1)%Z
      end do
      if(Nlp.gt.0)then 
        do I=1,Nlp
          XYZ_lp(I,1)=lp(I)%X-MOL_atoms(1)%X
          XYZ_lp(I,2)=lp(I)%Y-MOL_atoms(1)%Y
          XYZ_lp(I,3)=lp(I)%Z-MOL_atoms(1)%Z
        end do
      end if
      write(6,'(3a)')'Before transforming Cartesian coordinates of ',trim(molecule%name),' (bohr)'
      write(6,'(a)')''
      do I=1,molecule%Natoms
        write(6,'(a2,x,3F12.6)')MOL_atoms(I)%symbol,XYZ(I,1),XYZ(I,2),XYZ(I,3)
      end do
      write(6,'(a)')''
! Put Atom 2 on z-axis
      allocate(i_prime(3),j_prime(3),k_prime(3))
      allocate(T(3,3))
      TOLER=0.00000001D0
! But first check if it is already on z-axis
      if((dabs(XYZ(2,1)).gt.TOLER).or.(dabs(XYZ(2,2)).gt.TOLER))then
! Define new coordinate system
        i_prime(1)=-XYZ(2,2)
        i_prime(2)=XYZ(2,1)
        i_prime(3)=0.0D0
        j_prime(1)=-XYZ(2,1)*XYZ(2,3)
        j_prime(2)=-XYZ(2,2)*XYZ(2,3)
        j_prime(3)=XYZ(2,1)**2+XYZ(2,2)**2
        k_prime(1)=XYZ(2,1)
        k_prime(2)=XYZ(2,2)
        k_prime(3)=XYZ(2,3)
        i_prime(1:3)=i_prime(1:3)/dsqrt(i_prime(1)**2+i_prime(2)**2+i_prime(3)**2)
        j_prime(1:3)=j_prime(1:3)/dsqrt(j_prime(1)**2+j_prime(2)**2+j_prime(3)**2)
        k_prime(1:3)=k_prime(1:3)/dsqrt(k_prime(1)**2+k_prime(2)**2+k_prime(3)**2)
!
! Define coordinate transformation matrix
        T(1,1:3)=i_prime(1:3)
        T(2,1:3)=j_prime(1:3)
        T(3,1:3)=k_prime(1:3)
!
        XYZ(1:molecule%Natoms,1:3)=transpose(matmul(T(1:3,1:3),transpose(XYZ(1:molecule%Natoms,1:3))))
        if(Nlp.gt.0)then
          XYZ_lp(1:Nlp,1:3)=transpose(matmul(T(1:3,1:3),transpose(XYZ_lp(1:Nlp,1:3))))
        end if 
      end if
!
      write(6,'(3a)')'Atom 2 placed on z-axis'
      write(6,'(a)')''
      do I=1,molecule%Natoms
        write(6,'(a2,x,3F12.6)')MOL_atoms(I)%symbol,XYZ(I,1),XYZ(I,2),XYZ(I,3)
      end do
      write(6,'(a)')''
!
! Now put any atom in the  xz-plane
!
! First find an atom that is not on the z-axis
      atom_three=0
      do I=3,molecule%Natoms
        if((dabs(XYZ(I,1)).gt.TOLER).or.(dabs(XYZ(I,2)).gt.TOLER))then
          atom_three=I
          exit
        end if
      end do
!
! Define new coordinate system
!     
      if(atom_three.gt.0)then
        i_prime(1)=XYZ(atom_three,1)
        i_prime(2)=XYZ(atom_three,2)
        i_prime(3)=0.0D0
        j_prime(1)=-XYZ(atom_three,2)
        j_prime(2)=XYZ(atom_three,1)
        j_prime(3)=0.0D0
        k_prime(1)=0.0D0
        k_prime(2)=0.0D0
        k_prime(3)=1.0D0
        i_prime(1:3)=i_prime(1:3)/dsqrt(i_prime(1)**2+i_prime(2)**2)
        j_prime(1:3)=j_prime(1:3)/dsqrt(j_prime(1)**2+j_prime(2)**2)
!
        T(1,1:3)=i_prime(1:3)
        T(2,1:3)=j_prime(1:3)
        T(3,1:3)=k_prime(1:3)
!
        XYZ(1:molecule%Natoms,1:3)=transpose(matmul(T(1:3,1:3),transpose(XYZ(1:molecule%Natoms,1:3))))
        if(Nlp.gt.0)then
          XYZ_lp(1:Nlp,1:3)=transpose(matmul(T(1:3,1:3),transpose(XYZ_lp(1:Nlp,1:3))))
        end if
      end if
! 
!
      write(6,'(3a)')'Transformed Cartesian coordinates of ',trim(molecule%name),' (bohr)'
      write(6,'(a)')''
      do I=1,molecule%Natoms
        write(6,'(a2,x,3F12.6)')MOL_atoms(I)%symbol,XYZ(I,1),XYZ(I,2),XYZ(I,3)
      end do
      write(6,'(a)')''
!
      if(Nlp.gt.0)then
        write(6,'(a)')'Coordinates of Lone Pairs (angstroms)'
        do I=1,Nlp
          write(6,'(3F14.8)')XYZ_lp(I,1), XYZ_lp(I,2), XYZ_lp(I,3)
        end do
        write(6,'(a)')''
      end if
!
! Copy new coordinates
      do I=1,molecule%Natoms
        MOL_atoms(I)%X=XYZ(I,1)
        MOL_atoms(I)%Y=XYZ(I,2)
        MOL_atoms(I)%Z=XYZ(I,3)
      end do
      if(Nlp.gt.0)then
        do I=1,Nlp
          lp(I)%X=XYZ_lp(I,1)
          lp(I)%Y=XYZ_lp(I,2)
          lp(I)%Z=XYZ_lp(I,3)
        end do
      end if
!
! Copy molecule to CARTESIAN, the rest of this code uses type molecule(should change this to cartesian eventually)
      do Aatom=1,molecule%Natoms
        CARTESIAN(Aatom)%X=MOL_atoms(Aatom)%X
        CARTESIAN(Aatom)%Y=MOL_atoms(Aatom)%Y
        CARTESIAN(Aatom)%Z=MOL_atoms(Aatom)%Z
      end do
!
      deallocate(XYZ,i_prime,j_prime,k_prime,T)
      if(Nlp.gt.0)then
        deallocate(XYZ_lp)
      end if
!
      call PRG_manager('exit','SEST_COORD_TRANS','UTILITY')
!
      end subroutine SEST_COORD_TRANS
!      
      subroutine SEST_BOND_PARAMS(SEST_BOND_type, BOND_ORDER, R_e)
!***********************************************************************
!     Date last modified: July 20, 2007                                *
!     Author: Joshua Hollett                                           *
!     Description: Takes the bond type and returns with the            *
!                  parameters associated with that bond type.          *
!***********************************************************************
!
! Modules:
      USE program_manager
      USE SEST_tools
!
      implicit none
!
! Input:
      character(len=10), intent(IN) :: SEST_BOND_type
!
! Output:
      double precision, intent(OUT) :: R_e
      integer, intent(OUT) :: BOND_ORDER
!
!Begin: 
!
      call PRG_manager('enter','SEST_BOND_PARAMS','UTILITY')
!
      select case(trim(SEST_BOND_type))
        case('HH')
        !A=H B=H         
        ! V_Ne terms
          V_Ne_ABa(1:1)=0.00000000D0
          V_Ne_BAa(1:1)=0.00000000D0
          V_Ne_ABi(1)=-0.81239078D0 ! VNeHHi
          V_Ne_BAi(1)=-0.81239078D0 ! VNeHHi
          r_Ne_ABa(1:1)=0.00000000D0
          r_Ne_BAa(1:1)=0.00000000D0
          r_Ne_ABi(1)=0.27291230D0 ! rNeHHi
          r_Ne_BAi(1)=0.27291230D0 ! rNeHHi
        ! V_ee terms
          V_ee_AaBb(1:1,1:1)=0.00000000D0
          V_ee_AaBi(1:1,1:1)=0.00000000D0
          V_ee_BbAi(1:1,1:1)=0.00000000D0
          V_ee_AiBj(1,1)=0.65398467D0 ! VeeHiHj
          r_ee_AaBb(1:1,1:1)=0.00000000D0
          r_ee_AaBi(1:1,1:1)=0.00000000D0
          r_ee_BbAi(1:1,1:1)=0.00000000D0
          r_ee_AiBj(1,1)=0.68583812D0 ! reeHiHj
          R_e=1.37949997D0
          BOND_ORDER=1
        case('HLi')
          R_e=3.09085104D0
        case('HBe')
        !A=H B=Be         
        ! V_Ne terms
          V_Ne_ABa(1)=-0.39670988D0 ! HBecore
          V_Ne_BAa(1:1)=0.00000000D0
          V_Ne_ABi(1)=-0.71412471D0 ! HBei
          V_Ne_ABi(2)=-0.34659216D0 ! HBej
          V_Ne_BAi(1)=-1.79862011D0 ! BeHi
          r_Ne_ABa(1)=0.00000000D0 ! HBecore
          r_Ne_BAa(1:1)=0.00000000D0
          r_Ne_ABi(1)=0.63849863D0 ! HBei
          r_Ne_ABi(2)=0.00000000D0 ! HBej
          r_Ne_BAi(1)=0.30000000D0 ! BeHi
        ! V_ee terms
          V_ee_AaBb(1:1,1:1)=0.00000000D0
          V_ee_AaBi(1:1,1:2)=0.00000000D0
          V_ee_BbAi(1,1)=0.83020652D0 ! BecoreHi
          V_ee_AiBj(1,1)=0.56516540D0 ! HiBej
          V_ee_AiBj(1,2)=0.55775259D0 ! HiBek
          r_ee_AaBb(1:1,1:1)=0.00000000D0
          r_ee_AaBi(1:1,1:2)=0.00000000D0
          r_ee_BbAi(1,1)=0.00000000D0 ! BecoreHi
          r_ee_AiBj(1,1)=1.44716213D0 ! HiBej
          r_ee_AiBj(1,2)=0.00000000D0 ! HiBek
          R_e=2.52129131D0
          BOND_ORDER=1
        case('HB')
        !A=H B=B         
        ! V_Ne terms
          V_Ne_ABa(1)=-0.44539380D0 ! HBcore
          V_Ne_BAa(1:1)=0.00000000D0
          V_Ne_ABi(1)=-0.83198840D0 ! HBi
          V_Ne_ABi(2:3)=-0.46540346D0 ! HBj
          V_Ne_BAi(1)=-2.61178118D0 ! BHi
          r_Ne_ABa(1)=0.00000000D0 ! HBcore
          r_Ne_BAa(1:1)=0.00000000D0
          r_Ne_ABi(1)=0.95367632D0 ! HBi
          r_Ne_ABi(2:3)=0.00000000D0 ! HBj
          r_Ne_BAi(1)=0.00000000D0 ! BHi
        ! V_ee terms
          V_ee_AaBb(1:1,1:1)=0.00000000D0
          V_ee_AaBi(1:1,1:3)=0.00000000D0
          V_ee_BbAi(1,1)=0.95553159D0 ! BcoreHi
          V_ee_AiBj(1,1)=0.63642609D0 ! HiBj
          V_ee_AiBj(1,2:3)=0.67508917D0 ! HiBk
          r_ee_AaBb(1:1,1:1)=0.00000000D0
          r_ee_AaBi(1:1,1:3)=0.00000000D0
          r_ee_BbAi(1,1)=0.00000000D0 ! BcoreHi
          r_ee_AiBj(1,1)=1.38816189D0 ! HiBj
          r_ee_AiBj(1,2:3)=0.00000000D0 ! HiBk
          R_e=2.24546880D0
          BOND_ORDER=1
        case('Hsp3C')
        !A=H B=C         
        ! V_Ne terms
          V_Ne_ABa(1)=-0.48825391D0 ! HCcore
          V_Ne_BAa(1:1)=0.00000000D0
          V_Ne_ABi(1)=-0.88982445D0 ! HCi
          V_Ne_ABi(2:4)=-0.55830838D0 ! HCj
          V_Ne_BAi(1)=-3.57995349D0 ! CHi
          r_Ne_ABa(1)=0.00000000D0 ! HCcore
          r_Ne_BAa(1:1)=0.00000000D0
          r_Ne_ABi(1)=0.86020816D0 ! HCi
          r_Ne_ABi(2:4)=0.00000000D0 ! HCj
          r_Ne_BAi(1)=-0.04518330D0 ! CHi
        ! V_ee terms
          V_ee_AaBb(1:1,1:1)=0.00000000D0
          V_ee_AaBi(1:1,1:4)=0.00000000D0
          V_ee_BbAi(1,1)=1.09918121D0 ! CcoreHi
          V_ee_AiBj(1,1)=0.68942342D0 ! HiCj
          V_ee_AiBj(1,2:4)=0.77566345D0 ! HiCk
          r_ee_AaBb(1:1,1:1)=0.00000000D0
          r_ee_AaBi(1:1,1:4)=0.00000000D0
          r_ee_BbAi(1,1)=0.00000000D0 ! CcoreHi
          r_ee_AiBj(1,1)=1.29131654D0 ! HiCj
          r_ee_AiBj(1,2:4)=0.00000000D0 ! HiCk
          R_e=2.04814361D0
          BOND_ORDER=1
!         case('Hsp2C')
!         case('HspC')
        case('Hsp3N')
        !A=H B=N         
        ! V_Ne terms
          V_Ne_ABa(1)=-0.52796061D0 ! HNcore
          V_Ne_BAa(1:1)=0.00000000D0
          V_Ne_ABi(1)=-0.93831920D0 ! HNi
          V_Ne_ABi(2:3)=-0.63928490D0 ! HNj
          V_Ne_BAi(1)=-4.25114140D0 ! NHi
          r_Ne_ABa(1)=0.00000000D0 ! HNcore
          r_Ne_BAa(1:1)=0.00000000D0
          r_Ne_ABi(1)=0.68424983D0 ! HNi
          r_Ne_ABi(2:3)=0.00000000D0 ! HNj
          r_Ne_BAi(1)=0.50000000D0 ! NHi
        ! V_ee terms
          V_ee_AaBb(1:1,1:1)=0.00000000D0
          V_ee_AaBi(1:1,1:3)=0.00000000D0
          V_ee_BbAi(1,1)=1.25393914D0 ! NcoreHi
          V_ee_AiBj(1,1)=0.76103997D0 ! HiNj
          V_ee_AiBj(1,2:3)=0.88056010D0 ! HiNk
          r_ee_AaBb(1:1,1:1)=0.00000000D0
          r_ee_AaBi(1:1,1:3)=0.00000000D0
          r_ee_BbAi(1,1)=0.00000000D0 ! NcoreHi
          r_ee_AiBj(1,1)=1.29499212D0 ! HiNj
          r_ee_AiBj(1,2:3)=0.00000000D0 ! HiNk
          R_e=1.89440034D0
          BOND_ORDER=1
!         case('Hp2N')
!         case('HpN')
        case('Hsp3O')
        !A=H B=O         
        ! V_Ne terms
          V_Ne_ABa(1)=-0.55872392D0 ! HOcore
          V_Ne_BAa(1:1)=0.00000000D0
          V_Ne_ABi(1)=-0.96071123D0 ! HOi
          V_Ne_ABi(2)=-0.71262400D0 ! HOj
          V_Ne_BAi(1)=-5.05074528D0 ! OHi
          r_Ne_ABa(1)=0.00000000D0 ! HOcore
          r_Ne_BAa(1:1)=0.00000000D0
          r_Ne_ABi(1)=0.00000000D0 ! HOi
          r_Ne_ABi(2)=0.00000000D0 ! HOj
          r_Ne_BAi(1)=0.80648715D0 ! OHi
        ! V_ee terms
          V_ee_AaBb(1:1,1:1)=0.00000000D0
          V_ee_AaBi(1:1,1:2)=0.00000000D0
          V_ee_BbAi(1,1)=1.39330912D0 ! OcoreHi
          V_ee_AiBj(1,1)=0.83776778D0 ! HiOj
          V_ee_AiBj(1,2)=0.97506417D0 ! HiOk
          r_ee_AaBb(1:1,1:1)=0.00000000D0
          r_ee_AaBi(1:1,1:2)=0.00000000D0
          r_ee_BbAi(1,1)=0.00000000D0 ! OcoreHi
          r_ee_AiBj(1,1)=1.31771044D0 ! HiOj
          r_ee_AiBj(1,2)=0.00000000D0 ! HiOk
          R_e=1.79014019D0
          BOND_ORDER=1
!         case('Hsp2O')
        case('HF')
        !A=H B=F         
        ! V_Ne terms
        ! a= 1s
          V_Ne_ABa(1)=-0.58100628D0 ! V_Ne_HFcore
          V_Ne_BAa(1:1)=0.00000000D0
          V_Ne_ABi(1)=-0.95428615D0 ! V_Ne_HFi
          V_Ne_BAi(1)=-6.00266211D0 ! V_Ne_FHi
          r_Ne_ABa(1)=0.00000000D0 ! r_Ne_HFcore
          r_Ne_BAa(1:1)=0.00000000D0
          r_Ne_ABi(1)=0.00000000D0 ! r_Ne_HFi
          r_Ne_BAi(1)=0.66564720D0 ! r_Ne_FHi
        ! V_ee terms
          V_ee_AaBb(1:1,1:1)=0.00000000D0
          V_ee_AaBi(1:1,1:1)=0.00000000D0
          V_ee_BbAi(1,1)=1.54992800D0 ! V_ee_FcoreHi
          V_ee_AiBj(1,1)=0.92482066D0 ! V_ee_HiFj
          r_ee_AaBb(1:1,1:1)=0.00000000D0
          r_ee_AaBi(1:1,1:1)=0.00000000D0
          r_ee_BbAi(1,1)=0.00000000D0 ! r_ee_FcoreHi
          r_ee_AiBj(1,1)=1.16632848D0 ! r_ee_HiFj
          R_e=1.72142132D0
          BOND_ORDER=1
        case('sp3Csp3C')
        !A=sp3C B=sp3C
        !V_Ne terms
          V_Ne_ABa(1)=-2.02501583D0 ! V_Ne_CCcore
          V_Ne_BAa(1)=-2.02501583D0 ! V_Ne_CCcore
          V_Ne_ABi(1)=-3.41188682D0 ! V_Ne_CCi
          V_Ne_ABi(2:4)=-1.87375711D0 ! V_Ne_CCj
          V_Ne_BAi(1)=-3.41188682D0 ! V_Ne_CCi
          V_Ne_BAi(2:4)=-1.87375711D0 ! V_Ne_CCj
          r_Ne_ABa(1)=0.00000000D0 ! r_Ne_CCcore
          r_Ne_BAa(1)=0.00000000D0 ! r_Ne_CCcore
          r_Ne_ABi(1)=0.75909325D0 ! r_Ne_CCi
          r_Ne_ABi(2:4)=0.00000000D0 ! r_Ne_CCj
          r_Ne_BAi(1)=0.75909325D0 ! r_Ne_CCi
          r_Ne_BAi(2:4)=0.00000000D0 ! r_Ne_CCj
        !V_ee terms
          V_ee_AaBb(1,1)=0.69433043D0 ! V_ee_CcoreCcore
          V_ee_AaBi(1,1)=1.07455017D0 ! V_ee_CcoreCi
          V_ee_AaBi(1,2:4)=0.64211214D0 ! V_ee_CcoreCk
          V_ee_BbAi(1,1)=1.07455017D0 ! V_ee_CcoreCi
          V_ee_BbAi(1,2:4)=0.64211214D0 ! V_ee_CcoreCk
          V_ee_AiBj(1,1)=0.69538548D0 ! V_ee_CiCj
          V_ee_AiBj(1,2:4)=0.87817741D0 ! V_ee_CiCk
          V_ee_AiBj(2:4,1)=0.87817741D0 ! V_ee_CiCk
          V_ee_AiBj(2:4,2:4)=0.63245598D0 ! V_ee_CkCl
          r_ee_AaBb(1,1)=0.00000000D0 ! r_ee_CcoreCcore
          r_ee_AaBi(1,1)=0.00000000D0 ! r_ee_CcoreCi
          r_ee_AaBi(1,2:4)=0.00000000D0 ! r_ee_CcoreCk
          r_ee_BbAi(1,1)=0.00000000D0 ! r_ee_CcoreCi
          r_ee_BbAi(1,2:4)=0.00000000D0 ! r_ee_CcoreCk
          r_ee_AiBj(1,1)=2.03715770D0 ! r_ee_CiCj
          r_ee_AiBj(1,2:4)=0.00000000D0 ! r_ee_CiCk
          r_ee_AiBj(2:4,1)=0.00000000D0 ! r_ee_CiCk
          r_ee_AiBj(2:4,2:4)=0.00000000D0 ! r_ee_CkCl
          R_e=2.88630910D0
          BOND_ORDER=1
        case default
          stop 'Error, bond type not supported'
      end select
!
      call PRG_manager('exit','SEST_BOND_PARAMS','UTILITY')
!
      end subroutine SEST_BOND_PARAMS
!
      subroutine SEST_NON_BOND_PARAMS(Aatom, Batom, R_e)
!***********************************************************************
!     Date last modified: August 17, 2007                              *
!     Author: Joshua Hollett                                           *
!     Description: Takes the atom pair and returns with the            *
!                  parameters associated with that atom pair.          *
!***********************************************************************
!
! Modules:
      USE program_manager
      USE SEST_tools
      USE type_molecule
!
      implicit none
!
! Input:
      integer, intent(IN) :: Aatom, Batom
!
! Output:
      double precision, intent(OUT) :: R_e
!
! Local:
      character(len=10) :: ATOM_PAIR
      integer :: valency_A, valency_B
!
!Begin: 
!
      call PRG_manager('enter','SEST_NON_BOND_PARAMS','UTILITY')
!
      valency_A=sum(SEST_BOND(Aatom,1:molecule%Natoms)%connect)
      valency_B=sum(SEST_BOND(Batom,1:molecule%Natoms)%connect)
!
      if(MOL_atoms(Aatom)%Atomic_number.eq.MOL_atoms(Batom)%Atomic_number)then
        if(valency_A.le.valency_B)then
          ATOM_PAIR=trim(MOL_atoms(Aatom)%type)//trim(MOL_atoms(Batom)%type)
        else
          ATOM_PAIR=trim(MOL_atoms(Batom)%type)//trim(MOL_atoms(Aatom)%type)
        end if
      else if(MOL_atoms(Aatom)%Atomic_number.lt.MOL_atoms(Batom)%Atomic_number)then
        ATOM_PAIR=trim(MOL_atoms(Aatom)%type)//trim(MOL_atoms(Batom)%type)
      else
        ATOM_PAIR=trim(MOL_atoms(Batom)%type)//trim(MOL_atoms(Aatom)%type)
      end if
!
      select case(trim(ATOM_PAIR))
        case('HH')
        !A=H B=H   ! Molecule independent parameters (i.e. HH nonbond) determined, using CH4 parameters      
        ! V_Ne terms
          V_Ne_ABa(1:1)=0.00000000D0
          V_Ne_BAa(1:1)=0.00000000D0
          V_Ne_ABi(1)=-0.13392678D0 ! V_Ne_HHj
          V_Ne_BAi(1)=-0.13392678D0 ! V_Ne_HHj
          r_Ne_ABa(1:1)=0.00000000D0
          r_Ne_BAa(1:1)=0.00000000D0
          r_Ne_ABi(1)=0.00000000D0 ! r_Ne_HHj
          r_Ne_BAi(1)=0.00000000D0 ! r_Ne_HHj
        ! V_ee terms
          V_ee_AaBb(1:1,1:1)=0.00000000D0
          V_ee_AaBi(1:1,1:1)=0.00000000D0
          V_ee_BbAi(1:1,1:1)=0.00000000D0
          V_ee_AiBj(1,1)=0.57676321D0 ! V_ee_HkHl
          r_ee_AaBb(1:1,1:1)=0.00000000D0
          r_ee_AaBi(1:1,1:1)=0.00000000D0
          r_ee_BbAi(1:1,1:1)=0.00000000D0
          r_ee_AiBj(1,1)=1.15567539D0 ! r_ee_HkHl
          R_e=3.34460451D0
        case('Hsp3C')
        !A=H B=C    
        ! V_Ne terms
          V_Ne_ABa(1)=-0.23750969D0 ! V_Ne_HCcore
          V_Ne_BAa(1:1)=0.00000000D0
          V_Ne_ABi(1:4)=-0.32775087D0 ! V_Ne_HCj
          V_Ne_BAi(1)=-1.31861329D0 ! V_Ne_CHj
          r_Ne_ABa(1)=0.00000000D0 ! r_Ne_HCcore
          r_Ne_BAa(1:1)=0.00000000D0
          r_Ne_ABi(1:4)=0.00000000D0 ! r_Ne_HCj
          r_Ne_BAi(1)=0.00000000D0 ! r_Ne_CHj
        ! V_ee terms
          V_ee_AaBb(1:1,1:1)=0.00000000D0
          V_ee_AaBi(1:1,1:1)=0.00000000D0
          V_ee_BbAi(1,1)=0.45185223D0 ! V_ee_CcoreHk
          V_ee_AiBj(1,1:4)=0.44505722D0 ! V_ee_HkCl
          r_ee_AaBb(1:1,1:1)=0.00000000D0
          r_ee_AaBi(1:1,1:1)=0.00000000D0
          r_ee_BbAi(1,1)=0.00000000D0 ! r_ee_CcoreHk
          r_ee_AiBj(1,1:4)=0.92265542D0 ! r_ee_HkCl
          R_e=4.10163762D0
        case('HF')
        !A=H B=F         
        ! V_Ne terms
          V_Ne_ABa(1)=-0.23672941D0 ! V_Ne_HFcore
          V_Ne_BAa(1:1)=0.00000000D0
          V_Ne_ABi(1)=-0.40156681D0 ! V_Ne_HFj
          V_Ne_BAi(1)=-1.85277523D0 ! V_Ne_FHj
          r_Ne_ABa(1)=0.00000000D0 ! r_Ne_HFcore
          r_Ne_BAa(1:1)=0.00000000D0
          r_Ne_ABi(1)=0.00000000D0 ! r_Ne_HFj
          r_Ne_BAi(1)=0.00000000D0 ! r_Ne_FHj
        ! V_ee terms
          V_ee_AaBb(1:1,1:1)=0.00000000D0
          V_ee_AaBi(1:1,1:1)=0.00000000D0
          V_ee_BbAi(1,1)=0.50713542D0 ! V_ee_FcoreHk
          V_ee_AiBj(1,1)=0.53357792D0 ! V_ee_HkFl
          r_ee_AaBb(1:1,1:1)=0.00000000D0
          r_ee_AaBi(1:1,1:1)=0.00000000D0
          r_ee_BbAi(1,1)=0.00000000D0 ! r_ee_FcoreHk
          r_ee_AiBj(1,1)=0.00000000D0 ! V_ee_HkFl
          R_e=4.54494020D0
        case('FF')
        ! V_Ne terms
          V_Ne_ABa(1)=-1.89192287D0 ! V_Ne_FFcore
          V_Ne_BAa(1)=-1.89192287D0 ! V_Ne_FFcore
          V_Ne_ABi(1)=-2.14112534D0 ! V_Ne_FFj
          V_Ne_BAi(1)=-2.14112534D0 ! V_Ne_FFj
          r_Ne_ABa(1)=0.00000000D0 ! r_Ne_FFcore
          r_Ne_BAa(1)=0.00000000D0 ! r_Ne_FFcore
          r_Ne_ABi(1)=0.00000000D0 ! r_Ne_FFj
          r_Ne_BAi(1)=0.00000000D0 ! r_Ne_FFj
        ! V_ee terms
          V_ee_AaBb(1,1)=0.51882870D0 ! V_ee_FcoreFcore
          V_ee_AaBi(1,1)=0.58794564D0 ! V_ee_FcoreFk
          V_ee_BbAi(1,1)=0.58794564D0 ! V_ee_FcoreFk
          V_ee_AiBj(1,1)=0.61860167D0 ! V_ee_FkFl
          r_ee_AaBb(1,1)=0.00000000D0 ! r_ee_FcoreFcore
          r_ee_AaBi(1,1)=0.00000000D0 ! r_ee_FcoreFk
          r_ee_BbAi(1,1)=0.00000000D0 ! r_ee_FcoreFk
          r_ee_AiBj(1,1)=0.00000000D0 ! r_ee_FkFl
          R_e=5.11820634D0
        case('sp3Csp3C')
        !A=sp3C B=sp3C
        !V_Ne terms
          V_Ne_ABa(1)=-1.22232673D0 ! V_Ne_CCcore
          V_Ne_BAa(1)=-1.22232673D0 ! V_Ne_CCcore
          V_Ne_ABi(1:4)=-1.28336811D0 ! V_Ne_CCj
          V_Ne_BAi(1:4)=-1.28336811D0 ! V_Ne_CCj
          r_Ne_ABa(1)=0.00000000D0 ! r_Ne_CCcore
          r_Ne_BAa(1)=0.00000000D0 ! r_Ne_CCcore
          r_Ne_ABi(1:4)=0.00000000D0 ! r_Ne_CCj
          r_Ne_BAi(1:4)=0.00000000D0 ! r_Ne_CCj
        !V_ee terms
          V_ee_AaBb(1,1)=0.41846064D0 ! V_ee_CcoreCcore
          V_ee_AaBi(1,1:4)=0.42760364D0 ! V_ee_CcoreCk
          V_ee_BbAi(1,1:4)=0.42760364D0 ! V_ee_CcoreCk
          V_ee_AiBj(1:4,1:4)=0.47404541D0 ! V_ee_CkCl
          r_ee_AaBb(1,1)=0.00000000D0 ! r_ee_CcoreCcore
          r_ee_AaBi(1,1:4)=0.00000000D0 ! r_ee_CcoreCk
          r_ee_BbAi(1,1:4)=0.00000000D0 ! r_ee_CcoreCk
          r_ee_AiBj(1:4,1:4)=0.00281198D0 ! r_ee_CkCl
          R_e=4.80898607D0
        case default
          write(6,'(2a)')'ATOM_PAIR: ',ATOM_PAIR
          stop 'Error, atom pair not supported'
      end select
!
      call PRG_manager('exit','SEST_NON_BOND_PARAMS','UTILITY')
!
      end subroutine SEST_NON_BOND_PARAMS
!
      subroutine SEST_LP_onA_PARAMS(Aatom, R_e)
!***********************************************************************
!     Date last modified: August 17, 2007                              *
!     Author: Joshua Hollett                                           *
!     Description: Takes a lone pair and the atom it belongs to and    *
!                  returns with the associated parameters.             *
!***********************************************************************
!
! Modules:
      USE program_manager
      USE SEST_tools
      USE type_molecule
!
      implicit none
!
! Input:
      integer, intent(IN) :: Aatom
!
! Output:
      double precision, intent(OUT) :: R_e
!
!Begin: 
!
      call PRG_manager('enter','SEST_LP_onA_PARAMS','UTILITY')
!
      select case(trim(MOL_atoms(Aatom)%type))
        case('sp3N')
        ! V_Ne term 
          V_Ne_Alp=-6.24800388D0 ! V_Ne_Nlp(N)
          r_Ne_Alp=0.00000000D0 ! r_Ne_Nlp(N)
        ! V_ee terms
          V_ee_Aalp(1)=1.73544039D0 ! V_ee_Ncorelp(N)
          V_ee_Ailp(1:3)=1.18652949D0 ! V_ee_Nilp(N)
          r_ee_Aalp(1)=0.39768834D0 ! r_ee_Ncorelp(N)
          r_ee_Ailp(1:3)=0.00000000D0 ! r_ee_Nilp(N)
          R_e=0.67750762D0
!        case('sp2N')
!        case('spN')
        case('sp3O')
        ! V_Ne term 
          V_Ne_Alp=-8.50597992D0 ! V_Ne_Olp(O)
          r_Ne_Alp=0.00000000D0 ! r_Ne_Olp(O)
        ! V_ee terms
          V_ee_Aalp(1)=2.06378819D0 ! V_ee_Ocorelp(O)
          V_ee_Ailp(1:2)=1.44873941D0 ! V_ee_Oilp(O)
          r_ee_Aalp(1)=0.39788104D0 ! r_ee_Ocorelp(O)
          r_ee_Ailp(1:2)=0.00000000D0 ! r_ee_Oilp(O)
          R_e=0.57967121D0
!        case('sp2O')
        case('F')
        ! V_Ne term 
          V_Ne_Alp=-11.20681180D0 ! V_Ne_Flp(F)
          r_Ne_Alp=0.00000000D0 ! r_Ne_Flp(F)
        ! V_ee terms
          V_ee_Aalp(1)=2.41421057D0 ! V_ee_Fcorelp(F)
          V_ee_Ailp(1)=1.73685668D0 ! V_ee_Filp(F)
          r_ee_Aalp(1)=0.38430540D0 ! r_ee_Fcorelp(F)
          r_ee_Ailp(1)=0.00000000D0 ! r_ee_Filp(F)
          R_e=0.50828536D0
        case default
          stop 'Error, atom not supported'
      end select
!
      call PRG_manager('exit','SEST_LP_onA_PARAMS','UTILITY')
!
      end subroutine SEST_LP_onA_PARAMS
!
      subroutine SEST_LP_notonA_PARAMS(alp, Aatom, R_e)
!***********************************************************************
!     Date last modified: August 17, 2007                              *
!     Author: Joshua Hollett                                           *
!     Description: Takes a lone pair and an atom and returns with the  *
!                  associated parameters.                              *
!***********************************************************************
!
! Modules:
      USE program_manager
      USE SEST_tools
      USE type_molecule
!
      implicit none
!
! Input:
      integer, intent(IN) :: alp, Aatom
!
! Output:
      double precision, intent(OUT) :: R_e
!
! Local:
      character(len=10) :: ATOM_PAIR
!
!Begin: 
!
      call PRG_manager('enter','SEST_LP_notonA_PARAMS','UTILITY')
!
      ATOM_PAIR=trim(MOL_atoms(lp(alp)%atom)%type)//trim(MOL_atoms(Aatom)%type)
      select case(trim(ATOM_PAIR))
        case('sp3NH')
        ! V_Ne term
          V_Ne_Alp=-0.42096071D0 !V_Ne_Hlp(N)
          r_Ne_Alp=-1.55938873D0 !r_Ne_Hlp(N)
        ! V_ee terms
          V_ee_Aalp(1:1)=0.00000000D0
          V_ee_Ailp(1)=0.82368428D0 !V_ee_Hilp(N)
          r_ee_Aalp(1:1)=0.00000000D0
          r_ee_Ailp(1)=0.96238256D0 !r_ee_Hilp(N)
          R_e=2.23514234D0
        case('sp3OH')
        ! V_Ne term
          V_Ne_Alp=-0.46519491D0 !V_Ne_Hlp(O)
          r_Ne_Alp=-0.50353411D0 !r_Ne_Hlp(O)
        ! V_ee terms
          V_ee_Aalp(1:1)=0.00000000D0
          V_ee_Ailp(1)=0.92459743D0 !V_ee_Hilp(O)
          r_ee_Aalp(1:1)=0.00000000D0
          r_ee_Ailp(1)=1.10213426D0 !r_ee_Hilp(O)
          R_e=2.04347569D0
        case('FH')
        ! V_Ne term
          V_Ne_Alp=-0.50259620D0 !V_Ne_Hlp(F)
          r_Ne_Alp=0.20448068D0 !r_Ne_Hlp(F)
        ! V_ee terms
          V_ee_Aalp(1:1)=0.00000000D0
          V_ee_Ailp(1)=1.03671827D0 !V_ee_Hilp(F)
          r_ee_Aalp(1:1)=0.00000000D0
          r_ee_Ailp(1)=0.91732704D0 !r_ee_Hilp(F)
          R_e=1.91382074D0
        case('FF')
        ! V_Ne term 
          V_Ne_Alp=-1.74860380D0 ! V_Ne_Flp(F)
          r_Ne_Alp=0.00000000D0 ! r_Ne_Flp(F)
        ! V_ee terms
          V_ee_Aalp(1)=0.38993750D0 ! V_ee_Fcorelp(F)
          V_ee_Ailp(1)=0.54693091D0 ! V_ee_Filp(F)
          r_ee_Aalp(1)=0.00000000D0 ! r_ee_Fcorelp(F)
          r_ee_Ailp(1)=0.00000000D0 ! r_ee_Filp(F)
          R_e=5.13653968D0
        case default
          write(6,'(2a)')'ATOM_PAIR: ',ATOM_PAIR
          stop 'Error, atom pair not supported'
      end select
!
      call PRG_manager('exit','SEST_LP_notonA_PARAMS','UTILITY')
!
      end subroutine SEST_LP_notonA_PARAMS
!
      subroutine SEST_LP_A_LP_A_PARAMS(alp, R_e)
!***********************************************************************
!     Date last modified: September 10, 2007                           *
!     Author: Joshua Hollett                                           *
!     Description: Takes a lone pair and returns with the parameters   *
!                  associated with two lone pairs on that atom type.   *
!***********************************************************************
!
! Modules:
      USE program_manager
      USE SEST_tools
      USE type_molecule
!
      implicit none
!
! Input:
      integer, intent(IN) :: alp
!
! Output:
      double precision, intent(OUT) :: R_e
!
!Begin: 
!
      call PRG_manager('enter','SEST_LP_A_LP_A_PARAMS','UTILITY')
!
      select case(trim(MOL_atoms(lp(alp)%atom)%type))
        case('sp3O')
        ! V_ee terms
          V_ee_alpblp=1.27934775D0
          r_ee_alpblp=-1.29178930D0
          R_e=1.00022604D0
        case('F')
        ! V_ee terms
          V_ee_alpblp=1.51146260D0
          r_ee_alpblp=-2.50000000D0
          R_e=0.85195369D0
        case default
          stop 'Error, atom pair not supported'
      end select
!
      call PRG_manager('exit','SEST_LP_A_LP_A_PARAMS','UTILITY')
!
      end subroutine SEST_LP_A_LP_A_PARAMS
!
      subroutine SEST_LP_A_LP_B_PARAMS(alp, blp, R_e)
!***********************************************************************
!     Date last modified: September 10, 2007                           *
!     Author: Joshua Hollett                                           *
!     Description: Takes two lone pairs from separate atoms and        *
!                  returns with the associated parameters.             *
!***********************************************************************
!
! Modules:
      USE program_manager
      USE SEST_tools
      USE type_molecule
!
      implicit none
!
! Input:
      integer, intent(IN) :: alp, blp
!
! Output:
      double precision, intent(OUT) :: R_e
!
! Local:
      character(len=10) :: ATOM_PAIR
      integer :: valency_A, valency_B
!
!Begin: 
!
      call PRG_manager('enter','SEST_LP_A_LP_B_PARAMS','UTILITY')
!
      valency_A=sum(SEST_BOND(lp(alp)%atom,1:molecule%Natoms)%connect)
      valency_B=sum(SEST_BOND(lp(blp)%atom,1:molecule%Natoms)%connect)
!
      if(MOL_atoms(lp(alp)%atom)%Atomic_number.eq.MOL_atoms(lp(blp)%atom)%Atomic_number)then
        if(valency_A.le.valency_B)then
          ATOM_PAIR=trim(MOL_atoms(lp(alp)%atom)%type)//trim(MOL_atoms(lp(blp)%atom)%type)
        else
          ATOM_PAIR=trim(MOL_atoms(lp(blp)%atom)%type)//trim(MOL_atoms(lp(alp)%atom)%type)
        end if
      else if(MOL_atoms(lp(alp)%atom)%Atomic_number.lt.MOL_atoms(lp(blp)%atom)%Atomic_number)then
        ATOM_PAIR=trim(MOL_atoms(lp(alp)%atom)%type)//trim(MOL_atoms(lp(blp)%atom)%type)
      else
        ATOM_PAIR=trim(MOL_atoms(lp(blp)%atom)%type)//trim(MOL_atoms(lp(alp)%atom)%type)
      end if
!
      select case(trim(atom_pair))
        case('FF')
        ! V_ee terms
          V_ee_alpblp=0.38308660D0
          r_ee_alpblp=0.00000000D0
          R_e=5.21213235D0
        case default
          stop 'Error, atom pair not supported'
      end select
!
      call PRG_manager('exit','SEST_LP_A_LP_B_PARAMS','UTILITY')
!
      end subroutine SEST_LP_A_LP_B_PARAMS
!
      subroutine SEST_COUNT_ELECTRONS(Xatom, N_pair_X, N_i_X)
!***********************************************************************
!     Date last modified: July 20, 2007                                *
!     Author: Joshua Hollett                                           *
!     Description: Counts and sorts electrons and electron pairs       *
!                  associated with a given atom type.                  *
!                  Note! now that lone pairs are seperate, the core    *
!                  electrons are the only pair on an atom.             *
!***********************************************************************
!
! Modules:
      USE program_manager
      USE SEST_tools
      USE type_molecule
!
      implicit none
!
! Input:
      integer, intent(IN) :: Xatom
! Output: 
      integer, intent(OUT) :: N_pair_X, N_i_X
!
! Begin:
!
      call PRG_manager('enter','SEST_COUNT_ELECTRONS','UTILITY')
!
      select case(trim(MOL_atoms(Xatom)%type))
        case('H')
        N_pair_X=0
        N_i_X=1
        case('F')
        N_pair_X=1
        N_i_X=1
        case('Be')
        N_pair_X=1
        N_i_X=2
        case('B')
        N_pair_X=1
        N_i_X=3
        case('sp3C')
        N_pair_X=1
        N_i_X=4
        case('sp3N')
        N_pair_X=1
        N_i_X=3
        case('sp3O')
        N_pair_X=1
        N_i_X=2
        case default
          write(6,'(a)')MOL_atoms(Xatom)%type
          stop 'Error, atom type not supported'
      end select
!
      call PRG_manager('exit','SEST_COUNT_ELECTRONS','UTILITY')
!
      end subroutine SEST_COUNT_ELECTRONS
!
      subroutine SEST_PES_PLOT
!***********************************************************************
!     Date last modified: May 28, 2007                                 *
!     Author: Joshua Hollett                                           *
!     Description: Plot a one parameter Potential Energy Surface.      *
!***********************************************************************
!Modules:
      USE program_manager
      USE SEST_tools
      USE type_molecule
!
      implicit none
!
! Local:
      integer :: system, retcod, I, Aatom
      double precision, allocatable, dimension(:) :: Xdata, Ydata
      double precision :: stepsize, E_high, R_high, R_e, c_v
      character(len=20) :: filename 
      character(len=300) :: command
!
! Begin:
!
      call PRG_manager('enter','SEST_PES_PLOT','UTILITY')
!
! Produce data for plotting
!
      allocate(Xdata(6000),Ydata(6000))
!
      c_v=2.001988665D0
!
      call SEST_COORD_TRANS(I)
!
! Now modified for scanning PES of lone pair of NH3
      stepsize=CARTESIAN(2)%Z/1000.0
      R_e=CARTESIAN(2)%Z
      CARTESIAN(2)%Z=stepsize
!      stepsize=lp(1)%Z/1000.0
!      R_e=lp(1)%Z-MOL_atoms(lp(1)%atom)%Z
!      lp(1)%Z=stepsize
      E_high=SEST_ENERGY%V*(c_v-1.0D0)/c_v
      do I=1,6000
        CARTESIAN(2)%Z=CARTESIAN(2)%Z+stepsize
        MOL_atoms(2)%Z=CARTESIAN(2)%Z
!        lp(1)%Z=lp(1)%Z+stepsize
        call SEST_DEFINE_BONDS
        Xdata(I)=SEST_BOND(2,1)%distance
!        Xdata(I)=lp(1)%Z-MOL_atoms(lp(1)%atom)%Z
!        Xdata(I)=Xdata(I)/R_e
        call SEST_ENERGY_CALC
        Ydata(I)=SEST_ENERGY%V*(c_v-1.0D0)/c_v
        if(I.gt.1000)then
          if(SEST_ENERGY%V/2.0D0.gt.E_high)then
            E_high=SEST_ENERGY%V*(c_v-1.0D0)/c_v
            R_high=SEST_BOND(2,1)%distance
!            R_high=lp(1)%Z-MOL_atoms(lp(1)%atom)%Z
          end if
        end if
      end do
!
      write(6,'(a,F16.8,a)')'Maximum energy of ',E_high, ' hartrees'
      write(6,'(a,F16.8,a)')'at R= ',R_high, ' bohr'
      write(6,'(a)')''
!
      write(6,'(a)')'Energy Components at last PES point'
      write(6,'(a,F16.8)')'V_NN= ',SEST_ENERGY%V_NN 
      write(6,'(a,F16.8)')'V_Ne_AB= ',SEST_ENERGY%V_Ne_AB 
      write(6,'(a,F16.8)')'V_ee_AB= ',SEST_ENERGY%V_ee_AB 
      write(6,'(a,F16.8)')'E= ',SEST_ENERGY%V/2.0D0 
!
! Copy Cartesian to molecule, the rest of this code uses type molecule(should change this to cartesian eventually)
      do Aatom=1,molecule%Natoms
        MOL_atoms(Aatom)%X=CARTESIAN(Aatom)%X
        MOL_atoms(Aatom)%Y=CARTESIAN(Aatom)%Y
        MOL_atoms(Aatom)%Z=CARTESIAN(Aatom)%Z
      end do
!
! Write data to file
      filename='X_vs_Y.dat'
!      filename='PES_plot_'//trim(molecule%name)//'.dat'
      open(4,FILE=filename, form='FORMATTED') ! open file to write data
      do I=1,6000
        write(4,'(F14.6,X,F14.6)')Xdata(I),Ydata(I)
      end do
      close(4)
!    
! The rest is only good for running on a PC
!
! Create batch file for Grace (Title, x-axis and y-axis labels)
!      filename='bfile'
!      open(4,FILE=filename, form='FORMATTED')
!      write(4,'(a)')'# Grace batch file' 
!      write(4,'(3a)')'title "MM representation of ',trim(molecule%name),' molecule"'
!      write(4,'(a)')'xaxis label "R"'
!      write(4,'(a)')'yaxis label "E"'
!      close(4)
!
! Execute gracebat to print eps graphs
!      command='gracebat -autoscale xy -p graph.par X_vs_Y.dat&
!             & -printfile X_vs_Y.eps -fixed $11 $8.5 -batch bfile'
! Can't use fixed $8.5 $11 command in Ubuntu
!      command='gracebat -autoscale xy -p /home/josh/epMM/plots_graphs_etc/graph.par X_vs_Y.dat&
!             & -printfile X_vs_Y.eps -batch bfile'
!      retcod=system(command)
!
! Produce pdf graphs if requested
!      if(pdf.eq.'y')then
!        command='epstopdf X_vs_Y.eps'
!        retcod=system(command)
!      end if
!
      call PRG_manager('exit','SEST_PES_PLOT','UTILITY')
!
      end subroutine SEST_PES_PLOT
!
      subroutine SEST_XYZ_to_OPT
!************************************************************************
!     Date last modified: November 5, 2007                              *
!     Author: Joshua Hollett                                            *
!     Description: Produces PARSET for cartesian optimization.  However *
!                  , in the case of SEST lone pair coordinates are       *
!                  included.                                            *
!************************************************************************
!Modules:
      USE program_manager
      USE SEST_tools
      USE type_molecule
      USE OPT_objects
      USE type_elements
!
      implicit none
!
! Local:
      integer :: Aatom, alp, I
      double precision :: TOLER
!
! Begin:
!
      call PRG_manager('enter','SEST_XYZ_to_OPT','UTILITY')
!
      TOLER=0.00000001D0
!
! Copy Cartesian to molecule, the rest of this code uses type molecule(should change this to cartesian eventually)
      do Aatom=1,molecule%Natoms
        MOL_atoms(Aatom)%symbol=trim(ELEMENT_SYMBOLS(CARTESIAN(Aatom)%Atomic_number))
        MOL_atoms(Aatom)%X=CARTESIAN(Aatom)%X
        MOL_atoms(Aatom)%Y=CARTESIAN(Aatom)%Y
        MOL_atoms(Aatom)%Z=CARTESIAN(Aatom)%Z
      end do
!
! Need valency of atoms with lone pairs
      call SEST_DEFINE_BONDS
! Determine number of lone pairs and their positions
      if(.not.allocated(LP))then
        call SEST_COUNT_LONE_PAIRS
        if(Nlp.gt.0)then
          call SEST_PUT_LONE_PAIRS
        end if
      end if
!
! Define Noptpr
      Noptpr=3*(molecule%Natoms+Nlp)
!
      if(allocated(PARSET))then
        deallocate(PARSET)
        allocate(PARSET(Noptpr)) 
      else 
        allocate(PARSET(Noptpr)) 
      end if  
!
! Define PARSET
! Atoms
      do Aatom=1,molecule%Natoms
        PARSET(3*Aatom-2)=CARTESIAN(Aatom)%X
        PARSET(3*Aatom-1)=CARTESIAN(Aatom)%Y
        PARSET(3*Aatom)=CARTESIAN(Aatom)%Z
      end do
! Lone pairs
      if(Nlp.gt.0)then
        alp=1
        do I=molecule%Natoms+1,molecule%Natoms+Nlp
          PARSET(3*I-2)=lp(alp)%X
          PARSET(3*I-1)=lp(alp)%Y
          PARSET(3*I)=lp(alp)%Z
          alp=alp+1
        end do
      end if
!
      if(.not.allocated(PAR_type))then
        allocate(PAR_type(Noptpr))
      end if
      do I=1,Noptpr
        PAR_type(I)=.true.
      end do ! I
!
!      write(6,'(a,F16.8)')'PARSET'
!      do I=1,Noptpr
!        write(6,'(F16.8)')PARSET(I)
!      end do
!      write(6,'(a)')''
!
      call PRG_manager('exit','SEST_XYZ_to_OPT','UTILITY')
!
      end subroutine SEST_XYZ_to_OPT
!
      subroutine SEST_OPT_to_XYZ
!************************************************************************
!     Date last modified: November 5, 2007                              *
!     Author: Joshua Hollett                                            *
!     Description: Updates cartesians from PARSET.  However, in the     *
!                  case of SEST lone pair coordinates are included.      *
!************************************************************************
!Modules:
      USE program_manager
      USE SEST_tools
      USE type_molecule
      USE OPT_objects
!
      implicit none
!
! Local:
      integer :: Aatom, Iq, alp
!
! Begin:
!
      call PRG_manager('enter','SEST_OPT_to_XYZ','UTILITY')
!
! Copy nuclear coordinates back
      Iq=1
      do Aatom=1,molecule%Natoms
        MOL_atoms(Aatom)%X=PARSET(Iq)
        MOL_atoms(Aatom)%Y=PARSET(Iq+1)
        MOL_atoms(Aatom)%Z=PARSET(Iq+2)
        CARTESIAN(Aatom)%X=PARSET(Iq)
        CARTESIAN(Aatom)%Y=PARSET(Iq+1)
        CARTESIAN(Aatom)%Z=PARSET(Iq+2)
        Iq=Iq+3
      end do
! Lone pairs
      if(Nlp.gt.0)then
        Iq=Noptpr-3*Nlp+1
        do alp=1,Nlp
          lp(alp)%X=PARSET(Iq)
          lp(alp)%Y=PARSET(Iq+1)
          lp(alp)%Z=PARSET(Iq+2)
          Iq=Iq+3
        end do
      end if
!
!      write(6,'(a,F16.8)')'PARSET'
!      do Iq=1,Noptpr
!        write(6,'(F16.8)')PARSET(Iq)
!      end do
!      write(6,'(a)')''
!
      call PRG_manager('exit','SEST_OPT_to_XYZ','UTILITY')
!
      end subroutine SEST_OPT_to_XYZ
!
      subroutine SEST_XYZ_GRD_OPT
!************************************************************************
!     Date last modified: November 5, 2007                              *
!     Author: Joshua Hollett                                            *
!     Description: Creates PARGRD for optimization.  However, in the    *
!                  case of SEST lone pair coordinates are included.      *
!************************************************************************
!Modules:
      USE program_manager
      USE SEST_tools
      USE type_molecule
      USE OPT_objects
      USE type_elements
!
      implicit none
!
! Local:
      double precision, allocatable, dimension(:,:) :: H_all 
      integer :: Aatom, Ioptpr
!
! Begin:
!
      call PRG_manager('enter','SEST_XYZ_GRD_OPT','UTILITY')
!
! Copy Cartesian to molecule, the rest of this code uses type molecule(should change this to cartesian eventually)
      do Aatom=1,molecule%Natoms
        MOL_atoms(Aatom)%symbol=trim(ELEMENT_SYMBOLS(CARTESIAN(Aatom)%Atomic_number))
        MOL_atoms(Aatom)%X=CARTESIAN(Aatom)%X
        MOL_atoms(Aatom)%Y=CARTESIAN(Aatom)%Y
        MOL_atoms(Aatom)%Z=CARTESIAN(Aatom)%Z
      end do
!
      call SEST_DEFINE_BONDS
!
      allocate(H_all(Noptpr,Noptpr))
!
      if(allocated(PARGRD))then
        deallocate(PARGRD) 
        allocate(PARGRD(Noptpr))
      else
        allocate(PARGRD(Noptpr))
      end if
!
      call SEST_CALC_DERIVS(PARGRD,H_all)
!
      PARGRD(1:Noptpr)=PARGRD(1:Noptpr)/2.0D0
!
!      write(6,'(a)')'PARGRD'
!      do Ioptpr=1,Noptpr  
!        write(6,'(F14.8)')PARGRD(Ioptpr)
!      end do 
!
      call PRG_manager('exit','SEST_XYZ_GRD_OPT','UTILITY')
!
      end subroutine SEST_XYZ_GRD_OPT
!
      subroutine SEST_HESSIAN
!************************************************************************
!     Date last modified: November 5, 2007                              *
!     Author: Joshua Hollett                                            *
!     Description: Calculates the analytical SEST Hessian.               *
!************************************************************************
!Modules:
      USE program_manager
      USE SEST_tools
      USE type_molecule
      USE OPT_objects
      USE mod_type_hessian
      USE type_elements
!
      implicit none
!
! Local:
      double precision, allocatable, dimension(:) :: g_all 
      integer :: Aatom
!
! Begin:
!     
      call PRG_Manager('enter','SEST_HESSIAN','UTILITY') 
!
!
! Copy Cartesian to molecule, the rest of this code uses type molecule(should change this to cartesian eventually)
      do Aatom=1,molecule%Natoms
        MOL_atoms(Aatom)%symbol=trim(ELEMENT_SYMBOLS(CARTESIAN(Aatom)%Atomic_number))
        MOL_atoms(Aatom)%X=CARTESIAN(Aatom)%X
        MOL_atoms(Aatom)%Y=CARTESIAN(Aatom)%Y
        MOL_atoms(Aatom)%Z=CARTESIAN(Aatom)%Z
      end do
!
      call SEST_DEFINE_BONDS
!
      allocate(g_all(Noptpr))
!
      if(allocated(Hessian_matrix))then
        deallocate(Hessian_matrix) 
        allocate(Hessian_matrix(Noptpr,Noptpr))
      else
        allocate(Hessian_matrix(Noptpr,Noptpr))
      end if
!
      call SEST_CALC_DERIVS(g_all,Hessian_matrix)
!
      call PRG_manager('exit','SEST_Hessian','UTILITY')
!
      end subroutine SEST_Hessian
!      
      subroutine SEST_mingrad_TO_OPT
!************************************************************************
!     Date last modified: May 19, 2003                     Version 2.0  *
!     Author: R.A. Poirier                                              *
!     Description: Initialize parameters for minimization (i.e. g=0)    *
!                  of the SEST gradient with undefined function          *
!                  parameters.                                          *
!************************************************************************
! Modules:
      USE program_manager
      USE OPT_objects
      USE SEST_tools
      USE type_molecule

      implicit none
!
! Local scalars:
      integer :: I, N_set, Aatom, Batom, alp, a, b, j, I_OPT_AB, I_OPT_AlpB
      integer :: N_pair_A, N_i_A, N_pair_B, N_i_B, valency_A, valency_B, temp_pair, temp_i, N_A, N_B
      character(len=10) :: OPT_AB_type, AB_type, BA_type, OPT_AlpB_type, AlpB_type
      logical :: AB_found, AlpB_found, AeqB
! Begin:
      call PRG_manager ('enter', 'SEST_mingrad_TO_OPT', 'UTILITY')
!
! Give the Hartree-Fock energy
      E_HF=-199.85127623D0
! Get energy values which remain constant during gradient minimization(V_NN, V_A)
! Also initialize arrays such as lone pairs, emm_bond etc.
      call E_SEST
! Give Number of undefined interactions
      N_AB_und=1
      N_AlpB_und=0
!
! Describe AB atom pairs with undefined parameters(ensure valency of A and Z_A are less than valency of B and Z_B)
! To assign which specific r values are optimized edit PARAMS_OPT
!
      if(N_AB_und.gt.0)then
        allocate(SEST_OPT_AB(N_AB_und))
      end if
      if(N_AlpB_und.gt.0)then
        allocate(SEST_OPT_AlpB(N_AlpB_und))
      end if
!
      SEST_OPT_AB(1)%Aatom_type='H'
      SEST_OPT_AB(1)%Batom_type='F'
      SEST_OPT_AB(1)%connect=0 ! or 1
      SEST_OPT_AB(1)%set=.false.
!
!      SEST_OPT_AB(2)%Aatom_type='F'
!      SEST_OPT_AB(2)%Batom_type='F'
!      SEST_OPT_AB(2)%connect=0 ! or 1
!      SEST_OPT_AB(2)%set=.false.
!
!      SEST_OPT_AlpB(1)%Aatom_type='F'
!      SEST_OPT_AlpB(1)%Batom_type='F'
!      SEST_OPT_AlpB(1)%connect=0 ! or 1
!      SEST_OPT_AlpB(1)%set=.false.
!
! Count parameters to be optimized, and therefore put in PARSET
!
      Noptpr=0 
! Need to cycle through atoms
      if(N_AB_und.gt.0)then
      Aatom_loop: do Aatom=2,molecule%Natoms
        Batom_loop: do Batom=1,Aatom-1
!          
! First check if all undefined OPT_AB pairs have been found and optimization parameters set
          N_set=0
          do I=1,N_AB_und
            if(.not.SEST_OPT_AB(I)%set) exit
            N_set=N_set+1
          end do
!
          if(N_set.eq.N_AB_und)exit Aatom_loop
!
          AB_type=trim(MOL_atoms(Aatom)%type)//trim(MOL_atoms(Batom)%type)
          BA_type=trim(MOL_atoms(Batom)%type)//trim(MOL_atoms(Aatom)%type)
          AB_found=.false.
          do I=1,N_AB_und
            OPT_AB_type=trim(SEST_OPT_AB(I)%Aatom_type)//trim(SEST_OPT_AB(I)%Batom_type)
            if((SEST_BOND(Aatom,Batom)%connect.eq.1).and.(SEST_OPT_AB(I)%connect.eq.1))then
              if(SEST_BOND(Aatom,Batom)%type.eq.OPT_AB_type)then
                SEST_OPT_AB(I)%set=.true.
                AB_found=.true.
                I_OPT_AB=I
                exit
              end if
            else if((SEST_BOND(Aatom,Batom)%connect.eq.0).and.(SEST_OPT_AB(I)%connect.eq.0))then
              if((AB_type.eq.OPT_AB_type).or.(BA_type.eq.OPT_AB_type))then
                SEST_OPT_AB(I)%set=.true.
                AB_found=.true.
                I_OPT_AB=I
                exit
              end if
            end if ! connected or not
          end do ! N_AB_und
!         
! Get another AB pair if it is not an interaction that needs to be optimized(i.e. in SEST_OPT_AB)        
          if(.not.AB_found)cycle
!          write(6,'(a,a)')'AB_found: ',OPT_AB_type
!
          call SEST_COUNT_ELECTRONS(Aatom, N_pair_A, N_i_A)
          call SEST_COUNT_ELECTRONS(Batom, N_pair_B, N_i_B)
! Order atom A and B
          valency_A=sum(SEST_BOND(Aatom,1:molecule%Natoms)%connect)
          valency_B=sum(SEST_BOND(Batom,1:molecule%Natoms)%connect)
          if(MOL_atoms(Aatom)%Atomic_number.eq.MOL_atoms(Batom)%Atomic_number)then
            if(valency_B.lt.valency_A)then
              temp_pair=N_pair_A
              temp_i=N_i_A
              N_pair_A=N_pair_B
              N_i_A=N_i_B
              N_pair_B=temp_pair
              N_i_B=temp_i
            end if
          else if(MOL_atoms(Batom)%Atomic_number.lt.MOL_atoms(Aatom)%Atomic_number)then
            temp_pair=N_pair_A
            temp_i=N_i_A
            N_pair_A=N_pair_B
            N_i_A=N_i_B
            N_pair_B=temp_pair
            N_i_B=temp_i
          end if
!
! Allocate parameter arrays
          if((N_pair_A.eq.0).and.(N_pair_B.eq.0))then
            N_A=1
            N_B=1
          else if(N_pair_A.eq.0)then
            N_A=1
            N_B=N_pair_B
          else if(N_pair_B.eq.0)then
            N_A=N_pair_A
            N_B=1
          else
            N_A=N_pair_A
            N_B=N_pair_B
          end if
!
          allocate(r_Ne_ABa_opt(1:N_B),r_Ne_BAa_opt(1:N_A),r_Ne_ABi_opt(1:N_i_B),r_Ne_BAi_opt(1:N_i_A))
          allocate(r_ee_AaBb_opt(1:N_A,1:N_B),r_ee_AaBi_opt(1:N_A,1:N_i_B),r_ee_BbAi_opt(1:N_B,1:N_i_A))
          allocate(r_ee_AiBj_opt(1:N_i_A,1:N_i_B))
!
          if(SEST_BOND(Aatom,Batom)%connect.eq.1)then
            call SEST_OPT_BOND_PARAMS(SEST_BOND(Aatom,Batom)%type)
          else
            call SEST_OPT_NON_BOND_PARAMS(Aatom,Batom)
          end if
!
! Cycle through parameters and count those to be optimized
          SEST_OPT_AB(I_OPT_AB)%I_und=Noptpr+1
! Check if Aatom is same type as Batom, want parameters to occur just once in PARSET
          AeqB=.false.
          if(trim(SEST_OPT_AB(I_OPT_AB)%Aatom_type).eq.trim(SEST_OPT_AB(I_OPT_AB)%Batom_type)) AeqB=.true.
!
! r_Ne_BAa
          if(N_pair_A.gt.0)then
            do a=1,N_pair_A
              if(r_Ne_BAa_opt(a))Noptpr=Noptpr+1
            end do
          end if
! r_Ne_ABa
          if(.not.AeqB)then
            if(N_pair_B.gt.0)then
              do a=1,N_pair_B
                if(r_Ne_ABa_opt(a))Noptpr=Noptpr+1
              end do
            end if
          end if
! r_Ne_BAi
          do i=1,N_i_A
            if(r_Ne_BAi_opt(i))Noptpr=Noptpr+1
          end do
! r_Ne_ABi
          if(.not.AeqB)then
            do i=1,N_i_B
              if(r_Ne_ABi_opt(i))Noptpr=Noptpr+1
            end do
          end if
! r_ee_AaBb
          if(N_pair_A.gt.0)then
            do a=1,N_pair_A
              if(N_pair_B.gt.0)then ! r_ee_AaBb
                do b=1,N_pair_B
                  if(r_ee_AaBb_opt(a,b))Noptpr=Noptpr+1
                end do
              end if
! r_ee_AaBi
              do i=1,N_i_B ! r_ee_AaBi
                if(r_ee_AaBi_opt(a,i))Noptpr=Noptpr+1
              end do
            end do
          end if
! r_ee_BbAi
          if(.not.AeqB)then
            if(N_pair_B.gt.0)then
              do b=1,N_pair_B
                do i=1,N_i_A ! r_ee_BbAi
                  if(r_ee_BbAi_opt(b,i))Noptpr=Noptpr+1
                end do
              end do
            end if
          end if
! r_ee_AiBj
          do i=1,N_i_A
            do j=1,N_i_B ! r_ee_AiBj
              if(r_ee_AiBj_opt(i,j))Noptpr=Noptpr+1
            end do
          end do
!
!          write(6,'(a,I2)')'Noptpr= ', Noptpr
!
          deallocate(r_Ne_ABa_opt,r_Ne_BAa_opt,r_Ne_ABi_opt,r_Ne_BAi_opt)
          deallocate(r_ee_AaBb_opt,r_ee_AaBi_opt,r_ee_BbAi_opt)
          deallocate(r_ee_AiBj_opt)
!
        end do Batom_loop
      end do Aatom_loop
      end if ! N_AB_und .gt. 0
!
! Now cycle through Atoms with lone pairs
      if(Nlp.gt.0)then
      if(N_AlpB_und.gt.0)then
        Aatom_loop2: do Aatom=1,molecule%Natoms
!
          call SEST_COUNT_ELECTRONS(Aatom, N_pair_A, N_i_A)
!
          if(N_pair_A.eq.0)then
            N_A=1
          else
            N_A=N_pair_A
          end if
!
          allocate(r_ee_Aalp_opt(1:N_A),r_ee_Ailp_opt(1:N_i_A))
!
          alp_loop: do alp=1,Nlp
!
            if(lp(alp)%atom.eq.Aatom)cycle ! Won't be optimizing lone pairs with their own atom, at least not now
!
! First check if all undefined OPT_AlpB pairs have been found and optimization parameters set
            N_set=0
            do I=1,N_AlpB_und
              if(.not.SEST_OPT_AlpB(I)%set) exit
              N_set=N_set+1
            end do
!
            if(N_set.eq.N_AlpB_und)then
              deallocate(r_ee_Aalp_opt,r_ee_Ailp_opt)
              exit Aatom_loop2
            end if
!
            AlpB_type=trim(MOL_atoms(Aatom)%type)//trim(lp(alp)%atom_type)
            AlpB_found=.false.
            do I=1,N_AlpB_und
              OPT_AlpB_type=trim(SEST_OPT_AlpB(I)%Aatom_type)//trim(SEST_OPT_AlpB(I)%Batom_type)
              if(AlpB_type.eq.OPT_AlpB_type)then
                SEST_OPT_AlpB(I)%set=.true.
                AlpB_found=.true.
                I_OPT_AlpB=I
                exit
              end if
            end do ! N_AB_und
!
! Get another AB pair if it is not an interaction that needs to be optimized(i.e. in SEST_OPT_AB)
            if(.not.AlpB_found)cycle
!            write(6,'(a,a)')'AlpB_found: ', OPT_AlpB_type
!
            call SEST_OPT_LP_notonA_PARAMS(alp, Aatom)
!
! Cycle through parameters and count those to be optimized
            SEST_OPT_AlpB(I_OPT_AlpB)%I_und=Noptpr+1
!r_Ne_Alp(B)
            if(r_Ne_Alp_opt)Noptpr=Noptpr+1
!r_ee_Aalp(B)
            if(N_pair_A.gt.0)then
              do a=1,N_pair_A
                if(r_ee_Aalp_opt(a))Noptpr=Noptpr+1
              end do
            end if
!r_ee_Ailp(B)
            do i=1,N_i_A
              if(r_ee_Ailp_opt(i))Noptpr=Noptpr+1
            end do
!
!          write(6,'(a,I2)')'Noptpr= ', Noptpr
!
          end do alp_loop
!
          deallocate(r_ee_Aalp_opt,r_ee_Ailp_opt)
!
        end do Aatom_loop2
      end if ! N_AlpB_und
      end if! Nlp
!
      if(N_AB_und.gt.0)then
        do I=1,N_AB_und
          write(6,'(a,I1,a,I0)')'SEST_OPT_AB(',I,')%I_und= ', SEST_OPT_AB(I)%I_und
        end do
      end if
      if(N_AlpB_und.gt.0)then
        do I=1,N_AlpB_und
          write(6,'(a,I1,a,I0)')'SEST_OPT_AlpB(',I,')%I_und= ', SEST_OPT_AlpB(I)%I_und
        end do
      end if
!
      if(.not.allocated(PARSET))then
        allocate (PARSET(Noptpr))
        allocate (GA_domain(Noptpr))
        PARSET(1)=-1.7953740D+00
        PARSET(2)=2.0643772D+00
        PARSET(3)=-5.2940352D-01
        PARSET(4)=-1.5722049D+00
        GA_domain(1:Noptpr)%high=1.0D0
        GA_domain(1:Noptpr)%low=-100.0D0
      end if
!
! End of routine SEST_mingrad_TO_OPT
      call PRG_manager ('exit', 'SEST_mingrad_TO_OPT', 'UTILITY')
      end
!
      subroutine SEST_OPT_BOND_PARAMS(SEST_BOND_type)
!***********************************************************************
!     Date last modified: February 21, 2008                            *
!     Author: Joshua Hollett                                           *
!     Description: Takes the bond type and returns with the            *
!                  parameters associated with that bond type to be     *
!                  optimized.                                          *
!***********************************************************************
!
! Modules:
      USE program_manager
      USE SEST_tools
!
      implicit none
!
! Input:
      character(len=10), intent(IN) :: SEST_BOND_type
!
!Begin:
!
      call PRG_manager('enter','SEST_OPT_BOND_PARAMS','UTILITY')
!
      select case(trim(SEST_BOND_type))
        case default
          stop 'Error, bond type not supported'
      end select
!
      call PRG_manager('exit','SEST_OPT_BOND_PARAMS','UTILITY')
!
      end subroutine SEST_OPT_BOND_PARAMS
!
      subroutine SEST_OPT_NON_BOND_PARAMS(Aatom, Batom)
!***********************************************************************
!     Date last modified: February 21, 2008                            *
!     Author: Joshua Hollett                                           *
!     Description: Takes the atom pair and returns with the            *
!                  parameters associated with that atom pair to be     *
!                  optimized.                                          *
!***********************************************************************
!
! Modules:
      USE program_manager
      USE SEST_tools
      USE type_molecule
!
      implicit none
!
! Input:
      integer, intent(IN) :: Aatom, Batom
!
! Local:
      character(len=10) :: ATOM_PAIR
      integer :: valency_A, valency_B
!
!Begin:
!
      call PRG_manager('enter','SEST_OPT_NON_BOND_PARAMS','UTILITY')
!
      valency_A=sum(SEST_BOND(Aatom,1:molecule%Natoms)%connect)
      valency_B=sum(SEST_BOND(Batom,1:molecule%Natoms)%connect)
!
      if(MOL_atoms(Aatom)%Atomic_number.eq.MOL_atoms(Batom)%Atomic_number)then
        if(valency_A.le.valency_B)then
          ATOM_PAIR=trim(MOL_atoms(Aatom)%type)//trim(MOL_atoms(Batom)%type)
        else
          ATOM_PAIR=trim(MOL_atoms(Batom)%type)//trim(MOL_atoms(Aatom)%type)
        end if
      else if(MOL_atoms(Aatom)%Atomic_number.lt.MOL_atoms(Batom)%Atomic_number)then
        ATOM_PAIR=trim(MOL_atoms(Aatom)%type)//trim(MOL_atoms(Batom)%type)
      else
        ATOM_PAIR=trim(MOL_atoms(Batom)%type)//trim(MOL_atoms(Aatom)%type)
      end if
!
      select case(trim(ATOM_PAIR))
        case('HH')
        !A=H B=H
        ! Ne terms
          r_Ne_ABa_opt(1:1)=.false.
          r_Ne_BAa_opt(1:1)=.false.
          r_Ne_ABi_opt(1)=.false. ! r_Ne_HHj
          r_Ne_BAi_opt(1)=.false. ! r_Ne_HHj
        ! ee terms
          r_ee_AaBb_opt(1:1,1:1)=.false.
          r_ee_AaBi_opt(1:1,1:1)=.false.
          r_ee_BbAi_opt(1:1,1:1)=.false.
          r_ee_AiBj_opt(1,1)=.false. ! r_ee_HkHl
        case('HF')
        !A=H B=F 
        ! V_Ne terms
          r_Ne_ABa_opt(1)=.false. ! r_Ne_HFcore
          r_Ne_BAa_opt(1:1)=.false.
          r_Ne_ABi_opt(1)=.true. ! r_Ne_HFj
          r_Ne_BAi_opt(1)=.true. ! r_Ne_FHj
        ! V_ee terms
          r_ee_AaBb_opt(1:1,1:1)=.false.
          r_ee_AaBi_opt(1:1,1:1)=.false.
          r_ee_BbAi_opt(1,1)=.true. ! r_ee_FcoreHk
          r_ee_AiBj_opt(1,1)=.true. ! V_ee_HkFl
        case('FF')
        ! V_Ne terms
          r_Ne_ABa_opt(1)=.true. ! r_Ne_FFcore
          r_Ne_BAa_opt(1)=.true. ! r_Ne_FFcore
          r_Ne_ABi_opt(1)=.true. ! r_Ne_FFj
          r_Ne_BAi_opt(1)=.true. ! r_Ne_FFj
        ! V_ee terms
          r_ee_AaBb_opt(1,1)=.true. ! r_ee_FcoreFcore
          r_ee_AaBi_opt(1,1)=.true. ! r_ee_FcoreFk
          r_ee_BbAi_opt(1,1)=.true. ! r_ee_FcoreFk
          r_ee_AiBj_opt(1,1)=.true. ! r_ee_FkFl
        case default
          write(6,'(2a)')'ATOM_PAIR: ',ATOM_PAIR
          stop 'Error, atom pair not supported'
      end select
!
      call PRG_manager('exit','SEST_OPT_NON_BOND_PARAMS','UTILITY')
!
      end subroutine SEST_OPT_NON_BOND_PARAMS
!
      subroutine SEST_OPT_LP_notonA_PARAMS(alp, Aatom)
!***********************************************************************
!     Date last modified: August 17, 2007                              *
!     Author: Joshua Hollett                                           *
!     Description: Takes a lone pair and an atom and returns with the  *
!                  associated parameters to be optimized.              *
!***********************************************************************
!
! Modules:
      USE program_manager
      USE SEST_tools
      USE type_molecule
!
      implicit none
!
! Input:
      integer, intent(IN) :: alp, Aatom
!
! Local:
      character(len=10) :: ATOM_PAIR
!
!Begin:
!
      call PRG_manager('enter','SEST_OPT_LP_notonA_PARAMS','UTILITY')
!
      ATOM_PAIR=trim(MOL_atoms(lp(alp)%atom)%type)//trim(MOL_atoms(Aatom)%type)
      select case(trim(ATOM_PAIR))
        case('FH')
        ! Ne term
          r_Ne_Alp_opt=.true. !r_Ne_Hlp(F)
        ! ee terms
          r_ee_Aalp_opt(1:1)=.false.
          r_ee_Ailp_opt(1)=.true. !r_ee_Hilp(F)
        case('FF')
        ! Ne term
          r_Ne_Alp_opt=.true. ! r_Ne_Flp(F)
        ! ee terms
          r_ee_Aalp_opt(1)=.true. ! r_ee_Fcorelp(F)
          r_ee_Ailp_opt(1)=.true. ! r_ee_Filp(F)
        case default
          write(6,'(2a)')'ATOM_PAIR: ',ATOM_PAIR
          stop 'Error, atom pair not supported'
      end select
!
      call PRG_manager('exit','SEST_OPT_LP_notonA_PARAMS','UTILITY')
!
      end subroutine SEST_OPT_LP_notonA_PARAMS
!
      subroutine SEST_ENERGY_OPT_PARAMS(E_MM_def, E_MM_und)
!***********************************************************************
!     Date last modified: April 24, 2007                               *
!     Author: Joshua Hollett                                           *
!     Description: Calculate the energy associated with bonding in     *
!                  the molecule.  However, keep the energy divided     *
!                  into defined parameter and undefined paramter       *
!                  contributions.                                      *
!***********************************************************************
! Modules:
      USE program_manager
      USE SEST_tools
      USE type_molecule
      USE OPT_objects
!
      implicit none
!
! Output:
      double precision, intent(OUT) :: E_MM_def, E_MM_und
!    
! Local:
      integer :: Aatom, Batom, N_pair_A, N_pair_B, N_i_A, N_i_B, a, b, i, j, alp, blp
      integer :: N_A, N_B, BOND_ORDER, valency_A, valency_B, temp_pair, temp_i, I_und
      integer :: I_OPT_AB, I_OPT_AlpB, I_temp
      double precision :: R_e, R, max_r, delta
      character(len=10) :: AB_type, BA_type, OPT_AB_type, AlpB_type, OPT_AlpB_type
      logical :: OPT_AB, OPT_AlpB, AeqB
!
! Begin:
!
      call PRG_manager('enter','SEST_ENERGY_OPT_PARAMS','UTILITY')
!
! Set delta value for piecewise functions
      delta=0.02
!
      SEST_ENERGY%V_ee_AB=0.00000000D0
      SEST_ENERGY%V_Ne_AB=0.00000000D0
      SEST_ENERGY%V_Ne_Alp=0.00000000D0
      SEST_ENERGY%V_ee_Alp=0.00000000D0
      SEST_ENERGY%V_ee_lplp=0.00000000D0
      SEST_def%V_ee_AB=0.00000000D0
      SEST_def%V_Ne_AB=0.00000000D0
      SEST_def%V_Ne_Alp=0.00000000D0
      SEST_def%V_ee_Alp=0.00000000D0
      SEST_def%V_ee_lplp=0.00000000D0
      SEST_und%V_ee_AB=0.00000000D0
      SEST_und%V_Ne_AB=0.00000000D0
      SEST_und%V_Ne_Alp=0.00000000D0
      SEST_und%V_ee_Alp=0.00000000D0
      SEST_und%V_ee_lplp=0.00000000D0
      E_MM_def=0.00000000D0
      E_MM_und=0.00000000D0
      do Aatom=2,molecule%Natoms
        do Batom=1,Aatom-1
!
          call SEST_COUNT_ELECTRONS(Aatom, N_pair_A, N_i_A)
          call SEST_COUNT_ELECTRONS(Batom, N_pair_B, N_i_B)
! Order atom A and B
          valency_A=sum(SEST_BOND(Aatom,1:molecule%Natoms)%connect)
          valency_B=sum(SEST_BOND(Batom,1:molecule%Natoms)%connect)
          if(MOL_atoms(Aatom)%Atomic_number.eq.MOL_atoms(Batom)%Atomic_number)then
            if(valency_B.lt.valency_A)then
              temp_pair=N_pair_A
              temp_i=N_i_A
              N_pair_A=N_pair_B
              N_i_A=N_i_B
              N_pair_B=temp_pair
              N_i_B=temp_i
            end if
          else if(MOL_atoms(Batom)%Atomic_number.lt.MOL_atoms(Aatom)%Atomic_number)then
            temp_pair=N_pair_A
            temp_i=N_i_A
            N_pair_A=N_pair_B
            N_i_A=N_i_B
            N_pair_B=temp_pair
            N_i_B=temp_i
          end if
!
! Allocate parameter arrays
          if((N_pair_A.eq.0).and.(N_pair_B.eq.0))then
            N_A=1
            N_B=1
          else if(N_pair_A.eq.0)then
            N_A=1
            N_B=N_pair_B
          else if(N_pair_B.eq.0)then
            N_A=N_pair_A
            N_B=1
          else
            N_A=N_pair_A
            N_B=N_pair_B
          end if
!
          allocate(V_Ne_ABa(1:N_B),V_Ne_BAa(1:N_A),V_Ne_ABi(1:N_i_B),V_Ne_BAi(1:N_i_A))
          allocate(r_Ne_ABa(1:N_B),r_Ne_BAa(1:N_A),r_Ne_ABi(1:N_i_B),r_Ne_BAi(1:N_i_A))
          allocate(V_ee_AaBb(1:N_A,1:N_B),V_ee_AaBi(1:N_A,1:N_i_B),V_ee_BbAi(1:N_B,1:N_i_A))
          allocate(r_ee_AaBb(1:N_A,1:N_B),r_ee_AaBi(1:N_A,1:N_i_B),r_ee_BbAi(1:N_B,1:N_i_A))
          allocate(V_ee_AiBj(1:N_i_A,1:N_i_B),r_ee_AiBj(1:N_i_A,1:N_i_B))
!
! Determine if AB interaction has parameters to be optimized
          AB_type=trim(MOL_atoms(Aatom)%type)//trim(MOL_atoms(Batom)%type)
          BA_type=trim(MOL_atoms(Batom)%type)//trim(MOL_atoms(Aatom)%type)
          OPT_AB=.false.
          do I=1,N_AB_und
            OPT_AB_type=trim(SEST_OPT_AB(I)%Aatom_type)//trim(SEST_OPT_AB(I)%Batom_type)
            if(((SEST_BOND(Aatom,Batom)%connect.eq.1).and.(SEST_OPT_AB(I)%connect.eq.1))&
            .or.((SEST_BOND(Aatom,Batom)%connect.eq.0).and.(SEST_OPT_AB(I)%connect.eq.0)))then 
              if((AB_type.eq.OPT_AB_type).or.(BA_type.eq.OPT_AB_type))then
                OPT_AB=.true.
!                write(6,'(a,a)')'interaction to be optimized: ', OPT_AB_type
                I_OPT_AB=I
                exit
              end if
            end if ! both bonding or non-bonding
          end do ! N_AB_und
!         
          if(OPT_AB)then
! Logical arrays that determine whether parameter is defined or undefined(part of PARSET)
            allocate(r_Ne_ABa_opt(1:N_B),r_Ne_BAa_opt(1:N_A),r_Ne_ABi_opt(1:N_i_B),r_Ne_BAi_opt(1:N_i_A))
            allocate(r_ee_AaBb_opt(1:N_A,1:N_B),r_ee_AaBi_opt(1:N_A,1:N_i_B),r_ee_BbAi_opt(1:N_B,1:N_i_A))
            allocate(r_ee_AiBj_opt(1:N_i_A,1:N_i_B))
          end if 
!
          if(SEST_BOND(Aatom,Batom)%connect.eq.1)then
            call SEST_BOND_PARAMS(SEST_BOND(Aatom,Batom)%type,BOND_ORDER,R_e)
            if(OPT_AB)then
              call SEST_OPT_BOND_PARAMS(SEST_BOND(Aatom,Batom)%type)
            end if 
          else
            call SEST_NON_BOND_PARAMS(Aatom,Batom,R_e)
            if(OPT_AB)then
              call SEST_OPT_NON_BOND_PARAMS(Aatom,Batom)
            end if
            BOND_ORDER=0
          end if
!
          R=SEST_BOND(Aatom,Batom)%distance
!
! To remove singularities, energy functions are now piecewise
! Find maximum r
          max_r=0.00000000D0
! r_Ne_BAa
          if(N_pair_A.gt.0)then
            do a=1,N_pair_A
              if(r_Ne_BAa(a).gt.max_r)then
                max_r=r_Ne_BAa(a)
              end if
            end do
          end if
! r_Ne_ABa
          if(N_pair_B.gt.0)then
            do a=1,N_pair_B
              if(r_Ne_ABa(a).gt.max_r)then
                max_r=r_Ne_ABa(a)
              end if
            end do
          end if
! r_Ne_BAi
          do i=1,N_i_A
            if(r_Ne_BAi(i).gt.max_r)then
              max_r=r_Ne_BAi(i)
            end if
          end do
! r_Ne_ABi
          do i=1,N_i_B
            if(r_Ne_ABi(i).gt.max_r)then
              max_r=r_Ne_ABi(i)
            end if
          end do
! r_ee_AaBb
          if(N_pair_A.gt.0)then
            do a=1,N_pair_A
              if(N_pair_B.gt.0)then ! r_ee_AaBb
                do b=1,N_pair_B
                  if(r_ee_AaBb(a,b).gt.max_r)then
                    max_r=r_ee_AaBb(a,b)
                  end if
                end do
              end if
! r_ee_AaBi
              do i=1,N_i_B ! r_ee_AaBi
                if(r_ee_AaBi(a,i).gt.max_r)then
                  max_r=r_ee_AaBi(a,i)
                end if
              end do
            end do
          end if
! r_ee_BbAi
          if(N_pair_B.gt.0)then
            do b=1,N_pair_B
              do i=1,N_i_A ! r_ee_BbAi
                if(r_ee_BbAi(b,i).gt.max_r)then
                  max_r=r_ee_BbAi(b,i)
                end if
              end do
            end do
          end if
! r_ee_AiBj
          do i=1,N_i_A
            do j=1,N_i_B ! r_ee_AiBj
              if(r_ee_AiBj(i,j).gt.max_r)then
                max_r=r_ee_AiBj(i,j)
              end if
            end do
          end do
!
          if(R.le.(max_r+delta))then
            R=max_r+delta
          end if
!
! If optimizing AB parameters then use PARSET in energy calculation
          if(OPT_AB)then 
            I_und=SEST_OPT_AB(I_OPT_AB)%I_und
! Check if Aatom is same type as Batom(redundant parameters)
            AeqB=.false.
            if(trim(SEST_OPT_AB(I_OPT_AB)%Aatom_type).eq.trim(SEST_OPT_AB(I_OPT_AB)%Batom_type)) AeqB=.true.
!
! Calculate electron-nuclear attraction potential energy (V_Ne_AB)
            if(N_pair_A.gt.0)then ! V_Ne for pairs on A with B
              if(AeqB) I_temp=I_und ! avoids storing parameters twice in PARSET
              do a=1,N_pair_A
                if(r_Ne_BAa_opt(a))then
                  SEST_ENERGY%V_Ne_AB=SEST_ENERGY%V_Ne_AB+2.0D0*V_Ne_BAa(a)*(R_e-PARSET(I_und))/(R-PARSET(I_und))
                  SEST_und%V_Ne_AB=SEST_und%V_Ne_AB+2.0D0*V_Ne_BAa(a)*(R_e-PARSET(I_und))/(R-PARSET(I_und))
                  E_MM_und=E_MM_und+2.0D0*V_Ne_BAa(a)*(R_e-PARSET(I_und))/(R-PARSET(I_und))
                  I_und=I_und+1
!                  write(6,'(a,I1,a)')'r_Ne_BAa(',a,') from PARSET'
                else
                  SEST_ENERGY%V_Ne_AB=SEST_ENERGY%V_Ne_AB+2.0D0*V_Ne_BAa(a)*(R_e-r_Ne_BAa(a))/(R-r_Ne_BAa(a))
                  SEST_und%V_Ne_AB=SEST_und%V_Ne_AB+2.0D0*V_Ne_BAa(a)*(R_e-r_Ne_BAa(a))/(R-r_Ne_BAa(a))
                  E_MM_und=E_MM_und+2.0D0*V_Ne_BAa(a)*(R_e-r_Ne_BAa(a))/(R-r_Ne_BAa(a))
                end if
              end do
            end if
            if(N_pair_B.gt.0)then ! V_Ne for pairs on B with A
              if(AeqB) I_und=I_temp ! go back and use BA parameters for AB
              do a=1,N_pair_B
                if(r_Ne_ABa_opt(a))then
                  SEST_ENERGY%V_Ne_AB=SEST_ENERGY%V_Ne_AB+2.0D0*V_Ne_ABa(a)*(R_e-PARSET(I_und))/(R-PARSET(I_und))
                  SEST_und%V_Ne_AB=SEST_und%V_Ne_AB+2.0D0*V_Ne_ABa(a)*(R_e-PARSET(I_und))/(R-PARSET(I_und))
                  E_MM_und=E_MM_und+2.0D0*V_Ne_ABa(a)*(R_e-PARSET(I_und))/(R-PARSET(I_und))
                  I_und=I_und+1
!                  write(6,'(a,I1,a)')'r_Ne_ABa(',a,') from PARSET'
                else
                  SEST_ENERGY%V_Ne_AB=SEST_ENERGY%V_Ne_AB+2.0D0*V_Ne_ABa(a)*(R_e-r_Ne_ABa(a))/(R-r_Ne_ABa(a))
                  SEST_und%V_Ne_AB=SEST_und%V_Ne_AB+2.0D0*V_Ne_ABa(a)*(R_e-r_Ne_ABa(a))/(R-r_Ne_ABa(a))
                  E_MM_und=E_MM_und+2.0D0*V_Ne_ABa(a)*(R_e-r_Ne_ABa(a))/(R-r_Ne_ABa(a))
                end if
              end do
            end if
            if(AeqB) I_temp=I_und ! avoids storing parameters twice in PARSET
            do i=1,N_i_A ! V_Ne for bonding electrons on A with B
              if(r_Ne_BAi_opt(i))then
                SEST_ENERGY%V_Ne_AB=SEST_ENERGY%V_Ne_AB+V_Ne_BAi(i)*(R_e-PARSET(I_und))/(R-PARSET(I_und))
                SEST_und%V_Ne_AB=SEST_und%V_Ne_AB+V_Ne_BAi(i)*(R_e-PARSET(I_und))/(R-PARSET(I_und))
                E_MM_und=E_MM_und+V_Ne_BAi(i)*(R_e-PARSET(I_und))/(R-PARSET(I_und))
                I_und=I_und+1
!                write(6,'(a,I1,a)')'r_Ne_BAi(',i,') from PARSET'
              else
                SEST_ENERGY%V_Ne_AB=SEST_ENERGY%V_Ne_AB+V_Ne_BAi(i)*(R_e-r_Ne_BAi(i))/(R-r_Ne_BAi(i))
                SEST_und%V_Ne_AB=SEST_und%V_Ne_AB+V_Ne_BAi(i)*(R_e-r_Ne_BAi(i))/(R-r_Ne_BAi(i))
                E_MM_und=E_MM_und+V_Ne_BAi(i)*(R_e-r_Ne_BAi(i))/(R-r_Ne_BAi(i))
              end if
            end do
            if(AeqB) I_und=I_temp ! go back and use BA parameters for AB
            do i=1,N_i_B ! V_Ne for bonding electrons on B with A
              if(r_Ne_ABi_opt(i))then
                SEST_ENERGY%V_Ne_AB=SEST_ENERGY%V_Ne_AB+V_Ne_ABi(i)*(R_e-PARSET(I_und))/(R-PARSET(I_und))
                SEST_und%V_Ne_AB=SEST_und%V_Ne_AB+V_Ne_ABi(i)*(R_e-PARSET(I_und))/(R-PARSET(I_und))
                E_MM_und=E_MM_und+V_Ne_ABi(i)*(R_e-PARSET(I_und))/(R-PARSET(I_und))
                I_und=I_und+1
!                write(6,'(a,I1,a)')'r_Ne_ABi(',i,') from PARSET'
              else
                SEST_ENERGY%V_Ne_AB=SEST_ENERGY%V_Ne_AB+V_Ne_ABi(i)*(R_e-r_Ne_ABi(i))/(R-r_Ne_ABi(i))
                SEST_und%V_Ne_AB=SEST_und%V_Ne_AB+V_Ne_ABi(i)*(R_e-r_Ne_ABi(i))/(R-r_Ne_ABi(i))
                E_MM_und=E_MM_und+V_Ne_ABi(i)*(R_e-r_Ne_ABi(i))/(R-r_Ne_ABi(i))
              end if
            end do
!
! Calculate electron-electron potential energy (V_ee_AB)
            if(N_pair_A.gt.0)then
              do a=1,N_pair_A
                if(N_pair_B.gt.0)then ! V_ee_AaBb
                  do b=1,N_pair_B
                    if(r_ee_AaBb_opt(a,b))then
                      SEST_ENERGY%V_ee_AB=SEST_ENERGY%V_ee_AB+2.0D0*V_ee_AaBb(a,b)*(R_e-PARSET(I_und))/(R-PARSET(I_und))
                      SEST_und%V_ee_AB=SEST_und%V_ee_AB+2.0D0*V_ee_AaBb(a,b)*(R_e-PARSET(I_und))/(R-PARSET(I_und))
                      E_MM_und=E_MM_und+2.0D0*V_ee_AaBb(a,b)*(R_e-PARSET(I_und))/(R-PARSET(I_und))
                      I_und=I_und+1
!                      write(6,'(a,I1,a,I1,a)')'r_ee_AaBb(',a,',',b,') from PARSET'
                    else 
                      SEST_ENERGY%V_ee_AB=SEST_ENERGY%V_ee_AB+2.0D0*V_ee_AaBb(a,b)*(R_e-r_ee_AaBb(a,b))/(R-r_ee_AaBb(a,b))
                      SEST_und%V_ee_AB=SEST_und%V_ee_AB+2.0D0*V_ee_AaBb(a,b)*(R_e-r_ee_AaBb(a,b))/(R-r_ee_AaBb(a,b))
                      E_MM_und=E_MM_und+2.0D0*V_ee_AaBb(a,b)*(R_e-r_ee_AaBb(a,b))/(R-r_ee_AaBb(a,b))
                    end if
                  end do
                end if
                if(AeqB) I_temp=I_und ! avoids storing parameters twice in PARSET
                do i=1,N_i_B ! V_ee_AaBi
                  if(r_ee_AaBi_opt(a,i))then
                    SEST_ENERGY%V_ee_AB=SEST_ENERGY%V_ee_AB+V_ee_AaBi(a,i)*(R_e-PARSET(I_und))/(R-PARSET(I_und))
                    SEST_und%V_ee_AB=SEST_und%V_ee_AB+V_ee_AaBi(a,i)*(R_e-PARSET(I_und))/(R-PARSET(I_und))
                    E_MM_und=E_MM_und+V_ee_AaBi(a,i)*(R_e-PARSET(I_und))/(R-PARSET(I_und))
                    I_und=I_und+1
!                    write(6,'(a,I1,a,I1,a)')'r_ee_AaBb(',a,',',i,') from PARSET'
                  else
                    SEST_ENERGY%V_ee_AB=SEST_ENERGY%V_ee_AB+V_ee_AaBi(a,i)*(R_e-r_ee_AaBi(a,i))/(R-r_ee_AaBi(a,i))
                    SEST_und%V_ee_AB=SEST_und%V_ee_AB+V_ee_AaBi(a,i)*(R_e-r_ee_AaBi(a,i))/(R-r_ee_AaBi(a,i))
                    E_MM_und=E_MM_und+V_ee_AaBi(a,i)*(R_e-r_ee_AaBi(a,i))/(R-r_ee_AaBi(a,i))
                  end if
                end do
              end do
            end if
            if(N_pair_B.gt.0)then
              do b=1,N_pair_B
                if(AeqB) I_und=I_temp ! goes back to BA terms for AB (only works here because loop over pairs has only one iteration)
                do i=1,N_i_A ! V_ee_BbAi
                  if(r_ee_BbAi_opt(b,i))then
                    SEST_ENERGY%V_ee_AB=SEST_ENERGY%V_ee_AB+V_ee_BbAi(b,i)*(R_e-PARSET(I_und))/(R-PARSET(I_und))
                    SEST_und%V_ee_AB=SEST_und%V_ee_AB+V_ee_BbAi(b,i)*(R_e-PARSET(I_und))/(R-PARSET(I_und))
                    E_MM_und=E_MM_und+V_ee_BbAi(b,i)*(R_e-PARSET(I_und))/(R-PARSET(I_und))
                    I_und=I_und+1
!                    write(6,'(a,I1,a,I1,a)')'r_ee_BbAi(',b,',',i,') from PARSET'
                  else
                    SEST_ENERGY%V_ee_AB=SEST_ENERGY%V_ee_AB+V_ee_BbAi(b,i)*(R_e-r_ee_BbAi(b,i))/(R-r_ee_BbAi(b,i))
                    SEST_und%V_ee_AB=SEST_und%V_ee_AB+V_ee_BbAi(b,i)*(R_e-r_ee_BbAi(b,i))/(R-r_ee_BbAi(b,i))
                    E_MM_und=E_MM_und+V_ee_BbAi(b,i)*(R_e-r_ee_BbAi(b,i))/(R-r_ee_BbAi(b,i))
                  end if
                end do
              end do
            end if
            do i=1,N_i_A
              do j=1,N_i_B
                if((i.le.BOND_ORDER).and.(j.le.BOND_ORDER).and.(i.eq.j))then
                  ! i and j belong to the same bond
                  if(r_ee_AiBj_opt(i,j))then
                    SEST_ENERGY%V_ee_AB=SEST_ENERGY%V_ee_AB+V_ee_AiBj(i,j)*(R_e-PARSET(I_und))/(R-PARSET(I_und))
                    SEST_und%V_ee_AB=SEST_und%V_ee_AB+V_ee_AiBj(i,j)*(R_e-PARSET(I_und))/(R-PARSET(I_und))
                    E_MM_und=E_MM_und+V_ee_AiBj(i,j)*(R_e-PARSET(I_und))/(R-PARSET(I_und))
                    I_und=I_und+1
!                    write(6,'(a,I1,a,I1,a)')'r_ee_AiBj(',i,',',j,') from PARSET'
                  else
                    SEST_ENERGY%V_ee_AB=SEST_ENERGY%V_ee_AB+V_ee_AiBj(i,j)*(R_e-r_ee_AiBj(i,j))/(R-r_ee_AiBj(i,j))
                    SEST_und%V_ee_AB=SEST_und%V_ee_AB+V_ee_AiBj(i,j)*(R_e-r_ee_AiBj(i,j))/(R-r_ee_AiBj(i,j))
                    E_MM_und=E_MM_und+V_ee_AiBj(i,j)*(R_e-r_ee_AiBj(i,j))/(R-r_ee_AiBj(i,j))
                  end if
                else
                  if(r_ee_AiBj_opt(i,j))then
                    SEST_ENERGY%V_ee_AB=SEST_ENERGY%V_ee_AB+0.5D0*V_ee_AiBj(i,j)*(R_e-PARSET(I_und))/(R-PARSET(I_und))
                    SEST_und%V_ee_AB=SEST_und%V_ee_AB+0.5D0*V_ee_AiBj(i,j)*(R_e-PARSET(I_und))/(R-PARSET(I_und))
                    E_MM_und=E_MM_und+0.5D0*V_ee_AiBj(i,j)*(R_e-PARSET(I_und))/(R-PARSET(I_und))
                    I_und=I_und+1
!                    write(6,'(a,I1,a,I1,a)')'r_ee_AiBj(',i,',',j,') from PARSET'
                  else
                    SEST_ENERGY%V_ee_AB=SEST_ENERGY%V_ee_AB+0.5D0*V_ee_AiBj(i,j)*(R_e-r_ee_AiBj(i,j))/(R-r_ee_AiBj(i,j))
                    SEST_und%V_ee_AB=SEST_und%V_ee_AB+0.5D0*V_ee_AiBj(i,j)*(R_e-r_ee_AiBj(i,j))/(R-r_ee_AiBj(i,j))
                    E_MM_und=E_MM_und+0.5D0*V_ee_AiBj(i,j)*(R_e-r_ee_AiBj(i,j))/(R-r_ee_AiBj(i,j))
                  end if
                end if
              end do
            end do
!
          else ! AB has no parameters to be optimized
! Calculate electron-nuclear attraction potential energy (V_Ne_AB)
            if(N_pair_A.gt.0)then ! V_Ne for pairs on A with B
              do a=1,N_pair_A
                SEST_ENERGY%V_Ne_AB=SEST_ENERGY%V_Ne_AB+2.0D0*V_Ne_BAa(a)*(R_e-r_Ne_BAa(a))/(R-r_Ne_BAa(a))
                SEST_def%V_Ne_AB=SEST_def%V_Ne_AB+2.0D0*V_Ne_BAa(a)*(R_e-r_Ne_BAa(a))/(R-r_Ne_BAa(a))
                E_MM_def=E_MM_def+2.0D0*V_Ne_BAa(a)*(R_e-r_Ne_BAa(a))/(R-r_Ne_BAa(a))
              end do
            end if
            if(N_pair_B.gt.0)then ! V_Ne for pairs on B with A
              do a=1,N_pair_B
                SEST_ENERGY%V_Ne_AB=SEST_ENERGY%V_Ne_AB+2.0D0*V_Ne_ABa(a)*(R_e-r_Ne_ABa(a))/(R-r_Ne_ABa(a))
                SEST_def%V_Ne_AB=SEST_def%V_Ne_AB+2.0D0*V_Ne_ABa(a)*(R_e-r_Ne_ABa(a))/(R-r_Ne_ABa(a))
                E_MM_def=E_MM_def+2.0D0*V_Ne_ABa(a)*(R_e-r_Ne_ABa(a))/(R-r_Ne_ABa(a))
              end do
            end if
            do i=1,N_i_A ! V_Ne for bonding electrons on A with B
              SEST_ENERGY%V_Ne_AB=SEST_ENERGY%V_Ne_AB+V_Ne_BAi(i)*(R_e-r_Ne_BAi(i))/(R-r_Ne_BAi(i))
              SEST_def%V_Ne_AB=SEST_def%V_Ne_AB+V_Ne_BAi(i)*(R_e-r_Ne_BAi(i))/(R-r_Ne_BAi(i))
              E_MM_def=E_MM_def+V_Ne_BAi(i)*(R_e-r_Ne_BAi(i))/(R-r_Ne_BAi(i))
            end do
            do i=1,N_i_B ! V_Ne for bonding electrons on B with A
              SEST_ENERGY%V_Ne_AB=SEST_ENERGY%V_Ne_AB+V_Ne_ABi(i)*(R_e-r_Ne_ABi(i))/(R-r_Ne_ABi(i))
              SEST_def%V_Ne_AB=SEST_def%V_Ne_AB+V_Ne_ABi(i)*(R_e-r_Ne_ABi(i))/(R-r_Ne_ABi(i))
              E_MM_def=E_MM_def+V_Ne_ABi(i)*(R_e-r_Ne_ABi(i))/(R-r_Ne_ABi(i))
            end do
!
! Calculate electron-electron potential energy (V_ee_AB)
            if(N_pair_A.gt.0)then
              do a=1,N_pair_A
                if(N_pair_B.gt.0)then ! V_ee_AaBb
                  do b=1,N_pair_B
                    SEST_ENERGY%V_ee_AB=SEST_ENERGY%V_ee_AB+2.0D0*V_ee_AaBb(a,b)*(R_e-r_ee_AaBb(a,b))/(R-r_ee_AaBb(a,b))
                    SEST_def%V_ee_AB=SEST_def%V_ee_AB+2.0D0*V_ee_AaBb(a,b)*(R_e-r_ee_AaBb(a,b))/(R-r_ee_AaBb(a,b))
                    E_MM_def=E_MM_def+2.0D0*V_ee_AaBb(a,b)*(R_e-r_ee_AaBb(a,b))/(R-r_ee_AaBb(a,b))
                  end do
                end if
                do i=1,N_i_B ! V_ee_AaBi
                  SEST_ENERGY%V_ee_AB=SEST_ENERGY%V_ee_AB+V_ee_AaBi(a,i)*(R_e-r_ee_AaBi(a,i))/(R-r_ee_AaBi(a,i))
                  SEST_def%V_ee_AB=SEST_def%V_ee_AB+V_ee_AaBi(a,i)*(R_e-r_ee_AaBi(a,i))/(R-r_ee_AaBi(a,i))
                  E_MM_def=E_MM_def+V_ee_AaBi(a,i)*(R_e-r_ee_AaBi(a,i))/(R-r_ee_AaBi(a,i))
                end do
              end do
            end if
            if(N_pair_B.gt.0)then
              do b=1,N_pair_B
                do i=1,N_i_A ! V_ee_BbAi
                  SEST_ENERGY%V_ee_AB=SEST_ENERGY%V_ee_AB+V_ee_BbAi(b,i)*(R_e-r_ee_BbAi(b,i))/(R-r_ee_BbAi(b,i))
                  SEST_def%V_ee_AB=SEST_def%V_ee_AB+V_ee_BbAi(b,i)*(R_e-r_ee_BbAi(b,i))/(R-r_ee_BbAi(b,i))
                  E_MM_def=E_MM_def+V_ee_BbAi(b,i)*(R_e-r_ee_BbAi(b,i))/(R-r_ee_BbAi(b,i))
                end do
              end do
            end if
            do i=1,N_i_A
              do j=1,N_i_B
                if((i.le.BOND_ORDER).and.(j.le.BOND_ORDER).and.(i.eq.j))then
                  ! i and j belong to the same bond
                  SEST_ENERGY%V_ee_AB=SEST_ENERGY%V_ee_AB+V_ee_AiBj(i,j)*(R_e-r_ee_AiBj(i,j))/(R-r_ee_AiBj(i,j))
                  SEST_def%V_ee_AB=SEST_def%V_ee_AB+V_ee_AiBj(i,j)*(R_e-r_ee_AiBj(i,j))/(R-r_ee_AiBj(i,j))
                  E_MM_def=E_MM_def+V_ee_AiBj(i,j)*(R_e-r_ee_AiBj(i,j))/(R-r_ee_AiBj(i,j))
                else
                  SEST_ENERGY%V_ee_AB=SEST_ENERGY%V_ee_AB+0.5D0*V_ee_AiBj(i,j)*(R_e-r_ee_AiBj(i,j))/(R-r_ee_AiBj(i,j))
                  SEST_def%V_ee_AB=SEST_def%V_ee_AB+0.5D0*V_ee_AiBj(i,j)*(R_e-r_ee_AiBj(i,j))/(R-r_ee_AiBj(i,j))
                  E_MM_def=E_MM_def+0.5D0*V_ee_AiBj(i,j)*(R_e-r_ee_AiBj(i,j))/(R-r_ee_AiBj(i,j))
                end if
              end do
            end do
!
          end if ! OPT_AB
!
          if(OPT_AB)then
            deallocate(r_Ne_ABa_opt, r_Ne_BAa_opt, r_Ne_ABi_opt, r_Ne_BAi_opt)
            deallocate(r_ee_AaBb_opt, r_ee_AaBi_opt, r_ee_BbAi_opt, r_ee_AiBj_opt)
          end if
!
          deallocate(V_Ne_ABa, V_Ne_BAa, V_Ne_ABi, V_Ne_BAi)
          deallocate(r_Ne_ABa, r_Ne_BAa, r_Ne_ABi, r_Ne_BAi)
          deallocate(V_ee_AaBb, V_ee_AaBi, V_ee_BbAi, V_ee_AiBj)
          deallocate(r_ee_AaBb, r_ee_AaBi, r_ee_BbAi, r_ee_AiBj)
!
        end do ! Batom
      end do ! Aatom
! Lone pairs and atoms
      if(Nlp.gt.0)then
        do Aatom=1,molecule%Natoms
!
          call SEST_COUNT_ELECTRONS(Aatom, N_pair_A, N_i_A)
!
          if(N_pair_A.eq.0)then
            N_A=1
          else
            N_A=N_pair_A
          end if
!
          allocate(V_ee_Aalp(1:N_A),V_ee_Ailp(1:N_i_A))
          allocate(r_ee_Aalp(1:N_A),r_ee_Ailp(1:N_i_A))
          allocate(r_ee_Aalp_opt(1:N_A),r_ee_Ailp_opt(1:N_i_A))
!
          do alp=1,Nlp
!
            R=dsqrt((MOL_atoms(Aatom)%X-lp(alp)%X)**2&
                   +(MOL_atoms(Aatom)%Y-lp(alp)%Y)**2&
                   +(MOL_atoms(Aatom)%Z-lp(alp)%Z)**2)
!
            OPT_AlpB=.false.
            if(lp(alp)%atom.eq.Aatom)then
              call SEST_LP_onA_PARAMS(Aatom, R_e)
            else
              call SEST_LP_notonA_PARAMS(alp, Aatom, R_e)
              AlpB_type=trim(MOL_atoms(Aatom)%type)//trim(lp(alp)%atom_type)
              do I=1,N_AlpB_und
                OPT_AlpB_type=trim(SEST_OPT_AlpB(I)%Aatom_type)//trim(SEST_OPT_AlpB(I)%Batom_type)
                if(OPT_AlpB_type.eq.AlpB_type)then
                  OPT_AlpB=.true.
!                  write(6,'(a,a)')'interaction to be optimized: ', OPT_AlpB_type
                  I_OPT_AlpB=I 
                  call SEST_OPT_LP_notonA_PARAMS(alp, Aatom)
                  exit
                end if
              end do
            end if
!
! Energy functions are piecewise to remove singularities
! Find maximum r parameter
            max_r=0.00000000D0
            !r_Ne_Alp(B)
            if(r_Ne_Alp.gt.max_r)then
               max_r=r_Ne_Alp
            end if
            !r_ee_Aalp(B)
            if(N_pair_A.gt.0)then
              do a=1,N_pair_A
                if(r_ee_Aalp(a).gt.max_r)then
                  max_r=r_ee_Aalp(a)
                end if
              end do
            end if
            !r_ee_Ailp(B)
            do i=1,N_i_A
              if(r_ee_Ailp(i).gt.max_r)then
                max_r=r_ee_Ailp(i)
              end if
            end do
!
            if(OPT_AlpB)then
!
              I_und=SEST_OPT_AlpB(I_OPT_AlpB)%I_und
!
              if(R.le.max_r+delta)then
                !V_ee_Aalp(B)
                if(N_pair_A.gt.0)then
                  do a=1,N_pair_A
                    if(r_ee_Aalp_opt(a))then 
                      SEST_ENERGY%V_ee_Alp=SEST_ENERGY%V_ee_Alp+2.0D0*V_ee_Aalp(a)*(R_e-PARSET(I_und))/R
                      SEST_und%V_ee_Alp=SEST_und%V_ee_Alp+2.0D0*V_ee_Aalp(a)*(R_e-PARSET(I_und))/R
                      E_MM_und=E_MM_und+2.0D0*V_ee_Aalp(a)*(R_e-PARSET(I_und))/R
                      I_und=I_und+1
                    else
                      SEST_ENERGY%V_ee_Alp=SEST_ENERGY%V_ee_Alp+2.0D0*V_ee_Aalp(a)*(R_e-r_ee_Aalp(a))/R
                      SEST_und%V_ee_Alp=SEST_und%V_ee_Alp+2.0D0*V_ee_Aalp(a)*(R_e-r_ee_Aalp(a))/R
                      E_MM_und=E_MM_und+2.0D0*V_ee_Aalp(a)*(R_e-r_ee_Aalp(a))/R
                    end if
                  end do
                end if
                !V_ee_Ailp(B)
                do i=1,N_i_A
                  if(r_ee_Ailp_opt(i))then
                    SEST_ENERGY%V_ee_Alp=SEST_ENERGY%V_ee_Alp+V_ee_Ailp(i)*(R_e-PARSET(I_und))/R
                    SEST_und%V_ee_Alp=SEST_und%V_ee_Alp+V_ee_Ailp(i)*(R_e-PARSET(I_und))/R
                    E_MM_und=E_MM_und+V_ee_Ailp(i)*(R_e-PARSET(I_und))/R
                    I_und=I_und+1 
                  else
                    SEST_ENERGY%V_ee_Alp=SEST_ENERGY%V_ee_Alp+V_ee_Ailp(i)*(R_e-r_ee_Ailp(i))/R
                    SEST_und%V_ee_Alp=SEST_und%V_ee_Alp+V_ee_Ailp(i)*(R_e-r_ee_Ailp(i))/R
                    E_MM_und=E_MM_und+V_ee_Ailp(i)*(R_e-r_ee_Ailp(i))/R
                  end if
                end do
                R=max_r+delta
              end if
!
              !V_Ne_Alp(B)
              if(r_Ne_Alp_opt)then
                SEST_ENERGY%V_Ne_Alp=SEST_ENERGY%V_Ne_Alp+2.0D0*V_Ne_Alp*(R_e-PARSET(I_und))/(R-PARSET(I_und))
                SEST_und%V_Ne_Alp=SEST_und%V_Ne_Alp+2.0D0*V_Ne_Alp*(R_e-PARSET(I_und))/(R-PARSET(I_und))
                E_MM_und=E_MM_und+2.0D0*V_Ne_Alp*(R_e-PARSET(I_und))/(R-PARSET(I_und))
                I_und=I_und+1
!                write(6,'(a)')'r_Ne_Alp from PARSET'
              else
                SEST_ENERGY%V_Ne_Alp=SEST_ENERGY%V_Ne_Alp+2.0D0*V_Ne_Alp*(R_e-r_Ne_Alp)/(R-r_Ne_Alp)
                SEST_und%V_Ne_Alp=SEST_und%V_Ne_Alp+2.0D0*V_Ne_Alp*(R_e-r_Ne_Alp)/(R-r_Ne_Alp)
                E_MM_und=E_MM_und+2.0D0*V_Ne_Alp*(R_e-r_Ne_Alp)/(R-r_Ne_Alp)
              end if
              !V_ee_Aalp(B)
              if(N_pair_A.gt.0)then
                do a=1,N_pair_A
                  if(r_ee_Aalp_opt(a))then
                    SEST_ENERGY%V_ee_Alp=SEST_ENERGY%V_ee_Alp+2.0D0*V_ee_Aalp(a)*(R_e-PARSET(I_und))/(R-PARSET(I_und))
                    SEST_und%V_ee_Alp=SEST_und%V_ee_Alp+2.0D0*V_ee_Aalp(a)*(R_e-PARSET(I_und))/(R-PARSET(I_und))
                    E_MM_und=E_MM_und+2.0D0*V_ee_Aalp(a)*(R_e-PARSET(I_und))/(R-PARSET(I_und))
                    I_und=I_und+1
!                    write(6,'(a,I1,a)')'r_ee_Aalp(',a,') from PARSET'
                  else
                    SEST_ENERGY%V_ee_Alp=SEST_ENERGY%V_ee_Alp+2.0D0*V_ee_Aalp(a)*(R_e-r_ee_Aalp(a))/(R-r_ee_Aalp(a))
                    SEST_und%V_ee_Alp=SEST_und%V_ee_Alp+2.0D0*V_ee_Aalp(a)*(R_e-r_ee_Aalp(a))/(R-r_ee_Aalp(a))
                    E_MM_und=E_MM_und+2.0D0*V_ee_Aalp(a)*(R_e-r_ee_Aalp(a))/(R-r_ee_Aalp(a))
                  end if
                end do
              end if
              !V_ee_Ailp(B)
              do i=1,N_i_A
                if(r_ee_Ailp_opt(i))then
                  SEST_ENERGY%V_ee_Alp=SEST_ENERGY%V_ee_Alp+V_ee_Ailp(i)*(R_e-PARSET(I_und))/(R-PARSET(I_und))
                  SEST_und%V_ee_Alp=SEST_und%V_ee_Alp+V_ee_Ailp(i)*(R_e-PARSET(I_und))/(R-PARSET(I_und))
                  E_MM_und=E_MM_und+V_ee_Ailp(i)*(R_e-PARSET(I_und))/(R-PARSET(I_und))
                  I_und=I_und+1
!                  write(6,'(a,I1,a)')'r_Ne_Ailp(',i,') from PARSET'
                else
                  SEST_ENERGY%V_ee_Alp=SEST_ENERGY%V_ee_Alp+V_ee_Ailp(i)*(R_e-r_ee_Ailp(i))/(R-r_ee_Ailp(i))
                  SEST_und%V_ee_Alp=SEST_und%V_ee_Alp+V_ee_Ailp(i)*(R_e-r_ee_Ailp(i))/(R-r_ee_Ailp(i))
                  E_MM_und=E_MM_und+V_ee_Ailp(i)*(R_e-r_ee_Ailp(i))/(R-r_ee_Ailp(i))
                end if
              end do
!
            else ! AlpB interaction has no parameters to be optimized
!
              if(R.le.max_r+delta)then
                !V_ee_Aalp(B)
                if(N_pair_A.gt.0)then
                  do a=1,N_pair_A
                    SEST_ENERGY%V_ee_Alp=SEST_ENERGY%V_ee_Alp+2.0D0*V_ee_Aalp(a)*(R_e-r_ee_Aalp(a))/R
                    SEST_def%V_ee_Alp=SEST_def%V_ee_Alp+2.0D0*V_ee_Aalp(a)*(R_e-r_ee_Aalp(a))/R
                    E_MM_def=E_MM_def+2.0D0*V_ee_Aalp(a)*(R_e-r_ee_Aalp(a))/R
                  end do
                end if
                !V_ee_Ailp(B)
                do i=1,N_i_A
                  SEST_ENERGY%V_ee_Alp=SEST_ENERGY%V_ee_Alp+V_ee_Ailp(i)*(R_e-r_ee_Ailp(i))/R
                  SEST_def%V_ee_Alp=SEST_def%V_ee_Alp+V_ee_Ailp(i)*(R_e-r_ee_Ailp(i))/R
                  E_MM_def=E_MM_def+V_ee_Ailp(i)*(R_e-r_ee_Ailp(i))/R
                end do
                R=max_r+delta
              end if
!
              !V_Ne_Alp(B)
              SEST_ENERGY%V_Ne_Alp=SEST_ENERGY%V_Ne_Alp+2.0D0*V_Ne_Alp*(R_e-r_Ne_Alp)/(R-r_Ne_Alp)
              SEST_def%V_Ne_Alp=SEST_def%V_Ne_Alp+2.0D0*V_Ne_Alp*(R_e-r_Ne_Alp)/(R-r_Ne_Alp)
              E_MM_def=E_MM_def+2.0D0*V_Ne_Alp*(R_e-r_Ne_Alp)/(R-r_Ne_Alp)
              !V_ee_Aalp(B)
              if(N_pair_A.gt.0)then
                do a=1,N_pair_A
                  SEST_ENERGY%V_ee_Alp=SEST_ENERGY%V_ee_Alp+2.0D0*V_ee_Aalp(a)*(R_e-r_ee_Aalp(a))/(R-r_ee_Aalp(a))
                  SEST_def%V_ee_Alp=SEST_def%V_ee_Alp+2.0D0*V_ee_Aalp(a)*(R_e-r_ee_Aalp(a))/(R-r_ee_Aalp(a))
                  E_MM_def=E_MM_def+2.0D0*V_ee_Aalp(a)*(R_e-r_ee_Aalp(a))/(R-r_ee_Aalp(a))
                end do
              end if
              !V_ee_Ailp(B)
              do i=1,N_i_A
                SEST_ENERGY%V_ee_Alp=SEST_ENERGY%V_ee_Alp+V_ee_Ailp(i)*(R_e-r_ee_Ailp(i))/(R-r_ee_Ailp(i))
                SEST_def%V_ee_Alp=SEST_def%V_ee_Alp+V_ee_Ailp(i)*(R_e-r_ee_Ailp(i))/(R-r_ee_Ailp(i))
                E_MM_def=E_MM_def+V_ee_Ailp(i)*(R_e-r_ee_Ailp(i))/(R-r_ee_Ailp(i))
              end do
!
            end if ! OPT_AlpB
!
          end do ! alp
!
          deallocate(V_ee_Aalp,V_ee_Ailp)
          deallocate(r_ee_Aalp,r_ee_Ailp)
          deallocate(r_ee_Aalp_opt,r_ee_Ailp_opt)
!
        end do  ! Aatom
      end if! Nlp
!
! Lone pairs with lone pairs
      if(Nlp.gt.1)then
        do alp=2,Nlp
          do blp=1,alp-1
!
            R=dsqrt((lp(alp)%X-lp(blp)%X)**2&
                  +(lp(alp)%Y-lp(blp)%Y)**2&
                  +(lp(alp)%Z-lp(blp)%Z)**2)
!
            if(lp(alp)%atom.eq.lp(blp)%atom)then
              call SEST_LP_A_LP_A_PARAMS(alp, R_e)
            else
              call SEST_LP_A_LP_B_PARAMS(alp, blp, R_e)
            end if
!
            ! V_ee_lp(A)_lp(B)
            SEST_ENERGY%V_ee_lplp=SEST_ENERGY%V_ee_lplp+2.0D0*V_ee_alpblp*(R_e-r_ee_alpblp)/(R-r_ee_alpblp)
            SEST_def%V_ee_lplp=SEST_def%V_ee_lplp+2.0D0*V_ee_alpblp*(R_e-r_ee_alpblp)/(R-r_ee_alpblp)
            E_MM_def=E_MM_def+2.0D0*V_ee_alpblp*(R_e-r_ee_alpblp)/(R-r_ee_alpblp)
!
          end do
        end do
      end if
!
! Energies are first calculated as potential energy, but SEST uses the virial E=V/2
!
!        write(6,'(/a,F14.8)')'V_Ne_AB= ',SEST_ENERGY%V_Ne_AB
!        write(6,'(a,F14.8)')'V_Ne_AB_und= ',SEST_und%V_Ne_AB
!        write(6,'(a,F14.8)')'V_Ne_AB_def= ',SEST_def%V_Ne_AB
!        write(6,'(a,F14.8)')'V_Ne_AB_def+V_Ne_AB_und= ',SEST_def%V_Ne_AB+SEST_und%V_Ne_AB
!        write(6,'(a,F14.8)')'V_ee_AB= ',SEST_ENERGY%V_ee_AB
!        write(6,'(a,F14.8)')'V_ee_AB_und= ',SEST_und%V_ee_AB
!        write(6,'(a,F14.8)')'V_ee_AB_def= ',SEST_def%V_ee_AB
!        write(6,'(a,F14.8)')'V_ee_AB_def+V_ee_AB_und= ',SEST_def%V_ee_AB+SEST_und%V_ee_AB
!        write(6,'(a,F14.8)')'V_Ne_Alp= ',SEST_ENERGY%V_Ne_Alp
!        write(6,'(a,F14.8)')'V_Ne_Alp_und= ',SEST_und%V_Ne_Alp
!        write(6,'(a,F14.8)')'V_Ne_Alp_def= ',SEST_def%V_Ne_Alp
!        write(6,'(a,F14.8)')'V_Ne_Alp_def+V_Ne_Alp_und= ',SEST_def%V_Ne_Alp+SEST_und%V_Ne_Alp
!        write(6,'(a,F14.8)')'V_ee_Alp= ',SEST_ENERGY%V_ee_Alp
!        write(6,'(a,F14.8)')'V_ee_Alp_und= ',SEST_und%V_ee_Alp
!        write(6,'(a,F14.8)')'V_ee_Alp_def= ',SEST_def%V_ee_Alp
!        write(6,'(a,F14.8)')'V_ee_Alp_def+V_ee_Alp_und= ',SEST_def%V_ee_Alp+SEST_und%V_ee_Alp
!        write(6,'(a,F14.8)')'V_ee_lplp= ',SEST_ENERGY%V_ee_lplp
!        write(6,'(a,F14.8)')'V_ee_lplp_und= ',SEST_und%V_ee_lplp
!        write(6,'(a,F14.8)')'V_ee_lplp_def= ',SEST_def%V_ee_lplp
!        write(6,'(a,F14.8)')'V_ee_lplp_def+V_ee_lplp_und= ',SEST_def%V_ee_lplp+SEST_und%V_ee_lplp
!        write(6,'(a,F14.8)')'sum_V= ',SEST_ENERGY%V_ee_lplp+SEST_ENERGY%V_Ne_AB+SEST_ENERGY%V_ee_AB&
!                                     +SEST_ENERGY%V_Ne_Alp+SEST_ENERGY%V_ee_Alp
!        write(6,'(a,F14.8)')'E_MM_def= ',E_MM_def
!        write(6,'(a,F14.8)')'E_MM_und= ',E_MM_und
!        write(6,'(a,F14.8)')'E_MM_def+E_MM_und= ',E_MM_def+E_MM_und
!
        E_MM_def=E_MM_def/2.0D0
        E_MM_und=E_MM_und/2.0D0
!
!
      call PRG_manager('exit','SEST_ENERGY_OPT_PARAMS','UTILITY')
!
      end subroutine SEST_ENERGY_OPT_PARAMS
!
      subroutine SEST_GRADIENT_OPT_PARAMS(g_def,g_und)
!***********************************************************************
!     Date last modified: May 8, 2007                                  *
!     Author: Joshua Hollett                                           *
!     Description: Calculate first and second derivatives of q         *
!***********************************************************************
!
! Modules:
      USE program_manager
      USE SEST_tools
      USE type_molecule 
      USE OPT_objects
!
      implicit none
!
! Output:
      double precision, dimension(:), intent(OUT) :: g_def(3*(molecule%Natoms+Nlp)),g_und(3*(molecule%Natoms+Nlp))
! Local:
      integer :: Batom,Aatom,I_Ax,I_Ay,I_Az,I_Bx,I_By,I_Bz
      double precision :: R_e,R,dV
      double precision :: DX_AB,DY_AB,DZ_AB
      integer :: N_pair_A, N_pair_B, N_i_A, N_i_B, N_A, N_B, a, b, i, j, alp, blp, BOND_ORDER
      integer :: I_alpx,I_alpy,I_alpz,I_blpx,I_blpy,I_blpz
      double precision :: DX_Aalp,DY_Aalp,DZ_Aalp
      double precision :: DX_alpA,DY_alpA,DZ_alpA
      double precision :: DX_alpblp,DY_alpblp,DZ_alpblp
      integer :: valency_A, valency_B, temp_pair, temp_i, I_und
      double precision :: delta, max_r 
      character(len=10) :: AB_type, BA_type, OPT_AB_type, AlpB_type, OPT_AlpB_type
      logical :: OPT_AB, OPT_AlpB 
      integer :: I_OPT_AB, I_OPT_AlpB 
!
! Begin:
!
      call PRG_manager('enter','SEST_GRADIENT_OPT_PARAMS','UTILITY')
!
! Set delta value for piecewise functions
      delta=0.02
!
! Calculate first and second derivatives
      g_def(1:3*(molecule%Natoms+Nlp))=0.0D0
      g_und(1:3*(molecule%Natoms+Nlp))=0.0D0
!
      do Aatom=1,molecule%Natoms
        I_Ax=3*Aatom-2
        I_Ay=3*Aatom-1
        I_Az=3*Aatom
        do Batom=1,molecule%Natoms
          if(Aatom.eq.Batom)cycle ! same atom
          R=SEST_BOND(Aatom,Batom)%distance
          dV=-dble(MOL_atoms(Aatom)%Atomic_number)*dble(MOL_atoms(Batom)%Atomic_number)/R**2 ! Nuclear repulsion terms
!
          call SEST_COUNT_ELECTRONS(Aatom, N_pair_A, N_i_A)
          call SEST_COUNT_ELECTRONS(Batom, N_pair_B, N_i_B)
! Order atom A and B
          valency_A=sum(SEST_BOND(Aatom,1:molecule%Natoms)%connect)
          valency_B=sum(SEST_BOND(Batom,1:molecule%Natoms)%connect)
          if(MOL_atoms(Aatom)%Atomic_number.eq.MOL_atoms(Batom)%Atomic_number)then 
            if(valency_B.lt.valency_A)then
              temp_pair=N_pair_A
              temp_i=N_i_A
              N_pair_A=N_pair_B
              N_i_A=N_i_B
              N_pair_B=temp_pair
              N_i_B=temp_i
            end if
          else if(MOL_atoms(Batom)%Atomic_number.lt.MOL_atoms(Aatom)%Atomic_number)then
            temp_pair=N_pair_A
            temp_i=N_i_A
            N_pair_A=N_pair_B
            N_i_A=N_i_B
            N_pair_B=temp_pair
            N_i_B=temp_i
          end if
!
! Allocate parameter arrays
          if((N_pair_A.eq.0).and.(N_pair_B.eq.0))then
            N_A=1
            N_B=1
          else if(N_pair_A.eq.0)then
            N_A=1
            N_B=N_pair_B
          else if(N_pair_B.eq.0)then
            N_A=N_pair_A
            N_B=1
          else
            N_A=N_pair_A
            N_B=N_pair_B
          end if
!
          allocate(V_Ne_ABa(1:N_B),V_Ne_BAa(1:N_A),V_Ne_ABi(1:N_i_B),V_Ne_BAi(1:N_i_A)) 
          allocate(r_Ne_ABa(1:N_B),r_Ne_BAa(1:N_A),r_Ne_ABi(1:N_i_B),r_Ne_BAi(1:N_i_A)) 
          allocate(V_ee_AaBb(1:N_A,1:N_B),V_ee_AaBi(1:N_A,1:N_i_B),V_ee_BbAi(1:N_B,1:N_i_A)) 
          allocate(r_ee_AaBb(1:N_A,1:N_B),r_ee_AaBi(1:N_A,1:N_i_B),r_ee_BbAi(1:N_B,1:N_i_A)) 
          allocate(V_ee_AiBj(1:N_i_A,1:N_i_B),r_ee_AiBj(1:N_i_A,1:N_i_B))
!
! Determine if AB interaction has parameters to be optimized
          AB_type=trim(MOL_atoms(Aatom)%type)//trim(MOL_atoms(Batom)%type)
          BA_type=trim(MOL_atoms(Batom)%type)//trim(MOL_atoms(Aatom)%type)
          OPT_AB=.false.
          do I=1,N_AB_und
            OPT_AB_type=trim(SEST_OPT_AB(I)%Aatom_type)//trim(SEST_OPT_AB(I)%Batom_type)
            if(((SEST_BOND(Aatom,Batom)%connect.eq.1).and.(SEST_OPT_AB(I)%connect.eq.1))&
            .or.((SEST_BOND(Aatom,Batom)%connect.eq.0).and.(SEST_OPT_AB(I)%connect.eq.0)))then
              if((AB_type.eq.OPT_AB_type).or.(BA_type.eq.OPT_AB_type))then
                OPT_AB=.true.
                I_OPT_AB=I
                exit
              end if
            end if ! both bonding or non-bonding
          end do ! N_AB_und
!
          if(OPT_AB)then
! Logical arrays that determine whether parameter is defined or undefined(part of PARSET)
            allocate(r_Ne_ABa_opt(1:N_B),r_Ne_BAa_opt(1:N_A),r_Ne_ABi_opt(1:N_i_B),r_Ne_BAi_opt(1:N_i_A))
            allocate(r_ee_AaBb_opt(1:N_A,1:N_B),r_ee_AaBi_opt(1:N_A,1:N_i_B),r_ee_BbAi_opt(1:N_B,1:N_i_A))
            allocate(r_ee_AiBj_opt(1:N_i_A,1:N_i_B))
          end if
!
          if(SEST_BOND(Aatom,Batom)%connect.eq.1)then
            call SEST_BOND_PARAMS(SEST_BOND(Aatom,Batom)%type,BOND_ORDER,R_e)
            if(OPT_AB)then
              call SEST_OPT_BOND_PARAMS(SEST_BOND(Aatom,Batom)%type)
            end if
          else
            call SEST_NON_BOND_PARAMS(Aatom,Batom,R_e)
            if(OPT_AB)then
              call SEST_OPT_NON_BOND_PARAMS(Aatom,Batom)
            end if
            BOND_ORDER=0
          end if
!
! To remove singularities, energy functions are now piecewise
! Find maximum r
          max_r=0.00000000D0
! r_Ne_BAa
          if(N_pair_A.gt.0)then
            do a=1,N_pair_A  
              if(r_Ne_BAa(a).gt.max_r)then 
                max_r=r_Ne_BAa(a)
              end if
            end do
          end if
! r_Ne_ABa
          if(N_pair_B.gt.0)then
            do a=1,N_pair_B  
              if(r_Ne_ABa(a).gt.max_r)then 
                max_r=r_Ne_ABa(a)
              end if
            end do
          end if
! r_Ne_BAi
          do i=1,N_i_A
            if(r_Ne_BAi(i).gt.max_r)then 
              max_r=r_Ne_BAi(i)
            end if
          end do
! r_Ne_ABi
          do i=1,N_i_B
            if(r_Ne_ABi(i).gt.max_r)then 
              max_r=r_Ne_ABi(i)
            end if
          end do
! r_ee_AaBb
          if(N_pair_A.gt.0)then 
            do a=1,N_pair_A
              if(N_pair_B.gt.0)then ! r_ee_AaBb
                do b=1,N_pair_B
                  if(r_ee_AaBb(a,b).gt.max_r)then
                    max_r=r_ee_AaBb(a,b)
                  end if
                end do
              end if
! r_ee_AaBi
              do i=1,N_i_B ! r_ee_AaBi
                if(r_ee_AaBi(a,i).gt.max_r)then
                  max_r=r_ee_AaBi(a,i)
                end if
              end do
            end do
          end if
! r_ee_BbAi
          if(N_pair_B.gt.0)then
            do b=1,N_pair_B
              do i=1,N_i_A ! r_ee_BbAi
                if(r_ee_BbAi(b,i).gt.max_r)then
                  max_r=r_ee_BbAi(b,i)
                end if
              end do
            end do
          end if
! r_ee_AiBj
          do i=1,N_i_A
            do j=1,N_i_B ! r_ee_AiBj
              if(r_ee_AiBj(i,j).gt.max_r)then
                max_r=r_ee_AiBj(i,j)
              end if
            end do
          end do
!
          if(R.le.(max_r+delta))then
            R=max_r+delta
          end if
!
          if(OPT_AB)then
            I_und=SEST_OPT_AB(I_OPT_AB)%I_und
!
! Calculate electron-nuclear attraction potential energy (V_Ne_AB)
            if(N_pair_A.gt.0)then ! V_Ne for pairs on A with B
              do a=1,N_pair_A
                if(r_Ne_BAa_opt(a))then
                  dV=dV-2.0D0*V_Ne_BAa(a)*(R_e-PARSET(I_und))/(R-PARSET(I_und))**2
                  I_und=I_und+1
                else
                  dV=dV-2.0D0*V_Ne_BAa(a)*(R_e-r_Ne_BAa(a))/(R-r_Ne_BAa(a))**2
                end if
              end do
            end if
            if(N_pair_B.gt.0)then ! V_Ne for pairs on B with A
              do a=1,N_pair_B
                if(r_Ne_ABa_opt(a))then
                  dV=dV-2.0D0*V_Ne_ABa(a)*(R_e-PARSET(I_und))/(R-PARSET(I_und))**2
                  I_und=I_und+1
                else
                  dV=dV-2.0D0*V_Ne_ABa(a)*(R_e-r_Ne_ABa(a))/(R-r_Ne_ABa(a))**2
                end if
              end do
            end if
            do i=1,N_i_A ! V_Ne for bonding electrons on A with B
              if(r_Ne_BAi_opt(i))then
                dV=dV-V_Ne_BAi(i)*(R_e-PARSET(I_und))/(R-PARSET(I_und))**2
                I_und=I_und+1
              else
                dV=dV-V_Ne_BAi(i)*(R_e-r_Ne_BAi(i))/(R-r_Ne_BAi(i))**2
              end if
            end do
            do i=1,N_i_B ! V_Ne for bonding electrons on B with A
              if(r_Ne_ABi_opt(i))then
                dV=dV-V_Ne_ABi(i)*(R_e-PARSET(I_und))/(R-PARSET(I_und))**2
                I_und=I_und+1
              else
                dV=dV-V_Ne_ABi(i)*(R_e-r_Ne_ABi(i))/(R-r_Ne_ABi(i))**2
              end if
            end do
!   
! Calculate electron-electron potential energy (V_ee_AB)
            if(N_pair_A.gt.0)then 
              do a=1,N_pair_A
                if(N_pair_B.gt.0)then ! V_ee_AaBb
                  do b=1,N_pair_B
                    if(r_ee_AaBb_opt(a,b))then
                      dV=dV-2.0D0*V_ee_AaBb(a,b)*(R_e-PARSET(I_und))/(R-PARSET(I_und))**2
                      I_und=i_und+1
                    else
                      dV=dV-2.0D0*V_ee_AaBb(a,b)*(R_e-r_ee_AaBb(a,b))/(R-r_ee_AaBb(a,b))**2
                    end if
                  end do
                end if
                do i=1,N_i_B ! V_ee_AaBi
                  if(r_ee_AaBi_opt(a,i))then
                    dV=dV-V_ee_AaBi(a,i)*(R_e-PARSET(I_und))/(R-PARSET(I_und))**2
                    I_und=I_und+1
                  else
                    dV=dV-V_ee_AaBi(a,i)*(R_e-r_ee_AaBi(a,i))/(R-r_ee_AaBi(a,i))**2
                  end if
                end do
              end do
            end if
            if(N_pair_B.gt.0)then
              do b=1,N_pair_B
                do i=1,N_i_A ! V_ee_BbAi
                  if(r_ee_BbAi_opt(b,i))then
                    dV=dV-V_ee_BbAi(b,i)*(R_e-PARSET(I_und))/(R-PARSET(I_und))**2
                    I_und=I_und+1
                  else
                    dV=dV-V_ee_BbAi(b,i)*(R_e-r_ee_BbAi(b,i))/(R-r_ee_BbAi(b,i))**2
                  end if
                end do
              end do
            end if
            do i=1,N_i_A
              do j=1,N_i_B
                if((i.le.BOND_ORDER).and.(j.le.BOND_ORDER).and.(i.eq.j))then ! i and j belong to same bond
                  if(r_ee_AiBj_opt(i,j))then
                    dV=dV-V_ee_AiBj(i,j)*(R_e-PARSET(I_und))/(R-PARSET(I_und))**2
                    I_und=I_und+1
                  else
                    dV=dV-V_ee_AiBj(i,j)*(R_e-r_ee_AiBj(i,j))/(R-r_ee_AiBj(i,j))**2
                  end if
                else
                  if(r_ee_AiBj_opt(i,j))then
                    dV=dV-0.5D0*V_ee_AiBj(i,j)*(R_e-PARSET(I_und))/(R-PARSET(I_und))**2
                    I_und=I_und+1
                  else
                    dV=dV-0.5D0*V_ee_AiBj(i,j)*(R_e-r_ee_AiBj(i,j))/(R-r_ee_AiBj(i,j))**2
                  end if
                end if
              end do
            end do
!
          else ! AB interaction has no parameters to be optimized
!
! Calculate electron-nuclear attraction potential energy (V_Ne_AB)
            if(N_pair_A.gt.0)then ! V_Ne for pairs on A with B
              do a=1,N_pair_A
                dV=dV-2.0D0*V_Ne_BAa(a)*(R_e-r_Ne_BAa(a))/(R-r_Ne_BAa(a))**2
              end do
            end if
            if(N_pair_B.gt.0)then ! V_Ne for pairs on B with A
              do a=1,N_pair_B
                dV=dV-2.0D0*V_Ne_ABa(a)*(R_e-r_Ne_ABa(a))/(R-r_Ne_ABa(a))**2
              end do
            end if
            do i=1,N_i_A ! V_Ne for bonding electrons on A with B
              dV=dV-V_Ne_BAi(i)*(R_e-r_Ne_BAi(i))/(R-r_Ne_BAi(i))**2
            end do
            do i=1,N_i_B ! V_Ne for bonding electrons on B with A
              dV=dV-V_Ne_ABi(i)*(R_e-r_Ne_ABi(i))/(R-r_Ne_ABi(i))**2
            end do
!
! Calculate electron-electron potential energy (V_ee_AB)
            if(N_pair_A.gt.0)then
              do a=1,N_pair_A
                if(N_pair_B.gt.0)then ! V_ee_AaBb
                  do b=1,N_pair_B
                    dV=dV-2.0D0*V_ee_AaBb(a,b)*(R_e-r_ee_AaBb(a,b))/(R-r_ee_AaBb(a,b))**2
                  end do
                end if
                do i=1,N_i_B ! V_ee_AaBi
                  dV=dV-V_ee_AaBi(a,i)*(R_e-r_ee_AaBi(a,i))/(R-r_ee_AaBi(a,i))**2
                end do
              end do
            end if
            if(N_pair_B.gt.0)then
              do b=1,N_pair_B
                do i=1,N_i_A ! V_ee_BbAi
                  dV=dV-V_ee_BbAi(b,i)*(R_e-r_ee_BbAi(b,i))/(R-r_ee_BbAi(b,i))**2
                end do
              end do
            end if
            do i=1,N_i_A
              do j=1,N_i_B
                if((i.le.BOND_ORDER).and.(j.le.BOND_ORDER).and.(i.eq.j))then ! i and j belong to same bond
                  dV=dV-V_ee_AiBj(i,j)*(R_e-r_ee_AiBj(i,j))/(R-r_ee_AiBj(i,j))**2
                else
                  dV=dV-0.5D0*V_ee_AiBj(i,j)*(R_e-r_ee_AiBj(i,j))/(R-r_ee_AiBj(i,j))**2
                end if
              end do
            end do
!
          end if ! OPT_AB
!
          deallocate(V_Ne_ABa, V_Ne_BAa, V_Ne_ABi, V_Ne_BAi)
          deallocate(r_Ne_ABa, r_Ne_BAa, r_Ne_ABi, r_Ne_BAi)
          deallocate(V_ee_AaBb, V_ee_AaBi, V_ee_BbAi, V_ee_AiBj)
          deallocate(r_ee_AaBb, r_ee_AaBi, r_ee_BbAi, r_ee_AiBj)
!
          DX_AB=MOL_atoms(Aatom)%X-MOL_atoms(Batom)%X
          DY_AB=MOL_atoms(Aatom)%Y-MOL_atoms(Batom)%Y
          DZ_AB=MOL_atoms(Aatom)%Z-MOL_atoms(Batom)%Z
          I_Bx=3*Batom-2
          I_By=3*Batom-1
          I_Bz=3*Batom
! Calculate gradient
          if(OPT_AB)then
            g_und(I_Ax)=g_und(I_Ax)+dV*DX_AB/R !x
            g_und(I_Ay)=g_und(I_Ay)+dV*DY_AB/R !y
            g_und(I_Az)=g_und(I_Az)+dV*DZ_AB/R !z
            deallocate(r_Ne_ABa_opt, r_Ne_BAa_opt, r_Ne_ABi_opt, r_Ne_BAi_opt)
            deallocate(r_ee_AaBb_opt, r_ee_AaBi_opt, r_ee_BbAi_opt, r_ee_AiBj_opt)
          else
            g_def(I_Ax)=g_def(I_Ax)+dV*DX_AB/R !x
            g_def(I_Ay)=g_def(I_Ay)+dV*DY_AB/R !y
            g_def(I_Az)=g_def(I_Az)+dV*DZ_AB/R !z
          end if
!
        end do !Batom
!
! Lone pairs and Aatom
        if(Nlp.gt.0)then
          call SEST_COUNT_ELECTRONS(Aatom, N_pair_A, N_i_A)
!
          if(N_pair_A.eq.0)then
            N_A=1
          else
            N_A=N_pair_A
          end if
!
          allocate(V_ee_Aalp(1:N_A),V_ee_Ailp(1:N_i_A)) 
          allocate(r_ee_Aalp(1:N_A),r_ee_Ailp(1:N_i_A)) 
          allocate(r_ee_Aalp_opt(1:N_A),r_ee_Ailp_opt(1:N_i_A))
!
          do alp=1,Nlp
!
            dV=0.0D0
!
            I_alpx=3*(molecule%Natoms+alp)-2
            I_alpy=3*(molecule%Natoms+alp)-1
            I_alpz=3*(molecule%Natoms+alp)
!
            R=dsqrt((MOL_atoms(Aatom)%X-lp(alp)%X)**2&
                   +(MOL_atoms(Aatom)%Y-lp(alp)%Y)**2&
                   +(MOL_atoms(Aatom)%Z-lp(alp)%Z)**2)
!
            OPT_AlpB=.false.
            if(lp(alp)%atom.eq.Aatom)then
              call SEST_LP_onA_PARAMS(Aatom, R_e)
            else
              call SEST_LP_notonA_PARAMS(alp, Aatom, R_e)
              AlpB_type=trim(MOL_atoms(Aatom)%type)//trim(lp(alp)%atom_type)
              do I=1,N_AlpB_und
                OPT_AlpB_type=trim(SEST_OPT_AlpB(I)%Aatom_type)//trim(SEST_OPT_AlpB(I)%Batom_type)
                if(OPT_AlpB_type.eq.AlpB_type)then
                  OPT_AlpB=.true.
                  I_OPT_AlpB=I
                  call SEST_OPT_LP_notonA_PARAMS(alp, Aatom)
                  exit
                end if
              end do
            end if
!
! Energy functions are piecewise to remove singularities
! Find maximum r parameter
            max_r=0.00000000D0
            !r_Ne_Alp(B)
            if(r_Ne_Alp.gt.max_r)then
               max_r=r_Ne_Alp
            end if
            !r_ee_Aalp(B)
            if(N_pair_A.gt.0)then
              do a=1,N_pair_A
                if(r_ee_Aalp(a).gt.max_r)then
                  max_r=r_ee_Aalp(a)
                end if
              end do
            end if
            !r_ee_Ailp(B)
            do i=1,N_i_A
              if(r_ee_Ailp(i).gt.max_r)then
                max_r=r_ee_Ailp(i)
              end if
            end do
!
            if(OPT_AlpB)then
              I_und=SEST_OPT_AlpB(I_OPT_AlpB)%I_und
!
              if(R.le.max_r+delta)then
                !V_ee_Aalp(B)
                if(N_pair_A.gt.0)then
                  do a=1,N_pair_A
                    if(r_ee_Aalp_opt(a))then 
                      dV=dV-2.0D0*V_ee_Aalp(a)*(R_e-PARSET(I_und))/R**2
                      I_und=I_und+1
                    else
                      dV=dV-2.0D0*V_ee_Aalp(a)*(R_e-r_ee_Aalp(a))/R**2
                    end if
                  end do
                end if
                !V_ee_Ailp(B)
                do i=1,N_i_A
                  if(r_ee_Ailp_opt(i))then
                    dV=dV-V_ee_Ailp(i)*(R_e-PARSET(I_und))/R**2
                    I_und=I_und+1
                  else
                    dV=dV-V_ee_Ailp(i)*(R_e-PARSET(I_und))/R**2
                  end if
                end do
                R=max_r+delta
              end if
!
            !V_Ne_Alp(B)
              if(r_Ne_Alp_opt)then
                dV=dV-2.0D0*V_Ne_Alp*(R_e-PARSET(I_und))/(R-PARSET(I_und))**2
                I_und=I_und+1
              else
                dV=dV-2.0D0*V_Ne_Alp*(R_e-r_Ne_Alp)/(R-r_Ne_Alp)**2
              end if
            !V_ee_Aalp(B)
              if(N_pair_A.gt.0)then
                do a=1,N_pair_A
                  if(r_ee_Aalp_opt(a))then
                    dV=dV-2.0D0*V_ee_Aalp(a)*(R_e-PARSET(I_und))/(R-PARSET(I_und))**2
                    I_und=I_und+1
                  else
                    dV=dV-2.0D0*V_ee_Aalp(a)*(R_e-r_ee_Aalp(a))/(R-r_ee_Aalp(a))**2
                  end if
                end do
              end if
            !V_ee_Ailp(B)
              do i=1,N_i_A
                if(r_ee_Ailp_opt(i))then
                  dV=dV-V_ee_Ailp(i)*(R_e-PARSET(I_und))/(R-PARSET(I_und))**2
                  I_und=I_und+1
                else
                  dV=dV-V_ee_Ailp(i)*(R_e-r_ee_Ailp(i))/(R-r_ee_Ailp(i))**2
                end if
              end do
!
            else ! AlpB interaction has no parameters to be optimized
!
              if(R.le.max_r+delta)then
                !V_ee_Aalp(B)
                if(N_pair_A.gt.0)then
                  do a=1,N_pair_A
                    dV=dV-2.0D0*V_ee_Aalp(a)*(R_e-r_ee_Aalp(a))/R**2
                  end do
                end if
                !V_ee_Ailp(B)
                do i=1,N_i_A
                  dV=dV-V_ee_Ailp(i)*(R_e-r_ee_Ailp(i))/R**2
                end do
                R=max_r+delta
              end if
!
            !V_Ne_Alp(B)
              dV=dV-2.0D0*V_Ne_Alp*(R_e-r_Ne_Alp)/(R-r_Ne_Alp)**2
            !V_ee_Aalp(B)
              if(N_pair_A.gt.0)then
                do a=1,N_pair_A
                  dV=dV-2.0D0*V_ee_Aalp(a)*(R_e-r_ee_Aalp(a))/(R-r_ee_Aalp(a))**2
                end do
              end if
            !V_ee_Ailp(B)
              do i=1,N_i_A
                dV=dV-V_ee_Ailp(i)*(R_e-r_ee_Ailp(i))/(R-r_ee_Ailp(i))**2
              end do
! 
            end if ! OPT_AB
!
            DX_Aalp=MOL_atoms(Aatom)%X-lp(alp)%X
            DY_Aalp=MOL_atoms(Aatom)%Y-lp(alp)%Y
            DZ_Aalp=MOL_atoms(Aatom)%Z-lp(alp)%Z
!
            DX_alpA=-DX_Aalp
            DY_alpA=-DY_Aalp
            DZ_alpA=-DZ_Aalp
!
! Calculate lone pair contribution to gradient
!
            if(OPT_AlpB)then
              g_und(I_Ax)=g_und(I_Ax)+dV*DX_Aalp/R
              g_und(I_Ay)=g_und(I_Ay)+dV*DY_Aalp/R
              g_und(I_Az)=g_und(I_Az)+dV*DZ_Aalp/R
!
              g_und(I_alpx)=g_und(I_alpx)+dV*DX_alpA/R
              g_und(I_alpy)=g_und(I_alpy)+dV*DY_alpA/R
              g_und(I_alpz)=g_und(I_alpz)+dV*DZ_alpA/R
            else
              g_def(I_Ax)=g_def(I_Ax)+dV*DX_Aalp/R
              g_def(I_Ay)=g_def(I_Ay)+dV*DY_Aalp/R
              g_def(I_Az)=g_def(I_Az)+dV*DZ_Aalp/R
!
              g_def(I_alpx)=g_def(I_alpx)+dV*DX_alpA/R
              g_def(I_alpy)=g_def(I_alpy)+dV*DY_alpA/R
              g_def(I_alpz)=g_def(I_alpz)+dV*DZ_alpA/R
            end if
!
          end do ! alp
!
          deallocate(V_ee_Aalp,V_ee_Ailp) 
          deallocate(r_ee_Aalp,r_ee_Ailp) 
          deallocate(r_ee_Aalp_opt,r_ee_Ailp_opt) 
!
        end if ! Nlp.gt.0
!
      end do ! Aatom
!
! Calculate lone pair-lone pair components of gradient and Hessian
!
      if(Nlp.gt.1)then
        do alp=1,Nlp
!
          I_alpx=3*(molecule%Natoms+alp)-2
          I_alpy=3*(molecule%Natoms+alp)-1
          I_alpz=3*(molecule%Natoms+alp)
!
          do blp=1,Nlp
            if(alp.eq.blp)cycle
!
            R=dsqrt((lp(alp)%X-lp(blp)%X)**2&
                  +(lp(alp)%Y-lp(blp)%Y)**2&
                  +(lp(alp)%Z-lp(blp)%Z)**2)
!
            if(lp(alp)%atom.eq.lp(blp)%atom)then
              call SEST_LP_A_LP_A_PARAMS(alp, R_e)
            else
              call SEST_LP_A_LP_B_PARAMS(alp, blp, R_e)
            end if
!
            ! V_ee_alp_blp
            dV=-2.0D0*V_ee_alpblp*(R_e-r_ee_alpblp)/(R-r_ee_alpblp)**2
!
            DX_alpblp=lp(alp)%X-lp(blp)%X
            DY_alpblp=lp(alp)%Y-lp(blp)%Y
            DZ_alpblp=lp(alp)%Z-lp(blp)%Z
!
            I_blpx=3*(molecule%Natoms+blp)-2
            I_blpy=3*(molecule%Natoms+blp)-1
            I_blpz=3*(molecule%Natoms+blp)
! Calculate gradient
            g_def(I_alpx)=g_def(I_alpx)+dV*DX_alpblp/R
            g_def(I_alpy)=g_def(I_alpy)+dV*DY_alpblp/R
            g_def(I_alpz)=g_def(I_alpz)+dV*DZ_alpblp/R
!
          end do
!
        end do
      end if
!
      call PRG_manager('exit','SEST_GRADIENT_OPT_PARAMS','UTILITY')
!
      end subroutine SEST_GRADIENT_OPT_PARAMS
!
      subroutine emm_mingrad_fn
!**********************************************************************
! Date last modified: February 20, 2008                               *
! Author: J.W. Hollett                                                *
! Description:  Given a set of r parameters, calculates the necessary *
!               scale factor S, and then the total gradient length.   *
!**********************************************************************
! Modules:
      use program_manager
      use OPT_objects
      use SEST_tools
      use type_molecule

      implicit none

! Local scalars:
      double precision :: E_MM_und, E_MM_def, S
      integer :: I
! Local arrays:
      double precision, allocatable, dimension(:) :: g, g_def, g_und
!
! Begin:
!
      call PRG_Manager('enter','emm_mingrad_fn','UTILITY')
!
      write(6,'(/a)')'PARSET'
      do I=1,Noptpr
        write(6,'(F14.8)')PARSET(I)
      end do
!
! Get SEST energy (V_AB, V_AlpB, and V_lpAlpB)  for current parameters
      call SEST_ENERGY_OPT_PARAMS(E_MM_def, E_MM_und)
!
      E_MM_def=E_MM_def+(SEST_ENERGY%V_ee_A+SEST_ENERGY%V_Ne_A+SEST_ENERGY%V_NN)/2.0D0
!
! Calculate scale factor required such that E_HF=E_MM holds
      S=(E_HF-E_MM_def)/E_MM_und
      write(6,'(/a,F14.8)')'scale factor S= ',S
      write(6,'(a,F14.8)')'E_MM_def= ',E_MM_def
      write(6,'(a,F14.8)')'E_MM_und= ',E_MM_und
      write(6,'(a,F14.8)')'E_MM_def+E_MM_und= ',E_MM_def+E_MM_und
      write(6,'(a,F14.8)')'E_MM_tot= ',E_MM_def+S*E_MM_und
!
      allocate(g(3*(molecule%Natoms+Nlp)), g_und(3*(molecule%Natoms+Nlp)), g_def(3*(molecule%Natoms+Nlp)))
      call SEST_GRADIENT_OPT_PARAMS(g_def, g_und)
! Calculate total gradient
      g(1:3*(molecule%Natoms+Nlp))=g_def(1:3*(molecule%Natoms+Nlp))+S*g_und(1:3*(molecule%Natoms+Nlp))
!
! OPT_function%value is the gradient length
!      OPT_function%value=0.0D0
!      do I=1,3*(molecule%Natoms+Nlp)
!        OPT_function%value=OPT_function%value+g(I)*g(I)
!      end do
! Take OPT_function as sections of the gradient
! In this case, x,y,z for H1 and H2
      OPT_function%value=0.0D0
      do I=7,12
        OPT_function%value=OPT_function%value+g(I)*g(I)
      end do
      OPT_function%value=dsqrt(OPT_function%value)
!
      deallocate(g,g_und,g_def)
!
      call PRG_Manager('exit','emm_mingrad_fn','UTILITY')
!
      end subroutine emm_mingrad_fn
!
      subroutine SEST_Force_Constant(N_lonepairs)
!**********************************************************************
! Date last modified: April 10, 2008                                  *
! Author: J.W. Hollett                                                *
! Description:  Allocates and defines the SEST force constant matrix.  *
!**********************************************************************
! Modules:
      use program_manager
      use type_molecule
      use SEST_tools
      use QM_objects
      use type_elements
!
      implicit none
!
! Output:
      integer, intent(OUT) :: N_lonepairs
! Work array:
      double precision, allocatable :: g(:), H(:,:)
! Local scalars:
      integer :: Aatom
      double precision :: c_v
!
! Begin:
!
      call PRG_Manager('enter','SEST_Force_Constant','UTILITY')
!
! Copy Cartesian to molecule, the rest of this code uses type molecule(should change this to cartesian eventually)
      do Aatom=1,molecule%Natoms
        MOL_atoms(Aatom)%symbol=trim(ELEMENT_SYMBOLS(CARTESIAN(Aatom)%Atomic_number))
        MOL_atoms(Aatom)%X=CARTESIAN(Aatom)%X
        MOL_atoms(Aatom)%Y=CARTESIAN(Aatom)%Y
        MOL_atoms(Aatom)%Z=CARTESIAN(Aatom)%Z
      end do
!
! Need valency of atoms with lone pairs
      call SEST_DEFINE_BONDS
! Determine number of lone pairs and their positions
      if(.not.allocated(LP))then
        call SEST_COUNT_LONE_PAIRS
        if(Nlp.gt.0)then
          call SEST_PUT_LONE_PAIRS
        end if
      end if
! 
      allocate(g(3*(molecule%Natoms+Nlp)),H(3*(molecule%Natoms+Nlp),3*(molecule%Natoms+Nlp)))
!
! Get gradient and Hessian
      call SEST_CALC_DERIVS(g,H)
!
     if(.not.allocated(Force_Constant))then
       allocate(Force_Constant(3*(molecule%Natoms+Nlp),3*(molecule%Natoms+Nlp)))
     end if
! Force constant is Hessian multiplied by (c_v-1)/c_v
      c_v=2.000362789D0
!      c_v=2.0D0
      Force_Constant=H*(c_v-1.0D0)/c_v
      N_lonepairs=Nlp
!
      deallocate(g,H)
!
      call PRG_Manager('exit','SEST_Force_Constant','UTILITY')
!
! end of subroutine SEST_Force_constant
      end
