      subroutine MENU_program
!***********************************************************************
!     Date last modified: February 12, 2003               Version 2.0  *
!     Authors: G. Jansen & J. G. Angyan, R.A. Poirier                  *
!     Desciption: set-up menu for EXECUTE command                      *
!***********************************************************************
! Modules:
      USE program_parser
      USE menu_gets

      implicit none
!
! Local scalars:
      logical done, lrun
      character(len=MAX_length)  prgnam, outnam, inpnam
      integer lenprg, leninp, lenout
!
! Begin:
      call PRG_manager ('enter', 'MENU_program', 'UTILITY')
!
! Defaults:
      prgnam = '        '
      lenprg = 8
      inpnam = '        '
      leninp = 8
      outnam = '        '
      lenout = 8

! Menu:
      done=.false.

      do while (.not.done)
      call new_token ('PROgram:')
!
      if(token(1:4).eq.'HELP')then
      write(UNIout,'(a)') &
      ' Command "PROGRAM":                              ', &
      '   Purpose: execute a program                    ', &
      '   Syntax : PROgram                              ', &
      '     NAMe      = <progname>                      ', &
      '     INPut     = <inputname>                     ', &
      '     OUTput    = <outputname>                    ', &
      '     RUN       = run the program                 ', &
      '     end                                         ', &
      '  Remarks:                                       ', &
      '                                                             ', &
      '  On UNIX and similar systems the full path names should     ', &
      '  be provided, otherwise everything should be in the         ', &
      '  current working directory from which the program is        ', &
      '  executed. Example:                                         ', &
      '     name   = "/dir/subdir/program"                          ', &
      '     input  = "/dir/subdir/input"                            ', &
      '     output = "/dir/subdir/output"                           ', &
      '     run                                                     ', &
      '   end'

      else if(token(1:3).eq.'RUN')then
        lrun = .true.

      else if(token(1:3).eq.'NAM')then
        call GET_value (prgnam, lenprg)

      else if(token(1:3).eq.'INP')then
        call GET_value (inpnam,leninp)

      else if(token(1:3).eq.'OUT')then
        call GET_value (outnam,lenout)
!
      else
         call MENU_end (done)
      end if

      end do
!
      if (lrun)then
        if (prgnam(1:3).eq.'   ')then
          write(UNIout,'(a)') &
        ' Warning_EXEPRG> no program name supplied'
        else
          call EXEPRG (prgnam, inpnam, outnam, .true.)
        end if
      end if
!
! end of routine  MENU_program
      call PRG_manager ('exit', 'MENU_program', 'UTILITY')
      return
      end
      subroutine MENU_free
!***********************************************************************
!     Date last modified: September 24, 2001              Version 2.0  *
!     Author: R.A. Poirier                                             *
!     Desciption: Dump an object to a disk file                        *
!***********************************************************************
! Modules:
      USE program_parser
      USE menu_gets

      implicit none
!
! Local scalars:
      character(len=MAX_length) dumobj,istring,format
      integer lenfrm,lenstr
      logical done

! Begin:
      call PRG_manager ('enter', 'MENU_FREE', 'UTILITY')

      format ='????????'
!
! Menu:
      done =.false.
      do while (.not.done)
         call new_token (' FREe:')

      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      ' Command FREe: ', &
      '   Purpose: free a object from stack and dump on disk ', &
      '   Syntax :', &
      '   FREe ', &
      '     VARiable|OBJect = <obj-name|all|*> ', &
      '     * = all ' , &
      '     FORmat   = <string> : dump file format ', &
      '                string = (fortran format) or "BINARY"', &
      '                default= (3D23.15)                   ', &
      '   end '

      else if(token(1:3).eq.'VAR'.or.token(1:3).eq.'OBJ')  then
        call GET_value (dumobj, lenstr)

      else if(token(1:3).eq.'FOR')  then
        call GET_value (Istring, lenstr)
        call trimld (Istring, format, lenfrm)

      else
        call MENU_end (done)
      end if

      end do

      if(dumobj(1:3).eq.'ALL'.or.dumobj(1:1).eq.'*')then
        write(UNIout,'(a)')'MENU_free: all objects are freed and dumped'
        write(UNIout,'(a)')'MENU_free: This option is not available'
        stop ' MENU_free: This option is not available'
      else
!       call FREE_object (dumobj, format)
      end if

! end of routine MENU_free
      call PRG_manager ('exit', 'MENU_FREE', 'UTILITY')
      return
      end
      subroutine MENU_command
!***********************************************************************
!     Date last modified: September 24, 2001              Version 2.0  *
!     Author: R.A. Poirier                                             *
!     Desciption: Set command menu                                     *
!***********************************************************************
! Modules:
      USE program_manager
      USE program_files
      USE program_parser
      USE menu_gets

      implicit none
!
! Local scalars:
      character(len=MAX_length) string, target
      character(len=MAX_length) Object
      character(len=3) defnam, dumptp, stotyp
      integer lenstr, mole
      logical done, ldumping, lstore

! Begin:
      done=.false.

      dumptp ='OBJ'
      stotyp ='OBJ'

! Menu:
      do while(.not.done)
      call new_token (' Set:')
      if(token(1:4).eq.'HELP')then
      write(UNIout,'(a)') &
      ' Command "Set":                                                ', &
      '   Purpose:  Set a token to some value                         ', &
      '   Syntax :  <token>  =  <value>                               ', &
      '  BLK= <integer> :blksize                                      ', &
      '  BOM= <integer> :bomlev                                       ', &
      '  CHK= <alias CHE>                                             ', &
      '  CHE=  PROgram       : pgmchk = .true.                        ', &
      '        EXEcution     : exechk = .true. (check mode of exec.)  ', &
      '        PATh-to-Object=<Object-name> (check path to target-Obj)', &
      '  CPU= <routine-name> : displays total cpu and cpu spent here  ', &
      '  DEB= <routine-name> : activates debugging mode of routine    ', &
      '  DBGlen= <integer>   : number of elements printed in debug    ', &
      '  DEP= <true|false>   : activates writing dependancy list      ', &
      '  DIR= <directory-name>                                        ', &
      '  DUM= SCALar|OBject = <boolean=true> : dump of object allowed ', &
      '  ECH= <boolean>                                               ', &
      '  EQU= <boolean=true> keep = sign when parsing                 ', &
      '  ERR= <integer> if integer > level stops program              ', &
      '  INTeractive = <boolean=false>                                ', &
      '  FMT = format for Dumped Objects                              ', &
      '  FMT =<type>=(format)ex: " set fmt integer = (3i15)  end "    ', &
      '       <type>= INTeger| DOUble | REAl | BOOlean | LOGical      ', &
      '  ITErative = <logical=false>                                  ', &
      '  LENgth =<integer>                                            ', &
      '  LREC                                                         ', &
      '  MOLecule = <mol-name> assign a DEFAULT molecule name         ', &
      '  OBJect =<Object-Command>                                     ', &
      '  PATh = <path sub-command>                                    ', &
      '  RESet = <Object-Name> reset all Objects path from Data-Object', &
      '  PGM or PRO = program name                                    ', &
      '  PRImitive = <logical=true> way for ObjRes= primitive default ', &
      '  PRM                                                          ', &
      '  PRT_level =<integer> printing level                          ', &
      '  PURge = <boolean=false>                                      ', &
      '           purge all files  catalogued by the program          ', &
      '  RUN   = <run sub-command>                                    ', &
      '  SEC                                                          ', &
      '  SILent = <routine-name>                                      ', &
      '  STO = SCALar|OBject = <boolean=true>                         ', &
      '        storage of dumped-object allowed                       ', &
      '  SEC                                                          ', &
      '  STEp <integer|incr> modify Current Step for ObjStp in putobj ', &
      '  TEMpdirectory= <string> temp ditectory (used with SLURM)     ', &
      '  TIM                                                          ', &
      '  TRACE_level =<integer> routine tracing level (msgprt/msgend) ', &
      '  USER                                                         ', &
      '  VOL                                                          ', &
      '  WARNING_level =<integer> warning''s level                    ', &
      '  end '

!      else if(token(1:3).eq.'BLK')then
!                                        call GET_value (BLKSIZ)
!      else if(token(1:3).eq.'BOM')then
!                                        call GET_value (errlev)
!      else if(token(1:3).eq.'CHK')then
!                                        call GET_value (string, lenstr)
!               exechk = string(1:3).eq.'EXE'
!               pgmchk = string(1:3).eq.'PRO'
!      else if(token(1:3).eq.'CHE')then
!                                        call GET_value (string, lenstr)
!               exechk = string(1:3).eq.'EXE'
!               pgmchk = string(1:3).eq.'PRO'
!               if(string(1:3).eq.'PAT')then
!                  call GET_value (target, lenstr)
!                  if(target(1:2).eq.'TO') call GET_value (target, lenstr)
!                  call CHKLNK (Target)
!               end if

      else if(token(1:3).eq.'CPU')then
        call MENU_timing

!      else if(token(1:3).eq.'DBG')then
!         call GET_value (DBGLEN)

      else if(token(1:3).eq.'DEB')then
        call MENU_debug

!      else if(token(1:3).eq.'DEP')then
!        call GET_value (depbld)
      else if(token(1:3).eq.'DIR')then
        call GET_value (Directory_name, lenstr)
!
! TEMpdirectory
      else if(token(1:3).eq.'TEM')then
        call GET_value (string, lenstr)
         write(UNIout,'(2a)')'Temp directory set to:',string(1:len_trim(string))
        if(index(string, 'SLURM_TMPDIR').ne.0)then
          call get_environment_variable("SLURM_TMPDIR", tempdir)
          write(UNIout,'(2a)')'Temp directory set to:',tempdir(1:len_trim(tempdir))
        end if
!
      else if(token(1:3).eq.'DUM')then
           call GET_value (ldumping)
!           dusmod=dumptp(1:3).eq.'SCA'.and.ldumping
!           dummod=dumptp(1:3).eq.'OBJ'.and.ldumping

      else if(token(1:3).eq.'ECH')then
        call GET_value (Lecho)
      else if(token(1:3).eq.'EQU')then
        call GET_value (KeepEqual)
!      else if(token(1:3).eq.'ERR')then
!        call GET_value (errlev)
      else if(token(1:3).eq.'FMT')  then
! set fmt integer = (3i15)  end
!                       call GET_value (string, lenstr)
!                       call GET_value (DUMFMT, lenstr)
!                       if(string(1:3).eq.'BOL') dumfmb = dumfmt
!                       if(string(1:3).eq.'LOG') dumfmb = dumfmt
!                       if(string(1:3).eq.'INT') dumfmi = dumfmt
!                       if(string(1:3).eq.'DOU') dumfmr = dumfmt
!                       if(string(1:3).eq.'REA') dumfmr = dumfmt

!      else if(token(1:3).eq.'INT')then
!         call GET_value (Interactive)

!      else if(token(1:3).eq.'ITE')then
!         call GET_value (iterative)

!      else if(token(1:4).eq.'LREC')then
!         call GET_value (LRECL)

      else if(token(1:3).eq.'OBJ')then
         call GET_value (Object, lenstr)
!         call SETOBJ (Object)

!      else if(token(1:3).eq.'MOL')then
! Improve: need something like ADDMOL (defnam)
!         call GET_value (defnam, lenstr)
!         call putscc ('MODALITY_NAMES:MOL=_SCC_CURRENT', defnam)

      else if(token(1:3).eq.'OBJ')then
           stotyp = 'OBJ'
           dumptp = 'OBJ'

      else if(token(1:3).eq.'PAT')then
           call MENU_path

      else if(token(1:3).eq.'RES')then
            call GET_value (Object, lenstr)
!            call OBJPATH (Object)

!      else if(token(1:3).eq.'PGM'.or.token(1:3).eq.'PRO')then
!        call GET_value (DUMPGM, lenstr)
!      else if(token(1:3).eq.'PRI')then
!        call GET_value (Primitive)
!      else if(token(1:3).eq.'PRM')then
!        call GET_value (PRMALL)
      else if(token(1:3).eq.'PRT')then
        call GET_value (PRINT_level)
        MUN_prtlev=PRINT_level
!
!      else if(token(1:3).eq.'PUR')then
!        call GET_value (ctgpur)

      else if(token(1:3).eq.'RUN')then
        call MENU_run

      else if(token(1:3).eq.'STO')then
           call GET_value (lstore)
!           stsmod=stotyp(1:3).eq.'SCA'.and.lstore
!           stomod=stotyp(1:3).eq.'OBJ'.and.lstore

      else if(token(1:3).eq.'SCA')then
           stotyp = 'SCA'
           dumptp = 'SCA'
!      else if(token(1:3).eq.'TIM')then
!                                  call GET_value (timlev)
      else if(token(1:3).eq.'TRA')then
        call GET_value (TRACE_level)
        if(TRACE_level.gt.0)Ltrace=.true.

      else if(token(1:4).eq.'USER')then
        call GET_value (USER_name, lenstr)

      else if(token(1:3).eq.'WRN')then
        call GET_value (WARNING_level)

      else
        call MENU_end (done)
      end if
      end do !. while not done

!      if(PRINT_level.ge.5)then
!        write(UNIout,'(a,l1)')
!     .' MENU_command: check execution set to:',exechk
!        write(UNIout,'(a,l1)')
!     .' MENU_command: check  program  set to:',pgmchk
!        write(UNIout,'(a,l1)')
!     .' MENU_command: purge all catalogued files set to :',ctgpur
!        write(UNIout,'(a,l1)')
!     .' MENU_command: store objects   set to:',stomod
!        write(UNIout,'(a,l1)')
!     .' MENU_command: store scalars   set to:',stsmod
!        write(UNIout,'(a,l1)')
!     .' MENU_command: dump  objects   set to:',dummod
!        write(UNIout,'(a,l1)')
!     .' MENU_command: dump  scalars   set to:',dusmod
!      end if

! end of routine  MENU_command
      return
      end
      subroutine MENU_build
!***********************************************************************
!     Date last modified: September 24, 2001              Version 2.0  *
!     Author: F.Colonna and R.A. Poirier                               *
!     Desciption: Build an Object (will not necessarily print)         *
!***********************************************************************
! Modules:
      USE program_parser
      USE menu_gets

      implicit none
!
! local scalars:
      logical done
      character(len=MAX_length) object
      integer lenstr, indobj

! Begin:
      call PRG_manager ('enter', 'MENU_build', 'UTILITY')

      done =.false.
      do while (.not.done)
      call new_token (' BUIld:')
      if(token(1:4).eq.'HELP')then
        write(UNIout,'(A)') &
      ' Command "BUIld" ', &
      '   Purpose:  Build an Object (execute a subroutine)', &
      '   Syntax:  BUIld ', &
      '              Object|Variable = <object-name> or <alias>', &
      '            END'

      else if(token(1:1).eq.'O'.or.token(1:1).eq.'V')then
        call GET_value (Object, lenstr)
        if(index(Object,':').ne.0)call GET_objectM (Object, .false.) ! If contains : must be an object
      else
        call MENU_end (done)
      end if
      end do !. while

! end of routine MENU_build
      call PRG_manager ('exit', 'MENU_build', 'UTILITY')
      return
      end
      subroutine MENU_run
!***********************************************************************
!     Date last modified: September 24, 2001              Version 2.0  *
!     Author: F.Colonna and R.A. Poirier                               *
!     Desciption: allocate RUN_name                                    *
!     parameters : Name, Input-unit, Output-unit                       *
!***********************************************************************
! Modules:
      USE program_parser
      USE menu_gets

      implicit none
!
! Local scalars:
      integer begin, skip, stop, lenstr
      logical done
      character(len=MAX_length) string

! begin:
      call PRG_manager ('enter', 'MENU_run', 'UTILITY')
!
! Rotating command menu:
      done=.false.

      do while (.not.done)
      call new_token (' Run:')
      if     (token(1:4).eq.'HELP')then
      write(UNIout,'(A)') &
      ' Command "RUN":', &
      '   Purpose: attributes RUN parameters:', &
      '   Syntax RUN                                             ', &
      '   Syntax     INPut = <integer=5>                         ', &
      '   Syntax     OUTput= <integer=6>                         ', &
      '   Syntax     NAMe  = <8*character>                       ', &
      '   Syntax name= name  end prefix added to file names:', &
      '   end'
      else if(token(1:4).eq.'NAME')then
        call GET_value (string,lenstr)
        RUN_name(1:)=string(1:lenstr)//'       '
        JOB_name(1:8)=RUN_name(1:8)
        Name_set=.true.

      else if(token(1:4).eq.'BEGI')then
        call GET_value (begin)

      else if(token(1:4).eq.'SKIP')then
        call GET_value (skip)

      else if(token(1:4).eq.'stop ')then
        call GET_value (stop)

      else if(token(1:3).eq.'INP')then
        call GET_value (unitin)

      else if(token(1:3).eq.'OUT')then
        call GET_value (UNIout)

      else
         call MENU_end (done)
      end if
!
      end do
!
      if(Lecho)write(UNIout,'(a,a)')'MENU_run:run name set to: ',RUN_name

! end of routine
      call PRG_manager ('exit', 'MENU_run', 'UTILITY')
      return
      end
      subroutine MENU_debug
!*************************************************************************************************
!     Date last modified:  March 14, 2001                                                        *
!     Author:  Darryl Reid                                                                       *
!     Description: Sets Object_Debug(ObjNum) logical array to true for the selected routine,     *
!                  provided from the input using the 'DEB' token.                                *
!*************************************************************************************************
! Modules
      USE program_objects ! contains Object_Debug
      USE menu_gets

      implicit none
!
! Local scalars:
      character(len=MAX_length) routineIN
      integer lenstr, LocalObjNum
!
! Begin:
      call PRG_manager ('enter', 'MENU_debug', 'UTILITY')
!
      call GET_value (routineIN, lenstr)
      call CNV_SLtoU (routineIN)
      NRoutines=NRoutines+1
      Routine_list(NRoutines)=routineIN(1:lenstr)
      Routine(NRoutines)%debug=.true.

! end of routine MENU_debug
      call PRG_manager ('exit', 'MENU_debug', 'UTILITY')
      return
      end subroutine MENU_debug
      subroutine MENU_timing
!*************************************************************************************************
!     Date last modified:  March 14, 2001                                                        *
!     Author:  Darryl Reid                                                                       *
!     Description: Sets Object_Debug(ObjNum) logical array to true for the selected routine,     *
!                  provided from the input using the 'DEB' token.                                *
!*************************************************************************************************
! Modules
      USE program_objects ! contains Object_Debug
      USE menu_gets

      implicit none
!
! Local scalars:
      character(len=MAX_length) routineIN
      integer IRoutine,lenstr,LocalObjNum
!
! Local functions:
      integer GET_routine_number
!
! Begin:
      call PRG_manager ('enter', 'MENU_timing', 'UTILITY')
!
      call GET_value (routineIN, lenstr)
      call CNV_SLtoU (routineIN)
      IRoutine=GET_routine_number(routineIN)
      if(IRoutine.gt.0)then ! Already in list
        Routine(IRoutine)%cputiming=.true.
      else
        NRoutines=NRoutines+1
        Routine_list(NRoutines)=routineIN(1:lenstr)
        Routine(NRoutines)%cputiming=.true.
      end if

! end of routine MENU_timing
      call PRG_manager ('exit', 'MENU_timing', 'UTILITY')
      return
      end subroutine MENU_timing
      subroutine MENU_output
!***********************************************************************
!     Date last modified: September 24, 2001              Version 2.0  *
!     Author: F.Colonna and R.A. Poirier                               *
!     Desciption: Output an Object                                     *
!***********************************************************************
! Modules:
      USE program_parser
      USE menu_gets

      implicit none
!
! local scalars:
      logical done
      character(len=MAX_length) object
      integer lenstr, indobj

! Begin:
      call PRG_manager ('enter', 'MENU_output', 'UTILITY')

      done =.false.

      do while (.not.done)
      call new_token (' Output:')
      if(token(1:4).eq.'HELP')then
      write(UNIout,'(A)') &
      ' Command "Output" ', &
      '   Purpose:  print an Object on standard output', &
      '   Syntax:  OUTput ', &
      '              Dump = <name> (not an object)', &
      '              Object|Variable = <object-name> or <alias>', &
      '              Table (collection of objects = <table-name>', &
      '            END'

! Dump 
      else if(token(1:1).eq.'D')then
        call GET_value (Object, lenstr)
        call GET_objectM (Object, .true.) ! print=.true.

! Output or Variable
      else if(token(1:1).eq.'O'.or.token(1:1).eq.'V')then
        call GET_value (Object, lenstr)
        call GET_objectM (Object, .true.) ! print=.true.

! Table
      else if(token(1:1).eq.'T')then
        call GET_value (Object, lenstr)
        call PRT_table (Object)
!       call GET_objectM (Object, .true.) ! print=.true.

      else
        call MENU_end (done)
      end if
      end do !. while

! end of routine MENU_output
      call PRG_manager ('exit', 'MENU_output', 'UTILITY')
      return
      end subroutine MENU_output

      subroutine MENU_path
!***********************************************************************
!     Date last modified: December 10, 2013                            *
!     Author: O. Stueker                                               *
!     Desciption: Define Paths for external components                 *
!***********************************************************************
! Modules:
      USE program_parser
      USE program_files
      USE menu_gets

      implicit none
!
! local scalars:
      logical done
      integer lenstr

! Begin:
      call PRG_manager ('enter', 'MENU_path', 'UTILITY')

      done =.false.

      do while (.not.done)
      call new_token ('Print:')
      if(token(1:4).eq.'HELP')then
      write(UNIout,'(A)') &
      ' Command "Path" ', &
      '   Purpose: Define Paths for external components         ', &
      '   Syntax:  PATh                                         ', &
      '              MOPac_bin = <path to mopac binary>         ', &
      '            END'
!
      else if(token(1:3).eq.'MOP')then
        call GET_value (Mopac_Bin, lenstr)
!       write(UNIout,'(3a)')' Set MOPAC_BIN to: "',trim(Mopac_Bin),'"'
!
      else
        call MENU_end (done)
      end if
      end do !. while

! end of routine MENU_path
      call PRG_manager ('exit', 'MENU_path', 'UTILITY')
      return
      end subroutine MENU_path

      subroutine MENU_print
!***********************************************************************
!     Date last modified: September 24, 2001              Version 2.0  *
!     Author: F.Colonna and R.A. Poirier                               *
!     Desciption: Output an Object                                     *
!***********************************************************************
! Modules:
      USE program_parser
      USE menu_gets

      implicit none
!
! local scalars:
      logical done
      character(len=MAX_length) object
      integer lenstr, indobj

! Begin:
      call PRG_manager ('enter', 'MENU_print', 'UTILITY')

      done =.false.

      do while (.not.done)
      call new_token ('Print:')
      if(token(1:4).eq.'HELP')then
      write(UNIout,'(A)') &
      ' Command "Print" ', &
      '   Purpose: Request printing on standard output', &
      '   Syntax:  PRInt ', &
      '              Dump = <name> (not an object)', &
      '              Documentation = <name> (not an object)', &
      '              Object|Variable = <object-name> or <alias>', &
      '              Table (collection of objects = <table-name>', &
      '            END'
! Dump
      else if(token(1:1).eq.'D')then
        call GET_value (Object, lenstr)
        call GET_objectM (Object, .true.)

! Object or Variable
      else if(token(1:1).eq.'O'.or.token(1:1).eq.'V')then
        call GET_value (Object, lenstr)
        call GET_objectM (Object, .true.)
! Table
      else if(token(1:1).eq.'T')then
        call GET_value (Object, lenstr)
        call GET_objectM (Object, .true.)

      else
        call MENU_end (done)
      end if
      end do !. while

! end of routine MENU_print
      call PRG_manager ('exit', 'MENU_print', 'UTILITY')
      return
      end
