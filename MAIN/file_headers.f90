      subroutine WRITE_header (ObjectI)
!*****************************************************************************************************************
!     Date last modified: July 25, 2001                                                             Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Desciption: Writes a file header on unit UNIT                                                              *
!*****************************************************************************************************************
! Modules:
      USE program_constants
      USE program_timing
      USE program_files

      implicit none
!
! Input scalars:
      character*(*), intent(IN) :: ObjectI
!
! Local scalars:
      character(len=MAX_string) string,writtn
      integer fmtlen, prim, lenstr, lns

! Begin:
      call PRG_manager ('enter', 'WRITE_header', 'UTILITY')

! units:
      call PRG_date
      call PRG_time
      writtn = ' by '//USER_name//' on '//sdate//' at '//stime

! dump variable:
      if(header_unit.ge.0)then
         write(header_unit,'(2a)')' ACCURACY:'
         write(header_unit,'(2a)')' FORMAT: ',file_format(1:len_trim(file_format))
         write(header_unit,'(a)')' PROGRAM: MUNgauss'
         write(header_unit,'(a)')' UNITS: Atomic units'
         write(header_unit,'(2a)')' OBJECT: ',ObjectI
         write(header_unit,'(a,I8)')' HEADER_dim1:',header_dim1
         write(header_unit,'(a,I8)')' HEADER_dim2:',header_dim2
         write(header_unit,'(2a)')' WRITTEN:',writtn(1:len_trim(writtn))
         write(header_unit,'(a)')' END OF HEADER'
      end if

! end of routine  WRITE_header
      call PRG_manager ('exit', 'WRITE_header', 'UTILITY')
      return
      end subroutine WRITE_header
      subroutine READ_header (UNIT)
!*****************************************************************************************************************
!     Date last modified: July 25, 2001                                                             Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Desciption: Reads a file header on unit UNIT                                                               *
!*****************************************************************************************************************
! Modules:
      USE program_constants
      USE program_files
!     USE scalar_objects
      USE type_basis_set
      USE type_molecule

      implicit none
!
! Input:
      integer, intent(IN) :: UNIT
!
! Local scalars:
!     integer :: Natoms,Nbasis
      integer i,prim,strlen,record,indl,indv,lennam,lenfil
      integer indn,indt,iostat,typlen,lenlin,lenfmt
      character(len=MAX_string) ligne,ObjectIN,string
      character*7 sname
      logical end_of_file,eofhdr,ldump
!
! Begin:
      call PRG_manager ('enter', 'READ_header', 'UTILITY')

! Improve:
      end_of_file=.false.
      eofhdr = .false.
      ldump = .false.
      file_basis = '        '
      file_formula = '        '
      file_format = '        '
      file_object = '        '
!
      do while (.not.eofhdr)
        ligne = ' '
        read(unit,'(a)',iostat=iostat) ligne
        call STR_size (ligne, prim, lenlin)
        write(UNIout,*)ligne(prim:lenlin)

        if(iostat.lt.0)then
          write(UNIout,'(a,I2)')'error_READ_header> end of file on UNIT ',UNIT
!         call STR_size(file_name(unit), prim, lenfil)
!         write(UNIout,'(3a)')'error_READ_header> file ="',file_name(unit)(1:lenfil),'"'
          call PRG_stop ('READ_header> End of File')
        else if(iostat.gt.0)then
          write(UNIout,'(a,i3,3a)')'error_READ_header> iostat=',iostat,' ligne:"',ligne(1:lenlin),'"'
          call PRG_stop ('READ_header> iostat > 0 ')
        end if

! if ***X skip reading title and rewind : MUNgauss File
        if(index(ligne(1:3),'***').ne.0)then
           write(UNIout,'(a)')'READ_header: reading MUNgauss / file rewind'
           rewind (unit=unit)
           call PRG_manager ('exit', 'READ_header', 'UTILITY')
           return
!
! data accuracy:
        else if(index(ligne(1:15), 'ACCURACY:').ne.0)then
           indv=index(ligne(1:15),':')
           string = ligne(indv+1:)
           call STR_size (string, prim, strlen)
!
! basis-name:
        else if(index(ligne(1:15),'BASIS_NAME:').ne.0)then
           indv=index(ligne(1:15),':')
           string = ligne(indv+1:)
           call STR_size (string, prim, strlen)
           file_basis = string(prim:strlen)
           write(UNIout,'(4a)')'Current basis: ',BASIS_name(1:len_trim(BASIS_name)), &
                             ' File basis: ',file_BASIS(1:len_trim(file_BASIS))
!
! content:
        else if(index (ligne(1:15),'CONTENT:').ne.0)then
           indv=index(ligne(1:15),':')
           string = ligne(indv+1:)
           call STR_size (string, prim, strlen)
!
! data-set name:
        else if(index(ligne(1:20),'FILENAME:').ne.0)then
           write (UNIout,'(3a)') ' READ_header: ',ligne(1:lenlin)

        else if(index(ligne(1:15),'DATASET_NAME:').ne.0)then
           indv=index(ligne(1:15),':')
           string=ligne(indv+1:)
           call STR_size (string, prim, strlen )
!
! dump:
        else if(index(ligne(1:8),'DUMP:').ne.0)then
           ldump  = .true.
           indv   = index(ligne(1:8),':')
!
! dump variable type:
        else if(index(ligne(1:8),'type:').ne.0)then
           indv   = index(ligne(1:8),':')
!
! nb of elements:
        else if(index(ligne(1:15),'NB_ELEMENTS:').ne.0)then
           indv=index(ligne(1:15),':')
           string=ligne(indv+1:)
!
! nb of bytes per word:
        else if(index(ligne(1:15),'NB_BYTES:').ne.0)then
           indv=index(ligne(1:15),':')
           string=ligne(indv+1:)
!
! format:
        else if(index(ligne(1:10),'FORMAT:').ne.0)then
           indv=index(ligne(1:10),':')
           string = ligne(indv+1:)
           call STR_size (string, prim, strlen)
           file_format = string(prim:strlen)
!
! formula:
        else if(index(ligne(1:15),'FORMULA:').ne.0)then
           indv=index(ligne(1:15),':')
           string = ligne(indv+1:)
           call STR_size (string, prim, strlen)
           file_formula = string(prim:strlen)
!
! HEADER_dim1:
        else if(index(ligne(1:15),'HEADER_dim1:').ne.0)then
           indv=index(ligne(1:15),':')
           string = ligne(indv+1:)
           call CNV_StoI (string, header_dim1)
!
! HEADER_dim2:
        else if(index(ligne(1:15),'HEADER_dim2:').ne.0)then
           indv=index(ligne(1:15),':')
           string = ligne(indv+1:)
           call CNV_StoI (string, header_dim2)
!
! nb of record:
        else if(index(ligne(1:15),'NB_RECORDS:').ne.0)then
           indv=index(ligne(1:15),':')
           string=ligne(indv+1:)
!
! nb of atoms:
        else if(index(ligne(1:15),'NB_ATOMS:').ne.0)then
          indv=index(ligne(1:15),':')
          string=ligne(indv+1:)
          call CNV_StoI (string, file_Natoms)
!         call GET_scalar ('NATOMS', Natoms)
          write(UNIout,'(a,I8,a,I8)')'Current Natoms: ',Natoms, ' File Natoms: ',file_Natoms
!
! Number of basis functions:
        else if(index(ligne(1:15),'NB_BASISF:').ne.0)then
          indv=index(ligne(1:15),':')
          string = ligne(indv+1:)
          call STR_size (string, prim, strlen)
          call CNV_StoI (string, file_Nbasis)
!         call GET_scalar ('NBASIS', Nbasis)
          write(UNIout,'(a,I8,a,I8)')'Current Nbasis: ',Basis%Nbasis, ' File Nbasis: ',file_Nbasis
!
! Object
        else if(index(ligne(1:15),'OBJECT:').ne.0)then
           indv=index(ligne(1:15),':')
           string = ligne(indv+1:)
           call STR_size (string, prim, strlen)
           file_object= string(prim:strlen)
!
! program:
        else if(index(ligne(1:10),'PROGRAM:').ne.0)then
           indv=index(ligne(1:15),':')
           string=ligne(indv+1:)
           call STR_size (string, prim, strlen )
!
! STATE
        else if(index(ligne(1:15),'STATE:').ne.0)then
           indv=index(ligne(1:15),':')
           string = ligne(indv+1:)
           call STR_size (string, prim, strlen)
!
! title:
        else if(index(ligne(1:8),'TITLE:').ne.0)then
           indv=index(ligne(1:8),':')
           string = ligne(indv+1:)
           call STR_size (string, prim, strlen)
           write(UNIout,'(2a)') ' file-title:',string(1:strlen)
!
! units:
        else if(index(ligne(1:8),'UNITS:').ne.0)then
           indv=index(ligne(1:15),':')
           file_units=ligne(indv+1:)
!
! written:
        else if(index(ligne(1:15),'WRITTEN:').ne.0)then
           call STR_size (ligne, prim, strlen)
           write(UNIout,'(2a)') ' file-title:',ligne(1:strlen)
!
! end of header:
        else if(index(ligne(1:15),'END OF HEADER').ne.0)then
              eofhdr = .true.
! end of file:
        else if(index(ligne(1:15),'end OF FILE').ne.0)then
              end_of_file = .true.
              eofhdr = .true.
        else
!           if(wrnlev.gt.0)then
!             call STR_size (ligne, prim, strlen)
!             write(UNIout,'(a/3a)')' warning_READ_header> unknown header item:',' "',ligne(1:strlen),'"'
!           end if
        end if

      end do ! while

      call PRG_manager ('exit', 'READ_header', 'UTILITY')
      return
! end of routine READ_header
      end subroutine READ_header
