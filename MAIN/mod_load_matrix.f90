      MODULE matrix_load
!****************************************************************************************************************************
!     Date last modified: February 16, 2000                                                                    Version 2.0  *
!     Author: R.A. Poirier                                                                                                  *
!     Description: Scalars and arrays used to read-in the basis set                                                         *
!****************************************************************************************************************************
! Modules:

      implicit none

      interface LOAD_MATRIX
        module procedure LOAD_matrixR
        module procedure LOAD_matrixI
        module procedure LOAD_matrixB
        module procedure LOAD_matrixL
      end interface

CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine LOAD_matrixI (Array,   & ! Array to be printed
                              Nrow,   & ! Dimension (NROWS)
                              Ncol)  ! Dimension (NCOLUMNS)
!***********************************************************************
!     Date last modified: June 26, 1992                                *
!     Author:                                                          *
!     Description: Matrix print routine.                               *
!***********************************************************************
! Modules:
      USE program_files

      implicit none
!
! Input scalars:
      integer, intent(in) :: Nrow,Ncol
!
! Input array:
      integer, dimension(:,:), intent(out) :: Array
!
! Local scalars:
      integer Irow,IrowIN,IFLAG,ILOWER,IUPPER,Jcol
!
! Begin:
      call PRG_manager ('enter', 'LOAD_matrixI', 'UTILITY')
!
      IFLAG=1
      ILOWER=1
    1 IUPPER=ILOWER+9
      IF(IUPPER.GE.Ncol)then
         IUPPER=Ncol
         IFLAG=0
      end if

      do Irow=1,Nrow
         read(header_unit,'(I3,10I12)')IrowIN,(Array(Irow,Jcol),Jcol=ILOWER,IUPPER)
      end do
      read(header_unit,*)
      IF(IFLAG.NE.0)then
        ILOWER=ILOWER+10
        GO TO 1
      end if
!
! End of routine LOAD_matrixI
      call PRG_manager ('exit', 'LOAD_matrixI', 'UTILITY')
      return
      end subroutine LOAD_matrixI
      subroutine LOAD_matrixB (Array,  & ! Boolean array to be printed
                              Nrow,   & ! Dimension (NROWS)
                              Ncol)  ! Dimension (NCOLUMNS)
!***********************************************************************
!     Date last modified: June 26, 1992                                *
!     Author:                                                          *
!     Description: Matrix print routine.                               *
!***********************************************************************
! Modules:
      USE program_files

      implicit none
!
! Input scalars:
      integer, intent(in) :: Nrow,Ncol
!
! Input array:
      logical, dimension(:,:), intent(out) :: Array
!
! Local scalars:
      integer Irow,IrowIN,IFLAG,ILOWER,IUPPER,Jcol
!
! Begin:
      call PRG_manager ('enter', 'LOAD_matrixB', 'UTILITY')
!
      IFLAG=1
      ILOWER=1
    1 IUPPER=ILOWER+19
      IF(IUPPER.GE.Ncol)then
         IUPPER=Ncol
         IFLAG=0
      end if

      do Irow=1,Nrow
         read(header_unit,'(1X,I3,2X,20L4)')IrowIN,(Array(Irow,Jcol),Jcol=ILOWER,IUPPER)
      end do
      read(header_unit,*)
      IF(IFLAG.NE.0)then
        ILOWER=ILOWER+20
        GO TO 1
      end if
!
! End of routine LOAD_matrixB
      call PRG_manager ('exit', 'LOAD_matrixB', 'UTILITY')
      return
      end subroutine LOAD_matrixB
      subroutine LOAD_matrixL (Array, &
                         Nelem, &
                         Nrow)  !
!***********************************************************************
!     Date last modified: January 26, 2005                             *
!     Author: R.A. Poirier and D. Keefe                                *
!     Description: Load a Matrix stored in linear from a file          *
!***********************************************************************
! Modules:
      USE program_files

      implicit none
!
! Input scalars:
      integer, intent(in) :: Nrow,Nelem
!
! Input array:
      double precision, dimension(:), intent(out) :: Array
!
! Local scalars:
      integer Irow,IrowIN,IFLAG,ILOWER,IUPPER,J
!
! Begin:
      call PRG_manager ('enter', 'LOAD_matrixL', 'UTILITY')
!
      IFLAG=1
      ILOWER=1
    1 IUPPER=ILOWER+9
      IF(IUPPER.GE.Nrow)then
         IUPPER=Nrow
         IFLAG=0
      end if
      read(header_unit,*)
      do Irow=1,Nrow
      read(header_unit,1002)IrowIN,(Array(max0(Irow,J)*(max0(Irow,J)-1)/2+min0(Irow,J)),J=ILOWER,IUPPER)
      end do ! Irow
      read(header_unit,*)
      IF(IFLAG.NE.0)then
        ILOWER=ILOWER+10
        GO TO 1
      end if
!
 1000 FORMAT(3X,10(9X,I3))
 1002 FORMAT(1X,I3,2X,10F12.6)
!
! End of routine LOAD_matrixL
      call PRG_manager ('exit', 'LOAD_matrixL', 'UTILITY')
      return
      end subroutine LOAD_matrixL
      subroutine LOAD_matrixR (Array,  & ! Array to be printed
                               Nrow,   & ! Dimension (NROWS)
                               Ncol)  ! Dimension (NCOLUMNS)
!***********************************************************************
!     Date last modified: June 26, 1992                                *
!     Author:                                                          *
!     Description: Matrix print routine.                               *
!***********************************************************************
! Modules:
      USE program_files

      implicit none
!
! Input scalars:
      integer, intent(in) :: Nrow,Ncol
!
! Input array:
      double precision, dimension(:,:), intent(out) :: Array
!
! Local scalars:
      integer Irow,Jcol
!
! Begin:
      call PRG_manager ('enter', 'LOAD_matrixR', 'UTILITY')
!
      write(uniout,'(a,2i8)')'LOAD_matrixR> Nrow,Ncol: ',Nrow,Ncol

      read(header_unit,fmt=file_format)((Array(Irow,Jcol),Irow=1,Nrow),Jcol=1,Ncol)
!
! End of routine LOAD_matrixR
      call PRG_manager ('exit', 'LOAD_matrixR', 'UTILITY')
      return
      end subroutine LOAD_matrixR

      end MODULE matrix_load
