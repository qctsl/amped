      MODULE program_constants
!*****************************************************************************************************************
!     Date last modified: February 18, 2000                                                         Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Fundamental Constants and numbers.                                                            *
!*****************************************************************************************************************
      implicit none

      integer, parameter :: MAX_string=132   ! Maximum length for strings
      double precision, parameter :: ZERO=+0.0D0
      double precision, parameter :: ONE=+1.0D0
      double precision, parameter :: TWO=+2.0D0
      double precision, parameter :: THREE=+3.0D0
      double precision, parameter :: FOUR=+4.0D0
      double precision, parameter :: PT5=+0.5D0
      double precision, parameter :: F1_3=+1.0D0/3.0D0

!*********************** Fundamental constants ***************************************
      double precision, parameter :: PI_VAL=+3.14159265358979D0
      double precision, parameter :: FourPi=Four*PI_VAL
      double precision, parameter :: Bohr_radius=+0.529177249D-10  ! meters

!     Planck's constant (CODATA approved)
      double precision, parameter :: Planck_h=6.6260693D-34 ! Joules*seconds

!     Planck's constant/ 2 pi
      double precision, parameter :: Planck_h_bar=Planck_h/(TWO*PI_VAL) ! =1.05457266D-34 ! Joules*seconds

!     Electron rest mass (CODATA approved)
      double precision, parameter :: E_mass=9.1093826D-31 ! kg

!     Elementary charge
      double precision, parameter :: E_charge=4.803242D-10 ! esu

!     Elementary charge (CODATA approved)
      double precision, parameter :: E_coul=1.60217653D-19   ! Coulombs

!     Avogadro's number
      double precision, parameter :: Avogadro=6.0221367D+23 ! particles/mol

!     Speed of light
      double precision, parameter :: C_speed=2.99792458D+08 ! meters/second

! Boltzman constant
      double precision, parameter :: Boltz_k=1.38066D-23 ! Boltzman constant k in J/K
!
! Conversion factors
      double precision, parameter :: Bohr_to_Angstrom=+0.529177249D0
      double precision, parameter :: Angstrom_to_Bohr=ONE/Bohr_to_Angstrom
      double precision, parameter :: Deg_to_Radian=PI_VAL/180.0D0

!******************************* Mass ******************************************* 
      double precision, parameter :: amu_to_kg=1.660538D-27

!****************************** Energy *******************************************
!     Calories to Joules.
      double precision, parameter :: Cal_to_J=4.184D0

!     Electron-Volts to Joules.
      double precision, parameter :: ev_to_J=1.6021892D-19

!     Converts EV to Wavenumbers.
      double precision, parameter :: ev_to_CM=8065.479D00

!     Hartree in steps to prevent underflow...
      double precision, parameter :: tempH=(Planck_h_bar/E_mass)/Bohr_radius
      double precision, parameter :: Hartree=tempH*Planck_h_bar/Bohr_radius  ! (was 2625.5001)

!     Hartrees to kJoules:
      double precision, parameter :: hartree_to_kJ=Hartree*Avogadro*1.0D-03

!     Hartrees/particle to KCal/mol.
      double precision, parameter :: Hartree_to_cal=Hartree_to_kJ/Cal_to_J

!     Hartrees/particle to EV/Particle.
      double precision, parameter :: Hartree_to_EV=Hartree/ev_to_J

!      Hartrees/particle to cm-1/particle.
!      HTOCM=HTOEV*evtoCM
!
!**************************** Forces and Force constants *********************
!     Hartree/Bohr to MDyne
!      Temp=(Phbar/Emass)/BohrA0

! Force conversion: Hartree/Bohr to MDyne.
!      FCONV=HARTREE*TEN8/BohrA0
!      double precision, parameter :: FCONV=Hartree*1.0D8/Bohr_radius
      double precision, parameter :: FCONV=8.238849568633083D0 ! mdynes/(Hartree/Bohr)
! Added energy parameter (Mdyne/Angstroms to Hartrees):
      double precision, parameter :: MDA_to_Hartree = Angstrom_to_Bohr/FCONV

! Force constant conversion: Hartree/Angstrom**2 to MDyne/Angstrom.
!      FCCONV=HARTRE*TEN18
!     Hartree/Angstrom**2 to MDyne/Angstrom
! Change to Hartree/bohr**2        ????
      double precision, parameter :: FCCONV=Hartree*1.0D18 ! =4.3598149
! Force constant conversion - stretches: Hartree/bohr**2 to MDyne/Angstrom = Fconv*Angstrom_to_Bohr
      double precision, parameter :: FCconv_str=Fconv*Angstrom_to_Bohr ! = 15.569168 ...

! Dipole moment conversion: Electron*Bohr to ESU*CM, 1D=10**-18 ESU*CM.
!     Electron*Bohr to Debye
      double precision, parameter :: DCONST=E_charge*Bohr_radius*1.0D20 ! =2.5417655
!
      end MODULE program_constants
