      MODULE program_parser
!***********************************************************************
!     Date last modified: May 15, 2001                    Version 2.0  *
!     Author: R.A. Poirier                                             *
!     Description: All scalars ... used by the parser                  *
!***********************************************************************
! Modules

      implicit none
!
      integer, parameter :: MAX_length=132  ! Maximum string lengths for menu
      integer, parameter :: MAX_units=99  ! Maximum number of units
      integer, parameter :: MAX_files=2000 ! maximum number of file names

      integer  :: linlen(MAX_units) ! current line length
      integer  :: wrdlen(MAX_units) ! current word length
      integer  :: curpos(MAX_units) ! current Position of CURWRD in CURLIN
      integer  :: comfll ! length of string "COMFIL"
      integer  :: curcml ! current command name length
      integer  :: curuni ! current UNIT
      integer  :: toklen ! token length
      integer  :: curpos_last ! Last current cursor position

      logical :: eofinp ! logical true if premature "end of file" on input file
      logical :: Lecho  ! logical true if echo switch on
      logical :: lline(MAX_units)  ! logical true if no error in getting CURLIN
      logical :: lword(MAX_units)  ! logical true if no error in getting CURWRD
      logical :: notart ! logical true if expression between " (not an arithmetic exp.)
      logical :: lread  ! logical true if expression not between  { }
      logical :: prompt ! true when interactive job
!     logical :: SUBSTI  ! logical true if symbol substitution is done in GET_word
      logical :: KeepEqual ! logical true if = sign is kept when parsing
      logical :: Interactive

      character(len=MAX_length) :: curlin(MAX_units) ! current line read from current unit
      character(len=MAX_length) :: curwrd(MAX_units) ! current word read in CURLIN
      character(len=MAX_length) :: comfil ! current command FILE
      character(len=MAX_length) token ! current sub-command token
      character*(14) :: curcom ! current command name
!
! FOR loops: FOR <forvar> = <forfst> to <forlst> step/by <forstp>
      logical :: loopexe ! true if loop commands are to be read and not executed
!
CONTAINS !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine INI_parser
!*****************************************************************************************************************
!     Date last modified: October 8, 2002                                                           Version 2.0  *
!     Author: F. Colonna and R.A. Poirier                                                                        *
!     Description: Initialize the parser.                                                                        *
!*****************************************************************************************************************
! Modules:
      USE program_files

      implicit none
!
! Local scalars:
      integer  char
!
! Begin:
      Lecho=.false.
!
      if(interactive)then
        Lecho=.false.
      end if
!
! Loop initialization:
!     SUBSTI =.true.    !. symbol substitution done in GET_word
      lread  =.true.    !. { } handling
!
! Parser initialization:
      do curuni=1,MAX_units
        do char=1,MAX_length
          curlin(curuni)(char:char)=' '
          curwrd(curuni)(char:char)=' '
        end do ! char
        wrdlen(curuni) = MAX_length
        lword(curuni)=.false.
        lline(curuni)=.false.
        curpos(curuni)=0
      end do ! curuni

! Parser:
      token = ' '
      toklen= MAX_length
      curuni = unitin

! end of routine INI_parser
      return
      end  subroutine INI_parser
      subroutine MENU_end (done)
!*************************************************************************************************
!     Date last modified:  March 11, 2008                                                        *
!     Author: F. Colonna and R. Poirier                                                          *
!           exit from rotating command menu
!           This routine is called by the else statement:
!           logical DONE is set to true when "END" statement is found
!           otherwise message is issued for invalid command name
!*************************************************************************************************
! Modules
      USE program_files

      implicit none
!
! Input scalars:
      logical :: done
!
! Begin:
      if(token(1:3).eq.'END')then
         done=.true.
      else
         write(UNIout,'(3a)')'WARNING MENU_end> in command:"',curcom(1:curcml),'"', &
        '        illegal sub-command:"',token(1:toklen),'"','        or "end" forgotten'
         if(ERROR_level.gt.0)then
           call PRG_stop ('Illegal sub-command ')
         end if
      end if

! end of routine MENU_end
      return
      end subroutine MENU_end
      logical function CHK_string (String, Set)
!*************************************************************************************************
!     Date last modified:  July 19, 1991                                                         *
!     Author: : F. Colonna                                                                       *
!           true if "String" only consists in characters from set "Set"
!*************************************************************************************************
! Modules:

      implicit none
!
! input scalars:
      character*(*)  String, Set

! local
      integer char, lenstr, lenset
!
! Begin:
      lenset = len_trim(Set)
      lenstr = len_trim(String)
      CHK_string = .true.
      do char =1, lenstr
         CHK_string = CHK_string .and.index(Set(1:lenset),string(char:char)).ne.0
      end do
!
! end of routine CHK_string
      return
      end function CHK_string

      subroutine NEW_token (mark)
!*************************************************************************************************
!     Date last modified:  July 19, 1991                                                         *
!     Author: : F. Colonna                                                                       *
!           puts the next input word into TOKEN, streams in new command file, mark is a prompt
!*************************************************************************************************
! Modules
      USE program_files

      implicit none
!
! Input scalars:
      character*(*) mark
!
! local   :
      character(len=MAX_length) filnam
      logical lerror,lexist
      integer unit,prim

! Begin:
      curcom = mark
      call STR_size (curcom, prim, curcml)
      if(curcom(curcml:curcml).ne.':')then
        if(curcml.lt.14) curcml = curcml +1
        curcom(curcml:curcml) = ':'
      end if

      call NEW_word

! in case of STREAM:
      if(token(1:7).eq.'STREAM:'.or. &
         token(1:7).eq.'STREAM='.or. &
         token(1:7).eq.'STREAM ')then

! get stream file name:
         if(token(8:8).eq.' ')then
            call new_word
            comfil=token
            comfll=toklen
         else
           comfil=token(8:)
           comfll=toklen-7
           if(comfil(comfll:comfll).eq.'"') comfll =  comfll -1
           if(comfil(1:1).eq.'"')then
             comfil =  token(9:toklen-1)
             comfll =  comfll -1
           end if
         end if

! alloc new STREAM file on unit UNIT:
      filnam=comfil
      call GET_unit (filnam, curuni, Lerror)
      write(UNIout,*)'NEW_token> File connected to unit ',curuni
      NStream_files=NStream_files+1
      if(NStream_files.le.MAX_stream)then
        PRG_stream_unit(NStream_files)=curuni
        PRG_file(curuni)%unit=curuni
        PRG_file(curuni)%status='OLD '
        PRG_file(curuni)%type='STREAM '
        PRG_file(curuni)%form='FORMATTED '
        PRG_file(curuni)%action='READ '
        PRG_file(curuni)%name=filnam
        call FILE_open (curuni)
      else
        write(UNIout,'(a)')'NEW_token> Exceeded maximum Number of Stream files'
        call PRG_stop ('Exceeded maximum Number of Stream files')
      end if

        if(PRINT_level.ge.20)then
          call STR_size (comfil, prim, comfll)
          write(UNIout,'(3a)')' new_token: File  "',comfil(1:comfll),'" Streamed-in'
          write(UNIout,'(a,i2)')' new_token: unit  =',curuni
        end if

!  get new word from new file:
        call NEW_word
      end if

! end of routine  NEW_token
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine NEW_word
!*************************************************************************************************
!     Date last modified:  July 19, 1991                                                         *
!     Author: : F. Colonna                                                                       *
!           follows NEW_token
!           checks for end of line in input. gets a new word
!*************************************************************************************************
! Modules

      implicit none
!
! Begin:
   10 continue
      call GET_word
! when end_of_line is reached get a new line:
      if(curwrd(curuni)(1:11).eq.'END_OF_LINE')then
        call GET_line
        go to 10
      end if

! end of routine  NEW_word
      return
      end subroutine NEW_word
      subroutine GET_word
!*************************************************************************************************
!     Date last modified:  July 19, 1991                                                         *
!     Author: : F. Colonna                                                                       *
!           gets a word from  current line of current input file
!           get rid of "
!           empty lines have been skipped in GET_line
!*************************************************************************************************
! Modules

      implicit none
! i/o     :
      integer unit
! local   :
      character(len=MAX_length) stemp, Otoken
      integer i,j,k,start,STOP, lenstr, shift, debut
!
! Begin:
      lword(curuni)=.true.

!  check END_OF_LINE then return
      if(curpos(curuni).gt.linlen(curuni))then
        lword(curuni)=.false.
        curwrd(curuni)='END_OF_LINE'
        return
      end if

!  if END_OF_LINE then return
      if(curwrd(curuni)(1:11).eq.'END_OF_LINE')then
        lword(curuni)=.false.
        return
      end if

!  get next word from  curpos + 1
      start = 0
      debut = curpos(curuni)
      if(curpos(curuni).eq.0) debut=1

      do i=debut, linlen(curuni)
          if(curlin(curuni)(i:i).ne.' ')then
            start=i
            go to 1000
          end if
      end do

! END_OF_LINE reached:
      lword(curuni)=.false.
      curwrd(curuni)(1:11)='END_OF_LINE'
      return
1000  continue
!
! one word is found in current line terminates with " or blank or =
! search for word length
      stemp = curlin(curuni)(start:)
      lenstr=len_trim(stemp)
      if(stemp(1:2).eq.'""')then ! No string given (RAP Feb 8, 2007)
        call PRT_warning ('GET_word', 'No word given - found ""')
        stop=3
        shift=-1
      else if(stemp(1:1).eq.'"')then
         shift = 1
         stemp = curlin(curuni)(start+1:)
         stop = index(stemp,'"')
         if(stop.eq.0)then
           call PRT_warning ('GET_word', 'one  "  is missing: rest of line included')
           stop=lenstr+1
         end if
      else
         shift = -1
         stop = index(stemp,' ')
         if(stop.eq.0) stop=lenstr + 1
      end if
!
      curwrd(curuni) = stemp(1:stop-1)
      wrdlen(curuni) = stop - 1

!  save TOKEN for MENUs
      token = curwrd(curuni)
      TOKLEN= wrdlen(curuni)

!  cut-out "=" stuck at the end of the current word:
      if(token(toklen:toklen).eq.'='.and.toklen.gt.1)then
         TOKLEN = toklen - 1
         wrdlen(curuni) = toklen
         curwrd(curuni)= curwrd(curuni)(1:toklen)
         token = curwrd(curuni)
      end if

! substitute symbols:
!     if(SUBSTI.and.index(token,'&').ne.0)then
!        call SYMTRL (token, Otoken, toklen)
!        token = Otoken
!        toklen=len_trim(token)
!     end if

! put cursor at new position
      curpos_last   = curpos(curuni)
      curpos(curuni)=start + stop + shift
!
! end of routine  GET_word
      return
      end subroutine GET_word

      end subroutine NEW_token
      subroutine GET_line
!*************************************************************************************************
!     Date last modified: September 29,2010                                                      *
!     Author: F. Colonna and R. A. Poirier                                                       *
!           Gets a new line from current INPUT unit and processes it (remove comments, ...)      *
!*************************************************************************************************
! Modules
      USE program_files

      implicit none
!
! local arrays:
      logical linside(MAX_length)

! local scalars:
      character*1 char1,CTAB
      character(len=MAX_length) line,stemp
      integer :: Ichar,prim,first,i,j,k,max,len,indc,ind1,ind2,lenlin
      logical found
      logical carret, boolean

! Begin:
!     call PRG_manager ('enter', 'GET_line', 'UTILITY')

      CTAB='	'

!  get a newline from unit "curuni"
      lline(curuni)=.false.
      curwrd(curuni)=' '
      curpos(curuni)=0
      token =' '
      do k=1,MAX_length
        linside(k)=.false.
      end do

 10   continue
!
! Improve
!      if(interactive.and.curuni.eq.5)then
!       line(1:MAX_length) = ' '
!       prompt = CARRET (stemp, len)
!       if(prompt)then
!         stemp='PROMPT'
!         len=6
!       end if
!       if(len.gt.0) line = stemp(1:len)//' '
!      else
        READ(curuni,'(a)',end=999)line
        call STR_size (line, prim, len)
        prompt =.false.
!      end if

! skip empty lines:
      if(len.eq.0.and..not.prompt)go to 10
      lline(curuni)=.true.

      if(Lecho)then
        len = max(len, 1)
        write(UNIout,'(a,a,a)')' input-cmd:',curcom(1:curcml),line(1:len)
      end if

! define current line
      curlin(curuni)=line(1:len)//' '
      linlen(curuni)=len

! Check for tabs:
      do Ichar=1,len
        if(line(Ichar:Ichar).eq.CTAB)line(Ichar:Ichar)=' '
      end do ! Ichar

! cut out commentaries  after | or !
      indc=index(line,'|')-1
      if(indc.ne.-1)then
        if(indc.eq.0)then
          curlin(curuni)=' '
        else
          curlin(curuni)=line(:indc)
        end if
        linlen(curuni)=indc
      end if
      stemp=curlin(curuni)
      indc=index(stemp,'!')-1
      if(indc.ne.-1)then
        if(indc.eq.0)then
          curlin(curuni)=' '
        else
          curlin(curuni)=stemp(:indc)
        end if
        linlen(curuni)=indc
      end if

! cut out commentaries  between { }
      ind1=index(line,'{')
      ind2=index(line,'}')
      if(ind1.ne.0)then
        if(ind2.ne.0)then
          if(ind1.ne.1)then
            curlin(curuni)= line(1:ind1-1)//' '//line(ind2+1:)
          else
            curlin(curuni)= ' '//line(ind2+1:)
          end if
        else
          lread=.false.
          if(ind1.ne.1)then
            curlin(curuni)= line(1:ind1-1)
          else
            curlin(curuni)= ' '
          end if
        end if
      else                          !. no {
        if(ind2.ne.0)then
          lread=.true.
          curlin(curuni)=' '//line(ind2+1:)
        else                      !. neither }
          if(lread)then          !. if no { or } read or skip
            curlin(curuni)=line
          else
            go to 10
          end if
        end if
      end if

! Find all pairs of " and prevent the strings from being converted to upper-case.
       stemp=curlin(curuni)(1:)
       ind1 = index(stemp,'"')
       do while (ind1.gt.0)
         stemp(ind1:ind1)=' '
         ind2 = index(stemp,'"')
         if(ind2.eq.0)then
           write(6,'(a)')' warning_GET_line> closing " character missing'
           write(6,'(2a)')' in line:',curlin(curuni)
         else
           stemp(ind2:ind2)=' '
           do k=ind1,ind2
             linside(k)=.true.
           end do
         end if ! ind2.eq.0
        ind1 = index(stemp,'"')
      end do ! while

! convert to upper-case and cut out "=" sign outside " "
      do i=1,linlen(curuni)
        if(linside(i))cycle
          char1 = curlin(curuni)(i:i)
          if(.not.KeepEqual.and.char1.eq.'=')then
             curlin(curuni)(i:i)= ' '
          else
             call CNV_CLtoU (char1)
             curlin(curuni)(i:i) = char1
          end if
      end do

!     call PRG_manager ('exit', 'GET_line', 'UTILITY')
      return
! eof:
 999  continue

! EOF: come back to preceding input file
      do i=1,MAX_length
         curlin(curuni)(i:i) = ' '
      end do

      call PRG_file_back (comfil, curuni)

! end of routine GET_line
!     call PRG_manager ('exit', 'GET_line', 'UTILITY')
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine PRG_file_back (FILNAM, unit)
!*************************************************************************************************
!     Date last modified:  July 19, 1991                                                         *
!     Author: : F. Colonna                                                                       *
!           Come back from Stream File "unit"
!           to previous Stream File
!*************************************************************************************************
! Modules:

      implicit none
! i/o :
      integer unit
      character*(*), intent(INOUT) :: FILNAM
!
! local :
      integer filen, prim
      logical Lexist,Lopen

! Begin:
      filen=len_trim(FILNAM)
      if(unit.ne.unitin)then
        close (unit=unit)
        PRG_file(unit)%name='???'
        PRG_file(unit)%status='FREE '
        PRG_file(unit)%type='??? '
        PRG_file(unit)%form='??? '
        PRG_file(unit)%action='??? '
        if(PRINT_level.ge.10)then
            write(UNIout,'(3a)')'PRG_file_back> Input file  "',FILNAM(1:filen),'" closed '
        end if
      end if
!
      NStream_files=NStream_files-1
      if(NStream_files.le.0)then
        write(UNIout,'(//a)')'PRG_file_back> end of input file: "stop" statement missing'
         call PRG_stop ('"stop" statement missing')
      end if

      unit =PRG_stream_unit(NStream_files)
      FILNAM=PRG_file(unit)%name
      if(unit.ne.unitin)then
! Inquire which file is connected:
      inquire (name=FILNAM, exist=Lexist, opened=Lopen, unit=unit)
      if(.not.Lexist)then
        write(UNIout,'(4a,i5)')'PRG_file_back> File "',FILNAM,'" does not exists ', &
        'returned UNIT is:',UNIT
         call PRG_stop ('File does not exist')
      end if
      else
        write(UNIout,'(3a)')'PRG_file_back> Input file reset to standard input'
      end if

      if(PRINT_level.ge.10)then
        call STR_size (FILNAM,prim,filen)
        write(UNIout,'(3a)')'PRG_file_back> Stream-File  "',FILNAM(1:filen),'" re-activated'
      end if
!
      return
      end subroutine PRG_file_back
      end subroutine GET_line
      end MODULE program_parser
