      module program_timing
!*************************************************************************************************
!     Date last modified:  February 29, 2008                                                     *
!     Author:  R.A. Poirier                                                                      *
!     Description: Controls Printing, Timing                                                     *
!*************************************************************************************************
! Modules:

      implicit none

      integer, parameter :: MAX_routines=10 ! Maximum number of routines for which timing can be kept
      logical, private :: LCPU_Time

      real, private, dimension(:), allocatable :: BeginTimeArray, EndTimeArray
!
! current Time in seconds
      double precision :: Time_Current
      double precision :: Time_Begin
      double precision :: Time_End
      double precision :: Time_Total
      CHARACTER(len=11) ::  SDATE ! date
      CHARACTER(len=8) ::  STIME ! time
      CHARACTER(len=8) ::  DATE08
      CHARACTER(len=16) ::  DATE_CML
!
      integer :: HCPU ! cpu hours
      integer :: MCPU ! cpu minutes
      integer :: SCPU ! cpu secondes
      integer :: CCPU ! cpu cent-secondes
!
      double precision :: CURCPU ! current Cpu
      double precision :: LSTCPU ! elapsed Cpu
      double precision :: DIFCPU ! difference in Cpu

      character(len=14) :: SECPU ! Cpu elapsed
      character(len=14) :: SETIM ! Total elapsed time
!
      logical :: LCPU   ! Cpu active
      logical :: LOCCPU ! local CPU variable
!
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine INI_time
!************************************************************************************************************
!     Date last modified: October 8, 2002                                                      Version 2.0  *
!     Author: F. Colonna and R.A. Poirier                                                                   *
!     Description: Initialize Time and date                                                                 *
!************************************************************************************************************
! Modules:

      implicit none
!
! Local scalars:
!
! Begin:
      if(.not.allocated(BeginTimeArray))then
        allocate (BeginTimeArray(MAX_routines))
      end if
      BeginTimeArray(1:MAX_routines) = 0

      if(.not.allocated(EndTimeArray))then
        allocate (EndTimeArray(MAX_routines))
      end if
      EndTimeArray(1:MAX_routines) = 0

      LCPU_Time = .false.

      call PRG_time

      Time_begin=Time_current

      lstcpu = 0.d0
      call PRG_CPU_time
      call PRG_date
!
      return
      end subroutine INI_time
      subroutine PRG_CPU_time
!*****************************************************************************************************************
!     Date last modified: July 11, 2005                                                             Version 2.0  *
!     Author: F. Colonna and R.A. Poirier                                                                        *
!     Description: Get CPU time.                                                                                 *
!     AIX version: displays current CPU time including "TEXT".                                                   *
!*****************************************************************************************************************
! Modules:
!
      implicit none
!
! Local scalars:
      integer :: rcode
      double precision :: secs
!
! Begin:
      call unxtim (secs, rcode)
      difcpu = secs - lstcpu
      lstcpu = secs

      return
      end subroutine PRG_CPU_time
      subroutine PRG_date
!*****************************************************************************************************************
!     Date last modified: July 11, 2005                                                             Version 2.0  *
!     Author: F. Colonna and R.A. Poirier                                                                        *
!     Description: Get the date.                                                                                 *
!*****************************************************************************************************************
! Modules:

      implicit none
!
! Local scalars:
      integer :: length,rcode
      character*24 :: date
      character*2 :: DAY
      character*4 :: YEAR
      character*3 :: MONTH
      character*2 :: MOIS

! AIX-3090:
      call sysinf (DATE, 'DATE', length, rcode)
      if(rcode.ne.0)stop 'PRG_date'

      MONTH = date(5:7)
      if(date(9:9).eq.' ')then
        DAY = '0'//date(10:10)
      else
        DAY = date(9:10)
      end if
      YEAR = date(21:24)
      SDATE=DAY//'-'//MONTH//'-'//YEAR
      if(month.eq.'Jan')mois = '01'
      if(month.eq.'Feb')mois = '02'
      if(month.eq.'Mar')mois = '03'
      if(month.eq.'Apr')mois = '04'
      if(month.eq.'May')mois = '05'
      if(month.eq.'Jun')mois = '06'
      if(month.eq.'Jul')mois = '07'
      if(month.eq.'Aug')mois = '08'
      if(month.eq.'Sep')mois = '09'
      if(month.eq.'Oct')mois = '10'
      if(month.eq.'Nov')mois = '11'
      if(month.eq.'Dec')mois = '12'
      DATE08=DAY//'-'//MOIS//'-'//YEAR
      DATE_CML=date(21:24)//'-'//MOIS//'-'//DAY

! End of Routine  PRG_date
      return
      end subroutine PRG_date
      subroutine PRG_time
!*****************************************************************************************************************
!     Date last modified: July 11, 2005.                                                            Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Gets the current time.                                                                        *
!*****************************************************************************************************************
! Modules:

      implicit none
!
! includes:
      integer length,rcode
      double precision :: Rsec,Rmin,Rhour
      character*24 :: date
      character*2  :: Csec,Cmin,Chour
      character*8 :: TEMP

! Unix
      call sysinf (DATE, 'DATE', length, rcode)

      if(rcode.ne.0)stop 'PRG_time rcode # 0'

      Csec = date(18:19)
      Cmin = date(15:16)
      Chour = date(12:13)

      STIME=Chour//':'//Cmin//':'//Csec

      call CNV_StoD (Csec, Rsec)
      call CNV_StoD (Cmin, Rmin)
      call CNV_StoD (Chour, Rhour)

      Time_current = Rsec + 60.d0*Rmin + 3600.d0*Rhour

! End of Routine  PRG_time
      return
      end subroutine PRG_time
      subroutine PRG_time_Begin (Iroutine)
!*****************************************************************************************************************
!     Date last modified: March 6, 2009.                                                            Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Gets the begin time.                                                                        *
!*****************************************************************************************************************
! Modules:

      implicit none
!
! Input scalar:
      integer :: Iroutine

      call CPU_TIME(Time_Begin)
      BeginTimeArray(IRoutine) = Time_Begin

! End of Routine  PRG_time_Begin
      return
      end subroutine PRG_time_Begin
      subroutine PRG_time_End (Iroutine, RoutineName)
!*****************************************************************************************************************
!     Date last modified: March 6, 2009.                                                            Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Gets the begin time.                                                                        *
!*****************************************************************************************************************
! Modules:
      USE program_files

      implicit none
!
! Input scalar:
      integer :: Iroutine
      character(*) :: Routinename

      call CPU_TIME(Time_End)
      EndTimeArray(IRoutine) = Time_End
      Time_Total=EndTimeArray(IRoutine)-BeginTimeArray(IRoutine)
      write(uniout,'(2a)')'For routine> ',RoutineName(1:len_trim(RoutineName))
      write(uniout,'(3(a,f14.2),a)')'End= ',EndTimeArray(IRoutine),' Begin= ',BeginTimeArray(IRoutine),&
          ' total= ',Time_Total, ' seconds'

! End of Routine  PRG_time_End
      return
      end subroutine PRG_time_End
      end module program_timing
