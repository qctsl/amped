BUILDING MUNgauss WITH CMAKE
============================


A) Out of Source build in sub-directory
===========================================
Layout:
  MUNgauss        (current directory)
     +--build     (build directory)
     +--...

# 1) make build directory and cd into that:
mkdir build
cd    build
# 2) run cmake pointing to source directory (in this case ..)
cmake ..
# 3) run make
make


B) Out of Source build in sibling-directory
===========================================
Layout:
  some_directory        (current directory)
     +--MUNgauss        (source directory
     +--MUNgauss_build  (build directory)

# 1) make build directory and cd into that:
mkdir MUNgauss_build
cd    MUNgauss_build
# 2) run cmake pointing to source directory (in this case ../MUNgauss )
cmake ../MUNgauss
# 3) run make with 4 jobs in parallel
make -j 4


C) force CMake to use Fortran compiler: /path/to/bin/f95 
========================================================
Layout:
  MUNgauss        (current directory)
     +--build     (build directory)

mkdir build
cd    build
cmake -DCMAKE_Fortran_COMPILER=/path/to/bin/f95  ..
make

Useful CMake variables:
=======================
 -DCMAKE_BUILD_TYPE=DEBUG
 -DCMAKE_Fortran_COMPILER=/path/to/bin/f95
 -DCMAKE_CC_COMPILER=/path/to/bin/cc


D) Toggle building of CCCDB module
==================================

If the CCCDB submodule is available, run cmake as followed:

mkdir build
cd    build
cmake -DCMAKE_CCCDB=ON  ..
make  -j 4





