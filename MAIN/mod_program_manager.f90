      module program_manager
!*************************************************************************************************
!     Date last modified:  March 8, 2001                                                         *
!     Author:  Darryl Reid                                                                       *
!     Description: Controls Printing, Timing, Dependancy, etc.                                   *
!             Variable initialized in subroutine INI_Program                                     *
!*************************************************************************************************
! Modules:

      implicit none

      integer, parameter :: MAX_Rname=132  ! Maximum length for routine name
      integer, public :: ObjectArrayIndex
      integer, public :: LevelCount
      integer, public :: DependCounter
      integer, public :: PRG_cycles ! Counts the number of function evaluations, e.g., for optimization
      integer, public, dimension(:), allocatable :: Object_Number_list !Array of the Object Numbers
      logical, public :: LCPU_Time,Ldependancy

      logical, public :: Ltrace
!
! Print management
      integer :: TRACE_level

      end module program_manager
