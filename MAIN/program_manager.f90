      subroutine PRG_manager (EnterOrExit, Routine_Name, Local_Object_Name)
!*************************************************************************************************
!     Date last modified:  July 9,2004                                                           *
!     Author:  Darryl Reid and R.A. Poirier                                                      *
!     Description: keep a running trace of the execution of the program and do some              *
!            some error checking to ensure name matching, Controls timing and dependancy         *
!*************************************************************************************************
! Modules:
      USE program_files
      USE program_manager
      USE program_objects

      implicit none

! Input Scalars
      character*(*) EnterOrExit,Routine_Name,Local_Object_Name
!
! Begin
      if(Index(Local_Object_Name,'UTILITY').ne. 0)then
        call Util_manager (EnterOrExit, Routine_Name)
      else if(Index(Local_Object_Name,'INITIALIZE').ne. 0)then
        call INI_manager
      else
        call OBJ_manager (EnterOrExit, Routine_Name, Local_Object_Name)
      end if !Local_Object_Name,'UTILITY'

      if(NRoutines.gt.0)then ! Check if debug or cputiming requested for this routine
        call Routine_manager (EnterOrExit, Routine_Name)
      end if

      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine OBJ_manager (EnterOrExit, Routine_Name, Local_Object_Name)
!*************************************************************************************************
!     Date Created:  March 28, 2001                                                              *
!     Date last modified:  March 28, 2001                                                        *
!     Author:  Darryl Reid                                                                       *
!     Description: keep a running trace of the execution of the programs objects and do some     *
!            some error checking to ensure name matching, Controls timing and dependancy         *
!*************************************************************************************************
! Modules:

      implicit none

! Input Scalars
      character*(*) EnterOrExit,Routine_Name,Local_Object_Name
!
! Local Scalar:
      integer, parameter :: MAX_ObjName=132
      character(len=MAX_ObjName) :: ObjName
!
! Begin
       ObjName(1:)=Object(ObjNum)%name(1:len_trim(Object(ObjNum)%name))//'%'// &
                   Object(ObjNum)%modality(1:len_trim(Object(ObjNum)%modality))
      SelectEnterOrExit: select case (EnterOrExit)
      case ('enter') !                                                                   ENTER
       if(Routine_Name.ne.Object(ObjNum)%routine(1:len_trim(Object(ObjNum)%routine)))then
         write(UNIout,*)'ERROR> OBJ_manager:'
         write(UNIout, *)'Routine Names do not match ', Routine_Name,' and ', &
                          Object(ObjNum)%routine(1:len_trim(Object(ObjNum)%routine)), &
                          ' upon ',EnterOrExit,' Object Number ',ObjNum,' Index ',ObjectArrayIndex
         stop 'ERROR> OBJ_manager: Routine names do not match'
       end if
       if(Local_Object_Name.ne.ObjName(1:len_trim(ObjName)))then
         write(UNIout,*)'ERROR> OBJ_manager:'
         write(UNIout,'(8a)')'Object Names do not match ',Local_Object_Name,' and ', &
                          ObjName(1:len_trim(ObjName)),' upon ',EnterOrExit,' Routine Name ',Routine_Name
         stop 'ERROR> OBJ_manager: Object names do not match'
       end if

        ObjectArrayIndex = ObjectArrayIndex + 1
        if(ObjectArrayIndex.gt.Nobjects)then
          write(UNIout,*)'ERROR> OBJ_manager: ObjectArrayIndex > Nobjects'
          stop 'ERROR> OBJ_manager: ObjectArrayIndex > Nobjects'
        end if
        Object_Number_list(ObjectArrayIndex) = ObjNum
!
        if(Ltrace)then
          LevelCount=LevelCount+1
          write(UNIout,'(a,i5,4a/)')'Level: ',LevelCount,' Entering ', &
                         Routine_Name,' to build ',Local_Object_Name
        end if !Ltrace

       case ('exit') !                                                                   EXIT
        ObjNum = Object_Number_list(ObjectArrayIndex)
        if(Ltrace)then ! Print message upon exiting a routine
          write(UNIout,'(a,i5,4a/)')'Level: ',LevelCount, ' Exiting  ', &
                         Routine_Name,' to build ',Local_Object_Name
!                        Routine_Name(len_trim(Routine_Name)),' to build ',Local_Object_Name(len_trim(Local_Object_Name))
          LevelCount=LevelCount-1
        end if !Ltrace

! End of Computation Time Stuff
        ObjectArrayIndex = ObjectArrayIndex - 1
        if(ObjectArrayIndex.gt.0)then
          ObjNum = Object_Number_list(ObjectArrayIndex) ! Reset the object number to the parent object
        end if

      case default
        write(UNIout, *) 'Input for PRG_mamager is incorrect, must be enter or exit'
      end select SelectEnterOrExit

      return
      end subroutine OBJ_manager
      subroutine Util_manager (EnterOrExit, Routine_Name)
!*************************************************************************************************
!     Date Created:  March 28, 2001                                                              *
!     Date last modified:  March 28, 2001                                                        *
!     Author:  Darryl Reid                                                                       *
!     Description: keep a running trace of the execution of the program utilities                *
!                  and do some printing                                                          *
!*************************************************************************************************
! Modules:

      implicit none

! Input Scalars
      character*(*) EnterOrExit,Routine_Name
!
! Begin
! Enter
      SelectEnterOrExit: select case (EnterOrExit)
      case ('enter') !                                                                   ENTER
        if(Ltrace)then
        LevelCount = LevelCount+1
          write(UNIout,'(a,i5,2a/)')'Level: ', LevelCount, ' Entering Utility ', &
                         Routine_Name(1:len_trim(Routine_Name))
        end if !Ltrace
       case ('exit') !                                                                   EXIT
        if(Ltrace) then
          write(UNIout,'(a,i5,2a/)')'Level: ',LevelCount, ' Exiting Utility ', &
                         Routine_Name(1:len_trim(Routine_Name))
        LevelCount = LevelCount-1
        end if !Ltrace
      case default
         write(UNIout, *) 'Input for Message Print is incorrect, must be enter or exit'
      end select SelectEnterOrExit
      return
      end subroutine Util_manager
      subroutine Routine_manager (EnterOrExit, Routine_Name)
!*************************************************************************************************
!     Date last modified:  July 9, 2004                                                          *
!     Author:  R.A. Poirier                                                                      *
!     Description: Controls the debugging and cputiming                                              *
!*************************************************************************************************
! Modules:
      USE program_timing

      implicit none

! Input Scalars
      character*(*) EnterOrExit, Routine_Name
!
! Local Scalar:
      integer :: IRoutine
      integer :: GET_routine_number
!
! Begin
      IRoutine=GET_routine_number (Routine_name)
      if(IRoutine.gt.0)then
      SelectEnterOrExit: select case (EnterOrExit)
      case('enter')
        if(Routine(IRoutine)%debug)then ! Debugging
        else
        end if !Debug_object

        if(Routine(IRoutine)%cputiming)then ! CPU_Time Computation
          call PRG_time_Begin (Iroutine)
        end if ! LCPU_Time

      case('exit')
        if(Routine(IRoutine)%cputiming)then ! ! Computation time printing
          call PRG_time_End (Iroutine, Routine_list(IRoutine))
        end if !LCPU_Time

      case default
        write(UNIout, *) 'Input for PRG_mamager is incorrect, must be enter or exit'
      end select SelectEnterOrExit
      end if ! IRoutine.gt.0
      return
      end subroutine Routine_manager
      subroutine INI_manager
!*****************************************************************************************************************
!     Date last modified: February 18, 2000                                                         Version 2.0  *
!     Author: Darryl Reid, R.A. Poirier                                                                          *
!     Description: Initialize PRG_manager variables.                                                             *
!*****************************************************************************************************************
! Modules:

      implicit none

      DependCounter = 0
      if(.not.allocated(Object_Number_list))then
        allocate (Object_Number_list(Nobjects))
      end if
      Object_Number_list(1:Nobjects) = 0

      ObjectArrayIndex = 1
      LCPU_Time = .false.
      Ldependancy = .true.
      LevelCount = 0
      PRG_cycles=0

      return
      end subroutine INI_manager
      end subroutine PRG_manager
      recursive integer function GET_routine_number (routine_name)
!**************************************************************************************************************
!     Date last modified: March 14, 2001                                                                      *
!     Author: Darryl Reid, R.A. Poirier                                                                       *
!     Description: Given an Routine name, determine the object number it corresponds to.                      *
!**************************************************************************************************************
! Modules:
      USE program_manager
      USE program_objects

      implicit none
!
! Input scalars:
      character*(*) routine_name
      character(len=MAX_Rname) routineU
!      character(len=MAX_Rname) routine_list
!
! Local scalars:
      integer IRoutine,lenstr
!
! Begin:
!     CANNOT call PRG_manager!!!
      GET_routine_number=0

      lenstr=len_trim(routine_name)
      routineU=routine_name(1:lenstr)
      call CNV_SLtoU(routineU)
      if(NRoutines.gt.0)then
      do IRoutine=1,NRoutines
        if(routineU.eq.Routine_list(IRoutine))then
          GET_routine_number=IRoutine
          exit
        end if
      end do ! IRoutine
      end if

      return
      end

