      MODULE menu_gets
!*******************************************************************************************
!     Date last modified: April 5, 2001                                       Version 2.0  *
!     Author: R.A. Poirier                                                                 *
!     Description: Interface for the GET_* used by the menu to read values for the input   *
!*******************************************************************************************
! Modules:
      USE program_files
      USE program_parser

      implicit none

! MAXimum values:
      integer, private, parameter :: MAX_tokenlen=132 ! MAXimum string length for token

      interface GET_value
        module procedure GET_integer
        module procedure GET_integer8
        module procedure GET_real
        module procedure GET_logical
        module procedure GET_string
        module procedure GET_Ilist
        module procedure GET_Rlist
      end interface

CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine GET_string (string, lenstr)
!*******************************************************************************************
!     Date last modified: April 5, 2001                                       Version 2.0  *
!     Authors: F.Colonna and R.A. Poirier                                                  *
!     Description: Get an alphabetic string from input stream                              *
!*******************************************************************************************
      implicit none
!
! i/o     :
      character*(*), intent(out) :: string
      integer, intent(out) :: lenstr
!
! local   :
      logical done
!
! Begin:
!     call PRG_manager ('enter', 'GET_string', 'UTILITY')

      call NEW_token (curcom)
      if(token(1:1).eq.'=') call NEW_token (curcom)
      string = token
      lenstr = toklen
      if(lenstr.eq.0) then
        call PRT_warning ('GET_string', 'zero length string')
      end if
!
! end of routine GET_string
!     call PRG_manager ('exit', 'GET_string', 'UTILITY')
      return
      end subroutine GET_string
      subroutine GET_real (value)
!*******************************************************************************************
!     Date last modified: April 5, 2001                                       Version 2.0  *
!     Authors: F.Colonna and R.A. Poirier                                                  *
!     Description: Gets a double precision Value from current word                         *
!*******************************************************************************************
! Modules:

      implicit none

! i/o     :
      double precision, intent(out) :: value
!
! local   :
      integer int
!
! Begin:
!     call PRG_manager ('enter', 'GET_real', 'UTILITY')
!
      call NEW_token (curcom)
      if(token(1:1).eq.'=') call NEW_token (curcom)

      if(CHK_string(token, '(*-)/+0123456789.defDEF')) then
        if(index(token,'.').eq.0) then
           call CNV_StoI (token,int)
           value=dble(int)
        else
           call CNV_StoD (token, value)
        end if
      else
        call PRT_warning ('GET_real', 'input token must be numerical')
      end if

! end of routine  GET_real
!     call PRG_manager ('exit', 'GET_real', 'UTILITY')
      return
      end subroutine GET_real
      subroutine GET_integer (value)
!*******************************************************************************************
!     Date last modified: April 5, 2001                                       Version 2.0  *
!     Authors: F.Colonna and R.A. Poirier                                                  *
!     Description: Get an integer value from string input                                  *
!*******************************************************************************************
! Modules:

      implicit none
! i/o     :
      integer, intent(out) :: value
!
! local   :
      double precision dbl
!
! Begin:
      call NEW_token (curcom)
      if(token(1:1).eq.'=') call NEW_token (curcom)

      if(CHK_string(token, '(*-)/+0123456789.defDEF')) then
        if(index(token,'.').eq.0) then
           call CNV_StoI (token, value)
        else
           call CNV_StoD (token, dbl)
           value=int(dbl)
        end if
      else
          write(UNIout,'(3a)')'WARNING> GET_integer: input token "', &
                               token(1:toklen),'" must be integer'
      end if
      return
      end subroutine GET_integer
      subroutine GET_integer8 (value)
!*******************************************************************************************
!     Date last modified: April 5, 2001                                       Version 2.0  *
!     Authors: F.Colonna and R.A. Poirier                                                  *
!     Description: Get an integer value from string input                                  *
!*******************************************************************************************
! Modules:

      implicit none
! i/o     :
      integer(kind=8), intent(out) :: value
!
! local   :
      double precision dbl
!
! Begin:
      call NEW_token (curcom)
      if(token(1:1).eq.'=') call NEW_token (curcom)

      if(CHK_string(token, '(*-)/+0123456789.defDEF'))then
        if(index(token,'.').eq.0)then
           call CNV_StoI8 (token, value)
        else
           call CNV_StoD (token, dbl)
           value=int(dbl, 8)
        end if
      else
        call PRT_warning ('GET_integer8', 'input token must be integer')
      end if
      return
      end subroutine GET_integer8
      subroutine GET_logical (value)
!*******************************************************************************************
!     Date last modified: April 5, 2001                                       Version 2.0  *
!     Authors: F.Colonna and R.A. Poirier                                                  *
!     Description: Get a logical value from input stream                                   *
!*******************************************************************************************
! Modules:

      implicit none
!
! INput scalar:
      logical, intent(OUT) :: value
!
! Local scalar:
      logical done
!
! Begin:
      call NEW_token (curcom)
      if(token(1:1).eq.'=') call NEW_token (curcom)

      if(token(1:1).eq.'T') then
         value= .true.
      else if(token(1:1).eq.'O') then
         value= .true.
      else if(token(1:1).eq.'Y') then
         value= .true.
      else if(token(1:2).eq.'.T') then
         value= .true.
      else if(token(1:1).eq.'F') then
         value= .false.
      else if(token(1:2).eq.'.F') then
         value= .false.
      else if(token(1:1).eq.'N') then
         value= .false.
      else
         write(UNIout,'(3a)') &
      ' error_GET_logical> illegal logical Value= "', &
        token(1:wrdlen(curuni)),'". should be True, False, Yes, No, Oui'
      end if

! end of routine GET_logical
      return
      end subroutine GET_logical
      subroutine GET_Clist (list, nbbyte, maxelm, nbelem)
!*******************************************************************************************
!     Date last modified: April 5, 2001                                       Version 2.0  *
!     Authors: F.Colonna and R.A. Poirier                                                  *
!     Description: Gets a list of Character*nbbyte  enclosed between parentheses           *
!     the number of elements is set to max at input and returns the actual number nbbyte   *
!     is defined from input                                                                *
!*******************************************************************************************
! Modules:

      implicit none
!
! input scalars:
      integer, intent(in) :: nbbyte
      integer, intent(in) :: maxelm
      integer, intent(out) :: nbelem
!
! output arrays   :
      character(len=nbbyte), intent(out) :: list(maxelm)
!
! local scalars:
      character(len=MAX_tokenlen) string
      logical done
      integer char,i,Iblank,Icomma,Ifirst,Ilast,lenstr

! Begin:
!     call PRG_manager ('enter', 'GET_Clist', 'UTILITY')
      done=.false.
      nbelem=0
      Ifirst=1
      Ilast=0

      list(1:maxelm)(1:nbbyte)=' '

! First read the full list from "(" to ")" and save it in string
      do while (.not.done)
        call NEW_token (' Get_list_Character:')
        if(token(1:1).eq.'=') call NEW_token (' Get_list_Character:')
        if(token(1:2).eq.'( ')call NEW_token (' Get_list_Character:')
! remove imbbeded ) ( or / from token if any:
        call removeC (token,'(')
        toklen=len_trim(token)
        lenstr=toklen
        Ilast=Ilast+lenstr
        string(Ifirst:Ilast)=token(1:lenstr)
        if(string(Ilast:Ilast).ne.')')then ! make a "," separates each entry
          Ilast=Ilast+1
        end if
        string(Ilast:Ilast)=','
        Ifirst=Ilast+1
        if(index(token, ')').ne.0)done=.true. ! Found a ")" - keep it
      end do ! while
      lenstr=Ilast

! Next extract each item from string which now has I1,I2,...,)
      done=.false.
      Ifirst=1
      do while (.not.done)
        Icomma=index(string, ',')
        if(Icomma.ne.0)then
          string(Icomma:Icomma)=' '
          nbelem=nbelem+1
          list(nbelem)(1:)=string(Ifirst:Icomma-1)
          Ifirst=Icomma+1
        end if
        if(Ifirst.ge.lenstr)done=.true.
      end do

! end of routine GET_Clist
!     call PRG_manager ('exit', 'GET_Clist', 'UTILITY')
      return
      end subroutine GET_Clist
      subroutine GET_Rlist (list, nlist)
!*******************************************************************************************
!     Date last modified: April 5, 2001                                       Version 2.0  *
!     Authors: F.Colonna and R.A. Poirier                                                  *
!     Description: Gets a list of REAL*8   enclosed between parentheses                    *
!*******************************************************************************************
! Modules:

      implicit none
!
! i/o arrays   :
      double precision, intent(OUT) :: list(*)
! i/o scalars  :
      integer, intent(OUT) :: nlist
!
! local scalars:
      integer Icomma,Ifirst,Ilast,lenstr
      character(len=MAX_tokenlen) :: string
      double precision :: var
      logical :: done

! Begin:
      done=.false.
      nlist=0
      Ifirst=1
      Ilast=0
! The following should be a routine
! First read the full list from "(" to ")" and save it in string
      do while (.not.done)
        call NEW_token (' Get_list_Character:')
        if(token(1:1).eq.'=') call NEW_token (' Get_list_Character:')
        if(token(1:2).eq.'( ')call NEW_token (' Get_list_Character:')
! remove imbbeded ) ( or / from token if any:
        call removeC (token,'(')
        toklen=len_trim(token)
        lenstr=toklen
        Ilast=Ilast+lenstr
        string(Ifirst:Ilast)=token(1:lenstr)
        if(string(Ilast:Ilast).ne.')')then ! make a "," separates each entry
          Ilast=Ilast+1
        end if
        string(Ilast:Ilast)=','
        Ifirst=Ilast+1
        if(index(token, ')').ne.0)done=.true. ! Found a ")" - keep it
      end do
      lenstr=Ilast

! Next extract each item from string which now has I1,I2,...,)
      done=.false.
      Ifirst=1
      do while (.not.done)
        Icomma=index(string, ',')
        if(Icomma.ne.0)then
          string(Icomma:Icomma)=' '
          nlist=nlist+1
          call  CNV_StoD (string(Ifirst:Icomma-1), var)
          list(nlist)=var
          Ifirst=Icomma+1
        end if
        if(Ifirst.ge.lenstr)done=.true.
      end do

! end of routine GET_Rlist
      return
      end subroutine GET_Rlist
      subroutine GET_Ilist (list, nlist)
!*******************************************************************************************
!     Date last modified: April 5, 2001                                       Version 2.0  *
!     Authors: F.Colonna and R.A. Poirier                                                  *
!     Description: Gets a list of Integers enclosed between parentheses                    *
!*******************************************************************************************
! Modules:

      implicit none
!
! i/o arrays:
      integer, intent(out) :: list(*)
! i/o scalars:
      integer, intent(out) :: nlist
! local:
      character(len=MAX_tokenlen) string
      integer move, refer, n, inds
      integer Icomma,Ifirst,Ilast,lenstr
      logical done

! Begin:
      done=.false.
      nlist=0
      Ifirst=1
      Ilast=0
! The following should be a routine
! First read the full list from "(" to ")" and save it in string
      do while (.not.done)
        call NEW_token (' Get_list_Character:')
        if(token(1:1).eq.'=') call NEW_token (' Get_list_Character:')
        if(token(1:2).eq.'( ')call NEW_token (' Get_list_Character:')
! remove imbbeded ) ( or / from token if any:
        call removeC (token,'(')
        toklen=len_trim(token)
        lenstr=toklen
        Ilast=Ilast+lenstr
        string(Ifirst:Ilast)=token(1:lenstr)
        if(string(Ilast:Ilast).ne.')')then ! make a "," separates each entry
          Ilast=Ilast+1
        end if
        string(Ilast:Ilast)=','
        Ifirst=Ilast+1
        if(index(token, ')').ne.0)done=.true. ! Found a ")" - keep it
      end do
      lenstr=Ilast

! Next extract each item from string which now has I1,I2,...,)
      done=.false.
      Ifirst=1
      do while (.not.done)
        Icomma=index(string, ',')
        if(Icomma.ne.0)then
          string(Icomma:Icomma)=' '
          nlist=nlist+1
          call  CNV_StoI (string(Ifirst:Icomma-1),n)
          list(nlist)=n
          Ifirst=Icomma+1
        end if
        if(Ifirst.ge.lenstr)done=.true.
      end do

      return
      end subroutine GET_Ilist
      subroutine removeC (string, char)
!***************************************************************************
!     Date last modified:  July 19, 1991                                   *
!     Author: : F. Colonna                                                 *
!           Removes one character from a string                            *
!***************************************************************************
! Modules:

      implicit none
!
! Input scalars:
      character*(*) string, char
!
! Local scalars:
      character(len=MAX_tokenlen) begstr, endstr
      integer indc,lenstr

! Begin:
      lenstr=len_trim(string)
      indc=index(string,char)
      if(indc.gt.0)then
         if     (indc.eq.1)then
            endstr = string(2:)
            string = endstr
         else if(indc.eq.lenstr)then
            begstr = string(1:lenstr-1)
            string = begstr
         else
            begstr=string(1:indc-1)
            endstr=string(indc+1:lenstr)
            string=begstr(1:indc-1)//endstr
         end if
      end if
! end of routine removeC
      return
      end subroutine removeC
      end MODULE menu_gets
