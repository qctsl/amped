      module program_files
!*************************************************************************************************
!     Date last modified:  February 29, 2008                                                     *
!     Author:  R.A. Poirier                                                                      *
!     Description: Controls files.                                                               *
!*************************************************************************************************
! Modules:

      implicit none

      integer, parameter :: MAX_namelen=255        ! maximum string length for a file name
      integer, parameter :: MAX_specifyer=128     ! maximum string length for a file specifyers
      integer, parameter :: MAX_files=999         ! maximum number of files (units)
      integer, parameter :: MAX_stream=200        ! maximum number of STREAM files (units)
      integer, parameter :: MAX_files_to_process=20000        ! maximum number of files to be processed

      integer :: UNITin  ! Input file
      integer :: UNIout ! Output file
      integer :: CMLunit ! Output file for CML
      integer :: COM_unit ! Output file for CML
      integer :: FFunit ! Output file for ForceField data
      integer :: MUN_prtlev ! MUNgauss print level
      integer :: NCI_Number ! Output file for CML

      integer :: PRINT_level
      integer :: WARNING_level
      integer :: ERROR_level ! level at which the program is stopped

      logical :: LGenerate_CML ! Generate CML file (default=false)
      logical :: LGenerate_COM ! Generate Gaussian com file (default=false)
      logical :: LGenerate_FF ! Generate Force Field file (default=false)
      logical :: LFinalization_CML ! 
      logical :: LNCI ! Generate CML file for NCI data
      logical :: LCML_file ! Generate CML file for CML data
      logical :: Name_set

      character(len=128) :: DUDE_Number ! Output file ID for CML
      character(len=128) :: RUN_name                 ! Prefix added to file names
      character(len=128) :: I2I_BasicFileID          ! Prefix added to two-electron integral files
      character(len=16) :: USER_name                ! User name
      character(len=8) ::  JOB_name                 ! JOB name
      character(len=64) :: Basic_FileID
      character(len=MAX_specifyer), public :: Directory_name
      character(len=255), public :: tempdir
      character(len=MAX_specifyer), public :: Mopac_Bin
      character(len=MAX_namelen) :: CML_FileOUT
      character(len=MAX_namelen) :: FF_FileOUT
      character(len=8) ::  Read_From_FileType        ! File type that is being read (Gaussian or CML)
      character(len=8) ::  Write_To_FileType         ! File type that is being written (Gaussian or MUNgauss input, CML, FF)
      character(len=MAX_namelen) :: Read_From_FileName
      character(len=MAX_namelen) :: Write_To_FileName
      character(len=MAX_namelen) :: File_name_list(MAX_files_to_process) !  A list of file names to be processed


! File management tools
      type type_files
        integer :: unit     ! unit number
        character(len=MAX_specifyer) :: name     ! file name
        character(len=MAX_specifyer) :: form     ! FORMATTED or UNFORMATTED
        character(len=MAX_specifyer) :: type     ! BINARY, STREAM, INPUT, OUTPUT, BASIS_SET, ARCHIVE, GAUSSIAN
        character(len=MAX_specifyer) :: status   ! OLD, NEW, SCRATCH, REPLACE or UNKNOWN
        character(len=MAX_specifyer) :: action   ! READ, WRITE or READWRITE
        character(len=MAX_specifyer) :: position   ! Position: ASIS, REWIND or APPEND
      end type type_files

      type(type_files) :: PRG_file(MAX_files)

      integer :: PRG_stream_unit(MAX_files)

      integer :: NStream_files ! Number of currently open STREAM files

! Header variables
      integer :: header_dim1
      integer :: header_dim2
      integer :: header_unit
      integer :: file_Natoms
      integer :: file_Nbasis
      logical :: DUMP_to_file ! true if dumping to file (header_unit) instead of standard output unit
      character(len=132) :: file_format
      character(len=132) :: file_basis
      character(len=132) :: file_object
      character(len=132) :: file_formula
      character(len=132) :: file_units
!
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine INI_files
!*************************************************************************************************
!     Date last modified:  February 29, 2008                                                     *
!     Author: F. Colonna and R.A. Poirier                                                        *
!     Description: Initialize Time and date        .                                             *
!*************************************************************************************************
! Modules:

      implicit none
!
! Local scalars:
      integer :: length,rcode,lenEnv
      character(len=MAX_namelen) :: DIRECT

! Begin:
      call PRG_files
      UNITin= PRG_file(5)%unit
      UNIout= PRG_file(6)%unit
      MUN_prtlev=1

! Get user name
      call sysinf (USER_name, 'USER', length, rcode)
      if(rcode.ne.0)stop 'INI_files> rcode.ne.o for sysinf(USER)'
!
! Get directory name
      DIRECT(1:)=' '
      call sysinf (DIRECT, 'CWD', length, rcode)
      if(rcode.ne.0)stop 'PRG_GET_directoy> rcode .ne.0'
      Directory_name(1:) = DIRECT(1:length)//'           '
!
! Default names
      RUN_name='RUN'
      JOB_name='RUN'

! initialize MOPAC_BIN
      ! can be explicitly set by SET PATh MOPac_bin = <path to mopac.exe> END END
      ! use value of environment var MOPAC_BIN
      ! otherwise default to 'mopac' (and asume it's in PATH)
      call GET_ENVIRONMENT_VARIABLE("MOPAC_BIN", mopac_bin, lenEnv)
      if (lenEnv.eq.0) then   ! if environment var MOPAC_BIN is not set,
        Mopac_Bin="mopac"
      endif

      return
      end subroutine INI_files
      subroutine PRG_files
!*************************************************************************************************
!     Date last modified:  February 14, 2018                                                     *
!     Author: R.A. Poirier                                                                       *
!     Description: Define the program files.                                                     *
!*************************************************************************************************
! Modules:

      implicit none
!
! Local scalars:
!
! Initialize file management tools:
      PRG_file(1:MAX_files)%name='???'
      PRG_file(1:MAX_files)%form='???'
      PRG_file(1:MAX_files)%unit=0
      PRG_file(1:MAX_files)%type='???'
      PRG_file(1:MAX_files)%status='FREE'
      PRG_file(1:MAX_files)%action='???'
      PRG_file(1:MAX_files)%position='ASIS'

! The following files are always connected to the specified unit
      PRG_file(1)%name='DATA_BASIS'
      PRG_file(1)%form='FORMATTED'
      PRG_file(1)%unit=1
      PRG_file(1)%type='BASIS_SET'
      PRG_file(1)%status='LOCKED'
      PRG_file(1)%action='READ'

      PRG_file(5)%name='INPUT'
      PRG_file(5)%form='FORMATTED'
      PRG_file(5)%unit=5
      PRG_file(5)%type='INPUT'
      PRG_file(5)%status='LOCKED'
      PRG_file(5)%action='READ'

      PRG_file(6)%name='OUTPUT'
      PRG_file(6)%form='FORMATTED'
      PRG_file(6)%unit=6
      PRG_file(6)%type='OUTPUT'
      PRG_file(6)%status='LOCKED'
      PRG_file(6)%action='WRITE'

      PRG_file(13)%name='ARCHIVE'
      PRG_file(13)%form='FORMATTED'
      PRG_file(13)%unit=13
      PRG_file(13)%type='ARCHIVE'
      PRG_file(13)%status='LOCKED'
      PRG_file(13)%action='READWRITE'

      PRG_file(14)%name='GAU_INPUT'
      PRG_file(14)%form='FORMATTED'
      PRG_file(14)%unit=14
      PRG_file(14)%type='GAUSSIAN'
      PRG_file(14)%status='LOCKED'
      PRG_file(14)%action='READWRITE'

      PRG_file(15)%name='GAU_OUTPUT'
      PRG_file(15)%form='FORMATTED'
      PRG_file(15)%unit=15
      PRG_file(15)%type='GAUSSIAN'
      PRG_file(15)%status='LOCKED'
      PRG_file(15)%action='READWRITE'

      PRG_file(16)%name='GAU_FORCES'
      PRG_file(16)%form='FORMATTED'
      PRG_file(16)%unit=16
      PRG_file(16)%type='GAUSSIAN'
      PRG_file(16)%status='LOCKED'
      PRG_file(16)%action='READWRITE'

      PRG_file(17)%name='MPI'
      PRG_file(17)%form='FORMATTED'
      PRG_file(17)%unit=17
      PRG_file(17)%type='INPUT'
      PRG_file(17)%status='LOCKED'
      PRG_file(17)%action='READ'

      PRG_file(18)%name='BASIS'
      PRG_file(18)%form='FORMATTED'
      PRG_file(18)%unit=18
      PRG_file(18)%type='OUTPUT'
      PRG_file(18)%status='LOCKED'
      PRG_file(18)%action='READWRITE'

      PRG_file(19)%name='MUN_INPUT'
      PRG_file(19)%form='FORMATTED'
      PRG_file(19)%unit=19
      PRG_file(19)%type='MUNgauss'
      PRG_file(19)%status='LOCKED'
      PRG_file(19)%action='READWRITE'

      PRG_file(20)%name='MUN_OUTPUT'
      PRG_file(20)%form='FORMATTED'
      PRG_file(20)%unit=20
      PRG_file(20)%type='MUNgauss'
      PRG_file(20)%status='LOCKED'
      PRG_file(20)%action='READWRITE'

      PRG_file(21)%name='CCCDB_XYZ'
      PRG_file(21)%form='FORMATTED'
      PRG_file(21)%unit=21
      PRG_file(21)%type='MUNgauss'
      PRG_file(21)%status='LOCKED'
      PRG_file(21)%action='READWRITE'

      PRG_file(22)%name='CCCDB_FC'
      PRG_file(22)%form='FORMATTED'
      PRG_file(22)%unit=22
      PRG_file(22)%type='MUNgauss'
      PRG_file(22)%status='LOCKED'
      PRG_file(22)%action='READWRITE'

      PRG_file(23)%name='CCCDB_SI'
      PRG_file(23)%form='FORMATTED'
      PRG_file(23)%unit=23
      PRG_file(23)%type='MUNgauss'
      PRG_file(23)%status='LOCKED'
      PRG_file(23)%action='READWRITE'

      PRG_file(24)%name='CCCDB_SCRF'
      PRG_file(24)%form='FORMATTED'
      PRG_file(24)%unit=24
      PRG_file(24)%type='MUNgauss'
      PRG_file(24)%status='LOCKED'
      PRG_file(24)%action='READWRITE'

      PRG_file(25)%name='CML'
      PRG_file(25)%form='FORMATTED'
      PRG_file(25)%unit=25
      PRG_file(25)%type='MUNgauss'
      PRG_file(25)%status='NEW'
      PRG_file(25)%action='READWRITE'

      NStream_files=1 ! Always have unit unitin as stream file
      PRG_stream_unit(1)=5
      PRG_stream_unit(2:MAX_stream)=0

      return
      end subroutine PRG_files
      function BLD_FILE_name (filnam) result (FILE_name)
!*************************************************************************************************
!     Date last modified:  February 29, 2008                                                     *
!     Author:  R.A. Poirier                                                                      *
!     Description: check that file names are suitable for UNIX                                   *
!     input  : filnam file name using program convention                                         *
!     output : File_name with complete UNIX convention                                           *
!*************************************************************************************************
! Modules:

      implicit none

! Input scalar:
      character*(*), intent(IN) :: filnam
!
! Output scalar:
      character(len=MAX_namelen) :: FILE_name
!
! Local scalars:
      integer prim,lenstr,lenfil,char,charf,dirlen,usrlen,totlen
      character(len=MAX_namelen) string,user
!
! Begin:
      lenfil=len_trim(filnam)

      if(lenfil.gt.MAX_namelen)then
         write(UNIout,'(a,i8/3a)') &
       '   error_BLD_FILE_name> filnam longer than  ',MAX_namelen, &
       '               > for file:"',filnam(1:lenfil),'"'
           call PRG_stop ('filnam too long')
      end if
!
! add directory name:
      if(filnam(1:1).eq.'/'.or.filnam(1:2).eq.'./')then
         FILE_name = filnam
      else
         dirlen=len_trim(Directory_name)
         if(dirlen.le.0)then
            write(UNIout,'(a)') &
        '   error_BLD_FILE_name> zero length directory name '
            stop '   error_BLD_FILE_name> wrong file name '
         else if(dirlen+lenfil.gt.MAX_namelen)then
           write(UNIout,'(a,i8/5a)') &
       '   ERROR: BLD_FILE_name> total filnam longer than  ',MAX_namelen, &
       '      for file:"',Directory_name(1:dirlen),'/',filnam(1:lenfil),'"'
           call PRG_stop ('filnam too long')
         else ! Need to check that length does not exceed MAX_namelen!!!!!!!!!!!!!!!!!!!!
            FILE_name = Directory_name(1:dirlen)//'/'//filnam(1:lenfil)
         end if
      end if

      call STR_size (FILE_name, prim, totlen)

! check that no truncature occured:
      if(index(FILE_name, filnam(1:lenfil)).eq.0)then
            write(UNIout,'(a)') &
        '   error_BLD_FILE_name> truncature occur after adding root name '
            write(UNIout,'(2a)') &
        '               > input  file name:', filnam, &
        '               > output file name:', FILE_name
            write(UNIout,'(a,i10)') &
        '               > FILE_name length =',len(FILE_name)
            call PRG_stop ('error_BLD_FILE_name> truncature in file name ')
      end if

! Pad with blanks:
      FILE_name(totlen+1:MAX_namelen)=' '
!
! end of routine BLD_FILE_name
      return
      end function BLD_FILE_name
      end module program_files
