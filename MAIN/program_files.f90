      subroutine FILE_open (unit)
!*****************************************************************************************************************
!     Date last modified: February 23, 2005                                                         Version 2.0  *
!     Author: R.A. Poirier/F. Colonna                                                                            *
!     Description:                                                                                               *
!  Opens a file. Machine dependant routine  UNIX version                                                         *
!  on input:                                                                                                     *
!      filnam : file name known by the program (is converted into                                                *
!                    corresponding dataset name by machine dependent                                             *
!                    functions like BLD_FILE_name ...)                                                           *
!      status : UNKNOWN, old, new, scratch                                                                       *
!      action : not used                                                                                         *
!      access : only SEQUENTIAL is supported                                                                     *
!      form   : FORMATTED, unformatted returned if status OLD                                                    *
!  on output:                                                                                                    *
!      unit   : fortran unit number                                                                              *
!      Lerror : true if error occured when opening file                                                          *
!      Lexist : true if file does exists on disk                                                                 *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE program_constants

      implicit none
!
! Input scalars:
      integer unit
!
! Local:
      integer totlen, rcode, unit8, fmtlen, usrlen, recl, prim
      integer unifil, lenfil, file, lentyp, lenfmt, lenfrm
      logical Lexist,Lerror
      logical Lopen,LAppend
!      character(len=MAX_namelen) :: filnam
      character(len=MAX_specifyer) :: filnam,status,action,form
      character(len=MAX_specifyer) :: form8
      character(len=MAX_namelen) :: totnam

! Begin:
      call PRG_manager ('enter', 'FILE_open', 'UTILITY')

! Check unit and define information
      if(unit.gt.0.and.unit.le.MAX_files)then
        filnam=PRG_file(unit)%name
        status=PRG_file(unit)%status
        action=PRG_file(unit)%action
        form=PRG_file(unit)%form
      else
        write(UNIout,*)'FILE_open> unit ',unit,' out of range'
        call PRG_stop ('FILE_open> unit out of range')
      end if

      Lexist=.false.
      Lerror=.false.
      Lopen =.false.
      LAppend=.false.

! inquire:
      totnam=BLD_FILE_name(filnam)

      inquire (file=totnam, exist=Lexist, opened=Lopen, number=unit8, form=form8)
      totlen=len_trim(totnam)
      fmtlen=len_trim(form)

! file is currently open:
      if(Lopen)then
        unit =unit8 ! Should not do this???
        if(status(1:3).eq.'APP')then
          write(UNIout,'(a)')'> no REWIND done "APPEND status"'
        else
          rewind (unit=unit)
        end if
        call PRG_manager ('exit', 'FILE_open', 'UTILITY')
        return
      end if
!
      if(status(1:3).eq.'APP')then
        LAppend=.true.
        if(Lexist)then
          status = 'OLD    '
          write(UNIout,'(a)')'FILE_OPEN>'
          write(UNIout,'(3a/a,i3,/2a,/a)')'file   ="',totnam(1:totlen),'"', &
              '  on unit= ',UNIT,' form   =',form,' in APPEND disposition'
        else
          status = 'NEW    '
        end if
      else if(status(1:3).eq.'OLD')then
        if(.NOT.Lexist)then
          Lerror=.true.
          write(UNIout,'(a/)')'FILE_open>'
          write(UNIout,'(4a)')'Trying to read inexistant file: ', &
                              '"',totnam(1:totlen),'"'
          call PRG_stop ('FILE_open> OLD file does not exist')
        end if
      else if(status(1:3).eq.'NEW')then
        if(Lexist)then
          Lerror=.TRUE.
          write(UNIout,'(4a)')'FILE_open> file "',totnam(1:totlen),'"', &
            ' already exists on disk: NOT over-written'
          write(UNIout,'(a)')'Most probable cause: Integral files exist'
          write(UNIout,'(a)')'Remove the files (IJKL...) and re-run your job'
          call PRG_stop ('FILE_open> NEW file already exists')
        end if
      else if(status(1:3).eq.'SCR')then
        Lerror=.FALSE.
        close (unit=unit)
        OPEN (unit=unit, form=form, access='SEQUENTIAL', status=status)
        call PRG_manager ('exit', 'FILE_open', 'UTILITY')
        return
      else
        Lerror=.FALSE.
      end if
!
      if(.not.Lerror)then
        close(unit=unit)
        totlen=len_trim(totnam)
        if(LAppend)then
          open (unit=unit, file=totnam(1:totlen), access='SEQUENTIAL', form=form, status=status, position="APPEND")
        else
          open (unit=unit, file=totnam(1:totlen), access='SEQUENTIAL', form=form, status=status, position="REWIND")
        end if
        PRG_file(unit)%status='OLD '
      end if

      call PRG_manager ('exit', 'FILE_open', 'UTILITY')
! end of routine FILE_open
      return
      end subroutine FILE_open
      subroutine GET_unit (filename, &
                           unit,     & !. output
                           Lerror)     !. output
!******************************************************************************************************
!     Date last modified:  February 15, 2005                                                          *
!     Author: R. A. Poirier                                                                           *
!     Description: Finds an available unit                                                            *
!           Lerror is true if an error occured.                                                       *
!******************************************************************************************************
! Modules:
      USE program_files

      implicit none
!
! Input scalars:
      character*(*), intent(IN) :: filename
      integer, intent(OUT) :: unit
      logical, intent(OUT) :: Lerror

! Local scalars:
      integer :: Iunit,lenstr
      logical :: Lfound
!
! Begin:
      call PRG_manager ('enter', 'GET_UNIT', 'UTILITY')
!
! First look for a unit associated with given filename
      Lfound=.false.
      Lerror=.false.
      lenstr=len_trim(filename)
      do Iunit=1,MAX_files
        if(PRG_file(Iunit)%name(1:lenstr).eq.filename(1:lenstr))then
          unit=PRG_file(Iunit)%unit
          Lfound=.true.
          exit
        end if
      end do ! Iunit

      if(.not.Lfound)then ! Look for an unused unit
      do Iunit=1,MAX_files
        if(PRG_file(Iunit)%status(1:4).eq.'FREE')then
          PRG_file(Iunit)%unit=Iunit
          PRG_file(Iunit)%status='OLD '
          PRG_file(Iunit)%name=filename(1:lenstr)
          unit=PRG_file(Iunit)%unit
          Lfound=.true.
          exit
        end if
      end do ! Iunit
      end if

      if(.not.Lfound)then ! Look for an unused unit
        Lerror=.true.
        call PRG_stop ('GET_unit> No more free units')
      end if

! end of routine GET_unit
      call PRG_manager ('exit', 'GET_UNIT', 'UTILITY')
      return
      end subroutine GET_unit
      subroutine BLD_FileID
!********************************************************************
!     Date last modified: August 1, 2014                            *
!     Authors: R.A. Poirier                                         *
!     Description: Build the basic file ID.                         *
!********************************************************************
! Modules:
      USE program_files
      USE type_molecule
      USE type_basis_set
      USE QM_defaults

      implicit none
!
! Input scalars:
!
! Local scalars:
      character(len=64) :: Basis_file_name
      character(len=32):: Ctemp
!
! Begin:
      call PRG_manager ('enter','BLD_FileID', 'UTILITY')

      call Basis_set_file_name (BASIS%name, Basis_file_name)
      if(index(molecule%point_group,'?').ne.0)then
        Basic_FileID=trim(MOLECULE%FORMULA)// &
        '_'//Wavefunction(1:len_trim(Wavefunction))//'_'//Basis_file_name(1:len_trim(Basis_file_name))
      else
        write(Ctemp,'(a)')molecule%point_group(1:len_trim(molecule%point_group))
        Basic_FileID=trim(MOLECULE%FORMULA)//'_'//Ctemp(1:len_trim(Ctemp))// &
        '_'//Wavefunction(1:len_trim(Wavefunction))//'_'//Basis_file_name(1:len_trim(Basis_file_name))
      end if
!
! End of routine BLD_FileID
      call PRG_manager ('exit', 'BLD_FileID', 'UTILITY')
      return
      end subroutine BLD_FileID
     subroutine FILE_close (unit)
!************************************************************************
!     Date last modified: February 16, 2018                             *
!     Author: R.A. Poirier                                              *
!     Description: Release and close a file.                            *
!************************************************************************
! Modules:
      USE program_files

      implicit none
!
! Input scalar:
      integer :: unit

      PRG_file(unit)%name='???'
      PRG_file(unit)%form='???'
      PRG_file(unit)%unit=0
      PRG_file(unit)%type='???'
      PRG_file(unit)%status='FREE'
      PRG_file(unit)%action='???'
      PRG_file(unit)%position='ASIS'
      close (unit)
!
! End of routine FILE_close
      return
      end subroutine FILE_close

