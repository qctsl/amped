      subroutine CNV_StoI8 (String, value)
!******************************************************************************************************
!     Date last modified:  February 29, 2008                                                          *
!     Author: R.A. Poirier and F. Colonna                                                             *
!     Desciption: Converts a String into an integer*8 value                                           *
!******************************************************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer(kind=8), intent(out) :: value
      character*(*), intent(in) :: string
!
! Local scalars:
      integer first,last,lenstr
      character*30  text
!
      call STR_size (string, first, last)
      lenstr = last-first+1

      if(lenstr.ne.0)then
         text = '                                        '
         text(31-lenstr:30) = string(first:last)
         read(text,'(i30)') value
      else
         call PRT_warning ('CNV_StoI8', 'zero length string. converted to 0')
         value = 0
      end if

      return
      end subroutine CNV_StoI8
      subroutine CNV_cpu (secs,  & ! seconds (real*8)
                          hcpu, mcpu, scpu, ccpu, string)
!*************************************************************************************************
!     Date last modified:  February 29, 2008                                                     *
!     Author: : F. Colonna and R. Poirier                                                        *
!     Description: converts secs into integer hcpu, mcpu, scpu, ccpu                             *
!                  and write the time to string                                                  *
!*************************************************************************************************
! Modules:

      implicit none

! Input scalar:
      double precision, intent(IN) :: secs

! Output scalars:
      integer, intent(OUT) :: hcpu,mcpu,scpu,ccpu
      character*(*) string

! Local scalars:
      real :: rsecs,hours,minuts,second,cents

! Begin:
      rsecs  = real(secs)
      hcpu   = ifix(rsecs/3600.)
      hours  = float(hcpu)
      mcpu   = ifix((rsecs-3600.*hours)/60.)
      minuts = float(mcpu)
      scpu   = ifix(rsecs-3600.*hours-60.*minuts)
      second = float(scpu)
      cents  = 100.d0*secs -100.d0*ifix(rsecs)
      ccpu   = nint(cents)
      write(string,'(i4.2,a,i2.2,a,i2.2,a,i2.2,a)') &
                hcpu,'h',mcpu,'m',scpu,'s',ccpu,'c'
      return
      end subroutine CNV_cpu
      subroutine CNV_StoL (String, value)
!*************************************************************************************************
!     Date last modified:  February 29, 2008                                                     *
!     Author: R.A. Poirier and F. Colonna                                                        *
!     Description: Converts a String into a Logical  value                                       *
!*************************************************************************************************
! Modules:
      USE program_files

      implicit none
!
! Input scalar:
      character*(*), intent(IN) :: string
!
! Output scalar:
      logical, intent(OUT) :: value
!
! Local scalars:
      character*30  text
      integer first, last, lenstr
!
! Begin:
      call STR_size (string, first, last)
      lenstr = last-first+1

      if(lenstr.ne.0)then
         if(String(first:first).eq.'T')then
           value = .true.
         else if(String(first:first).eq.'F')then
           value = .false.
         else
           write(UNIout,'(3a)') &
       '   error_CNV_StoL> Illegal Boolean value "',string(first:last),'"'
           call PRG_stop ('Illegal Boolean value')
         end if
      else
         call PRT_warning ('CNV_StoL', 'zero length string. converted to .false.')
         value = .false.
      end if

      return
      end subroutine CNV_StoL
      subroutine CNV_StoD (String, value)
!*************************************************************************************************
!     Date last modified:  February 29, 2008                                                     *
!     Author: R.A. Poirier and F. Colonna                                                        *
!           Converts a string into a double precision value                                      *
!*************************************************************************************************
! Modules:
      USE program_files

      implicit none
!
! Input scalars:
      character*(*), intent(IN) :: String
      double precision, intent(OUT) :: value
!
! Local scalars:
      integer lenstr,first,last
      character*(30) Stemp
!
! Begin:
      call STR_size (String, first, last)
      lenstr = last-first+1

      if(lenstr.gt.0)then
         Stemp = '                               '
         Stemp(31-lenstr:30) = String(first:last)
         read(Stemp,'(D30.0)',err=999) value
      else if(lenstr.lt.0)then
         write(UNIout,'(a,i10)') &
       '   error_CNV_StoD> string length =',lenstr
         call PRG_stop ('error_CNV_StoD> string length < 0')
      else
         call PRT_warning ('CNV_StoD', 'zero length string. converted to 0.0d0')
         value = 0.d0
      end if

      return
 999  continue
      write(UNIout,'(a)') &
      '   error_CNV_StoD> conversion error for string:',String(1:lenstr)
      call PRG_stop ('CNV_StoD> conversion error ')

! end of routine CNV_StoD
      return
      end subroutine CNV_StoD
      subroutine CNV_StoI (String, value)
!*************************************************************************************************
!     Date last modified:  February 29, 2008                                                     *
!     Author: R.A. Poirier and F. Colonna                                                        *
!           Converts a String into an integer value                                              *
!*************************************************************************************************
! Modules:

      implicit none

! Input scalars:
      character*(*), intent(IN) :: string
      integer, intent(OUT) :: value
!
! Local scalars:
      integer :: first,last,lenstr
      character(len=30) :: text 
!
! Begin:
      call STR_size (string, first, last)
      lenstr = last-first+1

      if(lenstr.ne.0)then
         text(1:30)=' '
         text(31-lenstr:30)=string(first:last)
         read(text,'(i30)')value
      else
         call PRT_warning ('CNV_StoI', 'zero length string. converted to 0')
         value = 0
      end if
!
! end of routine CNV_StoI
      return
      end subroutine CNV_StoI
      subroutine CNV_CUtoL (chr)
!*************************************************************************************************
!     Date last modified:  February 29, 2008                                                     *
!     Author: R.A. Poirier and F. Colonna                                                        *
!           Converts one character from Upper-case to Lower-case                                 *
!*************************************************************************************************
      implicit none

! input/output scalars:
      character*1 chr

! local scalars:
      integer ichr
      character*1 lita, litz, biga, bigz, space
      parameter (lita='a',litz='z',biga='A',bigz='Z',space=' ')

! Begin:
      ichr = ichar(chr)
      if(ichr.ge.ichar(biga).and.ichr.le.ichar(bigz))then
        chr = char(ichr-(ichar(biga)-ichar(lita)))
      end if
      if(ichr.lt.ichar(space)) chr = space
!
! end of routine CNV_CUtoL
      return
      end subroutine CNV_CUtoL
      subroutine CNV_CLtoU (chr)
!*************************************************************************************************
!     Date last modified:  February 29, 2008                                                     *
!     Author: R.A. Poirier and F. Colonna                                                        *
!           Converts one character from Lower-case to Upper-case                                 *
!*************************************************************************************************
      implicit none
!
! Input scalar:
      character*1 chr
!
! Local scalars:
      integer ichr
      character*1 lita,litz,biga,space
      parameter (lita='a',litz='z',biga='A',space=' ')
!
! Begin:
      ichr = ichar(chr)
      if(ichr.ge.ichar(lita).and.ichr.le.ichar(litz))then
        chr = char(ichr+(ichar(biga)-ichar(lita)))
      end if
      if(ichr.lt.ichar(space))chr = space
!
! end of routine CNV_CtoU
      return
      end subroutine CNV_CLtoU
      subroutine CNV_LtoS (value, string)
!*************************************************************************************************
!     Date last modified:  February 29, 2008                                                     *
!     Author: R.A. Poirier and F. Colonna                                                        *
!               converts a Boolean value into a string                                           *
!*************************************************************************************************
!
      implicit none
!
! Input scalar:
      character*(*) string
      logical value
!
! Begin:
      string = 'FALSE'
      if(value) string = 'TRUE'

! end of routine CNV_LtoS
      return
      end subroutine CNV_LtoS
      subroutine CNV_DtoS (value, string)
!*************************************************************************************************
!     Date last modified:  February 29, 2008                                                     *
!     Author: R.A. Poirier and F. Colonna                                                        *
!               Converts a double precision word into a string                                   *
!     if the value has an integer value conversion to integer is done                            *
!*************************************************************************************************
! Modules:
      USE program_files

      implicit none
!
! Input scalar:
      character*(*) string
      double precision :: value
!
! Local scalars:
      character*(30) zeroes,Stemp
!     character(len=MAX_string)temp
      character(len=136)temp
      integer i,first,lenstr,ivalue,decimal,nbzero
      logical Moins
!
! Begin:
      Moins = value .lt. 0.d0
      if(Moins) value = -value

      zeroes='0000000000000000000000000000000'
      lenstr = len(string)
      do i = 1, lenstr
         string(i:i) =' '
      end do
! convert value into integer ivalue , get number of decimals:
      call CNTDEC (value, ivalue, decimal)

      if(decimal.eq.0)then
        write(Stemp,'(I30)') ivalue
        call trimld (stemp, String, lenstr)
      else if(decimal.gt.0)then
        write(Stemp,'(I30)') ivalue
        call trimld (stemp, String, lenstr)
        nbzero=decimal-lenstr
        if(nbzero.lt.0)then
           temp = string(1:lenstr-decimal)//'.' &
                //string(lenstr-decimal+1:)
        else
           temp = zeroes(1:nbzero+1)//string(1:lenstr)
           string = temp(1:1)//'.'//temp(2:)
           temp = string
        end if

        string = temp
      else if(decimal.lt.0)then
         write(Stemp,'(D30.15)') value
         call trimld (stemp, String, lenstr)
      end if

      if(Moins)then
         temp = string
         string='-'//temp
      end if
!
! end of routine CNV_DtoS
      return
      end subroutine CNV_DtoS
      subroutine CNV_ItoS (value, string)
!*************************************************************************************************
!     Date last modified:  February 29, 2008                                                     *
!     Author: R.A. Poirier and F. Colonna                                                        *
!               converts an integer value into a string                                          *
!     left justifies the string                                                                  *
!*************************************************************************************************
! Modules:
      USE program_files

      implicit none
!
! Input scalar:
      character*(*) string
      character*(30) stemp
!
! Local scalars:
      integer value
      integer first,lenstr,last
!
! Begin:
      stemp = '                             '
      write(stemp,'(I30)') value
      call trimld (stemp, String, lenstr)

      if(lenstr.gt.len(string))then
         write(UNIout,'(3a)') &
       '   error_CNV_ItoS> String truncated:"',string(1:lenstr),'"'
         call PRG_stop ('String truncated')
      end if
!
! end of routine CNV_ItoS
      return
      end subroutine CNV_ItoS
      subroutine CNV_SLtoU (string)
!*************************************************************************************************
!     Date last modified:  February 29, 2008                                                     *
!     Author: R.A. Poirier and F. Colonna                                                        *
!               Converts a string from Lower-case to Upper-case                                  *
!*************************************************************************************************
! Modules:
      USE program_files

      implicit none
!
! Input scalar:
      character*(*) string
!
! Local scalars:
      integer first,lenstr
      integer char
!
! Begin:
      call STR_size (string, first, lenstr)
      if(lenstr.gt.0)then
         do char=1,lenstr
            call CNV_CLtoU (string(char:char))
         end do
      else
         write(UNIout,'(a)')'error_CNV_SLtoU> zero length String '
         call PRG_stop ('error_CNV_SLtoU> zero length String')
      end if
!
! end of routine CNV_SLtoU
      return
      end subroutine CNV_SLtoU
      subroutine CNV_SUtoL (string)
!*************************************************************************************************
!     Date last modified:  February 29, 2008                                                     *
!     Author: R.A. Poirier and F. Colonna                                                        *
!           Converts a string from Upper-case to Lower-case                                      *
!*************************************************************************************************
! Modules:
      USE program_files

      implicit none
!
! Input scalar:
      character*(*) string
!
! Local scalars:
      integer i
      integer first,lenstr
!
! Begin:
      call STR_size (string, first, lenstr)

      if(lenstr.gt.0)then
         do i=1,lenstr
         call CNV_CUtoL (string(i:i))
         end do
      else
         write(UNIout,'(a)')'   error_CNV_SUtoL> zero lenstr String '
      end if
!
! end of routine CNV_SLUoL
      return
      end subroutine CNV_SUtoL
      subroutine CNTDEC (value, ivalue, decimal)
!*************************************************************************************************
!     Date last modified:  February 29, 2008                                                     *
!     Author: R.A. Poirier and F. Colonna                                                        *
!           counts decimals in floating point number                                             *
!                value : floating point number                                                   *
!              decimal : number of decimals                                                      *
!               ivalue : integer value returned if decimal inf to  7                             *
!*************************************************************************************************
      implicit none
!
! Input scalars:
      double precision value
      integer ivalue, decimal
!
! Local scalars:
      double precision mult, dif
      integer dec
!
! Begin:
      decimal = -1
      do dec = 0, 6
         mult   =      10.**dec
         ivalue = nint(mult*value)
         dif =  dabs(float(ivalue) - mult*value)/mult
         if(abs(dif).lt.1.d-12)then
            decimal = dec
            go to 10
         end if
      end do
      return
10    continue
!
! end of routine CNTDEC
      return
      end subroutine CNTDEC
      subroutine TRIMLD (input,   & !. input string
                         output,  & !. output string
                         strlen)    !. output length
!*************************************************************************************************
!     Date last modified:  February 29, 2008                                                     *
!     Author: R.A. Poirier and F. Colonna                                                        *
!     Description:                                                                               *
!           cut out LEADING blanks from string "input"                                           *
!           returned length is length after being trimed                                         *
!*************************************************************************************************
! Modules:
      USE program_files

      implicit none
!
! Input scalars:
      character*(*), intent(IN) :: input
      character*(*), intent(OUT) :: output
      integer, intent(OUT) :: strlen
!
! Local scalars:
      integer is,strold
      logical found
!
! Begin:
      found = .false.
      strold = len(input)

      if(strold.gt.0)then
        is = 0
        do while (.not.found)
           is = is +1
           found = input(is:is) .ne. ' '
        end do
        output = input(is:)
        strlen=strold-is +1
      else
         write(UNIout,'(a)')'   error_trimld> zero length string'
         call PRG_stop ('ERROR> trimld: zero length string')
      end if

! end routine TRIMLD
      return
      end subroutine TRIMLD
      subroutine STR_size (string, first, last)
!*************************************************************************************************
!     Date last modified:  February 29, 2008                                                     *
!     Author: R.A. Poirier and F. Colonna                                                        *
!           returns first and last non blank character                                           *
!           string may include BLANKs                                                            *
!*************************************************************************************************
! Modules:
      USE program_files

      implicit none
!
! Input scalars:
      character*(*) string
      integer first, last
! local   :
      integer is, lenstr
      logical found, max

! Begin:
      found = .false.

      lenstr = len(string)

      if(lenstr.eq.0)then
          write(UNIout,'(a)')'ERROR> STR_size: zero length string'
          call PRG_stop ('ERROR> STR_size: zero length  string')
      end if

!     write(*,*) ' STR_size> input string "',string,'"'
      max = .false.
      is = lenstr

! search not blank from end.
      do while (.not.found.and..not.max)
           last = is
           found = string(is:is) .ne. ' ' &
              .and.ichar(string(is:is)).ne.0
!          write(*,*)
!    .   ' STR_size> end cursor =',is,' = "',string(is:is),'"'
!          write(*,*) ' STR_size> code =',ichar(string(is:is))
           is = is - 1
           max = is .eq. 0
      end do

!     write(*,*) ' STR_size> last ',last

      if(max)then
          if(.not.found)then
!           if(WARNING_level.gt.4)write(UNIout,'(a)')'warning_STR_size> empty string'
            first = 1
            last  = 0
            string = ' '
          else
            first = 1
            last  = 1
          end if
          return
      end if

! search not blank from Beginning.
      found = .false.
      max = .false.
      is = 0
      do while (.not.found.and..not.max)
           is = is +1
           max = is .eq. lenstr+1
           first = is
           found = string(is:is) .ne. ' '
!          write(*,*)
!    . ' STR_size> Begin cursor =',is,' = "',string(is:is),'"'
      end do

!     write(*,*) ' first ',first

      if(max)then
          write(UNIout,'(a)')'ERROR> STR_size: strange  string'
          write(UNIout,'(2a)')'string: ',string(first:last)
          call PRG_stop ('ERROR> STR_size: strange  string')
      end if

      if(last.lt.first)then
          write(UNIout,'(a,i5)') &
        ' error_STR_size> first non-blank position=',first, &
        ' error_STR_size> last  non-blank position=',last
          call PRG_stop ('error_STR_size> last < first')
      end if
!
! end of routine STR_size
      return
      end subroutine STR_size
      subroutine STREAM (filnam)
!*************************************************************************************************
!     Date last modified: July 19, 1991                                                          *
!     Author: F. Colonna                                                                         *
!           Streams a command file
!           It is recursive
!*************************************************************************************************
! Modules
      USE program_files
      USE program_parser

      implicit none
!
! Input scalars:
      character*(*) filnam
! local   :
      logical lerror,lexist
! Begin:
      call PRG_manager ('enter', 'STREAM', 'UTILITY')

      comfil=filnam
      comfll=len_trim (comfil)

! alloc new STREAM file on unit UNIT:
      call GET_unit (filnam, curuni, Lerror)
      write(uniout,*)'STREAM> File connected to unit ',curuni
      NStream_files=NStream_files+1
      PRG_stream_unit(NStream_files)=curuni
      PRG_file(curuni)%unit=curuni
      PRG_file(curuni)%status='OLD '
      PRG_file(curuni)%type='STREAM '
      PRG_file(curuni)%form='FORMATTED '
      PRG_file(curuni)%action='READ '
      PRG_file(curuni)%name=filnam
      call FILE_open (curuni)

      if(PRINT_level.ge.20)then
        comfll=len_trim (comfil)
        write(uniout,'(3a)')' stream: File  "',comfil(1:comfll),'" Streamed-in'
        write(uniout,'(a,i2)')' stream: unit  =',curuni
      end if

! end of routine  STREAM
      call PRG_manager ('exit', 'STREAM', 'UTILITY')
      return
      end
      subroutine FILE_delete (FilnamI, lerror)
!*************************************************************************************************
!     Date last modified:  July 19, 1991                                                         *
!     Author: : F. Colonna                                                                       *
!           Deletes a file by its name
!           catalog the deleted file to keep trace of it
!*************************************************************************************************
! Modules:
      USE program_files

      implicit none
!
! i/o scalars:
      character*(*) FilnamI
      logical Lopen,Lerror,Lexist
      integer unit
!
! Local scalars:
      character(len=255) totnam
      character(len=255) filnam
      integer totlen,lenfil,first,last,prim,Ierror
      logical Lerral

! Begin:
      call PRG_manager ('enter', 'FILE_delete', 'UTILITY')

      Lopen=.false.
      Lerror=.false.
      call STR_size (filnamI, prim, last)
      filnam = FilnamI(prim:last)//'  '
      lenfil  = last-prim+1

      totnam = BLD_FILE_name (filnam)
      call STR_size (totnam, first, totlen)

      inquire (file=totnam, exist=Lexist, opened=Lopen, number=unit, iostat=Ierror)

      if(Ierror.ne.0)then
        write(6,*)'Ierror= ',Ierror
        call flush(6)
        stop
      end if

      if(.not.Lexist.and.WARNING_level.gt.0)then
         write(UNIout,'(a/3a)')'WARNING> FILE_delete: trying to delete inexisting file:', &
         '"',totnam(first:totlen),'"'
      end if

      if(Lopen)then
        close (UNIT=UNIT, status='DELETE')
      else   ! not opened
        if(Lexist)then
          call GET_unit (filnam, unit, Lerror)
          if(PRINT_level.ge.1)write(UNIout,*)'FILE_delete> File connected to unit ',unit
          PRG_file(unit)%unit=unit
          PRG_file(unit)%status='OLD '
          PRG_file(unit)%action='ANYTHING '
          PRG_file(unit)%form='FORMATTED '
          PRG_file(unit)%name=filnam
          call FILE_open (unit)
          close (unit=unit, status='DELETE')
        else  !  not opened and does not exists on disk
          Lerror=.true.
          if (WARNING_level.gt.1)then
             write(UNIout,'(3a)')'FILE_delete: file "',totnam(1:totlen), &
             '" does not exist: NOT deleted '
          end if   !. WARNING_level
        end if ! Lexist
      end if   ! Lopen

      if(Lexist.and.(unit.gt.99.or.unit.lt.0))then
        write(UNIout,*)'FILE_delete ERROR> unit out of range ',unit
        stop 'FILE_delete ERROR> unit out of range '
      end if

      if(.not.Lerror)then
        if(unit.gt.99.or.unit.lt.0)then
          write(UNIout,*)'FILE_delete ERROR> unit out of range ',unit
          stop 'FILE_delete ERROR> unit out of range '
        end if

        PRG_file(unit)%status='FREE '
        PRG_file(unit)%type='??? '
        PRG_file(unit)%name='??? '
        PRG_file(unit)%form='??? '
        PRG_file(unit)%action='??? '

        if(PRINT_level.ge.1)write(UNIout,'(3a)')'FILE_delete> file "',totnam(1:totlen),'" deleted'
      end if

! end of FILE_delete
      call PRG_manager ('exit', 'FILE_delete', 'UTILITY')
      return
      end
      subroutine FILE_release (Unit)
!*************************************************************************************************
!     Date last modified:  July 19, 1991                                                         *
!     Author: : R.A. Poirier                                                                     *
!           Releases a MUngauss file and closes it.
!*************************************************************************************************
! Modules:
      USE program_files

      implicit none
!
! i/o scalars:
      integer Unit
!
! Local scalars:

! Begin:
      call PRG_manager ('enter', 'FILE_release', 'UTILITY')

      if(Unit.gt.24)then
        PRG_file(unit)%status='FREE '
        PRG_file(unit)%type='??? '
        PRG_file(unit)%name='??? '
        PRG_file(unit)%form='??? '
        PRG_file(unit)%action='??? '
      else
        PRG_file(unit)%status='FREE '
      end if

      close (UNIT=UNIT)
!
! end of FILE_release
      call PRG_manager ('exit', 'FILE_release', 'UTILITY')
      return
      end
      subroutine RemoveBlanks(string_in, string_out, lenstr_out)
!**********************************************************************************
!     Date last modified:  December 3, 2010                                       *
!     Author: : J.-P. Becker                                                      *
!     Writes the string string_in to the string string_out removing blank spaces  *
!**********************************************************************************
! Modules:

      implicit none
!
! Input scalars:
      character*(*) :: string_in, string_out
      integer :: lenstr_out
!
! Local scalares:
      character*1 :: char_cur
      integer :: lenstr_in, ind_in, ind_out
!     
! Begin:
      string_out(1:)=' '
      lenstr_in=len_trim(string_in)
      ind_out=0
      do ind_in=1,lenstr_in
        char_cur(1:1)=string_in(ind_in:ind_in)
        if(char_cur(1:1).eq.' ') cycle
        ind_out=ind_out+1
        string_out(ind_out:ind_out)=char_cur(1:1)
      enddo ! ind_in=1, lenstr_in
      lenstr_out=ind_out
! end of subroutine RemoveBlanks
      return
      end subroutine RemoveBlanks
