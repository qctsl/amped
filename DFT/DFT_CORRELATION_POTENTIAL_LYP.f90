      subroutine DFT_CORRELATION_POTENTIAL_LYP
!*********************************************************************************************
!     Date last modified: June 25, 2015                                                      *
!     Author: J. Hollett                                                                     *
!     Desciption: Compute the LYP correlation potential over AOs.                            *
!*********************************************************************************************
! Modules:
      USE N_integration
      USE type_molecule
      USE program_constants
      USE program_timing
      USE program_files
      USE QM_defaults

      implicit none
!
! Input scalars:
!
! Local scalars:
      integer :: Iatom,NApoints_tot,Ifound,Znum
      double precision :: t0,t1,t2,time

! Begin:
      call PRG_manager ('enter', 'DFT_CORRELATION_POTENTIAL_LYP', 'CORRELATION_POTENTIAL%LYP')
!
      call GET_object ('QM', 'CMO', Wavefunction)
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
!
      call CPU_time(t0)
      call CPU_time(t1)      
!
      call INI_LYP_potential
!
      NApoints_tot = 0
      do Iatom=1,Natoms
        Znum=CARTESIAN(Iatom)%Atomic_number
        if(Znum.le.0)cycle
        Ifound=GRID_loc(Znum)
        NApts_atom=NApoints_atom(Ifound)
!
! Set up grid points for a particular atom
        NApoints_tot = NApoints_tot + NApts_atom
!
! Calculate the atomic contribution to v_LYP
      call LYP_potential (Iatom, Ifound)
      end do ! Iatom
!
      call PRT_LYP_potential

      call CPU_time(t2)
      time = t2 - t1
!
! End of routine DFT_CORRELATION_POTENTIAL_LYP
      call PRG_manager ('exit', 'DFT_CORRELATION_POTENTIAL_LYP', 'CORRELATION_POTENTIAL%LYP')
      end subroutine DFT_CORRELATION_POTENTIAL_LYP
      subroutine LYP_potential (Iatom, Ifound)
!************************************************************************************
!     Date last modified: June 25, 2014                                             *
!     Author: JWH                                                                   *
!     Description: Calculate the LYP potential (Phys.Rev.B. 1988, 37, 785)          *
!************************************************************************************
! Modules:
      USE program_constants
      USE type_molecule
!     USE type_density 
      USE QM_objects 
      USE N_integration
      USE NI_defaults
      USE DFT_LYP
      USE type_basis_set

      implicit none

! Input scalars:
      integer :: Iatom,Ifound
!
! Local scalars:
      integer :: IApoint
      integer :: IAO,JAO,IJ_index
      double precision :: CF
      double precision :: BWeight,XApt,YApt,ZApt,WApt,rho
      double precision :: drho_X,drho_Y,drho_Z,delrho_2, del2rho
      double precision :: TE_LYP_r,vLYP_r, F1, F1p, F1pp, G1, G1p, G1pp
      double precision :: Hess(3,3)
      double precision, dimension(:), allocatable :: AOprod

! Begin: 
!
      CF = 0.3D0*(THREE*PI_VAL**2)**(F2_3)
      TE_LYP_r = ZERO
      do IApoint=1,NApts_atom
          XApt=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          YApt=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          ZApt=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
          WApt=Egridpts(Ifound,IApoint)%w
        call BeckeW1 (XApt, YApt, ZApt, Iatom, Bweight)
        call GET_rho_Atom (XApt, YApt, ZApt, rho)  ! Allocates AOprod!!!
        call GET_drho_Atom (XApt, YApt, ZApt, drho_X, drho_Y, drho_Z)
        delrho_2=drho_X**2+drho_Y**2+drho_Z**2    ! gradient (squared)
        call GET_d2rho_Atom (XApt, YApt, ZApt, Hess)
        del2rho=Hess(1,1)+Hess(2,2)+Hess(3,3)

        ! Get AO products
        if(.not.allocated (AOprod))allocate (AOprod(1:MATlen))
        call AO_products (XApt, YApt, ZApt, AOprod, MATlen)
        ! Calculate the "Weizsacker" kinetic energy density
        if(rho.lt.TOL2)then
          vLYP_r = ZERO
        else if(rho.lt.TOL1)then
          F1 = rho**(F1_3)/(rho**(F1_3) + dLYP)
        ! all G terms vanish due to exponential
          vLYP_r = Bweight*WApt &
                   *(-aLYP)*(F1 + dLYP*rho**(F1_3)/(THREE*(rho**(F1_3)+dLYP)**TWO))
        else
! Calculate the functions F1 and G1 and their derivatives
          F1 = ONE/(ONE + dLYP*rho**(-F1_3))
          F1p = F1**TWO*dLYP/(THREE*rho**(F4_3))
          F1pp = F1**THREE*TWO*dLYP**TWO/(9.0D0*rho**(F8_3))&
               - F1p*4.0D0/(THREE*rho)
          G1 = F1*rho**(-F5_3)*dexp(-cLYP*rho**(-F1_3))
          G1p = dexp(-cLYP*rho**(-F1_3))*((cLYP-5.0D0*rho**(F1_3))*F1&
              + THREE*rho**(F4_3)*F1p)/(THREE*rho**THREE)
          G1pp = dexp(-cLYP*rho**(-F1_3))*((cLYP**TWO-14.0D0*cLYP*rho**(F1_3)&
               + 40.0D0*rho**(F2_3))*F1 + 6.0D0*(cLYP-5.0D0*rho**(F1_3))&
               *rho**(F4_3)*F1p + 9.0D0*rho**(F8_3)*F1pp)/(9.0D0*rho**(13.0D0/3.0D0))
!
          vLYP_r = Bweight*WApt &
                   *(-aLYP*(F1p*rho + F1) - aLYP*bLYP*CF*rho**(F5_3)*(G1p*rho + F8_3*G1)&
                   - 0.25D0*aLYP*bLYP*(G1pp*rho*delrho_2 + G1p*(THREE*delrho_2 + TWO*rho*del2rho)&
                   + 4.0D0*G1*del2rho) - aLYP*bLYP/72.0D0*(THREE*G1pp*rho*delrho_2 + G1p*(5.0D0&
                   *delrho_2 + 6.0D0*rho*del2rho) + 4.0D0*G1*del2rho))
        end if
!
! Add integration point to atomic contribution
        vLYP_atom(Iatom,1:MATlen) = vLYP_atom(Iatom,1:MATlen) + FourPi*vLYP_r*AOprod(1:MATlen)
!
      deallocate(AOprod)
      end do ! IApoint
!
! Add to total v_AO matrix
      vLYP(1:MATlen) = vLYP(1:MATlen) + vLYP_atom(Iatom,1:MATlen)
!
      end subroutine LYP_potential
      subroutine INI_LYP_potential
!************************************************************************************
!     Date last modified: June 25, 2015                                             *
!     Author: JWH                                                                   *
!     Description: Initialization for LYP potential.                                *
!************************************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE DFT_LYP
      USE QM_objects

      implicit none

! Begin:
!
      call PRG_manager('enter','INI_LYP_potential','UTILITY')
!
      if(allocated(vLYP))then
        deallocate(vLYP,vLYP_atom)
      end if
      allocate(vLYP(MATlen),vLYP_atom(Natoms,MATlen))
!
      vLYP_atom = ZERO
      vLYP = ZERO
!
! End subroutine INI_LYP_potential
      call PRG_manager('enter','INI_LYP_potential','UTILITY')
      end subroutine INI_LYP_potential
      subroutine PRT_LYP_potential
!***********************************************************************
!     Date last modified: June 25, 2015                                *
!     Author: JWH                                                      *
!     Description: Print the LYP potential.                            *
!***********************************************************************
! Modules:
      USE program_files
      USE type_molecule
      USE DFT_LYP
      USE QM_objects

      implicit none
!
! Local scalars:
      integer :: IAO, JAO, IJ_index
!
! Begin:
!
      write(UNIout,'(/a)')'       LYP correlation potential         '
      write(UNIout,'(a)')'-----------------------------------------'
      write(UNIout,'(a)')'  IAO     JAO           vLYP_ij          '
      write(UNIout,'(a)')'-----------------------------------------'
! loop over AOs      
      do IAO = 1, Nbasis
        do JAO = 1, IAO
          IJ_index = Nbasis*(IAO-1) + JAO
          write(UNIout,'(X,I4,4X,I4,5X,F14.8)')IAO,JAO,vLYP(IJ_index)
        end do ! JAO
      end do ! JAO
!
      end subroutine PRT_LYP_potential
!
