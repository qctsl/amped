      subroutine SET_defaults_DFT
!*****************************************************************************************************************
!     Date last modified: March 31, 2000                                                            Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Set code defaults.                                                                            *
!*****************************************************************************************************************
! Modules:
      USE DFT_LYP

      implicit none
!
! Begin:
! LYP parameters
      aLYP = 0.049D0
      bLYP = 0.132D0
      cLYP = 0.2533D0
      dLYP = 0.349D0

      return
      end subroutine SET_defaults_DFT

