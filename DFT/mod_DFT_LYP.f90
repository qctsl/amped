      module DFT_LYP
!***********************************************************************************
!     Date last modified: May 22, 2015                                             *
!     Author: JWH                                                                  *
!     Description: Quantities used in LYP correlation functional calculations.     *
!***********************************************************************************
! Modules:

      implicit none
!
! Scalars:
      double precision :: E_LYP
! 
! Arrays:
      double precision, dimension(:), allocatable :: E_LYP_atom, vLYP
      double precision, dimension(:,:), allocatable :: vLYP_atom
!
! Parameters 
      double precision :: aLYP,bLYP,cLYP,dLYP
      double precision, parameter :: TOL1=1.0D-10
      double precision, parameter :: TOL2=1.0D-20
      double precision, parameter :: F1_3=1.0D0/3.0D0
      double precision, parameter :: F2_3=2.0D0/3.0D0
      double precision, parameter :: F4_3=4.0D0/3.0D0
      double precision, parameter :: F5_3=5.0D0/3.0D0
      double precision, parameter :: F8_3=8.0D0/3.0D0
      double precision, parameter :: F1_4=1.0D0/4.0D0
      double precision, parameter :: F1_8=1.0D0/8.0D0
      double precision, parameter :: F1_9=1.0D0/9.0D0
      double precision, parameter :: F1_18=1.0D0/18.0D0

!
      end module DFT_LYP
