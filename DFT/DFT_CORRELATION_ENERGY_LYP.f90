      subroutine DFT_CORRELATION_ENERGY_LYP
!*********************************************************************************************
!     Date last modified: June 25, 2015                                                      *
!     Author: J. Hollett                                                                     *
!     Desciption: Compute the LYP correlation energy.                                        *
!*********************************************************************************************
! Modules:
      USE program_constants
      USE program_timing
      USE program_files
      USE N_integration
      USE type_molecule
      USE QM_defaults
      USE DFT_LYP

      implicit none
!
! Input scalars:
      integer :: Iatom,Ifound,NApoints_tot,Znum
!
! Local scalars:
      double precision :: t0,t1,t2,time

! Begin:
      call PRG_manager ('enter', 'DFT_CORRELATION_ENERGY_LYP', 'CORRELATION_ENERGY%LYP')
!
      call GET_object ('QM', 'CMO', Wavefunction)
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
!
      call CPU_time(t0)
      call CPU_time(t1)      

      call BLD_BeckeW_XI
      call INI_LYP_energy
!
      NApoints_tot = 0
      do Iatom=1,Natoms
        Znum=CARTESIAN(Iatom)%Atomic_number
        if(Znum.le.0)cycle
        Ifound=GRID_loc(Znum)
        NApts_atom=NApoints_atom(Ifound)
!
        NApoints_tot = NApoints_tot + NApts_atom
!
! Calculate the atomic contribution to E_LYP
        call LYP_energy (Iatom, Ifound)
      end do ! Iatom
      if(MUN_prtlev.gt.0)write(UNIout,'(a,i8/)')'Total number of grid points: ',NApoints_tot
!
      write(UNIout,'(a,f20.12)')'LYP energy   = ',E_LYP


      call CPU_time(t2)
      time = t2 - t1
!
! End of routine DFT_CORRELATION_ENERGY_LYP
      call PRG_manager ('exit', 'DFT_CORRELATION_ENERGY_LYP', 'CORRELATION_ENERGY%LYP')
      end subroutine DFT_CORRELATION_ENERGY_LYP
      subroutine LYP_energy (Iatom, Ifound)
!************************************************************************************
!     Date last modified: April 29th, 2014                                          *
!     Author: JWH                                                                   *
!     Description: Calculate the LYP correlation energy (Phys.Rev.B. 1988, 37, 785) *
!************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE N_integration
      USE type_molecule
      USE type_density 
      USE module_grid_points
      USE NI_defaults
      USE DFT_LYP
      USE type_basis_set
      USE QM_objects

      implicit none

! Input scalars:
      integer :: Iatom,Ifound
!
! Local scalars:
      integer :: IApoint
      double precision :: CF,tw
      double precision :: BWeight,XApt,YApt,ZApt,WApt,rho
      double precision :: delrho_2,del2rho,E_LYP_r,TE_LYP_r
      double precision :: drho_X,drho_Y,drho_Z
      double precision :: Hess(3,3)

! Begin: 
!
      CF = 0.3D0*(THREE*PI_VAL**2)**(F2_3)
      TE_LYP_r = ZERO
      do IApoint=1,NApts_atom
          XApt=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          YApt=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          ZApt=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
          WApt=Egridpts(Ifound,IApoint)%w
        call BeckeW1 (XApt, YApt, ZApt, Iatom, Bweight)
        call GET_rho_Atom (XApt, YApt, ZApt, rho)
        call GET_drho_Atom (XApt, YApt, ZApt, drho_X, drho_Y, drho_Z)
        delrho_2=drho_X**2+drho_Y**2+drho_Z**2    ! gradient (squared)
        call GET_d2rho_Atom (XApt, YApt, ZApt, Hess)
        del2rho=Hess(1,1)+Hess(2,2)+Hess(3,3)
        ! Calculate the "Weizsacker" kinetic energy density
        if(rho.lt.TOL2)then
          E_LYP_r = ZERO
        else if(rho.lt.TOL1)then
          E_LYP_r = Bweight*WApt*(-aLYP)*rho**(F4_3)/(rho**(F1_3)+dLYP)
        else
          tw = F1_8*(delrho_2/rho - del2rho)
          E_LYP_r = Bweight*WApt*(-aLYP)*(rho+bLYP*rho**(-F2_3)*(CF*rho**(F5_3)-TWO*tw &
                  + (F1_9*tw+F1_18*del2rho))*dexp(-cLYP*rho**(-F1_3)))     &
                  /(ONE+dLYP*rho**(-F1_3))
        end if
!
! Add integration point to atomic contribution
        TE_LYP_r = TE_LYP_r + E_LYP_r
      end do ! IApoint
!
      E_LYP_atom(Iatom) = + FourPi*TE_LYP_r
      E_LYP = E_LYP + E_LYP_atom(Iatom)
!
      end subroutine LYP_energy
      subroutine INI_LYP_energy
!************************************************************************************
!     Date last modified: April 30, 2014                                            *
!     Author: R.A. Poirier                                                          *
!     Description: Initialization for LYP energy.                                   *
!************************************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE DFT_LYP

      implicit none

! Begin:
      if(allocated(E_LYP_atom))then
        deallocate(E_LYP_atom)
      end if
      allocate(E_LYP_atom(Natoms))
      E_LYP_atom = ZERO 
      E_LYP = ZERO

      end subroutine INI_LYP_energy
