      subroutine GET_DFT_object (class, Objname, Modality)
!**************************************************************************
!     Date last modified: June 21, 2001                       Version 2.0 *
!     Author: R.A. Poirier                                                *
!     Description: Objects belonging to the class DFT                     *
!**************************************************************************
! Modules:
      USE program_files
      USE program_objects

      implicit none
!
! Input scalars:
      character*(*) :: class
      character*(*) :: Objname
      character*(*) :: Modality
!
! Local scalars:
      integer :: Object_number
!
! Local scalars:
!
! Begin:
      call PRG_manager ('enter', 'GET_DFT_object', 'UTILITY')
!
      call GET_object_number (OBJ_DFT, NDFTobjects, FirstDFT, Objname, Modality, Object_number)

      if(OBJ_DFT(Object_number)%current)then
        call PRG_manager ('exit', 'GET_DFT_object', 'UTILITY')
        return
      end if

      OBJ_DFT(Object_number)%exist=.true.
      OBJ_DFT(Object_number)%Current=.true.
      Object(ObjNum)%exist=.true.
      Object(ObjNum)%Current=.true.

      select_Object: select case (Objname)
        case ('CORRELATION_ENERGY') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
          case ('LYP')
            call DFT_CORRELATION_ENERGY_LYP
          case default
            write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
            stop 'No such object'
        end select
        case ('CORRELATION_POTENTIAL') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        select case (Modality)
          case ('LYP')
            call DFT_CORRELATION_POTENTIAL_LYP
          case default
            write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                         '" for Modality "',Modality(1:len_trim(Modality)),'"'
            stop 'No such object'
        end select

      case default
        write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                       '" for Modality "',Modality(1:len_trim(Modality)),'"'
        stop 'No such object'
      end select select_Object

      call PRG_manager ('exit', 'GET_DFT_object', 'UTILITY')
      return
      end subroutine GET_DFT_object
