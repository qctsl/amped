      subroutine BLD_DFT_objects
!******************************************************************************************************
!       Date last modified June 25, 2015                                                              *
!       Author: R.A. Poirier                                                                          *
!       Description:  Builds a complete list of all the objects which can be created in MUNgauss      *
!       which belong to the "class" DFT                                                               *
!******************************************************************************************************
! Modules:
      USE program_objects

      implicit none

! Local scalar:
      integer :: Iobject
!
! Begin:
      OBJ_DFT(1:Max_objects)%modality = 'NONE'
      OBJ_DFT(1:Max_objects)%class = 'DFT'
      OBJ_DFT(1:Max_objects)%depend = .true.
      NDFTobjects = 0
!
! CORRELATION FUNCTIONALS
      NDFTobjects = NDFTobjects + 1
      OBJ_DFT(NDFTobjects)%name = 'CORRELATION_ENERGY'
      OBJ_DFT(NDFTobjects)%modality = 'LYP'
      OBJ_DFT(NDFTobjects)%routine = 'DFT_CORRELATION_ENERGY_LYP'
!
! CORRELATION POTENTIALS
      NDFTobjects = NDFTobjects + 1
      OBJ_DFT(NDFTobjects)%name = 'CORRELATION_POTENTIAL'
      OBJ_DFT(NDFTobjects)%modality = 'LYP'
      OBJ_DFT(NDFTobjects)%routine = 'DFT_CORRELATION_POTENTIAL_LYP'
!
! Dummy
      NDFTobjects = NDFTobjects + 1
      OBJ_DFT(NDFTobjects)%name = '?'
      OBJ_DFT(NDFTobjects)%modality = '?'
      OBJ_DFT(NDFTobjects)%routine = '?'
!
      do Iobject=1,NDFTobjects
        OBJ_DFT(Iobject)%exist=.false.
        OBJ_DFT(Iobject)%Current=.false.
      end do

      return
      end subroutine BLD_DFT_objects
