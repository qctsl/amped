      subroutine MENU_DFT
!***********************************************************************
!     Date last modified: September 11, 2015                           *
!     Author: JWH                                                      *
!     Description: Density functional settings.                        *
!***********************************************************************
!Modules:
      USE DFT_LYP
      USE program_parser
      USE MENU_gets

      implicit none
!
! Local scalars:
      logical done
      integer :: lenstr
      character(len=8) :: LYPpar
!
! Begin:
      call PRG_manager ('enter', 'MENU_DFT', 'UTILITY')
!
! Defaults:
      
      done=.false.
!
! Menu:
      do while (.not.done)
      call new_token ('DFT')
!
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      ' Command DFT:', &
      '   Purpose: Calculate density functional energies ', &
      '   Syntax :', &
      '   PArameters   = <string> : parameters to use for LYP functional (TM)', &
      '   end'
!
! PArameters
      else if(token(1:2).eq.'PA')then
        call GET_value(LYPpar, lenstr)
        ! Thakkar and McCarthy LYP parameters, JCP 2009, 131, 134109
        if(LYPpar(1:2).eq.'TM')then
        aLYP = 0.0393D0
        bLYP = 0.21D0
        cLYP = 0.41D0
        dLYP = 0.15D0
        else ! default
!         write(UNIout,'(2a)')'Invalid parameters for LYP: ',LYPpar
!         stop 'Invalid parameters for LYP'
        end if
!
      else

        call MENU_end (done)
      end if
 
      end do !(.not.done)
!
! End of routine MENU_DFT
      call PRG_manager ('exit', 'MENU_DFT', 'UTILITY')
      return
      end
