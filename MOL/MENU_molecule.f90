      subroutine MENU_molecule
!****************************************************************************************
!     Date last modified: January 31, 2001                                              *
!     Author: R.A. Poirier                                                              *
!     Description: Set up molecule, coordinates. Molecular information is stored in     *
!                  module type_molecule.                                                *
!****************************************************************************************
! Modules:
      USE program_constants
      USE program_parser
      USE program_files
      USE program_objects
      USE menu_gets
      USE type_molecule
      USE GRAPH_objects
      USE OPT_objects
      USE OPT_defaults
      USE type_basis_set
      USE QM_objects

      implicit none
!
! Local scalars:
      integer, parameter :: MAX_Qatoms=1000 ! Maximum number of atoms
      integer :: CHARGEw,NatomsW,NMCOORw,NZMVARw,MULTIPw
      integer :: NZMVAR,NAelectron,NBelectron,NoccMO
      integer :: NZM_fixedw,NZM_names
      integer :: Iatom,ixOBJ,leninp,lenstr,lentitlew,lenuni,status
      integer :: Ipos1,Ipos2
      character(len=MAX_Iname) CURRENT_coordinates
      character(len=MAX_Iname) MOLINPw
      character(len=MAX_title) MOL_TITLEw
      logical done,LPICOR,LUSE_previousH
      character(len=MAX_length) string
!
      type (cartesian_coordinates), dimension(:), allocatable :: XYZC_work
      type (atom_definition), dimension(:), allocatable ::  MOL_atomsW
      type (Zmatrix_coordinates) ZmatrixW
!
! Work arrays:
      character*8, dimension(:), allocatable :: MOLEID_work
!
! Begin:
      call PRG_manager ('enter', 'MENU_molecule', 'UTILITY')
!
      CURRENT_coordinates=OPT_parameters%name
      LUSE_previousH=.false. ! Assume will not use the previous Hessian (if it exist)
      call INI_MENU_molecule
!
! Menu:
      done=.false.
      do while (.not.done)
      call NEW_token (' Molecule:')
!
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      ' Command MOLecule :', &
      '   Purpose: Define the molecule', &
      '   Syntax :', &
      '   ADDatom <command> to add a new atom', &
      '   BSradii = <command>: command to change BS radii', &
      '   CArtesian <command>: Free format cartesians: atom X Y Z', &
      '   CHarge = <integer>: Total charge on the molecule, Default=0', &
      '   DEFine <command> to assign values to the variables', &
      '   FREez, ZMatrix or Z-Matrix <command>: read free format z-matrix', &
      '   HEssian = <string>: Previous (only option currently)', &
      '   MUltiplicity = <integer>: Default=1', &
      '   TITle = <string>: Title (should be very detailed)', &
      '   TYpe = <string>: GS(Ground State) or TS (Transition State)', &
      '   UNits = <string>: ANGstroms, A.U. or Bohr, Default=ANGstroms', &
      '   end'
!
! ADDatom
      else if(token(1:3).EQ.'ADD')then
        call ADD_atom
!
! TYpe ??? See below
      else if(token(1:2).EQ.'TY')then
        call GET_value (MOL_type, lenstr)
!
! BSradii
      else if(token(1:2).EQ.'BS')then
        call MENU_BS_radii
!
! CArtesians
      else if(token(1:2).EQ.'CA')then
        MOLINPw(1:9)='CARTESIAN'
        call CARTESIAN_input (XYZC_work, MAX_Qatoms, ZmatrixW%units, NatomsW)
!
! DEFine
      else if(token(1:3).EQ.'DEF')then
        MOLINPw(1:5)='FREEZ'
        call SET_define
!
! FREe or Z_Matrix
      else if(token(1:3).EQ.'FRE'.or.token(1:1).EQ.'Z')then
        MOLINPw(1:8)='Z-MATRIX'
        call Zmatrix_input

! HEssian
      else if(token(1:2).EQ.'HE')then
        call GET_value (string, lenstr)
        if(index(string, 'P').ne.0)then
          OPT_method_previous=OPT_method
          LUSE_previousH=.true. ! Use the previous Hessian
        end if ! index(string, 'P').ne.0
! PIC
      else if(token(1:3).EQ.'PIC')then
        call GET_value (LPICOR)
!
! Title
      else if(token(1:3).EQ.'TIT')then
        call GET_value (MOL_TITLEw, lentitlew)
!
! TYpe
      else if(token(1:2).EQ.'TY')then
        call GET_value (MOLINPw, leninp)
        if(MOLINPw(1:1).EQ.'F')then
          MOLINPw='FREEZ'
        else if(MOLINPw(1:1).EQ.'Z')then
          MOLINPw='Z-MATRIX'
        else if(MOLINPw(1:1).EQ.'C')then
          MOLINPw='CARTESIAN'
        end if
!
! UNits
      else if(token(1:2).EQ.'UN')then
        call GET_value (ZmatrixW%units, lenuni)
        if(ZmatrixW%units(1:2).EQ.'AN')then
          ZmatrixW%units(1:8)='ANGSTROM'
        else if(ZmatrixW%units(1:2).EQ.'A.')then
         ZmatrixW%units(1:8)='BOHR'
        else if(ZmatrixW%units(1:2).EQ.'BO')then
          ZmatrixW%units(1:8)='BOHR'
        else
          write(UNIout,*)'error_> Illegal units: ',ZmatrixW%units(1:lenuni)
           call PRG_stop ('MENU_molecule> Illegal units')
        end if ! ZmatrixW%units
!
! CHarge
      else if(token(1:2).EQ.'CH')then
        call GET_value (CHARGEw)

! MUltiplicity
      else if(token(1:2).EQ.'MU')then
        call GET_value (MULTIPw)

      else
        call MENU_end (done)
      end if
!
      end do ! .not.done
!
! Initialize code
      if(Natoms.gt.0.and.NatomsW.gt.0)then ! Previous molecule existed and new one defined
        call load_molecule
        call PRG_reset ! treat as a $NEW if new molecule
!       call KILL_objects (.true.) ! Kill all
!       if(.not.Larchive)call set_defaults_PROGRAM ! reset program defaults everytime a new molecule is defined
      else if(Natoms.eq.0.and.NatomsW.eq.0)then ! No molecule has been defined
        write(UNIout,'(/a)') 'ERROR: MENU_molecule>'
        write(UNIout,'(a)') 'No molecule has been defined'
        write(UNIout,'(a)') 'Must input cartesians or a Z-matrix'
        call PRG_stop ('No molecule has been defined')
      else if(NatomsW.gt.0)then ! First molecule defined
        call load_molecule
      else if(Natoms.gt.0)then ! Must be using a previously defined molecule
        call KILL_objects (.false.)
        call set_defaults_PROGRAM         ! reset program defaults everytime a new molecule is defined
        if(index(MOL_TITLEw, 'UNKNOWN').eq.0)then
          MOL_TITLE=MOL_TITLEw ! New title has been defined for previous molecule
        end if
      end if
!
! Allow the Charge/Multiplicity to change
      molecule%charge=CHARGEw
      Multiplicity=MULTIPw

      if(Natoms.eq.1)MOLINP(1:9)='CARTESIAN'

      if(MOLINP(1:5).ne.'CARTE')then
        call PRT_FZM
        call PRT_ZMAT
      end if
!
      call PRT_XYZ
!
! Always build and print the distance matrix.
      call GET_object ('MOL', 'ATOMIC', 'DISTANCES')
      call PRT_distance_matrix
!
! Check Charge/multiplicity and Define the number of occupied MO.
      call DEF_NoccMO (NoccMO, NAelectron, NBelectron)
      write(UNIout,'(a,i4,a,i4/)')'Charge= ',MOLECULE%charge,', Number of electrons= ',Nelectrons
      if(Multiplicity.gt.1)call SET_ROHF_defaults
!
! Now define the default (STO-3G) basis set
      if(.not.associated(Basis))then
      call GET_basis_set ('STO-3G', Current_basis, .false.)
      Basis=>Current_basis
      Nbasis=Basis%Nbasis
      MATlen=Nbasis*(Nbasis+1)/2
      end if
!
! Must intialize fragments (single fragment)
!     call BLD_Fragments (1)   ! Comment for Mark
!
! Deallocate work arrays:
      deallocate (XYZC_work, MOL_atomsW, MOLEID_work, ZmatrixW%atoms, ZmatrixW%define, STAT=status)
      OPT_parameters%name=CURRENT_coordinates
!
! End of routine MENU_molecule
      call PRG_manager ('exit', 'MENU_molecule', 'UTILITY')
      return

CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine Zmatrix_store (value, zmname)
!***********************************************************************
!     Date last modified: January 18, 1991                             *
!     Author:                                                          *
!     Description: Store the Z-matrix value                            *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      character*(*) zmname
      double precision value
!
! Local scalars:
      integer first,last,Iatom,Ilast1,Ilast2,IPMC1,IPMC2,ISIGN
      logical done
!
! Begin:
      call PRG_manager ('enter', 'Zmatrix_store', 'UTILITY')
!
      done=.false.
!
! First check if have a bond length
      do Iatom=2,NatomsW
      if(zmname.eq.ZmatrixW%bond(Iatom)%label)then
        ZmatrixW%bond(Iatom)%length=value
        done=.true.
      end if
      end do ! Iatom

      if(done)then
        call PRG_manager ('exit', 'Zmatrix_store', 'UTILITY')
        return
      end if
!
! Check if have an angle
      do Iatom=3,NatomsW
      if(zmname.eq.ZmatrixW%angle(Iatom)%label)then
        ZmatrixW%angle(Iatom)%value=value
        done=.true.
      end if
      end do ! Iatom

      if(done)then
        call PRG_manager ('exit', 'Zmatrix_store', 'UTILITY')
        return
      end if
!
! Check if have a torsion
      do Iatom=4,NatomsW
      if(index(ZmatrixW%torsion(Iatom)%label,'-').gt.0)then
        ISIGN=-1
        IPMC2=index(ZmatrixW%torsion(Iatom)%label,'-')+1
      else if(index(ZmatrixW%torsion(Iatom)%label,'+').gt.0)then
        ISIGN=+1
        IPMC2=index(ZmatrixW%torsion(Iatom)%label,'+')+1
      else
        ISIGN=+1
        IPMC2=1
      end if
!
! IMPROVE!!!
      call STR_size (ZmatrixW%torsion(Iatom)%label, first, last)
      if(zmname(1:).eq.ZmatrixW%torsion(Iatom)%label(2:last))then ! (IPMC2:last))then
        ZmatrixW%torsion(Iatom)%value=ISIGN*value
        done=.true.
      end if
      end do !Iatom

      if(done)then
        call PRG_manager ('exit', 'Zmatrix_store', 'UTILITY')
        return
      end if
!
      write(UNIout,'(a)')'ERROR> Zmatrix_store:'
      write(UNIout,'(2a)')'illegal Z-MATRIX parameter ',zmname(1:)
      call PRG_stop ('Illegal Z-MATRIX parameter')
!
! End of routine Zmatrix_store
      end subroutine Zmatrix_store
      subroutine ANGLE_store (angle, &
                              lenang)
!***********************************************************************
!     Date last modified: January 18, 1991                             *
!     Author:                                                          *
!     Description: Store the angle.                                    *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer lenang
      character*(*) angle
!
! Local scalars:
      integer first,length,lenstr
      double precision :: TETRA,value
!
! Begin:
      call PRG_manager ('enter', 'ANGLE_store', 'UTILITY')
!
      TETRA=TWO*DATAN(DSQRT(TWO))/Deg_to_Radian
      if(NatomsW.LT.3)then
        ZmatrixW%angle(NatomsW)%value=ZERO
        ZmatrixW%angle(NatomsW)%label=' '
        call PRG_manager ('exit', 'ANGLE_store', 'UTILITY')
        return
      end if
!
      NMCOORw=NMCOORw+1
!
      call STR_size (angle, first, lenstr)
      call CNV_SLtoU (angle) ! Jan. 18, 2006
      ZmatrixW%angle(NatomsW)%label=angle(first:lenstr)
      if(CHK_string(angle,'+-0123456789.'))then
        call CNV_StoD (angle, value)
        ZmatrixW%angle(NatomsW)%value=value
        NZM_fixedw=NZM_fixedw+1
      else if(index(angle, 'TETRA').ne.0)then ! Oct. 14, 2009
        ZmatrixW%angle(NatomsW)%value=TETRA
        NZM_fixedw=NZM_fixedw+1
      else
        length=lenstr-first+1
        if(length.gt.MAX_label)then
          write(UNIout,'(/a)')'ERROR> ANGLE_store:'
          write(UNIout,'(a,i8)')'Angle label too long, max= ',MAX_label
          write(UNIout,'(2a)')'Angle label = ',angle(first:lenstr)
          call PRG_stop ('ANGLE_store>  Angle label too long')
        end if
        if(length.le.0)then
          write(UNIout,'(/a)')'ERROR> ANGLE_store:'
          write(UNIout,'(a)')'Angle label has zero length'
          call PRG_stop ('ANGLE_store>  Angle label has zero length')
        end if
        NZM_names=NZM_names+1
      end if
!
! End of routine ANGLE_store
      call PRG_manager ('exit', 'ANGLE_store', 'UTILITY')
      return
      end subroutine ANGLE_store
      subroutine BOND_store (bond, &
                            lenbon)
!***********************************************************************
!     Date last modified: January 18, 1991                             *
!     Author:                                                          *
!     Description: Store a bond.                                       *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer lenbon
      character*(*) bond
!
! Local scalars:
      integer first,i,length,lenstr
      double precision value
!
! Begin:
      call PRG_manager ('enter', 'BOND_store', 'UTILITY')
!
      if(NatomsW.eq.1)then
        ZmatrixW%bond(NatomsW)%length=ZERO
        ZmatrixW%bond(NatomsW)%label=' '
        call PRG_manager ('exit', 'BOND_store', 'UTILITY')
        return
      end if
!
      NMCOORw=NMCOORw+1
!
      call STR_size (bond, first, lenstr)
      call CNV_SLtoU (bond) ! Jan. 18, 2006
      ZmatrixW%bond(NatomsW)%label=bond(first:lenstr)
      if(CHK_string(bond,'+-0123456789.'))then
        call CNV_StoD (bond, value)
        ZmatrixW%bond(NatomsW)%length=value
        NZM_fixedw=NZM_fixedw+1
      else
        length=lenstr-first+1
        if(length.gt.MAX_label)then
          write(UNIout,'(/a)')'ERROR> BOND_store:'
          write(UNIout,'(a)')'Bond label too long, max= ',MAX_label
          write(UNIout,'(2a)')' Bond label = ',bond(first:lenstr)
          call PRG_stop ('BOND_store>  Bond label too long')
        end if
        if(length.le.0)then
          write(UNIout,'(/a)')'ERROR> BOND_store:'
          write(UNIout,'(a)')'Bond label has zero length'
          call PRG_stop ('BOND_store>  Bond label has zero length')
        end if
        NZM_names=NZM_names+1
      end if  ! if(CHK_string(bond)
!
! End of routine BOND_store
      call PRG_manager ('exit', 'BOND_store', 'UTILITY')
      return
      end subroutine BOND_store
      subroutine TORSION_store (torsion)
!***********************************************************************
!     Date last modified: January 18, 1991                             *
!     Author:                                                          *
!     Description: Store a torsion.                                    *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      character*(*) torsion
!
! Local scalars:
      integer first,length,lenstr
      double precision :: TETRA,value
!
! Begin:
      call PRG_manager ('enter', 'TORSION_store', 'UTILITY')
!
      TETRA=TWO*DATAN(DSQRT(TWO))/Deg_to_Radian
      ZmatrixW%atoms(NatomsW)%atom4=0
      if(NatomsW.lt.4)then
        ZmatrixW%torsion(NatomsW)%value=ZERO
        ZmatrixW%torsion(NatomsW)%label='        '
        call PRG_manager ('exit', 'TORSION_store', 'UTILITY')
        return
      end if
!
      call STR_size (torsion, first, lenstr)
      call CNV_SLtoU (torsion) ! Jan. 18, 2006
      if(index(torsion,'+').eq.0.and.index(torsion,'-').eq.0)then
       ZmatrixW%torsion(NatomsW)%label=' '//torsion(first:lenstr)
      else
       ZmatrixW%torsion(NatomsW)%label=torsion(1:lenstr)
      end if
      NMCOORw=NMCOORw+1
!
      if(CHK_string(torsion,'+-0123456789.'))then
        call CNV_StoD (torsion, value)
        ZmatrixW%torsion(NatomsW)%value=value
        NZM_fixedw=NZM_fixedw+1
      else if(index(torsion, 'TETRA').ne.0)then ! Oct. 14, 2009
        ZmatrixW%torsion(NatomsW)%value=TETRA
        NZM_fixedw=NZM_fixedw+1
      else
        length=lenstr-first+1
        if(length.gt.MAX_label)then
          write(UNIout,'(a)')'ERROR> TORSION_store: Torsion label too long, max= ',MAX_label
          write(UNIout,'(2a)')' Torsion label = ',torsion(first:lenstr)
          call PRG_stop ('TORSION_store> Torsion label too long')
        end if
        if(length.le.0)then
          write(UNIout,'(a)')'ERROR> TORSION_store: Torsion label has zero length string'
          call PRG_stop ('TORSION_store> Torsion label has zero length string')
        end if
        NZM_names=NZM_names+1
      end if ! CHK_string

!
! End of routine TORSION_store
      call PRG_manager ('exit', 'TORSION_store', 'UTILITY')
      return
      end subroutine TORSION_store
      subroutine EULER_store (euler)
!***********************************************************************
!     Date last modified: January 18, 1991                             *
!     Author:                                                          *
!     Description: Store a euler angle.                                *
!***********************************************************************
! Modules:

      implicit none
!
! Local scalars:
      integer first,Frstpos,last,length,Msignpos,Psignpos
      double precision value
!
! Input scalars:
      character*(*) euler
!
! Begin:
      call PRG_manager ('enter', 'EULER_store', 'UTILITY')
!
      ZmatrixW%atoms(NatomsW)%atom4=0
      if(NatomsW.lt.4)then
        ZmatrixW%torsion(NatomsW)%value=ZERO
        ZmatrixW%torsion(NatomsW)%label=' '
        call PRG_manager ('exit', 'EULER_store', 'UTILITY')
        return
      end if

      ZmatrixW%atoms(NatomsW)%atom4=+1
!
! For Euler angles remove + or - sign from the label
      Msignpos=index(euler,'-')
      Psignpos=index(euler,'+')
      Frstpos=1
      if(Msignpos.ne.0)then
        ZmatrixW%atoms(NatomsW)%atom4=-1
        euler(Msignpos:Msignpos)=' '
        Frstpos=Msignpos+1
      else if(Psignpos.ne.0)then
        euler(Psignpos:Psignpos)=' '
        Frstpos=Psignpos+1
      end if
!
      NMCOORw=NMCOORw+1
      call STR_size (euler, first, last)
      call CNV_SLtoU (euler) ! Jan. 18, 2006
      ZmatrixW%torsion(NatomsW)%label=' '//euler(Frstpos:last)
      if(CHK_string(euler,'+-0123456789.'))then
        call CNV_StoD (euler, value)
        ZmatrixW%torsion(NatomsW)%value=DABS(value)
        NZM_fixedw=NZM_fixedw+1
      else
        length=last-frstpos+1
        if(length.gt.MAX_label)then
          write(UNIout,'(a)')'ERROR> EULER_store: Euler label too long, max= ',MAX_label
          write(UNIout,'(2a)')' Euler label = ',Euler(frstpos:last)
          call PRG_stop ('EULER_store> Euler label too long')
        end if
        NZM_names=NZM_names+1
      end if ! CHK_string
!
! End of routine EULER_store
      call PRG_manager ('exit', 'EULER_store', 'UTILITY')
      return
      end subroutine EULER_store
      subroutine ATOM_store (atom)
!***********************************************************************
!     Date last modified: January 18, 1991                             *
!     Author:                                                          *
!     Description:                                                     *
!***********************************************************************
! Modules:
      USE type_elements

      implicit none
!
! Input scalar:
      character*(*) atom
!
! Local scalars:
      integer ATMNUM
!
! Begin:
      call PRG_manager ('enter', 'ATOM_store', 'UTILITY')
!
      XYZC_work(NatomsW)%element=atom(1:4)
!
      if(CHK_string(atom,'+-0123456789'))then
        call CNV_StoI (atom, ATMNUM)
        XYZC_work(NatomsW)%Atomic_number=ATMNUM
        XYZC_work(NatomsW)%element=Element_symbols(ATMNUM)
      else
        call GET_atomic_number (atom, ATMNUM)
        XYZC_work(NatomsW)%element=atom(1:4)
        XYZC_work(NatomsW)%Atomic_number=ATMNUM
      end if
!
! End of routine ATOM_store
      call PRG_manager ('exit', 'ATOM_store', 'UTILITY')
      return
      end subroutine ATOM_store
      subroutine ATOM_list_store (atmlst, &
                         nbbyte, &
                         nlist)
!***********************************************************************
!     Date last modified: January 18, 1991                             *
!     Author:                                                          *
!     Description:                                                     *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer nbbyte,nlist
!
! Input arrays:
      character*(*) atmlst(4)
!
! Local arrays:
      integer atmnum(3)
!
! Local scalars:
      integer I,J
!
! Begin:
      call PRG_manager ('enter', 'ATOM_list_store', 'UTILITY')
!
      ATMNUM(1:3)=0
!
      do I=1,nlist
      if(CHK_string(atmlst(I),'+-0123456789'))then
        call CNV_StoI (atmlst(I), ATMNUM(I))
      else
        do j=1,NatomsW
        if(XYZC_work(J)%element(1:4).eq.atmlst(I))then
          ATMNUM(I)=j
        end if
        end do ! J
      end if
      if(ATMNUM(I).le.0.or.ATMNUM(I).ge.NatomsW)then
        write(UNIout,'(1x,a,a)')'ERROR: ATOM_list_store> illegal atom in list (FROM=(?,?,?))',atmlst(I)
      end if
      end do ! I
!
      ZmatrixW%atoms(NatomsW)%atom1=ATMNUM(1)
      ZmatrixW%atoms(NatomsW)%atom2=ATMNUM(2)
      ZmatrixW%atoms(NatomsW)%atom3=ATMNUM(3)
!
! End of routine ATOM_list_store
      call PRG_manager ('exit', 'ATOM_list_store', 'UTILITY')
      return
      end subroutine ATOM_list_store
      subroutine CARTESIAN_store (ccval, IXYZ)
!***********************************************************************
!     Date last modified: January 18, 1991                             *
!     Author:                                                          *
!     Description:                                                     *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer IXYZ
      double precision ccval
!
! Begin:
      call PRG_manager ('enter', 'CARTESIAN_store', 'UTILITY')
!
      if(ZmatrixW%units(1:3).EQ.'ANG')then
        ccval=ccval*Angstrom_to_Bohr
      end if

      select case (IXYZ)
      case (1)
        XYZC_work(NatomsW)%X=ccval
      case (2)
        XYZC_work(NatomsW)%Y=ccval
      case (3)
        XYZC_work(NatomsW)%Z=ccval
      end select
!
! End of routine CARTESIAN_store
      call PRG_manager ('exit', 'CARTESIAN_store', 'UTILITY')
      return
      end subroutine CARTESIAN_store
      subroutine Zmatrix_input
!***********************************************************************
!     Date last modified: March 16, 1998                               *
!     Author: R. A. Poirier                                            *
!     Description: Free Format Z Matrix input routine.                 *
!     Local variables:                                                 *
!     TEMPLINE   - input line                                          *
!***********************************************************************
! Modules:
      USE atommass

      implicit none
!
! Local scalars:
      integer CHAR1,CHAR2,I,INTEGR,IS1,IS2,IZ2,IZ3,IZ4,J,K,lelem,LENSTR,LENLINE
      double precision REALNO,TETRA,VALUE
      logical :: FOUND,Lmatch
      character*1 LINE(MAX_length),Z5
      character(len=MAX_length) atom,Cisotope,STRING,TEMPLINE
      character(len=MAX_length) Position
!
! Local parameters:
      character*1 CTAB
!
! Begin:
      call PRG_manager ('enter', 'Zmatrix_input', 'UTILITY')
!
      Position(1:MAX_length)=' '
      CTAB='	'
! Initialization
      NatomsW=0
      TETRA=TWO*DATAN(DSQRT(TWO))/Deg_to_Radian
      if(Lecho)write(UNIout,*)' FREE FORMAT Z MATRIX:'
!
 read:do ! loop until an end is found
      IS1=0
      IS2=0
      READ(UNITIN,'(a)',END=999)TEMPLINE ! Get next line
      if(Lecho)write(UNIout,'(1x,a,a80)')'input: ',TEMPLINE
      LENLINE=len_trim(TEMPLINE)
      if(LENLINE.eq.0.or.TEMPLINE(1:1).eq.'!')cycle   ! Blank line or comment
!
! Allow for comments on input line - NOTE the first character may be a !
      if(index(TEMPLINE,'!').ne.0)then
        LENLINE=index(TEMPLINE,'!')-1
      end if

      if(index(TEMPLINE,'{').ne.0)then
        FOUND=.false.
        do WHILE (.not.FOUND) ! Look for "}"
          READ(UNITIN,'(a)')TEMPLINE
          if(Lecho)write(UNIout,'(1x,a,a80)')'input: ',TEMPLINE
          if(index(TEMPLINE,'}').ne.0)FOUND=.true.
        end do ! while

        if(FOUND)then
          cycle
        else
          write(UNIout,'(/a)')'ERROR: MENU_molecule> missing }'
          call PRG_stop ('Missing }')
        end if
      end if

! Check for tabs
      do I=1,LENLINE
        if(TEMPLINE(I:I).eq.CTAB)TEMPLINE(I:I)=' '
        LINE(I)=TEMPLINE(I:I)
      end do
!
      call CNV_SLtoU (TEMPLINE(1:LENLINE)) ! New Jan 16, 2006
!
! Check for an end
      if(index(TEMPLINE,'END').ne.0)exit
!
! Get Z1
      call GET_WORD_MOL (LINE, IS1, IS2, STRING, LENSTR, LENLINE)
      NatomsW=NatomsW+1

      if(NatomsW.GT.MAX_Qatoms)then
        write(UNIout,'(/a)')'ERROR: Zmatrix_input>'
        write(UNIout,'(/a)')'Too many atoms: must increase MAX_Qatoms'
        call PRG_stop ('Zmatrix input error')
      end if
!
! Look for isotope specification (n), i.e., H1(2)
      char1=index(string,'(')
      char2=index(string,')')
      if(char1.gt.0)lenstr=char1-1
!
! String found - Save it in IELEM.
      atom=' '
      ATOM(1:lenstr)=STRING(1:lenstr)
!
! Get the Atomic Number -
      call ATOM_store (ATOM)

! Deal with the isotope (n)
      if(char1.ne.0)then ! found a (
        if(char2.eq.0)then
          write(UNIout,*)'ERROR> Zmatrix_input: missing )'
          call PRG_stop ('Zmatrix_input> missing )')
        end if
        Cisotope(1:)=string(char1+1:char2-1)
        call CNV_StoI (Cisotope, MOL_atomsW(NatomsW)%isotope)
        if(.not.checkisotope (XYZC_work(NatomsW)%Atomic_number, MOL_atomsW(NatomsW)%isotope))then
          write(UNIout,*)'ERROR: Zmatrix_input> illegal isotope '
          call PRG_stop ('Zmatrix_input> illegal isotope')
        end if
      else
        if(.not.set_def_isotope (XYZC_work(NatomsW)%Atomic_number, MOL_atomsW(NatomsW)%isotope))then
          write(UNIout,'(/a)')'ERROR: Zmatrix_input>'
          write(UNIout,'(a)')'isotope error'
          call PRG_stop ('Zmatrix input error')
        end if
      end if ! char1

      if(NatomsW.EQ.1)cycle
!
! Z2 String found - Match it against IELEM.
      call GET_WORD_MOL (LINE, IS1, IS2, STRING, LENSTR, LENLINE)
      IZ2=IS1
      if(LENSTR.EQ.0)then
        write(UNIout,*)'ERROR: GET_WORD_MOL> no Z2 atom'
      end if

! Check if integer (actual atom number)
      if(CHK_string(string,'+-0123456789'))then
        call CNV_StoI (string, INTEGR) ! Should check value
      else
      K=NatomsW-1
      Lmatch=.false.
      do I=1,K
        lelem=len_trim(XYZC_work(I)%element)
        if(lelem.ne.lenstr)cycle ! Not a match
        if(XYZC_work(I)%element(1:lelem).eq.STRING(1:lelem))then
          Lmatch=.true.
          INTEGR=I
          exit
        end if
      end do ! I
!
      if(.not.Lmatch)then
        write(UNIout,'(/a)')'ERROR: Zmatrix_input> Z2'
        write(UNIout,'(a)')TEMPLINE(1:LENLINE)
        write(UNIout,'(3a)')'Atom "',STRING(1:lenstr),'" does not match previously defined atom'
        call PRG_stop ('Z-matrix input error')
      end if
      end if
!
      ZmatrixW%atoms(NatomsW)%atom1=INTEGR
!
! Get a bond length.
      call GET_WORD_MOL (LINE, IS1, IS2, STRING, LENSTR, LENLINE)
      if(LENSTR.le.0)then
        write(UNIout,*)'ERROR: GET_WORD_MOL> No bond'
      end if
      call BOND_store (STRING, LENSTR)
      if(NatomsW.EQ.2)cycle
!
! Get Z3 String found - Match it against IELEM.
      call GET_WORD_MOL (LINE, IS1, IS2, STRING, LENSTR, LENLINE)
      IZ3=IS1
      if(LENSTR.EQ.0)then
        write(UNIout,*)'ERROR: GET_WORD_MOL> no Z3 atom'
      end if

! Check if integer (actual atom number)
      if(CHK_string(string,'+-0123456789'))then
        call CNV_StoI (string, INTEGR) ! Should check value
      else
      K=NatomsW-1
      Lmatch=.false.
      do I=1,K
        lelem=len_trim(XYZC_work(I)%element)
        if(lelem.ne.lenstr)cycle ! Not a match
        if(XYZC_work(I)%element(1:lelem).eq.STRING(1:lelem))then
          Lmatch=.true.
          INTEGR=I
          exit
        end if
      end do ! I
!
      if(.not.Lmatch)then
        write(UNIout,'(/a)')'ERROR: Zmatrix_input> Z3'
        write(UNIout,'(a)')TEMPLINE(1:LENLINE)
        write(UNIout,'(3a)')'Atom "',STRING(1:lenstr),'" does not match previously defined atom'
        call PRG_stop ('Z-matrix input error')
      end if
      end if

      ZmatrixW%atoms(NatomsW)%atom2=INTEGR
      if(ZmatrixW%atoms(NatomsW)%atom1.eq.ZmatrixW%atoms(NatomsW)%atom2)then
        write(UNIout,'(/a)')'ERROR: Zmatrix_input>'
        write(UNIout,'(a)')TEMPLINE(1:LENLINE)
        Position(IZ3:IZ3)='^'
        Position(IZ2:IZ2)='^'
        write(UNIout,'(a)')Position(1:LENLINE)
        Position(IZ3:IZ3)=' '
        Position(IZ2:IZ2)=' '
        write(UNIout,'(4a)')'Atom (Z3)"',STRING(1:lenstr),'"',' same as Z2'
        call PRG_stop ('Z-matrix input error')
      end if
!
! Get a bond angle.
      call GET_WORD_MOL (LINE, IS1, IS2, STRING, LENSTR, LENLINE)
      if(LENSTR.EQ.0)then
        write(UNIout,*)'ERROR: GET_WORD_MOL> no angle'
      end if
      call ANGLE_store (STRING, LENSTR)
!
      if(NatomsW.eq.3)cycle

! Get Z4
      call GET_WORD_MOL (LINE, IS1, IS2, STRING, LENSTR, LENLINE)
      IZ4=IS1
      if(LENSTR.EQ.0)then
        write(UNIout,*)'ERROR: GET_WORD_MOL> no Z4 atom'
      end if

! Check if integer (actual atom number)
      if(CHK_string(string,'+-0123456789'))then
        call CNV_StoI (string, INTEGR) ! Should check value
      else
        K=NatomsW-1
        Lmatch=.false.
        do I=1,K
          lelem=len_trim(XYZC_work(I)%element)
          if(lelem.ne.lenstr)cycle ! Not a match
          if(XYZC_work(I)%element(1:lelem).eq.STRING(1:lelem))then
            Lmatch=.true.
            INTEGR=I
            exit
          end if
        end do ! I
!
      if(.not.Lmatch)then
        write(UNIout,'(/a)')'ERROR: Zmatrix_input> Z4'
        write(UNIout,'(a)')TEMPLINE(1:LENLINE)
        write(UNIout,'(3a)')'Atom "',STRING(1:lenstr),'" does not match previously defined atom'
        call PRG_stop ('Z-matrix input error')
        return
      end if
      end if

      ZmatrixW%atoms(NatomsW)%atom3=INTEGR
      if(ZmatrixW%atoms(NatomsW)%atom1.eq.ZmatrixW%atoms(NatomsW)%atom3)then
        write(UNIout,'(/a)')'ERROR: Zmatrix_input>'
        write(UNIout,'(a)')TEMPLINE(1:LENLINE)
        Position(IZ4:IZ4)='^'
        Position(IZ2:IZ2)='^'
        write(UNIout,'(a)')Position(1:LENLINE)
        Position(IZ4:IZ4)=' '
        Position(IZ2:IZ2)=' '
        write(UNIout,'(4a)')'Atom(Z4) "',STRING(1:lenstr),'"',' same as Z2'
        call PRG_stop ('Z-matrix input error')
      end if
      if(ZmatrixW%atoms(NatomsW)%atom2.eq.ZmatrixW%atoms(NatomsW)%atom3)then
        write(UNIout,'(/a)')'ERROR: Zmatrix_input>'
        write(UNIout,'(a)')TEMPLINE(1:LENLINE)
        Position(IZ4:IZ4)='^'
        Position(IZ3:IZ3)='^'
        write(UNIout,'(a)')Position(1:LENLINE)
        Position(IZ4:IZ4)=' '
        Position(IZ3:IZ3)=' '
        write(UNIout,'(4a)')'Atom(Z4) "',STRING(1:lenstr),'"',' same as Z3'
        call PRG_stop ('Z-matrix input error')
      end if
!
! Get a torsion.
      call GET_WORD_MOL (LINE, IS1, IS2, STRING, LENSTR, LENLINE)
      if(LENSTR.EQ.0)then
        write(UNIout,*)'ERROR: GET_WORD_MOL> no torsion'
      end if
! Get Z5
      call GET_WORD_MOL (LINE, IS1, IS2, Z5, LENSTR, LENLINE)
      if(Z5.EQ.' '.or.Z5.EQ.'0')then
        call TORSION_store (STRING)
      else ! Euler angle
        ZmatrixW%atoms(NatomsW)%atom4=+1
        if(CHK_string(Z5,'-'))ZmatrixW%atoms(NatomsW)%atom4=-1
        NMCOORw=NMCOORw+1
        if(CHK_string(STRING,'+-0123456789.'))then
          call CNV_StoD (STRING, VALUE)
          ZmatrixW%torsion(NatomsW)%value=VALUE
        else if(index(string, 'TETRA').ne.0)then ! Oct. 14, 2009
          ZmatrixW%torsion(NatomsW)%value=TETRA
          ZmatrixW%torsion(NatomsW)%label=' '//STRING(1:5)
        else
          ZmatrixW%torsion(NatomsW)%label=' '//STRING
        end if
      end if
 
      if(NatomsW.ge.4)cycle
  999 write(UNIout,'(a)')'Zmatrix_input error> Missing "END"'
      stop 'Zmatrix_input error> Missing "END"'
 
      end do read

      if(NatomsW.eq.0)then
        write(UNIout,'(/a)')'ERROR: Zmatrix_input>'
        write(UNIout,'(a)')'No atoms defined!'
        call PRG_stop ('Zmatrix input error')
        return
      end if
!
! End of routine Zmatrix_input
      call PRG_manager ('exit', 'Zmatrix_input', 'UTILITY')
      return
      end subroutine Zmatrix_input
      subroutine ADD_atom
!***********************************************************************
!     Date last modified: October 13, 1994                             *
!     Author:                                                          *
!     Description: Add an atom to the molecule.                        *
!***********************************************************************
! Modules:
      USE type_elements

      implicit none
!
! Local scalars:
      integer lenang,lenatm,lenbon,lenmol,lentor,nbbyte,nlist
      double precision coord,mass
      logical done
      character(len=MAX_length) angle,atom,bond,molcul,torson
      character*4 atmlst(4)
!
! Begin:
      call PRG_manager ('enter', 'ADD_atom', 'UTILITY')
!
      NatomsW=NatomsW+1

      if(NatomsW.GT.MAX_Qatoms)then
        write(UNIout,*)'ERROR_ADD_atom> Too many atoms: must increase MAX_Qatoms'
        call PRG_stop ('ADD_atom> Too many atoms: must increase MAX_Qatoms')
      end if
!
! Menu:
      done=.false.
      do while (.not.done)
      call NEW_token (' ADDatom:')
!
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      ' Command ADDatom :', &
      '   Purpose: Define an atom', &
      '   Syntax :', &
      '   ANgle = <string or real>: examples: C1C2C3 or 120.0', &
      '   ATom = <string>: examples: C1, H2, He, ...', &
      '   BOnd = <string or real>: examples: C1C2 or 1.4567', &
      '   FRom = <string list>: examples: (C1[,C2,C3])', &
!     .'   MAss = <real>: examples: 12.0000', &
      '   MOLecule = <string>: examples: A, B', &
      '   TOrsion = <string or real>: examples: C1C2C3C4 or 180.0000', &
      '   EUler = <string or real>: examples: C1C2C3C4 or 109.0000', &
      '   X-coordinate = <real>: examples: X=0.98453', &
      '   Y-coordinate = <real>: examples: Y=0.00000', &
      '   Z-coordinate = <real>: examples: Z=0.98453', &
      '   end'
!
! ANgle
      else if(token(1:2).EQ.'AN')then
        call GET_value (angle, lenang)
        call ANGLE_store (angle, lenang)
!ATom
      else if(token(1:2).EQ.'AT')then
        call GET_value (atom, lenatm)
        call ATOM_store (atom)
! BOnd
      else if(token(1:2).EQ.'BO')then
        call GET_value (bond, lenbon)
        call BOND_store (bond, lenbon)
!EUler
      else if(token(1:2).EQ.'EU')then
        call GET_value (torson, lentor)
        call EULER_store (torson)
!FRom
      else if(token(1:2).EQ.'FR')then
         nlist=4
         nbbyte=4
         call GET_Clist (atmlst, nbbyte, 4, nlist)
         call ATOM_list_store (atmlst, nbbyte, nlist)
!MAss
!      else if(token(1:2).EQ.'MA')then
!              call GET_value (mass)
!              call masbld (atmmas, mass)
!              write(UNIout,*)'ERROR: ADD_atom> masbld not available'
!              stop
! MOLecule
      else if(token(1:3).EQ.'MOL')then
        call GET_value (molcul, lenmol)
        MOLEID_work(NatomsW)(1:8)=molcul(1:8)

! TORsion
      else if(token(1:3).EQ.'TOR')then
        call GET_value (torson, lentor)
        call TORSION_store (torson)

! X-coordinate
      else if(token(1:1).EQ.'X')then
        MOLINPw(1:9)='CARTESIAN'
        call GET_value (coord)
        call CARTESIAN_store (coord, 1)

! Y-coordinate
      else if(token(1:1).EQ.'Y')then
        MOLINPw(1:9)='CARTESIAN'
        call GET_value (coord)
        call CARTESIAN_store (coord, 2)

! Z-coordinate
      else if(token(1:1).EQ.'Z')then
        MOLINPw(1:9)='CARTESIAN'
        call GET_value (coord)
        call CARTESIAN_store (coord, 3)

      else
        call MENU_end (done)
      end if
!
      end do ! .not.done
!
! End of routine ADD_atom
      call PRG_manager ('exit', 'ADD_atom', 'UTILITY')
      return
      end subroutine ADD_atom
      subroutine SET_define
!***********************************************************************
!     Date last modified: January 18, 1991                             *
!     Author:                                                          *
!     Description: Define the Z-matrix variables                       *
!***********************************************************************
! Modules:

      implicit none
!
! Local scalars:
      integer first,Iatom,last,lenbon,lenang,lenstr
      double precision TETRA,value
      character(len=MAX_length) string
      character*16 zmname
      logical done,Logical
!
! Begin:
      call PRG_manager ('enter', 'SET_define', 'UTILITY')
!
      TETRA=TWO*DATAN(DSQRT(TWO))/Deg_to_Radian
!
      NZMVARw=0
!
! Menu:
      done=.false.
      do while (.not.done)
      call NEW_token (' DEFine:')
!
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      ' Command DEFine :', &
      '   Purpose: Assign values to the Z-matrix variables', &
      '   Syntax :', &
      '   OPT = <logical>: examples: OPT=true', &
      '   <string> = <real>: examples: C1C2C3=120.0', &
      '   end'
!
      else if(token(1:3).eq.'OPT')then
        call GET_value (Logical)
        if(NZMVARw.gt.0)then
          ZmatrixW%define(NZMVARw)%opt=Logical
        else
          write(UNIout,*)'ERROR: SET_define> OPT must be associated with a parameter'
          call PRG_stop ('SET_define> error: OPT')
        end if ! NZMVARw

      else if(token(1:3).ne.'END')then
        NZMVARw=NZMVARw+1
        zmname=token(1:toklen)
        call STR_size (zmname, first, last)
        ZmatrixW%define(NZMVARw)%name=zmname(first:last)
        call GET_value (string, lenstr)
        if(CHK_string(string,'+-0123456789.'))then
          call CNV_StoD (string, value)
        else if(index(string, 'TETRA').ne.0)then ! Check this
          value=TETRA
        end if
        call Zmatrix_store (value, zmname(first:last))
        ZmatrixW%define(NZMVARw)%value=value
      else

        call MENU_end (done)
      end if
!
      end do ! .not.done
!
! End of routine SET_define
      call PRG_manager ('exit', 'SET_define', 'UTILITY')
      return
      end subroutine SET_define
      subroutine load_molecule
!**********************************************************************
!     Date last modified: June 10, 1999                               *
!     Author: R.A. Poirier                                            *
!     Description: Copy the molecule from the work arrays             *
!**********************************************************************
! Modules:

      implicit none
!
! Local scalars:
      integer status
      character(len=MAX_formula) formula
!
! Begin:
      call PRG_manager ('enter', 'LOAD_MOLECULE', 'UTILITY')

!     if(Natoms.GT.0)then ! Previous molecule exists
!       deallocate (CARTESIAN, MOL_atoms, STAT=status)
!       if(status.ne.0)then
!         write(UNIout,*)'MENU_molecule> deallocation of IAN, ... failed'
!       end if
!     end if

!      if(NZMVAR.gt.0)then ! Previous Z-matrix exists
      if(associated(Zmatrix%define))then ! Previous Z-matrix exists
        deallocate (Zmatrix%atoms, Zmatrix%bond, Zmatrix%angle, Zmatrix%torsion, Zmatrix%define, STAT=status)
        if(status.ne.0)then
          write(UNIout,*)'MENU_molecule> deallocation of Zmatrix%, ... failed'
        end if
      end if

      N_molecules=N_molecules+1
      Natoms=NatomsW
      NZM_fixed=NZM_fixedw
      NMCOOR=NMCOORw
      NZMVAR=NZMVARw
      MOLINP=MOLINPw
      MOL_TITLE=MOL_TITLEw
      NICOOR=NMCOOR
!
! Allocate arrays and copy data
      write(UNIout,'(a14,i2)')'N_molecules: ',N_molecules
      if(N_molecules.eq.1)then
        if(allocated (CartesianA))deallocate (CartesianA, MOL_atomsA, STAT=status)
        allocate (CartesianA(1:Natoms), MOL_atomsA(1:Natoms), STAT=status)
        CARTESIAN=>CartesianA
        molecule=>moleculeA
        MOL_atoms=>MOL_atomsA
        MOL_NAME='A'
        MOL_TitleA=MOL_TITLE
      else if(N_molecules.eq.2)then
        if(allocated (CartesianB))deallocate (CartesianB, MOL_atomsB, STAT=status)
        allocate (CartesianB(1:Natoms), MOL_atomsB(1:Natoms), STAT=status)
        CARTESIAN=>CartesianB
        molecule=>moleculeB
        MOL_atoms=>MOL_atomsB
        MOL_NAME='B'
        MOL_TitleB=MOL_TITLE
      end if

      CARTESIAN(1:Natoms)%Atomic_number=XYZC_work(1:Natoms)%Atomic_number
      CARTESIAN(1:Natoms)%element=XYZC_work(1:Natoms)%element
!
      molecule%Natoms=NatomsW
      MOL_atoms(1:Natoms)%Atomic_number=CARTESIAN(1:Natoms)%Atomic_number
      MOL_atoms(1:Natoms)%name=CARTESIAN(1:Natoms)%element
      MOL_atoms(1:Natoms)%isotope=MOL_atomsW(1:Natoms)%isotope
!
      if(MOLINP(1:5).eq.'CARTE')then
        CARTESIAN(1:Natoms)%X=XYZC_work(1:Natoms)%X
        CARTESIAN(1:Natoms)%Y=XYZC_work(1:Natoms)%Y
        CARTESIAN(1:Natoms)%Z=XYZC_work(1:Natoms)%Z
        CURRENT_coordinates='XYZ'
      else if(Natoms.eq.1)then ! Single atom (no Z-matrix)
        CARTESIAN(Natoms)%X=ZERO
        CARTESIAN(Natoms)%Y=ZERO
        CARTESIAN(Natoms)%Z=ZERO
      else
!
! What do we do if nzmvar=0?
        CURRENT_coordinates='Z-MATRIX'
        allocate (Zmatrix%atoms(Natoms), Zmatrix%bond(Natoms), Zmatrix%angle(Natoms), &
                  Zmatrix%torsion(Natoms), STAT=status)
          if(status.ne.0)then
            write(UNIout,'(a)')'ERROR> MENU_molecule: allocation of Zmatrix%, ... failed'
          end if
        if(NZMVAR.ne.0)then ! New Z-matrix
           allocate (Zmatrix%define(NZMVAR), STAT=status)
          if(status.ne.0)then
            write(UNIout,'(a)')'ERROR> MENU_molecule: allocation of Zmatrix%, ... failed'
          end if
          Zmatrix%define(1:NZMVAR)%name=ZmatrixW%define(1:NZMVAR)%name
          Zmatrix%define(1:NZMVAR)%value=ZmatrixW%define(1:NZMVAR)%value
          Zmatrix%define(1:NZMVAR)%opt=ZmatrixW%define(1:NZMVAR)%opt
        else if(NZM_names.eq.0.and.NZM_fixed.eq.(3*Natoms-6))then
          MOLINP='MATRIX' ! Use "Z-matrix"
          CURRENT_coordinates='MATRIX'
          write(UNIout,'(a)')'NOTE> MENU_molecule: Z-Matrix will not allow for constraints'
          write(UNIout,'(a)')'NOTE> MENU_molecule: Z-Matrix will not be checked'
        else
          write(UNIout,'(a)')'ERROR> MENU_molecule: No Z-Matrix variables defined'
          write(UNIout,'(a)')'ERROR> MENU_molecule: See the DEFine command'
          call PRG_stop ('MENU_molecule> no Z-matrix variables')
        end if

        Zmatrix%units=ZmatrixW%units
        Zmatrix%Natoms=Natoms
        Zmatrix%Ndefines=NZMVAR
        Zmatrix%bond(1:Natoms)%length=ZmatrixW%bond(1:Natoms)%length
        Zmatrix%angle(1:Natoms)%value=ZmatrixW%angle(1:Natoms)%value
        Zmatrix%torsion(1:Natoms)%value=ZmatrixW%torsion(1:Natoms)%value
        Zmatrix%bond(1:Natoms)%label=ZmatrixW%bond(1:Natoms)%label
        Zmatrix%angle(1:Natoms)%label=ZmatrixW%angle(1:Natoms)%label
        Zmatrix%torsion(1:Natoms)%label=ZmatrixW%torsion(1:Natoms)%label
        Zmatrix%atoms(1:Natoms)%atom1=ZmatrixW%atoms(1:Natoms)%atom1
        Zmatrix%atoms(1:Natoms)%atom2=ZmatrixW%atoms(1:Natoms)%atom2
        Zmatrix%atoms(1:Natoms)%atom3=ZmatrixW%atoms(1:Natoms)%atom3
        Zmatrix%atoms(1:Natoms)%atom4=ZmatrixW%atoms(1:Natoms)%atom4

        if(MOLINP(1:6).ne.'MATRIX')then
          call CHECK_ZMATRIX ! Defines a few other variables
        end if

        if(Zmatrix%units(1:3).EQ.'ANG')then
          if(NZMVAR.ne.0)Zmatrix%define(1:Zmatrix%Nbonds)%value=Zmatrix%define(1:Zmatrix%Nbonds)%value*Angstrom_to_Bohr
          Zmatrix%bond(1:Natoms)%length=Zmatrix%bond(1:Natoms)%length*Angstrom_to_Bohr
        end if

        call BLD_cartesians
      end if

      call CHECK_molecule
!
      call set_isotopes ! set default isotopes
      formula=' '
      call CHEMICAL_FORMULA (formula)
      molecule%formula=formula
      call GET_point_group
        write(UNIout,'(2a)')'Point group: ',molecule%point_group(1:len_trim(molecule%point_group))
      if(MOL_title(1:7).eq.'UNKNOWN')then
        MOL_title(1:)=formula(1:len_trim(formula)) &
                      //', ('//molecule%point_group(1:len_trim(molecule%point_group))//')'
!     else
!       write(UNIout,'(2a)')'Point group: ',molecule%point_group(1:len_trim(molecule%point_group))
      end if
!
! Determine the number of dummy and real atoms in the molecule.
      NDummies=0
      do Iatom=1,Natoms
        if(CARTESIAN(Iatom)%Atomic_number.le.0)NDummies=NDummies+1
      end do ! Iatom
      NReal_atoms=Natoms-NDummies
!
! End of routine load_molecule
      call PRG_manager ('exit', 'LOAD_MOLECULE', 'UTILITY')
      return
      end subroutine load_molecule
      subroutine CHECK_molecule
!***********************************************************************
!     Date last modified: February 3, 1998                             *
!     Author: R.A. Poirier                                             *
!     Description: Check molecule for errors.                          *
!***********************************************************************
! Modules:

      implicit none
!
! Local scalars:
      integer :: Iatom,IFLT,INEG,IPOS,IZERO
      logical :: LXallZero,LYallZero,LZallZero
      double precision :: NoZero
!
! Begin:
      call PRG_manager ('enter', 'CHECK_molecule', 'UTILITY')
!
! Check that the last atom is not a dummy.
      if(CARTESIAN(Natoms)%Atomic_number.EQ.0)then
        call PRT_warning ('CHECK_molecule', 'The last atom is a dummy atom - it should be removed')
      end if
!
! Check the Geometry
      INEG=0
      IPOS=0
      IFLT=0
      IZERO=0
      NoZero=1.0D-8 ! Special
      LXallZero=.true. ! Special
      LYallZero=.true. ! Special
      LZallZero=.true. ! Special
      if(Natoms.gt.1)then
      do Iatom=1,Natoms
        if(IABS(CARTESIAN(Iatom)%Atomic_number).EQ.999)IFLT=IFLT+1
        if(CARTESIAN(Iatom)%Atomic_number.LT.0)INEG=INEG+1
        if(CARTESIAN(Iatom)%Atomic_number.GT.0)IPOS=IPOS+1
        if(CARTESIAN(Iatom)%Atomic_number.EQ.0)IZERO=IZERO+1
        if(dabs(CARTESIAN(Iatom)%X).gt.NoZero)LXallZero=.false. ! Special
        if(dabs(CARTESIAN(Iatom)%Y).gt.NoZero)LYallZero=.false. ! Special
        if(dabs(CARTESIAN(Iatom)%Z).gt.NoZero)LZallZero=.false. ! Special
      end do

!     if(LXallZero.or.LYallZero.or.LZallZero.and..not.Lacceptable)then
!       call PRT_warning ('CHECK_molecule', 'The X,Y or Z coordinates are all zero and the molecule is not likely planar')
!     end if

! Make sure the cartesian have been defined
      if(LXallZero.and.LYallZero.and.LZallZero)then
        write(uniOUT,'(a)')'ERROR>'
        write(uniOUT,'(a)')'No X,Y or Z coordinates'
        call PRG_stop ('No X,Y or Z coordinates')
      end if
      end if

      if(INEG.NE.0.or.IFLT.NE.0)write(UNIout,1480)INEG,IFLT,IZERO
!
! End of routine CHECK_molecule
      call PRG_manager ('exit', 'CHECK_molecule', 'UTILITY')
      return
!
 1480 FORMAT('0***THIS MOLECULE CONTAINS: ',I4,' FLOATING CENTERS(-Z)'/ &
       28X,I4,' FLOATING FUNCTIONS(F*)'/28X,I4,' DUMMY ATOMS(D*)'/)
!
      end subroutine CHECK_molecule
      subroutine CHECK_ZMATRIX
!***********************************************************************
!     Date last modified: October 18, 1996                             *
!     Author: R.A. Poirier                                             *
!     Description: Check the Z Matrix parameters.      MUN Version 1.0 *
!***********************************************************************
! Modules:

      implicit none
!
! Work arrays:
      double precision, dimension(:), allocatable :: WORKV
      character(len=MAX_label), dimension(:), allocatable :: WORK
      logical, dimension(:), allocatable :: WORKL
!
! Local scalars:
      integer :: Iatom,ISIGN,ISORT,IVAL,IZMVAR,JZMVAR,FIRST,last,status
      integer :: NBONDS,NANGLES,NTORSIONS
      logical :: found,found_all
      logical :: ERROR
!
! Begin:
      call PRG_manager ('enter', 'CHECK_ZMATRIX', 'UTILITY')
!
      allocate (WORK(1:NZMVAR), WORKV(1:NZMVAR), WORKL(1:NZMVAR), STAT=status)
!
      found_all =.true.
! Make sure all variables have been defined
      do Iatom=2,Natoms
      if(CHK_string(Zmatrix%bond(Iatom)%label,'+-0123456789.'))cycle
      found =.false.
      do IZMVAR=1,NZMVAR
        if(Zmatrix%define(IZMVAR)%name.eq.Zmatrix%bond(Iatom)%label)then
           found=.true.
        end if
      end do ! IZMVAR

      if(.not.FOUND)then
        found_all =.false.
        write(UNIout,*)'Error_CHECK_ZMATRIX> Z-Matrix Bond ',Zmatrix%bond(Iatom)%label,' not defined'
      end if
!     end if
      end do ! Iatom
!
      do Iatom=3,Natoms
      if(CHK_string(Zmatrix%angle(Iatom)%label,'+-0123456789.'))cycle
      if(index(Zmatrix%angle(Iatom)%label,'TETRA').ne.0)cycle
!     if(CHK_string(Zmatrix%angle(Iatom)%label,'TETRA'))cycle
      found =.false.
      do IZMVAR=1,NZMVAR
      if(Zmatrix%define(IZMVAR)%name.eq.Zmatrix%angle(Iatom)%label)then
        found=.true.
      end if
      end do ! IZMVAR

      if(.not.FOUND)then
        found_all =.false.
        write(UNIout,*)'Error_CHECK_ZMATRIX> Z-Matrix Angle ',Zmatrix%angle(Iatom)%label,' not defined'
      end if
      end do ! Iatom
!
      do Iatom=4,Natoms
      if(CHK_string(Zmatrix%torsion(Iatom)%label(2:),'+-0123456789.'))cycle
      if(index(Zmatrix%torsion(Iatom)%label,' TETRA').ne.0)cycle
!     if(CHK_string(Zmatrix%torsion(Iatom)%label,' TETRA'))cycle
      found =.false.
      do IZMVAR=1,NZMVAR
!       IPMC2=1
!       if(INDEX(Zmatrix%torsion(Iatom)%label,'-').NE.0)then
!         IPMC2=INDEX(Zmatrix%torsion(Iatom)%label,'-')+1
!       else if(INDEX(Zmatrix%torsion(Iatom)%label,'+').NE.0)then
!         IPMC2=INDEX(Zmatrix%torsion(Iatom)%label,'+')+1
!       end if
        call STR_size (Zmatrix%torsion(Iatom)%label, first, last)
        if(Zmatrix%define(IZMVAR)%name(1:).eq.Zmatrix%torsion(Iatom)%label(2:last))then
          found=.true.
        end if
      end do ! IZMVAR

      if(.not.FOUND)then
        found_all =.false.
        write(UNIout,*)'Error_CHECK_ZMATRIX> Z-Matrix Torsion ',Zmatrix%torsion(Iatom)%label,' not defined'
      end if
      end do ! Iatom=3
!
      if(.not.FOUND_ALL)then
        call PRG_stop ('Z-Matrix input error')
      end if
!
! Make sure no variable has been defined twice
      do IZMVAR=1,NZMVAR
      do JZMVAR=IZMVAR+1,NZMVAR
        if(Zmatrix%define(IZMVAR)%name.eq.Zmatrix%define(JZMVAR)%name)then
           write(UNIout,*)'ERROR> CHECK_ZMATRIX: Z-matrix parameter defined twice'
           write(UNIout,*)'Z-matrix parameter: ',Zmatrix%define(IZMVAR)%name
           call PRG_stop ('CHECK_ZMATRIX> Z-matrix parameter defined twice')
        end if
      end do ! JZMVAR
      end do ! IZMVAR
!
! Sort variable names:
      ISORT=0
      NBONDS=0
      NANGLES=0
      NTORSIONS=0

      do Iatom=2,Natoms
        if(CHK_string(Zmatrix%bond(Iatom)%label,'+-0123456789.'))cycle
      do IZMVAR=1,NZMVAR
        if(Zmatrix%define(IZMVAR)%name.ne.Zmatrix%bond(Iatom)%label)cycle
        ISORT=ISORT+1
        WORK(ISORT)=Zmatrix%define(IZMVAR)%name ! Create sorted list of names
        WORKV(ISORT)=Zmatrix%define(IZMVAR)%value ! Sorted list of values
        WORKL(ISORT)=Zmatrix%define(IZMVAR)%opt ! Create sorted list of opt
        NBONDS=NBONDS+1
        Zmatrix%define(IZMVAR)%name=' '
      end do ! IZMVAR
      end do ! Iatom
!
      do Iatom=3,Natoms
      if(CHK_string(Zmatrix%angle(Iatom)%label,'+-0123456789.'))cycle
      if(index(Zmatrix%angle(Iatom)%label,'TETRA').ne.0)cycle
!     if(CHK_string(Zmatrix%angle(Iatom)%label,'TETRA'))cycle
      do IZMVAR=1,NZMVAR
      if(Zmatrix%define(IZMVAR)%name.ne.Zmatrix%angle(Iatom)%label)cycle
        ISORT=ISORT+1
        WORK(ISORT)=Zmatrix%define(IZMVAR)%name ! Create sorted list
        WORKV(ISORT)=Zmatrix%define(IZMVAR)%value ! Sorted list of values
        WORKL(ISORT)=Zmatrix%define(IZMVAR)%opt ! Create sorted list of opt
        NANGLES=NANGLES+1
        Zmatrix%define(IZMVAR)%name=' '
      end do ! IZMVAR
      end do ! Iatom
!
      do Iatom=4,Natoms
      if(CHK_string(Zmatrix%torsion(Iatom)%label(2:),'+-0123456789.'))cycle
      if(index(Zmatrix%torsion(Iatom)%label,' TETRA').ne.0)cycle
!     if(CHK_string(Zmatrix%torsion(Iatom)%label,' TETRA'))cycle
      call STR_size (Zmatrix%torsion(Iatom)%label, first, last)
      do IZMVAR=1,NZMVAR
        if(Zmatrix%define(IZMVAR)%name(1:).ne.Zmatrix%torsion(Iatom)%label(2:last))cycle
        ISORT=ISORT+1
        WORK(ISORT)=Zmatrix%define(IZMVAR)%name ! Create sorted list
        WORKV(ISORT)=Zmatrix%define(IZMVAR)%value ! Sorted list of values
        WORKL(ISORT)=Zmatrix%define(IZMVAR)%opt ! Create sorted list of opt
        NTORSIONS=NTORSIONS+1
        Zmatrix%define(IZMVAR)%name=' '
      end do ! IZMVAR
      end do ! Iatom
!
! Copy sorted variable names:
      if(ISORT.NE.NZMVAR)then
        write(UNIout,*)'CHECK_ZMATRIX> error in sorting'
        call PRG_stop ('CHECK_ZMATRIX> error in sorting')
      end if

      do IZMVAR=1,NZMVAR
        Zmatrix%define(IZMVAR)%name=WORK(IZMVAR)
        Zmatrix%define(IZMVAR)%value=WORKV(IZMVAR)
        Zmatrix%define(IZMVAR)%opt=WORKL(IZMVAR)
      end do ! IZMVAR
!
      Zmatrix%units=ZmatrixW%units
      Zmatrix%Natoms=Natoms
      Zmatrix%Nbonds=NBONDS
      Zmatrix%Nangles=NANGLES
      Zmatrix%Ntorsions=NTORSIONS
      Zmatrix%Ndefines=NZMVAR
!
! Define Mapping
! Bonds
      do Iatom=2,Natoms
      do IZMVAR=1,NZMVAR
      if(CHK_string(Zmatrix%bond(Iatom)%label,'+-0123456789.'))then
        Zmatrix%bond(Iatom)%map=0
      else
        if(Zmatrix%define(IZMVAR)%name.eq.Zmatrix%bond(Iatom)%label)then
          Zmatrix%bond(Iatom)%map=IZMVAR
        end if
      end if
      end do ! IZMVAR
      end do ! Iatom
!
! Angles
      do Iatom=3,Natoms
      if(CHK_string(Zmatrix%angle(Iatom)%label,'+-0123456789.'))then
        Zmatrix%angle(Iatom)%map=0
      else if(index(Zmatrix%angle(Iatom)%label,'TETRA').ne.0)then
        Zmatrix%angle(Iatom)%map=0
      else
      do IZMVAR=1,NZMVAR
      if(Zmatrix%define(IZMVAR)%name.eq.Zmatrix%angle(Iatom)%label)then
        Zmatrix%angle(Iatom)%map=IZMVAR
      end if
      end do ! IZMVAR
      end if
      end do ! Iatom
!
      do Iatom=4,Natoms
      if(CHK_string(Zmatrix%torsion(Iatom)%label(2:),'+-0123456789.'))then
        Zmatrix%torsion(Iatom)%map=0
      else if(index(Zmatrix%torsion(Iatom)%label,' TETRA').ne.0)then ! Is this possible?
        Zmatrix%torsion(Iatom)%map=0
      else
        ISIGN=+1
      do IZMVAR=1,NZMVAR
!       IPMC2=1
        if(INDEX(Zmatrix%torsion(Iatom)%label,'-').NE.0)then
          ISIGN=-1
!         IPMC2=INDEX(Zmatrix%torsion(Iatom)%label,'-')+1
        else if(INDEX(Zmatrix%torsion(Iatom)%label,'+').NE.0)then
          ISIGN=+1
!         IPMC2=INDEX(Zmatrix%torsion(Iatom)%label,'+')+1
        end if
        call STR_size (Zmatrix%torsion(Iatom)%label, first, last)
        if(Zmatrix%define(IZMVAR)%name(1:).eq.Zmatrix%torsion(Iatom)%label(2:last))then
          Zmatrix%torsion(Iatom)%map=ISIGN*IZMVAR
        end if
      end do ! IZMVAR
      end if ! CHK_string
      end do ! Iatom
!
! Define Z-Matrix bonds, angles, torsions from Z-matrix values (Define)
! Bond Length (BONDS).
      do Iatom=2,Natoms
        IVAL=Zmatrix%bond(Iatom)%map
        if(IVAL.GT.0)then
          Zmatrix%bond(Iatom)%length=Zmatrix%define(IVAL)%value
        else if(IVAL.LT.0)then
          Zmatrix%bond(Iatom)%length=-Zmatrix%define(-IVAL)%value
        end if ! IVAL
      end do ! Iatom
!
! Bond Angle (ANGLES).
      do Iatom=3,Natoms
        IVAL=Zmatrix%angle(Iatom)%map
        if(IVAL.GT.0)then
          Zmatrix%angle(Iatom)%value=Zmatrix%define(IVAL)%value
        end if ! IVAL
      end do ! Iatom
!
! Torsion Angle (TORSON).
      do Iatom=4,Natoms
        IVAL=Zmatrix%torsion(Iatom)%map
        if(IVAL.GT.0)then
          Zmatrix%torsion(Iatom)%value=Zmatrix%define(IVAL)%value
        else if(IVAL.LT.0)then
          Zmatrix%torsion(Iatom)%value=-Zmatrix%define(-IVAL)%value
        end if ! IVAL
      end do ! Iatom

      ERROR=.FALSE.
      do Iatom=1,Natoms
      if(Zmatrix%bond(Iatom)%length.EQ.-4096.0D0)then
      ERROR=.TRUE.
      write(UNIout,'(X)')
      write(UNIout,*)'Bond length for atom: ',Iatom,' is ',Zmatrix%bond(Iatom)%length
      end if
      if(Zmatrix%angle(Iatom)%value.EQ.-4096.0D0)then
        ERROR=.TRUE.
        write(UNIout,*)'ERROR> CHECK_ZMATRIX: uninitialized Z-MATRIX parameter'
        write(UNIout,*)'Bond angle for atom: ', Iatom,' is ',Zmatrix%angle(Iatom)%value
      end if
      if(Zmatrix%torsion(Iatom)%value.EQ.-4096.0D0)then
        ERROR=.TRUE.
        write(UNIout,*)'error_CHECK_ZMATRIX: uninitialized Z-MATRIX parameter'
        write(UNIout,*)'Bond torsion for atom: ',Iatom, ' is ',Zmatrix%torsion(Iatom)%value
      end if
      end do !  IatomS
!
      call CLEAN_Zmatrix
!
      deallocate (WORK, WORKV, WORKL, STAT=status)
!
      if(ERROR)stop ' Z-MATRIX error: uninitialized parameter(s)'

! End of routine CHECK_ZMATRIX
      call PRG_manager ('exit', 'CHECK_ZMATRIX', 'UTILITY')
      return
      end subroutine CHECK_ZMATRIX
      subroutine INI_MENU_molecule
!********************************************************************************
!     Date last modified: June 10, 1999                                         *
!     Author: R.A. Poirier                                                      *
!     Description: Initialize the variables related to MENU_molecule.           *
!********************************************************************************
!
! Initialize mungauss scalars/set defaults:
      MUN_prtlev=1 ! Reset printing for every new molecule ! improve
      NZM_fixedw=0
      NZM_names=0
      LPICOR=.false.
      MOL_NAME=' '
      MOL_type='SGS'
      MOL_TITLEw='UNKNOWN'
      ZmatrixW%units='ANGSTROMS'
      MOLINPw='Z-MATRIX'
      CHARGEw=0
      MULTIPw=1
!
! Do not destroy structure information in case it exists and needs to be kept
      NatomsW=0
      NZMVARw=0
      NMCOORw=0
!
! Allocate work arrays:
      allocate (XYZC_work(1:MAX_Qatoms), MOL_atomsW(1:MAX_Qatoms), MOLEID_work(1:MAX_Qatoms), STAT=status)
      if(status.ne.0)then
        write(UNIout,*)'MENU_molecule> allocation of work arrays failed'
      end if
      allocate (ZmatrixW%atoms(1:MAX_Qatoms), ZmatrixW%bond(1:MAX_Qatoms), ZmatrixW%angle(1:MAX_Qatoms), &
                ZmatrixW%torsion(1:MAX_Qatoms), ZmatrixW%define(1:MAX_Qatoms))
!
! Defaults
      do Iatom=1,MAX_Qatoms
        ZmatrixW%bond(Iatom)%length=-4096.0D0
        ZmatrixW%angle(Iatom)%value=-4096.0D0
        ZmatrixW%torsion(Iatom)%value=-4096.0D0
        ZmatrixW%atoms(Iatom)%atom1=0
        ZmatrixW%atoms(Iatom)%atom2=0
        ZmatrixW%atoms(Iatom)%atom3=0
        ZmatrixW%atoms(Iatom)%atom4=0
        MOL_atomsW(Iatom)%isotope=0
        ZmatrixW%bond(Iatom)%label=' '
        ZmatrixW%angle(Iatom)%label=' '
        ZmatrixW%torsion(Iatom)%label=' '
        MOLEID_work(Iatom)=' '
      end do ! Iatom

      ZmatrixW%bond(1)%length=ZERO
      ZmatrixW%angle(1)%value=ZERO
      ZmatrixW%angle(2)%value=ZERO
      ZmatrixW%torsion(1)%value=ZERO
      ZmatrixW%torsion(2)%value=ZERO
      ZmatrixW%torsion(3)%value=ZERO

      ZmatrixW%define(1:MAX_Qatoms)%name=' '
      ZmatrixW%define(1:MAX_Qatoms)%value=ZERO
      ZmatrixW%define(1:MAX_Qatoms)%opt=.true.
!
      return
      end subroutine INI_MENU_molecule
      end subroutine MENU_molecule
! END CONTAINS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine BLD_cartesians
!***********************************************************************
!     Date last modified: January 28, 1992                             *
!     Author:R.A. Poirier                                              *
!     Description: Get addresses to build molecule.  MUN Version.      *
!***********************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE type_molecule

      implicit none
!
! Local scalars:
      integer I,Iatom,IER,ITEMP,I1,I2,I3,J,JTEMP,KTEMP
      double precision A,ARAD,B,BRAD,D,DCAJ,DCBJ,DSAJ,DSBJ,GAMMA,R,RZ,T,TETRA,U1(3),U2(3),U3(3),U4(3),VP(3),V3(3),ZETA
!
! Local parameters:
      double precision TENM5,TENM6
      parameter (TENM5=1.0D-5,TENM6=1.0D-6)
!
! Begin:
      call PRG_manager ('enter', 'BLD_cartesians', 'UTILITY')
!
! IER is set to 1 if an error is found in the Z Matrix.
      IER=0
!
! Check for tetrahedral angle substitution.
      TETRA=TWO*DATAN(DSQRT(TWO))/Deg_to_Radian
      do I=1,Natoms
        if(Zmatrix%atoms(I)%atom2.LT.0)Zmatrix%angle(I)%value=TETRA
        if(Zmatrix%atoms(I)%atom3.LT.0)Zmatrix%torsion(I)%value=TETRA
      end do ! I
!
! Zero the coordinate arrays
      do Iatom=1,Natoms
        CARTESIAN(Iatom)%X=ZERO
        CARTESIAN(Iatom)%Y=ZERO
        CARTESIAN(Iatom)%Z=ZERO
      end do ! Iatom
!
      if(Natoms.eq.1)GO TO 260
!
! Convert angles from Degrees to Radians.
      do Iatom=1,Natoms
! Remove tetrahedral angle substitution indicators.
        Zmatrix%atoms(Iatom)%atom2=IABS(Zmatrix%atoms(Iatom)%atom2)
        Zmatrix%atoms(Iatom)%atom3=IABS(Zmatrix%atoms(Iatom)%atom3)
        if(Iatom.NE.1.and.Zmatrix%bond(Iatom)%length.LT.TENM6)then
          write(UNIout,'(A,I5)') &
               'ERROR> BLD_cartesians: illegal bond length line: ',Iatom
          IER=1
        end if
      end do ! Iatom

      CARTESIAN(2)%Z=Zmatrix%bond(2)%length
      if(Zmatrix%atoms(2)%atom1.NE.1)then
        write(UNIout,'(a)')'ERROR> BLD_cartesians:'
        write(UNIout,'(a)')'ON Z-MATRIX line 2, Z1 MUST BE 1'
        IER=1
      end if

      if(Natoms.EQ.2)GO TO 260
      ARAD=Zmatrix%angle(3)%value*Deg_to_radian
      call SINCOS (ARAD, DSAJ, DCAJ)
      CARTESIAN(3)%X=Zmatrix%bond(3)%length*DSAJ
      J=3
      ITEMP=Zmatrix%atoms(3)%atom1
      JTEMP=Zmatrix%atoms(3)%atom2
      if(ITEMP.NE.1)then
        if(ITEMP.EQ.2)GO TO 50
        write(UNIout,1080)J,ITEMP
        IER=1
      end if

      CARTESIAN(3)%Z=Zmatrix%bond(3)%length*DCAJ
      if(JTEMP.NE.2)then
        write(UNIout,1080)J,JTEMP
        IER=1
      end if
      GO TO 60

   50 CARTESIAN(3)%Z=CARTESIAN(2)%Z-Zmatrix%bond(3)%length*DCAJ

      if(JTEMP.NE.1)then
        write(UNIout,1080)J,JTEMP
        IER=1
      end if

   60 if(Natoms.EQ.3)GO TO 260
      if(DABS(CARTESIAN(3)%X).LT.TENM5)then
        write(UNIout,'(a)')'ERROR> BLD_cartesians:'
        write(UNIout,'(a)')'ATOM 3 CANNOT LIE ON THE Z AXIS'
        IER=1
      end if

loop4:do J=4,Natoms
        ITEMP=Zmatrix%atoms(J)%atom1
        if(ITEMP.LE.0.or.ITEMP.GE.J)then
          write(UNIout,1080)J,ITEMP
          IER=1
        end if

        JTEMP=Zmatrix%atoms(J)%atom2
        if(JTEMP.LE.0.or.JTEMP.GE.J)then
          write(UNIout,1080)J,JTEMP
          IER=1
        end if
        KTEMP=Zmatrix%atoms(J)%atom3

        if(KTEMP.LE.0.or.KTEMP.GE.J)then
          write(UNIout,1080)J,KTEMP
          IER=1
        end if

        if(ITEMP.EQ.JTEMP.or.ITEMP.EQ.KTEMP.or.JTEMP.EQ.KTEMP)then
          write(UNIout,1120)J
          IER=1
        end if

        ARAD=Zmatrix%angle(J)%value*Deg_to_radian
        call SINCOS (ARAD, DSAJ, DCAJ)
        BRAD=Zmatrix%torsion(J)%value*Deg_to_radian
        call SINCOS (BRAD, DSBJ, DCBJ)

        if(Zmatrix%atoms(J)%atom4.eq.0)then ! Must be a torsion
! Bond angle and Dihedral angle.
        call VEC (U1, RZ, JTEMP, KTEMP)
        call VEC (U2, RZ, ITEMP, JTEMP)
        call VEC_product (VP, U1, U2, T, 0)
        ZETA=U1(1)*U2(1)+U1(2)*U2(2)+U1(3)*U2(3)
        T=ONE-ZETA*ZETA
        if(T.LE.TENM5)then
          write(UNIout,1110)J,JTEMP
          IER=1
          cycle loop4
        end if
        R=DSQRT(T)
        do I=1,3
          U3(I)=VP(I)/R
        end do !I
        call VEC_product (U4, U3, U2, T, 0)
        CARTESIAN(J)%X=Zmatrix%bond(J)%length*(-U2(1)*DCAJ+U4(1)*DSAJ*DCBJ+U3(1)*DSAJ*DSBJ)+CARTESIAN(ITEMP)%X
        CARTESIAN(J)%Y=Zmatrix%bond(J)%length*(-U2(2)*DCAJ+U4(2)*DSAJ*DCBJ+U3(2)*DSAJ*DSBJ)+CARTESIAN(ITEMP)%Y
        CARTESIAN(J)%Z=Zmatrix%bond(J)%length*(-U2(3)*DCAJ+U4(3)*DSAJ*DCBJ+U3(3)*DSAJ*DSBJ)+CARTESIAN(ITEMP)%Z
        else ! Must be euler angle
        if(IABS(Zmatrix%atoms(J)%atom4).NE.1)then
          write(UNIout,'(a)')'ERROR> BLD_cartesians:'
          write(UNIout,'(a,i4,a)')'Z-MATRIX line ',J,' illegal Z4 CODE'
          IER=1
          cycle loop4
        end if
!
! Two Euler (bond) angles.
        call VEC (U1, RZ, ITEMP, KTEMP)
        call VEC (U2, RZ, JTEMP, ITEMP)
        ZETA=-(U1(1)*U2(1)+U1(2)*U2(2)+U1(3)*U2(3))
        T=ONE-ZETA*ZETA
        if(T.LE.TENM5)then
          write(UNIout,1130)J,ITEMP
          IER=1
          cycle loop4
        end if
!
! Check that both Euler angles are between 0 and 180 degrees.
        if(Zmatrix%angle(J)%value.LT.ZERO.or.Zmatrix%angle(J)%value.GT.180.0D0)then
          write(UNIout,1140)J,JTEMP
          IER=1
          cycle loop4
        end if
        if(Zmatrix%torsion(J)%value.LT.ZERO.or.Zmatrix%torsion(J)%value.GT.180.0D0)then
          write(UNIout,1140)J,KTEMP
          IER=1
          cycle loop4
        end if
        A=(-DCBJ+ZETA*DCAJ)/T
        B=(DCAJ-ZETA*DCBJ)/T
        T=DSQRT(T)
        R=ZERO
        GAMMA=PI_VAL/TWO
        if(ZETA.NE.ZERO)then
        if(ZETA.LT.ZERO)R=PI_VAL
          GAMMA=DATAN(T/ZETA)+R
        end if

        D=ZERO
        ARAD=Zmatrix%angle(J)%value*Deg_to_radian
        BRAD=Zmatrix%torsion(J)%value*Deg_to_radian
        if(DABS(GAMMA+ARAD+BRAD-TWO*PI_VAL).GE.TENM6)D=Zmatrix%atoms(J)%atom4*(DSQRT(ONE+A*DCBJ-B*DCAJ))/T
        call VEC_product (V3, U1, U2, T, 0)
        CARTESIAN(J)%X=(A*U1(1)+B*U2(1)+D*V3(1))*Zmatrix%bond(J)%length+CARTESIAN(ITEMP)%X
        CARTESIAN(J)%Y=(A*U1(2)+B*U2(2)+D*V3(2))*Zmatrix%bond(J)%length+CARTESIAN(ITEMP)%Y
        CARTESIAN(J)%Z=(A*U1(3)+B*U2(3)+D*V3(3))*Zmatrix%bond(J)%length+CARTESIAN(ITEMP)%Z
        end if ! Zmatrix%atoms(J)%atom4.eq.0
      end do loop4 ! J

  260 if(IER.NE.0)then
        write(UNIout,'(a)')'RUN TERMINATED DUE TO Z MATRIX ERROR(S)'
        call PRG_stop ('RUN TERMINATED DUE TO Z MATRIX ERROR(S)')
      end if
!
 1080 FORMAT('ERROR> BLD_cartesians: Z-MATRIX line',I4, &
             ' REFERS TO UNDEFINED ATOM NUMBER',I5)
 1110 FORMAT('ERROR> BLD_cartesians: ON Z-MATRIX line',I4/ &
             'TORSION NOT DEFINED SINCE THE ANGLE Z1-Z2-Z3 AT ATOM', &
             I4,' (Z2) IS 180 DEGREES')
 1120 FORMAT('ERROR> BLD_cartesians: ON Z-MATRIX line',I4, &
             ', Z1, Z2 AND Z3 ARE NOT ALL DifFERENT')
 1130 FORMAT('ERROR> BLD_cartesians: ON Z-MATRIX line',I4, &
              ' FOR EULER ANGLES AT ATOM',I4, &
              ' (Z1), THE ATOMS Z2-Z1-Z3 ARE COLINEAR')
 1140 FORMAT('ERROR> BLD_cartesians: ON Z-MATRIX line',I4, &
             ', THE EULER ANGLE TO ATOM',I4, &
             ' IS LESS THAN 0.0 OR GREATER THAN 180.0')
!
! End of routine BLD_cartesians
      call PRG_manager ('exit', 'BLD_cartesians', 'UTILITY')
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine SINCOS (A, &
                         ASIN, &
                         ACOS)
!***********************************************************************
!     Date last modified: June 25, 1992                                *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!***********************************************************************
! Modules:

      implicit none
!
! Includes:
!
! Input scalar:
      double precision A
!
! Output scalars:
      double precision ACOS,ASIN
!
! Local parameters:
      double precision TENM6
      parameter (TENM6=1.D-6)
!
! Begin:
      ASIN=DSIN(A)
      ACOS=DCOS(A)
!
! Check for SIN or COS close to zero.
      if(DABS(ASIN).LE.TENM6)then
! Angle is 0 or 180 degrees.
        ASIN=ZERO
        ACOS=DSIGN(ONE,ACOS)
      else if(DABS(ACOS).LE.TENM6)then
! Angle is 90 or 270 degrees.
        ASIN=DSIGN(ONE,ASIN)
        ACOS=ZERO
      end if
!
! End of routine SINCOS
      return
      end subroutine SINCOS
      end subroutine BLD_cartesians
      subroutine CARTESIAN_input (XYZC_work, NatomsIN, ZM_units, NatomsW)
!***********************************************************************
!     Date last modified: March 16, 1998                               *
!     Author: R. A. Poirier                                            *
!     Description: Free Format Cartesian input routine.                *
!     TEMPLINE   - input line                                          *
!***********************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE program_parser
      USE type_elements
      USE type_molecule

      implicit none
!
! Input scalars:
      integer, intent(IN) :: NatomsIN
      integer, intent(OUT) :: NatomsW
      character*(*) ZM_units
!
      type (cartesian_coordinates) :: XYZC_work(NatomsIN)
!
! Local scalars:
      integer ATMNUM,I,IS1,IS2,lenline,lenstr
      double precision VALUE
      character*1 CTAB
      character*1 LINE(MAX_length)
      character(len=MAX_length) string,TEMPLINE
!
! Begin:
      call PRG_manager ('enter', 'CARTESIAN_input', 'UTILITY')
!
! Clear everything first.
      CTAB='	'
      NatomsW=0
      XYZC_work(1:NatomsIN)%element='    '
!
      if(Lecho)write(UNIout,*)' Free Format Cartesians:'
!
 read:do
      IS1=0
      IS2=0
      READ(UNITIN,'(a)')TEMPLINE
      if(Lecho)write(UNIout,'(1x,a,a80)')'input: ',TEMPLINE
      LENLINE=len_trim(TEMPLINE)
      if(LENLINE.eq.0)cycle          ! Blank line
!
! Allow for comment line only
      if(index(TEMPLINE(1:1),'!').ne.0)cycle ! NOTE: only allows ! in column 1!
! Check for tabs
      do I=1,LENLINE
        if(TEMPLINE(I:I).eq.CTAB)TEMPLINE(I:I)=' '
        LINE(I)=TEMPLINE(I:I)
      end do
!
! Check for an end
      call CNV_SLtoU (TEMPLINE(1:LENLINE))
      if(index(TEMPLINE,'END').ne.0)exit
!
      NatomsW=NatomsW+1
      if(NatomsW.GT.NatomsIN)then
        write(UNIout,*) &
          'ERROR: CARTESIAN_input> Too many atoms: increase MAX_Qatoms'
        call PRG_stop ('CARTESIAN_input> Too many atoms: increase MAX_Qatoms')
      end if
!
! Get atom label / atomic number
      call GET_WORD_MOL (LINE, IS1, IS2, STRING, LENSTR, LENLINE)
      if(CHK_string(string(1:lenstr),'+-0123456789'))then
        XYZC_work(NatomsW)%element=string(1:4)
        call CNV_StoI (string(1:lenstr), ATMNUM)
        XYZC_work(NatomsW)%Atomic_number=ATMNUM
        if(ATMNUM.gt.0)then
          XYZC_work(NatomsW)%element=Element_symbols(ATMNUM)
        else
          XYZC_work(NatomsW)%element='D*'
        end if
      else
        call GET_atomic_number (string(1:lenstr), ATMNUM)
        XYZC_work(NatomsW)%element=string(1:4)
        XYZC_work(NatomsW)%Atomic_number=ATMNUM
      end if
!
! Get X, Y, Z Coordinates
      call GET_WORD_MOL (LINE, IS1, IS2, STRING, LENSTR, LENLINE)
      call CNV_StoD (string(1:lenstr), VALUE)
      XYZC_work(NatomsW)%X=VALUE
      call GET_WORD_MOL (LINE, IS1, IS2, STRING, LENSTR, LENLINE)
      call CNV_StoD (string(1:lenstr), VALUE)
      XYZC_work(NatomsW)%Y=VALUE
      call GET_WORD_MOL (LINE, IS1, IS2, STRING, LENSTR, LENLINE)
      call CNV_StoD (string(1:lenstr), VALUE)
      XYZC_work(NatomsW)%Z=VALUE
      end do read
!
      if(ZM_units(1:3).EQ.'ANG')then ! Convert Cartesians from Angstroms to A.U.
        XYZC_work(1:NatomsW)%X=XYZC_work(1:NatomsW)%X*Angstrom_to_Bohr
        XYZC_work(1:NatomsW)%Y=XYZC_work(1:NatomsW)%Y*Angstrom_to_Bohr
        XYZC_work(1:NatomsW)%Z=XYZC_work(1:NatomsW)%Z*Angstrom_to_Bohr
      end if
!
! End of routine CARTESIAN_input
      call PRG_manager ('exit', 'CARTESIAN_input', 'UTILITY')
      return
      end subroutine CARTESIAN_input
!
      subroutine VEC (U, &
                      R, &
                      Jatom, &
                      Katom)
!***********************************************************************
!     Date last modified: June 25, 1992                                *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version                                        *
!***********************************************************************
! Modules:
      USE type_molecule

      implicit none
!
! Input scalars:
      integer Jatom,Katom
!
! Output scalars:
      double precision R,U(3)
!
! Local scalars:
      integer I
!
! Begin:
      U(1)=CARTESIAN(Jatom)%X-CARTESIAN(Katom)%X
      U(2)=CARTESIAN(Jatom)%Y-CARTESIAN(Katom)%Y
      U(3)=CARTESIAN(Jatom)%Z-CARTESIAN(Katom)%Z
      R=dsqrt(U(1)*U(1)+U(2)*U(2)+U(3)*U(3))
      do I=1,3
        U(I)=U(I)/R
      end do
!
! End of routine VEC
      return
      end subroutine VEC
      subroutine CLEAN_Zmatrix
!***********************************************************************
!     Date last modified: August 1, 2002                               *
!     Author: R.A. Poirier                                             *
!     Description: Check the Z Matrix parameters.      MUN Version 1.0 *
!***********************************************************************
! Modules:
      USE program_files
      USE type_molecule

      implicit none
!
! Local scalars:
      integer :: aIVAL,Iatom,IVAL
      double precision :: Ztemp
!
! Begin:
      call PRG_manager ('enter', 'CLEAN_Zmatrix', 'UTILITY')
!
! Check for angles > 180.0 and make sure torsions are in the range -180 to +180
! Bond Angles.
      if(Natoms.ge.3)then
      do Iatom=3,Natoms
        IVAL=Zmatrix%angle(Iatom)%map
        if(Ival.ne.0)then
        aIVAL=abs(IVAL)
        if(Zmatrix%define(aIVAL)%value.gt.180.0E0)then
          Zmatrix%define(aIVAL)%value=360.0E0-Zmatrix%define(aIVAL)%value
          write(UNIout,'(a,f14.6)') &
              '!NOTE: Z-matrix angle greater than 180.0 changed to: ', &
               Zmatrix%define(aIVAL)%value
          Zmatrix%angle(Iatom)%value=Zmatrix%define(aIVAL)%value ! Changed IVAL to aIVAL Jan. 04
          if(Iatom.gt.3)then
            IVAL=Zmatrix%torsion(Iatom)%map
            if(IVAL.ne.0)then
            aIVAL=abs(IVAL)
            Zmatrix%define(aIVAL)%value=Zmatrix%define(aIVAL)%value+180.0E0
              Zmatrix%torsion(Iatom)%value=Zmatrix%define(aIVAL)%value
            else
              Zmatrix%torsion(Iatom)%value=Zmatrix%torsion(Iatom)%value+180.0E0
              call CNV_DtoS (Zmatrix%torsion(Iatom)%value, Zmatrix%torsion(Iatom)%label)
            end if ! IVAL.ne.0
            write(UNIout,'(a,f12.6)') &
              '!and corresponding torsion changed to: ', &
               Zmatrix%torsion(Iatom)%value
          end if ! Iatom.gt.3
        end if ! Zmatrix%define
        end if ! Ival.ne.0
      end do ! Iatom
      end if ! Natoms.ge.3
!
! Torsion Angles.
      if(Natoms.ge.4)then
      do Iatom=4,Natoms
      IVAL=Zmatrix%torsion(Iatom)%map
      if(Ival.ne.0)then
      aIVAL=abs(IVAL)
      if(Zmatrix%torsion(Iatom)%value.lt.-180.0E0)then
        Ztemp=Zmatrix%torsion(Iatom)%value
        Zmatrix%torsion(Iatom)%value=ZTemp+360.0E0
        write(UNIout,'(a,f14.6,a,f14.6)')'!NOTE: Z-matrix torsion out of range, changed from: ', &
                                   Ztemp, &
                                   ' to: ',Zmatrix%torsion(Iatom)%value
!     else if(Zmatrix%define(aIVAL)%value.gt.180.0E0)then
      else if(Zmatrix%torsion(Iatom)%value.gt.180.0E0)then
        Ztemp=Zmatrix%torsion(Iatom)%value
        Zmatrix%torsion(Iatom)%value=ZTemp-360.0E0
        write(UNIout,'(a,f14.6,a,f14.6)')'!NOTE: Z-matrix torsion out of range, changed from: ', &
                                   Ztemp, &
                                   ' to: ',Zmatrix%torsion(Iatom)%value
        if(IVAL.GT.0)then
          Zmatrix%define(IVAL)%value=Zmatrix%torsion(Iatom)%value
        else if(IVAL.LT.0)then
          Zmatrix%define(-IVAL)%value=-Zmatrix%torsion(Iatom)%value
        end if ! IVAL
      end if
      end if ! Ival.ne.0
      end do ! Iatom
      end if ! Natoms.ge.4

! Change Dummies from DU to X
      do Iatom=1,Natoms
        if(CARTESIAN(Iatom)%element(1:2).eq.'DU')then
          CARTESIAN(Iatom)%element(1:1)='X'
          CARTESIAN(Iatom)%element(2:3)=CARTESIAN(Iatom)%element(3:4)
          CARTESIAN(Iatom)%element(4:4)=' '
        end if
      end do ! Iatom
!
! End of routine CLEAN_Zmatrix
      call PRG_manager ('exit', 'CLEAN_Zmatrix', 'UTILITY')
      return
      end subroutine CLEAN_Zmatrix
      subroutine GET_atomic_number (atmlbl, atmnum)
!***********************************************************************
!     Date last modified: February 28, 1992                Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: Given the element symbol returns the atomic number. *
!***********************************************************************
! Modules:
      USE program_files
      USE type_elements

      implicit none
!
! Input scalar:
      character*(*) atmlbl
!
! Output scalar:
      integer atmnum
!
! Local scalar:
      integer I,lenstr
      character*2 string,ATMCAPS
!
! Begin:
      call PRG_manager ('enter', 'GET_atomic_number', 'UTILITY')
!
      lenstr=min(2,len_trim(atmlbl))
      ATMCAPS='  '
      ATMCAPS(1:lenstr)=atmlbl(1:lenstr)
      call CNV_SLtoU (ATMCAPS)
! Start with 2 character symbols
      atmnum=0
      do I=1,Nelements
      string=Element_symbols(I)(1:2)
      call CNV_SLtoU (string)
      if(string(1:2).eq.ATMCAPS(1:2))then
        atmnum=I
      end if
      end do ! I
! If not 2 characters now check for 1 character
      if(atmnum.eq.0)then
      do I=1,Nelements
      string=Element_symbols(I)(1:2)
      call CNV_SLtoU (string)
      if(string(1:1).eq.ATMCAPS(1:1).and.string(2:2).eq.' ')then
        atmnum=I
      end if
      end do !I
      end if
!
      if(atmnum.eq.0)then
      if(atmlbl(1:2).ne.'DU'.and.atmlbl(1:2).ne.'D*'.and. &
         atmlbl(1:2).ne.'FL'.and.atmlbl(1:2).ne.'F*'.and. &
         atmlbl(1:2).ne.'X*'.and.atmlbl(1:2).ne.'x*')then
        write(UNIout,'(2a)')'ERROR> GET_atomic_number: illegal element ',atmlbl(1:2)
        call PRG_stop ('GET_atomic_number> illegal element')
      end if
      end if
!
! End of routine GET_atomic_number
      call PRG_manager ('exit', 'GET_atomic_number', 'UTILITY')
      return
      end subroutine GET_atomic_number
      subroutine GET_WORD_MOL (LINE, &
                           IS1, &
                           IS2, &
                           STRING, &
                           LENSTR, &
                           LENLINE)
!***********************************************************************
!     Date last modified: August 19, 1992                              *
!     Author: Mike Peterson / R.A. Poirier, MUN                        *
!     Description:                                                     *
!     Arguments:                                                       *
!     LINE : Input array of alphabetic characters to be decoded,       *
!            stored one character per array element (i.e. A1 format).  *
!***********************************************************************
! Modules:
      USE program_files

      implicit none
!
! Input scalars:
      integer IS1,IS2,LENSTR,LENLINE
      character*(*) STRING
!
! Input arrays:
      character*1 LINE(LENLINE)
!
! Local scalars:
      integer I,J
      logical FOUND
!
! Begin:
      call PRG_manager ('enter', 'GET_WORD_MOL', 'UTILITY')
!
      STRING=' '
      LENSTR=1
      IS1=IS2
      FOUND=.FALSE.
      do WHILE (.not.FOUND.and.IS1.LT.LENLINE)
        IS1=IS1+1
        FOUND=LINE(IS1).NE.' '
      end do
!
      if(.not.FOUND)then
        call PRG_manager ('exit', 'GET_WORD_MOL', 'UTILITY')
        return
      end if
!
      IS2=IS1
      FOUND=.FALSE.
      do WHILE (.not.FOUND.and.IS2.LT.LENLINE)
        IS2=IS2+1
        FOUND=LINE(IS2).EQ.' '
      end do
!
      if(IS2.LT.LENLINE)IS2=IS2-1
      J=0
! Need to check that J is le length of string
      do I=IS1,IS2
        J=J+1
        STRING(J:J)=LINE(I)
      end do ! I
      lenstr=is2-is1+1
!
! End of routine GET_WORD_MOL
      call PRG_manager ('exit', 'GET_WORD_MOL', 'UTILITY')
      return
      end subroutine GET_WORD_MOL
      subroutine BLD_Zmatrix (LError)
!********************************************************************************
!     Date last modified: July 07, 1992                            Version 1.0  *
!     Author: R. A. Poirier                                                     *
!     Description: Form the final Z-Matrix coordinates from the new Cartesians  *
!                  which were updated from the PIC.                             *
!********************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE type_molecule

      implicit none
!
! Input Scalar:
      logical LError

! Local scalars:
      integer I,J,K,L,M,Iatom,IVAL
      double precision CUV,CZX,R1,R2,R3,S,SI3
      double precision AI,AJ,AK,CUT,CUTOFF,T
!
! Local arrays:
      double precision AA(3),BB(3),V1(3),V2(3),U(3),V(3),W(3),X(3),Z(3)
!
! Local functions:
      double precision GET_R_ij
!
! Local parameters:
      double precision HPI,Radian_to_Deg,TENM11,BLCRIT,F180,F360,F90,TENM5,TENP5,THRPT5
      parameter (THRPT5=3.5D0,TENM5=1.0D-5,TENP5=1.0D5,BLCRIT=0.75D0,F90=90.0D0,F180=180.0D0,F360=360.0D0, &
                 TENM11=1.0D-11,HPI=PI_VAL/TWO)
!
! Begin:
      call PRG_manager ('enter', 'BLD_Zmatrix', 'UTILITY')
      LError=.false.
!
      Radian_to_Deg=ONE/Deg_to_Radian
!
      if(MOLINP(1:4).EQ.'CART')GO TO 300
!
! Now convert Cartesian coordinates to Z-Matrix coordinates.
! Bond Lengths.
      do Iatom=2,Natoms
        if(CARTESIAN(Iatom)%Atomic_number.le.0)cycle
        Zmatrix%bond(Iatom)%length=GET_R_ij(Iatom,Zmatrix%atoms(Iatom)%atom1)
      end do ! Iatom=2
!
! Handle bond and dihedral angles.
      if(Natoms.le.2)GO TO 300
      CUT=THRPT5
      CUTOFF=TENP5
      do Iatom=3,Natoms
         I=Iatom
         J=Zmatrix%atoms(Iatom)%atom1
         K=Zmatrix%atoms(Iatom)%atom2
         L=Zmatrix%atoms(Iatom)%atom3
         AI=GET_R_ij(J,K)
         AJ=GET_R_ij(I,K)
         AK=GET_R_ij(I,J)
! Ensure that we actually have a triangle.
         if(AI.LE.TENM5.or.AI.GT.CUTOFF)GO TO 180
         if(AJ.LE.TENM5)GO TO 180
         if(AK.LE.TENM5.or.AK.GT.CUTOFF)GO TO 180
         T=(AI*AI+AK*AK-AJ*AJ)/(TWO*AI*AK)
         if(DABS(T).GE.ONE)T=DSIGN(ONE,T)
         if(DABS(T).LT.TENM5)GO TO 140
! Save the bond angle in Zmatrix%angle(Iatom)%value
         Zmatrix%angle(Iatom)%value=DATAN(DSQRT(ONE-T*T)/T)*Radian_to_Deg
         if(T.LT.ZERO)Zmatrix%angle(Iatom)%value=Zmatrix%angle(Iatom)%value+F180
         GO TO 180
  140    Zmatrix%angle(Iatom)%value=F90
  180    if(Iatom.EQ.3)GO TO 280
         if(Zmatrix%atoms(Iatom)%atom4.EQ.0)GO TO 200
! Euler Angle.
! Ensure that we actually have a triangle.
         AI=GET_R_ij(J,L)
         AJ=GET_R_ij(I,L)
         AK=GET_R_ij(I,J)
         if(AI.LE.TENM5.or.AI.GT.CUTOFF)GO TO 280
         if(AJ.LE.TENM5)GO TO 180
         if(AK.LE.TENM5.or.AK.GT.CUTOFF)GO TO 280
         T=(AI*AI+AK*AK-AJ*AJ)/(TWO*AI*AK)
         if(DABS(T).GE.ONE)T=DSIGN(ONE,T)
         if(DABS(T).LT.TENM5)GO TO 190
         Zmatrix%torsion(Iatom)%value=DATAN(DSQRT(ONE-T*T)/T)*Radian_to_Deg
         if(T.LT.ZERO)Zmatrix%torsion(Iatom)%value=Zmatrix%torsion(Iatom)%value+F180
         if(Zmatrix%torsion(Iatom)%value.GE.ZERO.and.Zmatrix%torsion(Iatom)%value.LE.F180)GO TO 280
         write(UNIout,'(A,I4,A,I4,A)')'ERROR> BLD_Zmatrix: ON Z-MATRIX line ',Iatom, &
               ' THE EULER ANGLE TO ATOM ',L,' IS LESS THAN 0.0 OR GREATER THAN 180.0'
         LError=.TRUE.
         GO TO 280
  190    Zmatrix%torsion(Iatom)%value=F90
         GO TO 280
!
! Torsion Angles.
  200 continue
         call VEC (U, R1, I, J)
         call VEC (V, R2, K, J)
         call VEC (W, R3, K, L)
         call VEC_product (Z, U, V, T, 1)
         call VEC_product (X, W, V, T, 1)
         CZX=-doT_Product (Z,X)                      ! |Z| |X| COS(theta)
         call VEC_product (U, Z, X, T, 0)
         CUV=doT_Product (U,V)
         S=HPI
         if(DABS(CZX).GE.TENM11)then
           SI3=DSQRT(U(1)*U(1)+U(2)*U(2)+U(3)*U(3)) ! |Z| |X| SIN(theta)
           S=DATAN(SI3/CZX)                         ! SI3/CZX = SIN(theta)/COS(theta)
           if(CZX.LT.ZERO)S=S+PI_VAL
         end if
         if(CUV.LT.ZERO)S=-S
         Zmatrix%torsion(Iatom)%value=-S*Radian_to_Deg
  280 continue
      end do ! N=3
!
  300 continue
!
! Update Z-Matrix values
! Bond Length (Zmatrix%bond(N)%length).
      do Iatom=2,Natoms
        IVAL=Zmatrix%bond(Iatom)%map
        if(IVAL.GT.0)then
          Zmatrix%define(IVAL)%value=Zmatrix%bond(Iatom)%length
        else if(IVAL.LT.0)then
          Zmatrix%define(-IVAL)%value=Zmatrix%bond(Iatom)%length
        end if ! IVAL
      end do ! Iatom
!
! Bond Angles
      do Iatom=3,Natoms
        IVAL=Zmatrix%angle(Iatom)%map
        if(IVAL.GT.0)then
          Zmatrix%define(IVAL)%value=Zmatrix%angle(Iatom)%value
        else if(IVAL.LT.0)then
          Zmatrix%define(-IVAL)%value=Zmatrix%angle(Iatom)%value
        end if ! IVAL
      end do ! Iatom
!
! Torsion Angles
      do Iatom=4,Natoms
        IVAL=Zmatrix%torsion(Iatom)%map
        if(IVAL.GT.0)then
          Zmatrix%define(IVAL)%value=Zmatrix%torsion(Iatom)%value
        else if(IVAL.LT.0)then
          Zmatrix%define(-IVAL)%value=Zmatrix%torsion(Iatom)%value
        end if ! IVAL
      end do ! Iatom
!
! End of routine BLD_Zmatrix
      call PRG_manager ('exit', 'BLD_Zmatrix', 'UTILITY')
      return
      end
      subroutine SET_ISOTOPES
!********************************************************************************
!     Date last modified: April 28, 1999                                        *
!     Author: R.A. Poirier                                                      *
!     Description: Set default isotopes.                                        *
!********************************************************************************
! Modules:
      USE type_elements
      USE type_molecule

      implicit none
!
! Local scalars:
      integer Iatom
!
! Begin:
      call PRG_manager ('enter', 'SET_ISOTOPES', 'UTILITY')
!
! Set default isotope for those not specified in the input
      do Iatom=1,Natoms
       if(MOL_atoms(Iatom)%isotope.eq.0.and.CARTESIAN(Iatom)%Atomic_number.gt.0)then
         MOL_atoms(Iatom)%isotope=DEF_isotope(CARTESIAN(Iatom)%Atomic_number)
       end if
      end do ! Iatom
!
! End of routine SET_ISOTOPES
      call PRG_manager ('exit', 'SET_ISOTOPES', 'UTILITY')
      return
      end subroutine SET_ISOTOPES

      subroutine MENU_BS_radii
!***********************************************************************
!     Date last modified: January 18, 1991                             *
!     Author:                                                          *
!     Description:                                                     *
!***********************************************************************
! Modules:
      USE program_files
      USE program_parser
      USE MENU_gets
      USE type_elements

      implicit none
!
! Local scalars:
      logical done,lrun
!
! Local scalars:
      integer :: ATMNUM
      double precision :: BSrad
      character(len=2) :: atom
!
! Begin:
      call PRG_manager ('enter', 'MENU_BS_radii', 'UTILITY')
!
      done=.false.
      lrun=.false.
!
! Menu:
      do while (.not.done)
      call new_token ('BSradii:')
!
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      ' Command BSradii:', &
      '  Purpose: Set parameters of topology code (molecular graph)', &
      '  Syntax:', &
      '  Element  : <string>: Element, e.g., H, He, ...', &
      '  end'
!
      else if(token(1:3).ne.'END')then
        atom='  '
        atom=token(1:2)
        call GET_value (BSrad)
!
      if(CHK_string(atom,'+-0123456789'))then
        call CNV_StoI (atom, ATMNUM)
      else
        call GET_atomic_number (atom, ATMNUM)
      end if

      BS_radii(ATMnum)=BSrad
      write(UNIout,'(3a,F6.3/)')'Brag-Slater radii for ',Element_symbols(ATMNUM),' set to ',BSrad

      else
        call MENU_end (done)
      end if

      end do !(.not.done)
!
! End of routine MENU_BS_radii
      call PRG_manager ('exit', 'MENU_BS_radii', 'UTILITY')
      return
      end subroutine MENU_BS_radii
