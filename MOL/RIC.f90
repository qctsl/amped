      subroutine BLD_RIC
!*****************************************************************************************
! Date last modified: 1 April, 2000                                          Version 1.0 *
! Author: R. A. Poirier                                                                  *
! Description: Create Redundant Internal Coordinates (RIC): bonds, angles, torsions, ... *
!*****************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE redundant_coordinates
      USE GRAPH_objects
      USE OPT_defaults
      USE OPT_objects

      implicit none
!
! Local scalars:
      integer I,J,K,L,IN,JN,KN,Ibond,Iangle,Itorsion,Ioopbend,Inonbonded
      double precision OLD_val,NEW_val
      logical Lprevious
!
! Local function:
      double precision GET_R_IJ,GET_angle,GET_torsion,OOPBEND_IN
!
! Begin:
      call PRG_manager ('enter', 'BLD_RIC', 'INTERNAL%RIC')
!
      LPrevious=.true.
!
! Stretches:
      if(Natoms.eq.2)then
        write(UNIout,'(a)')'ERROR: BLD_RIC> For diatomics please use Zmatrix coordinates'
        call PRG_stop ('For diatomics please use Zmatrix coordinates')
      end if

      call GET_object ('MOL', 'GRAPH', 'BOND_ATOMS')
      RIC%Nbonds=NGbonds
      if(.not.associated(RIC%bond))then
        allocate (RIC%bond(NGbonds))
      end if
      do Ibond = 1,RIC%Nbonds
        I = BOND(Ibond)%ATOM1
        J = BOND(Ibond)%ATOM2
!       if(CARTESIAN(I)%Atomic_number.gt.0.and.CARTESIAN(J)%Atomic_number.gt.0) then
          RIC%bond(Ibond)%atom1 = I
          RIC%bond(Ibond)%atom2 = J
          RIC%bond(Ibond)%value = GET_R_IJ(I,J)
!       end if
      end do
      RIC%NIcoordinates=RIC%Nbonds
!
! Bends:
      call GET_object ('MOL', 'GRAPH', 'ANGLE_ATOMS')
      RIC%Nangles=NGangle
      if(.not.associated(RIC%angle))then
        allocate (RIC%angle(NGangle))
      end if
      if(RIC%Nangles.gt.0)then
        do Iangle = 1,RIC%Nangles
          I = ANGLE(Iangle)%ATOM1
          K = ANGLE(Iangle)%ATOM2   ! kATM is the central atom...
          J = ANGLE(Iangle)%ATOM3

          IN = CARTESIAN(I)%Atomic_number
          JN = CARTESIAN(J)%Atomic_number
          KN = CARTESIAN(K)%Atomic_number
          if(IN.gt.0.and.KN.gt.0.and.JN.gt.0) then
            RIC%angle(Iangle)%atom1=I
            RIC%angle(Iangle)%atom2=K
            RIC%angle(Iangle)%atom3=J
            RIC%angle(Iangle)%value=GET_angle(I,K,J)
          else
            RIC%angle(Iangle)%value = ZERO
          end if
        end do
        RIC%NIcoordinates=RIC%NIcoordinates+RIC%Nangles
      end if

! Torsions:
      call GET_object ('MOL', 'GRAPH', 'TORSION_ATOMS')
      RIC%Ntorsions=NGtorsion
      if(.not.associated(RIC%torsion))then
        allocate (RIC%torsion(NGtorsion))
        LPrevious=.false.
      end if
      if(RIC%Ntorsions.gt.0) then
        do Itorsion = 1,RIC%Ntorsions
          I = TORSION(Itorsion)%ATOM1
          J = TORSION(Itorsion)%ATOM2
          K = TORSION(Itorsion)%ATOM3
          L = TORSION(Itorsion)%ATOM4
          OLD_VAL = RIC%torsion(Itorsion)%value
          NEW_VAL = GET_torsion (I, J, K, L)
! Adopted from routine MACHQ8 to ensure old and new torsion values do not differ by 180 degrees:
          if(LPrevious)then
          IF(DABS(OLD_VAL-NEW_VAL).GT.PI_VAL)then
            IF(OLD_VAL.LT.ZERO)then
              NEW_VAL=NEW_VAL-(TWO*PI_VAL)
              write(UNIout,*)'OLD torsion: ',OLD_VAL
              write(UNIout,*)'NEW torsion: ',NEW_VAL
            else
              NEW_VAL=NEW_VAL+(2.0D0*PI_VAL)
              write(UNIout,*)'OLD torsion: ',OLD_VAL
              write(UNIout,*)'NEW torsion: ',NEW_VAL
            end if ! OLD_VAL
          end if ! DABS
          end if ! LPrevious
          RIC%torsion(Itorsion)%atom1=I
          RIC%torsion(Itorsion)%atom2=J
          RIC%torsion(Itorsion)%atom3=K
          RIC%torsion(Itorsion)%atom4=L
          RIC%torsion(Itorsion)%value=NEW_VAL
        end do
        RIC%NIcoordinates=RIC%NIcoordinates+RIC%Ntorsions
      end if

! Out-of-plane bends:
      call GET_object ('MOL', 'GRAPH', 'OOPBEND_ATOMS')
      RIC%Noopbends=Noopbend
      if(.not.associated(RIC%oopbend))then
        allocate (RIC%oopbend(Noopbend))
      end if
      if(RIC%Noopbends.gt.0) then
        do Ioopbend = 1,RIC%Noopbends
          I = OOPBEND(Ioopbend)%ATOM1
          J = OOPBEND(Ioopbend)%ATOM2
          K = OOPBEND(Ioopbend)%ATOM3
          L = OOPBEND(Ioopbend)%ATOM4

          RIC%oopbend(Ioopbend)%atom1=I
          RIC%oopbend(Ioopbend)%atom2=J
          RIC%oopbend(Ioopbend)%atom3=K
          RIC%oopbend(Ioopbend)%atom4=L
          RIC%oopbend(Ioopbend)%value=OOPBEND_IN(I,J,K,L)
        end do
        RIC%NIcoordinates=RIC%NIcoordinates+RIC%Noopbends
      end if

! Nonbonded distances:
      if(OPT_function%name(1:2).eq.'MM')then
      call GET_object ('MOL', 'GRAPH', 'NON_BONDED_ATOMS')
      RIC%Nnonbonded=Nelec
      if(.not.associated(RIC%nonbonded))then
        allocate (RIC%nonbonded(Nelec))
      end if
      if(RIC%Nnonbonded.gt.0)then
        do Inonbonded = 1,RIC%Nnonbonded
          I = pair_IJ(Inonbonded)%I
          J = pair_IJ(Inonbonded)%J
          if(CARTESIAN(I)%Atomic_number.gt.0.and.CARTESIAN(J)%Atomic_number.gt.0) then
            RIC%nonbonded(Inonbonded)%atom1=I
            RIC%nonbonded(Inonbonded)%atom2=J
            RIC%nonbonded(Inonbonded)%value=GET_R_IJ (I, J)
          end if
        end do
        RIC%NIcoordinates=RIC%NIcoordinates+RIC%Nnonbonded
      end if
      else
      RIC%Nnonbonded=0 ! Excluded for ab initio
      end if

! Print the RIC
      if(MUN_prtlev.gt.0)then
        call PRT_RIC
      end if
!
! End of routine BLD_RIC
      call PRG_manager ('exit', 'BLD_RIC', 'INTERNAL%RIC')
      return
      end subroutine BLD_RIC
      subroutine BLD_RIC_nonbonded
!*****************************************************************************************
! Date last modified: 1 April, 2000                                          Version 1.0 *
! Author: R. A. Poirier                                                                  *
! Description: Create Redundant Internal Coordinates (RIC): bonds, angles, torsions, ... *
!*****************************************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE redundant_coordinates
      USE GRAPH_objects

      implicit none
!
! Local scalars:
      integer I,J,Inonbonded
!
! Local function:
      double precision GET_R_IJ
!
! Begin:
      call PRG_manager ('enter', 'BLD_RIC_nonbonded', 'UTILITY')

! Nonbonded distances:
      if(RIC%Nnonbonded.gt.0)then
        call KILL_object ('MOL', 'ATOMIC', 'DISTANCES')
        call KILL_object ('MOL', 'GRAPH', 'NON_BONDED')
        call KILL_object ('MOL', 'GRAPH', 'NON_BONDED_ATOMS')
!
        call GET_object ('MOL', 'ATOMIC', 'DISTANCES')
        call GET_object ('MOL', 'GRAPH', 'NON_BONDED')
        call GET_object ('MOL', 'GRAPH', 'NON_BONDED_ATOMS')
!
        do Inonbonded = 1,RIC%Nnonbonded
          I = pair_IJ(Inonbonded)%I
          J = pair_IJ(Inonbonded)%J
          if(CARTESIAN(I)%Atomic_number.gt.0.and.CARTESIAN(J)%Atomic_number.gt.0)then
            RIC%nonbonded(Inonbonded)%atom1=I
            RIC%nonbonded(Inonbonded)%atom2=J
            RIC%nonbonded(Inonbonded)%value=GET_R_IJ (I, J)
          end if
        end do
        write(6,*)'debug_BLD_RIC_nonbonded > Nonbonded have been updated'
      end if

!
! End of routine BLD_RIC_nonbonded
      call PRG_manager ('exit', 'BLD_RIC_nonbonded', 'UTILITY')
      return
      end subroutine BLD_RIC_nonbonded
