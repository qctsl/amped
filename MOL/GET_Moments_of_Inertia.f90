      subroutine GET_Moments_of_Inertia
!***********************************************************************
!     Date last modified: November 22, 2019                            *
!     Author: R.A. Poirier                                             *
!     Description: Determine the 3 moments of inertia                  *
!***********************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE type_molecule
      USE atommass

      implicit none
!
! Work arrays
!
! Local scalars:
      integer Iatom
      double precision :: X,Y,Z,Zmass
      double precision :: SEC_MOM(3,3),P_AXES(3,3),MOMENTS(3),CMass(3)
!
! Begin:
      call PRG_manager ('enter', 'GET_MOMENTS_OF_INERTIA', 'UTILITY')
!
      call CENTER_OF_MASS (CMass)
      SEC_MOM=ZERO

! This has been verified and is correct
      do Iatom=1,NAtoms
        if(CARTESIAN(Iatom)%atomic_number.le.0)cycle
          Zmass=MOL_atoms(Iatom)%mass
!         write(6,*)'Masses: ',Zmass
          X=CARTESIAN(Iatom)%X-CMass(1)
          Y=CARTESIAN(Iatom)%Y-CMass(2)
          Z=CARTESIAN(Iatom)%Z-CMass(3)
! XX
          SEC_MOM(1,1)=SEC_MOM(1,1)+Zmass*(Y*Y+Z*Z)
! XY
          SEC_MOM(2,1)=SEC_MOM(2,1)-Zmass*X*Y
! YY
          SEC_MOM(2,2)=SEC_MOM(2,2)+Zmass*(X*X+Z*Z)
! XZ
          SEC_MOM(3,1)=SEC_MOM(3,1)-Zmass*X*Z
! YZ
          SEC_MOM(3,2)=SEC_MOM(3,2)-Zmass*Y*Z
! ZZ
          SEC_MOM(3,3)=SEC_MOM(3,3)+Zmass*(X*X+Y*Y)
      end do ! Iatom
!
      call MATRIX_diagonalize (SEC_MOM, P_AXES, MOMENTS, 3, 1, .true.)
!
! Save the moments of inertial for the CML
      MomIx=MOMENTS(1)
      MomIy=MOMENTS(2)
      MomIz=MOMENTS(3)
!     write(UNIout,'(a,3f20.6)')'Moments of Inertia: ',MomIx,MomIy,MomIz
!
! End of routine GET_Moments_of_Inertia
      call PRG_manager ('exit', 'GET_MOMENTS_OF_INERTIA', 'UTILITY')
      return
      end subroutine GET_Moments_of_Inertia
