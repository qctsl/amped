      subroutine MENU_fragments
!***********************************************************************
!     Date last modified: June 24, 1992                                *
!     Author: F. Colonna                                               *
!     Description: Set up one-electron integrals calculation.          *
!***********************************************************************
! Modules:
      USE program_files
      USE program_parser
      USE MENU_gets
      USE type_molecule
      USE type_fragments

      implicit none
!
! Local Work array:
      integer, dimension(:), allocatable :: el_by_fragW
!
! Local scalars:
      integer :: Iatom,IERROR,IFragment,Nbbyte,Nlist,NFragmentsM
      logical :: AUTOfrag,done,LelectronIN
!
! Begin:
      call PRG_manager ('enter', 'MENU_fragments', 'UTILITY')

      done=.false.
      IFragment=0
      LelectronIN=.false.
      AUTOfrag=.false.
      Nbbyte=4

      call INI_fragments
      allocate (el_by_fragW(Nelectrons))
!
! Menu:
      do while (.not.done)
      call new_token ('Fragments:')
!
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      ' Command FRAgments:', &
      '  Purpose: Define molecular fragments ', &
      '  Syntax:', &
      '  FRAGments', &
      '  DEfine = <command>, Define a fragment', &
      '  NFragments = <integer> ', &
      '  end'
!
      else if(token(1:2).EQ.'NF' )then
        call GET_value (NFragmentsM)
        AUTOfrag=.true.

      else if(token(1:2).EQ.'DE' )then
        IFragment=IFragment+1
        call MENU_Define_Frag

      else
        call MENU_end (done)

      end if
!
      end do !(.not.done)

      if(AUTOfrag)then
        call BLD_Fragments (NFragmentsM)
      else
        NFragments=IFragment
        if(LelectronIN)then
          allocate (Electron_by_fragment(1:NFragments))
          Electron_by_fragment(1:NFragments)=el_by_fragW(1:NFragments)
        else
          call BLD_ELbyFragment
        end if
        call PRT_Fragments
      end if ! AUTOfrag

      deallocate (el_by_fragW)
!
! End of routine MENU_fragments
      call PRG_manager ('exit', 'MENU_fragments', 'UTILITY')
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine MENU_Define_Frag
!***********************************************************************
!     Date last modified: June 24, 1992                                *
!     Author: F. Colonna                                               *
!     Description: Set up one-electron integrals calculation.          *
!***********************************************************************
! Modules:

      implicit none
!
! Local Work array:
      character(len=4), dimension(:), allocatable :: atmlst
!
! Local scalars:
      integer :: NFrag_electrons
      logical :: done
!
! Begin:
      call PRG_manager ('enter', 'MENU_Define_Frag', 'UTILITY')

      done=.false.
      LelectronIN=.false.
      Nbbyte=4

      allocate (atmlst(Natoms))
!
! Menu:
      do while (.not.done)
      call new_token ('Define Fragments:')
!
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      ' Command FRAgments:', &
      '  Purpose: Define molecular fragments ', &
      '  Syntax:', &
      '  DEfinefragments', &
      '  AToms = <list>, example, ( C1 H2 H3 H4 )', &
      '  ELectrons = <integer>, number of electrons belonging to fragment', &
      '  end'
!
      else if(token(1:2).EQ.'AT' )then
        call GET_Clist (atmlst, nbbyte, Natoms, Nlist)
        call STORE_ATM_frag (atmlst, nbbyte, Nlist, IFragment)

      else if(token(1:2).EQ.'EL' )then
        call GET_value (NFrag_electrons)
        el_by_fragW(IFragment)=NFrag_electrons
        LelectronIN=.true.
 
      else
        call MENU_end (done)

      end if
!
      end do !(.not.done)
!
      deallocate (atmlst)
!
! End of routine MENU_Define_Frag
      call PRG_manager ('exit', 'MENU_Define_Frag', 'UTILITY')
      return
      end subroutine MENU_Define_Frag
      subroutine STORE_ATM_frag (atmlst, &
                                 nbbyte, &
                                 nlist,  &
                                 IFragment)
!*****************************************************************************************************************
!     Date last modified: August 6, 2004                                                            Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Identify the atoms in atmlst as belonging to IFragment (Atom_to_fragment)                     *
!*****************************************************************************************************************
! Modules:
!     USE type_molecule
!     USE type_fragments

      implicit none
!
! Input scalars:
      integer :: IFragment,nbbyte,nlist
!
! Input arrays:
      character*(*) :: atmlst(Nlist)
!
! Local arrays:
      integer :: atmnum
!
! Local scalars:
      integer :: Ilist,Iatom
!
! Begin:
      call PRG_manager ('enter', 'STORE_ATM_frag', 'UTILITY')
!
      do Ilist=1,nlist
      if(CHK_string(atmlst(Ilist),'+-0123456789'))then
        call CNV_StoI (atmlst(Ilist), ATMNUM)
      else
        do Iatom=1,Natoms
        if(CARTESIAN(Iatom)%element(1:4).eq.atmlst(Ilist))then
          ATMNUM=Iatom
        end if
        end do ! Iatom
      end if
      if(ATMNUM.le.0.or.ATMNUM.gt.Natoms)then
        write(UNIout,'(1x,a,a)')'ERROR: STORE_ATM_frag> illegal atom in list ',atmlst(Ilist)
      end if
      Atom_to_Fragment(ATMNUM)=IFragment
      end do ! Ilist
!
! End of routine STORE_ATM_frag
      call PRG_manager ('exit', 'STORE_ATM_frag', 'UTILITY')
      return
      end subroutine STORE_ATM_frag
      end subroutine MENU_fragments
