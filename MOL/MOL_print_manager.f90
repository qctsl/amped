      subroutine MOL_PRINT_manager (ObjName, Modality)
!********************************************************************************
!     Date last modified: April 24, 2008                           Version 2.0 *
!     Author: R.A. Poirier                                                      *
!     Description: Calls the appropriate routine to "Command" the object nameIN *
!     Command can be "PRINT" or "KILL"                                          *
!     Pwhat can be "OBJECT", "DOCUMENTATION" or "TABLE"                         *
!********************************************************************************
! Modules:
      USE program_files
      USE program_objects

      implicit none
!
! Input scalars:
      character*(*), intent(IN) :: ObjName,Modality
!
! Local scalars:
!
! Begin:
      call PRG_manager ('enter', 'MOL_PRINT_manager', 'UTILITY')

      SelectObject: select case (ObjName)
        case ('ATOMIC')
          select case (Modality)
          case ('DISTANCES')
            call PRT_distance_matrix
          case ('MASSES')
            call PRT_masses
          case default
            write(UNIout,*)'MOL_PRINT_manager>'
            write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                           '" for Modality "',Modality(1:len_trim(Modality)),'"'
            stop 'No such object'
          end select

        case ('BMATRIX') ! Do nothing
        case ('SHAPE') ! Do nothing
        case ('SIMILARITY') ! Do nothing
        case ('FRAGMENTS') ! Do nothing
        case ('EL_BY_FRAGMENTS') ! Do nothing
        case ('AO_TO_FRAGMENTS') ! Do nothing
        case ('MOMENTS_OF_INERTIA') ! Do nothing
        case ('INTERNAL')
          select case (Modality)
          case('FZM')
            call PRT_FZM
          case('ZM')
            call PRT_ZMat
          case('RIC')
            call PRT_RIC
          case('ZMATRIX')
          case default
            write(UNIout,*)'MOL_PRINT_manager>'
            write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                           '" for Modality "',Modality(1:len_trim(Modality)),'"'
            stop 'No such object'
          end select

        case ('GRADIENTS')
          select case (Modality)
          case('ZM')
            call PRT_GZMatrix
          case('XYZ')
            call PRT_gradients_XYZ
          case('PIC')
            call PRT_gradients_PIC
          case('RIC')
!           call PRT_gradients_RIC  ! Does not exist
          case('COMPONENTS')
            call PRT_gradient_components
          case default
            write(UNIout,*)'MOL_PRINT_manager>'
            write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                           '" for Modality "',Modality(1:len_trim(Modality)),'"'
            stop 'No such object'
          end select

        case ('GRAPH')
          select case (Modality)
          case ('BOND_ATOMS') ! object
            call PRT_GRAPH_MAKEBOND
          case ('CNCOMP') ! object
            call PRT_GRAPH_CNCOMP
          case ('CONNECT') ! object
            call PRT_GRAPH_CONNECT
          case ('CONVAL')
            call PRT_GRAPH_VALENCE
          case ('ANGLE_ATOMS')
            call PRT_GRAPH_MAKEANG
          case ('TORSION_ATOMS')
            call PRT_GRAPH_MAKETORS
          case ('CLOSE_CONTACT')
            call PRT_GRAPH_CLOSE_CONT
          case ('COMCON')
            call PRT_GRAPH_COMP_CONN
          case ('CONN_FAILSAFE')
            call PRT_GRAPH_COMP_CONN_last
          case ('VERY_WEAK_BONDS')
            call PRT_GRAPH_VWEAK_BOND
          case ('VAN_DER_WAAL_BONDS')
            call PRT_GRAPH_VderW_BOND
          case ('CONN_PRUN')
            call PRT_GRAPH_PRUNE
          case ('PRUNE_HOMRED')
            call PRT_GRAPH_HOMREDUCED
          case ('CONN_PRUN_HOMRED_INCIDENCE')
            call PRT_GRAPH_INCIDENCE
          case ('FUNDRING')
            call PRT_GRAPH_FUND_RINGS
          case ('EDGE_RING_TO_RING_ASSEMBLY')
            call PRT_GRAPH_RING_ASSEMBLY
          case ('VERTEX_LIST')
            call PRT_GRAPH_ATOM_BETWEEN
          case ('EDGE_TO_RING')
            call PRT_GRAPH_EDGE_RINGASS
          case ('OOPBEND_ATOMS')
            call PRT_MAKE_OOPBENDS
          case ('NON_BONDED')
            call PRT_CONN_NBOND
          case ('NON_BONDED_ATOMS')
            call PRT_GRAPH_NO_BONDS
          case default
            write(UNIout,*)'MOL_PRINT_manager>'
            write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                           '" for Modality "',Modality(1:len_trim(Modality)),'"'
            stop 'No such object'
          end select

      case default
            write(UNIout,*)'MOL_PRINT_manager>'
      write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                     '" for Modality "',Modality(1:len_trim(Modality)),'"'
      end select SelectObject
!
! end of function MOL_PRINT_manager
      call PRG_manager ('exit', 'MOL_PRINT_manager', 'UTILITY')
      return
      end subroutine MOL_PRINT_manager
