      subroutine MENU_topology
!***********************************************************************
!     Date last modified: June 24, 1992                                *
!     Author: F. Colonna                                               *
!     Description: Set up one-electron integrals calculation.          *
!***********************************************************************
! Modules:
      USE program_parser
      USE MENU_gets
      USE type_elements

      implicit none
!
! Local scalars:
      logical done,lrun
!
! Begin:
      call PRG_manager ('enter', 'MENU_topology', 'UTILITY')
      done=.false.
      lrun=.false.
!
! Menu:
      do while (.not.done)
      call new_token ('Topology:')
!
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      ' Command Topology:', &
      '  Purpose: Set parameters of topology code (molecular graph)', &
      '  Syntax:', &
      '  TOPology', &
      '  BSscale  : <real*8>: scale factor for Bragg-Slater radii', &
      '  VDWscale  : <real*8>: scale factor for van der Waal radii', &
      '  end'
!
      else if(token(1:3).EQ.'VDW' )then
        call GET_value (vdW_scalef)
!
      else if(token(1:2).EQ.'BS' )then
        call GET_value (BS_scalef)

      else
        call MENU_end (done)

      end if
!
      end do !(.not.done)
!
! End of routine MENU_topology
      call PRG_manager ('exit', 'MENU_topology', 'UTILITY')
      return
      end
