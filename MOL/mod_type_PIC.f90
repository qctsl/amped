      MODULE proper_internals
!***************************************************************************************************************************
!     Date last modified: November 3, 2000                                                                    Version 1.0  *
!     Author: R.A. Poirier                                                                                                 *
!     Description: Proper Internal Coordinates (PIC)                                                                       *
!***************************************************************************************************************************

      implicit none
!
      type PI_coordinates
        integer :: Ncoordinates              ! Number of proper internal coordinates
        integer :: Total_components          ! Total number of components
!
! The following are dimensioned Ncoordinates:
        integer, dimension(:), pointer :: type => null()
        integer, dimension(:), pointer :: symmetry => null()
        integer, dimension(:), pointer :: Ncomponents => null()
        double precision, dimension(:), pointer :: SCALE => null()
        double precision, dimension(:), pointer :: value => null()
        character(len=8), dimension(:), pointer :: LABEL => null()
!
! The following are dimensioned Total_components
        integer, dimension(:), pointer :: ATOM1 => null()
        integer, dimension(:), pointer :: ATOM2 => null()
        integer, dimension(:), pointer :: ATOM3 => null()
        integer, dimension(:), pointer :: ATOM4 => null()
        double precision, dimension(:), pointer :: component => null()
        double precision, dimension(:), pointer :: coeff => null()
      end type PI_coordinates

      type (PI_coordinates), save :: PIC

      end MODULE proper_internals
