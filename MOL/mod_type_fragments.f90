      MODULE type_fragments
!*****************************************************************************
! Date last modified: 8 August, 2000                             Version 1.0 *
! Author: R. A. Poirier                                                      *
! Description: Fragment information.                                         *
!*****************************************************************************
! Modules:

      implicit none

      integer :: NFragments
      integer :: Nseed_atoms
      integer, dimension(:), allocatable :: Atom_to_fragment
      integer, dimension(:), allocatable :: Electron_by_fragment
      integer, dimension(:), allocatable :: Basis_to_fragment
      integer, dimension(:), allocatable :: SEED_atoms

      end MODULE type_fragments
