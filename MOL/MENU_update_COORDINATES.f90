      subroutine MENU_update_COORDINATES
!***********************************************************************
!     Date last modified: June 24, 1992                                *
!     Author: R. A. Poirier                                            *
!     Description: .                                                   *
!***********************************************************************
!Modules:
      USE program_files
      USE program_parser
      USE type_molecule

      implicit none
!
! Local scalars:
      integer NatomsW
      logical done,LIerror
!
      type (cartesian_coordinates), dimension(:), allocatable :: XYZC_work
!
! Begin:
      call PRG_manager ('enter', 'MENU_update_COORDINATES', 'UTILITY')
!
! Allocate work arrays:
      allocate (XYZC_work(1:Natoms))
!
! Menu:
      done=.false.
      do while (.not.done)
      call NEW_token (' update:')
!
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      ' Command update:', &
      '   Purpose: Update the Z-matrix from given cartesians ', &
      '   Syntax :', &
      '     CArtesian  <command> ', &
      '     OPT  <command> for updating optimization parameters', &
      '   end'
!
! CARtesians
      else if(token(1:3).EQ.'CAR')then
        call CARTESIAN_input (XYZC_work, Natoms, Zmatrix%units, NatomsW)
        CARTESIAN(1:Natoms)%X=XYZC_work(1:Natoms)%X
        CARTESIAN(1:Natoms)%Y=XYZC_work(1:Natoms)%Y
        CARTESIAN(1:Natoms)%Z=XYZC_work(1:Natoms)%Z
        call BLD_Zmatrix (LIerror)
        write(UNIout,'(a)') &
             'Updated Z-matrix using the cartesians provided:'
        call PRT_FZM
        call PRT_XYZ
      else if(token(1:3).EQ.'OPT')then
        call OPT_param_input
      else
        call MENU_end (done)

      end if
!
      end do !(.not.done)

      deallocate (XYZC_work)
!
! End of routine MENU_update_COORDINATES
      call PRG_manager ('exit', 'MENU_update_COORDINATES', 'UTILITY')
      return
      end subroutine MENU_update_COORDINATES
