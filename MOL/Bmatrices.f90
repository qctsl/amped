      subroutine BLD_Bmatrix_ZM
!****************************************************************************************************************************
!     Date last modified: February 28, 2000                                                                    Version 2.0  *
!     Author: R.A. Poirier                                                                                                  *
!     Description: B matrices (carstesians to ZM internal)                                                                  *
!     The B Matrix elements are as defined in  D.F. McIntosh, K. Michaelian and M.R. Peterson, Can. J. Chem., 56, 1289 (1978)
!     The programs BSTRETCH, BENDNG, TORSN and LIBend are essentially those of QCPE Program 342, by D.F. McIntosh and       *
!     M.R. Peterson.                                                                                                        *
!****************************************************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE matrix_print

      implicit none
!
! Local scalars:
      integer I,IER,J,K,NOB,N1,N2,N3,N4
      double precision COEFF
!
! Local array:
      double precision A(3)
!
! Begin:
      call PRG_manager ('enter', 'BLD_Bmatrix_ZM', 'BMATRIX%ZM')
!
! Object:
      if(.not.allocated(Bmatrix_ZM))then
        allocate (Bmatrix_ZM(NMCOOR,3*Natoms))
        NICOOR=NMCOOR
      else
        deallocate (Bmatrix_ZM)
        allocate (Bmatrix_ZM(NMCOOR,3*Natoms))
        NICOOR=NMCOOR
      end if
!
      Bmatrix=>Bmatrix_ZM

      COEFF=ONE    ! Always one for Z-Matrix

      Bmatrix_ZM=ZERO
!
      do I=2,Natoms
! Bond Stretch.
      N1=Zmatrix%atoms(I)%atom1
      NOB=I-1
      call BSTRETCH (NOB, N1, I, NMCOOR, COEFF)

      IF(I.GT.2)then ! Bond Angle 1.
      N2=Zmatrix%atoms(I)%atom2
      N3=Zmatrix%atoms(I)%atom3
      N4=Zmatrix%atoms(I)%atom4
      NOB=Natoms+I-3
      IER=0
      call BENDNG (NOB, N2, N1, I, IER, NMCOOR, COEFF)
      IF(IER.EQ.0)then
! Ensure that Alpha angle lies between 0 and 180 degrees.
! If not, reverse B Matrix elements.
      IF(DSIN(Zmatrix%angle(I)%value*Deg_to_Radian).LT.ZERO)then
      do J=1,3*Natoms
        Bmatrix_ZM(NOB,J)=-Bmatrix_ZM(NOB,J)
      end do ! J
      end if ! DSIN(Zmatrix%angle(I)%value*Deg_to_Radian).LT.ZERO
!
! Bond Angle is linear - Use Linear Bend. Use atom N3 as reference.
! Watch out in case N3 was not specified.
      else
!
      IF(N3.GT.0)then
         A(1)=CARTESIAN(N3)%X
         A(2)=CARTESIAN(N3)%Y
         A(3)=CARTESIAN(N3)%Z
      else
! N3 was not specified - Use X axis if this is a 3-atom molecule.
        IF(Natoms.NE.3)GO TO 700
        A(1)=ONE
        A(2)=ZERO
        A(3)=ZERO
      end if ! N3.GT.0
!
      IER=0
! Use in-plane component for torsions, out-of-plane component for
! Euler Angle specification of the geometry.
      J=1
      IF(IABS(N4).EQ.1)J=2
      call LIBend (NOB, N2, N1, I, A, J, IER, NMCOOR)
      IF(IER.NE.0)GO TO 700
      end if ! IER.EQ.0
!
      IF(I.EQ.3)GO TO 590
      IF(N3.EQ.0)GO TO 700
      NOB=NOB+Natoms-3
!
      IF(N4.EQ.0)then ! Torsion.
        call B_Torsion (NOB, I, N1, N2, N3, IER, NMCOOR)
        if(IER.GT.0)then ! Torsion was linear - Get perpendicular Linear Bend.
           A(1)=CARTESIAN(N3)%X
           A(2)=CARTESIAN(N3)%Y
           A(3)=CARTESIAN(N3)%Z
           IER=0
           call LIBend (NOB, N2, N1, I, A, 2, IER, NMCOOR)
         end if ! IER.GT.0
         IF(IER.LT.0)GO TO 700
         GO TO 590
      end if ! N4.EQ.0
!
! Second Euler Angle.
      IF(IABS(N4).NE.1)GO TO 700
      call BENDNG (NOB, N3, N1, I, IER, NMCOOR, COEFF)
      IF(IER.EQ.0)GO TO 590
! Euler Angle was linear - Use Linear Bend (perpendicular comp.).
      A(1)=CARTESIAN(N2)%X
      A(2)=CARTESIAN(N2)%Y
      A(3)=CARTESIAN(N2)%Z
      IER=0
      call LIBend (NOB, N3, N1, I, A, 2, IER, NMCOOR)
      IF(IER.NE.0)GO TO 700
      end if ! I.GT.2

  590 continue
      end do ! I
!
  600 continue
!
! End of routine BLD_Bmatrix_ZM
      call PRG_manager ('exit', 'BLD_Bmatrix_ZM', 'BMATRIX%ZM')
      return
!
! Error exit.
!
  700 continue
! Error defining an internal coordinate.
      write(UNIout,'(A,I4)')'ERROR> BLD_Bmatrix_ZM: ERROR DEFINING B MATRIX FOR Z-MATRIX INTERNAL COORDINATE ',NOB
!      OPTEND=.TRUE.
      GO TO 600
!
 1190 FORMAT('1B MATRIX (Z-MATRIX INTERNAL X CARTESIAN COORDINATES)'//)
!
      end subroutine BLD_Bmatrix_ZM
      subroutine BLD_Bmatrix_RIC
!****************************************************************************
! Date last modified: 1 April, 2000                             Version 1.0 *
! Author: Michelle Shaw                                                     *
! Description: Builds b-matrix for RIC system (MM coords).  Modification of *
!              existing routine BPIBL8 written by R. A. Poirier (for PIC).  *
!****************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE matrix_print
      USE OPT_defaults
      USE OPT_objects

      implicit none
!
! Local scalars:
      integer A,B,C,D,iSTR,iBND,iTOR,iOBND,iELEC,IER,iCOOR,J,Ninternal,Ntotal
      double precision C0
!
! Local functions:
      double precision GET_angle
!
! Begin:
      call PRG_manager ('enter', 'BLD_Bmatrix_RIC', 'BMATRIX%RIC')

! Object:
      if(OPT_coord%nonbonded)then
        Ninternal=RIC%NIcoordinates
      else
        Ninternal=RIC%NIcoordinates-RIC%Nnonbonded
      end if

      if(.not.allocated(Bmatrix_RIC))then
        allocate (Bmatrix_RIC(Ninternal, 3*Natoms))
      else
        deallocate (Bmatrix_RIC)
        allocate (Bmatrix_RIC(Ninternal, 3*Natoms))
      end if

      Bmatrix => Bmatrix_RIC

      Bmatrix_RIC = ZERO
      C0 = ONE

      Ntotal=RIC%Nbonds
! B-matrix elements (s-vectors) for bond stretches:
      iCOOR = 0
      do iSTR=1,RIC%Nbonds
        iCOOR = iCOOR + 1
        A = RIC%bond(iSTR)%atom1
        B = RIC%bond(iSTR)%atom2
        call BSTRETCH (iCOOR, A, B,  Ninternal, C0)
      end do !stretches

! B-matrix elements (s-vectors) for angle bends:
      if(RIC%Nangles.gt.0) then
        do iBND =1,RIC%Nangles
          iCOOR = Ntotal + iBND
          A = RIC%angle(iBND)%atom1
          B = RIC%angle(iBND)%atom2
          C = RIC%angle(iBND)%atom3
          call BENDNG (iCOOR, A, B, C, IER, Ninternal, C0)
          if(IER.ne.0)then
            write(UNIout,*)'error in BENDNG.'
            stop 'error in BENDNG.'
          else ! Ensure that Alpha angle lies between 0 and 180 degrees, if not, reverse B Matrix elements.
            IF(DSIN(GET_angle(A,B,C)).LT.ZERO)then
!            IF(DSIN(GET_angle(A,B,C)*Deg_to_Radian).LT.ZERO) then
            write(UNIout,*)'BENDNG> Changed sign of angle'
            do J=1,3*Natoms
              Bmatrix_RIC(iCOOR,J)=-Bmatrix_RIC(iCOOR,J)
            end do ! J
            end if
          end if
        end do ! bends
        Ntotal=Ntotal+RIC%Nangles
      end if

! B-matrix elements (s-vectors) for torsions:
      if(RIC%Ntorsions.gt.0) then
        do iTOR = 1,RIC%Ntorsions
          iCOOR = NTotal + iTOR
          A = RIC%torsion(iTOR)%atom1
          B = RIC%torsion(iTOR)%atom2
          C = RIC%torsion(iTOR)%atom3
          D = RIC%torsion(iTOR)%atom4
          call B_Torsion (iCOOR, A, B, C, D, IER, Ninternal)
          if(IER.ne.0)write(UNIout,*)'error in B_Torsion.'
        end do !torsions
        Ntotal=Ntotal+RIC%Ntorsions
      end if

! B-matrix elements (s-vectors) for out-of-plane bends:
      if(RIC%Noopbends.gt.0)then
        do iOBND = 1,RIC%Noopbends
          iCOOR = Ntotal + iOBND
          A = RIC%oopbend(iOBND)%atom1
          D = RIC%oopbend(iOBND)%atom2
          B = RIC%oopbend(iOBND)%atom3
          C = RIC%oopbend(iOBND)%atom4
          call BOOPBend (iCOOR, A, B, C, D, Ninternal, C0)
        end do !out-of-plane bends
        Ntotal=Ntotal+RIC%Noopbends
      end if

! Added nonbonded Bmatrix s-vector contributions (stretch-type):
      IF(OPT_function%name(1:2).EQ.'MM')then
      if(RIC%Nnonbonded.gt.0.and.OPT_coord%nonbonded)then
        do iELEC = 1,RIC%Nnonbonded
          iCOOR = Ntotal+iELEC
          A = RIC%nonbonded(iELEC)%atom1
          B = RIC%nonbonded(iELEC)%atom2
          call BSTRETCH (iCOOR, A, B, Ninternal, C0)
        end do !nonbonded interactions
        Ntotal=Ntotal+RIC%Nnonbonded
      end if
      end if

!
! End of subroutine BLD_Bmatrix_RIC
      call PRG_manager ('exit', 'BLD_Bmatrix_RIC', 'BMATRIX%RIC')
      return
      end subroutine BLD_Bmatrix_RIC
      subroutine BLD_Bmatrix_PIC
!****************************************************************************
! Date last modified: 1 April, 2000                             Version 1.0 *
! Author: Michelle Shaw                                                     *
! Description: Builds B-matrix for RIC system.  Modification of             *
!              existing routine BPIBL8 written by R. A. Poirier (for PIC).  *
!****************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE proper_internals
      USE matrix_print

      implicit none
!
! Local scalars:
      integer IER,IPICOR,J,J1,K,KA,KB,KC,KD,Ktype,L,L1,M1,M2,M3,M4,N,NT
      double precision CFI1,CFI2,CFI3,CO,CO2,CTETA,C0,C1,DEN,RM1,RM2,R1,R2,R3,SFI1,SI,SI2,STETA,ST2,ST3,T
!
! Local arrays:
      integer IA(4)
      double precision U(3),UU(3),UV(12),V(3),VV(3),W(3),WW(3),X(3),Z(3),ZZ(3)
!
! Local function:
!
      EQUIVALENCE (KA,IA(1)),(KB,IA(2)),(KC,IA(3)),(KD,IA(4)),(UV(1),UU(1)),(UV(4),VV(1)),(UV(7),WW(1)),(UV(10),ZZ(1))
!
! Begin:
      call PRG_manager ('enter', 'BLD_Bmatrix_PIC', 'BMATRIX%PIC')

! Object:
      if(PIC%Ncoordinates.eq.0)then
        write(UNIout,'(a)')'No Proper internal coordinates were define: Define them using the MENU'
        return
!       stop 'No Proper internal coordinates were define: Define them using the MENU'
      end if
      if(.not.allocated(Bmatrix_PIC))then
        allocate (Bmatrix_PIC(PIC%Ncoordinates, 3*Natoms))
      else
        deallocate (Bmatrix_PIC)
        allocate (Bmatrix_PIC(PIC%Ncoordinates, 3*Natoms))
      end if

      Bmatrix => Bmatrix_PIC
      NICOOR=PIC%Ncoordinates

      Bmatrix_PIC = ZERO
      C0 = ONE

      NT=0
      do IPICOR=1,PIC%Ncoordinates
        Ktype=PIC%type(IPICOR)

        C1=ZERO
        Bmatrix_PIC(IPICOR,1:3*Natoms)=ZERO

        do N=1,PIC%Ncomponents(IPICOR)
          NT=NT+1
          KA=PIC%atom1(NT)
          KB=PIC%atom2(NT)
          KC=PIC%atom3(NT)
          KD=PIC%atom4(NT)
          C0=PIC%coeff(NT)
          C1=C1+C0*C0

          select case (Ktype)
          case (1)                        ! Stretch.
            call BSTRETCH (IPICOR, KA, KB, PIC%Ncoordinates, C0)

          case (2)                    ! Inverse
            call VEC (UU, R1, KA, KB)
            RM1=ONE/R1
            RM2=RM1*RM1
            UU(1)=-RM2*UU(1)
            UU(2)=-RM2*UU(2)
            UU(3)=-RM2*UU(3)
              do L=1,3
                M1=3*(KA-1)+L
                M2=3*(KB-1)+L
                Bmatrix_PIC(IPICOR,M1)= UU(L)*C0+Bmatrix_PIC(IPICOR,M1)
                Bmatrix_PIC(IPICOR,M2)=-UU(L)*C0+Bmatrix_PIC(IPICOR,M2)
              end do ! L

          case (3)                    ! Bending A-B-C (B is the central atom).
            call BENDNG (IPICOR, KA, KB, KC, IER, PIC%Ncoordinates, C0)
            IF(IER.GT.0)then
              write(UNIout,*)'ERROR> BPIBL8: linear bond cannot be handled in this case'
              write(UNIout,*)'ATOMS: ',KA,KB,KC
              stop 'ERROR> BPIBL8: linear bond cannot be handled in this case'
            end if
!
          case (4)                    ! Out of Plane.
            call VEC (U, R1, KA, KD)
            call VEC (V, R2, KB, KD)
            call VEC (W, R3, KC, KD)
            call VEC_product (Z, V, W, T, 1)
            STETA=DOT_product (U,Z)
            CTETA=DSQRT(ONE-STETA*STETA)
            CFI1=DOT_product (V,W)
            SFI1=DSQRT(ONE-CFI1*CFI1)
            CFI2=DOT_product (W,U)
            CFI3=DOT_product (V,U)
            DEN=CTETA*SFI1**2
            ST2=(CFI1*CFI2-CFI3)/(R2*DEN)
            ST3=(CFI1*CFI3-CFI2)/(R3*DEN)
            do L=1,3
              VV(L)=Z(L)*ST2
              WW(L)=Z(L)*ST3
            end do ! L
            call VEC_product (X ,Z ,U, T, 1)
            call VEC_product (Z ,U ,X, T, 1)
            do L=1,3
              UU(L)=Z(L)/R1
              ZZ(L)=-UU(L)-VV(L)-WW(L)
            end do ! L
              do L=1,3
                M1=3*(KA-1)+L
                M2=3*(KB-1)+L
                M3=3*(KC-1)+L
                M4=3*(KD-1)+L
                Bmatrix_PIC(IPICOR,M1)=UU(L)*C0+Bmatrix_PIC(IPICOR,M1)
                Bmatrix_PIC(IPICOR,M2)=VV(L)*C0+Bmatrix_PIC(IPICOR,M2)
                Bmatrix_PIC(IPICOR,M3)=WW(L)*C0+Bmatrix_PIC(IPICOR,M3)
                Bmatrix_PIC(IPICOR,M4)=ZZ(L)*C0+Bmatrix_PIC(IPICOR,M4)
              end do ! L

          case (5)                    ! Torsion
            call VEC (U, R1, KA, KB)
            call VEC (V, R2, KC, KB)
            call VEC (W, R3, KC, KD)
            call VEC_product (Z, U, V, T, 1)
            call VEC_product (X, W, V, T, 1)
            CO=DOT_product (U,V)
            CO2=DOT_product (V,W)
            SI=DSQRT(ONE-CO*CO)
            SI2=DSQRT(ONE-CO2*CO2)
            do L=1,3
              UU(L)=Z(L)/(R1*SI)
              ZZ(L)=X(L)/(R3*SI2)
              VV(L)=(R1*CO/R2-ONE)*UU(L)-R3*CO2/R2*ZZ(L)
              WW(L)=-UU(L)-VV(L)-ZZ(L)
            end do ! L
            do L=1,3
              M1=3*(KA-1)+L
              M2=3*(KB-1)+L
              M3=3*(KC-1)+L
              M4=3*(KD-1)+L
              Bmatrix_PIC(IPICOR,M1)=UU(L)*C0+Bmatrix_PIC(IPICOR,M1)
              Bmatrix_PIC(IPICOR,M2)=VV(L)*C0+Bmatrix_PIC(IPICOR,M2)
              Bmatrix_PIC(IPICOR,M3)=WW(L)*C0+Bmatrix_PIC(IPICOR,M3)
              Bmatrix_PIC(IPICOR,M4)=ZZ(L)*C0+Bmatrix_PIC(IPICOR,M4)
            end do ! L

!**********Untested
!          else IF(Ktype.EQ.6)then                    ! Linear Coplanar Bending
! Remember that the range of this coordinate is -PI/2 to 3*PI/2 in order to shift the discontinuity off the planar position.
!            call VEC (U, R1, KA, KC)
!            call VEC (V, R2, KD, KC)
!            call VEC (X, R2, KB, KC)
!            call VEC_product (W, V, U, T, 1)
!            call VEC_product (Z, U, W, T, 1)
!            call VEC_product (W, X, V, T, 1)
!            call VEC_product (U, W, X, T, 1)
! Coordinate positive if atom A moves towards atom D.
!            do L=1,3
!              UU(L)=Z(L)/R1
!              VV(L)=U(L)/R2
!              WW(L)=-UU(L)-VV(L)
!            end do ! L
!          do J=1,4
!            ATOM=IA(J)
!            IF(ATOM.GT.0)then
!              ATOM=ATOM-1
!              J1=3*(J-1)
!              do L=1,3
!                M1=3*ATOM+L
!                L1=J1+L
!                Bmatrix_PIC(IPICOR,M1)=UV(L1)*C0+Bmatrix_PIC(IPICOR,M1)
!              end do ! L
!            end if ! IF(ATOM.GT.0)
!          end do ! J
!
!          else IF(Ktype.EQ.7)then                    ! Linear Perpendicular Bending
!            call VEC (U, R1, KA, KC)
!            call VEC (V, R2, KD, KC)
!            call VEC (Z, R2, KB, KC)
!            call VEC_product (W, V, U, T, 1)
!            call VEC_product (X, Z, V, T, 1)
!            do L=1,3
!              UU(L)=W(L)/R1
!              VV(L)=X(L)/R2
!              WW(L)=-UU(L)-VV(L)
!            end do ! L
!          do J=1,4
!            ATOM=IA(J)
!            IF(ATOM.GT.0)then
!              ATOM=ATOM-1
!              J1=3*(J-1)
!              do L=1,3
!                M1=3*ATOM+L
!                L1=J1+L
!                Bmatrix_PIC(IPICOR,M1)=UV(L1)*C0+Bmatrix_PIC(IPICOR,M1)
!              end do ! L
!            end if ! IF(ATOM.GT.0)
!          end do ! J
           case default
            write(UNIout,*)'ERROR> BPIBL8: Coordinate type = ',Ktype,' not supported'
            stop 'ERROR> BPIBL8: Coordinate type not supported'
          end select
!
        end do ! PIC%Ncomponents(IPICOR)

        C1=DSQRT(ONE/C1)*PIC%scale(IPICOR)
        do K=1,3*Natoms
          Bmatrix_PIC(IPICOR,K)=Bmatrix_PIC(IPICOR,K)*C1
        end do ! K
      end do ! IPICOR
!
! End of subroutine BLD_Bmatrix_PIC
      call PRG_manager ('exit', 'BLD_Bmatrix_PIC', 'BMATRIX%PIC')
      return
      end subroutine BLD_Bmatrix_PIC
      subroutine BSTRETCH (Icoord,  & ! Coordinate being defined
                         atom_A,  & ! Atom A
                         atom_B,  & ! Atom B
!                        Bmatrix,  & ! B-Matrix
                         NCOORD,  & ! Number of internal coordinates
                         COEFF)  ! Coefficient (Used for Natural Internal coordinates)
!****************************************************************************************************************************
!     Date last modified: January 18, 2000                                                                      Version 1.2 *
!     Author: M. R. Peterson, R. A. Poirier                                                                                 *
!     Description: Compute the B-Matrix elements for a Bond Stretch                                                         *
!                  as defined by Wilson.                                                                                    *
!****************************************************************************************************************************
! Modules:
      USE type_molecule

      implicit none
!
! Input scalars:
      integer atom_A,atom_B,NCOORD,Icoord
      double precision COEFF
!
! Input array:
!     double precision Bmatrix(NCOORD,3*Natoms)
!
! Local scalars:
      integer M
      double precision R1
!
! Local array:
      double precision EIJ(3)
!
! Local parameters:
      double precision TENM8
      parameter (TENM8=1.0D-8)
!
! Begin:
!      call PRG_manager ('enter', 'BSTRETCH', 'UTILITY')
!
      call VEC (EIJ, R1, atom_B, atom_A)
      do M=1,3
        IF(DABS(EIJ(M)).GE.TENM8) then
          Bmatrix(Icoord,3*(atom_A-1)+M)=-EIJ(M)*COEFF+Bmatrix(Icoord,3*(atom_A-1)+M)
          Bmatrix(Icoord,3*(atom_B-1)+M)= EIJ(M)*COEFF+Bmatrix(Icoord,3*(atom_B-1)+M)
        end if
      end do ! M
!
! End of routine BSTRETCH
!      call PRG_manager ('exit', 'BSTRETCH', 'UTILITY')
      return
      end subroutine BSTRETCH
      subroutine BENDNG (Icoord,  & ! Coordinate being defined
                         atom_A,  & ! atom A
                         atom_B,  & ! atom B
                         atom_C,  & ! atom C
                         IER,     & ! Error indicator
!                        Bmatrix,  & ! B-matrix
                         NCOORD,  & ! Number of internal coordinates
                         COEFF)  ! Coefficient
!****************************************************************************************************************************
!     Date last modified: February 28, 2000                                                                    Version 2.0  *
!     Author: R.A. Poirier                                                                                                  *
!     Description: Compute the B-Matrix elements of a Valence Angle Bending Coordinate as defined by Wilson.                *
!     I and K are the numbers of the end atoms.  J is the number of the central atom.                                       *
!****************************************************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule

      implicit none
!
! Input scalars:
      integer atom_A,IER,atom_B,atom_C,NCOORD,Icoord
      double precision COEFF
!
! Input arrays:
!     double precision Bmatrix(NCOORD,3*Natoms)
!
! Local scalars:
      integer M
      double precision DJI,DJK,DOTJ,SINJ,SMI,SMK,SUM
!
! Local arrays:
      double precision EJI(3),EJK(3)
!
! Local parameters:
      double precision PT9999,TENM10
      parameter (TENM10=1.0D-10,PT9999=0.99999D0)
!
! Begin:
!
      IER=0
      call VEC (EJI, DJI, atom_A, atom_B)
      call VEC (EJK, DJK, atom_C, atom_B)
!
      DOTJ=ZERO
      do M=1,3
        DOTJ=DOTJ+EJI(M)*EJK(M)
      end do ! M
!
      IF(DABS(DOTJ).GT.PT9999)then ! Switch to a linear bend as SIN(I-J-K) is close to 0.0.
        write(UNIout,*)'switching to linear bend'
        IER=1
      else

      SINJ=DSQRT(ONE-DOTJ*DOTJ)
!
      do M=1,3
        SMI=(DOTJ*EJI(M)-EJK(M))/(DJI*SINJ)
        IF(DABS(SMI).GE.TENM10)Bmatrix(Icoord,3*(atom_A-1)+M)=SMI*COEFF+Bmatrix(Icoord,3*(atom_A-1)+M)
        SMK=(DOTJ*EJK(M)-EJI(M))/(DJK*SINJ)
        IF(DABS(SMK).GE.TENM10)Bmatrix(Icoord,3*(atom_C-1)+M)=SMK*COEFF+Bmatrix(Icoord,3*(atom_C-1)+M)
        SUM=SMI+SMK
        IF(DABS(SUM).GE.TENM10)Bmatrix(Icoord,3*(atom_B-1)+M)=-SUM*COEFF+Bmatrix(Icoord,3*(atom_B-1)+M)
      end do ! M
!
      end if
!
! End of routine BENDNG
      return
      end subroutine BENDNG
      subroutine LIBend (NOB, &
                         I, &
                         J, &
                         K, &
                         A, &
                         IC, &
                         IER, &
!                        Bmatrix, &
                         NCOORD)
!****************************************************************************************************************************
!     Date last modified: February 28, 2000                                                                    Version 2.0  *
!     Author: M. R. Peterson and R.A. Poirier                                                                               *
!     Description: Compute the B-Matrix elements for a Linear Bend, or for a pair of perpendicular Linear Bends.            *
!     I and K are the end atoms; J is the central atom. A gives the Cartesian coordinates of a point in space, such         *
!     that the vector from atom J to point A is perpendicular to the line I-J-K and serves to orient the coordinates in space
!     IC = 1 : the in-plane component is returned.  IC = 2 : the out-of-plane (perpendicular) component is returned.        *
!****************************************************************************************************************************
! Modules:
      USE program_constants
      USE type_molecule

      implicit none
!
! Input scalars:
      integer I,IC,IER,J,K,NCOORD,NOB
!
! Input arrays:
      double precision A(3)
!     double precision Bmatrix(NCOORD,3*Natoms)
!
! Local scalars:
      integer II,JJ,KK,M
      double precision DJA,DJASQ,DJI,DJISQ,DJK,DJKSQ,DUP,DUPSQ,SMI,SMK,T
!
! Local arrays:
      double precision EJK(3),UN(3),UP(3)
!
! Local parameters:
      double precision TENM8
      parameter (TENM8=1.0D-8)
!
! Begin:
      call PRG_manager ('enter', 'LIBEND', 'UTILITY')
!
      DJASQ=ZERO
      DUPSQ=ZERO
      DJISQ=(CARTESIAN(I)%X-CARTESIAN(J)%X)**2+(CARTESIAN(I)%Y-CARTESIAN(J)%Y)**2+(CARTESIAN(I)%Z-CARTESIAN(J)%Z)**2
      EJK(1)=CARTESIAN(K)%X-CARTESIAN(J)%X
      EJK(2)=CARTESIAN(K)%Y-CARTESIAN(J)%Y
      EJK(3)=CARTESIAN(K)%Z-CARTESIAN(J)%Z
      DJKSQ=EJK(1)**2+EJK(2)**2+EJK(3)**2
      UN(1)=A(1)-CARTESIAN(J)%X
      UN(2)=A(2)-CARTESIAN(J)%Y
      UN(3)=A(3)-CARTESIAN(J)%Z
      DJI=DSQRT(DJISQ)
      DJK=DSQRT(DJKSQ)
! Make JA perpendicular to I-J-K.
      call VEC_product (UP, EJK, UN, T, 0)
      call VEC_product (UN, UP, EJK, T, 0)
!
      do M=1,3
        DJASQ=DJASQ+UN(M)*UN(M)
        DUPSQ=DUPSQ+UP(M)*UP(M)
      end do ! M
!
      IF(DJASQ.LE.TENM8)then
!       A is collinear with I-J-K.
        IER=-2
      else
!
      II=3*(I-1)
      JJ=3*(J-1)
      KK=3*(K-1)
      IF(IC.NE.2) then
      DJA=DSQRT(DJASQ)
      do M=1,3
        T=-UN(M)/DJA
        IF(DABS(T).GE.TENM8)then
        SMI=T/DJI
        Bmatrix(NOB,II+M)=SMI
        SMK=T/DJK
        Bmatrix(NOB,KK+M)=SMK
        Bmatrix(NOB,JJ+M)=-SMI-SMK
        end if
      end do ! M

      else
!
      DUP=DSQRT(DUPSQ)
      do M=1,3
        T=-UP(M)/DUP
        IF(DABS(T).GT.TENM8)then
        SMI=T/DJI
        Bmatrix(NOB,II+M)=SMI
        SMK=T/DJK
        Bmatrix(NOB,KK+M)=SMK
        Bmatrix(NOB,JJ+M)=-SMI-SMK
        end if
      end do ! M
!
      end if
      end if
!
! End of routine LIBEND
      call PRG_manager ('exit', 'LIBEND', 'UTILITY')
      return
      end subroutine LIBEND
      subroutine B_Torsion (Icoord,  & ! Coordinate being defined
                            atom_A,  & ! atom A
                            atom_B,  & ! atom B
                            atom_C,  & ! atom C
                            atom_D,  & ! atom D
                            IER, &
!                           Bmatrix, &
                            NCOORD)
!****************************************************************************************************************************
!     Date last modified: February 28, 2000                                                                    Version 2.0  *
!     Author: M. R. Peterson, R. A. Poirier                                                                                 *
!     Description: Compute the B-Matrix elements for Torsion, as defined by R.L. Hilderbrandt in                            *
!     J Molec. Spec., 44, 599 (1972).                                                                                       *
!     The code will handle entire group torsions, but is currently used only for simple 4 atom torsions (A-B-C-D).          *
!****************************************************************************************************************************
! Modules:
      USE program_constants
      USE type_molecule

      implicit none
!
! Input scalars:
      integer atom_A,atom_B,atom_C,atom_D,IER,NCOORD,Icoord
!
! Input arrays:
!     double precision Bmatrix(NCOORD,3*Natoms)
!
! Local scalars:
      integer M
      double precision COSJ,COSK,DIJ,DJK,DLK,SIN2J,SIN2K,SMI,SMJ,SMK,SML,T
!
! Local arrays:
      double precision CR(3),EIJ(3),EJK(3),ELK(3),SJ(3),SK(3)
!
! Local parameters:
      double precision PT9999,TENM8
      parameter (TENM8=1.0D-8,PT9999=0.99999D0)
!
! Begin:
!      call PRG_manager ('enter', 'B_Torsion', 'UTILITY')
!
      call VEC (EJK, DJK, atom_C, atom_B)
      DJK=ONE/DJK
!
      call VEC (EIJ, DIJ, atom_B, atom_A)
      DIJ=ONE/DIJ

      COSJ=ZERO
      do M=1,3
        SJ(M)=ZERO
        SK(M)=ZERO
        COSJ=COSJ-EIJ(M)*EJK(M)
      end do ! M
!
      IF(DABS(COSJ).GT.PT9999)then          ! ATOMS I-J-K ARE COLLINEAR - TRY LINEAR BEND
        IER = 1
!        call PRG_manager ('exit', 'B_Torsion', 'UTILITY')
        return
      end if

      SIN2J=(ONE-COSJ*COSJ)
      call VEC_product (CR, EIJ, EJK, T, 0)
      do M=1,3
        T=CR(M)/SIN2J
        SMI=T*DIJ
        IF(DABS(SMI).GE.TENM8)Bmatrix(Icoord,3*(atom_A-1)+M)=-SMI
        SMK=T*COSJ*DJK
        SK(M)=SK(M)+SMK
        SJ(M)=SJ(M)+SMI-SMK
      end do ! M
!
      call VEC (ELK, DLK, atom_C, atom_D)
      DLK=ONE/DLK

      COSK=ZERO
      do M=1,3
        COSK=COSK+EJK(M)*ELK(M)
      end do ! M

      IF(DABS(COSK).GT.PT9999)then            ! FATAL ERROR - ATOMS J-K-L ARE COLLINEAR
        IER = -2
!        call PRG_manager ('exit', 'B_Torsion', 'UTILITY')
        return
      end if

      SIN2K=(ONE-COSK*COSK)
      call VEC_product (CR, EJK, ELK, T, 0)
      do M=1,3
        T=CR(M)/SIN2K
        SML=T*DLK
        IF(DABS(SML).GE.TENM8)Bmatrix(Icoord,3*(atom_D-1)+M)=-SML
        SMJ=T*COSK*DJK
        SJ(M)=SJ(M)+SMJ
        SK(M)=SK(M)+SML-SMJ
      end do ! M
!
      do M=1,3
        IF(DABS(SJ(M)).GE.TENM8)Bmatrix(Icoord,3*(atom_B-1)+M)=SJ(M)
        IF(DABS(SK(M)).GE.TENM8)Bmatrix(Icoord,3*(atom_C-1)+M)=SK(M)
      end do ! M
!
! End of routine B_Torsion
!      call PRG_manager ('exit', 'B_Torsion', 'UTILITY')
      return
      end subroutine B_Torsion
      subroutine B_GTorsion (Icoord,  & ! Coordinate being defined
                            atom_A,  & ! atom A
                            atom_B,  & ! atom B
                            atom_C,  & ! atom C
                            atom_D,  & ! atom D
                            IER, &
!                           Bmatrix, &
                            NCOORD)
!****************************************************************************************************************************
!     Date last modified: February 28, 2000                                                                    Version 2.0  *
!     Author: M. R. Peterson and R.A. Poirier                                                                               *
!     Description: Compute the B-Matrix elements for Torsion, as defined by R.L. Hilderbrandt in                            *
!     J Molec. Spec., 44, 599 (1972).                                                                                       *
!     The code will handle entire group torsions, but is currently used only for simple 4 atom torsions (A-B-C-D).          *
!     B and C define the bond under torsion; NI and NL are the number of atoms attached at B and C, respectively.           *
!     Iatom,Katom: Atom numbers for the A- and D-type atoms (Size:5). NOTE: Group torsions is disabled                      *
!****************************************************************************************************************************
! Modules:
      USE program_constants
      USE type_molecule

      implicit none
!
! Input scalars:
      integer atom_A,atom_B,atom_C,atom_D,IER,NCOORD,Icoord
!
! Input arrays:
!     double precision Bmatrix(NCOORD,3*Natoms)
!
! Local scalars:
      integer M,N
      double precision COSJ,COSK,DIJ,DJK,DLK,SIN2J,SIN2K,SMI,SMJ,SMK,SML,T
!
! Local arrays:
!      integer Iatom(5),Katom(5)
      double precision CR(3),EIJ(3),EJK(3),ELK(3),SJ(3),SK(3)
!
! Local parameters:
      integer NI,NL
      double precision PT9999,TENM8
      parameter (NI=1,NL=1)
      parameter (TENM8=1.0D-8,PT9999=0.99999D0)
!
! Begin:
      call PRG_manager ('enter', 'B_GTorsion', 'UTILITY')
!
      call VEC (EJK, DJK, atom_C, atom_B)
      DJK=ONE/DJK
!
! Loop over the I-type atoms.
      do N=1,NI
!      atom_A=Iatom(N)
      call VEC (EIJ, DIJ, atom_B, atom_A)
      DIJ=ONE/DIJ

      COSJ=ZERO
      do M=1,3
        SJ(M)=ZERO
        SK(M)=ZERO
        COSJ=COSJ-EIJ(M)*EJK(M)
      end do ! M
!
      IF(DABS(COSJ).GT.PT9999)then          ! ATOMS I-J-K ARE COLLINEAR - TRY LINEAR BEND
        IER = 1
        call PRG_manager ('exit', 'B_GTorsion', 'UTILITY')
        return
      end if

      SIN2J=(ONE-COSJ*COSJ)*DBLE(NI)
      call VEC_product (CR, EIJ, EJK, T, 0)
      do M=1,3
        T=CR(M)/SIN2J
        SMI=T*DIJ
        IF(DABS(SMI).GE.TENM8)Bmatrix(Icoord,3*(atom_A-1)+M)=-SMI
        SMK=T*COSJ*DJK
        SK(M)=SK(M)+SMK
        SJ(M)=SJ(M)+SMI-SMK
      end do ! M
      end do ! N
!
! Loop over the L-type atoms.
      do N=1,NL
!      atom_D=Katom(N)
      call VEC (ELK, DLK, atom_C, atom_D)
      DLK=ONE/DLK

      COSK=ZERO
      do M=1,3
        COSK=COSK+EJK(M)*ELK(M)
      end do ! M

      IF(DABS(COSK).GT.PT9999)then            ! FATAL ERROR - ATOMS J-K-L ARE COLLINEAR
        IER = -2
        call PRG_manager ('exit', 'B_GTorsion', 'UTILITY')
        return
      end if

      SIN2K=(ONE-COSK*COSK)*DBLE(NL)
      call VEC_product (CR, EJK, ELK, T, 0)
      do M=1,3
        T=CR(M)/SIN2K
        SML=T*DLK
        IF(DABS(SML).GE.TENM8)Bmatrix(Icoord,3*(atom_D-1)+M)=-SML
        SMJ=T*COSK*DJK
        SJ(M)=SJ(M)+SMJ
        SK(M)=SK(M)+SML-SMJ
      end do ! M
      end do ! N
!
      do M=1,3
        IF(DABS(SJ(M)).GE.TENM8)Bmatrix(Icoord,3*(atom_B-1)+M)=SJ(M)
        IF(DABS(SK(M)).GE.TENM8)Bmatrix(Icoord,3*(atom_C-1)+M)=SK(M)
      end do ! M
!
! End of routine B_GTorsion
      call PRG_manager ('exit', 'B_GTorsion', 'UTILITY')
      return
      end subroutine B_GTorsion
      subroutine BOOPBend (iCOOR, A, B, C, D, &
!                          Bmatrix, &
                           Ninternal, C0)
!************************************************************************************
! Date last modified: 1 April, 2000                                     Version 1.0 *
! Author: Michelle Shaw                                                             *
! Description: Computes B-Matrix elements for out-of-plane bends (based on code     *
!              written by R. A. Poirier for proper internal coordinates).           *
!************************************************************************************
! Modules:
      USE program_constants
      USE type_molecule

      implicit none
!
! Input scalars:
      integer A, B, C, D, iCOOR, Ninternal
      double precision C0

! Input arrays:
!     double precision Bmatrix(Ninternal, 3*Natoms)

! Local Scalars:
      integer xyzcomp, compA, compD, compB, compC
      double precision sin_theta, cos_theta, sin_phiI, cos_phiI, cos_phiK, cos_phiL, denom_sKL, &
                       term2_sK, term2_sL, RU, RV, RW, RZ, RX
!
! Local arrays:
      double precision U(3), V(3), W(3), X(3), Z(3), UU(3), VV(3), WW(3), ZZ(3)
!
! Begin:
      call PRG_manager ('enter', 'BOOPBEND', 'UTILITY')

      call VEC (U, RU, A, D)
      call VEC (V, RV, B, D)
      call VEC (W, RW, C, D)
      call VEC_product (Z, V, W, RZ, 1)
      sin_theta=DOT_Product(U,Z)
      cos_theta=DSQRT(ONE-sin_theta*sin_theta)
      cos_phiI=DOT_Product (V,W)
      sin_phiI=DSQRT(ONE-cos_phiI*cos_phiI)
      cos_phiK=DOT_Product (W,U)
      cos_phiL=DOT_Product (V,U)
      denom_sKL=cos_theta*sin_phiI**2
      term2_sK=(cos_phiI*cos_phiK-cos_phiL)/(RV*denom_sKL)
      term2_sL=(cos_phiI*cos_phiL-cos_phiK)/(RW*denom_sKL)
      do XYZComp = 1, 3
        VV(XYZCOMP)=Z(XYZCOMP)*term2_sK
        WW(XYZCOMP)=Z(XYZCOMP)*term2_sL
      end do
      call VEC_product (X, Z, U, RX, 1)
      call VEC_product (Z, U, X, RU, 1)
      do XYZCOMP = 1, 3
        UU(XYZCOMP)=Z(XYZCOMP)/RU
        ZZ(XYZCOMP)=-UU(XYZCOMP)-VV(XYZCOMP)-WW(XYZCOMP)
      end do
      do XYZCOMP = 1, 3
        compA = 3*(A-1)+XYZCOMP
        compD = 3*(D-1)+XYZCOMP
        compB = 3*(B-1)+XYZCOMP
        compC = 3*(C-1)+XYZCOMP
        Bmatrix(iCOOR,compA)=UU(XYZCOMP)*C0+Bmatrix(iCOOR,compA)
        Bmatrix(iCOOR,compD)=VV(XYZCOMP)*C0+Bmatrix(iCOOR,compD)
        Bmatrix(iCOOR,compB)=WW(XYZCOMP)*C0+Bmatrix(iCOOR,compB)
        Bmatrix(iCOOR,compC)=ZZ(XYZCOMP)*C0+Bmatrix(iCOOR,compC)
      end do

! End of subroutine BOOPBEND
      call PRG_manager ('exit', 'BOOPBEND', 'UTILITY')
      return
      end subroutine BOOPBEND
      subroutine PRT_Bmatrix (nameIN)
!***************************************************************************
!     Date last modified: November 8, 2001                     Version 2.0 *
!     Author: R.A. Poirier                                                 *
!     Description: Print the B-Matrix                                      *
!***************************************************************************
! Modules:
      USE program_files
      USE type_molecule
      USE matrix_print

      implicit none
!
! Local scalars:
      integer NXYZ
      character(len=*) nameIN
!
! Begin:
      call PRG_manager ('enter', 'PRT_Bmatrix', 'UTILITY')
!
! Internal Coordinates - Set up the B Matrix.
!
! Using Z-Matrix.
      IF(index(nameIN,'%ZM').NE.0)then
      call GET_object ('COORDINATES:BMATRIX%ZM')
      write(UNIout,'(a)')'Z-Matrix B-Matrix (internal X cartesian coordinates)'

      else IF(index(nameIN,'%RIC').NE.0)then
      call GET_object ('COORDINATES:BMATRIX%RIC')
      write(UNIout,'(a)')'RIC B-Matrix (internal X cartesian coordinates)'
!
! Proper internal coordinates used, repeat.
      else IF(index(nameIN,'%PIC').NE.0)then
      call GET_object ('COORDINATES:BMATRIX%PIC')
      write(UNIout,'(a)')'PIC B-Matrix (internal X cartesian coordinates)'

       else
         write(UNIout,*)'PRT_Bmatrix> No B-matrix for specified coordinates'
         stop 'PRT_Bmatrix> No B-matrix for specified coordinates'
      end if

      NICOOR=size(Bmatrix,1)
      NXYZ=size(Bmatrix,2)
      call PRT_matrix (Bmatrix, NICOOR, NXYZ)
!
! End of routine PRT_Bmatrix
      call PRG_manager ('exit', 'PRT_Bmatrix', 'COORDINATES:BMATRIX')
      return
      END
      subroutine KILL_Bmatrix (nameIN)
!***************************************************************************
!     Date last modified: November 8, 2001                     Version 2.0 *
!     Author: R.A. Poirier                                                 *
!     Description: Kill the B-Matrix                                       *
!***************************************************************************
! Modules:
      USE program_files
      USE type_molecule

      implicit none
!
! Local scalars:
      character(len=*) nameIN
!
! Begin:
      call PRG_manager ('enter', 'KILL_Bmatrix', 'UTILITY')
!
! Internal Coordinates - Set up the B Matrix.
! Using Z-Matrix.
      IF(index(nameIN,'%ZM').NE.0)then
        write(UNIout,'(a)')'Z-Matrix B-Matrix KILLED'
        deallocate (Bmatrix_ZM)

      else IF(index(nameIN,'%RIC').NE.0)then
        write(UNIout,'(a)')'RIC B-Matrix KILLED'
        deallocate (Bmatrix_RIC)
!
! Proper internal coordinates used, repeat.
      else IF(index(nameIN,'%PIC').NE.0)then
        write(UNIout,'(a)')'PIC B-Matrix KILLED'
        deallocate (Bmatrix_PIC)

       else
         write(UNIout,*)'KILL_Bmatrix> No B-matrix for specified coordinates - NOT killed'
         stop 'KILL_Bmatrix> B-matrix NOT killed'
      end if
!
! End of routine KILL_Bmatrix
      call PRG_manager ('exit', 'KILL_Bmatrix', 'COORDINATES:BMATRIX')
      return
      END
      subroutine OBJ_Bmatrix (nameIN, Command)
!***************************************************************************
!     Date last modified: November 8, 2001                     Version 2.0 *
!     Author: R.A. Poirier                                                 *
!     Description: Kill the B-Matrix                                       *
!***************************************************************************
! Modules:
      USE program_files
      USE type_molecule
      USE matrix_print

      implicit none
!
!Input scalars:
      character(len=*) nameIN,Command
!
! Local scalars:
!
! Begin:
      call PRG_manager ('enter', 'OBJ_Bmatrix', 'UTILITY')
!
      COMMAND_name: select case (Command)
      case ('KILL')
! Internal Coordinates - Set up the B Matrix.
! Using Z-Matrix.
      IF(index(nameIN,'%ZM').NE.0)then
        write(UNIout,'(a)')'Z-Matrix B-Matrix KILLED'
        deallocate (Bmatrix_ZM)

      else IF(index(nameIN,'%RIC').NE.0)then
        write(UNIout,'(a)')'RIC B-Matrix KILLED'
        deallocate (Bmatrix_RIC)
!
! Proper internal coordinates used, repeat.
      else IF(index(nameIN,'%PIC').NE.0)then
        write(UNIout,'(a)')'PIC B-Matrix KILLED'
        deallocate (Bmatrix_PIC)

       else
         write(UNIout,*)'OBJ_Bmatrix> No B-matrix for specified coordinates - NOT killed'
         stop 'OBJ_Bmatrix> B-matrix NOT killed'
      end if

      case ('PRINT')
        write(UNIout,'(a)')'B-Matrix (internal X cartesian coordinates)'
        call PRT_matrix (Bmatrix, size(Bmatrix,1), size(Bmatrix,2))
      case ('FREE')

      end select COMMAND_name
!
! End of routine OBJ_Bmatrix
      call PRG_manager ('exit', 'OBJ_Bmatrix', 'UTILITY')
      return
      END
