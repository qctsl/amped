      subroutine MENU_RIC
!****************************************************************************************************************************
!     Date last modified: April 10, 2001                                                                       Version 2.0  *
!     Author: R.A. Poirier                                                                                                  *
!     Description: Set up Redundant Internal Coordinates (RIC's).                                                           *
!****************************************************************************************************************************
! Modules:
      USE program_files
      USE program_parser
      USE type_molecule
      USE redundant_coordinates
      USE OPT_objects

      implicit none
!
! Local scalar:
      logical done
!
! Begin:
      call PRG_manager ('enter', 'MENU_RIC', 'UTILITY')
      done=.false.
!
! Initialize
      OPT_parameters%name='RIC'
!
! Menu:
      do while (.not.done)
      call new_token (' RICoord:')
!
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      ' Command RIC :', &
      '   Purpose: Set up proper internal coordinates (RIC) ', &
      '   Syntax :', &
      '   end'
!
      else
        call MENU_end (done)
      end if
!
      end do ! .not.done

      call get_object ('MOL', 'INTERNAL', 'RIC')
      call PRT_RIC
!
! End of routine MENU_RIC
      call PRG_manager ('exit', 'MENU_RIC', 'UTILITY')
      return
      end subroutine MENU_RIC
      subroutine PRT_RIC
!***********************************************************************
!     Date last modified: May 10, 1995                     Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!***********************************************************************
! Modules:
      USE program_files
      USE program_manager
      USE type_molecule
      USE program_constants
      USE GRAPH_objects
      USE OPT_defaults
      USE OPT_objects

      implicit none
!
! Local scalars:
      integer Icoord,Itotal
!
! Begin:
      call PRG_manager ('enter', 'PRT_RIC', 'UTILITY')
!
      write(UNIout,2000)
 2000 FORMAT('REDUNDANT INTERNAL COORDINATES(new):'/ &
              80('-')/3X,'Number',4X,'PARAMETER',5X,'ATOMS'/80('-'))
!
      Itotal=0
      write(UNIout,*)'Stretches:'
      do Icoord=1,RIC%Nbonds
        Itotal=Itotal+1
        write(UNIout,'(2i8,2x,F12.6,2I8)') &
              Itotal,Icoord,RIC%bond(Icoord)%value*Bohr_to_Angstrom, &
              RIC%bond(Icoord)%atom1,RIC%bond(Icoord)%atom2
      end do ! Icoord

      if(RIC%Nangles.gt.0) then
      write(UNIout,*)'Bends:'
      do Icoord=1,RIC%Nangles
        Itotal=Itotal+1
        write(UNIout,'(2i8,2x,F12.6,3I8)')Itotal,Icoord,RIC%angle(Icoord)%value/Deg_to_Radian, &
        RIC%angle(Icoord)%atom1,RIC%angle(Icoord)%atom2,RIC%angle(Icoord)%atom3
      end do ! Icoord
      end if

      if(RIC%Ntorsions.gt.0) then
      write(UNIout,*)'Torsions:'
      do Icoord=1,RIC%Ntorsions
        Itotal=Itotal+1
        write(UNIout,'(2i8,2x,F12.6,4I8)')Itotal,Icoord,RIC%torsion(Icoord)%value/Deg_to_Radian, &
        RIC%torsion(Icoord)%atom1,RIC%torsion(Icoord)%atom2,RIC%torsion(Icoord)%atom3,RIC%torsion(Icoord)%atom4
      end do ! Icoord
      end if

      if(RIC%Noopbends.gt.0)then
      write(UNIout,*)'Out-of-plane Bends:'
      do Icoord=1,RIC%Noopbends
        Itotal=Itotal+1
        write(UNIout,'(2i8,2x,F12.6,4I8)')Itotal,Icoord,RIC%oopbend(Icoord)%value/Deg_to_Radian, &
        RIC%oopbend(Icoord)%atom1,RIC%oopbend(Icoord)%atom2,RIC%oopbend(Icoord)%atom3,RIC%oopbend(Icoord)%atom4
      end do ! Icoord
      end if

      if(OPT_function%name(1:2).eq.'MM')then
      if(RIC%Nnonbonded.gt.0)then
      write(UNIout,*)'Non-Bonded:'
      do Icoord=1,RIC%Nnonbonded
        Itotal=Itotal+1
        RIC%nonbonded(Icoord)%label='Electronic '
        if(PAIR_IJ(Icoord)%is_vdw.eq.1)then
          RIC%nonbonded(Icoord)%label='van der Waals '
        else if(PAIR_IJ(Icoord)%is_tors.eq.1)then
          RIC%nonbonded(Icoord)%label='torsion '
        end if
        write(UNIout,'(2i8,2x,F12.6,2I8,1x,A14)')Itotal,Icoord,RIC%nonbonded(Icoord)%value*Bohr_to_Angstrom, &
              RIC%nonbonded(Icoord)%atom1,RIC%nonbonded(Icoord)%atom2,RIC%nonbonded(Icoord)%label
      end do ! Icoord
      end if
      end if
!
      write(UNIout,1080)
 1080 FORMAT(80('-'))
      write(UNIout,'(a,I8)')'Total number of Redundant Internal Coordinates: ',RIC%NIcoordinates
!
! End of routine PRT_RIC
      call PRG_manager ('exit', 'PRT_RIC', 'UTILITY')
      return
      end subroutine PRT_RIC
