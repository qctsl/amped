      MODULE GRAPH_objects
!*****************************************************************************************************************
!     Date last modified: March 24, 2004                                                            Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Topology related objects.                                                                     *
!*****************************************************************************************************************
! Modules:

      implicit none
!
! Abbreviations used:
!     Vx = Vertex
!     HMRpG = Homeomorphically Reduced pseudo-Graph
!     PG = Pruned Graph
!
! Scalars:
      integer :: NVertices ! Number of vertices in Graph (=Natoms)
      integer :: NComponents
      integer :: NEDGE
      integer :: NRING
      integer :: NRING_assembly
      integer :: NGBonds
      integer :: NGANGLE
      integer :: NGTORSION
      integer :: NPRUNE     ! number of atoms pruned graph
      integer :: NATHRP     ! number of atoms pruned HOMRED graph
      integer :: MXEDGR
      integer :: DIMLST
      integer :: NvWBond
      integer :: NOOPBEND
      integer :: NVderWBond
      integer :: Nnonbond
      integer :: NatomTypes
      integer :: NRingAtoms ! Number of ring atoms

      integer :: NELEC

!
      type pair
        integer :: I
        integer :: J
        integer :: is_vdw
        integer :: is_elec
        integer :: is_tors
      end type pair

      type (pair), dimension(:), allocatable :: pair_IJ
!
! Dimensioned Natoms:
      integer, dimension(:,:), allocatable :: CONNECT ! bonded atoms
      integer, dimension(:), allocatable :: CONVAL    ! Valence of atoms
      integer, dimension(:,:), allocatable :: Ring_Connect ! bonded atoms belonging to rings
      integer, dimension(:), allocatable :: Ring_valencies    ! Valence of atoms belonging to rings
!     integer, dimension(:,:), allocatable :: RCONNECT ! bonded atoms belonging to rings
!     integer, dimension(:), allocatable :: RVAL    ! Valence of atoms belonging to rings
      integer, dimension(:,:), allocatable :: CONTACT ! vderW contacts
      integer, dimension(:,:), allocatable :: ICOCON  ! Connects components, if any unconnected
      logical, dimension(:,:), allocatable :: CONN_NB ! true if atoms are NOT bonded
      logical, dimension(:), allocatable :: LRingVertex ! true if vertex is a part of a ring

! Components:
      integer, dimension(:), allocatable :: CNCOMP    ! Component atom belongs to (Natoms)
      integer, dimension(:), allocatable :: COMPEN    ! Number of atoms in component (Ncomponents)
      integer, dimension(:,:), allocatable :: COMCON ! Close contacts bet'n components (Ncomponents)

      type MOL_BONDS
        integer :: AtomA
        integer :: AtomB
        character(len=16) :: CtypeA ! The atom type as a character string
        character(len=16) :: CtypeB ! The atom type as a character string
        character(len=1) :: Atype ! The bond type as a character string (-, =, ...)
        double precision :: Btype      ! Bond type, i.e., 1 for single, 2 for double, 3 for triple, 66 aromatic, ...
        integer :: ring      ! Ring that the bond belongs to
        integer :: ring_size ! Bond is part of a ring of size 0(not ring), 3,4,5,6
      end type MOL_BONDS

      type BOND_ATOMS
        integer :: ATOM1
        integer :: ATOM2
        integer :: type      ! Bond type, i.e., 1 for single, 2 for double, 3 for triple, 66 aromatic, ...
        integer :: ring      ! Ring that the bond belongs to
        integer :: ring_size ! Bond is part of a ring of size 0(not ring), 3,4,5,6
      end type BOND_ATOMS

      type (MOL_BONDS), dimension(:), allocatable :: BONDS   ! Bonds
      type (bond_atoms), dimension(:), allocatable :: BOND   ! Bonds for RIC
      type (bond_atoms), dimension(:), allocatable :: vdWBOND ! van der Waals bonds
      type (bond_atoms), dimension(:), allocatable :: vWBOND ! Very weak bonds

      type ANGLE_ATOMS
        integer :: ATOM1
        integer :: ATOM2
        integer :: ATOM3
        integer :: ring_size ! Angle is part of a ring of size 0(not ring), 3,4,5,6
      end type ANGLE_ATOMS

      type (angle_atoms), dimension(:), allocatable :: ANGLE

      type TORSION_ATOMS
        integer :: ATOM1
        integer :: ATOM2
        integer :: ATOM3
        integer :: ATOM4
      end type TORSION_ATOMS

      type (torsion_atoms), dimension(:), allocatable :: TORSION

      type oopbend_atoms
        integer :: ATOM1
        integer :: ATOM2
        integer :: ATOM3
        integer :: ATOM4
      end type oopbend_atoms
      type(oopbend_atoms), dimension(:), allocatable :: oopbend

! Objects related to Homeomorph Reduced pseudo-Graph (HMRpG)
! Diemnsioned NATHRP, NATHRP (NATHRP=Number of vertices for HMRpG)
      integer, dimension(:,:), allocatable :: HMRPCN ! Give the number of edges between vertices
! Diemnsioned NATHRP
      integer, dimension(:), allocatable :: HMINDX   ! maps vertex to original atom
! Connectivity for HMRpG, dimensioned NATHRP,NEDGE:
      integer, dimension(:,:), allocatable :: HRPINC

! Dimensioned NRING,MXEDGR:
      integer, dimension(:,:), allocatable :: FUNDAM
!
! Dimensioned NEDGE: Iedge belongs to ring assembly RNASSE(Iedge)
      integer, dimension(:), allocatable :: RNASSE
!
! Dimensioned NRING: Ring_size(IRing)=ring size of IRing
      integer, dimension(:), allocatable :: Ring_size
!
! List of vertices for each edge, dimensioned NEDGE,DIMLST(Number of vertices):
      integer, dimension(:,:), allocatable :: INBETW
!
! Dimensioned NPRUNE (Number of vertices for Pruned Graph(PG)):
      integer, dimension(:,:), allocatable :: PRUNCN ! Connectivity for PG 
      integer, dimension(:), allocatable :: PRINDX ! maps vertex to original atom
      integer, dimension(:), allocatable :: HMRAT1
      integer, dimension(:), allocatable :: HMRAT2

!     type type_vertex
!       integer  :: atom
!       integer  :: Z
!       integer  :: valency
!       integer  :: ring     ! Ring that vertex belongs to
!       integer  :: ring_size
!       integer  :: type
!     end type type_vertex

      type type_edges
        integer :: vertexI
        integer :: vertexJ
        integer :: ring ! The ring number to which an edge belongs.  (0 if not member of a ring)
      end type type_edges

      type (type_edges), dimension(:), allocatable :: edges
!     type (type_vertex), dimension(:), allocatable :: vertex

      end MODULE GRAPH_objects
