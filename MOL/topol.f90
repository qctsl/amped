      subroutine GRAPH_CONN_BS
!***********************************************************************
!     Date last modified: April 11, 1996                   Version 1.0 *
!     Author: Cory C. Pye and R.A. Poirier                             *
!     Description: Determines whether two atoms are connected          *
!     (based on Bragg-Slater radii)                                    *
!     stores result in array CONNECT (1=Yes,0=No)                      *
!***********************************************************************
! Modules:
      USE GRAPH_objects
      USE type_elements
      USE type_molecule

      implicit none
!
! Local scalars:
!
! Begin:
      call PRG_manager ('enter', 'GRAPH_CONN_BS', 'GRAPH%CONNECT')

      call GET_object ('MOL', 'ATOMIC', 'DISTANCES')
!
! Object:
      NVertices=Natoms
      if(.not.allocated(CONNECT))then
        allocate (CONNECT(Natoms,Natoms))
      else
        deallocate (CONNECT)
        allocate (CONNECT(Natoms,Natoms))
      end if
!
      if(Natoms.gt.1)then
        call GRAPH_CONN (BS_RADII, CONNECT, NELEMENTS, BS_scalef)
      end if

! End of routine GRAPH_CONN_BS
      call PRG_manager ('exit', 'GRAPH_CONN_BS', 'GRAPH%CONNECT')
      return
      end subroutine GRAPH_CONN_BS
      subroutine GRAPH_CONN (RADII,   & ! Bragg-Slater atomic radii
                             CONNECT, & ! Connectivity Matrix (output)
                             ELMLEN,  & ! Length of data array RADII
                             FUDGE)  ! Fudge factor for Connectivity determination
!***********************************************************************
!     Date last modified: April 11, 1996                   Version 1.0 *
!     Author: Cory C. Pye and R.A. Poirier                             *
!     Description: Determines whether two atoms are connected, stores  *
!                  result in array CONNECT (1=Yes,0=No)                *
!***********************************************************************
! Modules:
      USE program_constants
      USE type_molecule

      implicit none
!
! Input scalars:
      integer ELMLEN
      double precision :: FUDGE
!
! Input arrays:
      integer CONNECT(Natoms,Natoms)
      double precision RADII(ELMLEN)
!
! Local scalars:
      integer :: Iatom,Jatom,IIAN,JIAN
      double precision :: FUDGE_save,RIJ_Ang
!
! Begin:
      call PRG_manager ('enter', 'GRAPH_CONN', 'UTILITY')
!
      FUDGE_save=FUDGE
      CONNECT(1:Natoms,1:Natoms)=0
      do Iatom=1,Natoms
        IIAN=CARTESIAN(Iatom)%Atomic_number
        if(IIAN.le.0)cycle ! Don't check if not 'real'.
!       CONNECT(Iatom,Iatom)=IIAN ! Atoms are not connected to themselves, store the atomic number
        do Jatom=Iatom+1,Natoms
          JIAN=CARTESIAN(Jatom)%Atomic_number
          if(JIAN.le.0)cycle ! Don't check if not 'real'.
! disabled special case for H2 molecule -- see ticket mungauss:#46
!         if(IIAN.eq.1.and.JIAN.eq.1)then ! Special case for H2 molecule
!           if(DISMAT(Iatom,Jatom).le.1.80)CONNECT(Iatom,Jatom)=1
!         else 
          if((IIAN.eq.1.and.JIAN.eq.9).or.(IIAN.eq.9.and.JIAN.eq.1))then ! Special case for HF molecule
            FUDGE=1.273
          end if
          if((IIAN.eq.1.and.JIAN.eq.1).and.(NAtoms.eq.2))then ! Special case for H2 molecule
            FUDGE=1.475
          end if
          RIJ_Ang=bohr_to_angstrom*DISMAT(Iatom,Jatom)
          if(RIJ_Ang-FUDGE*(RADII(IIAN)+RADII(JIAN)).le.ZERO)then
            CONNECT(Iatom,Jatom)=1
          end if !
          CONNECT(Jatom,Iatom)=CONNECT(Iatom,Jatom) ! Connectivity is symmetric.
          FUDGE=FUDGE_save
        end do ! ! Jatom
      end do ! ! Iatom
!
! End of routine GRAPH_CONN
      call PRG_manager ('exit', 'GRAPH_CONN', 'UTILITY')
      return
      end subroutine GRAPH_CONN
      subroutine GRAPH_VALENCE
!***********************************************************************
!     Date last modified: April 13, 1996                   Version 1.0 *
!     Author: Cory C. Pye and R.A. Poirier                             *
!     Description: Determines the valency of each atom from the        *
!     connectivity matrix by summing rows.                             *
!***********************************************************************
! Modules:
      USE program_files
      USE GRAPH_objects
      USE type_molecule

      implicit none
!
! Local scalars:
      integer Iatom,Jatom
!
! Begin:
      call PRG_manager ('enter', 'GRAPH_VALENCE', 'GRAPH%CONVAL')

      call GET_object ('MOL', 'GRAPH', 'CONNECT')
!
! Object:
      if(.not.allocated(CONVAL))then
        allocate (CONVAL(Natoms))
      else
        deallocate (CONVAL)
        allocate (CONVAL(Natoms))
      end if
!
      CONVAL(1:Natoms)=0
      do Iatom=1,Natoms
        if(Natoms.eq.1)exit
        if(CARTESIAN(Iatom)%Atomic_number.le.0)cycle ! Skip dummy atoms
        do Jatom=1,Natoms
!         if(Iatom.eq.Jatom)cycle ! skip diagonals!
          CONVAL(Iatom)=CONVAL(Iatom)+CONNECT(Iatom,Jatom)
        end do ! Jatom
        if(CONVAL(Iatom).eq.0)then
          write(UNIout,'(a)')'WARNING> GRAPH_VALENCE:'
          write(UNIout,'(a,i6,a)')'Atom ',Iatom,' has no bonds'
        end if
      end do ! Iatom
!
! End of routine GRAPH_VALENCE
      call PRG_manager ('exit', 'GRAPH_VALENCE', 'GRAPH%CONVAL')
      return
      end subroutine GRAPH_VALENCE
      subroutine GRAPH_CNCOMP
!***********************************************************************
!     Date last modified: July 7, 2004                     Version 1.0 *
!     Author: C. C. Pye and R.A. Poirier                               *
!     Description: Determines which component each atom belongs to     *
! and counts the number of atoms in each component.                    *
!***********************************************************************
! Modules:
      USE GRAPH_objects
      USE type_molecule

      implicit none
!
! Local scalars:
      integer :: COMP,I
!
! Work arrays:
      integer, dimension(:,:), allocatable :: WORKC
      integer, dimension(:), allocatable :: WORKV
      integer, dimension(:), allocatable :: WORKR
      logical, dimension(:), allocatable :: WORKU
!
! Begin:
      call PRG_manager ('enter', 'GRAPH_CNCOMP', 'GRAPH%CNCOMP')
!
      call GET_object ('MOL', 'GRAPH', 'CONNECT')
!
! Object:
      if(.not.allocated(CNCOMP))then
        allocate (CNCOMP(Natoms))
      else
        deallocate (CNCOMP)
        allocate (CNCOMP(Natoms))
      end if
!
      CNCOMP(1:Natoms)=0
      allocate (WORKC(Natoms,Natoms), WORKV(Natoms), WORKR(Natoms), WORKU(Natoms))
      WORKC=CONNECT
      do I=1,Natoms
        WORKC(I,I)=0
      end do
      WORKU(1:Natoms)=.FALSE.
      call GRAPH_COMP (WORKC, WORKV,  WORKR, WORKU, CNCOMP, NComponents)
! Now count the number of atoms in each components
      if(.not.allocated(COMPEN))then
        allocate (COMPEN(1:NComponents))
      else
        deallocate (COMPEN)
        allocate (COMPEN(1:NComponents))
      end if
!
! Initialize component enumeration array.
      COMPEN(1:NComponents)=0
!
! Enumerate components
      do I=1,Natoms
        COMP=CNCOMP(I)
        if(COMP.ne.0)COMPEN(COMP)=COMPEN(COMP)+1 ! Anything but dummy atoms
      end do !
!
! End of routine GRAPH_CNCOMP
      call PRG_manager ('exit', 'GRAPH_CNCOMP', 'GRAPH%CNCOMP')
      return
      end subroutine GRAPH_CNCOMP
      subroutine GRAPH_COMP (CONNECTw, & ! Work array: at input, is connectivity matrix
                             CONVALw,  & ! Work array.
                             ROW,     & ! Work array
                             USED,    & ! Work array
                             XCNCOMP,  & ! Output: Component array
                             XNCOMP)  ! Number of 'molecules'
!**************************************************************************************************
!     Date last modified: April 14, 1995                                              Version 1.0 *
!     Author: Cory C. Pye                                                                         *
!     Description: Determine whether a molecular graph is connected, and partition the atoms into *
! molecular subunits. Dummy atoms are placed in subunit 0. Converted from algorithm in            *
! Narsingh Deo, Graph Theory with Applications to Engineering and Computer Science, p 323,        *
! Prentice-Hall, Englewood Cliffs, NJ 1974.                                                       *
!**************************************************************************************************
! Modules:
      USE GRAPH_objects
      USE type_molecule

      implicit none
!
! Work arrays:
      integer CONNECTw(Natoms,Natoms),CONVALw(Natoms),ROW(Natoms)
      logical USED(Natoms)
!
! Input arrays:
      integer XCNCOMP(Natoms)
!
! Input scalars:
      integer XNCOMP
!
! Local scalars:
      integer I,J,MAXDEG,MAXWHERE,ATMCNT
      logical MOREATOMS,CHANGE
!
! Begin:
      call PRG_manager ('enter', 'GRAPH_COMP', 'UTILITY')
!
      XNCOMP=0
!
! Dummy atoms -> Component 0
      ATMCNT=0
      do I=1,Natoms
        IF (CARTESIAN(I)%Atomic_number.EQ.0)then
          XCNCOMP(I)=0
          USED(I)=.TRUE.
          ATMCNT=ATMCNT+1
        end if !
      end do !
!
      MOREATOMS=ATMCNT.LT.Natoms
!
! Loop while there are still atoms left.
      do while(MOREATOMS)
        XNCOMP=XNCOMP+1
!
! Find degree of each vertex in fused graph
        do I=1,Natoms
          CONVALw(I)=0
          do J=1, Natoms
            CONVALw(I)=CONVALw(I)+CONNECTw(I,J)
          end do !
        end do !
!
! Find vertex (atom) of maximum degree (valence) in fused graph
        MAXWHERE=1
        do while(USED(MAXWHERE))
          MAXWHERE=MAXWHERE+1
        end do !
        MAXDEG=CONVALw(MAXWHERE)
        do I=2,Natoms
          IF(CONVALw(I).GT.MAXDEG)then
            MAXWHERE=I
            MAXDEG=CONVALw(I)
          end if !
        end do !
!
! Copy connectivity pattern of chosen vertex into work array.
        do I=1,Natoms
          ROW(I)=CONNECTw(MAXWHERE,I)
        end do !
!
! Vertex atom belongs in current component
        XCNCOMP(MAXWHERE)=XNCOMP
        USED(MAXWHERE)=.TRUE.
        ATMCNT=ATMCNT+1
        CHANGE=.TRUE.
!
! Loop while we can fuse more vertices together.
        do while(CHANGE)
          CHANGE=.FALSE.
          do I=1,Natoms
            IF(ROW(I).EQ.1)then
!
! We have found another vertex to condense.
               CHANGE=.TRUE.
               XCNCOMP(I)=XNCOMP
               ATMCNT=ATMCNT+1
               USED(I)=.TRUE.
!
! Condense the vertex by 'OR'ing the connectivity of I to MAXWHERE
               do J=1,Natoms
                 IF(CONNECTw(I,J).EQ.1)then
                   CONNECTw(MAXWHERE,J)=1
                 end if !
!
! Now that the connectivity info is reassigned, delete the vertex.
                 CONNECTw(I,J)=0
                 CONNECTw(J,I)=0
               end do !
            end if !
          end do !
!
! Reload ROW with the 'fused' vertex.
          CONNECTw(MAXWHERE,MAXWHERE)=0
          do I=1,Natoms
            ROW(I)=CONNECTw(MAXWHERE,I)
          end do !
        end do ! ! WHILE(CHANGE)
!
! Any more atoms left?
        MOREATOMS=ATMCNT.LT.Natoms
!
! Delete current component and move on.
        do I=1,Natoms
          CONNECTw(MAXWHERE,I) = 0
        end do !
!
      end do ! ! WHILE(MOREATOMS)
!
! End of routine GRAPH_COMP
      call PRG_manager ('exit', 'GRAPH_COMP', 'UTILITY')
      return
      end subroutine GRAPH_COMP
      subroutine GRAPH_PRUNE
!***********************************************************************
!     Date last modified: August 12, 1996                  Version 1.0 *
!     Author: Cory C. Pye                                              *
!     Description: Prunes a graph.                                     *
!***********************************************************************
! Modules:
      USE GRAPH_objects
      USE type_molecule

      implicit none
!
! Input scalars:
      integer NLEFT
!
! Work arrays:
      integer, dimension(:,:), allocatable :: WORKC
      integer, dimension(:), allocatable :: WORKV
      integer, dimension(:), allocatable :: PRUNCNw
      integer, dimension(:), allocatable :: PRINDXw
!
! Local scalars:
      integer I,J,K,IAT,JAT,PASS
      logical CHANGED
!
! Begin:
      call PRG_manager ('enter', 'GRAPH_PRUNE', 'GRAPH%CONN_PRUN')
!
      call GET_object ('MOL', 'GRAPH', 'CONNECT')
!
      allocate (PRUNCNw(1:Natoms*Natoms), PRINDXw(1:Natoms))
      allocate (WORKC(Natoms,Natoms), WORKV(Natoms))
!
      WORKC(1:Natoms,1:Natoms)=CONNECT(1:Natoms,1:Natoms)
!
!     write(UNIout,'(a)')'Pruning graph...'
      NLEFT=Natoms
      CHANGED=.TRUE.
      PASS=0
!
      do while(CHANGED)
        CHANGED=.FALSE.
        PASS=PASS+1
!
! Get degree sequence of all atoms (valence)
        do I=1,Natoms
          WORKV(I)=0
          do J=1,Natoms
            WORKV(I)=WORKV(I)+WORKC(I,J)
          end do !
        end do !
!
! Check for vertices of degree 1
        do I=1,Natoms
          IF(WORKV(I).EQ.1)then
            CHANGED=.TRUE.
            do J=1,Natoms
              WORKC(I,J)=0
              WORKC(J,I)=0
            end do !
          ENDIF
        end do !
      end do !
!
! Graph has been pruned. Transfer information to indexing array.
      NLEFT=0
      do I=1,Natoms
        IF(WORKV(I).NE.0)then
          NLEFT=NLEFT+1
          PRINDXw(NLEFT)=I
        end if !
      end do !
!
! Now use indexing array to transfer information to PRUNCN.
      K=0
      do I=1,NLEFT
        do J=1, NLEFT
          K=K+1
          IAT=PRINDXw(I)
          JAT=PRINDXw(J)
          PRUNCNw(K)=WORKC(IAT,JAT)
        end do !
      end do !
!
! Special case: No cycles in graph
      IF(NLEFT.EQ.0)then
        PRINDXw(1)=0
        PRUNCNw(1)=0
      end if !
!
      NPRUNE=NLEFT
      if(.not.allocated(PRUNCN))then
        allocate (PRUNCN(1:max0(1,NPRUNE),1:max0(1,NPRUNE)), PRINDX(1:max0(1,NPRUNE)))
      else
        deallocate (PRUNCN, PRINDX)
        allocate (PRUNCN(1:max0(1,NPRUNE),1:max0(1,NPRUNE)), PRINDX(1:max0(1,NPRUNE)))
      end if

      K=0
      do I=1,NPRUNE
        PRINDX(I)=PRINDXw(I)
      do J=1,NPRUNE
        K=K+1
        PRUNCN(I,J)=PRUNCNw(K)
      end do ! ! J
      end do ! ! I
!
      deallocate (PRUNCNw, PRINDXw)
      deallocate (WORKC, WORKV)
!
! End of routine GRAPH_PRUNE
      call PRG_manager ('exit', 'GRAPH_PRUNE', 'GRAPH%CONN_PRUN')
      return
      end subroutine GRAPH_PRUNE
      subroutine GRAPH_HOMREDUCED
!***********************************************************************
!     Date last modified: August 14, 1996                  Version 1.0 *
!     Author: Cory C. Pye                                              *
!     Description: Reduces graph to homeomorph.                        *
!***********************************************************************
! Modules:
      USE program_files
      USE GRAPH_objects

      implicit none
!
! Work arrays:
      integer, dimension(:,:), allocatable :: WORKC
      integer, dimension(:), allocatable :: WORKV
      integer, dimension(:), allocatable :: HMRPCNw
      integer, dimension(:), allocatable :: HMINDXw
!
! Local scalars:
      integer I,J,AT1,AT2,IAT,JAT,K
      integer NLEFT,DIM,DIM2
      character*6 HOWFAR
!
! Begin:
      call PRG_manager ('enter', 'GRAPH_HOMREDUCED', 'GRAPH%PRUNE_HOMRED')
!
      call GET_object ('MOL', 'GRAPH', 'CONN_PRUN')
!
! Object:
      if(.not.allocated(HMRPCN))then
        allocate (HMRAT1(max0(1,NPRUNE)), HMRAT2(max0(1,NPRUNE)))
      else
        deallocate (HMRAT1, HMRAT2)
        allocate (HMRAT1(max0(1,NPRUNE)), HMRAT2(max0(1,NPRUNE)))
      end if
!
      HOWFAR='NEMPTY'
      DIM=max0(1,NPRUNE)
      allocate (HMRPCNw(1:DIM*DIM), HMINDXw(DIM))
      allocate (WORKC(DIM,DIM), WORKV(DIM))
!
      WORKC=PRUNCN
      HMRAT1=0
      HMRAT2=0
      HMINDXw=0
!
!     write(UNIout,'(a)')'Homeomorphically reducing graph...'
      NLEFT=NPRUNE
!
! Get degree sequence of all atoms (valence)
      do I=1,NPRUNE
        WORKV(I)=0
        do J=1,NPRUNE
          WORKV(I)=WORKV(I)+WORKC(I,J)
        end do !
      end do !
!
! Check if degree = 2.
      do I=1,NPRUNE
        IF(WORKV(I).EQ.2)then
!
! Degree = 2: Find adjacent vertices with regards to old numbering
          do J=1,NPRUNE
            IF(PRUNCN(I,J).EQ.1)then
              IF(HMRAT1(I).EQ.0)then
                AT1=J
                HMRAT1(I)=AT1
              else
                AT2=J
                HMRAT2(I)=AT2
              end if !
            end if !
          end do ! ! J
!
! Find adjacent vertices in new graph
          AT1=0
          AT2=0
          do J=1,NPRUNE
            IF(WORKC(I,J).EQ.1)then
              IF(AT1.EQ.0)then
                AT1=J
              else
                AT2=J
              end if !
            else IF(WORKC(I,J).EQ.2)then
              AT1=J
              AT2=J
            end if !
          end do ! ! J
!
! Make connection between two neighbors to form new (pseudo)graph.
!  Sept 1: Addition of criteria.
!
! Don't remove if the pope is Jewish.
          IF(HOWFAR(1:6).EQ.'NOREST'.or. &
! Don t remove if resulting ring would be annihilated. &
             (HOWFAR(1:6).EQ.'NEMPTY'.and.I.NE.AT1).or. &
! Don t remove if a loop is produced. &
             (HOWFAR(1:6).EQ.'NOLOOP'.and.AT1.EQ.AT2).or. &
! Don t remove if new pseudograph is not a graph. &
             (HOWFAR(1:5).EQ.'GRAPH'))then
          WORKC(AT1,AT2)=WORKC(AT1,AT2)+1
          WORKC(AT2,AT1)=WORKC(AT2,AT1)+1
          WORKC(I,AT1)=0
          WORKC(AT1,I)=0
          WORKC(I,AT2)=0
          WORKC(AT2,I)=0
          WORKV(I)=0
!
          else
            write(UNIout,'(a,i10)')' Skipped vertex: ', I
            HMRAT1(I)=0
            HMRAT2(I)=0
          end if !
!
        end if !
      end do !
!
! Now create index map.
      NLEFT=0
      do I=1,NPRUNE
        IF(WORKV(I).NE.0)then
          NLEFT=NLEFT+1
          HMINDXw(NLEFT)=I
        end if !
      end do !
!
! From index map, create homeomorphically reduced (pseudo)graph.
      IF(NLEFT.GT.0)then
        K=0
        do I=1,NLEFT
          do J=1, NLEFT
            K=K+1
            IAT=HMINDXw(I)
            JAT=HMINDXw(J)
            HMRPCNw(K)=WORKC(IAT,JAT)
          end do !
        end do !
      else
        HMINDXw(1)=0
        HMRPCNw(1)=0
      end if !

      NATHRP=NLEFT
      DIM2=NATHRP
      if(.not.allocated(HMRPCN))then
        allocate (HMRPCN(DIM2,DIM2), HMINDX(DIM2))
      else
        deallocate (HMRPCN, HMINDX)
        allocate (HMRPCN(DIM2,DIM2), HMINDX(DIM2))
      end if

      K=0
      do I=1,NATHRP
        HMINDX(I)=HMINDXw(I)
      do J=1,NATHRP
        K=K+1
        HMRPCN(I,J)=HMRPCNw(K)
      end do ! ! J
      end do ! ! I

      deallocate (HMRPCNw, HMINDXw)
      deallocate (WORKC, WORKV)
!
! End of routine GRAPH_HOMREDUCED
      call PRG_manager ('exit', 'GRAPH_HOMREDUCED', 'GRAPH%PRUNE_HOMRED')
      return
      end subroutine GRAPH_HOMREDUCED
      subroutine GRAPH_INCIDENCE
!***********************************************************************
!     Date last modified: August 15, 1996                  Version 1.0 *
!     Author: Cory C. Pye                                              *
!     Description: Build incidence matrix of (pseudo)graph.            *
!***********************************************************************
! Modules:
      USE GRAPH_objects

      implicit none
!
! Work array:
      integer, dimension(:), allocatable :: HRPINCw
!
! Local scalars:
      integer I,J,K,NCONN,DIM,DIM2
!
! Begin:
      call PRG_manager ('enter', 'GRAPH_INCIDENCE', 'GRAPH%CONN_PRUN_HOMRED_INCIDENCE')
!
      call GET_object ('MOL', 'GRAPH', 'PRUNE_HOMRED')
!
      DIM=max0(1,NATHRP)
      allocate (HRPINCw(6*DIM*DIM*DIM))

      NEDGE=0
      do I=1,DIM*DIM*DIM
        HRPINCw(I)=0
      end do !
!
      do I=1,NATHRP
!
! Self-loops.
        NCONN=HMRPCN(I,I)/2
        do K=1,NCONN
          NEDGE=NEDGE+1
          HRPINCw(NATHRP*(NEDGE-1)+I)=2
!         HRPINCw(NATHRP*(NEDGE-1)+I)=1
        end do !
!
! Everything else
        do J=I+1,NATHRP
          NCONN=HMRPCN(I,J)
          do K=1,NCONN
            NEDGE=NEDGE+1
            HRPINCw(NATHRP*(NEDGE-1)+I)=1
            HRPINCw(NATHRP*(NEDGE-1)+J)=1
          end do ! ! K
        end do !
      end do !

      if(.not.allocated(HRPINC))then
        allocate (HRPINC(1:max0(1,NATHRP),1:max0(1,NEDGE)))
      else
        deallocate (HRPINC)
        allocate (HRPINC(1:max0(1,NATHRP),1:max0(1,NEDGE)))
      end if

      K=0
      do I=1,NATHRP
      do J=1,NEDGE
        K=K+1
        HRPINC(I,J)=HRPINCw(K)
      end do ! ! J
      end do ! ! I
!
      deallocate (HRPINCw)
!
! End of routine GRAPH_INCIDENCE
      call PRG_manager ('exit', 'GRAPH_INCIDENCE', 'GRAPH%CONN_PRUN_HOMRED_INCIDENCE')
      return
      end subroutine GRAPH_INCIDENCE
      subroutine GRAPH_FUND_RINGS
!***********************************************************************
!     Date last modified: August 20, 1996                  Version 1.0 *
!     Author: Cory C. Pye                                              *
!     Description: Finds set of fundamental rings according to         *
!                  Matyska's algorithm (L. Matyska, J. Comput. Chem.   *
!                  6 (1988) 455.) The algorithm is extended to handle  *
!                  pseudographs with self-loops (inc = 2) for which    *
!                  the binary representation fails.                    *
!***********************************************************************
! Modules:
      USE program_files
      USE GRAPH_objects
      USE type_molecule

      implicit none
!
! Work arrays:
      integer, dimension(:,:), allocatable :: WINC ! Initially equal to HRPINC
      integer, dimension(:), allocatable :: FUNDAMw
      integer, dimension(:), allocatable :: LA
      logical, dimension(:), allocatable :: A1
      logical, dimension(:), allocatable :: B1
!
! Local scalars:
      integer I,J,K,L,IRING,TEMP,MAXTEMP,DIM1,DIM2
!
! Begin:
      call PRG_manager ('enter', 'GRAPH_FUND_RINGS', 'GRAPH%FUNDRING')
!
      call GET_object ('MOL', 'GRAPH', 'CONN_PRUN_HOMRED_INCIDENCE')
!
      DIM1=max0(1,NATHRP)
      DIM2=max0(1,NEDGE)
      allocate (FUNDAMw(1:DIM2*DIM2))
      allocate (WINC(DIM1,DIM2), LA(DIM1), A1(DIM1), B1(DIM2))
!
      LA(1:DIM1)=0
      FUNDAMw(1:DIM2*DIM2)=0
      A1(1:DIM1)=.true.
      B1(1:DIM2)=.true.
      WINC(1:DIM1,1:DIM2)=HRPINC(1:DIM1,1:DIM2) ! Do not change HRPINC
!
      do I=1,NEDGE
        do J=1,NATHRP
          IF(A1(J))then
            IF(WINC(J,I).EQ.1)then
              LA(J)=I
              A1(J)=.FALSE.
              B1(I)=.FALSE.
! Exclusive OR
              do K=1,J-1
                IF(WINC(K,I).EQ.1)then
                  do L=1,NEDGE
!                    WINC(K,L)=MOD(WINC(K,L)+WINC(J,L),2)
                    IF(WINC(K,L).LE.1.and.WINC(J,L).LE.1)then
                    IF(WINC(K,L).EQ.WINC(J,L))then
                      WINC(K,L)=0
                    else
                      WINC(K,L)=1
                    end if !
                    end if !
                  end do !
                end if !
              end do !
              do K=J+1,NATHRP
                IF(WINC(K,I).EQ.1)then
                  do L=1,NEDGE
!                    WINC(K,L)=MOD(WINC(K,L)+WINC(J,L),2)
                    IF(WINC(K,L).LE.1.and.WINC(J,L).LE.1)then
                    IF(WINC(K,L).EQ.WINC(J,L))then
                      WINC(K,L)=0
                    else
                      WINC(K,L)=1
                    end if !
                    end if !
                  end do !
                end if !
              end do !
            end if !
!
          end if !
        end do !
      end do !
!
      NRING=0
      do I=1,NEDGE
        IF(B1(I))then
          NRING=NRING+1
        end if ! B1(I)
      end do ! I
!
      write(UNIout,'(a,i10)')'# Fundamental Rings = ',NRING
!
      IRING=0
      MAXTEMP=0
      TEMP=0
      do I=1,NEDGE
!
        IF(B1(I))then
          TEMP=1
          IRING=IRING+1
          FUNDAMw(IRING)=I
!
          do J=1,NATHRP
            IF(WINC(J,I).EQ.1)then
              TEMP=TEMP+1
              FUNDAMw(NRING*(TEMP-1)+IRING)=LA(J)
            end if !
          end do !
        end if !
!
      MAXTEMP=max0(MAXTEMP,TEMP)
      end do ! Iedge

      MXEDGR=MAXTEMP
!
      if(.not.allocated(FUNDAM))then
        allocate (FUNDAM(1:max0(1,NRING),1:max0(1,MXEDGR)))
      else
        deallocate (FUNDAM)
        allocate (FUNDAM(1:max0(1,NRING),1:max0(1,MXEDGR)))
      end if

      K=0
      do I=1,NRING
      do J=1,MXEDGR
        K=K+1
        FUNDAM(I,J)=FUNDAMw(K)
      end do ! ! J
      end do ! ! I
!
      deallocate (FUNDAMw)
      deallocate (WINC, LA, A1, B1)
!
! End of routine GRAPH_FUND_RINGS
      call PRG_manager ('exit', 'GRAPH_FUND_RINGS', 'GRAPH%FUNDRING')
      return
      end subroutine GRAPH_FUND_RINGS
      subroutine GRAPH_RING_ASSEMBLY
!***********************************************************************
!     Date last modified: August 22, 1996                  Version 1.0 *
!     Author: Cory C. Pye                                              *
!     Description: Finds ring assemblies from fundamental ring set.    *
!***********************************************************************
! Modules:
      USE GRAPH_objects
      USE type_molecule

      implicit none
!
! Work arrays:
      integer, dimension(:), allocatable :: LENRIN
      integer, dimension(:), allocatable :: B
!
! Dimensioned NRING:
      integer, dimension(:), allocatable :: RNASSR
!
! Local scalars:
      integer I,IRing,J,K,L,TEMP,LENB,DIMR,DIME
      logical NOTEND,ASSINTER,INTERSECT,T
!
! Begin:
      call PRG_manager ('enter', 'GRAPH_RING_ASSEMBLY',  'GRAPH%EDGE_RING_TO_RING_ASSEMBLY')
!
      call GET_object ('MOL', 'GRAPH', 'FUNDRING')

      DIMR=max0(1,NRING)
      DIME=max0(1,NEDGE)
!
! Object:
      if(.not.allocated(RNASSE))then
        allocate (RNASSE(1:DIME))
      else
        deallocate (RNASSE)
        allocate (RNASSE(1:DIME))
      end if
!
      allocate (RNASSR(1:DIMR))
      allocate (B(DIME), LENRIN(DIMR))

      RNASSE(1:DIME)=0
      RNASSR(1:DIMR)=0
      B(1:DIME)=0
!
      NRING_assembly=0
!
      do I=1,NRING
        J=1
        NOTEND=.TRUE.
        do while (J.LE.MXEDGR.and.NOTEND)
          NOTEND=FUNDAM(I,J).NE.0
          IF(NOTEND)J=J+1
        end do ! while
        LENRIN(I)=J-1
      end do ! ! Iring
!
      do I=1,NRING
        ASSINTER=.FALSE.
        do J=1,NRING_assembly
          do K=1,LENRIN(I)
            TEMP=RNASSE(FUNDAM(I,K))
            IF(TEMP.NE.0) ASSINTER=.TRUE.
          end do ! K
        end do ! ! J
!
        IF(.not.ASSINTER)then
          NRING_assembly=NRING_assembly+1
          do J=1,LENRIN(I)
            B(J)=FUNDAM(I,J)
            RNASSE(FUNDAM(I,J))=NRING_assembly
            RNASSR(I)=NRING_assembly
          end do ! ! J
!
          T=.FALSE.
          LENB=LENRIN(I)
          do while(.not.T)
            T=.TRUE.
            do J=I+1,NRING
!
            IF(RNASSR(J).EQ.0)then
              INTERSECT=.FALSE.
              do K=1,LENRIN(J)
                do L=1,LENB
                  IF(B(L).EQ.FUNDAM(J,K))INTERSECT=.TRUE.
                end do ! ! L
              end do ! ! K
!
              IF(INTERSECT)then
                T=.FALSE.
                do K=1,LENRIN(J)
                  IF(RNASSE(FUNDAM(J,K)).EQ.0)then
                    LENB=LENB+1
                    RNASSE(FUNDAM(J,K))=NRING_assembly
                    B(LENB)=FUNDAM(J,K)
                  end if !
                end do ! ! K
                RNASSR(J)=NRING_assembly
              end if ! ! .not.INTERSECT
            end if ! RNASSR(J).eq.0
            end do ! ! J
          end do ! ! WHILE(.not.T)
        end if ! ! .not.ASSINTER
      end do ! ! Iring

      deallocate (RNASSR)
      deallocate (B, LENRIN)
!
! End of routine GRAPH_RING_ASSEMBLY
      call PRG_manager ('exit', 'GRAPH_RING_ASSEMBLY', 'GRAPH%EDGE_RING_TO_RING_ASSEMBLY')
      return
      end subroutine GRAPH_RING_ASSEMBLY
      subroutine GRAPH_ATOM_BETWEEN
!***********************************************************************
!     Date last modified: Sept 23, 1996                    Version 1.0 *
!     Author: Cory C. Pye                                              *
!     Description: Builds in-between list expanding HRpG to pG.        *
!***********************************************************************
! Modules:
      USE program_files
      USE GRAPH_objects
      USE type_molecule

      implicit none
!
! Work arrays:
      integer, dimension(:,:), allocatable ::FLLIST
      integer, dimension(:), allocatable :: LIST
      integer, dimension(:), allocatable :: BEGIN
      integer, dimension(:), allocatable :: END
      logical, dimension(:), allocatable :: EXCLUD
      logical, dimension(:), allocatable :: BRIDGE
      logical, dimension(:), allocatable :: EALLOW
      integer, dimension(:,:), allocatable :: INBETWw
!
! Local scalars:
      integer :: I,J,K,SEED1,SEED2,MAXJ,TEMP,BEGINH,ENDH,NUMLST,SIDE1,SIDE2,DIMEH,DIMAH,DIMAP
      logical :: DONE,FOUND
!
! Begin:
      call PRG_manager ('enter', 'GRAPH_ATOM_BETWEEN', 'GRAPH%VERTEX_LIST')
!
      call GET_object ('MOL', 'GRAPH', 'PRUNE_HOMRED')
      call GET_object ('MOL', 'GRAPH', 'CONN_PRUN_HOMRED_INCIDENCE')
!
      ENDH=0
      BEGINH=0
      DIMEH=max0(1,NEDGE)
      DIMAH=max0(1,NATHRP)
      DIMAP=max0(1,NPRUNE)
      allocate (INBETWw(1:DIMEH,1:DIMAP))
      allocate (FLLIST(1:DIMEH,1:DIMAP), LIST(1:DIMAP), BEGIN(1:DIMEH), END(1:DIMEH), EXCLUD(1:DIMAP), BRIDGE(1:DIMAP), &
                EALLOW(1:DIMEH))
!
      INBETWw(1:DIMEH,1:DIMAP)=0
      FLLIST(1:DIMEH,1:DIMAP)=0
      LIST(1:DIMAP)=0
      BEGIN(1:DIMEH)=0
      END(1:DIMEH)=0
      EXCLUD(1:DIMAP)=.FALSE.
      BRIDGE(1:DIMAP)=.FALSE.
      EALLOW(1:DIMEH)=.TRUE.

      MAXJ=0
!
! Find bridgeheads and exclude.
      do I=1,NATHRP
        J=HMINDX(I)
        BRIDGE(J)=.TRUE.
        EXCLUD(J)=.TRUE.
      end do !
!
! Now find all in-between lists.
      NUMLST=0
      do I=1,NPRUNE
        IF(.not.EXCLUD(I))then
!
! Exclude current start atom and start list
          EXCLUD(I)=.TRUE.
          J=1
          NUMLST=NUMLST+1
          LIST(J)=I
!
! Set initial seeds for chain determination.
          SEED1=HMRAT1(I)
          SEED2=HMRAT2(I)
!
! Start with seed 1.
          DONE=EXCLUD(SEED1)
          IF(DONE)then
            BEGIN(NUMLST)=SEED1
          else
          do while(.not.DONE)
            J=J+1
            LIST(J)=SEED1
            EXCLUD(SEED1)=.TRUE.
            SIDE1=HMRAT1(SEED1)
            SIDE2=HMRAT2(SEED1)
            IF(.not.EXCLUD(SIDE1))then
              SEED1=SIDE1
            else IF(.not.EXCLUD(SIDE2))then
              SEED1=SIDE2
            else
              DONE=.TRUE.
            end if !
          end do !
!
! Set Beginning bridgehead.
          IF(BRIDGE(SIDE1))then
            BEGIN(NUMLST)=SIDE1
          else IF(BRIDGE(SIDE2))then
            BEGIN(NUMLST)=SIDE2
          else
            write(UNIout,'(a)')'ERROR> GRAPH_ATOM_BETWEEN: No Beginning bridge'
          end if !
          end if !
!
! Invert order.
          do K=1,J/2
            TEMP=LIST(K)
            LIST(K)=LIST(J+1-K)
            LIST(J+1-K)=TEMP
          end do !
!
! Proceed with seed 2.
          DONE=EXCLUD(SEED2)
          IF(DONE)then
            END(NUMLST)=SEED2
          else
          do while(.not.DONE)
            J=J+1
            LIST(J)=SEED2
            EXCLUD(SEED2)=.TRUE.
            SIDE1=HMRAT1(SEED2)
            SIDE2=HMRAT2(SEED2)
            IF(.not.EXCLUD(SIDE1))then
              SEED2=SIDE1
            else IF(.not.EXCLUD(SIDE2))then
              SEED2=SIDE2
            else
              DONE=.TRUE.
            end if !
          end do !
!
! Set ending bridgehead.
          IF(BRIDGE(SIDE1))then
            END(NUMLST)=SIDE1
          else IF(BRIDGE(SIDE2))then
            END(NUMLST)=SIDE2
          else
            write(UNIout,'(a)')'ERROR> GRAPH_ATOM_BETWEEN: No ending bridge'
          end if !
          end if !
!
! Copy current list into `big' list.
!          do K=1, J
!            FLLIST(NUMLST,K)=LIST(K)
!          end do !
! Modified: Nov 24 s.t. atom adjacent to bridgehead with lower index is first.
          IF(BEGIN(NUMLST).LE.END(NUMLST))then
            do K=1, J
              FLLIST(NUMLST,K)=LIST(K)
            end do !
          else
            do K=1, J
              FLLIST(NUMLST,K)=LIST(J+1-K)
            end do !
          end if !
!
! Get maximum list size.
          MAXJ=max0(J,MAXJ)
        end if !
      end do !
!
! Associate lists with edges of the HRpG.
      do I=1,NUMLST
! Find bridgeheads in HRpG.
        do K=1,NATHRP
          IF(HMINDX(K).EQ.BEGIN(I))BEGINH=K
          IF(HMINDX(K).EQ.END(I))ENDH=K
        end do !
!
! Find edge and copy into in-between matrix.
        J=1
        FOUND=.FALSE.
        do while(.not.FOUND.and.J.LE.NEDGE)
          IF(EALLOW(J))then
            IF((BEGINH.NE.ENDH.and.HRPINC(BEGINH,J).EQ.1.and.HRPINC(ENDH,J).EQ.1).or.(BEGINH.EQ.ENDH.and.HRPINC(BEGINH,J).EQ.2)) &
              then
              FOUND=.TRUE.
              do K=1,MAXJ
                INBETWw(J,K)=FLLIST(I,K)
              end do !
              EALLOW(J)=.FALSE.
            else
              J=J+1
            end if !
          else
            J=J+1
          end if !
        end do !
        IF(J.GT.NEDGE)then
          write(UNIout,'(a)')'ERROR> GRAPH_ATOM_BETWEEN: Cannot find edge.'
        end if !
      end do !
!
      DIMLST=MAXJ
      if(.not.allocated(INBETW))then
        allocate (INBETW(1:max0(1,NEDGE), 1:max0(1,DIMLST)))
      else
        deallocate (INBETW)
        allocate (INBETW(1:max0(1,NEDGE), 1:max0(1,DIMLST)))
      end if

      do I=1,NEDGE
      do J=1,DIMLST
        INBETW(I,J)=INBETWw(I,J)
      end do ! ! J
      end do ! ! I

      deallocate (INBETWw)
      deallocate (FLLIST, LIST, BEGIN, END, EXCLUD, BRIDGE, EALLOW)
!
! End of routine GRAPH_ATOM_BETWEEN
      call PRG_manager ('exit', 'GRAPH_ATOM_BETWEEN', 'GRAPH%VERTEX_LIST')
      return
      end subroutine GRAPH_ATOM_BETWEEN
      subroutine GRAPH_EDGE_RINGASS
!***********************************************************************
!     Date last modified: November 23, 1996                Version 1.0 *
!     Author: Cory C. Pye                                              *
!     Description: Edge of G belongs to Ring assembly.                 *
!***********************************************************************
! Modules:
      USE program_files
      USE GRAPH_objects
      USE type_molecule

      implicit none
!
! Local scalars:
      integer CTR,DIMFB,DIMPA,DIMHA,DIMHE
      integer I,Iedge,IRing,J,K,start,END,Pstart,PEND,RNGASS,Fstart,FEND,FPREV,FNEXT,ATOM1,ATOM2
      logical DONE
!
! Begin:
      call PRG_manager ('enter', 'GRAPH_EDGE_RINGASS', 'GRAPH%EDGE_TO_RING')
!
      call GET_object ('MOL', 'GRAPH', 'CONN_PRUN')
      call GET_object ('MOL', 'GRAPH', 'BOND_ATOMS')
      call GET_object ('MOL', 'GRAPH', 'PRUNE_HOMRED')
      call GET_object ('MOL', 'GRAPH', 'CONN_PRUN_HOMRED_INCIDENCE')
      call GET_object ('MOL', 'GRAPH', 'EDGE_RING_TO_RING_ASSEMBLY')
      call GET_object ('MOL', 'GRAPH', 'VERTEX_LIST')

      DIMFB=max0(1,NGBonds)
      DIMPA=max0(1,NPRUNE)
      DIMHA=max0(1,NATHRP)
      DIMHE=max0(1,NEDGE)

! Object:
      if(.not.allocated(edges))then
        allocate(edges(1:DIMFB))
      else
        deallocate(edges)
        allocate(edges(1:DIMFB))
      end if

      edges(1:DIMFB)%vertexI=0
      edges(1:DIMFB)%vertexJ=0
      edges(1:DIMFB)%ring=0
!
! Loop over edges of HRpG.
      do I=1,NEDGE
! This edge of HRpG belongs to ring assembly RNGASS
        RNGASS=RNASSE(I)
        IF(RNGASS.NE.0)then
! Find start and end atoms.
        start=0
        END=0
        do J=1,NATHRP
          IF(HRPINC(J,I).EQ.2)then
            start=J
            END=J
          else IF(HRPINC(J,I).EQ.1)then
            IF(start.EQ.0)then
              start=J
            else
              END=J
            end if !
          end if ! HRPINC
        end do ! J=1,NATHRP
!
        IF(start.EQ.0.or.END.EQ.0)then
          write(UNIout,'(a)')'ERROR> GRAPH_EDGE_RINGASS'
          call PRG_stop ('Cannot find start or end vertex')
        end if !
!
        Pstart=HMINDX(start)
        PEND=HMINDX(END)
!
        Fstart=PRINDX(Pstart)
        FEND=PRINDX(PEND)
        ATOM1=MIN0(Fstart,FEND)
        ATOM2=MAX0(Fstart,FEND)
!
! Check to see if list is empty.
        J=1
        DONE=INBETW(I,J).EQ.0
        IF (.not.DONE)then
          FPREV=Fstart
          FNEXT=PRINDX(INBETW(I,J))
          ATOM1=MIN(FPREV,FNEXT)
          ATOM2=MAX0(FPREV,FNEXT)
        end if !
!
! For every edge, loop through list of vertices.
        do while(.not.DONE)
          do K=1,NGBonds
            IF(BOND(K)%ATOM1.EQ.ATOM1.and.BOND(K)%ATOM2.EQ.ATOM2)then
              edges(K)%vertexI=BOND(K)%ATOM1
              edges(K)%vertexJ=BOND(K)%ATOM2
              write(UNIout,'(a,i8,a,i8)')'Assigned bond ',K, &
                                       ' to ring assembly ',RNGASS
              edges(K)%ring=RNGASS
              write(UNIout,'(a,2i8)')'Bond atoms: ', &
                  edges(K)%vertexI,edges(K)%vertexJ
            end if !
          end do ! K
          J=J+1
          FPREV=FNEXT
          IF(J.GT.DIMLST)then
            DONE=.TRUE.
          else
            DONE=INBETW(I,J).EQ.0
          end if !
          IF(.not.DONE)then
            FNEXT=PRINDX(INBETW(I,J))
          else
            FNEXT=FEND
          end if !
          ATOM1=MIN(FPREV,FNEXT)
          ATOM2=MAX0(FPREV,FNEXT)
        end do ! while
!
        do K=1,NGBonds
          IF(BOND(K)%ATOM1.EQ.ATOM1.and.BOND(K)%ATOM2.EQ.ATOM2)then
            edges(K)%vertexI=BOND(K)%ATOM1
            edges(K)%vertexJ=BOND(K)%ATOM2
              write(UNIout,'(a,i8,a,i8)')'Assigned bond ',K, &
                                       ' to ring assembly ',RNGASS
            edges(K)%ring=RNGASS
              write(UNIout,'(a,2i8)')'Bond atoms: ', &
                  edges(K)%vertexI,edges(K)%vertexJ
          end if !
        end do ! K
!
      end if ! RNASSE.ne.0
      end do ! Iedge

      if(NRing.gt.0)then
      if(.not.allocated(Ring_size))then
        allocate (Ring_size(NRing))
      else
        deallocate (Ring_size)
        allocate (Ring_size(NRing))
      end if

      Ring_size(1:NRing)=0
! First determine the size of each ring
      do Iedge = 1,NGBonds
        if(edges(Iedge)%ring.eq.0)cycle
          CTR = edges(Iedge)%ring
          Ring_size(CTR)= Ring_size(CTR)+1
      end do
! Now assign a ring size of each edge
      do Iedge = 1,NGBonds
        if(edges(Iedge)%ring.eq.0)cycle
          CTR = edges(Iedge)%ring
          BOND(Iedge)%ring_size=Ring_size(CTR)
          BOND(Iedge)%ring = edges(Iedge)%ring
      end do
      end if

! End of routine GRAPH_EDGE_RINGASS
      call PRG_manager ('exit', 'GRAPH_EDGE_RINGASS', 'GRAPH%EDGE_TO_RING')
      return
      end subroutine GRAPH_EDGE_RINGASS
