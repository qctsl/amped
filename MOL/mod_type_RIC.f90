      MODULE redundant_coordinates
!*****************************************************************************
! Date last modified: 8 August, 2000                             Version 1.0 *
! Author: Michelle Shaw                                                      *
! Description: Contains object and initializing routine for the set of       *
!              redundant internal coordinates.                               *
!*****************************************************************************
! Modules:

      implicit none

      double precision, dimension(:), allocatable :: RIC_old
      double precision, dimension(:), allocatable :: RIC_new

      end MODULE redundant_coordinates
