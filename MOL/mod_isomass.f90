      module atommass
!*******************************************************************************************************************
!     Date last modified: May 05, 1999                                                                Version 1.0  *
!     Author: M.K.Brooker                                                                                          *
!     Description: Atomic masses for isotopes, uses functions and subroutines to access isotope data               * 
!           function CheckIsotope(ATOM_NUM,ISOTOPE_NUM)                                                            *
!           function Set_Def_Isotope(ATOM_NUM,ISOTOPE_NUM)                                                         *
!     From A. H. Wapstra and K. Bos, Atomic and Nuclear Data Tables, 1977, 19, 185.                                * 
!     The isotopic abundances from the 1981-82 CRC tables were used.  Masses of atoms after Kr are also from CRC.  *
!*******************************************************************************************************************
      implicit none

      integer, private ::J   !local (private) var
      double precision, dimension(:) :: Iso_H(1:3)=(/1.007825037d0,2.014101787d0,3.016049286d0/)
      double precision, dimension(:) :: Iso_He(3:4)=(/3.016029297d0,4.002603250d0/)
      double precision, dimension(:) :: Iso_Li(6:7)=(/6.015123200d0,7.016004500d0/)
      double precision, dimension(:) :: Iso_Be(9:10)=(/9.012182500d0,10.0135d0/)
      double precision, dimension(:) :: Iso_B(10:11)=(/10.012938000d0,11.009305300d0/)
      double precision, dimension(:) :: Iso_C(11:14)=(/11.01065d0,12.000000000d0,13.003354839d0,14.003241000d0/)
      double precision, dimension(:) :: Iso_N(14:15)=(/14.003074008d0,15.000108978d0/)
      double precision, dimension(:) :: Iso_O(16:18)=(/15.994914640d0,16.999130600d0,17.999159390d0/)
      double precision, dimension(:) :: Iso_F(18:19)=(/18.00087d0,18.998403250d0/)
      double precision, dimension(:) :: Iso_Ne(20:22)=(/19.992439100d0,20.993845300d0,21.991383700d0/)
      double precision, dimension(:) :: Iso_Na(23:23)=(/22.989769700d0/)
      double precision, dimension(:) :: Iso_Mg(24:24)=(/23.985045000d0/)
      double precision, dimension(:) :: Iso_Al(27:27)=(/26.981541300d0/)
      double precision, dimension(:) :: Iso_Si(28:30)=(/27.976928400d0,28.976496400d0,29.973771700d0/)
      double precision, dimension(:) :: Iso_P(31:33)=(/30.973763400d0,31.973907d0,32.971725d0/)
      double precision, dimension(:) :: Iso_S(32:36)= &
                                        (/31.972071800d0,32.971459100d0,33.967867740d0,34.968852729d0,35.967079000d0/)
      double precision, dimension(:) :: Iso_Cl(35:37)=(/34.968852729d0,35.968306d0,36.965902624d0/)
      double precision, dimension(:) :: Iso_Ar(36:40)=(/35.967545605d0,0.0d0,37.962732200d0,0.0d0,39.962383100d0/)
      double precision, dimension(:) :: Iso_Se(76:82)= &
                                     (/75.919206600d0,0.0d0,77.917304000d0,0.0d0,79.916520500d0,0.0d0,81.916709000d0/)
      double precision, dimension(:) :: Iso_Br(77:82)= &
                                      (/76.921378d0,77.921144d0,78.918336100d0,79.918528d0,80.916290000d0,81.916802d0/)
      double precision, dimension(:) :: Iso_I(124:129)= &
                                       (/123.906207d0,124.90462d0,125.905624d0,126.900400000d0,127.90581d0,128.904986d0/)

      integer, dimension(:) :: Isotope_Def(0:54)
      double precision, dimension(:) :: Imass_Def(0:107)

      data (Isotope_Def(J), J=0,18) /0,1,4,7,9,11,12,14,16,19,20,23,24,27,28,31,32,35,40/
      data (Isotope_Def(J), J=19,36) /39,40,45,48,51,52,55,56,59,58,63,64,69,74,75,80,79,84/
      data (Isotope_Def(J), J=37,54) /85,88,89,90,93,98,00,102,103,106,107,114,115,118,121,130,127,132/
      data (Imass_Def(J), J=0,107) /0.0d0,1.007825037d0,4.002603250d0,7.016004500d0,9.012182500d0,11.009305300d0,12.000000D0, &
       14.003074008d0, &
       15.994914640d0,18.998403250d0,19.992439100d0,22.989769700d0,23.985045000d0,26.981541300d0,27.976928400d0,30.973763400d0, &
       31.972071800d0,34.968852729d0,39.962383100d0,38.963707900d0,39.962590700d0,44.955913600d0,47.947946700d0,50.943962500d0, &
       51.940509700d0,54.938046300d0,55.934939300d0,58.933197800d0,57.935347100d0,62.929599200d0,63.929145400d0,68.925580900d0, &
       73.921178800d0,74.921595500d0,79.916520500d0,78.918336100d0,83.911506400d0,84.911700000d0,87.905600000d0,89.905400000d0, &
       89.904300000d0,92.906000000d0,97.905500000d0,98.906300000d0,101.903700000d0,102.904800000d0,105.903200000d0, &
       106.905090000d0, &
       113.903600000d0,114.904100000d0,117.901800000d0,120.903800000d0,129.906700000d0,126.900400000d0,131.904200000d0, &
            132.905429000d0,137.905000000d0,138.906100000d0,139.905300000d0,140.907400000d0,141.907500000d0,144.912700000d0, &
            151.919500000d0,152.920900000d0,157.924100000d0,158.925000000d0,163.928800000d0,164.930300000d0,165.930400000d0, &
            168.934400000d0,173.939000000d0,174.940900000d0,179.946800000d0,180.948000000d0,183.951000000d0,186.956000000d0, &
            189.958600000d0,192.963300000d0,194.964800000d0,196.966600000d0,201.970600000d0,204.974500000d0,207.976600000d0, &
            208.980400000d0,208.982500000d0,210.987500000d0,222.017500000d0,223.019800000d0,226.025400000d0,227.027800000d0, &
            232.038200000d0,231.035900000d0,238.050800000d0,237.048000000d0,242.058700000d0,243.061400000d0,246.067400000d0, &
            247.070200000d0,249.074800000d0,252.082900000d0,252.082700000d0,255.090600000d0,259.101000000d0,262.109700000d0, &
            261.108700000d0,262.114100000d0,266.121900000d0,264.124700000d0/

CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      logical function Set_Def_Isotope (ATOM_NUM, ISOTOPE_NUM)
!***********************************************************************
!     Date last modified: May 05, 1999                    Version 1.0  *
!     Author: M.K.Brooker                                              *
!     Description: sets default isotope for given atom number,         *
!                 returns 1 when successful and 0 when it fails        *
!***********************************************************************
      implicit none
!
!Input Parameter
      integer ATOM_NUM
!
!Output Parameter
      integer ISOTOPE_NUM
!
      Set_Def_Isotope = .true.

! check if default isotope exists,if not report error
      if((ATOM_NUM.LT.(LBOUND(Isotope_Def,1))).or.(ATOM_NUM.GT.(UBOUND(Isotope_Def,1))))then
        print *,'Set_Def_Isotope=>',ATOM_NUM, 'atoms isotope unsupported'
        Set_Def_Isotope = .false.    ! function returns failure

      else           ! default isotope supported and assigned
        ISOTOPE_NUM = Isotope_Def(ATOM_NUM)
        Set_Def_Isotope = .true.    ! function returns success
      end if
      end function Set_Def_Isotope
      logical function CheckIsotope (ATOM_NUM, ISOTOPE_NUM)
!***********************************************************************
!     Date last modified: May 05, 1999                    Version 1.0  *
!     Author: M.K.Brooker                                              *
!     Description: gets atomic masses for isotopes                     *
!***********************************************************************
      implicit none
!
!Input Parameter
      integer ATOM_NUM,ISOTOPE_NUM

      CheckIsotope = .true.  !set return value to true

      select case (ATOM_NUM)
      case (0)
!      Dummy atom, do nothing
      case (1)
        if((ISOTOPE_NUM.LT.(LBOUND(Iso_H,1))).or.(ISOTOPE_NUM.GT.(UBOUND(Iso_H,1))))then
          print *,'CheckIsotope=>atom number',ATOM_NUM, ': isotope unsupported'
          CheckIsotope = .false.
        end if
      case (2)
        if((ISOTOPE_NUM.LT.(LBOUND(Iso_He,1))).or.(ISOTOPE_NUM.GT.(UBOUND(Iso_He,1))))then
          print *,'CheckIsotope=>atom number',ATOM_NUM, ': isotope unsupported'
          CheckIsotope = .false.
        end if
      case (3)
        if((ISOTOPE_NUM.LT.(LBOUND(Iso_Li,1))).or.(ISOTOPE_NUM.GT.(UBOUND(Iso_Li,1))))then
          print *,'CheckIsotope=>atom number',ATOM_NUM, ': isotope unsupported'
          CheckIsotope = .false.
        end if
      case (4)
        if((ISOTOPE_NUM.LT.(LBOUND(Iso_Be,1))).or.(ISOTOPE_NUM.GT.(UBOUND(Iso_Be,1))))then
          print *,'CheckIsotope=>atom number',ATOM_NUM, ': isotope unsupported'
          CheckIsotope = .false.
        end if
      case (5)
        if((ISOTOPE_NUM.LT.(LBOUND(Iso_B,1))).or.(ISOTOPE_NUM.GT.(UBOUND(Iso_B,1))))then
          print *,'CheckIsotope=>atom number',ATOM_NUM, ': isotope unsupported'
          CheckIsotope = .false.
        end if
      case (6)
        if((ISOTOPE_NUM.LT.(LBOUND(Iso_C,1))).or.(ISOTOPE_NUM.GT.(UBOUND(Iso_C,1))))then
          print *,'CheckIsotope=>atom number',ATOM_NUM, ': isotope unsupported'
          CheckIsotope = .false.
        end if
      case (7)
        if((ISOTOPE_NUM.LT.(LBOUND(Iso_N,1))).or.(ISOTOPE_NUM.GT.(UBOUND(Iso_N,1))))then
          print *,'CheckIsotope=>atom number',ATOM_NUM, ': isotope unsupported'
          CheckIsotope = .false.
        end if
      case (8)
!       write(6,*)'ATOM_num, isotope_num: ',ATOM_num,isotope_num
        if((ISOTOPE_NUM.LT.(LBOUND(Iso_O,1))).or.(ISOTOPE_NUM.GT.(UBOUND(Iso_O,1))))then
          print *,'CheckIsotope=>atom number',ATOM_NUM, ': isotope unsupported'
          CheckIsotope = .false.
        end if
      case (9)
        if((ISOTOPE_NUM.LT.(LBOUND(Iso_F,1))).or.(ISOTOPE_NUM.GT.(UBOUND(Iso_F,1))))then
          print *,'CheckIsotope=>atom number',ATOM_NUM, ': isotope unsupported'
          CheckIsotope = .false.
        end if
      case (10)
        if((ISOTOPE_NUM.LT.(LBOUND(Iso_Ne,1))).or.(ISOTOPE_NUM.GT.(UBOUND(Iso_Ne,1))))then
          print *,'CheckIsotope=>atom number',ATOM_NUM, ': isotope unsupported'
          CheckIsotope = .false.
        end if
      case (14)
        if((ISOTOPE_NUM.LT.(LBOUND(Iso_Si,1))).or.(ISOTOPE_NUM.GT.(UBOUND(Iso_Si,1))))then
          print *,'CheckIsotope=>atom number',ATOM_NUM, ': isotope unsupported'
          CheckIsotope = .false.
        end if
      case (15)
        if((ISOTOPE_NUM.LT.(LBOUND(Iso_P,1))).or.(ISOTOPE_NUM.GT.(UBOUND(Iso_P,1))))then
          print *,'CheckIsotope=>atom number',ATOM_NUM, ': isotope unsupported'
          CheckIsotope = .false.
        end if
      case (16)
        if((ISOTOPE_NUM.LT.(LBOUND(Iso_S,1))).or.(ISOTOPE_NUM.GT.(UBOUND(Iso_S,1))))then
          print *,'CheckIsotope=>atom number',ATOM_NUM, ': isotope unsupported'
          CheckIsotope = .false.
        end if
      case (17)
        if((ISOTOPE_NUM.LT.(LBOUND(Iso_Cl,1))).or.(ISOTOPE_NUM.GT.(UBOUND(Iso_Cl,1))))then
          print *,'CheckIsotope=>atom number',ATOM_NUM, ': isotope unsupported'
          CheckIsotope = .false.
        end if
      case (34)
        if((ISOTOPE_NUM.LT.(LBOUND(Iso_Se,1))).or.(ISOTOPE_NUM.GT.(UBOUND(Iso_Se,1))))then
          print *,'CheckIsotope=>atom number',ATOM_NUM, ': isotope unsupported'
          CheckIsotope = .false.
        end if
      case (35)
        if((ISOTOPE_NUM.LT.(LBOUND(Iso_Br,1))).or.(ISOTOPE_NUM.GT.(UBOUND(Iso_Br,1))))then
          print *,'CheckIsotope=>atom number',ATOM_NUM, ': isotope unsupported'
          CheckIsotope = .false.
        end if
      case (53)
        if((ISOTOPE_NUM.LT.(LBOUND(Iso_I,1))).or.(ISOTOPE_NUM.GT.(UBOUND(Iso_I,1))))then
          print *,'CheckIsotope=>atom number',ATOM_NUM, ': isotope unsupported'
          CheckIsotope = .false.
        end if
      case default
        if((ATOM_NUM.LT.(LBOUND(Isotope_Def,1))).or.(ATOM_NUM.GT.(UBOUND(Isotope_Def,1))))then
          print *,'CheckIsotope=>atom number',ATOM_NUM, ': isotope unsupported'
          CheckIsotope = .false.
        end if
      end select
      end function CheckIsotope
      end module atommass
