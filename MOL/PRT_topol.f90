      subroutine PRT_GRAPH_CONNECT
!****************************************************************************************
!     Date last modified: July 12, 2001                                                 *
!     Author: R.A. Poirier                                                              *
!     Description: Print the molecular connectivity matrix.                             *
!****************************************************************************************
! Modules:
      USE program_files
      USE matrix_print
      USE GRAPH_objects

      implicit none
!
! Local scalars:
!
      write(UNIout,'(a)')'PRT_GRAPH_CONNECT>'
      write(UNIout,'(a,i6)')'Total number of vertices: ',NVertices
      write(UNIout,'(a)')'Connectivity matrix (1 bonded, 0 non-bonded):'
      call PRT_matrix (CONNECT, NVertices, NVertices)
!
! end of subroutine PRT_GRAPH_CONNECT
      return
      end subroutine PRT_GRAPH_CONNECT
      subroutine PRT_GRAPH_CNCOMP
!*****************************************************************************************************************
!     Date last modified: July 12, 2001                                                             Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Print the molecular components information.                                                   *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE GRAPH_objects

      implicit none
!
! Local scalars:
      integer :: Iatom,Icomponent
!
      write(UNIout,'(a)')'PRT_GRAPH_CNCOMP>'
      write(UNIout,'(a,I6)')'Number of molecular components: ',NComponents
      write(UNIout,'(a,I6)')'0 for dummy atoms, 1, 2, ... for components'
      write(UNIout,'(a)')'Atom Number Component Number'
      do Iatom=1,NVertices
        write(UNIout,'(5x,i6,10x,i6)')Iatom,CNCOMP(Iatom)
      end do
!
      write(UNIout,'(/a,I6)')' Number of components: ',Ncomponents
      write(UNIout,'(a)')' Number of atoms per components:'
      do Icomponent=1,NComponents
        write(UNIout,'(I5,1x,I5)')Icomponent,COMPEN(Icomponent)
      end do ! ! Icomponent
!
! end of subroutine PRT_GRAPH_CNCOMP
      return
      end subroutine PRT_GRAPH_CNCOMP
      subroutine PRT_GRAPH_MAKEBOND
!*****************************************************************************************************************
!     Date last modified: July 13, 2001                                                             Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Print the molecular components information.                                                   *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE GRAPH_objects
      USE matrix_print

      implicit none
!
! Local scalars:
      integer IBond
!
      write(UNIout,'(a)')'PRT_GRAPH_MAKEBOND>'
      write(UNIout,'(a,i6)')'Number of chemical bonds: ',NGBonds
      write(UNIout,'(a)')' BOND  order ---ATOMS---'
      do IBond=1,NGBonds
        write(UNIout,'(I5,1x,I5,1x,2I5)')IBond,BOND(IBond)%type,BOND(IBond)%ATOM1,BOND(IBond)%ATOM2
      end do ! ! IBond

! end of subroutine PRT_GRAPH_MAKEBOND
      return
      end subroutine PRT_GRAPH_MAKEBOND
      subroutine PRT_GRAPH_MAKEANG
!*****************************************************************************************************************
!     Date last modified: July 13, 2001                                                             Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Print the molecular components information.                                                   *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE GRAPH_objects

      implicit none
!
! Local scalars:
      integer Iatom
!
      write(UNIout,'(a)')'PRT_GRAPH_MAKEANG>'
      write(UNIout,'(a,i6)')'Number of angles: ',NGAngle
      write(UNIout,'(a)')' ANGLE -------ATOMS-------'
      do Iatom=1,NGANGLE
        write(UNIout,'(I5,1x,I5,1x,I5,1x,I5)')Iatom,ANGLE(Iatom)%ATOM1,ANGLE(Iatom)%ATOM2,ANGLE(Iatom)%ATOM3
      end do ! ! Iatom
!
! end of subroutine PRT_GRAPH_MAKEANG
      return
      end subroutine PRT_GRAPH_MAKEANG
      subroutine PRT_GRAPH_MAKETORS
!*****************************************************************************************************************
!     Date last modified: July 13, 2001                                                             Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Print the molecular components information.                                                   *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE GRAPH_objects

      implicit none
!
! Local scalars:
      integer Iatom
!
      write(UNIout,'(a)')'PRT_GRAPH_MAKETORS>'
      write(UNIout,'(a,i6)')'Number of torsions: ',NGTorsion
      write(UNIout,'(a)')' TORSION --------ATOMS--------'
      do Iatom=1,NGTORSION
        write(UNIout,'(I5,1x,I5,1x,I5,1x,I5,1x,I5)')Iatom,TORSION(Iatom)%ATOM1,TORSION(Iatom)%ATOM2,TORSION(Iatom)%ATOM3, &
             TORSION(Iatom)%ATOM4
      end do ! ! Iatom
!
! end of subroutine PRT_GRAPH_MAKETORS
      return
      end subroutine PRT_GRAPH_MAKETORS
      subroutine PRT_GRAPH_CLOSE_CONT
!*****************************************************************************************************************
!     Date last modified: July 13, 2001                                                             Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Print the molecular components information.                                                   *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE matrix_print
      USE GRAPH_objects

      implicit none
!
! Local scalars:
!
      write(UNIout,'(a)')'PRT_GRAPH_CLOSE_CONT>'
      write(UNIout,'(a)')' Close contacts:'
      call PRT_matrix (CONTACT, NVertices, NVertices)
!
! end of subroutine PRT_GRAPH_CLOSE_CONT
      return
      end subroutine PRT_GRAPH_CLOSE_CONT
      subroutine PRT_GRAPH_COMP_CONN
!*****************************************************************************************************************
!     Date last modified: July 13, 2001                                                             Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Print the molecular components information.                                                   *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE matrix_print
      USE GRAPH_objects

      implicit none
!
! Local scalars:
!
      write(UNIout,'(a)')'PRT_GRAPH_COMP_CONN>'
      write(UNIout,'(a)')' Component close contacts'
      call PRT_matrix (COMCON, Ncomponents, Ncomponents)
!
! end of subroutine PRT_GRAPH_COMP_CONN
      return
      end subroutine PRT_GRAPH_COMP_CONN
      subroutine PRT_GRAPH_COMP_CONN_last
!*****************************************************************************************************************
!     Date last modified: July 13, 2001                                                             Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Print the molecular components information.                                                   *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE matrix_print
      USE GRAPH_objects

      implicit none
!
! Local scalars:
!
      write(UNIout,'(a)')'PRT_GRAPH_COMP_CONN_last>'
      write(UNIout,'(a)')'ICOCON:'
      call PRT_matrix (ICOCON, NVertices, NVertices)
!
! end of subroutine PRT_GRAPH_COMP_CONN_last
      return
      end subroutine PRT_GRAPH_COMP_CONN_last
      subroutine PRT_GRAPH_VWEAK_BOND
!*****************************************************************************************************************
!     Date last modified: July 13, 2001                                                             Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Print the molecular components information.                                                   *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE GRAPH_objects

      implicit none
!
! Local scalars:
      integer IvWBond
!
      write(UNIout,'(a)')'PRT_GRAPH_VWEAK_BOND>'
      write(UNIout,'(a,I6)')'Number of very weak Bonds: ', NvWBond
      if(NvWBond.gt.0)then
        write(UNIout,'(a)')'Very Weak Bond Atoms:'
        do IvWBond=1,NvWBond
          write(UNIout,'(I5,1x,I5,1x,I5)')IvWBond,vWBOND(IvWBond)%ATOM1,vWBOND(IvWBond)%ATOM2
        end do ! ! IvWBond
      end if
!
! end of subroutine PRT_GRAPH_VWEAK_BOND
      return
      end subroutine PRT_GRAPH_VWEAK_BOND
      subroutine PRT_GRAPH_VderW_BOND
!*****************************************************************************************************************
!     Date last modified: July 13, 2001                                                             Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Print the molecular components information.                                                   *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE GRAPH_objects

      implicit none
!
! Local scalars:
      integer IvderWBond
!
      write(UNIout,'(a)')'PRT_GRAPH_VderW_BOND>'
      write(UNIout,'(a,I6)')' Number of Van der Waals Bonds: ', NvderWBond
      if(NvderWBond.gt.0)then
        write(UNIout,'(a)')' Bond Atom1 Atom2'
        do IvderWBond=1,NvderWBond
          write(UNIout,'(I5,1x,I5,1x,I5)')IvderWBond,VdWBond(IvderWBond)%ATOM1,VdWBond(IvderWBond)%ATOM2
        end do ! ! IvderWBond
      end if
!
! end of subroutine PRT_GRAPH_VderW_BOND
      return
      end subroutine PRT_GRAPH_VderW_BOND
      subroutine PRT_GRAPH_PRUNE
!*****************************************************************************************************************
!     Date last modified: July 13, 2001                                                             Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Print the molecular components information.                                                   *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE matrix_print
      USE GRAPH_objects

      implicit none
!
! Local scalars:
      integer Iprune,Jprune
!
      write(UNIout,'(a)')'PRT_GRAPH_PRUNE>'

      write(UNIout,'(a)')'Vertices for Pruned Graph (PRINDX):'
      write(UNIout,'(a)')'New Vertex Old Vertex'
      do Iprune=1,NPRUNE
        write(UNIout,'(i6,5x,i6)')Iprune,PRINDX(Iprune)
      end do ! ! Iprune

      write(UNIout,'(a)')'Connectivity for Pruned Graph (PRUNCN):'
      call PRT_matrix (PRUNCN, NPrune, NPrune)
!
! end of subroutine PRT_GRAPH_PRUNE
      return
      end subroutine PRT_GRAPH_PRUNE
      subroutine PRT_GRAPH_HOMREDUCED
!*****************************************************************************************************************
!     Date last modified: July 13, 2001                                                             Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Print the molecular components information.                                                   *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE matrix_print
      USE GRAPH_objects

      implicit none
!
! Local scalars:
      integer Iathrp,IPrune
!
      write(UNIout,'(a)')'PRT_GRAPH_HOMREDUCED>'
      write(UNIout,'(a)')'IPrune HMRAT1  HMRAT2'
      do IPrune=1,NPrune
        write(UNIout,'(3i6)')IPrune,HMRAT1(IPrune),HMRAT2(IPrune)
      end do ! ! IPrune

      write(UNIout,'(a)')'PRT_GRAPH_HOMREDUCED> HMINDX, HMRPCN'
      write(UNIout,'(a,i6)')'Nathrp: ',Nathrp
      write(UNIout,'(a)')'Vertices for Homeomorphically Reduced pseudo-Graph (HMINDX):'
      do Iathrp=1,Nathrp
        write(UNIout,'(i6,i6)')Iathrp,HMINDX(Iathrp)
      end do ! ! Iathrp

      write(UNIout,'(a)')'Connectivity for Homeomorphically Reduced pseudo-Graph (HMRPCN):'
      call PRT_matrix (HMRPCN, Nathrp, Nathrp)
!
! end of subroutine PRT_GRAPH_HOMREDUCED
      return
      end subroutine PRT_GRAPH_HOMREDUCED
      subroutine PRT_GRAPH_INCIDENCE
!*****************************************************************************************************************
!     Date last modified: July 13, 2001                                                             Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Print the molecular components information.                                                   *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE matrix_print
      USE GRAPH_objects

      implicit none
!
! Local scalars:
      integer Iatom
!
      write(UNIout,'(a)')'PRT_GRAPH_INCIDENCE>'
      write(UNIout,'(a)')'HRPINC:'
      call PRT_matrix (HRPINC, Nathrp, Nedge)
!
! end of subroutine PRT_GRAPH_INCIDENCE
      return
      end subroutine PRT_GRAPH_INCIDENCE
      subroutine PRT_GRAPH_FUND_RINGS
!*****************************************************************************************************************
!     Date last modified: July 13, 2001                                                             Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Print the molecular components information.                                                   *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE matrix_print
      USE GRAPH_objects

      implicit none
!
      write(UNIout,'(a)')'PRT_GRAPH_FUND_RINGS>'
      write(UNIout,'(a,I6)')'Number of Rings: ',Nring
      write(UNIout,'(a,I6)')'Number of Maximum ring edges: ',MXedgr
      if(Nring.GT.0)then
        write(UNIout,'(a)')'FUNDAM:'
        call PRT_matrix (Fundam, Nring, MXedgr)
      end if
!
! end of subroutine PRT_GRAPH_FUND_RINGS
      return
      end subroutine PRT_GRAPH_FUND_RINGS
      subroutine PRT_GRAPH_RING_ASSEMBLY
!*****************************************************************************************************************
!     Date last modified: July 13, 2001                                                             Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Print the molecular components information.                                                   *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE GRAPH_objects

      implicit none
!
! Local scalars:
      integer Iedge
!
      write(UNIout,'(a)')'PRT_GRAPH_RING_ASSEMBLY>'
      write(UNIout,'(a,i6)')'Nedge: ',Nedge
      write(UNIout,'(a)')'RNASSE:'
      write(UNIout,'(a)')' Edge   Ring'
      do Iedge=1,Nedge
        write(UNIout,'(i6,i6)')Iedge,RNASSE(Iedge)
      end do ! ! Iathrp

!
! end of subroutine PRT_GRAPH_RING_ASSEMBLY
      return
      end subroutine PRT_GRAPH_RING_ASSEMBLY
      subroutine PRT_GRAPH_ATOM_BETWEEN
!*****************************************************************************************************************
!     Date last modified: July 13, 2001                                                             Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Print the molecular components information.                                                   *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE matrix_print
      USE GRAPH_objects

      implicit none
!
! Local scalars:
      integer Iatom
!
      write(UNIout,'(a)')'PRT_GRAPH_ATOM_BETWEEN>'
      write(UNIout,'(a,i6,a,i6)')'Nedge= ',Nedge,' DIMLST= ',DIMLST
      write(UNIout,'(a)')'INBETW:'
      call PRT_matrix (INBETW, Nedge, DIMLST)
!
! end of subroutine PRT_GRAPH_ATOM_BETWEEN
      return
      end subroutine PRT_GRAPH_ATOM_BETWEEN
      subroutine PRT_GRAPH_EDGE_RINGASS
!*****************************************************************************************************************
!     Date last modified: July 13, 2001                                                             Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Print the molecular components information.                                                   *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE GRAPH_objects

      implicit none
!
! Local scalars:
      integer IGBond,IRing
!
      write(UNIout,'(a)')'PRT_GRAPH_EDGE_RINGASS>'
      write(UNIout,'(a,i6)')'Number of chemical bonds: ',NGBonds
      if(NRing.gt.0)then
      write(UNIout,'(a,i4,a)')'Have found ',NRing,' rings'
      write(UNIout,'(a)')'Ring Sizes:'
      write(UNIout,'(a)')'Ring    size'
      do IRing=1,NRing
        write(UNIout,'(i3,5x,i3)')IRing,Ring_size(IRing)
      end do
      end if
      write(UNIout,'(a)')'EDGE RING ASSEMBLIES'
      do IGBond=1,NGBonds
        write(UNIout,'(a,i8,a,i8)')'Assigned bond ',IGBond, &
                                    ' to ring assembly ',edges(IGBond)%ring
        write(UNIout,'(a,2i8)')'Bond atoms: ', &
                  edges(IGBond)%vertexI,edges(IGBond)%vertexJ
      end do ! ! IGBond
!
! end of subroutine PRT_GRAPH_EDGE_RINGASS
      return
      end subroutine PRT_GRAPH_EDGE_RINGASS
      subroutine PRT_MAKE_OOPBENDS
!*****************************************************************************************************************
!     Date last modified: July 13, 2001                                                             Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Print the molecular components information.                                                   *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE GRAPH_objects

      implicit none
!
! Local scalars:
      integer IOOPBEND
!
      write(UNIout,'(a)')'PRT_MAKE_OOPBENDS>'
      write(UNIout,'(a)')'Number of Out-of-Plane-Bend: ',NOOPBEND
      if(NOOPBEND.gt.0)then
        write(UNIout,'(a)')'Angle ---------Atoms---------'
        do IOOPBEND=1,NOOPBEND
        write(UNIout,'(5i6)')IOOPBEND,oopbend(IOOPBEND)%atom1,oopbend(IOOPBEND)%atom2, &
                                       oopbend(IOOPBEND)%atom3,oopbend(IOOPBEND)%atom4
        end do
      end if
!
! end of subroutine PRT_MAKE_OOPBENDS
      return
      end subroutine PRT_MAKE_OOPBENDS
      subroutine PRT_CONN_NBOND
!*****************************************************************************************************************
!     Date last modified: July 13, 2001                                                             Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Print the molecular components information.                                                   *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE matrix_print
      USE GRAPH_objects

      implicit none
!
! Local scalars:
      integer Iatom
!
      write(UNIout,'(a)')'PRT_CONN_NBOND>'
      write(UNIout,'(a,I6)')'Number of non-bonded: ',NNONBond
      write(UNIout,'(a)')'Non-bonded connectivity:'
      call PRT_matrix (CONN_NB, NVertices, NVertices)
!
! end of subroutine PRT_CONN_NBOND
      return
      end subroutine PRT_CONN_NBOND
      subroutine PRT_GRAPH_NO_BONDS
!*****************************************************************************************************************
!     Date last modified: July 13, 2001                                                             Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Print the molecular components information.                                                   *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE GRAPH_objects

      implicit none
!
! Local scalars:
      integer Ielec
!
      write(UNIout,'(a)')'PRT_GRAPH_NO_BONDS>'
      write(UNIout,'(a,i6)')'Number of non-bonded: ',Nelec
      write(UNIout,'(a)')'PAIR_ij:'
      write(UNIout,'(a)')'  Bond    --Atoms--   elec  vdw   tors'
      do Ielec=1,Nelec
        write(UNIout,'(6i6)')Ielec,pair_IJ(Ielec)%I,pair_IJ(Ielec)%J, &
                                   pair_IJ(Ielec)%is_elec,pair_IJ(Ielec)%is_vdw,pair_IJ(Ielec)%is_tors
      end do ! Ielec
!
! end of subroutine PRT_GRAPH_NO_BONDS
      return
      end subroutine PRT_GRAPH_NO_BONDS
      subroutine PRT_GRAPH_VALENCE
!***************************************************************************************
!     Date last modified: July 13, 2001                                                *
!     Author: R.A. Poirier                                                             *
!     Description: Print the molecular components information.                         *
!***************************************************************************************
! Modules:
      USE program_files
      USE GRAPH_objects

      implicit none
!
! Local scalars:
      integer Iatom
!
      write(UNIout,'(a)')'PRT_GRAPH_VALENCE>'
      write(UNIout,'(a)')'CONVAL:'
      do Iatom=1,NVertices
        write(UNIout,'(2i6)')Iatom,CONVAL(Iatom)
      end do ! Iatom
!
! end of subroutine PRT_GRAPH_VALENCE
      return
      end subroutine PRT_GRAPH_VALENCE
      subroutine PRT_GRAPH_ALL
!*****************************************************************************************************************
!     Date last modified: August 3, 2005                                                            Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Print the molecular connectivity matrix.                                                      *
!*****************************************************************************************************************
! Modules:
      USE program_files

      implicit none
!
! Local scalars:
!
      write(UNIout,'(a)')'************************************************************'
      write(UNIout,'(a)')'Topological objects:'
      write(UNIout,'(a)')'************************************************************'
      call PRT_GRAPH_CONNECT
      call PRT_GRAPH_VALENCE
      call PRT_GRAPH_CNCOMP
      call PRT_GRAPH_CLOSE_CONT
      call PRT_GRAPH_COMP_CONN
      call PRT_GRAPH_COMP_CONN_last
      call PRT_GRAPH_PRUNE
      call PRT_GRAPH_HOMREDUCED
      call PRT_GRAPH_INCIDENCE
      call PRT_GRAPH_FUND_RINGS
      call PRT_GRAPH_RING_ASSEMBLY
      call PRT_GRAPH_ATOM_BETWEEN
      call PRT_GRAPH_EDGE_RINGASS

      write(UNIout,'(a)')'************************************************************'
      write(UNIout,'(a)')'Coordinates built using topological objects:'
      write(UNIout,'(a)')'************************************************************'
      call PRT_GRAPH_MAKEBOND
      call PRT_GRAPH_MAKEANG
      call PRT_GRAPH_MAKETORS
      call PRT_GRAPH_VWEAK_BOND
      call PRT_GRAPH_VderW_BOND
      call PRT_GRAPH_NO_BONDS
!
! end of subroutine PRT_GRAPH_ALL
      return
      end subroutine PRT_GRAPH_ALL
