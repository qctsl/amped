      subroutine OEP_Nuclear  
!***********************************************************************
!     Date last modified: June 15, 2006                    Version 1.0 *
!     Author: R. Poirier                                               *
!     Description: Evaluate the nuclear component to the dipole and    *
!     second moment
!***********************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE type_molecule

      implicit none
!
! Local scalar:
      integer :: Iatom
      integer :: TCharge ! Total Nuclear charge(integer)
      double precision :: AN ! Nuclear Charge
      double precision :: TNuclearCharge ! Total Nuclear charge(real)
      double precision :: shape(3,3),eigvec(3,3),Diag(3),scrvec(3)
      double precision :: DipoleX,DipoleY,DipoleZ
      double precision :: SecondXX,SecondXY,SecondXZ,SecondYY,SecondYZ,SecondZZ

      DipoleX=ZERO
      DipoleY=ZERO
      DipoleZ=ZERO
      SecondXX=ZERO
      SecondXY=ZERO
      SecondYY=ZERO
      SecondYZ=ZERO
      SecondXZ=ZERO
      SecondZZ=ZERO

! Add in nuclear contribution.
      do Iatom=1,Natoms
        IF(CARTESIAN(Iatom)%Atomic_number.GT.0)then
        AN=CARTESIAN(Iatom)%Atomic_number
        DipoleX=DipoleX+CARTESIAN(Iatom)%X*AN
        DipoleY=DipoleY+CARTESIAN(Iatom)%Y*AN
        DipoleZ=DipoleZ+CARTESIAN(Iatom)%Z*AN
      end if ! CARTESIAN(Iatom)%Atomic_number.GT.0
      end do ! Iatom

! Add in nuclear contribution.
       TCharge=0
       do Iatom=1,Natoms
         IF(CARTESIAN(Iatom)%Atomic_number.GT.0)then
         AN=CARTESIAN(Iatom)%Atomic_number
         TCharge=TCharge+AN
         SecondXX=SecondXX+(CARTESIAN(Iatom)%X)*(CARTESIAN(Iatom)%X)*AN
         SecondXY=SecondXY+(CARTESIAN(Iatom)%X)*(CARTESIAN(Iatom)%Y)*AN
         SecondYY=SecondYY+(CARTESIAN(Iatom)%Y)*(CARTESIAN(Iatom)%Y)*AN
         SecondYZ=SecondYZ+(CARTESIAN(Iatom)%Y)*(CARTESIAN(Iatom)%Z)*AN
         SecondXZ=SecondXZ+(CARTESIAN(Iatom)%X)*(CARTESIAN(Iatom)%Z)*AN
         SecondZZ=SecondZZ+(CARTESIAN(Iatom)%Z)*(CARTESIAN(Iatom)%Z)*AN
         end if ! CARTESIAN(Iatom)%Atomic_number.GT.0
       end do ! Iatom

! Make the property origin invariant!!
!      DipoleX=DipoleX
!      DipoleY=DipoleY
!      DipoleZ=DipoleZ

!      SecondXX=SecondXX
!      SecondXY=SecondXY
!      SecondYY=SecondYY
!      SecondYZ=SecondYZ
!      SecondXZ=SecondXZ
!      SecondZZ=SecondZZ

       TNuclearCharge=dble(TCharge)
       shape=zero
       shape(1,1)=SecondXX-DipoleX**2/TNuclearCharge
       shape(2,1)=SecondXY-DipoleX*DipoleY/TNuclearCharge
       shape(2,2)=SecondYY-DipoleY**2/TNuclearCharge
       shape(3,1)=SecondXZ-DipoleX*DipoleZ/TNuclearCharge
       shape(3,2)=SecondYZ-DipoleY*DipoleZ/TNuclearCharge
       shape(3,3)=SecondZZ-DipoleZ**2/TNuclearCharge

!      call PRT_matrix (shape, 3, 3)
       call MATRIX_diagonalize (shape, eigvec, Diag, 3, 1, .true.)
      OI_XX=Diag(1)
      OI_YY=Diag(2)
      OI_ZZ=Diag(3)

      return
      end subroutine OEP_nuclear
