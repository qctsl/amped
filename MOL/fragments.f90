      subroutine BLD_Fragments (NFragmentsIN)
!*****************************************************************************************************************
!     Date last modified: August 6, 2004                                                            Version 1.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Initialize the molecular fragments to be the entire molecule (A single fragment).             *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE type_molecule
      USE type_basis_set
      USE type_fragments

      implicit none
!
! Input scalars:
      integer, intent(IN) :: NFragmentsIN
!
      NFragments=NFragmentsIN
      if(NFragments.eq.1)then
        call INI_Fragments
      else
        if(associated(basis))then
          call BLD_fragments_pop ! Only option available at this time
          call BLD_ELbyFragment
          call PRT_fragments
        else
          write(UNIout,'(a)')'ERROR> BLD_fragments:'
          write(UNIout,'(a)')'The basis set must be defined before fragments can be defined'
          call PRG_stop ('The basis set must be defined before fragments can be defined')
        end if
      end if
!
      return
      end subroutine BLD_Fragments
      subroutine INI_Fragments
!*****************************************************************************************************************
!     Date last modified: August 6, 2004                                                            Version 1.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Initialize the molecular fragments to be the entire molecule (A single fragment).             *
!*****************************************************************************************************************
! Modules:
      USE type_molecule
      USE type_fragments

      implicit none
!
! Input scalars:
      integer :: Iatom
!
      if(.not.allocated(Atom_to_fragment))then
        allocate (Atom_to_fragment(Natoms))
      else
        deallocate (Atom_to_fragment)
        allocate (Atom_to_fragment(Natoms))
      end if
!
      NFragments=1
      do Iatom=1,Natoms
        Atom_to_Fragment(Iatom)=1
      end do ! Iatom

      return
      end subroutine INI_Fragments

      subroutine BLD_AOtofragment
!***********************************************************************
!     Date last modified: February 24, 1992                Version 1.0 *
!     Author: R. A. Poirier                                            *
!     Description: Initialize general basis set arrays.                *
!***********************************************************************
! Modules:
      USE program_files
      USE type_molecule
      USE type_basis_set
      USE type_fragments

      implicit none
!
! Local scalars:
      integer :: AOI,Fbasis,Iao,Iatom,Iatmshl,Ibasis,IFragment,Ifrst,Ilast,Irange,Ishell,Istart,Iend
!
! Begin:
      call PRG_manager ('enter', 'BLD_AOtofragment', 'UTILITY')

      if(associated(Basis))then
! May have been defined by the basis set menu
      if(.not.allocated(Basis_to_fragment))then
        allocate (Basis_to_fragment(Basis%Nbasis))
        Basis_to_fragment(1:Basis%Nbasis)=0
      else
        deallocate (Basis_to_fragment)
        allocate (Basis_to_fragment(Basis%Nbasis))
        Basis_to_fragment(1:Basis%Nbasis)=0
      end if
!
      do IFragment=1,NFragments
      Fbasis=0 
      do Ishell=1,Basis%Nshells
        Istart=Basis%shell(Ishell)%Xstart
        Iend=Basis%shell(Ishell)%Xend
        Irange=Iend-Istart+1
        Ifrst=Basis%shell(Ishell)%frstSHL
        Ilast=Basis%shell(Ishell)%lastSHL
        do Iatmshl=Ifrst,Ilast
          Iatom=Basis%atmshl(Iatmshl)%ATMLST
          AOI=Basis%atmshl(Iatmshl)%frstAO-1
          do Iao=1,Irange
            Fbasis=Iao+AOI
            Basis_to_fragment(Fbasis)=Atom_to_fragment(Iatom)
          end do ! Iao
        end do ! Iatmshl
      end do ! Ishell
      end do ! IFragment
      else
        write(UNIout,'(a)')'ERROR> BLD_AOtoFragment:'
        call PRG_stop ('Must define the basis set before fragments can be defined')
      end if

      call PRT_fragments

! End of routine BLD_AOtofragment
      call PRG_manager ('exit', 'BLD_AOtofragment', 'UTILITY')
      return
      end subroutine BLD_AOtofragment
      subroutine BLD_ELbyFragment
!***********************************************************************
!     Date last modified: February 24, 1992                Version 1.0 *
!     Author: R. A. Poirier                                            *
!     Description: Initialize general basis set arrays.                *
!***********************************************************************
! Modules:
      USE program_files
      USE type_molecule
      USE type_basis_set
      USE type_fragments

      implicit none
!
! Local scalars:
      integer :: Iatom,IFragment,IZ,Nelec
!
! Begin:
      call PRG_manager ('enter', 'BLD_ELbyFragment', 'UTILITY')
!
      if(.not.allocated(Electron_by_fragment))then
        allocate (Electron_by_fragment(Nelectrons))
      else
        deallocate (Electron_by_fragment)
        allocate (Electron_by_fragment(Nelectrons))
      end if

      do IFragment=1,NFragments
        Electron_by_fragment(IFragment)=0
      end do ! IFragment

      Nelec=0
      do Iatom=1,NAtoms
        IFragment=Atom_to_Fragment(Iatom)
        IZ=CARTESIAN(Iatom)%Atomic_number
        Electron_by_fragment(IFragment)=Electron_by_fragment(IFragment)+IZ
        Nelec=Nelec+IZ
      end do ! Iatom

      if(Nelec.ne.Nelectrons)then
        write(UNIout,'(a)')'ERROR> BLD_ELbyFragment: Number of electrons incorrect'
        write(UNIout,'(a)')'If the net charge is not zero, must provide the number of electrons via the menu'
        stop ')ERROR> BLD_ELbyFragment: Number of electrons incorrect'
      end if
!
! End of routine BLD_ELbyFragment
      call PRG_manager ('exit', 'BLD_ELbyFragment', 'UTILITY')
      return
      end subroutine BLD_ELbyFragment
      subroutine BLD_Fragments_pop
!*****************************************************************************************************************
!     Date last modified: Dec. 3rd, 2004                                                            Version 2.0  *
!     Author: Aisha El-Sherbiny                                                                                  *
!     Description: Dividing the molecule into fragments.                                                         *
!*****************************************************************************************************************
! Modules:
      USE type_molecule
      USE type_basis_set
      USE type_fragments
      USE GRAPH_objects
      USE QM_defaults
      USE pop_analysis

      implicit none
!
! Input scalars:
      integer :: Iatom,Jatom,seed_A,seed_B,seed_C
      integer, dimension(1) :: seed_new
      integer, dimension(2) :: min_AP
      integer, dimension(2) :: max_AP
      integer, dimension(:), allocatable :: terminal_list  
      double precision, dimension(:,:), allocatable :: copy_AP 
      
!Begin
      call PRG_manager ('enter', 'BLD_Fragments_pop', 'UTILITY')
!
      call INI_Fragments
!
! Set the basis set to STO-3G 
      call GET_basis_set ('STO-3G', GUESS_basis, .false.)
      Basis=>GUESS_basis
      call RESET_basis_set
!
! Now get the Mulliken population at STO-3G
      call GET_object ('QM', 'ATOMIC', 'MULLIKEN')
!
! Now reset to current basis ...
      call SET_MO_project
      call KILL_object ('QM', '1EINT', 'AO')
      call KILL_object ('QM', '2EINT', I2E_type)
!     MUN_PRTLEV=MUN_PRTLEV+1
      Basis=>Current_basis
      call RESET_basis_set
      call KILL_object ('QM', 'CMO_GUESS', 'RHF')
      call KILL_object ('QM', 'CMO', 'RHF')

      allocate(copy_AP(Natoms,Natoms),terminal_list(Natoms))

      NFragments=2 ! Only produces 2 fragments for now
      copy_AP = ABS(Atom_pop)
!      write(6,*)'copy_AP', copy_AP
      call GET_object ('MOL', 'GRAPH', 'CONVAL')
      terminal_list(1:Natoms) = 0
      do Iatom= 1,Natoms
        if(CARTESIAN(Iatom)%Atomic_number .eq. 0 .or. conval(Iatom).eq.1) then
          copy_AP(Iatom,1:Natoms) = 100
          copy_AP(1:Natoms,Iatom) = 100
          terminal_list(Iatom) = Iatom
        end if 
!         write(6,*)'Iatom,terminal_list(Iatom)', Iatom, terminal_list(Iatom)
      end do

      min_AP(1:2) = MINLOC(copy_AP)
      seed_A = min_AP(1)
      seed_B = min_AP(2)
      Atom_to_fragment(1:Natoms) = 0
      Atom_to_fragment(seed_A)= 1
      Atom_to_fragment(seed_B)= 2
      do Iatom= 1,Natoms
        if(CARTESIAN(Iatom)%Atomic_number.eq.1 .or. conval(Iatom).eq.1) then
          copy_AP(Iatom,1:Natoms) = -100
          copy_AP(1:Natoms,Iatom) = -100
        end if 
      end do
    
      do Iatom= 1,Natoms
        if(CARTESIAN(Iatom)%Atomic_number.ne.0)then
        if(copy_AP(Iatom,seed_A).ge.copy_AP(Iatom,seed_B))then
           Atom_to_fragment(Iatom) = Atom_to_fragment(seed_A)
        else
          Atom_to_fragment(Iatom) = Atom_to_fragment(seed_B)
        end if
        end if
      end do

      do Iatom =1,Natoms
        if(CARTESIAN(Iatom)%Atomic_number.ne.0.and.conval(Iatom).eq.1)then
          do Jatom =1, Natoms
            if(CARTESIAN(Jatom)%Atomic_number.ne.0.and.connect(Iatom,Jatom).eq.1) then
             Atom_to_fragment(Iatom)=Atom_to_fragment(Jatom)
            end if
          end do
         end if
       end do

      Nseed_atoms=2
      allocate (SEED_atoms(Nseed_atoms))
      SEED_atoms(1)=seed_A
      SEED_atoms(2)=seed_B

      call PRG_manager ('exit', 'BLD_Fragments_pop', 'UTILITY')
      return
      end subroutine BLD_Fragments_pop
      subroutine PRT_Fragments
!*****************************************************************************************************************
!     Date last modified: August 6, 2004                                                            Version 1.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Initialize the molecular fragments to be the entire molecule (A single fragment).             *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE type_molecule
      USE type_basis_set
      USE type_fragments

      implicit none
!
! Input scalars:
      integer :: Iatom,Ibasis,IFragment
!
! Print the Fragment information
      write(UNIout,'(/a,i8)')'***********Fragment Definition***********'
      write(UNIout,'(a,i8)')'Total number of Fragments: ',NFragments
      if(allocated(SEED_atoms))then
        Write(UNIout,'(a)')'SEED atoms:'
        Write(UNIout,'(20i6)')SEED_atoms(1:Nseed_atoms)
      end if
        write(UNIout,'(a,2x,a)')'Atom','Fragment'
      do Iatom=1,NAtoms
        write(UNIout,'(i4,2x,i8)')Iatom,Atom_to_fragment(Iatom)
      end do ! Iatom
!
      if(allocated(Electron_by_fragment))then
      write(UNIout,'(/a,2x,a)')'Fragment','Nelectrons '
      do IFragment=1,NFragments
        write(UNIout,'(i8,i8)')IFragment,Electron_by_fragment(IFragment)
      end do ! IFragment
      end if

      if(allocated(Basis_to_fragment))then
      write(UNIout,'(/a,a)')'Basis',' belongs to fragment'
      do Ibasis=1,BASIS%Nbasis
        write(UNIout,'(i4,4x,i8)')Ibasis,Basis_to_fragment(Ibasis)
      end do
      end if
      write(UNIout,'(X)')
!
      return
      end subroutine PRT_Fragments
