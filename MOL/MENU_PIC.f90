      subroutine MENU_PIC
!***********************************************************************
!     Date last modified: May 10, 1995                     Version 1.0 *
!     Author: C. C. Pye                                                *
!     Description: Set up Proper Internal Coordinates (PIC's).         *
!                  This is a menu-driven input of PIC's and is similar *
!                  in definition to the TEXAS format.                  *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE program_parser
      USE menu_gets
      USE type_molecule
      USE proper_internals
      USE GRAPH_objects
      USE OPT_objects

      implicit none
!
! Local scalar:
      logical done
!
! Scalars:
      integer :: NPICOR
      integer :: NPICMP
      integer :: MAX_components
      integer :: MAX_PIC
      integer :: Icomponent
      logical REDUND
      character(len=8) EXTENT
!
      type (PI_coordinates) PICw
!
! Begin:
      call PRG_manager ('enter', 'MENU_PIC', 'UTILITY')
      done=.false.
!
! Initialize
      OPT_parameters%name='PIC'
      NPICOR=0
      NPICMP=0
      REDUND=.false.  ! for now
      EXTENT='MIN'    ! for now
      MAX_PIC=30*Natoms
      MAX_components=300*Natoms
!
      PICw%Ncoordinates=MAX_PIC
      allocate (PICw%type(MAX_PIC), PICw%symmetry(MAX_PIC), PICw%scale(MAX_PIC), PICw%label(MAX_PIC), &
                PICw%atom1(MAX_components), PICw%atom2(MAX_components), PICw%atom3(MAX_components), &
                PICw%atom4(MAX_components), PICw%coeff(MAX_components), PICw%Ncomponents(MAX_PIC))
!
      call DEF_PICSYM
!
! Menu:
      do while (.not.done)
      call new_token (' PICoord:')
!
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      ' Command PIC :', &
      '   Purpose: Set up proper internal coordinates (PIC) ', &
      '   Syntax :', &
      '   NEWCoordinate <command> ', &
      '   AUTOmatic <command> ', &
      '   SYMmetry <command> ', &
      '   end'
!
      else if(token(1:4).EQ.'NEWC')then
        call SET_NEWPIC

      else if(token(1:4).EQ.'AUTO')then
        call PIC_AUTO
!
! SYMmetry
      else if(token(1:3).EQ.'SYM')then
        call PIC_SYM

      else
        call MENU_end (done)
      end if
!
      end do ! .not.done
!
! Since you have bothered to enter all these proper internal coordinates,
! You had better be optimizing with them.
!
      call BLD_PIC
! Print PIC's
      call PRT_PIC
!
!      call MOD_PIC_SYM
!
! End of routine MENU_PIC
      call PRG_manager ('exit', 'MENU_PIC', 'UTILITY')
      return
!
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine SET_NEWPIC
!***********************************************************************
!     Date last modified: May 10, 1995                     Version 1.0 *
!     Author: C. C. Pye                                                *
!     Description: Add new Proper internal coordinate.                 *
!***********************************************************************
! Modules:

      implicit none
!
! Local scalars:
      integer CURCOR,lentyp,I,lennam
      double precision SCALE
      character(len=MAX_string) type,ALLtype(7),CURNAM
      logical FOUND,done
!
! Initialize type array.
      DATA ALLtype/'STRE','INVR','BEND','OUT ','TORS','LIN1','LIN2'/

!
! Begin:
      call PRG_manager ('enter', 'SET_NEWPIC', 'UTILITY')
!
! Increment PIC counter; initialize component counter to zero.
      NPICOR=NPICOR+1
      CURCOR=0
!
      done=.false.
!
! Menu:
      do while (.not.done)
      call new_token (' NEWPIC:')
!
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      '   ADDcomp <command>: Add component to PIC ', &
      '   NAMe = <string>: Symbolic name for PIC ', &
      '   SCale = <real>: Scale factor for coordinate ', &
      '   TYpe = <string>: examples STREtch, BEND, TORSion, INVErsion ', &
      '   end'
!
      else if(token(1:3).EQ.'ADD')then
         call PIC_ADDCOMP (CURCOR)

      else if(token(1:3).EQ.'NAM')then
        call GET_value (CURNAM, lennam)
        PICw%label(NPICOR)=CURNAM(1:lennam)

      else if(token(1:2).EQ.'SC')then
        call GET_value (SCALE)
        PICw%scale(NPICOR)=SCALE

      else if(token(1:2).EQ.'TY')then
        call GET_value (type, lentyp)
        I=1
        FOUND=.FALSE.
        do while (.not.FOUND.and.I.LE.7)
          IF(type(1:4).EQ.ALLtype(I))then
            PICw%type(NPICOR)=I
            FOUND=.TRUE.
          else
            I=I+1
          end if
        end do
        IF(.not.FOUND)then
          write(UNIout,1000)NPICOR,CURNAM,type
          STOP
        end if

      else
           call MENU_end (done)
      end if
!
      end do ! .not.done
!
! Set number of components per coordinate
      PICw%Ncomponents(NPICOR)=CURCOR
!
! End of routine SET_NEWPIC
      call PRG_manager ('exit', 'SET_NEWPIC', 'UTILITY')
      return
 1000 FORMAT ('ERROR> SET_NEWPIC: UNDEFINED PIC type FOR COORDINATE (',I4,'), NAME = ',A8, ', type = ',A8)
      end subroutine SET_NEWPIC
      subroutine PIC_ADDCOMP (CURCOR)
!***********************************************************************
!     Date last modified: May 10, 1995                     Version 1.0 *
!     Author: C. C. Pye                                                *
!     Description: Add new Proper internal coordinate component.       *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer CURCOR
!
! Local scalars:
      integer nbbyte,nlist
      logical done
      double precision COEFF
      character*4 atmlst(10)
!
! Begin:
      call PRG_manager ('enter', 'PIC_ADDCOMP', 'UTILITY')
!
! Increment current counter and full counter.
      CURCOR=CURCOR+1
      NPICMP=NPICMP+1
!
      done=.false.
!
! Menu:
      do while (.not.done)
      call new_token (' ADDComp:')
!
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      '   AToms = <list>: Examples ( 1 4 5 ), ( H1 O2 H6 ) ', &
      '   COeff = <real> : Coefficient for this component ', &
      '   end'
!
      else if(token(1:2).EQ.'AT')then
        nlist=10
        nbbyte=4
        call GET_Clist (atmlst, nbbyte, 10, nlist)
        call atindx (atmlst,  nlist, CURCOR)

      else if(token(1:2).EQ.'CO')then
        call GET_value (COEFF)
        PICw%coeff(NPICMP)=COEFF

      else
           call MENU_end (done)
      end if
!
      end do ! .not.done
!
! End of routine PIC_ADDCOMP
      call PRG_manager ('exit', 'PIC_ADDCOMP', 'UTILITY')
      return
      end subroutine PIC_ADDCOMP
      subroutine ATINDX (atmlst,  nlist, CURCOR)
!***********************************************************************
!     Date last modified: May 10, 1995                     Version 1.0 *
!     Author: C. C. Pye                                                *
!     Description: Check stuff. Set atoms for PIC component.           *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer CURCOR,NLIST
      character*4 atmlst(10)
!
! Local scalars:
      integer ATMNUM,I
      character*4 Iatom
!
! Begin:
      call PRG_manager ('enter', 'ATINDX', 'UTILITY')
!
! Make sure type has been defined.
      IF(PICw%type(NPICOR).LT.1.or.PICw%type(NPICOR).GT.7) then
        write (UNIout,1030) NPICOR
        STOP
      end if
!
! Check to see if list has 2,3,4 atoms in it
      IF(NLIST.LT.2.or.NLIST.GT.4) then
        write(UNIout,1000)CURCOR,NPICOR
        stop
      end if
!
! Check list for consistency with type.
      IF(PICw%type(NPICOR).LE.2) then
        IF(NLIST.NE.2)then
          write(UNIout,1020)CURCOR,NPICOR
          STOP
        end if
      else IF(PICw%type(NPICOR).LE.3) then
        IF (NLIST.NE.3) then
          write(UNIout,1020)CURCOR,NPICOR
          STOP
        end if
      else
        IF (NLIST.NE.4) then
          write(UNIout,1020)CURCOR,NPICOR
          STOP
        end if
      end if !
!
! Loop over elements of atom list.
      do I=1,NLIST
        Iatom=atmlst(I)
        if(CHK_string(Iatom,'+-0123456789'))then ! You have specified the atom index directly.
          call CNV_StoI (Iatom, ATMNUM)
        else ! You have specified the atom symbol. Need to get its index.
          call FIND_ATOM_SYMBOL (Iatom, ATMNUM)
        end if
!
! Do we have a valid atom number?
        IF(atmnum.lt.1.or.atmnum.gt.natoms) then
          write(UNIout,1010)atmnum, natoms
          call PRG_stop ('Invalid atom')
        end if
!
! Is atom a dummy atom, floating atom or centre ?
        IF(index(CARTESIAN(ATMNUM)%element,'*').NE.0) then
          write(UNIout,1050)CURCOR,NPICOR
          stop
        end if
!
! Set appropriate array value
        IF(I.EQ.1)then
          PICw%atom1(NPICMP)=atmnum
        else IF(I.EQ.2) then
          PICw%atom2(NPICMP)=atmnum
        else IF(I.EQ.3) then
          PICw%atom3(NPICMP)=atmnum
        else IF(I.EQ.4) then
          PICw%atom4(NPICMP)=atmnum
        end if

      end do
!
! Check for internal consistency of component, i.e. atoms are distinct.
      IF(PICw%atom1(NPICMP).EQ.PICw%atom2(NPICMP))then
        write(UNIout,1040)CURCOR,NPICOR
        stop
      end if
      IF(PICw%type(NPICOR).GE.3)then
        IF(PICw%atom1(NPICMP).EQ.PICw%atom3(NPICMP).or. &
           PICw%atom2(NPICMP).EQ.PICw%atom3(NPICMP))then
          write(UNIout,1040)CURCOR,NPICOR
          stop
        end if
      end if
      IF(PICw%type(NPICOR).GE.4)then
        IF(PICw%atom1(NPICMP).EQ.PICw%atom4(NPICMP).or. &
           PICw%atom2(NPICMP).EQ.PICw%atom4(NPICMP).or. &
           PICw%atom3(NPICMP).EQ.PICw%atom4(NPICMP))then
          write(UNIout,1040)CURCOR,NPICOR
          stop
        end if
      end if
!
! End of routine ATINDX
      call PRG_manager ('exit', 'ATINDX', 'UTILITY')
      return
 1000 FORMAT ('ERROR> ATINDX: TOO FEW/MANY ATOMS IN COMPONENT',I3, ' OF COORDINATE ', I3)
 1010 FORMAT ('ERROR> ATINDX: ATOM INDEX ', I3, ' NOT VALID',/'0INDEX SHOULD range FROM 1 TO ', I3)
 1020 FORMAT ('ERROR> ATINDX: ATOM LIST FOR COMPONENT ',I3,' NOT CONSISTENT WITH type FOR COORDINATE ', I3)
 1030 FORMAT ('ERROR> ATINDX: type NOT SET FOR COORDINATE ', I3)
 1040 FORMAT ('ERROR> ATINDX: TWO ATOMS IDENTICAL IN COMPONENT ', I3,' OF COORDINATE ', I3)
 1050 FORMAT ('ERROR> ATINDX: ILLEGAL ATOM IN COMPONENT ', I3,' OF COORDINATE ', I3)
      end subroutine ATINDX
      subroutine FIND_ATOM_SYMBOL (Iatom,    & ! Atom name in list
                                   ATMNUM) ! Index
!***********************************************************************
!     Date last modified: May 10, 1995                     Version 1.0 *
!     Author: C. C. Pye                                                *
!     Description: Find atom symbol.                                   *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer ATMNUM
      character*(*) Iatom
!
! Local scalars:
      integer I
      logical FOUND
!
! Begin:
      call PRG_manager ('enter', 'FIND_ATOM_SYMBOL', 'UTILITY')
!
! Loop over all defined atoms.
      FOUND=.FALSE.
      I=1
      ATMNUM=0
      do while (.not.FOUND.and.I.LE.Natoms)
        IF(Iatom(1:4).EQ.CARTESIAN(I)%element(1:4)) then
          FOUND=.TRUE.
          ATMNUM=I
        else
          I=I+1
        end if
      end do
      IF(.not.FOUND) then
        write(UNIout,1000)
        stop
      end if
!
! End of routine FIND_ATOM_SYMBOL
      call PRG_manager ('exit', 'FIND_ATOM_SYMBOL', 'UTILITY')
      return
 1000 FORMAT ('ERROR> FIND_ATOM_SYMBOL: UNDEFINED ATOM')
      end subroutine FIND_ATOM_SYMBOL
      subroutine PIC_AUTO
!***********************************************************************
!     Date last modified: May 13th, 1996                   Version 1.0 *
!     Author: Cory C. Pye                                              *
!     Description: Generates automatic set of natural internal         *
!                  coordinates.                                        *
!***********************************************************************
! Modules:

      implicit none
!
! Local scalars:
      integer I, AT1,AT2,COMP1,COMP2, DIMB,DIMT,DIMAN,lennam,DIMB1,DIMB2
      character*32 Message, pad
      logical done,SET
!
! Work arrays:
      integer, dimension(:), allocatable :: IWORK1
      integer, dimension(:), allocatable :: IWORK2
      integer, dimension(:), allocatable :: IWORK3
      integer, dimension(:), allocatable :: IWORK4
      integer, dimension(:), allocatable :: IWORK5
      logical, dimension(:,:), allocatable :: WRKAUT
      logical, dimension(:), allocatable :: LSET
!
! Begin:
      call PRG_manager ('enter', 'PIC_AUTO', 'UTILITY')
!
      call GET_object ('MOL', 'GRAPH', 'BOND_ATOMS')
      call GET_object ('MOL', 'GRAPH', 'ANGLE_ATOMS')
      call GET_object ('MOL', 'GRAPH', 'TORSION_ATOMS')
      call GET_object ('MOL', 'GRAPH', 'EDGE_TO_RING')
      call GET_object ('MOL', 'GRAPH', 'EDGE_RING_TO_RING_ASSEMBLY')
      call GET_object ('MOL', 'GRAPH', 'CONVAL')
      call GET_object ('MOL', 'GRAPH', 'CNCOMP')
      call GET_object ('MOL', 'GRAPH', 'COMCON')
      call GET_object ('MOL', 'GRAPH', 'VERY_WEAK_BONDS')
      call GET_object ('MOL', 'GRAPH', 'VAN_DER_WAAL_BONDS')

      allocate(IWORK1(MAX_PIC), IWORK3(MAX_PIC), IWORK4(MAX_PIC), IWORK2(MAX_PIC), IWORK5(MAX_PIC), &
               WRKAUT(NComponents,NComponents), LSET(MAX_PIC))
!
      WRKAUT(1:NComponents,1:NComponents)=.false.
!
      pad(1:32)='                                '
      write (UNIout,'(a/a)')'Automatic coordinate generation not yet fully implemented. ','A partial set may be created.'
!
      done=.false.
!
! Menu:
      do while (.not.done)
      call new_token (' AUTOPIC:')
!
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      '   Command AUTOmatic: ', &
      '     Purpose: Automatic generation of Proper Internal Coordinates ', &
      '     Syntax: ', &
      '     AUTOmatic ', &
      '       REDundant=<logical> ', &
      '       EXTent=<char>: examples valence, MINimal', &
      '       HELP ', &
      '     end '

      else if(token(1:3).EQ.'EXT')then
        call GET_value (EXTENT, lennam)

      else if(token(1:3).EQ.'RED')then
        call GET_value (REDUND)

      else
        call MENU_end (done)
      end if
!
      end do ! .not.done
!
!
! Make 'normal' bonds.
      DIMB=max(1,NGBonds)
      Message=pad
      Message(1:4)='bond'
      call PIC_AUTO_BOND (BOND, NGBonds, DIMB, Message)
!
! Make the non-ring angles.
      DIMB=max(1,NGBonds)
      DIMAN=max(1,NGAngle)
      Message=pad
      Message(1:5)='angle'
      call PIC_AUTO_ANGLE (IWORK1, IWORK3, IWORK4, DIMB, DIMAN, Message)

!
! Make the non-ring torsions.
      DIMB=max(1,NGBonds)
      DIMT=max(1,NGTorsion)
      Message=pad
      Message(1:16)='non-ring torsion'
      call PIC_AUTO_TORS (DIMB, DIMT, Message)

!
! Make the ring angles/torsions.
      DIMB=max(1,NGBonds)
      DIMAN=max(1,NGAngle)
      Message=pad
      Message(1:)='ring'
      call PIC_AUTO_RINGASS (IWORK1, IWORK2, LSET, DIMB, DIMAN, Message)
!
! Make the stretches/angles/torsions about weak bonds.
      DIMB1=max(1,NGBonds)
      DIMB2=max(1,NVderWBond)
      Message=pad
      Message(1:)='weak bond def'
      call PIC_AUTO_WBOND (IWORK1, IWORK2, IWORK3, IWORK4, IWORK5, WRKAUT, LSET, DIMB2, DIMB1, Message)

!
! Make the stretches/angles/torsions about really weak bonds.
      DIMB1=max(1,NvWBond)
      Message=pad
      Message(1:)='v. weak bond'
      call PIC_AUTO_VWBOND (vWBond, IWORK1, IWORK2, IWORK3, NvWBond, DIMB1, Message)
!
      deallocate (IWORK1, IWORK2, IWORK3, IWORK4, IWORK5, WRKAUT, LSET)
!
! End of routine PIC_AUTO
      call PRG_manager ('exit', 'PIC_AUTO', 'UTILITY')
      return
      end subroutine PIC_AUTO
      subroutine PIC_AUTO_BOND (BONDSin, &
                                NBondsin,  & !
                                DIMB,   & !
                                Message & !
                                 )
!***********************************************************************
!     Date last modified: November 24, 1996                Version 1.0 *
!     Author: Cory C. Pye                                              *
!     Description: Copy bond list into PIC arrays.                     *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer DIMB,NBondsin
!
! Input arrays:
      type(bond_atoms) BONDSin(DIMB)
!
! Local scalars:
      integer I
      character*32 Message
      character*4 chartemp
!
! Begin:
      call PRG_manager ('enter', 'PIC_AUTO_BOND', 'UTILITY')
!
      do I=1,NBondsin
        NPICOR=NPICOR+1
        NPICMP=NPICMP+1
        PICw%type(NPICOR)=1
        PICw%Ncomponents(NPICOR)=1
        PICw%scale(NPICOR)=1.0D0
        PICw%coeff(NPICMP)=1.0D0
        PICw%atom1(NPICMP)=BONDSin(I)%ATOM1
        PICw%atom2(NPICMP)=BONDSin(I)%ATOM2
        PICw%atom3(NPICMP)=0
        PICw%atom4(NPICMP)=0
        call CNV_ItoS(NPICOR,chartemp) ! Convert integer to character
        PICw%label(NPICOR)='STRE_'//chartemp
        write(UNIout,2000)Message,PICw%atom1(NPICMP),CARTESIAN(PICw%atom1(NPICMP))%element, &
                                  PICw%atom2(NPICMP),CARTESIAN(PICw%atom2(NPICMP))%element
      end do
!
! End of routine PIC_AUTO_BOND
      call PRG_manager ('exit', 'PIC_AUTO_BOND', 'UTILITY')
      return
2000  FORMAT (1x,'Created coordinate:',1x,a23,1x,'about atoms ',i4,1x,'(',a,') and ',1x,i4,1x,'(',a,')')
      end subroutine PIC_AUTO_BOND
      subroutine PIC_AUTO_TORS (DIMB,   & !
                                DIMT,   & !
                                Message & !
                                 )
!***********************************************************************
!     Date last modified: November 24, 1996                Version 1.0 *
!     Author: Cory C. Pye                                              *
!     Description: Group torsions and copy into PIC arrays.            *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer DIMB,DIMT
!
! Local scalars:
      integer I, J, BAT2, BAT3
      character*32 Message
      character*4 chartemp
      logical FOUND
!
! Begin:
      call PRG_manager ('enter', 'PIC_AUTO_TORS', 'UTILITY')
!
      do I=1,NGBonds
!
! Include only if bond not part of a ring.
      IF(edges(I)%ring.EQ.0) then
!
        BAT2=BOND(I)%ATOM1
        BAT3=BOND(I)%ATOM2
        FOUND=.FALSE.
!
! Check torsion list
        do J=1, NGTorsion
!
! Found a match!
          IF(TORSION(J)%ATOM2.EQ.BAT2.and.TORSION(J)%ATOM3.EQ.BAT3)then
!
! If this is the first, increment PIC counter.
            IF(.not.FOUND) then
              FOUND=.TRUE.
              NPICOR=NPICOR+1
              PICw%Ncomponents(NPICOR)=0 ! First time, initialize to 0.
              PICw%type(NPICOR)=5
            end if ! .not.FOUND
!
! Increment component counter and assign torsion to component.
            NPICMP=NPICMP+1
            PICw%Ncomponents(NPICOR)=PICw%Ncomponents(NPICOR)+1
            PICw%coeff(NPICMP)=1.0D0
            PICw%atom1(NPICMP)=TORSION(J)%ATOM1
            PICw%atom2(NPICMP)=BAT2
            PICw%atom3(NPICMP)=BAT3
            PICw%atom4(NPICMP)=TORSION(J)%ATOM4
          end if ! TORSION(J)%ATOM2.EQ.BAT2.and.TORSION(J)%ATOM3.EQ.BAT3
        end do ! J
!
! Print message if torsion was found about this bond.
        IF(FOUND)then
          PICw%scale(NPICOR)=1.0D0/PICw%Ncomponents(NPICOR)
          call CNV_ItoS(NPICOR,chartemp) ! Convert integer to character
          PICw%label(NPICOR)='TORS_'//chartemp
          write(UNIout, 2000) Message, &
            BAT2, CARTESIAN(BAT2)%element, BAT3, CARTESIAN(BAT3)%element
        end if
!
      end if
      end do
!
! End of routine PIC_AUTO_TORS
      call PRG_manager ('exit', 'PIC_AUTO_TORS', 'UTILITY')
      return
2000  FORMAT (1x,'Created coordinate:',1x,a23,1x,'about atoms ',i4,1x,'(',a,') and ',1x,i4,1x,'(',a,')')
      end subroutine PIC_AUTO_TORS
      subroutine PIC_AUTO_ANGLE (ANGCMP, &
                                 RASS1, &
                                 RASS2, &
                                 DIMB,   & !
                                 DIMAN,  & !
                                 Message & !
                                  )
!***********************************************************************
!     Date last modified: November 24, 1996                Version 1.0 *
!     Author: Cory C. Pye                                              *
!     Description: Group non-ring angles and copy into PIC arrays.     *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer DIMB,DIMAN
!
! Input arrays:
      integer ANGCMP(MAX_PIC),RASS1(MAX_PIC),RASS2(MAX_PIC)
!
! Local scalars:
      integer :: I,J,K,ANGCT,BNUM1,BNUM2,AMINAT,AMAXAT,PICANC,AAT1,AAT2,AAT3,DEGREE,CURCOM,COUNT, &
              ANG1,ANG2,ANG3,ANG4,ODDMAN1,ODDMAN2,ODDMAN3,XCOM,SAMCNT,COUNT2
      double precision SSCALE
      character*32 Message,pad,BigMessage
      character*4 chartemp
      logical FOUND
!
! Begin:
      call PRG_manager ('enter', 'PIC_AUTO_ANGLE', 'UTILITY')
!
      pad='                                '
!
      BNUM1=0
      BNUM2=0
      do I=1,Natoms
        ANGCT=0
        FOUND=.FALSE.
        do J=1,NGAngle
          IF(ANGLE(J)%ATOM2.EQ.I) then
!
! Found a match! Now what do we do with it!
            FOUND=.TRUE.
            ANGCT=ANGCT+1
            AAT1=ANGLE(J)%ATOM1
            AAT2=ANGLE(J)%ATOM2
            AAT3=ANGLE(J)%ATOM3
!
! Find the bonds matching these.
            AMINAT=MIN(AAT1,AAT2)
            AMAXAT=MAX(AAT1,AAT2)
            do K=1,NGBonds
              IF(AMINAT.EQ.BOND(K)%atom1.and.AMAXAT.EQ.BOND(K)%atom2) then
                BNUM1=K
              end if ! AMINAT
            end do ! K
!
            AMINAT=MIN(AAT2,AAT3)
            AMAXAT=MAX(AAT2,AAT3)
            do K=1,NGBonds
              IF(AMINAT.EQ.BOND(K)%atom1.and.AMAXAT.EQ.BOND(K)%atom2) then
                BNUM2=K
              end if ! AMINAT
            end do ! K
!
! Find the ring assembly to which the bonds belong.
            ANGCMP(ANGCT)=J
            RASS1(ANGCT)=edges(BNUM1)%ring
            RASS2(ANGCT)=edges(BNUM2)%ring
!
          end if ! ANGLE(J)%ATOM2.EQ.I
        end do ! J=1,NGAngle
!
! Found all angles with I as central angle.
        IF(FOUND)then
          DEGREE=INT(DSQRT(DBLE(2*ANGCT)))+1
!
          IF(2*ANGCT.NE.DEGREE*(DEGREE-1)) then
            write(UNIout,'(a)')'ERROR> PIC_AUTO_ANGLE: Programming Error: Impossible angle count'
            stop 'ERROR> PIC_AUTO_ANGLE: Programming Error: Impossible angle count'
          end if
!
          write(UNIout,1000) 'Atom '//CARTESIAN(I)%element//' has degree',DEGREE,' and ',ANGCT,' valence angles.'
          IF(DEGREE.EQ.2) then
!
! Degree 2:
! Cases: 1) Angle in ring: Do not build.
!        2) Angle not in ring: Build.
!
            IF(RASS1(ANGCT).EQ.0.and.RASS2(ANGCT).EQ.0) then
              NPICOR=NPICOR+1
              PICw%type(NPICOR)=3
              PICw%Ncomponents(NPICOR)=1
              PICw%scale(NPICOR)=1.0D0
!
              call PIC_AUTO_ADDCOMP (ANGCMP, DIMAN, ANGCT, 1.0D0)
!
              call CNV_ItoS(NPICOR,chartemp) ! Convert integer to character
              PICw%label(NPICOR)='BEND_'//chartemp
              BigMessage=pad
              BigMessage(1:)=Message(1:6)
              write(UNIout, 2000) BigMessage, I, CARTESIAN(I)%element
            else
              BigMessage=pad
              BigMessage(1:)=Message(1:6)//' - all'
              write(UNIout, 2010)BigMessage, I, CARTESIAN(I)%element
            end if
          else IF(DEGREE.EQ.3) then
!
! Degree 3:
! Cases: 1) All angles (3 bonds) in ring assembly: Do not build.
!        2) One angle in ring ass,(1 bond not in ring): Rocking, out-of-plane.
!        3) No angles in ring ass: Build rock, scissor and out-of-plane.
!
!
! Attempt to pick most symmetrical way to do this.
            COUNT=0
            do J=1,ANGCT
              IF (RASS1(J).NE.0)COUNT=COUNT+1
              IF (RASS2(J).NE.0)COUNT=COUNT+1
            end do ! J
            COUNT=COUNT/2
            IF(COUNT.EQ.3) then
!
! Case 1) All angles (3 bonds) in ring assembly: Do not build.
              BigMessage=pad
              BigMessage(1:)=Message(1:6)//'- all'
              write(UNIout, 2010)BigMessage, I, CARTESIAN(I)%element
            else IF(COUNT.EQ.2) then
!
! Case 2) One angle in ring ass, (1 bond not in ring): Rocking, out-of-plane.
              do J=1,ANGCT
                IF(RASS1(J).NE.0.and.RASS2(J).NE.0)then
                  ODDMAN1=J
                end if
              end do ! J
              ANG1=0
              ANG2=0
              do J=1,ANGCT
                IF(J.NE.ODDMAN1) then
                  IF(ANG1.EQ.0) then
                    ANG1=J
                  else IF(ANG2.EQ.0) then
                    ANG2=J
                  else
                    write(UNIout,1010)
                    STOP
                  end if
                end if
              end do
!
! Rocking motion.
              NPICOR=NPICOR+1
              PICw%type(NPICOR)=3
              PICw%Ncomponents(NPICOR)=2
              PICANC=NPICMP
!
              call PIC_AUTO_ADDCOMP (ANGCMP, DIMAN, ANG1, 1.0D0)
!
              call PIC_AUTO_ADDCOMP (ANGCMP, DIMAN, ANG2, -1.0D0)
!
              SSCALE=0.0D0
              do J=1,PICw%Ncomponents(NPICOR)
                SSCALE=SSCALE+PICw%COEFF(PICANC+J)**2
              end do
              PICw%scale(NPICOR)=1.0D0/DSQRT(SSCALE)
!
              call CNV_ItoS(NPICOR,chartemp) ! Convert integer to character
              PICw%label(NPICOR)='BEND_'//chartemp
              BigMessage=pad
              BigMessage(1:)=Message(1:6)//'- rock'
              write(UNIout, 2000)BigMessage, I, CARTESIAN(I)%element
!
! Out-of-plane bend.
              NPICOR=NPICOR+1
              PICw%type(NPICOR)=4
              PICw%Ncomponents(NPICOR)=1

              NPICMP=NPICMP+1
              PICw%coeff(NPICMP)=1.0D0
              CURCOM=ANGCMP(ODDMAN1)
              XCOM=ANGCMP(ANG1)
              IF(ANGLE(XCOM)%atom1.NE.ANGLE(CURCOM)%atom1.and.ANGLE(XCOM)%atom1.NE.ANGLE(CURCOM)%atom1.and. &
                 ANGLE(XCOM)%atom1.NE.ANGLE(CURCOM)%atom1)then
                PICw%atom1(NPICMP)=ANGLE(XCOM)%atom1
              else
                PICw%atom1(NPICMP)=ANGLE(XCOM)%atom3
              end if
              PICw%atom2(NPICMP)=ANGLE(CURCOM)%atom1
              PICw%atom3(NPICMP)=ANGLE(CURCOM)%atom2
              PICw%atom4(NPICMP)=ANGLE(CURCOM)%atom3
              PICw%scale(NPICOR)=1.0D0
              call CNV_ItoS(NPICOR,chartemp) ! Convert integer to character
              PICw%label(NPICOR)='OOP_'//chartemp
              BigMessage=pad
              BigMessage(1:)=Message(1:6)//'- out-of-plane'
              write(UNIout, 2000)BigMessage , I, CARTESIAN(I)%element
!
              BigMessage=pad
              BigMessage(1:)=Message(1:6)//'- scissor'
              write(UNIout, 2010)BigMessage, I, CARTESIAN(I)%element

            else IF(COUNT.EQ.1) then
              write(UNIout, 1020)
                STOP
            else
!
! Case 3) No angles in ring ass: Build rock, scissor and out-of-plane.
              COUNT2=0
              do J=1,ANGCT
                AAT1=ANGLE(ANGCMP(J))%atom1
                AAT3=ANGLE(ANGCMP(J))%atom3
                IF(CONVAL(AAT1).NE.1) COUNT2=COUNT2+1
                IF(CONVAL(AAT3).NE.1) COUNT2=COUNT2+1
              end do ! J
              COUNT2=COUNT2/2
              IF(COUNT2.EQ.2) then
                do J=1,ANGCT
                  AAT1=ANGLE(ANGCMP(J))%atom1
                  AAT3=ANGLE(ANGCMP(J))%atom3
                  IF(CONVAL(AAT1).NE.1.and.CONVAL(AAT3).NE.1)ODDMAN1=J
                end do ! J
              else IF(COUNT2.EQ.1) then
                do J=1,ANGCT
                  AAT1=ANGLE(ANGCMP(J))%atom1
                  AAT3=ANGLE(ANGCMP(J))%atom3
                  IF(CONVAL(AAT1).EQ.1.and.CONVAL(AAT3).EQ.1)ODDMAN1=J
                end do ! J
              else
                ODDMAN1=1
              end if
!
! Sort out symmetry.
                ANG1=0
                ANG2=0
                do J=1,ANGCT
                  IF(J.NE.ODDMAN1) then
                    IF(ANG1.EQ.0) then
                      ANG1=J
                    else IF(ANG2.EQ.0) then
                      ANG2=J
                    else
                      write(UNIout,1010)
                      STOP
                    end if
                  end if
                end do
!
              BigMessage=pad
              BigMessage(1:)=Message(1:6)//'3-valent'
      call PIC_AUTO_ADDCOORD_13 ( ANGCMP, I, DIMAN, ODDMAN1, ANG1, ANG2, BigMessage)
!
! Out-of-plane bend.
              NPICOR=NPICOR+1
              PICw%type(NPICOR)=4
              PICw%Ncomponents(NPICOR)=1

              NPICMP=NPICMP+1
              PICw%coeff(NPICMP)=1.0D0
              CURCOM=ANGCMP(ODDMAN1)
              XCOM=ANGCMP(ANG1)
              IF(ANGLE(XCOM)%atom1.NE.ANGLE(CURCOM)%atom1.and.ANGLE(XCOM)%atom1.NE.ANGLE(CURCOM)%atom1.and. &
                ANGLE(XCOM)%atom1.NE.ANGLE(CURCOM)%atom1)then
                PICw%atom1(NPICMP)=ANGLE(XCOM)%atom1
              else
                PICw%atom1(NPICMP)=ANGLE(XCOM)%atom3
              end if
              PICw%atom2(NPICMP)=ANGLE(CURCOM)%atom1
              PICw%atom3(NPICMP)=ANGLE(CURCOM)%atom2
              PICw%atom4(NPICMP)=ANGLE(CURCOM)%atom3
              PICw%scale(NPICOR)=1.0D0

              call CNV_ItoS(NPICOR,chartemp) ! Convert integer to character
              PICw%label(NPICOR)='OOP_'//chartemp
              BigMessage=pad
              BigMessage(1:)=Message(1:6)//'- out-of-plane'
              write(UNIout, 2000)BigMessage, I, CARTESIAN(I)%element

            end if ! COUNT
          else IF(DEGREE.EQ.4) then
!
! Degree 4:
! Cases: 1) All angles (4 bonds) in 1 ring assembly: Do not build.
!        2) Opposite angles in two different ring assemblies (Spiro system).
!        3) 3 Angles (3 bonds) in one ring assembly .
!        4) 1 Angle (2 bonds) in one ring assembly.
!        5) No angles in ring assemblies.
!
! Attempt to pick most symmetrical way to do this.
            COUNT=0
            do J=1,ANGCT
              IF (RASS1(J).NE.0)COUNT=COUNT+1
              IF (RASS2(J).NE.0)COUNT=COUNT+1
            end do ! J
            COUNT=COUNT/3

            IF(COUNT.EQ.4) then
!
! Either all bonds in same ring assembly or two bonds in two different rings.
              SAMCNT=0
              do J=1,ANGCT
                IF(RASS1(J).EQ.RASS2(J))SAMCNT=SAMCNT+1
              end do
              IF(SAMCNT.EQ.6) then
!
! Case 1) All angles (4 bonds) in 1 ring assembly: Do not build.
              BigMessage=pad
              BigMessage(1:)=Message(1:6)//'- all'
                write(UNIout, 2010)BigMessage(1:6), I, CARTESIAN(I)%element
              else IF(SAMCNT.EQ.2) then
!
! Case 2) Opposite angles in two different ring assemblies (Spiro system).
                ODDMAN1=0
                ODDMAN2=0
                do J=1,ANGCT
                  IF(RASS1(J).EQ.RASS2(J)) then
                    IF(ODDMAN1.EQ.0)then
                      ODDMAN1=J
                    else
                      ODDMAN2=J
                    end if
                  end if
                end do
!
! Sort out symmetry.
                ANG1=0
                ANG2=0
                ANG3=0
                ANG4=0
                do J=1,ANGCT
                  IF(J.NE.ODDMAN1.and.J.NE.ODDMAN2) then
                    IF(ANG1.EQ.0) then
                      ANG1=J
                    else IF(ANG2.EQ.0) then
                      ANG2=J
                    else IF(ANG3.EQ.0) then
                      ANG3=J
                    else IF(ANG4.EQ.0) then
                      ANG4=J
                    else
                      write(UNIout,1010)
                    end if
                  end if
                end do
!
! Spiro ring motion.
              BigMessage=pad
              BigMessage(1:)=Message(1:6)//'- spiro'
      call PIC_AUTO_ADDCOORD_22 (ANGCMP, I, DIMAN, ANG1, ANG2, ANG3, ANG4, BigMessage)
!
              else
                write(UNIout, 1010)
                STOP
              end if ! SAMCMP
            else IF(COUNT.EQ.3) then
!
! Case 3) 3 Angles (3 bonds) in one ring assembly (Bridgehead)
!
! Which angles are part of the assembly?
                ODDMAN1=0
                ODDMAN2=0
                ODDMAN3=0
                do J=1,ANGCT
                  IF(RASS1(J).EQ.RASS2(J)) then
                    IF(ODDMAN1.EQ.0)then
                      ODDMAN1=J
                    else IF(ODDMAN2.EQ.0)then
                      ODDMAN2=J
                    else
                      ODDMAN3=J
                    end if
                  end if
                end do
! Sort out symmetry.
                ANG1=0
                ANG2=0
                ANG3=0
                do J=1,ANGCT
                  IF(J.NE.ODDMAN1.and.J.NE.ODDMAN2.and.J.NE.ODDMAN3) then
                    IF(ANG1.EQ.0) then
                      ANG1=J
                    else IF(ANG2.EQ.0) then
                      ANG2=J
                    else IF(ANG3.EQ.0) then
                      ANG3=J
                    else
                      write(UNIout,1010)
                    end if
                  end if
                end do
!
! Bridgehead substituent degenerate bends.
              BigMessage=pad
              BigMessage(1:)=Message(1:6)//'- bridgehead'
      call PIC_AUTO_ADDCOORD_13 (ANGCMP, I, DIMAN, ANG1, ANG2, ANG3, BigMessage)

            else IF(COUNT.EQ.2) then
!
! Case 4) 1 Angle (2 bonds) in one ring assembly.
!
! Which angles are part of the assembly? And opposite?
                ODDMAN1=0
                ODDMAN2=0
                do J=1,ANGCT
                  IF (RASS1(J).NE.0.and.RASS2(J).NE.0) ODDMAN1=J ! In
                  IF (RASS1(J).EQ.0.and.RASS2(J).EQ.0) ODDMAN2=J ! Opposite
                end do
!
! Sort out symmetry.
                ANG1=0
                ANG2=0
                ANG3=0
                ANG4=0
                do J=1,ANGCT
                  IF(J.NE.ODDMAN1.and.J.NE.ODDMAN2) then
                    IF(ANG1.EQ.0) then
                      ANG1=J
                    else IF(ANG2.EQ.0) then
                      ANG2=J
                    else IF(ANG3.EQ.0) then
                      ANG3=J
                    else IF(ANG4.EQ.0) then
                      ANG4=J
                    else
                      write(UNIout,1010)
                    end if
                  end if
                end do
!
! Methylene scissor.
                NPICOR=NPICOR+1
                PICw%type(NPICOR)=3
                PICw%Ncomponents(NPICOR)=5
                PICANC=NPICMP
!
                call PIC_AUTO_ADDCOMP (ANGCMP, DIMAN, ODDMAN2, 4.0D0)
!
                call PIC_AUTO_ADDCOMP (ANGCMP, DIMAN, ANG1, -1.0D0)
!
                call PIC_AUTO_ADDCOMP (ANGCMP, DIMAN, ANG2, -1.0D0)
!
                call PIC_AUTO_ADDCOMP (ANGCMP, DIMAN, ANG3, -1.0D0)
!
                call PIC_AUTO_ADDCOMP (ANGCMP, DIMAN, ANG4, -1.0D0)
!
                SSCALE=0.0D0
                do J=1,PICw%Ncomponents(NPICOR)
                  SSCALE=SSCALE+PICw%coeff(PICANC+J)**2
                end do
                PICw%scale(NPICOR)=1.0D0/DSQRT(SSCALE)
!
                call CNV_ItoS(NPICOR,chartemp) ! Convert integer to character
                PICw%label(NPICOR)='BEND_'//chartemp
                BigMessage=pad
                BigMessage(1:)=Message(1:6)//'- methylene sciss'
                write(UNIout, 2000)BigMessage, I, CARTESIAN(I)%element
!
! Methylene rock/wag/twist.
              BigMessage=pad
              BigMessage(1:)=Message(1:6)//'- ring methylene'
              call PIC_AUTO_ADDCOORD_22 (ANGCMP, I, DIMAN, ANG1, ANG2, ANG3, ANG4, BigMessage)
!
                BigMessage=pad
                BigMessage(1:)=Message(1:6)//'- ring methylene'
                write(UNIout, 2010) &
                  BigMessage, I, CARTESIAN(I)%element
            else IF(COUNT.EQ.1) then
              write(UNIout, 1020)
              STOP
            else
!
! Case 5) No angles in ring assemblies.
!      5a) Exactly 2 terminal atoms.
!      5b) Either 1 or 3 terminal atoms.
!      5c) All or no terminal atoms.
              COUNT2=0
              do J=1,ANGCT
                AAT1=ANGLE(ANGCMP(J))%atom1
                AAT3=ANGLE(ANGCMP(J))%atom3
                IF(CONVAL(AAT1).NE.1) COUNT2=COUNT2+1
                IF(CONVAL(AAT3).NE.1) COUNT2=COUNT2+1
              end do ! J
              COUNT2=COUNT2/3
              IF(COUNT2.EQ.2) then
!
! Case 5a) Exactly 2 terminal atoms.
                do J=1,ANGCT
                  AAT1=ANGLE(ANGCMP(J))%atom1
                  AAT3=ANGLE(ANGCMP(J))%atom3
                  IF(CONVAL(AAT1).NE.1.and.CONVAL(AAT3).NE.1)ODDMAN1=J
                  IF(CONVAL(AAT1).EQ.1.and.CONVAL(AAT3).EQ.1)ODDMAN2=J
                end do ! J
!
! Sort out symmetry.
                ANG1=0
                ANG2=0
                ANG3=0
                ANG4=0
                do J=1,ANGCT
                  IF(J.NE.ODDMAN1.and.J.NE.ODDMAN2) then
                    IF(ANG1.EQ.0) then
                      ANG1=J
                    else IF(ANG2.EQ.0) then
                      ANG2=J
                    else IF(ANG3.EQ.0) then
                      ANG3=J
                    else IF(ANG4.EQ.0) then
                      ANG4=J
                    else
                      write(UNIout,1010)
                    end if
                  end if
                end do
!
! Methylene scissor # 1.
                NPICOR=NPICOR+1
                PICw%type(NPICOR)=3
                PICw%Ncomponents(NPICOR)=2
                PICANC=NPICMP
!
                call PIC_AUTO_ADDCOMP (ANGCMP, DIMAN, ODDMAN1, 5.0D0)
!
                call PIC_AUTO_ADDCOMP (ANGCMP, DIMAN, ODDMAN2, 1.0D0)
!
                SSCALE=0.0D0
                do J=1,PICw%Ncomponents(NPICOR)
                  SSCALE=SSCALE+PICw%coeff(PICANC+J)**2
                end do
                PICw%scale(NPICOR)=1.0D0/DSQRT(SSCALE)
!
                call CNV_ItoS(NPICOR,chartemp) ! Convert integer to character
                PICw%label(NPICOR)='BEND_'//chartemp
                BigMessage=pad
                BigMessage(1:)=Message(1:6)//'- methylene sciss'
                write(UNIout, 2000)BigMessage, I, CARTESIAN(I)%element
!
! Methylene scissor # 2.
                NPICOR=NPICOR+1
                PICw%type(NPICOR)=3
                PICw%Ncomponents(NPICOR)=2
                PICANC=NPICMP
!
                call PIC_AUTO_ADDCOMP (ANGCMP, DIMAN, ODDMAN1, 1.0D0)
!
                call PIC_AUTO_ADDCOMP (ANGCMP, DIMAN, ODDMAN2, 5.0D0)
!
                SSCALE=0.0D0
                do J=1,PICw%Ncomponents(NPICOR)
                  SSCALE=SSCALE+PICw%coeff(PICANC+J)**2
                end do
                PICw%scale(NPICOR)=1.0D0/DSQRT(SSCALE)
!
                call CNV_ItoS(NPICOR,chartemp) ! Convert integer to character
                PICw%label(NPICOR)='BEND_'//chartemp
                BigMessage=pad
                BigMessage(1:)=Message(1:6)//'- methylene sciss'
                write(UNIout, 2000)BigMessage, I, CARTESIAN(I)%element
!
! Methylene rock/wag/twist # 1.
              BigMessage=pad
              BigMessage(1:)=Message(1:6)//'- methylene'
              call PIC_AUTO_ADDCOORD_22 (ANGCMP, I, DIMAN, ANG1, ANG2, ANG3, ANG4, BigMessage)

              else IF(COUNT2.EQ.3.or.COUNT2.EQ.1) then
! 3 terminal atoms or 1 terminal atom.
                ODDMAN1=0
                ODDMAN2=0
                ODDMAN3=0
                do J=1,ANGCT
                  AAT1=ANGLE(ANGCMP(J))%atom1
                  AAT3=ANGLE(ANGCMP(J))%atom3
                  IF((COUNT2.EQ.3.and.CONVAL(AAT1).NE.1.and.CONVAL(AAT3).NE.1).or. &
                     (COUNT2.EQ.1.and.CONVAL(AAT1).EQ.1.and.CONVAL(AAT3).EQ.1))then
                    IF(ODDMAN1.EQ.0)then
                      ODDMAN1=J
                    else IF(ODDMAN2.EQ.0)then
                      ODDMAN2=J
                    else
                      ODDMAN3=J
                    end if
                  end if
                end do
! Sort out symmetry.
                ANG1=0
                ANG2=0
                ANG3=0
                do J=1,ANGCT
                  IF(J.NE.ODDMAN1.and.J.NE.ODDMAN2.and.J.NE.ODDMAN3) then
                    IF(ANG1.EQ.0) then
                      ANG1=J
                    else IF(ANG2.EQ.0) then
                      ANG2=J
                    else IF(ANG3.EQ.0) then
                      ANG3=J
                    else
                      write(UNIout,1010)
                    end if
                  end if
                end do
!
! Methyl symmetric deformation.
                NPICOR=NPICOR+1
                PICw%type(NPICOR)=3
                PICw%Ncomponents(NPICOR)=6
                PICANC=NPICMP
!
                call PIC_AUTO_ADDCOMP (ANGCMP, DIMAN, ODDMAN1, 1.0D0)
!
                call PIC_AUTO_ADDCOMP (ANGCMP, DIMAN, ODDMAN2, 1.0D0)
!
                call PIC_AUTO_ADDCOMP (ANGCMP, DIMAN, ODDMAN3, 1.0D0)
!
                call PIC_AUTO_ADDCOMP (ANGCMP, DIMAN, ANG1, -1.0D0)
!
                call PIC_AUTO_ADDCOMP (ANGCMP, DIMAN, ANG2, -1.0D0)
!
                call PIC_AUTO_ADDCOMP (ANGCMP, DIMAN, ANG3, -1.0D0)
!
                SSCALE=0.0D0
                do J=1,PICw%Ncomponents(NPICOR)
                  SSCALE=SSCALE+PICw%coeff(PICANC+J)**2
                end do
                PICw%scale(NPICOR)=1.0D0/DSQRT(SSCALE)
!
                call CNV_ItoS(NPICOR,chartemp) ! Convert integer to character
                PICw%label(NPICOR)='BEND_'//chartemp
                BigMessage=pad
                BigMessage(1:)=Message(1:6)//'- methyl sym def'
                write(UNIout, 2000)BigMessage, I, CARTESIAN(I)%element

!
! Methyl substituent degenerate deformation.
              BigMessage=pad
              BigMessage(1:)=Message(1:6)//'- methyl asym def'
              call PIC_AUTO_ADDCOORD_13 (ANGCMP, I, DIMAN, ODDMAN1, ODDMAN2, ODDMAN3, BigMessage)
!
! Methyl substituent degenerate rock.
              BigMessage=pad
              BigMessage(1:)=Message(1:6)//'- methyl rock'
              call PIC_AUTO_ADDCOORD_13 (ANGCMP, I, DIMAN, ANG1, ANG2, ANG3, BigMessage)
!
! All or none are terminal atoms.
             else IF(COUNT2.EQ.4.or.COUNT2.EQ.0) then
                ODDMAN1=1
                ODDMAN2=2
                ODDMAN3=3
                ANG1=4
                ANG2=5
                ANG3=6
!
! Methyl symmetric deformation.
                NPICOR=NPICOR+1
                PICw%type(NPICOR)=3
                PICw%Ncomponents(NPICOR)=6
                PICANC=NPICMP
!
                call PIC_AUTO_ADDCOMP (ANGCMP, DIMAN, ODDMAN1, 1.0D0)
!
                call PIC_AUTO_ADDCOMP (ANGCMP, DIMAN, ODDMAN2, 1.0D0)
!
                call PIC_AUTO_ADDCOMP (ANGCMP, DIMAN, ODDMAN3, 1.0D0)
!
                call PIC_AUTO_ADDCOMP (ANGCMP, DIMAN, ANG1, -1.0D0)
!
                call PIC_AUTO_ADDCOMP (ANGCMP, DIMAN, ANG2, -1.0D0)
!
                call PIC_AUTO_ADDCOMP (ANGCMP, DIMAN, ANG3, -1.0D0)
!
                SSCALE=0.0D0
                do J=1,PICw%Ncomponents(NPICOR)
                  SSCALE=SSCALE+PICw%coeff(PICANC+J)**2
                end do
                PICw%scale(NPICOR)=1.0D0/DSQRT(SSCALE)
!
                call CNV_ItoS(NPICOR,chartemp) ! Convert integer to character
                PICw%label(NPICOR)='BEND_'//chartemp
                BigMessage=pad
                BigMessage(1:)=Message(1:6)//'- methyl sym def'
                write(UNIout, 2000)BigMessage, I, CARTESIAN(I)%element
!
! Methyl substituent degenerate def # 1.
              BigMessage=pad
              BigMessage(1:)=Message(1:6)//'- methyl asym def'
              call PIC_AUTO_ADDCOORD_13 (ANGCMP, I, DIMAN, ODDMAN1, ODDMAN2, ODDMAN3, BigMessage)
!
! Methyl substituent degenerate rock # 1.
              BigMessage=pad
              BigMessage(1:)=Message(1:6)//'- methyl rock'
              call PIC_AUTO_ADDCOORD_13 (ANGCMP, I, DIMAN, ANG1, ANG2, ANG3, BigMessage)

              else
                write(UNIout,1010)
              end if
            end if ! COUNT
          else ! DEGREE
            write(UNIout,*)'Degree 5 and above not yet supported.'
          end if ! DEGREE
        end if! FOUND
      end do ! I=1
!
! End of routine PIC_AUTO_ANGLE
      call PRG_manager ('exit', 'PIC_AUTO_ANGLE', 'UTILITY')
      return
1000  FORMAT(1x,a,2(I4,a))
1010  FORMAT('ERROR> PIC_AUTO_ANGLE: Programming error. Cannot symmetrize properly')
1020  FORMAT('ERROR> PIC_AUTO_ANGLE: Programming error. '//'Ring assembly assignments incorrect')
2000  FORMAT(1x,'Created coordinate:',1x,a23,1x,'about atom #',i4,1x,'(',a,')')
2010  FORMAT(1x,'Skipped coordinate:',1x,a23,1x,'about atom #',i4,1x,'(',a,')')
      end subroutine PIC_AUTO_ANGLE
      subroutine PIC_AUTO_ADDCOMP (ANGCMP, & !
                                   DIMAN,  & !
                                   ANGNUM, & !
                                   COEFF   & !
                                    )
!***********************************************************************
!     Date last modified: December 2, 1996                 Version 1.0 *
!     Author: Cory C. Pye                                              *
!     Description: Add component to PIC.                               *
!***********************************************************************
! Modules:

      implicit none
!
! Includes:
!
! Input scalars:
      integer CURCOM,ANGNUM,DIMAN
      double precision COEFF
!
! Input arrays:
      integer ANGCMP(MAX_PIC)
!
! Begin:
      call PRG_manager ('enter', 'PIC_AUTO_ADDCOMP',  'UTILITY')
!
      NPICMP=NPICMP+1
      PICw%coeff(NPICMP)=COEFF
      CURCOM=ANGCMP(ANGNUM)
      PICw%atom1(NPICMP)=ANGLE(CURCOM)%atom1
      PICw%atom2(NPICMP)=ANGLE(CURCOM)%atom2
      PICw%atom3(NPICMP)=ANGLE(CURCOM)%atom3
      PICw%atom4(NPICMP)=0
!
! End of routine PIC_AUTO_ADDCOMP
      call PRG_manager ('exit', 'PIC_AUTO_ADDCOMP', 'UTILITY')
      return
      end subroutine PIC_AUTO_ADDCOMP
      subroutine PIC_AUTO_ADDCOORD_13 ( ANGCMP, & !
                                       I,      & !
                                       DIMAN,  & !
                                       ANG1,   & !
                                       ANG2,   & !
                                       ANG3, &
                                       Message  & !
                                        )
!***********************************************************************
!     Date last modified: December 3, 1996                 Version 1.0 *
!     Author: Cory C. Pye                                              *
!     Description: Add PIC's for a 4-valent, 1-3 partitioned system.   *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer ANG1,ANG2,ANG3,DIMAN,I
!
! Input arrays:
      integer ANGCMP(MAX_PIC)
!
! Local scalars:
      integer J, PICANC
      double precision SSCALE
      character*32 Message
      character*4 chartemp
!
! Begin:
      call PRG_manager ('enter', 'PIC_AUTO_ADDCOORD_13', 'UTILITY')
!
! Methyl substituent degenerate rock # 1.
      NPICOR=NPICOR+1
      PICw%type(NPICOR)=3
      PICw%Ncomponents(NPICOR)=3
      PICANC=NPICMP
!
      call PIC_AUTO_ADDCOMP (ANGCMP, DIMAN, ANG1, 2.0D0)
!
      call PIC_AUTO_ADDCOMP (ANGCMP, DIMAN, ANG2, -1.0D0)
!
      call PIC_AUTO_ADDCOMP (ANGCMP, DIMAN, ANG3, -1.0D0)
!
      SSCALE=0.0D0
      do J=1,PICw%Ncomponents(NPICOR)
        SSCALE=SSCALE+PICw%coeff(PICANC+J)**2
      end do
      PICw%scale(NPICOR)=1.0D0/DSQRT(SSCALE)
!
      call CNV_ItoS(NPICOR,chartemp) ! Convert integer to character
      PICw%label(NPICOR)='BEND_'//chartemp
      write(UNIout, 2000) Message, I, CARTESIAN(I)%element

!
! Methyl substituent degenerate rock # 2.
      NPICOR=NPICOR+1
      PICw%type(NPICOR)=3
      PICw%Ncomponents(NPICOR)=2
      PICANC=NPICMP
!
      call PIC_AUTO_ADDCOMP (ANGCMP, DIMAN, ANG2, 1.0D0)
!
      call PIC_AUTO_ADDCOMP (ANGCMP, DIMAN, ANG3, -1.0D0)
!
      SSCALE=0.0D0
      do J=1,PICw%Ncomponents(NPICOR)
        SSCALE=SSCALE+PICw%coeff(PICANC+J)**2
      end do
      PICw%scale(NPICOR)=1.0D0/DSQRT(SSCALE)
!
      call CNV_ItoS(NPICOR,chartemp) ! Convert integer to character
      PICw%label(NPICOR)='BEND_'//chartemp
      write(UNIout, 2000)Message, I, CARTESIAN(I)%element

!
! End of routine PIC_AUTO_ADDCOORD_13
      call PRG_manager ('exit', 'PIC_AUTO_ADDCOORD_13', 'UTILITY')
      return
2000  FORMAT(1x,'Created coordinate:',1x,a23,1x,'about atom #',i4,1x,'(',a,')')
      end subroutine PIC_AUTO_ADDCOORD_13
      subroutine PIC_AUTO_ADDCOORD_22 ( &
                                       ANGCMP, & !
                                       I,      & !
                                       DIMAN,  & !
                                       ANG1,   & !
                                       ANG2,   & !
                                       ANG3,   & !
                                       ANG4,   & !
                                       Message  & !
                                        )
!***********************************************************************
!     Date last modified: December 3, 1996                 Version 1.0 *
!     Author: Cory C. Pye                                              *
!     Description: Add rock/wag/twist PIC's for a 4-valent, 2-2        *
!                  partitioned (C2v) system.                           *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer ANG1,ANG2,ANG3,ANG4,DIMAN,I
!
! Input arrays:
      integer ANGCMP(MAX_PIC)
!
! Local scalars:
      integer J, PICANC
      double precision SSCALE
      character*32 Message
      character*4 chartemp
!
! Begin:
      call PRG_manager ('enter', 'PIC_AUTO_ADDCOORD_22', 'UTILITY')
!
! Methylene rock/wag/twist # 1.
      NPICOR=NPICOR+1
      PICw%type(NPICOR)=3
      PICw%Ncomponents(NPICOR)=4
      PICANC=NPICMP
!
      call PIC_AUTO_ADDCOMP (ANGCMP, DIMAN, ANG1, 1.0D0)
!
      call PIC_AUTO_ADDCOMP (ANGCMP, DIMAN, ANG2, 1.0D0)
!
      call PIC_AUTO_ADDCOMP (ANGCMP, DIMAN, ANG3, -1.0D0)
!
      call PIC_AUTO_ADDCOMP (ANGCMP, DIMAN, ANG4, -1.0D0)
!
      SSCALE=0.0D0
      do J=1,PICw%Ncomponents(NPICOR)
        SSCALE=SSCALE+PICw%coeff(PICANC+J)**2
      end do
      PICw%scale(NPICOR)=1.0D0/DSQRT(SSCALE)
!
      call CNV_ItoS(NPICOR,chartemp) ! Convert integer to character
      PICw%label(NPICOR)='BEND_'//chartemp
      write(UNIout, 2000)Message, I, CARTESIAN(I)%element
!
! Methylene rock/wag/twist # 2.
       NPICOR=NPICOR+1
       PICw%type(NPICOR)=3
       PICw%Ncomponents(NPICOR)=4
       PICANC=NPICMP
!
       call PIC_AUTO_ADDCOMP (ANGCMP, DIMAN, ANG1, 1.0D0)
!
       call PIC_AUTO_ADDCOMP (ANGCMP, DIMAN, ANG2, -1.0D0)
!
       call PIC_AUTO_ADDCOMP (ANGCMP, DIMAN, ANG3, 1.0D0)
!
       call PIC_AUTO_ADDCOMP (ANGCMP, DIMAN, ANG4, -1.0D0)
!
      SSCALE=0.0D0
      do J=1,PICw%Ncomponents(NPICOR)
        SSCALE=SSCALE+PICw%coeff(PICANC+J)**2
      end do
      PICw%scale(NPICOR)=1.0D0/DSQRT(SSCALE)
!
      call CNV_ItoS(NPICOR,chartemp) ! Convert integer to character
      PICw%label(NPICOR)='BEND_'//chartemp
      write(UNIout, 2000)Message, I, CARTESIAN(I)%element
!
! Methylene rock/wag/twist # 1.
       NPICOR=NPICOR+1
       PICw%type(NPICOR)=3
       PICw%Ncomponents(NPICOR)=4
       PICANC=NPICMP
!
       call PIC_AUTO_ADDCOMP (ANGCMP, DIMAN, ANG1, 1.0D0)
!
      call PIC_AUTO_ADDCOMP (ANGCMP, DIMAN, ANG2, -1.0D0)
!
      call PIC_AUTO_ADDCOMP (ANGCMP, DIMAN, ANG3, -1.0D0)
!
      call PIC_AUTO_ADDCOMP (ANGCMP, DIMAN, ANG4, 1.0D0)
!
      SSCALE=0.0D0
      do J=1,PICw%Ncomponents(NPICOR)
        SSCALE=SSCALE+PICw%coeff(PICANC+J)**2
      end do
      PICw%scale(NPICOR)=1.0D0/DSQRT(SSCALE)
!
      call CNV_ItoS(NPICOR,chartemp) ! Convert integer to character
      PICw%label(NPICOR)='BEND_'//chartemp
      write(UNIout, 2000)Message, I, CARTESIAN(I)%element
!
! End of routine PIC_AUTO_ADDCOORD_22
      call PRG_manager ('exit', 'PIC_AUTO_ADDCOORD_22', 'UTILITY')
      return
2000  FORMAT(1x,'Created coordinate:',1x,a23,1x,'about atom #',i4,1x,'(',a,')')
      end subroutine PIC_AUTO_ADDCOORD_22
      subroutine PIC_AUTO_RINGASS (ANGCMP, &
                                   SORTRING,   & !
                                   LSET,   & !
                                   DIMB,   & !
                                   DIMAN,  & !
                                   Message & !
                                    )
!***********************************************************************
!     Date last modified: December 16, 1996                Version 1.0 *
!     Author: Cory C. Pye                                              *
!     Description: Group ring angles/torsions, copy into PIC arrays.   *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer DIMB,DIMAN
!
! Work arrays:
      integer ANGCMP(MAX_PIC),SORTRING(MAX_PIC)
      logical LSET(MAX_PIC)
!
! Local scalars:
      integer :: I,J,K,ANGCT,BNUM1,BNUM2,AMINAT,AMAXAT,AAT1,AAT2,AAT3,COUNT,XCOM,COUNT2,MAXVAL,MINVAL,MAXJ,MINJ,startJ,SEED
      character*32 Message
      logical FOUND, SIMPLE, DONE
!
! Begin:
      call PRG_manager ('enter', 'PIC_AUTO_RINGASS', 'UTILITY')
!
      BNUM1=0
      BNUM2=0
! Loop over ring assemblies.
      do I=1,NRING_assembly
        ANGCT=0
        FOUND=.FALSE.
!
! Determine if angle is in current ring assembly.
        do J=1,NGAngle
          AAT1=ANGLE(J)%ATOM1
          AAT2=ANGLE(J)%ATOM2
          AAT3=ANGLE(J)%ATOM3
!
! Find the bonds matching these.
          AMINAT=MIN(AAT1,AAT2)
          AMAXAT=MAX(AAT1,AAT2)
          do K=1,NGBonds
            IF(AMINAT.EQ.BOND(K)%atom1.and.AMAXAT.EQ.BOND(K)%atom2) then
              BNUM1=K
            end if ! AMINAT
          end do ! K
!
          AMINAT=MIN(AAT2,AAT3)
          AMAXAT=MAX(AAT2,AAT3)
          do K=1,NGBonds
            IF(AMINAT.EQ.BOND(K)%atom1.and.AMAXAT.EQ.BOND(K)%atom2) then
              BNUM2=K
            end if ! AMINAT
          end do ! K
!
! If both bonds belong to current ring assembly, add angle to list.
          IF(edges(BNUM1)%ring.EQ.I.and.edges(BNUM2)%ring.EQ.I) then
            FOUND=.TRUE.
            ANGCT=ANGCT+1
            ANGCMP(ANGCT)=J
          end if
        end do ! J=1,NGAngle
!
        IF(.not.FOUND) then
          write(UNIout,1020)
          STOP
        else
          write(UNIout,3010) I,ANGCT
        end if
!
! Is this a simple cycle or more complicated ?
        SIMPLE=.TRUE.
        do J=1,ANGCT
          AAT1=ANGLE(ANGCMP(J))%atom2
          do K=J+1,ANGCT
            AAT2=ANGLE(ANGCMP(K))%atom2
            IF(AAT1.EQ.AAT2)SIMPLE=.FALSE.
          end do
        end do
!
        IF(SIMPLE)then
          write(UNIout,*) 'Simple cycle - generating coordinates.'
!
! We have a simple ring. Order angles around the ring.
          IF(ANGCT.LE.3) then
            write(UNIout,*)'3-membered ring. No coordinates generated.'
          else
! Find a starting atom.
! 1) First try valence criteria.
            write(UNIout,*) 'Attempting to determine ring symmetry.'
            MAXVAL=CONVAL(ANGLE(ANGCMP(1))%atom2)
            MINVAL=CONVAL(ANGLE(ANGCMP(1))%atom2)
            COUNT=1
            COUNT2=1
            MAXJ=1
            MINJ=1
            startJ=0
            do J=2,ANGCT
              XCOM=CONVAL(ANGLE(ANGCMP(J))%atom2)
              IF(XCOM.EQ.MAXVAL) then
                COUNT=COUNT+1
              else IF(XCOM.GT.MAXVAL) then
                COUNT=1
                MAXVAL=XCOM
                MAXJ=J
              end if
              IF(XCOM.EQ.MINVAL) then
                COUNT2=COUNT2+1
              else IF(XCOM.LT.MINVAL) then
                COUNT2=1
                MINVAL=XCOM
                MINJ=J
              end if
            end do ! J=2,ANGCT
!
            IF(COUNT.EQ.1) then
              startJ=MAXJ
            else IF(COUNT2.EQ.1) then
              startJ=MINJ
            end if
            IF(startJ.NE.0) then
              write(UNIout,*) 'Ring symmetry by valence worked'
            else
              write(UNIout,*) 'Ring symmetry by valence failed'
!
! valence criteria failed.
! 2)  Try finding most highly substituted by non-terminals.
              XCOM=0
              AAT2=ANGLE(ANGCMP(1))%atom2
! Find # of non-terminal atoms attached to first atom.
              do K=1,NGBonds
                IF(BOND(K)%atom1.EQ.AAT2) then
                  AAT1=BOND(K)%atom2
                  IF(CONVAL(AAT1).GT.1) XCOM=XCOM+1
                else IF(BOND(K)%atom2.EQ.AAT2) then
                  AAT1=BOND(K)%atom1
                  IF(CONVAL(AAT1).GT.1) XCOM=XCOM+1
                end if
              end do
!
              MAXVAL=XCOM
              MINVAL=XCOM
              COUNT=1
              COUNT2=1
              MAXJ=1
              MINJ=1
              startJ=0
              do J=2,ANGCT
                XCOM=0
                AAT2=ANGLE(ANGCMP(J))%atom2
!                do K=1,NBonds     ?? Zmatrix
                do K=1,Zmatrix%NBonds
                  IF(BOND(K)%atom1.EQ.AAT2) then
                    AAT1=BOND(K)%atom2
                    IF(CONVAL(AAT1).GT.1) XCOM=XCOM+1
                  else IF(BOND(K)%atom2.EQ.AAT2) then
                    AAT1=BOND(K)%atom1
                    IF(CONVAL(AAT1).GT.1) XCOM=XCOM+1
                  end if
                end do
!
! If number of nonterminal atoms attached to current atom exceeds previous
! minima or maxima, reset extrema. If equal, increment counter.
                IF(XCOM.EQ.MAXVAL) then
                  COUNT=COUNT+1
                else IF(XCOM.GT.MAXVAL) then
                  COUNT=1
                  MAXVAL=XCOM
                  MAXJ=J
                end if
                IF(XCOM.EQ.MINVAL) then
                  COUNT2=COUNT2+1
                else IF(XCOM.LT.MINVAL) then
                  COUNT2=1
                  MINVAL=XCOM
                  MINJ=J
                end if
              end do
              IF(COUNT.EQ.1) then
                startJ=MAXJ
              else IF(COUNT2.EQ.1) then
                startJ=MINJ
              end if
!
! If neither of these criteria work, then arbitrarily set starting angle to 1.
          IF(startJ.NE.0) then
            write(UNIout,*) 'Ring symmetry by terminal neighbors worked'
          else
            write(UNIout,*) 'Ring symmetry by terminal neighbors failed'
            write(UNIout,*) 'Using default'
            startJ=1
          end if
!
            end if
!
! Sort ring into adjacent angles
            do J=1,ANGCT
              LSET(J)=.FALSE.
            end do
            SORTRING(1)=ANGLE(ANGCMP(startJ))%atom2
            SEED=startJ
            LSET(startJ)=.TRUE.
            do J=2,ANGCT
              DONE=.FALSE.
              K=1
              do while(K.LE.ANGCT.and..not.DONE)
                IF(.not.LSET(K)) then
                  IF(ANGLE(ANGCMP(K))%atom3.EQ.ANGLE(ANGCMP(SEED))%atom2) then
                    DONE=.TRUE.
                    LSET(K)=.TRUE.
                    SEED=K
                    SORTRING(J)=ANGLE(ANGCMP(K))%atom2
                  else IF(ANGLE(ANGCMP(K))%atom1.EQ.ANGLE(ANGCMP(SEED))%atom2)then
                    DONE=.TRUE.
                    LSET(K)=.TRUE.
                    SEED=K
                    SORTRING(J)=ANGLE(ANGCMP(K))%atom2
                  else
                    K=K+1
                  end if
                else
                  K=K+1
                end if
              end do ! WHILE(K.LE.ANGCT.and..not.DONE)
              IF(K.GT.ANGCT) then
                write(UNIout,'(a)')'ERROR> PIC_AUTO_RINGASS: Programming error'
                stop 'ERROR> PIC_AUTO_RINGASS: Programming error'
              end if
            end do ! J=2,ANGCT
!
! Ring is sorted. Generate coordinates.
            call PIC_AUTO_RING (SORTRING, I, ANGCT, Message)
          end if ! ANGCT.LE.3
        else
          write(UNIout,*)'Not a simple ring. No coordinates generated.'
        end if ! SIMPLE
!
      end do ! I=1,NRING_assembly
!
! Let user know if there are no ring assemblies.'
      IF(NRING_assembly.LT.1) write (UNIout,3000)
!
! End of routine PIC_AUTO_RINGASS
      call PRG_manager ('exit', 'PIC_AUTO_RINGASS', 'UTILITY')
      return
1000  FORMAT(1x,a,2(I4,a))
1010  FORMAT('ERROR> PIC_AUTO_RINGASS: Programming error. Cannot symmetrize properly')
1020  FORMAT('ERROR> PIC_AUTO_RINGASS: Programming error. '//'Ring assembly assignments incorrect')
2000  FORMAT(1x,'Created coordinate:',1x,a23,1x,'about atom #',i4,1x,'(',a,')')
2010  FORMAT(1x,'Skipped coordinate:',1x,a23,1x,'about atom #',i4,1x,'(',a,')')
3000  FORMAT('NOTE: No ring assemblies in this system.')
3010  FORMAT(1x,'Number of angles in ring assembly ',I4,': ',I4)
      end subroutine PIC_AUTO_RINGASS
      subroutine PIC_AUTO_RING (SORTRING,  & !
                                CURASS,  & !
                                ANGCT,  & !
                                Message & !
                                 )
!***********************************************************************
!     Date last modified: December 16, 1996                Version 1.0 *
!     Author: Cory C. Pye                                              *
!     Description: Group ring angles/torsions, copy into PIC arrays.   *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer ANGCT,CURASS
!
! Input arrays:
      integer SORTRING(MAX_PIC)
!
! Local scalars:
      integer I,J,PICANC,CURCOM,AT1,AT2,AT3,AT4,ENDLOOP,INC
      double precision SSCALE, COEFF, TOL, PI
      character*32 Message, BigMessage
      character*4 chartemp
!
      parameter (TOL=1.0D-06)
!
! Begin:
      call PRG_manager ('enter', 'PIC_AUTO_RING', 'UTILITY')
!
! Bends
      BigMessage(1:)=Message(1:5)//'- def. - bend'
      do I=2,ANGCT/2
        NPICOR=NPICOR+1
        PICw%type(NPICOR)=3
        PICANC=NPICMP
        CURCOM=0
        do J=1,ANGCT
          COEFF=COS(I*(J-1)*TWO*PI_VAL/ANGCT)
          IF(ABS(COEFF).GT.TOL) then
!
            CURCOM=CURCOM+1
            NPICMP=NPICMP+1
            PICw%coeff(NPICMP)=COEFF
            AT2=J
            AT1=J-1
            IF(AT1.LT.1)AT1=AT1+ANGCT
            AT3=J+1
            IF(AT3.GT.ANGCT)AT3=AT3-ANGCT
!
            PICw%atom1(NPICMP)=SORTRING(AT1)
            PICw%atom2(NPICMP)=SORTRING(AT2)
            PICw%atom3(NPICMP)=SORTRING(AT3)
            PICw%atom4(NPICMP)=0
!
          end if
        end do
!
        SSCALE=0.0D0
        do J=1,CURCOM
          SSCALE=SSCALE+PICw%coeff(PICANC+J)**2
        end do
        PICw%Ncomponents(NPICOR)=CURCOM
        PICw%scale(NPICOR)=ONE/DSQRT(SSCALE)
!
        call CNV_ItoS(NPICOR,chartemp) ! Convert integer to character
        PICw%label(NPICOR)='BEND_'//chartemp
        write(UNIout, 2000)  BigMessage, CURASS

      end do
      ENDLOOP=ANGCT/2
      IF((ANGCT/2)*2.EQ.ANGCT)ENDLOOP=ENDLOOP-1
      do I=2,ENDLOOP
        NPICOR=NPICOR+1
        PICw%type(NPICOR)=3
        PICANC=NPICMP
        CURCOM=0
        do J=1,ANGCT
          COEFF=SIN(I*(J-1)*TWO*PI_VAL/ANGCT)
          IF(ABS(COEFF).GT.TOL) then
!
            CURCOM=CURCOM+1
            NPICMP=NPICMP+1
            PICw%coeff(NPICMP)=COEFF
            AT2=J
            AT1=J-1
            IF(AT1.LT.1)AT1=AT1+ANGCT
            AT3=J+1
            IF(AT3.GT.ANGCT)AT3=AT3-ANGCT
!
            PICw%atom1(NPICMP)=SORTRING(AT1)
            PICw%atom2(NPICMP)=SORTRING(AT2)
            PICw%atom3(NPICMP)=SORTRING(AT3)
            PICw%atom4(NPICMP)=0
          end if
        end do
!
        SSCALE=0.0D0
        do J=1,CURCOM
          SSCALE=SSCALE+PICw%coeff(PICANC+J)**2
        end do
        PICw%Ncomponents(NPICOR)=CURCOM
        PICw%scale(NPICOR)=ONE/DSQRT(SSCALE)
!
        call CNV_ItoS(NPICOR,chartemp) ! Convert integer to character
        PICw%label(NPICOR)='BEND_'//chartemp
        write(UNIout, 2000)  BigMessage, CURASS

      end do
!
! Torsions
      BigMessage(1:)=Message(1:5)//'- def. - torsion'
      ENDLOOP=ANGCT/2
      INC=0
      IF((ANGCT/2)*2.EQ.ANGCT)then
        ENDLOOP=ENDLOOP-1
      else
        INC=ENDLOOP-1
      end if
!
      do I=2,ANGCT/2
        NPICOR=NPICOR+1
        PICw%type(NPICOR)=5
        PICANC=NPICMP
        CURCOM=0
        do J=1,ANGCT
          COEFF=COS(I*(J-1)*TWO*PI_VAL/ANGCT)
          IF(ABS(COEFF).GT.TOL) then
!
            CURCOM=CURCOM+1
            NPICMP=NPICMP+1
            PICw%coeff(NPICMP)=COEFF
            AT1=J+INC
            IF(AT1.GT.ANGCT)AT1=AT1-ANGCT
            AT2=J+1+INC
            IF(AT2.GT.ANGCT)AT2=AT2-ANGCT
            AT3=J+2+INC
            IF(AT3.GT.ANGCT)AT3=AT3-ANGCT
            AT4=J+3+INC
            IF(AT4.GT.ANGCT)AT4=AT4-ANGCT
!
            PICw%atom1(NPICMP)=SORTRING(AT1)
            PICw%atom2(NPICMP)=SORTRING(AT2)
            PICw%atom3(NPICMP)=SORTRING(AT3)
            PICw%atom4(NPICMP)=SORTRING(AT4)
          end if
        end do
!
        SSCALE=0.0D0
        do J=1,CURCOM
          SSCALE=SSCALE+PICw%coeff(PICANC+J)**2
        end do
        PICw%Ncomponents(NPICOR)=CURCOM
        PICw%scale(NPICOR)=ONE/DSQRT(SSCALE)
!
        call CNV_ItoS(NPICOR,chartemp) ! Convert integer to character
        PICw%label(NPICOR)='TORS_'//chartemp
        write(UNIout, 2000)  BigMessage, CURASS

      end do
!
      do I=2,ENDLOOP
        NPICOR=NPICOR+1
        PICw%type(NPICOR)=5
        PICANC=NPICMP
        CURCOM=0
        do J=1,ANGCT
          COEFF=SIN(I*(J-1)*TWO*PI_VAL/ANGCT)
          IF(ABS(COEFF).GT.TOL) then
!
            CURCOM=CURCOM+1
            NPICMP=NPICMP+1
            PICw%coeff(NPICMP)=COEFF
            AT1=J+INC
            IF(AT1.GT.ANGCT)AT1=AT1-ANGCT
            AT2=J+1+INC
            IF(AT2.GT.ANGCT)AT2=AT2-ANGCT
            AT3=J+2+INC
            IF(AT3.GT.ANGCT)AT3=AT3-ANGCT
            AT4=J+3+INC
            IF(AT4.GT.ANGCT)AT4=AT4-ANGCT
!
            PICw%atom1(NPICMP)=SORTRING(AT1)
            PICw%atom2(NPICMP)=SORTRING(AT2)
            PICw%atom3(NPICMP)=SORTRING(AT3)
            PICw%atom4(NPICMP)=SORTRING(AT4)
          end if
        end do
!
        SSCALE=0.0D0
        do J=1,CURCOM
          SSCALE=SSCALE+PICw%coeff(PICANC+J)**2
        end do
        PICw%Ncomponents(NPICOR)=CURCOM
        PICw%scale(NPICOR)=ONE/DSQRT(SSCALE)
!
        call CNV_ItoS(NPICOR,chartemp) ! Convert integer to character
        PICw%label(NPICOR)='TORS_'//chartemp
        write(UNIout, 2000)  BigMessage, CURASS

      end do
!
! End of routine PIC_AUTO_RING
      call PRG_manager ('exit', 'PIC_AUTO_RING', 'UTILITY')
      return
1000  FORMAT(1x,a,2(I4,a))
2000  FORMAT(1x,'Created coordinate:',1x,a23,1x,'in ring assembly',i4)
      end subroutine PIC_AUTO_RING
      subroutine PIC_AUTO_VWBOND (ATSETB, &
                                  ATSET1, &
                                  ATSET2, &
                                  ATSET3, &
                                  NweakB,  & !
                                  DIMB1,  & !
                                  Message & !
                                   )
!***********************************************************************
!     Date last modified: December 20, 1996                Version 1.0 *
!     Author: Cory C. Pye                                              *
!     Description: Find coordinates about weak bonds.                  *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer DIMB1,NWeakB
!
! Input arrays:
      integer ATSET1(MAX_PIC),ATSET2(MAX_PIC),ATSET3(MAX_PIC)
      type (bond_atoms) ATSETB(DIMB1)
!
! Local scalars:
      integer I,J,K,AT1,AT2,ATCNT1,ATCNT2,ATCNT3,TAT1,TAT2,PICANC
      double precision SSCALE
      character*32 Message, BigMessage,pad
      character*4 chartemp
!
! Begin:
      call PRG_manager ('enter', 'PIC_AUTO_VWBOND', 'UTILITY')
!
      pad = '                                '
!
! Connect the really weak bonds!
      BigMessage=pad
      BigMessage(1:)=Message(1:12)
      call PIC_AUTO_BOND (ATSETB, NWeakB, DIMB1, BigMessage)
!
! Loop over all weak bonds.
      do I=1,NWeakB
        AT1=ATSETB(I)%atom1
        AT2=ATSETB(I)%atom2
        ATCNT1=0
        ATCNT2=0
!
! Now loop over all `normal' bonds to find adjacent real bonds.
        do J=1,NGBonds
          TAT1=BOND(J)%atom1
          TAT2=BOND(J)%atom2
!
! Check for adjacencies and add to set.
          IF(TAT1.EQ.AT1)then         ! *___1...2
            ATCNT1=ATCNT1+1
            ATSET1(ATCNT1)=TAT2
          else IF (TAT2.EQ.AT1)then
            ATCNT1=ATCNT1+1
            ATSET1(ATCNT1)=TAT1
          else IF (TAT1.EQ.AT2)then   ! 1...2___*
            ATCNT2=ATCNT2+1
            ATSET2(ATCNT2)=TAT2
          else IF (TAT2.EQ.AT2)then
            ATCNT2=ATCNT2+1
            ATSET2(ATCNT2)=TAT1
          end if
        end do
!
! Rocking about atom 1.
        IF(ATCNT1.EQ.0) then
          write(UNIout,1010) AT1
        else IF(ATCNT1.EQ.1) then
          BigMessage(1:)=pad
          BigMessage(1:)=Message(1:16)//'- bend'
! Loop over all `normal' bonds to find vicinal real bonds.
          ATCNT3=0
          do J=1,NGBonds
            TAT1=BOND(J)%atom1
            TAT2=BOND(J)%atom2
!
! Check for adjacencies and add to set.
            IF(TAT1.EQ.ATSET1(1).and.TAT2.NE.AT1)then ! *___?___1...2
              ATCNT3=ATCNT3+1
              ATSET3(ATCNT3)=TAT2
            else IF (TAT2.EQ.ATSET1(1).and.TAT1.NE.AT1)then
              ATCNT3=ATCNT3+1
              ATSET3(ATCNT3)=TAT1
            end if
          end do
!
! Bending mode.
          NPICOR=NPICOR+1
          PICw%type(NPICOR)=3
          PICw%Ncomponents(NPICOR)=1
          PICw%scale(NPICOR)=1.0D0
          NPICMP=NPICMP+1
          PICw%coeff(NPICMP)=1.0D0
          PICw%atom1(NPICMP)=ATSET1(1)
          PICw%atom2(NPICMP)=AT1
          PICw%atom3(NPICMP)=AT2
          PICw%atom4(NPICMP)=0
          call CNV_ItoS(NPICOR,chartemp) ! Convert integer to character
          PICw%label(NPICOR)='BEND_'//chartemp
          write(UNIout, 2000) BigMessage, AT1, CARTESIAN(AT1)%element
!
! Torsion Mode.
          IF(ATCNT3.GT.0)then
          BigMessage(1:)=pad
          BigMessage(1:)=Message(1:16)//'- tors.'
          NPICOR=NPICOR+1
          PICw%type(NPICOR)=5
          PICw%Ncomponents(NPICOR)=ATCNT3
          PICw%scale(NPICOR)=1.0D0/ATCNT3
          do J=1,ATCNT3
            NPICMP=NPICMP+1
            PICw%coeff(NPICMP)=1.0D0
            PICw%atom1(NPICMP)=AT2
            PICw%atom2(NPICMP)=AT1
            PICw%atom3(NPICMP)=ATSET1(1)
            PICw%atom4(NPICMP)=ATSET3(J)
          end do
          call CNV_ItoS(NPICOR,chartemp) ! Convert integer to character
          PICw%label(NPICOR)='TORS_'//chartemp
          write(UNIout, 2010) BigMessage, AT1, CARTESIAN(AT1)%element,ATSET1(1),CARTESIAN(ATSET1(1))%element

          end if
        else IF(ATCNT1.EQ.2) then
!
! Bend first
          BigMessage(1:)=pad
          BigMessage(1:)=Message(1:16)//'- rock'
          NPICOR=NPICOR+1
          PICw%type(NPICOR)=3
          PICw%Ncomponents(NPICOR)=2
          PICw%scale(NPICOR)=1.0D0
          PICANC=NPICMP
!
          NPICMP=NPICMP+1
          PICw%coeff(NPICMP)=1.0D0
          PICw%atom1(NPICMP)=ATSET1(1)
          PICw%atom2(NPICMP)=AT1
          PICw%atom3(NPICMP)=AT2
          PICw%atom4(NPICMP)=0
!
          NPICMP=NPICMP+1
          PICw%coeff(NPICMP)=-1.0D0
          PICw%atom1(NPICMP)=ATSET1(2)
          PICw%atom2(NPICMP)=AT1
          PICw%atom3(NPICMP)=AT2
          PICw%atom4(NPICMP)=0
!
          SSCALE=0.0D0
          do J=1,PICw%Ncomponents(NPICOR)
            SSCALE=SSCALE+PICw%coeff(PICANC+J)**2
          end do
          PICw%scale(NPICOR)=1.0D0/DSQRT(SSCALE)
!
          call CNV_ItoS(NPICOR,chartemp) ! Convert integer to character
          PICw%label(NPICOR)='BEND_'//chartemp
          write(UNIout, 2000) BigMessage, AT1, CARTESIAN(AT1)%element
! Next the out-of-plane
          BigMessage(1:)=pad
          BigMessage(1:)=Message(1:16)//'- oop'
          NPICOR=NPICOR+1
          PICw%type(NPICOR)=4
          PICw%Ncomponents(NPICOR)=1
          PICw%scale(NPICOR)=1.0D0
!
          NPICMP=NPICMP+1
          PICw%coeff(NPICMP)=1.0D0
          PICw%atom1(NPICMP)=AT2
          PICw%atom2(NPICMP)=ATSET1(1)
          PICw%atom3(NPICMP)=AT1
          PICw%atom4(NPICMP)=ATSET1(2)
!
          call CNV_ItoS(NPICOR,chartemp) ! Convert integer to character
          PICw%label(NPICOR)='OOP_'//chartemp
          write(UNIout, 2000) BigMessage, AT1, CARTESIAN(AT1)%element
!
        else IF(ATCNT1.EQ.3) then
!
! Rock #1
          BigMessage(1:)=pad
          BigMessage(1:)=Message(1:16)//'- rock'
          NPICOR=NPICOR+1
          PICw%type(NPICOR)=3
          PICw%Ncomponents(NPICOR)=3
          PICw%scale(NPICOR)=1.0D0
          PICANC=NPICMP
!
          NPICMP=NPICMP+1
          PICw%coeff(NPICMP)=2.0D0
          PICw%atom1(NPICMP)=ATSET1(1)
          PICw%atom2(NPICMP)=AT1
          PICw%atom3(NPICMP)=AT2
          PICw%atom4(NPICMP)=0
!
          NPICMP=NPICMP+1
          PICw%coeff(NPICMP)=-1.0D0
          PICw%atom1(NPICMP)=ATSET1(2)
          PICw%atom2(NPICMP)=AT1
          PICw%atom3(NPICMP)=AT2
          PICw%atom4(NPICMP)=0
!
          NPICMP=NPICMP+1
          PICw%coeff(NPICMP)=-1.0D0
          PICw%atom1(NPICMP)=ATSET1(3)
          PICw%atom2(NPICMP)=AT1
          PICw%atom3(NPICMP)=AT2
          PICw%atom4(NPICMP)=0
!
          SSCALE=0.0D0
          do J=1,PICw%Ncomponents(NPICOR)
            SSCALE=SSCALE+PICw%coeff(PICANC+J)**2
          end do
          PICw%scale(NPICOR)=1.0D0/DSQRT(SSCALE)
!
          call CNV_ItoS(NPICOR,chartemp) ! Convert integer to character
          PICw%label(NPICOR)='BEND_'//chartemp
          write(UNIout, 2000) BigMessage, AT1, CARTESIAN(AT1)%element
! Rock # 2
          BigMessage(1:)=pad
          BigMessage(1:)=Message(1:16)//'- rock'
          NPICOR=NPICOR+1
          PICw%type(NPICOR)=3
          PICw%Ncomponents(NPICOR)=2
          PICw%scale(NPICOR)=1.0D0
          PICANC=NPICMP
!
          NPICMP=NPICMP+1
          PICw%coeff(NPICMP)=1.0D0
          PICw%atom1(NPICMP)=ATSET1(2)
          PICw%atom2(NPICMP)=AT1
          PICw%atom3(NPICMP)=AT2
          PICw%atom4(NPICMP)=0
!
          NPICMP=NPICMP+1
          PICw%coeff(NPICMP)=-1.0D0
          PICw%atom1(NPICMP)=ATSET1(3)
          PICw%atom2(NPICMP)=AT1
          PICw%atom3(NPICMP)=AT2
          PICw%atom4(NPICMP)=0
!
          SSCALE=0.0D0
          do J=1,PICw%Ncomponents(NPICOR)
            SSCALE=SSCALE+PICw%coeff(PICANC+J)**2
          end do
          PICw%scale(NPICOR)=1.0D0/DSQRT(SSCALE)
          call CNV_ItoS(NPICOR,chartemp) ! Convert integer to character
          PICw%label(NPICOR)='BEND_'//chartemp
          write(UNIout, 2000) BigMessage, AT1, CARTESIAN(AT1)%element
        else
          write(UNIout, 1000) AT1
        end if
!
! Rocking about atom 2.
!
        IF(ATCNT2.EQ.0) then
          write(UNIout,1010) AT2
        else IF(ATCNT2.EQ.1) then
! Loop over all `normal' bonds to find vicinal real bonds.
          ATCNT3=0
          do J=1,NGBonds
            TAT1=BOND(J)%atom1
            TAT2=BOND(J)%atom2
!
! Check for adjacencies and add to set.
            IF(TAT1.EQ.ATSET2(1).and.TAT2.NE.AT2)then ! 1...2___?___*
              ATCNT3=ATCNT3+1
              ATSET3(ATCNT3)=TAT2
            else IF (TAT2.EQ.ATSET2(1).and.TAT1.NE.AT2)then
              ATCNT3=ATCNT3+1
              ATSET3(ATCNT3)=TAT1
            end if
          end do
!
! Bending mode.
          BigMessage(1:)=pad
          BigMessage(1:)=Message(1:16)//'- bend'
          NPICOR=NPICOR+1
          PICw%type(NPICOR)=3
          PICw%Ncomponents(NPICOR)=1
          PICw%scale(NPICOR)=1.0D0
          NPICMP=NPICMP+1
          PICw%coeff(NPICMP)=1.0D0
          PICw%atom1(NPICMP)=ATSET2(1)
          PICw%atom2(NPICMP)=AT2
          PICw%atom3(NPICMP)=AT1
          PICw%atom4(NPICMP)=0
          call CNV_ItoS(NPICOR,chartemp) ! Convert integer to character
          PICw%label(NPICOR)='BEND_'//chartemp
          write(UNIout, 2000) BigMessage, AT2, CARTESIAN(AT2)%element
!
! Torsion Mode.
          IF(ATCNT3.GT.0)then
          BigMessage(1:)=pad
          BigMessage(1:)=Message(1:16)//'- tors.'
          NPICOR=NPICOR+1
          PICw%type(NPICOR)=5
          PICw%Ncomponents(NPICOR)=ATCNT3
          PICw%scale(NPICOR)=1.0D0/ATCNT3
          do J=1,ATCNT3
            NPICMP=NPICMP+1
            PICw%coeff(NPICMP)=1.0D0
            PICw%atom1(NPICMP)=AT1
            PICw%atom2(NPICMP)=AT2
            PICw%atom3(NPICMP)=ATSET2(1)
            PICw%atom4(NPICMP)=ATSET3(J)
          end do
          call CNV_ItoS(NPICOR,chartemp) ! Convert integer to character
          PICw%label(NPICOR)='TORS_'//chartemp
          write(UNIout, 2010) BigMessage, AT2, CARTESIAN(AT2)%element,ATSET2(1),CARTESIAN(ATSET2(1))%element
          end if
!
        else IF(ATCNT2.EQ.2) then
!
! Bend first
          BigMessage(1:)=pad
          BigMessage(1:)=Message(1:16)//'- rock'
          NPICOR=NPICOR+1
          PICw%type(NPICOR)=3
          PICw%Ncomponents(NPICOR)=2
          PICw%scale(NPICOR)=1.0D0
          PICANC=NPICMP
!
          NPICMP=NPICMP+1
          PICw%coeff(NPICMP)=1.0D0
          PICw%atom1(NPICMP)=ATSET2(1)
          PICw%atom2(NPICMP)=AT2
          PICw%atom3(NPICMP)=AT1
          PICw%atom4(NPICMP)=0
!
          NPICMP=NPICMP+1
          PICw%coeff(NPICMP)=-1.0D0
          PICw%atom1(NPICMP)=ATSET2(2)
          PICw%atom2(NPICMP)=AT2
          PICw%atom3(NPICMP)=AT1
          PICw%atom4(NPICMP)=0
!
          SSCALE=0.0D0
          do J=1,PICw%Ncomponents(NPICOR)
            SSCALE=SSCALE+PICw%coeff(PICANC+J)**2
          end do
          PICw%scale(NPICOR)=1.0D0/DSQRT(SSCALE)
!
          call CNV_ItoS(NPICOR,chartemp) ! Convert integer to character
          PICw%label(NPICOR)='BEND_'//chartemp
          write(UNIout, 2000) BigMessage, AT2, CARTESIAN(AT2)%element
! Next the out-of-plane
          BigMessage(1:)=pad
          BigMessage(1:)=Message(1:16)//'- oop'
          NPICOR=NPICOR+1
          PICw%type(NPICOR)=4
          PICw%Ncomponents(NPICOR)=1
          PICw%scale(NPICOR)=1.0D0
!
          NPICMP=NPICMP+1
          PICw%coeff(NPICMP)=1.0D0
          PICw%atom1(NPICMP)=AT1
          PICw%atom2(NPICMP)=ATSET2(1)
          PICw%atom3(NPICMP)=AT2
          PICw%atom4(NPICMP)=ATSET2(2)
!
          call CNV_ItoS(NPICOR,chartemp) ! Convert integer to character
          PICw%label(NPICOR)='OOP_'//chartemp
          write(UNIout, 2000) BigMessage, AT2, CARTESIAN(AT2)%element
!
        else IF(ATCNT2.EQ.3) then
!
! Rock #1
          BigMessage(1:)=pad
          BigMessage(1:)=Message(1:16)//'- rock'
          NPICOR=NPICOR+1
          PICw%type(NPICOR)=3
          PICw%Ncomponents(NPICOR)=3
          PICw%scale(NPICOR)=1.0D0
          PICANC=NPICMP
!
          NPICMP=NPICMP+1
          PICw%coeff(NPICMP)=2.0D0
          PICw%atom1(NPICMP)=ATSET2(1)
          PICw%atom2(NPICMP)=AT2
          PICw%atom3(NPICMP)=AT1
          PICw%atom4(NPICMP)=0
!
          NPICMP=NPICMP+1
          PICw%coeff(NPICMP)=-1.0D0
          PICw%atom1(NPICMP)=ATSET2(2)
          PICw%atom2(NPICMP)=AT2
          PICw%atom3(NPICMP)=AT1
          PICw%atom4(NPICMP)=0
!
          NPICMP=NPICMP+1
          PICw%coeff(NPICMP)=-1.0D0
          PICw%atom1(NPICMP)=ATSET2(3)
          PICw%atom2(NPICMP)=AT2
          PICw%atom3(NPICMP)=AT1
          PICw%atom4(NPICMP)=0
!
          SSCALE=0.0D0
          do J=1,PICw%Ncomponents(NPICOR)
            SSCALE=SSCALE+PICw%coeff(PICANC+J)**2
          end do
          PICw%scale(NPICOR)=1.0D0/DSQRT(SSCALE)
!
          call CNV_ItoS(NPICOR,chartemp) ! Convert integer to character
          PICw%label(NPICOR)='BEND_'//chartemp
          write(UNIout, 2000) BigMessage, AT2, CARTESIAN(AT2)%element
! Rock # 2
          BigMessage(1:)=pad
          BigMessage(1:)=Message(1:16)//'- rock'
          NPICOR=NPICOR+1
          PICw%type(NPICOR)=3
          PICw%Ncomponents(NPICOR)=2
          PICw%scale(NPICOR)=1.0D0
          PICANC=NPICMP
!
          NPICMP=NPICMP+1
          PICw%coeff(NPICMP)=1.0D0
          PICw%atom1(NPICMP)=ATSET2(2)
          PICw%atom2(NPICMP)=AT2
          PICw%atom3(NPICMP)=AT1
          PICw%atom4(NPICMP)=0
!
          NPICMP=NPICMP+1
          PICw%coeff(NPICMP)=-1.0D0
          PICw%atom1(NPICMP)=ATSET2(3)
          PICw%atom2(NPICMP)=AT2
          PICw%atom3(NPICMP)=AT1
          PICw%atom4(NPICMP)=0
!
          SSCALE=0.0D0
          do J=1,PICw%Ncomponents(NPICOR)
            SSCALE=SSCALE+PICw%coeff(PICANC+J)**2
          end do
          PICw%scale(NPICOR)=1.0D0/DSQRT(SSCALE)
!
          call CNV_ItoS(NPICOR,chartemp) ! Convert integer to character
          PICw%label(NPICOR)='BEND_'//chartemp
          write(UNIout, 2000) BigMessage, AT2, CARTESIAN(AT2)%element
        else
          write(UNIout, 1000)AT2
        end if
        IF(ATCNT1*ATCNT2.NE.0) then
          BigMessage(1:)=pad
          BigMessage(1:)=Message(1:16)//'- tors.'
          NPICOR=NPICOR+1
          PICw%type(NPICOR)=5
          PICw%Ncomponents(NPICOR)=ATCNT1*ATCNT2
          PICw%scale(NPICOR)=1.0D0/(ATCNT1*ATCNT2)
          do J=1,ATCNT1
            do K=1,ATCNT2
              NPICMP=NPICMP+1
              PICw%coeff(NPICMP)=1.0D0
              PICw%atom1(NPICMP)=ATSET1(J)
              PICw%atom2(NPICMP)=AT1
              PICw%atom3(NPICMP)=AT2
              PICw%atom4(NPICMP)=ATSET2(K)
            end do
          end do
          call CNV_ItoS(NPICOR,chartemp) ! Convert integer to character
          PICw%label(NPICOR)='TORS_'//chartemp
          write(UNIout, 2010) BigMessage,AT1,CARTESIAN(AT1)%element,AT2,CARTESIAN(AT2)%element
        end if
      end do
!
! End of routine PIC_AUTO_VWBOND
      call PRG_manager ('exit', 'PIC_AUTO_VWBOND', 'UTILITY')
      return
1000  FORMAT(1x,'Rocking coordinates about weak bond not '//'implemented for valence > 3 about atom', I4)
1010  FORMAT(1x,'No rocking coordinates about weak bond to atom ',I4)
2000  FORMAT(1x,'Created coordinate:',1x,a23,1x,'about atom #',i4,1x,'(',a,')')
2010  FORMAT(1x,'Created coordinate:',1x,a23,1x,'about atoms ',i4,1x,'(',a,') and', i4,1x,'(',a,')')
      end subroutine PIC_AUTO_VWBOND
      subroutine PIC_AUTO_WBOND (ATSET1,   & !
                                 ATSET2,   & !
                                 ATSET3,   & !
                                 ATSET4,   & !
                                 ATSET5,   & !
                                 WRKAUT,   & !
                                 LSET,   & !
                                 DIMB2,  & !
                                 DIMB1,  & !
                                 Message & !
                                  )
!***********************************************************************
!     Date last modified: January   2, 1997                Version 1.0 *
!     Author: Cory C. Pye                                              *
!     Description: Find coordinates about weak bonds.                  *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer DIMB1,DIMB2

!
! Input arrays:
      integer ATSET1(MAX_PIC),ATSET2(MAX_PIC),ATSET3(MAX_PIC),ATSET4(MAX_PIC),ATSET5(MAX_PIC)
      logical LSET(MAX_PIC)
      logical WRKAUT(NComponents,NComponents)
      type (bond_atoms) ATSETB(MAX_PIC)
!
! Local scalars:
      integer I,J,K,L,AT1,AT2,ATCNT1,ATCNT2,ATCNT3,TAT1,TAT2,A11,A12,A13,A21,A22,A23,NSNGCN,COMP1,COMP2,COUNT,TCOMP1,TCOMP2
      character*32 Message, BigMessage,pad
      character*4 chartemp
      logical DONE,SET,STEP
!
! Begin:
      call PRG_manager ('enter', 'PIC_AUTO_WBOND', 'UTILITY')
!
      pad = '                                '
!
      IF(REDUND.and.EXTENT(1:3).EQ.'VAL') then
!
! Make the stretches/angles/torsions about all weak bonds.
        DIMB1=max(1,NvderWBond)
        Message=pad
        Message(1:)='weak bond def'
        call PIC_AUTO_VWBOND (VdWBond, ATSET1, ATSET2, ATSET3, NvderWBond, DIMB1, Message)
      else
! The code in this IF block is old and needs to be replaced.

!
! Don't connect component to itself.
      do I=1,NComponents
        WRKAUT(I,I)=.TRUE.
      end do
!
      NSNGCN=0
!
! Make coordinates.
        do I=1, NvderWBond
          LSET(I)=.FALSE.
          AT1=VdWBond(I)%atom1
          AT2=VdWBond(I)%atom2
          COMP1=CNCOMP(AT1)
          COMP2=CNCOMP(AT2)
          SET=WRKAUT(COMP1,COMP2)
          IF(.not.SET) then
            COUNT=1
            WRKAUT(COMP1,COMP2)=.TRUE.
            WRKAUT(COMP2,COMP1)=.TRUE.
!
! If weak connection found between components COMP1 and COMP2 where none
! was before, register this and disallow any new connections which would
! form a ring of weak connections.
            DONE=.FALSE.
            do while (.not.DONE)
              do J=1,NComponents
                DONE=.TRUE.
                do K=J+1,NComponents
                  STEP=.FALSE.
                  do L=1,NComponents
                    IF(WRKAUT(J,L).and.WRKAUT(K,L))STEP=.TRUE.
                  end do
                  DONE=DONE.and..not.STEP
                  IF(STEP)then
                    do L=1,NComponents
                      WRKAUT(J,L)=WRKAUT(J,L).or.WRKAUT(K,L)
                      WRKAUT(K,L)=WRKAUT(J,L)
                      WRKAUT(L,J)=WRKAUT(J,L)
                      WRKAUT(L,K)=WRKAUT(J,L)
                    end do ! L=1
                  end if ! STEP
                end do ! K=J+1
              end do ! J=1
            end do ! WHILE.not.DONE
!
            LSET(I)=.TRUE.
            ATSET1(COUNT)=AT1
            ATSET2(COUNT)=AT2
!
! Find all other connections between COMP1 and COMP2.
            do J=I+1,NvderWBond
              TAT1=VdWBond(J)%atom1
              TAT2=VdWBond(J)%atom2
              TCOMP1=CNCOMP(TAT1)
              TCOMP2=CNCOMP(TAT2)
              IF(TCOMP1.EQ.COMP1.and.TCOMP2.EQ.COMP2)then
                COUNT=COUNT+1
                ATSET1(COUNT)=TAT1
                ATSET2(COUNT)=TAT2
              else IF(TCOMP1.EQ.COMP2.and.TCOMP2.EQ.COMP1)then
                COUNT=COUNT+1
                ATSET1(COUNT)=TAT2
                ATSET2(COUNT)=TAT1
              end if
            end do ! J=1
            IF(COUNT.EQ.1)then
!
! The usual connection, as for VERY weak bonds! Store into ATSET3 and ATSET4
! for call to PIC_AUTO_VWBOND.
              NSNGCN=NSNGCN+1
              ATSET3(NSNGCN)=AT1
              ATSET4(NSNGCN)=AT2

            else IF(COUNT.EQ.2)then
!
! Check for coincidences.
              write(UNIout,1100)
              IF(ATSET1(1).EQ.ATSET1(2))then
                write(UNIout,1120)2,ATSET1(1),CARTESIAN(ATSET1(1))%element
                write(UNIout,1140)
              else IF(ATSET2(1).EQ.ATSET2(2))then
                write(UNIout,1120)2,ATSET2(1),CARTESIAN(ATSET2(1))%element
                write(UNIout,1140)
              else
                write(UNIout,1110)
                DIMB1=max(1,NGBonds)
                DIMB2=max(1,NvderWBond)
                Message=pad
                Message(1:)='2-weak bond butterfly'
!
                call PIC_AUTO_BUTTERFLY (ATSET1, ATSET2, ATSET5, DIMB2, DIMB1, Message)
!
! 4-ring def and pucker.
                ATSET5(1)=ATSET1(1)
                ATSET5(2)=ATSET1(2)
                ATSET5(3)=ATSET2(2)
                ATSET5(4)=ATSET2(1)
                Message=pad
                Message(1:)='4-ring 2-weak bond'
!
                call PIC_AUTO_RING (ATSET5, 0, 4, Message)

              end if
              do J=1,COUNT
                write(UNIout,1130)ATSET1(J),CARTESIAN(ATSET1(J))%element,ATSET2(J),CARTESIAN(ATSET2(J))%element
              end do
            else IF(COUNT.GE.3)then
              IF(COUNT.NE.3)write(UNIout,3000)
              write(UNIout,1200)
!
              A11=ATSET1(1)
              A12=ATSET1(2)
              A13=ATSET1(3)
              A21=ATSET2(1)
              A22=ATSET2(2)
              A23=ATSET2(3)
! 3-fold
              IF(A11.EQ.A12.and.A11.EQ.A13)then
                write(UNIout,1120)3,A11,CARTESIAN(A11)%element
                write(UNIout,1220)
              else IF(A21.EQ.A22.and.A21.EQ.A23)then
                write(UNIout,1120)3,A21,CARTESIAN(A21)%element
                write(UNIout,1220)
! 2x2-fold
              else IF(A11.EQ.A12.and.A21.EQ.A23) then
                write(UNIout,1120)2,A12,CARTESIAN(A12)%element
                write(UNIout,1120)2,A23,CARTESIAN(A23)%element
                write(UNIout,1230)
              else IF(A11.EQ.A12.and.A22.EQ.A23) then
                write(UNIout,1120)2,A11,CARTESIAN(A11)%element
                write(UNIout,1120)2,A23,CARTESIAN(A23)%element
                write(UNIout,1230)
              else IF(A11.EQ.A13.and.A21.EQ.A22) then
                write(UNIout,1120)2,A13,CARTESIAN(A13)%element
                write(UNIout,1120)2,A22,CARTESIAN(A22)%element
                write(UNIout,1230)
              else IF(A11.EQ.A13.and.A22.EQ.A23) then
                write(UNIout,1120)2,A11,CARTESIAN(A11)%element
                write(UNIout,1120)2,A22,CARTESIAN(A22)%element
                write(UNIout,1230)
              else IF(A12.EQ.A13.and.A21.EQ.A22) then
                write(UNIout,1120)2,A13,CARTESIAN(A13)%element
                write(UNIout,1120)2,A21,CARTESIAN(A21)%element
                write(UNIout,1230)
              else IF(A12.EQ.A13.and.A21.EQ.A23) then
                write(UNIout,1120)2,A12,CARTESIAN(A12)%element
                write(UNIout,1120)2,A21,CARTESIAN(A21)%element
                write(UNIout,1230)
! 2-fold
              else IF(A11.EQ.A12) then
                write(UNIout,1120)2,A11,CARTESIAN(A11)%element
                write(UNIout,1240)
              else IF(A11.EQ.A13) then
                write(UNIout,1120)2,A13,CARTESIAN(A13)%element
                write(UNIout,1240)
              else IF(A12.EQ.A13) then
                write(UNIout,1120)2,A12,CARTESIAN(A12)%element
                write(UNIout,1240)
              else IF(A21.EQ.A22) then
                write(UNIout,1120)2,A21,CARTESIAN(A21)%element
                write(UNIout,1240)
              else IF(A21.EQ.A23) then
                write(UNIout,1120)2,A23,CARTESIAN(A23)%element
                write(UNIout,1240)
              else IF(A22.EQ.A23) then
                write(UNIout,1120)2,A22,CARTESIAN(A22)%element
                write(UNIout,1240)
              else
                write(UNIout,1210)
              end if
!
              do J=1,min(3,COUNT)
                write(UNIout,1130)ATSET1(J),CARTESIAN(ATSET1(J))%element,ATSET2(J),CARTESIAN(ATSET2(J))%element
              end do
            end if
!
! Add in weak bonds.
            IF(COUNT.GE.2)then
              DIMB2=max(1,min(3,COUNT))
              Message=pad
              Message(1:9)='weak bond'
              do J=1,count
              ATSETB(J)%atom1=ATSET1(J)
              ATSETB(J)%atom2=ATSET2(J)
              end do
              call PIC_AUTO_BOND (ATSETB, COUNT, DIMB2, Message)
            end if ! COUNT.GE.2
          end if ! COUNT
        end do
!
! Make the stretches/angles/torsions about all `single' weak bonds.
        DIMB1=max(1,NSNGCN)
        Message=pad
        Message(1:)='weak bond'
        do i=1,nsngcn
        ATSETB(i)%atom1=ATSET3(i)
        ATSETB(i)%atom2=ATSET4(i)
        end do
        call PIC_AUTO_VWBOND (ATSETB, ATSET1, ATSET2, ATSET5, NSNGCN, DIMB1, Message)

! End of replacements.
      end if! REDUND.and.EXTENT(1:3).EQ.'VAL'
!
! End of routine PIC_AUTO_WBOND
      call PRG_manager ('exit', 'PIC_AUTO_WBOND', 'UTILITY')
      return
1000  FORMAT(1x,'Rocking coordinates about weak bond not '//'implemented for valence > 3 about atom', I4)
1010  FORMAT(1x,'No rocking coordinates about weak bond to atom ',I4)
1100  FORMAT(1x,'Two connections between components',1x,I4,' and ',I4)
1110  FORMAT(1x,'No coincidences: Adding '//'4-ring def (2) and butterfly (2) coordinates')
1120  FORMAT(1x,I4,'-fold coincidence at atom ',I4,1x,'(',a,')')
1130  FORMAT(1x,'Connect atoms ',i4,1x,'(',a,') and', i4,1x,'(',a,')')
1140  FORMAT(1x,'Please manually add appropriate rocking (1), twisting '//'(1) and deformation (2) coordinates')
1200  FORMAT(1x,'Three connections between components',1x,I4,' and ',I4)
1210  FORMAT(1x,'No coincidences: Please manually add appropriate '//'twisting (1) and shearing (2) coordinates')
1220  FORMAT(1x,'Please manually add appropriate rocking (2) and '//'twisting (1) coordinates')
1230  FORMAT(1x,'Please manually add appropriate rocking (2) '//'and ring pucker (1) coordinates')
1240  FORMAT(1x,'Please manually add appropriate rocking (1), twisting '//'(1) and shearing (1) coordinates')
2000  FORMAT(1x,'Created coordinate:',1x,a23,1x,'about atom #',i4,1x,'(',a,')')
2010  FORMAT(1x,'Created coordinate:',1x,a23,1x,'about atoms ',i4,1x,'(',a,') and', i4,1x,'(',a,')')
3000  FORMAT(1x,'More than 3 close contacts between components//'//'Using first three found')
      end subroutine PIC_AUTO_WBOND
      subroutine PIC_AUTO_BUTTERFLY (ATSET1, ATSET2, ATSET5,  & !
                                     DIMB2,  & !
                                     DIMB1,  & !
                                     Message & !
                                      )
!***********************************************************************
!     Date last modified: June      6, 1997                Version 1.0 *
!     Author: Cory C. Pye                                              *
!     Description: Find butterfly coordinates about 2 weak bonds.      *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer DIMB1,DIMB2
!
! Input arrays:
      integer ATSET1(MAX_PIC),ATSET2(MAX_PIC),ATSET5(MAX_PIC)
!
      integer I,J,ATCNT1,ATCNT2,AT1,AT2
      character*32 Message, BigMessage,pad
      character*4 chartemp
      logical NEWCOR
!
! Begin:
      call PRG_manager ('enter', 'PIC_AUTO_BUTTERFLY', 'UTILITY')
!
      pad = '                                '
!
!    X-11:::::12...22
      BigMessage(1:)=pad
      BigMessage(1:)=Message(1:16)
      ATCNT1=0
      do I=1,NGBonds
        AT1=BOND(I)%ATOM1
        AT2=BOND(I)%ATOM2
        IF(AT1.EQ.ATSET1(1).and.AT2.NE.ATSET1(2))then
          ATCNT1=ATCNT1+1
          ATSET5(ATCNT1)=AT2
        else IF(AT2.EQ.ATSET1(1).and.AT1.NE.ATSET1(2))then
          ATCNT1=ATCNT1+1
          ATSET5(ATCNT1)=AT1
        end if
      end do
      IF(ATCNT1.NE.0)then
        NPICOR=NPICOR+1
        PICw%type(NPICOR)=5
        do J=1,ATCNT1
          NPICMP=NPICMP+1
          PICw%coeff(NPICMP)=1.0D0
          PICw%atom1(NPICMP)=ATSET5(J)
          PICw%atom2(NPICMP)=ATSET1(1)
          PICw%atom3(NPICMP)=ATSET1(2)
          PICw%atom4(NPICMP)=ATSET2(2)
        end do
      end if
!
!    21...11:::::12-X
      ATCNT2=0
      do I=1,NGBonds
        AT1=BOND(I)%ATOM1
        AT2=BOND(I)%ATOM2
        IF(AT1.EQ.ATSET1(2).and.AT2.NE.ATSET1(1))then
          ATCNT2=ATCNT2+1
          ATSET5(ATCNT2)=AT2
        else IF(AT2.EQ.ATSET1(2).and.AT1.NE.ATSET1(1))then
          ATCNT2=ATCNT2+1
          ATSET5(ATCNT2)=AT1
        end if
      end do
      IF(ATCNT2.NE.0)then
        IF(ATCNT1.EQ.0)then
          NPICOR=NPICOR+1
          PICw%type(NPICOR)=5
        end if
        do J=1,ATCNT2
          NPICMP=NPICMP+1
          PICw%coeff(NPICMP)=-1.0D0
          PICw%atom1(NPICMP)=ATSET5(J)
          PICw%atom2(NPICMP)=ATSET1(2)
          PICw%atom3(NPICMP)=ATSET1(1)
          PICw%atom4(NPICMP)=ATSET2(1)
        end do
      end if
      IF(ATCNT1+ATCNT2.NE.0)then
        PICw%Ncomponents(NPICOR)=ATCNT1+ATCNT2
        PICw%scale(NPICOR)=1.0D0/(ATCNT1+ATCNT2)
        call CNV_ItoS(NPICOR,chartemp) ! Convert integer to character
        PICw%label(NPICOR)='WBUT_'//chartemp
        write(UNIout,2010) BigMessage, ATSET1(1),CARTESIAN(ATSET1(1))%element, &
                                       ATSET1(2),CARTESIAN(ATSET1(2))%element
      end if
!
!    X-21:::::22...12
      BigMessage(1:)=pad
      BigMessage(1:)=Message(1:16)
      ATCNT1=0
      do I=1,NGBonds
        AT1=BOND(I)%ATOM1
        AT2=BOND(I)%ATOM2
        IF(AT1.EQ.ATSET2(1).and.AT2.NE.ATSET2(2))then
          ATCNT1=ATCNT1+1
          ATSET5(ATCNT1)=AT2
        else IF(AT2.EQ.ATSET2(1).and.AT1.NE.ATSET2(2))then
          ATCNT1=ATCNT1+1
          ATSET5(ATCNT1)=AT1
        end if
      end do
      IF(ATCNT1.NE.0)then
        NPICOR=NPICOR+1
        PICw%type(NPICOR)=5
        do J=1,ATCNT1
          NPICMP=NPICMP+1
          PICw%coeff(NPICMP)=1.0D0
          PICw%atom1(NPICMP)=ATSET5(J)
          PICw%atom2(NPICMP)=ATSET2(1)
          PICw%atom3(NPICMP)=ATSET2(2)
          PICw%atom4(NPICMP)=ATSET1(2)
        end do
      end if
!
!    11...21:::::22-X
      ATCNT2=0
      do I=1,NGBonds
        AT1=BOND(I)%ATOM1
        AT2=BOND(I)%ATOM2
        IF(AT1.EQ.ATSET2(2).and.AT2.NE.ATSET2(1))then
          ATCNT2=ATCNT2+1
          ATSET5(ATCNT2)=AT2
        else IF(AT2.EQ.ATSET2(2).and.AT1.NE.ATSET2(1))then
          ATCNT2=ATCNT2+1
          ATSET5(ATCNT2)=AT1
        end if
      end do
      IF(ATCNT2.NE.0)then
        IF(ATCNT1.EQ.0)then
          NPICOR=NPICOR+1
          PICw%type(NPICOR)=5
        end if
        do J=1,ATCNT2
          NPICMP=NPICMP+1
          PICw%coeff(NPICMP)=-1.0D0
          PICw%atom1(NPICMP)=ATSET5(J)
          PICw%atom2(NPICMP)=ATSET2(2)
          PICw%atom3(NPICMP)=ATSET2(1)
          PICw%atom4(NPICMP)=ATSET1(1)
        end do
      end if
      IF(ATCNT1+ATCNT2.NE.0)then
        PICw%Ncomponents(NPICOR)=ATCNT1+ATCNT2
        PICw%scale(NPICOR)=1.0D0/(ATCNT1+ATCNT2)
        call CNV_ItoS(NPICOR,chartemp) ! Convert integer to character
        PICw%label(NPICOR)='WBUT_'//chartemp
        write(UNIout,2010) BigMessage, ATSET2(1),CARTESIAN(ATSET2(1))%element, &
                                       ATSET2(2),CARTESIAN(ATSET2(2))%element
      end if
!
! End of routine PIC_AUTO_BUTTERFLY
      call PRG_manager ('exit', 'PIC_AUTO_BUTTERFLY', 'UTILITY')
      return
 2010 FORMAT(1x,'Created coordinate:',1x,a23,1x,'about atoms ',i4,1x,'(',a,') and', i4,1x,'(',a,')')
      end subroutine PIC_AUTO_BUTTERFLY
      subroutine DEF_PICSYM
!***********************************************************************
!     Date last modified: May 20, 1997                     Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: Set default symmetry for proper internal coordinates*
!***********************************************************************
! Modules:

      implicit none
!
! Includes:
!
! Local scalars:
      integer IPICOR
!
! Begin:
      call PRG_manager ('enter', 'DEF_PICSYM', 'UTILITY')
!
      do IPICOR=1,MAX_PIC
        PICw%symmetry(IPICOR)=IPICOR
      end do ! IPICOR

!
! End of routine DEF_PICSYM
      call PRG_manager ('exit', 'DEF_PICSYM', 'UTILITY')
      return
      end subroutine DEF_PICSYM
      subroutine PIC_SYM
!***********************************************************************
!     Date last modified: May 20, 1997                                 *
!     Author: R.A. Poirier                                             *
!     Description: Set up symmetry for PIC.                            *
!***********************************************************************
! Modules:

      implicit none
!
! Work array:
      integer, dimension(:), allocatable :: SYMLST
!
! Local scalar:
      integer I,J,frstPIC,Ioptpr,IPICOR,JPICOR,NSYMPAR
      logical done,FOUND
!
! Begin:
      call PRG_manager ('enter', 'PIC_SYM', 'UTILITY')
      done=.false.
!
      allocate (SYMLST(1:NPICOR))
!
! Menu:
      do while (.not.done)
      call new_token (' SYM:')
!
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      ' Command SYM :', &
      '   Purpose: Set symmetry for proper internal coordinates', &
      '   NOTE: coordinates must already be defined!', &
      '   Syntax :', &
      '   SET  = <integer list> or', &
      '   OPT  = <integer list> ', &
      '   FIXed  = <integer list> ', &
      '   end'
!
! SET or OPT
      else if(token(1:3).eq.'SET'.or.token(1:3).eq.'OPT')then
        call GET_value (SYMLST, NSYMPAR)
        frstPIC=MINVAL(IABS(SYMLST(1:NSYMPAR))) ! Find the smallest element
        do IPICOR=1,NSYMPAR
          IF(SYMLST(IPICOR).GT.0)then
            PICw%symmetry(SYMLST(IPICOR))=frstPIC
          else
            PICw%symmetry(IABS(SYMLST(IPICOR)))=-frstPIC
          end if
        end do ! IPICOR
! FIXed
      else if(token(1:3).eq.'FIX')then
        call GET_value (SYMLST, NSYMPAR)
        do IPICOR=1,NSYMPAR
          PICw%symmetry(SYMLST(IPICOR))=0
        end do ! IPICOR
      else
        call MENU_end (done)
      end if
!
      end do !(.not.done)
!
      deallocate (SYMLST)
!
! End of routine PIC_SYM
      call PRG_manager ('exit', 'PIC_SYM', 'UTILITY')
      return
      end subroutine PIC_SYM
      subroutine MOD_PIC_SYM
!***********************************************************************
!     Date last modified: May 20, 1997                                 *
!     Author: R.A. Poirier                                             *
!     Description: Set up symmetry for PIC.                            *
!***********************************************************************
! Modules:

      implicit none
!
! Local scalar:
      integer I,J,Ioptpr,IPICOR,JPICOR
      logical FOUND
!
! Begin:
      call PRG_manager ('enter', 'MOD_PIC_SYM', 'UTILITY')
!
      Ioptpr=0
      do IPICOR=1,PIC%Ncoordinates
        FOUND=.FALSE.
        do JPICOR=1,PIC%Ncoordinates
          IF(IPICOR.EQ.IABS(PIC%symmetry(JPICOR)))then
            PIC%symmetry(JPICOR)=ISIGN(Ioptpr+1, PIC%symmetry(JPICOR))
            FOUND=.TRUE.
          end if
        end do ! JPICOR
        IF(FOUND)then
          Ioptpr=Ioptpr+1
        end if
      end do ! IPICOR
!
! End of routine MOD_PIC_SYM
      call PRG_manager ('exit', 'MOD_PIC_SYM', 'UTILITY')
      return
      end subroutine MOD_PIC_SYM
      subroutine BLD_PIC
!***********************************************************************
!     Date last modified: May 10, 1995                     Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!***********************************************************************
! Modules:

      implicit none
!
! Local scalars:
      integer I,IC,KA,KB,KC,KD,Ktype,NT
      double precision A180,CCC,CTETA,CUV,CUW,CVU,CX,CXV,CZW,CZX,C0,C1,HPI,PI2,QT,RTODEG,R1,R2,R3,S,SCALAR,SI3, &
                       STETA,T,TENM11,U(3),V(3),W(3),X(3),Z(3)
      logical ERROR
!
      parameter (TENM11=1.0D-11,A180=180.0D0,HPI=1.5707963268D0,PI2=6.2831853072D0,RTODEG=A180/PI_VAL)
!
! Begin:
      call PRG_manager ('enter', 'BLD_PIC', 'UTILITY')
!
      ERROR=.false.
      PIC%Ncoordinates=NPICOR
      PIC%Total_components=NPICMP
      allocate (PIC%type(NPICOR), PIC%symmetry(NPICOR), PIC%scale(NPICOR), PIC%value(NPICOR), PIC%label(NPICOR), &
                PIC%atom1(NPICMP), PIC%atom2(NPICMP), PIC%atom3(NPICMP), &
                PIC%atom4(NPICMP), PIC%coeff(NPICMP), PIC%component(NPICMP), &
                PIC%Ncomponents(NPICOR))

      PIC%type(1:PIC%Ncoordinates)=PICw%type(1:PIC%Ncoordinates)
      PIC%label(1:PIC%Ncoordinates)=PICw%label(1:PIC%Ncoordinates)
      PIC%scale(1:PIC%Ncoordinates)=PICw%scale(1:PIC%Ncoordinates)
      PIC%Ncomponents(1:PIC%Ncoordinates)=PICw%Ncomponents(1:PIC%Ncoordinates)

      PIC%symmetry(1:PIC%Ncoordinates)=PICw%symmetry(1:PIC%Ncoordinates)
      PIC%atom1(1:PIC%Total_components)=PICw%atom1(1:PIC%Total_components)
      PIC%atom2(1:PIC%Total_components)=PICw%atom2(1:PIC%Total_components)
      PIC%atom3(1:PIC%Total_components)=PICw%atom3(1:PIC%Total_components)
      PIC%atom4(1:PIC%Total_components)=PICw%atom4(1:PIC%Total_components)
      PIC%coeff(1:PIC%Total_components)=PICw%coeff(1:PIC%Total_components)

      deallocate (PICw%type, PICw%symmetry, PICw%scale, PICw%label, PICw%atom1, PICw%atom2, PICw%atom3, &
                PICw%atom4, PICw%coeff, PICw%Ncomponents)
!
! Define the internal coordinates.
      NT=0
      do I=1,PIC%Ncoordinates
        Ktype=PIC%type(I)
        CCC=PIC%scale(I)
        C1=ZERO
        QT=ZERO
        do Icomponent=1,PIC%Ncomponents(I)
          NT=NT+1
          KA=PIC%atom1(NT)
          KB=PIC%atom2(NT)
          KC=PIC%atom3(NT)
          KD=PIC%atom4(NT)
          C0=PIC%coeff(NT)
          C1=C1+C0*C0
          IC=4
          IF(Ktype.EQ.1)then ! Stretch (A-B).
            IC=2
            call VEC (U, R1, KA, KB)
            PIC%component(NT)=R1
            QT=QT+PIC%component(NT)*C0
          else IF(Ktype.EQ.2)then ! Inverse distance
            call VEC (U, R1, KA, KB)
            PIC%component(NT)=ONE/R1
            QT=QT+PIC%component(NT)*C0
            PIC%component(NT)=PIC%component(NT)*RTODEG

          else IF(Ktype.EQ.3)then ! Bend A-B-C (B is the central atom).
            IC=3
            call VEC (U, R1, KA, KB)
            call VEC (V, R2, KC, KB)
            CUV=DOT_product(U,V)
            PIC%component(NT)=DACOS(CUV)
            QT=QT+PIC%component(NT)*C0
            PIC%component(NT)=PIC%component(NT)*RTODEG
!
          else IF(Ktype.EQ.4)then   ! Out-of-plane bend
            call VEC (U, R1, KA, KD)
            call VEC (V, R2, KB, KD)
            call VEC (W, R3, KC, KD)
            call VEC_product (Z, V, W, T, 1)
            STETA=DOT_product(U,Z)
            CTETA=DSQRT(ONE-STETA*STETA)
            CX=-C0
            IF(STETA.LT.ZERO)CX=C0
            PIC%component(NT)=-DACOS(CTETA)
            QT=QT+CX*PIC%component(NT)
            PIC%component(NT)=PIC%component(NT)*RTODEG
!
          else IF(Ktype.EQ.5)then ! Torsion
            call VEC (U, R1, KA, KB)
            call VEC (V, R2, KC, KB)
            call VEC (W, R3, KC, KD)
            call VEC_product (Z, U, V, T, 1)
            call VEC_product (X, W, V, T, 1)
            CZX=-DOT_product(Z,X)
            call VEC_product (U, Z, X, T, 0)
            CUV=DOT_product(U,V)
            S=HPI

            IF(DABS(CZX).GE.TENM11)then
              SI3=DSQRT(U(1)*U(1)+U(2)*U(2)+U(3)*U(3))
              S=DATAN(SI3/CZX)
              IF(CZX.LT.ZERO)S=S+PI_VAL

            end if
            IF(CUV.LT.ZERO)S=-S
! PICTOC Convergence difficulties caused by next line.
!            IF(S.GT.HPI)S=S-PI2
            PIC%component(NT)=-S
            QT=QT+PIC%component(NT)*C0
            PIC%component(NT)=PIC%component(NT)*RTODEG

          else IF(Ktype.EQ.6)then ! Linear Bend 1
            call VEC (U, R1, KA, KC)
            call VEC (V, R2, KD, KC)
            call VEC (X, R2, KB, KC)
            CVU=DOT_product(V,U)
            CXV=DOT_product(X,V)
            PIC%component(NT)=(PI_VAL-DACOS(CVU)-DACOS(CXV))
            QT=QT+PIC%component(NT)*C0
            PIC%component(NT)=PIC%component(NT)*RTODEG
          else IF(Ktype.EQ.7)then  ! Linear Bend 2
            call VEC (U, R1, KA, KC)
            call VEC (V, R2, KD, KC)
            call VEC (Z, R2, KB, KC)
            call VEC_product (W, V, U, T, 1)
            call VEC_product (X, Z, V, T, 1)
            CUW=DOT_product(U,W)
            CZW=DOT_product(Z,W)
            PIC%component(NT)=(PI_VAL-DACOS(CUW)-DACOS(CZW))
            QT=QT+PIC%component(NT)*C0
            PIC%component(NT)=PIC%component(NT)*RTODEG
          end if

        IF(PIC%atom1(NT).LE.0.or.PIC%atom1(NT).EQ.999)ERROR=.true.
        IF(PIC%atom2(NT).LE.0.or.PIC%atom2(NT).EQ.999)ERROR=.true.
        IF(IC.NE.2)then
          IF(PIC%atom3(NT).LE.0.or.PIC%atom3(NT).EQ.999)ERROR=.true.
          IF(IC.NE.3)then
            IF(PIC%atom4(NT).LE.0.or.PIC%atom4(NT).EQ.999)ERROR=.true.
          end if
        end if

        end do ! Icomponent
        C1=DSQRT(ONE/C1)*CCC
        PIC%value(I)=QT*C1

      end do !I
!
! Error.
      if(ERROR)then
        write(UNIout,'(A/16X,A,4I4)')'ERROR> BLD_PIC: illegal atom used to define a proper '// &
        'INTERNAL COORDINATE (I.E. DUMMY, FLOATING):','>',PIC%atom1(Icomponent),PIC%atom2(Icomponent), &
        PIC%atom3(Icomponent),PIC%atom4(Icomponent)
        stop 'ERROR> BLD_PIC: illegal atom used'
      end if
!
! End of routine BLD_PIC
      call PRG_manager ('exit', 'BLD_PIC', 'UTILITY')
      return
      end subroutine BLD_PIC
!
! END CONTAINS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine MENU_PIC
      subroutine PRT_PIC
!***********************************************************************
!     Date last modified: May 10, 1995                     Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE proper_internals

      implicit none
!
! Local scalars:
      integer I,Icomponent,IC,J,Ktype,NC
      double precision A180,CCC,QQI,RTODEG
      character type(7)*13,TYP*16
!
      parameter (A180=180.0D0,RTODEG=A180/PI_VAL)
!
      DATA type/'STRETCHING   ','INVERSE R    ','BENDING      ','OPL BENDING  ','TORSION      ','LINEAR Bend 1', &
                'LINEAR Bend 2'/
!
! Begin:
      call PRG_manager ('enter', 'PRT_PIC', 'UTILITY')
!
      write(UNIout,'(a)')'PROPER INTERNAL COORDINATES:'
      write(UNIout,2000)
 2000 FORMAT(/1X,80('-')/2X,'NO.',1X,'type',12X,'SCALE',2X,'COEFFICIENT',2X,'PARAMETER', &
       2X,'COMPONENT',5X,'ATOMS'/1X,80('-'))

      Icomponent=0
      do I=1,PIC%Ncoordinates
        IC=4
        Ktype=PIC%type(I)
        IF(Ktype.EQ.1)IC=2
        IF(Ktype.EQ.3)IC=3
        TYP=type(Ktype)
        NC=PIC%Ncomponents(I)
        CCC=PIC%scale(I)
        QQI=PIC%value(I)
        IF(Ktype.GT.1)QQI=QQI*RTODEG
        Icomponent=Icomponent+1
        IF(IC.EQ.2)then
          write(UNIout,1062)I,TYP,CCC,PIC%coeff(Icomponent),QQI,PIC%component(Icomponent), &
                                      PIC%atom1(Icomponent),PIC%atom2(Icomponent)
        else IF(IC.EQ.3)then
          write(UNIout,1060)I,TYP,CCC,PIC%coeff(Icomponent),QQI,PIC%component(Icomponent), &
                                      PIC%atom1(Icomponent),PIC%atom2(Icomponent),PIC%atom3(Icomponent)
        else IF(IC.EQ.4)then
          write(UNIout,1060)I,TYP,CCC,PIC%coeff(Icomponent),QQI,PIC%component(Icomponent), &
                                      PIC%atom1(Icomponent),PIC%atom2(Icomponent),PIC%atom3(Icomponent), &
                                      PIC%atom4(Icomponent)
        end if
!
        IF(NC.NE.1)then
          do J=2,NC
            Icomponent=Icomponent+1
            IF(IC.EQ.2)then
              write(UNIout,1072)PIC%coeff(Icomponent),PIC%component(Icomponent),PIC%atom1(Icomponent), &
                                PIC%atom2(Icomponent)
            else IF(IC.EQ.3)then
              write(UNIout,1070)PIC%coeff(Icomponent),PIC%component(Icomponent),PIC%atom1(Icomponent), &
                                PIC%atom2(Icomponent),PIC%atom3(Icomponent)
            else IF(IC.EQ.4)then
              write(UNIout,1070)PIC%coeff(Icomponent),PIC%component(Icomponent),PIC%atom1(Icomponent), &
                                PIC%atom2(Icomponent),PIC%atom3(Icomponent),PIC%atom4(Icomponent)
            end if
!
          end do !J
        end if
      end do !I
      write(UNIout,1080)
!
! End of routine PRT_PIC
      call PRG_manager ('exit', 'PRT_PIC', 'UTILITY')
      return
!
 1060 FORMAT(1X,I3,2X,A13,1X,F9.5,2X,F9.5,2X,F9.4,2X,F9.4,2X,4I4)
 1062 FORMAT(1X,I3,2X,A13,1X,F9.5,2X,F9.5,2X,F9.6,2X,F9.6,2X,4I4)
 1070 FORMAT(31X,F9.5,13X,F9.4,2X,4I4)
 1072 FORMAT(31X,F9.5,13X,F9.6,2X,4I4)
 1080 FORMAT(1X,80('-'))
      end subroutine PRT_PIC
