      subroutine MENU_trig
!***********************************************************************
!     Date last modified: August 26, 1999                              *
!     Author: R. Poirier                                               *
!     Description: Trigonometry                                        *
!***********************************************************************
! Modules:
      USE program_constants
      USE program_parser
      USE MENU_gets
      USE type_molecule

      implicit none
!
! Local array:
      integer ATMNUM(4)
      character*4 atmlst(4)
!
! Local scalars:
      integer I,J,nlist
      double precision value
      logical done
!
! Local functions:
      double precision GET_angle,GET_torsion,torsion_in,MM_angle
!
! Local parameter:
      integer nbbyte
      parameter (nbbyte=4) ! must be the same as atmlst
!
! Begin:
      call PRG_manager ('enter', 'MENU_trig', 'UTILITY')
!
! Defaults:
      done=.false.
!
! Menu:
      do while (.not.done)
      call new_token (' TRIGonometry:')
!
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      ' Command TRIGonometry:', &
      '   Purpose: Perform trigonometry Calculations ', &
      '   Syntax :', &
      '   ANgle=<list>, i.e., (C1 C2 C3) or (1 2 4)', &
      '   TOrsion=<list>, i.e., (C1 C2 C3 C4) or (1 2 4 7)', &
      '   end'
!
! Angles
      else if(token(1:2).EQ.'AN')then
        nlist=3
        atmlst(1:4)='    '
        call GET_Clist (atmlst, nbbyte, 4, nlist)
        ATMNUM(1)=0
        ATMNUM(2)=0
        ATMNUM(3)=0
!
      do I=1,nlist
      if(CHK_string(atmlst(I),'+-0123456789'))then
        call CNV_StoI (atmlst(I), ATMNUM(I))
      else
        do j=1,Natoms
        if(CARTESIAN(J)%element(1:len_trim(CARTESIAN(J)%element)).eq.atmlst(I)(1:len_trim(atmlst(I))))then
          ATMNUM(I)=j
        end if
        end do ! J
      end if
      if(ATMNUM(I).le.0.or.ATMNUM(I).gt.Natoms)then
        write(UNIout,'(2a)')'ERROR MENU_trig> illegal atom in list (ANgle=( ? ? ? ))',atmlst(I)
        stop 'ERROR MENU_trig> illegal atom in list (ANgle=( ? ? ? ))'
      end if
      end do ! I
      value=GET_angle (ATMNUM(1), ATMNUM(2), ATMNUM(3)) ! in radians
      value=value/Deg_to_Radian
      write(UNIout,'(7a,F20.10)')'Angle: ',ATMLST(1),'-',ATMLST(2),'-',ATMLST(3),' = ',value
!
! Torsions
      else if(token(1:2).EQ.'TO')then
        nlist=4
        atmlst(1:4)='    '
        call GET_Clist (atmlst, nbbyte, 4, nlist)
        ATMNUM(1)=0
        ATMNUM(2)=0
        ATMNUM(3)=0
        ATMNUM(4)=0
!
      do I=1,nlist
      if(CHK_string(atmlst(I),'+-0123456789'))then
        call CNV_StoI (atmlst(I), ATMNUM(I))
      else
        do j=1,Natoms
        if(CARTESIAN(J)%element(1:len_trim(CARTESIAN(J)%element)).eq.atmlst(I)(1:len_trim(atmlst(I))))then
          ATMNUM(I)=j
        end if
        end do ! J
      end if
      if(ATMNUM(I).le.0.or.ATMNUM(I).gt.Natoms)then
        write(UNIout,'(1x,a,a)')'ERROR MENU_trig> illegal atom in list (TOrsion=( ? ? ? ? )) ',atmlst(I)
        stop 'ERROR MENU_trig> illegal atom in list (TOrsion=( ? ? ? ? ))'
      end if
      end do ! I
      value=GET_torsion (ATMNUM(1), ATMNUM(2), ATMNUM(3), ATMNUM(4))
      value=value/Deg_to_Radian
      write(UNIout,'(9a,F12.6)')'Torsion: ',ATMLST(1),'-',ATMLST(2),'-',ATMLST(3),'-',ATMLST(4),' = ',value
      else
        call MENU_end (done)
      end if
!
      end do !(.not.done)
!
! End of routine MENU_trig
      call PRG_manager ('exit', 'MENU_trig', 'UTILITY')
      return
      end
