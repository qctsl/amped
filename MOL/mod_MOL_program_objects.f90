      subroutine BLD_MOLECULE_objects
!****************************************************************************************************************
!       Date last modified June 23, 2000                                                                        *
!       Author: Darryl Reid and R.A. Poirier                                                                    *
!       Description:  Builds a complete list of all the objects which can be created in MUNgauss                *
!       which belong to the "class" MOLECULE                                                                    *
!****************************************************************************************************************
! Modules:
      USE program_objects

      implicit none

! Local scalar:
      integer :: Iobject
!
! Begin:
      OBJ_MOLECULE(1:Max_objects)%modality = 'NONE'
      OBJ_MOLECULE(1:Max_objects)%class = 'MOL'
      OBJ_MOLECULE(1:Max_objects)%depend = .true.
      NMOLobjects = 0
!
! COORDINATES
      NMOLobjects = NMOLobjects + 1
      OBJ_MOLECULE(NMOLobjects)%name = 'BMATRIX'
      OBJ_MOLECULE(NMOLobjects)%modality = 'ZM'
      OBJ_MOLECULE(NMOLobjects)%routine = 'BLD_Bmatrix_ZM'
!
      NMOLobjects = NMOLobjects + 1
      OBJ_MOLECULE(NMOLobjects)%name = 'BMATRIX'
      OBJ_MOLECULE(NMOLobjects)%modality = 'PIC'
      OBJ_MOLECULE(NMOLobjects)%routine = 'BLD_Bmatrix_PIC'
!
      NMOLobjects = NMOLobjects + 1
      OBJ_MOLECULE(NMOLobjects)%name = 'BMATRIX'
      OBJ_MOLECULE(NMOLobjects)%modality = 'RIC'
      OBJ_MOLECULE(NMOLobjects)%routine = 'BLD_Bmatrix_RIC'
!
      NMOLobjects = NMOLobjects + 1
      OBJ_MOLECULE(NMOLobjects)%name = 'INTERNAL'
      OBJ_MOLECULE(NMOLobjects)%modality = 'RIC'
      OBJ_MOLECULE(NMOLobjects)%routine = 'BLD_RIC'
!
      NMOLobjects = NMOLobjects + 1
      OBJ_MOLECULE(NMOLobjects)%name = 'INTERNAL'
      OBJ_MOLECULE(NMOLobjects)%modality = 'PIC'
      OBJ_MOLECULE(NMOLobjects)%routine = 'BLD_PIC_new'
!
      NMOLobjects = NMOLobjects + 1
      OBJ_MOLECULE(NMOLobjects)%name = 'INTERNAL'
      OBJ_MOLECULE(NMOLobjects)%modality = 'ZM'
      OBJ_MOLECULE(NMOLobjects)%routine = 'BLD_ZMatrix' ! Not available, should only print
!
      NMOLobjects = NMOLobjects + 1
      OBJ_MOLECULE(NMOLobjects)%name = 'INTERNAL'
      OBJ_MOLECULE(NMOLobjects)%modality = 'FZM'
      OBJ_MOLECULE(NMOLobjects)%routine = 'BLD_ZMatrix' ! Not available, should only print
!
      NMOLobjects = NMOLobjects + 1
      OBJ_MOLECULE(NMOLobjects)%name = 'INTERNAL'
      OBJ_MOLECULE(NMOLobjects)%modality = 'ZMATRIX'
      OBJ_MOLECULE(NMOLobjects)%routine = 'XYZ_to_Zmatrix' ! will build a Zmatrix
!
! GRAPH
      NMOLobjects = NMOLobjects + 1
      OBJ_MOLECULE(NMOLobjects)%name = 'GRAPH'
      OBJ_MOLECULE(NMOLobjects)%modality = 'ANGLE_ATOMS'
      OBJ_MOLECULE(NMOLobjects)%routine = 'GRAPH_MAKEANG'
!
      NMOLobjects = NMOLobjects + 1
      OBJ_MOLECULE(NMOLobjects)%name = 'GRAPH'
      OBJ_MOLECULE(NMOLobjects)%modality = 'BOND_ATOMS'
      OBJ_MOLECULE(NMOLobjects)%routine = 'GRAPH_MAKEBOND'
!
      NMOLobjects = NMOLobjects + 1
      OBJ_MOLECULE(NMOLobjects)%name = 'GRAPH'
      OBJ_MOLECULE(NMOLobjects)%modality = 'CLOSE_CONTACT'
      OBJ_MOLECULE(NMOLobjects)%routine = 'GRAPH_CLOSE_CONT'
!
      NMOLobjects = NMOLobjects + 1
      OBJ_MOLECULE(NMOLobjects)%name = 'GRAPH'
      OBJ_MOLECULE(NMOLobjects)%modality = 'COMCON'
      OBJ_MOLECULE(NMOLobjects)%routine = 'GRAPH_COMP_CONN'
!
      NMOLobjects = NMOLobjects + 1
      OBJ_MOLECULE(NMOLobjects)%name = 'GRAPH'
      OBJ_MOLECULE(NMOLobjects)%modality = 'CNCOMP'
      OBJ_MOLECULE(NMOLobjects)%routine = 'GRAPH_CNCOMP'
!
      NMOLobjects = NMOLobjects + 1
      OBJ_MOLECULE(NMOLobjects)%name = 'GRAPH'
      OBJ_MOLECULE(NMOLobjects)%modality = 'CONNECT'
      OBJ_MOLECULE(NMOLobjects)%routine = 'GRAPH_CONN_BS'
!
      NMOLobjects = NMOLobjects + 1
      OBJ_MOLECULE(NMOLobjects)%name = 'GRAPH'
      OBJ_MOLECULE(NMOLobjects)%modality = 'CONN_FAILSAFE'
      OBJ_MOLECULE(NMOLobjects)%routine = 'GRAPH_COMP_CONN_last'
!
      NMOLobjects = NMOLobjects + 1
      OBJ_MOLECULE(NMOLobjects)%name = 'GRAPH'
      OBJ_MOLECULE(NMOLobjects)%modality = 'CONN_PRUN'
      OBJ_MOLECULE(NMOLobjects)%routine = 'GRAPH_PRUNE'
!
      NMOLobjects = NMOLobjects + 1
      OBJ_MOLECULE(NMOLobjects)%name = 'GRAPH'
      OBJ_MOLECULE(NMOLobjects)%modality = 'CONN_PRUN_HOMRED_INCIDENCE'
      OBJ_MOLECULE(NMOLobjects)%routine = 'GRAPH_INCIDENCE'
!
      NMOLobjects = NMOLobjects + 1
      OBJ_MOLECULE(NMOLobjects)%name = 'GRAPH'
      OBJ_MOLECULE(NMOLobjects)%modality = 'CONVAL'
      OBJ_MOLECULE(NMOLobjects)%routine = 'GRAPH_VALENCE'
!
      NMOLobjects = NMOLobjects + 1
      OBJ_MOLECULE(NMOLobjects)%name = 'GRAPH'
      OBJ_MOLECULE(NMOLobjects)%modality = 'EDGE_RING_TO_RING_ASSEMBLY'
      OBJ_MOLECULE(NMOLobjects)%routine = 'GRAPH_RING_ASSEMBLY'
!
      NMOLobjects = NMOLobjects + 1
      OBJ_MOLECULE(NMOLobjects)%name = 'GRAPH'
      OBJ_MOLECULE(NMOLobjects)%modality = 'EDGE_TO_RING'
      OBJ_MOLECULE(NMOLobjects)%routine = 'GRAPH_EDGE_RINGASS'
!
      NMOLobjects = NMOLobjects + 1
      OBJ_MOLECULE(NMOLobjects)%name = 'GRAPH'
      OBJ_MOLECULE(NMOLobjects)%modality = 'FUNDRING'
      OBJ_MOLECULE(NMOLobjects)%routine = 'GRAPH_FUND_RINGS'
!
      NMOLobjects = NMOLobjects + 1
      OBJ_MOLECULE(NMOLobjects)%name = 'GRAPH'
      OBJ_MOLECULE(NMOLobjects)%modality = 'OOPBEND_ATOMS'
      OBJ_MOLECULE(NMOLobjects)%routine = 'MAKE_OOPBENDS'
!
      NMOLobjects = NMOLobjects + 1
      OBJ_MOLECULE(NMOLobjects)%name = 'GRAPH'
      OBJ_MOLECULE(NMOLobjects)%modality = 'PRUNE_HOMRED'
      OBJ_MOLECULE(NMOLobjects)%routine = 'GRAPH_HOMREDUCED'
!
      NMOLobjects = NMOLobjects + 1
      OBJ_MOLECULE(NMOLobjects)%name = 'GRAPH'
      OBJ_MOLECULE(NMOLobjects)%modality = 'TORSION_ATOMS'
      OBJ_MOLECULE(NMOLobjects)%routine = 'GRAPH_MAKETORS'
!
      NMOLobjects = NMOLobjects + 1
      OBJ_MOLECULE(NMOLobjects)%name = 'GRAPH'
      OBJ_MOLECULE(NMOLobjects)%modality = 'VERTEX_LIST'
      OBJ_MOLECULE(NMOLobjects)%routine = 'GRAPH_ATOM_BETWEEN'
!
      NMOLobjects = NMOLobjects + 1
      OBJ_MOLECULE(NMOLobjects)%name = 'GRAPH'
      OBJ_MOLECULE(NMOLobjects)%modality = 'VERY_WEAK_BONDS'
      OBJ_MOLECULE(NMOLobjects)%routine = 'GRAPH_VWEAK_BOND'
!
      NMOLobjects = NMOLobjects + 1
      OBJ_MOLECULE(NMOLobjects)%name = 'GRAPH'
      OBJ_MOLECULE(NMOLobjects)%modality = 'VAN_DER_WAAL_BONDS'
      OBJ_MOLECULE(NMOLobjects)%routine = 'GRAPH_VderW_BOND'
!
      NMOLobjects = NMOLobjects + 1
      OBJ_MOLECULE(NMOLobjects)%name = 'GRAPH'
      OBJ_MOLECULE(NMOLobjects)%modality = 'NON_BONDED'
      OBJ_MOLECULE(NMOLobjects)%routine = 'CONN_NBOND'
!
      NMOLobjects = NMOLobjects + 1
      OBJ_MOLECULE(NMOLobjects)%name = 'GRAPH'
      OBJ_MOLECULE(NMOLobjects)%modality = 'NON_BONDED_ATOMS'
      OBJ_MOLECULE(NMOLobjects)%routine = 'GRAPH_NO_BONDS'
!
! MOLECULE
      NMOLobjects = NMOLobjects + 1
      OBJ_MOLECULE(NMOLobjects)%name = 'ATOMIC'
      OBJ_MOLECULE(NMOLobjects)%modality = 'DISTANCES'
      OBJ_MOLECULE(NMOLobjects)%routine = 'ATOM_DISTANCES'
!
      NMOLobjects = NMOLobjects + 1
      OBJ_MOLECULE(NMOLobjects)%name = 'GRADIENTS'
      OBJ_MOLECULE(NMOLobjects)%modality = 'COMPONENTS'
      OBJ_MOLECULE(NMOLobjects)%routine = 'GRADIENTS_COMPONENTS'
!
      NMOLobjects = NMOLobjects + 1
      OBJ_MOLECULE(NMOLobjects)%name = 'GRADIENTS'
      OBJ_MOLECULE(NMOLobjects)%modality = 'XYZ'
      OBJ_MOLECULE(NMOLobjects)%routine = 'GRADIENTS_XYZ'
!
      NMOLobjects = NMOLobjects + 1
      OBJ_MOLECULE(NMOLobjects)%name = 'GRADIENTS'
      OBJ_MOLECULE(NMOLobjects)%modality = 'ZM'
      OBJ_MOLECULE(NMOLobjects)%routine = 'GRADIENTS_ZM'
!
      NMOLobjects = NMOLobjects + 1
      OBJ_MOLECULE(NMOLobjects)%name = 'GRADIENTS'
      OBJ_MOLECULE(NMOLobjects)%modality = 'PIC'
      OBJ_MOLECULE(NMOLobjects)%routine = 'GRADIENTS_PIC'
!
      NMOLobjects = NMOLobjects + 1
      OBJ_MOLECULE(NMOLobjects)%name = 'GRADIENTS'
      OBJ_MOLECULE(NMOLobjects)%modality = 'RIC'
      OBJ_MOLECULE(NMOLobjects)%routine = 'GRADIENTS_RIC'
!
      NMOLobjects = NMOLobjects + 1
      OBJ_MOLECULE(NMOLobjects)%name = 'SHAPE'
      OBJ_MOLECULE(NMOLobjects)%modality = 'NUCLEAR'
      OBJ_MOLECULE(NMOLobjects)%routine = 'OEP_nuclear'
!
      NMOLobjects = NMOLobjects + 1
      OBJ_MOLECULE(NMOLobjects)%name = 'SIMILARITY'
      OBJ_MOLECULE(NMOLobjects)%modality = 'ALL'
      OBJ_MOLECULE(NMOLobjects)%routine = 'MOL_similarity'
!
      NMOLobjects = NMOLobjects + 1
      OBJ_MOLECULE(NMOLobjects)%name = 'MOMENTS_OF_INERTIA'
      OBJ_MOLECULE(NMOLobjects)%modality = 'ALL'
      OBJ_MOLECULE(NMOLobjects)%routine = 'GET_MOMENTS_OF_INERTIA'
!
      NMOLobjects = NMOLobjects + 1
      OBJ_MOLECULE(NMOLobjects)%name = 'FRAGMENTS'
      OBJ_MOLECULE(NMOLobjects)%modality = 'POP'
      OBJ_MOLECULE(NMOLobjects)%routine = 'BLD_FRAGMENTS_POP'
!
      NMOLobjects = NMOLobjects + 1
      OBJ_MOLECULE(NMOLobjects)%name = 'EL_BY_FRAGMENTS'
      OBJ_MOLECULE(NMOLobjects)%modality = '   '
      OBJ_MOLECULE(NMOLobjects)%routine = 'BLD_ELbyFragments'
!
      NMOLobjects = NMOLobjects + 1
      OBJ_MOLECULE(NMOLobjects)%name = 'AO_TO_FRAGMENTS'
      OBJ_MOLECULE(NMOLobjects)%modality = '   '
      OBJ_MOLECULE(NMOLobjects)%routine = 'BLD_AOtoFragments'
!
! Dummy
      NMOLobjects = NMOLobjects + 1
      OBJ_MOLECULE(NMOLobjects)%name = '?'
      OBJ_MOLECULE(NMOLobjects)%modality = '?'
      OBJ_MOLECULE(NMOLobjects)%routine = '?'
!
      do Iobject=1,NMOLobjects
        OBJ_MOLECULE(Iobject)%exist=.false.
        OBJ_MOLECULE(Iobject)%Current=.false.
      end do

      return
      end subroutine BLD_MOLECULE_objects
