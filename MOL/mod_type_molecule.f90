      MODULE type_molecule
!*******************************************************************************
!     Date last modified: January 17, 2017                                     *
!     Author: R.A. Poirier                                                     *
!     Description: Molecule.                                                   *
!*******************************************************************************
! Modules

      implicit none
!
! MAXimum values:
      integer, parameter :: MAX_Uname=132 ! Maximum length for Unit name
      integer, parameter :: MAX_Iname=132 ! Maximum length for Input coordinate name
      integer, parameter :: MAX_Mname=132 ! Maximum length for Molecule name
      integer, parameter :: MAX_title=132 ! Maximum length for title
      integer, parameter :: MAX_formula=132 ! Maximum length for formula
      integer, parameter :: MAX_label=16 ! Maximum length for label (Bonds, Angles, Torsions)
      integer, parameter :: MAX_AIM=1000 ! Maximum number of AIM in AIMlist
      integer :: NAIMprint

      integer :: N_molecules
!
      type atom_definition
        integer :: Atomic_number
        integer :: Isotope
        character(len=8) :: symbol
        character(len=8) :: name
        character(len=4) :: belongsto
        character(len=5) :: type
        double precision :: mass
        double precision :: X
        double precision :: Y
        double precision :: Z
      end type atom_definition

      type (atom_definition), dimension(:), pointer :: MOL_atoms => null()
      type (atom_definition), dimension(:), allocatable, target :: MOL_atomsA
      type (atom_definition), dimension(:), allocatable, target :: MOL_atomsB
!
      type molecule_definition
        integer :: Natoms
        integer :: charge ! Charge on the molecule
        character(len=64) :: name
        character(len=MAX_formula) :: formula
        character(len=8) :: point_group
        double precision :: mass
!       type (atom_definition), dimension(:), pointer :: atom => null()
      end type molecule_definition

      type (molecule_definition), pointer :: molecule => null()
      type (molecule_definition), target ::  moleculeA
      type (molecule_definition), target ::  moleculeB
!
      type cartesian_coordinates
        integer ::  Atomic_number
        double precision :: X
        double precision :: Y
        double precision :: Z
        character(len=8) :: ELEMENT
      end type cartesian_coordinates

      type (cartesian_coordinates), dimension(:), pointer :: CARTESIAN => null()
      type (cartesian_coordinates), dimension(:), allocatable, target :: CartesianA
      type (cartesian_coordinates), dimension(:), allocatable, target :: CartesianB
      double precision, allocatable :: XYZSTD(:,:)

!
      integer :: Natoms ! Number of atoms
      integer :: NReal_atoms ! Number of real atoms (Natoms-NDummies)
      integer :: NDummies ! Number of dummy atoms
      integer :: Nelectrons ! Number of electrons
      integer :: NAelectrons ! Number of alpha electrons
      integer :: NBelectrons ! Number of beta electrons
      integer :: Multiplicity ! Multiplicity
      character(len=MAX_Uname) :: MOLUNI ! Units (i.e., A.U., Angstroms)
      character(len=MAX_Iname) :: MOLINP ! Input (i.e., cartesians, Z-matrix, ...)
      character(len=MAX_title) :: MOL_TITLE ! Title of molecule
      character(len=MAX_title) :: MOL_TitleA ! Title of molecule
      character(len=MAX_title) :: MOL_TitleB ! Title of molecule
      character(len=1) :: MOL_NAME ! Name of molecule (A, B, ...)
      character(len=3) :: MOL_type ! GS or TS (used for archiving)
!
      type coordinate_atoms
        integer :: atom1
        integer :: atom2
        integer :: atom3
        integer :: atom4
      end type coordinate_atoms

      type coordinate_bonds
        integer :: map        ! map from internal coordinates (i.e., Zmatrix) to coordinate_optimize
        integer :: type      ! Bond type, i.e., 1 for single, 2 for aromatic, ...
        double precision :: length
        character(len=MAX_label) :: label
      end type coordinate_bonds

      type coordinate_angles
        integer :: map        ! map from internal coordinates (i.e., Zmatrix) to coordinate_optimize
        integer :: type      !
        double precision :: value
        character(len=MAX_label) :: label
      end type coordinate_angles

      type coordinate_torsions
        integer :: map        ! map from internal coordinates (i.e., Zmatrix) to coordinate_optimize
        integer :: type      !
        double precision :: value
        character(len=MAX_label) :: label
        integer :: euler
      end type coordinate_torsions

      type coordinate_optimize
        integer :: type       ! 1, 2, 3, ... bond, angle, torsion
        logical :: opt        ! true or false (fixed)
        double precision :: value
        character(len=MAX_label) :: name
      end type coordinate_optimize

! Z-Matrix coordinates
      type Zmatrix_coordinates
        integer :: Natoms ! Number of atoms
        type (coordinate_atoms), dimension(:), pointer :: atoms => null()
        integer :: Nbonds ! Number of bonds
        type (coordinate_bonds), dimension(:), pointer :: bond => null()
        integer :: Nangles ! Number of angles
        type (coordinate_angles), dimension(:), pointer :: angle => null()
        integer :: Ntorsions ! Number of torsions
        type (coordinate_torsions), dimension(:), pointer :: torsion => null()
        integer :: Ndefines ! Number of ZM variables (Define)
        type (coordinate_optimize), dimension(:), pointer :: define => null()
        character(len=16) :: units
      end type Zmatrix_coordinates

      type (Zmatrix_coordinates) Zmatrix

! Z-matrix variables (from the define): depends on the number of z-matrix variables (NZMVAR)
      integer :: NZM_fixed ! Number of fixed ZM parameters
      integer :: NMCOOR ! Number of molecule coordinates
      logical :: ZM_constraints ! Apply constraints if .true. else ignore them
!
! Other molecular information
      integer :: NICOOR   ! Number of internal coordinates = NPICOR, NMCOOR, ... depending on the coordiantes used
      character*8, dimension(:), allocatable :: MOLEID ! Molecule atom belongs to
      double precision, dimension(:,:), allocatable :: DISMAT ! distance matrix

      double precision, dimension(:,:), allocatable, save, target :: Bmatrix_ZM
      double precision, dimension(:,:), allocatable, save, target :: Bmatrix_PIC
      double precision, dimension(:,:), allocatable, save, target :: Bmatrix_RIC
      double precision, dimension(:,:), save, pointer :: Bmatrix => null()
!
      double precision, dimension(:), allocatable :: Anuclear ! Atomic nuclear repulsions
!
! Types related to defining internal coordinates and the molecular graph.
      type coordinate_bond
        integer :: atom1      ! atom one of bond
        integer :: atom2      ! atom two of bond
        integer :: constraints ! constraints, i.e., symmetry constraints
        integer :: map        ! map from internal coordinates (i.e., Zmatrix) to coordinate_optimize
        integer :: type      ! Bond type, i.e., 1 for single, 2 for aromatic, ...
        double precision :: value ! bond length
        character(len=16) :: label ! i.e., C1H1
        character(len=16) :: units
      end type coordinate_bond

      type coordinate_angle
        integer :: atom1      ! atom one of angle
        integer :: atom2      ! atom two of angle
        integer :: atom3      ! atom three of angle
        integer :: constraints ! constraints, i.e., symmetry constraints
        integer :: map        ! map from internal coordinates (i.e., Zmatrix) to coordinate_optimize
        integer :: type      !
        double precision :: value ! of bond angle
        character(len=16) :: label ! i.e., alpha
        character(len=16) :: units
      end type coordinate_angle

      type coordinate_torsion
        integer :: atom1      ! atom one of torsion
        integer :: atom2      ! atom two of torsion
        integer :: atom3      ! atom three of torsion
        integer :: atom4      ! atom four of torsion
        integer :: constraints ! constraints, i.e., symmetry constraints
        integer :: map        ! map from internal coordinates (i.e., Zmatrix) to coordinate_optimize
        integer :: type      !
        integer :: euler     !
        double precision :: value ! of torsion
        logical :: opt        ! true or false (fixed)
        character(len=16) :: label ! i.e., DH1
        character(len=16) :: units
      end type coordinate_torsion

      type coordinate_oopbend ! out-of-plane bend
        integer :: atom1      ! atom one of oopdend
        integer :: atom2      ! atom two of oopdend
        integer :: atom3      ! atom three of oopdend
        integer :: atom4      ! atom four of oopdend
        integer :: constraints ! constraints, i.e., symmetry constraints
        integer :: map        ! map from internal coordinates (i.e., Zmatrix) to coordinate_optimize
        integer :: type      !
        integer :: euler
        double precision :: value ! out-of-plane bend
        logical :: opt        ! true or false (fixed)
        character(len=16) :: label  ! i.e., alpha
        character(len=16) :: units
      end type coordinate_oopbend

      type coordinate_nonbonded
        integer :: atom1      ! atom one of bond
        integer :: atom2      ! atom two of bond
        integer :: constraints ! constraints, i.e., symmetry constraints
        integer :: map        ! map from internal coordinates (i.e., Zmatrix) to coordinate_optimize
        integer :: type       ! Bond type, i.e., 1 for single, 2 for aromatic, ...
        double precision :: value ! nonbonded distance
        logical :: opt        ! true or false (fixed)
        character(len=16) :: label  ! i.e., C1C2
        character(len=16) :: units
      end type coordinate_nonbonded
!
! Internal Coordinates
      type internal_coordinates
        integer :: NIcoordinates
        integer :: Nbonds
        integer :: Nangles
        integer :: Ntorsions
        integer :: Noopbends
        integer :: Nnonbonded
        type (coordinate_bond), dimension(:), pointer :: bond => null()
        type (coordinate_angle), dimension(:), pointer :: angle => null()
        type (coordinate_torsion), dimension(:), pointer :: torsion => null()
        type (coordinate_oopbend), dimension(:), pointer :: oopbend => null()
        type (coordinate_nonbonded), dimension(:), pointer :: nonbonded => null()
      end type internal_coordinates

! Origin invariant nuclear second moment
      double precision :: OI_XX
      double precision :: OI_YY
      double precision :: OI_ZZ
!
! Moments of Inertia
      double precision :: MomIx
      double precision :: MomIy
      double precision :: MomIz
!
! Defining internal coordinates
      type (internal_coordinates) RIC ! 2 definitions?
!     type (internal_coordinates) PIC ! 2 definitions!
      type (internal_coordinates) ZMatrix_coord

      integer, dimension(:) :: AIMprint(Max_AIM)

      end MODULE type_molecule
