      subroutine GET_MOL_object (class, Objname, Modality)
!**************************************************************************
!     Date last modified: June 21, 2001                       Version 2.0 *
!     Author: R.A. Poirier                                                *
!     Description: Objects belonging to the class MOLECULE                *
!**************************************************************************
! Modules:
      USE program_files
      USE program_objects

      implicit none
!
! Input scalars:
      character*(*) :: class
      character*(*) :: Objname
      character*(*) :: Modality
!
! Local scalars:
      integer :: Object_number
!
! Begin:
      call PRG_manager ('enter', 'GET_MOL_object', 'UTILITY')
!
      call GET_object_number (OBJ_MOLECULE, NMOLobjects, FirstMOL, Objname, Modality, Object_number)

      if(OBJ_MOLECULE(Object_number)%current)then
        call PRG_manager ('exit', 'GET_MOL_object', 'UTILITY')
        return
      end if

      OBJ_MOLECULE(Object_number)%exist=.true.
      OBJ_MOLECULE(Object_number)%Current=.true.
      Object(ObjNum)%exist=.true.
      Object(ObjNum)%Current=.true.

      select_Object: select case (Objname)
      case ('ATOMIC') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      select case (Modality)
      case ('DISTANCES')
        call ATOM_DISTANCES
      case default
        write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                       '" for Modality "',Modality(1:len_trim(Modality)),'"'
        stop 'No such object'
      end select

      case ('MOLECULE') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      select case (Modality)
      case ('MASS')
        call GET_masses
      case default
        write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                       '" for Modality "',Modality(1:len_trim(Modality)),'"'
        stop 'No such object'
      end select

      case ('SIMILARITY') ! Ignore modality for now
        call MOL_similarity

      case ('MOMENTS_OF_INERTIA') ! Ignore modality for now
        call GET_Moments_of_Inertia

      case ('BMATRIX') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      select case (Modality)
      case ('ZM')
        call BLD_Bmatrix_ZM
      case ('PIC')
        call BLD_Bmatrix_PIC
      case ('RIC')
        call BLD_Bmatrix_RIC
      case default
        write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                       '" for Modality "',Modality(1:len_trim(Modality)),'"'
        stop 'No such object'
      end select

      case ('GRADIENTS') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      select case (Modality)
      case ('ZM')
        call GRADIENTS_ZM
      case ('PIC')
        call GRADIENTS_PIC
      case ('RIC')
        call GRADIENTS_RIC
      case ('XYZ')
        call GRADIENTS_XYZ
      case ('COMPONENTS')
        call GRADIENTS_COMPONENTS
      case default
        write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                       '" for Modality "',Modality(1:len_trim(Modality)),'"'
        stop 'No such object'
      end select

      case ('INTERNAL') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      select case (Modality)
      case ('FZM')
        ! Do nothing, the ZMatrix will be printed assuming it exist
      case ('ZM')
        ! Do nothing, the ZMatrix will be printed assuming it exist
!     case ('PIC')
!       call BLD_PIC ! Cannot build PIC from scratch
      case ('RIC')
        call BLD_RIC
      case ('ZMATRIX')
        call XYZ_to_ZMATRIX
      case default
        write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                       '" for Modality "',Modality(1:len_trim(Modality)),'"'
        stop 'No such object'
      end select

      case ('GRAPH') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      select case (Modality)
      case ('CONNECT')
        call GRAPH_CONN_BS
      case ('CONVAL')
        call GRAPH_VALENCE
      case ('CNCOMP')
        call GRAPH_CNCOMP
      case ('BOND_ATOMS')
        call GRAPH_MAKEBOND
      case ('ANGLE_ATOMS')
        call GRAPH_MAKEANG
      case ('TORSION_ATOMS')
        call GRAPH_MAKETORS
      case ('CLOSE_CONTACT')
        call GRAPH_CLOSE_CONT
      case ('COMCON')
        call GRAPH_COMP_CONN
      case ('CONN_FAILSAFE')
        call GRAPH_COMP_CONN_last
      case ('VERY_WEAK_BONDS')
        call GRAPH_VWEAK_BOND
      case ('VAN_DER_WAAL_BONDS')
        call GRAPH_VderW_BOND
      case ('CONN_PRUN')
        call GRAPH_PRUNE
      case ('PRUNE_HOMRED')
        call GRAPH_HOMREDUCED
      case ('CONN_PRUN_HOMRED_INCIDENCE')
        call GRAPH_INCIDENCE
      case ('FUNDRING')
        call GRAPH_FUND_RINGS
      case ('EDGE_RING_TO_RING_ASSEMBLY')
        call GRAPH_RING_ASSEMBLY
      case ('VERTEX_LIST')
        call GRAPH_ATOM_BETWEEN
      case ('EDGE_TO_RING')
        call GRAPH_EDGE_RINGASS
      case ('OOPBEND_ATOMS')
        call MAKE_OOPBENDS
      case ('NON_BONDED')
        call CONN_NBOND
      case ('NON_BONDED_ATOMS')
        call GRAPH_NO_BONDS
      case default
        write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                       '" for Modality "',Modality(1:len_trim(Modality)),'"'
        stop 'No such object'
      end select

      case ('SHAPE')
        call OEP_Nuclear

      case ('FRAGMENTS') ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        if(Modality(1:3).eq.'POP')then
          call BLD_fragments_pop
          call PRT_Fragments
        else
          write(UNIout,'(a)')'ERROR> MOLECULE objects'
          call PRG_stop ('For object FRAGMENTS must provide a Modality (POP)')
        end if

      case ('AO_TO_FRAGMENTS')
        call BLD_AOtoFragment

      case ('EL_BY_FRAGMENTS')
        call BLD_ELbyFragment

      case default
        write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                       '" for Modality "',Modality(1:len_trim(Modality)),'"'
        stop 'No such object'
      end select select_Object

      call PRG_manager ('exit', 'GET_MOL_object', 'UTILITY')
      return
      end subroutine GET_MOL_object
