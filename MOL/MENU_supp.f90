      subroutine MENU_SUPP
!************************************************************************
!     Date last modified: June 24, 1992                                 *
!     Author: R. A. Poirier                                             *
!     Description: Read an archive and generates the supplementary data *
!     NOTE: The input is case sensitive                                 *
!************************************************************************
! Modules:
      USE program_parser
      USE menu_gets

      implicit none
!
! Local scalars:
      integer :: Ipos,lenstr
      logical done,lrun,LendXYZ,LZmatrix
      character(len=128) line
!
! Begin:
      call PRG_manager ('enter', 'MENU_SUPP', 'UTILITY')
      done=.false.
      lrun=.false.
      LZmatrix=.false.
!
! Menu:
      do while (.not.done)
      read(unitIN,'(a)')Line
      lenstr=len_trim(line)
!
      if(index(line, 'HELP').ne.0)then
         write(UNIout,'(a)') &
      ' Command SUPPlementary:', &
      '  Purpose: Create supplementary data file', &
      '  Syntax:', &
      '  SUPPlementary', &
      '  start: <string>: start ARCHIVE line...', &
      '  end'
!
      else if(index(line,'START').ne.0)then
      else if(index(line,'MOLECULE').ne.0)then
      else if(index(line,'Title = ').ne.0)then
        Ipos=index(line,'"')+1
        write(UNIout,'(/a)')line(Ipos:lenstr-1)
      else if(index(line,'Z-Matrix').ne.0)then
        LZmatrix=.true.
      else if(index(line,'END ! Z-Matrix').ne.0)then
      else if(index(line,'DEFINE !').ne.0)then
        write(UNIout,'(2a)')'Basis set: ',line(10:lenstr)
      else if(index(line,'END !DEFINE').ne.0)then
      else if(index(line,'Nuclear').ne.0)then
        Ipos=index(line, 'Optimization')-1
        write(UNIout,'(/a)')line(3:Ipos)
      else if(index(line,'FORMULA').ne.0)then
        write(UNIout,'(a)')line(3:lenstr)
      else if(index(line,'END ARCHIVE').ne.0)then
      else if(index(line,'NATOMS').ne.0)then
          LendXYZ=.false.
          read(unitIN,'(a)')Line
          if(.not.LZmatrix)write(UNIout,'(a)')'Cartesian:'
        do while (.not.LendXYZ)
          read(unitIN,'(a)')Line
          lenstr=len_trim(line)
          if(index(line,'END ARCHIVE').ne.0)then
            LendXYZ=.true.
            LZmatrix=.false.
            exit
          end if
          if(.not.LZmatrix.and..not.LendXYZ)write(UNIout,'(a)')line(1:lenstr)
        end do ! while
      else if(index(line,'END').ne.0)then
        done=.true.
      else if(index(line,'end').ne.0)then
        done=.true.
      else
        write(UNIout,'(a)')line(1:lenstr)
      end if

      end do !(.not.done)
!
! End of routine MENU_SUPP
      call PRG_manager ('exit', 'MENU_SUPP', 'UTILITY')
      return
      END
