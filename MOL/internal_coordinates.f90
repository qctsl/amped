      subroutine GRAPH_MAKEBOND
!***********************************************************************
!     Date last modified: April 16, 1996                   Version 1.0 *
!     Author: Cory C. Pye                                              *
!     Description: Generates bond list from connectivity matrix. If    *
!                  no bonds exist, then puts object of length 1.       *
!***********************************************************************
! Modules:
      USE GRAPH_objects
      USE type_molecule

      implicit none
!
! Local work arrays:
      integer, dimension(:), allocatable :: ATOM1
      integer, dimension(:), allocatable :: ATOM2
!
! Local scalars:
      integer I,J
!
! Begin:
      call PRG_manager ('enter', 'GRAPH_MAKEBOND', 'GRAPH%BOND_ATOMS')
!
      call GET_object ('MOL', 'GRAPH', 'CONNECT')
!
      allocate (ATOM1(1:Natoms*(Natoms-1)/2), ATOM2(1:Natoms*(Natoms-1)/2))

      NGBonds=0
      do I=1,Natoms
        do J=I+1,Natoms
          IF(CONNECT(I,J).EQ.1)then
            NGBonds=NGBonds+1
            ATOM1(NGBonds)=I
            ATOM2(NGBonds)=J
          end if !
        end do ! J
      end do ! I
!
! If there are no bonds, then zero first element
      IF(NGBonds.EQ.0)then
        ATOM1(1)=0
        ATOM2(1)=0
      end if !

      if(.not.allocated(BOND))then
        allocate (BOND(1:max0(1,NGBonds)))
      else
        deallocate (BOND)
        allocate (BOND(1:max0(1,NGBonds)))
      end if

      do I=1,NGBonds
        BOND(I)%ATOM1=ATOM1(I)
        BOND(I)%ATOM2=ATOM2(I)
        BOND(I)%type=0          ! initialize to single bonds for now
        BOND(I)%ring=0
        BOND(I)%ring_size=0
      end do ! ! I

!     call GRAPH_BOND_type ! Aisha's code

      deallocate (ATOM1, ATOM2)
!
! End of routine GRAPH_MAKEBOND
      call PRG_manager ('exit', 'GRAPH_MAKEBOND', 'GRAPH%BOND_ATOMS')
      return
      end subroutine GRAPH_MAKEBOND
      subroutine GRAPH_MAKEANG
!***********************************************************************
!     Date last modified: April 16, 1996                   Version 1.0 *
!     Author: Cory C. Pye                                              *
!     Description: Determines angle list from connectivity matrix. If  *
!                  no angles exist, then puts object of length 1.      *
!***********************************************************************
! Modules:
      USE GRAPH_objects
      USE type_molecule

      implicit none
!
! Local work arrays:
      integer, dimension(:), allocatable :: ATOM1
      integer, dimension(:), allocatable :: ATOM2
      integer, dimension(:), allocatable :: ATOM3
!
! Local scalars:
      integer Iangle,Iatom,Jatom,Katom,Iring,Jring,Kring
!
! Begin:
      call PRG_manager ('enter', 'GRAPH_MAKEANG', 'GRAPH%ANGLE_ATOMS')
!
      call GET_object ('MOL', 'GRAPH', 'CONNECT')
!
      allocate (ATOM1(1:Natoms*(Natoms-1)/2), ATOM2(1:Natoms*(Natoms-1)/2), ATOM3(1:Natoms*(Natoms-1)/2))

      NGANGLE=0
!
! We only need to consider J > I, since angles J-K-I == I-K-J. However the
! central atom can be anything. If I=K or I=J, the connectivity ensures that
! the condition is false.
      do Iatom=1,Natoms
        do Jatom=Iatom+1,Natoms
          do Katom=1,Natoms
!           if(Iatom.eq.Katom)cycle
!           if(Jatom.eq.Katom)cycle
            if(CONNECT(Iatom,Katom).eq.1.and.CONNECT(Katom,Jatom).eq.1)then
              NGANGLE=NGANGLE+1
              ATOM1(NGANGLE)=Iatom ! terminal atom
              ATOM2(NGANGLE)=Katom ! central atom
              ATOM3(NGANGLE)=Jatom ! terminal atom
            end if !
          end do !
        end do !
      end do !
!
      IF(NGANGLE.EQ.0)then
        ATOM1(1)=0
        ATOM2(1)=0
        ATOM3(1)=0
      end if !

      if(.not.allocated(ANGLE))then
        allocate (ANGLE(1:max0(1,NGANGLE)))
      else
        deallocate (ANGLE)
        allocate (ANGLE(1:max0(1,NGANGLE)))
      end if

! Save angles to type_angle and define ring size
      do Iangle=1,NGANGLE
        ANGLE(Iangle)%ATOM1=ATOM1(Iangle)
        ANGLE(Iangle)%ATOM2=ATOM2(Iangle)
        ANGLE(Iangle)%ATOM3=ATOM3(Iangle)
        ANGLE(Iangle)%ring_size=0
! NOTE obkect vertex is not available (NOT an object - built by GRAPH_ATOM_typeS)
!       Iring=vertex(ATOM1(Iangle))%ring
!       Jring=vertex(ATOM2(Iangle))%ring
!       Kring=vertex(ATOM3(Iangle))%ring
!       if(Iring.eq.Jring.and.Jring.eq.Kring)then ! all three atoms part of the same ring
!         ANGLE(Iangle)%ring_size=vertex(ATOM1(Iangle))%ring_size
!       end if
      end do ! ! Iangle

      deallocate (ATOM1, ATOM2, ATOM3)
!
! End of routine GRAPH_MAKEANG
      call PRG_manager ('exit', 'GRAPH_MAKEANG', 'GRAPH%ANGLE_ATOMS')
      return
      end subroutine GRAPH_MAKEANG
      subroutine GRAPH_MAKETORS
!***********************************************************************
!     Date last modified: April 17, 1996                   Version 1.0 *
!     Author: Cory C. Pye                                              *
!     Description: Determines torsion list from connectivity matrix.   *
!                  If no torsions exist, then puts object of length 1. *
!***********************************************************************
! Modules:
      USE GRAPH_objects
      USE type_molecule

      implicit none
!
! Local work arrays:
      integer, dimension(:), allocatable :: ATOM1
      integer, dimension(:), allocatable :: ATOM2
      integer, dimension(:), allocatable :: ATOM3
      integer, dimension(:), allocatable :: ATOM4
!
! Local scalars:
      integer I,J,K,B1,B2
!
! Begin:
      call PRG_manager ('enter', 'GRAPH_MAKETORS', 'GRAPH%TORSION_ATOMS')
!
      call GET_object ('MOL', 'GRAPH', 'CONNECT')
      call GET_object ('MOL', 'GRAPH', 'BOND_ATOMS')
!
      allocate (ATOM1(1:Natoms*(Natoms-1)/2), ATOM2(1:Natoms*(Natoms-1)/2), ATOM3(1:Natoms*(Natoms-1)/2), &
                ATOM4(1:Natoms*(Natoms-1)/2))

      NGTORSION=0
      do I=1, NGBonds
        B1=BOND(I)%ATOM1
        B2=BOND(I)%ATOM2
!
! Note: By construction, B2 > B1.
        do J=1,Natoms
!         if(J.eq.B1)cycle
! J must be connected to B1 and be different than B2
          if(CONNECT(J,B1).eq.1.and.J.ne.B2)then
            do K=1,Natoms
!             if(K.eq.B2)cycle
! K must be connected to B2 and be different than B1
              if(CONNECT(K,B2).eq.1.and.K.ne.B1)then
! K cannot be equal to J
                if(J.ne.K)then
                  NGTORSION=NGTORSION+1
                  ATOM1(NGTORSION)=J
                  ATOM2(NGTORSION)=B1
                  ATOM3(NGTORSION)=B2
                  ATOM4(NGTORSION)=K
                end if !
              end if !
            end do !
          end if !
        end do !
      end do !
!
      IF(NGTORSION.EQ.0)then
        ATOM1(1)=0
        ATOM2(1)=0
        ATOM3(1)=0
        ATOM4(1)=0
      end if !

      if(.not.allocated(TORSION))then
        allocate (TORSION(1:max0(1,NGTORSION)))
      else
        deallocate (TORSION)
        allocate (TORSION(1:max0(1,NGTORSION)))
      end if

      do I=1,NGTORSION
        TORSION(I)%ATOM1=ATOM1(I)
        TORSION(I)%ATOM2=ATOM2(I)
        TORSION(I)%ATOM3=ATOM3(I)
        TORSION(I)%ATOM4=ATOM4(I)
      end do ! ! I

      deallocate (ATOM1, ATOM2, ATOM3, ATOM4)
!
! End of routine GRAPH_MAKETORS
      call PRG_manager ('exit', 'GRAPH_MAKETORS', 'GRAPH%TORSION_ATOMS')
      return
      end subroutine GRAPH_MAKETORS
      subroutine GRAPH_CLOSE_CONT
!***********************************************************************
!     Date last modified: April 23, 1996                   Version 1.0 *
!     Author: Cory C. Pye                                              *
!     Description: If two atoms are close and geminal, then remove     *
!                  contact.                                            *
!***********************************************************************
! Modules:
      USE GRAPH_objects
      USE type_molecule
      USE type_elements
      USE matrix_print

      implicit none
!
! Work arrays:
      integer, dimension(:,:), allocatable :: CONNECTw
!
! Local scalars:
      integer I,J,K
!
! Begin:
      call PRG_manager ('enter', 'GRAPH_CLOSE_CONT', 'GRAPH%CLOSE_CONTACT')
!
      call GET_object ('MOL', 'GRAPH', 'CONNECT')
! Object:
      if(allocated(CONTACT))deallocate(CONTACT)
      allocate (CONNECTw(Natoms,Natoms))
      call GRAPH_CONN (vdW_RADII, CONNECTw, NELEMENTS, vdW_scalef)
      allocate (CONTACT(1:Natoms,1:Natoms))
      CONTACT=CONNECTw-CONNECT
      deallocate (CONNECTw)
!
      do I=1, Natoms
        do J=I+1,Natoms
          IF(CONTACT(I,J).EQ.1)then
            K=1
            do while(K.LE.Natoms)
!             if(I.eq.K)cycle
!             if(J.eq.K)cycle
              IF(CONNECT(I,K).EQ.1.and.CONNECT(K,J).EQ.1)then ! Atoms Iand J  are geminal
                CONTACT(I,J)=0
                CONTACT(J,I)=0
                K=Natoms+1
              else
                K=K+1
              end if !
            end do ! while
          end if !
        end do !
      end do !
!
! End of routine GRAPH_CLOSE_CONT
      call PRG_manager ('exit', 'GRAPH_CLOSE_CONT', 'GRAPH%CLOSE_CONTACT')
      return
      end subroutine GRAPH_CLOSE_CONT
      subroutine GRAPH_COMP_CONN
!***********************************************************************
!     Date last modified: May 14, 1996                     Version 1.0 *
!     Author: Cory C. Pye                                              *
!     Description: Calculates connectivity matrix of component         *
!                  (pseudo)graph from close contact matrix.            *
!***********************************************************************
! Modules:
      USE GRAPH_objects
      USE type_molecule

      implicit none
!
! Local scalars:
      integer I,J,ICOMP,JCOMP
!
! Begin:
      call PRG_manager ('enter', 'GRAPH_COMP_CONN', 'GRAPH%COMCON')
!
      call GET_object ('MOL', 'GRAPH', 'CNCOMP')
      call GET_object ('MOL', 'GRAPH', 'CLOSE_CONTACT')
!
! Object:
      if(.not.allocated(COMCON))then
        allocate (COMCON(1:NComponents,1:NComponents))
      else
        deallocate (COMCON)
        allocate (COMCON(1:NComponents,1:NComponents))
      end if
!
! Zero array
      COMCON(1:NComponents,1:NComponents)=0
!
! Condense close contact to components
      do I=1,Natoms
        ICOMP=CNCOMP(I)
        do J=I+1,Natoms
          JCOMP=CNCOMP(J)
          IF(CONTACT(I,J).EQ.1)then
            COMCON(ICOMP,JCOMP)=COMCON(ICOMP,JCOMP)+1
            COMCON(JCOMP,ICOMP)=COMCON(ICOMP,JCOMP)   ! copy immediately
          end if !
        end do !
      end do !
!
! Double the main diagonal; edge-loop adds 2 per occurence
      do I=1,NComponents
        COMCON(I,I)=2*COMCON(I,I)
      end do !
!
! End of routine GRAPH_COMP_CONN
      call PRG_manager ('exit', 'GRAPH_COMP_CONN', 'GRAPH%COMCON')
      return
      end subroutine GRAPH_COMP_CONN
      subroutine GRAPH_COMP_CONN_last
!***********************************************************************
!     Date last modified: May 21, 1996                     Version 1.0 *
!     Author: Cory C. Pye                                              *
!     Description: Connect all components if not done already.         *
!     Create object 'CONN_FAILSAFE'                                    *
!***********************************************************************
! Modules:
      USE program_constants
      USE GRAPH_objects
      USE type_elements
      USE type_molecule
      USE matrix_print

      implicit none
!
! Input scalars:
      integer ELMLEN
!
! Work arrays:
      double precision, dimension(:,:), allocatable :: WORK
      integer, dimension(:,:), allocatable :: WORKC
      integer, dimension(:), allocatable :: WORKV
      integer, dimension(:), allocatable :: WORKR
      integer, dimension(:), allocatable :: AT2CMP
      logical, dimension(:,:), allocatable :: FLAG
      logical, dimension(:), allocatable :: WORKU
!
! Local scalars:
      integer I,J,ICOMP,JCOMP,Iatom,Jatom,XNCOMP
      double precision DISMAX,DISMIN
      logical FINISHED
!
! Begin:
      call PRG_manager ('enter', 'GRAPH_COMP_CONN_last', 'GRAPH%CONN_FAILSAFE')
!
      call GET_object ('MOL', 'GRAPH', 'CONNECT')
      call GET_object ('MOL', 'GRAPH', 'CNCOMP')
      call GET_object ('MOL', 'GRAPH', 'CLOSE_CONTACT')
      call GET_object ('MOL', 'ATOMIC', 'DISTANCES')
!
! Object:
      if(.not.allocated(ICOCON))then
        allocate (ICOCON(1:Natoms,1:Natoms))
      else
        deallocate (ICOCON)
        allocate (ICOCON(1:Natoms,1:Natoms))
      end if
!
      allocate (WORKC(Natoms,Natoms), WORKV(Natoms), WORKR(Natoms), WORKU(Natoms), &
                WORK(Natoms,Natoms), FLAG(Natoms,Natoms), AT2CMP(Natoms))
!
      WORK(1:Natoms,1:Natoms)=bohr_to_angstrom*DISMAT(1:Natoms,1:Natoms)
      FLAG(1:Natoms,1:Natoms)=.TRUE.
!
! Subtract van der Waals' distance from distance matrix
      DISMAX=-1.0d05
      do I=1,Natoms
        do J=1,Natoms
          ICOCON(I,J)=0 ! Initialize connection matrix
        end do ! ! J
        IF(CARTESIAN(I)%Atomic_number.GT.0)then
          do J=I+1,Natoms
            IF (CARTESIAN(J)%Atomic_number.GT.0)then
              WORK(I,J)=WORK(I,J)-vdW_RADII(CARTESIAN(I)%Atomic_number)-vdW_RADII(CARTESIAN(J)%Atomic_number)
              WORK(J,I)=WORK(I,J)
              DISMAX=MAX(DISMAX,WORK(I,J))
            end if !
          end do ! ! J
        else
          do J=1,Natoms
            WORK(I,J)=1.0d5
          end do ! ! J
        end if !
      end do ! ! I
!
! Delete dummies from consideration.
      do I=1,Natoms
        ICOMP=CNCOMP(I)
        IF(ICOMP.EQ.0)then ! Dummy atom
          do J=1,Natoms
            FLAG(I,J)=.FALSE.
            FLAG(J,I)=.FALSE.
          end do !
        end if !
      end do !
!
! Calculate to see of there is more than one component.
!     WORKC=0
      do I=1,Natoms
        WORKU(I)=.FALSE.
        do J=1,Natoms
!         if(I.eq.J)cycle
          WORKC(I,J)=CONNECT(I,J)+CONTACT(I,J)  ! +ICOCON(I,J)
        end do !
      end do !
!
! First pass.
      call GRAPH_COMP (WORKC, WORKV,  WORKR, WORKU, AT2CMP, XNCOMP)
!
      IF(XNCOMP.EQ.1)then
        FINISHED=.TRUE.
      else
        FINISHED=.FALSE.
        do I=1,Natoms
          ICOMP=AT2CMP(I)
          do J=1,Natoms
            JCOMP=AT2CMP(J)
            IF(ICOMP.EQ.JCOMP)then
              FLAG(I,J)=.FALSE.
              FLAG(J,I)=.FALSE.
            end if !
          end do !
        end do !
      end if !
!
! Do you feel lucky, punk? Go ahead. Make my connection.
      do while(.not.FINISHED)
!
! Find smallest distance between allowed atoms.
        DISMIN=DISMAX
        do I=1,Natoms
          do J=1,Natoms
            IF(FLAG(I,J))then
              IF(WORK(I,J).LT.DISMIN)then
                Iatom=I
                Jatom=J
                DISMIN=WORK(I,J)
              end if !
            end if !
          end do !
        end do !
!
! Assign a contact to this distance.
        ICOCON(Iatom,Jatom)=1
        ICOCON(Jatom,Iatom)=1
!
! Let the world know!
        write(UNIout,1000)Iatom,Jatom
!
! rebuild connectivity and find components:
!     WORKC=0
      do I=1,Natoms
        WORKU(I)=.FALSE.
        do J=1,Natoms
!         if(I.eq.J)cycle
          WORKC(I,J)=CONNECT(I,J)+CONTACT(I,J)+ICOCON(I,J)
        end do !
      end do !
!
      call GRAPH_COMP (WORKC, WORKV, WORKR, WORKU, AT2CMP, XNCOMP)
!
      IF(XNCOMP.EQ.1)then
        FINISHED=.TRUE.
      else
        FINISHED=.FALSE.
        do I=1,Natoms
          ICOMP=AT2CMP(I)
          do J=1,Natoms
            JCOMP=AT2CMP(J)
            IF(ICOMP.EQ.JCOMP)then
              FLAG(I,J)=.FALSE.
              FLAG(J,I)=.FALSE.
            end if !
          end do !
        end do !
      end if !
!
      end do ! ! while

      deallocate (WORKC, WORKV, WORKR, WORKU, WORK, FLAG, AT2CMP)
!
! End of routine GRAPH_COMP_CONN_last
      call PRG_manager ('exit', 'GRAPH_COMP_CONN_last', 'GRAPH%CONN_FAILSAFE')
      return
 1000 FORMAT ('Connecting components via atoms ',2I5)
      end subroutine GRAPH_COMP_CONN_last
      subroutine MAKE_OOPBENDS
!***********************************************************************
!     Date last modified: 28 March, 2000                   Version 1.0 *
!     Author: Michelle Shaw                                            *
!     Description: Determines list of out-of-plane bends using the     *
!                  connectivity information.  If no oop bends exist,   *
!                  then puts object of length 1.  One must never use   *
!                  objelm to get the # of oop bends.  Modification of  *
!                  the torsion routine GRAPH_MAKETORS written by C. C. *
!                  Pye.                                                *
!***********************************************************************
! Modules:
      USE GRAPH_objects
      USE type_molecule

      implicit none
!
! Local scalars:
      integer iSTR,jATM,kATM,B1,B2,NBONDS_B1,NBONDS_B2,MAXDIM
!
! Begin:
      call PRG_manager ('enter', 'MAKE_OOPBENDS', 'GRAPH%OOPBEND_ATOMS')

      call GET_object ('MOL', 'GRAPH', 'CONNECT')
      call GET_object ('MOL', 'GRAPH', 'CONVAL')
      call GET_object ('MOL', 'GRAPH', 'BOND_ATOMS')

! Object:
      if(.not.allocated(oopbend))then
        MAXDIM = Natoms*(Natoms-1)/2
        allocate (oopbend(1:MAXDIM))
      else
        deallocate (oopbend)
        MAXDIM = Natoms*(Natoms-1)/2
        allocate (oopbend(1:MAXDIM))
      end if

      NOOPBend = 0
      NBONDS_B1 = 0

      do iSTR=1, NGBonds
        B1=BOND(iSTR)%ATOM1
        B2=BOND(iSTR)%ATOM2

! First, we must make sure that the central atom is a "tricoordinate centre", or is bonded to only
!   three other atoms...  The valency of the atom will give this information...
        NBONDS_B1 = CONVAL(B1)
        NBONDS_B2 = CONVAL(B2)

! Note: By construction, B2 > B1.
        do jATM=1, Natoms
          do kATM=1, Natoms

! J must be connected to B1 or B2 and be different than B2 and B1
! K must be connected to B1 and be different than B2 and J
! Also, make sure K > J to prevent "repeats" of the out-of-plane bends...
            IF(CONNECT(jATM,B1).EQ.1.and.CONNECT(kATM,B1).EQ.1.and.jATM.NE.B2.and.kATM.NE.B2.and. &
               kATM.GT.jATM.and.NBONDS_B1.EQ.3)then
              NOOPBEND=NOOPBEND+1
              oopbend(NOOPBEND)%ATOM1=jATM
              oopbend(NOOPBEND)%ATOM2=B1
              oopbend(NOOPBEND)%ATOM3=B2
              oopbend(NOOPBEND)%ATOM4=kATM
            end if !
! Because B2 > B1, some of the out-of-plane bends are missing.  They can be accounted for by
!  reversing the atoms in the bond B1-B2 and checking what is attached to B2...
            IF(CONNECT(jATM,B2).EQ.1.and.CONNECT(kATM,B2).EQ.1.and.jATM.NE.B1.and.kATM.NE.B1.and. &
               kATM.GT.jATM.and.NBONDS_B2.EQ.3)then
              NOOPBEND=NOOPBEND+1
              oopbend(NOOPBEND)%ATOM1=jATM
              oopbend(NOOPBEND)%ATOM2=B2
              oopbend(NOOPBEND)%ATOM3=B1
              oopbend(NOOPBEND)%ATOM4=kATM
            end if !
          end do !
        end do !
      end do !

      IF(NOOPBEND.EQ.0)then
        oopbend(1)%ATOM1=0
        oopbend(1)%ATOM2=0
        oopbend(1)%ATOM3=0
        oopbend(1)%ATOM4=0
      end if !

! End of routine MAKE_OOPBENDS
      call PRG_manager ('exit', 'MAKE_OOPBENDS', 'GRAPH%OOPBEND_ATOMS')
      return
      end subroutine MAKE_OOPBENDS
      subroutine GRAPH_VWEAK_BOND
!***********************************************************************
!     Date last modified: August 8, 1996                   Version 1.0 *
!     Author: C. C. Pye                                                *
!     Description: Creates object GRAPH%VERY_WEAK_BONDS                *
!***********************************************************************
! Modules:
      USE GRAPH_objects
      USE type_molecule

      implicit none
!
! Local work arrays:
      integer, dimension(:), allocatable :: ATOM1
      integer, dimension(:), allocatable :: ATOM2
!
! Local scalars:
      integer I,J
!
! Begin:
      call PRG_manager ('enter', 'GRAPH_VWEAK_BOND', 'GRAPH%VERY_WEAK_BONDS')
!
      call GET_object ('MOL', 'GRAPH', 'CONN_FAILSAFE')
!
      allocate (ATOM1(1:Natoms*(Natoms-1)/2), ATOM2(1:Natoms*(Natoms-1)/2))

      NvWBond=0
      do I=1,Natoms
        do J=I+1,Natoms
          IF(ICOCON(I,J).EQ.1)then
            NvWBond=NvWBond+1
            ATOM1(NvWBond)=I
            ATOM2(NvWBond)=J
          end if !
        end do !
      end do !
!
! If there are no bonds, then zero first element
      IF(NvWBond.EQ.0)then
        ATOM1(1)=0
        ATOM2(1)=0
      end if !

      if(.not.allocated(vWBOND))then
        allocate (vWBOND(1:max0(1,NvWBond)))
      else
        deallocate (vWBOND)
        allocate (vWBOND(1:max0(1,NvWBond)))
      end if

      do I=1,NvWBond
        vWBOND(I)%ATOM1=ATOM1(I)
        vWBOND(I)%ATOM2=ATOM2(I)
        vWBOND(I)%type=0       ! Very weak bonds
      end do ! ! I

      deallocate (ATOM1, ATOM2)

! End of routine GRAPH_VWEAK_BOND
      call PRG_manager ('exit', 'GRAPH_VWEAK_BOND', 'GRAPH%VERY_WEAK_BONDS')
      return
      end subroutine GRAPH_VWEAK_BOND
      subroutine GRAPH_VderW_BOND
!***********************************************************************
!     Date last modified: May    9, 1996                   Version 1.0 *
!     Author: C. C. Pye                                                *
!***********************************************************************
! Modules:
      USE GRAPH_objects
      USE type_molecule

      implicit none
!
! Local work arrays:
      integer, dimension(:), allocatable :: ATOM1
      integer, dimension(:), allocatable :: ATOM2
!
! Local scalars:
      integer I,J
!
! Begin:
      call PRG_manager ('enter', 'GRAPH_VderW_BOND', 'GRAPH%VAN_DER_WAAL_BONDS')
!
      call GET_object ('MOL', 'GRAPH', 'CLOSE_CONTACT')

      allocate (ATOM1(1:Natoms*(Natoms-1)/2), ATOM2(1:Natoms*(Natoms-1)/2))
!
      NvderWBond=0
      do I=1,Natoms
        do J=I+1,Natoms
          IF(CONTACT(I,J).EQ.1)then
            NvderWBond=NvderWBond+1
            ATOM1(NvderWBond)=I
            ATOM2(NvderWBond)=J
          end if !
        end do !
      end do !
!
! If there are no bonds, then zero first element
      IF(NvderWBond.EQ.0)then
        ATOM1(1)=0
        ATOM2(1)=0
      end if !

      if(.not.allocated(VdWBond))then
        allocate (VdWBond(1:max0(1,NvderWBond)))
      else
        deallocate (VdWBond)
        allocate (VdWBond(1:max0(1,NvderWBond)))
      end if

      do I=1,NvderWBond
        VdWBOND(I)%ATOM1=ATOM1(I)
        VdWBOND(I)%ATOM2=ATOM2(I)
        VdWBOND(I)%type=0       ! van der Waal bonds
      end do ! ! I

      deallocate (ATOM1, ATOM2)

! End of routine GRAPH_VderW_BOND
      call PRG_manager ('exit', 'GRAPH_VderW_BOND', 'GRAPH%VAN_DER_WAAL_BONDS')
      return
      end subroutine GRAPH_VderW_BOND
      subroutine CONN_NBOND
!*********************************************************************************
! Date last modified: 26 March, 2000                                Version 1.0  *
! Author: Michelle Shaw                                                          *
! Description: Builds nonbonded interaction connectivity matrix.                 *
!*********************************************************************************
! Modules:
      USE GRAPH_objects
      USE type_molecule

      implicit none
!
! Local scalars:
      integer I,J,K,NBONDED,TOTAL
!
! Begin:
      call PRG_manager ('enter', 'CONN_NBOND', 'GRAPH%NON_BONDED')
!
! Allocate work arrays:
      if(.not.allocated(CONN_NB))then
        call GET_object ('MOL', 'GRAPH', 'CONNECT')
        allocate (CONN_NB(Natoms, Natoms))
      else
        deallocate (CONN_NB)
        call GET_object ('MOL', 'GRAPH', 'CONNECT')
        allocate (CONN_NB(Natoms, Natoms))
      end if

      TOTAL = Natoms*(Natoms-1)/2
      NBONDED = 0

      do I = 1,Natoms
        do J = 1,Natoms
          CONN_NB(I,J) =.true.
          if(CONNECT(I,J).eq.1)NBONDED = NBONDED + 1
        end do
      end do

! Get pairs of atoms that are: 1) not bonded
!                              2) separated by 3 or more bonds (1,4-interactions or greater)

      do I = 1, Natoms
        do J = 1, Natoms
          do K = 1, Natoms
            IF(CONNECT(I,J).eq.1.or.(CONNECT(I,K).eq.1.and.CONNECT(J,K).eq.1).or. &
              (CONNECT(K,I).eq.1.and.CONNECT(K,J).eq.1))then
              CONN_NB(I,J) = .false.
            end if !
          end do !
        end do !
        CONN_NB(I,I) = .false.
      end do !

      NNONBOND = TOTAL - (NBONDED/2)

! End of routine CONN_NBOND
      call PRG_manager ('exit', 'CONN_NBOND', 'GRAPH%NON_BONDED')
      return
      end subroutine CONN_NBOND
      subroutine GRAPH_NO_BONDS
!********************************************************************************
! Date last modified: 14 January, 2000                              Version 1.0 *
! Author: Michelle Shaw                                                         *
! Description: Get both atoms for the electrostatic interactions.               *
!********************************************************************************
! Modules:
      USE GRAPH_objects
      USE type_elements
      USE matrix_print
      USE type_molecule

      implicit none
!
! Local scalars:
      integer I,J,CTR,iTOR,iELEC
!
! Begin:
      call PRG_manager ('enter', 'GRAPH_NO_BONDS', 'GRAPH%NON_BONDED_ATOMS')

      call GET_object ('MOL', 'ATOMIC', 'DISTANCES')
      call GET_object ('MOL', 'GRAPH', 'TORSION_ATOMS')
      call GET_object ('MOL', 'GRAPH', 'NON_BONDED')

! Allocate array:
      if(.not.allocated(pair_IJ))then
        write(UNIout,'(a,I10)')'Nnonbond: ',Nnonbond
        allocate (pair_IJ(NNONBOND))
      else
        deallocate (pair_IJ)
        write(UNIout,'(a,I10)')'Nnonbond: ',Nnonbond
        allocate (pair_IJ(NNONBOND))
      end if

! Initialize local variables:
      NELEC = 0
!
! No cutoffs
! From nonbonded connectivity matrix determine for which atoms the electrostatic interactions
!  will be included.  Store the atom numbers in work_ATOM1 and work_ATOM2.
      do I = 1,Natoms
        do J = I+1,Natoms
          if(CONN_NB(I,J))then
              if(CARTESIAN(I)%Atomic_number.ne.0.and.CARTESIAN(J)%Atomic_number.ne.0)then
                NELEC = NELEC + 1
                pair_IJ(NELEC)%I = I
                pair_IJ(NELEC)%J = J
                pair_IJ(NELEC)%is_elec = 1
                pair_IJ(NELEC)%is_vdw = 1
              end if
          end if
        end do
      end do

! Check whether the nonbonded atoms are a 1,4-interaction:
      do iELEC = 1,NELEC
        I = pair_IJ(iELEC)%I
        J = pair_IJ(iELEC)%J
        pair_IJ(iELEC)%is_tors = 0
        CTR = 0
        do iTOR = 1,NGTORSION
          if((I.eq.TORSION(iTOR)%ATOM1.and.J.eq.TORSION(iTOR)%ATOM4).or. &
             (J.eq.TORSION(iTOR)%ATOM1.and.I.eq.TORSION(iTOR)%ATOM4))then
            CTR = CTR + 1
          end if
        end do
        if(CTR.gt.0)pair_IJ(iELEC)%is_tors = 1
      end do

! End of routine GRAPH_NO_BONDS
      call PRG_manager ('exit', 'GRAPH_NO_BONDS', 'GRAPH%NON_BONDED_ATOMS')
      return
      end subroutine GRAPH_NO_BONDS
