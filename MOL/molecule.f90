      double precision function GET_Enuclear()
!***********************************************************************
!     Date last modified: July 07, 1992                                *
!     Author: R.A. Poirier                                             *
!     Description: Calculate nuclear repulsion energy.                 *
!***********************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE type_molecule

      implicit none
!
! Local scalars:
      integer :: I,J
      double precision :: Enuclear,RAB
      logical :: Lerror
!
! Begin:
      call PRG_manager ('enter', 'GET_Enuclear', 'UTILITY')
!
      Lerror=.false.
      Enuclear=ZERO
      if(Natoms.ge.2)then
        do I=1,Natoms-1
          if(CARTESIAN(I)%Atomic_number.le.0)cycle
          do J=I+1,Natoms
            if(CARTESIAN(J)%Atomic_number.le.0)cycle
            RAB=DSQRT((CARTESIAN(I)%X-CARTESIAN(J)%X)**2+(CARTESIAN(I)%Y-CARTESIAN(J)%Y)**2+ &
                  (CARTESIAN(I)%Z-CARTESIAN(J)%Z)**2)
            if(RAB.eq.0.0)then
              Lerror=.true.
              exit
            end if
            Enuclear=Enuclear+ DBLE(CARTESIAN(I)%Atomic_number*CARTESIAN(J)%Atomic_number)/RAB
          end do ! J
        end do ! I
      end if
!
      if(Lerror)then
        write(UNIout,'(a)')'GET_nuclear> Two or more atoms have the same cartesian'
!       stop 'GET_nuclear> Two or more atoms have the same cartesian'
      end if
      GET_Enuclear=Enuclear
!
! End of routine GET_Enuclear
      call PRG_manager ('exit', 'GET_Enuclear', 'UTILITY')
      return
      end function GET_Enuclear
      double precision function GET_R_IJ (AtomI, AtomJ)
!****************************************************************************************
!     Date last modified: July 6, 2000                                                  *
!     Author: R.A. Poirier                                                              *
!     Description: Calculate interatomic distance between AtomI and AtomJ in Bohr       *
!****************************************************************************************
! Modules:
      USE type_molecule

      implicit none
!
! Input scalars:
      integer AtomI,AtomJ
!
! Begin:
! Calculate distance in bohr.
      GET_R_IJ=DSQRT((CARTESIAN(AtomI)%X-CARTESIAN(AtomJ)%X)**2+ &
                     (CARTESIAN(AtomI)%Y-CARTESIAN(AtomJ)%Y)**2+ &
                     (CARTESIAN(AtomI)%Z-CARTESIAN(AtomJ)%Z)**2)
!
! End of routine GET_R_IJ
      return
      end
      subroutine ATOM_DISTANCES
!***********************************************************************
!     Date last modified: June 26, 1992                                *
!     Author: R.A. Poirier                                             *
!     Description: Calculate interatomic distance matrix in Angstroms  *
!***********************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE matrix_print

      implicit none
!
! Local scalars:
      integer I,J
      double precision temp
!
! Begin:
      call PRG_manager ('enter', 'ATOM_DISTANCES', 'UTILITY')
!
! Object:
      if(.not.allocated(DISMAT))then
        allocate (DISMAT(1:Natoms,1:Natoms))
      else
        deallocate (DISMAT)
        allocate (DISMAT(1:Natoms,1:Natoms))
      end if

! Calculate distance matrix in bohr.
      do I=1,Natoms
        do J=1,I
          temp=DSQRT((CARTESIAN(I)%X-CARTESIAN(J)%X)**2+(CARTESIAN(I)%Y-CARTESIAN(J)%Y)**2+ &
                      (CARTESIAN(I)%Z-CARTESIAN(J)%Z)**2)
          DISMAT(I,J)=temp
          DISMAT(J,I)=temp
        end do ! J=1,I
      end do ! I=1,I
!
      call CHECK_DISTANCES
!
! End of routine ATOM_DISTANCES
      call PRG_manager ('exit', 'ATOM_DISTANCES', 'UTILITY')
      return
      end
      subroutine PRT_distance_matrix
!***********************************************************************
!     Date last modified: July 17, 2002                                *
!     Author: R.A. Poirier                                             *
!     Description: Print the interatomic distance matrix in Angstroms  *
!***********************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE matrix_print

      implicit none
!
! Local scalars:
!
! Begin:
      call PRG_manager ('enter', 'PRT_distance_matrix', 'UTILITY')
!
      if(Natoms.gt.2)then
        write(UNIout,'(/2a)')'Distance Matrix for: ',MOL_title(1:len_trim(MOL_title))
        call PRT_matrix (DISMAT*Bohr_to_Angstrom, Natoms, Natoms)
      end if
!
! End of routine PRT_distance_matrix
      call PRG_manager ('exit', 'PRT_distance_matrix', 'UTILITY')
      return
      end
      subroutine PRT_FZM
!***********************************************************************
!     Date last modified: June 25, 1992                                *
!     Author: R.A. Poirier                                             *
!     Description: Get addresses of free format Z matrices.            *
!***********************************************************************
! Modules:
      USE program_files
      USE type_molecule

      implicit none
!
! Local scalars:
      integer IatomS,IZMPAR
!
! Begin:
      call PRG_manager ('enter', 'PRT_FZM', 'UTILITY')
!
      write(UNIout,'(2a)')'Free format Z-Matrix for: ',MOL_TITLE(1:len_trim(MOL_title))
      call PRT_FZMATRIX (UNIout)
      write(UNIout,'(X)')
!
      if(Natoms.gt.1)then
        write(UNIout,'(a)')'VARIABLES:'
        call PRT_FZVALUES (UNIout, 3)
        write(UNIout,'(X)')
      end if
!
! End of routine PRT_FZM
      call PRG_manager ('exit', 'PRT_FZM', 'UTILITY')
      return
      end subroutine PRT_FZM
      subroutine PRT_XYZ
!***********************************************************************
!     Date last modified: April 1, 1999                                *
!     Author: R.A. Poirier                                             *
!     Description: Print the X,Y,Z coordinates.                        *
!***********************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE type_molecule

      implicit none
!
! Local scalars:
      integer Iatom
      double precision E_nuclear
!
! Local function:
      double precision GET_Enuclear
!
! Begin:
      call PRG_manager ('enter', 'PRT_XYZ', 'UTILITY')
!
      write(UNIout,'(2a)')'Cartesian coordinates for: ',MOL_TITLE(1:len_trim(MOL_title))
      write(UNIout,1000)
      do Iatom=1,Natoms
      write(UNIout,1010)Iatom,CARTESIAN(Iatom)%element,CARTESIAN(Iatom)%Atomic_number, &
                        CARTESIAN(Iatom)%X*Bohr_to_Angstrom,CARTESIAN(Iatom)%Y*Bohr_to_Angstrom, &
                        CARTESIAN(Iatom)%Z*Bohr_to_Angstrom,CARTESIAN(Iatom)%X,CARTESIAN(Iatom)%Y,CARTESIAN(Iatom)%Z
      end do ! Iatom
      write(UNIout,1020)
      E_nuclear=GET_Enuclear()
      write(UNIout,'(a,F17.9/)')'Nuclear repulsion energy: ',E_nuclear
!
! End of routine PRT_XYZ
      call PRG_manager ('exit', 'PRT_XYZ', 'UTILITY')
      return
!
 1000 FORMAT(/114('-')/29X,'COORDINATES IN ANGSTROMS',33X, &
            'COORDINATES IN BOHR'/'   I EL        AN',8X,'X',14X,'Y', &
            14X,'Z',24X,'X',14X,'Y',14X,'Z'/114('-'))
 1010 FORMAT(1X,I3,1x,A8,I4,F13.8,2F15.8,10X,3F15.8)
 1020 FORMAT(114('-'))
!
      end subroutine PRT_XYZ
      subroutine PRT_ZMAT
!***********************************************************************
!     Date last modified: January 28, 1992                             *
!     Author:R.A. Poirier                                              *
!     Description: Get addresses to build molecule.  MUN Version.      *
!***********************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE type_molecule

      implicit none
!
! Local scalars:
      integer I,I1,I2,I3
      double precision TETRA
!
! Begin:
      call PRG_manager ('enter', 'PRT_ZMAT', 'UTILITY')
!
! Print Z Matrix.
      write(UNIout,'(2a)')'Z MATRIX FOR: ',MOL_TITLE(1:len_trim(MOL_title))
      write(UNIout,1020)
      I=1
      write(UNIout,1060)I,CARTESIAN(1)%Atomic_number
!
      if(Natoms.GT.1)then
        TETRA=TWO*DATAN(DSQRT(TWO))/Deg_to_radian   ! Check for tetrahedral angle substitution.
        do I=1,Natoms
          if(Zmatrix%atoms(I)%atom2.LT.0)Zmatrix%angle(I)%value=TETRA
          if(Zmatrix%atoms(I)%atom3.LT.0)Zmatrix%torsion(I)%value=TETRA
        end do ! I
!
        I=2
        I1=1
        write(UNIout,1060)I,CARTESIAN(2)%Atomic_number,Zmatrix%atoms(2)%atom1,Zmatrix%bond(2)%length*Bohr_to_Angstrom,I1
      if(Natoms.GT.2)then
        I=3
        I1=I1+1
        I2=Natoms
        write(UNIout,1060)I,CARTESIAN(3)%Atomic_number,Zmatrix%atoms(3)%atom1,Zmatrix%bond(3)%length*Bohr_to_Angstrom, &
                          I1,Zmatrix%atoms(3)%atom2,Zmatrix%angle(3)%value,I2
      if(Natoms.GT.3)then
        I3=2*Natoms-3
        do I=4,Natoms
          I1=I1+1
          I2=I2+1
          I3=I3+1
          write(UNIout,1060)I,CARTESIAN(I)%Atomic_number,Zmatrix%atoms(I)%atom1,Zmatrix%bond(I)%length*Bohr_to_Angstrom, &
                            I1,Zmatrix%atoms(I)%atom2,Zmatrix%angle(I)%value,I2, &
                                     Zmatrix%atoms(I)%atom3,Zmatrix%torsion(I)%value,I3,Zmatrix%atoms(I)%atom4
        end do !I
      end if
      end if
      end if

      write(UNIout,1070)
!
 1020 FORMAT(96('-')/'   I   AN   Z1     BL',18X,'Z2    ALPHA',16X, &
                                 'Z3     BETA',16X,'Z4'/96('-'))
 1060 FORMAT(1X,I3,2I5,F11.6,'   (',I3,')   ',I5,F11.4,'   (',I3,')   ', &
                    I5,F11.4,'   (',I3,')   ',I5)
 1070 FORMAT(96('-')/)
!
! End of routine PRT_ZMAT
      call PRG_manager ('exit', 'PRT_ZMAT', 'UTILITY')
      return
      end subroutine PRT_ZMAT
      subroutine CHEMICAL_FORMULA (formula)
!************************************************************************
!     Date last modified: June, 1997                        Version 1.0 *
!     Author: R.A. Poirier                                              *
!     Description: Generate the formula C, H and others in increasing Z *
!************************************************************************
! Modules:
      USE program_files
      USE type_molecule
      USE type_elements

      implicit none
!
! Output scalar:
      character*(*) FORMULA
!
! Local scalar:
      integer Iatom,ifIRST,IZ,LENIEL,LENSTR,NCARBON,NHYDROGEN,NIAN
      character(len=MAX_formula) :: catom
      character(len=MAX_formula) :: string
!
! Begin:
      call PRG_manager ('enter', 'CHEMICAL_FORMULA', 'UTILITY')
!
      FORMULA=' '
!
      NCARBON=0
      NHYDROGEN=0
      do Iatom=1,Natoms
        if(CARTESIAN(Iatom)%Atomic_number.eq.6)NCARBON=NCARBON+1
        if(CARTESIAN(Iatom)%Atomic_number.eq.1)NHYDROGEN=NHYDROGEN+1
      end do ! Iatom
!
      if(NCARBON.GT.0)then
      ifIRST=INDEX(FORMULA, ' ')
      if(NCARBON.GT.1)then
        write(string,'(I30)')NCARBON
        call trimld (string, CATOM, lenstr)
        FORMULA(ifIRST:)='C'//CATOM(1:lenstr)
      else
        FORMULA(ifIRST:)='C'
      end if ! NCARBON.GT.1
      end if ! NCARBON.GT.0

      if(NHYDROGEN.GT.0)then
      ifIRST=INDEX(FORMULA, ' ')
      if(NHYDROGEN.GT.1)then
        write(string,'(I30)')NHYDROGEN
        call trimld (string, CATOM, lenstr)
        FORMULA(ifIRST:)='H'//CATOM(1:lenstr)
      else
        FORMULA(ifIRST:)='H'
      end if ! NHYDROGEN.GT.1
      end if ! NHYDROGEN.GT.0

! Do the other elements in order of atomic number
      do IZ=2,Nelements
      NIAN=0
      do Iatom=1,Natoms
        if(CARTESIAN(Iatom)%Atomic_number.EQ.IZ)NIAN=NIAN+1
      end do ! Iatom
      if(NIAN.gt.0.and.IZ.ne.6)then
      ifIRST=INDEX(FORMULA, ' ')
      leniel=len_trim(Element_symbols(IZ))
      if(NIAN.gt.1)then
        write(string,'(I30)')NIAN
        call trimld (string, CATOM, lenstr)
        FORMULA(ifIRST:)=Element_symbols(IZ)(1:leniel)//CATOM(1:lenstr)
      else
        FORMULA(ifIRST:)=Element_symbols(IZ)(1:leniel)
      end if ! NIAN.GT.1
      end if ! NIAN.GT.0
      end do ! IZ
!
! End of routine CHEMICAL_FORMULA
      call PRG_manager ('exit', 'CHEMICAL_FORMULA', 'UTILITY')
      return
      end subroutine CHEMICAL_FORMULA
      subroutine PRT_FZVALUES (unit_out, MAXcol)
!***********************************************************************
!     Date last modified: June 25, 1992                                *
!     Author: R.A. Poirier                                             *
!     Description: Get addresses of free format Z matrices.            *
!***********************************************************************
! Modules:
      USE program_constants
      USE type_molecule

      implicit none
!
! Input scalars:
      integer :: unit_out
      integer :: MAXcol
!
! Local scalars:
      integer :: Len,MAX_len
      integer :: Icol,IZMVAR,L1,L2
! Local parameters:
      integer, parameter :: MAX_output=132 ! Maximum length of output line
      character(len=MAX_output) :: output
!
! Begin:
      call PRG_manager ('enter', 'PRT_FZVALUES', 'UTILITY')
!
! Determine the maximum name length
      MAX_len=0
      do IZMVAR=1,Zmatrix%Ndefines
        Len=len_trim(Zmatrix%define(IZMVAR)%name)
        if(Len.gt.MAX_len)MAX_len=Len 
      end do ! IZMVAR
! Write Z-Matrix parameters and their values
      output(1:)=' '
      Icol=0
      L1=1
      do IZMVAR=1,Zmatrix%Nbonds
        if(Zmatrix%define(IZMVAR)%opt)then
          Icol=Icol+1
          write(OUTPUT(L1:),'(a,a,F11.8,1x)') &
               Zmatrix%define(IZMVAR)%name(1:MAX_len),' =', &
               Zmatrix%define(IZMVAR)%value*Bohr_to_Angstrom
          L1=L1+30
        else
          write(unit_out,'(a,a,F11.8,a)') &
            Zmatrix%define(IZMVAR)%name(1:MAX_len),' =', &
            Zmatrix%define(IZMVAR)%value*Bohr_to_Angstrom,' OPT=false'
        end if
        if(Icol.EQ.MAXCOL)then
          write(unit_out,'(a)')OUTPUT(1:110)
          Icol=0
          L1=1
        end if
      end do ! IZMVAR
!
! Print angles
      do IZMVAR=Zmatrix%Nbonds+1,Zmatrix%Ndefines
        if(Zmatrix%define(IZMVAR)%opt)then
          Icol=Icol+1
          write(OUTPUT(L1:),'(a,a,F11.5,1x)') &
          Zmatrix%define(IZMVAR)%name(1:MAX_len),' =',Zmatrix%define(IZMVAR)%value
          L1=L1+30
        else
          write(unit_out,'(a,a,F11.5,a)') &
          Zmatrix%define(IZMVAR)%name(1:MAX_len),' =',Zmatrix%define(IZMVAR)%value, &
          ' OPT=false'
        end if
        if(Icol.EQ.MAXCOL)then
          write(unit_out,'(a)')OUTPUT(1:110)
          Icol=0
          L1=1
        end if
      end do ! IZMVAR
!
      if(Icol.GT.0)then
        L2=Icol*30-1
        write(unit_out,'(a)')OUTPUT(1:L2)
      end if
!
! End of routine PRT_FZVALUES
      call PRG_manager ('exit', 'PRT_FZVALUES', 'UTILITY')
      return
      end subroutine PRT_FZVALUES
      subroutine PRT_FZMATRIX (unit_out)
!***********************************************************************
!     Date last modified: June 25, 1992                                *
!     Author: R.A. Poirier                                             *
!     Description: Get addresses of free format Z matrices.            *
!***********************************************************************
! Modules:
      USE type_molecule

      implicit none
!
! Input scalars:
      integer unit_out
!
! Local scalars:
      integer :: Len,MAX_len
      integer :: Iatom,I1,I2,I3,IZMVAR
!
! Begin:
      call PRG_manager ('enter', 'PRT_FZMATRIX', 'UTILITY')
!
! Determine the maximum name length
      MAX_len=0
        do Iatom=2,Natoms
         Len=len_trim(Zmatrix%bond(Iatom)%label)
        if(Len.gt.MAX_len)MAX_len=Len 
         Len=len_trim(Zmatrix%angle(Iatom)%label)
        if(Len.gt.MAX_len)MAX_len=Len 
         Len=len_trim(Zmatrix%torsion(Iatom)%label)
        if(Len.gt.MAX_len)MAX_len=Len 
      end do ! IZMVAR
!
! Print Free Format Z-Matrix.
      write(unit_out,'(a8)')CARTESIAN(1)%element
      if(Natoms.GT.1)then
        I1=Zmatrix%atoms(2)%atom1
        write(unit_out,'(2(a8,1x),a)')CARTESIAN(2)%element,CARTESIAN(I1)%element, &
                                       Zmatrix%bond(2)%label(1:MAX_len)
      if(Natoms.GT.2)then
        I1=Zmatrix%atoms(3)%atom1
        I2=Zmatrix%atoms(3)%atom2
        write(unit_out,'(a8,2(1x,a8,1x,a))')CARTESIAN(3)%element,CARTESIAN(I1)%element,  &
                                       Zmatrix%bond(3)%label(1:MAX_len), &
                                       CARTESIAN(I2)%element,Zmatrix%angle(3)%label(1:MAX_len)
      if(Natoms.GT.3)then
        do Iatom=4,Natoms
        I1=Zmatrix%atoms(Iatom)%atom1
        I2=Zmatrix%atoms(Iatom)%atom2
        I3=Zmatrix%atoms(Iatom)%atom3
        if(Zmatrix%atoms(Iatom)%atom4.eq.0)then ! torsion
        write(unit_out,'(a8,3(1x,a8,1x,a))') &
              CARTESIAN(Iatom)%element,CARTESIAN(I1)%element, &
              Zmatrix%bond(Iatom)%label(1:MAX_len),CARTESIAN(I2)%element, &
              Zmatrix%angle(Iatom)%label(1:MAX_len),CARTESIAN(I3)%element, &
              Zmatrix%torsion(Iatom)%label(1:MAX_len)
        else ! euler angle
        write(unit_out,'(a8,3(1x,a8,1x,a),i2)') &
              CARTESIAN(Iatom)%element,CARTESIAN(I1)%element, &
              Zmatrix%bond(Iatom)%label(1:MAX_len),CARTESIAN(I2)%element, &
              Zmatrix%angle(Iatom)%label(1:MAX_len),CARTESIAN(I3)%element, &
              Zmatrix%torsion(Iatom)%label(1:MAX_len), &
              Zmatrix%atoms(Iatom)%atom4
        end if
        end do ! Iatom
      end if ! Natoms.GT.3
      end if ! Natoms.GT.2
      end if ! Natoms.GT.1
!
      call flush(unit_out)
!
! End of routine PRT_FZMATRIX
      call PRG_manager ('exit', 'PRT_FZMATRIX', 'UTILITY')
      return
      end subroutine PRT_FZMATRIX
      subroutine DEF_NoccMO (NoccMO, NAelectron, NBelectron)
!***********************************************************************
!     Date last modified: February 3, 1998                 Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: Define number of electrons / no. of occupied MO.    *
!***********************************************************************
! Modules:
      USE program_files
      USE type_molecule

      implicit none
!
! Input scalar:
      integer :: NAelectron,NBelectron,NoccMO
!
! Local scalars:
      integer Iatom
!
! Begin:
      call PRG_manager ('enter', 'DEF_NoccMO', 'UTILITY')
!
! Calculate total number of electrons and number of alpha and beta electrons
      Nelectrons=0
      do Iatom=1,Natoms
        if(CARTESIAN(Iatom)%Atomic_number.le.0)cycle
        Nelectrons=Nelectrons+IABS(CARTESIAN(Iatom)%Atomic_number)
!     end if
      end do ! Iatom
!
      Nelectrons=Nelectrons-MOLECULE%charge
      NAelectron=(Nelectrons+Multiplicity-1)/2
      NBelectron=(Nelectrons-Multiplicity+1)/2
      NoccMO=NAelectron
!
! Check the Charge/Multiplicity.
      if(Multiplicity.le.0.or. &
        NAelectron+NBelectron.ne.Nelectrons.or. &
        NAelectron.le.0.or. &
        NBelectron.lt.0)then     ! Charge/Multiplicity error.
        write(UNIout,'(a)')'ERROR> DEF_NoccMO:'
        write(UNIout,'(a)')'CHARGE/MULTIPLICITY ERROR'
        if(LNCI)then
          MOLECULE%charge=+1
        else
          call PRG_stop ('DEF_NoccMO> CHARGE/MULTIPLICITY ERROR')
        end if
      end if
!
! End of routine DEF_NoccMO
      call PRG_manager ('exit', 'DEF_NoccMO', 'UTILITY')
      return
      end subroutine DEF_NoccMO
      subroutine GET_masses
!***********************************************************************
!     Date last modified: June 26, 1992                                *
!     Author: R.A. Poirier                                             *
!     Description: Get atomic masses                                   *
!***********************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE type_molecule
      USE matrix_print
      USE atommass

      implicit none
!
! Local scalars:
      integer :: Iatom
      double precision :: sum
!
! Begin:
      call PRG_manager ('enter', 'GET_masses', 'UTILITY')
!
! Object:
      call GetIsotopeMass

      sum=ZERO
      do Iatom=1,Natoms
        if(CARTESIAN(Iatom)%Atomic_number.le.0)cycle
        sum=sum+MOL_atoms(Iatom)%mass
      end do
      molecule%mass=sum
!
! End of routine GET_masses
      call PRG_manager ('exit', 'GET_masses', 'UTILITY')
      return
      end subroutine GET_masses
      subroutine PRT_masses
!***********************************************************************
!     Date last modified: June 26, 1992                                *
!     Author: R.A. Poirier                                             *
!     Description: Get atomic masses                                   *
!***********************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE type_molecule
      USE matrix_print
      USE atommass

      implicit none
!
! Local scalars:
      integer :: Iatom
      double precision :: sum
!
! Begin:
      call PRG_manager ('enter', 'PRT_masses', 'UTILITY')
!
! Object:
      call GetIsotopeMass

      write(UNIout,*)'Atomic number and atomic mass:'
      sum=ZERO
      do Iatom=1,Natoms
        write(UNIout,'(i8,f20.6)')CARTESIAN(Iatom)%Atomic_number,MOL_atoms(Iatom)%mass
      end do
      write(UNIout,'(X)')
      write(UNIout,'(a,f20.6)')'Molecular mass: ',Molecule%mass
!
! End of routine PRT_masses
      call PRG_manager ('exit', 'PRT_masses', 'UTILITY')
      return
      end subroutine PRT_masses
      subroutine GetIsotopeMass
!***********************************************************************
!     Date last modified: May 05, 1999                    Version 1.0  *
!     Author: R.A. Poirier                                             *
!     Description: gets atomic masses for isotopes                     *
!***********************************************************************
!Modules:
      USE program_files
      USE type_molecule
      USE atommass

      implicit none
!
! Local scalars:
      integer Iatom
!
      do Iatom=1,Natoms    ! loop through all element in the molecule
        if(CARTESIAN(Iatom)%Atomic_number.lt.0)cycle
        if(CheckIsotope(CARTESIAN(Iatom)%Atomic_number,MOL_atoms(Iatom)%isotope))then
!  check if atom number and isotope number are supported,if any fail the loop is terminated
      select case (CARTESIAN(Iatom)%Atomic_number)
        case (0)
        MOL_atoms(Iatom)%mass=0.0
        case (1)
        MOL_atoms(Iatom)%mass=Iso_H(MOL_atoms(Iatom)%isotope)
        case (2)
        MOL_atoms(Iatom)%mass=Iso_He(MOL_atoms(Iatom)%isotope)
        case (3)
        MOL_atoms(Iatom)%mass=Iso_Li(MOL_atoms(Iatom)%isotope)
        case (4)
        MOL_atoms(Iatom)%mass=Iso_Be(MOL_atoms(Iatom)%isotope)
        case (5)
        MOL_atoms(Iatom)%mass=Iso_B(MOL_atoms(Iatom)%isotope)
        case (6)
        MOL_atoms(Iatom)%mass=Iso_C(MOL_atoms(Iatom)%isotope)
        case (7)
        MOL_atoms(Iatom)%mass=Iso_N(MOL_atoms(Iatom)%isotope)
        case (8)
        MOL_atoms(Iatom)%mass=Iso_O(MOL_atoms(Iatom)%isotope)
        case (9)
        MOL_atoms(Iatom)%mass=Iso_F(MOL_atoms(Iatom)%isotope)
        case (10)
        MOL_atoms(Iatom)%mass=Iso_Ne(MOL_atoms(Iatom)%isotope)
        case (14)
        MOL_atoms(Iatom)%mass=Iso_Si(MOL_atoms(Iatom)%isotope)
        case (15)
        MOL_atoms(Iatom)%mass=Iso_P(MOL_atoms(Iatom)%isotope)
        case (16)
        MOL_atoms(Iatom)%mass=Iso_S(MOL_atoms(Iatom)%isotope)
        case (17)
        MOL_atoms(Iatom)%mass=Iso_Cl(MOL_atoms(Iatom)%isotope)
        case (34)
        MOL_atoms(Iatom)%mass=Iso_Se(MOL_atoms(Iatom)%isotope)
        case (35)
        MOL_atoms(Iatom)%mass=Iso_Br(MOL_atoms(Iatom)%isotope)
        case (53)
        MOL_atoms(Iatom)%mass=Iso_I(MOL_atoms(Iatom)%isotope)
        case DEFAULT
        MOL_atoms(Iatom)%mass=Imass_Def(CARTESIAN(Iatom)%Atomic_number)
      end select
      else
        write(UNIout,'(a)')'ERROR: GetIsotopeMass>'
        write(UNIout,'(a,i5,a)')'Isotope not supported for atomic number ',CARTESIAN(Iatom)%Atomic_number,' or'
        write(UNIout,'(a,i5,a)')'for isotope ',MOL_atoms(Iatom)%isotope ,' or'
        call PRG_stop ('Isotope not supported')
      end if
      end do ! Iatom
      end subroutine GetIsotopeMass
      subroutine CHECK_DISTANCES
!***********************************************************************
!     Date last modified: July 10, 2008                                *
!     Author: R.A. Poirier                                             *
!     Description: Calculate interatomic distance matrix in Angstroms  *
!***********************************************************************
! Modules:
      USE program_files
      USE OPT_defaults
      USE type_molecule

      implicit none
!
! Local scalars:
      integer Iatom,Jatom
      double precision AUCRIT,temp
!
      parameter (AUCRIT=1.37D0) ! in bohr (allows for H2)
!
! Begin:
      call PRG_manager ('enter', 'CHECK_DISTANCES', 'UTILITY')
!
      Lacceptable=.true.
!
! Check for short bond lengths.
      do Iatom=2,Natoms
      if(CARTESIAN(Iatom)%Atomic_number.eq.0)cycle
        do Jatom=1,Iatom-1
          if(CARTESIAN(Jatom)%Atomic_number.eq.0)cycle
            temp=DSQRT((CARTESIAN(Iatom)%X-CARTESIAN(Jatom)%X)**2+(CARTESIAN(Iatom)%Y-CARTESIAN(Jatom)%Y)**2+ &
                       (CARTESIAN(Iatom)%Z-CARTESIAN(Jatom)%Z)**2)
            if(temp.LE.AUCRIT)then
              write(UNIout,'(1X,2(A,I4),A,F7.4/22X,A)')'WARNING: Distance between atoms ',Iatom,' and ',Jatom, &
              ' is ',temp,' Check the geometry'
              Lacceptable=.false.
            end if
        end do ! Jatom
      end do ! Iatom
!
! End of routine CHECK_DISTANCES
      call PRG_manager ('exit', 'CHECK_DISTANCES', 'UTILITY')
      return
      end
      subroutine MOL_similarity
!***********************************************************************
!     Date last modified: July 10, 2008                                *
!     Author: R.A. Poirier                                             *
!     Description: Calculate interatomic distance matrix in Angstroms  *
!***********************************************************************
! Modules:
      USE program_files
      USE type_molecule

      implicit none
!
! Local scalars:
      integer :: Iatom
!
! Begin:
      call PRG_manager ('enter', 'MOL_similarity', 'UTILITY')
!
      if(moleculeA%formula(1:len_trim(moleculeA%formula)).eq. &
         moleculeB%formula(1:len_trim(moleculeB%formula)))then
        write(UNIout,'(a)')'The molecules have the same formula'
      else
        write(UNIout,'(a)')'The molecules have different formula'
      end if
!
! End of routine MOL_similarity
      call PRG_manager ('exit', 'MOL_similarity', 'UTILITY')
      return
      end
      subroutine XMOL_output (output_unit)
!***********************************************************************
!     Date last modified: April 8, 1998                                *
!     Author: R.A. Poirier                                             *
!     Description: Output cartesians in Angstrom for viewing with XMOL *
!***********************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE type_molecule
      USE GRAPH_objects

      implicit none
!
! Input scalar:
      integer output_unit
!
! Local scalars:
      integer :: Iatom
!
! Begin:
      call PRG_manager ('enter', 'XMOL_output', 'UTILITY')
!
! Only include real atoms
      write(output_unit,'(I10)')Natoms-NDummies
      write(output_unit,'(2a)')'Cartesian ! coordinates for: ',MOL_TITLE(1:len_trim(MOL_title))
      do Iatom=1,Natoms
        if(CARTESIAN(Iatom)%Atomic_number.gt.0)then
          write(output_unit,'(i6,3F14.8)')&
                CARTESIAN(Iatom)%Atomic_number, &
                CARTESIAN(Iatom)%X*bohr_to_angstrom, &
                CARTESIAN(Iatom)%Y*bohr_to_angstrom, &
                CARTESIAN(Iatom)%Z*bohr_to_angstrom
        end if
      end do ! Iatom
!
! End of routine XMOL_output
      call PRG_manager ('exit', 'XMOL_output', 'UTILITY')
      return
      end subroutine XMOL_output
      subroutine XYZ_output (output_unit)
!***********************************************************************
!     Date last modified: September 30, 2010                           *
!     Author: R.A. Poirier                                             *
!     Description: Output cartesians in Angstrom                       *
!***********************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE type_molecule
      USE GRAPH_objects

      implicit none
!
! Input scalar:
      integer output_unit
!
! Local scalars:
      integer :: Iatom
!
! Begin:
      call PRG_manager ('enter', 'XYZ_output', 'UTILITY')
!
! Only include real atoms
      do Iatom=1,Natoms
        if(CARTESIAN(Iatom)%Atomic_number.gt.0)then
          write(output_unit,'(i6,3F14.8)')&
                CARTESIAN(Iatom)%Atomic_number, &
                CARTESIAN(Iatom)%X*bohr_to_angstrom, &
                CARTESIAN(Iatom)%Y*bohr_to_angstrom, &
                CARTESIAN(Iatom)%Z*bohr_to_angstrom
        end if
      end do ! Iatom
!
! End of routine XYZ_output
      call PRG_manager ('exit', 'XYZ_output', 'UTILITY')
      return
      end subroutine XYZ_output
      subroutine XYZ_to_Zmatrix
!********************************************************************************
!     Date last modified: November 20, 2013                                     *
!     Author: R. A. Poirier                                                     *
!     Description: Generate a Z-Matrix the new Cartesians                       *
!********************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE type_molecule
      USE type_elements
      USE GRAPH_objects
      USE OPT_objects

      implicit none
!
! Local scalars:
      integer :: lenstr,Znum,status
      integer :: Iatom,Jatom,Katom,Latom,NZMVAR,IZMVAR,TZMVAR,IvWBond
      integer :: NBonds,NAngles,NTorsions
      logical :: LError,LFound
      character(len=64) :: Ctemp
      character(len=16) :: CLabel
!
! Begin:
      call PRG_manager ('enter', 'XYZ_to_Zmatrix', 'UTILITY')
!
      call GET_object ('MOL', 'GRAPH', 'CONNECT')
!
! NOTE: Dummy atoms must be excluded!
      if(NDummies.ne.0)then
        write(UNIout,'(a)')'To build a Z-Matrix no dummy atoms must be present'
        write(UNIout,'(a)')'No Z-Matrix was built'
!       stop 'To build a Z-Matrix no dummy atoms must be present'
        return
      end if

      if(associated(Zmatrix%define))then ! Previous Z-matrix exists
        deallocate (Zmatrix%atoms, Zmatrix%bond, Zmatrix%angle, Zmatrix%torsion, Zmatrix%define, STAT=status)
        if(status.ne.0)then
          write(UNIout,'(a)')'MENU_molecule> deallocation of Zmatrix%, ... failed'
        end if
      end if

      allocate (Zmatrix%atoms(1:Natoms), Zmatrix%bond(1:Natoms), Zmatrix%angle(1:Natoms), &
                Zmatrix%torsion(1:Natoms)) !, Zmatrix%define(1:Natoms))
!
      NZMVAR=3*Natoms-6
      allocate (Zmatrix%define(1:NZMVAR))

      Zmatrix%define(1:NZMVAR)%opt=.true.
      Zmatrix%define(1:NZMVAR)%name=' '
!     Zmatrix%units=
      Zmatrix%Natoms=Natoms
      Zmatrix%Nbonds=Natoms-1
      Zmatrix%Nangles=Natoms-2
      Zmatrix%Ntorsions=Natoms-3
      Zmatrix%Ndefines=NZMVAR
!
      Ctemp=' '
      CLabel=' '
      IZMVAR=0
      TZMVAR=0
      Zmatrix%atoms(1)%atom1=0
!
! First deal with bonds (define atom1)
      Zmatrix%atoms(1:Natoms)%atom1=0
      Zmatrix%atoms(1:Natoms)%atom2=0
      Zmatrix%atoms(1:Natoms)%atom3=0
      Zmatrix%atoms(1:Natoms)%atom4=0
      if(Natoms.ge.2)then
      NBonds=0
      do Iatom=2,Natoms
        LFound=.false.
      do Jatom=1,Iatom-1 ! Find an atom that is connect to Iatom
        if(CONNECT(Iatom,Jatom).eq.0)cycle
        NBonds=NBonds+1
        Zmatrix%atoms(Iatom)%atom1=Jatom
        LFound=.true.
        exit
      end do ! Jatom=1
      if(LFound)cycle
      call Find_Closest_Atom (Iatom, Jatom) ! Find the closest atom to Iatom
      Zmatrix%atoms(Iatom)%atom1=Jatom
      NBonds=NBonds+1
      end do ! Iatom=2
      end if ! Natoms.ge.2
!
! Now deal with angles (define atom2)
      NAngles=0
      if(Natoms.ge.3)then
      do Iatom=3,Natoms
        LFound=.false.
      Jatom=Zmatrix%atoms(Iatom)%atom1
      do Katom=1,Iatom-1 ! Find an atom that is connect to Jatom
        if(Jatom.eq.Katom)cycle
        if(CONNECT(Jatom,Katom).eq.0)cycle
        NAngles=NAngles+1
        Zmatrix%atoms(Iatom)%atom2=Katom
        LFound=.true.
        exit
      end do ! Katom=1
      if(LFound)cycle
      call Find_Closest_Atom (Jatom, Katom) ! Find the closest atom to Jatom
      NAngles=NAngles+1
      Zmatrix%atoms(Iatom)%atom2=Katom
      end do ! Iatom=3
      end if
!
! Now deal with torsions (define atom3)
      NTorsions=0
      if(Natoms.ge.4)then
      do Iatom=4,Natoms
        LFound=.false.
      Jatom=Zmatrix%atoms(Iatom)%atom1
      Katom=Zmatrix%atoms(Iatom)%atom2
      do Latom=1,Iatom-1 ! Find an atom that is connect to Jatom
        if(Jatom.eq.Latom)cycle
        if(Katom.eq.Latom)cycle
        if(CONNECT(Katom,Latom).eq.0.and.CONNECT(Jatom,Latom).eq.0)cycle
        NTorsions=NTorsions+1
        Zmatrix%atoms(Iatom)%atom3=Latom
        Zmatrix%atoms(Iatom)%atom4=0
        LFound=.true.
        exit
      end do ! Latom=1
      if(LFound)cycle
      call Find_Closest_Atom (Katom, Latom) ! Find the closest atom to Katom
      if(Latom.eq.Jatom)call Find_Closest_Atom (Jatom, Latom) ! Find the closest atom to Jatom
      if(Latom.eq.Katom)exit
!     do Latom=1,Iatom-1 ! Check for close contacts
!       if(Jatom.eq.Latom)cycle
!       if(Katom.eq.Latom)cycle
!       if(CONTACT(Katom,Latom).eq.0.and.CONTACT(Jatom,Latom).eq.0)cycle ! New
        NTorsions=NTorsions+1
        Zmatrix%atoms(Iatom)%atom3=Latom
        Zmatrix%atoms(Iatom)%atom4=0
!       exit
!     end do ! Latom=1
      end do ! Iatom=4
      end if

! First Define the labels
      do Iatom=1,Natoms
        Znum=CARTESIAN(Iatom)%Atomic_number
        if(Znum.le.0)cycle
        write(Ctemp,'(a,i8)')trim(element_symbols(Znum)),Iatom
        call RemoveBlanks (Ctemp, CLabel, lenstr)
        CARTESIAN(Iatom)%element=trim(CLabel)
      end do ! Iatom=2
!
! Now define the bond labels and the map to the define parameter.
      if(Natoms.ge.2)then
      do Iatom=2,Natoms
        Jatom=Zmatrix%atoms(Iatom)%atom1
        Znum=CARTESIAN(Iatom)%Atomic_number
        if(Znum.le.0)cycle
        Ctemp=' '
        CLabel=' '
        write(Ctemp,'(2a)')trim(CARTESIAN(Iatom)%element),trim(CARTESIAN(Jatom)%element)
        call RemoveBlanks (Ctemp, CLabel, lenstr)
        Zmatrix%bond(Iatom)%label=' '
        Zmatrix%bond(Iatom)%label=trim(CLabel)
        IZMVAR=IZMVAR+1
        TZMVAR=TZMVAR+1
        Zmatrix%bond(Iatom)%map=IZMVAR
        Zmatrix%define(TZMVAR)%name=trim(CLabel)
      end do ! Iatom=2
      end if
!
! Handle bond angles.
      Zmatrix%angle(1)%label=' '
      if(Natoms.ge.3)then
      do Iatom=3,Natoms
        Jatom=Zmatrix%atoms(Iatom)%atom1
        Katom=Zmatrix%atoms(Iatom)%atom2
        Ctemp=' '
        CLabel=' '
        write(Ctemp,'(3a)')trim(CARTESIAN(Iatom)%element),trim(CARTESIAN(Jatom)%element), &
                           trim(CARTESIAN(Katom)%element)
        call RemoveBlanks (Ctemp, CLabel, lenstr)
        Zmatrix%angle(Iatom)%label=' '
        Zmatrix%angle(Iatom)%label=trim(CLabel)
        IZMVAR=IZMVAR+1
        TZMVAR=TZMVAR+1
        Zmatrix%angle(Iatom)%map=IZMVAR
        Zmatrix%define(TZMVAR)%name=trim(CLabel)
      end do ! Iatom=3
      end if
!
! Torsion Angles.
      Zmatrix%torsion(1)%label=' '
      if(Natoms.ge.4)then
      do Iatom=4,Natoms
        Jatom=Zmatrix%atoms(Iatom)%atom1
        Katom=Zmatrix%atoms(Iatom)%atom2
        Latom=Zmatrix%atoms(Iatom)%atom3
        Ctemp=' '
        CLabel=' '
        write(Ctemp,'(4a)')trim(CARTESIAN(Iatom)%element),trim(CARTESIAN(Jatom)%element), &
                           trim(CARTESIAN(Katom)%element),trim(CARTESIAN(Latom)%element)
        call RemoveBlanks (Ctemp, CLabel, lenstr)
        Zmatrix%torsion(Iatom)%label=' '
        Zmatrix%torsion(Iatom)%label=trim(CLabel)
        IZMVAR=IZMVAR+1
        TZMVAR=TZMVAR+1
        Zmatrix%torsion(Iatom)%map=IZMVAR
        Zmatrix%define(TZMVAR)%name=trim(CLabel)
      end do ! Iatom=4
      end if
!
      MOLINP='FREEZ'
      call BLD_Zmatrix (LError)
      call PRT_FZM

      Lerror=.false.
      if(NBonds.lt.Zmatrix%Nbonds)then
        Lerror=.true.
        write(UNIout,'(a)')'Insufficient number of bonds found'
        write(UNIout,'(2(a,i8))')'Found ',NBonds,' but need ',Zmatrix%Nbonds
      end if
      if(NAngles.lt.Zmatrix%Nangles)then
        Lerror=.true.
        write(UNIout,'(a)')'Insufficient number of angles found'
        write(UNIout,'(2(a,i8))')'Found ',NAngles,' but need ',Zmatrix%Nangles
      end if
      if(NTorsions.lt.Zmatrix%Ntorsions)then
        Lerror=.true.
        write(UNIout,'(a)')'Insufficient number of torsions found'
        write(UNIout,'(2(a,i8))')'Found ',NTorsions,' but need ',Zmatrix%Ntorsions
      end if
      if(Lerror)stop 'Insufficient number of bonds, angles or torsions found'
!
! Need a better to deal with this
      NMCOOR=TZMVAR
      OPT_parameters%name='Z-MATRIX'
! Make sure to update the cartesians!
      call BLD_cartesians
      call PRT_XYZ

!
! End of routine XYZ_to_Zmatrix
      call PRG_manager ('exit', 'XYZ_to_Zmatrix', 'UTILITY')
      return
      end
      subroutine Find_Closest_Atom (Iatom, JatomOUT)
!***********************************************************************
!     Date last modified: June 26, 1992                                *
!     Author: R.A. Poirier                                             *
!     Description: Calculate interatomic distance matrix in Angstroms  *
!***********************************************************************
! Modules:
      USE program_constants
      USE type_molecule

      implicit none
!
! Local scalars:
      integer :: Iatom,Jatom,JatomOUT
      double precision :: MIN_dist,temp
!
! Begin:
! Calculate distance matrix in bohr.
      temp=100.0D0
      MIN_dist=100.0D0
      do Jatom=1,Iatom-1
        temp=dsqrt((CARTESIAN(Iatom)%X-CARTESIAN(Jatom)%X)**2+ &
                   (CARTESIAN(Iatom)%Y-CARTESIAN(Jatom)%Y)**2+ &
                   (CARTESIAN(Iatom)%Z-CARTESIAN(Jatom)%Z)**2)
      if(temp.gt.MIN_dist)cycle
      MIN_dist=temp
      JatomOUT=Jatom
      end do ! Jatom
!
! End of routine ATOM_DISTANCES
      call PRG_manager ('exit', 'ATOM_DISTANCES', 'UTILITY')
      return
      end
      subroutine GET_atomic_masses
!***********************************************************************
!     Date last modified: May 05, 1999                    Version 1.0  *
!     Author: R.A> Poirier                                             *
!     Description: gets atomic masses for isotopes                     *
!***********************************************************************
!Modules:
      USE program_constants
      USE program_files
      USE type_molecule
      USE atommass

      implicit none
!
! Local scalars:
      integer Iatom
!
      MOL_atoms(1:Natoms)%mass=ZERO
      do Iatom=1,Natoms
        if(CARTESIAN(Iatom)%Atomic_number.le.0)cycle
        if(CARTESIAN(Iatom)%Atomic_number.gt.54)then
          write(UNIout,'(a,i5,a)')'Element not supported for atomic number ',CARTESIAN(Iatom)%Atomic_number
          call PRG_stop ('Element not supported')
        end if
        if(CARTESIAN(Iatom)%Atomic_number.eq.43)then
          write(UNIout,'(a,i5,a)')'Element not supported for atomic number ',CARTESIAN(Iatom)%Atomic_number
          call PRG_stop ('Element not supported')
        end if
        MOL_atoms(Iatom)%mass=Imass_Def(CARTESIAN(Iatom)%Atomic_number)
      end do ! Iatom
!
! End of routine GET_atomic_masses
      return
      end subroutine GET_atomic_masses
