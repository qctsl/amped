      MODULE type_elements
!****************************************************************************************************************************
!     Date last modified: February 9, 2001                                                                     Version 1.0  *
!     Author: R.A. Poirier                                                                                                  *
!     Description: Global parameters associated with an atom (elements)                                                     *
!****************************************************************************************************************************
      implicit none
!
      type type_element
        character(len=2) :: name
        character(len=12) :: symbol
        integer :: atomic_number
        double precision :: mass
        double precision :: BS_radii
        double precision :: vdW_radii
      end type type_element
!
      integer, parameter :: Nelements=92

      type (type_element) element(Nelements)
!
      character(len=2), parameter :: Dummy='D*'
      character(len=2), parameter :: Floating='F*'
!
! Symbols
      character(len=2), dimension(Nelements), parameter :: element_symbols=(/ &
       'H ',                                                                                'He', &
       'Li','Be',                                                  'B ','C ','N ','O ','F ','Ne', &
       'Na','Mg',                                                  'Al','Si','P ','S ','Cl','Ar', &
       'K ','Ca','Sc','Ti','V ','Cr','Mn','Fe','Co','Ni','Cu','Zn','Ga','Ge','As','Se','Br','Kr', &
       'Rb','Sr','Y ','Zr','Nb','Mo','Tc','Ru','Rh','Pd','Ag','Cd','In','Sn','Sb','Te','I ','Xe', &
       'Cs','Ba','La','Ce','Pr','Nd','Pm','Sm','Eu','Gd','Tb','Dy','Ho','Er','Tm','Yb','Lu','Hf', &
       'Ta','W ','Re','Os','Ir','Pt','Au','Hg','Tl','Pb','Bi','Po','At','Rn','Fr','Ra','Ac','Th','Pa','U '/)
!
! Element Names
      character(len=12), dimension(Nelements), parameter :: element_names=(/ &
         'HYDROGEN    ','HELIUM      ','LITHIUM     ','BERYLLIUM   ', &
         'BORON       ','CARBON      ','NITROGEN    ','OXYGEN      ', &
         'FLUORINE    ','NEON        ','SODIUM      ','MAGNESIUM   ', &
         'ALUMINUM    ','SILICON     ','PHOSPHORUS  ','SULFUR      ', &
         'CHLORINE    ','ARGON       ','POTASSIUM   ','CALCIUM     ', &
         'SCANDIUM    ','TITANIUM    ','VANADIUM    ','CHROMIUM    ', &
         'MANGANESE   ','IRON        ','COBALT      ','NICKEL      ', &
         'COPPER      ','ZINC        ','GALLIUM     ','GERMANIUM   ', &
         'ARSENIC     ','SELENIUM    ','BROMINE     ','KRYPTON     ', &
         'RUBIDIUM    ','STRONTIUM   ','YTTRIUM     ','ZIRCONIUM   ', &
         'NIOBIUM     ','MOLYBDENUM  ','TECHNETIUM  ','RUTHENIUM   ', &
         'RHODIUM     ','PALLADIUM   ','SILVER      ','CADMIUM     ', &
         'INDIUM      ','TIN         ','ANTIMONY    ','TELLURIUM   ', &
         'IODINE      ','XENON       ','Cesium      ','Barium      ', &
         'Lanthanum   ','Cerium      ','Praseodymium', &
         'Neodymium   ','Promethium  ','Samarium    ','Europium    ', &
         'Gadolinium  ','Terbium     ','Dysprosium  ','Holmium     ', &
         'Erbium      ','Thulium     ','Ytterbium   ', &
         'Lutetium    ','Hafnium     ','Tantalum    ','Tungsten    ', &
         'Rhenium     ','Osmium      ','Iridium     ','Platinum    ', &
         'Gold        ','Mercury     ','Thallium    ','Lead        ', &
         'Bismuth     ','Polonium    ','Astatine    ','Radon       ', &
         'Francium    ','Radium      ','Actinium    ','Thorium     ', &
         'Protactinium','Uranium     '/)
!
! Default isotopes
      integer DEF_isotope(Nelements)
!                      H He Li Be  B  C  N  O  F Ne
      data DEF_isotope/1, 4, 7, 9,11,12,14,16,19,20, &
!                           Na Mg Al Si  P  S Cl Ar 
                            23,24,27,28,31,32,35,40, &
!                            K Ca Sc Ti  V Cr Mn Fe Co Ni Cu Zn Ga Ge As Se Br Kr 
                            39,40,45,48,51,52,55,56,59,58,63,64,69,74,75,80,79,84, &
!                           Rb Sr  Y Zr Nb Mo Tc Ru  Rh  Pd   Ag  Cd  In  Sn  Sb  Te  I   Xe 
                            85,88,89,90,93,98,97,102,103,106,107,114,115,118,121,130,127,132,38*0/
!
! Scale factor for Bragg-Slater radii
      double precision :: BS_scalef=1.22D0 ! default=1.20D0 (fails for HF)
! BS_scalef=1.273D0 works for HF but breaks others -- see ticket mungauss:#46
!
! These data are taken from J. E. Huheey, Inorganic Chemistry - Principles of
! Structure and reactivity, 2ed., Harper & Row, New York 1978 .
! For the noble gases, the (estimated) covalent radii were used.
! Bragg-Slater radii in Angstroms
      double precision, dimension(Nelements) :: BS_RADII
      data BS_RADII/ &
                   0.25,                                                 0.50,  & !  H-He
                   1.45, 1.05,             0.85, 0.70, 0.65, 0.60, 0.50, 0.65,  & ! Li-Ne
                   1.80, 1.50,             1.25, 1.10, 1.00, 1.00, 1.00, 0.95,  & ! Na-Ar
                   2.20, 1.80,                                                  & !  K-Ca
                   1.60, 1.40, 1.35, 1.40, 1.40, 1.40, 1.35, 1.35, 1.35, 1.35,  & ! Sc-Zn
                                           1.30, 1.25, 1.15, 1.15, 1.15, 1.10,  & ! Ga-Kr
                   2.35, 2.00,                                                  & ! Rb-Sr
                   1.80, 1.55, 1.45, 1.45, 1.35, 1.30, 1.35, 1.40, 1.60, 1.55,  & !  Y-Cd
                                           1.55, 1.45, 1.45, 1.40, 1.37, 1.30,  & ! In-Xe  (I changed from 1.40 to 1.37)
                   38*0.0D0/

! Scale factor for van der Waal radii
      double precision :: vdW_scalef=0.80D0
!
! These data are taken from J. E. Huheey, Inorganic Chemistry - Principles of
! Structure and reactivity, 2nd ed., Harper & Row, New York 1978 .
! If a range of values was given, the smallest was used.
! van der Waal radii in Angstroms
      double precision, dimension(Nelements) :: vdW_RADII
      data vdW_RADII/ &
                   1.20,                                                 1.80,  & !  H-He
                   1.80, 1.40,             1.17, 1.65, 1.55, 1.50, 1.50, 1.60,  & ! Li-Ne
                   2.30, 1.70,             1.82, 2.10, 1.85, 1.80, 1.70, 1.90,  & ! Na-Ar
                   2.80, 2.23,                                                  & !  K-Ca
                   2.09, 2.00, 1.92, 1.85, 1.79, 1.72, 1.67, 1.60, 1.40, 1.40,  & ! Sc-Zn
                                           1.90, 2.00, 2.00, 1.90, 1.80, 2.00,  & ! Ga-Kr
                   2.98, 2.45,                                                  & ! Rb-Sr
                   2.27, 2.16, 2.09, 2.01, 1.95, 1.89, 1.83, 1.60, 1.70, 1.60,  & !  Y-Cd
                                           1.90, 2.20, 2.20, 2.10, 2.12, 2.20,  & ! In-Xe
                   38*0.0D0/

!
! From L. Pauling, The nature of the chemical bond, 3rd ed., Cornell University
! Press, Ithaca, 1960      RADII(33)=2.00 ! As      RADII(51)=2.20 ! Sb
! From the 'xpt' program (atomic radius). Nonmetal radii here are too small.
!      RADII(4) =1.40 ! Be      RADII(5) =1.17 ! B       RADII(13)=1.82 ! Al
!      RADII(20)=2.23 ! Ca      RADII(21)=2.09 ! Sc      RADII(22)=2.00 ! Ti
!      RADII(23)=1.92 ! V       RADII(24)=1.85 ! Cr      RADII(25)=1.79 ! Mn
!      RADII(26)=1.72 ! Fe      RADII(27)=1.67 ! Co      RADII(37)=2.98 ! Rb
!      RADII(38)=2.45 ! Sr      RADII(39)=2.27 ! Y       RADII(40)=2.16 ! Zr
!      RADII(41)=2.09 ! Nb      RADII(42)=2.01 ! Mo      RADII(43)=1.95 ! Tc
!      RADII(44)=1.89 ! Ru      RADII(45)=1.83 ! Rh
!      RADII(32)=2.00 ! Ge (guess)
!
! Minimum valence for each atom
      integer, dimension(Nelements) :: MinValence
      data MinValence/ &
      1,0,&
      1,2,3,2,1,1,1,0,&
      1,2,3,2,1,1,1,0,&
      1,2,3,4,2,2,2,2,2,2,1,2,3,4,3,2,1,0,&
      1,2,3,4,2,2,2,2,2,2,1,2,3,4,3,2,1,0,38*0/
!
! Maximum valence for each atom
      integer, dimension(Nelements) :: MaxVal
      data MaxVal/ &
      1,0,&
      1,2,3,4,3,2,1,0,&
      1,2,3,4,3,2,1,0,&
      1,2,3,4,2,2,2,2,2,2,1,2,3,4,3,2,1,0,&
      1,2,3,4,2,2,2,2,2,2,1,2,3,4,3,2,1,0,38*0/
!
! Maximum valence for each atom
      integer, dimension(Nelements) :: MaxVal2
      data MaxVal2/ &
      1,0,&
      1,2,3,4,3,2,1,0,&
      1,2,3,4,5,4,3,0,&
      1,2,3,4,3,3,3,3,3,3,1,2,3,4,3,2,1,0,&
      1,2,3,4,3,3,3,3,3,3,1,2,3,4,3,2,1,0,38*0/
!
! Maximum valence for each atom
      integer, dimension(Nelements) :: MaxVal3
      data MaxVal3/ &
      1,0,&
      1,2,3,4,3,2,1,0,&
      1,2,3,4,5,6,5,0,&
      1,2,3,4,4,6,4,3,3,3,1,2,3,4,3,2,1,0,&
      1,2,3,4,4,6,4,3,3,3,1,2,3,4,3,2,1,0,38*0/

      end MODULE type_elements
