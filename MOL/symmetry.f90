      subroutine GET_POINT_GROUP
!***********************************************************************
!     Date last modified: November 16, 1997                Version 1.0 *
!     Author: James Xidos, Josj Hollett and R. Poirier                 *
!     Description: Determine the point group of a molecule. In the     *
!                  process of this determination, the molecule is      *
!                  translated and rotated into standard orientation.   *
!***********************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE type_molecule

      implicit none

! Local Arrays:
      double precision :: NUCCC(3)
      double precision :: SEC_MOM(3,3),MOMENTS(3),P_AXES(3,3)
!     double precision, allocatable :: XYZSTD(:,:)
      
!   Local:
      double precision :: diff12,diff13,diff23,TOLER
      integer :: Iatom, i

!   Begin:
      call PRG_manager ('enter', 'GET_POINT_GROUP', 'UTILITY')

      molecule%point_group='????'

      if(Natoms.eq.1)then
!     if(NReal_atoms.eq.1)then
        molecule%point_group='Kh'
      else

      call CENTER_OF_CHARGE (NUCCC)

      if(.not.allocated(XYZSTD))then
        allocate (XYZSTD(Natoms,3))
      else
        deallocate (XYZSTD)
        allocate (XYZSTD(Natoms,3))
      end if

! Move center of charge to origin for standard orientation 
      do Iatom=1,Natoms
        XYZSTD(Iatom,1)=CARTESIAN(Iatom)%X-NUCCC(1)
        XYZSTD(Iatom,2)=CARTESIAN(Iatom)%Y-NUCCC(2)
        XYZSTD(Iatom,3)=CARTESIAN(Iatom)%Z-NUCCC(3)
      end do ! Iatom
 
!   Determine principal axes from diagonalization of second moment of nuclear
!   charge matrix.
      call SEC_MOM_NUC_CHRG
!   Determine whether molecule is a spherical, symmetric, or asymmetric top.
      diff12=dabs(MOMENTS(1)-MOMENTS(2))
      diff13=dabs(MOMENTS(1)-MOMENTS(3))
      diff23=dabs(MOMENTS(2)-MOMENTS(3))
      TOLER=0.00001
 
      If((diff12.LT.TOLER).and.(diff13.LT.TOLER).and.(diff23.LT.TOLER))then
        write(UNIout,*)'Molecule is a spherical top.'
        call SPHERICAL_TOP
      else if((diff12.LT.TOLER).or.(diff13.LT.TOLER).or.(diff23.LT.TOLER))then
        write(UNIout,*)'Molecule is a symmetric top.'
        call SYMMETRIC_TOP
      else
        write(UNIout,*)'Molecule is an asymmetric top.'
        call ASYMMETRIC_TOP
      end if ! Determine type of top

!     write(UNIout,*)'Standard orientation:'
!     do Iatom=1,Natoms
!       write(UNIout,'(i8,3F15.8)')Iatom,XYZSTD(Iatom,1),XYZSTD(Iatom,2),XYZSTD(Iatom,3)
!     end do ! Iatom

!     deallocate (XYZSTD)

      end if ! Natoms.eq.1
!   End of routine GET_POINT_GROUP
      call PRG_manager ('exit', 'GET_POINT_GROUP', 'UTILITY')
      return
!
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine ASYMMETRIC_TOP
!*******************************************************************************
!     Date last modified: October 7, 2002                          Version 1.2 *
!     Author: James Xidos, R. Poirier                                          *
!     Description: Distinguish between the various assymmetric top             *
!                  type point groups and place the molecule in                 *
!                  standard orientation which reflects this symmetry.          *
!                  Possible point groups: D2h, D2, C2v C2h, C2, Ci, Cs, and C1 *
!*******************************************************************************
! Modules:

      implicit none

!   Local:
      logical :: LxisC2, LzisC2, LyisC2,Linverts
      logical :: Lxy_reflects, Lyz_reflects, Lxz_reflects
      double precision :: THETA,xcoord,ycoord,zcoord
      integer :: I,Iatom,atomsony,atomsonz,atomsonx,atomsinxzplane

!   Work Arrays:
      double precision :: XYZW(Natoms,3)

!   Begin:
      call PRG_manager ('enter', 'ASYMMETRIC_TOP', 'UTILITY')

!   Place first principal axis on z-axis
      call ALIGN_PAXES
      call SEC_MOM_NUC_CHRG

!   Calculate angle between principal axis and y-axis
      THETA=PI_VAL/2.0
      if(dabs(P_AXES(2,2)).gt.TOLER)then
        THETA=-datan(P_AXES(1,2)/P_AXES(2,2))
      end if
!   Align other principal axes with x and y-axes 
      call ROTATE_THETA (XYZSTD,'z', THETA)
      call SEC_MOM_NUC_CHRG

!   Determine if z-axis is C2
      XYZW(1:Natoms,1:3)=XYZSTD(1:Natoms,1:3)
      call ROTATE_THETA (XYZW,'z',PI_VAL)
      call EQUIV_MOL (XYZW, LzisC2)
!   Determine if y-axis is C2
      XYZW(1:Natoms,1:3)=XYZSTD(1:Natoms,1:3)
      call ROTATE_THETA (XYZW,'y',PI_VAL)
      call EQUIV_MOL (XYZW, LyisC2)
!   Determine if x-axis is C2
      XYZW(1:Natoms,1:3)=XYZSTD(1:Natoms,1:3)
      call ROTATE_THETA (XYZW,'x',PI_VAL)
      call EQUIV_MOL (XYZW, LxisC2)

      TOLER=0.000001

!   Determine the number of atoms that lie on each axis
          atomsonx=0
          atomsony=0
          atomsonz=0
        do Iatom=1,Natoms
          if(CARTESIAN(Iatom)%atomic_number.le.0.0)cycle
          xcoord=XYZSTD(Iatom,1)
          ycoord=XYZSTD(Iatom,2)
          zcoord=XYZSTD(Iatom,3)
          if((dabs(xcoord).lt.TOLER).and.(dabs(zcoord).lt.TOLER))atomsony=atomsony+1
          if((dabs(xcoord).lt.TOLER).and.(dabs(ycoord).lt.TOLER))atomsonz=atomsonz+1
          if((dabs(ycoord).lt.TOLER).and.(dabs(zcoord).lt.TOLER))atomsonx=atomsonx+1
        end do! Iatom

!   Two C2 axes
      if((LzisC2.and.LyisC2).or.(LzisC2.and.LxisC2).or.(LxisC2.and.LyisC2))then
        molecule%point_group='D2h'
! 
        XYZW(1:Natoms,1:3)=XYZSTD(1:Natoms,1:3)
!
        if(LzisC2.and.LyisC2)then 
          if(atomsony.gt.atomsonz)then
!   Change y-axis to z-axis as it is the principal rotation axis
            do Iatom=1,Natoms
              XYZSTD(Iatom,3)=XYZW(Iatom,2) 
              XYZSTD(Iatom,2)=XYZW(Iatom,3)
            end do ! Iatom
          end if! y principal axis 
        end if! y and z C2 axes
!
        if(LzisC2.and.LxisC2)then
          if(atomsonx.gt.atomsonz)then
!   Change x-axis to z-axis as it is the principal rotation axis
            do Iatom=1,Natoms
              XYZSTD(Iatom,3)=XYZW(Iatom,1)
              XYZSTD(Iatom,1)=XYZW(Iatom,3)
            end do ! Iatom
          end if! x principal axis
        end if! x and z C2 axes
!
        if(LxisC2.and.LyisC2)then
          if(atomsony.gt.atomsonx)then
!   Change y-axis to z-axis as it is the principal rotation axis
            do Iatom=1,Natoms
              XYZSTD(Iatom,3)=XYZW(Iatom,2)
              XYZSTD(Iatom,2)=XYZW(Iatom,3)
            end do ! Iatom
          else
!   Change x-axis to z-axis as it is the principal rotation axis
            do Iatom=1,Natoms
              XYZSTD(Iatom,3)=XYZW(Iatom,1)
              XYZSTD(Iatom,1)=XYZW(Iatom,3)
            end do ! Iatom
          end if! x principal axis
        end if! y and x C2 axes
!
!   Only one C2 axis
      else if(LxisC2.or.LyisC2.or.LzisC2)then
        if(LxisC2)then
!   Change x-axis to z-axis as it is the principal rotation axis
          do Iatom=1,Natoms
            XYZSTD(Iatom,3)=XYZW(Iatom,1)
            XYZSTD(Iatom,1)=XYZW(Iatom,3)
          end do ! Iatom
        end if! x principal axis
        if(LyisC2)then 
!   Change y-axis to z-axis as it is the principal rotation axis
            do Iatom=1,Natoms
              XYZSTD(Iatom,3)=XYZW(Iatom,2)
              XYZSTD(Iatom,2)=XYZW(Iatom,3)
            end do ! Iatom
        end if! y principal axis
!
        XYZW(1:Natoms,1:3)=XYZSTD(1:Natoms,1:3) 
! Planar molecules oriented so that x-axis is normal to plane
        atomsinxzplane=0
        do Iatom=1,Natoms
          if(CARTESIAN(Iatom)%atomic_number.lt.1)cycle
          if(XYZSTD(Iatom,2).lt.TOLER) atomsinxzplane=atomsinxzplane+1
        end do! Iatom
        if(atomsinxzplane.ge.Natoms-Ndummies)then
! Change y-axis to x-axis as it is normal to the plane of the molecule
          do Iatom=1,Natoms
            XYZSTD(Iatom,2)=XYZW(Iatom,1)
            XYZSTD(Iatom,1)=XYZW(Iatom,2)
          end do ! Iatom
        end if! y-axis was normal to plane of molecule
        XYZW(1:Natoms,1:3)=XYZSTD(1:Natoms,1:3)
! 
! Check for C2v, C2h, or C2
        call TEST_REFLECT (XYZW,'z',Lxy_reflects)
        call TEST_REFLECT (XYZW,'x',Lyz_reflects)
        if(Lxy_reflects)then 
          molecule%point_group='C2h'
        else if(Lyz_reflects)then
          molecule%point_group='C2v'
        else
          molecule%point_group='C2'
        end if! C2v,C2h or C2 symmetry
!
      else ! Molecule has no C2 axis
        call TEST_REFLECT (XYZW,'z',Lxy_reflects)
        call TEST_REFLECT (XYZW,'x',Lyz_reflects)
        call TEST_REFLECT (XYZW,'y',Lxz_reflects)
        XYZW(1:Natoms,1:3)=-XYZSTD(1:Natoms,1:3)
        call EQUIV_MOL (XYZW, Linverts)
!
        if(Lxy_reflects.or.Lyz_reflects.or.Lxz_reflects)then
          molecule%point_group='Cs'
        else if(Linverts)then
          molecule%point_group='Ci'
        else
          molecule%point_group='C1'
        end if! Point groups with no C2 axis
      end if!  
 
!   Molecule point group determined and put in standard orientation
! End of routine ASYMMETRIC_TOP
      call PRG_manager ('exit', 'ASYMMETRIC_TOP', 'UTILITY')
      return
      end subroutine ASYMMETRIC_TOP
      subroutine SYMMETRIC_TOP
!***********************************************************************
!     Date last modified: October 7, 2002                  Version 1.2 *
!     Author: James Xidos, R. Poirier                                  *
!     Description: Distinguish between the various symmetric top       *
!                  type point groups and place the molecule in         *
!                  standard orientation according to this symmetry.    *
!***********************************************************************
! Modules:

      implicit none

! Local:
      logical :: Lxy_reflects, Lyz_reflects, Lxz_reflects, Lzimproper
      logical :: LxisCn,LyisCn,LzisCn,Linverts,Laligned
      integer :: attempts,n,orderx,ordery,orderz,i,Iatom,j
      integer :: closestatom
      double precision :: Rangle,THETA,TOLER,Px,Py,Pz,x,y,z,mindist
      double precision :: XYZW(Natoms,3), NUCCC(3)
      
! Begin:
      call PRG_manager ('enter', 'SYMMETRIC_TOP', 'UTILITY')

      TOLER=0.000001D0
      closestatom=1
  
! Place first principal axis on z-axis
      call ALIGN_PAXES
      call SEC_MOM_NUC_CHRG

      XYZW(1:Natoms,1:3)=XYZSTD(1:Natoms,1:3)

! Determine whether linear molecules are C(inf)v or D(inf)h
      if((dabs(MOMENTS(1)).lt.TOLER).or.(dabs(MOMENTS(2)).lt.TOLER).or.(dabs(MOMENTS(3)).lt.TOLER))then
        do Iatom=1,Natoms
          XYZSTD(Iatom,3)=XYZW(Iatom,1)
          XYZSTD(Iatom,1)=XYZW(Iatom,3)
        end do ! Iatom
        XYZW(1:Natoms,1:3)=XYZSTD(1:Natoms,1:3)
        call TEST_REFLECT (XYZW,'z',Lxy_reflects)
        if(Lxy_reflects)then
          molecule%point_group='Dinf_h'
        else
          molecule%point_group='Cinf_v'
        end if! Linear type
      else! Non-Linear Molecules
!
! Determine order of each rotational axis
      attempts=0
   66 orderx=0
      ordery=0
      orderz=0
      attempts=attempts+1

      do n=2,8
! Check x-axis
        Rangle=TWO*PI_VAL/dble(n)
        call ROTATE_THETA (XYZW, 'x', Rangle)
        call EQUIV_MOL (XYZW, LxisCn)
        if(LxisCn)then
          orderx=n
        end if
!   Check y-axis
        call ROTATE_THETA (XYZW, 'y', Rangle)
        call EQUIV_MOL (XYZW, LyisCn)
        if(LyisCn)then
          ordery=n
        end if
!   Check z-axis
        call ROTATE_THETA (XYZW, 'z', Rangle)
        call EQUIV_MOL (XYZW, LzisCn)
        if(LzisCn)then
          orderz=n
        end if
      end do ! n (order of axes)
!
      n=max(orderx,ordery,orderz) 
!
! In the case no rotational axes were found
        if(n.lt.2)then
          THETA=PI_VAL/TWO
          if(dabs(P_AXES(2,2)).gt.TOLER)then
            THETA=-datan(P_AXES(1,2)/P_AXES(2,2))
          end if
!
!   Align other principal axes with x and y-axes
          call ROTATE_THETA (XYZSTD, 'z', THETA)
          call SEC_MOM_NUC_CHRG
          if(attempts.lt.4)then
            go to 66
          else
            write(UNIout,'(a)')'WARNING> SYMMETRIC_TOP: No symmetry axes found'
          end if
        end if ! No rotational axes found
!
!   Symmetric top molecules have only one axis of order higher than 2(Unless they are D2d)
        XYZSTD=XYZW ! NEW!!!
        if((orderx.gt.orderz).and.(orderx.gt.ordery))then ! Change x-axis to z-axis as it is the principal rotation axis
          do Iatom=1,Natoms
            XYZSTD(Iatom,3)=XYZW(Iatom,1)
            XYZSTD(Iatom,1)=XYZW(Iatom,3)
          end do ! Iatom 
        end if! x-axis is highest order  
        if((ordery.gt.orderz).and.(ordery.gt.orderx))then ! Change y-axis to z-axis as it is the principal rotation axis
          do Iatom=1,Natoms
            XYZSTD(Iatom,3)=XYZW(Iatom,2)
            XYZSTD(Iatom,2)=XYZW(Iatom,3)
          end do ! Iatom 
        end if! y-axis is highest order
!
        XYZW(1:Natoms,1:3)=XYZSTD(1:Natoms,1:3)
!
!   Align x-axis
        mindist=100.0
        Laligned=.false.
        do Iatom=1,Natoms 
          if(CARTESIAN(Iatom)%atomic_number.lt.1)cycle
          X=XYZSTD(Iatom,1)
          Y=XYZSTD(Iatom,2)
          Z=XYZSTD(Iatom,3)
          if(X.le.0.0)cycle
          if((dabs(Y).lt.TOLER).and.(X.gt.TOLER))then
            Laligned=.true.
            exit
          end if
          if((dsqrt(X*X+Y*Y+Z*Z).lt.mindist).and.(dabs(X)+dabs(Y).gt.TOLER))then
            mindist=dsqrt(X*X+Y*Y+Z*Z)
            closestatom=Iatom
          end if
        end do! Iatom
!
        if(.not.Laligned)then
          THETA=Pi_val/TWO
          if(XYZSTD(closestatom,1).gt.TOLER)then
            THETA=datan(XYZSTD(closestatom,2)/XYZSTD(closestatom,1))
          end if
          call ROTATE_THETA (XYZSTD, 'z',THETA)
        end if 
!          
        XYZW(1:Natoms,1:3)=XYZSTD(1:Natoms,1:3)
!              
!   Check for mirror planes
        call TEST_REFLECT (XYZW, 'z', Lxy_reflects)
        call TEST_REFLECT (XYZW, 'x', Lyz_reflects)
        call TEST_REFLECT (XYZW, 'y', Lxz_reflects)
!   Check for improper axis
        if(n.ge.1)then
        call ROTATE_THETA (XYZW, 'z', PI_VAL/dble(n))
        call TEST_REFLECT (XYZW, 'z', Lzimproper)
        else
          write(UNIout,'(a)')'WARNING> SYMMETRIC_TOP: Did not attempt to find an improper axis'
        end if
!  
!   If molecule has horizontal and vertical mirror planes it is Dnh
        if((Lxy_reflects).and.((Lyz_reflects).or.(Lxz_reflects)))then
          select case(n)
            case(3)
              molecule%point_group='D3h'
            case(4)
              molecule%point_group='D4h'
            case(5)
              molecule%point_group='D5h'
            case(6)
              molecule%point_group='D6h'
            case default
          end select!Dnh group
!   If molecule has only vertical mirror planes it is Cnv or Dnd
        else if((Lyz_reflects).or.(Lxz_reflects))then
          if(Lzimproper)then
            select case(n)
              case(2)
                molecule%point_group='D2d'
              case(3)
                molecule%point_group='D3d'
              case(4)
                molecule%point_group='D4d'
              case(5)
                molecule%point_group='D5d'
              case(6)
                molecule%point_group='D6d'
            end select!Dnd group
          else
            select case(n)
              case(3)
                molecule%point_group='C3v'
              case(4)
                molecule%point_group='C4v'
              case(5)
                molecule%point_group='C5v'
              case(6)
                molecule%point_group='C6v'
            end select!Cnv group
          end if! Determine between Cnv or Dnd
!   If molecule has only a horizontal mirror plane it is Cnh      
        else if(Lxy_reflects)then
          select case(n)
            case(3)
              molecule%point_group='C3h'
            case(4)
              molecule%point_group='C4h'
            case(5)
              molecule%point_group='C5h'
            case(6)
              molecule%point_group='C6h'
          end select!Cnh group
!   If molecule has no mirror planes it is Cn
        else
          select case(n)
            case(3)
              molecule%point_group='C3'
            case(4)
              molecule%point_group='C4'
            case(5)
              molecule%point_group='C5'
            case(6)
              molecule%point_group='C6'
          end select!Cn group
        end if! Determination of symmetric top point group
      end if! Linear and Non-linear
!   Molecule point group determined and put in standard orientation

! End of routine SYMMETRIC_TOP
      call PRG_manager ('exit', 'SYMMETRIC_TOP', 'UTILITY')
      return
      end subroutine SYMMETRIC_TOP
      subroutine SPHERICAL_TOP
!***********************************************************************
!     Date last modified: November 20, 1997                Version 1.0 *
!     Author: James Xidos                                              *
!     Description: Determine type of spherical top molecule            *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer :: n,orderx,ordery,orderz
      double precision :: Rnew,Theta
      logical :: LxisCn,LyisCn,LzisCn
!
! Input arrays:
      double precision :: XYZW(Natoms,3)
! Work arrays:
      double precision :: Inew(3),Jnew(3),Knew(3),Transform(3,3)
! Local Scalars
      integer :: closestatom,Iatom
      double precision :: mindist,X,Y,Z
      double precision :: TOLER
      parameter (TOLER=0.000001)
!
! Begin:
      call PRG_manager ('enter', 'SPHERICAL_TOP', 'UTILITY')
!
! Need to place atom on an axis to find a Cn axis(n>2).
! May not work for all cases, but I can't think of any right now
!   Find closest atom to origin
      closestatom=1
      mindist=100.0
      do Iatom=1,Natoms
        if(CARTESIAN(Iatom)%atomic_number.lt.1)cycle
        X=XYZSTD(Iatom,1)
        Y=XYZSTD(Iatom,2)
        Z=XYZSTD(Iatom,3)
        if((dabs(X).lt.TOLER).and.(dabs(Y).lt.TOLER))then
          if(dabs(Z).lt.TOLER)then
            cycle ! Atom is on origin, not useful
          else
            closestatom=Iatom
            exit ! Atom is on z-axis as required
          end if
        end if ! Atom on z-axis
        if(dsqrt(X*X+Y*Y+Z*Z).lt.mindist)then
          mindist=dsqrt(X*X+Y*Y+Z*Z)
          closestatom=Iatom
        end if
      end do! Iatom
!
! NOTE not required if found to be above
! Define origin to closestatom as new z-axis, and new x,y-axes
      Knew(1:3)=XYZSTD(closestatom,1:3)
!
      if(dabs(Knew(1)).gt.TOLER.or.dabs(Knew(2)).gt.TOLER)then
        Jnew(1)=-Knew(2)
        Jnew(2)=Knew(1)
        Jnew(3)=ZERO
        Inew(1)=Knew(1)*Knew(3)
        Inew(2)=Knew(2)*Knew(3)
        Inew(3)=-Knew(1)**2-Knew(2)**2
      else
        Jnew(1)=ZERO
        Jnew(2)=-Knew(3)
        Jnew(3)=Knew(2)
        Inew(1)=Knew(2)*Knew(2)+Knew(3)*Knew(3)
        Inew(2)=-Knew(1)*Knew(2)
        Inew(3)=-Knew(1)*Knew(3)
      end if
!
! Normalize vectors
      Rnew=dsqrt(Inew(1)**2+Inew(2)**2+Inew(3)**2)
      Inew(1:3)=Inew(1:3)/Rnew
      Rnew=dsqrt(Jnew(1)**2+Jnew(2)**2+Jnew(3)**2)
      Jnew(1:3)=Jnew(1:3)/Rnew
      Rnew=dsqrt(Knew(1)**2+Knew(2)**2+Knew(3)**2)
      Knew(1:3)=Knew(1:3)/Rnew
! Define Transfomation Matrix
      Transform(1,1:3)=Inew(1:3)
      Transform(2,1:3)=Jnew(1:3)
      Transform(3,1:3)=Knew(1:3)
!
! Transform Standard coordinates
      XYZSTD=transpose(matmul(Transform,transpose(XYZSTD)))
!     XYZSTD(1:Natoms,1:3)=transpose(matmul(Transform(1:3,1:3),transpose(XYZSTD(1:Natoms,1:3))))
!
! Print transformed coordinates
      XYZW(1:Natoms,1:3)=XYZSTD(1:Natoms,1:3)
!
!   Check for order of rotational axis
      orderx=0
      ordery=0
      orderz=0
      do n=2,8
      Theta=TWO*PI_VAL/dble(n)
! Check x-axis
        call ROTATE_THETA (XYZW, 'x', Theta)
        call EQUIV_MOL (XYZW, LxisCn)
        if(LxisCn)then
          orderx=n
        end if
! Check y-axis
        call ROTATE_THETA (XYZW, 'y', Theta)
        call EQUIV_MOL (XYZW, LyisCn)
        if(LyisCn)then
          ordery=n
        end if
! Check z-axis
        call ROTATE_THETA (XYZW, 'z', Theta)
        call EQUIV_MOL (XYZW, LzisCn)
        if(LzisCn)then
          orderz=n
        end if
      end do ! order of axes
!
      n=max(orderx,ordery,orderz)
      select case(n)
        case(3)
          molecule%point_group='Td'
        case(5)
          molecule%point_group='Ih'
        case(4)
          molecule%point_group='Oh'
        case default
          write(UNIout,'(a)')'NOTE: failed to find point group for symmetric top'
          write(UNIout,'(a,i6)')'Found n= ',n
      end select! Spherical top point groups
!
! End of routine SPHERICAL_TOP
      call PRG_manager ('exit', 'SPHERICAL_TOP', 'UTILITY')
      return
      end subroutine SPHERICAL_TOP
      subroutine ALIGN_PAXES
!***********************************************************************
!     Date last modified: October 20, 2006                 Version 1.0 *
!     Author: Joshua W. Hollett                                        *
!     Description: Transform X,Y, and Z axes to P_AXES.                *
!***********************************************************************
! Modules:

      implicit none
!
! Local scalars:
!
! Begin:
      call PRG_manager ('enter', 'ALIGN_PAXES', 'UTILITY')
!
! Transformation Matrix is transpose(P_AXES)
! Transform Standard Coordinates
      XYZSTD=transpose(matmul(transpose(P_AXES),transpose(XYZSTD)))
!
! End of routine ALIGN_PAXES
      call PRG_manager ('exit', 'ALIGN_PAXES', 'UTILITY')
      return
      end subroutine ALIGN_PAXES
      subroutine ROTATE_V2XYZ (TOAXIS)! Axis FRAXIS being rotated to
!***********************************************************************
!     Date last modified: December 2, 1997                 Version 1.0 *
!     Author: James Xidos                                              *
!     Description: Rotate vector P_AXES to axis TOAXIS.                *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      character(len=1) TOAXIS
!
! Work arrays:
      double precision :: T(3,3),WORK(3)
!
! Local scalars:
      integer Iatom,I1,I2,I3,COL,ROW,I,J
      double precision V1,V2,V3,V1M,V2M,V3M,MAGV,V2V2,V3V3,IV2233,SV2V3
!
! Begin:
!     call PRG_manager ('enter', 'ROTATE_V2XYZ', 'UTILITY')
!
      I1 = 0
      I2 = 0
      I3 = 0
      select case (TOaxis)
!     TOAXIS=1, Rotation to x-axis:
      case ('x')
      I1 = 1
      I2 = 2
      I3 = 3
!
!     TOAXIS=2, Rotation to y-axis:
      case ('y')
      I1 = 2
      I2 = 3
      I3 = 1
!
!     TOAXIS=3, Rotation to z-axis:
      case ('z')
      I1 = 3
      I2 = 1
      I3 = 2
      case default
        write(UNIout,'(a)')'ERROR> ROTATE_V2XYZ: TOaxis not x, y or z???'
        call PRG_stop ('ROTATE_V2XYZ: TOaxis not x, y or z???')
      end select
!
      V1 = P_AXES(I1,3)
      V2 = P_AXES(I2,3)
      V3 = P_AXES(I3,3)
!
      MAGV  = dsqrt(V1*V1+V2*V2+V3*V3)
      V1M   = V1/MAGV
      V2M   = V2/MAGV
      V3M   = V3/MAGV
      V2V2  = V2*V2
      V3V3  = V3*V3
      SV2V3=V2V2+V3V3
      if(SV2V3.gt.1.0D-10)then ! ???
      IV2233= ONE/(SV2V3)
!
      T(I1,I1) = V1M
      T(I1,I2) = V2M
      T(I1,I3) = V3M
      T(I2,I1) =-T(I1,I2)
      T(I2,I2) = (V2V2*V1M+V3V3)*IV2233
      T(I2,I3) = V2*V3*(V1M-ONE)*IV2233
      T(I3,I1) =-T(I1,I3)
      T(I3,I2) = T(I2,I3)
      T(I3,I3) = (V3V3*V1M+V2V2)*IV2233
!
!   Evaluate transformation matrix T times vector P_AXES:
      XYZSTD=matmul(XYZSTD,T)
!      do Iatom=1,NAtoms
!        WORK(1:3)=ZERO
!        do I=1,3
!          do J=1,3
!            WORK(I)=WORK(I)+XYZSTD(Iatom,J)*T(J,I)
!          end do
!        end do
!          XYZSTD(Iatom,1:3)=WORK(1:3)
!      end do
      else
        write(UNIout,'(a)')'WARNING> ROTATE_V2XYZ: Did not rotate!!!'
      end if
!
! End of routine ROTATE_V2XYZ
!     call PRG_manager ('exit', 'ROTATE_V2XYZ', 'UTILITY')
      return
      end subroutine ROTATE_V2XYZ
!
      subroutine TEST_REFLECT (XYZW,    & ! work array (Natoms X 3)
                               AXIS,    & ! axis to be checked for symmetry
                               LREFLCT)! Logical symmetry
!***********************************************************************
!     Date last modified: December 7, 1997                 Version 1.0 *
!     Author: James Xidos                                              *
!     Description: Check for reflection symmetry about the AXIS-axis   *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      character(len=1) AXIS
      logical LREFLCT
!
! Work arrays:
      double precision XYZW(Natoms,3)
!
! Local scalars:
      integer Iatom,XYZ
!
! Begin:
!     call PRG_manager ('enter', 'TEST_REFLECT', 'UTILITY')
!
! Reflect molecule across the AXIS-axis:
!
      do Iatom=1,Natoms
        select case(AXIS)
          case('x')
            XYZW(Iatom,1)=-XYZSTD(Iatom,1)
          case('y')
            XYZW(Iatom,2)=-XYZSTD(Iatom,2)
          case('z')
            XYZW(Iatom,3)=-XYZSTD(Iatom,3) 
        end select
      end do! Iatom
!
      call EQUIV_MOL (XYZW, LREFLCT)
!
! End of routine TEST_REFLECT
!     call PRG_manager ('exit', 'TEST_REFLECT', 'UTILITY')
      return
      end subroutine TEST_REFLECT
      subroutine SEC_MOM_NUC_CHRG
!***********************************************************************
!     Date last modified: December 5, 1997                 Version 1.0 *
!     Author: James Xidos                                              *
!     Description: Determine the 3 moments of nuclear charge and the   *
!                  principle axis of nuclear charge corresponding to   *
!                  them                                                *
!***********************************************************************
! Modules:

      implicit none
!
! Work arrays
!
! Local scalars:
      integer :: Iatom
      double precision :: X,Y,Z,ZATOM
!
! Begin:
      call PRG_manager ('enter', 'SEC_MOM_NUC_CHRG', 'UTILITY')
!
      SEC_MOM=ZERO

      do Iatom=1,NAtoms
        if(CARTESIAN(Iatom)%atomic_number.le.0)cycle
          ZATOM=DBLE(CARTESIAN(Iatom)%atomic_number)
          X=XYZSTD(Iatom,1)
          Y=XYZSTD(Iatom,2)
          Z=XYZSTD(Iatom,3)
! XX
          SEC_MOM(1,1)=SEC_MOM(1,1)+ZATOM*(Y*Y+Z*Z)
! XY
          SEC_MOM(2,1)=SEC_MOM(2,1)-ZATOM*X*Y
! YY
          SEC_MOM(2,2)=SEC_MOM(2,2)+ZATOM*(X*X+Z*Z)
! XZ
          SEC_MOM(3,1)=SEC_MOM(3,1)-ZATOM*X*Z
! YZ
          SEC_MOM(3,2)=SEC_MOM(3,2)-ZATOM*Y*Z
! ZZ
          SEC_MOM(3,3)=SEC_MOM(3,3)+ZATOM*(X*X+Y*Y)
      end do ! Iatom
!
! Diagonolize SEC_MOM to get 3 eigenvalues (MOMENTS) + 3 eigenvectors
! (principle axes,PRIM_AXES). Note that upom return,
!     MOMENTS(1) < MOMENTS(2) < MOMENTS(3)
!
      call MATRIX_diagonalize (SEC_MOM, P_AXES, MOMENTS, 3, 2, .true.)
!
! End of routine SEC_MOM_NUC_CHRG
      call PRG_manager ('exit', 'SEC_MOM_NUC_CHRG', 'UTILITY')
      return
      end subroutine SEC_MOM_NUC_CHRG
      subroutine ROTATE_THETA (XYZW, & ! Cartesian coordinates
                               AXIS,   & ! Axis of rotation
                               THETA) ! Angle of rotation
!***********************************************************************
!     Date last modified: December 5, 1997                 Version 1.0 *
!     Author: James Xidos                                              *
!     Description: Rotate molecule THETA degrees counterclockwise      *
!                  around the AXIS-axis                                *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      character(len=1) :: AXIS
      double precision :: THETA
!
! Input arrays:
      double precision XYZW(Natoms,3)
!
! Work arrays:
      double precision T(3,3),WORK(3)
!
! Local scalars:
      integer I1,I2,I3,Iatom,COL,ROW,XYZ
      double precision CTHETA,STHETA
!
! Begin:
      call PRG_manager ('enter', 'ROTATE_THETA', 'UTILITY')
!
      I1 = 0
      I2 = 0
      I3 = 0
      select case (AXIS)
! AXIS=1, counterclockwise rotation around x-axis:
      case ('x')
      I1 = 1
      I2 = 2
      I3 = 3
!
! AXIS=2, counterclockwise rotation around y-axis:
      case ('y')
      I1 = 2
      I2 = 3
      I3 = 1
!
! AXIS=3, counterclockwise rotation around z-axis:
      case ('z')
      I1 = 3
      I2 = 1
      I3 = 2
!
      end select
!
      STHETA=dsin(THETA)
      CTHETA=dcos(THETA)
!
      T(I1,I1) = ONE
      T(I1,I2) = ZERO
      T(I1,I3) = ZERO
      T(I2,I1) = ZERO
      T(I2,I2) = CTHETA
      T(I2,I3) = STHETA
      T(I3,I1) = ZERO
      T(I3,I2) =-STHETA
      T(I3,I3) = CTHETA
!
      do Iatom=1,NAtoms
        do ROW=1,3
          WORK(ROW)=ZERO
          do COL=1,3
            WORK(ROW)=WORK(ROW)+T(ROW,COL)*XYZW(Iatom,COL)
          end do
        end do
        do XYZ=1,3
          XYZW(Iatom,XYZ)=WORK(XYZ)
        end do
      end do
!
! End of routine ROTATE_THETA
      call PRG_manager ('exit', 'ROTATE_THETA', 'UTILITY')
      return
      end subroutine ROTATE_THETA
      subroutine EQUIV_MOL (XYZW,   & ! work array (Natoms X 3)
                            LEQMOL)! logical, identical molecules?
!***********************************************************************
!     Date last modified: December 7, 1997                 Version 1.0 *
!     Author: James Xidos                                              *
!     Description: Determine whether two sets of Cartesian coordinates *
!                  represent equivalent molecules, answer returned in  *
!                  logical LEQMOL                                       *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      logical :: LEQMOL
!
! Input arrays:
      double precision XYZW(Natoms,3)
!
! Local scalars:
      integer Iatom,Jatom
      double precision :: DIFF
      logical :: FOUND
!
! Local parameters:
      double precision :: TOLER
      parameter (TOLER=0.000001D0)
!
! Begin:
!     call PRG_manager ('enter', 'EQUIV_MOL', 'UTILITY')
!
      LEQMOL=.true.
      Iatom=1
       do while(Iatom.le.Natoms)
         if(CARTESIAN(Iatom)%atomic_number.gt.0)then ! skip dummies
           FOUND=.false.
           do Jatom=1,Natoms
             if(CARTESIAN(Iatom)%atomic_number.eq.CARTESIAN(Jatom)%Atomic_number)then
               DIFF=dabs(XYZW(Iatom,1)-XYZSTD(Jatom,1))
               if(DIFF.le.TOLER)then
                 DIFF=dabs(XYZW(Iatom,2)-XYZSTD(Jatom,2))
                 if(DIFF.le.TOLER)then
                   DIFF=dabs(XYZW(Iatom,3)-XYZSTD(Jatom,3))
                   if(DIFF.le.TOLER)then
                     FOUND=.true.
                   end if
                 end if
               end if
             end if
           end do ! Jatom
           if(.not.FOUND)then
             LEQMOL=.false.
             exit
           end if
         end if ! skip dummies
         Iatom=Iatom+1
       end do ! LEQMOL,Iatom

! Restore the work array to standard orientation
       XYZW(1:Natoms,1:3)=XYZSTD(1:Natoms,1:3)
!
! End of routine EQUIV_MOL
!     call PRG_manager ('exit', 'EQUIV_MOL', 'UTILITY')
      return
      end subroutine EQUIV_MOL
      end subroutine GET_POINT_GROUP
      subroutine CENTER_OF_MASS (CM)
!***********************************************************************
!     Date last modified: November 10, 1997                Version 1.0 *
!     Author: James Xidos                                              *
!     Description: Calculates the center of mass of the molecule.      *
!                  Result is in atomic units (bohr)                    *
!***********************************************************************
! Modules:
      USE type_molecule

      implicit none
!
! Output array:
      double precision CM(3)
!
! Local scalars:
      integer :: Iatom
      double precision :: MassTot,SUMx,SUMy,SUMz
!
! Begin:
      call PRG_manager ('enter', 'CENTER_OF_MASS', 'UTILITY')
!
!     call GET_object ('MOL', 'ATOMIC', 'MASSES')
      call GET_atomic_masses
!
!     MassTot = sum of atomic masses in the molecule
!     SUM = sum of (atomic mass X Cartesian coordinate) terms
      MassTot = 0.0D0
      SUMx = 0.0D0
      SUMy = 0.0D0
      SUMz = 0.0D0
!
      do Iatom = 1,NAtoms
        if(CARTESIAN(Iatom)%atomic_number.gt.0)then ! Do not count dummies!
          SUMx=SUMx+MOL_atoms(Iatom)%mass*CARTESIAN(Iatom)%X
          SUMy=SUMy+MOL_atoms(Iatom)%mass*CARTESIAN(Iatom)%Y 
          SUMz=SUMz+MOL_atoms(Iatom)%mass*CARTESIAN(Iatom)%Z
          MassTot = MassTot + MOL_atoms(Iatom)%mass ! Calculate MassTot
        end if
      end do

      CM(1) = SUMx/MassTot
      CM(2) = SUMy/MassTot
      CM(3) = SUMz/MassTot
!
! End of routine CENTER_OF_MASS
      call PRG_manager ('exit', 'CENTER_OF_MASS', 'UTILITY')
      return
      end subroutine CENTER_OF_MASS
      subroutine CENTER_OF_CHARGE (NUCCC)
!***********************************************************************
!     Date last modified: November 15, 1997                Version 1.0 *
!     Author: James Xidos                                              *
!     Description: Calculates the center of charge of the molecule.    *
!                  Result is in atomic units (bohr)                    *
!***********************************************************************
! Modules:
      USE program_constants
      USE type_molecule

      implicit none
!
! Output array:
      double precision NUCCC(3)
!
! Local scalars:
      integer Iatom
      double precision SUMx,SUMy,SUMz,ZTOT
!
! Begin:
      call PRG_manager ('enter', 'CENTER_OF_CHARGE', 'UTILITY')
!
!     ZTOT = sum of nuclear charges in the molecule
!     SUM = sum of (atomic charge X Cartesian coordinate) terms
      ZTOT = ZERO
      SUMx=ZERO
      SUMy=ZERO
      SUMz=ZERO
      do Iatom = 1,NAtoms
! Do not count dummies!
        if(CARTESIAN(Iatom)%atomic_number.gt.0)then
          SUMx=SUMx+dble(CARTESIAN(Iatom)%atomic_number)*CARTESIAN(Iatom)%X
          SUMy=SUMy+dble(CARTESIAN(Iatom)%atomic_number)*CARTESIAN(Iatom)%Y
          SUMz=SUMz+dble(CARTESIAN(Iatom)%atomic_number)*CARTESIAN(Iatom)%Z
          ZTOT=ZTOT+dble(CARTESIAN(Iatom)%atomic_number)
        end if
      end do
         NUCCC(1) = SUMx/ZTOT
         NUCCC(2) = SUMy/ZTOT
         NUCCC(3) = SUMz/ZTOT
!
! End of routine CENTER_OF_CHARGE
      call PRG_manager ('exit', 'CENTER_OF_CHARGE', 'UTILITY')
      return
      end subroutine CENTER_OF_CHARGE
