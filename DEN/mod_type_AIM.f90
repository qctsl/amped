      module AIM_objects
!*****************************************************************************************************************
!     Date last modified: February 16, 2000                                                         Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Contains Atoms In Molecule objects                                                            *
!*****************************************************************************************************************
! Modules:

      implicit none
!
! Scalars
       integer :: Icoeff
       integer :: MAX_Apts   ! Maximum number of angular points/atom
       integer :: Atom_num   ! The atom to be fitted
       double precision :: FromVAL,ToVAL,ByVAL
       character(len=8) :: AIM_weights
       logical :: LScanXI
!
! Arrays
      double precision, dimension(:,:), allocatable :: Patoms ! Results for all atoms
      double precision, dimension(:,:), allocatable :: Satoms ! Results for all atoms
      double precision, dimension(:,:), allocatable :: P_j    ! The density matrices for each MO j
      double precision, dimension(:), allocatable :: AN_elec
      double precision, dimension(:), allocatable :: Gprod
      double precision, dimension(:), allocatable :: Patomic ! MATlen by Nbasis (For G10S, for a given atom)
      double precision, dimension(:), allocatable :: Satomic
      double precision, dimension(:), allocatable :: f_j     ! Fractional occupancies
      double precision, dimension(:,:), allocatable :: rhoj_r  ! rho_j(r) for all MO

      double precision, dimension(36,36):: XIval

      type  :: type_charge_density
        double precision    :: r
        double precision    :: rho_r
      end type type_charge_density

      type(type_charge_density), dimension(:,:), allocatable :: Adensity ! Atomic densities

      double precision, dimension(10,10) :: He_Coeff
      data (He_Coeff(Icoeff,1),Icoeff=1,10)/ &
      0.00005934,0.00046289,0.00242246,0.00999457,0.03424914, &
      0.09630244,0.21200599,0.34614653,0.35196448,0.11988049/
      data (He_Coeff(Icoeff,2),Icoeff=1,10)/ &
      -0.00003055,-0.00006992,-0.00165420,0.00030266,-0.03097261, &
       0.03048551,-0.30590927,0.47802816,-1.82215239,2.03982693/
      data (He_Coeff(Icoeff,3),Icoeff=1,10)/ &
      -0.00001764,-0.00070002,0.00063211,-0.02137207,0.03382735, &
      -0.31591813,0.60361910,-2.96431511,4.15074826,-1.74670404/
      data (He_Coeff(Icoeff,4),Icoeff=1,10)/ &
      -0.00016208,0.00041622,-0.01074242,0.02764290,-0.24026882, &
       0.60940364,-3.44516139,5.18077821,-3.39464698,1.03187951/
      data (He_Coeff(Icoeff,5),Icoeff=1,10)/ &
       0.00011521,-0.00422360,0.01728083,-0.15619451,0.53311845, &
      -3.42665493,5.22392406,-4.03242048,2.02818857,-0.57709139/
      data (He_Coeff(Icoeff,6),Icoeff=1,10)/ &
      -0.00109175,0.00822882,-0.08960567,0.41438177,-3.06428533, &
       4.59090841,-3.78902155,2.33996908,-1.12247945,0.32088683/
      data (He_Coeff(Icoeff,7),Icoeff=1,10)/ &
       0.00246141,-0.04326113,0.28607116,-2.55541504,3.61452034, &
      -3.02753334,2.07955118,-1.24643460,0.60345132,-0.17455374/
      data (He_Coeff(Icoeff,8),Icoeff=1,10)/ &
      -0.01459397,0.16955190,-2.04801597,2.61200520,-2.11551300, &
       1.53373307,-1.04037686,0.63156855,-0.30930190,0.09009579/
      data (He_Coeff(Icoeff,9),Icoeff=1,10)/ &
       0.07503997,-1.60901176,1.73573984,-1.30174824,0.95180694, &
      -0.69233096,0.47658719,-0.29230560,0.14398149,-0.04206551/
      data (He_Coeff(Icoeff,10),Icoeff=1,10)/ &
       1.26213621,-0.99865540,0.66206505,-0.47039049,0.34942342, &
      -0.25788143,0.17896321,-0.11021948,0.05440240,-0.01590994/
      end
