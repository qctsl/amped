      subroutine MENU_SXI
!***********************************************************************
!     Date last modified: June 10, 2004                                *
!     Author: R. A. Poirier                                            *
!     Description: Set parameters for Gausian execution.               *
!***********************************************************************
! Modules:
      USE program_manager
      USE program_parser
      USE menu_gets
      USE AIM_objects

      implicit none
!
! Local scalars:
      logical done
!
! Begin:
      call PRG_manager ('enter', 'MENU_SXI', 'UTILITY')
      done=.false.
!
! Menu:
      do while (.not.done)
      call new_token ('SXI:')
!
      if(token(1:4).eq.'HELP')then
         write(uniout,'(a)') &
      ' Command SXI:', &
      '  Purpose: Perform a grid of XI values ', &
      '  Syntax:', &
      '  SXI', &
      '  FRom =  <real>: start value for scan', &
      '  TO =  <real>: final value for scan', &
      '  BY =  <real>: step size value for scan', &
      '  end'
!
      else if(token(1:2).EQ.'FR' )then
        call GET_value (FromVAL)

      else if(token(1:2).EQ.'TO' )then
        call GET_value (ToVAL)

      else if(token(1:2).EQ.'BY' )then
        call GET_value (ByVAL)

      else
        call sortie (done)

      end if
!
      end do !(.not.done)
!
! End of routine MENU_SXI
      call PRG_manager ('exit', 'MENU_SXI', 'UTILITY')
      return
      end subroutine MENU_SXI
