      subroutine BLD_LDMatrix_ABIM
!****************************************************************************************
!     Date last modified: June 13, 2016                                                 *
!     Authors: R.A. Poirier                                                             *
!     Description:                                                                      *
!     Calculate the Localized-Delocalized matrix using weights.                         *
!****************************************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE type_NI_Nelec_AB
      USE QM_objects
      USE QM_defaults
      USE NI_defaults
      USE type_density
      USE type_plotting
      USE matrix_print

      implicit none
!
! Input scalars:
!
! Local scalars:
      integer :: Iatom,Jatom,NoccMO,ZNumI,ZNumJ
      double precision :: Nelec
!
! Local function:
!
! Begin:
      call PRG_manager ('enter','BLD_LDMatrix_ABIM', 'LDMATRIX%ABIM')
!
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)
      call GET_object ('QM','ALL_BONDS_ELECTRONS','ABIM')
!
      LDM_type='DM_ABIM'
      if(.not.allocated(LDMw))then
        allocate (LDMw(Natoms,Natoms))
      else
        deallocate (LDMw)
        allocate (LDMw(Natoms,Natoms))
      end if
!
! Initialize LDMw to N_elec_AB
      LDMw=N_elec_AB
      do Iatom=1,Natoms
      ZnumI=CARTESIAN(Iatom)%Atomic_number
      if(ZnumI.le.0)cycle
      
      do Jatom=1,Natoms
      ZnumJ=CARTESIAN(Jatom)%Atomic_number
      if(ZnumJ.le.0)cycle
      if(Iatom.eq.Jatom)cycle
!     LDMw(Iatom,Iatom)=LDMw(Iatom,Iatom)-N_elec_AB(Iatom,Jatom)
      LDMw(Iatom,Iatom)=LDMw(Iatom,Iatom)-N_elec_A_AB(Iatom,Jatom) ! Electrons contributed to the bond by Jatom.
      LDMw(Iatom,Jatom)=N_elec_A_AB(Iatom,Jatom)
      end do ! Jatom
      end do ! Iatom

      Nelec=ZERO
      do Iatom=1,Natoms
      ZnumI=CARTESIAN(Iatom)%Atomic_number
      if(ZnumI.le.0)cycle
      LDMw(Iatom,Iatom)=ZERO  ! For DM case
      do Jatom=1,Natoms
      ZnumJ=CARTESIAN(Jatom)%Atomic_number
      if(ZnumJ.le.0)cycle
      Nelec=Nelec+LDMw(Iatom,Jatom)
      end do ! Jatom
      end do ! Iatom

      write(UNIout,'(a)')'DM '
      Call PRT_matrix (LDMw, Natoms, Natoms)
      write(UNIout,'(a,f12.6)')'Total number of electrons: ',Nelec

      call BLD_LDMatrixW_SuperAtom
!
! End of routine BLD_LDMatrix_ABIM
      call PRG_manager ('exit', 'BLD_LDMatrix_ABIM', 'LDMATRIX%ABIM')
      return
      end subroutine BLD_LDMatrix_ABIM
