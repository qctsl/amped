      subroutine MENU_AIM
!***********************************************************************
!     Date last modified: June 10, 2004                                *
!     Author: R. A. Poirier                                            *
!     Description: Set parameters for Gausian execution.               *
!***********************************************************************
! Modules:
      USE program_manager
      USE program_parser
      USE menu_gets
      USE module_grid_points
      USE AIM_objects

      implicit none
!
! Local scalars:
      logical done
!
! Begin:
      call PRG_manager ('enter', 'MENU_AIM', 'UTILITY')
      done=.false.
!
! Menu:
      do while (.not.done)
      call new_token ('AIM:')
!
      if(token(1:4).eq.'HELP')then
         write(uniout,'(a)') &
      ' Command AIM:', &
      '  Purpose: Perform a grid of XI values ', &
      '  Syntax:', &
      '  AIM', &
      '  ATOM =  <real>: start value for scan', &
      '  CYcles = <integer>: Maximum cycles for find critical points', &
      '  end'
!
      else if(token(1:2).EQ.'AT' )then
        call GET_value (Atom_num)
!
      else if(token(1:2).EQ.'CY' )then
        call GET_value (MAXCycles)

      else
        call MENU_end (done)

      end if
!
      end do !(.not.done)
!
! End of routine MENU_AIM
      call PRG_manager ('exit', 'MENU_AIM', 'UTILITY')
      return
      end subroutine MENU_AIM
