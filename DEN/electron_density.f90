      subroutine DENSITY_MESH
!*****e**********************************************************************************
!     Date last modified: August 15, 2012                                               *
!     Authors: R.A. Poirier                                                             *
!     Description:                                                                      *
!                 Calculate the charge density on a set of grid points (mesh)           *
!****************************************************************************************
! Modules:
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE module_grid_points
      USE type_density
      USE type_molecule
      USE type_plotting

      implicit none
!
! Local scalars:
      integer :: Ipoint
      double precision :: TraceAB
      double precision, dimension(:), allocatable :: AOprod
!
! Begin:
      call PRG_manager ('enter','DENSITY_MESH', 'DENSITY%MESH')
!
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)
!
      allocate (AOprod(1:MATlen))
      allocate(CHARGE_DENSITY(NGridPoints))
!
! Get a temporary file to store plotting data	  
      PlotFunction='rho(x,y,z)'
      if(MoleculePrint.or.AtomPrint)then
        write(PlotFileID,'(a)')'DENSITY_MESH'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
      else
        Plotfile_unit=UNIout
      end if

      do Ipoint=1,NGridPoints
        call Gaussian_products (grid_points(Ipoint)%x, grid_points(Ipoint)%y, grid_points(Ipoint)%z, AOprod, MATlen)
        CHARGE_DENSITY(Ipoint)=TraceAB (PM0, AOprod, NBasis)
      end do ! Ipoint
      fmt_rho(1:)='f20.6'
      call PRT_GRID_MOL (CHARGE_DENSITY, NGridPoints, fmt_rho)
!
      if(MoleculePrint.or.AtomPrint)close (unit=Plotfile_unit)

      deallocate (AOprod,CHARGE_DENSITY)
!
! End of routine DENSITY_MESH
      call PRG_manager ('exit', 'DENSITY_MESH', 'DENSITY%MESH')
      return
      end subroutine DENSITY_MESH
      subroutine DENSITY_BOND
!********************************************************************
!     Date last modified: July 31, 2014                           *
!     Authors: R.A. Poirier                                         *
!     Description: Calculate the density belonging to bonds         *
!********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE type_density
      USE module_grid_points
      USE type_plotting

      implicit none
!
! Local scalars:
      integer :: Aatom,Batom,Ipoint,ZA,ZB,lenstr
      double precision :: BweightA,BweightB,WAB
      double precision :: XVal,YVal,ZVal,ABcharge
      character(len=32):: Ctemp

      double precision :: GET_density_at_xyz
!
! Begin:
      call PRG_manager ('enter','DENSITY_BOND', 'DENSITY%BOND')
!
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)
!
      PlotFileID=' '
      allocate(Rden(NGridPoints))
! Now build a mesh based on the radial grids
      call BLD_BeckeW_XI
!
      do Aatom=1,Natoms
        ZA=CARTESIAN(Aatom)%Atomic_number
        if(ZA.le.0)cycle
      do Batom=Aatom,Natoms
        if(Aatom.eq.Batom)cycle
        ZB=CARTESIAN(Batom)%Atomic_number
        if(ZB.le.0)cycle
        write(Ctemp,'(a,i4,a,i4)')'DENSITY_BOND_',Aatom,'_',Batom
        call RemoveBlanks (Ctemp, PlotFileID, lenstr)
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a,i4,a,i4)')'Density for Bond between atom ',Aatom,' and ',Batom
! A-B (should add A-B + B-A)
      do Ipoint=1,NGridPoints
         XVal=grid_points(Ipoint)%x
         YVal=grid_points(Ipoint)%y
         ZVal=grid_points(Ipoint)%z
          call GET_Weight1 (XVal, YVal, ZVal, Aatom, BweightA)
          call GET_Weight1 (XVal, YVal, ZVal, Batom, BweightB)
          WAB=dsqrt(BweightA*BweightB)
          Rden(Ipoint)=WAB*GET_density_at_xyz (XVal, YVal, ZVal)
        end do ! Ipoint
      call PRT_GRID_MOL (Rden, NGridPoints, fmt_rho)
      close (unit=Plotfile_unit)
      end do ! Batom
      end do ! Aatom

      deallocate(Rden)
!
! End of routine DENSITY_BOND
      call PRG_manager ('exit', 'DENSITY_BOND', 'DENSITY%BOND')
      return
      end subroutine DENSITY_BOND
      subroutine DEL_DENSITY_MESH
!******************************************************************************
!     Date last modified: August 15, 2012                                     *
!     Authors: R.A. Poirier                                                   *
!     Description:                                                            *
!                 Calculate DEL charge density on a set of grid points (mesh) *
!******************************************************************************
! Modules:
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE module_grid_points
      USE type_density
      USE type_molecule
      USE type_plotting

      implicit none
!
! Local scalars:
      integer :: Ipoint
      double precision :: Xmin,Ymin,Zmin,DXmin,DYmin,DZmin,MinGL
      double precision :: TraceAB
      double precision, dimension(:), allocatable :: GradLen
      double precision, dimension(:), allocatable  :: dAOprodX,dAOprodY,dAOprodZ
!
! Begin:
      call PRG_manager ('enter','DEL_DENSITY_MESH', 'DEL_DENSITY%MESH')
!
      call GET_object ('QM', 'CMO', Wavefunction)

      allocate (dAOprodx(1:MATlen))
      allocate (dAOprody(1:MATlen))
      allocate (dAOprodz(1:MATlen))
      allocate(DEL_DENSITY_X(NGridPoints))
      allocate(DEL_DENSITY_Y(NGridPoints))
      allocate(DEL_DENSITY_Z(NGridPoints))  
      allocate (delrho(NGridPoints))
      allocate (GradLen(NGridPoints))

! Get a temporary file to store plotting data	  
      if(MoleculePrint.or.AtomPrint)then
        write(PlotFileID,'(a)')'DEL_DENSITY_MESH'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
      else
        Plotfile_unit=UNIout
      end if
!
      write(Plotfile_unit,'(a)')'The first derivatives of density:'
      write(Plotfile_unit,'(a)')'X,Y,Z,dX,dY,dZ:'
      MinGL=999.0D0
      DXmin=999.0D0
      DYmin=999.0D0
      DZmin=999.0D0
      Xmin=999.0D0
      Ymin=999.0D0
      Zmin=999.0D0

      do Ipoint=1,NGridPoints
        call dG_products (grid_points(Ipoint)%x, grid_points(Ipoint)%y, grid_points(Ipoint)%z, &
                          dAOprodX, dAOprodY, dAOprodZ, MATlen)
        DEL_DENSITY_X(Ipoint)=TraceAB (PM0, dAOprodx, NBasis)
        DEL_DENSITY_Y(Ipoint)=TraceAB (PM0, dAOprody, NBasis)
        DEL_DENSITY_Z(Ipoint)=TraceAB (PM0, dAOprodz, NBasis)
        delrho(Ipoint)%X=DEL_DENSITY_X(Ipoint)
        delrho(Ipoint)%Y=DEL_DENSITY_Y(Ipoint)
        delrho(Ipoint)%Z=DEL_DENSITY_Z(Ipoint)
        delrho(Ipoint)%length=dsqrt((delrho(Ipoint)%X)**2+(delrho(Ipoint)%Y)**2+(delrho(Ipoint)%Z)**2)
        GradLen(Ipoint)=delrho(Ipoint)%length
         if(delrho(Ipoint)%length.lt.MinGL)then
           MinGL=delrho(Ipoint)%length
           DXmin=delrho(Ipoint)%X
           DYmin=delrho(Ipoint)%y
           DZmin=delrho(Ipoint)%z
           Xmin=grid_points(Ipoint)%x
           Ymin=grid_points(Ipoint)%y
           Zmin=grid_points(Ipoint)%z
         end if

        write(Plotfile_unit,Pfmt_drho)grid_points(Ipoint)%x,grid_points(Ipoint)%y,grid_points(Ipoint)%z, &
                            DEL_DENSITY_X(Ipoint),DEL_DENSITY_Y(Ipoint),DEL_DENSITY_Z(Ipoint)
      end do ! Ipoint
      write(UNIout,'(a,f12.6)')'Minimum gradient length: ',MinGL
      write(UNIout,'(a,3f12.6)')'Gradient: ',DXmin,DYmin,DZmin
      write(UNIout,'(a,3f12.6)')'Grid point: ',Xmin,Ymin,Zmin
      if(MoleculePrint.or.AtomPrint)close (unit=Plotfile_unit)
! print gradient length of density into a separate file if external output is set
     if(GrdLenPrint)then
       PlotFileID=' '
       write(PlotFileID,'(a)')'GRDLEN_RHO_DENSITY_MESH'
       call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
       call PRT_GRID_MOL (GradLen, NGridPoints, fmt_rho)
       close (unit=Plotfile_unit)
      end if

      deallocate (dAOprodx, dAOprody, dAOprodz)
!
! End of routine DEL_DENSITY_MESH
      call PRG_manager ('exit', 'DEL_DENSITY_MESH', 'DEL_DENSITY%MESH')
      return
      end subroutine DEL_DENSITY_MESH
      subroutine DEL2_DENSITY_MESH
!***********************************************************************************
!     Date last modified: August 15, 2012                                          *
!     Authors: R.A. Poirier                                                        *
!     Description:                                                                 *
!                Calculate DEL2 charge density on a set of grid points (mesh)      *
!***********************************************************************************
! Modules:
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE module_grid_points
      USE module_type_basin
      USE type_density
      USE type_molecule
      USE type_plotting

      implicit none
!
! Local scalars:
      integer :: Ipoint,File_unit
      double precision :: NEGdel2rho
      double precision :: TraceAB
  
! Local Array	  
      double precision, dimension(3,3) :: Hess, EigVector
      double precision, dimension(3) :: EigValue
!
! Begin:
      call PRG_manager ('enter','DEL2_DENSITY_MESH', 'DEL2_DENSITY%MESH')
!
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)

!     allocate (d2AOprodxx(1:MATlen))
!     allocate (d2AOprodyy(1:MATlen))
!     allocate (d2AOprodzz(1:MATlen))
!     allocate (d2AOprodxy(1:MATlen))
!     allocate (d2AOprodxz(1:MATlen))
!     allocate (d2AOprodyz(1:MATlen))   

      allocate(DEL2_DENSITY_XX(NGridPoints))
      allocate(DEL2_DENSITY_YY(NGridPoints))
      allocate(DEL2_DENSITY_ZZ(NGridPoints))
      allocate(DEL2_DENSITY_XY(NGridPoints))
      allocate(DEL2_DENSITY_XZ(NGridPoints))
      allocate(DEL2_DENSITY_YZ(NGridPoints))
!
      do Ipoint=1,NGridPoints
        call GET_d2rho_Atom (grid_points(Ipoint)%x, grid_points(Ipoint)%y, grid_points(Ipoint)%z, Hess)
        DEL2_DENSITY_XX(Ipoint)=Hess(1,1)
        DEL2_DENSITY_YY(Ipoint)=Hess(2,2)
        DEL2_DENSITY_ZZ(Ipoint)=Hess(3,3)
        DEL2_DENSITY_XY(Ipoint)=Hess(1,2)
        DEL2_DENSITY_XZ(Ipoint)=Hess(1,3)
        DEL2_DENSITY_YZ(Ipoint)=Hess(2,3)
!       call d2AO_products (grid_points(Ipoint)%x, grid_points(Ipoint)%y, grid_points(Ipoint)%z)
!       DEL2_DENSITY_XX(Ipoint)=TraceAB (PM0, d2AOprodxx, NBasis)
!       DEL2_DENSITY_YY(Ipoint)=TraceAB (PM0, d2AOprodyy, NBasis)
!       DEL2_DENSITY_ZZ(Ipoint)=TraceAB (PM0, d2AOprodzz, NBasis)
!       DEL2_DENSITY_XY(Ipoint)=TraceAB (PM0, d2AOprodxy, NBasis)
!       DEL2_DENSITY_XZ(Ipoint)=TraceAB (PM0, d2AOprodxz, NBasis)
!       DEL2_DENSITY_YZ(Ipoint)=TraceAB (PM0, d2AOprodyz, NBasis)
      end do ! Ipoint

! Now print the results
      if(MoleculePrint)then
        write(PlotFileID,'(a)')'DEL2_DENSITY_MESH'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, File_unit)
      else
        File_unit=UNIout
      end if
  
      call PRT_DEL2_DENSITY

      write(File_unit,'(a)')'Eigenvalues and NEGdel2rho:'
      write(File_unit,'(a)')'X,Y,Z,Eig1,Eig2,Eig3,NEGdel2rho:'

      do Ipoint=1,NGridPoints
        Hess(1,1)=DEL2_DENSITY_XX(Ipoint)
        Hess(1,2)=DEL2_DENSITY_XY(Ipoint)
        Hess(1,3)=DEL2_DENSITY_XZ(Ipoint)
        Hess(2,1)=Hess(1,2)
        Hess(2,2)=DEL2_DENSITY_YY(Ipoint)
        Hess(2,3)=DEL2_DENSITY_YZ(Ipoint)
        Hess(3,1)=Hess(1,3)
        Hess(3,2)=Hess(2,3)
        Hess(3,3)=DEL2_DENSITY_ZZ(Ipoint)

        call MATRIX_diagonalize (Hess, EigVector, EigValue, 3, 2, .true.)
        NEGdel2rho = -(EigValue(1)+EigValue(2)+EigValue(3))

        write(File_unit,Pfmt_Eig)grid_points(Ipoint)%x,grid_points(Ipoint)%y,grid_points(Ipoint)%z, &
                                EigValue(1),EigValue(2),EigValue(3),NEGdel2rho
        call CP_classification (EigValue, CPRS)
        write(File_unit,'(2a)')'Rank and Signature of the critical point is: ',CPRS
      end do ! Ipoint

      if(MoleculePrint)close (unit=File_unit)
!
!     deallocate (d2AOprodxx)
!     deallocate (d2AOprodyy)
!     deallocate (d2AOprodzz)
!     deallocate (d2AOprodxy)
!     deallocate (d2AOprodxz)
!     deallocate (d2AOprodyz)   
!
! End of routine DEL2_DENSITY_MESH
      call PRG_manager ('exit', 'DEL2_DENSITY_MESH', 'DEL2_DENSITY%MESH')
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine PRT_DEL2_DENSITY
!********************************************************************
!     Date last modified: August 15, 2012                           *
!     Authors: R.A. Poirier                                         *
!     Description: Calculate the charge density at point.           *
!********************************************************************
! Modules:

      implicit none
!
! Local scalars:
      integer :: Ipoint
!
! Begin:
      call PRG_manager ('enter','PRT_DEL2_DENSITY', 'UTILITY')

      write(File_unit,'(a)')'Second Derivative of density calculated'
      if(NX.eq.0.and.NY.eq.0)then
        write(File_unit,'(a,2f12.6)')'X and Y fixed to: ',grid_points(1)%x,grid_points(Ipoint)%y
        write(File_unit,'(a)')'Z,d2xx,d2yy,d2zz,d2xy,d2xz,d2yz:'
        do Ipoint=1,NGridPoints
        write(File_unit,Pfmt1_d2rho)grid_points(Ipoint)%z, &
                           DEL2_DENSITY_XX(Ipoint),DEL2_DENSITY_YY(Ipoint), &
                           DEL2_DENSITY_ZZ(Ipoint),DEL2_DENSITY_XY(Ipoint), & 
                           DEL2_DENSITY_XZ(Ipoint),DEL2_DENSITY_YZ(Ipoint)
        end do
      else if(NX.eq.0.and.NZ.eq.0)then
        write(File_unit,'(a,2f12.6)')'X and Z fixed to: ',grid_points(1)%x,grid_points(Ipoint)%z
        write(File_unit,'(a)')'Y,d2xx,d2yy,d2zz,d2xy,d2xz,d2yz:'
        do Ipoint=1,NGridPoints
        write(File_unit,Pfmt1_d2rho)grid_points(Ipoint)%y, &
                           DEL2_DENSITY_XX(Ipoint),DEL2_DENSITY_YY(Ipoint), &
                           DEL2_DENSITY_ZZ(Ipoint),DEL2_DENSITY_XY(Ipoint), & 
                           DEL2_DENSITY_XZ(Ipoint),DEL2_DENSITY_YZ(Ipoint)
        end do
      else if(NY.eq.0.and.NZ.eq.0)then
        write(File_unit,'(a,2f12.6)')'Y and Z fixed to: ',grid_points(1)%y,grid_points(Ipoint)%z
        write(File_unit,'(a)')'X,d2xx,d2yy,d2zz,d2xy,d2xz,d2yz:'
        do Ipoint=1,NGridPoints
        write(File_unit,Pfmt1_d2rho)grid_points(Ipoint)%x, &
                           DEL2_DENSITY_XX(Ipoint),DEL2_DENSITY_YY(Ipoint), &
                           DEL2_DENSITY_ZZ(Ipoint),DEL2_DENSITY_XY(Ipoint), & 
                           DEL2_DENSITY_XZ(Ipoint),DEL2_DENSITY_YZ(Ipoint)
        end do
      else if(NX.eq.0)then
        write(File_unit,'(a,2f12.6)')'X fixed to: ',grid_points(1)%x
        write(File_unit,'(a)')'Y,Z,d2xx,d2yy,d2zz,d2xy,d2xz,d2yz:'
        do Ipoint=1,NGridPoints
        write(File_unit,Pfmt2_d2rho)grid_points(Ipoint)%y,grid_points(Ipoint)%z, &
                           DEL2_DENSITY_XX(Ipoint),DEL2_DENSITY_YY(Ipoint), &
                           DEL2_DENSITY_ZZ(Ipoint),DEL2_DENSITY_XY(Ipoint), & 
                           DEL2_DENSITY_XZ(Ipoint),DEL2_DENSITY_YZ(Ipoint)
        end do
      else if(NY.eq.0)then
        write(File_unit,'(a,2f12.6)')'Y fixed to: ',grid_points(1)%y
        write(File_unit,'(a)')'X,Z,d2xx,d2yy,d2zz,d2xy,d2xz,d2yz:'
        do Ipoint=1,NGridPoints
        write(File_unit,Pfmt2_d2rho)grid_points(Ipoint)%x,grid_points(Ipoint)%z, &
                           DEL2_DENSITY_XX(Ipoint),DEL2_DENSITY_YY(Ipoint), &
                           DEL2_DENSITY_ZZ(Ipoint),DEL2_DENSITY_XY(Ipoint), & 
                           DEL2_DENSITY_XZ(Ipoint),DEL2_DENSITY_YZ(Ipoint)
        end do
      else if(NZ.eq.0)then
        write(File_unit,'(a,2f12.6)')'Z fixed to: ',grid_points(1)%z
        write(File_unit,'(a)')'X,Y,d2xx,d2yy,d2zz,d2xy,d2xz,d2yz:'
        do Ipoint=1,NGridPoints
        write(File_unit,Pfmt2_d2rho)grid_points(Ipoint)%x,grid_points(Ipoint)%y, &
                           DEL2_DENSITY_XX(Ipoint),DEL2_DENSITY_YY(Ipoint), &
                           DEL2_DENSITY_ZZ(Ipoint),DEL2_DENSITY_XY(Ipoint), & 
                           DEL2_DENSITY_XZ(Ipoint),DEL2_DENSITY_YZ(Ipoint)
        end do
      else
        write(File_unit,'(a)')'Full 3D Mesh used'
        write(File_unit,'(a)')'X,Y,Z,d2xx,d2yy,d2zz,d2xy,d2xz,d2yz:'
        do Ipoint=1,NGridPoints
        write(File_unit,Pfmt_d2rho)grid_points(Ipoint)%x,grid_points(Ipoint)%y,grid_points(Ipoint)%z, &
                           DEL2_DENSITY_XX(Ipoint),DEL2_DENSITY_YY(Ipoint), &
                           DEL2_DENSITY_ZZ(Ipoint),DEL2_DENSITY_XY(Ipoint), & 
                           DEL2_DENSITY_XZ(Ipoint),DEL2_DENSITY_YZ(Ipoint)
        end do
      end if
!
! End of routine PRT_DEL2_DENSITY
      call PRG_manager ('exit', 'PRT_DEL2_DENSITY', 'UTILITY')
      return
      end subroutine PRT_DEL2_DENSITY
! END CONTAINS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine DEL2_DENSITY_MESH
      function GET_density_at_xyz (Xcoor, Ycoor, Zcoor) result(density)
!**************************************************************************************************
!     Date last modified: September 28, 2010                                                      *
!     Authors: R.A. Poirier                                                                       *
!     Description:                                                                                *
!                 Calculate the charge density at point Xcoor, Ycoor, Zcoor                       *
!**************************************************************************************************
! Modules:
      USE QM_objects

      implicit none
!
! Input scalars:
      double precision :: density
      double precision, intent(IN) :: Xcoor,Ycoor,Zcoor
!
! Local scalars:
      double precision :: TraceAB
      double precision, dimension(:), allocatable :: AOprod
!
! Begin:
      call PRG_manager ('enter','GET_density_at_xyz', 'UTILITY')
!
      if (.not.allocated(AOprod)) allocate (AOprod(1:MATlen))

      call Gaussian_products (Xcoor, Ycoor, Zcoor, AOprod, MATlen)
      density= TraceAB (PM0, AOprod, NBasis)

      deallocate (AOprod)
!
! End of routine GET_density_at_xyz
      call PRG_manager ('exit', 'GET_density_at_xyz', 'UTILITY')
      return
      end
      subroutine GET_MO_density_at_xyz (Xcoor, Ycoor, Zcoor, CMO, MO_Den, Nbasis, NoccMO)
!************************************************************************************************************
!     Date last modified: March 15, 2011                                                                    *
!     Authors: R.A. Poirier                                                                                 *
!     Description:                                                                                          *
!                 Calculate the charge density for each MO at point Xcoor, Ycoor, Zcoor                     *
!************************************************************************************************************
! Modules:
      USE program_constants

      implicit none
!
! Input scalars:
      integer :: Nbasis,NoccMO
      double precision :: CMO(Nbasis,Nbasis),MO_Den(NoccMO)
      double precision, intent(IN) :: Xcoor,Ycoor,Zcoor
!
! Local scalars:
      integer :: IJ,IMO,Mu,Nu,MATlen
      double precision :: density,fac,sum
!
! Local function:
      double precision, dimension(:), allocatable :: AOprod
!
! Begin:
      call PRG_manager ('enter','GET_MO_density_at_xyz', 'UTILITY')
!
      MATlen=Nbasis*(Nbasis+1)/2

      if (.not.allocated(AOprod)) allocate (AOprod(1:MATlen))
      call Gaussian_products (Xcoor, Ycoor, Zcoor, AOprod, MATlen)
!
      density=ZERO
      do IMO=1,NoccMO
        sum=ZERO
        do Mu=1,Nbasis
        do Nu=1,Mu
          fac=ONE
          if(Mu.ne.Nu)Fac=TWO
          IJ=Mu*(Mu-1)/2+Nu
          sum=sum+FAC*CMO(Mu,IMO)*CMO(Nu,IMO)*AOprod(IJ)
        end do ! Nu
        end do ! Mu
        MO_DEN(IMO)=sum
      density=density+sum
      end do ! IMO

      deallocate (AOprod)
!
! End of routine GET_MO_density_at_xyz
      call PRG_manager ('exit', 'GET_MO_density_at_xyz', 'UTILITY')
      return
      end
      subroutine GET_DENSITY_ATOM (Iatom)
!************************************************************************************
!     Date last modified: June 26, 2015                                             *
!     Authors: R.A. Poirier                                                         *
!     Description: Calculate the electron density for Iatom at a set of             *
!                  grid points (e.g., SG1, G20, ...)                                *
!************************************************************************************
! Modules
      USE program_constants
      USE program_files
      USE type_molecule
      USE module_grid_points
      USE type_density
      USE QM_objects

      implicit none
!
! Input scalars:
      integer :: Iatom
!
! Local scalars:
      integer :: Ipoint,Ifound,Znum
      double precision :: XApt,YApt,ZApt
      double precision :: TraceAB
      double precision, dimension(:), allocatable :: AOprod
!
! Begin
      call PRG_manager ('enter', 'GET_DENSITY_ATOM', 'UTILITY')
!
      Znum=CARTESIAN(Iatom)%Atomic_number

      if(Znum.gt.0)then
      allocate (AOprod(1:MATlen))
      Ifound=GRID_loc(Znum)
      NApts_atom=NApoints_atom(Ifound)
      if(.not.allocated(rho_Atom))then
        allocate (rho_Atom(NApts_atom))
      else
        deallocate (rho_Atom)
        allocate (rho_Atom(NApts_atom))
      end if
!           
! Loop over grid points of Iatom
      do Ipoint=1,NApts_atom
!
! Moving grid points on cartesian coordinate grid
        XApt=Egridpts(Ifound,Ipoint)%X+CARTESIAN(Iatom)%X
        YApt=Egridpts(Ifound,Ipoint)%Y+CARTESIAN(Iatom)%Y
        ZApt=Egridpts(Ifound,Ipoint)%Z+CARTESIAN(Iatom)%Z
        call Gaussian_products (XApt, YApt, ZApt, AOprod, MATlen)
        rho_Atom(Ipoint)=TraceAB (PM0, AOprod, NBasis)
      end do ! Ipoint

      deallocate (AOprod)
      end if
!
! End of routine GET_DENSITY_ATOM
      call PRG_manager ('exit', 'GET_DENSITY_ATOM', 'UTILITY')
      return
      end subroutine GET_DENSITY_ATOM
      subroutine GET_DEL_DENSITY_ATOM (Iatom)
!******************************************************************************
!     Date last modified: June 26, 2015                                       *
!     Authors: R.A. Poirier                                                   *
!     Description: Calculate DEL electron density for Iatom at a set of       *
!                  grid points (e.g., SG1, G20, ...)                          *
!******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE type_molecule
      USE QM_objects
      USE type_density
      USE module_grid_points

      implicit none
!
! Input scalars:
      integer :: Iatom
!
! Local scalars:
      integer :: Ipoint,Ifound,Znum
      double precision :: XApt,YApt,ZApt
      double precision :: TraceAB
      double precision, dimension(:), allocatable  :: dAOprodX,dAOprodY,dAOprodZ
!
! Begin:
      call PRG_manager ('enter','GET_DEL_DENSITY_ATOM', 'UTILITY')

      Znum=CARTESIAN(Iatom)%Atomic_number

      if(Znum.gt.0)then
      allocate (dAOprodx(1:MATlen))
      allocate (dAOprody(1:MATlen))
      allocate (dAOprodz(1:MATlen))
!
      Ifound=GRID_loc(Znum)
      NApts_atom=NApoints_atom(Ifound)
      if(.not.allocated(delrho_Atom))then
        allocate (delrho_Atom(NApts_atom))
      else
        deallocate (delrho_Atom)
        allocate (delrho_Atom(NApts_atom))
      end if
!
! Loop over grid points of Iatom
      do Ipoint=1,NApts_atom
! Moving grid points on cartesian coordinate grid
        XApt=Egridpts(Ifound,Ipoint)%X+CARTESIAN(Iatom)%X
        YApt=Egridpts(Ifound,Ipoint)%Y+CARTESIAN(Iatom)%Y
        ZApt=Egridpts(Ifound,Ipoint)%Z+CARTESIAN(Iatom)%Z
        call dG_products (XApt, YApt, ZApt, dAOprodX, dAOprodY, dAOprodZ, MATlen)
        delrho_Atom(Ipoint)%X=TraceAB (PM0, dAOprodX, NBasis)
        delrho_Atom(Ipoint)%Y=TraceAB (PM0, dAOprodY, NBasis)
        delrho_Atom(Ipoint)%Z=TraceAB (PM0, dAOprodZ, NBasis)
      end do ! Ipoint

      deallocate (dAOprodx)
      deallocate (dAOprody)
      deallocate (dAOprodz)
      end if
!
! End of routine GET_DEL_DENSITY_ATOM
      call PRG_manager ('exit', 'GET_DEL_DENSITY_ATOM', 'UTILITY')
      return
      end subroutine GET_DEL_DENSITY_ATOM
      subroutine GET_DEL2_DENSITY_ATOM (Iatom)
!******************************************************************************
!     Date last modified: June 26, 2015                                       *
!     Authors: R.A. Poirier                                                   *
!     Description: Calculate DEL2 electron density for Iatom at a set of      *
!                  grid points (e.g., SG1, G20, ...)                          *
!******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE type_molecule
      USE QM_objects
      USE type_density
      USE module_grid_points

      implicit none
!
! Input scalars:
      integer :: Iatom
!
! Local scalars:
      integer :: Ipoint,Ifound,Znum
      double precision :: XApt,YApt,ZApt
      double precision :: TraceAB
!
! Local Array	  
      double precision, dimension(3,3) :: Hess
!
! Begin:
      call PRG_manager ('enter','GET_DEL2_DENSITY_ATOM', 'UTILITY')

      Znum=CARTESIAN(Iatom)%Atomic_number
      if(Znum.gt.0)then
!     allocate (d2AOprodxx(1:MATlen))
!     allocate (d2AOprodyy(1:MATlen))
!     allocate (d2AOprodzz(1:MATlen))
!     allocate (d2AOprodxy(1:MATlen))
!     allocate (d2AOprodxz(1:MATlen))
!     allocate (d2AOprodyz(1:MATlen))   

      Ifound=GRID_loc(Znum)
      NApts_atom=NApoints_atom(Ifound)

      if(.not.allocated(del2rho_Atom))then
        allocate (del2rho_Atom(NApts_atom))
      else
        deallocate (del2rho_Atom)
        allocate (del2rho_Atom(NApts_atom))
      end if
!
! Loop over grid points of Iatom
      do Ipoint=1,NApts_atom
! Moving grid points on cartesian coordinate grid
        XApt=Egridpts(Ifound,Ipoint)%X+CARTESIAN(Iatom)%X
        YApt=Egridpts(Ifound,Ipoint)%Y+CARTESIAN(Iatom)%Y
        ZApt=Egridpts(Ifound,Ipoint)%Z+CARTESIAN(Iatom)%Z
        call GET_d2rho_Atom (XApt, YApt, ZApt, Hess)
        del2rho_Atom(Ipoint)%XX=Hess(1,1)
        del2rho_Atom(Ipoint)%YY=Hess(2,2)
        del2rho_Atom(Ipoint)%ZZ=Hess(3,3)
        del2rho_Atom(Ipoint)%XY=Hess(1,2)
        del2rho_Atom(Ipoint)%XZ=Hess(1,3)
        del2rho_Atom(Ipoint)%YZ=Hess(2,3)
!       call d2AO_products (XApt, YApt, ZApt)
!       del2rho_Atom(Ipoint)%XX=TraceAB (PM0, d2AOprodxx, NBasis)
!       del2rho_Atom(Ipoint)%YY=TraceAB (PM0, d2AOprodyy, NBasis)
!       del2rho_Atom(Ipoint)%ZZ=TraceAB (PM0, d2AOprodzz, NBasis)
!       del2rho_Atom(Ipoint)%XY=TraceAB (PM0, d2AOprodxy, NBasis)
!       del2rho_Atom(Ipoint)%XZ=TraceAB (PM0, d2AOprodxz, NBasis)
!       del2rho_Atom(Ipoint)%YZ=TraceAB (PM0, d2AOprodyz, NBasis)
      end do ! Ipoint

!     deallocate (d2AOprodxx)
!     deallocate (d2AOprodyy)
!     deallocate (d2AOprodzz)
!     deallocate (d2AOprodxy)
!     deallocate (d2AOprodxz)
!     deallocate (d2AOprodyz)   
      end if
!
! End of routine GET_DEL2_DENSITY_ATOM
      call PRG_manager ('exit', 'GET_DEL2_DENSITY_ATOM', 'UTILITY')
      return
      end subroutine GET_DEL2_DENSITY_ATOM
      subroutine GET_rho_Atom (XApt, YApt, ZApt, rho_Atom_r)
!************************************************************************************
!     Date last modified: June 26, 2015                                             *
!     Authors: R.A. Poirier                                                         *
!     Description: Calculate the electron density for Iatom at a grid point         *
!************************************************************************************
! Modules
      USE QM_objects

      implicit none
!
! Input scalars:
      double precision :: XApt,YApt,ZApt
!
! Output scalars:
      double precision :: rho_Atom_r
!
! Local functions:
      double precision :: TraceAB
      double precision, dimension(:), allocatable :: AOprod
!
! Begin
      allocate (AOprod(1:MATlen))
      call Gaussian_products (XApt, YApt, ZApt, AOprod, MATlen)
!      rho_Atom_r = 0.0D0
!      K=0
!      do J=1,Nbasis
!      do I=1,J
!        K=K+1
!        rho_Atom_r=rho_Atom_r+TWO*PM0(K)*AOprod(K)
!      end do ! I
!      rho_Atom_r=rho_Atom_r-PM0(K)*AOprod(K)
!      end do ! J
      rho_Atom_r=TraceAB (PM0, AOprod, NBasis)
      deallocate (AOprod)
!
! End of routine GET_rho_Atom
      return
      end subroutine GET_rho_Atom
      subroutine GET_drho_Atom (XApt, YApt, ZApt, drho_X, drho_Y, drho_Z)
!******************************************************************************
!     Date last modified: June 26, 2015                                       *
!     Authors: R.A. Poirier                                                   *
!     Description: Calculate DEL electron density for Iatom at a point        *
!******************************************************************************
! Modules:
      USE QM_objects

      implicit none
!
! Input scalars:
      double precision :: XApt,YApt,ZApt
!
! Output scalars:
      double precision :: drho_X,drho_Y,drho_Z
!
! Local functions:
      double precision :: TraceAB
      double precision, dimension(:), allocatable  :: dAOprodX,dAOprodY,dAOprodZ
!
! Begin:
      allocate (dAOprodx(1:MATlen))
      allocate (dAOprody(1:MATlen))
      allocate (dAOprodz(1:MATlen))
      call dG_products (XApt, YApt, ZApt, dAOprodX, dAOprodY, dAOprodZ, MATlen)
      drho_X=TraceAB (PM0, dAOprodX, NBasis)
      drho_Y=TraceAB (PM0, dAOprodY, NBasis)
      drho_Z=TraceAB (PM0, dAOprodZ, NBasis)
      deallocate (dAOprodx)
      deallocate (dAOprody)
      deallocate (dAOprodz)
!
! End of routine GET_drho_Atom
      return
      end subroutine GET_drho_Atom
      subroutine DENSITY_ATOM
!********************************************************************
!     Date last modified: July 31, 2014                           *
!     Authors: R.A. Poirier                                         *
!     Description: Calculate the density belonging to bonds         *
!********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE type_density
      USE module_grid_points
      USE type_plotting

      implicit none
!
! Local scalars:
      integer :: Aatom,Ipoint,ZA,lenstr
      double precision :: BweightA
      double precision :: XVal,YVal,ZVal
      character(len=32):: Ctemp

      double precision :: GET_density_at_xyz
!
! Begin:
      call PRG_manager ('enter','DENSITY_ATOM', 'DENSITY%ATOM')
!
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)
!
      PlotFileID=' '
      allocate(Rden(NGridPoints))
! Now build a mesh based on the radial grids
      call BLD_BeckeW_XI
!
      do Aatom=1,Natoms
        ZA=CARTESIAN(Aatom)%Atomic_number
        if(ZA.le.0)cycle
        write(Ctemp,'(a,i4,a,i4)')'DENSITY_ATOM_',Aatom
        call RemoveBlanks (Ctemp, PlotFileID, lenstr)
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a,i4,a,i4)')'Density for Atom ',Aatom
      do Ipoint=1,NGridPoints
         XVal=grid_points(Ipoint)%x
         YVal=grid_points(Ipoint)%y
         ZVal=grid_points(Ipoint)%z
          call GET_Weight1 (XVal, YVal, ZVal, Aatom, BweightA)
          Rden(Ipoint)=BweightA*GET_density_at_xyz (XVal, YVal, ZVal)
        end do ! Ipoint
      call PRT_GRID_MOL (Rden, NGridPoints, fmt_rho)
      close (unit=Plotfile_unit)
      end do ! Aatom

      deallocate(Rden)
!
! End of routine DENSITY_ATOM
      call PRG_manager ('exit', 'DENSITY_ATOM', 'DENSITY%ATOM')
      return
      end subroutine DENSITY_ATOM
      subroutine WEIGHTS_MESH
!********************************************************************
!     Date last modified: July 31, 2014                           *
!     Authors: R.A. Poirier                                         *
!     Description: Calculate the density belonging to bonds         *
!********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE module_grid_points
      USE type_molecule
      USE type_plotting
      USE type_WEIGHTS

      implicit none
!
! Local scalars:
      integer :: Aatom,Ipoint,lenstr,Save_UNIout
      double precision :: Weight
      double precision :: XVal,YVal,ZVal
      character(len=128):: Ctemp
      character(len=64) :: File_name
      double precision, dimension(:), allocatable :: Weights
!
! Begin:
      call PRG_manager ('enter','WEIGHTS_MESH', 'WEIGHTS%MESH')
!
      allocate(Weights(NGridPoints))
! Now build a mesh based on the radial grids
      call BLD_BeckeW_XI
!
      do Aatom=1,Natoms ! For now!!!
      PlotFileID=' '
      write(Ctemp,'(a,i4)')'MESH',Aatom
      call RemoveBlanks (Ctemp, PlotFileID, lenstr)
      call BLD_FileID
      if(DEN_partitioning(1:1).eq.'H')then ! For now !!!!!
        Ctemp(1:)='plot_'//DEN_partitioning//'_'//Type_Hirshfeld//'_WEIGHTS_'//trim(PlotFileID)//'_'// &
                   trim(Basic_FileID)//'.dat'
      else
        Ctemp(1:)='plot_'//DEN_partitioning//'_WEIGHTS_'//trim(PlotFileID)//'_'//trim(Basic_FileID)//'.dat'
      end if
      call RemoveBlanks (Ctemp, File_name, lenstr)
      call GET_file_unit ('REPLACE', File_name, 'FORMATTED', PlotFileID, Plotfile_unit)
      write(PlotFunction,'(a)')'Weights on a mesh'
      Save_UNIout=UNIout
      UNIout=Plotfile_unit
!     call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
      do Ipoint=1,NGridPoints
         XVal=grid_points(Ipoint)%x
         YVal=grid_points(Ipoint)%y
         ZVal=grid_points(Ipoint)%z
          call GET_Weight1 (XVal, YVal, ZVal, Aatom, Weight)
          Weights(Ipoint)=Weight
        end do ! Ipoint
      call PRT_GRID_MOL (Weights, NGridPoints, fmt_rho)
      close (unit=Plotfile_unit)
      UNIout=Save_UNIout
      close (unit=Plotfile_unit)
      end do ! Aatom
!
! End of routine WEIGHTS_MESH
      call PRG_manager ('exit', 'WEIGHTS_MESH', 'WEIGHTS%MESH')
      return
      end subroutine WEIGHTS_MESH
      subroutine WEIGHTS_WAWB_MESH
!********************************************************************
!     Date last modified: July 31, 2014                           *
!     Authors: R.A. Poirier                                         *
!     Description: Calculate the density belonging to bonds         *
!********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE module_grid_points
      USE type_molecule
      USE type_plotting

      implicit none
!
! Local scalars:
      integer :: Aatom,Batom,ZA,ZB,Ipoint,lenstr
      double precision :: WeightA,WeightB
      double precision :: XVal,YVal,ZVal
      character(len=32):: Ctemp
      double precision, dimension(:), allocatable :: Weights
!
! Begin:
      call PRG_manager ('enter','WEIGHTS_WAWB_MESH', 'WEIGHTS_WAWB%MESH')
!
      PlotFileID=' '
      allocate(Weights(NGridPoints))
! Now build a mesh based on the radial grids
      call BLD_BeckeW_XI
!
      do Aatom=1,Natoms ! For now!!!
        ZA=CARTESIAN(Aatom)%Atomic_number
        if(ZA.le.0)cycle
      do Batom=Aatom,Natoms
        if(Aatom.eq.Batom)cycle
        ZB=CARTESIAN(Batom)%Atomic_number
        if(ZB.le.0)cycle
      write(Ctemp,'(a,i4,a,i4)')'WEIGHTS_WAWB_MESH',Aatom,'_',Batom
      call RemoveBlanks (Ctemp, PlotFileID, lenstr)
      call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
      do Ipoint=1,NGridPoints
         XVal=grid_points(Ipoint)%x
         YVal=grid_points(Ipoint)%y
         ZVal=grid_points(Ipoint)%z
          call GET_Weight1 (XVal, YVal, ZVal, Aatom, WeightA)
          call GET_Weight1 (XVal, YVal, ZVal, Batom, WeightB)
          Weights(Ipoint)=dsqrt(WeightA*WeightB)
        end do ! Ipoint
      call PRT_GRID_MOL (Weights, NGridPoints, fmt_rho)
      close (unit=Plotfile_unit)
      end do ! Aatom
      end do ! Batom
!
! End of routine WEIGHTS_WAWB_MESH
      call PRG_manager ('exit', 'WEIGHTS_WAWB_MESH', 'WEIGHTS_WAWB%MESH')
      return
      end subroutine WEIGHTS_WAWB_MESH
      subroutine DENSITY_ATOMIC
!*******************************************************************************************************
!     Date last modified: August 15, 2012                                                              *
!     Authors: R.A. Poirier                                                                            *
!     Description:                                                                                     *
!         Calculate the radial density on radial grids        (e.g., SG1, G20, ...)                    *
!*******************************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE module_grid_points
      USE type_plotting

      implicit none
!
! Local scalars:
      integer :: Ipoint,IAIMprint,Iatom,Ifound,Znum,File_MOL
      double precision :: Xpt,Ypt,Zpt
      double precision :: WeightA,XApt,YApt,ZApt,RadSq,rho
      double precision :: r_val,theta_val,phi_val

      double precision :: GET_density_at_xyz

      double precision :: cutoff
      parameter (cutoff=1.0E-2)
!
! Begin
      call PRG_manager ('enter', 'DENSITY_ATOMIC', 'DENSITY%ATOMIC')

      call GET_object ('QM', 'CMO', Wavefunction)  ! Get the density matrix (PM0) 
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)  ! Get the grid points for all the atoms (Egridpts)
!
      do IAIMprint=1,NAIMprint
        Iatom=AIMprint(IAIMprint)
        Znum=CARTESIAN(Iatom)%Atomic_number
        if(Znum.le.0)cycle ! Skip dummy atoms
        Ifound=GRID_loc(Znum)
        NApts_atom=NApoints_atom(Ifound)
        if(AtomPrint)then
          PlotFileID=' '
          PlotFunction=' '
          write(PlotFunction,'(a,i4)')'Radial Density Atomic for atom ',Iatom
          write(PlotFileID,'(a)')'DENSITY_ATOMIC'
          call BLD_plot_file_ATOMIC (PlotFileID, NApts_atom, Iatom, PlotFile_unit)
        else
          PlotFile_unit=UNIout
        end if
        write(PlotFile_unit,'(a)')'r_val,theta_val,phi_val,rho(x,y,z):'
        write(PlotFile_unit,'(a)')trim(PlotFunction)
!           
! Loop over grid points of Iatom
        do Ipoint=1,NApts_atom
          Xpt=Egridpts(Ifound,Ipoint)%X
          Ypt=Egridpts(Ifound,Ipoint)%Y
          Zpt=Egridpts(Ifound,Ipoint)%Z
!         write(6,*)Xpt,Ypt,Zpt
!         call flush(6)
!         RadSq=(Xpt)**2+(Ypt)**2+(Zpt)**2          
!         r_val=dsqrt(RadSq)
!         write(6,*)r_val
!         call flush(6)
!         theta_val=dacos(Zpt/r_val)
!         write(6,*)theta_val
!         call flush(6)
!         if(Xpt.le.cutoff)then
!           phi_val=ZERO
!         else
!           phi_val=datan(Ypt/Xpt)
!         end if
!         write(6,*)phi_val
!         call flush(6)
! Moving grid points on cartesian coordinate grid
          XApt=Xpt+CARTESIAN(Iatom)%X
          YApt=Ypt+CARTESIAN(Iatom)%Y
          ZApt=Zpt+CARTESIAN(Iatom)%Z
          call GET_Weight1 (XApt, YApt, ZApt, Iatom, WeightA)
          if(WeightA.le.cutoff)cycle ! Only include if belongs to atom
          rho=GET_density_at_xyz (XApt, YApt, ZApt) ! For atom a
          write(PlotFile_unit,Pfmt_r2rho)XApt,YApt,ZApt,rho
!         write(PlotFile_unit,Pfmt_r2rho)r_val,theta_val,phi_val,rho
        end do ! Ipoint
!
        if(AtomPrint)close (unit=PlotFile_unit)
      end do ! Iatom
!
! End of routine DENSITY_ATOMIC
      call PRG_manager ('exit', 'DENSITY_ATOMIC', 'DENSITY%ATOMIC')
      return
      end subroutine DENSITY_ATOMIC
      subroutine MO_DENSITY_MESH
!****************************************************************************************
!     Date last modified: Sept 26, 2013                                                 *
!     Authors: Devin Nippard                                                            *
!     Description:                                                                      *
!                 Calculate the MO density on a set of grid points (mesh)               *
!****************************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE module_grid_points
      USE type_density
      USE type_molecule
      USE type_elements
      USE type_plotting
      USE type_Weights

      implicit none

      integer :: Ipoint,NMOs,IMO,IMOprint,File_unit,lenstr
      double precision :: Xg,Yg,Zg
      double precision :: MO_den
      double precision, allocatable,dimension(:) :: MOrhoMO
      character(len=32):: Ctemp
!
      call PRG_manager ('enter','MO_DENSITY_MESH', 'DENSITY_MO%MESH')

      call GET_object ('QM', 'CMO', Wavefunction)
!
      NMOs=CMO%NoccMO
      allocate (MOrhoMO(NGridPoints))

      do IMOprint=1,NMOprint
      IMO=MOprint(IMOprint)

      MOrhoMO=ZERO
        do Ipoint=1,NGridPoints
          Xg=grid_points(Ipoint)%x
          Yg=grid_points(Ipoint)%y
          Zg=grid_points(Ipoint)%z
          call GET_MO_density_point (Xg, Yg, Zg, CMO%coeff, MO_den, Nbasis, IMO)
          MOrhoMO(Ipoint)=MO_den
        end do !Ipoint
! Now print the densities for Iatom and IMO
      if(MoleculePrint.or.AtomPrint)then
       PlotFileID=' '
       write(Ctemp,'(a,i4)')'DENSITY_MESH_MO',IMO
        call RemoveBlanks (Ctemp, PlotFileID, lenstr)
       call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
      else
        File_unit=UNIout
      end if
       call PRT_GRID_MOL (MOrhoMO, NGridPoints, fmt_r2rho)
      end do ! IMO
      deallocate (MOrhoMO)

      call PRG_manager ('exit','MO_DENSITY_MESH', 'DENSITY_MO%MESH')

      end subroutine MO_DENSITY_MESH
      subroutine GET_MO_density_point (Xcoor, Ycoor, Zcoor, CMO, MOden, Nbasis, IMO)
!***********************************************************************************************************
!     Date last modified: March 18, 2016                                                                   *
!     Authors: R.A. Poirier                                                                                *
!     Description:                                                                                         *
!                 Calculate the charge density for one MO at point Xcoor, Ycoor, Zcoor                     *
!***********************************************************************************************************
! Modules:
      USE program_constants

      implicit none
!
! Input scalars:
      integer :: Nbasis,IMO
      double precision :: CMO(Nbasis,Nbasis),MOden
      double precision, intent(IN) :: Xcoor,Ycoor,Zcoor
!
! Local scalars:
      integer :: IJ,Mu,Nu,MATlen
      double precision :: fac
!
! Local function:
      double precision, dimension(:), allocatable :: AOprod
!
! Begin:
      call PRG_manager ('enter','GET_MO_density_point', 'UTILITY')
!
      MATlen=Nbasis*(Nbasis+1)/2

      allocate (AOprod(1:MATlen))
      call Gaussian_products (Xcoor, Ycoor, Zcoor, AOprod, MATlen)
!
      MOden=ZERO
      do Mu=1,Nbasis
      do Nu=1,Mu
        fac=ONE
        if(Mu.ne.Nu)Fac=TWO
        IJ=Mu*(Mu-1)/2+Nu
        MOden=MOden+FAC*CMO(Mu,IMO)*CMO(Nu,IMO)*AOprod(IJ)
      end do ! Nu
      end do ! Mu

      deallocate (AOprod)
!
! End of routine GET_MO_density_point
      call PRG_manager ('exit', 'GET_MO_density_point', 'UTILITY')
      return
      end
      subroutine DENSITY_MO_by_ATOM_MESH
!****************************************************************************************
!     Date last modified: Sept 26, 2013                                                 *
!     Authors: Devin Nippard                                                            *
!     Description:                                                                      *
!                 Calculate the MO density on a set of grid points (mesh)               *
!****************************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE module_grid_points
      USE type_density
      USE type_molecule
      USE type_elements
      USE type_plotting
      USE type_Weights

      implicit none

      integer :: Ipoint,NMOs,Iatom,IMO,IMOprint,lenstr,Znum
      double precision :: Xg,Yg,Zg,Xa,Ya,Za
      double precision :: WeightA,MO_den
      double precision, allocatable,dimension(:) :: MOrhoA
      character(len=64):: Ctemp
!
      call PRG_manager ('enter','DENSITY_MO_BY_ATOM_MESH', 'DENSITY_MO_BY_ATOM%MESH')

      call GET_object ('QM', 'CMO', Wavefunction)
!
      if(DEN_partitioning(1:5).eq.'BECKE')call BLD_BeckeW_XI

      NMOs=CMO%NoccMO
      allocate (MOrhoA(NGridPoints))

      MOrhoA=ZERO
      do IMOprint=1,NMOprint
      IMO=MOprint(IMOprint)
      do Iatom=1,Natoms
        Xa=CARTESIAN(Iatom)%X
        Ya=CARTESIAN(Iatom)%Y
        Za=CARTESIAN(Iatom)%Z
        do Ipoint=1,NGridPoints
          Xg=grid_points(Ipoint)%x
          Yg=grid_points(Ipoint)%y
          Zg=grid_points(Ipoint)%z
          call GET_MO_density_point (Xg, Yg, Zg, CMO%coeff, MO_den, Nbasis, IMO)
          call GET_weight1 (Xg, Yg, Zg, Iatom, WeightA)
          MOrhoA(Ipoint)=WeightA*MO_den
        end do !Ipoint
! Now print the densities for Iatom and IMO
      if(MoleculePrint.or.AtomPrint)then
        PlotFileID=' '
        Znum=CARTESIAN(Iatom)%Atomic_number
        write(Ctemp,'(a,i4,a,i8,2a)')'DENSITY_MESH_MO',IMO,'_atom_',Iatom,'_',element_symbols(Znum)
        call RemoveBlanks (Ctemp, PlotFileID, lenstr)
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)

!       call BLD_plot_file_MO ('DENSITY_MESH', NGridPoints, Iatom, IMO, Plotfile_unit)
      else
        Plotfile_unit=UNIout
      end if
      write(Plotfile_unit,'(a,i8,a,i8)')'MO: ',IMO,' for atom: ',Iatom
      PlotFunction(1:)='|MO(x,y,z)|^2'
      call PRT_GRID_MOL (MOrhoA, NGridPoints, fmt_rho)
      if(MoleculePrint.or.AtomPrint)close (unit=Plotfile_unit)
      end do ! Iatom
      end do ! IMO

      call PRG_manager ('exit','DENSITY_MO_BY_ATOM_MESH', 'DENSITY_MO_BY_ATOM%MESH')
      end subroutine DENSITY_MO_by_ATOM_MESH
      subroutine GRADLEN_DENSITY_OVER_DENSITY_MESH
!******************************************************************************
!     Date last modified: August 15, 2012                                     *
!     Authors: R.A. Poirier                                                   *
!     Description:                                                            *
!                 Calculate DEL charge density on a set of grid points (mesh) *
!******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE module_grid_points
      USE type_density
      USE type_molecule
      USE type_plotting

      implicit none
!
! Local scalars:
      integer :: Ipoint
      double precision :: rho,DELrhoX,DELrhoY,DELrhoZ,GradLen

      double precision :: GET_density_at_xyz
      double precision :: TraceAB
      double precision, allocatable, dimension(:) :: DELrhoOVERrho
      double precision, dimension(:), allocatable  :: dAOprodX,dAOprodY,dAOprodZ
!
! Begin:
      call PRG_manager ('enter','GRADLEN_DENSITY_OVER_DENSITY_MESH', 'GRADLEN_DENSITY_OVER_DENSITY%MESH')
!
      call GET_object ('QM', 'CMO', Wavefunction)

      allocate (dAOprodx(1:MATlen))
      allocate (dAOprody(1:MATlen))
      allocate (dAOprodz(1:MATlen))
      allocate (DELrhoOVERrho(1:NGridPoints))
! Get a temporary file to store plotting data	  
      if(MoleculePrint.or.AtomPrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'GRADLEN_DENSITY_OVER_DENSITY_MESH'
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
      else
        Plotfile_unit=UNIout
      end if
!
       write(Plotfile_unit,'(a)')'The gradient of density over the density:'

      DELrhoOVERrho=ZERO
      do Ipoint=1,NGridPoints
        call dG_products (grid_points(Ipoint)%x, grid_points(Ipoint)%y, grid_points(Ipoint)%z, &
                          dAOprodX, dAOprodY, dAOprodZ, MATlen)
        rho=GET_density_at_xyz (grid_points(Ipoint)%x, grid_points(Ipoint)%y, grid_points(Ipoint)%z)
        DELrhoX=TraceAB (PM0, dAOprodx, NBasis)
        DELrhoY=TraceAB (PM0, dAOprody, NBasis)
        DELrhoZ=TraceAB (PM0, dAOprodz, NBasis)
        GradLen=dsqrt((DELrhoX)**2+(DELrhoY)**2+(DELrhoZ)**2)
        DELrhoOVERrho(Ipoint)=GradLen/rho ! What about rho=zero
      end do ! Ipoint
! print gradient length of density into a separate file if external output is set
       if(MoleculePrint.or.AtomPrint)then
       call PRT_GRID_MOL (DELrhoOVERrho, NGridPoints, fmt_rho)
       close (unit=Plotfile_unit)
      end if

      deallocate (dAOprodx)
      deallocate (dAOprody)
      deallocate (dAOprodz)
      deallocate (DELrhoOVERrho)
!
! End of routine GRADLEN_DENSITY_OVER_DENSITY_MESH
      call PRG_manager ('exit', 'GRADLEN_DENSITY_OVER_DENSITY_MESH', 'GRADLEN_DENSITY_OVER_DENSITY%MESH')
      return
      end subroutine GRADLEN_DENSITY_OVER_DENSITY_MESH
