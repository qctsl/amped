      subroutine BLD_LDMatrix_QTAIM
!****************************************************************************************
!     Date last modified: June 13, 2016                                                 *
!     Authors: R.A. Poirier                                                             *
!     Description:                                                                      *
!     Calculate the Localized-Delocalized matrix using weights.                         *
!****************************************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE NI_defaults
      USE N_integration
      USE QM_objects
      USE QM_defaults
      USE type_density
      USE type_weights
      USE type_plotting
      USE matrix_print

      implicit none
!
! Input scalars:
!
! Local scalars:
      integer :: Iatom,Jatom,Ifound,NAB,NoccMO,ZNumI,ZNumJ
      integer :: Iorbital,Jorbital
      double precision :: Nelec,F_AB,sum_ii,sum_ij
      double precision, dimension(:,:), allocatable :: SA_ij,SB_ij
!
! Local function:
      double precision, dimension(:), allocatable :: AOprod
!
! Begin:
      call PRG_manager ('enter','BLD_LDMatrix_QTAIM', 'LDMATRIX%QTAIM')
!
      call GET_object ('QM', 'CMO', Wavefunction)
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
!
      LDM_type='LMD_'//LDM_QTAIM_type
      if(DEN_partitioning(1:5).eq.'BECKE')call BLD_BeckeW_XI ! Only needed if using Becke weights
      Nelec=ZERO
      NoccMO=CMO%NoccMO
      allocate (SA_ij(NoccMO,NoccMO))
      allocate (SB_ij(NoccMO,NoccMO))
      allocate (AOprod(1:MATlen))
      if(.not.allocated(LDMw))then
        allocate (LDMw(Natoms,Natoms))
      else
        deallocate (LDMw)
        allocate (LDMw(Natoms,Natoms))
      end if
!
! Calculate SA_ij and SB_ij for atoms A and B
      do Iatom=1,Natoms
      ZnumI=CARTESIAN(Iatom)%Atomic_number
      if(ZnumI.le.0)cycle
      Ifound=GRID_loc(ZnumI)
      NApts_atom=NApoints_atom(Ifound)
      call GET_Sij_Atom (Iatom, SA_ij, NoccMO)
      write(UNIout,'(a,i8)')'S_ij for atom',Iatom
      call PRT_matrix (SA_ij, NoccMO, NoccMO)
      sum_ii=ZERO
      do Iorbital=1,NoccMO
      sum_ii=sum_ii+SA_ij(Iorbital,Iorbital)
      end do
      write(UNIout,'(a,i8,2f12.6)')'sum S_ii for atom',Iatom,sum_ii
      LDMw(Iatom,Iatom)=TWO*sum_ii
      
      do Jatom=1,Iatom
      ZnumJ=CARTESIAN(Jatom)%Atomic_number
      if(ZnumJ.le.0)cycle
      if(Iatom.eq.Jatom)cycle
      Ifound=GRID_loc(ZnumJ)
      NApts_atom=NApoints_atom(Ifound)
      if(LDM_QTAIM_type(1:6).eq.'DOUBLE')then
        call GET_Sij_Atom (Jatom, SB_ij, NoccMO)
      else if(LDM_QTAIM_type(1:6).eq.'SINGLE')then
        call GET_Sij_AB (Iatom, Jatom, SB_ij, NoccMO)
      end if
      write(UNIout,'(a,2i8)')'SAB_ij for atoms: ',Iatom,Jatom
      call PRT_matrix (SB_ij, NoccMO, NoccMO)
      F_AB=ZERO
      if(LDM_QTAIM_type(1:6).eq.'DOUBLE')then
      do Iorbital=1,NoccMO
      do Jorbital=1,NoccMO
      F_AB=F_AB+SA_ij(Iorbital,Jorbital)*SB_ij(Jorbital,Iorbital)
      end do ! Jorbital
      end do ! Iorbital
      else if(LDM_QTAIM_type(1:6).eq.'SINGLE')then
      do Iorbital=1,NoccMO
      do Jorbital=1,NoccMO
      F_AB=F_AB+SB_ij(Iorbital,Jorbital)
      end do ! Jorbital
      end do ! Iorbital
      end if
      write(UNIout,'(a,f12.6,a,2i8)')'F_AB:',F_AB,' for atoms: ',Iatom,Jatom
!     LDMw(Iatom,Jatom)=TWO*dabs(F_AB)
      LDMw(Iatom,Jatom)=dabs(F_AB)
      LDMw(Jatom,Iatom)=LDMw(Iatom,Jatom)
      LDMw(Iatom,Iatom)=LDMw(Iatom,Iatom)-PT5*dabs(F_AB)
      LDMw(Jatom,Jatom)=LDMw(Jatom,Jatom)-PT5*dabs(F_AB)
      Nelec=Nelec+LDMw(Iatom,Jatom)
      end do ! Jatom
      end do ! Iatom
      do Iatom=1,Natoms
      Nelec=Nelec+LDMw(Iatom,Iatom)
      end do ! Iatom

      write(UNIout,'(a,f12.6)')'Total number of electrons: ',Nelec
      write(UNIout,'(a)')'LDM '
      Call PRT_matrix (LDMw, Natoms, Natoms)

      deallocate (AOprod)

      call BLD_LDMatrixW_SuperAtom
!
! End of routine BLD_LDMatrix_QTAIM
      call PRG_manager ('exit', 'BLD_LDMatrix_QTAIM', 'LDMATRIX%QTAIM')
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine GET_Sij_Atom (Iatom, S_ij, NoccMO)
!****************************************************************************************
!     Date last modified: June 13, 2016                                                 *
!     Authors: R.A. Poirier                                                             *
!     Description: Calculate S_ij for a given atom                                      *
!****************************************************************************************

      implicit none
!
! Input scalars:
      integer :: Iatom,NoccMO
      double precision :: S_ij(NoccMO,NoccMO)
!
! Local scalars:
      integer :: IJ,Iorbital,Jorbital,Ipoint,Mu,Nu
      double precision :: fac,Psi_ij
      double precision :: XApt,YApt,ZApt,WeightA

! Begin:
      S_ij=ZERO
!           
! Loop over grid points of Iatom
      do Ipoint=1,NApts_atom
        XApt=Egridpts(Ifound,Ipoint)%X+CARTESIAN(Iatom)%X
        YApt=Egridpts(Ifound,Ipoint)%Y+CARTESIAN(Iatom)%Y
        ZApt=Egridpts(Ifound,Ipoint)%Z+CARTESIAN(Iatom)%Z
        call GET_Weight1 (XApt, YApt, ZApt, Iatom, WeightA)
        if(WeightA.le.WCutoff)cycle
        call Gaussian_products (XApt, YApt, ZApt, AOprod, MATlen)
        do Iorbital=1,NoccMO
        do Jorbital=1,Iorbital
! Calculate int W_A(r) Psi_1(r) Psi_j(r) dr 
        Psi_ij=ZERO
        do Mu=1,Nbasis
        do Nu=1,Mu
          fac=ONE
          if(Mu.ne.Nu)fac=TWO
          IJ=Mu*(Mu-1)/2+Nu
          Psi_ij=Psi_ij+fac*CMO%coeff(Mu,Iorbital)*CMO%coeff(Nu,Jorbital)*AOprod(IJ)
        end do ! Nu
        end do ! Mu
        S_ij(Iorbital,Jorbital)=S_ij(Iorbital,Jorbital)+Psi_ij*WeightA*Egridpts(Ifound,Ipoint)%W
        S_ij(Jorbital,Iorbital)=S_ij(Iorbital,Jorbital)
        end do ! Jorbital
        end do ! Iorbital
      end do ! Ipoint
      S_ij=FourPi*S_ij

      return
      end subroutine GET_Sij_Atom
      subroutine GET_Sij_AB (Iatom, Jatom, S_ij, NoccMO)
!****************************************************************************************
!     Date last modified: June 13, 2016                                                 *
!     Authors: R.A. Poirier                                                             *
!     Description: Calculate S_ij for atom A and atom B                                 *
!****************************************************************************************

      implicit none
!
! Input scalars:
      integer :: Iatom,Jatom,NoccMO
      double precision :: S_ij(NoccMO,NoccMO)
!
! Local scalars:
      integer :: Atom,IJ,Ifound,Iorbital,Jorbital,Ipoint,Mu,Nu,NApts_atom,Znum
      double precision :: fac,Psi_ij
      double precision :: XApt,YApt,ZApt,WeightA,WeightB
      double precision :: WCutoff
      parameter (WCutoff=1.0D-10)

! Begin:
      S_ij=ZERO
!           
      do Atom=1,2
      if(Atom.eq.1)then
      Znum=CARTESIAN(Iatom)%Atomic_number
      Ifound=GRID_loc(Znum)
      NApts_atom=NApoints_atom(Ifound)
      else
      Znum=CARTESIAN(Iatom)%Atomic_number
      Ifound=GRID_loc(Znum)
      NApts_atom=NApoints_atom(Ifound)
      end if
! Loop over grid points of Iatom
      do Ipoint=1,NApts_atom
        XApt=Egridpts(Ifound,Ipoint)%X+CARTESIAN(Iatom)%X
        YApt=Egridpts(Ifound,Ipoint)%Y+CARTESIAN(Iatom)%Y
        ZApt=Egridpts(Ifound,Ipoint)%Z+CARTESIAN(Iatom)%Z
        call GET_Weight1 (XApt, YApt, ZApt, Iatom, WeightA)
        if(WeightA.le.WCutoff)cycle
        call GET_Weight1 (XApt, YApt, ZApt, Jatom, WeightB)
        if(WeightB.le.WCutoff)cycle
        call Gaussian_products (XApt, YApt, ZApt, AOprod, MATlen)
        do Iorbital=1,NoccMO
        do Jorbital=1,Iorbital
! Calculate int W_A(r) Psi_1(r) Psi_j(r) dr 
        Psi_ij=ZERO
        do Mu=1,Nbasis
        do Nu=1,Mu
          fac=ONE
          if(Mu.ne.Nu)fac=TWO
          IJ=Mu*(Mu-1)/2+Nu
          Psi_ij=Psi_ij+fac*CMO%coeff(Mu,Iorbital)*CMO%coeff(Nu,Jorbital)*AOprod(IJ)
        end do ! Nu
        end do ! Mu
        S_ij(Iorbital,Jorbital)=S_ij(Iorbital,Jorbital)+Psi_ij*dsqrt(WeightA*WeightB)*Egridpts(Ifound,Ipoint)%W
        S_ij(Jorbital,Iorbital)=S_ij(Iorbital,Jorbital)
        end do ! Jorbital
        end do ! Iorbital
      end do ! Ipoint
      end do ! Atom
      S_ij=FourPi*S_ij

      return
      end subroutine GET_Sij_AB
! END CONTAINS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine BLD_LDMatrix_QTAIM
      subroutine GET_ORBITAL_Atom (Iatom, Iorbital)
!****************************************************************************************
!     Date last modified: June 26, 2015                                                 *
!     Authors: R.A. Poirier                                                             *
!     Description:                                                                      *
!     Calculate the orbital psi(r) for a given orbital and a given atom                 *
!****************************************************************************************
! Modules:
      USE program_constants
      USE N_integration
      USE QM_objects
      USE type_molecule
      USE type_density

      implicit none
!
! Input scalars:
      integer :: Iatom,Iorbital
!
! Local scalars:
      integer :: Ifound,Ipoint,NoccMO,Znum
      integer :: IJ,Mu,Nu
      double precision :: fac,sum
      double precision :: XApt,YApt,ZApt,WeightA
!
! Local function:
      double precision, dimension(:), allocatable :: AOprod
!
! Begin:
      call PRG_manager ('enter','GET_ORBITAL_ATOM', 'UTILITY')
!
      NoccMO=CMO%NoccMO
      call BLD_BeckeW_XI
      allocate (AOprod(1:MATlen))
      Znum=CARTESIAN(Iatom)%Atomic_number
      Ifound=GRID_loc(Znum)
      NApts_atom=NApoints_atom(Ifound)
      if(.not.allocated(orbital_Atom))then
        allocate (orbital_Atom(NApts_atom))
      else
        deallocate (orbital_Atom)
        allocate (orbital_Atom(NApts_atom))
      end if
!           
! Loop over grid points of Iatom
      do Ipoint=1,NApts_atom
        XApt=Egridpts(Ifound,Ipoint)%X+CARTESIAN(Iatom)%X
        YApt=Egridpts(Ifound,Ipoint)%Y+CARTESIAN(Iatom)%Y
        ZApt=Egridpts(Ifound,Ipoint)%Z+CARTESIAN(Iatom)%Z
        call AO_products (XApt, YApt, ZApt, AOprod, MATlen)
        call GET_Weight1 (XApt, YApt, ZApt, Iatom, WeightA)
! Calculate the value of orbital Iorbital at grid point XApt,YApt,ZApt for Iatom
      sum=ZERO
      do Mu=1,Nbasis
      do Nu=1,Mu
        fac=ONE
        if(Mu.ne.Nu)Fac=TWO
        IJ=Mu*(Mu-1)/2+Nu
        sum=sum+FAC*CMO%coeff(Mu,Iorbital)*CMO%coeff(Nu,Iorbital)*AOprod(IJ)
      end do ! Nu
      end do ! Mu
      orbital_Atom(Ipoint)=WeightA*sum

      end do ! Ipoint
      deallocate (AOprod)
!
! End of routine GET_ORBITAL_ATOM
      call PRG_manager ('exit', 'GET_ORBITAL_ATOM', 'UTILITY')
      return
      end
      subroutine BLD_LDMatrixW_SuperAtom
!****************************************************************************************
!     Date last modified: June 13, 2016                                                 *
!     Authors: R.A. Poirier                                                             *
!     Description: The atoms that form the Super Atom must be the last atoms!           *
!****************************************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE NI_Nelectrons
      USE NI_defaults
      USE QM_objects
      USE QM_defaults
      USE type_density
      USE type_weights
      USE type_plotting
      USE matrix_print

      implicit none
!
! Local scalars:
      integer :: Iatom,Jatom,NAsubGroup,NAKeep,NAKeep1,NAKeep2,NANew,ZNumI,ZNumJ,Save_UNIout
      integer :: File_unit
      character(len=64) :: File_name
!
! Local function:
!
! Begin:
      call PRG_manager ('enter','BLD_LDMatrixW_SuperAtom', 'UTILITY')
!
      NAsubGroup=NAIMprint
      NAKeep=Natoms-NAsubGroup
      NANew=NAKeep+1

      allocate (LDMw_SA(NANew,NANew))
!
      LDMw_SA=ZERO
      if(NAIMprint.gt.1)then

      NAKeep1=NAKeep+1
! First deal with all the rows for atoms that are kept.
      do Iatom=1,NAKeep
      ZnumI=CARTESIAN(Iatom)%Atomic_number
      if(ZnumI.le.0)cycle
      do Jatom=1,Natoms
      ZnumJ=CARTESIAN(Jatom)%Atomic_number
      if(ZnumJ.le.0)cycle
      if(Jatom.le.NAKeep)then
        LDMw_SA(Iatom,Jatom)=LDMw(Iatom,Jatom) 
      else
        LDMw_SA(Iatom,NAKeep1)=LDMw_SA(Iatom,NAKeep1)+LDMw(Iatom,Jatom) 
      end if
      end do ! Jatom
      end do ! Iatom

! Now deal with all the rows for atoms that are to be summed.
      do Iatom=NAKeep1,Natoms
      ZnumI=CARTESIAN(Iatom)%Atomic_number
      if(ZnumI.le.0)cycle
      do Jatom=1,Natoms
      ZnumJ=CARTESIAN(Jatom)%Atomic_number
      if(ZnumJ.le.0)cycle
      if(Jatom.le.NAKeep)then
      LDMw_SA(NAKeep1,Jatom)=LDMw_SA(NAKeep1,Jatom)+LDMw(Iatom,Jatom) 
      else
      LDMw_SA(NAKeep1,NAKeep1)=LDMw_SA(NAKeep1,NAKeep1)+LDMw(Iatom,Jatom) 
      end if
      end do ! Jatom
      end do ! Iatom

      else
        LDMw_SA=LDMw
      end if

      write(UNIout,'(a)')'LDM REduced'
      call PRT_matrix (LDMw_SA, NANew, NANew)

      PlotFileID=' '
      write(PlotFileID,'(a)')LDM_type
      call BLD_FileID
      File_name='LDM_'//trim(Basic_FileID)//'_'//trim(PlotFileID)//'.dat'
      call GET_file_unit ('REPLACE', File_name, 'FORMATTED', PlotFileID, File_unit)
      write(PlotFunction,'(a)')'Localized-Delocalized Matrix'
      Save_UNIout=UNIout
      UNIout=File_unit
      call PRT_matrix (LDMw_SA, NANew, NANew)
      close (unit=File_unit)
      UNIout=Save_UNIout
!
! End of routine BLD_LDMatrixW
      call PRG_manager ('exit', 'BLD_LDMatrixW_SuperAtom', 'UTILITY')
      return
      end subroutine BLD_LDMatrixW_SuperAtom
