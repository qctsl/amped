      MODULE type_density
!********************************************************************************************
!     Date last modified: May 24, 2011                                                      *
!     Author: Ahmad and R.A. Poirier                                                        *
!     Description: Define grid points for charge density  and delrho                        *
!********************************************************************************************
!
      implicit none
!
! Save the charge density at every point
      double precision, dimension(:), allocatable :: CHARGE_DENSITY
      double precision, dimension(:), allocatable :: rho_Atom

      type  :: type_delrho
        double precision    :: X
        double precision    :: Y
        double precision    :: Z
        double precision    :: length
      end type type_delrho

      type(type_delrho), dimension(:), allocatable :: delrho
      type(type_delrho), dimension(:), allocatable :: delrho_Atom
      type  :: type_del2rho
        double precision    :: XX
        double precision    :: YY
        double precision    :: ZZ
        double precision    :: XY
        double precision    :: XZ
        double precision    :: YZ
      end type type_del2rho
      type(type_del2rho), dimension(:), allocatable :: del2rho_Atom

      double precision, dimension(:), allocatable :: orbital_Atom
      type  :: type_delOrbital
        double precision    :: X
        double precision    :: Y
        double precision    :: Z
      end type type_delOrbital
      type  :: type_del2Orbital
        double precision    :: X
        double precision    :: Y
        double precision    :: Z
      end type type_del2Orbital
      double precision, dimension(:), allocatable :: delorbital_Atom
      double precision, dimension(:), allocatable :: del2orbital_Atom

! Save the gradient of charge density at every point
      double precision, allocatable, dimension(:) :: Rden
      double precision, dimension(:), allocatable :: DEL_DENSITY_X
      double precision, dimension(:), allocatable :: DEL_DENSITY_Y
      double precision, dimension(:), allocatable :: DEL_DENSITY_Z
      double precision, dimension(:), allocatable :: DEL2_DENSITY_XX
      double precision, dimension(:), allocatable :: DEL2_DENSITY_YY
      double precision, dimension(:), allocatable :: DEL2_DENSITY_ZZ
      double precision, dimension(:), allocatable :: DEL2_DENSITY_XY
      double precision, dimension(:), allocatable :: DEL2_DENSITY_XZ
      double precision, dimension(:), allocatable :: DEL2_DENSITY_YZ

! Localized-Delocalized Matrix
      double precision, dimension(:,:), allocatable :: LDMw
! Localized-Delocalized Matrix Reduced (Super Atom)
      double precision, dimension(:,:), allocatable :: LDMw_SA
!
      end MODULE type_density   
