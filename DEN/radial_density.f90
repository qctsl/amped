      subroutine RADIAL_DENSITY_MESH
!*********************************************************************************************
!     Date last modified: December 19, 2012                                                  *
!     Authors: R.A. Poirier & Jessica Besaw                                                  *
!     Description:                                                                           *
!                 Calculate the radial density on a set of grid points (mesh)                *
!*********************************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE module_grid_points
      USE type_density
      USE type_molecule
      USE type_plotting
      USE type_Weights

      implicit none 

! Local scalars:
      integer :: Ipoint,Iatom,Znum,lenstr
      double precision :: Xg,Yg,Zg,Xa,Ya,Za
      double precision :: WeightA,RadSq,Den_coef
 
! Local Array
      double precision, allocatable,dimension(:,:) :: Rden_ATOM
      double precision, dimension(:), allocatable :: AOprod
!
! Local functions:
      double precision :: TraceAB
!
! Begin:
      call PRG_manager ('enter','RADIAL_DENSITY_MESH', 'RADIAL_DENSITY%MESH')
!
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)
      if(DEN_partitioning(1:5).eq.'BECKE')call BLD_BeckeW_XI ! Only needed if using Becke weights
!
      allocate (AOprod(1:MATlen))
      allocate(CHARGE_DENSITY(NGridPoints))
      allocate(Rden(NGridPoints))
      allocate(Rden_ATOM(Natoms,NGridPoints)) 
  
      Rden=ZERO
      Rden_ATOM=ZERO
      do Ipoint=1,NGridPoints
        Den_coef=0.0D0
         Xg=grid_points(Ipoint)%x
         Yg=grid_points(Ipoint)%y
         Zg=grid_points(Ipoint)%z         
         call AO_products (grid_points(Ipoint)%x, grid_points(Ipoint)%y, grid_points(Ipoint)%z, AOprod, MATlen)
         CHARGE_DENSITY(Ipoint)=TraceAB (PM0, AOprod, NBasis)

        do Iatom=1,Natoms 
          Znum=CARTESIAN(Iatom)%Atomic_number
          if(Znum.le.0)cycle
          Xa=CARTESIAN(Iatom)%X
          Ya=CARTESIAN(Iatom)%Y
          Za=CARTESIAN(Iatom)%Z
          RadSq=(Xg-Xa)**2+(Yg-Ya)**2+(Zg-Za)**2          
          call GET_weight1 (XG, YG, ZG, Iatom, WeightA)
          Rden_ATOM(Iatom, Ipoint)=WeightA*RadSq*CHARGE_DENSITY(Ipoint)
          den_coef=den_coef+WeightA*RadSq
        end do !Iatom

        Rden(Ipoint)=den_coef*CHARGE_DENSITY(Ipoint)
      end do ! Ipoint

! Print coordinates a radial density to a separate data file for graphing using Mathematica 
! Print the atom in molecule radial density 
      if(AtomPrint)then
      do Iatom=1,Natoms  
        Znum=CARTESIAN(Iatom)%Atomic_number
        if(Znum.le.0)cycle
        PlotFileID=' '
        PlotFunction=' '
        write(PlotFunction,'(a,i4)')'Radial Density Atomic for atom ',Iatom
        write(PlotFileID,'(a)')'RADIAL_DENSITY_ATOMIC'
        call BLD_plot_file_ATOMIC (PlotFileID, NGridPoints, Iatom, Plotfile_unit)
        call PRT_MESH_ATOMIC (Rden_ATOM, Iatom, Natoms, NGridPoints)
        close (unit=Plotfile_unit)
      end do ! Iatom
     end if

! Print the molecular radial density 
     if(MoleculePrint)then
       PlotFileID=' '
       write(PlotFileID,'(a)')'RADIAL_DENSITY_MESH'
       call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
       write(PlotFunction,'(a)')'Molecular Radial Density'
       call PRT_GRID_MOL (Rden, NGridPoints, fmt_r2rho)
       close (unit=Plotfile_unit)
      else ! print to standard output
       write(PlotFunction,'(a)')'Molecular Radial Density'
       Plotfile_unit=UNIout
       call PRT_GRID_MOL (Rden, NGridPoints, fmt_r2rho)
      end if  

      deallocate (AOprod,CHARGE_DENSITY,Rden,Rden_Atom)
!
! End of routine RADIAL_DENSITY_MESH
      call PRG_manager ('exit', 'RADIAL_DENSITY_MESH', 'RADIAL_DENSITY%MESH')
      return
      end subroutine RADIAL_DENSITY_MESH
      subroutine DEL_RADIAL_DENSITY_MESH
!********************************************************************
!     Date last modified: August 15, 2012                           *
!     Authors: R.A. Poirier                                         *
!     Description: Calculate del charge density at point.           *
!********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE type_density
      USE module_grid_points
      USE type_plotting
      USE type_Weights

      implicit none
!
! Local scalars:
      integer :: Iatom,Ipoint
      double precision :: rho,Del_rho,RvalSq,ValueX,ValueY,ValueZ
      double precision :: Weight1,Xa,Ya,Za,XVal,YVal,ZVal,Znum
      double precision :: DEL_rhoX,DEL_rhoY,DEL_rhoZ
      double precision :: Xmin,Ymin,Zmin,DXmin,DYmin,DZmin,MinGL
      double precision :: DEL_W1(3)
      double precision, dimension(:), allocatable :: ddX,ddY,ddZ
      double precision, dimension(:), allocatable :: GradLen
      double precision, dimension(:,:), allocatable :: del_W
      double precision, dimension(:), allocatable :: WeightA
      character(len=32) :: FMT_gridsPoints
!
! Local functions:
      double precision :: TraceAB
      double precision, dimension(:), allocatable :: AOprod
      double precision, dimension(:), allocatable  :: dAOprodX,dAOprodY,dAOprodZ
!
! Begin:
      call PRG_manager ('enter','DEL_RADIAL_DENSITY_MESH', 'DEL_RADIAL_DENSITY%MESH')
!
!     call GET_object ('GRID', 'GRIDS', 'MESH')   ! The MENU must build the MESH!!
      call GET_object ('QM', 'CMO', Wavefunction)
      if(DEN_partitioning(1:5).eq.'BECKE')call BLD_BeckeW_XI  ! this finds XI

      allocate (AOprod(1:MATlen))
      allocate (dAOprodx(1:MATlen))
      allocate (dAOprody(1:MATlen))
      allocate (dAOprodz(1:MATlen))
      allocate (ddX(NGridPoints))
      allocate (ddY(NGridPoints))
      allocate (ddZ(NGridPoints))
      allocate (GradLen(NGridPoints))
      allocate (del_W(Natoms,3))
      allocate (WeightA(Natoms))

      FMT_gridsPoints='(3'//fmt_GridPts//',4'//trim(fmt_dr2rho)//')'
      write(UNIout,'(a)')'The first derivatives of radial density:'
      write(UNIout,'(2a)')'Weights used:     ',DEN_partitioning

      write(UNIout,'(a)')'X, Y, Z, dX, dY, dZ, gradlen:'
      MinGL=999.0D0
      DXmin=999.0D0
      DYmin=999.0D0
      DZmin=999.0D0
      Xmin=999.0D0
      Ymin=999.0D0
      Zmin=999.0D0
      ddX=ZERO
      ddY=ZERO
      ddZ=ZERO
      do Ipoint=1,NGridPoints
        XVal=grid_points(Ipoint)%x
        YVal=grid_points(Ipoint)%y
        ZVal=grid_points(Ipoint)%z
        call AO_products (XVal, YVal, ZVal, AOprod, MATlen)
        rho= TraceAB (PM0, AOprod, NBasis)
        call dG_products (XVal, YVal, ZVal, dAOprodX, dAOprodY, dAOprodZ, MATlen)
        DEL_rhoX=TraceAB (PM0, dAOprodX, NBasis)
        DEL_rhoY=TraceAB (PM0, dAOprodY, NBasis)
        DEL_rhoZ=TraceAB (PM0, dAOprodZ, NBasis)

! Now compute the contribution to each atom:
       ValueX=ZERO
       ValueY=ZERO
       ValueZ=ZERO
       if(LdW_analytical)then
         call GET_dWeights (XVAL, YVAL, ZVAL, del_W, WeightA, Natoms) ! For all atoms
       else
         call GET_dWN_NUMERICAL (XVAL, YVAL, ZVAL, del_W, WeightA, Natoms) ! For all atoms
       end if
       do Iatom=1,Natoms 
         Znum=CARTESIAN(Iatom)%Atomic_number
         if(Znum.le.0)cycle
         Xa=CARTESIAN(Iatom)%X
         Ya=CARTESIAN(Iatom)%Y
         Za=CARTESIAN(Iatom)%Z          
         RvalSq=(XVal-Xa)**2+(YVal-Ya)**2+(ZVal-Za)**2
           ValueX=ValueX+WeightA(IAtom)*(TWO*(XVal-Xa)*rho+RvalSq*DEL_rhoX)+RvalSq*del_W(Iatom,1)*rho
           ValueY=ValueY+WeightA(IAtom)*(TWO*(YVal-Ya)*rho+RvalSq*DEL_rhoY)+RvalSq*del_W(Iatom,2)*rho
           ValueZ=ValueZ+WeightA(IAtom)*(TWO*(ZVal-Za)*rho+RvalSq*DEL_rhoZ)+RvalSq*del_W(Iatom,3)*rho
       end do !Iatom
         ddX(Ipoint)=ValueX
         ddY(Ipoint)=ValueY
         ddZ(Ipoint)=ValueZ
         GradLen(Ipoint)=sqrt(ValueX**2+ValueY**2+ValueZ**2)
         write(UNIout,FMT_gridsPoints)XVal,YVal,ZVal,ValueX,ValueY,ValueZ,GradLen(Ipoint)
         if(GradLen(Ipoint).lt.MinGL)then
           MinGL=GradLen(Ipoint)
           DXmin=ValueX
           DYmin=ValueY
           DZmin=ValueZ
           Xmin=Xval
           Ymin=Yval
           Zmin=Zval
         end if
      end do ! Ipoint
      write(UNIout,'(a,f12.6)')'Minimum gradient length: ',MinGL
      write(UNIout,'(a,3f12.6)')'Gradient: ',DXmin,DYmin,DZmin
      write(UNIout,'(a,3f12.6)')'Grid point: ',Xmin,Ymin,Zmin

     if(GrdLenPrint)then
       PlotFileID=' '
       write(PlotFileID,'(a)')'GRDLEN_RDENSITY_MESH'
       call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
       PlotFunction(1:)='gradlen radial density'
       call PRT_GRID_MOL (GradLen, NGridPoints, fmt_r2rho)
       close (unit=Plotfile_unit)
      end if

     if(MoleculePrint)then
       if(NX.ne.0)then
       PlotFileID=' '
       write(PlotFileID,'(a)')'X_GRADIENT_RADIAL_DENSITY_MESH'
       call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
       write(PlotFunction,'(a)')'Molecular X-Gradient Radial Density'
       call PRT_GRID_MOL (ddX, NGridPoints, fmt_r2rho)
       close (unit=Plotfile_unit)
       end if
       if(NY.ne.0)then
       PlotFileID=' '
       write(PlotFileID,'(a)')'Y_GRADIENT_RADIAL_DENSITY_MESH'
       call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
       write(PlotFunction,'(a)')'Molecular Y-Gradient Radial Density'
       call PRT_GRID_MOL (ddY, NGridPoints, fmt_r2rho)
       close (unit=Plotfile_unit)
       end if
       if(NZ.ne.0)then
       PlotFileID=' '
       write(PlotFileID,'(a)')'Z_GRADIENT_RADIAL_DENSITY_MESH'
       call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
       write(PlotFunction,'(a)')'Molecular Z-Gradient Radial Density'
       call PRT_GRID_MOL (ddZ, NGridPoints, fmt_r2rho)
       close (unit=Plotfile_unit)
       end if
      end if

      deallocate (AOprod)
      deallocate (dAOprodx)
      deallocate (dAOprody)
      deallocate (dAOprodz)
      deallocate (GradLen)
      deallocate (del_W)
      deallocate (WeightA)
!
! End of routine DEL_RADIAL_DENSITY_MESH
      call PRG_manager ('exit', 'DEL_RADIAL_DENSITY_MESH', 'DEL_RADIAL_DENSITY%MESH')
      return
      end subroutine DEL_RADIAL_DENSITY_MESH
      subroutine DEL2_RADIAL_DENSITY_MESH
!********************************************************************
!     Date last modified: August 15, 2012                           *
!     Authors: R.A. Poirier                                         *
!     Description: Calculate del charge density at point.           *
!********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE type_density
      USE module_grid_points
      USE module_type_basin
      USE type_plotting
      USE type_Weights

      implicit none
!
! Local scalars:
      integer :: Iatom,Ipoint
      double precision :: rho,Del_rho,VAlueXX,VAlueYY,VAlueZZ,VAlueXY,VAlueXZ,VAlueYZ,RvalSq
      double precision :: WeightA,Xa,Ya,Za,XVal,YVal,ZVal,Znum
      double precision :: DEL_rhoX,DEL_rhoY,DEL_rhoZ
      double precision :: DEL2_rho_XX,DEL2_rho_YY,DEL2_rho_ZZ,DEL2_rho_XY,DEL2_rho_XZ,DEL2_rho_YZ
      double precision :: NEGdel2rho,BWeight
      double precision, dimension(3,3) :: Hess,EigVector
      double precision, dimension(3) :: EigValue
      double precision :: DEL_W(3),DEL2_W(3,3)
!
! Begin:
      call PRG_manager ('enter','DEL2_RADIAL_DENSITY_MESH', 'DEL2_RADIAL_DENSITY%MESH')
!
      call GET_object ('QM', 'CMO', Wavefunction)
      if(DEN_partitioning(1:5).eq.'BECKE')call BLD_BeckeW_XI  ! this finds XI

      do Ipoint=1,NGridPoints
        XVal=grid_points(Ipoint)%x
        YVal=grid_points(Ipoint)%y
        ZVal=grid_points(Ipoint)%z
        call DEL2_RADIAL_DENSITY (XVal, YVal,ZVal, Hess)
        write(UNIout,'(/a,3f10.6)')'Grid point: ',XVal,YVal,ZVal

! Now compute the contribution to each atom:
       write(UNIout,'(a)')'DEL2_rad at grid point: d2XX,d2YY,d2ZZ,d2XY,d2XZ,d2YZ'
       write(UNIout,Pfmt_d2Rad)Hess(1,1),Hess(2,2),Hess(3,3),Hess(1,2),Hess(1,3),Hess(2,3)

        call MATRIX_diagonalize (Hess, EigVector, EigValue, 3, 2, .true.)
        NEGdel2rho = -(EigValue(1)+EigValue(2)+EigValue(3))

        write(UNIout,'(a)')'Eig1,Eig2,Eig3,NEGdel2rho:'
! Format for now - needs to be change (Pfmt_Eig)
        write(UNIout,'(4f20.6)')EigValue(1),EigValue(2),EigValue(3),NEGdel2rho
        call CP_classification (EigValue, CPRS)
        write(UNIout,'(2a)')'Rank and Signature of the critical point is: ',CPRS
      end do ! Ipoint

!
! End of routine DEL2_RADIAL_DENSITY_MESH
      call PRG_manager ('exit', 'DEL2_RADIAL_DENSITY_MESH', 'DEL2_RADIAL_DENSITY%MESH')
      return
      end subroutine DEL2_RADIAL_DENSITY_MESH
      subroutine CP_classification (EigValues, CPRS)
!********************************************************************
!     Date last modified: August 15, 2012                           *
!     Authors: R.A. Poirier                                         *
!     Description: Calculate the charge density at point.           *
!********************************************************************
! Modules:
      USE program_constants

      implicit none
!
! Input array:
      double precision :: EigValues(3)
!
! Output scalar:
      character(*) :: CPRS ! (R,S)
!
! Local scalars:
      integer :: Ieig,Rank,Signature
      double precision :: cutoff
      parameter (cutoff=5.0E-4)
!
! Begin:
      call PRG_manager ('enter','CP_classification', 'UTILITY')
!
      Rank=0
      Signature=0
      do Ieig=1,3
        if(dabs(EigValues(Ieig)).ge.cutoff)then
          Rank=Rank+1
        else
          EigValues(Ieig)=ZERO
        end if
        if(EigValues(Ieig).lt.ZERO)Signature=Signature-1
        if(EigValues(Ieig).gt.ZERO)Signature=Signature+1
      end do ! Ieig
      write(CPRS,'(a,i1,a,i2,a)')'(',Rank,',',Signature,')'
!
! End of routine CP_classification
      call PRG_manager ('exit', 'CP_classification', 'UTILITY')
      return
      end subroutine CP_classification
      subroutine DEL_RADIAL_DENSITY_NUMERICAL
!********************************************************************
!     Date last modified: August 15, 2012                           *
!     Authors: R.A. Poirier                                         *
!     Description: Calculate del charge density at point.           *
!********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE QM_defaults
      USE QM_objects
      USE OPT_objects
      USE type_density
      USE module_grid_points

      implicit none
!
! Local scalars:
      integer :: Ipoint
!
! Begin:
      call PRG_manager ('enter','DEL_RADIAL_DENSITY_NUMERICAL', 'UTILITY')
!
      call GET_object ('QM', 'RADIAL_DENSITY', 'MESH')
      call GET_object ('OPT', 'PARAMETERS', 'GRIDPOINT')

      OPT_parameters%name='GRIDPOINT'
      OPT_function%name='RADIAL_DENSITY'
!     do Ipoint=1,NGridPoints
!       OPT_par(Ishell)%value=Basis%shell(Ishell)%SHLSCL
        Ipoint=1
        PARset(1)=grid_points(Ipoint)%x
        PARset(2)=grid_points(Ipoint)%y
        PARset(3)=grid_points(Ipoint)%z
        call Numerical_First
!     end do
!
! End of routine DEL_RADIAL_DENSITY_NUMERICAL
      call PRG_manager ('exit', 'DEL_RADIAL_DENSITY_NUMERICAL', 'UTILITY')
      return
      end subroutine DEL_RADIAL_DENSITY_NUMERICAL
      subroutine Find_CP_Radial_density
!*************************************************************************************
!     Date last modified: August 15, 2012                                            *
!     Authors: R.A. Poirier                                                          *
!     Description:                                                                   *
!         Calculate the radial density at a set a grid points (e.g., SG1, G20, ...)  *
!*************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE module_grid_points
      USE type_density
      USE type_Weights

      implicit none
!
! Local scalars:
      integer :: Icycles,Ipoint,IXYZ,ILower,Znum
      double precision :: XYZptP(3),XYZptM(3),delXYZ(3)
      double precision :: GradLen,GradLenP,GradlenM,MIN_GradLen,MIN_GradLenP
      double precision :: XYZVal(3)
      double precision :: dRdX,dRdY,dRdZ,dRdXP,dRdYP,dRdZP,dRdXM,dRdYM,dRdZM
      double precision :: rho,DEL_rhoX,DEL_rhoY,DEL_rhoZ
      double precision :: Xpt_Min,Ypt_Min,Zpt_Min
      double precision :: ddx_Min,ddy_Min,ddz_Min

      double precision, dimension(:,:), allocatable :: del_W
      double precision, dimension(:), allocatable :: WeightA
      double precision :: cutoff
      parameter (cutoff=1.0E-6)
!
! Begin
      call PRG_manager ('enter', 'Find_CP_Radial_density', 'UTILITY')

      call GET_object ('QM', 'CMO', Wavefunction)  ! Get the density matrix (PM0) 
      if(DEN_partitioning(1:5).eq.'BECKE')call BLD_BeckeW_XI ! Only needed if using Becke weights

      allocate (del_W(Natoms,3))
      allocate (WeightA(Natoms))
!
      write(6,*)'NGridPoints: ',NGridPoints
      do Ipoint=1,NGridPoints
        delXYZ(1)=GRID_mesh(1)
        delXYZ(2)=GRID_mesh(2)
        delXYZ(3)=GRID_mesh(3)
        XYZVal(1)=grid_points(Ipoint)%x
        XYZVal(2)=grid_points(Ipoint)%y
        XYZVal(3)=grid_points(Ipoint)%z
        call DEL_RADIAL_DENSITY (XYZVal(1), XYZVal(2), XYZVal(3), dRdX, dRdY, dRdZ)
        GradLen=sqrt(dRdX**2+dRdY**2+dRdZ**2)
        write(UNIout,'(/a,3f20.6)')'Critical points for origin: ',XYZVal
        write(UNIout,'(a,4f12.6)')'Gradients:         ',dRdX,dRdY,dRdZ,GradLen
        Xpt_Min=XYZVal(1)
        Ypt_Min=XYZVal(2)
        Zpt_Min=XYZVal(3)
        ddx_Min=dRdX
        ddy_Min=dRdY
        ddz_Min=dRdZ

        ILower=0
        MIN_GradLen=GradLen
        MIN_GradLenP=GradLen
        do Icycles=1,MAXCycles
        if(ILower.gt.2)exit
        if(MIN_GradLen.le.cutoff)exit
        XYZptP=XYZVal
        XYZptM=XYZVal
        write(UNIout,'(a,i4,3f12.6)')'delXYZ   : ',Icycles,delXYZ
        do IXYZ=1,3
        XYZptP(IXYZ)=XYZptP(IXYZ)+delXYZ(IXYZ)
        call DEL_RADIAL_DENSITY (XYZptP(1), XYZptP(2), XYZptP(3), dRdXP, dRdYP, dRdZP)
        GradLenP=sqrt(dRdXP**2 + dRdYP**2 + dRdZP**2)
        XYZptM(IXYZ)=XYZptM(IXYZ)-delXYZ(IXYZ)
        call DEL_RADIAL_DENSITY (XYZptM(1), XYZptM(2), XYZptM(3), dRdXM, dRdYM, dRdZM)
        GradLenM=sqrt(dRdXM**2 + dRdYM**2 + dRdZM**2)
        if(GradLenP.lt.MIN_GradLen)then
          ILower=0
          XYZVal(IXYZ)=XYZptP(IXYZ)
          MIN_GradLen=GradLenP
          ddx_Min=dRdXP
          ddy_Min=dRdYP
          ddz_Min=dRdZP
        write(UNIout,'(a,i4,4f12.6)')'XYZP     : ',IXYZ,XYZVal,MIN_GradLen
        else if(GradLenM.lt.MIN_GradLen)then
          ILower=0
          XYZVal(IXYZ)=XYZptM(IXYZ)
          MIN_GradLen=GradLenM
          ddx_Min=dRdXM
          ddy_Min=dRdYM
          ddz_Min=dRdZM
        write(UNIout,'(a,i4,4f12.6)')'XYZM     : ',IXYZ,XYZVal,MIN_GradLen
        else
        write(UNIout,'(a,i4,4f12.6)')'No change: ',IXYZ,XYZVal,MIN_GradLen
        if((ILower.eq.1).and.(delXYZ(IXYZ).ge.1.0E-5))delXYZ(IXYZ)=delXYZ(IXYZ)/10.0D0
        end if
        end do ! IXYZ
        if(MIN_GradLen.lt.MIN_GradLenP)then
          MIN_GradLenP=MIN_GradLen
          ILower=0
        else
          ILower=ILower+1
        end if
        end do ! Icycles
!
      write(UNIout,'(a)')'Lowest gradient length found for: '
      write(UNIout,'(a,3f12.6)')'Grid point(x,y,z): ',XYZVal(1),XYZVal(2),XYZVal(3)
      write(UNIout,'(a,4f12.6)')'Gradients:         ',ddx_Min,ddy_Min,ddz_Min,MIN_GradLen
      end do ! Ipoint

      deallocate (del_W)
      deallocate (WeightA)
!
! End of routine Find_CP_Radial_density
      call PRG_manager ('exit', 'Find_CP_Radial_density', 'UTILITY')
      return
      end subroutine Find_CP_Radial_density
      subroutine RADIAL_DENSITY_BOND
!********************************************************************
!     Date last modified: July 31, 2014                           *
!     Authors: R.A. Poirier                                         *
!     Description: Calculate the density belonging to bonds         *
!********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE type_density
      USE module_grid_points
      USE type_plotting
      USE type_Weights

      implicit none
!
! Local scalars:
      integer :: Aatom,Batom,Iatom,Ipoint,ZA,ZB,Znum,lenstr
      double precision :: BweightA,BweightB,WAB
      double precision :: XVal,YVal,ZVal,XApt,YApt,ZApt,charge,RadSq,den_coef,RDenAB
      character(len=32):: Ctemp
!
! Local Array
      double precision, allocatable,dimension(:) :: RadialDen
!
! Local Function
      double precision :: GET_density_at_xyz
!
! Begin:
      call PRG_manager ('enter','RADIAL_DENSITY_BOND', 'RADIAL_DENSITY%BOND')
!
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)
!
      if(DEN_partitioning(1:5).eq.'BECKE')call BLD_BeckeW_XI
!
      PlotFileID=' '
      allocate(RadialDen(NGridPoints))
      do Aatom=1,Natoms
        ZA=CARTESIAN(Aatom)%Atomic_number
        if(ZA.le.0)cycle
      do Batom=Aatom,Natoms
        if(Aatom.eq.Batom)cycle
        ZB=CARTESIAN(Batom)%Atomic_number
        if(ZB.le.0)cycle
        write(Ctemp,'(a,i4,a,i4)')'RADIAL_DENSITY_BOND_',Aatom,'_',Batom
        call RemoveBlanks (Ctemp, PlotFileID, lenstr)
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
        write(PlotFunction,'(a,i4,a,i4)')'Radial Density for Bond between atom ',Aatom,' and ',Batom
! A-B (should add A-B + B-A)
      do Ipoint=1,NGridPoints
         XVal=grid_points(Ipoint)%x
         YVal=grid_points(Ipoint)%y
         ZVal=grid_points(Ipoint)%z
! Now get the radial density
        den_coef=ZERO
        do Iatom=1,Natoms
          Znum=CARTESIAN(Iatom)%Atomic_number
          if(Znum.le.0)cycle
          XApt=CARTESIAN(Iatom)%X
          YApt=CARTESIAN(Iatom)%Y
          ZApt=CARTESIAN(Iatom)%Z
          RadSq=(XVal-XApt)**2+(YVal-YApt)**2+(ZVal-ZApt)**2
          call GET_weight1 (XVal, YVal, ZVal, Iatom, BweightA)
          den_coef=den_coef+BweightA*RadSq
        end do !Iatom
          charge=GET_density_at_xyz (XVal, YVal, ZVal)
          RDenAB=den_coef*charge
          call GET_Weight1 (XVal, YVal, ZVal, Aatom, BweightA)
          call GET_Weight1 (XVal, YVal, ZVal, Batom, BweightB)
          WAB=dsqrt(BweightA*BweightB)
          RadialDen(Ipoint)=WAB*RDenAB
        end do ! Ipoint
       call PRT_GRID_MOL (RadialDen, NGridPoints, fmt_r2rho)
       close (unit=Plotfile_unit)
      end do ! Batom
      end do ! Aatom

      deallocate(RadialDen)
!
! End of routine RADIAL_DENSITY_BOND
      call PRG_manager ('exit', 'RADIAL_DENSITY_BOND', 'RADIAL_DENSITY%BOND')
      return
      end subroutine RADIAL_DENSITY_BOND
      double precision function GET_RADIAL_DENSITY_XYZ (XVal,YVal,ZVal)
!****************************************************************************************
!     Date last modified: August 11, 2014                                               *
!     Authors: R.A. Poirier                                                             *
!     Description:                                                                      *
!                 Calculate the charge density at a point.                              *
!****************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE type_Weights

      implicit none
!
! Local scalars:
      integer :: Ipoint,Iatom,Znum
      double precision :: WeightA,RadSq,Den_coef,rho
      double precision :: XVal,YVal,ZVal,Xa,Ya,Za
!
! Local Function
      double precision :: GET_density_at_xyz
!
! Begin:
      call PRG_manager ('enter', 'GET_RADIAL_DENSITY_XYZ', 'UTILITY')
!
      call GET_object ('QM', 'CMO', Wavefunction)

      if(DEN_partitioning(1:5).eq.'BECKE')call BLD_BeckeW_XI ! Only needed if using Becke weights

      rho=GET_density_at_xyz (XVAl, YVal, ZVal)
      den_coef=ZERO

      do Iatom=1,Natoms 
        Znum=CARTESIAN(Iatom)%Atomic_number
        if(Znum.le.0)cycle
        Xa=CARTESIAN(Iatom)%X
        Ya=CARTESIAN(Iatom)%Y
        Za=CARTESIAN(Iatom)%Z          
        RadSq=(XVal-Xa)**2+(YVal-Ya)**2+(ZVal-Za)**2          
        call GET_weight1 (XVal, YVal, ZVal, Iatom, WeightA)
        den_coef=den_coef+WeightA*RadSq
      end do !Iatom              
      GET_RADIAL_DENSITY_XYZ=den_coef*rho
!
! End of routine GET_RADIAL_DENSITY_XYZ
      call PRG_manager ('exit', 'GET_RADIAL_DENSITY_XYZ', 'UTILITY')
      return
      end function GET_RADIAL_DENSITY_XYZ
      subroutine DEL_RADIAL_DENSITY (Xval, Yval, Zval, ValueX, ValueY, ValueZ)
!********************************************************************
!     Date last modified: February 8, 2016                          *
!     Authors: R.A. Poirier                                         *
!     Description: Calculate del radial density at point.           *
!********************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE type_density
      USE type_Weights

      implicit none
!
! Local scalars:
      integer :: Iatom,Znum
      double precision :: rho,Del_rho,VAlueX,VAlueY,VAlueZ,RvalSq
      double precision :: Xa,Ya,Za,XVal,YVal,ZVal
      double precision :: DEL_rhoX,DEL_rhoY,DEL_rhoZ
      double precision, dimension(:,:), allocatable :: del_W
      double precision, dimension(:), allocatable :: WeightA
!
! Local functions:
      double precision :: TraceAB
      double precision, dimension(:), allocatable :: AOprod
      double precision, dimension(:), allocatable  :: dAOprodX,dAOprodY,dAOprodZ
!
! Begin:
      call PRG_manager ('enter','DEL_RADIAL_DENSITY', 'UTILITY')
!
      allocate (AOprod(1:MATlen))
      allocate (dAOprodx(1:MATlen))
      allocate (dAOprody(1:MATlen))
      allocate (dAOprodz(1:MATlen))
      allocate (del_W(Natoms,3))
      allocate (WeightA(Natoms))

      call AO_products (XVal, YVal, ZVal, AOprod, MATlen)
      rho= TraceAB (PM0, AOprod, NBasis)
      call dG_products (XVal, YVal, ZVal, dAOprodX, dAOprodY, dAOprodZ, MATlen)
      DEL_rhoX=TraceAB (PM0, dAOprodX, NBasis)
      DEL_rhoY=TraceAB (PM0, dAOprodY, NBasis)
      DEL_rhoZ=TraceAB (PM0, dAOprodZ, NBasis)

! Now compute the contribution to each atom:
      ValueX=ZERO
      ValueY=ZERO
      ValueZ=ZERO
      if(LdW_analytical)then
        call GET_dWeights (XVAL, YVAL, ZVAL, del_W, WeightA, Natoms) ! For all atoms
      else
        call GET_dWN_NUMERICAL (XVAL, YVAL, ZVAL, del_W, WeightA, Natoms) ! For all atoms
      end if
      do Iatom=1,Natoms 
        Znum=CARTESIAN(Iatom)%Atomic_number
        if(Znum.le.0)cycle
        Xa=CARTESIAN(Iatom)%X
        Ya=CARTESIAN(Iatom)%Y
        Za=CARTESIAN(Iatom)%Z          
        RvalSq=(XVal-Xa)**2+(YVal-Ya)**2+(ZVal-Za)**2
! Now get the gradient for the Weights
        ValueX=ValueX+WeightA(IAtom)*(TWO*(XVal-Xa)*rho+RvalSq*DEL_rhoX)+RvalSq*del_W(Iatom,1)*rho
        ValueY=ValueY+WeightA(IAtom)*(TWO*(YVal-Ya)*rho+RvalSq*DEL_rhoY)+RvalSq*del_W(Iatom,2)*rho
        ValueZ=ValueZ+WeightA(IAtom)*(TWO*(ZVal-Za)*rho+RvalSq*DEL_rhoZ)+RvalSq*del_W(Iatom,3)*rho
      end do !Iatom

      deallocate (AOprod)
      deallocate (dAOprodx)
      deallocate (dAOprody)
      deallocate (dAOprodz)
      deallocate (del_W)
      deallocate (WeightA)
!
! End of routine DEL_RADIAL_DENSITY
      call PRG_manager ('exit', 'DEL_RADIAL_DENSITY', 'UTILITY')
      return
      end subroutine DEL_RADIAL_DENSITY
      subroutine DEL2_RADIAL_DENSITY (XVal, YVal, ZVal, Hess)
!********************************************************************
!     Date last modified: August 15, 2012                           *
!     Authors: R.A. Poirier                                         *
!     Description: Calculate del charge density at point.           *
!********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE type_density
      USE module_grid_points
      USE type_Weights

      implicit none
!
! Local scalars:
      integer :: Iatom,Znum
      double precision :: rho,Del_rho,VAlueXX,VAlueYY,VAlueZZ,VAlueXY,VAlueXZ,VAlueYZ,RvalSq
      double precision :: WeightA,Xa,Ya,Za,XVal,YVal,ZVal
      double precision :: DEL_rhoX,DEL_rhoY,DEL_rhoZ
      double precision :: DEL2_rho_XX,DEL2_rho_YY,DEL2_rho_ZZ,DEL2_rho_XY,DEL2_rho_XZ,DEL2_rho_YZ
      double precision :: NEGdel2rho,BWeight
      double precision, dimension(3,3) :: Hess,EigVector
      double precision, dimension(3) :: EigValue
      double precision :: XYZ0(3),DEL_W(3),DEL2_W(3,3)
!
! Local functions:
      double precision :: TraceAB
      double precision, dimension(:), allocatable :: AOprod
      double precision, dimension(:), allocatable  :: dAOprodX,dAOprodY,dAOprodZ
!
! Begin:
      call PRG_manager ('enter','DEL2_RADIAL_DENSITY', 'UTILITY')
!
      allocate (AOprod(1:MATlen))
      allocate (dAOprodx(1:MATlen))
      allocate (dAOprody(1:MATlen))
      allocate (dAOprodz(1:MATlen))
!     allocate (d2AOprodxx(1:MATlen))
!     allocate (d2AOprodyy(1:MATlen))
!     allocate (d2AOprodzz(1:MATlen))
!     allocate (d2AOprodxy(1:MATlen))
!     allocate (d2AOprodxz(1:MATlen))
!     allocate (d2AOprodyz(1:MATlen))   

      call AO_products (XVal, YVal, ZVal, AOprod, MATlen)
      rho= TraceAB (PM0, AOprod, NBasis)
      call dG_products (XVal, YVal, ZVal, dAOprodX, dAOprodY, dAOprodZ, MATlen)
      DEL_rhoX=TraceAB (PM0, dAOprodX, NBasis)
      DEL_rhoY=TraceAB (PM0, dAOprodY, NBasis)
      DEL_rhoZ=TraceAB (PM0, dAOprodZ, NBasis)
      call GET_d2rho_Atom (XVal, YVal, ZVal, Hess)
      DEL2_rho_XX=Hess(1,1)
      DEL2_rho_YY=Hess(2,2)
      DEL2_rho_ZZ=Hess(3,3)
      DEL2_rho_XY=Hess(1,2)
      DEL2_rho_XZ=Hess(1,3)
      DEL2_rho_YZ=Hess(2,3)
!     call d2AO_products (XVal, YVal, ZVal)
!     DEL2_rho_XX=TraceAB (PM0, d2AOprodxx, NBasis)
!     DEL2_rho_YY=TraceAB (PM0, d2AOprodyy, NBasis)
!     DEL2_rho_ZZ=TraceAB (PM0, d2AOprodzz, NBasis)
!     DEL2_rho_XY=TraceAB (PM0, d2AOprodxy, NBasis)
!     DEL2_rho_XZ=TraceAB (PM0, d2AOprodxz, NBasis)
!     DEL2_rho_YZ=TraceAB (PM0, d2AOprodyz, NBasis)
      XYZ0(1)=XVal
      XYZ0(2)=YVal
      XYZ0(3)=ZVal

! Now compute the contribution to each atom:
      ValueXX=ZERO
      ValueYY=ZERO
      ValueZZ=ZERO
      ValueXY=ZERO
      ValueXZ=ZERO
      ValueYZ=ZERO
      do Iatom=1,Natoms 
        Znum=CARTESIAN(Iatom)%Atomic_number
        if(Znum.le.0)cycle
        Xa=CARTESIAN(Iatom)%X
        Ya=CARTESIAN(Iatom)%Y
        Za=CARTESIAN(Iatom)%Z          
        RvalSq=(XVal-Xa)**2+(YVal-Ya)**2+(ZVal-Za)**2
        if(LdW_analytical.and.Ld2W_analytical)then
          call GET_d2Weights (XVAL, YVAL, ZVAL, Iatom, WeightA, DEL_W, DEL2_W)
        else
          call GET_weight1 (XVal, YVal, ZVal, Iatom, WeightA)
          call GET_dW_NUMERICAL (XVal, YVal, ZVal, Iatom, DEL_W)
          call GET_d2W_NUMERICAL (XVal, YVal, ZVal, Iatom, DEL2_W)
        end if
         ValueXX=ValueXX+(TWO*WeightA+FOUR*(XVal-Xa)*DEL_W(1)+RvalSq*DEL2_W(1,1))*rho &
                        +(FOUR*(XVal-Xa)*WeightA+TWO*RvalSq*DEL_W(1))*DEL_rhoX+RvalSq*WeightA*DEL2_rho_XX
         ValueYY=ValueYY+(TWO*WeightA+FOUR*(YVal-Ya)*DEL_W(2)+RvalSq*DEL2_W(2,2))*rho &
                        +(FOUR*(YVal-Ya)*WeightA+TWO*RvalSq*DEL_W(2))*DEL_rhoY+RvalSq*WeightA*DEL2_rho_YY
         ValueZZ=ValueZZ+(TWO*WeightA+FOUR*(ZVal-Za)*DEL_W(3)+RvalSq*DEL2_W(3,3))*rho &
                        +(FOUR*(ZVal-Za)*WeightA+TWO*RvalSq*DEL_W(3))*DEL_rhoZ+RvalSq*WeightA*DEL2_rho_ZZ
         ValueXY=ValueXY+(TWO*(XVal-Xa)*DEL_W(2)+TWO*(YVal-Ya)*DEL_W(1)+RvalSq*DEL2_W(1,2))*rho &
                        +(TWO*(XVal-Xa)*WeightA+RvalSq*DEL_W(1))*DEL_rhoY &
                        +(TWO*(YVal-Ya)*WeightA+RvalSq*DEL_W(2))*DEL_rhoX+RvalSq*WeightA*DEL2_rho_XY
         ValueXZ=ValueXZ+(TWO*(XVal-Xa)*DEL_W(3)+TWO*(ZVal-Za)*DEL_W(1)+RvalSq*DEL2_W(1,3))*rho &
                        +(TWO*(XVal-Xa)*WeightA+RvalSq*DEL_W(1))*DEL_rhoZ &
                        +(TWO*(ZVal-Za)*WeightA+RvalSq*DEL_W(3))*DEL_rhoX+RvalSq*WeightA*DEL2_rho_XZ
         ValueYZ=ValueYZ+(TWO*(YVal-Ya)*DEL_W(3)+TWO*(ZVal-Za)*DEL_W(2)+RvalSq*DEL2_W(2,3))*rho &
                        +(TWO*(YVal-Ya)*WeightA+RvalSq*DEL_W(2))*DEL_rhoZ &
                        +(TWO*(ZVal-Za)*WeightA+RvalSq*DEL_W(3))*DEL_rhoY+RvalSq*WeightA*DEL2_rho_YZ
       end do !Iatom

        Hess(1,1)=ValueXX
        Hess(1,2)=ValueXY
        Hess(1,3)=ValueXZ
        Hess(2,1)=Hess(1,2)
        Hess(2,2)=ValueYY
        Hess(2,3)=ValueYZ
        Hess(3,1)=Hess(1,3)
        Hess(3,2)=Hess(2,3)
        Hess(3,3)=ValueZZ

!       call MATRIX_diagonalize (Hess, EigVector, EigValue, 3, 2, .true.)

! Format for now - needs to be change (Pfmt_Eig)
!       write(UNIout,'(4f20.6)')EigValue(1),EigValue(2),EigValue(3),NEGdel2rho

      deallocate (AOprod)
      deallocate (dAOprodx)
      deallocate (dAOprody)
      deallocate (dAOprodz)
!     deallocate (d2AOprodxx)
!     deallocate (d2AOprodyy)
!     deallocate (d2AOprodzz)
!     deallocate (d2AOprodxy)
!     deallocate (d2AOprodxz)
!     deallocate (d2AOprodyz)   
!
! End of routine DEL2_RADIAL_DENSITY
      call PRG_manager ('exit', 'DEL2_RADIAL_DENSITY', 'UTILITY')
      return
      end subroutine DEL2_RADIAL_DENSITY
      subroutine RADIAL_DENSITY_ATOMIC
!*******************************************************************************************************
!     Date last modified: August 15, 2012                                                              *
!     Authors: R.A. Poirier                                                                            *
!     Description:                                                                                     *
!         Calculate the radial density on radial grids        (e.g., SG1, G20, ...)                    *
!*******************************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE module_grid_points
      USE type_plotting

      implicit none
!
! Local scalars:
      integer :: Ipoint,IGridpt,Iatom,Jatom,Ifound,Znum,File_MOL,NGridpts
      double precision :: Xpt,Ypt,Zpt
      double precision :: WeightA,XApt,YApt,ZApt,RadSq,Rden,rho,RiBsq
      double precision :: r_val,theta_val,phi_val
      double precision, dimension(:), allocatable :: RdenMOL
!
! Local Function
      double precision :: GET_density_at_xyz

      double precision :: cutoff
      parameter (cutoff=1.0E-6)
!
! Begin
      call PRG_manager ('enter', 'RADIAL_DENSITY_ATOMIC', 'RADIAL_DENSITY%ATOMIC')

      call GET_object ('QM', 'CMO', Wavefunction)  ! Get the density matrix (PM0) 
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)  ! Get the grid points for all the atoms (Egridpts)
!
! Count the total number of grid points for the molecule
      NGridpts=0
      do Iatom=1,Natoms
        Znum=CARTESIAN(Iatom)%Atomic_number
        if(Znum.le.0)cycle ! Skip dummy atoms
        Ifound=GRID_loc(Znum)
        NApts_atom=NApoints_atom(Ifound)
        NGridpts=NGridpts+NApts_atom
      end do ! Iatom
      allocate (RdenMOL(NGridpts))
      RdenMOL(1:NGridpts)=ZERO
!
! Create a file name and get a unit if required
      if(MoleculePrint)then
        PlotFileID=' '
        write(PlotFileID,'(a)')'RADIAL_DENSITY_RADIAL'
        call BLD_plot_file_MOL (PlotFileID, NGridpts, File_MOL)
      else
        File_MOL=UNIout
      end if

      IGridpt=0
      do Iatom=1,Natoms
        Znum=CARTESIAN(Iatom)%Atomic_number
        if(Znum.le.0)cycle ! Skip dummy atoms
        Ifound=GRID_loc(Znum)
        NApts_atom=NApoints_atom(Ifound)
        if(AtomPrint)then
          PlotFileID=' '
          PlotFunction=' '
          write(PlotFunction,'(a,i4)')'Radial Density Atomic for atom ',Iatom
          write(PlotFileID,'(a)')'RADIAL_DENSITY_ATOMIC'
          call BLD_plot_file_ATOMIC (PlotFileID, NApts_atom, Iatom, PlotFile_unit)
        else
          PlotFile_unit=UNIout
        end if
        write(PlotFile_unit,'(a)')'X,Y,Z,r^2rho(x,y,z):'
        write(PlotFile_unit,'(a)')trim(PlotFunction)
!           
! Loop over grid points of Iatom
        do Ipoint=1,NApts_atom
          Xpt=Egridpts(Ifound,Ipoint)%X
          Ypt=Egridpts(Ifound,Ipoint)%Y
          Zpt=Egridpts(Ifound,Ipoint)%Z
          RadSq=(Xpt)**2+(Ypt)**2+(Zpt)**2          
          r_val=dsqrt(RadSq)
          theta_val=dacos(Zpt/r_val)
          phi_val=Xpt/(r_val*dsin(theta_val))
! Moving grid points on cartesian coordinate grid
          XApt=Xpt+CARTESIAN(Iatom)%X
          YApt=Ypt+CARTESIAN(Iatom)%Y
          ZApt=Zpt+CARTESIAN(Iatom)%Z
          rho=GET_density_at_xyz (XApt, YApt, ZApt) ! For atom a
          call GET_weight1 (XApt, YApt, ZApt, Iatom, WeightA)
          Rden=WeightA*RadSq*rho
!
! Now compute the molecular radial density
          IGridpt=IGridpt+1
          do Jatom=1,Natoms
            Znum=CARTESIAN(Jatom)%Atomic_number
            if(Znum.le.0)cycle ! Skip dummy atoms
            RiBsq=(XApt-CARTESIAN(Jatom)%X)**2+(YApt-CARTESIAN(Jatom)%Y)**2+(ZApt-CARTESIAN(Jatom)%Z)**2          
            call GET_weight1 (XApt, YApt, ZApt, Jatom, WeightA)
            RdenMOL(IGridpt)=RdenMOL(IGridpt)+WeightA*RiBsq*rho
          end do ! Jatom
!         if(Rden.le.cutoff)cycle
!         write(PlotFile_unit,Pfmt_r2rho)XApt,YApt,ZApt,Rden
          write(PlotFile_unit,Pfmt_r2rho)r_val,theta_val,phi_val,Rden
        end do ! Ipoint
!
        if(AtomPrint)close (unit=PlotFile_unit)
      end do ! Iatom

      write(File_MOL,'(a)')'X,Y,Z,r^2rho(x,y,z):'
      write(File_MOL,'(a)')'Molecular Radial Density:'
      IGridpt=0
      do Iatom=1,Natoms
        Znum=CARTESIAN(Iatom)%Atomic_number
        if(Znum.le.0)cycle ! Skip dummy atoms
        Ifound=GRID_loc(Znum)
        NApts_atom=NApoints_atom(Ifound)
        do Ipoint=1,NApts_atom
          IGridpt=IGridpt+1
          XApt=Egridpts(Ifound,Ipoint)%X+CARTESIAN(Iatom)%X
          YApt=Egridpts(Ifound,Ipoint)%Y+CARTESIAN(Iatom)%Y
          ZApt=Egridpts(Ifound,Ipoint)%Z+CARTESIAN(Iatom)%Z
          if(RdenMOL(IGridpt).le.cutoff)cycle
        write(File_MOL,Pfmt_r2rho)XApt,YApt,ZApt,RdenMOL(IGridpt)
      end do ! IGridpt
      end do ! Iatom
      if(MoleculePrint)close (unit=File_MOL)

      deallocate(RdenMOL)
!
! End of routine RADIAL_DENSITY_ATOMIC
      call PRG_manager ('exit', 'RADIAL_DENSITY_ATOMIC', 'RADIAL_DENSITY%ATOMIC')
      return
      end subroutine RADIAL_DENSITY_ATOMIC
      subroutine RHO_RAD
!********************************************************************
!     Date last modified: August 15, 2012                           *
!     Authors: R.A. Poirier                                         *
!     Description: Calculate del charge density at point.           *
!********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE QM_defaults
      USE QM_objects
      USE OPT_objects
      USE type_density
      USE module_grid_points

      implicit none
!
! Local scalars:
      integer :: Ipoint
!
! Begin:
      call PRG_manager ('enter','RHO_RAD', 'UTILITY')
!
      call GET_object ('QM', 'RADIAL_DENSITY', 'MESH')
      Ipoint=1
      OPT_function%value=Rden(Ipoint)
!
! End of routine RHO_RAD
      call PRG_manager ('exit', 'RHO_RAD', 'UTILITY')
      return
      end subroutine RHO_RAD
      subroutine RADIAL_DENSITY_MO_MESH
!****************************************************************************************
!     Date last modified: Sept 26, 2013                                                 *
!     Authors: Devin Nippard                                                            *
!     Description:                                                                      *
!                 Calculate the radial MO density on a set of grid points (mesh)        *
!****************************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE module_grid_points
      USE type_density
      USE type_molecule
      USE type_elements
      USE type_plotting
      USE type_Weights

      implicit none

      integer :: Ipoint,Iatom,IMO,IMOprint,lenstr,Znum
      double precision :: Xg,Yg,Zg,Xa,Ya,Za
      double precision :: WeightA,radSq,MO_den
      double precision, allocatable,dimension(:) :: MOradMO
      character(len=32):: Ctemp
!
      call PRG_manager ('enter','RADIAL_DENSITY_MO_MESH', 'RADIAL_DENSITY_MO%MESH')

      call GET_object ('QM', 'CMO', Wavefunction)
!
      if(DEN_partitioning(1:5).eq.'BECKE')call BLD_BeckeW_XI

      allocate (MOradMO(NGridPoints))

      do IMOprint=1,NMOprint
      IMO=MOprint(IMOprint) 
      MOradMO=ZERO
      do Iatom=1,Natoms
        Znum=CARTESIAN(Iatom)%Atomic_number
        if(Znum.le.0)cycle
        Xa=CARTESIAN(Iatom)%X
        Ya=CARTESIAN(Iatom)%Y
        Za=CARTESIAN(Iatom)%Z
        do Ipoint=1,NGridPoints
          Xg=grid_points(Ipoint)%x
          Yg=grid_points(Ipoint)%y
          Zg=grid_points(Ipoint)%z
          radSq = (Xg-Xa)**2+(Yg-Ya)**2+(Zg-Za)**2
          call GET_weight1 (Xg, Yg, Zg, Iatom, WeightA)
          call GET_MO_density_point (Xg, Yg, Zg, CMO%coeff, MO_den, Nbasis, IMO)
          MOradMO(Ipoint)=MOradMO(Ipoint)+WeightA*radSq*MO_den
        end do !Ipoint
! Now print the radial densities for Iatom and IMO
      end do ! Iatom
      if(MoleculePrint.or.AtomPrint)then
       PlotFileID=' '
       write(PlotFunction,'(a,i4)')'Radial Density for MO ',IMO
       write(Ctemp,'(a,i4)')'RADIAL_DENSITY_MESH_MO',IMO
        call RemoveBlanks (Ctemp, PlotFileID, lenstr)
       call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
      else
        Plotfile_unit=UNIout
      end if
       call PRT_GRID_MOL (MOradMO, NGridPoints, fmt_r2rho)
      end do ! IMO

      call PRG_manager ('exit','RADIAL_DENSITY_MO_MESH', 'RADIAL_DENSITY_MO%MESH')

      end subroutine RADIAL_DENSITY_MO_MESH
      subroutine RADIAL_DENSITY_MO_BY_ATOM_MESH
!****************************************************************************************
!     Date last modified: Sept 26, 2013                                                 *
!     Authors: Devin Nippard                                                            *
!     Description:                                                                      *
!                 Calculate the radial MO density on a set of grid points (mesh)        *
!****************************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE module_grid_points
      USE type_density
      USE type_molecule
      USE type_elements
      USE type_plotting
      USE type_Weights

      implicit none

      integer :: IAIMprint,Ipoint,Iatom,IMO,IMOprint,lenstr,Znum
      double precision :: Xg,Yg,Zg,Xa,Ya,Za
      double precision :: WeightA,radSq,MO_den
      double precision, allocatable,dimension(:) :: MOradA
      character(len=64):: Ctemp
!
      call PRG_manager ('enter','RADIAL_DENSITY_MO_BY_ATOM_MESH', 'RADIAL_DENSITY_MO_BY_ATOM%MESH')

      call GET_object ('QM', 'CMO', Wavefunction)
!
      if(DEN_partitioning(1:5).eq.'BECKE')call BLD_BeckeW_XI

      allocate (MOradA(NGridPoints))

      do IMOprint=1,NMOprint
      IMO=MOprint(IMOprint) 
      do IAIMprint=1,NAIMprint
        Iatom=AIMprint(IAIMprint)
        Znum=CARTESIAN(Iatom)%Atomic_number
        if(Znum.le.0)cycle
      MOradA=ZERO
        Xa=CARTESIAN(Iatom)%X
        Ya=CARTESIAN(Iatom)%Y
        Za=CARTESIAN(Iatom)%Z
        do Ipoint=1,NGridPoints
          Xg=grid_points(Ipoint)%x
          Yg=grid_points(Ipoint)%y
          Zg=grid_points(Ipoint)%z
          radSq = (Xg-Xa)**2+(Yg-Ya)**2+(Zg-Za)**2
          call GET_weight1 (Xg, Yg, Zg, Iatom, WeightA)
          call GET_MO_density_point (Xg, Yg, Zg, CMO%coeff, MO_den, Nbasis, IMO)
          MOradA(Ipoint)=WeightA*radSq*MO_den
        end do !Ipoint
! Now print the radial densities for Iatom and IMO
      if(MoleculePrint.or.AtomPrint)then
        PlotFileID=' '
        write(Ctemp,'(a,i4,a,i8,2a)')'RADIAL_DENSITY_MESH_MO',IMO,'_atom_',Iatom,'_',element_symbols(Znum)
        call RemoveBlanks (Ctemp, PlotFileID, lenstr)
        call BLD_plot_file_MOL (PlotFileID, NGridPoints, Plotfile_unit)
      else
        Plotfile_unit=UNIout
      end if
      write(Plotfile_unit,'(a,i8,a,i8)')'MO: ',IMO,' for atom: ',Iatom
      PlotFunction(1:)='r^2 MO(x,y,z)'
      call PRT_GRID_MOL (MOradA, NGridPoints, fmt_r2rho)
      if(MoleculePrint.or.AtomPrint)close (unit=Plotfile_unit)
      end do ! IAIMprint
      end do ! IMO

      call PRG_manager ('exit','RADIAL_DENSITY_MO_BY_ATOM_MESH', 'RADIAL_DENSITY_MO_BY_ATOM%MESH')

      end subroutine RADIAL_DENSITY_MO_BY_ATOM_MESH
