      subroutine ENERGY_Kab_MO_NI
!*******************************************************************************
!     Date last modified: December 31, 2015                                    *
!     Authors:                                                                 *
!     Description:                                                             * 
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE module_grid_points

      implicit none
!
! Local scalars:
      integer :: Iatom,IApoint,IAIM,Znum,Ifound,aMO,bMO
      double precision :: Xpt,Ypt,Zpt
!
! Local array:
      double precision, dimension(:), allocatable :: WeightsA
      double precision :: KabGrid, KabAtom, KEnergy, KHFEnergy
      double precision, allocatable,dimension(:,:) :: Kab_E
      double precision, allocatable,dimension(:,:,:) :: Kab_Atomic
      double precision, dimension(:,:), allocatable :: occ_Kab, Occ_KHFab
!
! Begin:
      call PRG_manager ('enter', 'ENERGY_KAB_MO_NI', 'UTILITY')
!
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)
!
      allocate (Kab_E(1:CMO%NoccMO,1:CMO%NoccMO))
      allocate (Kab_Atomic(1:CMO%NoccMO,1:CMO%NoccMO,Natoms))  
      allocate (occ_Kab(1:CMO%NoccMO,1:CMO%NoccMO))
      allocate (occ_KHFab(1:CMO%NoccMO,1:CMO%NoccMO))   
      !      

      Kab_E=ZERO
      Kab_Atomic=ZERO
            
      call occupancy_Kab (occ_Kab, Occ_KHFab, CMO%NoccMO) 
      
!      write(UNIout,'(/5x,a)')'Atom     aMO      bMO     Kab'
      
      if(NAIMprint.eq.0)then ! If no atoms specified, do them all!
        NAIMprint=Natoms
        do Iatom=1,Natoms
          AIMprint(Iatom)=Iatom
        end do ! Iatom
      end if

      do IAIM=1,NAIMprint
      Iatom=AIMprint(IAIM)
      Znum=CARTESIAN(Iatom)%Atomic_number
      if(Znum.le.0)cycle
      Ifound=GRID_loc(Znum)
      NApts_atom=NApoints_atom(Ifound)

      allocate(grid_points(1:NApts_atom))
      allocate(WeightsA(NApts_atom))
!
! Moving grid points on cartesian coordinate grid
      do IApoint=1,NApts_atom
        grid_points(IApoint)%X=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
        grid_points(IApoint)%Y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
        grid_points(IApoint)%Z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
      end do ! IApoint

      call GET_weights (grid_points, NApts_atom, Iatom, WeightsA)
      
      do aMO=1,CMO%NoccMO !Nbasis 
      do bMO=1,aMO !Nbasis
      
      KabAtom=Zero
      do IApoint=1,NApts_atom
        if(WeightsA(IApoint).le.1.0D-6)cycle ! Makes no difference
        Xpt=grid_points(IApoint)%X
        Ypt=grid_points(IApoint)%Y
        Zpt=grid_points(IApoint)%Z
        call GET_Kab_MO_point (Xpt,Ypt,Zpt,aMO,bMO,KabGrid)
        KabAtom=KabAtom+KabGrid*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
      end do ! IApoint

      Kab_Atomic(aMO,bMO,Iatom)=Kab_Atomic(aMO,bMO,Iatom)+KabAtom

! Now multiply by 4pi and get total Kab for each atom
      Kab_Atomic(aMO,bMO,Iatom)=Kab_Atomic(aMO,bMO,Iatom)*FourPi
      Kab_Atomic(bMO,aMO,Iatom)=Kab_Atomic(aMO,bMO,Iatom)
      Kab_E(aMO,bMO)=Kab_E(aMO,bMO)+Kab_Atomic(aMO,bMO,Iatom)
      Kab_E(bMO,aMO)=Kab_E(aMO,bMO)
      
!      write(UNIout,'(3i8,1x,f12.6)')Iatom, aMO, bMO, Kab_Atomic(aMO,bMO,Iatom)
      end do ! bMO
      end do ! aMO
      
      deallocate(grid_points)
      deallocate(WeightsA)

      end do ! Iatom
      
      KEnergy=ZERO
      KHFEnergy=ZERO
      write(UNIout,'(/2(a6),5(5x,a7))')'aMO','bMO','Kab','K_occ','KHF_occ','K','KHF'
      do aMO=1,CMO%NoccMO !Nbasis 
      do bMO=aMO,CMO%NoccMO !Nbasis
       KEnergy=KEnergy+Kab_E(aMO,bMO)*occ_Kab(aMO,bMO)
       KHFEnergy=KHFEnergy+Kab_E(aMO,bMO)*occ_KHFab(aMO,bMO)
       write(UNIout,'(2i6,5f12.6)') aMO,bMO,Kab_E(aMO,bMO),occ_Kab(aMO,bMO),&
                                 & occ_KHFab(aMO,bMO), &
                                 & Kab_E(aMO,bMO)*occ_Kab(aMO,bMO), &
                                 & Kab_E(aMO,bMO)*occ_KHFab(aMO,bMO) 
      end do ! bMO
      end do ! aMO 
      
      write(UNIout,'(a,42x,2f12.6)') 'Total:', KEnergy, KHFEnergy
! End of routine ENERGY_K_NI
      deallocate(Kab_Atomic,Kab_E)
      deallocate (occ_Kab)
      deallocate (occ_KHFab)

      call PRG_manager ('exit', 'ENERGY_KAB_MO_NI', 'UTILITY')
      return
      end subroutine ENERGY_Kab_MO_NI      
