      subroutine ENERGY_J_NI
!****************************************************************************************
!     Date last modified: December 31, 2015                                             *
!     Authors: Ahmad I. Alrawashdeh                                                     *
!     Description: Calculate the velcocity numerically                                  *
!****************************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE module_grid_points

      implicit none
!
! Local scalars:
      integer :: IAIM,Iatom,IApoint,IRegion,Znum,Ifound,NRpoints
!     double precision :: J_total
!
! Local array:
      double precision, dimension(:), allocatable :: WeightsA
      double precision, dimension(:), allocatable :: charge
!
! Begin:
      call PRG_manager ('enter', 'ENERGY_J_NI', 'UTILITY')
!
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)
      call BLD_BeckeW_XI
!
      if(.not.allocated(Atomic_Coulomb))then
        allocate (Atomic_Coulomb(Natoms))
      else
        deallocate (Atomic_Coulomb)
        allocate (Atomic_Coulomb(Natoms))
      end if

      J_total=ZERO
      Atomic_Coulomb=ZERO
      if(NAIMprint.eq.0)then ! If no atoms specified, do them all!
        NAIMprint=Natoms
        do Iatom=1,Natoms
          AIMprint(Iatom)=Iatom
        end do ! Iatom
      end if

      write(UNIout,'(a)')'Atom           Coulomb'
      do IAIM=1,NAIMprint
      Iatom=AIMprint(IAIM)
!     do Iatom=1,Natoms
      Znum=CARTESIAN(Iatom)%Atomic_number
      if(Znum.le.0)cycle
      Ifound=GRID_loc(Znum)
      NApts_atom=NApoints_atom(Ifound)

      allocate(grid_points(1:NApts_atom))
      allocate(charge(NApts_atom))
      allocate(WeightsA(NApts_atom))
!
! Moving grid points on cartesian coordinate grid
        do IApoint=1,NApts_atom
          grid_points(IApoint)%X=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          grid_points(IApoint)%Y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          grid_points(IApoint)%Z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
          grid_points(IApoint)%w=Egridpts(Ifound,IApoint)%w
        end do ! IApoint

      call GET_weights (grid_points, NApts_atom, Iatom, WeightsA)
      call GET_density (grid_points, NApts_atom, charge)
      call J_GRID

      Atomic_Coulomb(Iatom)=PT5*Atomic_Coulomb(Iatom)
      J_total = J_total + Atomic_Coulomb(Iatom)
      write(UNIout,'(i8,1x,f14.6)')Iatom,Atomic_Coulomb(Iatom)
      deallocate(grid_points)
      deallocate(charge)
      deallocate(WeightsA)
      end do ! Iatom
!
      write(UNIout,'(a,2x,f14.6)')'J_total',J_total

! End of routine ENERGY_J_NI
      call PRG_manager ('exit', 'ENERGY_J_NI', 'UTILITY')
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine J_GRID
!****************************************************************************************
!     Date last modified: December 31, 2015                                             *
!     Authors: Ahmad I. Alrawashdeh                                                     *
!     Description: Calculate the velcoity at a point (Xpt, Ypt, Zpt)                    *
!****************************************************************************************
! Modules:

      implicit none

!
! Local scalars:
      integer :: IApoint
!
! Local functions:
!     double precision :: TraceAB
!
! Local arrays:
      double precision, dimension(:), allocatable :: Vpotl
      double precision, dimension(:), allocatable :: Pcharge

! Begin: 
      allocate (Pcharge(NApts_atom))
      allocate(Vpotl(NApts_atom))

      Pcharge(1:NApts_atom) = ZERO

      call I1E_potl (Vpotl, PM0, MATlen, NApts_atom)

      do IApoint=1,NApts_atom
        Pcharge(IApoint) = Vpotl(IApoint)*charge(IApoint)*WeightsA(IApoint)*grid_points(IApoint)%w
      end do

      call NI_Atomic_Property (Iatom, Ifound, Atomic_Coulomb, Natoms, Pcharge)

      deallocate(Pcharge)
      deallocate(Vpotl)
!
! End of routine J_GRID
      return
      end subroutine J_GRID

! END CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine ENERGY_J_NI
