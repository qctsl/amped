      subroutine ENERGY_CORR_NI
!****************************************************************************************
!     Date last modified: December 31, 2015                                             *
!     Authors:                                                                          *
!     Description:                                                                      * 
!****************************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE module_grid_points

      implicit none
!
! Local scalars:
      integer :: Iatom,IApoint,IAIM,Znum,Ifound
      double precision :: Tx,Ty,Tz,Jatom,CorrAtom,JHFatom,CorrHFatom
      double precision :: Xpt,Ypt,Zpt
      double precision :: Tx_total,Ty_total,Tz_total,TT_total
!      double precision :: J_total,JHF_total
      double precision :: Corr_total,CorrHF_total
      double precision :: TxgridA,TygridA,TzgridA,TTgridA
      double precision :: CorrgridA,CorrHFgridA,JgridA,JHFgridA
!
! Local array:
      double precision, dimension(:), allocatable :: WeightsA
      double precision, dimension(:), allocatable :: Tx_Atomic
      double precision, dimension(:), allocatable :: Ty_Atomic
      double precision, dimension(:), allocatable :: Tz_Atomic
      double precision, dimension(:), allocatable :: TT_Atomic
!      double precision, dimension(:), allocatable :: J_Atomic
!      double precision, dimension(:), allocatable :: JHF_Atomic
      double precision, dimension(:), allocatable :: Corr_Atomic
      double precision, dimension(:), allocatable :: CorrHF_Atomic

!
! Begin:
      call PRG_manager ('enter', 'ENERGY_CORR_NI', 'UTILITY')
!
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)
      call BLD_BeckeW_XI

!
      allocate (Tx_Atomic(Natoms))
      allocate (Ty_Atomic(Natoms))
      allocate (Tz_Atomic(Natoms))
      allocate (TT_Atomic(Natoms))
      allocate (J_Atomic(Natoms))
      allocate (JHF_Atomic(Natoms))
      allocate (Corr_Atomic(Natoms))
      allocate (CorrHF_Atomic(Natoms))

      Tx_total=ZERO
      Ty_total=ZERO
      Tz_total=ZERO
      TT_total=ZERO
      J_total=ZERO
      JHF_total=ZERO
      Corr_total=ZERO
      CorrHF_total=ZERO

      Tx_Atomic=ZERO
      Ty_Atomic=ZERO
      Tz_Atomic=ZERO
      TT_Atomic=ZERO
      J_Atomic=ZERO
      JHF_Atomic=ZERO
      Corr_Atomic=ZERO
      CorrHF_Atomic=ZERO

      write(UNIout,'(/a)')'Atom           J             JHF           &
                &    T           Corr           CorrHF'

      if(NAIMprint.eq.0)then ! If no atoms specified, do them all!
        NAIMprint=Natoms
        do Iatom=1,Natoms
          AIMprint(Iatom)=Iatom
        end do ! Iatom
      end if

      do IAIM=1,NAIMprint
      Iatom=AIMprint(IAIM)
!     do Iatom=1,Natoms
      Znum=CARTESIAN(Iatom)%Atomic_number
      if(Znum.le.0)cycle
      Ifound=GRID_loc(Znum)
      NApts_atom=NApoints_atom(Ifound)

      allocate(grid_points(1:NApts_atom))
      allocate(WeightsA(NApts_atom))
!
! Moving grid points on cartesian coordinate grid
        do IApoint=1,NApts_atom
          grid_points(IApoint)%X=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          grid_points(IApoint)%Y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          grid_points(IApoint)%Z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
        end do ! IApoint

      call GET_weights (grid_points, NApts_atom, Iatom, WeightsA)

      Tx=ZERO
      Ty=ZERO
      Tz=ZERO
      Jatom=ZERO
      JHFatom=ZERO
      Corratom=ZERO
      CorrHFatom=ZERO

      do IApoint=1,NApts_atom
!        if(WeightsA(IApoint).le.1.0D-6)cycle ! Makes no difference
        Xpt=grid_points(IApoint)%X
        Ypt=grid_points(IApoint)%Y
        Zpt=grid_points(IApoint)%Z
        call GET_COULOMB_MO_point (Xpt, Ypt, Zpt, JgridA, JHFgridA)
        call GET_Kinetic_GRID (Xpt, Ypt, Zpt, TxgridA, TygridA, TzgridA)
        Jatom=Jatom+JgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
        JHFatom=JHFatom+JHFgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
        Tx=Tx+PT5*TxgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
        Ty=Ty+PT5*TygridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
        Tz=Tz+PT5*TzgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
        TTgridA=PT5*(TxgridA+TygridA+TzgridA)
        if(TTgridA.gt.1.0e-6) then
!        write(*,*) "TTTTT",TTgridA
        CorrgridA=JgridA/dsqrt(TTgridA)
        CorrHFgridA=JHFgridA/dsqrt(TTgridA)
        Corratom=Corratom+CorrgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
        CorrHFatom=CorrHFatom+CorrHFgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
        EndIf
      end do ! IApoint

      Tx_Atomic(Iatom)=Tx
      Ty_Atomic(Iatom)=Ty
      Tz_Atomic(Iatom)=Tz
      TT_Atomic(Iatom)=Tx+Ty+Tz
      J_Atomic(Iatom)=Jatom
      JHF_Atomic(Iatom)=JHFatom
      Corr_Atomic(Iatom)=Corratom
      CorrHF_Atomic(Iatom)=CorrHFatom

      deallocate(grid_points)
      deallocate(WeightsA)

      end do ! IAIM

!
! Now multiply by 4pi and get total exchange
      do IAIM=1,NAIMprint
        Iatom=AIMprint(IAIM)
!     do Iatom=1,Natoms
        Znum=CARTESIAN(Iatom)%Atomic_number
        if(Znum.le.0)cycle
        Tx_Atomic(Iatom)=FourPi*Tx_Atomic(Iatom)
        Ty_Atomic(Iatom)=FourPi*Ty_Atomic(Iatom)
        Tz_Atomic(Iatom)=FourPi*Tz_Atomic(Iatom)
        TT_Atomic(Iatom)=FourPi*TT_Atomic(Iatom)
        J_Atomic(Iatom)=FourPi*J_Atomic(Iatom)
        JHF_Atomic(Iatom)=FourPi*JHF_Atomic(Iatom)
        Corr_Atomic(Iatom)=FourPi*Corr_Atomic(Iatom)
        CorrHF_Atomic(Iatom)=FourPi*CorrHF_Atomic(Iatom)
        write(UNIout,'(i8,1x,6f14.6)')Iatom,J_Atomic(Iatom),JHF_Atomic(Iatom),TT_Atomic(Iatom), &
                                            Corr_Atomic(Iatom),CorrHF_Atomic(Iatom)
        Tx_total = Tx_total + Tx_Atomic(Iatom)
        Ty_total = Ty_total + Ty_Atomic(Iatom)
        Tz_total = Tz_total + Tz_Atomic(Iatom)
        TT_total = TT_total + TT_Atomic(Iatom)
        J_total = J_total + J_Atomic(Iatom)
        JHF_total = JHF_total + JHF_Atomic(Iatom)
        Corr_total = Corr_total + Corr_Atomic(Iatom)
        CorrHF_total = CorrHF_total + CorrHF_Atomic(Iatom)
      end do ! IAIM
!
      if(NAIMprint.eq.Natoms)then
        write(UNIout,'(a,4x,6f14.6/)')'Total',J_total,JHF_total,TT_total,Corr_total,CorrHF_total
      end if

! End of routine ENERGY_VEE_MO_NI
      call PRG_manager ('exit', 'ENERGY_CORR_NI', 'UTILITY')
      return
      end subroutine ENERGY_CORR_NI
