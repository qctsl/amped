      subroutine ENERGY_Vee_NN
!****************************************************************************************
!     Date last modified: December 31, 2015                                             *
!     Authors: Ahmad I. Alrawashdeh                                                     *
!     Description: Calculate the velcocity numerically                                  *
!****************************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE module_grid_points

      implicit none
!
! Local scalars:
      integer :: Iatom,Ifound
      integer :: IApoint,JApoint
      integer :: Jatom,Jfound,Znum,JZ_num,JNApts_atom
      double precision :: WeightA,WeightB,rij,temp
      double precision :: coul_energy_NN_tot
      double precision, dimension(:), allocatable :: coul_energy_NN
      double precision, dimension(:), allocatable :: WeightsA
      double precision, dimension(:), allocatable :: charge
      double precision, dimension(:), allocatable :: CoulombA,CoulombB
      double precision, dimension(:), allocatable :: Jcharge,WeightsB
      type(type_grid_points),dimension(:),allocatable :: J_grid_points
!
! Local array:
!
! Begin:
      call PRG_manager ('enter', 'ENERGY_VEE_NN', 'UTILITY')
!
      call GET_object ('QM', 'ENERGY_COMPONENTS', Wavefunction)
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)
      call BLD_BeckeW_XI
!
! Begin: 
      allocate(coul_energy_NN(Natoms))
      coul_energy_NN_tot=ZERO
      coul_energy_NN(1:Natoms)=ZERO

      do Iatom=1,Natoms
      Znum=CARTESIAN(Iatom)%Atomic_number
      if(Znum.le.0)cycle
      Ifound=GRID_loc(Znum)
      NApts_atom=NApoints_atom(Ifound)

      allocate(grid_points(1:NApts_atom))
      allocate(charge(NApts_atom))
      allocate(WeightsA(NApts_atom))
      allocate(CoulombA(NApts_atom))
!
! Moving grid points on cartesian coordinate grid
        do IApoint=1,NApts_atom
          grid_points(IApoint)%X=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          grid_points(IApoint)%Y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          grid_points(IApoint)%Z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
          grid_points(IApoint)%w=Egridpts(Ifound,IApoint)%w
        end do ! IApoint

      call GET_weights (grid_points, NApts_atom, Iatom, WeightsA)
      call GET_density (grid_points, NApts_atom, charge)
!
! rho(ri)dri
      do IApoint=1,NApts_atom
        CoulombA(IApoint)=charge(IApoint)*WeightsA(IApoint)*grid_points(IApoint)%w
      end do

      do Jatom=Iatom,Natoms
        JZ_num=CARTESIAN(Jatom)%Atomic_number
        if(JZ_num.le.0)cycle
        Jfound=GRID_loc(JZ_num)
          if(Iatom.eq.Jatom)then
            JNApts_atom = NApts_atom
            allocate(J_grid_points(JNApts_atom),CoulombB(JNApts_atom))
            allocate(Jcharge(JNApts_atom),WeightsB(JNApts_atom))
            J_grid_points=grid_points
            CoulombB=CoulombA
          else
            JNApts_atom=NApoints_atom(Jfound)
            allocate(J_grid_points(1:JNApts_atom))
            do JApoint=1,JNApts_atom
              J_grid_points(JApoint)%X=Egridpts(Jfound,JApoint)%X+CARTESIAN(Jatom)%X
              J_grid_points(JApoint)%Y=Egridpts(Jfound,JApoint)%Y+CARTESIAN(Jatom)%Y
              J_grid_points(JApoint)%Z=Egridpts(Jfound,JApoint)%Z+CARTESIAN(Jatom)%Z
              J_grid_points(JApoint)%w=Egridpts(Jfound,JApoint)%w
            end do
            allocate(Jcharge(JNApts_atom),CoulombB(JNApts_atom),WeightsB(JNApts_atom))
            call GET_density (J_grid_points, JNApts_atom, Jcharge)
            call GET_weights (J_grid_points, JNApts_atom, Jatom, WeightsB)  

            do JApoint = 1,JNApts_atom
              CoulombB(JApoint) = Jcharge(JApoint)*WeightsB(JApoint)*J_grid_points(JApoint)%w
            end do
          end if ! Iatom.eq.Jatom
 
          do IApoint=1,NApts_atom
            do JApoint=1,JNApts_atom
              if(IApoint.eq.JApoint.and.Iatom.eq.Jatom)cycle
              rij = DSQRT((grid_points(IApoint)%X-J_grid_points(JApoint)%X)**2+ &
                          (grid_points(IApoint)%Y-J_grid_points(JApoint)%Y)**2+ &
                          (grid_points(IApoint)%Z-J_grid_points(JApoint)%Z)**2)
              temp = CoulombA(IApoint)*CoulombB(JApoint)/rij
              coul_energy_NN(Iatom)=coul_energy_NN(Iatom)+PT5*temp
              if(Iatom.ne.Jatom)then
              coul_energy_NN(Jatom)=coul_energy_NN(Jatom)+PT5*temp
              end if
            end do !JApoint
          end do !IApoint
          deallocate (J_grid_points)
          deallocate (Jcharge, CoulombB, WeightsB)
      end do ! Jatom

      coul_energy_NN(Iatom)=FourPi*FourPi*coul_energy_NN(Iatom)
      coul_energy_NN_tot = coul_energy_NN_tot+coul_energy_NN(Iatom)

      deallocate(CoulombA)
      deallocate(grid_points)
      deallocate(charge)
      deallocate(WeightsA)
      end do ! Iatom

      write(UNIout,'(a,f14.6)')'Coulomb NN ',coul_energy_NN_tot
!
! End of routine ENERGY_Vee_NN
      call PRG_manager ('exit', 'ENERGY_VEE_NN', 'UTILITY')
      return
      end subroutine ENERGY_Vee_NN
