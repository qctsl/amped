      subroutine ENERGY_CORRELATION
!****************************************************************************************
!     Date last modified: December 31, 2015                                             *
!     Authors: Ahmad I. Alrawashdeh                                                     *
!     Description: Calculate the velcocity numerically                                  *
!****************************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE module_grid_points

      implicit none
!
! Local scalars:
      integer :: Iatom,Ifound
      integer :: IApoint,JApoint
      integer :: Jatom,Jfound,ZnumA,ZnumB,JNApts_atom
      integer, parameter :: Nelements=92  ! For now!!
      double precision :: WeightA,WeightB,rij,rA_avg,rB_avg,temp,CFNal,MAX_CFNal,Pij
      double precision :: corr_energy_NN_tot
      double precision :: factor
      double precision, dimension(:), allocatable :: WeightsA
      double precision, dimension(:), allocatable :: charge
      double precision, dimension(:), allocatable :: Vri
      double precision, dimension(:), allocatable :: Vrj
      double precision, dimension(:), allocatable :: corr_energy_NN
      double precision, dimension(:), allocatable :: rhoA_ri,rhoB_rj
      double precision, dimension(:), allocatable :: Jcharge,WeightsB
      type(type_grid_points),dimension(:),allocatable :: grid_pointsJ
!
! Local array:
!     double precision, dimension(:), allocatable :: dAOprodX,dAOprodY,dAOprodZ
      double precision, dimension(:), allocatable :: dxdxGprod,dydyGprod,dzdzGprod
      double precision, dimension(Nelements) :: Atomic_factor
      data Atomic_factor/ &
                   2.15,                                                 3.08,  & !  H-He
                   5.83, 6.16,             6.78, 7.48, 8.12, 8.19, 8.31, 8.60,  & ! Li-Ne
                   1.80, 1.50,             1.25, 1.10, 1.00, 1.00, 1.00, 0.95,  & ! Na-Ar
                   2.20, 1.80,                                                  & !  K-Ca
                   1.60, 1.40, 1.35, 1.40, 1.40, 1.40, 1.35, 1.35, 1.35, 1.35,  & ! Sc-Zn
                                           1.30, 1.25, 1.15, 1.15, 1.15, 1.10,  & ! Ga-Kr
                   2.35, 2.00,                                                  & ! Rb-Sr
                   1.80, 1.55, 1.45, 1.45, 1.35, 1.30, 1.35, 1.40, 1.60, 1.55,  & !  Y-Cd
                                           1.55, 1.45, 1.45, 1.40, 1.40, 1.30,  & ! In-Xe
                   38*0.0D0/

!
! Begin:
      call PRG_manager ('enter', 'ENERGY_CORRELATION', 'UTILITY')
!
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)
      call BLD_BeckeW_XI
!
! Begin: 
!     allocate (dAOprodX(1:MATlen), dAOprodY(1:MATlen), dAOprodZ(1:MATlen))
      allocate (dxdxGprod(1:MATlen), dydyGprod(1:MATlen), dzdzGprod(1:MATlen))
      allocate(corr_energy_NN(Natoms))
      corr_energy_NN_tot=ZERO
      corr_energy_NN(1:Natoms)=ZERO

!     write(6,*)'rij,Vri,Vrj,Pij:'
      rA_avg=ZERO
      rB_avg=ZERO
      MAX_CFNal=ZERO
      do Iatom=1,Natoms
      ZnumA=CARTESIAN(Iatom)%Atomic_number
      if(ZnumA.le.0)cycle
      Ifound=GRID_loc(ZnumA)
      NApts_atom=NApoints_atom(Ifound)

      allocate(grid_points(1:NApts_atom))
      allocate(charge(NApts_atom))
      allocate(WeightsA(NApts_atom))
      allocate(rhoA_ri(NApts_atom))
      allocate(Vri(NApts_atom))
!
! Moving grid points on cartesian coordinate grid
        do IApoint=1,NApts_atom
          grid_points(IApoint)%X=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          grid_points(IApoint)%Y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          grid_points(IApoint)%Z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
          grid_points(IApoint)%w=Egridpts(Ifound,IApoint)%w
        end do ! IApoint

      call GET_weights (grid_points, NApts_atom, Iatom, WeightsA)
      call GET_density (grid_points, NApts_atom, charge)
!
! rho(ri)dri
      do IApoint=1,NApts_atom
        rhoA_ri(IApoint)=charge(IApoint)*WeightsA(IApoint)*grid_points(IApoint)%w
        rA_avg=rA_avg+rhoA_ri(IApoint)*dsqrt(grid_points(IApoint)%X**2+grid_points(IApoint)%Y**2+grid_points(IApoint)%Z**2)
!       WeightA=WeightsA(IApoint)*grid_points(IApoint)%w
!       call Pij_GRID (grid_points(IApoint)%X, grid_points(IApoint)%Y, grid_points(IApoint)%Z, &
!                            WeightA, Vri(IApoint))
      end do
      rA_avg=FourPi*rA_avg

      do Jatom=Iatom,Natoms
        ZnumB=CARTESIAN(Jatom)%Atomic_number
        if(ZnumB.le.0)cycle
        Jfound=GRID_loc(ZnumB)
        allocate(Vrj(NApts_atom))
          if(Iatom.eq.Jatom)then
            JNApts_atom = NApts_atom
            allocate(grid_pointsJ(JNApts_atom),rhoB_rj(JNApts_atom))
            allocate(Jcharge(JNApts_atom),WeightsB(JNApts_atom))
            grid_pointsJ=grid_points
            rhoB_rj=rhoA_ri
            Vrj=Vri
            rB_avg=rA_avg
          else
            JNApts_atom=NApoints_atom(Jfound)
            allocate(grid_pointsJ(1:JNApts_atom))
            do JApoint=1,JNApts_atom
              grid_pointsJ(JApoint)%X=Egridpts(Jfound,JApoint)%X+CARTESIAN(Jatom)%X
              grid_pointsJ(JApoint)%Y=Egridpts(Jfound,JApoint)%Y+CARTESIAN(Jatom)%Y
              grid_pointsJ(JApoint)%Z=Egridpts(Jfound,JApoint)%Z+CARTESIAN(Jatom)%Z
              grid_pointsJ(JApoint)%w=Egridpts(Jfound,JApoint)%w
            end do
            allocate(Jcharge(JNApts_atom),rhoB_rj(JNApts_atom),WeightsB(JNApts_atom))
            call GET_density (grid_pointsJ, JNApts_atom, Jcharge)
            call GET_weights (grid_pointsJ, JNApts_atom, Jatom, WeightsB)  

            do JApoint = 1,JNApts_atom
              rhoB_rj(JApoint) = Jcharge(JApoint)*WeightsB(JApoint)*grid_pointsJ(JApoint)%w
       rB_avg=rB_avg+rhoB_rj(JApoint)*dsqrt(grid_pointsJ(JApoint)%X**2+grid_pointsJ(JApoint)%Y**2+grid_pointsJ(JApoint)%Z**2)
!             WeightB=WeightsB(JApoint)*grid_pointsJ(JApoint)%w
!             call Pij_GRID (grid_pointsJ(JApoint)%X, grid_pointsJ(JApoint)%Y, grid_pointsJ(JApoint)%Z, &
!                            WeightB, Vrj(JApoint))
            end do
      rB_avg=FourPi*rB_avg
          end if ! Iatom.eq.Jatom
      write(UNIout,'(a,2f14.6)')'<rA>, <rB>: ',rA_avg,rB_avg
 
          do IApoint=1,NApts_atom
            do JApoint=1,JNApts_atom
              if(IApoint.eq.JApoint.and.Iatom.eq.Jatom)cycle
              rij = DSQRT((grid_points(IApoint)%X-grid_pointsJ(JApoint)%X)**2+ &
                          (grid_points(IApoint)%Y-grid_pointsJ(JApoint)%Y)**2+ &
                          (grid_points(IApoint)%Z-grid_pointsJ(JApoint)%Z)**2)
              temp = rhoA_ri(IApoint)*rhoB_rj(JApoint)/rij
!             Pij=FourPi*dabs(dsqrt(Vri(IApoint))+dsqrt(Vrj(JApoint)))
!             factor=1.0D04*dble(ZnumA*ZnumB)*(rij*Pij-0.0D0) ! /dble(ZnumA*ZnumB)
              factor=Atomic_factor(ZnumA)*Atomic_factor(ZnumB)*rij ! /dsqrt(dble(ZnumA*ZnumB))
              if(factor.ge.20.0D0)then
                CFNal=ZERO
              else
!               factor=8.0D0*(rij-r0)
!             write(6,*)'rij: ',rij  ! Fermi-Dirac does not work!
                CFNal=dexp(-factor)
              end if
              if(CFNal.gt.MAX_CFNal)MAX_CFNal=CFNal
!             write(6,'(1PE18.4,2E18.4)')rij,factor,CFNal ! ,Vri(IApoint),Vrj(JApoint),Pij
              corr_energy_NN(Iatom)=corr_energy_NN(Iatom)+PT5*CFNal*temp
              if(Iatom.ne.Jatom)then
              corr_energy_NN(Jatom)=corr_energy_NN(Jatom)+PT5*CFNal*temp
              end if
            end do !JApoint
          end do !IApoint
          deallocate(grid_pointsJ)
          deallocate(Jcharge,rhoB_rj,WeightsB)
      deallocate(Vrj)
      end do ! Jatom

      corr_energy_NN(Iatom)=FourPi*FourPi*corr_energy_NN(Iatom)
      corr_energy_NN_tot=corr_energy_NN_tot+corr_energy_NN(Iatom)

      deallocate(rhoA_ri)
      deallocate(grid_points)
      deallocate(charge)
      deallocate(WeightsA)
      deallocate(Vri)
      end do ! Iatom

      corr_energy_NN_tot=-corr_energy_NN_tot
      write(UNIout,'(a,f14.6)')'Maximum CFNal: ',MAX_CFNal
      write(UNIout,'(a,f14.6)')'Correlation energy NN ',corr_energy_NN_tot
!
! End of routine ENERGY_CORRELATION
      call PRG_manager ('exit', 'ENERGY_CORRELATION', 'UTILITY')
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine Pij_GRID (XApt, YApt, ZApt, WeightA, Tri)
!****************************************************************************************
!     Date last modified: January 5, 2018                                               *
!     Authors: R. A. Poirier                                                            *
!     Description: Calculate the velcoity at a point (Xpt, Ypt, Zpt)                    *
!****************************************************************************************
! Modules:

      implicit none

!
! Local scalars:
      double precision :: XApt,YApt,ZApt,WeightA,Vri
      double precision :: Vx,Vy,Vz
      double precision :: Tx,Ty,Tz,Tri
!
! Local functions:
      double precision :: TraceAB
!
! Begin: 
      call dGAdGB_products (XApt, YApt, ZApt, dxdxGprod, dydyGprod, dzdzGprod, MATlen)
      Tx=TraceAB (PM0, dxdxGprod, NBasis)
      Tx=Tx*WeightA
      Ty=TraceAB (PM0, dydyGprod, NBasis)
      Ty=Ty*WeightA
      Tz=TraceAB (PM0, dzdzGprod, NBasis)
      Tz=Tz*WeightA
      Tri=PT5*(Tx+Ty+Tz)
!     call Velocity_Gaussian_prod (XApt, YApt, ZApt, dAOprodX, dAOprodY, dAOprodZ, MATlen)
!     Vx=TraceAB (PM0, dAOprodX, NBasis)
!     Vx=Vx*WeightA
!     Vy=TraceAB (PM0, dAOprodY, NBasis)
!     Vy=Vy*WeightA
!     Vz=TraceAB (PM0, dAOprodZ, NBasis)
!     Vz=Vz*WeightA
!     Vri=dsqrt(Vx*Vx+Vy*Vy+Vz*Vz)

!
! End of routine Pij_GRID
      return
      end subroutine Pij_GRID

! END CONTAINS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine ENERGY_CORRELATION
