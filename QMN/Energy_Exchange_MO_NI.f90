      subroutine ENERGY_Exchange_MO_NI
!*******************************************************************************
!     Date last modified: December 31, 2015                                    *
!     Authors:                                                                 *
!     Description:                                                             * 
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE module_grid_points

      implicit none
!
! Local scalars:
      integer :: Iatom,IApoint,IAIM,Znum,Ifound
      double precision :: Katom,KHFatom,Xpt,Ypt,Zpt
!
! Local array:
      double precision, dimension(:), allocatable :: WeightsA
      double precision :: KgridA, KHFgridA

!
! Begin:
      call PRG_manager ('enter', 'ENERGY_EXCHANGE_MO_NI', 'UTILITY')
!
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)
      call BLD_BeckeW_XI
!

      allocate (K_Atomic(Natoms))
      allocate (KHF_Atomic(Natoms))

      K_total=ZERO   ! pure K
      K_Atomic=ZERO
      KHF_total=ZERO
      KHF_Atomic=ZERO
      write(UNIout,'(/a)')'Atom                K           KHF'

      if(NAIMprint.eq.0)then ! If no atoms specified, do them all!
        NAIMprint=Natoms
        do Iatom=1,Natoms
          AIMprint(Iatom)=Iatom
        end do ! Iatom
      end if

      do IAIM=1,NAIMprint
      Iatom=AIMprint(IAIM)
!     do Iatom=1,Natoms
      Znum=CARTESIAN(Iatom)%Atomic_number
      if(Znum.le.0)cycle
      Ifound=GRID_loc(Znum)
      NApts_atom=NApoints_atom(Ifound)

      allocate(grid_points(1:NApts_atom))
      allocate(WeightsA(NApts_atom))
!
! Moving grid points on cartesian coordinate grid
        do IApoint=1,NApts_atom
          grid_points(IApoint)%X=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          grid_points(IApoint)%Y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          grid_points(IApoint)%Z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
        end do ! IApoint

      call GET_weights (grid_points, NApts_atom, Iatom, WeightsA)

      Katom=ZERO
      KHFatom=ZERO
      
      do IApoint=1,NApts_atom
        if(WeightsA(IApoint).le.1.0D-6)cycle ! Makes no difference
        Xpt=grid_points(IApoint)%X
        Ypt=grid_points(IApoint)%Y
        Zpt=grid_points(IApoint)%Z
        call GET_Exchange_MO_point (Xpt, Ypt, Zpt, KgridA, KHFgridA)
        Katom=Katom+KgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
        KHFatom=KHFatom+KHFgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
      end do ! IApoint

      K_Atomic(Iatom)=K_Atomic(Iatom)+Katom
      KHF_Atomic(Iatom)=KHF_Atomic(Iatom)+KHFatom

      deallocate(grid_points)
      deallocate(WeightsA)

      end do ! Iatom

!
! Now multiply by 4pi and get total exchange
      do IAIM=1,NAIMprint
        Iatom=AIMprint(IAIM)
!     do Iatom=1,Natoms
      Znum=CARTESIAN(Iatom)%Atomic_number
      if(Znum.le.0)cycle
      K_Atomic(Iatom)=FourPi*K_Atomic(Iatom)
      KHF_Atomic(Iatom)=FourPi*KHF_Atomic(Iatom)

      write(UNIout,'(i8,1x,3f14.6)')Iatom,K_Atomic(Iatom),KHF_Atomic(Iatom)

      K_total = K_total + K_Atomic(Iatom)
      KHF_total = KHF_total + KHF_Atomic(Iatom)
      end do ! Iatom
!
      if(NAIMprint.eq.Natoms)write(UNIout,'(a,2x,3f14.6)')'Total',K_total,KHF_total

! End of routine ENERGY_K_NI
      deallocate(K_Atomic,KHF_Atomic)
      call PRG_manager ('exit', 'ENERGY_EXCHANGE_MO_NI', 'UTILITY')
      return
      end subroutine ENERGY_Exchange_MO_NI      
