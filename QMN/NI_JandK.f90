      module NI_JandK
!***********************************************************************
!  Date last modified: June 4, 2008     version 7
!  Author: Angela Crane
!  Description: Compute the Coulomb and Exchange integrals numerically
!***********************************************************************
! Modules
      USE module_grid_points
!
      implicit none
!
!  Local scalars
      integer :: Iatom
      integer :: IApoint                 ! angular grid points
      integer :: Ifound                  ! loop over the atom
      integer :: NApoints_tot            ! Total angular grid points
      integer :: MAX_Apts                ! Maximum number of angular points/atom
      integer :: Z_num                   ! Atomic Number
      integer :: MuBasis,NuBasis,MuNu    ! Basis functions and length
      integer :: TApoints                ! Total number of angular points (molecule)
      double precision, dimension(:), allocatable :: IGprod  ! Gaussian Product for a grid point
      double precision, dimension(:,:), allocatable :: GprodA ! For all grid points
      double precision, dimension(:,:), allocatable :: VintA ! Vint for all grid points
      double precision, dimension(:), allocatable :: Weights ! Combine Becke and Angular
      double precision, dimension(:,:), allocatable :: ABweights ! For all grid points
!
!
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine INI_NI_JandK (Length)
!***********************************************************************
!  Date last modified: June 4, 2008     version 7
!  Author: Angela Crane
!  Description: Compute the Coulomb and Exchange integrals numerically
!***********************************************************************
! Modules
!
      implicit none
!
      integer :: Length

! Allocating and zeroing arrays
      allocate(IGprod(Length))

      return
      end subroutine INI_NI_JandK
      subroutine BLD_NI_JandK (J_MuNu, K_MuNu, Length, LskipG, LskipK, LskipP)
!***********************************************************************
!  Date last modified: June 4, 2008     version 7
!  Author: Angela Crane
!  Description: Compute the Coulomb and Exchange integrals numerically
!***********************************************************************
! Modules
      USE program_constants
      USE type_molecule
      USE N_integration
      USE QM_defaults
      USE QM_objects
      USE INT_objects
!
      implicit none
!
! Input scalars
      integer :: Length             ! length of array J_MuNu
      logical :: LskipK             ! Logical skip the exchange integrals
      logical :: LskipG(Length)      ! Logical arrray for skipping on deltaG
      logical :: LskipP(Length)      ! Logical arrray for skipping on deltaP
      double precision :: J_MuNu(Length)  ! Coulomb integrals
      double precision :: K_MuNu(Length)  ! Exchange integrals
!
! Local scalars
      logical :: Lfound
      integer :: LamBasis,SigBasis,LamSig ! lambda and sigma     
      integer :: LamNu,MuSig,MuLam,SigNu       ! indices for linear array
      double precision :: Xpt,Ypt,Zpt    ! 
      double precision :: JAsum,KAsum    ! 
      double precision :: Vpotl    ! 
      double precision :: TraceM    ! 
!
! Parameters:
      double precision :: SkipW     ! Skip based on weights 
      parameter (SkipW=1.0D-14)
!
! Begin
! zero arrays
      Length=Nbasis*(Nbasis+1)/2
      J_MuNu(1:Length)=0.0D0
      K_MuNu(1:Length)=0.0D0
      Vint(1:Length)=0.0D0  

LApK: do IApoint=1,TApoints
        Xpt=Grid_points(IApoint)%X
        Ypt=Grid_points(IApoint)%Y
        Zpt=Grid_points(IApoint)%Z
        call I1E_Vint (Vint, IGprod, Length, Xpt, Ypt, Zpt) ! Vint and IGprod for grid point i
        Vpotl=-TraceM (Vint, PM0, LskipP, Nbasis, Length)
        JAsum=Vpotl*Grid_points(IApoint)%w
!
! Looping over Mu and Nu
LMu:  do MuBasis=1,Nbasis
LNu:    do NuBasis=1,MuBasis
          MuNu=MuBasis*(MuBasis-1)/2+NuBasis
!         if(LskipG(MuNu))cycle LNu
!         if(LskipP(MuNu))cycle LNu
!
            KAsum=ZERO
LLam:       do LamBasis=1,NBasis
              if(NuBasis.gt.LamBasis)then
                LamNu=NuBasis*(NuBasis-1)/2+LamBasis
              else
                LamNu=LamBasis*(LamBasis-1)/2+NuBasis
              end if
              if(MuBasis.gt.LamBasis)then
                MuLam=MuBasis*(MuBasis-1)/2+LamBasis
              else
                MuLam=LamBasis*(LamBasis-1)/2+MuBasis
              end if
LSig:         do SigBasis=1,LamBasis
                LamSig=LamBasis*(LamBasis-1)/2+SigBasis
!               if(LskipP(LamSig))cycle Lsig
                if(MuBasis.gt.SigBasis)then
                  MuSig=MuBasis*(MuBasis-1)/2+SigBasis
                else
                  MuSig=SigBasis*(SigBasis-1)/2+MuBasis
                end if
                if(NuBasis.gt.SigBasis)then
                  SigNu=NuBasis*(NuBasis-1)/2+SigBasis
                else
                  SigNu=SigBasis*(SigBasis-1)/2+NuBasis
                end if
                if(LamBasis.eq.SigBasis)then
!                 JAsum=JAsum+PM0(LamSig)*Vint(LamSig)
                  KAsum=KAsum+PM0(LamSig)*Vint(LamNu)*IGprod(MuSig)
                else
!                 JAsum=JAsum+TWO*PM0(LamSig)*Vint(LamSig)
                  KAsum=KAsum+PM0(LamSig)*(Vint(LamNu)*IGprod(MuSig)+ &
                                           Vint(SigNu)*IGprod(MuLam))
                end if
              end do LSig ! SigBasis
            end do LLAM ! LamBasis
!         J_MuNu(MuNu)=J_MuNu(MuNu)-JAsum*IGprod(MuNu)*Grid_points(IApoint)%w
          J_MuNu(MuNu)=J_MuNu(MuNu)+JAsum*IGprod(MuNu)
          K_MuNu(MuNu)=K_MuNu(MuNu)-KAsum*Grid_points(IApoint)%w
        end do LNu ! NuBasis
      end do LMu ! MuBasis
      end do LApK ! IApoint
!
      return
      end subroutine BLD_NI_JandK
      function NI_Radial (Ibasis, ITemp) result (TRsum)
!***********************************************************************
!  Date last modified: June 4, 2008     version 7
!  Author: Angela Crane
!  Description: Compute the Coulomb and Exchange integrals numerically
!***********************************************************************
! Modules
      USE program_constants
      USE N_integration
!
      implicit none
!
!  Input scalar:
      integer :: Ibasis
!
!  Local scalars:
      integer :: IApoint
      double precision :: ITemp(NApts_atom)
!
!  Output scalar:
      double precision :: TRsum
!
      TRsum=ZERO
! Sum over the angular points in the region
      do IApoint=1,NApts_atom
        TRsum=TRsum+ITemp(IApoint)*GprodA(Ibasis,IApoint)
      end do ! IApoint

      return
      end function NI_Radial
      subroutine BLD_Bweights
!***********************************************************************
!  Date last modified: May 12, 2009     version 1
!  Author: R.A. Poirier
!  Description: Compute the Becke weights for all grid points of all atoms
!***********************************************************************
! Modules
      USE program_constants
      USE type_molecule
      USE N_integration
      USE module_grid_points
!
      implicit none
!
! Local scalars:
      integer :: RIend,AIend             ! loop over regions
      integer :: RIbegn,AIbegn           ! loop over regions
      integer :: Iregion                 ! loop over regions
      integer :: Nap_sphere,Nrp_region   ! loop over regions
      integer :: KRpoint                 ! loop over radial points
      double precision :: Rsum
      double precision :: Wt
      type(type_grid_points), dimension(:), allocatable :: grid_point_temp

      double precision :: SkipW     ! Skip based on weights 
      parameter (SkipW=1.0D-14)

! Get total number of angular points
!     write(6,'(a)')' BLD grids: '
      NApoints_tot=0
      MAX_Apts=0
      do Iatom=1,Natoms
        Z_num=CARTESIAN(Iatom)%Atomic_number
        if(Z_num.le.0)cycle
! Obtaining grid points
        Ifound=GRID_loc(Z_num)
        NApts_atom=NApoints_atom(Ifound)
        if(NApts_atom.gt.MAX_Apts)MAX_Apts=NApts_atom
        NApoints_tot=NApoints_tot+NApts_atom
      end do ! Iatom
!     write(6,'(a,x,I0)')' MAX angular points: ',MAX_Apts
!     call flush(6)

      allocate (Grid_points(MAX_Apts*Natoms))
! Compute the Becke weights for all grid points of all atoms
      TApoints=0
      do Iatom=1,Natoms
        Z_num=CARTESIAN(Iatom)%Atomic_number
        if(Z_num.le.0)cycle
! Obtaining grid points
        Ifound=GRID_loc(Z_num)
        NApts_atom=NApoints_atom(Ifound)
        allocate(grid_point_temp(1:NApts_atom), Weights(NApts_atom))
!           
! Moving grid points on cartesian coordinate grid
        do IApoint=1,NApts_atom
          grid_point_temp(IApoint)%X=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          grid_point_temp(IApoint)%Y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          grid_point_temp(IApoint)%Z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
          grid_point_temp(IApoint)%w=Egridpts(Ifound,IApoint)%w
        end do ! IApoint
!            
! Getting Becke weights
          call BeckeWs (grid_point_temp, NApts_atom, Iatom, Weights)

! Loop over the radial points/regions
          RIend = 0
          AIend = 0
          do Iregion=1,num_regions
            Nrp_region=NRpoints_region(Ifound,Iregion)
            RIbegn = RIend+1
            RIend = RIbegn+Nrp_region-1
            do KRpoint=RIbegn, RIend
              Nap_sphere=Nap_grid(Ifound,Iregion)
              AIbegn=AIend+1
              AIend=AIbegn+Nap_sphere-1
!
! Sum over the angular points in the region
              do IApoint=AIbegn,AIend
                Wt=Weights(IApoint)*Egridpts(Ifound,IApoint)%w
!               if(Wt.gt.SkipW)then ! Save it
                  TApoints=TApoints+1
                  Grid_points(TApoints)%X=grid_point_temp(IApoint)%X
                  Grid_points(TApoints)%Y=grid_point_temp(IApoint)%Y
                  Grid_points(TApoints)%Z=grid_point_temp(IApoint)%Z
                  Grid_points(TApoints)%w=Wt
!       write(6,'(i8,3f28.6)')IApoint,Grid_points(TApoints)%X,Grid_points(TApoints)%y,Grid_points(TApoints)%z
!               end if
              end do ! IApoint
            end do ! KRpoint
          end do ! Iregion
          deallocate(grid_point_temp, Weights)
      end do ! Iatom
!     write(6,'(a,x,I0)')' Total number of angular points kept: ',TApoints
!     call flush(6)

      return
      end subroutine BLD_Bweights
! END CONTAINS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end module NI_JandK
      function TraceM (A, B, LskipP, Nelem, MATlen)
!***********************************************************************
!     Date last modified: July 07, 1992                                *
!     Author: R.A. Poirier                                             *
!     Description: Trace of a product of symmetric matrices A and B    *
!                  stored in linear vectors.                           *
!***********************************************************************
! Modules:
      USE program_constants

      implicit none
!
! Input scalars:
      integer MATlen,Nelem
!
! Input arrays:
      logical :: LskipP(MATlen)      ! Logical arrray for skipping on deltaP
      double precision :: A(MATlen),B(MATlen)
!
! Local scalars:
      integer I,J,K
      double precision :: TraceM
!
! Begin:
      TraceM=ZERO
      K=0
      do J=1,Nelem
      do I=1,J
        K=K+1
!       if(LskipP(K))cycle
        TraceM=TraceM+TWO*A(K)*B(K)
      end do ! I
!     if(LskipP(K))cycle
      TraceM=TraceM-A(K)*B(K)
      end do ! J
!
! End of function TraceM
      return
      end function TraceM
