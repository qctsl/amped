      subroutine ENERGY_Vee_MO_NI
!****************************************************************************************
!     Date last modified: December 31, 2015                                             *
!     Authors:                                                                          *
!     Description:                                                                      * 
!****************************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE module_grid_points

      implicit none
!
! Local scalars:
      integer :: Iatom,IApoint,IAIM,Znum,Ifound
      double precision :: Jatom,Katom,JHFatom,KHFatom,Xpt,Ypt,Zpt
!
! Local array:
      double precision, dimension(:), allocatable :: WeightsA
      double precision :: JgridA,JHFgridA
      double precision :: KgridA,KHFgridA

!
! Begin:
      call PRG_manager ('enter', 'ENERGY_VEE_MO_NI', 'UTILITY')
!
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)
      call BLD_BeckeW_XI
!

      allocate (J_Atomic(Natoms))
      allocate (JAA_Atomic(Natoms))
      allocate (K_Atomic(Natoms))
      allocate (Vee_Atomic(Natoms))
      allocate (VeeHF_Atomic(Natoms))
      allocate (JHF_Atomic(Natoms))
      allocate (KHF_Atomic(Natoms))

      J_total=ZERO
      JAA_total=ZERO
      J_Atomic=ZERO
      K_total=ZERO
      K_Atomic=ZERO
      JHF_total=ZERO
      JHF_Atomic=ZERO
      KHF_total=ZERO
      KHF_Atomic=ZERO
     
      write(UNIout,'(3a)')'Using ',RADIAL_grid(1:len_trim(RADIAL_grid)),' grid'
      write(UNIout,'(/a)')'Atom           J             K            Vee           JHF           KHF           VeeHF          JAA'

      if(NAIMprint.eq.0)then ! If no atoms specified, do them all!
        NAIMprint=Natoms
        do Iatom=1,Natoms
          AIMprint(Iatom)=Iatom
        end do ! Iatom
      end if

      do IAIM=1,NAIMprint
      Iatom=AIMprint(IAIM)
!     do Iatom=1,Natoms
      Znum=CARTESIAN(Iatom)%Atomic_number
      if(Znum.le.0)cycle
      Ifound=GRID_loc(Znum)
      NApts_atom=NApoints_atom(Ifound)

      allocate(grid_points(1:NApts_atom))
      allocate(WeightsA(NApts_atom))
!
! Moving grid points on cartesian coordinate grid
        do IApoint=1,NApts_atom
          grid_points(IApoint)%X=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          grid_points(IApoint)%Y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          grid_points(IApoint)%Z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
        end do ! IApoint

      call GET_weights (grid_points, NApts_atom, Iatom, WeightsA)

      Jatom=ZERO
      Katom=ZERO
      JHFatom=ZERO
      KHFatom=ZERO
      do IApoint=1,NApts_atom
        if(WeightsA(IApoint).le.1.0D-6)cycle ! Makes no difference
        Xpt=grid_points(IApoint)%X
        Ypt=grid_points(IApoint)%Y
        Zpt=grid_points(IApoint)%Z
        call GET_COULOMB_MO_point (Xpt, Ypt, Zpt, JgridA, JHFgridA)
        call GET_EXCHANGE_MO_point (Xpt, Ypt, Zpt, KgridA, KHFgridA)
        Jatom=Jatom+JgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
        Katom=Katom+KgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
        JHFatom=JHFatom+JHFgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
        KHFatom=KHFatom+KHFgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
      end do ! IApoint

      J_Atomic(Iatom)=J_Atomic(Iatom)+Jatom
      K_Atomic(Iatom)=K_Atomic(Iatom)+Katom
      JHF_Atomic(Iatom)=JHF_Atomic(Iatom)+JHFatom
      KHF_Atomic(Iatom)=KHF_Atomic(Iatom)+KHFatom

      deallocate(grid_points)
      deallocate(WeightsA)

      end do ! IAIM

!
! Now multiply by 4pi and get total exchange
      do IAIM=1,NAIMprint
        Iatom=AIMprint(IAIM)
!     do Iatom=1,Natoms
        Znum=CARTESIAN(Iatom)%Atomic_number
        if(Znum.le.0)cycle
        J_Atomic(Iatom)=FourPi*J_Atomic(Iatom)
        K_Atomic(Iatom)=FourPi*K_Atomic(Iatom)
        Vee_Atomic(Iatom)=J_Atomic(Iatom)+K_Atomic(Iatom)
        JHF_Atomic(Iatom)=FourPi*JHF_Atomic(Iatom)
        KHF_Atomic(Iatom)=FourPi*KHF_Atomic(Iatom)
        JAA_Atomic(Iatom)=JHF_Atomic(Iatom)-J_Atomic(Iatom)
        VeeHF_Atomic(Iatom)=JHF_Atomic(Iatom)+KHF_Atomic(Iatom)
        write(UNIout,'(i8,1x,7f14.6)')Iatom,J_Atomic(Iatom),   &
                                            K_Atomic(Iatom),   &
                                            Vee_Atomic(Iatom), &
                                            JHF_Atomic(Iatom), &
                                            KHF_Atomic(Iatom), &
                                            VeeHF_Atomic(Iatom), &
                                            JAA_Atomic(Iatom)
        J_total = J_total + J_Atomic(Iatom)
        K_total = K_total + K_Atomic(Iatom)
        JHF_total = JHF_total + JHF_Atomic(Iatom)
        KHF_total = KHF_total + KHF_Atomic(Iatom)
      end do ! IAIM
!
      if(NAIMprint.eq.Natoms)then
        Vee_total=J_total+K_total
        VeeHF_total=JHF_total+KHF_total
        JAA_total=JHF_total-J_total
        write(UNIout,'(a,4x,7f14.6/)')'Total',J_total,K_total,Vee_total,JHF_total,KHF_total,VeeHF_total,JAA_total
      end if

! End of routine ENERGY_VEE_MO_NI
      call PRG_manager ('exit', 'ENERGY_VEE_MO_NI', 'UTILITY')
      return
      end subroutine ENERGY_Vee_MO_NI
