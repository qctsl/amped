      subroutine ENERGY_COULOMB_MO_NI
!****************************************************************************************
!     Date last modified: December 31, 2015                                             *
!     Authors:                                                                          *
!     Description:                                                                      * 
!****************************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE module_grid_points

      implicit none
!
! Local scalars:
      integer :: Iatom,IApoint,IAIM,Znum,Ifound
      double precision :: Jatom,Xpt,Ypt,Zpt
!
! Local array:
      double precision, dimension(:), allocatable :: WeightsA
      double precision :: JgridA, JHFgridA

!
! Begin:
      call PRG_manager ('enter', 'ENERGY_COULOMB_MO_NI', 'UTILITY')
!
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)
      call BLD_BeckeW_XI
!

      allocate (J_Atomic(Natoms))

      J_total=ZERO   ! pure J
      J_Atomic=ZERO
      write(UNIout,'(/a)')'Atom           Pure J'

      if(NAIMprint.eq.0)then ! If no atoms specified, do them all!
        NAIMprint=Natoms
        do Iatom=1,Natoms
          AIMprint(Iatom)=Iatom
        end do ! Iatom
      end if

      do IAIM=1,NAIMprint
      Iatom=AIMprint(IAIM)
!     do Iatom=1,Natoms
      Znum=CARTESIAN(Iatom)%Atomic_number
      if(Znum.le.0)cycle
      Ifound=GRID_loc(Znum)
      NApts_atom=NApoints_atom(Ifound)

      allocate(grid_points(1:NApts_atom))
      allocate(WeightsA(NApts_atom))
!
! Moving grid points on cartesian coordinate grid
        do IApoint=1,NApts_atom
          grid_points(IApoint)%X=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          grid_points(IApoint)%Y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          grid_points(IApoint)%Z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
        end do ! IApoint

      call GET_weights (grid_points, NApts_atom, Iatom, WeightsA)

      Jatom=ZERO
      do IApoint=1,NApts_atom
        if(WeightsA(IApoint).le.1.0D-6)cycle ! Makes no difference
        Xpt=grid_points(IApoint)%X
        Ypt=grid_points(IApoint)%Y
        Zpt=grid_points(IApoint)%Z
        call GET_COULOMB_MO_point (Xpt, Ypt, Zpt, JgridA, JHFgridA) 
        Jatom=Jatom+JgridA*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
      end do ! IApoint

      J_Atomic(Iatom)=J_Atomic(Iatom)+Jatom

      deallocate(grid_points)
      deallocate(WeightsA)

      end do ! Iatom

!
! Now multiply by 4pi and get total exchange
      do IAIM=1,NAIMprint
        Iatom=AIMprint(IAIM)
!     do Iatom=1,Natoms
        Znum=CARTESIAN(Iatom)%Atomic_number
        if(Znum.le.0)cycle
        J_Atomic(Iatom)=FourPi*J_Atomic(Iatom)
        write(UNIout,'(i8,1x,2f14.6)')Iatom,J_Atomic(Iatom)
        J_total = J_total + J_Atomic(Iatom)
      end do ! Iatom
!
      if(NAIMprint.eq.Natoms)write(UNIout,'(a,2x,2f14.6/)')'J_total',J_total

! End of routine ENERGY_K_NI
      call PRG_manager ('exit', 'ENERGY_COULOMB_MO_NI', 'UTILITY')
      return
      end subroutine ENERGY_COULOMB_MO_NI
