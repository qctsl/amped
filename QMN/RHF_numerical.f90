      subroutine RHF_numerical
!***********************************************************************
!     Date last modified: April 18, 1997                  Version 1.0  *
!     Author: R.A. Poirier                                             *
!     Description: Perform closed shell SCF calculation (also Direct). *
!***********************************************************************
! Modules:
      USE program_files
      USE type_basis_set
      USE QM_defaults
      USE QM_objects
      USE INT_objects
      USE NI_JandK

      implicit none
!
! Local scalars:

      type (basis_set), target :: FROM_basis ! FROM basis set
!
! Begin:
      call PRG_manager ('enter', 'RHF_numerical', 'CMO%RHF_NI')
!
      call GET_object ('GRID', 'RADIAL', 'SG1')
      call BLD_Bweights
      if (GUESS_from_basis(1:4).ne.'NONE')then
        call GET_basis_set (GUESS_from_basis, FROM_basis, .false.)
        Basis=>FROM_basis
        call RESET_basis_set
        call SCF_RHF_NI

        call SET_MO_project
        call KILL_object ('INT', '1EINT', 'AO')
        call KILL_object ('INT', '2EINT', I2E_type)
!       call CLEAR_objects ! CANNOT do THIS!!!!!!!!!!!!!!!!!
        MUN_PRTLEV=MUN_PRTLEV+1
        Basis=>Current_basis
        call RESET_basis_set
        call KILL_object ('QM', 'CMO_GUESS', 'RHF') ! Must build guess for second SCF
      end if

      call SCF_RHF_NI
!
! End of routine RHF_numerical
      call PRG_manager ('exit', 'RHF_numerical', 'CMO%RHF_NI')
      return
      end subroutine RHF_numerical
      subroutine SCF_RHF_NI
!***********************************************************************
!     Date last modified: April 18, 1997                  Version 1.0  *
!     Author: R.A. Poirier                                             *
!     Description: Perform closed shell Direct SCF calculation.        *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE QM_objects
      USE INT_objects
      USE matrix_print
      USE ixtrapolate
      USE NI_JandK

      implicit none
!
! Local scalars:
      integer :: JCYCLE
      integer :: I,IERROR,J
      logical :: LskipK
      double precision :: SCFCON             ! actual SCF convergence of density matrix
      double precision :: TEN
!
! Local parameters:
      double precision :: dGSkip,Pskip
      parameter (dGSkip=5.0D-08, Pskip=5.0D-06)
!
! Work arrays:
      double precision, dimension(:), allocatable :: SCRVEC
      double precision, dimension(:), allocatable :: GMn
      double precision, dimension(:), allocatable :: dGMn
      double precision, dimension(:), allocatable, target :: DPME
      double precision, dimension (:), allocatable :: J_MuNu
      double precision, dimension (:), allocatable :: K_MuNu
      logical, dimension(:), allocatable :: LskipG
      logical, dimension(:), allocatable :: LskipP
!     integer :: MuNu,NuBasis,MuBasis
      integer :: Iskip
!
! RHF Objects:
!      type (molecular_orbital), save, target :: CMO_RHF
!
! Local function:
      double precision ETRACE
!
      parameter (TEN=10.0D0)
!
! Begin:
      call PRG_manager ('enter', 'SCF_RHF_NI', 'UTILITY')
!
      call ALLOC_SCF
      call GET_object ('INT', '1EINT', 'AO')
      call INI_SCF_RHF_NI

      SCFCON=TEN
!
! Build the first Fock matrix
      LskipK=.false.
      call BLD_JandK_RHF_NI
      FockM(1:MATlen)=HCORE(1:MATLEN)+dGMn(1:MATLEN) ! F = H+2J-K = H+G
      call INI_Xpolation (PM0_RHF, MATlen)
      DPME(1:MATlen)=PM0_RHF(1:MATLEN)

      ENERGY_RHF%elec=PT5*ETRACE (HCORE, FockM, PM0_RHF, Nbasis, MATlen)
      ENERGY_RHF%HF=ENERGY_RHF%nuclear+ENERGY_RHF%elec
      ENERGY_RHF%total=ENERGY_RHF%HF

      JCYCLE=1
      IF(MUN_PRTLEV.GT.0)then
        write(UNIout,*)' ***** Using the Numerical SCF *****'
        write(UNIout,*)' No skipping being done '
        write(UNIout,'(a,1PE16.1)')'dG Skip set to ',dGSkip
        write(UNIout,'(a,1PE16.1)')' P Skip set to ',Pskip
        write(UNIout,'(/a,15x,a,F17.9,a/a,1PE12.4//10x,a,6x,a)') &
        'CLOSED SHELL SCF','Nuclear Repulsion Energy is',ENERGY_RHF%nuclear,' Hartrees', &
        'Convergence on Density Matrix Required to Exit is',SCFACC, &
        'CYCLE   ELECTRONIC ENERGY', 'TOTAL ENERGY    CONVERGENCE   EXTRAPOLATION'
         write(UNIout,'(a,i4,1x,2f20.9)')' SCF_CYCLE:',JCYCLE,ENERGY_RHF%elec,ENERGY_RHF%total
        call flush(UNIout)
      end if
!
! Start SCF cycling.
      converged=.false.
      do while (JCYCLE.le.SCFITN)
      JCYCLE=JCYCLE+1
!
      call DETlinW (OVRLAP, FockM, CMO_RHF%coeff, CMO_RHF%EIGVAL, MATlen, Nbasis, .true.)
      call DENBLD (CMO_RHF%coeff, CMO_RHF%occupancy, PM0_RHF, Nbasis, MATlen, CMO_RHF%NoccMO)
      PnM0(1:MATlen)=PM0_RHF(1:matlen)
      call Xpolation (SCFACC, SCFCON, Nbasis) ! call the extrapolation routine
      if(Converged)exit
      if(Lextrapolated)then
        PM0_RHF(1:MATlen)=extra(1:matlen)
      end if
!     if(SCFCON.le.5.0D-02)then ! 
!       LskipK=.false.
!     end if
      DPME(1:MATlen)=delta(1:MATlen)
      PM0=>DPME
      call BLD_JandK_RHF_NI   
!     FockM(1:MATlen)=HCORE(1:MATLEN)+GMn(1:MATLEN) ! F = H+2J-K = H+G
      FockM(1:MATlen)=FockM(1:MATLEN)+dGMn(1:MATLEN) ! F = H+2J-K = H+G
!
! Calculate the energy.
      ENERGY_RHF%elec=PT5*ETRACE (HCORE, FockM, PM0_RHF, Nbasis, MATlen)
      ENERGY_RHF%HF=ENERGY_RHF%nuclear+ENERGY_RHF%elec
      ENERGY_RHF%total=ENERGY_RHF%HF
!
! Print iteration results.
      IF(MUN_PRTLEV.GT.0)then
        if(Lextrapolated)then
          write(UNIout,'(a,i4,1x,2f20.9,20x,a8)')' SCF_CYCLE:',JCYCLE,ENERGY_RHF%elec,ENERGY_RHF%total,LABEL
        else
          write(UNIout,'(a,i4,1x,2f20.9,1PE14.5)')' SCF_CYCLE:',JCYCLE,ENERGY_RHF%elec,ENERGY_RHF%total,SCFCON
        end if
      end if
!
      end do ! while SCF cycle

      IF(converged)then
        call PHASE (CMO_RHF%coeff, Nbasis)
        call DENBLD (CMO_RHF%coeff, CMO_RHF%occupancy, PM0_RHF, Nbasis, MATlen, CMO_RHF%NoccMO)
        LSCF_converged=.true.
        IF(MUN_PRTLEV.GT.0)then
          write(UNIout,'(A,F17.6,A/)')'At Termination Total Energy is',ENERGY_RHF%total,'  Hartrees'
        end if ! MUN_PRTLEV.GT.0
      end if ! if(converged)
!
! Maximum iteration count exceeded without convergence?
      if(.not.converged)then
        if(LOptimization.and.LSCF_converged)then
          LSCF_converged=.false.
          write(UNIout,'(a,i4,a)')'WARNING> SCF_RHF_NI: SCF DID NOT CONVERGE AFTER ',SCFITN,' ITERATIONS'
          write(UNIout,'(a)')'Optimization allowed to continue'
        else
          write(UNIout,'(a,i4,a)')'ERROR> SCF_RHF_NI: SCF DID NOT CONVERGE AFTER ',SCFITN,' ITERATIONS'
          call PRG_stop ('SCF_RHF_NI> SCF DID NOT CONVERGE')
        end if
      else
! Copy the coefficients so they form the next guess
        CMOG%coeff=CMO_RHF%coeff
      end if
!
      call END_SCF_RHF_NI
      call END_Xpolation
!
! End of routine SCF_RHF_NI
      call PRG_manager ('exit', 'SCF_RHF_NI', 'UTILITY')
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine INI_SCF_RHF_NI
!***********************************************************************
!     Date last modified: October 11, 2001                             *
!     Author: R.A. Poirier                                             *
!     Description: Initialize all RHF parameters.                      *
!***********************************************************************
! Modules:
      USE type_basis_set
      USE INT_objects
      USE NI_JandK

      implicit none
!
! Local function:
      double precision GET_Enuclear
!
! Begin:
      call PRG_manager ('enter', 'INI_SCF_RHF_NI', 'UTILITY')
!
! Object:
      if(.not.associated(CMO_RHF%coeff))then
        allocate (CMO_RHF%coeff(Nbasis,NBASIS), CMO_RHF%EIGVAL(NBASIS),&
                  CMO_RHF%occupancy(NBASIS), STAT=IERROR)
        if(IERROR.ne.0)then
          stop ' INI_SCF_RHF_NI> allocate failure: CMO...'
        end if
      else
        deallocate (CMO_RHF%coeff, CMO_RHF%EIGVAL, CMO_RHF%occupancy, STAT=IERROR)
        if(IERROR.ne.0)then
          stop ' INI_SCF_RHF_NI> deallocate failure: CMO...'
        end if
        allocate (CMO_RHF%coeff(Nbasis,NBASIS), CMO_RHF%EIGVAL(NBASIS),&
                  CMO_RHF%occupancy(NBASIS), STAT=IERROR)
        if(IERROR.ne.0)then
          stop ' INI_SCF_RHF_NI> Re-allocate failure: CMO...'
        end if
      end if

      if(.not.allocated(PM0_RHF))then
        allocate (PM0_RHF(MATlen), STAT=IERROR)
        if(IERROR.ne.0)then
          stop ' INI_SCF_RHF_NI> allocate failure: PMO...'
        end if
      else
        deallocate (PM0_RHF, STAT=IERROR)
        if(IERROR.ne.0)then
          stop ' INI_SCF_RHF_NI> deallocate failure: PMO...'
        end if
        allocate (PM0_RHF(MATlen), STAT=IERROR)
        if(IERROR.ne.0)then
          stop ' INI_SCF_RHF_NI> Re-allocate failure: PMO...'
        end if
      end if
!
! Work arrays
      allocate (SCRVEC(1:Nbasis), DPME(MATlen), STAT=IERROR)
      if(IERROR.ne.0)then
        write(UNIout,*)'INI_SCF_RHF_NI> allocate failure: SCRVEC...'
        stop ' INI_SCF_RHF_NI> allocate failure: SCRVEC...'
      end if

      if(.not.allocated(FockM))then
        allocate (FockM(1:MATlen), STAT=IERROR)
      else
        deallocate (FockM)
        allocate (FockM(1:MATlen), STAT=IERROR)
      end if
!
! Initialization:
      CMO => CMO_RHF
      PM0 => PM0_RHF
!
! Intialize the energies
      ENERGY=>ENERGY_RHF
      ENERGY%nuclear=GET_Enuclear()
      ENERGY%HF=ZERO
      ENERGY%elec=ZERO
      ENERGY%total=ZERO
      ENERGY%wavefunction='RHF'//Basis_name(1:len_trim(Basis_name))
!
! Initialize OCCVEC
      CMO_RHF%NoccMO=CMOG%NOCCMO
      CMO_RHF%occupancy(1:CMO_RHF%NoccMO)=TWO
      CMO_RHF%occupancy(CMO_RHF%NoccMO+1:Nbasis)=ZERO
      CMO_RHF%coeff=CMOG%coeff
!
! Copy arrays:
      PM0_RHF(1:MATlen)=PM0G(1:MATLEN)
      FockM(1:MATlen)=HCORE(1:MATLEN)
!
      allocate(GMn(MATLEN))
      allocate(dGMn(MATLEN))
      allocate(LskipG(MATLEN), LskipP(MATLEN))
      GMn(1:MATLEN)=ZERO
      dGMn(1:MATLEN)=ZERO
      LskipG(1:MATLEN)=.false.
      LskipP(1:MATLEN)=.false.
      allocate(J_MuNu(MATlen),K_MuNu(MATlen))
!
! Clearing/allocating Vint
      if(.not.allocated(Vint))then
        allocate (Vint(MATlen))
      else
        deallocate(Vint)
        allocate (Vint(MATlen))
      end if
   
      call INI_NI_JandK (MATlen)
      Lextrapolated=.true.

! End of routine INI_SCF_RHF_NI
      call PRG_manager ('exit', 'INI_SCF_RHF_NI', 'UTILITY')
      return
      end subroutine INI_SCF_RHF_NI
      subroutine END_SCF_RHF_NI
!***********************************************************************
!     Date last modified: October 11, 2001                             *
!     Author: R.A. Poirier                                             *
!     Description: Initialize all RHF parameters.                      *
!***********************************************************************
! Modules:
      USE NI_JandK

      implicit none
!
! Deallocate the work arrays
      deallocate (SCRVEC)
      deallocate (GMn)
      deallocate (dGMn)
      deallocate (DPME)
      deallocate (IGprod)
      deallocate (J_MuNu)
      deallocate (K_MuNu)

      return
      end subroutine END_SCF_RHF_NI
      subroutine BLD_JandK_RHF_NI
!***********************************************************************
!     Date last modified: October 11, 2001                             *
!     Author: R.A. Poirier                                             *
!     Description: Initialize all RHF parameters.                      *
!***********************************************************************
! Modules:
      USE matrix_print

      implicit none

      double precision :: Temp

      call BLD_NI_JandK (J_MuNu, K_MuNu, MATlen, LskipG, LskipK, LskipP)

!     write(6,*)'PM0'
!     call PRT_matrix (PM0, MATlen, Nbasis)
!     write(6,*)'J_MuNu'
!     call PRT_matrix (J_MuNu, MATlen, Nbasis)
!     write(6,*)'K_MuNu'
!     call PRT_matrix (K_MuNu, MATlen, Nbasis)
!     call flush (6)
!
! Form DGM = 2J_MuNu-K_MuNu (Maybe the 4pi could be done here??
      do NuBasis=1, Nbasis
        do MuBasis=1, NuBasis
          MuNu = NuBasis*(NuBasis-1)/2+MuBasis
          LskipP(MuNu)=.false.
!         if(LskipG(MuNu))cycle
           Temp=FourPI*(J_MuNu(MuNu)-PT5*K_MuNu(MuNu)) ! Delta J and K
           dGMn(MuNu)=Temp
!          dGMn(MuNu)=Temp-GMn(MuNu)
!          GMn(MuNu)=Temp
           if(dabs(dGMn(MuNu)).le.dGSkip)LskipG(MuNu)=.true.
           if(dabs(PM0(MuNu)).le.Pskip)LskipP(MuNu)=.true.   ! NOTE: PM0 = DPME
        end do ! Nu
      end do ! Mu          

!     write(6,*)'DGM'
!     call PRT_matrix (DGM, MATlen, Nbasis)

      return
      end subroutine BLD_JandK_RHF_NI
! END CONTAINS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine SCF_RHF_NI
