      subroutine ENERGY_KINETIC_NI
!*******************************************************************************
!     Date last modified: December 31, 2015                                    *
!     Authors: Ahmad I. Alrawashdeh                                            *
!     Description: Calculate the velcocity numerically                         *
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE module_grid_points
      USE type_plotting

      implicit none
!
! Local scalars:
      integer :: IAIM,Iatom,Ipoint,IRegion,Znum,Ifound,NRpoints
      double precision :: XApt,YApt,ZApt,WApt,Bweight
      double precision :: Tx,Ty,Tz,TTx,TTy,TTz,Ttotal
      double precision :: TAx,TAy,TAz
!
! Local array:
      double precision, dimension(:), allocatable :: Tx_Atomic
      double precision, dimension(:), allocatable :: Ty_Atomic
      double precision, dimension(:), allocatable :: Tz_Atomic
!
! Begin:
      call PRG_manager ('enter', 'ENERGY_KINETIC_NI', 'UTILITY')
!
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)
      call BLD_BeckeW_XI
!
      if(.not.allocated(Atomic_Kinetic))then
        allocate (Atomic_Kinetic(Natoms))
      else
        deallocate (Atomic_Kinetic)
        allocate (Atomic_Kinetic(Natoms))
      end if

      TTx=ZERO
      TTy=ZERO
      TTz=ZERO
!
      allocate (Tx_Atomic(Natoms))
      allocate (Ty_Atomic(Natoms))
      allocate (Tz_Atomic(Natoms))
      Tx_Atomic(1:Natoms)=ZERO
      Ty_Atomic(1:Natoms)=ZERO
      Tz_Atomic(1:Natoms)=ZERO

      if(NAIMprint.eq.0)then ! If no atoms specified, do them all!
        NAIMprint=Natoms
        do Iatom=1,Natoms
          AIMprint(Iatom)=Iatom
        end do ! Iatom
      end if

      write(UNIout,'(2a)')'Kinetic Energy ',KineticMethod
      write(UNIout,'(a,f14.6)')'Atom        Kinetic(x)    Kinetic(y)    Kinetic(z)       Total'
      do IAIM=1,NAIMprint
      Iatom=AIMprint(IAIM)
      Znum=CARTESIAN(Iatom)%Atomic_number
      if(Znum.le.0)cycle
      Ifound=GRID_loc(Znum)
      NApts_atom=NApoints_atom(Ifound)
      TAx=ZERO
      TAy=ZERO
      TAz=ZERO
!
! Moving grid points on cartesian coordinate grid
      do Ipoint=1,NApts_atom
        XApt=Egridpts(Ifound,Ipoint)%X
        YApt=Egridpts(Ifound,Ipoint)%Y
        ZApt=Egridpts(Ifound,Ipoint)%Z
        XApt=XApt+CARTESIAN(Iatom)%X
        YApt=YApt+CARTESIAN(Iatom)%Y
        ZApt=ZApt+CARTESIAN(Iatom)%Z
        WApt=Egridpts(Ifound,Ipoint)%w
        call GET_weight1 (XApt, YApt, ZApt, Iatom, Bweight)
!        call GET_Kinetic_GRID (XApt, YApt, ZApt, Tx, Ty, Tz)
        Tx=Tx*Bweight*WApt
        Ty=Ty*Bweight*WApt
        Tz=Tz*Bweight*WApt

        TTx=TTx+Tx
        TTy=TTy+Ty
        TTz=TTz+Tz
        TAx=TAx+Tx
        TAy=TAy+Ty
        TAz=TAz+Tz
      end do ! Ipoint
      Tx_Atomic(Iatom)=PT5*TAx*FourPi
      Ty_Atomic(Iatom)=PT5*TAy*FourPi
      Tz_Atomic(Iatom)=PT5*TAz*FourPi
      Ttotal=Tx_Atomic(Iatom)+Ty_Atomic(Iatom)+Tz_Atomic(Iatom)
      Atomic_Kinetic(Iatom)=Ttotal
      write(UNIout,'(i8,4f14.6)')Iatom,Tx_Atomic(Iatom),Ty_Atomic(Iatom),Tz_Atomic(Iatom),Ttotal
      end do ! Iatom
!
      TTx=PT5*TTx*FourPi
      TTy=PT5*TTy*FourPi
      TTz=PT5*TTz*FourPi
      Ttotal=TTx+TTy+TTz
      write(UNIout,'(a,4f14.6)')'Ttotal: ',TTx,TTy,TTz,Ttotal
      write(UNIout,'(a)')' '

      deallocate (Tx_Atomic)
      deallocate (Ty_Atomic)
      deallocate (Tz_Atomic)
!
! End of routine ENERGY_KINETIC_NI
      call PRG_manager ('exit', 'ENERGY_KINETIC_NI', 'UTILITY')
      return
      end subroutine ENERGY_KINETIC_NI
