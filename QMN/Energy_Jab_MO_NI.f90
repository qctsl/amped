      subroutine ENERGY_Jab_MO_NI
!*******************************************************************************
!     Date last modified: December 31, 2015                                    *
!     Authors:                                                                 *
!     Description:                                                             * 
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE module_grid_points

      implicit none
!
! Local scalars:
      integer :: Iatom,IApoint,IAIM,Znum,Ifound,aMO,bMO
      double precision :: Katom,KHFatom,Xpt,Ypt,Zpt,Jab,Jba
!
! Local array:
      double precision, dimension(:), allocatable :: WeightsA
      double precision :: JabGrid, JabAtom, JEnergy, JHFEnergy
      double precision, allocatable,dimension(:,:) :: Jab_E
      double precision, allocatable,dimension(:,:,:) :: Jab_Atomic
      double precision, dimension(:,:), allocatable :: occ_Jab, Occ_JHFab
!
! Begin:
      call PRG_manager ('enter', 'ENERGY_JAB_MO_NI', 'UTILITY')
!
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)
!
      allocate (Jab_E(1:CMO%NoccMO,1:CMO%NoccMO))
      allocate (Jab_Atomic(1:CMO%NoccMO,1:CMO%NoccMO,Natoms))   
      allocate (occ_Jab(1:CMO%NoccMO,1:CMO%NoccMO))
      allocate (occ_JHFab(1:CMO%NoccMO,1:CMO%NoccMO)) 
      !      
      
      Jab_E=ZERO
      Jab_Atomic=ZERO
      
      call occupancy_Jab (occ_Jab, Occ_JHFab, CMO%NoccMO)
      
!      write(UNIout,'(/5x,a)')'Atom     aMO      bMO     Jab'
      
      if(NAIMprint.eq.0)then ! If no atoms specified, do them all!
        NAIMprint=Natoms
        do Iatom=1,Natoms
          AIMprint(Iatom)=Iatom
        end do ! Iatom
      end if

      do IAIM=1,NAIMprint
      Iatom=AIMprint(IAIM)
      Znum=CARTESIAN(Iatom)%Atomic_number
      if(Znum.le.0)cycle
      Ifound=GRID_loc(Znum)
      NApts_atom=NApoints_atom(Ifound)

      allocate(grid_points(1:NApts_atom))
      allocate(WeightsA(NApts_atom))
!
! Moving grid points on cartesian coordinate grid
      do IApoint=1,NApts_atom
        grid_points(IApoint)%X=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
        grid_points(IApoint)%Y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
        grid_points(IApoint)%Z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
      end do ! IApoint

      WeightsA=ONE
      if(Natoms.gt.1)call GET_weights (grid_points, NApts_atom, Iatom, WeightsA)
      
      do aMO=1,CMO%NoccMO !Nbasis 
      do bMO=1,aMO !Nbasis
      
      JabAtom=Zero
      do IApoint=1,NApts_atom
        if(WeightsA(IApoint).le.1.0D-6)cycle ! Makes no difference
        Xpt=grid_points(IApoint)%X
        Ypt=grid_points(IApoint)%Y
        Zpt=grid_points(IApoint)%Z
        call GET_Jab_MO_point (Xpt,Ypt,Zpt,aMO,bMO,Jab,Jba)
        JabGrid=(Jab+Jba)/TWO ! Note: Jab=Jba, we took the average to reduce the error in NI.
        JabAtom=JabAtom+JabGrid*Egridpts(Ifound,IApoint)%w*WeightsA(IApoint)
      end do ! IApoint

      Jab_Atomic(aMO,bMO,Iatom)=Jab_Atomic(aMO,bMO,Iatom)-JabAtom

! Now multiply by 4pi and get total Jab for each atom
      Jab_Atomic(aMO,bMO,Iatom)=Jab_Atomic(aMO,bMO,Iatom)*FourPi
      Jab_Atomic(bMO,aMO,Iatom)=Jab_Atomic(aMO,bMO,Iatom)
      Jab_E(aMO,bMO)=Jab_E(aMO,bMO)+Jab_Atomic(aMO,bMO,Iatom)
      Jab_E(bMO,aMO)=Jab_E(aMO,bMO)
      
!      write(UNIout,'(3i8,1x,f12.6)')Iatom, aMO, bMO, Jab_Atomic(aMO,bMO,Iatom)
      end do ! bMO
      end do ! aMO
      
      deallocate(grid_points)
      deallocate(WeightsA)

      end do ! Iatom
      
      JEnergy=ZERO
      JHFEnergy=ZERO
      write(UNIout,'(/2(a6),5(5x,a7))')'aMO','bMO','Jab','J_occ','JHF_occ','J','JHF'
      do aMO=1,CMO%NoccMO ! 
      do bMO=aMO,CMO%NoccMO ! Note: Jab=Jba but Jab(r).ne.Jba(r) 
       JEnergy=JEnergy+Jab_E(aMO,bMO)*occ_Jab(aMO,bMO)
       jHFEnergy=JHFEnergy+Jab_E(aMO,bMO)*occ_JHFab(aMO,bMO)
       write(UNIout,'(2i6,5f12.6)') aMO,bMO,Jab_E(aMO,bMO),occ_Jab(aMO,bMO),&
                                 & occ_JHFab(aMO,bMO), &
                                 & Jab_E(aMO,bMO)*occ_Jab(aMO,bMO), &
                                 & Jab_E(aMO,bMO)*occ_JHFab(aMO,bMO) 
      end do ! bMO
      end do ! aMO    
      
      write(UNIout,'(a,42x,2f12.6)') 'Total:', JEnergy, JHFEnergy      
! End of routine ENERGY_K_NI
      deallocate(Jab_Atomic,Jab_E)
      deallocate (occ_Jab)
      deallocate (occ_JHFab)

      call PRG_manager ('exit', 'ENERGY_JAB_MO_NI', 'UTILITY')
      return
      end subroutine ENERGY_Jab_MO_NI      
