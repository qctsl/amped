      subroutine ENERGY_VNE_NI
!****************************************************************************************
!     Date last modified: December 31, 2015                                             *
!     Authors: Ahmad I. Alrawashdeh                                                     *
!     Description: Calculate the velcocity numerically                                  *
!****************************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE module_grid_points
      USE type_plotting

      implicit none
!
! Local scalars:
      integer :: IAIM,Iatom,IApoint,IRegion,Znum,Ifound,NRpoints
      double precision :: Vne_total
!
! Local array:
      double precision, dimension(:), allocatable :: WeightsA
      double precision, dimension(:), allocatable :: charge
!
! Begin:
      call PRG_manager ('enter', 'ENERGY_VNE_NI', 'UTILITY')
!
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)
      call BLD_BeckeW_XI
!
      if(.not.allocated(Atomic_Vne))then
        allocate (Atomic_Vne(Natoms))
      else
        deallocate (Atomic_Vne)
        allocate (Atomic_Vne(Natoms))
      end if

      Vne_total=ZERO
      Atomic_Vne=ZERO
      if(NAIMprint.eq.0)then ! If no atoms specified, do them all!
        NAIMprint=Natoms
        do Iatom=1,Natoms
          AIMprint(Iatom)=Iatom
        end do ! Iatom
      end if

      write(UNIout,'(a)')'Atom           Vne'
      do IAIM=1,NAIMprint
      Iatom=AIMprint(IAIM)
      Znum=CARTESIAN(Iatom)%Atomic_number
      if(Znum.le.0)cycle
      Ifound=GRID_loc(Znum)
      NApts_atom=NApoints_atom(Ifound)
      write(UNIout,'(a,i8)')'Number of angular points: ',NApts_atom

      allocate(grid_points(1:NApts_atom))
      allocate(charge(NApts_atom))
      allocate(WeightsA(NApts_atom))
!
! Moving grid points on cartesian coordinate grid
        do IApoint=1,NApts_atom
          grid_points(IApoint)%X=Egridpts(Ifound,IApoint)%X+CARTESIAN(Iatom)%X
          grid_points(IApoint)%Y=Egridpts(Ifound,IApoint)%Y+CARTESIAN(Iatom)%Y
          grid_points(IApoint)%Z=Egridpts(Ifound,IApoint)%Z+CARTESIAN(Iatom)%Z
          grid_points(IApoint)%w=Egridpts(Ifound,IApoint)%w
        end do ! IApoint

!     if(Natoms.gt.1)then
        call GET_weights (grid_points, NApts_atom, Iatom, WeightsA)
!     end if
      call GET_density (grid_points, NApts_atom, charge)
      call Vne_GRID

      Atomic_Vne(Iatom)=-Atomic_Vne(Iatom)
      Vne_total = Vne_total + Atomic_Vne(Iatom)
      write(UNIout,'(i8,1x,f14.6)')Iatom,Atomic_Vne(Iatom)
      deallocate(grid_points)
      deallocate(charge)
      deallocate(WeightsA)
      end do ! Iatom
!
      write(UNIout,'(a,5x,f14.6)')'Vne_total: ',Vne_total

! End of routine ENERGY_VNE_NI
      call PRG_manager ('exit', 'ENERGY_VNE_NI', 'UTILITY')
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine Vne_GRID
!****************************************************************************************
!     Date last modified: December 31, 2015                                             *
!     Authors: Ahmad I. Alrawashdeh                                                     *
!     Description: Calculate the velcoity at a point (Xpt, Ypt, Zpt)                    *
!****************************************************************************************
! Modules:

      implicit none

!
! Local scalars:
      integer :: IApoint,Jatom,ZnumJ
      double precision :: temp
!
! Local functions:
      double precision :: TraceAB
!
! Local arrays:
      double precision, dimension(:), allocatable :: Pcharge

! Begin: 
      allocate (Pcharge(NApts_atom))

      Pcharge(1:NApts_atom) = ZERO
      do IApoint=1,NApts_atom
        do Jatom=1,Natoms
          ZnumJ = CARTESIAN(Jatom)%Atomic_number
          if(ZnumJ.le.0)cycle
          temp = dble(ZnumJ)*charge(IApoint)/    &
          dsqrt((grid_points(IApoint)%X-CARTESIAN(Jatom)%X)**2 + &
                (grid_points(IApoint)%Y-CARTESIAN(Jatom)%Y)**2 + &
                (grid_points(IApoint)%Z-CARTESIAN(Jatom)%Z)**2)
          Pcharge(IApoint) = Pcharge(IApoint) + temp
        end do ! Jatom
      end do ! IApoint

      do IApoint = 1,NApts_atom
        Pcharge(IApoint) = Pcharge(IApoint)*WeightsA(IApoint)*grid_points(IApoint)%w
      end do

      call NI_Atomic_Property (Iatom, Ifound, Atomic_Vne, Natoms, Pcharge)


      deallocate(Pcharge)
!
! End of routine Vne_GRID
      return
      end subroutine Vne_GRID

! END CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine ENERGY_VNE_NI
