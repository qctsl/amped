      subroutine VELOCITIES_NI
!****************************************************************************************
!     Date last modified: December 31, 2015                                             *
!     Authors: Ahmad I. Alrawashdeh                                                     *
!     Description: Calculate the velcocity numerically                                  *
!****************************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE module_grid_points

      implicit none
!
! Local scalars:
      integer :: Iatom,Ipoint,IRegion,Znum,Ifound,NRpoints
      double precision :: XApt,YApt,ZApt,WApt,Bweight
      double precision :: Vx,Vy,Vz,TVx,TVy,TVz
      double precision :: rho
! Local array:
      double precision, dimension(:), allocatable :: dAOprodX,dAOprodY,dAOprodZ
!
! Begin:
      call PRG_manager ('enter', 'VELOCITIES_NI', 'VELOCITIES%NUMERICAL')
!
      call GET_object ('GRID', 'RADIAL', RADIAL_grid)
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)
      call BLD_BeckeW_XI
!
      allocate (dAOprodX(1:MATlen))
      allocate (dAOprodY(1:MATlen))
      allocate (dAOprodZ(1:MATlen))

      TVx=ZERO
      TVy=ZERO
      TVz=ZERO
!
      do Iatom=1,Natoms
      Znum=CARTESIAN(Iatom)%Atomic_number
      if(Znum.le.0)cycle
      Ifound=GRID_loc(Znum)
      NApts_atom=NApoints_atom(Ifound)
!
! Moving grid points on cartesian coordinate grid
      do Ipoint=1,NApts_atom
        XApt=Egridpts(Ifound,Ipoint)%X
        YApt=Egridpts(Ifound,Ipoint)%Y
        ZApt=Egridpts(Ifound,Ipoint)%Z
        XApt=XApt+CARTESIAN(Iatom)%X
        YApt=YApt+CARTESIAN(Iatom)%Y
        ZApt=ZApt+CARTESIAN(Iatom)%Z
        WApt=Egridpts(Ifound,Ipoint)%w
        call BeckeW1 (XApt, YApt, ZApt, Iatom, Bweight)
        call GET_rho_Atom (XApt, YApt, ZApt, rho)  ! Used for Ecorr and Vpotl
        call Velocity_GRID

        TVx=TVx+Vx
        TVy=TVy+Vy
        TVz=TVz+Vz
      end do ! Ipoint
      end do ! Iatom
!
      TVx=TVx*FourPi
      TVy=TVy*FourPi
      TVz=TVz*FourPi
      write(UNIout,'(a,f14.6)')'TVx: ',TVx
      write(UNIout,'(a,f14.6)')'TVy: ',TVy
      write(UNIout,'(a,f14.6)')'TVz: ',TVz

      deallocate (dAOprodX)
      deallocate (dAOprodY)
      deallocate (dAOprodZ)
!
! End of routine VELOCITIES_NI
      call PRG_manager ('exit', 'VELOCITIES_NI', 'VELOCITIES%NUMERICAL')
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine Velocity_GRID
!****************************************************************************************
!     Date last modified: December 31, 2015                                             *
!     Authors: Ahmad I. Alrawashdeh                                                     *
!     Description: Calculate the velcoity at a point (Xpt, Ypt, Zpt)                    *
!****************************************************************************************
! Modules:

      implicit none

! Local functions:
      double precision :: TraceAB
!
! Begin: 
      call Velocity_Gaussian_prod (XApt, YApt, ZApt, dAOprodX, dAOprodY, dAOprodZ, MATlen)
      Vx=TraceAB (PM0, dAOprodX, NBasis)
      Vx=Vx*Bweight*WApt
      Vy=TraceAB (PM0, dAOprodY, NBasis)
      Vy=Vy*Bweight*WApt
      Vz=TraceAB (PM0, dAOprodZ, NBasis)
      Vz=Vz*Bweight*WApt
!
! End of routine Velocity_GRID
      return
      end subroutine Velocity_GRID

! END CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine VELOCITIES_NI
