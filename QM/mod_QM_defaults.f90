      MODULE QM_defaults
!*****************************************************************************************************************
!     Date last modified: March 6, 2000                                                             Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Work arrays and storage related arrays and scalars for 2e integrals.                          *
!*****************************************************************************************************************
      implicit none
!
      character(len=8) :: Wavefunction ! Wavefunction theories only 
      character(len=8) :: Level_of_Theory  ! All theories (DFT and Wavefunction)
      character(len=16) :: Basis_set_name
      character(len=8) :: correlation ! particularly useful for 2RDM calculations

!     integer, dimension(:), allocatable :: porb_list
      double precision :: OEPXYZ(3)
      logical :: LOEPXYZ_defined ! Defined?

! I2E defaults:
      double precision :: I2E_ACC         ! integral accuracy, integrals .le.I2E_ACC are not kept
      double precision :: I2E_ACCF        ! Final integral accuracy, i.e., at end of optimization
! IDFCLC
      double precision :: I2E_EXPCUT                       ! Size of exponent which will give a zero
      double precision :: I2E_PQCUT1
      double precision :: I2E_PQCUT2
! ISPCLC
      double precision :: ERROR1                       ! integral accuracy
      double precision :: ERROR2                       ! integral accuracy
      double precision :: VAR1                       ! integral accuracy
      double precision :: VAR2                       ! integral accuracy
!
      logical :: integral_combinations                 ! use combinations? (Closed shell only)
      logical :: Lpurifyp                            ! Transform to pure P's

      integer(kind=8) :: I2EBUF                        ! Buffer size for INCORE arrays (can be quite large)
      character(len=8) :: IIII_store,IJJL_store,IJKJ_store,IIKL_store,IJKL_store ! INCORE,ONDISK or DIRECT
      character(len=16) I2E_type
      logical :: LI2ESP,LI2EDF   ! controls routines used for integral evaluation
      logical :: LdandC          ! true if Divide and Conquer
      logical :: LPFockOP        ! true if Projecting the Fock operator
      integer :: MAX_transform !  
      integer :: NFrozen_core                    ! Number of frozen core
      integer :: NFrozen_virt                    ! Number of frozen virtuals
!
! Initial guess defaults:
      double precision :: CORE_scale        ! Scale Huckel matrix element (core orbitals)
      double precision :: VAL_scale         ! Scale Huckel matrix element (valence orbitals)
      logical :: GUESS_previous             ! True if a previous MOs  or Fock matrix exist (any basis set)
      logical :: GUESS_Lscale               ! Use defaults if .true., use menu specified values if .false.
      character(len=16) :: GUESS_type       ! EXtended_Huckel, Projected MO,, Projected Fock matrix
      character(len=16) :: PREVIOUS_BASIS   ! previous basis set (if any)
      character(len=16) :: GUESS_from_basis  ! Project from specified basis set
      character(len=128) :: BASIS_project  ! Basis set projecting from

! RHF defaults:
      integer :: SCFITN                ! Maximum number of iterations
      integer :: NPorbitals            ! The number of p-orbitals (atoms only)
      double precision :: SCFACC       ! Convergence required for the density matrix
      double precision :: SCFDMP          ! Damping parameter
      double precision :: ShiftLevel    ! Fock matrix shift level
      logical :: LShift     ! true if ShiftLevel is set
      logical :: LDirect_SCF     ! true if Direct SCF
      logical :: LSCF_converged     ! true if SCF has converged (may not belong here)
      logical :: LOptimization   ! Optimization?
      logical :: LDandC_menu           ! Divide and Conquer SCF
      logical :: LPFockOP_menu         ! Use projected Fock operator
      logical :: LGMatrix_dump         ! Dump the G matrix (2e part of the Fock matrix)
      character(len=16) :: EXTRAPOLATION_MET   !
!
! MOM defaults:
      logical :: LMOM
      integer :: MOMstart
!
! MO read defaults
      logical :: LMOread 
!
! ROHF/GVB defaults:
      integer :: NVAL                     ! Number of valence
      integer :: NDOUBL                   ! Number of doubles
      integer :: NOPEN                    ! Number of open shell
      integer :: Ncore                    ! Number of open shell
      integer :: NCONF                    ! Number of configurations

      integer :: MAXPAR                   ! Number of pairs
      double precision :: ORBACC       ! Convergence required for the density matrix
      double precision :: SCALST
      double precision :: GVB_COEFF
      character(len=8) :: ROTMET
      character(len=8) :: ROHFGMO
      character(len=8) :: ROHFPPT
      character(len=8) :: GVB_guessMO ! Guess MO (HMO, CMO or LMO)

! GSCF defaults:
!! ALREDY DEFINED !!
!      integer :: NOPEN                    ! Number of open shell
!      integer :: NCore                    ! Number of core shell
!      character(len=8) :: ROTMET

!
! MP2
      logical :: MP2_full                            ! Full MP2?
!
! Gradient defaults:
      double precision :: GRD_EXPCUT       !
      double precision :: GRD_ACC       !
      double precision :: GRD_PQCUT1
      double precision :: GRD_PQCUT2
      double precision :: GRD_PQCUT3
      double precision :: VTOL1
      double precision :: VTOL2
      double precision :: VTOLS
      logical :: LG2ESP
      logical :: LG2EDF
      logical :: LGRDBA
      logical :: Lfixed_G2E
!
! LMO
      integer :: NUMLMO
      integer :: MXBLIT

! Thermodynamics:
      double precision :: Temperature ! Kelvin
      double precision :: Pressure ! 
!
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      function NFrozen_cores() result(NFcore)
!***********************************************************************
!     Date last modified: June 25, 1992                                *
!     Author: R.A. Poirier                                             *
!     Description: Returns the number of core MO.                      *
!***********************************************************************
! Modules:
      USE type_molecule

      implicit none
!
! Local scalars:
      integer :: Iatom,NFcore,ZA
!
! Begin:
      call PRG_manager ('enter', 'NFrozen_cores', 'UTILITY')
!
      NFcore=0
      do Iatom=1,Natoms
        ZA=CARTESIAN(Iatom)%Atomic_number
        IF(ZA.GT.54) then
          NFcore=NFcore+36
        else if(ZA.GT.36) then
          NFcore=NFcore+18
        else if(ZA.GT.18) then
          NFcore=NFcore+9
        else if(ZA.GT.10) then
          NFcore=NFcore+5
        else if(ZA.GT.2) then
          NFcore=NFcore+1
        end if
      end do ! Iatom
!
! End of routine NFrozen_cores
      call PRG_manager ('exit', 'NFrozen_cores', 'UTILITY')
      return
      end function NFrozen_cores
      end MODULE QM_defaults
