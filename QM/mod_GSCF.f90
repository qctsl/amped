      MODULE GSCF_work
!*****************************************************************************************************************
!     Date last modified: February 16, 2000                                                         Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Used by the GVB and ROHF for work arrays and scalars.                                         *
!*****************************************************************************************************************
! Modules:

      implicit none
!
      integer :: Icut,Jcut
      logical :: ROTMIX
      double precision :: CCscale
      double precision :: SLIMDN
      double precision :: FACTDN
! Arrays: These are initialized by the menu (SET_GVB)
      integer, dimension(:), allocatable :: ICONF   ! CONFIGURATION_LIST(Nbasis)
      integer, dimension(:), allocatable :: IJNDX           ! indexing array, must be dimension Nbasis+1
      double precision, dimension(:), allocatable :: ALPHCC  !
      double precision, dimension(:), allocatable :: BETACC  !
      double precision, dimension(:), allocatable :: OCC  !
      double precision, dimension(:,:), allocatable :: EJKN
!
      double precision, dimension(:,:), allocatable :: JcoulN    !
      double precision, dimension(:,:), allocatable :: KexchN    !
      double precision, dimension(:), allocatable :: PM0D
      double precision, dimension(:,:), allocatable :: PMN
      double precision, dimension(:,:), allocatable :: CMOC
      double precision, dimension(:,:), allocatable :: SCRMAT
      double precision, dimension(:), allocatable :: SCRVEC
      double precision, dimension(:,:), allocatable :: SQSM
      integer, dimension(:,:), allocatable :: MOTB
      double precision, dimension(:,:), allocatable :: SYMO
      double precision, dimension(:,:), allocatable :: SCMO
!
      end MODULE GSCF_work
