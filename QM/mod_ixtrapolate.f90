    module ixtrapolate
!*********************************************************************************************
!     Date last modified: January 11, 2005                                                   *
!     Author: R.A. Poirier                                                                   *
!     Description: Inter- and Extra-polation scalars and arrays.                             *
!*********************************************************************************************
! Modules:
      USE program_constants
      USE QM_defaults

      implicit none
!
      integer :: IXpolation_count
      integer :: IDEWIT
      double precision :: DCRIT
      double precision :: X1
      double precision :: X2
      double precision :: Y1
      double precision :: PREV_SCFCON   ! Previous SCF converged (to check for divergence)
      logical :: converged                               ! true when converged
      logical :: Lextrapolated                            ! true if extrapolated
      logical :: SWITCH                                  ! switch extroplation method?
      character(len=8) :: LABEL                           ! label for printing at scf cycle
!
! Work arrays (extrapolation):
      double precision, dimension(:), allocatable :: PnM0
      double precision, dimension(:), allocatable :: PnM1
      double precision, dimension(:), allocatable :: extra
      double precision, dimension(:), allocatable :: delta
      double precision, dimension(:), allocatable :: PDnM1
      double precision, dimension(:), allocatable :: PDnM2
      double precision, dimension(:), allocatable :: PDnM3
!     double precision, dimension(:), pointer :: PnM0=>null()  ! current matrix P
!     double precision, dimension(:), pointer :: PnM1=>null()  ! Previous matrix P
!     double precision, dimension(:), pointer :: extra=>null() ! extrapolated matrix P
!     double precision, dimension(:), pointer :: delta=>null() ! Change in density matrix
!     double precision, dimension(:), pointer :: PDnM1=>null() ! P0-P1
!     double precision, dimension(:), pointer :: PDnM2=>null() ! P1-P2
!     double precision, dimension(:), pointer :: PDnM3=>null() ! P2-P3

!
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine Xpolation (SCFACC, &
                            SCFCON, &
                            Nbasis)
!*****************************************************************************************************************
!     Date last modified: January 11, 2005                                                          Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Inter- and Extra-polation routine.  Also checks convergence.                                  *
!     M.J.S. Dewar and P.K. Weiner, Comp. & Chem., 2, 31 (1978), with changes from                               *
!     P. Badziag and D.F. Solms, Comp. & Chem., 12, 233 (1988).                                                  *
!*****************************************************************************************************************
      implicit none
!
! Input scalar:
      integer :: Nbasis
      double precision :: SCFACC
!
! Output scalars:
      double precision :: SCFCON
!
! Input arrays:
!
! Local scalars:
      integer IERROR,MATlen
      double precision :: COSPHI,COSPSI,DP1,DP2,DP3,SP11,SP12,SP13,SP22,SP23,SP33,X,C1,C2,XY,Y,Y2
      logical SWITCH
!
! Local function:
      double precision TRACLO
!
! Local parameters:
      double precision FOUR,ONEPT9,PT05,PT1,PT5,PT99,PT995,TEN
      double precision MAX_increase ! Jan. 18, 2005
      parameter (PT05=5.0D-2,PT1=0.1D0,PT5=0.5D0,PT99=0.99D0,PT995=0.995D0,ONEPT9=1.9D0,FOUR=4.0D0,TEN=10.0D0)
      parameter (MAX_increase=.01D0)
!
! Begin:
      MATlen=size(PnM0) ! NOTE for UHF MATlen=2*Nbasis*(Nbasis+1)/2
      converged=.false.
      Lextrapolated=.false.
      LABEL='        '
      IXpolation_count=IXpolation_count+1
!
      PDnM1(1:MATlen)=PnM0(1:matlen)-PnM1(1:matlen)
      SP11=TRACLO (PDnM1, PDnM1, Nbasis, MATlen)
      DP1=DSQRT(SP11)                          ! Find displacements DP1
!
! Test for convergence by finding if the root mean square DP is less than SCFCON.
      SCFCON=DP1/DBLE(Nbasis)
      if(SCFCON.LT.SCFACC)then
        delta=PDnM1
        converged=.true.
        Lextrapolated=.false.
        return
      end if

! If the convergence is increasing, then do a Dewar extrapolation on this iteration
!     if((SCFCON-PREV_SCFCON).GE.MAX_increase.and.IXpolation_count.gt.2)then  
!       EXTRAPOLATION_MET='DEWAR '
!     end if
      PREV_SCFCON=SCFCON
!
      METHOD: select case (EXTRAPOLATION_MET)
!***********************************************************************
      case ('3,4 POINT') METHOD !
      delta(1:MATlen)=PnM0(1:matlen)-extra(1:matlen)  ! Calculate displacement vector delta
      COUNT1: select case (IXpolation_count)

      case(1) COUNT1
        PDnM2=PDnM1
        PnM1=PnM0
        extra=PnM0

      case(2) COUNT1
        PDnM3=PDnM2
        PDnM2=PDnM1
        PnM1=PnM0
        extra=PnM0

      case default COUNT1
        SP12=TRACLO (PDnM2, PDnM1, Nbasis, MATlen)
        SP13=TRACLO (PDnM3, PDnM1, Nbasis, MATlen)
        SP22=TRACLO (PDnM2, PDnM2, Nbasis, MATlen)
        SP23=TRACLO (PDnM3, PDnM2, Nbasis, MATlen)
        SP33=TRACLO (PDnM3, PDnM3, Nbasis, MATlen)
        DP2=DSQRT(SP22)
        DP3=DSQRT(SP33)
        COSPHI=SP12/(DP1*DP2)          ! Find cosine of angle between successive displacements.
        X=(SP13*SP22-SP12*SP23)/(SP11*SP22-SP12*SP12)
        Y=(SP23*SP11-SP12*SP13)/(SP11*SP22-SP12*SP12)
        COSPSI=DSQRT(X*X*SP11+Y*Y*SP22+TWO*X*Y*SP12)/DP3  ! Find cosine of angle between DP(3) and plane of DP(1) and DP(2).
!
        if(COSPSI.gt.PT99)then ! Do not extrapolate unless 4 consecutive points are nearly coplanar.
        Y=-Y/X
        X=ONE/X
! Test if 2x2 matrix has real eigenvalues between -.95 and +.95.
        XY=Y*Y+FOUR*X
        if(XY.GT.ZERO)then
          XY=DABS(Y)+DSQRT(XY)
          if(XY.GT.ONEPT9)then                  ! If 4-point extrapolation is not possible, try 3-point.
            if(DABS(COSPHI).LE.PT995)GO TO 200
            C1=DP1/(DP2*COSPHI-DP1)
            extra(1:MATlen)=extra(1:matlen)+delta(1:matlen)+C1*PDnM1(1:matlen)
            delta(1:MATlen)=delta(1:matlen)+C1*PDnM1(1:matlen)
            LABEL=' 3-POINT'
            Lextrapolated=.true.
          else                                  ! 4-point
            C1=X/(ONE-X-Y)
            C2=(X+Y)/(ONE-X-Y)
            extra(1:MATlen)=extra(1:matlen)+delta(1:matlen)+ &
                                    C1*PDnM2(1:MATlen)+C2*PDnM1(1:matlen)
            delta(1:MATlen)=delta(1:matlen)+C1*PDnM2(1:matlen)+C2*PDnM1(1:matlen)
            LABEL=' 4-POINT'
            Lextrapolated=.true.
          end if
          IXpolation_count=0
          return
        end if ! XY.GT.ZERO
        end if ! COSPSI.gt.PT99
!
  200   PDnM3=PDnM2
        PDnM2=PDnM1
        PnM1=PnM0
        extra=PnM0
      end select COUNT1
      return
!
!***********************************************************************
      case('DEWAR') METHOD ! Dewar extrapolation
        delta(1:MATlen)=PnM0(1:matlen)-extra(1:matlen)
      COUNT2: select case (IXpolation_count)
      case(1,2) COUNT2
        PDnM3=PDnM2
        PDnM2=PDnM1
        PnM1=PnM0
        extra=PnM0
      case default COUNT2
!
! IDEWIT controls the extrapolation procedure:
!   0       - Just initialize, no extrapolation.
!   1 and 2 - Allow two extrapolations with same value of Y1.
      if(IDEWIT.NE.0)then
      C1=X2-ONE
      extra(1:MATlen)=extra(1:matlen)+delta(1:matlen)+C1*PDnM1(1:matlen)
      delta(1:MATlen)=delta(1:matlen)+C1*PDnM1(1:matlen)
      LABEL=' DEWAR-1'
      Lextrapolated=.true.
!
      if(IDEWIT.LT.2)then
        IDEWIT=IDEWIT+1
        PDnM2=PDnM1
        PnM1=PnM0
        return
      end if
!
      Y2=Y1
      end if ! IDEWIT.NE.0

      SP22=TRACLO (PDnM2, PDnM2, Nbasis, MATlen)
      SP12=TRACLO (PDnM2, PDnM1, Nbasis, MATlen)
      Y1=SP12/SP22
!
      if(IDEWIT.GT.0)then
! Extrapolate if 2 successive Lambda's agree within DCRIT.
        if(DABS(Y1-Y2)/DMAX1(DABS(Y2),DABS(Y1)).GE.DCRIT)GO TO 355
        X1=ONE/(ONE-Y1)
        if(X1.LT.ZERO.or.X1.GT.ONE)GO TO 330
        C1=X1-ONE
        extra(1:MATlen)=extra(1:matlen)+C1*PDnM1(1:matlen)
        delta(1:MATlen)=delta(1:matlen)+C1*PDnM1(1:matlen)
        PDnM1(1:MATlen)=X1*PDnM1(1:matlen)
        LABEL=' DEWAR-2'
        Lextrapolated=.true.
      end if ! IDEWIT.GT.0
      Y1=X1*Y1
  330 X2=X1*X2
      if(X2.LT.ZERO.or.X2.GT.ONE)X2=TWO*X2/(ONE+X2)
!
      if(IDEWIT.EQ.0)then
        PnM1=PnM0
        extra=PnM0
        GO TO 360
      end if

      PDnM2=PDnM1
  355 PnM1=extra
  360 IDEWIT=1
      IXpolation_count=2
      end select COUNT2
!
!***********************************************************************
      case('NONE') METHOD
        PDnM3=PDnM2
        PDnM2=PDnM1
        delta=PDnM1
        PnM1=PnM0
        return
      end select METHOD

! July / 97
      if(SCFCON.LT.5.0D-03)then ! In case using Dewar
        EXTRAPOLATION_MET='3,4 POINT '
      end if

      return
      end subroutine Xpolation

      subroutine END_Xpolation
!*****************************************************************************************************************
!     Date last modified: January 11, 2005                                                          Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Terminates the Inter- and Extra-polation routine.                                             *
!*****************************************************************************************************************
      implicit none
!
! Deallocate the work arrays
      deallocate (PDnM1, PDnM2, PDnM3, extra, delta, PnM0, PnM1)

      return
      end subroutine END_Xpolation

      subroutine INI_Xpolation (PM0_RHF, MATlen)
!*****************************************************************************************************************
!     Date last modified: January 11, 2005                                                          Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Initialize the Inter- and Extra-polation routine.                                             *
!*****************************************************************************************************************
      implicit none
!
! Input scalar:
      integer :: MATlen
      double precision, target :: PM0_RHF(MATlen)
!
! Local scalars:
      integer :: IERROR

      LABEL='        '
      SWITCH=.false.
      IXpolation_count=0
      IDEWIT=0
      DCRIT=0.1D0
      X1=ONE
      X2=ONE
      Y1=ONE
      PREV_SCFCON=999.99D0
      Lextrapolated=.true. ! Prevents printing convergence on first iteration

! Work arrays (once Nbasis/MATlen defined):
      allocate (PDnM1(MATlen), PDnM2(matlen), PDnM3(matlen)) 
      allocate (PnM0(matlen), PnM1(matlen), extra(matlen), delta(MATlen))

      PnM1=PM0_RHF
      extra=PM0_RHF
      PDnM1(1:MATlen)=ZERO
      PDnM2(1:MATlen)=ZERO
      PDnM3(1:MATlen)=ZERO

      return
      end subroutine INI_Xpolation

      subroutine INI_Xpolation_UHF (PM0_alpha, PM0_beta, MATlen)
!*****************************************************************************************************************
!     Date last modified: January 11, 2005                                                          Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Initialize the Inter- and Extra-polation routine.                                             *
!*****************************************************************************************************************
      implicit none
!
! Input scalar:
      integer :: MATlen
      double precision :: PM0_alpha(MATlen),PM0_beta(MATlen)
!
! Local scalars:
      integer :: IERROR

      LABEL='        '
      SWITCH=.false.
      IXpolation_count=0
      IDEWIT=0
      DCRIT=0.1D0
      X1=ONE
      X2=ONE
      Y1=ONE
      PREV_SCFCON=999.99D0
      Lextrapolated=.true.

! Work arrays (once Nbasis/MATlen defined):
      if(.not.allocated(extra))then
        allocate (PDnM1(2*MATlen), PDnM2(2*MATlen), PDnM3(2*MATlen), &
                  PnM0(2*MATlen), PnM1(2*MATlen), extra(2*MATlen), &
                  delta(2*MATlen))
      end if


      PnM1(1:MATlen)=PM0_alpha(1:MATlen)
      PnM1(MATlen+1:2*MATlen)=PM0_beta(1:MATlen)
      extra=PnM1
      delta(1:2*MATlen)=ZERO
      PDnM1(1:2*MATlen)=ZERO
      PDnM2(1:2*MATlen)=ZERO
      PDnM3(1:2*MATlen)=ZERO

      return
      end subroutine INI_Xpolation_UHF

      subroutine CHECK_convergence (PM0, PM1, MATlen, Nbasis, SCFCON)
!*****************************************************************************************************************
!     Date last modified: January 11, 2005                                                          Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Initialize the Inter- and Extra-polation routine.                                             *
!*****************************************************************************************************************
      implicit none
!
! Input scalar:
      integer :: Nbasis
      integer :: MATlen
!
! Input arrays:
      double precision :: PM0(MATlen),PM1(MATlen)
!
! Output scalars:
      double precision :: SCFCON
!
! Local scalars:
      double precision :: DP1,SP11
!
! Local array:
      double precision :: DPM(MATlen)
!
! Local function:
      double precision TRACLO
!
! Begin:
      converged=.false.
!
      DPM(1:MATlen)=PM0(1:MATlen)-PM1(1:MATlen)
      SP11=TRACLO (DPM, DPM, Nbasis, MATlen)
      DP1=DSQRT(SP11)                          ! Find displacements DP1
!
! Test for convergence by finding if the root mean square DP is less than SCFCON.
      SCFCON=DP1/DBLE(Nbasis)
      if(SCFCON.LT.SCFACC)then
        converged=.true.
      end if

      return
      end subroutine CHECK_convergence

      end module ixtrapolate
