      subroutine SET_defaults_THERMO
!*****************************************************************************************************************
!     Date last modified: March 31, 2000                                                            Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Set code defaults.                                                                            *
!*****************************************************************************************************************
! Modules:
      USE QM_defaults

      implicit none
!
! Begin:
! Thermodynamics:
      Temperature=298.150D0 ! Kelvin
      Pressure=101325.0D0   ! Pascal (Pa)

! End of routine SET_defaults_THERMO
      return
      end subroutine SET_defaults_THERMO
      subroutine MENU_thermodynamics
!****************************************************************************
!     Date last modified: July 17,2004                                      *
!     Author: R.A. Poirier                                                  *
!     Description: Set up defaults for calculating thermodynamic properties *
!                  and computer them.                                       *
!****************************************************************************
! Modules:
      USE program_parser
      USE QM_defaults
      USE menu_gets
      USE type_elements

      implicit none
!
! Local scalars:
      logical done
!
! Begin:
      call PRG_manager ('enter', 'MENU_thermodynamics', 'UTILITY')
      done=.false.
!
! Menu:
      do while (.not.done)
      call new_token ('Thermodynamics:')
!
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      ' Command Thermodynamics:', &
      '  Purpose: Set Thermodynamic parameters ', &
      '  Syntax:', &
      '  THErmodynamics', &
      '  Temperature =  <real*8>: Temperature used for thermodynamic properties', &
      '  end'
!
      else if(token(1:1).EQ.'T' )then
        call GET_value (Temperature)

      else
        call MENU_end (done)

      end if
!
      end do !(.not.done)
!
      call THERMOdynamics
!
! End of routine MENU_thermodynamics
      call PRG_manager ('exit', 'MENU_thermodynamics', 'UTILITY')
      return
      end
