      subroutine GRADIENTS_COMPONENTS
!***********************************************************************************
!     Date last modified: May 10, 1995                                 Version 1.0 *
!     Author: R.A. Poirier                                                         *
!     Description: Calculate gradients over cartesians in A.U.(Hartree/bohr)       *
!***********************************************************************************
! Modules:
      USE program_constants
      USE gradients
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE matrix_print

      implicit none
!
! Local scalars:
!
! Begin:
      call PRG_manager ('enter', 'GRADIENTS_COMPONENTS', 'GRADIENTS%COMPONENTS')
!
! Object:
      if(.not.allocated(Gradient))then
        allocate (Gradient(3*Natoms))
      else
        deallocate (Gradient)
        allocate (Gradient(3*Natoms))
      end if
!
      TXX=ZERO
      VNEXX=ZERO
      VEEXX=ZERO
      VNNXX=ZERO
      Gradient(1:3*Natoms)%NEOPR=ZERO
      Gradient(1:3*Natoms)%NEWFN=ZERO
      Gradient(1:3*Natoms)%WFN=ZERO
      Gradient(1:3*Natoms)%COEFF=ZERO
      Gradient(1:3*Natoms)%EEWFN=ZERO
      Gradient(1:3*Natoms)%NUCLEAR=ZERO
!
! Retrieve appropriate density matrices ....
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)
      call GET_object ('QM', 'DENSITY_ENERGY_WEIGHTED', Wavefunction)

      call GNUCLC
! Need a better way, should require OPT_defaults
        call g1emol
        if(LG2EDF)then
          call gdfclc ! two electron contributions (GEEWFN)
        else ! for now
          call gspclc  ! two electron contributions (GEEWFN)
        end if
!
! End of routine GRADIENTS_COMPONENTS
      call PRG_manager ('exit', 'GRADIENTS_COMPONENTS', 'GRADIENTS%COMPONENTS')
      return
      END
      subroutine GRADIENTS_XYZ
!***********************************************************************************
!     Date last modified: May 10, 1995                                 Version 1.0 *
!     Author: R.A. Poirier                                                         *
!     Description: Calculate gradients over cartesians in A.U.(Hartree/bohr)       *
!***********************************************************************************
! Modules:
      USE program_constants
      USE gradients
      USE type_molecule

      implicit none
!
! Local scalars:
      integer Iatom,J,K
!
! Begin:
      call PRG_manager ('enter', 'GRADIENTS_XYZ', 'GRADIENTS%XYZ')
!
! Object:
      if(.not.allocated(GRDXYZ))then
        allocate (GRDXYZ(3*Natoms))
      else
        deallocate (GRDXYZ)
        allocate (GRDXYZ(3*Natoms))
      end if

      call GET_object ('MOL', 'GRADIENTS', 'COMPONENTS')
!
! Geometry derivatives: do NOT Convert to MDyne.
      K=0
      do Iatom=1,Natoms
      do J=1,3
        K=K+1
        GRDXYZ(K)=Gradient(K)%NEOPR+Gradient(K)%NEWFN+Gradient(K)%WFN+ &
                  Gradient(K)%EEWFN+Gradient(K)%COEFF+Gradient(K)%NUCLEAR
      end do ! J
      end do ! I

! These can be deallocated, although an object
      deallocate (Gradient)
!
! End of routine GRADIENTS_XYZ
      call PRG_manager ('exit', 'GRADIENTS_XYZ', 'GRADIENTS%XYZ')
      return
      END
      subroutine GRADIENTS_ZM
!**********************************************************************************************
!     Date last modified: September 20, 2001                                      Version 2.0 *
!     Author: R. A. Poirier                                                                   *
!     Description: Transform gradients from Cartesian to Z-Matrix Internal Coordinates(ZM). *
!**********************************************************************************************
! Modules
      USE gradients
      USE type_molecule

      implicit none
!
! Local scalars:
!
! Begin:
      call PRG_manager ('enter', 'GRADIENTS_ZM', 'GRADIENTS%ZM')
!
      call GET_object ('MOL', 'GRADIENTS', 'XYZ')
      call GET_object ('MOL', 'BMATRIX', 'ZM')
!
! Object:
      if(.not.allocated(GRD_ZM))then
        allocate (GRD_ZM(NICOOR))
      else
        deallocate (GRD_ZM)
        allocate (GRD_ZM(NICOOR))
      end if

      Bmatrix=>Bmatrix_ZM
      GRD_int=>GRD_ZM

      call GRD_XYZ_to_INT
!
! End of routine GRADIENTS_ZM
      call PRG_manager ('exit', 'GRADIENTS_ZM', 'GRADIENTS%ZM')
      return
      END
      subroutine GRADIENTS_PIC
!**********************************************************************************************
!     Date last modified: September 20, 2001                                      Version 2.0 *
!     Author: R. A. Poirier                                                                   *
!     Description: Transform gradients from Cartesian to Proper Internal Coordinates(PIC). *
!**********************************************************************************************
! Modules
      USE program_files
      USE gradients
      USE type_molecule
      USE proper_internals

      implicit none
!
! Begin:
      call PRG_manager ('enter', 'GRADIENTS_PIC', 'GRADIENTS%PIC')
!
      call GET_object ('MOL', 'GRADIENTS', 'XYZ')
      call GET_object ('MOL', 'BMATRIX', 'PIC')
!
! Object:      
      if(PIC%Ncoordinates.eq.0)then
        write(UNIout,'(a)')'No Proper internal coordinates were define: Define them using the MENU'
        return
!       stop 'No Proper internal coordinates were define: Define them using the MENU'
      end if

      if(.not.allocated(GRD_PIC))then
        allocate (GRD_PIC(NICOOR))
      else
        deallocate (GRD_PIC)
        allocate (GRD_PIC(NICOOR))
      end if

      Bmatrix=>Bmatrix_PIC
      GRD_int=>GRD_PIC

      call GRD_XYZ_to_INT
!
! End of routine GRADIENTS_PIC
      call PRG_manager ('exit', 'GRADIENTS_PIC', 'GRADIENTS%PIC')
      return
      END
      subroutine GRADIENTS_RIC
!**********************************************************************************************
!     Date last modified: September 20, 2001                                      Version 2.0 *
!     Author: R. A. Poirier                                                                   *
!     Description: Transform gradients from Cartesian to Redundant Internal Coordinates(RIC). *
!**********************************************************************************************
! Modules
      USE program_constants
      USE gradients
      USE type_molecule

      implicit none
!
! Local scalars:
      integer I,J,Natoms3
      double precision T
!
! Work arrays:
      double precision, dimension(:,:), allocatable :: Wmatrix
!
! Begin:
      call PRG_manager ('enter', 'GRADIENTS_RIC', 'GRADIENTS%RIC')
!
      call GET_object ('MOL', 'GRADIENTS', 'XYZ')
      call GET_object ('MOL', 'BMATRIX', 'RIC')

! Object:
      Bmatrix=>Bmatrix_RIC
      NICOOR=size(Bmatrix,1)
      Natoms3=size(Bmatrix,2)

      if(.not.allocated(GRD_RIC))then
        allocate (GRD_RIC(NICOOR))
      else
        deallocate (GRD_RIC)
        allocate (GRD_RIC(NICOOR))
      end if

      GRD_int=>GRD_RIC

      allocate (Wmatrix(Natoms3,NICOOR))

      call BLD_Wmatrix (Bmatrix, Wmatrix, NICOOR, Natoms)  ! Wmatrix(3*Natoms,NICOOR)

      do I=1,NICOOR
        T=ZERO
        do J=1,Natoms3
          T=T+Wmatrix(J,I)*GRDXYZ(J)
        end do ! J
        GRD_int(I)=T
      end do ! I

      deallocate (Wmatrix)
!
! End of routine GRADIENTS_RIC
      call PRG_manager ('exit', 'GRADIENTS_RIC', 'GRADIENTS%RIC')
      return
      END
      subroutine GRD_XYZ_to_INT
!***********************************************************************
!     Date last modified: May 10, 1995                     Version 1.0 *
!     Author: R. A. Poirier                                            *
!     Description: Transform gradients from Cartesian to internal      *
!                  coordinates.                                        *
!     Note GRDXYZ must be converted to MDynes
!***********************************************************************
! Modules
      USE program_constants
      USE gradients
      USE type_molecule
      USE matrix_print

      implicit none
!
! Local scalars:
      integer I,IER,J,L
      double precision T
!
! Work arrays:
      double precision, dimension(:), allocatable :: BB
      double precision, dimension(:), allocatable :: FD
      double precision, dimension(:,:), allocatable :: BWORK
      double precision, dimension(:,:), allocatable :: Wmatrix
!
! Begin:
      call PRG_manager ('enter', 'GRD_XYZ_to_INT', 'UTILITY')
!
! Object:
! Note: for Z-matrix 'NPICOR' = NMCOOR
!       for PIC's    'NPICOR' = NPICOR
!
      allocate (BB(1:3*Natoms), FD(1:3*Natoms), BWORK(NICOOR,3*Natoms))

! Copy B-matrix into work matrix
      BWORK(1:NICOOR,1:3*Natoms)=Bmatrix(1:NICOOR,1:3*Natoms)
!
! Form  ((BB')**-1) * B * g_xyz.
      do I=1,NICOOR
        T=ZERO
        do J=1,3*Natoms
          BB(J)=BWORK(I,J)
          T=T+BB(J)*GRDXYZ(J)
        end do ! J
        FD(I)=T
! Accumulate another row of  G = BB'.
        do J=I,NICOOR
          T=ZERO
          do L=1,3*Natoms
            T=T+BB(L)*BWORK(J,L)
          end do ! L
          BWORK(I,J)=T
        end do ! J
      end do ! I
!
      do I=1,NICOOR
        do J=1,I
          BWORK(I,J)=BWORK(J,I)
        end do ! J
      end do ! I

      call INVERT (BWORK, NICOOR, NICOOR, IER)

      IF(IER.EQ.0)then
        do I=1,NICOOR
          T=ZERO
          do J=1,NICOOR
            T=T+BWORK(I,J)*FD(J)
          end do ! J
          GRD_int(I)=T
        end do ! I
      end if

      deallocate (BB, FD, BWORK)
!
! End of routine GRD_XYZ_to_INT
      call PRG_manager ('exit', 'GRD_XYZ_to_INT', 'UTILITY')
      return
      END
      subroutine PRT_GZmatrix
!***********************************************************************
!     Date last modified: May 10, 1995                     Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: Print gradients in Z-matrix format.                 *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE gradients

      implicit none
!
! Local scalars:
      integer I,N1,N2,N3
      double precision GLTOT
!
! Begin:
      call PRG_manager ('enter', 'PRT_GZmatrix', 'UTILITY')
!
      call GET_object ('MOL', 'GRADIENTS', 'ZM')
      write(UNIout,1080)
      I=1
      write(UNIout,1090)I,CARTESIAN(I)%Atomic_number
      I=2
      N1=1
      GLTOT=ZERO
      GLTOT=GLTOT+GRD_ZM(1)*GRD_ZM(1)
      write(UNIout,1090)I,CARTESIAN(2)%Atomic_number,Zmatrix%atoms(2)%atom1,GRD_ZM(1)*Fconv,N1
      IF(Natoms.GT.2)then
        I=3
        N1=2
        N2=Natoms
        GLTOT=GLTOT+GRD_ZM(2)*GRD_ZM(2)+GRD_ZM(N2)*GRD_ZM(N2)
        write(UNIout,1090)I,CARTESIAN(3)%Atomic_number,Zmatrix%atoms(3)%atom1,GRD_ZM(2)*Fconv,N1, &
                                                        Zmatrix%atoms(3)%atom2,GRD_ZM(N2)*FCconv,N2
      end if ! Natoms.GT.2

      IF(Natoms.GT.3)then
        N3=Natoms+Natoms-3
        do I=4,Natoms
        N1=N1+1
        N2=N2+1
        N3=N3+1
        GLTOT=GLTOT+GRD_ZM(N1)*GRD_ZM(N1)+GRD_ZM(N2)*GRD_ZM(N2)+GRD_ZM(N3)*GRD_ZM(N3)
        write(UNIout,1090)I,CARTESIAN(I)%Atomic_number,Zmatrix%atoms(I)%atom1,GRD_ZM(N1)*Fconv,N1, &
                                                        Zmatrix%atoms(I)%atom2,GRD_ZM(N2)*FCconv,N2, &
                                   Zmatrix%atoms(I)%atom3,GRD_ZM(N3)*FCconv,N3,Zmatrix%atoms(I)%atom4
        end do ! I
      end if ! Natoms.GT.3
      GLTOT=DSQRT(GLTOT/DBLE(NMCOOR))
      write(UNIout,1082)GLTOT
!
 1080 FORMAT('Internal z-matrix coordinate derivatives (MDyne OR MDyne-angstrom','/radian)'//'0  I   AN   Z1     BL', &
       18X,'Z2      ALPHA',16X,'Z3      BETA',17X,'Z4'/)
 1082 FORMAT('Gradient length (in a.u.) over all internal parameters (NO CONSTRAINTS):',1PE14.2//)
 1090 FORMAT(1X,I3,2I5,F11.6,'   (',I3,')   ',I5,F13.6,'   (',I3,')   ',I5,F13.6,'   (',I3,')   ',I5)
!
! End of routine PRT_GZmatrix
      call PRG_manager ('exit', 'PRT_GZmatrix', 'UTILITY')
      return
      END
      subroutine PRT_gradient_components
!***********************************************************************
!     Date last modified: May 10, 1995                     Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_elements
      USE type_molecule
      USE gradients

      implicit none
!
! Local scalars:
      integer I,IA,J,K
      double precision :: G1,G2,G3,G4,G5,G6,GXYZ,Total
      character*2 ISYMBOL
!
! Begin:
      call PRG_manager ('enter', 'PRT_gradient_components', 'UTILITY')
!
      call GET_object ('MOL', 'GRADIENTS', 'COMPONENTS')
! Collect total derivative in first column of respective arrays.
      Total=TXX+VNEXX+VEEXX+VNNXX
      write(UNIout,1000)TXX,VNEXX,VEEXX,VNNXX,Total
 1000 FORMAT('ENERGY TERMS:'/' KINETIC:',F17.9/' V (NE) :',F17.9/' V (EE) :',F17.9/' NUCLEAR:',F17.9/10X,16('-')/' TOTAL  :', &
       F17.9//'ATOMIC POSITION DERIVATIVE COMPONENTS (Mdynes)'/'CENTER',11X,'VNE(OPR)',7X,'VNE(WFN)',8X,'T(WFN)',8X, &
      'VEE(WFN)',8X,'COEF',10X,'NUCLEAR',9X,'TOTAL')
!
! Geometry derivatives.
      K=0
      do I=1,Natoms
        IF (CARTESIAN(I)%Atomic_number.LT.0) then
          ISYMBOL=FLOATING
        else if (CARTESIAN(I)%Atomic_number.EQ.0) then
          ISYMBOL=DUMMY
        else
          IA=CARTESIAN(I)%Atomic_number
          ISYMBOL=Element_symbols(IA)
        end if
      do J=1,3
        K=K+1
!
! Convert to MDynes for printing
        G1=Gradient(K)%NEOPR*FCONV
        G2=Gradient(K)%NEWFN*FCONV
        G3=Gradient(K)%WFN*FCONV
        G4=Gradient(K)%EEWFN*FCONV
        G5=Gradient(K)%COEFF*FCONV
        G6=Gradient(K)%NUCLEAR*FCONV
        GXYZ=G1+G2+G3+G4+G5+G6
        IF(J.LT.2) then
          write(UNIout,1010)I,ISYMBOL,G1,G2,G3,G4,G5,G6,GXYZ
        else if (J.EQ.2) then
          write(UNIout,1020)G1,G2,G3,G4,G5,G6,GXYZ
        else if (J.GT.2) then
          write(UNIout,1030)G1,G2,G3,G4,G5,G6,GXYZ
        end if
      end do ! J
      end do ! I
!
! End of routine PRT_gradient_components
      call PRG_manager ('exit', 'PRT_gradient_components', 'UTILITY')
      return
 1010 FORMAT(1x,I4,1X,A2,' X',7F15.7)
 1020 FORMAT(9X,'Y',7F15.7)
 1030 FORMAT(9X,'Z',7F15.7)
      END
      subroutine PRT_gradients_XYZ
!***********************************************************************
!     Date last modified: May 10, 1995                     Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_elements
      USE type_molecule
      USE gradients

      implicit none
!
! Local scalars:
      integer I,IA,J,K
      double precision GXYZ
      character*2 ISYMBOL
!
! Begin:
      call PRG_manager ('enter', 'PRT_gradients_XYZ', 'UTILITY')
!
      call GET_object ('MOL', 'GRADIENTS', 'XYZ')
!
      write(UNIout,1000)
!
! Geometry derivatives.
      K=0
      do I=1,Natoms
        IF (CARTESIAN(I)%Atomic_number.LT.0) then
          ISYMBOL=FLOATING
        else if (CARTESIAN(I)%Atomic_number.EQ.0) then
          ISYMBOL=DUMMY
        else
          IA=CARTESIAN(I)%Atomic_number
          ISYMBOL=Element_symbols(IA)
        end if
      do J=1,3
        K=K+1
! Convert to MDynes for printing
        GXYZ=GRDXYZ(K)*FCONV
        IF(J.LT.2) then
          write(UNIout,1010)I,ISYMBOL,GXYZ
        else if (J.EQ.2) then
          write(UNIout,1020)GXYZ
        else if (J.GT.2) then
          write(UNIout,1030)GXYZ
        end if
      end do ! J
      end do ! I
!
!
! End of routine PRT_gradients_XYZ
      call PRG_manager ('exit', 'PRT_gradients_XYZ', 'UTILITY')
      return
 1000 FORMAT('ATOMIC POSITION DERIVATIVES (Mdynes)'/'CENTER',15X,'TOTAL')
 1010 FORMAT(1x,I2,2X,A2,'     X',7F17.7)
 1020 FORMAT(12X,'Y',7F17.7)
 1030 FORMAT(12X,'Z',7F17.7)
      END
      subroutine PRT_gradients_PIC
!***********************************************************************
!     Date last modified: May 10, 1995                     Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: Print gradients in Z-matrix format.                 *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE proper_internals
      USE gradients

      implicit none
!
! Local scalars:
      integer I
      double precision GLTOT
!
! Begin:
      call PRG_manager ('enter', 'PRT_gradients_PIC', 'UTILITY')
!
      call GET_object ('MOL', 'GRADIENTS', 'PIC')
!
      write(UNIout,'(a)')'Proper Internal Coordinate Derivatives (Hartree/Bohr or Hartree/radian)'
      write(UNIout,'(a/)')'parameter Gradient (No Constraints)'
      if(PIC%Ncoordinates.eq.0)then
        write(UNIout,'(a)')'No Proper internal coordinates were define: Define them using the MENU'
        return
!       stop 'No Proper internal coordinates were define: Define them using the MENU'
      end if

      GLTOT=ZERO
      do I=1,PIC%Ncoordinates
        GLTOT=GLTOT+GRD_PIC(I)*GRD_PIC(I)
        write(UNIout,'(1X,I5,F20.8)')I,GRD_PIC(I)
      end do ! I
      GLTOT=DSQRT(GLTOT/DBLE(PIC%Ncoordinates))
      write(UNIout,1270)GLTOT
!
 1270 FORMAT('Gradient Length Over all Internal parameters (No Constraints):',1PE14.2//)
!
! End of routine PRT_gradients_PIC
      call PRG_manager ('exit', 'PRT_gradients_PIC', 'UTILITY')
      return
      END
      subroutine GRDLEN_ZMAT (gradient_length)
!***********************************************************************
!     Date last modified: March 16, 1998                               *
!     Author: R. A. Poirier                                            *
!     Description: Calculate gradient length without constraints       *
!***********************************************************************
! Modules:
      USE program_constants
      USE gradients

      implicit none
!
! Output scalar:
      double precision :: gradient_length ! without constraints
!
! Local scalars:
      integer I,NICOOR
!
! Begin:
      call PRG_manager ('enter', 'GRDLEN_ZMAT', 'UTILITY')
!
      call GET_object ('MOL', 'GRADIENTS', 'ZM')
      NICOOR=size(GRD_ZM)
!
! Optimization gradient length g is defined as
!  g^2 = Sum g_i^2 / N
!
      gradient_length=ZERO
      do I=1,NICOOR
         gradient_length=gradient_length+GRD_ZM(I)*GRD_ZM(I)
      end do
      gradient_length=DSQRT(gradient_length/dble(NICOOR))
!
! End of routine GRDLEN_ZMAT
      call PRG_manager ('exit', 'GRDLEN_ZMAT', 'UTILITY')
      return
      END
      subroutine GRDLEN_XYZ (gradient_length)
!***********************************************************************
!     Date last modified: March 16, 1998                               *
!     Author: R. A. Poirier                                            *
!     Description: Calculate gradient length without constraints       *
!***********************************************************************
! Modules:
      USE program_constants
      USE gradients

      implicit none
!
! Output scalar:
      double precision :: gradient_length ! without constraints
!
! Local scalars:
      integer I,NICOOR
!
! Begin:
      call PRG_manager ('enter', 'GRDLEN_XYZ', 'UTILITY')
!
      call GET_object ('MOL', 'GRADIENTS', 'XYZ')
      NICOOR=size(GRDXYZ)
!
! Optimization gradient length g is defined as
!
!  g^2 = Sum g_i^2 / N
!
      gradient_length=ZERO
      do I=1,NICOOR
         gradient_length=gradient_length+GRDXYZ(I)*GRDXYZ(I)
      end do
      gradient_length=DSQRT(gradient_length/dble(NICOOR))
!
! End of routine GRDLEN_ZMAT
      call PRG_manager ('exit', 'GRDLEN_XYZ', 'UTILITY')
      return
      END
