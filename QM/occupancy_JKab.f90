      subroutine occupancy_JKab (occ_Jab, Occ_Kab)
!*******************************************************************************
!     Date last modified: November 19 2020                                     *
!     Authors: R.A. Poirier and I. Awad                                        *
!     Description: Compute the Coulomb and Exchange occupancies                *
!*******************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE type_molecule
      USE GSCF_work

      implicit none
      
      double precision, dimension(1:CMO%NoccMO,1:CMO%NoccMO) :: occ_Jab
      double precision, dimension(1:CMO%NoccMO,1:CMO%NoccMO) :: occ_Kab
      integer :: aMO,bMO,JJ,Lopen,Lopen1,Kopen
!    
      occ_Jab=ZERO
      occ_kab=ZERO
      if (wavefunction.eq.'RHF')then
      do aMO=1,CMO%NoccMO
      do bMO=1,aMO
        if(aMO.eq.bMO)then
          occ_Jab(aMO,bMO)=ONE
          occ_Kab(aMO,bMO)=ZERO
        else
          occ_Jab(aMO,bMO)=FOUR
          occ_Kab(aMO,bMO)=TWO
        end if
      occ_Jab(bMO,aMO)=occ_Jab(aMO,bMO)
      occ_Kab(bMO,aMO)=occ_Kab(aMO,bMO)
      end do ! bMO
      end do ! aMO
      end if     
     
      if (wavefunction.eq.'ROHF')then 
      do aMO=1,CMO%NoccMO
      do bMO=1,aMO
      
!     the closed-shell contribution      
      if((aMO.le.CMO_ROHF%NFcoreMO).and.(bMO.le.CMO_ROHF%NFcoreMO))then
        if(aMO.eq.bMO)then
          occ_Jab(aMO,bMO)=ONE
          occ_Kab(aMO,bMO)=ZERO
        else
          occ_Jab(aMO,bMO)=FOUR
          occ_Kab(aMO,bMO)=TWO
        end if
      end if !the closed-shell contribution
!
!     the closed-shell - open-shell contributions      
      if((aMO.gt.CMO_ROHF%NFcoreMO).and.(bMO.le.CMO_ROHF%NFcoreMO))then
        if(Nopen.gt.0)then
          Kopen=aMO-CMO_ROHF%NFcoreMO
          JJ=Kopen*(Kopen+1)/2+1
          occ_Jab(aMO,bMO)=TWO*ALPHCC(JJ) 
          occ_Kab(aMO,bMO)=TWO*BETACC(JJ)
        end if !
      end if !the closed-shell - open-shell contributions
      
!     the open-shell - open-shell contributions      
      if((aMO.gt.CMO_ROHF%NFcoreMO).and.(bMO.gt.CMO_ROHF%NFcoreMO))then      
      if(Nopen.gt.0)then
          Kopen=aMO-CMO_ROHF%NFcoreMO
          Lopen=bMO-CMO_ROHF%NFcoreMO        
          if(Lopen.lt.Kopen)then
            JJ=Kopen*(Kopen+1)/2+1+Lopen
            occ_Jab(aMO,bMO)=TWO*ALPHCC(JJ)
            occ_Kab(aMO,bMO)=TWO*BETACC(JJ)
          else if(Lopen.gt.Kopen)then
            JJ=Lopen*(Lopen+1)/2+1+Kopen
            occ_Jab(aMO,bMO)=TWO*ALPHCC(JJ)
            occ_Kab(aMO,bMO)=TWO*BETACC(JJ)
          else                                   ! Lopen.eq.Kopen
            JJ=Kopen*(Kopen+1)/2+1+Lopen
            occ_Jab(aMO,bMO)=0.50D0*ALPHCC(JJ)
            occ_Kab(aMO,bMO)=ZERO
          end if
      end if !  
      end if  
      
      occ_Jab(bMO,aMO)=occ_Jab(aMO,bMO)
      occ_Kab(bMO,aMO)=occ_Kab(aMO,bMO)

      end do ! bMO
      end do ! aMO
      
      end if
      end subroutine occupancy_JKab    
