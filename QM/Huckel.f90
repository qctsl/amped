      subroutine Huckel_MO
!*****************************************************************************************************************
!     Date last modified: February 18, 2000                                                         Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Create the Huckel initial guess MO.                                                           *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_basis_set
      USE type_molecule
      USE QM_defaults
      USE QM_objects

      implicit none
!
      integer :: NbasisG
      logical :: LPRT_basis
!
! Work arrays:
      double precision, dimension(:,:), allocatable :: HMATG
      double precision, dimension(:,:), allocatable :: PselfS
      double precision, dimension(:,:), allocatable :: AMHALF
      double precision, dimension(:,:), allocatable :: SCRM1
      double precision, dimension(:,:), allocatable :: SCRM2
      double precision, dimension(:), allocatable :: SCRV2
!
! Local scalars:
!
! Begin:
      call PRG_manager ('enter', 'Huckel_MO', 'UTILITY')
!
      Wavefunction='HUCKEL '
      Level_of_Theory='HUCKEL'

      LPRT_basis=.false.
      call GET_basis_set (BASIS_PROJECT, GUESS_basis, LPRT_basis)
      NbasisG=GUESS_basis%Nbasis

      allocate (CMOH%coeff(NbasisG,NbasisG), CMOH%eigval(NbasisG), CMOH%occupancy(NbasisG))
      allocate (HMATG(1:NbasisG,1:NbasisG))
      allocate (PselfS(NbasisG,NbasisG))
      call BASIS_overlap (GUESS_basis, GUESS_basis, PselfS)
!
! Get Huckel Hamiltonian:
      call Huckel (HMATG, PselfS)         ! --> HMATG
!
! Local work arrays:
      allocate (AMHALF(GUESS_basis%Nbasis,GUESS_basis%Nbasis), SCRM1(GUESS_basis%Nbasis,GUESS_basis%Nbasis), &
                SCRM2(GUESS_basis%Nbasis,GUESS_basis%Nbasis), SCRV2(GUESS_basis%Nbasis))
!
! Form PselfS**(-1/2).
      call NVSQRT (PselfS, AMHALF, SCRM2, SCRV2, GUESS_basis%Nbasis)
!
      SCRM1=matmul(AMHALF, HMATG)
      HMATG=matmul(SCRM1, transpose(AMHALF))
!     HMATG=matmul(AMHALF, matmul(HMATG, transpose(AMHALF)))
      call MATRIX_diagonalize (HMATG, SCRM1, CMOH%eigval, NbasisG, 2, .true.)
      CMOH%coeff=matmul(AMHALF, SCRM1)

      call DEF_NoccMO (CMOH%NoccMO, NAelectrons, NBelectrons)
      CMOH%occupancy(1:CMOH%NoccMO)=TWO
      CMOH%occupancy(CMOH%NoccMO+1:NbasisG)=ZERO
!
      deallocate (AMHALF, SCRM1, SCRM2, SCRV2, HMATG, PselfS)
!
! End of routine Huckel_MO
      call PRG_manager ('exit', 'Huckel_MO', 'UTILITY')
      return
      end subroutine Huckel_MO
      subroutine Huckel (HMATG, PselfS)
!*****************************************************************************************************************
!     Date last modified: February 18, 2000                                                         Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Create Huckel hamiltonian for an STO-NG(*) basis                                              *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE type_basis_set
      USE QM_defaults

      implicit none
!
! Local scalars:
      integer BPFON3
      logical LPDFPJ
      character(len=16) SHLtype
!
      double precision :: HMATG(1:GUESS_basis%Nbasis,1:GUESS_basis%Nbasis)
      double precision :: PselfS(1:GUESS_basis%Nbasis,1:GUESS_basis%Nbasis)
!
! Work arrays:
      integer, dimension(:), allocatable :: POINTER
      logical, dimension(:), allocatable :: LCORE
!
! Local scalars:
      integer Iatmshl,INDEXI,Ishell,IA,Ibasis,IM1,Jbasis,L
      double precision C3D,C3P,C4P,ISCALE,JSCALE,PT001,TENM10,XII
      logical L3DPOL,LBLOCS
!
! Local arrays:
      PARAMETER (TENM10=1.0D-10,PT001=0.001D0)
!
!     Data fudged pending a proper fit for elements 37-54.
!     The values for elements 1-17 are ad hoc values due mostly to
!     Ditchfield and Whiteside.  Values for elements 18-54 are from
!     Desclaux's numerical Dirac-Fock calculations (Atomic Data and
!     Nuclear Data Tables, Vol.12, PP.311, 1973).
!
      double precision :: Huckel_parameters(11,54) ! Nparameters * Nelements
!
! Huckel parameters
      DATA Huckel_parameters/ &
       0.537,10*0.0,                                                                      & ! H
       0.735,10*0.0,                                                                      & ! He
       2.88,0.3,0.1,8*0.0,                                                                & ! Li
       5.12,0.683,0.24,8*0.0,                                                             & ! Be
       8.0,1.1,0.33,   8*0.0,                                                             & ! B
       11.52,1.55,0.37,8*0.0,                                                             & ! C
       15.68,1.86,0.42,8*0.0,                                                             & ! N
       20.48,2.5,0.46, 8*0.0,                                                             & ! O
       25.92,3.001,0.52,8*0.0,                                                            & ! F
       32.0,3.3,0.58,  8*0.0,                                                             & ! Ne
       38.72,5.99,1.997,0.57,0.11,11.0,5*0.0,                                             & ! Na
       46.08,7.776,2.592,0.787,0.25,12.0,5*0.0,                                           & ! Mg
       54.08,9.887,3.296,0.906,0.371,13.0,5*0.0,                                          & ! Al
       62.72,12.348,4.116,1.3,0.7,14.0,5*0.0,                                             & ! Si
       72.0,15.187,5.063,1.5,0.9,15.0,5*0.0,                                              & ! P
       81.92,18.432,6.144,1.8,1.1,16.0,5*0.0,                                             & ! S
       92.48,22.109,7.37,2.1,1.3,17.0,5*0.0,                                              & ! Cl
       207.36,52.488,17.496,2.58,1.18,18.0,5*0.0,                                         & ! Ar
       231.04,61.731,30.866,20.577,15.433,0.0,0.3,0.0,0.140,0.0,0.0,                      & ! K
       256.0,72.0,36.0,24.0,18.0,0.0,0.4,0.2,0.0,0.0,0.0,                                 & ! Ca
       282.24,83.349,41.675,27.783,20.837,0.68,0.42,0.2,0.0,0.0,0.0,                      & ! Sc
       309.76,95.832,47.916,31.944,23.958,0.8,0.44,0.22,0.0,0.0,0.0,                      & ! Ti
       338.56,109.503,54.752,36.501,27.376,0.9,0.46,0.22,0.0,0.0,0.0,                     & ! V (23)
       368.64,124.416,62.208,41.472,31.104,0.64,0.42,0.2,0.0,0.0,0.0,                     & ! Cr
       400.0,140.625,70.313,46.875,35.156,1.1,0.5,0.24,0.0,0.0,0.0,                       & ! Mn
       432.64,158.184,79.092,52.728,39.546,1.2,0.52,0.26,0.0,0.0,0.0,                     & ! Fe
       466.56,177.147,88.574,59.049,44.287,1.28,0.54,0.26,0.0,0.0,0.0,                    & ! Co
       501.76,197.568,98.784,65.856,49.392,1.38,0.56,0.28,0.0,0.0,0.0,                    & ! Ni
       538.24,219.501,109.751,73.167,54.875,0.98,0.48,0.24,0.0,0.0,0.0,                   & ! Cu
       576.0,243.0,121.5,81.0,60.75,1.54,0.6,0.3,0.0,0.0,0.0,                             & ! Zn
       615.04,268.119,134.06,89.373,67.03,2.34,0.86,0.42,0.0,0.0,0.0,                     & ! Ga
       655.36,294.912,147.456,98.304,73.728,3.24,1.14,0.54,0.0,0.0,0.0,                   & ! Ge
       696.96,323.433,161.717,107.811,80.858,4.2,1.42,0.66,0.0,0.0,0.0,                   & ! As
       739.84,353.736,176.868,117.912,88.434,5.24,1.72,0.78,0.0,0.0,0.0,                  & ! Se
!      1212.5,160.0,145.0,22.5,17.5,5.75,2.0,0.75,0.0,0.0,0.0,                            & ! Br
       784.0,385.875,192.937,128.625,96.469,6.36,2.04,0.92,0.0,0.0,0.0,                   & ! Br
       829.44,419.904,209.952,139.968,104.976,7.56,2.38,1.04,0.0,0.0,0.0,                 & ! Kr
       876.16,455.877,227.939,151.959,113.969,65.119,91.175,75.98,0.0,0.28,0.14,          & ! Rb
       924.16,493.848,246.924,164.616,123.462,70.543,98.770,82.308,0.0,0.36,0.18,         & ! Sr (38)
       973.44,533.871,266.936,177.957,133.468,76.261,106.774,88.979,0.46,0.4,0.2,         & ! Y
       1024.0,576.0,288.0,192.0,144.0,82.278,115.2,96.0,0.58,0.44,0.22,                   & ! Zr
       1075.84,620.289,310.145,206.763,155.072,88.605,124.058,103.382,0.52,0.42,0.2,      & ! Nb
       1128.96,666.792,333.396,222.264,166.698,95.248,133.358,111.132,0.6,0.42,0.22,      & ! Mo
       1183.36,715.563,357.782,238.521,178.891,102.214,143.113,119.261,0.92,0.5,0.24,     & ! Tc
       1239.04,766.656,383.328,255.552,191.664,109.513,153.331,127.776,0.78,0.44,0.22,    & ! Ru
       1296.0,820.125,410.062,273.375,205.031,117.15,164.025,136.688,0.86,0.46,0.22,      & ! Rh
       1354.24,876.024,438.012,292.008,219.006,125.135,175.205,146.004,0.68,0.46,0.22,    & ! Pd
       1413.76,934.407,467.204,311.469,233.602,133.475,186.881,155.735,1.06,0.46,0.24,    & ! Ag
       1474.56,995.328,497.664,331.776,248.832,142.177,199.066,165.888,1.48,0.56,0.28,    & ! Cd
       1536.64,1058.841,529.421,352.947,264.71,151.25,211.768,176.474,2.06,0.8,0.38,      & ! In
       1600.0,1125.0,562.5,375.0,281.25,160.7,225.0,187.5,2.68,1.02,0.48,                 & ! Sn
       1664.64,1193.859,596.93,397.953,298.465,170.536,238.772,198.977,3.32,1.26,0.6,     & ! Sb
       1119.2,343.3,160.73,81.88,62.33,54.33,11.42,9.308,8.500,0.9641,0.6035,             & ! Te
       1797.76,1339.893,669.947,446.631,334.973,191.396,267.979,223.316,4.68,1.76,0.8,    & ! I
       1866.24,1417.176,708.588,472.392,354.294,202.436,283.435,236.196,5.42,2.02,0.9/      ! Xe
!
! Huckel_parameters(J,I) are the diagonal elements of the HUCKEL_DATA Hamiltonian
! in atomic units,  I = Atomic number.
!     J    Orbital            J    Orbital
!     1     1S                6     3D
!     2     2S                7     4S
!     3     2P                8     4P
!     4     3S                9     4D
!     5     3P                10    5S
!                             11    5P
!
! Begin:
      call PRG_manager ('enter', 'HUCKEL', 'UTILITY')
!
      LBLOCS=.false.
      LPDFPJ=.false.
      BPFON3=0
!
! Local work arrays:
      allocate (POINTER(GUESS_basis%Natmshls), LCORE(GUESS_basis%Nbasis))
!
      L3DPOL=BPFON3.NE.0.and.Basis%name(1:3).EQ.'STO'
!
! Fill in diagonal elements of HMATG.
! Set polarization function flags.
!
      do Ibasis=1,GUESS_basis%Nbasis
        LCORE(Ibasis)=.FALSE.
      end do ! Ibasis
!
! Loop over shells...
      do Ishell=1,GUESS_basis%Nshells
      SHLtype=GUESS_basis%shell(Ishell)%SHLTLB
      do Iatmshl=GUESS_basis%shell(Ishell)%frstSHL,GUESS_basis%shell(Ishell)%lastSHL

!     write(6,*)'SHLtype: ',SHLtype
      SHELL_type: select case (SHLtype)
      case ('1S')
        POINTER(Iatmshl)=1
      case ('2S')
        POINTER(Iatmshl)=2
      case ('2P')
        POINTER(Iatmshl)=3
      case ('3S','3S*')
        POINTER(Iatmshl)=4
      case ('3P','3P*')
        POINTER(Iatmshl)=5
      case ('3D','3D*')
        POINTER(Iatmshl)=6
      case ('4S','4S*')
        POINTER(Iatmshl)=7
      case ('4P','4P*')
        POINTER(Iatmshl)=8
      case ('4D','4D*')
        POINTER(Iatmshl)=9
      case ('5S')
        POINTER(Iatmshl)=10
      case ('5P')
        POINTER(Iatmshl)=11
      case ('D-POL')
        POINTER(Iatmshl)=12
      case default
        write(UNIout,*)'error_Huckel> Illegal Shell type lable: ',GUESS_basis%shell(Ishell)%SHLTLB
        stop 'error_Huckel> Illegal Shell type lable '
      end select SHELL_type
      end do ! Iatmshl
      end do ! Ishell
!
      L=1
      do Ishell=1,GUESS_basis%Nshells
      IA=GUESS_basis%shell(Ishell)%shlatm ! Get atomic number
!     if(IA.gt.20.and.GUESS_Lscale)then ! For atoms Sc ..., use different defaults (not tested)
!       CORE_scale=0.40D0
!       VAL_scale=1.00D0
!     end if
      do Iatmshl=GUESS_basis%shell(Ishell)%frstSHL,GUESS_basis%shell(Ishell)%lastSHL

      IF(IA.NE.0.and.IA.NE.999)then
      INDEXI=POINTER(Iatmshl)
!
      select case (INDEXI)
      case (1)                  ! 1S Orbital
      HMATG(L,L)=-Huckel_parameters(1,IA)
      IF(IA.GT.2)LCORE(L)=.TRUE.
      L=L+1

      case (2)                  ! 2S Orbital.
      HMATG(L,L)=-Huckel_parameters(2,IA)
      IF(IA.GT.10)then
      LCORE(L)=.TRUE.
      end if
      L=L+1

      case (3)                  ! 2P Orbitals.
      HMATG(L,L)=-Huckel_parameters(3,IA)
      HMATG(L+1,L+1)=-Huckel_parameters(3,IA)
      HMATG(L+2,L+2)=-Huckel_parameters(3,IA)
      IF(IA.GT.10)then
        LCORE(L)=.TRUE.
        LCORE(L+1)=.TRUE.
        LCORE(L+2)=.TRUE.
      end if
      L=L+3

      case (4)                  ! 3S Orbital
      HMATG(L,L)=-Huckel_parameters(4,IA)
      IF(IA.GT.18)then
        LCORE(L)=.TRUE.
      end if
      L=L+1

      case (5)                  ! 3P Orbitals.
      HMATG(L,L)=-Huckel_parameters(5,IA)
      HMATG(L+1,L+1)=-Huckel_parameters(5,IA)
      HMATG(L+2,L+2)=-Huckel_parameters(5,IA)
      IF(IA.GT.18)then
        LCORE(L)=.TRUE.
        LCORE(L+1)=.TRUE.
        LCORE(L+2)=.TRUE.
      end if
      L=L+3

      case (6)                  ! 3D Orbitals.
      HMATG(L,L)=-Huckel_parameters(6,IA)
      HMATG(L+1,L+1)=-Huckel_parameters(6,IA)
      HMATG(L+2,L+2)=-Huckel_parameters(6,IA)
      HMATG(L+3,L+3)=-Huckel_parameters(6,IA)
      HMATG(L+4,L+4)=-Huckel_parameters(6,IA)
      IF(IA.GT.30)then
        LCORE(L)=.TRUE.
        LCORE(L+1)=.TRUE.
        LCORE(L+2)=.TRUE.
        LCORE(L+3)=.TRUE.
        LCORE(L+4)=.TRUE.
      end if
      IF(.not.LPDFPJ)then
        HMATG(L+5,L+5)=-Huckel_parameters(6,IA)
        IF(IA.GT.30)then
          LCORE(L+5)=.TRUE.
        end if
        L=L+6
      else
        L=L+5
      end if

      case (7)                  ! 4S Orbital.
      HMATG(L,L)=-Huckel_parameters(7,IA)
!     IF(IA.GT.36)then
!     IF(IA.GT.20)then  ! May 31, 2019
      IF(IA.GT.36)then  ! July 10, 2019
        LCORE(L)=.TRUE.
      end if
      L=L+1

      case (8)                  ! 4P Orbitals
      HMATG(L,L)=-Huckel_parameters(8,IA)
      HMATG(L+1,L+1)=-Huckel_parameters(8,IA)
      HMATG(L+2,L+2)=-Huckel_parameters(8,IA)
      IF(IA.GT.36)then
        LCORE(L)=.TRUE.
        LCORE(L+1)=.TRUE.
        LCORE(L+2)=.TRUE.
      end if
      L=L+3

      case (9)                  ! 4D Orbitals.
      HMATG(L,L)=-Huckel_parameters(9,IA)
      HMATG(L+1,L+1)=-Huckel_parameters(9,IA)
      HMATG(L+2,L+2)=-Huckel_parameters(9,IA)
      HMATG(L+3,L+3)=-Huckel_parameters(9,IA)
      HMATG(L+4,L+4)=-Huckel_parameters(9,IA)
      IF(.not.LPDFPJ)then
        HMATG(L+5,L+5)=-Huckel_parameters(9,IA)
        L=L+6
      else
        L=L+5
      end if

      case (10)                 ! 5S Orbital.
      HMATG(L,L)=-Huckel_parameters(10,IA)
      L=L+1

      case (11)                 ! 5P Orbitals.
      HMATG(L,L)=-Huckel_parameters(11,IA)
      HMATG(L+1,L+1)=-Huckel_parameters(11,IA)
      HMATG(L+2,L+2)=-Huckel_parameters(11,IA)
      L=L+3

      case (12)                 ! D-polarization
      HMATG(L,L)=-Huckel_parameters(6,IA)
      HMATG(L+1,L+1)=-Huckel_parameters(6,IA)
      HMATG(L+2,L+2)=-Huckel_parameters(6,IA)
      HMATG(L+3,L+3)=-Huckel_parameters(6,IA)
      HMATG(L+4,L+4)=-Huckel_parameters(6,IA)
      IF(.not.LPDFPJ)then
        HMATG(L+5,L+5)=-Huckel_parameters(6,IA)
        L=L+6
      else
        L=L+5
      end if
      end select
      end if ! IA.EQ.0.and.IA.NE.999
      end do ! Iatmshl
      end do ! Ishell
!
      L=L-1
      IF(L.NE.GUESS_basis%Nbasis)GO TO 900
      IF(.not.LBLOCS) then
!
! Scan the diagonal elements of the H matrix for positive integral
! values.  It is necessary to remove any degeneracies at this point
! that have been caused by the empty outer shells.
      do Ibasis=1,GUESS_basis%Nbasis
      XII=HMATG(Ibasis,Ibasis)
      IF(XII.GE.ZERO) then
      IF(DABS(XII-DBLE(IDINT(XII))).LE.TENM10)HMATG(Ibasis,Ibasis)=XII+PT001*DBLE(Ibasis)
      end if
      end do ! Ibasis
      end if ! .not.LBLOCS
!
      IF(GUESS_basis%Nbasis.GT.1) then
!
! Form off-diagonal H matrix elements.
      do Ibasis=2,GUESS_basis%Nbasis
      IF(LCORE(Ibasis))then
        ISCALE=CORE_scale
      else
        ISCALE=VAL_scale
      end if
      IM1=Ibasis-1
      do Jbasis=1,IM1
      IF(LCORE(Jbasis))then
        JSCALE=CORE_scale
      else
        JSCALE=VAL_scale
      end if
      XII=PselfS(Ibasis,Jbasis)*(ISCALE*HMATG(Ibasis,Ibasis)+JSCALE*HMATG(Jbasis,Jbasis))
      HMATG(Ibasis,Jbasis)=XII
      HMATG(Jbasis,Ibasis)=XII
      end do ! Jbasis
      end do ! Ibasis
!
      end if ! GUESS_basis%Nbasis.GT.1
!
      deallocate (POINTER, LCORE)
!
! End of routine HUCKEL
      call PRG_manager ('exit', 'HUCKEL', 'UTILITY')
      return
!
! Error section.
  900 write(UNIout,'(A,I4,A,I4,A)')'ERROR> HUCKEL: GUESS_basis%Nbasis =',GUESS_basis%Nbasis,' BUT ',L, &
                                   ' BASIS functionS GENERATED INTERNALLY'
      call PRG_manager ('exit', 'HUCKEL', 'UTILITY')
! End of routine HUCKEL
      return
      end subroutine HUCKEL
