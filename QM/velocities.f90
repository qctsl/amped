      subroutine VELOCITIES
!***********************************************************************
!     Date last modified: June 24, 1992                                *
!     Author: R.A. Poirier                                             *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE QM_defaults
      USE QM_objects
      USE integral_transformation
      USE matrix_print

      implicit none
!
! Local scalar:
      double precision :: Velocity_X,Velocity_Y,Velocity_Z
!
! Local functions:
      double precision :: TraceAB
!
! Begin:
      call PRG_manager ('enter', 'VELOCITIES', 'UTILITY')
!
      call GET_object ('INT', 'VDIPOLE', 'AO')
      call GET_object ('QM', 'CMO', Wavefunction) ! Only RHF or ROHF

      Velocity_X= TraceAB (PM0, VDDx, NBasis)
      write(UNIout,'(a,f12.6)')'Velocity Vx:',Velocity_X

      Velocity_Y= TraceAB (PM0, VDDy, NBasis)
      write(UNIout,'(a,f12.6)')'Velocity Vy:',Velocity_Y

      Velocity_Z= TraceAB (PM0, VDDz, NBasis)
      write(UNIout,'(a,f12.6)')'Velocity Vz:',Velocity_Z
!
! End of routine VELOCITIES
      call PRG_manager ('exit', 'VELOCITIES', 'UTILITY')
      return
      end subroutine VELOCITIES
