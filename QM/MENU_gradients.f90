      subroutine SET_defaults_GRD
!*****************************************************************************************************************
!     Date last modified: March 31, 2000                                                            Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Set code defaults.                                                                            *
!*****************************************************************************************************************
! Modules:
      USE QM_defaults
!
      GRD_expcut=25.0D0
!     GRD_expcut=45.0D0
      GRD_ACC=1.0D-07
      GRD_PQCUT1=1.0D-06
      GRD_PQCUT2=1.0D-10
      GRD_PQCUT3=45.0D0
      VTOL1=1.0D-20
      VTOL2=1.0D-20
      VTOLS=1.0D-30
      LG2ESP=.false.
      LG2EDF=.true.
      LGRDBA=.false.
      Lfixed_G2E=.false.
!
      return
      end subroutine SET_defaults_GRD
      subroutine MENU_gradients
!***********************************************************************
!     Date last modified: April 2, 2001                                *
!     Author: R. Poirier                                               *
!     Description: Set up energy gradient calculation.                 *
!***********************************************************************
! Modules
      USE program_parser
      USE MENU_gets
      USE QM_defaults

      implicit none
!
! Local scalars:
      logical done,lrun
!
! Begin:
      call PRG_manager ('enter', 'MENU_gradients', 'UTILITY')
      done=.false.
      lrun=.false.
!
! Menu:
      do while (.not.done)
      call new_token (' Gradient:')
!
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      ' Command GRAdient:', &
      '   Purpose: calculates Energy Gradient ', &
      '   Syntax :', &
      '   GRAdient ', &
      '     ACCuracy: <real*8>     : accuracy of integral ', &
      '     BASis   : <logical>     ', &
      '     EXPCUT  : <real*8>       : exponent cutoff in GDFCLC ', &
      '     GDFCLC  : <logical>       : subroutine used ', &
      '     GSPCLC  : <logical>       : subroutine used ', &
      '     FIXed   : <logical>       : 2-e gradients fixed ', &
      '     PQCUT1  : <real*8>       : cutoff in GDFCLC ', &
      '     PQCUT2  : <real*8>       : cutoff in GDFCLC ', &
      '     PQCUT3  : <real*8>       : cutoff in GDFCLC ', &
      '     VTOL1   : <real*8>       : cutoff in GSPCLC ', &
      '     VTOL2   : <real*8>       : cutoff in GSPCLC ', &
      '     VTOLS   : <real*8>       : cutoff in GSPCLC ', &
      '   RUN command: Will cause execution', &
      '   end'
!
      else if(token(1:3).EQ.'ACC')then
        call GET_value (GRD_ACC)
        write(UNIout,'(9x,a,E22.15)') 'MENU_gradients> Accuracy =',GRD_acc

      else if(token(1:3).EQ.'BAS')then
        call GET_value (LGRDBA)

      else if(token(1:3).EQ.'GSP')then
        call GET_value (LG2ESP)

      else if(token(1:3).EQ.'GDF')then
        call GET_value (LG2EDF)

      else if(token(1:3).EQ.'FIX')then
        call GET_value (Lfixed_G2E)

! PQCUT1
      else if(token(1:6).EQ.'PQCUT1')then
        call GET_value (GRD_PQCUT1)
        write(UNIout,'(a,E22.15)')' MENU_gradients> Exponent/ccoef2 cutoff PQCUT1= ',GRD_PQCUT1
! PQCUT2
      else if(token(1:6).EQ.'PQCUT2')then
        call GET_value (GRD_PQCUT2)
        write(UNIout,'(a,E22.15)')' MENU_gradients> Exponent/ccoef2 cutoff PQCUT2= ',GRD_PQCUT2
! PQCUT3
      else if(token(1:6).EQ.'PQCUT3')then
        call GET_value (GRD_PQCUT3)
        write(UNIout,'(a,E22.15)')' MENU_gradients> Exponent cutoff PQCUT3= ',GRD_PQCUT3

      else if(token(1:3).EQ.'RUN')then
        lrun=.true.
! VTOL1
      else if(token(1:6).EQ.'VTOL1')then
        call GET_value (VTOL1)
        write(UNIout,'(a,E22.15)')' MENU_gradients> VTOL1 in GSPCLC = ',VTOL1
! VTOL2
      else if(token(1:6).EQ.'VTOL2')then
        call GET_value (VTOL2)
        write(UNIout,'(a,E22.15)')' MENU_gradients> VTOL2 in GSPCLC = ',VTOL2

! VTOLS
      else if(token(1:6).EQ.'VTOLS')then
        call GET_value (VTOLS)
        write(UNIout,'(a,E22.15)')' MENU_gradients> VTOLS in GSPCLC = ',VTOLS

      else
           call MENU_end (done)
      end if
!
      end do !(.not.done)

      if(LGRDBA)then
        LG2ESP=.false.
        LG2EDF=.false.
      end if
!
      if(lrun)call GET_object ('COORDINATES:GRADIENTS%COMPONENTS')
!
! End of routine MENU_gradients
      call PRG_manager ('exit', 'MENU_gradients', 'UTILITY')
      return
      end
