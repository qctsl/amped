      subroutine MO_COEFFICIENTS
!****************************************************************************************************************
!     Date last modified: September 28, 2000                                                       Version 2.0  *
!     Author: R.A. Poirier                                                                                      *
!     Description: Interface to get required MO coefficients                                                    *
!****************************************************************************************************************
! Modules:
      USE QM_defaults

      implicit none
!
! Begin:
      call PRG_manager ('enter', 'MO_COEFFICIENTS', 'CMO_COEFFICIENTS')
!
      call GET_object ('QM', 'CMO', Wavefunction)
!
! End of routine MO_COEFFICIENTS
      call PRG_manager ('exit', 'MO_COEFFICIENTS', 'CMO_COEFFICIENTS')
      return
      end
      subroutine BLD_density_1MATRIX
!****************************************************************************************************************
!     Date last modified: April 3, 2000                                                            Version 2.0  *
!     Author: R.A. Poirier                                                                                      *
!     Description: Get the density matrix.                                                                      *
!****************************************************************************************************************
! Modules:
      USE QM_defaults

      implicit none
!
! Begin:
      call PRG_manager ('enter', 'BLD_density_1MATRIX', 'DENSITY_1MATRIX')
!
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)
!
! End of routine BLD_density_1MATRIX
      call PRG_manager ('exit', 'BLD_density_1MATRIX', 'DENSITY_1MATRIX')
      return
      end
      subroutine DENSITY_1MATRIX_RHF
!***********************************************************************
!     Date last modified: July 07, 1992                                *
!     Author: R.A. Poirier                                             *
!     Description: interface to create the appropriate density matrix. *
!***********************************************************************
! Modules:

      implicit none
!
! Begin:
      call PRG_manager ('enter', 'DENSITY_1MATRIX_RHF', 'DENSITY_1MATRIX%RHF')
! This automatically creates the density matrix (run an SCF)
      call GET_object ('QM', 'CMO', 'RHF')
!
! End of routine DENSITY_1MATRIX_RHF
      call PRG_manager ('exit', 'DENSITY_1MATRIX_RHF', 'DENSITY_1MATRIX%RHF')
      return
      end
      subroutine DENSITY_1MATRIX_Huckel
!***********************************************************************
!     Date last modified: July 07, 1992                                *
!     Author: R.A. Poirier                                             *
!     Description: interface to create the appropriate density matrix. *
!***********************************************************************
! Modules:
      USE QM_defaults
      USE QM_objects

      implicit none
!
! Local scalars:
!
! Begin:
      call PRG_manager ('enter', 'DENSITY_1MATRIX_HUCKEL', 'DENSITY_1MATRIX%HUCKEL')
! This automatically creates the density matrix (run an SCF)
      Wavefunction='HUCKEL '
      Level_of_Theory='HUCKEL'
      call GET_object ('QM', 'CMO', 'HUCKEL')

      if(.not.allocated(PM0G))then
        allocate (PM0G(1:MATlen))
      else
        deallocate (PM0G)
        allocate (PM0G(1:MATlen))
      end if

      call DENBLD (CMOH%coeff, CMOH%occupancy, PM0G, Nbasis, MATlen, CMOH%NoccMO)
      PM0=>PM0G
!
! End of routine DENSITY_1MATRIX_RHF
      call PRG_manager ('exit', 'DENSITY_1MATRIX_HUCKEL', 'DENSITY_1MATRIX%HUCKEL')
      return
      end
      subroutine DiagH_PM0
!***********************************************************************
!     Date last modified: July 07, 1992                                *
!     Author: R.A. Poirier                                             *
!     Description: interface to create the appropriate density matrix. *
!***********************************************************************
! Modules:

      implicit none
!
! Begin:
      call PRG_manager ('enter', 'DIAGH_PM0', 'DENSITY_1MATRIX%DIAGH')
! This automatically creates the density matrix (run an SCF)
      call GET_object ('QM', 'CMO', 'DIAGH')
!
! End of routine DIAGH_PM0
      call PRG_manager ('exit', 'DIAGH_PM0', 'DENSITY_1MATRIX%DIAGH')
      return
      end
      subroutine DENSITY_1MATRIX_GVB
!***********************************************************************
!     Date last modified: July 07, 1992                                *
!     Author: R.A. Poirier                                             *
!     Description: interface to create the appropriate density matrix. *
!***********************************************************************
! Modules:

      implicit none
!
! Begin:
      call PRG_manager ('enter', 'DENSITY_1MATRIX_GVB', 'DENSITY_1MATRIX%GVB')
! This automatically creates the density matrix (run an SCF)
      call GET_object ('QM', 'CMO', 'GVB')
!
! End of routine DENSITY_1MATRIX_GVB
      call PRG_manager ('exit', 'DENSITY_1MATRIX_GVB', 'DENSITY_1MATRIX%GVB')
      return
      end
      subroutine DENSITY_1MATRIX_ROHF
!***********************************************************************
!     Date last modified: March 29, 2001                               *
!     Author: R.A. Poirier                                             *
!     Description: interface to create the appropriate density matrix. *
!***********************************************************************
! Modules:

      implicit none
!
! Begin:
      call PRG_manager ('enter', 'DENSITY_1MATRIX_ROHF', 'DENSITY_1MATRIX%ROHF')
! This automatically creates the density matrix (run an SCF)
      call GET_object ('QM', 'CMO', 'ROHF')
!
! End of routine DENSITY_1MATRIX_ROHF
      call PRG_manager ('exit', 'DENSITY_1MATRIX_ROHF', 'DENSITY_1MATRIX%ROHF')
      return
      end
      subroutine DENSITY_1MATRIX_UHF
!***********************************************************************
!     Date last modified: July 07, 1992                                *
!     Author: R.A. Poirier                                             *
!     Description: interface to create the appropriate density matrix. *
!***********************************************************************
! Modules:

      implicit none
!
! Begin:
      call PRG_manager ('enter', 'DENSITY_1MATRIX_UHF', 'DENSITY_1MATRIX%UHF')
! This automatically creates the density matrix (run an SCF)
      call GET_object ('QM', 'CMO', 'UHF')
!
! End of routine DENSITY_1MATRIX_UHF
      call PRG_manager ('exit', 'DENSITY_1MATRIX_UHF', 'DENSITY_1MATRIX%UHF')
      return
      end subroutine DENSITY_1MATRIX_UHF
      subroutine DENSITY_1MATRIX_OSHF
!***********************************************************************
!     Date last modified: July 07, 1992                                *
!     Author: R.A. Poirier                                             *
!     Description: interface to create the appropriate density matrix. *
!***********************************************************************
! Modules:

      implicit none
!
! Begin:
      call PRG_manager ('enter', 'DENSITY_1MATRIX_OSHF', 'DENSITY_1MATRIX%OSHF')
! This automatically creates the density matrix (run an SCF)
      call GET_object ('QM', 'CMO', 'OSHF')
!
! End of routine DENSITY_1MATRIX_OSHF
      call PRG_manager ('exit', 'DENSITY_1MATRIX_OSHF', 'DENSITY_1MATRIX%OSHF')
      return
      end subroutine DENSITY_1MATRIX_OSHF
      subroutine BLD_density_guess
!*****************************************************************************************************************
!     Date last modified: April 3, 2000                                                             Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Calculate the guess density matrix.                                                           *
!*****************************************************************************************************************
! Modules:
      USE QM_defaults

      implicit none
!
! Begin:
      call PRG_manager ('enter', 'BLD_density_guess', 'GUESS:DENSITY_1MATRIX')
!
      call GET_object ('GUESS', 'DENSITY_1MATRIX', Wavefunction)
!
! End of routine BLD_density_guess
      call PRG_manager ('exit', 'BLD_density_guess', 'GUESS:DENSITY_1MATRIX')
      return
      end
      subroutine DENSITY_guess_RHF
!***********************************************************************
!     Date last modified: April 3, 2000                   Version 2.0  *
!     Author: R.A. Poirier                                             *
!     Description: Form the RHF guess density matrix   .               *
!***********************************************************************
! Modules:
      USE program_constants
      USE QM_objects

      implicit none
!
! Local scalars:
!
! Begin:
      call PRG_manager ('enter', 'DENSITY_guess_RHF', 'GUESS:DENSITY_1MATRIX%RHF')
!
      call GET_object ('GUESS', 'CMO', 'RHF')

      if(.not.associated(CMOG%occupancy))then
        allocate(CMOG%occupancy(1:Nbasis))
      end if

      CMOG%occupancy(1:CMOG%NoccMO)=TWO
      CMOG%occupancy(CMOG%NoccMO+1:Nbasis)=ZERO

      if(.not.allocated(PM0G))then
        allocate(PM0G(1:MATlen))
      end if

      call DENBLD (CMOG%coeff, CMOG%occupancy, PM0G, Nbasis, MATlen, CMOG%NoccMO)
!
! End of routine DENSITY_guess_RHF
      call PRG_manager ('exit', 'DENSITY_guess_RHF', 'GUESS:DENSITY_1MATRIX%RHF')
      return
      end
      subroutine BLD_Energy_weighted_density
!*****************************************************************************************************************
!     Date last modified: April 3, 2000                                                             Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Interface to get the required energy-weighted density.                                        *
!*****************************************************************************************************************
! Modules:
      USE QM_defaults
      USE QM_objects

      implicit none
!
! Begin:
      call PRG_manager ('enter', 'BLD_Energy_weighted_density', 'DENSITY_ENERGY_WEIGHTED')
!
      call GET_object ('QM', 'DENSITY_ENERGY_WEIGHTED', Wavefunction)
!
! End of routine BLD_Energy_weighted_density
      call PRG_manager ('exit', 'BLD_Energy_weighted_density', 'DENSITY_ENERGY_WEIGHTED')
      return
      end
      subroutine DENSITY_Eweighted_RHF
!***********************************************************************
!     Date last modified: July 07, 1992                                *
!     Author: R.A. Poirier                                             *
!     Description: Form the RHF energy-weighted density.               *
!***********************************************************************
! Modules:
      USE program_constants
      USE QM_objects

      implicit none
!
! Local scalar:
      integer I
!
! Local work array:
      double precision, dimension(:), allocatable :: SCRVEC
!
! Begin:
      call PRG_manager ('enter', 'DENSITY_Eweighted_RHF', 'DENSITY_ENERGY_WEIGHTED%RHF')
!
      if(.not.allocated(PEWD))then
        allocate (PEWD(MATlen))
      else
        deallocate (PEWD)
        allocate (PEWD(MATlen))
      end if
!
! Work array:
      allocate (SCRVEC(Nbasis))

      SCRVEC(1:Nbasis)=ZERO
      PEWD(1:MATlen)=ZERO

      do I=1,CMO%NoccMO
        SCRVEC(I)=-CMO%EIGVAL(I)*CMO%occupancy(I)
      end do
!
      call DENBLD (CMO%coeff, SCRVEC, PEWD, Nbasis, MATlen, CMO%NoccMO)

      deallocate (SCRVEC)
!
! End of routine DENSITY_Eweighted_RHF
      call PRG_manager ('exit', 'DENSITY_Eweighted_RHF', 'DENSITY_ENERGY_WEIGHTED%RHF')
      return
      end
      subroutine DENSITY_Eweighted_UHF
!****************************************************************************
!     Date last modified: September 26, 2000                                *
!     Author: R.A. Poirier                                                  *
!     Description: Construct UHF energy-weighted density for gradient       *
!                  calculation.                                             *
!****************************************************************************
! MODULES:
      USE program_constants
      USE QM_objects

      implicit none
!
! Local scalars:
      integer I,J,K,L
      double precision T
!
! Begin:
      call PRG_manager ('enter', 'DENSITY_Eweighted_UHF', 'DENSITY_ENERGY_WEIGHTED%UHF')
!
      call GET_object ('QM', 'CMO', 'UHF')
!
      if(.not.allocated(PEWD))then
        allocate (PEWD(MATlen))
      else
        deallocate (PEWD)
        allocate (PEWD(MATlen))
      end if
!
      PEWD(1:MATlen)=ZERO
!
! Form the total and energy-weighted density matrices.
      do I=1,CMOA%NoccMO
      L=0
      do J=1,Nbasis
      do K=1,J
      L=L+1
      T=CMOA%coeff(J,I)*CMOA%coeff(K,I)
      PEWD(L)=PEWD(L)-T*CMOA%EIGVAL(I)
      end do ! K
      end do ! J
      end do ! I
!
      IF(CMOB%NoccMO.GT.0)then
        do I=1,CMOB%NoccMO
        L=0
        do J=1,Nbasis
        do K=1,J
        L=L+1
        T=CMOB%coeff(J,I)*CMOB%coeff(K,I)
        PEWD(L)=PEWD(L)-T*CMOB%EIGVAL(I)
        end do ! K
        end do ! J
        end do ! I
      end if
!
! End of routine DENSITY_Eweighted_UHF
      call PRG_manager ('exit', 'DENSITY_Eweighted_UHF', 'DENSITY_ENERGY_WEIGHTED%UHF')
      return
      end
      subroutine DENSITY_Eweighted_OSHF
!****************************************************************************
!     Date last modified: September 26, 2000                                *
!     Author: R.A. Poirier                                                  *
!     Description: Construct OSHF energy-weighted density for gradient      *
!                  calculation.                                             *
!     NOTE: Same as UHF should not have two copies!                         *
!****************************************************************************
! MODULES:
      USE program_constants
      USE QM_objects

      implicit none
!
! Local scalars:
      integer I,J,K,L
      double precision T
!
! Begin:
      call PRG_manager ('enter', 'DENSITY_Eweighted_OSHF', 'DENSITY_ENERGY_WEIGHTED%OSHF')
!
      call GET_object ('QM', 'CMO', 'OSHF')
!
      if(.not.allocated(PEWD))then
        allocate (PEWD(MATlen))
      else
        deallocate (PEWD)
        allocate (PEWD(MATlen))
      end if
!
      PEWD(1:MATlen)=ZERO
!
! Form the total and energy-weighted density matrices.
      do I=1,CMOA%NoccMO
      L=0
      do J=1,Nbasis
      do K=1,J
      L=L+1
      T=CMOA%coeff(J,I)*CMOA%coeff(K,I)
      PEWD(L)=PEWD(L)-T*CMOA%EIGVAL(I)
      end do ! K
      end do ! J
      end do ! I
!
      IF(CMOB%NoccMO.GT.0)then
        do I=1,CMOB%NoccMO
        L=0
        do J=1,Nbasis
        do K=1,J
        L=L+1
        T=CMOB%coeff(J,I)*CMOB%coeff(K,I)
        PEWD(L)=PEWD(L)-T*CMOB%EIGVAL(I)
        end do ! K
        end do ! J
        end do ! I
      end if
!
! End of routine DENSITY_Eweighted_OSHF
      call PRG_manager ('exit', 'DENSITY_Eweighted_OSHF', 'DENSITY_ENERGY_WEIGHTED%OSHF')
      return
      end
      subroutine DENSITY_Eweighted_GSCF
!**********************************************************************************************
!     Date last modified: March 28, 2001                                                      *
!     Author: R.A. Poirier                                                                    *
!     Description: Calculate the energy-weighted density for GSCF (GVB and ROHF).             *
!**********************************************************************************************
! Modules:
      USE program_constants
      USE QM_defaults
      USE QM_objects

      implicit none
!
! Local scalars:
      integer I,II,IJ,J,K,KK,NF
      double precision SUM
!
! Local work array:
      double precision, dimension(:), allocatable :: SCRVEC
      double precision, dimension(:,:), allocatable :: SCRMAT
!
! Begin:
      call PRG_manager ('enter', 'DENSITY_Eweighted_GSCF', 'DENSITY_ENERGY_WEIGHTED%'//Wavefunction(1:len_trim(Wavefunction)))
!
      call GET_object ('QM', 'CMO', Wavefunction)
!
      if(.not.allocated(PEWD))then
        allocate (PEWD(MATlen), PM0_full(MATlen*NFock))
      else
        deallocate (PEWD, PM0_full)
        allocate (PEWD(MATlen), PM0_full(MATlen*NFock))
      end if
!
      allocate (SCRMAT(Nbasis,Nbasis), SCRVEC(Nbasis))
!
      PEWD=ZERO
      PM0_full=ZERO
      SCRMAT=ZERO
      SCRVEC=ZERO
!
! CMO => CMO_GVB or CMO_ROHF
! Form  PEWD = -2 * CMO * LAGRGM * CMO'.
! First, Y = -LAGRGM * CMO'.
      do I=1,CMO%NoccMO
        do J=1,Nbasis
          SUM=ZERO
          do K=1,CMO%NoccMO
            SUM=SUM-LAGRGM(I,K)*CMO%coeff(J,K)
          end do ! K
          SCRMAT(I,J)=SUM
        end do ! J
      end do ! I
!
! Now,  PEWD = 2 * CMO * Y.
      do I=1,Nbasis
        do K=1,CMO%NoccMO
          SCRVEC(K)=CMO%coeff(I,K)+CMO%coeff(I,K)
        end do ! K
        do J=1,I
          IJ=I*(I-1)/2+J
          SUM=ZERO
          do K=1,CMO%NoccMO
            SUM=SUM+SCRVEC(K)*SCRMAT(K,J)
          end do ! K
          PEWD(IJ)=SUM
        end do ! J
      end do ! I
!
! Now for the 2e density matrix
      NF=NFOCK-1
      II=0
      do I=1,Nbasis
        do J=1,I
! Core part.
          SUM=ZERO
          IF(CMO%NFcoreMO.GT.0)then
            do K=1,CMO%NFcoreMO
              SUM=SUM+CMO%coeff(I,K)*CMO%coeff(J,K)
            end do ! K
          end if
          II=II+1
          PM0_full(II)=SUM+SUM
! Valence and open shell orbital part.
          IF(NF.GT.0)then
            do K=1,NF
              KK=CMO%NFcoreMO+K
              II=II+1
              PM0_full(II)=TWO*CMO%coeff(I,KK)*CMO%coeff(J,KK)
            end do ! K
          end if
        end do ! J
      end do ! I
!
      deallocate (SCRMAT, SCRVEC)
!
! End of routine DENSITY_Eweighted_GSCF
      call PRG_manager ('exit', 'DENSITY_Eweighted_GSCF', 'DENSITY_ENERGY_WEIGHTED%'//trim(Wavefunction))
      return
      end
      subroutine PHASE (A, &
                        Nbasis)
!***********************************************************************
!     Date last modified: June 26, 1992                                *
!     Author: R.A. Poirier                                             *
!     Description: Routine to standardize signs of the MO's.           *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer :: Nbasis
!
! Input array:
      double precision :: A(Nbasis,Nbasis)
!
! Local scalars:
      integer Ibasis,Jbasis
      double precision BIG
!
! Begin:
      call PRG_manager ('enter', 'PHASE', 'UTILITY')
!
      do Jbasis=1,Nbasis
         BIG=A(1,Jbasis)
         do Ibasis=2,Nbasis ! Find largest coefficient for MO J
           if(DABS(A(Ibasis,Jbasis)).gt.DABS(BIG))BIG=A(Ibasis,Jbasis)
         end do ! Ibasis
         if(BIG.lt.0.0D0)then ! Change sign of MO
           do Ibasis=1,Nbasis
             A(Ibasis,Jbasis)=-A(Ibasis,Jbasis)
           end do ! Ibasis
         end if
      end do ! Jbasis
!
! End of routine PHASE
      call PRG_manager ('exit', 'PHASE', 'UTILITY')
      return
      end
      subroutine AO_prod_atomic (Gprod, MATlen, Katom)
!************************************************************************************************************
!     Date last modified: February 18, 2000                                                    Version 2.0  *
!     Authors: James Xidos and R.A. Poirier                                                                 *
!     Description:                                       .                                                  *
!     Calculation of AO products PHI_i(r)*PHI_j(r) at point r (OEPXYZ).                                     *
!************************************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE type_basis_set
      USE QM_defaults

      implicit none
!
      integer, intent(IN) :: Katom,MATlen
! Output array:
      double precision :: Gprod(MATlen)
!
! Local scalars:
      integer :: Ishell,Jshell,Ifrst,Jfrst,Ilast,Jlast
      integer :: Istart,Jstart,Iend,Jend
      integer :: Iatmshl,Jatmshl
      integer :: LAMAX,LBMAX
      integer :: Iatom,Jatom
      integer :: Irange,Jrange
      integer :: Igauss,Jgauss
      integer :: IGbgn,JGbgn,IGend,JGend
      integer :: Iao,Jao,AOI,AOJ
      integer :: I,J,K,DEFCASE,IJao
      double precision :: COEF_SS,EG,EPAB,AS,BS,CX,CY,CZ,SUMDSQ,PX,PY,PZ,DX,DY,DZ,XAC,YAC,ZAC,XBC,YBC,ZBC
      double precision :: XA,XB,YA,YB,ZA,ZB,RABSQ
      logical :: LIatmshl
!
! Local arrays
      double precision SHLINTS(100)
!
! Begin:
      Gprod(1:MATlen)=ZERO

      CX=OEPXYZ(1)
      CY=OEPXYZ(2)
      CZ=OEPXYZ(3)
!
! Begin loop over shells.
! Loop over elemental SHELLs
! Loop over Ishell.
!      AOI=-1
          IJao=0
      do Ishell=1,Basis%Nshells
      LAMAX=Basis%shell(Ishell)%Xtype+1
      Istart=Basis%shell(Ishell)%Xstart
      Iend=Basis%shell(Ishell)%XEND
      Irange=Iend-Istart+1
      IGbgn=Basis%shell(Ishell)%EXPBGN
      IGend=Basis%shell(Ishell)%EXPEND
      Ifrst=Basis%shell(Ishell)%frstSHL
!
! Loop over Jshell.
      do Jshell=1,Ishell
      LBMAX=Basis%shell(Jshell)%Xtype+1
      Jstart=Basis%shell(Jshell)%Xstart
      Jend=Basis%shell(Jshell)%XEND
      Jrange=Jend-Jstart+1
      JGbgn=Basis%shell(Jshell)%EXPBGN
      JGend=Basis%shell(Jshell)%EXPEND
      Jfrst=Basis%shell(Jshell)%frstSHL
!
! Define CASE:
      DEFCASE=4*(LAMAX-1)+LBMAX
!
      Ilast= Basis%shell(Ishell)%lastSHL
      Jlast= Basis%shell(Jshell)%lastSHL

! Loop over all atomic shells
        do Iatmshl=Ifrst,Ilast
          Iatom=Basis%atmshl(Iatmshl)%ATMLST
          if(Iatom.ne.Katom)cycle
          XA=CARTESIAN(Iatom)%X
          YA=CARTESIAN(Iatom)%Y
          ZA=CARTESIAN(Iatom)%Z
!         AOI=Basis%atmshl(Iatmshl)%frstAO-1
!         AOI=AOI+1
!         AOJ=-1
          IF(Ishell.EQ.Jshell)Jlast=Iatmshl
        do Jatmshl=Jfrst,Jlast
          Jatom=Basis%atmshl(Jatmshl)%ATMLST
          if(Jatom.ne.Katom)cycle
          XB=CARTESIAN(Jatom)%X
          YB=CARTESIAN(Jatom)%Y
          ZB=CARTESIAN(Jatom)%Z
!         AOJ=Basis%atmshl(Jatmshl)%frstAO-1
!         AOJ=AOJ+1

          LIatmshl=.false.
          if(Iatmshl.eq.Jatmshl)LIatmshl=.true.

!         write(6,*)'AOI,AOJ: ',AOI,AOJ
          RABSQ=ZERO

          SHLINTS(1:100)=ZERO ! will handle up to <f|f>
! Loop over primitive gaussians
!
        do Igauss=IGbgn,IGend
          AS=Basis%gaussian(Igauss)%exp
        do Jgauss=JGbgn,JGend
          BS=Basis%gaussian(Jgauss)%exp
          EPAB=AS+BS
          PX=(AS*XA+BS*XB)/EPAB
          PY=(AS*YA+BS*YB)/EPAB
          PZ=(AS*ZA+BS*ZB)/EPAB
          DX = PX-CX
          DY = PY-CY
          DZ = PZ-CZ
          SUMDSQ = DX*DX + DY*DY + DZ*DZ

          EG=DEXP(-(AS*BS*RABSQ/EPAB+EPAB*SUMDSQ)) ! ??? combines K and exp
!
          XAC=CX-XA ! XAP-DX
          YAC=CY-YA ! YAP-DY
          ZAC=CZ-ZA ! ZAP-DZ
          XBC=CX-XB ! XBP-DX
          YBC=CY-YB ! YBP-DY
          ZBC=CZ-ZB ! ZBP-DZ
!
          select case (DEFCASE)
! S|S
          case (1)
          COEF_SS = Basis%gaussian(Igauss)%CONTRC*Basis%gaussian(Jgauss)%CONTRC
          SHLINTS(1) = SHLINTS(1)+COEF_SS*EG
          case default
            write(UNIout,*)'ERROR> AO_prod_atomic: DEFCASE type should not exist'
            stop ' ERROR> AO_prod_atomic: DEFCASE type should not exist'
          end select
          end do ! Jgauss
          end do ! Igauss

          IJao=IJao+1
!         write(6,*)'IJao: ',IJao
          Gprod(IJao)=SHLINTS(1)

        end do ! Jatmshl
        end do ! Iatmshl
! End of loop over shells.
!
      end do ! Jshell
      end do ! Ishell
! End of loop over locations.
!
! End of routine AO_prod_atomic
      return
      end
      subroutine DENBLD (CMAT, &
                         OCCVEC, &
                         PMAT, &
                         Nbasis, &
                         MATlen, &
                         NoccMO)
!***********************************************************************
!     Date last modified: July 07, 1992                    Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: Utility to build the density matrix from the M.O.   *
!                  coefficients and occupancy vector.  MUN Version.    *
!***********************************************************************
! Modules:
      USE program_constants

      implicit none
!
! Input scalars:
      integer MATlen,Nbasis,NoccMO
!
! Input arrays:
      double precision CMAT(Nbasis,Nbasis),OCCVEC(Nbasis)
!
! Output array:
      double precision PMAT(MATlen)
!
! Local scalars:
      integer Ibasis,IJ,Jbasis,KMO
      double precision TERM
!
! Begin:
      call PRG_manager ('enter', 'DENBLD', 'UTILITY')
!
! Form the density matrix.
      IJ=0
      do Ibasis=1,Nbasis
      do Jbasis=1,Ibasis
      IJ=IJ+1
      TERM=ZERO
      if(NoccMO.gt.0)then
      do KMO=1,NoccMO
        TERM=TERM+CMAT(Ibasis,KMO)*CMAT(Jbasis,KMO)*OCCVEC(KMO)
      end do
      end if
      PMAT(IJ)=TERM
      end do ! J
      end do ! I
!
! End of routine DENBLD
      call PRG_manager ('exit', 'DENBLD', 'UTILITY')
      return
      end
      subroutine BLD_density_RHF (CMAT, &
                                  PMAT, &
                                  NoccMO)
!***********************************************************************
!     Date last modified: August 2, 2010                   Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: Utility to build the density matrix from the M.O.   *
!                  coefficients for a closed shell system (RHF).       *
!***********************************************************************
! Modules:
      USE program_constants

      implicit none
!
! Input scalars:
      integer :: NoccMO
!
! Input arrays:
      double precision, dimension(:,:) ::  CMAT
!
! Output array:
      double precision, dimension(:) ::  PMAT
!
! Local scalars:
      integer :: Ibasis,IJ,Jbasis,KMO
      integer :: Nbasis
      double precision :: TERM
!
! Begin:
      call PRG_manager ('enter', 'BLD_density_RHF', 'UTILITY')
!
      Nbasis=size(CMAT,1)
!
! Form the density matrix.
      IJ=0
      do Ibasis=1,Nbasis
      do Jbasis=1,Ibasis
      IJ=IJ+1
      TERM=ZERO
      do KMO=1,NoccMO
        TERM=TERM+CMAT(Ibasis,KMO)*CMAT(Jbasis,KMO)
      end do
      PMAT(IJ)=TERM*TWO
      end do ! Jbasis
      end do ! Ibasis
!
! End of routine BLD_density_RHF
      call PRG_manager ('exit', 'BLD_density_RHF', 'UTILITY')
      return
      end subroutine BLD_density_RHF
