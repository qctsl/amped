      subroutine CORRELATION_PARR
!************************************************************************************************************
!     Date last modified: April 1, 2015                                                                     *
!     Author: R. A. Poirier                                                                                 *
!     Description: Compute the terms to calculation the correlation energy using density at the nucleus.    *
!************************************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE QM_objects
      USE QM_defaults
      USE type_molecule

      implicit none
!
! Local scalars:
      integer :: Iatom
      double precision :: rho_0,Ecorr,Sum,Temp
      double precision :: C1,C0
      parameter (C1=-0.0680336, C0= 0.0080354)
!
! Local functions:
      double precision :: TraceAB
      double precision, dimension(:), allocatable :: AOprod
!
! Begin:
      call PRG_manager ('enter', 'CORRELATION_PARR', 'UTILITY')
!
      call get_object ('QM', 'CMO', Wavefunction)

      allocate (AOprod(1:MATlen))
      Ecorr=ZERO
      Sum=ZERO
      do Iatom=1,Natoms
        if(CARTESIAN(Iatom)%Atomic_Number.le.0)cycle
        call AO_products (CARTESIAN(Iatom)%x, CARTESIAN(Iatom)%y, CARTESIAN(Iatom)%z, AOprod, MATlen)
        rho_0=TraceAB (PM0, AOprod, NBasis)
      write(UNIout,'(a,f12.6)')'rho(0): ',rho_0
        Temp=rho_0/CARTESIAN(Iatom)%Atomic_Number**2
        Ecorr=Ecorr+C1*Temp+C0
        Sum=Sum+Temp
      end do
      write(UNIout,'(a,i8)')'The density will be calculated at all the nuclei'
      write(UNIout,'(a,f12.6)')'The total of rho(0)/ZA^2: ',Temp
      write(UNIout,'(a,f12.6)')'Correlation energy: ',Ecorr

      deallocate (AOprod)
!
! End of routine CORRELATION_PARR
      call PRG_manager ('exit', 'CORRELATION_PARR', 'UTILITY')
      return
      end subroutine CORRELATION_PARR
