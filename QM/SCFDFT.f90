      SUBROUTINE BLD_r12(CMO, r12, Nbasis, NOCCMO)
!***********************************************************************
!     Date last modified: December 2, 2004                 Version 1.0 *
!     Author: Joshua Hollett                                           *
!     description: Calculate r12 without calling for MO coeffiecients. *
!***********************************************************************
! Modules:
      USE program_manager
      USE program_defaults
      USE type_one_electron_properties
      USE integrals_1e
      USE constants
      USE matrix_prt
      USE type_symmetry

      implicit none

! Input:
      integer, intent(IN) :: Nbasis,NOCCMO
      double precision, intent(IN) :: CMO(Nbasis,Nbasis)
! Output:
      double precision, intent(OUT) :: r12(Nbasis,Nbasis) 
! Local:
      external Dipole_Mom_ss,Dipole_Mom_sp,Dipole_Mom_pp,Dipole_Mom_sd,Dipole_Mom_pd,Dipole_Mom_dd
      external Second_Mom_ss,Second_Mom_sp,Second_Mom_pp,Second_Mom_sd,Second_Mom_pd,Second_Mom_dd
      double precision, allocatable :: DipMomXMO(:,:),DipMomYMO(:,:),DipMomZMO(:,:)
      double precision, allocatable :: SecMomXXMO(:,:),SecMomYYMO(:,:),SecMomZZMO(:,:)
      double precision, allocatable :: SecMomXYMO(:,:),SecMomYZMO(:,:),SecMomXZMO(:,:)
      integer :: AMO,BMO, matlen
      logical :: Ldebug

! Begin:
      call PRG_manager ('enter', 'BLD_r12', 'UTILITY')
      Ldebug=Local_Debug
      matlen=Nbasis*(Nbasis+1)/2

      if(.not.allocated(DipMomX))then
        allocate(DipMomX(MATLEN))
      end if
      if(.not.allocated(DipMomY))then
        allocate(DipMomY(MATLEN))
      end if
      if(.not.allocated(DipMomZ))then
        allocate(DipMomZ(MATLEN))
      end if
      if(.not.allocated(SecMomXX))then
        allocate(SecMomXX(MATLEN))
      end if
      if(.not.allocated(SecMomXY))then
        allocate(SecMomXY(MATLEN))
      end if
      if(.not.allocated(SecMomXZ))then
        allocate(SecMomXZ(MATLEN))
      end if
      if(.not.allocated(SecMomYY))then
        allocate(SecMomYY(MATLEN))
      end if
      if(.not.allocated(SecMomYZ))then
        allocate(SecMomYZ(MATLEN))
      end if
      if(.not.allocated(SecMomZZ))then
        allocate(SecMomZZ(MATLEN))
      end if

      DipMomX(1:matlen)=ZERO
      DipMomY(1:matlen)=ZERO
      DipMomZ(1:matlen)=ZERO
      SecMomXX(1:matlen)=ZERO
      SecMomXY(1:matlen)=ZERO
      SecMomXZ(1:matlen)=ZERO
      SecMomYY(1:matlen)=ZERO
      SecMomYZ(1:matlen)=ZERO
      SecMomZZ(1:matlen)=ZERO

! Use the CENTER_OF_CHARGE for now
      if(.not.LOEPXYZ_defined)then
        call CENTER_OF_CHARGE (OEPXYZ)
        write(uniout,'(a,3F12.6)')'Property calculated at center of charge: ',OEPXYZ
      else
        write(uniout,'(a,3F12.6)')'Property calculated at user specified point: ',OEPXYZ
      end if
      CX=OEPXYZ(1)
      CY=OEPXYZ(2)
      CZ=OEPXYZ(3)

! Loop over ISHELL.
      DO ISHELL=1,Basis%Nshells
      LAMAX=Basis%shell(ISHELL)%XTYPE+1
      ISTART=Basis%shell(ISHELL)%XSTART
      IEND=Basis%shell(ISHELL)%XEND
      IRANGE=IEND-ISTART+1
      IGBGN=Basis%shell(ISHELL)%EXPBGN
      IGEND=Basis%shell(ISHELL)%EXPEND
      IFRST=Basis%shell(ISHELL)%FRSTSHL

! Define gaussian exponents and contraction coefficients shell
! Loop over JSHELL.
      DO JSHELL=1,ISHELL
      LBMAX=Basis%shell(JSHELL)%XTYPE+1
      JSTART=Basis%shell(JSHELL)%XSTART
      JEND=Basis%shell(JSHELL)%XEND
      JRANGE=JEND-JSTART+1
      JGBGN=Basis%shell(JSHELL)%EXPBGN
      JGEND=Basis%shell(JSHELL)%EXPEND
      JFRST=Basis%shell(JSHELL)%FRSTSHL

! Define gaussian exponents and contraction coefficients shell
      ILAST= Basis%shell(ISHELL)%LASTSHL
      JLAST= Basis%shell(JSHELL)%LASTSHL

! Define CASE:
! (LAMAX,LBMAX)=(1,1) => <s|s> => DEFCASE=1
! (LAMAX,LBMAX)=(1,2) => <s|p> => DEFCASE=2
! (LAMAX,LBMAX)=(2,2) => <p|p> => DEFCASE=6

      DEFCASE=4*(LAMAX-1)+LBMAX

      select case(DEFCASE)

      CASE(1)
      call Moment_Int (Dipole_Mom_ss,Second_Mom_ss)

      CASE(2)
      call Moment_Int (Dipole_Mom_sp,Second_Mom_sp)

      CASE(6)
      call Moment_Int (Dipole_Mom_pp,Second_Mom_pp)

      CASE(3)
      call Moment_Int (Dipole_Mom_sd,Second_Mom_sd)

      CASE(7)
      call Moment_Int (Dipole_Mom_pd,Second_Mom_pd)

      CASE(11)
      call Moment_Int (Dipole_Mom_dd,Second_Mom_dd)

      case default
        write(uniout,*)'ERROR> SECOND_MOMENT_ETOT: invalid case'
        stop 'ERROR> SECOND_MOMENT_ETOT: invalid case'
      end select

      end do ! Jshell
      end do ! Ishell
! End of loop over locations.
!
      allocate(DipMomXMO(Nbasis,Nbasis),DipMomYMO(Nbasis,Nbasis),DipMomZMO(Nbasis,Nbasis))
! Calculate contribution to dipole moment per MO
      call I1E_transform(DipMomX,CMO,DipMomXMO,Nbasis,Nbasis,matlen)
      call I1E_transform(DipMomY,CMO,DipMomYMO,Nbasis,Nbasis,matlen)
      call I1E_transform(DipMomZ,CMO,DipMomZMO,Nbasis,Nbasis,matlen)
!      
      allocate(SecMomXXMO(Nbasis,Nbasis),SecMomYYMO(Nbasis,Nbasis),SecMomZZMO(Nbasis,Nbasis))
      allocate(SecMomXYMO(Nbasis,Nbasis),SecMomYZMO(Nbasis,Nbasis),SecMomXZMO(Nbasis,Nbasis))
! Calculate contribution to second moment per MO
      call I1E_transform(SecMomXX,CMO,SecMomXXMO,Nbasis,Nbasis,matlen)
      call I1E_transform(SecMomYY,CMO,SecMomYYMO,Nbasis,Nbasis,matlen)
      call I1E_transform(SecMomZZ,CMO,SecMomZZMO,Nbasis,Nbasis,matlen)
      call I1E_transform(SecMomXY,CMO,SecMomXYMO,Nbasis,Nbasis,matlen)
      call I1E_transform(SecMomYZ,CMO,SecMomYZMO,Nbasis,Nbasis,matlen)
      call I1E_transform(SecMomXZ,CMO,SecMomXZMO,Nbasis,Nbasis,matlen)
!
! Calculate dr12 for each ij MO pair
      do AMO=1,Nbasis
        do BMO=1,Nbasis
          r12(AMO,BMO)=dsqrt(SecMomXXMO(AMO,AMO)+SecMomXXMO(BMO,BMO)-2.0*DipMomXMO(AMO,AMO)*DipMomXMO(BMO,BMO)&
                       +SecMomYYMO(AMO,AMO)+SecMomYYMO(BMO,BMO)-2.0*DipMomYMO(AMO,AMO)*DipMomYMO(BMO,BMO)&
                       +SecMomZZMO(AMO,AMO)+SecMomZZMO(BMO,BMO)-2.0*DipMomZMO(AMO,AMO)*DipMomZMO(BMO,BMO))
        end do
      end do
!                                  
      deallocate(SecMomXYMO,SecMomXZMO,SecMomYZMO)
      deallocate(SecMomXXMO,SecMomYYMO,SecMomZZMO)
      deallocate(DipMomXMO,DipMomYMO,DipMomZMO)
      deallocate(SecMomXX,SecMomYY,SecMomZZ)
      deallocate(SecMomXY,SecMomXZ,SecMomYZ)
      deallocate(DipMomX,DipMomY,DipMomZ)
!
      call PRG_manager ('exit', 'BLD_r12', 'UTILITY')
!
      END SUBROUTINE BLD_r12
