      subroutine BLD_r12_ab
!***********************************************************************
!     Date last modified: July 30, 2010                    Version 1.0 *
!     Author: Joshua Hollett and R.A. Poirier                          *
!     description: Calculate r12 over MO for occupied MO only          *
!***********************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE INT_objects
      USE matrix_print

      implicit none
!
! Local Scalars:
      integer :: AMO,BMO,NoccMO
      double precision :: temp
      double precision, dimension(:,:), allocatable :: r12_ab

      OEPXYZ=ZERO
      call GET_object ('INT', 'MOMENT1', 'AO')
      call GET_object ('QM', 'CMO', Wavefunction)
      NoccMO=CMO%NoccMO

      allocate (r12_ab(NoccMO,NoccMO))

      allocate(DipoleX_MO(Nbasis,Nbasis),DipoleY_MO(Nbasis,Nbasis),DipoleZ_MO(Nbasis,Nbasis))
      allocate(SecMomXX_MO(Nbasis,Nbasis),SecMomYY_MO(Nbasis,Nbasis),SecMomZZ_MO(Nbasis,Nbasis))
      allocate(SecMomXY_MO(Nbasis,Nbasis),SecMomYZ_MO(Nbasis,Nbasis),SecMomXZ_MO(Nbasis,Nbasis))

! Calculate contribution to dipole moment per MO
      call I1E_transform (DipoleX_AO, CMO%coeff, DipoleX_MO, Nbasis, Nbasis, matlen)
      call I1E_transform (DipoleY_AO, CMO%coeff, DipoleY_MO, Nbasis, Nbasis, matlen)
      call I1E_transform (DipoleZ_AO, CMO%coeff, DipoleZ_MO, Nbasis, Nbasis, matlen)

! Calculate contribution to second moment per MO
      call I1E_transform (SecMomXX_AO, CMO%coeff, SecMomXX_MO, Nbasis, Nbasis, matlen)
      call I1E_transform (SecMomYY_AO, CMO%coeff, SecMomYY_MO, Nbasis, Nbasis, matlen)
      call I1E_transform (SecMomZZ_AO, CMO%coeff, SecMomZZ_MO, Nbasis, Nbasis, matlen)
      call I1E_transform (SecMomXY_AO, CMO%coeff, SecMomXY_MO, Nbasis, Nbasis, matlen)
      call I1E_transform (SecMomYZ_AO, CMO%coeff, SecMomYZ_MO, Nbasis, Nbasis, matlen)
      call I1E_transform (SecMomXZ_AO, CMO%coeff, SecMomXZ_MO, Nbasis, Nbasis, matlen)
!
! Calculate dr12 for each ij MO pair
      do AMO=1,NoccMO
        do BMO=1,NoccMO
          temp=SecMomXX_MO(AMO,AMO)+SecMomXX_MO(BMO,BMO)-2.0*DipoleX_MO(AMO,AMO)*DipoleX_MO(BMO,BMO)&
              +SecMomYY_MO(AMO,AMO)+SecMomYY_MO(BMO,BMO)-2.0*DipoleY_MO(AMO,AMO)*DipoleY_MO(BMO,BMO)&
              +SecMomZZ_MO(AMO,AMO)+SecMomZZ_MO(BMO,BMO)-2.0*DipoleZ_MO(AMO,AMO)*DipoleZ_MO(BMO,BMO)
          if(temp.ge.ZERO)then
            r12_ab(AMO,BMO)=dsqrt(temp)
          else
            r12_ab(AMO,BMO)=ZERO
            write(UNIout,'(a,2i4,a)')'Value for, ',AMO,BMO,' is negative'
          end if
        end do ! NoccMO
      end do ! NoccMO
!                                  
      write(UNIout,*)'Average inter-electron distance:'
      call PRT_matrix (r12_ab, NoccMO, NoccMO)

      call PRG_manager ('exit', 'BLD_r12_ab', 'UTILITY')
      return
      end subroutine BLD_r12_ab
