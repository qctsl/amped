      subroutine I2E_OVOV
!***********************************************************************
!     Date last modified: October 25, 1993                 Version 1.0 *
!     Author: R. A. Poirier                                            *
!     Description: Transform two-electron integrals.                   *
!     First, form  (PJ/KL) = SUM C(P,I)*(IJ/KL), where J, K and L      *
!     extend over all AO, and P over the maximum number of MO.         *
!     Next, form (PQ/KL) followed by (PQ/RL) and then (PQ/RS).         *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE integral_transformation
      USE QM_defaults
      USE QM_objects
      USE matrix_print

      implicit none
!
! Local scalars:
      integer I,IPASS,IPQRS,IPSRQ,Jao,JKLNDX,JKNDX,JNDX,Kao,KL,Lao,M,MO1,MO2,N,NPASS,PMO,NPJKL,PQINDX,PSINDX,QMO,RMO, &
              RQINDX,RSINDX,SHIFT,SMO,SMOMX
      logical PQVVorOO,RSVVorOO
!
! Local arrays:
      type (ints_2e_ijkl), dimension(:), allocatable ::  PQRS_ovov
!
! Begin:
      call PRG_manager ('enter', 'I2E_OVOV', '2EINT%MO_OVOV')
!
      call GET_object ('INT', '2EINT', 'RAW')
      call GET_object ('QM', 'CMO', 'RHF') ! Only RHF
      call BLD_CMO_ov
!
! Calculate the total space required for a single pass.
      NJKL=Nbasis*Nbasis*(Nbasis+1)/2
      if(NJKL.GT.MAX_transform)then
        write(UNIout,'(a)')'ERROR> I2ETRN:'
        write(UNIout,'(a)')' TOO MANY MO FOR TRANSFORMATION WITH CURRENT MAX_transform'
        write(UNIout,'(a)')'MAX_transform should be at least Nbasis*Nbasis*(Nbasis+1)/2'
        stop 'ERROR> I2ETRN: TOO MANY MO FOR TRANSFORMATION'
      end if
! Determine the number of passes required.
      NTMOMX=MIN0(NTROMO, MAX_transform/NJKL)
      NPASS=(NTROMO-1)/NTMOMX+1
      write(UNIout,1000)NPASS,NTMOMX
 1000 FORMAT(1X,'TWO-ELECTRON TRANSFORMATION:',/3X,'NUMBER OF PASSES REQUIRED: ',I4,/3X,'MAXIMUM MO PER PASS: ',I4)
!
      NPJKL=NTMOMX*NJKL

      PQMAX=NTROMO*NTRVMO
      PQRSMX=PQMAX*(PQMAX+1)/2

      if(.not.allocated(PQRS_ovov))then
        allocate (PQRS_ovov(PQRSMX))
      end if
!
      allocate (BPQMO(Nbasis,Nbasis), SCRVEC(Nbasis), NCODE(Nbasis), BPQRS(NTMOMX*NJKL))

      integral_combinations=.false.
!
!      if(UHFOPN.or.RHFOPN)then
!        write(UNIout,'(1H0,A)')'ERROR> I2E_OVOV: Integral transformation NOT POSSIBLE for UHF wavefunctions'
!        STOP
!      end if
!
! Set indexing arrays.
      allocate (IJNDX(Nbasis+1))
      call BLD_IJNDX (IJNDX, Nbasis)
      do I=1,Nbasis
        NCODE(I)=MATlen*(I-1)
      end do ! I
!
      NPQRS=0
      SHIFT=0
      do IPASS=1,NPASS
!
      MO1=(IPASS-1)*NTMOMX+1
      MO2=MIN0 (IPASS*NTMOMX, NTROMO)
!
      do M=1,NTMOMX*NJKL
        BPQRS(M)=ZERO
      end do ! M
!
      call I2EPMO (CMO_occ%coeff, MO1, MO2, NTROMO)
!
! End of atomic integral input - Start next transformation step
! for these MO.  Should have all (Pj,kl).
      PQVVorOO=.false.
      call I2EQMO (CMO_vrt%coeff, MO1, MO2, NTRVMO, PQVVorOO)
!
! Have all (PQ,kl) for all PQ and all kl.
      RSVVorOO=.false.
      call I2RSMO (CMO_occ%coeff, CMO_vrt%coeff, MO1, MO2, NTROMO, NTRVMO, PQVVorOO, RSVVorOO)
!
! Now have all MO integrals (PQ,RS) in BPQRS.
      do PMO=MO1,MO2
      do QMO=1,NTRVMO
      do RMO=1,PMO
!
      SMOMX=NTRVMO
      if(PMO.EQ.RMO)SMOMX=QMO
      do SMO=1,SMOMX

      PQINDX=(PMO-1)*NTRVMO+QMO
      RSINDX=(RMO-1)*NTRVMO+SMO
      PSINDX=(PMO-1)*NTRVMO+SMO
      RQINDX=(RMO-1)*NTRVMO+QMO

      IPQRS=PQINDX*(PQINDX-1)/2+RSINDX-SHIFT
      IPSRQ=PSINDX*(PSINDX-1)/2+RQINDX-SHIFT
      if(DABS(BPQRS(IPQRS)).GT.1.0D-06.or.DABS(BPQRS(IPSRQ)).GT.1.0D-06)then
        NPQRS=NPQRS+1
        if(PMO.NE.RMO)then
        PQRS_ovov(NPQRS)%ijkl=BPQRS(IPQRS)
        PQRS_ovov(NPQRS)%ikjl=BPQRS(IPSRQ)
        else
        PQRS_ovov(NPQRS)%ijkl=BPQRS(IPQRS)
        PQRS_ovov(NPQRS)%ikjl=BPQRS(IPQRS)
        end if
        PQRS_ovov(NPQRS)%I=PMO
        PQRS_ovov(NPQRS)%J=QMO
        PQRS_ovov(NPQRS)%K=RMO
        PQRS_ovov(NPQRS)%L=SMO
        write(UNIout,'(1X,5I4,F12.6,2X,F12.6)')PMO,QMO,RMO,SMO,NPQRS, &
                         PQRS_ovov(NPQRS)%ijkl,PQRS_ovov(NPQRS)%ikjl
      end if
      end do ! End SMO loop.
      end do ! End RMO loop.
      end do ! End QMO loop.
      end do ! End PMO loop.

      PQMAX=(MO2-1)*NTRVMO+NTRVMO
      SHIFT=PQMAX*(PQMAX-1)/2+PQMAX
!
      end do ! IPASS
!
      write(UNIout,'(a,i10,a)')' TRANSFORMATION COMPLETE ',NPQRS,' INTEGRALS KEPT'
!
      deallocate (BPQMO, SCRVEC, NCODE, BPQRS)
      deallocate (IJNDX)

! End of routine I2E_OVOV
      call PRG_manager ('exit', 'I2E_OVOV', '2EINT%MO_OVOV')
      return
      END
