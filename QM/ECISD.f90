      subroutine E_CISD
!*****************************************************************
!     Date last modified: May 5, 2009                            *
!     Author: J.W. Hollett                                       *
!     Description:  Obtain CISD energy and coefficients using a  *
!                   Newton-Rhapson minimization.                 *
!*****************************************************************
!Modules:
      USE QM_objects

      implicit none

!
! Local scalars: 
      integer :: NoccMO
      integer :: Nsingles,Ndoubles,NOdoubles,NPdoubles,Nvirtuals
      double precision :: E_0,c_0,ECISD
! Local arrays:
      double precision, allocatable, dimension(:,:) :: F_ab, c_sing
      double precision, allocatable, dimension(:,:,:,:) :: INT_ijkl,c_par,c_opp
! NEEDS to be re-written in terms of configurations: singles, doubles, ...
      type Singles
        integer(kind=4) :: a
        integer(kind=4) :: r
        real(8) :: Car
      end type
      type Doubles
        integer(kind=4) :: a
        integer(kind=4) :: b
        integer(kind=4) :: r
        integer(kind=4) :: s
        real(8) :: Cabrs
      end type
      type (Singles), dimension(:), allocatable :: CS
      type (Doubles), dimension(:), allocatable :: CD
!
! Begin:
      call PRG_manager ('enter', 'E_CISD', 'UTILITY')
!
      call GET_object ('QM', 'CMO', 'RHF')

      NoccMO=CMO%NoccMO
      Nvirtuals=Nbasis-NoccMO
      Nsingles=NoccMO*Nvirtuals
      NPdoubles=NoccMO*Nvirtuals
      NOdoubles=NoccMO*Nvirtuals
      Ndoubles=NPdoubles+NOdoubles
      E_0=ENERGY%total
      write(6,*)'Nbasis: ',Nbasis
      write(6,*)'NoccMO: ',NoccMO
      write(6,*)'E_0: ',E_0

! Determine configurations and number of coefficients
      allocate(c_sing(NoccMO,NoccMO+1:Nbasis),c_par(NoccMO,NoccMO,NoccMO+1:Nbasis,NoccMO+1:Nbasis))
      allocate(c_opp(NoccMO,NoccMO,NoccMO+1:Nbasis,NoccMO+1:Nbasis))
      allocate(CS(Nsingles), CD(Ndoubles))

! Initialize values of coefficients
      c_sing=0.0D0 ! single excitations
      c_par=0.0D0 ! parallel spin double excitations
      c_opp=0.0D0 ! opposite spin double excitations
! Enter coefficient values for testing
      c_sing(1,2)=-0.0047091D0
      c_sing(1,3)=0.0023188D0
      CS(1)%a=1 
      CS(1)%r=2 
      CS(1)%car=-0.0047091D0
      CS(2)%a=1 
      CS(2)%r=3 
      CS(2)%car=0.0023188D0
      c_opp(1,1,2,2)=-0.0573735D0
      c_opp(1,1,2,3)=-0.0182794D0
      c_opp(1,1,3,2)=-0.0182794D0
      c_opp(1,1,3,3)=-0.0161064D0
      CD(1)%a=1 
      CD(1)%b=1 
      CD(1)%r=2 
      CD(1)%s=2 
      CD(1)%cabrs=-0.0573735D0
      CD(2)%a=1 
      CD(2)%b=1 
      CD(2)%r=2 
      CD(2)%s=3 
      CD(2)%cabrs=-0.0182794D0
      CD(3)%a=1 
      CD(3)%b=1 
      CD(3)%r=3 
      CD(3)%s=2 
      CD(3)%cabrs=-0.0182794D0
      CD(4)%a=1 
      CD(4)%b=1 
      CD(4)%r=3 
      CD(4)%s=3 
      CD(4)%cabrs=-0.0161064D0
! Calculate value of c_0 from normalization
      call c_0_calc
!
! Get two-electron integrals, <ij|kl>
      allocate(INT_ijkl(Nbasis,Nbasis,Nbasis,Nbasis))
      call BLD_INT_ijkl (INT_ijkl, Nbasis) 

! Transform Fock Matrix over MOs
      allocate(F_ab(Nbasis,Nbasis))
      call I1E_transform (FockM, CMO, F_ab, Nbasis, Nbasis, MATlen)
!
! Calculate CISD energy
      call E_CISD_calc
!
      write(6,'(a,F14.8)')'ECISD= ',ECISD
!
      call PRG_manager ('exit', 'E_CISD', 'UTILITY')
!
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine BLD_INT_ijkl(INT_ijkl, basisN)
!***********************************************************************
!     Date last modified: May 4, 2009                                  *
!     Author: J.W. Hollett                                             *
!     Description: Transform two-electron integrals.                   *
!                  Put integrals in array INT_ijkl, which is labeled   *
!                  according to physicist notation, <ij|kl>.           *
!***********************************************************************
! Modules:
      USE program_constants
      USE integral_transformation

      implicit none
!
! Input scalar:
      integer, intent(IN) :: basisN ! basisN=Nbasis, but can't use Nbasis because it is already in intran  
! Output Arrays:
      double precision,intent(OUT) :: INT_ijkl(basisN,basisN,basisN,basisN)
! Local scalars:
      integer :: IMO,JMO,KMO,Ilmo,IPQRS
!
! Begin:
      call PRG_manager ('enter', 'BLD_INT_ijkl', 'UTILITY')
!
      write(6,*)'call I2E_AOtoMO '
      call GET_object ('INT', '2EINT', 'MO')
!     call I2E_AOtoMO
!
       write(6,*)'TNPQRS ',TNPQRS
!      write(6,*)'NPQRS ',NPQRS
      do IPQRS=1,TNPQRS
        IMO=PQRS_MOINT(IPQRS)%I
        JMO=PQRS_MOINT(IPQRS)%J
        KMO=PQRS_MOINT(IPQRS)%K
        Ilmo=PQRS_MOINT(IPQRS)%L
!
        INT_ijkl(IMO,KMO,JMO,Ilmo)=PQRS_MOINT(IPQRS)%ijkl
!
        if(JMO.ne.KMO)then
          INT_ijkl(IMO,JMO,KMO,Ilmo)=PQRS_MOINT(IPQRS)%ikjl
!
          ! Fill in integrals equivalent to <ij|kl>
          INT_ijkl(KMO,Ilmo,IMO,JMO)=INT_ijkl(IMO,JMO,KMO,Ilmo) ! <kl|ij>
          INT_ijkl(IMO,Ilmo,KMO,JMO)=INT_ijkl(IMO,JMO,KMO,Ilmo) ! <il|kj>
          INT_ijkl(KMO,JMO,IMO,Ilmo)=INT_ijkl(IMO,JMO,KMO,Ilmo) ! <kj|il>
          INT_ijkl(JMO,IMO,Ilmo,KMO)=INT_ijkl(IMO,JMO,KMO,Ilmo) ! <ji|lk>
          INT_ijkl(Ilmo,KMO,JMO,IMO)=INT_ijkl(IMO,JMO,KMO,Ilmo) ! <lk|ji>
          INT_ijkl(Ilmo,IMO,JMO,KMO)=INT_ijkl(IMO,JMO,KMO,Ilmo) ! <li|jk>
          INT_ijkl(JMO,KMO,Ilmo,IMO)=INT_ijkl(IMO,JMO,KMO,Ilmo) ! <jk|li>
        end if
        ! Fill in integrals equivalent to <ik|jl>
        INT_ijkl(JMO,Ilmo,IMO,KMO)=INT_ijkl(IMO,KMO,JMO,Ilmo) ! <jl|ik>
        INT_ijkl(IMO,Ilmo,JMO,KMO)=INT_ijkl(IMO,KMO,JMO,Ilmo) ! <il|jk>
        INT_ijkl(JMO,KMO,IMO,Ilmo)=INT_ijkl(IMO,KMO,JMO,Ilmo) ! <jk|il>
        INT_ijkl(KMO,IMO,Ilmo,JMO)=INT_ijkl(IMO,KMO,JMO,Ilmo) ! <ki|lj>
        INT_ijkl(Ilmo,JMO,KMO,IMO)=INT_ijkl(IMO,KMO,JMO,Ilmo) ! <lj|ki>
        INT_ijkl(KMO,JMO,Ilmo,IMO)=INT_ijkl(IMO,KMO,JMO,Ilmo) ! <kj|li>
        INT_ijkl(Ilmo,IMO,KMO,JMO)=INT_ijkl(IMO,KMO,JMO,Ilmo) ! <li|kj>
      end do
!
      write(6,'(a)')'INT_ijkl'
      do IMO=1,basisN
        do JMO=1,basisN
          do KMO=1,basisN
            do Ilmo=1,basisN
              write(6,'(4I2,F12.6)')IMO,JMO,KMO,Ilmo,INT_ijkl(IMO,JMO,KMO,Ilmo)
            end do
          end do
        end do
      end do 
!
! End of routine BLD_INT_ijkl
      call PRG_manager ('exit', 'BLD_INT_ijkl', 'UTILITY')
      end subroutine BLD_INT_ijkl
!
      subroutine E_CISD_calc
!***********************************************************************
!     Date last modified: June 17, 2009                                *
!     Author: R.A. Poirier                                             *
!     Description: Calculate the CISD energy from orbitals energies    *
!                  two-electron integrals.                             *
!***********************************************************************
! Modules:
!
      implicit none
!
! Input scalar:
! Input arrays:      
! Output scalars:
!
! Local scalars:
      integer :: AMO,BMO,CMO,DMO,RMO,SMO,TMO,UMO
      integer :: Isingle,Jsingle,Idouble,Jdouble
      double precision :: D_abrs,D_arbs,D_arst,D_abcr,D_abcd,D_rstu
      double precision :: D_ar,D_ab,D_rs
      double precision :: CSar,CSbs,CDabrs,CDcdtu
!
! Begin:
      call PRG_manager ('enter', 'E_CISD_calc', 'UTILITY')
!
! Calculate CISD energy
      ECISD=E_0
      write(6,'(a,F14.8)')'ECISD= ',ECISD
      write(6,'(a,F14.8)')'c_0= ',c_0
      do Isingle=1,NSingles
        AMO=CS(Isingle)%a
        RMO=CS(Isingle)%r
        CSar=CS(Isingle)%Car
          ECISD=ECISD+2.0D0*CSar**2*(F_ab(RMO,RMO)-F_ab(AMO,AMO))
          D_ar=0.0D0
      do Jsingle=Isingle,NSingles
        BMO=CS(Jsingle)%a
        SMO=CS(Jsingle)%r
        CSbs=CS(Jsingle)%Car
! <S|S> Contributions
         D_abrs=4.0D0*CSar*CSbs
         D_arbs=2.0D0*CSar*CSbs

      do IDouble=1,NDoubles
        AMO=CD(Idouble)%a
        BMO=CD(Idouble)%b
        RMO=CD(Idouble)%r
        SMO=CD(Idouble)%s
        CDabrs=CD(Idouble)%Cabrs
! <S|D> Contributions
        D_abrs=D_abrs+2.0D0*c_0*CDabrs
!       D_ar=D_ar+1.5D0*c_par(AMO,BMO,RMO,SMO)**2 ! C_par=0.0????
      do JDouble=Idouble,NDoubles
        CMO=CD(Jdouble)%a
        DMO=CD(Jdouble)%b
        TMO=CD(Jdouble)%r
        UMO=CD(Jdouble)%s
        CDcdtu=CD(Jdouble)%Cabrs
! <D|D> Contributions
                  D_abrs=D_abrs+2.0D0*c_opp(BMO,CMO,SMO,TMO)*c_opp(AMO,CMO,RMO,TMO)
                  D_arbs=D_arbs+2.0D0*c_opp(CMO,BMO,RMO,TMO)*c_opp(CMO,AMO,SMO,TMO)
              ECISD=ECISD+D_abrs*INT_ijkl(AMO,BMO,RMO,SMO)-D_arbs*INT_ijkl(AMO,RMO,BMO,SMO)
              ECISD=ECISD+(0.5D0*c_par(AMO,BMO,RMO,SMO)**2+c_opp(AMO,BMO,RMO,SMO)**2)&
                   *(F_ab(RMO,RMO)+F_ab(SMO,SMO)-F_ab(AMO,AMO)-F_ab(BMO,BMO))
            end do ! Jdouble
          end do ! Idouble
!         ECISD=ECISD+D_ar*(INT_ijkl(AMO,RMO,RMO,AMO)-INT_ijkl(AMO,RMO,AMO,RMO))
        end do ! Jsingle
      end do ! Isingle
!
      write(6,'(a,F14.8)')'ECISD= ',ECISD
!
      do AMO=1, NoccMO
        do RMO=NoccMO+1, Nbasis
          do SMO=NoccMO+1, Nbasis
            do TMO=NoccMO+1, Nbasis
              D_arst=0.0D0
              do BMO=1, NoccMO
                D_arst=D_arst+4.0D0*c_sing(BMO,RMO)*c_par(AMO,BMO,SMO,TMO)+4.0D0*c_sing(BMO,RMO)*c_opp(BMO,AMO,TMO,SMO) 
              end do ! BMO
              ECISD=ECISD+D_arst*INT_ijkl(AMO,RMO,SMO,TMO)
            end do ! TMO
          end do ! SMO
        end do ! RMO
      end do ! AMO
!
      write(6,'(a,F14.8)')'ECISD= ',ECISD
!
      do AMO=1, NoccMO
        do BMO=1, NoccMO
          do CMO=1, NoccMO
            do RMO=NoccMO+1, Nbasis
              D_abcr=0.0D0
              do SMO=NoccMO+1, Nbasis
                D_abcr=D_abcr+4.0D0*c_sing(CMO,SMO)*c_par(AMO,BMO,RMO,SMO)-4.0D0*c_sing(CMO,SMO)*c_opp(AMO,BMO,SMO,RMO)
              end do ! SMO
              ECISD=ECISD+D_abcr*INT_ijkl(AMO,BMO,CMO,RMO)
            end do ! RMO
          end do ! CMO
        end do ! BMO
      end do ! AMO
!
      write(6,'(a,F14.8)')'ECISD= ',ECISD
!
      do RMO=NoccMO+1, Nbasis
        do SMO=NoccMO+1, Nbasis
          do TMO=NoccMO+1, Nbasis
            do UMO=NoccMO+1, Nbasis
              D_rstu=0.0D0
              do AMO= 1, NoccMO
                do BMO=1, NoccMO
                  D_rstu=D_rstu+0.5D0*c_par(AMO,BMO,RMO,SMO)*c_par(AMO,BMO,TMO,UMO)+c_opp(AMO,BMO,RMO,SMO)*c_opp(AMO,BMO,TMO,UMO)
                end do ! BMO
              end do ! AMO
              ECISD=ECISD+D_rstu*INT_ijkl(RMO,SMO,TMO,UMO)
            end do ! UMO
          end do ! TMO 
        end do ! SMO
      end do ! RMO 
!
      write(6,'(a,F14.8)')'ECISD= ',ECISD
!
      do AMO=1, NoccMO
        do BMO=1, NoccMO
          do CMO=1, NoccMO
            do DMO=1, NoccMO
              D_abcd=0.0D0
              do RMO=NoccMO+1, Nbasis
                do SMO=NoccMO+1, Nbasis
                  D_abcd=D_abcd+0.5D0*c_par(AMO,BMO,RMO,SMO)*c_par(CMO,DMO,RMO,SMO)+c_opp(AMO,BMO,RMO,SMO)*c_opp(CMO,DMO,RMO,SMO) 
                end do ! SMO
              end do ! RMO
              ECISD=ECISD+D_abcd*INT_ijkl(AMO,BMO,CMO,DMO)
            end do ! DMO
          end do ! CMO
        end do ! BMO
      end do ! AMO
! 
      write(6,'(a,F14.8)')'ECISD= ',ECISD
!
      do AMO=1, NoccMO
        do BMO=1, NoccMO
          D_ab=0.0D0
          do RMO=NoccMO+1, Nbasis
            do SMO=NoccMO+1, Nbasis
              D_ab=D_ab+3.0D0/8.0D0*c_par(AMO,BMO,RMO,SMO)**2
            end do ! SMO
          end do ! RMO
          ECISD=ECISD-D_ab*(INT_ijkl(AMO,BMO,AMO,BMO)-INT_ijkl(AMO,BMO,BMO,AMO))
        end do ! BMO
      end do ! AMO
!
      write(6,'(a,F14.8)')'ECISD= ',ECISD
!
      do RMO=NoccMO+1, Nbasis
        do SMO=NoccMO+1, Nbasis
          D_rs=0.0D0
          do AMO=1, NoccMO
            do BMO=1, NoccMO
              D_rs=D_rs+3.0D0/8.0D0*c_par(AMO,BMO,RMO,SMO)**2
            end do ! BMO
          end do ! AMO
          ECISD=ECISD+D_rs*(INT_ijkl(RMO,SMO,RMO,SMO)-INT_ijkl(RMO,SMO,SMO,RMO))
        end do ! SMO
      end do ! RMO
!
! End of routine E_CISD_calc
      call PRG_manager ('exit', 'E_CISD_calc', 'UTILITY')
      end subroutine E_CISD_calc
!
      subroutine c_0_calc
!***********************************************************************
!     Date last modified: June 17, 2009                                *
!     Author: R.A. Poirier                                             *
!     Description: Calculate the CISD energy from orbitals energies    *
!                  two-electron integrals.                             *
!***********************************************************************
! Modules:
!
      implicit none
!
! Input scalar:
! Input arrays:      
! Output scalars:
!
! Local scalars:
      integer :: AMO,BMO,RMO,SMO
!
! Begin:
      call PRG_manager ('enter', 'c_0_calc', 'UTILITY')
!
! Calculate CISD energy
      c_0=1.0D0
      do AMO=1, NoccMO
        do RMO= NoccMO+1,Nbasis
          c_0=c_0-2.0D0*c_sing(AMO,RMO)**2
          do BMO=1,NoccMO
            do SMO= NoccMO+1,Nbasis
              c_0=c_0-0.5D0*c_par(AMO,BMO,RMO,SMO)**2-c_opp(AMO,BMO,RMO,SMO)**2
            end do
          end do
        end do
      end do
!
      c_0=dsqrt(c_0)
!
! End of routine c_0_calc
      call PRG_manager ('exit', 'c_0_calc', 'UTILITY')
      end subroutine c_0_calc
!
      end subroutine E_CISD
