      MODULE type_epairs
!*****************************************************************************************************************
!     Date last modified: February 16, 2000                                                         Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Type molecule orbitals.                                                                       *
!*****************************************************************************************************************
! Modules:

      implicit none
!
      type electron_pairs
        integer :: AtomA                   ! Atom A of MO (electron pair)
        integer :: AtomB                   ! Atom A of MO (electron pair)
        integer :: AtomC                   ! Atom A of MO (electron pair)
        character(len=16) :: type          ! Electron pair type (sigma bond, lone pair, ...)
      end type electron_pairs

      end MODULE type_epairs
