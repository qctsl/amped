      integer function DEFINE_Mtype (Iatmshl, Jatmshl, Katmshl, Latmshl)
      implicit none
!
! Input scalars:
      integer Iatmshl,Jatmshl,Katmshl,Latmshl,Mtype

      Mtype=1
      IF(Iatmshl.EQ.Jatmshl)Mtype=Mtype+4
      IF(Jatmshl.EQ.Katmshl)Mtype=Mtype+2
      IF(Katmshl.EQ.Latmshl)Mtype=Mtype+1
      DEFINE_Mtype=Mtype
!
      return
      END
      subroutine GDFCLC
!***********************************************************************
!     Date last modified: August 18, 1993                  Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description:  Based on gaussIAN 80,                              *
!     Two-electron integral derivative package for SPD functions,      *
!     using the method of Rys polynomials.                             *
!     Normally only derivatives involving D shells are evaluated here  *
!     but this routine is capable of evaluating all the derivatives.   *
!                                                                      *
!     Note: The two-electron energy component (VEEXX) cannot be        *
!     evaluated, as all integrals where all four shells are on the     *
!     same center are skipped.                                         *
!                                                                      *
!     The mode of operation is determined by LGRDSP  -                 *
!     If program GSPCLC was called during this overlay, LGRDSP=.TRUE.  *
!     GDFCLC assumes that all SP derivatives have already been         *
!     evaluated else all the derivatives are evaluated here.           *
!                                                                      *
!     The order of the functions produced is:                          *
!      1      S                                                        *
!      2      X                                                        *
!      3      Y                                                        *
!      4      Z                                                        *
!      5      X**2     D(0)   3*Z**2-R**2                              *
!      6      Z**2     D(1,+) X*Z                                      *
!      7      Z**2     D(1,-) Y*Z                                      *
!      8      X*Y      D(2,+) X**2-Y**2                                *
!      9      X*Z      D(2,-) X*Y                                      *
!     10      Y*Z                                                      *
!     11      X**3                           F(0)   Z*(5*Z**2-3*R**2)  *
!     12      Y**3                           F(1,+) X*(5*Z**2-R**2)    *
!     13      Z**3                           F(1,-) Y*(5*Z**2-R**2)    *
!     14      X*Y**2                         F(2,+) Z*(X**2-Y**2)      *
!     15      X**2*Y                         F(2,-) Z*(X*Y)            *
!     16      X**2*Z                         F(3,+) X*(X**2-3*Y**2)    *
!     17      X*Z**2                         F(3,-) Y*(3*X**2-Y**2)    *
!     18      Y*Z**2                                                   *
!     19      Y**2*Z                                                   *
!     20      X*Y*Z                                                    *
!                                                                      *
!     Note: Much of the code for F orbital derivatives is present,     *
!     but the block data BD704 will have to be extended, as will       *
!     arrays associated with Subroutine RPOLX.  There may also be      *
!     other array dimensions that need to be fixed (e.g., D1234,       *
!     XIP,...,ZIPK).                                                   *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE type_basis_set
      USE gradients

      implicit none
!
! Input scalars:
      integer Nstart
!
! Local scalars:
      integer :: Iatmshl,Iatom,Ifrst,Ilast,Ishell,Irange,Istart,Iend,LAMAX,Iaos,Igauss,IGBGN,IGEND,Itype
      integer :: Jatmshl,Jatom,Jfrst,Jlast,Jshell,Jrange,Jstart,Jend,LBMAX,Jaos,Jgauss,JGBGN,JGEND,Jtype
      integer :: Katmshl,Katom,Kfrst,Klast,Kshell,Krange,Kstart,Kend,LCMAX,Kaos,Kgauss,KGBGN,KGEND,Ktype
      integer :: Latmshl,Latom,Lfrst,Llast,Lshell,Lrange,Lstart,Lend,LDMAX,Laos,Lgauss,LGBGN,LGEND,Ltype
!
      integer Atom_case,One_centre
      integer DEFINE_Mtype
      integer :: Mtype
      double precision :: PICONST
      double precision :: XA,XB,XC,XD,YA,YB,YC,YD,ZA,ZB,ZC,ZD
      logical :: LTWOINT
!
! Work array
      double precision, dimension(:,:), allocatable :: SHRABSQ
!
! Begin:
      call PRG_manager ('enter', 'GDFCLC', 'UTILITY')
!
      Nstart=1 ! for now
      PICONST=(PI_VAL+PI_VAL)*PI_VAL*DSQRT(PI_VAL)
!
      if(.not.allocated(SHRABSQ))allocate (SHRABSQ(Natoms,Natoms))
      call I2E_RABSQ(SHRABSQ)
!
! Clear local variables.
      A2DF(1:174)=ZERO
!
      CCPX(1:48)=ZERO
      CCPY(1:48)=ZERO
      CCPZ(1:48)=ZERO
      CCQX(1:48)=ZERO
      CCQY(1:48)=ZERO
      CCQZ(1:48)=ZERO
!
! **********************************************************************
! Loop over Ishell.
      do Ishell=1,Basis%Nshells
      Itype=Basis%shell(Ishell)%Xtype
      Istart=Basis%shell(Ishell)%Xstart
      IGBGN=Basis%shell(Ishell)%EXPBGN
      IGEND=Basis%shell(Ishell)%EXPEND
      Ifrst=Basis%shell(Ishell)%frstSHL
!
! Loop over Jshell.
      do Jshell=1,Ishell
      Jtype=Basis%shell(Jshell)%Xtype
      Jstart=Basis%shell(Jshell)%Xstart
      JGBGN=Basis%shell(Jshell)%EXPBGN
      JGEND=Basis%shell(Jshell)%EXPEND
      Jfrst=Basis%shell(Jshell)%frstSHL
!
! Loop over Kshell.
      do Kshell=1,Jshell
      Ktype=Basis%shell(Kshell)%Xtype
      Kstart=Basis%shell(Kshell)%Xstart
      KGBGN=Basis%shell(Kshell)%EXPBGN
      KGEND= Basis%shell(Kshell)%EXPEND
      Kfrst= Basis%shell(Kshell)%frstSHL
!
! Loop over Lshell.
      do Lshell=1,Kshell
      Ltype=Basis%shell(Lshell)%Xtype
      Lstart=Basis%shell(Lshell)%Xstart
      LGBGN=Basis%shell(Lshell)%EXPBGN
      LGEND= Basis%shell(Lshell)%EXPEND
      Lfrst= Basis%shell(Lshell)%frstSHL

! NOTE: These must be re-initialized everytime!!
      Ilast= Basis%shell(Ishell)%lastSHL
      Jlast= Basis%shell(Jshell)%lastSHL
      Klast= Basis%shell(Kshell)%lastSHL
      Llast= Basis%shell(Lshell)%lastSHL
!
      do Iatmshl=Ifrst,Ilast
        Iatom=Basis%atmshl(Iatmshl)%ATMLST
        XA=CARTESIAN(Iatom)%X
        YA=CARTESIAN(Iatom)%Y
        ZA=CARTESIAN(Iatom)%Z
        Iaos=Basis%atmshl(Iatmshl)%frstAO-1
        IF(Ishell.EQ.Jshell)Jlast=Iatmshl
      do Jatmshl=Jfrst,Jlast
        Jatom=Basis%atmshl(Jatmshl)%ATMLST
        XB=CARTESIAN(Jatom)%X
        YB=CARTESIAN(Jatom)%Y
        ZB=CARTESIAN(Jatom)%Z
        Jaos=Basis%atmshl(Jatmshl)%frstAO-1
        IF(Ishell.EQ.Kshell)Klast=Iatmshl
        IF(Jshell.EQ.Kshell)Klast=Jatmshl
      do Katmshl=Kfrst,Klast
        Katom=Basis%atmshl(Katmshl)%ATMLST
        XC=CARTESIAN(Katom)%X
        YC=CARTESIAN(Katom)%Y
        ZC=CARTESIAN(Katom)%Z
        Kaos=Basis%atmshl(Katmshl)%frstAO-1
        IF(Ishell.EQ.Lshell)Llast=Iatmshl
        IF(Jshell.EQ.Lshell)Llast=Jatmshl
        IF(Kshell.EQ.Lshell)Llast=Katmshl
      do Latmshl=Lfrst,Llast
        Latom=Basis%atmshl(Latmshl)%ATMLST
        XD=CARTESIAN(Latom)%X
        YD=CARTESIAN(Latom)%Y
        ZD=CARTESIAN(Latom)%Z
        Laos=Basis%atmshl(Latmshl)%frstAO-1
!
      Iend=Basis%shell(Ishell)%XEND
      Jend=Basis%shell(Jshell)%XEND
      Kend=Basis%shell(Kshell)%XEND
      Lend=Basis%shell(Lshell)%XEND
!
        Atom_case=4
        IF(Iatom.EQ.Jatom)then
          IF(Iatom.EQ.Katom)then
            Atom_case=2                                ! AAA*
          else if(Iatom.EQ.Latom)then
            Atom_case=1                                ! AA*A
          else if(Katom.EQ.Latom)then
            Atom_case=4                                ! AABB
          else
            Atom_case=1                                ! AA**
          end if

        else if(Iatom.NE.Jatom)then
          IF(Iatom.EQ.Katom.and.Katom.EQ.Latom)then
            Atom_case=3                                  ! ABAA
          else if(Iatom.EQ.Katom.and.Jatom.EQ.Latom)then
            Atom_case=7                                  ! ABAB
          else if(Iatom.EQ.Katom)then
            Atom_case=5                                  ! ABAD
          else if(Iatom.EQ.Latom.and.Jatom.EQ.Katom)then
            Atom_case=6                                  ! ABBA
          else if(Jatom.EQ.Katom.and.Katom.NE.Latom)then
            Atom_case=6                                  ! ABBD
          else if(Iatom.EQ.Latom)then
            Atom_case=3                                  ! ABCA
          else
            Atom_case=4
          end if
        end if
!
      One_centre=1
      IF(Iatom.EQ.Jatom)One_centre=One_centre+4
      IF(Jatom.EQ.Katom)One_centre=One_centre+2
      IF(Katom.EQ.Latom)One_centre=One_centre+1

        Mtype=DEFINE_Mtype (Iatmshl,Jatmshl,Katmshl,Latmshl)
        IF(Mtype.EQ.4.or.Mtype.EQ.7.or.Mtype.EQ.8)then
          LTWOINT=.FALSE.
        else
          LTWOINT=.TRUE.
        end if

        IF(One_centre.LT.8)then
        select case (Atom_case)
        case (4) ! IJKL
        call G2E_SPDF (Iatom,Jatom,Katom,Latom,Iatmshl,Jatmshl,Katmshl,Latmshl,Istart,Jstart,Kstart,Lstart,Iend,Jend,Kend,Lend, &
                       Itype,Jtype,Ktype,Ltype,Iaos,Jaos,Kaos,Laos,IGBGN,JGBGN,KGBGN,LGBGN, &
                       IGEND,JGEND,KGEND,LGEND,XA,YA,ZA,XB,YB,ZB,XC,YC,ZC,XD,YD,ZD, PICONST, SHRABSQ)
        IF(LTWOINT)then
        call G2E_SPDF (Iatom,Latom,Jatom,Katom,Iatmshl,Latmshl,Jatmshl,Katmshl,Istart,Lstart,Jstart,Kstart,Iend,Lend,Jend,Kend, &
                       Itype,Ltype,Jtype,Ktype,Iaos,Laos,Jaos,Kaos,IGBGN,LGBGN,JGBGN,KGBGN, &
                       IGEND,LGEND,JGEND,KGEND,XA,YA,ZA,XD,YD,ZD,XB,YB,ZB,XC,YC,ZC, PICONST, SHRABSQ)
        end if
        IF(Mtype.EQ.1)then
        call G2E_SPDF (Iatom,Katom,Jatom,Latom,Iatmshl,Katmshl,Jatmshl,Latmshl,Istart,Kstart,Jstart,Lstart,Iend,Kend,Jend,Lend, &
                       Itype,Ktype,Jtype,Ltype,Iaos,Kaos,Jaos,Laos,IGBGN,KGBGN,JGBGN,LGBGN, &
                       IGEND,KGEND,JGEND,LGEND,XA,YA,ZA,XC,YC,ZC,XB,YB,ZB,XD,YD,ZD, PICONST, SHRABSQ)
        end if

        case (5) ! JLIK
        call G2E_SPDF (Jatom,Iatom,Latom,Katom,Jatmshl,Iatmshl,Latmshl,Katmshl,Jstart,Istart,Lstart,Kstart,Jend,Iend,Lend,Kend, &
                       Jtype,Itype,Ltype,Ktype,Jaos,Iaos,Laos,Kaos,JGBGN,IGBGN,LGBGN,KGBGN, &
                       JGEND,IGEND,LGEND,KGEND,XB,YB,ZB,XA,YA,ZA,XD,YD,ZD,XC,YC,ZC, PICONST, SHRABSQ)
        IF(LTWOINT)then
        call G2E_SPDF (Jatom,Katom,Latom,Iatom,Jatmshl,Katmshl,Latmshl,Iatmshl,Jstart,Kstart,Lstart,Istart,Jend,Kend,Lend,Iend, &
                       Jtype,Ktype,Ltype,Itype,Jaos,Kaos,Laos,Iaos,JGBGN,KGBGN,LGBGN,IGBGN, &
                       JGEND,KGEND,LGEND,IGEND,XB,YB,ZB,XC,YC,ZC,XD,YD,ZD,XA,YA,ZA, PICONST, SHRABSQ)
        end if
        IF(Mtype.EQ.1)then
        call G2E_SPDF (Jatom,Latom,Iatom,Katom,Jatmshl,Latmshl,Iatmshl,Katmshl,Jstart,Lstart,Istart,Kstart,Jend,Lend,Iend,Kend, &
                       Jtype,Ltype,Itype,Ktype,Jaos,Laos,Iaos,Kaos,JGBGN,LGBGN,IGBGN,KGBGN, &
                       JGEND,LGEND,IGEND,KGEND,XB,YB,ZB,XD,YD,ZD,XA,YA,ZA,XC,YC,ZC, PICONST, SHRABSQ)
        end if

        case (3) ! JIKL
        call G2E_SPDF (Jatom,Iatom,Katom,Latom,Jatmshl,Iatmshl,Katmshl,Latmshl,Jstart,Istart,Kstart,Lstart,Jend,Iend,Kend,Lend, &
                       Jtype,Itype,Ktype,Ltype,Jaos,Iaos,Kaos,Laos,JGBGN,IGBGN,KGBGN,LGBGN, &
                       JGEND,IGEND,KGEND,LGEND,XB,YB,ZB,XA,YA,ZA,XC,YC,ZC,XD,YD,ZD, PICONST, SHRABSQ)
        IF(LTWOINT)then
        call G2E_SPDF (Jatom,Katom,Iatom,Latom,Jatmshl,Katmshl,Iatmshl,Latmshl,Jstart,Kstart,Istart,Lstart,Jend,Kend,Iend,Lend, &
                       Jtype,Ktype,Itype,Ltype,Jaos,Kaos,Iaos,Laos,JGBGN,KGBGN,IGBGN,LGBGN, &
                       JGEND,KGEND,IGEND,LGEND,XB,YB,ZB,XC,YC,ZC,XA,YA,ZA,XD,YD,ZD, PICONST, SHRABSQ)
        end if
        IF(Mtype.EQ.1)then
        call G2E_SPDF (Jatom,Latom,Katom,Iatom,Jatmshl,Latmshl,Katmshl,Iatmshl,Jstart,Lstart,Kstart,Istart,Jend,Lend,Kend,Iend, &
                       Jtype,Ltype,Ktype,Itype,Jaos,Laos,Kaos,Iaos,JGBGN,LGBGN,KGBGN,IGBGN, &
                       JGEND,LGEND,KGEND,IGEND,XB,YB,ZB,XD,YD,ZD,XC,YC,ZC,XA,YA,ZA, PICONST, SHRABSQ)
        end if

        case (2) ! LKIJ    (AAAB)
        call G2E_SPDF (Latom,Katom,Iatom,Jatom,Latmshl,Katmshl,Iatmshl,Jatmshl,Lstart,Kstart,Istart,Jstart,Lend,Kend,Iend,Jend, &
                       Ltype,Ktype,Itype,Jtype,Laos,Kaos,Iaos,Jaos,LGBGN,KGBGN,IGBGN,JGBGN, &
                       LGEND,KGEND,IGEND,JGEND,XD,YD,ZD,XC,YC,ZC,XA,YA,ZA,XB,YB,ZB, PICONST, SHRABSQ)
        IF(LTWOINT)then
        call G2E_SPDF (Latom,Iatom,Katom,Jatom,Latmshl,Iatmshl,Katmshl,Jatmshl,Lstart,Istart,Kstart,Jstart,Lend,Iend,Kend,Jend, &
                       Ltype,Itype,Ktype,Jtype,Laos,Iaos,Kaos,Jaos,LGBGN,IGBGN,KGBGN,JGBGN, &
                       LGEND,IGEND,KGEND,JGEND,XD,YD,ZD,XA,YA,ZA,XC,YC,ZC,XB,YB,ZB, PICONST, SHRABSQ)
        end if
        IF(Mtype.EQ.1)then
        call G2E_SPDF (Latom,Jatom,Katom,Iatom,Latmshl,Jatmshl,Katmshl,Iatmshl,Lstart,Jstart,Kstart,Istart,Lend,Jend,Kend,Iend, &
                       Ltype,Jtype,Ktype,Itype,Laos,Jaos,Kaos,Iaos,LGBGN,JGBGN,KGBGN,IGBGN, &
                       LGEND,JGEND,KGEND,IGEND,XD,YD,ZD,XB,YB,ZB,XC,YC,ZC,XA,YA,ZA, PICONST, SHRABSQ)
        end if

        case (1)  ! KLIJ
        call G2E_SPDF (Katom,Latom,Iatom,Jatom,Katmshl,Latmshl,Iatmshl,Jatmshl,Kstart,Lstart,Istart,Jstart,Kend,Lend,Iend,Jend, &
                       Ktype,Ltype,Itype,Jtype,Kaos,Laos,Iaos,Jaos,KGBGN,LGBGN,IGBGN,JGBGN, &
                       KGEND,LGEND,IGEND,JGEND,XC,YC,ZC,XD,YD,ZD,XA,YA,ZA,XB,YB,ZB, PICONST, SHRABSQ)
        IF(LTWOINT)then
        call G2E_SPDF (Katom,Jatom,Latom,Iatom,Katmshl,Jatmshl,Latmshl,Iatmshl,Kstart,Jstart,Lstart,Istart,Kend,Jend,Lend,Iend, &
                       Ktype,Jtype,Ltype,Itype,Kaos,Jaos,Laos,Iaos,KGBGN,JGBGN,LGBGN,IGBGN, &
                       KGEND,JGEND,LGEND,IGEND,XC,YC,ZC,XB,YB,ZB,XD,YD,ZD,XA,YA,ZA, PICONST, SHRABSQ)
        end if
        IF(Mtype.EQ.1)then
        call G2E_SPDF (Katom,Iatom,Latom,Jatom,Katmshl,Iatmshl,Latmshl,Jatmshl,Kstart,Istart,Lstart,Jstart,Kend,Iend,Lend,Jend, &
                       Ktype,Itype,Ltype,Jtype,Kaos,Iaos,Laos,Jaos,KGBGN,IGBGN,LGBGN,JGBGN, &
                       KGEND,IGEND,LGEND,JGEND,XC,YC,ZC,XA,YA,ZA,XD,YD,ZD,XB,YB,ZB, PICONST, SHRABSQ)
        end if

        case (6)  ! ILJK
        call G2E_SPDF (Iatom,Jatom,Latom,Katom,Iatmshl,Jatmshl,Latmshl,Katmshl,Istart,Jstart,Lstart,Kstart,Iend,Jend,Lend,Kend, &
                       Itype,Jtype,Ltype,Ktype,Iaos,Jaos,Laos,Kaos,IGBGN,JGBGN,LGBGN,KGBGN, &
                       IGEND,JGEND,LGEND,KGEND,XA,YA,ZA,XB,YB,ZB,XD,YD,ZD,XC,YC,ZC, PICONST, SHRABSQ)
        IF(LTWOINT)then
        call G2E_SPDF (Iatom,Latom,Jatom,Katom,Iatmshl,Latmshl,Jatmshl,Katmshl,Istart,Lstart,Jstart,Kstart,Iend,Lend,Jend,Kend, &
                       Itype,Ltype,Jtype,Ktype,Iaos,Laos,Jaos,Kaos,IGBGN,LGBGN,JGBGN,KGBGN, &
                       IGEND,LGEND,JGEND,KGEND,XA,YA,ZA,XD,YD,ZD,XB,YB,ZB,XC,YC,ZC, PICONST, SHRABSQ)
        end if
        IF(Mtype.EQ.1)then
        call G2E_SPDF (Iatom,Katom,Latom,Jatom,Iatmshl,Katmshl,Latmshl,Jatmshl,Istart,Kstart,Lstart,Jstart,Iend,Kend,Lend,Jend, &
                       Itype,Ktype,Ltype,Jtype,Iaos,Kaos,Laos,Jaos,IGBGN,KGBGN,LGBGN,JGBGN, &
                       IGEND,KGEND,LGEND,JGEND,XA,YA,ZA,XC,YC,ZC,XD,YD,ZD,XB,YB,ZB, PICONST, SHRABSQ)
        end if

        case (7) ! IKJL
        call G2E_SPDF (Iatom,Jatom,Katom,Latom,Iatmshl,Jatmshl,Katmshl,Latmshl,Istart,Jstart,Kstart,Lstart,Iend,Jend,Kend,Lend, &
                       Itype,Jtype,Ktype,Ltype,Iaos,Jaos,Kaos,Laos,IGBGN,JGBGN,KGBGN,LGBGN, &
                       IGEND,JGEND,KGEND,LGEND,XA,YA,ZA,XB,YB,ZB,XC,YC,ZC,XD,YD,ZD, PICONST, SHRABSQ)
        IF(LTWOINT)then
        call G2E_SPDF (Iatom,Latom,Katom,Jatom,Iatmshl,Latmshl,Katmshl,Jatmshl,Istart,Lstart,Kstart,Jstart,Iend,Lend,Kend,Jend, &
                       Itype,Ltype,Ktype,Jtype,Iaos,Laos,Kaos,Jaos,IGBGN,LGBGN,KGBGN,JGBGN, &
                       IGEND,LGEND,KGEND,JGEND,XA,YA,ZA,XD,YD,ZD,XC,YC,ZC,XB,YB,ZB, PICONST, SHRABSQ)
        end if
        IF(Mtype.EQ.1)then
        call G2E_SPDF (Iatom,Katom,Jatom,Latom,Iatmshl,Katmshl,Jatmshl,Latmshl,Istart,Kstart,Jstart,Lstart,Iend,Kend,Jend,Lend, &
                       Itype,Ktype,Jtype,Ltype,Iaos,Kaos,Jaos,Laos,IGBGN,KGBGN,JGBGN,LGBGN, &
                       IGEND,KGEND,JGEND,LGEND,XA,YA,ZA,XC,YC,ZC,XB,YB,ZB,XD,YD,ZD, PICONST, SHRABSQ)
        end if

        case default
          write(UNIout,*)'ERROR> GDFCLC: illegal Atom_case'
          stop 'ERROR> GDFCLC: illegal Atom_case'
        end select
        end if
!
      end do ! Latmshl
      end do ! Katmshl
      end do ! Jatmshl
      end do ! Iatmshl

      end do ! Lshell
      end do ! Kshell
      end do ! Jshell
      end do ! Ishell

      deallocate (SHRABSQ)
!
! End of routine GDFCLC
      call PRG_manager ('exit', 'GDFCLC', 'UTILITY')
      return
      END
      subroutine G2E_SPDF (Iatom,Jatom,Katom,Latom, &
                           Iatmshl,Jatmshl,Katmshl,Latmshl, &
                           Istart,Jstart,Kstart,Lstart, &
                           Iends,Jends,Kends,Lends, &
                           Itype,Jtype,Ktype,Ltype, &
                           Iaos,Jaos,Kaos,Laos, &
                           IGBGN,JGBGN,KGBGN,LGBGN, &
                           IGEND,JGEND,KGEND,LGEND, &
                           XA,YA,ZA, &
                           XB,YB,ZB, &
                           XC,YC,ZC, &
                           XD,YD,ZD, PICONST, SHRABSQ)
!****************************************************************************************************************************
!     Date last modified: February 18, 2000                                                                    Version 2.0  *
!     Author: R.A. Poirier                                                                                                  *
!     Description: Compute the 2e contribution to the gradient for the given shells                                         *
!****************************************************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_basis_set
      USE QM_defaults
      USE gradients
      USE type_molecule

      implicit none
!
! Input scalars:
      integer, intent(IN) :: Iatom,Jatom,Katom,Latom,Iatmshl,Jatmshl,Katmshl,Latmshl, &
                             Istart,Jstart,Kstart,Lstart, &
              Iends,Jends,Kends,Lends,Itype,Jtype,Ktype,Ltype, &
              Iaos,Jaos,Kaos,Laos,IGBGN,JGBGN,KGBGN,LGBGN,IGEND,JGEND,KGEND,LGEND
      double precision, intent(IN) :: XA,YA,ZA,XB,YB,ZB,XC,YC,ZC,XD,YD,ZD,PICONST
      double precision, intent(IN) :: SHRABSQ(Natoms,Natoms)
!
! Local scalars:
      integer LPMAX,LQMAX
!
      double precision AB2I,CD2I,AIAB,BIAB,CICD,DICD,PQX,PQY,PQZ,D1ABX,D1ABY,D1ABZ,D1CDX,D1CDY,D1CDZ
      double precision, dimension(4) :: XYZA1,XYZB1,XYZC1,XYZA2,XYZB2,XYZC2,XYZA3,XYZB3,XYZC3,XYZA4,XYZB4,XYZC4
!
      integer Irange,Jrange,Krange,Lrange,LAMAX,LBMAX,LCMAX,LDMAX
      integer Iend,Jend,Kend,Lend
      integer Igauss,Jgauss,Kgauss,Lgauss
      integer IFORMS,IJKL,IMJ,KML,IMKJML
      integer I,IJCUTP,IOFFST,IPQCUT,IV,IX,IXYZNT,IY,IZ,IZERO,J,JOFFST,JX,JY,JZ,K,KLIND,KOFFST,KX,KY,KZ,L,LPQMAX, &
              LX,LY,LZ,N,NXYZNT,NZERO,NZROS
      double precision ABSC,AS,ASXA,ASYA,ASZA,BS,CC1,CC2,CC3,CMAXI,CMAXJ,CMAXK,CMAXL,CS,DNSMAX,DS,DTEMP,DXYZ, &
                       EAP,EBP,ECQ,EDQ,EPAB,EPEQ,EPABI, &
                       EPPEQI,EQCD,EQCDI,EXPARG,FXI,FXIV,FXJ,FXJV,FXK,FXKV,FXL,FYI,FYIV,FYJ,FYJV,FYK,FYKV,FYL, &
                       FZI,FZIV,FZJ,FZJV,FZK, &
                       FZKV,FZL,PEXP,PTEMP,PTEST,PX,PY,PZ,QEXP,QTEST,QX,QY,QZ,RHO,RHOT2,RPQSQ,TEMP,TSTIJ, &
                       TWORHO,XAP,XBP,XCQ,XDQ,XYIP,XZIP,YAP,YBP,YCQ,YDQ,YZIP,ZAP,ZBP,ZCONST,ZCQ,ZDQ,ZTEMP,ZTEST
      double precision DIJKL
      double precision RABSQ,RCDSQ
!
! Local arrays:
      integer INDX(20),INDY(20),INDZ(20),KLCUTQ(100),UBOUND(4)
      double precision CA(20),CB(20),CC(20),CD(20),TP(7),WP(7), &
                       D1234(1296),EQSAV(100),G2DFX(13),G2DFY(13),G2DFZ(13),QXPSAV(100),ECQSAV(100),EDQSAV(100), &
                       XINT(11),XIP(580),XIPI(405),XIPJ(405),XIPK(405),YIP(580),YIPI(405), &
                       YIPJ(405),YIPK(405),ZIP(580),ZIPI(405),ZIPJ(405),ZIPK(405)
!
      DATA XINT/1.0D0,2.0D0,3.0D0,4.0D0,5.0D0,6.0D0,7.0D0,8.0D0,9.0D0,10.0D0,11.0D0/, &
           INDX/0,1,0,0,2,0,0,1,1,0,3,0,0,1,2,2,1,0,0,1/, &
           INDY/0,0,1,0,0,2,0,1,0,1,0,3,0,2,1,0,0,1,2,1/, &
           INDZ/0,0,0,1,0,0,2,0,1,1,0,0,3,0,0,1,2,2,1,1/
!
! Local parameters:
      double precision TEN
      parameter (TEN=10.0D0)
!
! Must not destroy I,J,K,Lends
      Iend=Iends
      Jend=Jends
      Kend=Kends
      Lend=Lends
!
      Irange=Iend-Istart+1
      Jrange=Jend-Jstart+1
      Krange=Kend-Kstart+1
      Lrange=Lend-Lstart+1
!
      LAMAX=Itype+1
      LBMAX=Jtype+1
      LCMAX=Ktype+1
      LDMAX=Ltype+1

      LPMAX=LAMAX+Jtype
      LQMAX=LCMAX+Ltype
      LPQMAX=LPMAX+LQMAX-1
      RABSQ=SHRABSQ(Iatom,Jatom)
      RCDSQ=SHRABSQ(Katom,Latom)
!
      NZERO=((Itype+Jtype+Ktype+Ltype+1)/2)+1
!
! Determine IFORMS.  This variable determines branching to select 1, 2, etc. center code.
      IFORMS=1
      IF(Katom.EQ.Latom)IFORMS=IFORMS+3
      IF(Iatom.EQ.Jatom)IFORMS=IFORMS+2
      IF(Jatom.EQ.Latom)IFORMS=IFORMS+1
      IF(Iatom.EQ.Katom)IFORMS=IFORMS+1
! 1111=8 2111=5 2211=6 2121=3 3211=4 3121=2 4321=1.
      IF(IFORMS.NE.8) then
      IMJ=IABS(Iatmshl-Jatmshl)
      KML=IABS(Katmshl-Latmshl)
      IMKJML=IABS(Iatmshl-Katmshl)+IABS(Jatmshl-Latmshl)
!
! Obtain all density matrix contributions, given the four atmshls.
      call BLD_Pijkl (Irange, Jrange, Krange, Lrange, Iaos, Jaos, Kaos, Laos, IMJ, KML, IMKJML, D1234, DNSMAX)
!
      KOFFST=LDMAX
      JOFFST=LCMAX*KOFFST
      IOFFST=LBMAX*JOFFST
      NXYZNT=LAMAX*IOFFST
      FXI=ZERO
      FYI=ZERO
      FZI=ZERO
      FXJ=ZERO
      FYJ=ZERO
      FZJ=ZERO
      FXK=ZERO
      FYK=ZERO
      FZK=ZERO
!
! **********************************************************************
! Commence loop over Gaussian expansion.
! Preliminary Q-loop.
      KLIND=0
      do Kgauss=KGBGN,KGEND
      CS=Basis%gaussian(Kgauss)%exp
!
      do Lgauss=LGBGN,LGEND
      DS=Basis%gaussian(Lgauss)%exp
!
      KLIND=KLIND+1
! Compute saved quantities.
      EQCD=CS+DS
      EQCDI=ONE/EQCD
      EQSAV(KLIND)=EQCD
      ECQSAV(KLIND)=CS*EQCDI
      EDQSAV(KLIND)=DS*EQCDI
      EXPARG=RCDSQ*CS*DS*EQCDI
      IF(EXPARG.LE.GRD_expcut) then
         QEXP=DEXP(-EXPARG)
         QXPSAV(KLIND)=QEXP
! Determine the maximum coefficient at K AND L.
         CMAXK=DABS(Basis%gaussian(Kgauss)%CONTRC)
         CMAXL=DABS(Basis%gaussian(Lgauss)%CONTRC)
         QTEST=CMAXK*CMAXL*QEXP
         IF(QTEST.GE.GRD_PQCUT1) then
            KLCUTQ(KLIND)=0
         else
            IF(QTEST.LT.GRD_PQCUT2) then
               KLCUTQ(KLIND)=2
            else
               KLCUTQ(KLIND)=1
            end if ! QTEST.LT.GRD_PQCUT2
         end if ! QTEST.GE.GRD_PQCUT1
      else
         QXPSAV(KLIND)= ZERO
         KLCUTQ(KLIND)=2
      end if ! EXPARG.LE.GRD_expcut
      end do ! Lgauss
      end do ! Kgauss
!
      do Igauss=IGBGN,IGEND
      AS=Basis%gaussian(Igauss)%exp
      call FILLCC (LAMAX, Basis%gaussian(Igauss)%CONTRC, CA)
!
      do Jgauss=JGBGN,JGEND
      BS=Basis%gaussian(Jgauss)%exp
      call FILLCC (LBMAX, Basis%gaussian(Jgauss)%CONTRC, CB)
      EPAB=AS+BS
      EPABI=ONE/EPAB
      AB2I=PT5*EPABI
      EAP=AS*EPABI
      EBP=BS*EPABI
      EXPARG=RABSQ*AS*BS*EPABI
      IF(EXPARG.LE.GRD_expcut) then
      PEXP=DEXP(-EXPARG)
      PTEMP=PICONST*PEXP
      CMAXI=DABS(Basis%gaussian(Igauss)%CONTRC)
      CMAXJ=DABS(Basis%gaussian(Jgauss)%CONTRC)
      PTEST=CMAXI*CMAXJ*PEXP
      TSTIJ=DNSMAX*CMAXI*CMAXJ*TEN
      IJCUTP=0
      IF(PTEST.LT.GRD_PQCUT1)IJCUTP=1
      IF(PTEST.LT.GRD_PQCUT2)IJCUTP=2
      XAP=EBP*(XB-XA)
      XBP=EAP*(XA-XB)
      YAP=EBP*(YB-YA)
      YBP=EAP*(YA-YB)
      ZAP=EBP*(ZB-ZA)
      ZBP=EAP*(ZA-ZB)
      call GETCC_XYZ (CCPX, CCPY, CCPZ, XAP, YAP, ZAP, XBP, YBP, ZBP, LAMAX, LBMAX)
      AIAB=AS*EPABI
      BIAB=BS*EPABI
      TEMP=-(AS+AS)*BIAB
      D1ABX=(XA-XB)*TEMP
      D1ABY=(YA-YB)*TEMP
      D1ABZ=(ZA-ZB)*TEMP
!
      KLIND=0
      do Kgauss=KGBGN,KGEND
      call FILLCC (LCMAX, Basis%gaussian(Kgauss)%CONTRC, CC)
      CS=Basis%gaussian(Kgauss)%exp
!
      do Lgauss=LGBGN,LGEND
      KLIND=KLIND+1
      call FILLCC (LDMAX, Basis%gaussian(Lgauss)%CONTRC, CD)
!
! Test cutoffs.
      IPQCUT=IJCUTP+KLCUTQ(KLIND)
      IF(IPQCUT.LT.2) then
      EQCD=EQSAV(KLIND)
      ECQ=ECQSAV(KLIND)
      EDQ=EDQSAV(KLIND)
      QEXP=QXPSAV(KLIND)
      EPEQ=EPAB*EQCD
      EPPEQI=ONE/(EPAB+EQCD)
      RHO=EPEQ*EPPEQI
      TWORHO=RHO+RHO
      ZTEMP=PTEMP*QEXP*DSQRT(EPPEQI)/EPEQ
!
      XCQ=EDQ*(XD-XC)
      XDQ=ECQ*(XC-XD)
      YCQ=EDQ*(YD-YC)
      YDQ=ECQ*(YC-YD)
      ZCQ=EDQ*(ZD-ZC)
      ZDQ=ECQ*(ZC-ZD)
      call GETCC_XYZ (CCQX, CCQY, CCQZ, XCQ, YCQ, ZCQ, XDQ, YDQ, ZDQ, LCMAX, LDMAX)
      PQX=EAP*ECQ*(XC-XA)+EAP*EDQ*(XD-XA)+EBP*ECQ*(XC-XB)+EBP*EDQ*(XD-XB)
      PQY=EAP*ECQ*(YC-YA)+EAP*EDQ*(YD-YA)+EBP*ECQ*(YC-YB)+EBP*EDQ*(YD-YB)
      PQZ=EAP*ECQ*(ZC-ZA)+EAP*EDQ*(ZD-ZA)+EBP*ECQ*(ZC-ZB)+EBP*EDQ*(ZD-ZB)
      RPQSQ=PQX*PQX+PQY*PQY+PQZ*PQZ
      DXYZ=RHO*RPQSQ
      call RPOLX (NZERO, DXYZ, TP, WP)
      call IJKLA2 (A2DF, LPMAX, LQMAX, EQCD, AB2I)
      DS=Basis%gaussian(Lgauss)%exp
      EQCDI=ONE/EQCD
      CD2I=PT5*EQCDI
      CICD=CS*EQCDI
      DICD=DS*EQCDI
      TEMP=-(CS+CS)*DICD
      D1CDX=(XC-XD)*TEMP
      D1CDY=(YC-YD)*TEMP
      D1CDZ=(ZC-ZD)*TEMP
!
! **********************************************************************
! Commence loop over zeroes of Rys polynomial.
      G2DFX(1:13)=ZERO
      G2DFY(1:13)=ZERO
      G2DFZ(1:13)=ZERO
!
      NZROS=0
      IXYZNT=1-NXYZNT
      do IZERO=1,NZERO
! Obtain root of Rys polynomial.
      RHOT2=TWORHO*TP(IZERO)
      ZCONST=ZTEMP*WP(IZERO)
!
      NZROS=NZROS+1
      IXYZNT=IXYZNT+NXYZNT
!
! Fill G2DFX(IV).  Note indices incremented by 1.
! Note that RHOT2=2*RHO*T**2.
      G2DFX(1)=ONE
      G2DFY(1)=ONE
      G2DFZ(1)=ZCONST
      XIP(IXYZNT)=ONE
      YIP(IXYZNT)=ONE
      ZIP(IXYZNT)=ZCONST
      IF(LPQMAX.GT.1) then
      G2DFX(2)=RHOT2*PQX*G2DFX(1)
      G2DFY(2)=RHOT2*PQY*G2DFY(1)
      G2DFZ(2)=RHOT2*PQZ*G2DFZ(1)
      IF(LPQMAX.GT.2) then
      do IV=3,LPQMAX
      G2DFX(IV)=RHOT2*(PQX*G2DFX(IV-1)-XINT(IV-2)*G2DFX(IV-2))
      G2DFY(IV)=RHOT2*(PQY*G2DFY(IV-1)-XINT(IV-2)*G2DFY(IV-2))
      G2DFZ(IV)=RHOT2*(PQZ*G2DFZ(IV-1)-XINT(IV-2)*G2DFZ(IV-2))
      end do ! IV
! Find two-center integrals.
! In this section, the index IV is incremented by 2.
      end if  ! LPQMAX.GT.2
        IF(RABSQ.NE.ZERO.and.RCDSQ.NE.ZERO)then
        call ABCD4C (A2DF, G2DFX, CCQX, CCPX, XIP(IXYZNT), LAMAX, LBMAX, LCMAX, LDMAX)
        call ABCD4C (A2DF, G2DFY, CCQY, CCPY, YIP(IXYZNT), LAMAX, LBMAX, LCMAX, LDMAX)
        call ABCD4C (A2DF, G2DFZ, CCQZ, CCPZ, ZIP(IXYZNT), LAMAX, LBMAX, LCMAX, LDMAX)
        else if(RCDSQ.NE.ZERO)then
        call AACD3C (A2DF, G2DFX, CCQX, XIP(IXYZNT), LAMAX, LBMAX, LCMAX, LDMAX)
        call AACD3C (A2DF, G2DFY, CCQY, YIP(IXYZNT), LAMAX, LBMAX, LCMAX, LDMAX)
        call AACD3C (A2DF, G2DFZ, CCQZ, ZIP(IXYZNT), LAMAX, LBMAX, LCMAX, LDMAX)
        else if(RABSQ.NE.ZERO)then
        call ABCC3C (A2DF, G2DFX, CCPX, XIP(IXYZNT), LAMAX, LBMAX, LCMAX, LDMAX)
        call ABCC3C (A2DF, G2DFY, CCPY, YIP(IXYZNT), LAMAX, LBMAX, LCMAX, LDMAX)
        call ABCC3C (A2DF, G2DFZ, CCPZ, ZIP(IXYZNT), LAMAX, LBMAX, LCMAX, LDMAX)
        else
        call AABB2C (A2DF, G2DFX, XIP(IXYZNT), LAMAX, LBMAX, LCMAX, LDMAX)
        call AABB2C (A2DF, G2DFY, YIP(IXYZNT), LAMAX, LBMAX, LCMAX, LDMAX)
        call AABB2C (A2DF, G2DFZ, ZIP(IXYZNT), LAMAX, LBMAX, LCMAX, LDMAX)
        end if ! RABSQ
      end if ! LPQMAX.GT.1

      IF (IFORMS.EQ.1) then ! (AB|CD)
        call DIPABC (XIP, YIP, ZIP, XIPI, YIPI, ZIPI, XIPJ, YIPJ, ZIPJ, XIPK, YIPK, ZIPK, RHOT2, LAMAX, LBMAX, LCMAX, LDMAX, IXYZNT)
      else if (IFORMS.EQ.2) then ! (AB|CB) or (AB|AD)
        call DIPAC (XIP, YIP, ZIP, XIPI, YIPI, ZIPI, XIPK, YIPK, ZIPK, RHOT2, LAMAX, LBMAX, LCMAX, LDMAX, IXYZNT)
      else if (IFORMS.EQ.3) then ! (AA|CD) or (AB|AB)
        call DIPACI (XIP, YIP, ZIP, XIPI, YIPI, ZIPI, RHOT2, LAMAX, LBMAX, LCMAX, LDMAX, IXYZNT)
      else if (IFORMS.EQ.4) then ! (AB|CC) or ((AA|AD) or (AA|CA)
        call DIPAB (XIP, YIP, ZIP, XIPI, YIPI, ZIPI, XIPJ, YIPJ, ZIPJ, RHOT2, LAMAX, LBMAX, LCMAX, LDMAX, IXYZNT)
      else if (IFORMS.EQ.5) then ! (AB|BB) or (AB|AA)
        call DIPA (XIP, YIP, ZIP, XIPI, YIPI, ZIPI, RHOT2, LAMAX, LBMAX, LCMAX, LDMAX, IXYZNT)
      else if (IFORMS.EQ.6) then  ! (AA|BB)
        call DIPABI (XIP, YIP, ZIP, XIPI, YIPI, ZIPI, RHOT2, LAMAX, LBMAX, LCMAX, LDMAX, IXYZNT)
      else ! impossible case
       write(UNIout,*) 'Error_GDFCLC> illegal value of IFORMS'
       stop 'Error in GSPCLC'
      end if ! IFORMS
!
!      end if ! ZCONST.GE.PQCUT6
      end do ! IZERO
      IF(NZROS.GT.0) then
!
! **********************************************************************
! Commence loop over atomic orbitals.
      IJKL=0
      do I=Istart,Iend
      IX=INDX(I)*IOFFST+1-NXYZNT
      IY=INDY(I)*IOFFST+1-NXYZNT
      IZ=INDZ(I)*IOFFST+1-NXYZNT
      IF(IMJ.EQ.0)Jend=I
      IF(IMKJML.EQ.0)Kend=I
      CC1=CA(I)
!
      do J=Jstart,Jend
      JX=INDX(J)*JOFFST+IX
      JY=INDY(J)*JOFFST+IY
      JZ=INDZ(J)*JOFFST+IZ
      CC2=CC1*CB(J)
!
      do K=Kstart,Kend
      Lend=Lends
      KX=INDX(K)*KOFFST+JX
      KY=INDY(K)*KOFFST+JY
      KZ=INDZ(K)*KOFFST+JZ
      CC3=CC2*CC(K)
      IF(KML.EQ.0)Lend=K
      IF(IMKJML.EQ.0.and.I.EQ.K)Lend=J
!
      do  L=Lstart,Lend
      IJKL=IJKL+1
      DIJKL=D1234(IJKL)*CC3*CD(L)

!      IF(DABS(DIJKL).GE.PQCUT5) then
      LX=INDX(L)+KX
      LY=INDY(L)+KY
      LZ=INDZ(L)+KZ
      IF (IFORMS.EQ.1) then
      FXIV=ZERO
      FXJV=ZERO
      FXKV=ZERO
      FYIV=ZERO
      FYJV=ZERO
      FYKV=ZERO
      FZIV=ZERO
      FZJV=ZERO
      FZKV=ZERO
      do IZERO=1,NZROS
      LX=LX+NXYZNT
      LY=LY+NXYZNT
      LZ=LZ+NXYZNT
      XYIP=XIP(LX)*YIP(LY)
      XZIP=XIP(LX)*ZIP(LZ)
      YZIP=YIP(LY)*ZIP(LZ)
      FXIV=FXIV+XIPI(LX)*YZIP
      FXJV=FXJV+XIPJ(LX)*YZIP
      FXKV=FXKV+XIPK(LX)*YZIP
      FYIV=FYIV+YIPI(LY)*XZIP
      FYJV=FYJV+YIPJ(LY)*XZIP
      FYKV=FYKV+YIPK(LY)*XZIP
      FZIV=FZIV+ZIPI(LZ)*XYIP
      FZJV=FZJV+ZIPJ(LZ)*XYIP
      FZKV=FZKV+ZIPK(LZ)*XYIP
      end do ! IZERO

      FXI=FXI+FXIV*DIJKL
      FXJ=FXJ+FXJV*DIJKL
      FXK=FXK+FXKV*DIJKL
      FYI=FYI+FYIV*DIJKL
      FYJ=FYJ+FYJV*DIJKL
      FYK=FYK+FYKV*DIJKL
      FZI=FZI+FZIV*DIJKL
      FZJ=FZJ+FZJV*DIJKL
      FZK=FZK+FZKV*DIJKL
      else if (IFORMS.EQ.2) then
      FXIV=ZERO
      FXKV=ZERO
      FYIV=ZERO
      FYKV=ZERO
      FZIV=ZERO
      FZKV=ZERO
      do IZERO=1,NZROS
      LX=LX+NXYZNT
      LY=LY+NXYZNT
      LZ=LZ+NXYZNT
      XYIP=XIP(LX)*YIP(LY)
      XZIP=XIP(LX)*ZIP(LZ)
      YZIP=YIP(LY)*ZIP(LZ)
      FXIV=FXIV+XIPI(LX)*YZIP
      FXKV=FXKV+XIPK(LX)*YZIP
      FYIV=FYIV+YIPI(LY)*XZIP
      FYKV=FYKV+YIPK(LY)*XZIP
      FZIV=FZIV+ZIPI(LZ)*XYIP
      FZKV=FZKV+ZIPK(LZ)*XYIP
      end do ! IZERO
      FXI=FXI+FXIV*DIJKL
      FXK=FXK+FXKV*DIJKL
      FYI=FYI+FYIV*DIJKL
      FYK=FYK+FYKV*DIJKL
      FZI=FZI+FZIV*DIJKL
      FZK=FZK+FZKV*DIJKL
      else if (IFORMS.EQ.4) then
      FXIV=ZERO
      FXJV=ZERO
      FYIV=ZERO
      FYJV=ZERO
      FZIV=ZERO
      FZJV=ZERO
      do IZERO=1,NZROS
      LX=LX+NXYZNT
      LY=LY+NXYZNT
      LZ=LZ+NXYZNT
      XYIP=XIP(LX)*YIP(LY)
      XZIP=XIP(LX)*ZIP(LZ)
      YZIP=YIP(LY)*ZIP(LZ)
      FXIV=FXIV+XIPI(LX)*YZIP
      FXJV=FXJV+XIPJ(LX)*YZIP
      FYIV=FYIV+YIPI(LY)*XZIP
      FYJV=FYJV+YIPJ(LY)*XZIP
      FZIV=FZIV+ZIPI(LZ)*XYIP
      FZJV=FZJV+ZIPJ(LZ)*XYIP
      end do ! IZERO
      FXI=FXI+FXIV*DIJKL
      FXJ=FXJ+FXJV*DIJKL
      FYI=FYI+FYIV*DIJKL
      FYJ=FYJ+FYJV*DIJKL
      FZI=FZI+FZIV*DIJKL
      FZJ=FZJ+FZJV*DIJKL
      else if (IFORMS.EQ.3.or.IFORMS.GT.4) then
      FXIV=ZERO
      FYIV=ZERO
      FZIV=ZERO
      do IZERO=1,NZROS
      LX=LX+NXYZNT
      LY=LY+NXYZNT
      LZ=LZ+NXYZNT
      FXIV=FXIV+XIPI(LX)*YIP(LY)*ZIP(LZ)
      FYIV=FYIV+YIPI(LY)*XIP(LX)*ZIP(LZ)
      FZIV=FZIV+ZIPI(LZ)*XIP(LX)*YIP(LY)
      end do ! IZERO
      FXI=FXI+FXIV*DIJKL
      FYI=FYI+FYIV*DIJKL
      FZI=FZI+FZIV*DIJKL
      else
        write(UNIout,*)'ERROR> GDFCLC: illegal value of IFORMS: ',IFORMS
        stop 'ERROR> GDFCLC: illegal value of IFORMS'
      end if ! IFORMS
!      end if ! DABS(DIJKL).LT.PQCUT5
      end do ! L
      end do ! J
      end do ! K
      end do ! I
!
      end if ! NZROS.GT.0
!      end if ! ZTEST.GE.PQCUT5
      end if !IPQCUT.LT.2
      end do ! Lgauss
      end do ! Kgauss
      end if ! EXPARG.LE.GRD_expcut
      end do ! Jgauss
      end do ! Igauss
! End of loop over Gaussians.
! **********************************************************************
      FXL=-(FXI+FXJ+FXK)
      FYL=-(FYI+FYJ+FYK)
      FZL=-(FZI+FZJ+FZK)
      Gradient(1+3*(Iatom-1))%EEWFN=Gradient(1+3*(Iatom-1))%EEWFN+FXI
      Gradient(1+3*(Jatom-1))%EEWFN=Gradient(1+3*(Jatom-1))%EEWFN+FXJ
      Gradient(1+3*(Katom-1))%EEWFN=Gradient(1+3*(Katom-1))%EEWFN+FXK
      Gradient(1+3*(Latom-1))%EEWFN=Gradient(1+3*(Latom-1))%EEWFN+FXL
      Gradient(2+3*(Iatom-1))%EEWFN=Gradient(2+3*(Iatom-1))%EEWFN+FYI
      Gradient(2+3*(Jatom-1))%EEWFN=Gradient(2+3*(Jatom-1))%EEWFN+FYJ
      Gradient(2+3*(Katom-1))%EEWFN=Gradient(2+3*(Katom-1))%EEWFN+FYK
      Gradient(2+3*(Latom-1))%EEWFN=Gradient(2+3*(Latom-1))%EEWFN+FYL
      Gradient(3+3*(Iatom-1))%EEWFN=Gradient(3+3*(Iatom-1))%EEWFN+FZI
      Gradient(3+3*(Jatom-1))%EEWFN=Gradient(3+3*(Jatom-1))%EEWFN+FZJ
      Gradient(3+3*(Katom-1))%EEWFN=Gradient(3+3*(Katom-1))%EEWFN+FZK
      Gradient(3+3*(Latom-1))%EEWFN=Gradient(3+3*(Latom-1))%EEWFN+FZL
      end if ! IFORMS.NE.8
!
! End of routine G2E_SPDF
      return
!
      contains
      subroutine DIPA (XIP, &
                       YIP, &
                       ZIP, &
                       XIPI, &
                       YIPI, &
                       ZIPI, &
                       RHOT2, &
                       LAMAX, LBMAX, LCMAX, LDMAX, &
                       IXYZNT)
!***********************************************************************
!     Date last modified: June 17, 1993                    Version 1.0 *
!     Author:                                                          *
!     Description: Calculate derivatives of XIP,YIP,ZIP wrt center I.  *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalar:
      integer, intent(IN) :: IXYZNT,LAMAX,LBMAX,LCMAX,LDMAX
      double precision, intent(IN) :: RHOT2
!
! Input arrays:
      double precision XIP(580),XIPI(405),YIP(580),YIPI(405),ZIP(580),ZIPI(405)
!
! Local scalars:
      integer I,IND,IP,IPI,IPJ,IPK,IP1,IP16,IP4,IP64,J,K,L
      double precision ATEMP,AXCNST,AYCNST,AZCNST,D2APR,D2AQR,RHOT2A,XIPA,XTEMP,YIPA,YTEMP,ZIPA,ZTEMP
!
! Begin:
      RHOT2A=RHOT2*AIAB
      AXCNST=D1ABX+PQX*RHOT2A
      AYCNST=D1ABY+PQY*RHOT2A
      AZCNST=D1ABZ+PQZ*RHOT2A
      IF(LPMAX.GT.1)then
      D2APR=RHOT2A*AB2I
      IF(LAMAX.GT.1)then
      XYZA1(2)=-BIAB-D2APR
      XYZA1(3)=XYZA1(2)+XYZA1(2)
      XYZA1(4)=XYZA1(3)+XYZA1(2)
      end if ! LAMAX
      IF(LBMAX.GT.1)then
      XYZA2(2)=AIAB-D2APR
      XYZA2(3)=XYZA2(2)+XYZA2(2)
      XYZA2(4)=XYZA2(3)+XYZA2(2)
      end if ! LBMAX
      end if ! LPMAX
      IF(LQMAX.GT.1)then
      D2AQR=RHOT2A*CD2I
      IF(LCMAX.GT.1)then
      XYZA3(2)=D2AQR
      XYZA3(3)=XYZA3(2)+XYZA3(2)
      XYZA3(4)=XYZA3(3)+XYZA3(2)
      end if ! LCMAX
      IF(LDMAX.GT.1)then
      XYZA4(2)=D2AQR
      XYZA4(3)=XYZA4(2)+XYZA4(2)
      XYZA4(4)=XYZA4(3)+XYZA4(2)
      end if ! LDMAX
      end if ! LQMAX
      IND=IXYZNT-1
      do I=1,LAMAX
      IPI=(I-1)*64+IXYZNT-1
      do J=1,LBMAX
      IPJ=(J-1)*16+IPI
      do K=1,LCMAX
      IPK=(K-1)*4+IPJ
      do L=1,LDMAX
      IP=IPK+L
      IND=IND+1
      XTEMP=XIP(IP)
      YTEMP=YIP(IP)
      ZTEMP=ZIP(IP)
      XIPA=AXCNST*XTEMP
      YIPA=AYCNST*YTEMP
      ZIPA=AZCNST*ZTEMP
      IF(I.GT.1)then
      IP64=IP-64
      XTEMP=XIP(IP64)
      YTEMP=YIP(IP64)
      ZTEMP=ZIP(IP64)
      ATEMP=XYZA1(I)
      XIPA=XIPA+ATEMP*XTEMP
      YIPA=YIPA+ATEMP*YTEMP
      ZIPA=ZIPA+ATEMP*ZTEMP
      end if ! I
      IF(J.GT.1)then
      IP16=IP-16
      XTEMP=XIP(IP16)
      YTEMP=YIP(IP16)
      ZTEMP=ZIP(IP16)
      ATEMP=XYZA2(J)
      XIPA=XIPA+ATEMP*XTEMP
      YIPA=YIPA+ATEMP*YTEMP
      ZIPA=ZIPA+ATEMP*ZTEMP
      end if ! J
      IF(K.GT.1)then
      IP4=IP-4
      XTEMP=XIP(IP4)
      YTEMP=YIP(IP4)
      ZTEMP=ZIP(IP4)
      ATEMP=XYZA3(K)
      XIPA=XIPA+ATEMP*XTEMP
      YIPA=YIPA+ATEMP*YTEMP
      ZIPA=ZIPA+ATEMP*ZTEMP
      end if ! K
      IF(L.GT.1)then
      IP1=IP-1
      XTEMP=XIP(IP1)
      YTEMP=YIP(IP1)
      ZTEMP=ZIP(IP1)
      ATEMP=XYZA4(L)
      XIPA=XIPA+ATEMP*XTEMP
      YIPA=YIPA+ATEMP*YTEMP
      ZIPA=ZIPA+ATEMP*ZTEMP
      end if ! L
      XIPI(IND)=XIPA
      YIPI(IND)=YIPA
      ZIPI(IND)=ZIPA
      end do ! L
      end do ! K
      end do ! J
      end do ! I
!
! Pack down XIP,YIP,ZIP.
!
      IND=IXYZNT-1
      do I=1,LAMAX
      IPI=(I-1)*64+IXYZNT-1
      do J=1,LBMAX
      IPJ=(J-1)*16+IPI
      do K=1,LCMAX
      IPK=(K-1)*4+IPJ
      do L=1,LDMAX
      IP=IPK+L
      IND=IND+1
      XIP(IND)=XIP(IP)
      YIP(IND)=YIP(IP)
      ZIP(IND)=ZIP(IP)
      end do ! L
      end do ! K
      end do ! J
      end do ! I
!
! End of routine DIPA
      return
      end subroutine DIPA
      subroutine DIPAB (XIP, &
                        YIP, &
                        ZIP, &
                        XIPI, &
                        YIPI, &
                        ZIPI, &
                        XIPJ, &
                        YIPJ, &
                        ZIPJ, &
                        RHOT2, &
                        LAMAX, LBMAX, LCMAX, LDMAX, &
                        IXYZNT)
!***********************************************************************
!     Date last modified: June 24, 1994                    Version 1.0 *
!     Author:                                                          *
!     Description: Calculate derivatives of XIP,YIP,ZIP with respect   *
!                  to centers I,J.                                     *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalar:
      integer, intent(IN) :: IXYZNT,LAMAX,LBMAX,LCMAX,LDMAX
      double precision, intent(IN) :: RHOT2
!
! Input arrays:
      double precision XIP(580),XIPI(405),XIPJ(405),YIP(580),YIPI(405),YIPJ(405),ZIP(580),ZIPI(405),ZIPJ(405)
!
! Local scalars:
      integer I,IND,IP,IPI,IPJ,IPK,IP1,IP16,IP4,IP64,J,K,L
      double precision ATEMP,AXCNST,AYCNST,AZCNST,BTEMP,BXCNST,BYCNST,BZCNST,D2APR,D2AQR,D2BPR,D2BQR,RHOT2A,RHOT2B, &
                       XIPA,XIPB,XTEMP,YIPA,YIPB,YTEMP,ZIPA,ZIPB,ZTEMP
!
! Begin:
      RHOT2A=RHOT2*AIAB
      RHOT2B=RHOT2*BIAB
      AXCNST= D1ABX+PQX*RHOT2A
      AYCNST= D1ABY+PQY*RHOT2A
      AZCNST= D1ABZ+PQZ*RHOT2A
      BXCNST=-D1ABX+PQX*RHOT2B
      BYCNST=-D1ABY+PQY*RHOT2B
      BZCNST=-D1ABZ+PQZ*RHOT2B
      IF(LPMAX.GT.1)then
      D2APR=RHOT2A*AB2I
      D2BPR=RHOT2B*AB2I
      IF(LAMAX.GT.1)then
        XYZA1(2)=-BIAB-D2APR
        XYZB1(2)= BIAB-D2BPR
        XYZA1(3)=XYZA1(2)+XYZA1(2)
        XYZB1(3)=XYZB1(2)+XYZB1(2)
        XYZA1(4)=XYZA1(3)+XYZA1(2)
        XYZB1(4)=XYZB1(3)+XYZB1(2)
      end if ! LAMAX
      IF(LBMAX.GT.1)then
        XYZA2(2)= AIAB-D2APR
        XYZB2(2)=-AIAB-D2BPR
        XYZA2(3)=XYZA2(2)+XYZA2(2)
        XYZB2(3)=XYZB2(2)+XYZB2(2)
        XYZA2(4)=XYZA2(3)+XYZA2(2)
        XYZB2(4)=XYZB2(3)+XYZB2(2)
      end if ! LBMAX
      end if ! LPMAX
      IF(LQMAX.GT.1)then
      D2AQR=RHOT2A*CD2I
      D2BQR=RHOT2B*CD2I
      IF(LCMAX.GT.1)then
        XYZA3(2)=D2AQR
        XYZB3(2)=D2BQR
        XYZA3(3)=XYZA3(2)+XYZA3(2)
        XYZB3(3)=XYZB3(2)+XYZB3(2)
        XYZA3(4)=XYZA3(3)+XYZA3(2)
        XYZB3(4)=XYZB3(3)+XYZB3(2)
      end if ! LCMAX
      IF(LDMAX.GT.1)then
        XYZA4(2)=D2AQR
        XYZB4(2)=D2BQR
        XYZA4(3)=XYZA4(2)+XYZA4(2)
        XYZB4(3)=XYZB4(2)+XYZB4(2)
        XYZA4(4)=XYZA4(3)+XYZA4(2)
        XYZB4(4)=XYZB4(3)+XYZB4(2)
      end if ! LDMAX
      end if ! LQMAX
!
      IND=IXYZNT-1
      do I=1,LAMAX
      IPI=(I-1)*64+IXYZNT-1
      do J=1,LBMAX
      IPJ=(J-1)*16+IPI
      do K=1,LCMAX
      IPK=(K-1)*4+IPJ
      do L=1,LDMAX
      IP=IPK+L
      IND=IND+1
      XTEMP=XIP(IP)
      YTEMP=YIP(IP)
      ZTEMP=ZIP(IP)
      XIPA=AXCNST*XTEMP
      XIPB=BXCNST*XTEMP
      YIPA=AYCNST*YTEMP
      YIPB=BYCNST*YTEMP
      ZIPA=AZCNST*ZTEMP
      ZIPB=BZCNST*ZTEMP
      IF(I.GT.1)then
        IP64=IP-64
        XTEMP=XIP(IP64)
        YTEMP=YIP(IP64)
        ZTEMP=ZIP(IP64)
        ATEMP=XYZA1(I)
        BTEMP=XYZB1(I)
        XIPA=XIPA+ATEMP*XTEMP
        XIPB=XIPB+BTEMP*XTEMP
        YIPA=YIPA+ATEMP*YTEMP
        YIPB=YIPB+BTEMP*YTEMP
        ZIPA=ZIPA+ATEMP*ZTEMP
        ZIPB=ZIPB+BTEMP*ZTEMP
      end if ! I
      IF(J.GT.1)then
        IP16=IP-16
        XTEMP=XIP(IP16)
        YTEMP=YIP(IP16)
        ZTEMP=ZIP(IP16)
        ATEMP=XYZA2(J)
        BTEMP=XYZB2(J)
        XIPA=XIPA+ATEMP*XTEMP
        XIPB=XIPB+BTEMP*XTEMP
        YIPA=YIPA+ATEMP*YTEMP
        YIPB=YIPB+BTEMP*YTEMP
        ZIPA=ZIPA+ATEMP*ZTEMP
        ZIPB=ZIPB+BTEMP*ZTEMP
      end if ! J
      IF(K.GT.1)then
        IP4=IP-4
        XTEMP=XIP(IP4)
        YTEMP=YIP(IP4)
        ZTEMP=ZIP(IP4)
        ATEMP=XYZA3(K)
        BTEMP=XYZB3(K)
        XIPA=XIPA+ATEMP*XTEMP
        XIPB=XIPB+BTEMP*XTEMP
        YIPA=YIPA+ATEMP*YTEMP
        YIPB=YIPB+BTEMP*YTEMP
        ZIPA=ZIPA+ATEMP*ZTEMP
        ZIPB=ZIPB+BTEMP*ZTEMP
      end if ! K
      IF(L.GT.1)then
        IP1=IP-1
        XTEMP=XIP(IP1)
        YTEMP=YIP(IP1)
        ZTEMP=ZIP(IP1)
        ATEMP=XYZA4(L)
        BTEMP=XYZB4(L)
        XIPA=XIPA+ATEMP*XTEMP
        XIPB=XIPB+BTEMP*XTEMP
        YIPA=YIPA+ATEMP*YTEMP
        YIPB=YIPB+BTEMP*YTEMP
        ZIPA=ZIPA+ATEMP*ZTEMP
        ZIPB=ZIPB+BTEMP*ZTEMP
      end if ! L
      XIPI(IND)=XIPA
      YIPI(IND)=YIPA
      ZIPI(IND)=ZIPA
      XIPJ(IND)=XIPB
      YIPJ(IND)=YIPB
      ZIPJ(IND)=ZIPB
      end do ! L
      end do ! K
      end do ! J
      end do ! I
!
! Pack down XIP,YIP,ZIP.
      IND=IXYZNT-1
      do I=1,LAMAX
      IPI=(I-1)*64+IXYZNT-1
      do J=1,LBMAX
      IPJ=(J-1)*16+IPI
      do K=1,LCMAX
      IPK=(K-1)*4+IPJ
      do L=1,LDMAX
      IP=IPK+L
      IND=IND+1
      XIP(IND)=XIP(IP)
      YIP(IND)=YIP(IP)
      ZIP(IND)=ZIP(IP)
      end do ! L
      end do ! K
      end do ! J
      end do ! I
!
! End of routine DIPAB
      return
      end subroutine DIPAB
      subroutine DIPABI (XIP, &
                         YIP, &
                         ZIP, &
                         XIPI, &
                         YIPI, &
                         ZIPI, &
                         RHOT2, &
                         LAMAX, LBMAX, LCMAX, LDMAX, &
                         IXYZNT)
!***********************************************************************
!     Date last modified: June 27, 1994                    Version 1.0 *
!     Author:                                                          *
!     Description: Calculate derivatives of XIP,YIP,ZIP with respect   *
!                  to centers I,J.                                     *
!     This routine is a copy of DIPAB, modified for the case I=J       *
!     to avoid potential compiler problems.                            *
!                                                                      *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalar:
      integer, intent(IN) :: IXYZNT,LAMAX,LBMAX,LCMAX,LDMAX
      double precision, intent(IN) :: RHOT2
!
! Input arrays:
      double precision XIP(580),XIPI(405),YIP(580),YIPI(405),ZIP(580),ZIPI(405)
!
! Local scalars:
      integer I,IND,IP,IPI,IPJ,IPK,IP1,IP16,IP4,IP64,J,K,L
      double precision ATEMP,AXCNST,AYCNST,AZCNST,BTEMP,BXCNST,BYCNST,BZCNST,D2APR,D2AQR,D2BPR,D2BQR,RHOT2A,RHOT2B, &
                       XIPA,XIPB,XTEMP,YIPA,YIPB,YTEMP,ZIPA,ZIPB,ZTEMP
!
! Begin:
      RHOT2A=RHOT2*AIAB
      RHOT2B=RHOT2*BIAB
      AXCNST= D1ABX+PQX*RHOT2A
      AYCNST= D1ABY+PQY*RHOT2A
      AZCNST= D1ABZ+PQZ*RHOT2A
      BXCNST=-D1ABX+PQX*RHOT2B
      BYCNST=-D1ABY+PQY*RHOT2B
      BZCNST=-D1ABZ+PQZ*RHOT2B
      IF(LPMAX.GT.1)then
      D2APR=RHOT2A*AB2I
      D2BPR=RHOT2B*AB2I
      IF(LAMAX.GT.1)then
      XYZA1(2)=-BIAB-D2APR
      XYZB1(2)= BIAB-D2BPR
      XYZA1(3)=XYZA1(2)+XYZA1(2)
      XYZB1(3)=XYZB1(2)+XYZB1(2)
      XYZA1(4)=XYZA1(3)+XYZA1(2)
      XYZB1(4)=XYZB1(3)+XYZB1(2)
      end if ! LAMAX
      IF(LBMAX.GT.1)then
      XYZA2(2)= AIAB-D2APR
      XYZB2(2)=-AIAB-D2BPR
      XYZA2(3)=XYZA2(2)+XYZA2(2)
      XYZB2(3)=XYZB2(2)+XYZB2(2)
      XYZA2(4)=XYZA2(3)+XYZA2(2)
      XYZB2(4)=XYZB2(3)+XYZB2(2)
      end if ! LBMAX
      end if ! LPMAX
      IF(LQMAX.GT.1)then
      D2AQR=RHOT2A*CD2I
      D2BQR=RHOT2B*CD2I
      IF(LCMAX.GT.1)then
      XYZA3(2)=D2AQR
      XYZB3(2)=D2BQR
      XYZA3(3)=XYZA3(2)+XYZA3(2)
      XYZB3(3)=XYZB3(2)+XYZB3(2)
      XYZA3(4)=XYZA3(3)+XYZA3(2)
      XYZB3(4)=XYZB3(3)+XYZB3(2)
      end if ! LCMAX
      IF(LDMAX.GT.1)then
      XYZA4(2)=D2AQR
      XYZB4(2)=D2BQR
      XYZA4(3)=XYZA4(2)+XYZA4(2)
      XYZB4(3)=XYZB4(2)+XYZB4(2)
      XYZA4(4)=XYZA4(3)+XYZA4(2)
      XYZB4(4)=XYZB4(3)+XYZB4(2)
      end if ! LDMAX
      end if ! LQMAX
!
      IND=IXYZNT-1
      do I=1,LAMAX
      IPI=(I-1)*64+IXYZNT-1
      do  J=1,LBMAX
      IPJ=(J-1)*16+IPI
      do  K=1,LCMAX
      IPK=(K-1)*4+IPJ
      do  L=1,LDMAX
      IP=IPK+L
      IND=IND+1
      XTEMP=XIP(IP)
      YTEMP=YIP(IP)
      ZTEMP=ZIP(IP)
      XIPA=AXCNST*XTEMP
      XIPB=BXCNST*XTEMP
      YIPA=AYCNST*YTEMP
      YIPB=BYCNST*YTEMP
      ZIPA=AZCNST*ZTEMP
      ZIPB=BZCNST*ZTEMP
      IF(I.GT.1)then
      IP64=IP-64
      XTEMP=XIP(IP64)
      YTEMP=YIP(IP64)
      ZTEMP=ZIP(IP64)
      ATEMP=XYZA1(I)
      BTEMP=XYZB1(I)
      XIPA=XIPA+ATEMP*XTEMP
      XIPB=XIPB+BTEMP*XTEMP
      YIPA=YIPA+ATEMP*YTEMP
      YIPB=YIPB+BTEMP*YTEMP
      ZIPA=ZIPA+ATEMP*ZTEMP
      ZIPB=ZIPB+BTEMP*ZTEMP
      end if ! I
      IF(J.GT.1)then
      IP16=IP-16
      XTEMP=XIP(IP16)
      YTEMP=YIP(IP16)
      ZTEMP=ZIP(IP16)
      ATEMP=XYZA2(J)
      BTEMP=XYZB2(J)
      XIPA=XIPA+ATEMP*XTEMP
      XIPB=XIPB+BTEMP*XTEMP
      YIPA=YIPA+ATEMP*YTEMP
      YIPB=YIPB+BTEMP*YTEMP
      ZIPA=ZIPA+ATEMP*ZTEMP
      ZIPB=ZIPB+BTEMP*ZTEMP
      end if ! J
      IF(K.GT.1)then
      IP4=IP-4
      XTEMP=XIP(IP4)
      YTEMP=YIP(IP4)
      ZTEMP=ZIP(IP4)
      ATEMP=XYZA3(K)
      BTEMP=XYZB3(K)
      XIPA=XIPA+ATEMP*XTEMP
      XIPB=XIPB+BTEMP*XTEMP
      YIPA=YIPA+ATEMP*YTEMP
      YIPB=YIPB+BTEMP*YTEMP
      ZIPA=ZIPA+ATEMP*ZTEMP
      ZIPB=ZIPB+BTEMP*ZTEMP
      end if ! K
      IF(L.GT.1)then
      IP1=IP-1
      XTEMP=XIP(IP1)
      YTEMP=YIP(IP1)
      ZTEMP=ZIP(IP1)
      ATEMP=XYZA4(L)
      BTEMP=XYZB4(L)
      XIPA=XIPA+ATEMP*XTEMP
      XIPB=XIPB+BTEMP*XTEMP
      YIPA=YIPA+ATEMP*YTEMP
      YIPB=YIPB+BTEMP*YTEMP
      ZIPA=ZIPA+ATEMP*ZTEMP
      ZIPB=ZIPB+BTEMP*ZTEMP
      end if ! L
      XIPI(IND)=XIPA+XIPB
      YIPI(IND)=YIPA+YIPB
      ZIPI(IND)=ZIPA+ZIPB
      end do ! L
      end do ! K
      end do ! J
      end do ! I
!
! Pack down XIP,YIP,ZIP.
      IND=IXYZNT-1
      do I=1,LAMAX
      IPI=(I-1)*64+IXYZNT-1
      do J=1,LBMAX
      IPJ=(J-1)*16+IPI
      do K=1,LCMAX
      IPK=(K-1)*4+IPJ
      do L=1,LDMAX
      IP=IPK+L
      IND=IND+1
      XIP(IND)=XIP(IP)
      YIP(IND)=YIP(IP)
      ZIP(IND)=ZIP(IP)
      end do ! L
      end do ! K
      end do ! J
      end do ! I
!
! End of routine DIPABI
      return
      end subroutine DIPABI
      subroutine DIPABC (XIP, &
                         YIP, &
                         ZIP, &
                         XIPI, &
                         YIPI, &
                         ZIPI, &
                         XIPJ, &
                         YIPJ, &
                         ZIPJ, &
                         XIPK, &
                         YIPK, &
                         ZIPK, &
                         RHOT2, &
                         LAMAX, LBMAX, LCMAX, LDMAX, &
                         IXYZNT)
!***********************************************************************
!     Date last modified: June 27, 1994                    Version 1.0 *
!     Author:                                                          *
!     Description: Calculate derivatives of XIP,YIP,ZIP with respect   *
!                  to centers I,J,K.                                   *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalar:
      integer, intent(IN) :: IXYZNT,LAMAX,LBMAX,LCMAX,LDMAX
      double precision, intent(IN) :: RHOT2
!
! Input arrays:
      double precision XIP(580),XIPI(405),XIPJ(405),XIPK(405),YIP(580),YIPI(405),YIPJ(405),YIPK(405),ZIP(580),ZIPI(405), &
                       ZIPJ(405),ZIPK(405)
!
! Local scalars:
      integer I,IND,IP,IPI,IPJ,IPK,IP1,IP16,IP4,IP64,J,K,L
      double precision ATEMP,AXCNST,AYCNST,AZCNST,BTEMP,BXCNST,BYCNST,BZCNST,CTEMP,CXCNST,CYCNST,CZCNST,D2APR,D2AQR, &
                       D2BPR,D2BQR,D2CPR,D2CQR,RHOT2A,RHOT2B,RHOT2C,XIPA,XIPB,XIPC,XTEMP,YIPA,YIPB,YIPC,YTEMP,ZIPA, &
                       ZIPB,ZIPC,ZTEMP
!
! Begin:
      RHOT2A=RHOT2*AIAB
      RHOT2B=RHOT2*BIAB
      RHOT2C=RHOT2*CICD
      AXCNST= D1ABX+PQX*RHOT2A
      AYCNST= D1ABY+PQY*RHOT2A
      AZCNST= D1ABZ+PQZ*RHOT2A
      BXCNST=-D1ABX+PQX*RHOT2B
      BYCNST=-D1ABY+PQY*RHOT2B
      BZCNST=-D1ABZ+PQZ*RHOT2B
      CXCNST= D1CDX-PQX*RHOT2C
      CYCNST= D1CDY-PQY*RHOT2C
      CZCNST= D1CDZ-PQZ*RHOT2C
      IF(LPMAX.GT.1)then
      D2APR=RHOT2A*AB2I
      D2BPR=RHOT2B*AB2I
      D2CPR=RHOT2C*AB2I
      IF(LAMAX.GT.1)then
      XYZA1(2)=-BIAB-D2APR
      XYZB1(2)= BIAB-D2BPR
      XYZC1(2)=D2CPR
      XYZA1(3)=XYZA1(2)+XYZA1(2)
      XYZB1(3)=XYZB1(2)+XYZB1(2)
      XYZC1(3)=XYZC1(2)+XYZC1(2)
      XYZA1(4)=XYZA1(3)+XYZA1(2)
      XYZB1(4)=XYZB1(3)+XYZB1(2)
      XYZC1(4)=XYZC1(3)+XYZC1(2)
      end if ! LAMAX
      IF(LBMAX.GT.1)then
      XYZA2(2)= AIAB-D2APR
      XYZB2(2)=-AIAB-D2BPR
      XYZC2(2)=D2CPR
      XYZA2(3)=XYZA2(2)+XYZA2(2)
      XYZB2(3)=XYZB2(2)+XYZB2(2)
      XYZC2(3)=XYZC2(2)+XYZC2(2)
      XYZA2(4)=XYZA2(3)+XYZA2(2)
      XYZB2(4)=XYZB2(3)+XYZB2(2)
      XYZC2(4)=XYZC2(3)+XYZC2(2)
      end if ! LBMAX
      end if ! LPMAX
      IF(LQMAX.GT.1)then
      D2AQR=RHOT2A*CD2I
      D2BQR=RHOT2B*CD2I
      D2CQR=RHOT2C*CD2I
      IF(LCMAX.GT.1)then
      XYZA3(2)=D2AQR
      XYZB3(2)=D2BQR
      XYZC3(2)=-DICD-D2CQR
      XYZA3(3)=XYZA3(2)+XYZA3(2)
      XYZB3(3)=XYZB3(2)+XYZB3(2)
      XYZC3(3)=XYZC3(2)+XYZC3(2)
      XYZA3(4)=XYZA3(3)+XYZA3(2)
      XYZB3(4)=XYZB3(3)+XYZB3(2)
      XYZC3(4)=XYZC3(3)+XYZC3(2)
      end if ! LCMAX
      IF(LDMAX.GT.1)then
      XYZA4(2)=D2AQR
      XYZB4(2)=D2BQR
      XYZC4(2)=+CICD-D2CQR
      XYZA4(3)=XYZA4(2)+XYZA4(2)
      XYZB4(3)=XYZB4(2)+XYZB4(2)
      XYZC4(3)=XYZC4(2)+XYZC4(2)
      XYZA4(4)=XYZA4(3)+XYZA4(2)
      XYZB4(4)=XYZB4(3)+XYZB4(2)
      XYZC4(4)=XYZC4(3)+XYZC4(2)
      end if ! LDMAX
      end if ! LQMAX
!
      IND=IXYZNT-1
      do I=1,LAMAX
      IPI=(I-1)*64+IXYZNT-1
      do J=1,LBMAX
      IPJ=(J-1)*16+IPI
      do K=1,LCMAX
      IPK=(K-1)*4+IPJ
      do L=1,LDMAX
      IP=IPK+L
      IND=IND+1
      XTEMP=XIP(IP)
      YTEMP=YIP(IP)
      ZTEMP=ZIP(IP)
      XIPA=AXCNST*XTEMP
      XIPB=BXCNST*XTEMP
      XIPC=CXCNST*XTEMP
      YIPA=AYCNST*YTEMP
      YIPB=BYCNST*YTEMP
      YIPC=CYCNST*YTEMP
      ZIPA=AZCNST*ZTEMP
      ZIPB=BZCNST*ZTEMP
      ZIPC=CZCNST*ZTEMP
      IF(I.GT.1)then
      IP64=IP-64
      XTEMP=XIP(IP64)
      YTEMP=YIP(IP64)
      ZTEMP=ZIP(IP64)
      ATEMP=XYZA1(I)
      BTEMP=XYZB1(I)
      CTEMP=XYZC1(I)
      XIPA=XIPA+ATEMP*XTEMP
      XIPB=XIPB+BTEMP*XTEMP
      XIPC=XIPC+CTEMP*XTEMP
      YIPA=YIPA+ATEMP*YTEMP
      YIPB=YIPB+BTEMP*YTEMP
      YIPC=YIPC+CTEMP*YTEMP
      ZIPA=ZIPA+ATEMP*ZTEMP
      ZIPB=ZIPB+BTEMP*ZTEMP
      ZIPC=ZIPC+CTEMP*ZTEMP
      end if ! I
      IF(J.GT.1)then
      IP16=IP-16
      XTEMP=XIP(IP16)
      YTEMP=YIP(IP16)
      ZTEMP=ZIP(IP16)
      ATEMP=XYZA2(J)
      BTEMP=XYZB2(J)
      CTEMP=XYZC2(J)
      XIPA=XIPA+ATEMP*XTEMP
      XIPB=XIPB+BTEMP*XTEMP
      XIPC=XIPC+CTEMP*XTEMP
      YIPA=YIPA+ATEMP*YTEMP
      YIPB=YIPB+BTEMP*YTEMP
      YIPC=YIPC+CTEMP*YTEMP
      ZIPA=ZIPA+ATEMP*ZTEMP
      ZIPB=ZIPB+BTEMP*ZTEMP
      ZIPC=ZIPC+CTEMP*ZTEMP
      end if ! J
      IF(K.GT.1)then
      IP4=IP-4
      XTEMP=XIP(IP4)
      YTEMP=YIP(IP4)
      ZTEMP=ZIP(IP4)
      ATEMP=XYZA3(K)
      BTEMP=XYZB3(K)
      CTEMP=XYZC3(K)
      XIPA=XIPA+ATEMP*XTEMP
      XIPB=XIPB+BTEMP*XTEMP
      XIPC=XIPC+CTEMP*XTEMP
      YIPA=YIPA+ATEMP*YTEMP
      YIPB=YIPB+BTEMP*YTEMP
      YIPC=YIPC+CTEMP*YTEMP
      ZIPA=ZIPA+ATEMP*ZTEMP
      ZIPB=ZIPB+BTEMP*ZTEMP
      ZIPC=ZIPC+CTEMP*ZTEMP
      end if ! K
      IF(L.GT.1)then
      IP1=IP-1
      XTEMP=XIP(IP1)
      YTEMP=YIP(IP1)
      ZTEMP=ZIP(IP1)
      ATEMP=XYZA4(L)
      BTEMP=XYZB4(L)
      CTEMP=XYZC4(L)
      XIPA=XIPA+ATEMP*XTEMP
      XIPB=XIPB+BTEMP*XTEMP
      XIPC=XIPC+CTEMP*XTEMP
      YIPA=YIPA+ATEMP*YTEMP
      YIPB=YIPB+BTEMP*YTEMP
      YIPC=YIPC+CTEMP*YTEMP
      ZIPA=ZIPA+ATEMP*ZTEMP
      ZIPB=ZIPB+BTEMP*ZTEMP
      ZIPC=ZIPC+CTEMP*ZTEMP
      end if ! L
      XIPI(IND)=XIPA
      YIPI(IND)=YIPA
      ZIPI(IND)=ZIPA
      XIPJ(IND)=XIPB
      YIPJ(IND)=YIPB
      ZIPJ(IND)=ZIPB
      XIPK(IND)=XIPC
      YIPK(IND)=YIPC
      ZIPK(IND)=ZIPC
      end do ! L
      end do ! K
      end do ! J
      end do ! I
!
! Pack down XIP,YIP,ZIP.
      IND=IXYZNT-1
      do I=1,LAMAX
      IPI=(I-1)*64+IXYZNT-1
      do J=1,LBMAX
      IPJ=(J-1)*16+IPI
      do K=1,LCMAX
      IPK=(K-1)*4+IPJ
      do L=1,LDMAX
      IP=IPK+L
      IND=IND+1
      XIP(IND)=XIP(IP)
      YIP(IND)=YIP(IP)
      ZIP(IND)=ZIP(IP)
      end do ! L
      end do ! K
      end do ! J
      end do ! I
!
! End of routine DIPABC
      return
      end subroutine DIPABC
      subroutine DIPAC (XIP, &
                        YIP, &
                        ZIP, &
                        XIPI, &
                        YIPI, &
                        ZIPI, &
                        XIPK, &
                        YIPK, &
                        ZIPK, &
                        RHOT2, &
                        LAMAX, LBMAX, LCMAX, LDMAX, &
                        IXYZNT)
!***********************************************************************
!     Date last modified: June 27, 1994                    Version 1.0 *
!     Description: Calculate derivatives of XIP,YIP,ZIP with respect   *
!                  to centers I,K.                                     *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalar:
      integer, intent(IN) :: IXYZNT,LAMAX,LBMAX,LCMAX,LDMAX
      double precision, intent(IN) :: RHOT2
!
! Input arrays:
      double precision XIP(580),XIPI(405),XIPK(405),YIP(580),YIPI(405),YIPK(405),ZIP(580),ZIPI(405),ZIPK(405)
!
! Local scalars:
      integer I,IND,IP,IPI,IPJ,IPK,IP1,IP16,IP4,IP64,J,K,L
      double precision ATEMP,AXCNST,AYCNST,AZCNST,CTEMP,CXCNST,CYCNST,CZCNST,D2APR,D2AQR,D2CPR,D2CQR,RHOT2A,RHOT2C, &
                       XIPA,XIPC,XTEMP,YIPA,YIPC,YTEMP,ZIPA,ZIPC,ZTEMP
!
! Begin:
      RHOT2A=RHOT2*AIAB
      RHOT2C=RHOT2*CICD
      AXCNST=D1ABX+PQX*RHOT2A
      AYCNST=D1ABY+PQY*RHOT2A
      AZCNST=D1ABZ+PQZ*RHOT2A
      CXCNST=D1CDX-PQX*RHOT2C
      CYCNST=D1CDY-PQY*RHOT2C
      CZCNST=D1CDZ-PQZ*RHOT2C
      IF(LPMAX.GT.1)then
      D2APR=RHOT2A*AB2I
      D2CPR=RHOT2C*AB2I
      IF(LAMAX.GT.1)then
      XYZA1(2)=-BIAB-D2APR
      XYZC1(2)=D2CPR
      XYZA1(3)=XYZA1(2)+XYZA1(2)
      XYZC1(3)=XYZC1(2)+XYZC1(2)
      XYZA1(4)=XYZA1(3)+XYZA1(2)
      XYZC1(4)=XYZC1(3)+XYZC1(2)
      end if ! LAMAX
      IF(LBMAX.GT.1)then
      XYZA2(2)=AIAB-D2APR
      XYZC2(2)=D2CPR
      XYZA2(3)=XYZA2(2)+XYZA2(2)
      XYZC2(3)=XYZC2(2)+XYZC2(2)
      XYZA2(4)=XYZA2(3)+XYZA2(2)
      XYZC2(4)=XYZC2(3)+XYZC2(2)
      end if ! LBMAX
      end if ! LPMAX
      IF(LQMAX.GT.1)then
      D2AQR=RHOT2A*CD2I
      D2CQR=RHOT2C*CD2I
      IF(LCMAX.GT.1)then
      XYZA3(2)=D2AQR
      XYZC3(2)=-DICD-D2CQR
      XYZA3(3)=XYZA3(2)+XYZA3(2)
      XYZC3(3)=XYZC3(2)+XYZC3(2)
      XYZA3(4)=XYZA3(3)+XYZA3(2)
      XYZC3(4)=XYZC3(3)+XYZC3(2)
      end if ! LCMAX
      IF(LDMAX.GT.1)then
      XYZA4(2)=D2AQR
      XYZC4(2)=+CICD-D2CQR
      XYZA4(3)=XYZA4(2)+XYZA4(2)
      XYZC4(3)=XYZC4(2)+XYZC4(2)
      XYZA4(4)=XYZA4(3)+XYZA4(2)
      XYZC4(4)=XYZC4(3)+XYZC4(2)
      end if ! LDMAX
      end if ! LQMAX
!
      IND=IXYZNT-1
      do I=1,LAMAX
      IPI=(I-1)*64+IXYZNT-1
      do J=1,LBMAX
      IPJ=(J-1)*16+IPI
      do K=1,LCMAX
      IPK=(K-1)*4+IPJ
      do L=1,LDMAX
      IP=IPK+L
      IND=IND+1
      XTEMP=XIP(IP)
      YTEMP=YIP(IP)
      ZTEMP=ZIP(IP)
      XIPA=AXCNST*XTEMP
      XIPC=CXCNST*XTEMP
      YIPA=AYCNST*YTEMP
      YIPC=CYCNST*YTEMP
      ZIPA=AZCNST*ZTEMP
      ZIPC=CZCNST*ZTEMP
      IF(I.GT.1)then
      IP64=IP-64
      XTEMP=XIP(IP64)
      YTEMP=YIP(IP64)
      ZTEMP=ZIP(IP64)
      ATEMP=XYZA1(I)
      CTEMP=XYZC1(I)
      XIPA=XIPA+ATEMP*XTEMP
      XIPC=XIPC+CTEMP*XTEMP
      YIPA=YIPA+ATEMP*YTEMP
      YIPC=YIPC+CTEMP*YTEMP
      ZIPA=ZIPA+ATEMP*ZTEMP
      ZIPC=ZIPC+CTEMP*ZTEMP
      end if ! I
      IF(J.GT.1)then
      IP16=IP-16
      XTEMP=XIP(IP16)
      YTEMP=YIP(IP16)
      ZTEMP=ZIP(IP16)
      ATEMP=XYZA2(J)
      CTEMP=XYZC2(J)
      XIPA=XIPA+ATEMP*XTEMP
      XIPC=XIPC+CTEMP*XTEMP
      YIPA=YIPA+ATEMP*YTEMP
      YIPC=YIPC+CTEMP*YTEMP
      ZIPA=ZIPA+ATEMP*ZTEMP
      ZIPC=ZIPC+CTEMP*ZTEMP
      end if ! J
      IF(K.GT.1)then
      IP4=IP-4
      XTEMP=XIP(IP4)
      YTEMP=YIP(IP4)
      ZTEMP=ZIP(IP4)
      ATEMP=XYZA3(K)
      CTEMP=XYZC3(K)
      XIPA=XIPA+ATEMP*XTEMP
      XIPC=XIPC+CTEMP*XTEMP
      YIPA=YIPA+ATEMP*YTEMP
      YIPC=YIPC+CTEMP*YTEMP
      ZIPA=ZIPA+ATEMP*ZTEMP
      ZIPC=ZIPC+CTEMP*ZTEMP
      end if ! K
      IF(L.GT.1)then
      IP1=IP-1
      XTEMP=XIP(IP1)
      YTEMP=YIP(IP1)
      ZTEMP=ZIP(IP1)
      ATEMP=XYZA4(L)
      CTEMP=XYZC4(L)
      XIPA=XIPA+ATEMP*XTEMP
      XIPC=XIPC+CTEMP*XTEMP
      YIPA=YIPA+ATEMP*YTEMP
      YIPC=YIPC+CTEMP*YTEMP
      ZIPA=ZIPA+ATEMP*ZTEMP
      ZIPC=ZIPC+CTEMP*ZTEMP
      end if ! L
      XIPI(IND)=XIPA
      YIPI(IND)=YIPA
      ZIPI(IND)=ZIPA
      XIPK(IND)=XIPC
      YIPK(IND)=YIPC
      ZIPK(IND)=ZIPC
      end do ! L
      end do ! K
      end do ! J
      end do ! I
!
! Pack down XIP,YIP,ZIP.
      IND=IXYZNT-1
      do I=1,LAMAX
      IPI=(I-1)*64+IXYZNT-1
      do J=1,LBMAX
      IPJ=(J-1)*16+IPI
      do K=1,LCMAX
      IPK=(K-1)*4+IPJ
      do L=1,LDMAX
      IP=IPK+L
      IND=IND+1
      XIP(IND)=XIP(IP)
      YIP(IND)=YIP(IP)
      ZIP(IND)=ZIP(IP)
      end do ! L
      end do ! K
      end do ! J
      end do ! I
!
! End of routine DIPAC
      return
      end subroutine DIPAC
      subroutine DIPACI (XIP, &
                         YIP, &
                         ZIP, &
                         XIPI, &
                         YIPI, &
                         ZIPI, &
                         RHOT2, &
                         LAMAX, LBMAX, LCMAX, LDMAX, &
                         IXYZNT)
!***********************************************************************
!     Date last modified: June 27, 1994                    Version 1.0 *
!     Author: Mike Peterson, Dept. of Chemistry, U. of T.              *
!     Description: Calculate derivatives of XIP,YIP,ZIP with respect   *
!                  to centers I,K.                                     *
!     This routine is a copy of DIPAC, modified for the case I=K       *
!     to avoid potential compiler problems.                            *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalar:
      integer, intent(IN) :: IXYZNT,LAMAX,LBMAX,LCMAX,LDMAX
      double precision, intent(IN) :: RHOT2
!
! Input arrays:
      double precision XIP(580),XIPI(405),YIP(580),YIPI(405),ZIP(580),ZIPI(405)
!
! Local scalars:
      integer I,IND,IP,IPI,IPJ,IPK,IP1,IP16,IP4,IP64,J,K,L
      double precision ATEMP,AXCNST,AYCNST,AZCNST,CTEMP,CXCNST,CYCNST,CZCNST,D2APR,D2AQR,D2CPR,D2CQR,RHOT2A,RHOT2C, &
                       XIPA,XIPC,XTEMP,YIPA,YIPC,YTEMP,ZIPA,ZIPC,ZTEMP
!
! Begin:
      RHOT2A=RHOT2*AIAB
      RHOT2C=RHOT2*CICD
      AXCNST=D1ABX+PQX*RHOT2A
      AYCNST=D1ABY+PQY*RHOT2A
      AZCNST=D1ABZ+PQZ*RHOT2A
      CXCNST=D1CDX-PQX*RHOT2C
      CYCNST=D1CDY-PQY*RHOT2C
      CZCNST=D1CDZ-PQZ*RHOT2C
      IF(LPMAX.GT.1)then
      D2APR=RHOT2A*AB2I
      D2CPR=RHOT2C*AB2I
      IF(LAMAX.GT.1)then
      XYZA1(2)=-BIAB-D2APR
      XYZC1(2)=D2CPR
      XYZA1(3)=XYZA1(2)+XYZA1(2)
      XYZC1(3)=XYZC1(2)+XYZC1(2)
      XYZA1(4)=XYZA1(3)+XYZA1(2)
      XYZC1(4)=XYZC1(3)+XYZC1(2)
      end if ! LAMAX
      IF(LBMAX.GT.1)then
      XYZA2(2)=AIAB-D2APR
      XYZC2(2)=D2CPR
      XYZA2(3)=XYZA2(2)+XYZA2(2)
      XYZC2(3)=XYZC2(2)+XYZC2(2)
      XYZA2(4)=XYZA2(3)+XYZA2(2)
      XYZC2(4)=XYZC2(3)+XYZC2(2)
      end if ! LBMAX
      end if ! LPMAX
      IF(LQMAX.GT.1)then
      D2AQR=RHOT2A*CD2I
      D2CQR=RHOT2C*CD2I
      IF(LCMAX.GT.1)then
      XYZA3(2)=D2AQR
      XYZC3(2)=-DICD-D2CQR
      XYZA3(3)=XYZA3(2)+XYZA3(2)
      XYZC3(3)=XYZC3(2)+XYZC3(2)
      XYZA3(4)=XYZA3(3)+XYZA3(2)
      XYZC3(4)=XYZC3(3)+XYZC3(2)
      end if ! LCMAX
      IF(LDMAX.GT.1)then
      XYZA4(2)=D2AQR
      XYZC4(2)=+CICD-D2CQR
      XYZA4(3)=XYZA4(2)+XYZA4(2)
      XYZC4(3)=XYZC4(2)+XYZC4(2)
      XYZA4(4)=XYZA4(3)+XYZA4(2)
      XYZC4(4)=XYZC4(3)+XYZC4(2)
      end if ! LDMAX
      end if ! LQMAX
!
      IND=IXYZNT-1
      do I=1,LAMAX
      IPI=(I-1)*64+IXYZNT-1
      do J=1,LBMAX
      IPJ=(J-1)*16+IPI
      do K=1,LCMAX
      IPK=(K-1)*4+IPJ
      do L=1,LDMAX
      IP=IPK+L
      IND=IND+1
      XTEMP=XIP(IP)
      YTEMP=YIP(IP)
      ZTEMP=ZIP(IP)
      XIPA=AXCNST*XTEMP
      XIPC=CXCNST*XTEMP
      YIPA=AYCNST*YTEMP
      YIPC=CYCNST*YTEMP
      ZIPA=AZCNST*ZTEMP
      ZIPC=CZCNST*ZTEMP
      IF(I.GT.1)then
      IP64=IP-64
      XTEMP=XIP(IP64)
      YTEMP=YIP(IP64)
      ZTEMP=ZIP(IP64)
      ATEMP=XYZA1(I)
      CTEMP=XYZC1(I)
      XIPA=XIPA+ATEMP*XTEMP
      XIPC=XIPC+CTEMP*XTEMP
      YIPA=YIPA+ATEMP*YTEMP
      YIPC=YIPC+CTEMP*YTEMP
      ZIPA=ZIPA+ATEMP*ZTEMP
      ZIPC=ZIPC+CTEMP*ZTEMP
      end if ! I
      IF(J.GT.1)then
      IP16=IP-16
      XTEMP=XIP(IP16)
      YTEMP=YIP(IP16)
      ZTEMP=ZIP(IP16)
      ATEMP=XYZA2(J)
      CTEMP=XYZC2(J)
      XIPA=XIPA+ATEMP*XTEMP
      XIPC=XIPC+CTEMP*XTEMP
      YIPA=YIPA+ATEMP*YTEMP
      YIPC=YIPC+CTEMP*YTEMP
      ZIPA=ZIPA+ATEMP*ZTEMP
      ZIPC=ZIPC+CTEMP*ZTEMP
      end if ! J
      IF(K.GT.1)then
      IP4=IP-4
      XTEMP=XIP(IP4)
      YTEMP=YIP(IP4)
      ZTEMP=ZIP(IP4)
      ATEMP=XYZA3(K)
      CTEMP=XYZC3(K)
      XIPA=XIPA+ATEMP*XTEMP
      XIPC=XIPC+CTEMP*XTEMP
      YIPA=YIPA+ATEMP*YTEMP
      YIPC=YIPC+CTEMP*YTEMP
      ZIPA=ZIPA+ATEMP*ZTEMP
      ZIPC=ZIPC+CTEMP*ZTEMP
      end if ! K
      IF(L.GT.1)then
      IP1=IP-1
      XTEMP=XIP(IP1)
      YTEMP=YIP(IP1)
      ZTEMP=ZIP(IP1)
      ATEMP=XYZA4(L)
      CTEMP=XYZC4(L)
      XIPA=XIPA+ATEMP*XTEMP
      XIPC=XIPC+CTEMP*XTEMP
      YIPA=YIPA+ATEMP*YTEMP
      YIPC=YIPC+CTEMP*YTEMP
      ZIPA=ZIPA+ATEMP*ZTEMP
      ZIPC=ZIPC+CTEMP*ZTEMP
      end if ! L
      XIPI(IND)=XIPA+XIPC
      YIPI(IND)=YIPA+YIPC
      ZIPI(IND)=ZIPA+ZIPC
      end do ! L
      end do ! K
      end do ! J
      end do ! I
!
! Pack down XIP,YIP,ZIP.
      IND=IXYZNT-1
      do I=1,LAMAX
      IPI=(I-1)*64+IXYZNT-1
      do J=1,LBMAX
      IPJ=(J-1)*16+IPI
      do K=1,LCMAX
      IPK=(K-1)*4+IPJ
      do L=1,LDMAX
      IP=IPK+L
      IND=IND+1
      XIP(IND)=XIP(IP)
      YIP(IND)=YIP(IP)
      ZIP(IND)=ZIP(IP)
      end do ! L
      end do ! K
      end do ! J
      end do ! I
!
! End of routine DIPACI
      return
      end subroutine DIPACI
      end subroutine G2E_SPDF
      subroutine BLD_Pijkl (Iranges, Jranges, Kranges, Lranges, &
                            Iaos, Jaos, Kaos, Laos, IMJ, KML, IMKJML, &
                            D1234, DNSMAX)
!****************************************************************************************************************************
!     Date last modified: March 27, 2000                                                                       Version 2.0  *
!     Author: R.A. Poirier                                                                                                  *
!     Description:  Obtain all density matrix contributions, given the four atmshls.                                        *
!****************************************************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE QM_defaults
      USE QM_objects

      implicit none
!
! Input scalars:
      integer, intent(IN) :: Iranges,Jranges,Kranges,Lranges,Iaos,Jaos,Kaos,Laos
!
! Output scalar/array:
      double precision DNSMAX
      double precision D1234(1296)
!
! Local scalars
      integer IJKL,IMJ,KML,IMKJML,Irange,Jrange,Krange,Lrange
      integer I,IAS,J,JAS,K,KAS,L,LAS,LII,LIJ,LIK,LIL,LJJ,LJK,LJL,LKK,LKL,LLL,N,NFIJ,NFIK,NFIL,NFJK,NFJL,NFKL,NI,NJ
      double precision CC1,CC2,CC3,CC4,DTEMP
!
! Local parameters:
      double precision PT25
      parameter (PT25=0.25D0)
!
      IJKL=0
      DNSMAX=ZERO
!
! Donot destroy I,J,K,lranges
      Irange=Iranges
      Jrange=Jranges
      Krange=Kranges
      Lrange=Lranges
!
      do I=1,Irange
      IF(IMJ.EQ.0)Jrange=I
      IF(IMKJML.EQ.0)Krange=I
      IAS=Iaos+I
      LII=IAS*(IAS-1)/2
!
      do J=1,Jrange
      JAS=Jaos+J
      LJJ=JAS*(JAS-1)/2
      LIJ=LII+JAS
      IF(IAS.LT.JAS)LIJ=LJJ+IAS
      CC2=PT5
      IF(IAS.NE.JAS)CC2=CC2+CC2
!
      do K=1,Krange
      Lrange=Lranges
      IF(KML.EQ.0)Lrange=K
      IF(IMKJML.EQ.0.and.I.EQ.K)Lrange=J
      KAS=Kaos+K
      LKK=KAS*(KAS-1)/2
      LIK=LII+KAS
      IF(IAS.LT.KAS)LIK=LKK+IAS
      LJK=LJJ+KAS
      IF(JAS.LT.KAS)LJK=LKK+JAS
!
      do L=1,Lrange
      IJKL=IJKL+1
      LAS=Laos+L
      LLL=LAS*(LAS-1)/2
      LIL=LII+LAS
      IF(IAS.LT.LAS)LIL=LLL+IAS
      LJL=LJJ+LAS
      IF(JAS.LT.LAS)LJL=LLL+JAS
      LKL=LKK+LAS
      IF(KAS.LT.LAS)LKL=LLL+KAS
      CC4=CC2
      IF(KAS.NE.LAS)CC4=CC4+CC4
      IF(LIJ.NE.LKL)CC4=CC4+CC4
!
! Select appropriate formula based on SCF type.
      IF(Wavefunction(1:3).eq.'RHF')then
        DTEMP=PM0(LIJ)*PM0(LKL)-PT25*(PM0(LIK)*PM0(LJL)+PM0(LJK)*PM0(LIL))

      else if(Wavefunction(1:3).eq.'UHF'.or.Wavefunction(1:4).eq.'OSHF')then
        DTEMP=(PM0_alpha(LIJ)+PM0_beta(LIJ))*(PM0_alpha(LKL)+PM0_beta(LKL))- &
          PT5*(PM0_alpha(LIK)*PM0_alpha(LJL)+PM0_alpha(LJK)*PM0_alpha(LIL)+ &
               PM0_beta(LIK)*PM0_beta(LJL)+PM0_beta(LJK)*PM0_beta(LIL))

      else if(Wavefunction(1:3).eq.'GVB'.or.Wavefunction(1:4).eq.'ROHF')then ! PM0_full point to the correct density matrix
! General SCF - Offset each index by the number of Fock matrices.
        NFIJ=LIJ*NFOCK-NFOCK
        NFIK=LIK*NFOCK-NFOCK
        NFIL=LIL*NFOCK-NFOCK
        NFJK=LJK*NFOCK-NFOCK
        NFJL=LJL*NFOCK-NFOCK
        NFKL=LKL*NFOCK-NFOCK
! Core part.
        DTEMP=PM0_full(NFIJ+1)*PM0_full(NFKL+1)-PT25*(PM0_full(NFIK+1)*PM0_full(NFJL+1)+PM0_full(NFIL+1)*PM0_full(NFJK+1))
        IF(NFOCK.GT.1)then
          do NI=2,NFOCK
! Core - Valence-open part.
          DTEMP=DTEMP+PT5*VECCCN(NI,1)*(PM0_full(NFIJ+1)*PM0_full(NFKL+NI)+PM0_full(NFIJ+NI)*PM0_full(NFKL+1))+ &
                     PT25*VECCCN(1,NI)*(PM0_full(NFIK+1)*PM0_full(NFJL+NI)+PM0_full(NFIK+NI)*PM0_full(NFJL+1)+ &
                     PM0_full(NFIL+1)*PM0_full(NFJK+NI)+PM0_full(NFIL+NI)*PM0_full(NFJK+1))
! Valence-open - Valence-open part.
! Self-contribution.
          DTEMP=DTEMP+PT5*VECCCN(NI,NFOCK+1)*PM0_full(NFIJ+NI)*PM0_full(NFKL+NI)+ &
                     PT25*VECCCN(NI,NFOCK+2)*(PM0_full(NFIK+NI)*PM0_full(NFJL+NI)+PM0_full(NFIL+NI)*PM0_full(NFJK+NI))
          IF(NI.GT.2)then
          N=NI-1
!
          do NJ=2,N
          DTEMP=DTEMP+PT5*VECCCN(NI,NJ)*(PM0_full(NFIJ+NI)*PM0_full(NFKL+NJ)+PM0_full(NFIJ+NJ)*PM0_full(NFKL+NI))+ &
                     PT25*VECCCN(NJ,NI)*(PM0_full(NFIK+NI)*PM0_full(NFJL+NJ)+PM0_full(NFIK+NJ)*PM0_full(NFJL+NI)+ &
                                      PM0_full(NFIL+NI)*PM0_full(NFJK+NJ)+PM0_full(NFIL+NJ)*PM0_full(NFJK+NI))
          end do ! NJ
          end if ! NI.GT.2
          end do !NI
        end if ! NFOCK.GT.1
! ERROR
      else
        write(UNIout,'(2a)')'ERROR> BLD_Pijkl: incorrect Wavefunction: ',Wavefunction
        stop 'ERROR> BLD_Pijkl: incorrect Wavefunction'
      end if  ! Wavefunction
!
      DTEMP=DTEMP*CC4
      D1234(IJKL)=DTEMP
      IF(DABS(DTEMP).GT.DNSMAX)DNSMAX=DABS(DTEMP)
      end do ! L
      end do ! K
      end do ! J
      end do ! I
!
! End of routine BLD_Pijkl
      return
      END
