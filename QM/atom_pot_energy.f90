      subroutine ATOMIC_potential_energy
!************************************************************************
!     Date last modified: May 21, 2012                     Version 1.0  *
!     Author: R.A. Poirier & Jessica Besaw                              *
!     Description: Calculate the atomic potential integrals (VAint) and *
!                  the total potential integrals (VINT) in order to     *
!                  determine the atomic potential energies (EV)         *
!                  and the total potential energy (EVtotal)    		*
!************************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE type_basis_set
      USE QM_defaults
      USE QM_objects
      USE INT_objects
      USE matrix_print

      implicit none

! Local scalars:
      integer :: Ishell,Jshell,Ifrst,Jfrst,Ilast,Jlast
      integer :: Istart,Jstart,Iend,Jend
      integer :: Iatmshl,Jatmshl
      integer :: LAMAX,LBMAX
      integer :: Iatom
      integer :: Irange,Jrange
      integer :: Igauss,Jgauss
      integer :: Iaos,JaoS
      integer :: IGBGN,JGBGN,IGend,JGend
      integer :: LPMAX,I,IA,INTC,IX,IY,IZ,IZERO,J,JX,JY,JZ,LIM1DS,NZERO
      integer :: LENTQ
      double precision ABX,ABY,ABZ,ARABSQ,ARG,AS,ASXA,ASYA,ASZA,&
                       BS,COEF,CUT1,EP,EPI,EPIO2,PCX,PCY,PCZ,PEXP, &
                       PX,PY,PZ,RPCSQ,TWOASQ,TWOP,TWOPI,TWOPT2,XAP,XBP, &
                       YAP,YBP,ZAP,ZBP,ZCONST,ZT,ZTEMP
      double precision :: XA,XB,XC,YA,YB,YC,ZA,ZB,ZC,RABSQ,TRACLO,EVtotal

! Local arrays:
      double precision, dimension(:), allocatable :: VAint 
      double precision, dimension(:), allocatable :: EV
      integer INDIX(20),INDIY(20),INDIZ(20),INDJX(20),INDJY(20),INDJZ(20)
      double precision :: TP(7),WP(7)
      double precision A(45),CA(20),CB(20),CCX(192),CCY(192),CCZ(192), &
                       EEPB(100),EEPV(100), STWOCX(9), &
                       TWOCX(9),TWOCY(9),TWOCZ(9),XIP(80),YIP(80),ZIP(80)

      parameter (TWOPI=TWO*PI_VAL,CUT1=-75.0D0)

! Data related to Rys Polynomials
      DATA INDJX/1,2,1,1,3,1,1,2,2,1,4,1,1,2,3,3,2,1,1,2/, &
           INDJY/1,1,2,1,1,3,1,2,1,2,1,4,1,3,2,1,1,2,3,2/, &
           INDJZ/1,1,1,2,1,1,3,1,2,2,1,1,4,1,1,2,3,3,2,2/

! Begin:
       call PRG_manager ('enter', 'ATOMIC_potential_energy', 'ENERGY%ATOMIC_POTENTIAL')

! Obtaining the density matrix using HF theory
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)

! Object:
      if(.not.allocated(VINT))then
        allocate (VAint((Basis%Nbasis*(Basis%Nbasis+1))/2))
        allocate (VINT((Basis%Nbasis*(Basis%Nbasis+1))/2))
      else
        if(Basis%Nbasis.ne.size(VINT,1))then
          deallocate (VINT)
          allocate (VAint((Basis%Nbasis*(Basis%Nbasis+1))/2))
          allocate (VINT((Basis%Nbasis*(Basis%Nbasis+1))/2))
        end if
      end if
      
! Allocate space for Atomic Potential Energies      
       if(.not.allocated(EV))then        
         allocate (EV(Natoms)) 
       else
         deallocate (EV) 
         allocate (EV(Natoms)) 
       end if         
      
! Initialize Sum to Zero
      VINT=0.
      EVtotal=0.

      call RYSSET

! Fill INDIX. (Related to Rys polynomials)
      do I=1,20
        INDIX(I)=4*(INDJX(I)-1)
        INDIY(I)=4*(INDJY(I)-1)
        INDIZ(I)=4*(INDJZ(I)-1)
      end do ! I
      
! Loop over atoms.
      do Iatom=1,Natoms
        IA=CARTESIAN(Iatom)%Atomic_number

! Perform calculatation on atoms, and not dummy atoms.
      IF(IA.GT.0)then                       
        XC=CARTESIAN(Iatom)%X
        YC=CARTESIAN(Iatom)%Y
        ZC=CARTESIAN(Iatom)%Z

! Loop over shells.
! Loop over Ishell.
      do Ishell=1,Basis%Nshells
        LAMAX=Basis%shell(Ishell)%Xtype+1
        Istart=Basis%shell(Ishell)%Xstart
        Iend=Basis%shell(Ishell)%XEND
        Irange=Iend-Istart+1
        IGBGN=Basis%shell(Ishell)%EXPBGN
        IGEND=Basis%shell(Ishell)%EXPEND
        Ifrst=Basis%shell(Ishell)%frstSHL

! Loop over Jshell.
      do Jshell=1,Ishell
        LBMAX=Basis%shell(Jshell)%Xtype+1
        Jstart=Basis%shell(Jshell)%Xstart
        Jend=Basis%shell(Jshell)%XEND
        Jrange=Jend-Jstart+1
        JGBGN=Basis%shell(Jshell)%EXPBGN
        JGEND=Basis%shell(Jshell)%EXPEND
        Jfrst=Basis%shell(Jshell)%frstSHL

        Ilast= Basis%shell(Ishell)%lastSHL
        Jlast= Basis%shell(Jshell)%lastSHL

! Loop over Iatmshl
      do Iatmshl=Ifrst,Ilast
        XA=CARTESIAN(Basis%atmshl(Iatmshl)%ATMLST)%X
        YA=CARTESIAN(Basis%atmshl(Iatmshl)%ATMLST)%Y
        ZA=CARTESIAN(Basis%atmshl(Iatmshl)%ATMLST)%Z
        Iaos=Basis%atmshl(Iatmshl)%frstAO-1

! Loop over Jatmshl
      IF(Ishell.EQ.Jshell)Jlast=Iatmshl
      
      do Jatmshl=Jfrst,Jlast
        XB=CARTESIAN(Basis%atmshl(Jatmshl)%ATMLST)%X
        YB=CARTESIAN(Basis%atmshl(Jatmshl)%ATMLST)%Y
        ZB=CARTESIAN(Basis%atmshl(Jatmshl)%ATMLST)%Z
        Jaos=Basis%atmshl(Jatmshl)%frstAO-1

        LPMAX=LAMAX+LBMAX-1
        LIM1DS=(LPMAX+3)/2
        LENTQ=Irange*Jrange
        NZERO=(LAMAX+LBMAX-2)/2+1
        ABX=XB-XA
        ABY=YB-YA
        ABZ=ZB-ZA
        RABSQ=ABX*ABX+ABY*ABY+ABZ*ABZ
        
! Zero accumulation area
      do I=1,LENTQ
        EEPV(I)=ZERO
      end do ! I

! Loop over primitive Gaussians.

      do Igauss=IGBGN,IGEND
        AS=Basis%gaussian(Igauss)%exp
        TWOASQ=TWO*AS*AS
        ASXA=AS*XA
        ASYA=AS*YA
        ASZA=AS*ZA
        ARABSQ=AS*RABSQ
        
      call FILLCC (LAMAX, Basis%gaussian(Igauss)%CONTRC, CA)

      do Jgauss=JGBGN,JGEND
        BS=Basis%gaussian(Jgauss)%exp
        call FILLCC (LBMAX, Basis%gaussian(Jgauss)%CONTRC, CB)
        EP=AS+BS
        EPI=ONE/EP
        EPIO2=PT5*EPI
        TWOP=EP+EP
        ARG=-BS*ARABSQ*EPI
        PEXP=ZERO
        IF(ARG.GT.CUT1)PEXP=DEXP(ARG)
        ZTEMP=TWOPI*EPI*PEXP
        PX=(ASXA+BS*XB)*EPI
        PY=(ASYA+BS*YB)*EPI
        PZ=(ASZA+BS*ZB)*EPI
        XAP=PX-XA
        XBP=PX-XB
        YAP=PY-YA
        YBP=PY-YB
        ZAP=PZ-ZA
        ZBP=PZ-ZB
        
      call GETCC1 (CCX, XAP, XBP, LAMAX+2, LBMAX+2)
      call GETCC1 (CCY, YAP, YBP, LAMAX+2, LBMAX+2)
      call GETCC1 (CCZ, ZAP, ZBP, LAMAX+2, LBMAX+2)

! Zero accumulation area. (I want to just say EEBV = 0., or is loop more cost effective?)
      do I=1,LENTQ
        EEPB(I)=ZERO
      end do ! I
      
! Calculate information about atom

      ZT=ZTEMP*DBLE(IA)
      PCX=XC-PX
      PCY=YC-PY
      PCZ=ZC-PZ
      RPCSQ=PCX*PCX+PCY*PCY+PCZ*PCZ
      ARG=EP*RPCSQ
      
      call RPOLX (NZERO, ARG, TP, WP)
      call GETA1 (A, EPIO2, 0, LPMAX)

! Loop over zeroes of Rys polynomial.
      do IZERO=1,NZERO
        TWOPT2=TWOP*TP(IZERO)
        ZCONST=ZT*WP(IZERO)
        call GET2C (TWOCX, PCX, ONE, A, TWOPT2, 0, LPMAX)
        call GET2C (TWOCY, PCY, ONE, A, TWOPT2, 0, LPMAX)
        call GET2C (TWOCZ, PCZ, ZCONST, A, TWOPT2, 0, LPMAX)
        call GET3C (XIP, 0, TWOCX, CCX, LAMAX, LBMAX)
        call GET3C (YIP, 0, TWOCY, CCY, LAMAX, LBMAX)
        call GET3C (ZIP, 0, TWOCZ, CCZ, LAMAX, LBMAX)

! Loop over atomic orbitals for potential energy integrals,

      INTC=0
      do I=Istart,Iend
        IX=INDIX(I)
        IY=INDIY(I)
        IZ=INDIZ(I)

      do J=Jstart,Jend
        JX=INDJX(J)
        JY=INDJY(J)
        JZ=INDJZ(J)
        INTC=INTC+1
        EEPB(INTC)=EEPB(INTC)+XIP(IX+JX)*YIP(IY+JY)*ZIP(IZ+JZ)
      end do ! J
      end do ! I
      
! End of AO loop.

      end do ! IZERO
      
! End of loop over Rys zeroes.

! Begin loop over atomic orbitals for potential energy integrals
! and apply the contraction coefficients.
! The potential integrals are already in EEPB.

      INTC=0
      do I=Istart,Iend
      do J=Jstart,Jend
        INTC=INTC+1
        COEF=CA(I)*CB(J)
        EEPV(INTC)=EEPV(INTC)-EEPB(INTC)*COEF          
      end do ! J
      end do ! I
      
! End of loop over atomic orbitals      

      end do ! Jgauss
      end do ! Igauss
      
! End of loop over Gaussians.

! FILMAT takes the atomic integrals in EEPB and stores them in
! their proper places in VAINT.

      call FILMAT (EEPV, VAINT, MATlen, Iend, Jend, &
                         Iatmshl, Jatmshl, Irange, Jrange, Iaos, JaoS)
                         
      end do ! Jatmshl
      end do ! Iatmshl
      end do ! Jshell
      end do ! Ishell

! End of loop over shells.

! Calculate the atomic potential energies
      EV(Iatom) = TRACLO (VAint, PM0, Basis%Nbasis, MATlen)
      EVtotal = EVtotal + EV(Iatom)

! Print the atomic integrals
  !       write(UNIout,*)'Atomic Potential Integrals for atom ',CARTESIAN(Iatom)%ELEMENT,'label', Iatom
  !       call PRT_matrix (VAint, MATlen, Basis%Nbasis)         
         
! Calculate the total potential integral         
         
      Vint = Vint+VAint         
      
      end if ! IA       
      end do ! Iatom  
      
! End of loop over atoms.

! Write integrals, and atomic & total potential energy to file 

  !   write(UNIout,*)'Total Potential Integrals'
  !   call PRT_matrix (VINT, MATlen, Basis%Nbasis)
     
     do Iatom=1,Natoms
       write(UNIout,*)'Atomic Potential Energy for atom ',CARTESIAN(Iatom)%ELEMENT,'label',Iatom,' : ',EV(Iatom)
     end do ! Iatom
     
     write(UNIout,*)'Total Potential Energy: ',EVtotal
       
! End of routine ATOMIC_potential_energy
       call PRG_manager ('exit', 'ATOMIC_potential_energy', 'ENERGY%ATOMIC_POTENTIAL')
      return
      end subroutine ATOMIC_potential_energy
