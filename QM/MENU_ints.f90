      subroutine MENU_ints
!***********************************************************************
!     Date last modified: June 24, 1992                                *
!     Author: F. Colonna                                               *
!     Description: Set up integrals calculation.                       *
!***********************************************************************
! Modules:
      USE QM_defaults
      USE menu_gets
      USE program_parser

      implicit none
!
! Local scalar:
      logical done
!
! Begin:
      call PRG_manager ('enter', 'MENU_ints', 'UTILITY')
      done=.false.
!
! Menu:
      do while (.not.done)
      call new_token (' Integrals:')
!
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      ' Command INTegrals :', &
      '   Purpose: calculates integrals', &
      '   Syntax :', &
      '   INTegrals = TWOelectr| BIelectr| ONEelectr| MONoelectr', &
      '   end'
!
      else if(token(1:2).EQ.'BI')then
        call MENU_I2E

      else if(token(1:3).EQ.'TWO')then
        call MENU_I2E

      else if(token(1:3).EQ.'ONE')then
        call MENU_I1E

      else if(token(1:3).EQ.'MON')then
        call MENU_I1E

      else
           call MENU_end (done)
      end if
!
      end do !(.not.done)
!
! End of routine MENU_ints
      call PRG_manager ('exit', 'MENU_ints', 'UTILITY')
      return
      end
      subroutine MENU_I1E
!***********************************************************************
!     Date last modified: June 24, 1992                                *
!     Author: F. Colonna                                               *
!     Description: Set up one-electron integrals calculation.          *
!***********************************************************************
! Modules:
      USE QM_defaults
      USE program_parser
      USE menu_gets

      implicit none
!
! Local scalars:
      logical done,lrun
!
! Begin:
      call PRG_manager ('enter', 'MENU_I1E', 'UTILITY')
      done=.false.
      lrun=.false.
!
! Menu:
      do while (.not.done)
      call new_token (' One-electron:')
!
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      ' Command ONE-electron:', &
      '   Purpose: calculates one-electron integrals', &
      '   Syntax :', &
      '   ONEelectron', &
      '     TRAnsform:                : transform integrals ', &
      '   RUN command: Will cause execution', &
      '   end'
!
      else if(token(1:3).EQ.'RUN')then
        lrun=.true.

      else if(token(1:3).EQ.'TRA' )then
        call MENU_intran

      else
        call MENU_end (done)
      end if
!
      end do !(.not.done)
!
      if(lrun)call I1ECLC
!
! End of routine MENU_I1E
      call PRG_manager ('exit', 'MENU_I1E', 'UTILITY')
      return
      end
      subroutine MENU_I2E
!***********************************************************************
!     Date last modified: June 24, 1992                                *
!     Author: F. Colonna                                               *
!     Description: Set up two-electron integrals calculation.          *
!***********************************************************************
! Modules:
      USE program_parser
      USE menu_gets
      USE QM_defaults
      USE QM_objects
      USE INT_objects

      implicit none
!
! Local scalars:
      integer lenstr,maxbuf
      logical done,Lraw
      integer, parameter :: MAX_string=132
      character(len=MAX_string) I2EFIL,I2EFST,string
!
! Begin:
      call PRG_manager ('enter', 'MENU_I2E', 'UTILITY')
!
! Defaults:
      I2E_type='COMBINATIONS' ! set default
      done=.false.
!
! Menu:
      do while (.not.done)
      call new_token (' TWO-electron:')
!
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      ' Command TWO-electron:', &
      '   Purpose: calculates two-electron integrals', &
      '   Syntax :', &
      '   TWOelectron| BIelectronic', &
      '     ACCuracy: <real*8>     : accuracy of integral ', &
      '     ALLondisk <logical>     : Stores all integrals on disk if true', &
      '     BUFfersize: <integer>  : Buffer size in 64bits ', &
      '     EXpcut  :  <real*8>    : Exponent threshold in IDFCLC ', &
      '     IDFclc  : <logical>       : subroutine used ', &
      '     ISPclc  : <logical>       : subroutine used ', &
      '     MAXbuffersize: <integer> : Maximum buffer size in 64bits (same as buffersize)', &
      '     METhod  : <string>     : DIRect, (Default is CONventional) ', &
      '     PQCUT1: <real*8>       : cut off term 1 ', &
      '     PQCUT2: <real*8>       : cut off term 2 ', &
      '     RAW   : <logical>       : Use RAW integrals (no combinations) ', &
      '     STAtus  : <logical>       : true if files exist ', &
      '     TRAnsform:             : transform integrals ', &
      '   end'

! ALLondisk
      else if(token(1:3).EQ.'ALL')then
        call GET_value (Lall_ONDISK)

! BUFfersize or MAXbuffer
      else if(token(1:3).EQ.'BUF'.or.token(1:3).EQ.'MAX')then
        call GET_value (I2EBUF)
        write(UNIout,'(9x,a,i10)')'MENU_I2E> Buffer size set = ',i2ebuf
        write(UNIout,'(a)')'MENU_I2E> Arrays (if allocated) will be deallocated'
! Just in case - changing i2ebuf will otherwise cause errors since GET_I2E_buffer_SIZES called everytime!
! IMPROVE
!       call I2E_DEALLOCATES
!
! ERROR1
      else if(token(1:6).EQ.'ERROR1')then
        call GET_value (ERROR1)
        write(UNIout,'(a,E22.15)')' MENU_I2E> ERROR1 cutoff = ',ERROR1
!
! ERROR2
      else if(token(1:6).EQ.'ERROR2')then
        call GET_value (ERROR2)
        write(UNIout,'(a,E22.15)')' MENU_I2E> ERROR2 cutoff = ',ERROR2
!
! EXPCUT
      else if(token(1:2).EQ.'EX')then
        call GET_value (I2E_EXPCUT)
        write(UNIout,'(a,E22.15)')' MENU_I2E> Exponent threshold = ',I2E_EXPCUT

!
! IDFclc
      else if(token(1:3).EQ.'IDF')then
        call GET_value (LI2EDF)
        if(LI2EDF)write(UNIout,'(9x,a)')'MENU_I2E> Calculation of D and F integrals using IDFCLC'
!
! ISPclc
      else if(token(1:3).EQ.'ISP')then
        call GET_value (LI2ESP)
        if(LI2ESP)write(UNIout,'(9x,a)')'MENU_I2E> Calculation of S and P integrals using ISPCLC'
!
! I2EACC
      else if(token(1:3).EQ.'ACC')then
        call GET_value (I2E_ACC)
        write(UNIout,'(9x,a,E22.15)')'MENU_I2E> Accuracy    =',i2e_acc
!
! PQCUT2
      else if(token(1:6).EQ.'PQCUT2')then
        call GET_value (I2E_PQCUT2)
        write(UNIout,'(a,E22.15)')' MENU_I2E> RYSLOOP ZCONST cutoff = ',I2E_PQCUT2
!
! VAR1
      else if(token(1:4).EQ.'VAR1')then
        call GET_value (VAR1)
        write(UNIout,'(a,E22.15)')' MENU_I2E> VAR1 cutoff = ',VAR1

! VAR2
      else if(token(1:4).EQ.'VAR2')then
        call GET_value (VAR2)
        write(UNIout,'(a,E22.15)')' MENU_I2E> VAR2 cutoff = ',VAR2

! METhod
      else if(token(1:3).EQ.'MET')then
        call GET_value (string, lenstr)
        if(string(1:3).eq.'DIR')then
          LDirect_SCF=.true.
          LI2EDF=.true.       ! make sure all integrals are computed in IDFCLC
          LI2ESP=.false.
        else
          write(UNIout,'(2a)')'MENU_I2E> Unknown method  = ',string(1:12)
          stop 'MENU_I2E> Unknown method  = '
        end if
        write(UNIout,'(9x,2a)')      'MENU_I2E> Method      = ',string(1:12)
!
! STAtus
      else if(token(1:3).EQ.'STA')then
        call GET_value (I2EFST, lenstr)
        write(UNIout,'(9x,a)')'MENU_I2E> Two-electron integral file status',I2EFST(1:lenstr)

! TRAnsformation
      else if(token(1:3).EQ.'TRA')then
        call MENU_intran
!
! RAW
      else if(token(1:3).EQ.'RAW')then
        call GET_value (Lraw)
        if(Lraw)I2E_type='RAW '
!
      else
        call MENU_end (done)
      end if
!
      end do !(.not.done)

! End of routine MENU_I2E
      call PRG_manager ('exit', 'MENU_I2E', 'UTILITY')
      return
      end
