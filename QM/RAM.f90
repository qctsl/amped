      subroutine RAMcalc
!*********************************************************************************************
!     Date last modified: May 29, 2024                                                       *
!     Author: JWH                                                                            *
!     Desciption: Calculate the relative angular momentum analytically.                      *
!*********************************************************************************************
! Modules:
      USE program_constants
      USE program_timing
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE INT_objects

      implicit none
!
! Local scalars:
      double precision :: RAMaa,RAMHFaa
      double precision :: RAMbb,RAMHFbb
      double precision :: RAMab,RAMHFab
      double precision :: RAMba,RAMHFba
!
! Begin:
      call PRG_manager ('enter', 'RAMcalc', 'RAM%ANA')
!
      call GET_object ('QM', '2RDM', 'AO')
!
! Calculate value analytically
!
      call GET_object ('INT', 'RAMINT', 'AO')
!
      RAMHFaa = dot_product(TRDM_HF_AOaa,ramAO)
      RAMaa = dot_product(TRDM_AOaa,ramAO)
      RAMHFbb = dot_product(TRDM_HF_AObb,ramAO)
      RAMbb = dot_product(TRDM_AObb,ramAO)
      RAMHFab = dot_product(TRDM_HF_AOab,ramAO)
      RAMab = dot_product(TRDM_AOab,ramAO)
      RAMHFba = dot_product(TRDM_HF_AOba,ramAO)
      RAMba = dot_product(TRDM_AOba,ramAO)
!
      write(UNIout,'(/a)')'           Relative angular momentum analytically (same-spin) '
      write(UNIout,'(a)') '----------------------------------------------------------------------'
      write(UNIout,'(a)') '            Hartree-Fock                         Correlated      '
      write(UNIout,'(a)') '----------------------------------------------------------------------'
      write(UNIout,'(a)') '    alpha-alpha       beta-beta         alpha-alpha       beta-beta'
      write(UNIout,'(a)') '----------------------------------------------------------------------'
      write(UNIout,'(4(f14.8,4x))')RAMHFaa,RAMHFbb,RAMaa,RAMbb
!
      write(UNIout,'(/a)')'         Relative angular momentum analytically (opposite-spin) '
      write(UNIout,'(a)') '----------------------------------------------------------------------'
      write(UNIout,'(a)') '            Hartree-Fock                         Correlated      '
      write(UNIout,'(a)') '----------------------------------------------------------------------'
      write(UNIout,'(a)') '    alpha-beta        beta-alpha        alpha-beta        beta-alpha'
      write(UNIout,'(a)') '----------------------------------------------------------------------'
      write(UNIout,'(4(f14.8,4x))')RAMHFab,RAMHFba,RAMab,RAMba
!
! End of routine RAMcalc
      call PRG_manager ('exit', 'RAMcalc', 'RAM%ANA')
      end subroutine RAMcalc
