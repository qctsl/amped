      subroutine MENU_SCF (commandIN)
!***********************************************************************
!     Date last modified: June 24, 1992                                *
!     Author: F. Colonna and R. Poirier                                *
!     Description: Set up closed shell SCF calculation.                *
!***********************************************************************
!Modules:
      USE QM_defaults
      USE program_parser
      USE MENU_gets

      implicit none
!
! Input scalar:
      character*(*) commandIN
!
! Local scalars:
      logical :: done,lrun
      integer :: cnvlen,guslen,lenstr,NList
      integer, parameter :: MAX_string=132
      character(len=MAX_string) string
      character(len=MAX_string) command
!
! Begin:
      call PRG_manager ('enter', 'MENU_SCF', 'UTILITY')
!
! Defaults:
      Wavefunction=commandIN  ! command corresponds to the modality wavefunction (RHF, UHF, ...)
      Level_of_Theory=commandIN
      command=commandIN//':'
      done=.false.
      lrun=.false.
      cnvlen=9
!
! Menu:
      do while (.not.done)
      call new_token (command)
!
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      ' Command SCF:', &
      '   Purpose: Perform a SCF ', &
      '   Syntax :', &
      '     ACCuracy  = <real*8> ', &
      '     CONvergence = <string>, example,', &
      '                    DEWAR,', &
      '                    3,4 POINT,', &
      '                    SWITCH,', &
      '                    NONE,', &
      '     DAMping factor= <real*8> ', &
      '     DANdc   = <boolian> : = true for a Divide and Conquer calculation', &
      '     DIrect  = <boolian> : = true for a direct SCF', &
      '     GMatrix = <logical>  = true to dump the G matrix', &
      '     GUEss   = <string> ', &
      '     INTegral  <command> ', &
      '     ITEration = <integer> ', &
      '     METhod  : <string>     : DIRect (default is CONventional) ', &
      '     NCore = <integer>: Number of closed shells ', &
      '     NOpen = <integer>: Number of open shells(Degeneracy) ', &
      '     ROtation: <string>     : NORESET, ...', &
      '     SHIftlevel = <real*8> ', &
      '     TYpe  = <string>     : RHF, UHF, ROHF, GVB or AHF', &
      '     MOM   = <string>     : on or off (off is default)', &
      '     startMOM =  <integer> : SCF iteration to start MOM', &
      '     RUN command: Will cause execution', &
      '   end'
!
! ACCuracy
      else if(token(1:3).EQ.'ACC')then
        call GET_value (SCFACC)
        write(UNIout,'(9x,a,E22.15)')'MENU_SCF> SCF Convergence Accuracy =',SCFACC
!
! CONvergence (extrapolation method)
      else if(token(1:3).EQ.'CON' )then
        call GET_value (extrapolation_met, cnvlen)
! Check if allowed convergence method
        if(extrapolation_met(1:2).eq.'NO')then
          extrapolation_met(1:5) =  'NONE '
        else if(extrapolation_met(1:2).eq.'3,')then
          extrapolation_met(1:10) =  '3,4 POINT '
        else if(extrapolation_met(1:2).eq.'DE')then
          extrapolation_met(1:6) =  'DEWAR '
        else if(extrapolation_met(1:2).eq.'SW')then
          extrapolation_met(1:7)  = 'SWITCH '
          write(UNIout,'(9x,3a)')'error_MENU_SCF> Convergence method "',extrapolation_met(1:cnvlen),'" not available'
          stop 'Convergence method not available'
        else
          write(UNIout,'(9x,3a)')'error_MENU_SCF> Unknown Convergence method "',extrapolation_met(1:cnvlen),'"'
          stop 'Unknown Convergence method'
        end if
        write(UNIout,'(9x,3a)')'MENU_SCF> SCF Convergence method set to   "',extrapolation_met(1:cnvlen),'"'
!
! DAMping
      else if(token(1:3).EQ.'DAM' )then
        call GET_value (SCFDMP)
        write(UNIout,'(9x,a,E22.15)')'MENU_SCF> SCF Damping factor set to ',SCFDMP
!
! DIrect
      else if(token(1:2).EQ.'DI' )then
        call GET_value (LDirect_SCF)
        LI2EDF=.true.       ! make sure all integrals are computed in IDFCLC
        LI2ESP=.false.
        write(UNIout,'(a)')'MENU_SCF> Using the direct SCF '
!
! GMatrix
      else if(token(1:2).EQ.'GM' )then
        call GET_value (LGmatrix_dump)
!
! DandC
      else if(token(1:3).EQ.'DAN' )then
        call GET_value (LDandC_menu)
        LDandC_menu=.true.
        LDandC=.true.
        write(UNIout,'(a)')'MENU_SCF> Performing a Divide and Conquer calculation'
!
! PFockOP
      else if(token(1:3).EQ.'PFO' )then
        call GET_value (LPFockOP_menu)
        LPFockOP_menu=.true.
        LPFockOP=.true.
        write(UNIout,'(a)')'MENU_SCF> Performing a Projected Fock Operator Calculation (testing stage only)'
!
! GUEss
      else if(token(1:3).EQ.'GUE')then
        call MENU_guess
!
! INTegrals
      else if(token(1:3).EQ.'INT ')then
        call MENU_ints
!
! Iteration
      else if(token(1:3).EQ.'ITE')then
        call GET_value (SCFITN)
        write(UNIout,'(9x,a,i10)')'MENU_SCF> Maximum SCF iterations set to ',SCFITN
!
! METhod
      else if(token(1:3).EQ.'MET')then
        call GET_value (string, lenstr)
        if(string(1:3).eq.'DIR')then
          LDirect_SCF=.true.
          LI2EDF=.true.       ! make sure all integrals are computed in IDFCLC
          LI2ESP=.false.
        else
          write(UNIout,'(2a)')'MENU_SCF> Unknown method  = ',string(1:12)
          stop 'MENU_SCF> Unknown method  = '
        end if
        write(UNIout,'(9x,2a)')'MENU_SCF> Method set to = ',string(1:12)
!
! NCore
      else if(token(1:2).EQ.'NC')then
        call GET_value (NCore)
        write(UNIout,'(9x,a,i10)')'MENU_SCF> Number of closed shells set to ',NCore
!
! NOpen
      else if(token(1:2).EQ.'NO')then
        call GET_value (NOpen)
        write(UNIout,'(9x,a,i10)')'MENU_SCF> Number of open shells set to ',NOpen
!
! Porb_list
!     else if(token(1:3).EQ.'POR' )then
!       call GET_Number_of_Porbitals (NPorbitals)
!       allocate (Porb_list(NPorbitals))
!       call GET_IList (Porb_list, NList)
!       write(UNIout,'(9x,a,12i2)')'MENU_SCF> Pure P orbitals list ',Porb_list(1:NList)
!
! Purify
      else if(token(1:3).EQ.'PUR' )then
        call GET_value (LPurifyP)
        call GET_Number_of_Porbitals (NPorbitals)
        write(UNIout,'(9x,a,l1)')'MENU_SCF> Pure P orbitals set to ',LPurifyP
!
! ROTation
      else if(token(1:3).EQ.'ROT')then
        call GET_value (string, lenstr)
        if(string(1:3).eq.'NOR')then
          ROTMET(1:7)='NORESET'
        else
          write(UNIout,'(2a)')'MENU_SCF> Unknown ROT method  = ',string(1:12)
          stop 'MENU_SCF> Unknown ROT method  = '
        end if
!
! SHIftlevel
      else if(token(1:3).EQ.'SHI' )then
        call GET_value (ShiftLevel)
        LShift=.true.
        write(UNIout,'(9x,a,E22.15)')'MENU_SCF> SCF Fock matric shift level set to ',ShiftLevel
!
! TYpe
      else if(token(1:2).EQ.'TY')then
        call GET_value (string, lenstr)
        if(string(1:3).eq.'RHF')then
          Wavefunction='RHF  '
          Level_of_Theory='RHF'
        else if(string(1:3).eq.'ROH')then
          Wavefunction='ROHF '
          Level_of_Theory='ROHF'
        else if(string(1:3).eq.'UHF')then
          Wavefunction='UHF  '
          Level_of_Theory='UHF'
        else if(string(1:3).eq.'GVB')then
          Wavefunction='GVB  '
          Level_of_Theory='GVB'
        else if(string(1:3).eq.'AHF')then
          Wavefunction='AHF  '
          Level_of_Theory='AHF'
        else if(string(1:3).eq.'OSHF')then
          Wavefunction='OSHF  '
          Level_of_Theory='OSHF'
        else
          write(UNIout,'(2a)')'ERROR> MENU_SCF: SCF type not available = ',string(1:lenstr)
          stop 'ERROR> MENU_SCF: SCF type not available = '
        end if
!
! MOM
      else if(token(1:3).eq.'MOM')then
        call GET_value (string, lenstr)
        if(string(1:2).eq.'ON')then
          LMOM= .true.
        else
          LMOM= .false.
        end if
!
      else if(token(1:3).eq.'STA')then
        call GET_value (MOMstart)
        write(UNIout,'(9x,a,I4)')'MENU_SCF> First MOM iteration set to ',MOMstart
!
      else if(token(1:3).eq.'MOR')then
        call GET_value (string, lenstr)
        if(string(1:2).eq.'TR')then
          LMOread = .true.
        else
          LMOread = .false.
        end if
! 

      else if(token(1:3).EQ.'RUN')then
        lrun=.true.

      else

        call MENU_end (done)
      end if
 
      end do !(.not.done)

!     if(index(Wavefunction,'ROHF').ne.0)then
!       if(NCore.eq.0.and.NOpen.eq.0)then
!         write(UNIout,'(a)')'ERROR: MENU_SCF> For ROHF must specify NCore and NOpen'
!         write(UNIout,'(a)')'NCore is the number of Closed Shells'
!         write(UNIout,'(a)')'NOpen is the number of Open Shells (Degeneracy)'
!         call PRG_stop ('For ROHF must specify NCore and NOpen')
!       end if
!     end if
!
      if(lrun)then
        call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction)
      end if
!
! End of routine MENU_SCF
      call PRG_manager ('exit', 'MENU_SCF', 'UTILITY')
      return
      end
      subroutine MENU_UHF
!***********************************************************************
!     Date last modified: June 24, 1992                                *
!     Author: F. Colonna and R. Poirier                                *
!     Description: Set up closed shell SCF calculation.                *
!***********************************************************************
!Modules:
      USE QM_defaults
      USE program_parser
      USE MENU_gets

      implicit none
!
! Local scalars:
      logical done,lrun
      integer cnvlen,guslen,lenstr
      integer, parameter :: MAX_string=132
      character(len=MAX_string) string
!
! Begin:
      call PRG_manager ('enter', 'MENU_UHF', 'UTILITY')
!
! Defaults:
      Wavefunction='UHF '
      Level_of_Theory='UHF'
      done=.false.
      lrun=.false.
      cnvlen=9
!
! Menu:
      do while (.not.done)
      call new_token (' UHF:')
!
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      ' Command UHF:', &
      '   Purpose: Perform an open shell UHF ', &
      '   Syntax :', &
      '     ACCuracy  = <real*8> ', &
      '     CONvergence = <string>, example,', &
      '                    DEWAR,', &
      '                    3,4 POINT,', &
      '                    SWITCH,', &
      '                    NONE,', &
      '     DAMping = <real*8> ', &
      '     DANdc   = <boolian> : = true for a Divide and Conquer calculation', &
      '     GUEss   = <string> ', &
      '     INTegral  <command> ', &
      '     ITEration = <integer> ', &
      '     METhod  : <string>     : DIRect (default is CONventional) ', &
      '   RUN command: Will cause execution', &
      '   end'
!
! ACCuracy
      else if(token(1:3).EQ.'ACC')then
        call GET_value (SCFACC)
        write(UNIout,'(9x,a,E22.15)')'MENU_UHF> SCF Convergence Accuracy =',SCFACC

! CONvergence (extrapolation method)
      else if(token(1:3).EQ.'CON' )then
        call GET_value (extrapolation_met, cnvlen)
! Check if allowed convergence method
        if(extrapolation_met(1:2).eq.'NO')then
          extrapolation_met(1:5) =  'NONE '
        else if(extrapolation_met(1:2).eq.'3,')then
          extrapolation_met(1:10) =  '3,4 POINT '
        else if(extrapolation_met(1:2).eq.'DE')then
          extrapolation_met(1:6) =  'DEWAR '
        else if(extrapolation_met(1:2).eq.'SW')then
          extrapolation_met(1:7) =  'SWITCH '
          write(UNIout,'(9x,3a)')'error_MENU_SCF> Convergence method "',extrapolation_met(1:cnvlen),'" not available'
          stop 'Convergence method not available'
        else
          write(UNIout,'(9x,3a)')'error_MENU_SCF> Unknown Convergence method "',extrapolation_met(1:cnvlen),'"'
          stop 'Unknown Convergence method'
        end if
        write(UNIout,'(9x,3a)')'MENU_UHF> SCF Convergence method set to   "',extrapolation_met(1:cnvlen),'"'

! DAMping
      else if(token(1:3).EQ.'DAM' )then
        call GET_value (SCFDMP)
        write(UNIout,'(9x,a,E22.15)')'MENU_UHF> SCF Damping factor set to ',SCFDMP
!
! DandC
      else if(token(1:3).EQ.'DAN' )then
        call GET_value (LDandC_menu)
        LDandC_menu=.true.
        LDandC=.true.
        write(UNIout,'(a)')'MENU_SCF> Performing a Divide and Conquer calculation'
! GUEss
      else if(token(1:3).EQ.'GUE')then
        call MENU_guess
!       call GET_value (GUESS_type, guslen)
!       write(UNIout,'(9x,3a)')'MENU_UHF> SCF Initial Guess method set to "',GUESS_type(1:guslen),'"'

! INTegrals
!      else if(token(1:3).EQ.'INT ')then
!        call MENU_ints

! Iteration
      else if(token(1:3).EQ.'ITE')then
        call GET_value (SCFITN)
        write(UNIout,'(9x,a,i10)')'MENU_UHF> Maximum SCF iterations set to ',SCFITN

! METhod
      else if(token(1:3).EQ.'MET')then
        call GET_value (string, lenstr)
        if(string(1:3).eq.'DIR')then
          LDirect_SCF=.true.
          LI2EDF=.true.       ! make sure all integrals are computed in IDFCLC
          LI2ESP=.false.
        else
          write(UNIout,'(2a)')'MENU_UHF> Unknown method  = ',string(1:12)
          stop 'MENU_UHF> Unknown method  = '
        end if
        write(UNIout,'(9x,2a)')'MENU_UHF> Method set to = ',string(1:12)

      else if(token(1:3).EQ.'RUN')then
        lrun=.true.

      else
        call MENU_end (done)
      end if
!
      end do !(.not.done)
!
      if(lrun)then
        call GET_object ('QM','DENSITY_1MATRIX','UHF')
      end if
!
! End of routine MENU_UHF
      call PRG_manager ('exit', 'MENU_UHF', 'UTILITY')
      return
      end
      subroutine MENU_RHF
!***********************************************************************
!     Date last modified: June 24, 1992                                *
!     Author: F. Colonna and R. Poirier                                *
!     Description: Set up closed shell SCF calculation.                *
!***********************************************************************
!Modules:
      USE QM_defaults
      USE program_parser
      USE MENU_gets

      implicit none
!
! Local scalars:
      logical done,lrun
      integer cnvlen,guslen,lenstr
      integer, parameter :: MAX_string=132
      character(len=MAX_string) string
!
! Begin:
      call PRG_manager ('enter', 'MENU_RHF', 'UTILITY')
!
! Defaults:
      Wavefunction='RHF '
      Level_of_Theory='RHF'
      done=.false.
      lrun=.false.
      cnvlen=9
!
! Menu:
      do while (.not.done)
      call new_token (' RHF:')
!
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      ' Command RHF:', &
      '   Purpose: Perform a closed shell SCF ', &
      '   Syntax :', &
      '     ACCuracy  = <real*8> ', &
      '     CONvergence = <string>, example,', &
      '                    DEWAR,', &
      '                    3,4 POINT,', &
      '                    SWITCH,', &
      '                    NONE,', &
      '     DAMping = <real*8> ', &
      '     DANdc   = <boolian> : = true for a Divide and Conquer calculation', &
      '     GUEss   = <string> ', &
      '     INTegral  <command> ', &
      '     ITEration = <integer> ', &
      '     METhod  : <string>     : DIRect (default is CONventional) ', &
      '   RUN command: Will cause execution', &
      '   end'
!
! ACCuracy
      else if(token(1:3).EQ.'ACC')then
        call GET_value (SCFACC)
        write(UNIout,'(9x,a,E22.15)')'MENU_RHF> SCF Convergence Accuracy =',SCFACC

! CONvergence (extrapolation method)
      else if(token(1:3).EQ.'CON' )then
        call GET_value (extrapolation_met, cnvlen)
! Check if allowed convergence method
        if(extrapolation_met(1:2).eq.'NO')then
          extrapolation_met(1:5) =  'NONE '
        else if(extrapolation_met(1:2).eq.'3,')then
          extrapolation_met(1:10) =  '3,4 POINT '
        else if(extrapolation_met(1:2).eq.'DE')then
          extrapolation_met(1:6) =  'DEWAR '
        else if(extrapolation_met(1:2).eq.'SW')then
          extrapolation_met(1:7) =  'SWITCH '
          write(UNIout,'(9x,3a)')'error_MENU_SCF> Convergence method "',extrapolation_met(1:cnvlen),'" not available'
          stop 'Convergence method not available'
        else
          write(UNIout,'(9x,3a)')'error_MENU_SCF> Unknown Convergence method "',extrapolation_met(1:cnvlen),'"'
          stop 'Unknown Convergence method'
        end if
        write(UNIout,'(9x,3a)')'MENU_RHF> SCF Convergence method set to   "',extrapolation_met(1:cnvlen),'"'

! DAMping
      else if(token(1:3).EQ.'DAM' )then
        call GET_value (SCFDMP)
        write(UNIout,'(9x,a,E22.15)')'MENU_RHF> SCF Damping factor set to ',SCFDMP
!
! DandC
      else if(token(1:3).EQ.'DAN' )then
        call GET_value (LDandC_menu)
        LDandC_menu=.true.
        LDandC=.true.
        write(UNIout,'(a)')'MENU_SCF> Performing a Divide and Conquer calculation'
! GUEss
      else if(token(1:3).EQ.'GUE')then
        call MENU_guess
!       call GET_value (GUESS_type, guslen)
!       write(UNIout,'(9x,3a)')'MENU_RHF> SCF Initial Guess method set to "',GUESS_type(1:guslen),'"'

! INTegrals
!      else if(token(1:3).EQ.'INT ')then
!        call MENU_ints

! Iteration
      else if(token(1:3).EQ.'ITE')then
        call GET_value (SCFITN)
        write(UNIout,'(9x,a,i10)')'MENU_RHF> Maximum SCF iterations set to ',SCFITN

! METhod
      else if(token(1:3).EQ.'MET')then
        call GET_value (string, lenstr)
        if(string(1:3).eq.'DIR')then
          LDirect_SCF=.true.
          LI2EDF=.true.       ! make sure all integrals are computed in IDFCLC
          LI2ESP=.false.
        else
          write(UNIout,'(2a)')'MENU_RHF> Unknown method  = ',string(1:12)
          stop 'MENU_RHF> Unknown method  = '
        end if
        write(UNIout,'(9x,2a)')'MENU_RHF> Method set to = ',string(1:12)

      else if(token(1:3).EQ.'RUN')then
        lrun=.true.

      else
        call MENU_end (done)
      end if
!
      end do !(.not.done)
!
      if(lrun)then
        call GET_object ('QM','DENSITY_1MATRIX','RHF')
      end if
!
! End of routine MENU_RHF
      call PRG_manager ('exit', 'MENU_RHF', 'UTILITY')
      return
      end
      subroutine GET_Number_of_Porbitals (NPorb)
!***********************************************************************
!     Date last modified: March 20, 2019                               *
!     Author: R.A. Poirier                                             *
!     Description: Determinine the number of occupied P-orbitlas       *
!***********************************************************************
! Modules:
      USE type_molecule
      USE program_files
!
      integer :: NPorb
!
      NPorb=0
      if(CARTESIAN(1)%Atomic_number.ge.1.and.CARTESIAN(1)%Atomic_number.le.4)then
        write(UNIout,'(a)') "No P-orbitals for Z between 1 to 4"
      else if(CARTESIAN(1)%Atomic_number.gt.4.and.CARTESIAN(1)%Atomic_number.le.12)then
        NPorb=3
      else if(CARTESIAN(1)%Atomic_number.gt.12.and.CARTESIAN(1)%Atomic_number.le.20)then
        NPorb=6
      else if(CARTESIAN(1)%Atomic_number.gt.20.and.CARTESIAN(1)%Atomic_number.le.38)then
        NPorb=9
      else if(CARTESIAN(1)%Atomic_number.gt.38.and.CARTESIAN(1)%Atomic_number.le.56)then
        NPorb=12
      else
        stop 'Code not available for Z greater than 56'
      end if
!
! End of routine GET_Number_of_Porbitals
      return
      end subroutine GET_Number_of_Porbitals

