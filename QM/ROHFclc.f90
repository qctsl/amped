      subroutine RHOCLC
!***********************************************************************
!     Date last modified: June 26, 1992                                *
!     Author: Modified from routine GSCF (author: K. Morokuma et al.)  *
!             by Mike Peterson and R.A. Poirier.                       *
!     Description:  MUN Version  ROHF only                             *
!***********************************************************************
! Modules:
      USE program_constants
      USE QM_defaults
      USE QM_objects
      USE INT_objects
      USE ixtrapolate
      USE GSCF_work
      USE matrix_print

      implicit none
!
! Local scalars:
      integer :: CNTSCF,DWCSCF,Iend,IEXT,ISCF,CIcycle
      double precision :: FACTUP,SCFCON,SCRIT,SLIMUP
      double precision :: ACC,CCONV,CRIT,DEWTOL,E_ELEC,EPREV
!     double precision :: Etotal,ETandV,Ecoul,Eexch
!     logical :: Lconverged
      logical :: MATCH,TROUBL
      character(len=4) :: IRESET
!
! Work arrays:
      double precision, dimension(:,:), allocatable :: CMOBHF
      double precision, dimension(:,:), allocatable :: DCM1HF
      double precision, dimension(:,:), allocatable :: DCM2HF
      double precision, dimension(:,:), allocatable :: DCM3HF
      double precision, dimension(:), allocatable :: SCRPM
!
! Local function:
      double precision :: TRACLO
!
! Local parameters:
      double precision :: TEN,TENM5,TENM10
      parameter (TEN=10.0D0,TENM5=1.0D-5,TENM10=1.0D-10)
!
! Begin:
      call PRG_manager ('enter', 'RHOCLC', 'UTILITY')

      call GET_object ('QM', 'CMO_GUESS', 'ROHF')
!     call GET_object ('QM', 'CMO', 'OSHF')
! Make sure not combinations:
      call GET_object ('INT', '1EINT', 'AO')
      call GET_object ('INT', '2EINT', 'RAW')
!
      LABEL='        '
      Wavefunction='ROHF'
      NFock=Nopen+1
!     if(NFock.gt.NBasis)NFock=1 ! For H atom with Nbasis =1
!
      allocate(IJNDX(NBasis+1))
      call BLD_IJNDX (IJNDX, NBasis)
!
      if(allocated(PM0_ROHF))then
        deallocate (PM0_ROHF)
      end if
      if(allocated(LAGRGM))then
        deallocate (LAGRGM)
      end if
      if(allocated(VECCCN))then
        deallocate (VECCCN)
      end if
      allocate (PM0_ROHF(1:MATlen), LAGRGM(NBasis,NBasis), VECCCN(NFock,NFock+2))
      if(associated(CMO_ROHF%eigval))then
        deallocate (CMO_ROHF%coeff, CMO_ROHF%eigval, CMO_ROHF%occupancy)
      end if
      allocate (CMO_ROHF%coeff(NBasis,NBasis), CMO_ROHF%eigval(NBasis), CMO_ROHF%occupancy(1:NFock))

      call PHASE (CMOG%coeff, NBasis)
!     call PHASE (CMOAG%coeff, NBasis)
!     CMO_ROHF%coeff=CMOAG%coeff
!     CMO_ROHF%NFcoreMO=CMOB%NoccMO
!     CMO_ROHF%NoccMO=CMOA%NoccMO
      CMO_ROHF%coeff=CMOG%coeff
      CMO_ROHF%NFcoreMO=CMOG%NFcoreMO
      CMO_ROHF%NoccMO=CMOG%NoccMO

      if(allocated(FockMN))deallocate (FockMN)
      allocate (CMOB%coeff(NBasis,NBasis), CMOC(NBasis,NBasis))
      allocate (EJKN(NFock,NFock), PMN(MATlen,NFock))
      allocate (SCRMAT(NBasis,NBasis))
      allocate (SQSM(NBasis,NBasis)) 
      allocate (MOTB(CMO_ROHF%noccmo,CMO_ROHF%noccmo)) 
      allocate (SYMO(NBasis,CMO_ROHF%noccmo)) 
      allocate (SCMO(CMO_ROHF%noccmo,CMO_ROHF%noccmo)) 
      allocate (JcoulN(MATlen,NFock)) 
      allocate (KexchN(MATlen,NFock))
      allocate (FockMN(MATlen,NFock))
!
      allocate (CMOBHF(NBasis,NBasis), DCM1HF(NBasis,NBasis), DCM2HF(NBasis,NBasis), DCM3HF(NBasis,NBasis), &
                SCRPM(MATlen))
!
      call SET_RHOCLC

      if(MUN_PRTLEV.gt.0)then
        write(UNIout,'(a)')'RHF OPEN SHELL SCF'
        write(UNIout,'(a,1PD12.4,a,0PF8.5)')'CONVERGENCE ON DENSITY MATRIX REQUIRED TO EXIT IS',SCFACC, &
          ',  INITIAL SCALE FACTOR (SCALST) IS',SCALST
        write(UNIout,'(a,f8.5,a,f8.5)')'SCALE FACTOR CUT RATIO (FACTDN) IS',FACTDN, &
          ',  SCALE FACTOR INCREASE RATIO (FACTUP) IS',FACTUP
        write(UNIout,'(a,f8.5,a,f8.5)')'SCALE FACTOR LOWER LIMIT (SLIMDN) IS',SLIMDN, &
          ',  SCALE FACTOR UPPER LIMIT (SLIMUP) IS',SLIMUP
        write(UNIout,'(a,1PD12.4,a)')'SWITCH TO INCREASE MODE AT D CONVERGENCE',SCRIT,' (SCRIT, RHF=INCR MODE ONLY).'
        write(UNIout,'(a,F17.9,a)')'Nuclear repulsion energy is ',ENERGY%nuclear,' Hartrees'
      end if
!
! Regular open shell RHF - Allow only one CI cycle.
      if(MUN_PRTLEV.gt.0)then
        write(UNIout,'(a,18x,a,20x,a,4x,a,3x,a,5x,a)')'CYCLE','ENERGY','SCF CONVERGENCE','EXTRAPOLATION','SCALE','RESET'
        write(UNIout,'(a,9x,a,11x,a,9x,a,6x,a)')' SCF','ELECTRONIC','TOTAL','COEFF','DENSITY'
      end if
!
! Initialize for first MCSCF cycle.
      call UPT_to_SQS (OVRLAP, MATlen, SQSM, NBasis)
      call SCHMDT (CMO_ROHF%coeff, SQSM, NBasis)
      call PHASE (CMO_ROHF%coeff, NBasis)
      call RHODENN (CMO_ROHF%coeff, PMN, NBasis, MATlen, NFock, CMO_ROHF%NFcoreMO, NFock)
!
      call BLD_JKRN
      call RHFVCCN ! Build occupancy
! NOTE: Using CMOG%occupancy = CMO_ROHF%occupancy!!!!!!!!!!!!! CMOG Open shell now double
      call DENBLD (CMO_ROHF%coeff, CMOG%occupancy, PM0_ROHF, NBasis, MATlen, CMO_ROHF%NoccMO)
      call GET_ROHF_energyN
      EPREV=ENERGY_ROHF%elec
!
      CNTSCF=0
      DWCSCF=0
      CRIT=TEN
      ACC=TEN
      Iend=1
      TROUBL=.FALSE.
      IRESET='NO  '
      IF(ROTMET(1:5).eq.'RESET')IRESET='YES '
      IF(ROTMET(1:4).eq.'OMIT')IRESET='OMIT'
!
      CIcycle=0
  190 continue
! GSCF Cycle.
      CIcycle=CIcycle+1
      if(CIcycle.gt.4)then
        write(UNIout,'(a)')'WARNING> CI cyles exceeded' 
        go to 600
      end if
      ISCF=0
      IEXT=1
      CCscale=SCALST
      ICUT=0
!
! Print energy with initial guess coefficients.
      if(MUN_PRTLEV.gt.0)then
        write(UNIout,1091)ISCF,ENERGY_ROHF%elec,ENERGY_ROHF%total
      end if
!
! *************************** SCF Cycles *******************************
  200 do while (ISCF.le.SCFITN)
      ISCF=ISCF+1
!
! Switch from Noreset to Increase mode if density convergence is small enough (and ROTMET='INCREMENT').
      if(ROTMET(1:4).NE.'INCR'.or.CRIT.gt.SCRIT.or.IEXT.NE.0)then
!
! Reset scale factor and ICUT counter in Reset/Omit mode.
        if(ROTMET(1:4).eq.'OMIT'.or.ROTMET(1:5).eq.'RESET')then
          CCscale=SCALST
          ICUT=0
        end if
      else
        IRESET='INCR'
        CCscale=FACTUP*CCscale
        if(CCscale.GE.SLIMUP)then
          CCscale=SLIMUP
          ICUT=0
        end if
      end if
!
      if(ISCF.gt.SCFITN.and.IEXT.eq.0)GO TO 500

      call MCFockM
      JCUT=0
!
! Get coefficient corrections.
  210 call PAIRSN (Iend, SCFCON)
!
! If Iend=0, SCF has converged normally.
      if(Iend.gt.0.or.(Iend.lt.0.and.CRIT.GE.SCFACC))then
! Check if both the coefficients and density are converged, but only the orbital corrections are not converged.
        if(IEXT.gt.0.or.CCONV.gt.TENM10.or.CRIT.gt.TENM10)GO TO 215
      if(MUN_PRTLEV.gt.0)then
        write(UNIout,*)' Coefficients and density have converged to 1.0D-10'
        GO TO 500
      end if
      end if

      Iend=0
      if(CRIT.lt.SCFACC.and.ISCF.gt.2)GO TO 460
!
! Coefficients converged but density not converged - Bump default coefficient convergence to 1.0D-5 and try again.
      if(ORBACC.lt.TENM5)GO TO 460
      ORBACC=TENM5
      GO TO 210
!
! Get C(NEW)=C(OLD)+CCscale*CC, where CC is the coefficient correction matrix.
  215   continue
      call ROTSCFN (E_ELEC, EPREV, TROUBL, ENERGY_ROHF%elec)
!
      CMO_ROHF%coeff=SCRMAT ! New coefficient matrix
      SCRPM(1:MATlen)=PM0_ROHF(1:MATlen) ! save old density matrix

! Compute a new density matrix
      call PHASE (CMO_ROHF%coeff, NBasis)
      call DENBLD (CMO_ROHF%coeff, CMOG%occupancy, PM0_ROHF, NBasis, MATlen, CMO_ROHF%NoccMO)
!
! Compute delta P (change in density matrix)
      SCRPM(1:MATlen)=PM0_ROHF(1:MATlen)-SCRPM(1:MATlen)
      CRIT=DSQRT(TRACLO (SCRPM, SCRPM, NBasis, MATlen))/DBLE(NBasis)
!
! Extrapolate, if possible, the coefficient matrix
      call CONRHF (CMO_ROHF%coeff, CMOBHF, SQSM, DCM1HF, DCM2HF, DCM3HF, OVRLAP, MOTB, SYMO, SCMO, CNTSCF, DWCSCF, &
                   Extrapolation_met, IEXT, LABEL, CCONV, MATCH, NBasis, CMO_ROHF%NoccMO, MATlen, DEWTOL)
!
! Recalculate density for extrapolated coefficients.
      if(IEXT.lt.0)then
        call PHASE (CMO_ROHF%coeff, NBasis)
        call DENBLD (CMO_ROHF%coeff, CMOG%occupancy, PM0_ROHF, NBasis, MATlen, CMO_ROHF%NoccMO)
      end if ! IEXT.lt.0
      SCRPM(1:MATlen)=PM0_ROHF(1:MATlen)
!
      ENERGY_ROHF%HF=ENERGY%nuclear+ENERGY_ROHF%elec
      ENERGY_ROHF%total=ENERGY%HF
      if(MUN_PRTLEV.gt.0)then
      if(IEXT.LE.0)then
        write(UNIout,'(i4,f19.9,f19.9,2X,2(1Pd11.4),2X,A8,4X,d11.4,4X,a4)') &
        ISCF,ENERGY_ROHF%elec,ENERGY_ROHF%total,CCONV,CRIT,LABEL,CCscale,IRESET
!1090 FORMAT(i4,F19.9,F19.9,2X,2(1PD11.4),2X,A8,4X,D11.4,4X,A4)
      else
        write(UNIout,1091)ISCF,ENERGY_ROHF%elec,ENERGY_ROHF%total,CRIT,LABEL,CCscale,IRESET
      end if
      end if

      call RHODENN (CMO_ROHF%coeff, PMN, NBasis, MATlen, NFock, CMO_ROHF%NFcoreMO, NFock)
      call BLD_JKRN

      end do ! while (ISCF.le.SCFITN) _____________________________________________________________
!
! Scale factor cut too often - Switch from Noreset to Reset mode if this is possible, else abort.
      if(ROTMET(1:4).eq.'INCR'.or.ROTMET(1:7).eq.'NORESET')then
        write(UNIout,'(a)')'*** TOO MANY SCALE FACTOR CUTS IN NORESET/INCREASE MODE'
        write(UNIout,'(a/)')' *** SWITCHING TO RESET MODE'
!1210 FORMAT('*** TOO MANY SCALE FACTOR CUTS IN NORESET/INCREASE MODE',' (LIMIT IS SLIMDN)'/' *** SWITCHING TO RESET MODE'/)
        TROUBL=.FALSE.
        ROTMET='RESET   '
        IRESET='YES '
        ISCF=ISCF-1
        GO TO 200
      else
        write(UNIout,'(A/16X,A)')'ERROR> RHOCLC: TOO MANY SCALE FACTOR CUTS (LIMIT IS SLIMDN)', &
        '> TRY DIFFERENT RESET/NORESET OPTIONS, AND/OR DIFFERENT VALUE OF ROTMIX'
        GO TO 500
      end if
!
! SCF coefficients have converged - Restart the SCF if Noreset mode in effect.
  460 if(ROTMET(1:5).eq.'RESET'.or.ROTMET(1:4).eq.'OMIT')GO TO 500
      if(ROTMET(1:4).eq.'INCR'.and.CCscale.GE.PT5)GO TO 500
      write(UNIout,'(a)')'*** SCF CONVERGED IN NORESET/INCREASE MODE'
      write(UNIout,'(a)')' *** Restarting IN RESET MODE FOR CONVERGENCE CHECK'
      ROTMET='RESET   '
      IRESET='YES '
      ISCF=ISCF-1
      GO TO 200
!
! SCF has converged or failed.
  500 continue
      call PHASE (CMO_ROHF%coeff, NBasis)
      call RHODENN (CMO_ROHF%coeff, PMN, NBasis, MATlen, NFock, CMO_ROHF%NFcoreMO, NFock)
      call BLD_JKRN
      call GET_ROHF_energyN
!
      call PHASE (CMO_ROHF%coeff, NBasis)
      call DENBLD (CMO_ROHF%coeff, CMOG%occupancy, PM0_ROHF, NBasis, MATlen, CMO_ROHF%NoccMO)

! Compute delta P (change in density matrix)
      SCRPM(1:MATlen)=PM0_ROHF(1:MATlen)-SCRPM(1:MATlen)
      ACC=DSQRT(TRACLO (SCRPM, SCRPM, NBasis, MATlen))/DBLE(NBasis)

      if(.not.TROUBL)then
        if(.not.MATCH)write(UNIout,'(a)')'*** MOLECULAR ORBITAL MATCHING FAILURE IN GSCF'
      if(MUN_PRTLEV.gt.0)then
        write(UNIout,'(i4,f19.9,f19.9,13X,1Pd11.4,2X,a8,4X,d11.4,4X,a4)') &
        ISCF,ENERGY_ROHF%elec,ENERGY_ROHF%total,ACC,LABEL,CCscale,IRESET
!1161 FORMAT(i4,F19.9,F19.9,13X,1PD11.4,2X,A8,4X,D11.4,4X,A4)
      end if
        if(ISCF.le.SCFITN)then ! Always allow convergence for RHF runs.
          if(ACC.ge.SCFACC.or.Iend.ne.0)GO TO 190
        end if
      end if
! GSCF is terminated.
  600 call MCFockM
      call BLD_Lagrange_matrix
!
      if(ISCF.gt.SCFITN)then
        write(UNIout,'(A)')'ERROR> RHOCLC: MAXIMUM NUMBER OF SCF CYCLES EXCEEDED'
        stop 'MAXIMUM NUMBER OF SCF CYCLES EXCEEDED'
      end if
      if(MUN_PRTLEV.gt.0)then
        write(UNIout,'(a,f16.6,a)')'At termination total energy is',ENERGY_ROHF%total,'  Hartrees'
        write(UNIout,'(1x,a,1PD12.5)')'Orbital convergence is',SCFCON
      end if
!
      CMOG%coeff=CMO_ROHF%coeff
! Rebuild???
      call DENBLD (CMO_ROHF%coeff, CMOG%occupancy, PM0_ROHF, NBasis, MATlen, CMO_ROHF%NoccMO)
!     write(6,*)'PM0 at end of ROHF'
!     call PRT_matrix (PM0_ROHF, matlen, nbasis)
      PM0=>PM0_ROHF
!
! Transform the p-orbitals to pur Px,Py and Pz
      if(Lpurifyp)then
        call purify_p
        write(UNIout,'(a)')'P-orbitals have been transformed to pure Ps and the density matrix has been updated'
        call DENBLD (CMO_ROHF%coeff, CMOG%occupancy, PM0_ROHF, NBasis, MATlen, CMO_ROHF%NoccMO)
      end if
!
      deallocate (CMOBHF, CMOC, DCM1HF, DCM2HF, DCM3HF, SCRPM, SCRMAT, SQSM, MOTB, SYMO, SCMO)
      deallocate(IJNDX)
      deallocate (EJKN)
      deallocate (CMOB%coeff,  PMN, JcoulN,  KexchN, FockMN)
!
! End of routine RHOCLC
      call PRG_manager ('exit', 'RHOCLC', 'UTILITY')
      return
!
 1091 FORMAT(i4,F19.9,F19.9,13X,1PD11.4,2X,A8,4X,D11.4,4X,A4)
!
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine SET_RHOCLC
!***********************************************************************
!     Date last modified: October 11, 2001                             *
!     Author: R.A. Poirier                                             *
!     Description: Initialize all RHF parameters.                      *
!***********************************************************************
! Modules:
      USE type_basis_set

      implicit none
!
! Local function:
      double precision GET_Enuclear
!
! Begin:
      call PRG_manager ('enter', 'SET_RHOCLC', 'UTILITY')
!
      ROTMIX=.false. ! for now
      SCFCON=TEN
!
! Make sure not combinations:
      integral_combinations=.false.
!
! Intialize the energies
      ENERGY=>ENERGY_ROHF
      ENERGY%nuclear=GET_Enuclear()
      ENERGY%HF=ZERO
      ENERGY%elec=ZERO
      ENERGY%total=ZERO
      ENERGY%wavefunction='ROHF'//Basis_name(1:len_trim(Basis_name))
!
      FACTDN=0.666666666666667D0
      SCRIT=2.0D-3
      FACTUP=1.2D0
      SLIMDN=0.00050D0
      SLIMUP=0.80D0
!
! April 9/98
      CMO=>CMO_ROHF
      PM0=>PM0_ROHF
      CMOBHF=CMO_ROHF%coeff
      call UPT_to_SQS (OVRLAP, MATlen, SQSM, NBasis)
      PMN=ZERO
      CMO_ROHF%eigval=ZERO
      DCM1HF=ZERO
      DCM2HF=ZERO
      DCM3HF=ZERO

! Dec30th/99
      SCALST=0.60D0
!
! End of routine SET_RHOCLC
      call PRG_manager ('exit', 'SET_RHOCLC', 'UTILITY')
      return
      end subroutine SET_RHOCLC
! END CONTAINS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine RHOCLC
      subroutine CONRHF (CMO_ROHF, &
                         CMOB, &
                         SQSM, &
                         DCM2, &
                         DCM3, &
                         DCM4, &
                         SM, &
                         MOTB, &
                         SYMO, &
                         SCMO, &
                         ICOUNT, &
                         IDEWIT, &
                         EXTMET, &
                         IEXT, &
                         LABEL, &
                         CCONV, &
                         MATCH, &
                         NBasis, &
                         NoccMO, &
                         MATlen, &
                         DEWTOL)
!***********************************************************************
!     Date last modified: June 26, 1992                                *
!     Author:                                                          *
!     Description:  MUN Version                                        *
!     RHF SCF/CI convergence routines:                                 *
!     RHF CMO_ROHF, CMOB, DCM2, DCM3, DCM4 refer to SCF.                   *
!     CI  CMO_ROHF, CMOB, DCM2, DCM3, DCM4 refer to CI.                    *
!     On return, IEXT is -1 if C has just been extrapolated, 0 for no  *
!     extrapolation, 1 for the first iteration after extrapolation.    *
!     On return, MATCH is normally true, but is set false if the       *
!     MO symmetry matching failed.                                     *
!***********************************************************************
! Modules:
      USE program_constants

      implicit none
!
! Input scalars:
      integer :: MATlen,NBasis,NoccMO
      double precision :: DEWTOL
      character*(*) :: EXTMET
!
! Input arrays:
      double precision :: CMO_ROHF(NBasis,NBasis),CMOB(NBasis,NBasis),DCM2(NBasis,NBasis),DCM3(NBasis,NBasis), &
                       DCM4(NBasis,NBasis),SQSM(NBasis,NBasis),SM(MATlen)
!
! Work arrays:
      integer :: MOTB(NoccMO,NoccMO)
      double precision :: SYMO(NBasis,NoccMO),SCMO(NoccMO,NoccMO)
!
! Output scalars:
      logical :: MATCH
      integer :: ICOUNT,IDEWIT,IEXT
      double precision :: CCONV
      character(len=8) :: LABEL
!
! Local scalars:
      integer IBasis,IEXTRP,J
      double precision COSPHI,COSPSI,DP1,DP2,DP3,ONEPT9,PT06,PT99,PT995,SP11,SP12,SP13,SP22,SP23,SP33,X, &
                       XXX,XY,Y,YYY,Y2
!
! LOWBND: Any value smaller than LOWBND is considered ZERO
      double precision LOWBND
!
! Local function:
      double precision TRARHF
!
      parameter (PT06=0.06D0,PT99=0.99D0,PT995=0.995D0,ONEPT9=1.9D0,LOWBND=0.0D0)
!
      SAVE SP12,SP22,Y
!
! Begin:
      call PRG_manager ('enter', 'CONRHF', 'UTILITY')
!
      IEXT=1
      LABEL='        '
      MATCH=.TRUE.
!
      ICOUNT=ICOUNT+1
      if(ICOUNT.eq.1)GO TO 400
      IEXT=0
!
      call MATCH_MO (MATCH, CMO_ROHF, CMOB, SM, NoccMO)
      if(.not.MATCH)then
! Failure in CSD.
        IDEWIT=0
        CCONV=ZERO
        ICOUNT=1
        GO TO 400
      end if
! Get difference of current and previous coefficients.
      do J=1,NoccMO
      do IBasis=1,NBasis
      DCM4(IBasis,J)=CMO_ROHF(IBasis,J)-CMOB(IBasis,J)
      end do ! IBasis
      end do ! J
!
! Find length DP1.
      SP11=TRARHF (DCM4, DCM4, NBasis, NoccMO)
      DP1=DSQRT(SP11)
      CCONV=DP1/DSQRT(DBLE(NBasis*NoccMO))
!
! What type of extrapolation?
!
      if(EXTMET(1:9).eq.'3,4 POINT')then
!
! Pople 3,4 point extrapolation.
!
        if(ICOUNT.LE.2)GO TO 385
        if(ICOUNT.eq.3)then
          SP12=TRARHF (DCM3, DCM4, NBasis, NoccMO)
          SP22=TRARHF (DCM3, DCM3, NBasis, NoccMO)
! Find length DP2.
          DP2=DSQRT(SP22)
          GO TO 375
        end if
!
        SP23=SP12
        SP33=SP22
        SP13=TRARHF (DCM2, DCM4, NBasis, NoccMO)
! Find length DP3.
        DP3=DSQRT(SP33)
! DCM2 contains  C(N-1) - C(N-2).
        SP12=TRARHF (DCM3, DCM4, NBasis, NoccMO)
        SP22=TRARHF (DCM3, DCM3, NBasis, NoccMO)
!
        if(SP11.LE.LOWBND.or.SP22.LE.LOWBND.or.SP33.LE.LOWBND)GO TO 375
!
! Find length DP2.
        DP2=DSQRT(SP22)
! Find cosine of angle between successive displacements.
        COSPHI=SP12/(DP1*DP2)
! Find cosine of angle between DP(3) and plane of DP(1) and DP(2).
        X=(SP13*SP22-SP12*SP23)/(SP11*SP22-SP12*SP12)
        Y=(SP23*SP11-SP12*SP13)/(SP11*SP22-SP12*SP12)
        COSPSI=DSQRT(X*X*SP11+Y*Y*SP22+TWO*X*Y*SP12)/DP3
!
! Do not extrapolate unless 4 consecutive points are nearly coplanar.
        if(COSPSI.LE.PT99)GO TO 375
!
! Express DP(1) as  X*DP(3)(Projected) + Y*DP(2).
        Y=-Y/X
        X=ONE/X
! Test if 2x2 matrix has real eigenvalues between -.95 and +.95.
        XY=Y*Y+FOUR*X
        if(XY.ge.ZERO)then
          XY=DABS(Y)+DSQRT(XY)
          if(XY.LE.ONEPT9)then
            XXX=X/(ONE-X-Y)
            YYY=(X+Y)/(ONE-X-Y)
            do J=1,NoccMO
            do IBasis=1,NBasis
            CMO_ROHF(IBasis,J)=CMO_ROHF(IBasis,J)+XXX*DCM3(IBasis,J)+YYY*DCM4(IBasis,J)
            end do ! IBasis
            end do ! J
!
            LABEL='4-POINT '
            ICOUNT=0
            IEXT=-1
            call SCHMDT (CMO_ROHF, SQSM, NBasis)
            call PRG_manager ('exit', 'CONRHF', 'UTILITY')
            return
          end if
        end if
!
! If 4-point extrapolation is not possible, try 3-point.
        if(DABS(COSPHI).gt.PT995)then
          X=DP1/(DP2*COSPHI-DP1)
          do J=1,NoccMO
          do IBasis=1,NBasis
          CMO_ROHF(IBasis,J)=CMO_ROHF(IBasis,J)+X*DCM4(IBasis,J)
          end do ! IBasis
          end do ! J
!
          LABEL='3-POINT '
          ICOUNT=0
          IEXT=-1
          call SCHMDT (CMO_ROHF, SQSM, NBasis)
          call PRG_manager ('exit', 'CONRHF', 'UTILITY')
          return
        end if
!
      else if(EXTMET(1:5).eq.'DEWAR')then
!
! Dewar extrapolation: M.J.S. Dewar and P.K. Weiner,
!                      Comp. & Chem., 2, 31 (1978).
! Calculate a new value for Lambda and save previous value in Y2.
! IDEWIT controls the extrapolation procedure:
! 1 - Just extrapolated the density matrix.
! 2 - Calculate LAMBDA2.
! 3 - Calculate LAMBDA3 and extrapolate.
!
        IDEWIT=IDEWIT+1
!
        if(IDEWIT.lt.2)GO TO 385
!
        if(IDEWIT.gt.2)Y2=Y
        SP22=TRARHF (DCM3, DCM3, NBasis, NoccMO)
        SP12=TRARHF (DCM3, DCM4, NBasis, NoccMO)
        Y=SP12/SP22
! Check for divergence.
        if(DABS(Y).ge.ONE)then
! Special value of Lambda for divergent cases.
          Y=SP12/SP11
          LABEL=' DEWAR2 '
        else
! Extrapolate if 2 successive Lambda's agree within DEWTOL.
          if(IDEWIT.eq.2)GO TO 385
          if(DABS(Y-Y2).gt.DEWTOL)GO TO 385
          LABEL=' DEWAR1 '
        end if
        X=Y/(ONE-Y)
        do J=1,NoccMO
        do IBasis=1,NBasis
        CMO_ROHF(IBasis,J)=CMO_ROHF(IBasis,J)+X*DCM4(IBasis,J)
        end do ! IBasis
        end do ! J
!
        ICOUNT=1
        IDEWIT=0
        IEXT=-1
        call SCHMDT (CMO_ROHF, SQSM, NBasis)
        call PRG_manager ('exit', 'CONRHF', 'UTILITY')
        return
!
      else
!
! No extrapolation.
        ICOUNT=1
        GO TO 400
!
      end if
!
  375 DCM2=DCM3
  385 DCM3=DCM4
  400 CMOB=CMO_ROHF
!
! End of routine CONRHF
      call PRG_manager ('exit', 'CONRHF', 'UTILITY')
      return
      end subroutine CONRHF
      subroutine ROHF_energy
!********************************************************************************************************
!     Date last modified: September 18, 2000                                               Version 2.0  *
!     Author: R.A. Poirier                                                                              *
!     Description: This routine only serves as an interface - causes the energy to be calculated        *
!********************************************************************************************************
! Modules:
      USE QM_defaults

      implicit none
!
! Begin:
      call PRG_manager ('enter', 'ROHF_energy', 'ENERGY%ROHF')
!
! This also creates the energy, so do nothing.
      call GET_object ('QM', 'CMO', Wavefunction)
!
! End of routine ROHF_energy
      call PRG_manager ('exit', 'ROHF_energy', 'ENERGY%ROHF')
      return
      end
      subroutine PAIRSN (Iend, SCFCON)
!***********************************************************************
!     Date last modified: June 26, 1992                                *
!     Author:                                                          *
!     Description:                                                     *
!     FockMN contains the Fock matrices.                                *
!     CMOC contains the coefficient corrections.                       *
!     Iend returns a convergence code: 0 if coeff's are converged,     *
!     -1 if the individual coefficients are not converged but the RMS  *
!     average of the coefficients is converged, and a positive value   *
!     returns the number of non-converged coefficients.                *
!     SCFCON is the RMS coefficient correction.                        *
!***********************************************************************
! Modules:
      USE program_constants
      USE QM_defaults
      USE QM_objects
      USE INT_objects
      USE GSCF_work

      implicit none
!
! Input scalars:
      integer Iend
      double precision :: SCFCON
!
! Local scalars:
      integer I,ICORE,IFIX,I1,J,JFIX,MM,M1,NCP,NCP1,NOCC,NOCC1,N1
      double precision ALPHA,BET,CIJ,DA,DB,DC,FF,FIVM5,FMN,SUM,TEN,TENM7,TUM
!
      parameter (TENM7=1.0D-7,FIVM5=5.0D-5,TEN=10.0D0)
!
! Begin:
      call PRG_manager ('enter', 'PAIRSN', 'UTILITY')
!
      SCFCON=ZERO
      NOCC=CMO%NFcoreMO+NVAL+NOPEN
      NOCC1=NOCC+1
!
! Calculate zero-th order matrix.
      ICORE=1-CMO%NFcoreMO
      CMOC=ZERO
!
      NCP=CMO%NFcoreMO+1
      NCP1=NCP+1
      if(CMO%NFcoreMO.gt.0)then
      if(CMO%NFcoreMO.lt.NOCC)then
      do I=NCP,NOCC
      IFIX=I+ICORE
      do J=1,CMO%NFcoreMO
      ALPHA=ZERO
      BET=ZERO
      MM=0
      do M1=1,Nbasis
      FMN=TWO
      do N1=1,M1
        if(N1.eq.M1)FMN=ONE
        MM=MM+1
        DA=CMO%coeff(M1,I)*CMO%coeff(N1,J)+CMO%coeff(N1,I)*CMO%coeff(M1,J)
        DB=CMO%coeff(M1,I)*CMO%coeff(N1,I)
        DC=CMO%coeff(M1,J)*CMO%coeff(N1,J)
        FF=FockMN(MM,1)-FockMN(MM,IFIX)
        ALPHA=ALPHA+FMN*DA*FF
        BET=BET+FMN*(DB-DC)*FF
      end do ! N1
      end do ! M1
!
      CMOC(I,J)=ALPHA*PT5
      CMOC(J,I)=BET
      end do ! J
      end do ! I
      end if
!
      if(NOCC.lt.Nbasis)then
! Core-Virtual.
      do I=NOCC1,Nbasis
      do J=1,CMO%NFcoreMO
      ALPHA=ZERO
      BET=ZERO
      MM=0
      do M1=1,Nbasis
      FMN=TWO
      do N1=1,M1
      MM=MM+1
      if(N1.eq.M1)FMN=ONE
      DA=CMO%coeff(M1,I)*CMO%coeff(N1,J)+CMO%coeff(N1,I)*CMO%coeff(M1,J)
      DB=CMO%coeff(M1,I)*CMO%coeff(N1,I)
      DC=CMO%coeff(M1,J)*CMO%coeff(N1,J)
      ALPHA=ALPHA+FMN*DA*FockMN(MM,1)
      BET=BET+FMN*(DB-DC)*FockMN(MM,1)
      end do ! N1
      end do ! M1
!
      CMOC(I,J)=ALPHA*PT5
      CMOC(J,I)=BET
      end do ! J
      end do ! I
      end if
      end if
!
! Valence-Valence.
      if(NCP1.le.NOCC)then
      do I=NCP1,NOCC
      IFIX=I+ICORE
      I1=I-1
      do J=NCP,I1
      JFIX=J+ICORE
      ALPHA=ZERO
      BET=ZERO
      MM=0
      do M1=1,Nbasis
      FMN=TWO
      do N1=1,M1
      MM=MM+1
      if(N1.eq.M1)FMN=ONE
      DA=CMO%coeff(M1,I)*CMO%coeff(N1,J)+CMO%coeff(N1,I)*CMO%coeff(M1,J)
      DB=CMO%coeff(M1,I)*CMO%coeff(N1,I)
      DC=CMO%coeff(M1,J)*CMO%coeff(N1,J)
      FF=FockMN(MM,JFIX)-FockMN(MM,IFIX)
      ALPHA=ALPHA+FMN*DA*FF
      BET=BET+FMN*(DB-DC)*FF
      end do ! N1
      end do ! M1
!
      CMOC(I,J)=ALPHA*PT5
      CMOC(J,I)=BET
      end do ! J
      end do ! I
      end if
!
      if(NOCC.lt.Nbasis.and.CMO%NFcoreMO.lt.NOCC)then
! Valence-Virtual.
      do I=NOCC1,Nbasis
      do J=NCP,NOCC
      JFIX=J+ICORE
      ALPHA=ZERO
      BET=ZERO
      MM=0
      do M1=1,Nbasis
      FMN=TWO
      do N1=1,M1
      MM=MM+1
      if(N1.eq.M1)FMN=ONE
      DA=CMO%coeff(M1,I)*CMO%coeff(N1,J)+CMO%coeff(N1,I)*CMO%coeff(M1,J)
      DB=CMO%coeff(M1,I)*CMO%coeff(N1,I)
      DC=CMO%coeff(M1,J)*CMO%coeff(N1,J)
      ALPHA=ALPHA+FMN*DA*FockMN(MM,JFIX)
      BET=BET+FMN*(DB-DC)*FockMN(MM,JFIX)
      end do ! N1
      end do ! M1
!
      CMOC(I,J)=ALPHA*PT5
      CMOC(J,I)=BET
      end do ! J
      end do ! I
      end if
! Test of convergence.
      Iend=0
      if(Nbasis.gt.1)then
      do I=2,Nbasis
      I1=I-1
      do J=1,I1
      CIJ=DABS(CMOC(I,J))
      SCFCON=SCFCON+CIJ*CIJ
      if(CIJ.lt.TENM7)CMOC(I,J)=ZERO
      if(CIJ.ge.ORBACC)Iend=Iend+1
      end do ! J
      end do ! I
      SCFCON=DSQRT(SCFCON/DBLE(MATlen-Nbasis))
      if(Iend.ne.0)then
      if(SCFCON.lt.ORBACC)Iend=-1
! Calculate the higher order corrections.
! coulomb integrals.
      SCRMAT=ZERO
!
      if(CMO%NFcoreMO.gt.0.and.CMO%NFcoreMO.lt.NOCC)then
      do I=NCP,NOCC
      IFIX=I+ICORE
      do J=1,CMO%NFcoreMO
      SUM=ZERO
      MM=0
      do M1=1,Nbasis
      FMN=TWO
      do N1=1,M1
      MM=MM+1
      if(N1.eq.M1)FMN=ONE
      DA=CMO%coeff(M1,J)*CMO%coeff(N1,J)
      SUM=SUM+FMN*DA*JcoulN(MM,IFIX)
      end do ! N1
      end do ! M1
!
      SCRMAT(I,J)=SUM
      end do ! J
      end do ! I
      end if
!
! Valence-Valence.
      if(NCP1.le.NOCC)then
      do I=NCP1,NOCC
      IFIX=I+ICORE
      I1=I-1
      do J=NCP,I1
      SUM=ZERO
      MM=0
      do M1=1,Nbasis
      FMN=TWO
      do N1=1,M1
      MM=MM+1
      if(N1.eq.M1)FMN=ONE
      DA=CMO%coeff(M1,J)*CMO%coeff(N1,J)
      SUM=SUM+FMN*DA*JcoulN(MM,IFIX)
      end do ! N1
      end do ! M1
!
      SCRMAT(I,J)=SUM
      end do ! J
      end do ! I
      end if
!
      if(NOCC.lt.Nbasis.and.CMO%NFcoreMO.lt.NOCC)then
! Valence-Virtual.
      do I=NOCC1,Nbasis
      do J=NCP,NOCC
      JFIX=J+ICORE
      SUM=ZERO
      MM=0
      do M1=1,Nbasis
      FMN=TWO
      do N1=1,M1
      MM=MM+1
      if(N1.eq.M1)FMN=ONE
      DA=CMO%coeff(M1,I)*CMO%coeff(N1,I)
      SUM=SUM+FMN*DA*JcoulN(MM,JFIX)
      end do ! N1
      end do ! M1
!
      SCRMAT(I,J)=SUM
      end do ! J
      end do ! I
      end if
!
! exchange integral.
      if(CMO%NFcoreMO.gt.0.and.CMO%NFcoreMO.lt.NOCC)then
      do I=NCP,NOCC
      IFIX=I+ICORE
      do J=1,CMO%NFcoreMO
      SUM=ZERO
      MM=0
      do M1=1,Nbasis
      FMN=TWO
      do N1=1,M1
      MM=MM+1
      if(N1.eq.M1)FMN=ONE
      DA=CMO%coeff(M1,J)*CMO%coeff(N1,J)
      SUM=SUM+FMN*DA*KexchN(MM,IFIX)
      end do ! N1
      end do ! M1
!
      SCRMAT(J,I)=SUM
      end do ! J
      end do ! I
      end if
!
! Valence-Valence.
      if(NCP1.le.NOCC)then
      do I=NCP1,NOCC
      IFIX=I+ICORE
      I1=I-1
      do J=NCP,I1
      SUM=ZERO
      MM=0
      do M1=1,Nbasis
      FMN=TWO
      do N1=1,M1
      MM=MM+1
      if(N1.eq.M1)FMN=ONE
      DA=CMO%coeff(M1,J)*CMO%coeff(N1,J)
      SUM=SUM+FMN*DA*KexchN(MM,IFIX)
      end do ! N1
      end do ! M1
!
      SCRMAT(J,I)=SUM
      end do ! J
      end do ! I
      end if
!
      if(NOCC.lt.Nbasis.and.CMO%NFcoreMO.lt.NOCC)then
! Valence-Virtual.
      do I=NOCC1,Nbasis
      do J=NCP,NOCC
      JFIX=J+ICORE
      SUM=ZERO
      MM=0
      do M1=1,Nbasis
      FMN=TWO
      do N1=1,M1
      MM=MM+1
      if(N1.eq.M1)FMN=ONE
      DA=CMO%coeff(M1,I)*CMO%coeff(N1,I)
      SUM=SUM+FMN*DA*KexchN(MM,JFIX)
      end do ! N1
      end do ! M1
!
      SCRMAT(J,I)=SUM
      end do ! J
      end do ! I
      end if
!
! Correction matrix.
      if(CMO%NFcoreMO.gt.0.and.CMO%NFcoreMO.lt.NOCC)then
! Core-Valence.
      do I=NCP,NOCC
      IFIX=I+ICORE
      do J=1,CMO%NFcoreMO
      TUM=FOUR*(VECCCN(IFIX,IFIX)+ONE-VECCCN(IFIX,1))*SCRMAT(J,I)- &
               (VECCCN(IFIX,IFIX)+ONE+TWO*VECCCN(1,IFIX))*(SCRMAT(I,J)+SCRMAT(J,I))
      CMOC(J,I)=CMOC(J,I)+TUM
      end do ! J
      end do ! I
      end if
! Valence-Valence.
      if(NCP1.le.NOCC)then
      do I=NCP1,NOCC
      IFIX=I+ICORE
      I1=I-1
      do J=NCP,I1
      JFIX=J+ICORE
      TUM=FOUR*(VECCCN(IFIX,IFIX)+VECCCN(JFIX,JFIX)-VECCCN(IFIX,JFIX))*SCRMAT(J,I)-(VECCCN(IFIX,IFIX)+VECCCN(JFIX,JFIX)+ &
                TWO*VECCCN(JFIX,IFIX))*(SCRMAT(I,J)+SCRMAT(J,I))
      CMOC(J,I)=CMOC(J,I)+TUM
      end do ! J
      end do ! I
      end if
!
      if(NOCC.lt.Nbasis.and.CMO%NFcoreMO.lt.NOCC)then
! Valence-Virtual.
      do I=NOCC1,Nbasis
      do J=NCP,NOCC
      JFIX=J+ICORE
      TUM=VECCCN(JFIX,JFIX)*(THREE*SCRMAT(J,I)-SCRMAT(I,J))
      CMOC(J,I)=CMOC(J,I)+TUM
      end do ! J
      end do ! I
!
      end if
      end if ! Iend
      end if ! Nbasis.gt.1
!
! End of routine PAIRSN
      call PRG_manager ('exit', 'PAIRSN', 'UTILITY')
      return
      end subroutine PAIRSN
      subroutine ROTSCFN (E_ELEC, &
                         EPREV, &
                         TROUBL, &
                         EELEC)
!***************************************************************************************
!     Date last modified: February 21, 1995                                            *
!     Author:                                                                          *
!     Description:                                                                     *
!     Two-by-two rotation by Hinze.  Calculate ( I I FK I J ).                         *
!     CCscale  - Current value of the scale factor (may be cut by ROTSCFN).            *
!     ICUT   - Number of scale factor cuts in ROTSCFN.                                 *
!     FACTDN - Factor to multiply with scale to get the next scale factor to be used.  *
!     SLIMDN - Lower limit on the scale factor.                                        *
!     CMO    - Current MO coefficients (pointer)                                       *
!     CMOC   - Coefficient corrections.                                                *
!     SQSM   - Overlap matrix.                                                         *
!     SCRMAT - Rotated coefficients (output).                                          *
!***************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE QM_defaults
      USE QM_objects
      USE INT_objects
      USE GSCF_work

      implicit none
!
! Input scalars:
      logical TROUBL
      double precision EELEC,E_ELEC,EPREV
!
!
! Local scalars:
      integer I,Ilast,ISTAT,ISTAT1,I1,J,JSTAT,NCP,NCP1,NFERM,NOCC,NOCC1
      logical :: LScale
!
! Begin:
      call PRG_manager ('enter', 'ROTSCFN', 'UTILITY')
!
      NOCC=CMO%NFcoreMO+NVAL+NOPEN
      NOCC1=NOCC+1
      NFERM=CMO%NFcoreMO+NDOUBL
      NCP=CMO%NFcoreMO+1
      NCP1=NCP+1
      Ilast=NFERM
      if(.not.ROTMIX)Ilast=NOCC
      SCRMAT=CMO%coeff
!     GO TO 110
!
! Try rotations again with a smaller scale.
! 100 CCscale=FACTDN*CCscale
!     ICUT=ICUT+1
! 110 SCRMAT=CMO%coeff
!
! Check for too many scale factor cuts.
  100 continue
      if(CCscale.lt.SLIMDN)then
        TROUBL=.TRUE.
        call PRG_manager ('exit', 'ROTSCFN', 'UTILITY')
        return
      end if
!
! Between Core and Valence orbitals.
      if(CMO%NFcoreMO.lt.Ilast.and.CMO%NFcoreMO.gt.0)then
      do I=NCP,Ilast
        do J=1,CMO%NFcoreMO
        call ROTATE_MO (I,J)
        if(LScale)go to 100
        end do ! J
      end do ! I
      end if ! CMO%NFcoreMO.lt.Ilast.and.CMO%NFcoreMO.gt.0
!
! Between Valence orbitals.
      if(NCP1.le.Ilast)then
      do I=NCP1,Ilast
        I1=I-1
        do J=NCP,I1
        call ROTATE_MO (I,J)
        if(LScale)go to 100
        end do ! J
      end do ! I
      end if ! NCP1.le.Ilast
!
      if(ROTMIX)then
!
! Between Valence orbitals.
         ISTAT=NFERM+1
         ISTAT1=ISTAT+1
         if(ISTAT1.le.NOCC)then
         do I=ISTAT1,NOCC
         I1=I-1
         do J=ISTAT,I1
        call ROTATE_MO (I,J)
        if(LScale)go to 100
         end do !J
         end do !I
         end if ! ISTAT1.le.NOCC
      end if ! ROTMIX
!
      if(NOCC.lt.Nbasis)then
      JSTAT=NFERM+1
      if(.not.ROTMIX)JSTAT=NCP
      if(JSTAT.le.NOCC)then
! Between Valence and Virtual orbitals.
      do I=NOCC1,Nbasis
      do J=JSTAT,NOCC
        call ROTATE_MO (I,J)
        if(LScale)go to 100
      end do ! J
      end do ! I
      end if ! JSTAT

      if(.not.ROTMIX.and.CMO%NFcoreMO.gt.0)then
! Between Core and Virtual orbitals.
      do I=NOCC1,Nbasis
      do J=1,CMO%NFcoreMO
        call ROTATE_MO (I,J)
        if(LScale)go to 100
      end do ! J
      end do ! I
      end if ! .not.ROTMIX.and.CMO%NFcoreMO.gt.0
      end if ! NOCC.lt.Nbasis
!
! Schmidt orthogonalization.
      call SCHMDT (SCRMAT, SQSM, Nbasis)
!
! Check for too many scale factor cuts.
      if(CCscale.lt.SLIMDN)then
        TROUBL=.TRUE.
        call PRG_manager ('exit', 'ROTSCFN', 'UTILITY')
        return
      end if
!
      EPREV=EELEC
      call CALENGN (EELEC, SCRMAT)
      if(EELEC.ge.E_ELEC)then
      E_ELEC=EELEC
      CCscale=FACTDN*CCscale
      JCUT=JCUT+1
      SCRMAT=CMO%coeff
      GO TO 100
      end if
!
! End of routine ROTSCFN
      call PRG_manager ('exit', 'ROTSCFN', 'UTILITY')
      return
!
 1000 FORMAT('WARNING> ROTSCFN> SIN =',1PE13.5,/ &
             20X,'- continued WITH NO ROTATION AT I =',I4,' J =',I4)
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine ROTATE_MO (I,J)
!***********************************************************************
!     Date last modified: Nov 2, 2012                                  *
!     Author: Ahmad and  R. A. Poirier                                 *
!     Description:  MUN Version                                        *
!     Form the Fock operator in atomic orbital basis (FockM).          *
!***********************************************************************
! Modules:

      implicit none
!     
! Input scalars:
      integer, intent(IN) :: I,J
!     
! Local scalars:
      integer :: Ibasis
      double precision :: ALPHA,BET,COSF,SGS,SINF,ROOTPT5,TEMP,THRES

      parameter (THRES=1.0D-6)
!
      LScale=.false.
      ROOTPT5=DSQRT(PT5)
      ALPHA=TWO*CMOC(I,J)
      BET=CMOC(J,I)
      if(ALPHA.ne.ZERO)then
        if(ALPHA.lt.ZERO)then
          SGS=-ONE
        else
          SGS=ONE
        end if ! ALPHA.lt.ZERO
!
      if(DABS(ALPHA).gt.THRES.and.DABS(BET).gt.THRES)then
        ALPHA=CCscale*ALPHA/BET
        SINF=SGS*DSQRT(ONE-ONE/DSQRT(ONE+ALPHA*ALPHA))
        if(DABS(SINF).ge.ROOTPT5)then
          if(ROTMET(1:4).ne.'OMIT')then
            CCscale=FACTDN*CCscale
            ICUT=ICUT+1
            SCRMAT=CMO%coeff
            LScale=.true.
            return
          end if
          write(UNIout,1000)SINF,I,J
          GO TO 120
        end if ! DABS(SINF).ge.ROOTPT5
!
        COSF=DSQRT(ONE-SINF*SINF)
        do Ibasis=1,Nbasis
          TEMP=SCRMAT(Ibasis,I)*COSF+SCRMAT(Ibasis,J)*SINF
          SCRMAT(Ibasis,J)=SCRMAT(Ibasis,J)*COSF-SCRMAT(Ibasis,I)*SINF
          SCRMAT(Ibasis,I)=TEMP
        end do ! Ibasis

      end if ! DABS(ALPHA).gt.THRES.and.DABS(BET).gt.THRES
      end if ! ALPHA.ne.ZERO
  120   continue
 1000 FORMAT('WARNING> ROTSCFN> SIN =',1PE13.5,/ &
             20X,'- continued WITH NO ROTATION AT I =',I4,' J =',I4)
!
! End of routine ROTATE_MO
      call PRG_manager ('exit', 'ROTATE_MO', 'UTILITY')
      return
      end subroutine ROTATE_MO
! END CONTAINS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine ROTSCFN
      subroutine MCFOCKM
!***********************************************************************
!     Date last modified: Nov 2, 2012                                  *
!     Author: Ahmad and  R. A. Poirier                                 *
!     Description:  MUN Version                                        *
!     Form the Fock operator in atomic orbital basis (FockM).          *
!***********************************************************************
! Modules:
      USE program_constants
      USE QM_defaults
      USE QM_objects
      USE INT_objects
      USE GSCF_work
      USE matrix_print

      implicit none
!     
! Local scalars:
      integer :: I,IJ,Ibasis,Jbasis,KFock,KFock1,LFock
!
! Begin:
      call PRG_manager ('enter', 'MCFOCKM', 'UTILITY')
!
! Build of FockMN: 
      do KFock=1,NFock
! H-Core Part,
        FockMN(1:MATlen,KFock)=CMO%occupancy(KFock)*HCORE(1:MATlen)
! Coulomb and Exchange Part,
        FockMN(1:MATlen,KFock)=FockMN(1:MATlen,KFock)  &
                     +TWO*VECCCN(KFock,KFock)*JcoulN(1:MATlen,KFock) &! Coulomb 
                         -VECCCN(KFock,KFock)*KexchN(1:MATlen,KFock)  ! Exchange
!                                                                              
        do LFock=1,KFock
          if(LFock.eq.KFock)exit
          FockMN(1:MATlen,KFock)=FockMN(1:MATlen,KFock)  &
                         +VECCCN(KFock,LFock)*JcoulN(1:MATlen,LFock) &! Coulomb  
                         +VECCCN(LFock,KFock)*KexchN(1:MATlen,LFock)  ! Exchange
        end do ! LFock                                                         
!                                                                              
        KFock1=KFock+1
        if(KFock.ge.NFock)exit  ! Changed from cycle
        do LFock=KFock1,NFock
          FockMN(1:MATlen,KFock)=FockMN(1:MATlen,KFock)  &
                         +VECCCN(LFock,KFock)*JcoulN(1:MATlen,LFock) &! Coulomb  
                         +VECCCN(KFock,LFock)*KexchN(1:MATlen,LFock)  ! Exchange
        end do ! LFock
      end do ! KFock
!
! End of routine MCFOCKM
      call PRG_manager ('exit', 'MCFOCKM', 'UTILITY')
!
      return
      end subroutine MCFOCKM
      subroutine RHFVCCN
!***********************************************************************
!     Date last modified: December 8, 2012                             *
!     Author:                                                          *
!     Description:                                                     *
!     Calculate vector coupling constants.                             *
!     The coupling constants are collected in VECCCN(I,J), where the   *
!     lower triangle stores A(I,J), the upper triangle stores B(I,J)   *
!     and the diagonal elements are A(I,I)+B(I,I).  However, the energy*
!     gradient (force) requires A(I,I) and B(I,I) separately, so       *
!     these are kept in columns NFock+1 and NFock+2 respectively.      *
!     Useful reference:  F.W. Bobrowicz and W.A. Goddard III,          *
!     Modern Theoretical Chemistry, Vol. 3, H.F. Schaefer III (ed.),   *
!     1977, P 79.                                                      *
!***********************************************************************
! Modules:
      USE program_constants
      USE QM_defaults
      USE QM_objects
      USE INT_objects
      USE GSCF_work

      implicit none
!
! Local scalars:
      integer :: ICORE,Iopen,II,IJ,IK,IL,IX,I1,I2,I3,J,JJ,J1,J2,NFock1,NFock2
!
! Begin:
      call PRG_manager ('enter', 'RHFVCCN', 'UTILITY')
!
      NFock1=NFock+1
      NFock2=NFock+2
!
      CMO_ROHF%occupancy(1:NFock)=ZERO
! Occupation number of H-Core part.
      CMO_ROHF%occupancy(1)=ONE
!
      ICORE=1-CMO_ROHF%NFcoreMO
      I1=CMO_ROHF%NFcoreMO+ICORE+1
      I2=CMO_ROHF%NFcoreMO+ICORE
      I3=CMO_ROHF%NFcoreMO+ICORE
!
      if(Nopen.gt.0)then ! Open-Shell Part.
        do Iopen=1,Nopen
          IL=Iopen+NFock-Nopen
          CMO_ROHF%occupancy(IL)=OCC(Iopen)
        end do ! Iopen
      end if ! Nopen.GT.0
! Vector coupling constants from two-electron parts.
! A(I,J): Lower triangle,  B(I,J): Upper triangle.
      VECCCN=ZERO
!
! Reference configuration.
      VECCCN(1,1)=ONE
      VECCCN(1,NFock1)=TWO
      VECCCN(1,NFock2)=-ONE
!
      if(Nopen.gt.0)then ! Pair-Open.
        do Iopen=1,Nopen
          II=Iopen+I3
          IK=Iopen*(Iopen+1)/2+1
          do J=1,I2
          VECCCN(II,J)=ALPHCC(IK)
          VECCCN(J,II)=-BETACC(IK)
        end do ! J
      end do ! Iopen
!
! Open-Open.
      do Iopen=1,Nopen
        J1=I3+Iopen
        II=(Iopen+2)*(Iopen+1)/2
        VECCCN(J1,J1)=ALPHCC(II)-BETACC(II)
        VECCCN(J1,NFock1)=ALPHCC(II)
        VECCCN(J1,NFock2)=-BETACC(II)
      end do ! Iopen
!
      if(Nopen.gt.1)then
      do Iopen=2,Nopen
        J1=I3+Iopen
        IX=Iopen-1
        do J=1,IX
        J2=I3+J
        II=Iopen*(Iopen+1)/2+J+1
        VECCCN(J1,J2)=ALPHCC(II)
        VECCCN(J2,J1)=-BETACC(II)
        end do ! J
        end do ! Iopen
      end if ! Nopen.gt.1
      end if ! Nopen.gt.0
!
! End of routine RHFVCCN
      call PRG_manager ('exit', 'RHFVCCN', 'UTILITY')
      return
      end subroutine RHFVCCN
      subroutine CALENGN (EELECN, CMOin)
!***********************************************************************
!     Date last modified: June 26, 1992                                *
!     Author: R.A. Poirier                                             *
!     Description: MUN Version                                         *
!     On entry, CMO_ROHF%coeff contains the MO vectors.                        *
!     FockM contains the Fock matrix.                                  *
!***********************************************************************
! Modules:
      USE program_constants
      USE QM_objects
      USE INT_objects
      USE QM_defaults
      USE GSCF_work

      implicit none
!
! Input scalars:
!
! Local scalars:
      integer :: IJ,Ibasis,Jbasis,KK,KCore,KFock,NFM1
      double precision :: FIJ,HVALN
      double precision, dimension(Nbasis,Nbasis) :: CMOin
!
! Output scalars:
      double precision :: EELECN
!
! Begin:
      call PRG_manager ('enter', 'CALENGN', 'UTILITY')
!
! Build of density matrix (PMN):
      PMN=ZERO
      NFM1=NFock-1
      IJ=0
      do Ibasis=1,Nbasis
        do Jbasis=1,Ibasis
          IJ=IJ+1
! Closed-shell part,          
          if(CMO%NFcoreMO.gt.0)then
           do KCore=1,CMO%NFcoreMO
            PMN(IJ,1)=PMN(IJ,1)+CMOin(Ibasis,KCore)*CMOin(Jbasis,KCore)
           end do ! KCore
          end if ! CMO%NFcoreMO.gt.0
! Open-shell part,
          if(NFock.gt.1)then
           do KFock=1,NFM1
             KK=KFock+CMO%NFcoreMO
             PMN(IJ,KFock+1)=CMOin(Ibasis,KK)*CMOin(Jbasis,KK)
           end do ! KFock
          end if ! NFock.gt.1
        end do ! Jbasis
      end do ! Ibasis
!
      EELECN=ZERO
      IJ=0
      do Ibasis=1,Nbasis
        FIJ=TWO
        do Jbasis=1,Ibasis
          IJ=IJ+1
          if(Ibasis.eq.Jbasis)FIJ=ONE
          do KFock=1,NFOCK
            EELECN=EELECN+FIJ*PMN(IJ,KFock)*(FockMN(IJ,KFock)+HCORE(IJ)*CMO%occupancy(KFock))
          end do ! KFock
        end do ! Jbasis
      end do ! Ibasis
!
! End of routine CALENGN
      call PRG_manager ('exit', 'CALENGN', 'UTILITY')
      return
      end subroutine CALENGN
      subroutine EJKBLDN
!***********************************************************************
!     Date last modified: November,11 2012                             *
!     Author:  Ahmad and  R. A. Poirier                                *
!     Description: Build the JK matrix.                                *
!***********************************************************************
! Modules:
      USE program_constants
      USE QM_defaults
      USE QM_objects
      USE INT_objects
      USE GSCF_work
      USE matrix_print

      implicit none
!     
! Local scalars:
      integer :: IJ,IJK,Ibasis,Jbasis,KFock,LFock
      double precision :: FIJ
!     
! Begin: 
      call PRG_manager ('enter', 'EJKBLDN', 'UTILITY')    
!
! Build of coulomb and exchange integrals matrix (EJKN):
! Clear the matrix elements,
      EJKN=ZERO
! Diagonal elements,
      IJ=0
      do Ibasis=1,Nbasis
        FIJ=TWO
        do Jbasis=1,Ibasis
          IJ=IJ+1
          if(Ibasis.eq.Jbasis)FIJ=ONE
          do KFock=1,NFock
            EJKN(KFock,KFock)=EJKN(KFock,KFock)+ &
                          FIJ*PMN(IJ,KFock)*JcoulN(IJ,KFock) ! Diagonal.
          end do ! KFock
! Off-diagonal elements,
          if(NFock.gt.1)then
           do KFock=2,NFock
             do LFock=1,KFock-1
               EJKN(KFock,LFock)=EJKN(KFock,LFock)+ &
                          FIJ*PMN(IJ,KFock)*JcoulN(IJ,LFock) ! Upper off-diagonal,(Coulomb).
               EJKN(LFock,KFock)=EJKN(LFock,KFock)+ &
                          FIJ*PMN(IJ,KFock)*KexchN(IJ,LFock) ! Lower off-diagonal,(Exchange).
             end do ! LFock
           end do ! KFock
          end if ! NFock.gt.1 
        end do ! Jbasis
      end do ! Ibasis
!
! Print EJKN,
!     write(6,*)"EJKN="
!     call PRT_matrix (EJKN, NFock, NFock)
!
! End of routine EJKBLDN
      call PRG_manager ('exit', 'EJKBLDN', 'UTILITY')
      return
      end subroutine EJKBLDN
      subroutine GET_ROHF_energyN
!***********************************************************************
!     Date last modified: November 11, 2012                            *
!     Author: Ahmad and R.A. Poirier                                   *
!     Description: Compute the ROHF energy                             *
!     PMN contains the density matrix, JcoulN contains the             *
!     coulomb matrix, and KexchN contains the exchange matrix.         *
!***********************************************************************
! Modules:
      USE program_constants
      USE QM_defaults
      USE QM_objects
      USE INT_objects
      USE GSCF_work
      USE matrix_print

      implicit none
!
! Local scalars:
      double precision :: EX11N,FIJ,ETVN1,ETandVN,EexchN,EcoulN,JandK,EtotalN
      integer :: IJ,JJ,IJK,Ibasis,Jbasis,KFock,Kopen,Lopen,Lopen1
!
! Begin:
      call PRG_manager ('enter', 'GET_ROHF_energyN', 'UTILITY')
!
      call EJKBLDN
      JandK=ZERO
!
! Calculate the ROHF energy: 
      IJ=0
      EX11N=ZERO
      ETVN1=ZERO
      E_T_ROHF=ZERO
      E_V_ROHF=ZERO
      do Ibasis=1,Nbasis
        FIJ=TWO
        do Jbasis=1,Ibasis
          IJ=IJ+1
          if(Ibasis.eq.Jbasis)FIJ=ONE
          ETVN1=ETVN1+FIJ*PMN(IJ,1)*HCORE(IJ)  ! Diagonal Fock element of closed orbitals.
          E_T_ROHF=E_T_ROHF+FIJ*PMN(IJ,1)*Tint(IJ)  ! Diagonal Fock element of closed orbitals.
          E_V_ROHF=E_V_ROHF+FIJ*PMN(IJ,1)*Vint(IJ)  ! Diagonal Fock element of closed orbitals.
          if(CMO_ROHF%NFcoreMO.gt.0)then
           EX11N=EX11N+FIJ*PMN(IJ,1)*KexchN(IJ,1)
          end if ! CMO_ROHF%NFcoreMO.gt.0
          if(Nopen.gt.0)then
           do KFock=2,NFock
             Kopen=KFock-1
             ETVN1=ETVN1+FIJ*PMN(IJ,KFock)*HCORE(IJ)*OCC(Kopen)
             E_T_ROHF=E_T_ROHF+FIJ*PMN(IJ,KFock)*Tint(IJ)*OCC(Kopen)
             E_V_ROHF=E_V_ROHF+FIJ*PMN(IJ,KFock)*Vint(IJ)*OCC(Kopen)
           end do ! KFock
          end if ! Nopen.gt.0
        end do ! Jbasis
      end do ! Ibasis
      EexchN=EX11N
      ETandVN=TWO*ETVN1
      E_T_ROHF=TWO*E_T_ROHF
      E_V_ROHF=TWO*E_V_ROHF
!
! Add the closed-shell contribution and closed-shell - open-shell contributions,
      if(CMO_ROHF%NFcoreMO.gt.0)then
        EcoulN=TWO*EJKN(1,1)
        JandK=EcoulN-EexchN  ! Closed-shell (2J-K)
        if(Nopen.gt.0)then
         do Kopen=1,Nopen
           IJ=Kopen+1
           JJ=Kopen*(Kopen+1)/2+1
           EcoulN=EcoulN+ALPHCC(JJ)*EJKN(IJ,1)
           EexchN=EexchN+BETACC(JJ)*EJKN(1,IJ)
           JandK=JandK+ALPHCC(JJ)*EJKN(IJ,1)-BETACC(JJ)*EJKN(1,IJ)
         end do ! Kopen
        end if ! Nopen.GT.0
      end if ! CMO_ROHF%NFcoreMO.GT.0
!
! Add the open-shell - open-shell contributions,
      if(Nopen.gt.0)then
        do Kopen=1,Nopen
          IJ=Kopen+1
          JJ=Kopen*(Kopen+1)/2+1
          JandK=JandK+ALPHCC(JJ)*EJKN(IJ,1)-BETACC(JJ)*EJKN(1,IJ)
          EcoulN=EcoulN+ALPHCC(JJ)*EJKN(IJ,1)
          EexchN=EexchN+BETACC(JJ)*EJKN(1,IJ)
          do Lopen=1,Nopen
            Lopen1=Lopen+1
            if(Lopen.le.Kopen)then
             JJ=Kopen*(Kopen+1)/2+Lopen+1
             JandK=JandK+ALPHCC(JJ)*EJKN(IJ,Lopen1)-BETACC(JJ)*EJKN(Lopen1,IJ)
             EcoulN=EcoulN+ALPHCC(JJ)*EJKN(IJ,Lopen1)
             EexchN=EexchN+BETACC(JJ)*EJKN(Lopen1,IJ)
            else
             JJ=Lopen*(Lopen+1)/2+Kopen+1
             JandK=JandK+ALPHCC(JJ)*EJKN(Lopen1,IJ)-BETACC(JJ)*EJKN(IJ,Lopen1)
             EcoulN=EcoulN+ALPHCC(JJ)*EJKN(Lopen1,IJ)
             EexchN=EexchN+BETACC(JJ)*EJKN(IJ,Lopen1)
            end if
          end do ! K
        end do ! I
      end if ! Nopen.GT.0
      EtotalN=ETandVN+EcoulN-EexchN
      ENERGY_ROHF%elec=ETandVN+JandK 
      ENERGY_ROHF%HF=ENERGY%nuclear+ENERGY_ROHF%elec
      ENERGY_ROHF%total=ENERGY%HF
      E_TandV_ROHF=ETandVN
      E_EXCH_ROHF=-EexchN
      E_COUL_ROHF=EcoulN
!
! End of routine GET_ROHF_energy
      call PRG_manager ('exit', 'GET_ROHF_energyN', 'UTILITY')
      return
      end subroutine GET_ROHF_energyN
      subroutine RHODENN (CMO_A, &
                         PM0AN, &
                         Nbasis, &
                         PDim1, &
                         PDim2, &   
                         Ncore, &
                         NFock)
!***********************************************************************
!     Date last modified: November 20, 2012                            *
!     Author: Ahmad                                                    *
!     Description: Form RHO orbital density matrix.  MUN Version.      *
!***********************************************************************
! Modules:
      USE program_constants
      
      implicit none
!
! Input scalars:
      integer :: PDim1,PDim2,Nbasis,Ncore,NFock
!
! Input array:
      double precision :: CMO_A(Nbasis,Nbasis)
!
! Output array:
      double precision :: PM0AN(PDim1,PDim2)
!
! Local scalars:
      integer :: IJ,Ibasis,Jbasis,Kcore,KFock,KK,Nopen
      double precision :: SUM
!
! Begin:
! Calculation of orbital density matrix.
! Density matrix is stored in two-dimensional array.
      Nopen=NFock-1
      IJ=0
      do Ibasis=1,Nbasis
         do Jbasis=1,Ibasis
            IJ=IJ+1
            SUM=ZERO
            if(Ncore.gt.0)then ! Core part.
               do Kcore=1,Ncore
                  SUM=SUM+CMO_A(Ibasis,Kcore)*CMO_A(Jbasis,Kcore)
               end do ! Kcore
            end if ! Ncore.gt.0
            PM0AN(IJ,1)=SUM
            if(NFock.gt.1)then ! Split orbital part.
              do KFock=1,Nopen
                KK=KFock+Ncore
                PM0AN(IJ,KFock+1)=CMO_A(Ibasis,KK)*CMO_A(Jbasis,KK)
              end do ! KFock
            end if ! NFock.gt.1
         end do ! Jbasis
      end do ! Ibasis
!
! End of routine RHODENN
      return
      end subroutine RHODENN
      subroutine BLD_JKRN
!***********************************************************************
!     Date last modified: November 23, 2012                            *
!     Author: R. A. Poirier and Ahmad                                  *
!     Description:  MUN Version                                        *
!     Form J and K matrices in atomic orbital basis.                   *
!***********************************************************************
! moduleS:
      USE program_constants
      USE QM_defaults
      USE QM_objects
      USE INT_objects
      USE GSCF_work
      USE matrix_print

      implicit none
!
! Local scalars:
      integer BUFSIZ,LENGTH
      logical EOF
!
! Begin:
      call PRG_manager ('enter', 'BLD_JKRN', 'UTILITY')
!
       JcoulN=ZERO
       KexchN=ZERO
!
! Calculate (RIJIS) and (RIKIS) matrix element.
      if(LIJKL_ONDISK)then
        EOF=.FALSE.
        rewind IJKL_unit
        call READ_ijkl (IJKL_BUFFER, IJKL_MAX, IJKL_unit, LENGTH, EOF)
        do while (.not.EOF)
          call BLD_ijkl_JKRN (IJKL_BUFFER, LENGTH)
          call READ_ijkl (IJKL_BUFFER, IJKL_MAX, IJKL_unit, LENGTH, EOF)
        end do
      end if ! ONDISK
!
      if(LIIKL_ONDISK)then
        EOF=.FALSE.
        rewind IIKL_unit
        call READ_iikl (IIKL_BUFFER, IIKL_MAX, IIKL_unit, LENGTH, EOF)
        do while (.not.EOF)
          call BLD_iikl_JKRN (IIKL_BUFFER, LENGTH)
          call READ_iikl (IIKL_BUFFER, IIKL_MAX, IIKL_unit, LENGTH, EOF)
         end do
      end if ! ONDISK
!
      if(LIJKJ_ONDISK)then
        EOF=.FALSE.
        rewind IJKJ_unit
        call READ_ijkj (IJKJ_BUFFER, IJKJ_MAX, IJKJ_unit, LENGTH, EOF)
        do while(.not.EOF)
          call BLD_ijkj_JKRN (IJKJ_BUFFER, LENGTH)
          call READ_ijkj (IJKJ_BUFFER, IJKJ_MAX, IJKJ_unit, LENGTH, EOF)
         end do
      end if ! ONDISK
!
      if(LIJJL_ONDISK)then
        EOF=.FALSE.
        rewind IJJL_unit
        call READ_ijjl (IJJL_BUFFER, IJJL_MAX, IJJL_unit, LENGTH, EOF)
        do while (.not.EOF)
          call BLD_ijjl_JKRN (IJJL_BUFFER, LENGTH)
          call READ_ijjl (IJJL_BUFFER, IJJL_MAX, IJJL_unit, LENGTH, EOF)
         end do
      end if ! ONDISK
!
! Now Process the INCORE integrals:
      call BLD_ijkl_JKRN (IJKL_INCORE, IJKL_count)
      call BLD_iikl_JKRN (IIKL_INCORE, IIKL_count)
      call BLD_IJKJ_JKRN (IJKJ_INCORE, IJKJ_count)
      call BLD_ijjl_JKRN (IJJL_INCORE, IJJL_count)

      call BLD_IIKK_JKRN (IIKK_INCORE, IIKK_count)
      call BLD_ijjj_JKRN (IJJJ_INCORE, IJJJ_count)
      call BLD_iiil_JKRN (IIIL_INCORE, IIIL_count)
      call BLD_iiii_JKRN (IIII_INCORE, IIII_count)
!
! End of routine BLD_JKRN
      call PRG_manager ('exit', 'BLD_JKRN', 'UTILITY')
      return
      end subroutine BLD_JKRN
      subroutine BLD_ijjj_JKRN (IJJJ_IN, &
                         LENGTH)
!***********************************************************************
!     Date last modified: November 23, 2012                            *
!     Author: R. A. Poirier and Ahmad                                  *
!     Description:  MUN Version                                        *
!     Form J and K matrices in atomic orbital basis.                   *
!***********************************************************************
! Modules:
      USE program_constants
      USE QM_objects
      USE INT_objects
      USE GSCF_work

      implicit none
!
! Input scalars:
      integer LENGTH
!
! Input arrays:
      type (ints_2e_ijjj) IJJJ_IN(LENGTH)
!
! Local scalars:
      integer I,I1,I2,KFock,Iao,Jao
      double precision G1
!
! Begin:
      call PRG_manager ('enter', 'BLD_ijjj_JKRN', 'UTILITY')
!
      IF(LENGTH.GT.0)then
      do I=1,LENGTH
      Iao=IJJJ_IN(I)%I
      Jao=IJJJ_IN(I)%J
      G1=IJJJ_IN(I)%ijjj
      I1=IJNDX(Iao)+Jao
      I2=IJNDX(Jao+1)

      do KFock=1,NFock
      JcoulN(I1,KFock)=JcoulN(I1,KFock)+PMN(I2,KFock)*G1
      JcoulN(I2,KFock)=JcoulN(I2,KFock)+PMN(I1,KFock)*TWO*G1
      KexchN(I1,KFock)=KexchN(I1,KFock)+PMN(I2,KFock)*G1
      KexchN(I2,KFock)=KexchN(I2,KFock)+PMN(I1,KFock)*TWO*G1
      end do ! KFock
      end do
      end if ! length.le.0
!
! End of routine BLD_ijjj_JKRN
      call PRG_manager ('exit', 'BLD_ijjj_JKRN', 'UTILITY')
      return
      end subroutine BLD_ijjj_JKRN
      subroutine BLD_iiil_JKRN (IIIL_IN, &
                         LENGTH)
!***********************************************************************
!     Date last modified: November 23, 2012                            *
!     Author: R. A. Poirier and Ahmad                                  *
!     Description:  MUN Version                                        *
!     Form J and K matrices in atomic orbital basis.                   *
!***********************************************************************
! Modules:
      USE program_constants
      USE QM_objects
      USE INT_objects
      USE GSCF_work

      implicit none
!
! Input scalars:
      integer LENGTH
!
! Input arrays:
      type (ints_2e_iiil) IIIL_IN(LENGTH)
!
! Local scalars:
      integer I,I1,I2,KFock,Iao,Jao
      double precision G1
!
! Begin:
      call PRG_manager ('enter', 'BLD_iiil_JKRN', 'UTILITY')
!
      IF(LENGTH.GT.0)then
      do I=1,LENGTH
      Iao=IIIL_IN(I)%I
      Jao=IIIL_IN(I)%L
      G1=IIIL_IN(I)%iiil
      I1=IJNDX(Iao+1)
      I2=IJNDX(Iao)+Jao

      do KFock=1,NFock
      JcoulN(I1,KFock)=JcoulN(I1,KFock)+PMN(I2,KFock)*TWO*G1
      JcoulN(I2,KFock)=JcoulN(I2,KFock)+PMN(I1,KFock)*G1
      KexchN(I1,KFock)=KexchN(I1,KFock)+PMN(I2,KFock)*TWO*G1
      KexchN(I2,KFock)=KexchN(I2,KFock)+PMN(I1,KFock)*G1
      end do ! KFock
      end do
      end if ! length.le.0
!
! End of routine BLD_iiil_JKRN
      call PRG_manager ('exit', 'BLD_iiil_JKRN', 'UTILITY')
      return
      end subroutine BLD_iiil_JKRN
      subroutine BLD_iiii_JKRN (IIII_IN, &
                         LENGTH)
!***********************************************************************
!     Date last modified: November 23, 2012                            *
!     Author: R. A. Poirier and Ahmad                                  *
!     Description:  MUN Version                                        *
!     Form J and K matrices in atomic orbital basis.                   *
!                                                                      *
!     Routines called: none                                            *
!***********************************************************************
! Modules:
      USE QM_objects
      USE INT_objects
      USE GSCF_work

      implicit none
!
! Input scalars:
      integer LENGTH
!
! Input arrays:
      type (ints_2e_iiii) IIII_IN(LENGTH)
!
! Local scalars:
      integer I,I1,I2,KFock,Iao,Jao
      double precision G1
!
! Begin:
      call PRG_manager ('enter', 'BLD_iiii_JKRN', 'UTILITY')
!
      IF(LENGTH.GT.0)then
      do I=1,LENGTH
      Iao=IIII_IN(I)%I
      G1=IIII_IN(I)%iiii
      I1=IJNDX(Iao+1)

      do KFock=1,NFock
      JcoulN(I1,KFock)=JcoulN(I1,KFock)+PMN(I1,KFock)*G1
      KexchN(I1,KFock)=KexchN(I1,KFock)+PMN(I1,KFock)*G1
      end do ! KFock
      end do
      end if ! length.le.0
!
! End of routine BLD_iiii_JKRN
      call PRG_manager ('exit', 'BLD_iiii_JKRN', 'UTILITY')
      return
      end subroutine BLD_iiii_JKRN
      subroutine BLD_ijkl_JKRN (IJKL_IN, &
                               LENGTH)
!***********************************************************************
!     Date last modified: November 23, 2012                            *
!     Author: R. A. Poirier and Ahmad                                  *
!     Description:  MUN Version                                        *
!     Form J and K matrices in atomic orbital basis.                   *
!                                                                      *
!     Routines called: none                                            *
!***********************************************************************
! Modules:
      USE program_constants
      USE QM_objects
      USE INT_objects
      USE GSCF_work

      implicit none
!
! Input scalars:
      integer LENGTH
!
! Input arrays:
      type (ints_2e_ijkl):: IJKL_IN(LENGTH)
!
! Local scalars:
      integer I,I1,I2,I3,I4,I5,I6,KFock,Iao,Jao,Kao,Lao
      double precision G1,G2,G3
!
! Begin:
      call PRG_manager ('enter', 'BLD_ijkl_JKRN', 'UTILITY')
!
      IF(LENGTH.GT.0)then
      do I=1,LENGTH
      Iao=IJKL_IN(I)%I
      Jao=IJKL_IN(I)%J
      Kao=IJKL_IN(I)%K
      Lao=IJKL_IN(I)%L
      G1=IJKL_IN(I)%IJKL
      G2=IJKL_IN(I)%IKJL
      G3=IJKL_IN(I)%ILJK
!
      I1=IJNDX(Iao)+Jao
      I2=IJNDX(Kao)+Lao
      I3=IJNDX(Iao)+Kao
      I4=IJNDX(Jao)+Lao
      I5=IJNDX(Iao)+Lao
      I6=IJNDX(Jao)+Kao
!
      do KFock=1,NFock
      JcoulN(I1,KFock)=JcoulN(I1,KFock)+PMN(I2,KFock)*TWO*G1
      JcoulN(I2,KFock)=JcoulN(I2,KFock)+PMN(I1,KFock)*TWO*G1
      JcoulN(I3,KFock)=JcoulN(I3,KFock)+PMN(I4,KFock)*TWO*G2
      JcoulN(I4,KFock)=JcoulN(I4,KFock)+PMN(I3,KFock)*TWO*G2
      JcoulN(I5,KFock)=JcoulN(I5,KFock)+PMN(I6,KFock)*TWO*G3
      JcoulN(I6,KFock)=JcoulN(I6,KFock)+PMN(I5,KFock)*TWO*G3
      KexchN(I1,KFock)=KexchN(I1,KFock)+PMN(I2,KFock)*(G2+G3)
      KexchN(I2,KFock)=KexchN(I2,KFock)+PMN(I1,KFock)*(G2+G3)
      KexchN(I3,KFock)=KexchN(I3,KFock)+PMN(I4,KFock)*(G1+G3)
      KexchN(I4,KFock)=KexchN(I4,KFock)+PMN(I3,KFock)*(G1+G3)
      KexchN(I5,KFock)=KexchN(I5,KFock)+PMN(I6,KFock)*(G1+G2)
      KexchN(I6,KFock)=KexchN(I6,KFock)+PMN(I5,KFock)*(G1+G2)
      end do ! KFock
      end do
      end if ! length.le.0
!
! End of routine BLD_ijkl_JKRN
      call PRG_manager ('exit', 'BLD_ijkl_JKRN', 'UTILITY')
      return
      end subroutine BLD_ijkl_JKRN
      subroutine BLD_iikl_JKRN (IIKL_IN, &
                               LENGTH)
!***********************************************************************
!     Date last modified: November 23, 2012                            *
!     Author: R. A. Poirier and Ahmad                                  *
!     Description:  MUN Version                                        *
!     Form J and K matrices in atomic orbital basis.                   *
!***********************************************************************
! Modules:
      USE program_constants
      USE QM_objects
      USE INT_objects
      USE GSCF_work

      implicit none
!
! Input scalars:
      integer LENGTH
!
! Input arrays:
      type (ints_2e_iikl) IIKL_IN(LENGTH)
!
! Local scalars:
      integer I,I1,I2,I3,I4,KFock,Iao,Jao,Kao
      double precision G1,G2
!
! Begin:
      call PRG_manager ('enter', 'BLD_iikl_JKRN', 'UTILITY')
!
      IF(LENGTH.GT.0)then
      do I=1,LENGTH
      Iao=IIKL_IN(I)%I
      Jao=IIKL_IN(I)%K
      Kao=IIKL_IN(I)%L
      G1=IIKL_IN(I)%iikl
      G2=IIKL_IN(I)%ikil
!
      I1=IJNDX(Iao+1)
      I2=IJNDX(Jao)+Kao
      I3=IJNDX(Iao)+Jao
      I4=IJNDX(Iao)+Kao
      do KFock=1,NFock
      JcoulN(I1,KFock)=JcoulN(I1,KFock)+PMN(I2,KFock)*TWO*G1
      JcoulN(I2,KFock)=JcoulN(I2,KFock)+PMN(I1,KFock)*G1
      JcoulN(I3,KFock)=JcoulN(I3,KFock)+PMN(I4,KFock)*TWO*G2
      JcoulN(I4,KFock)=JcoulN(I4,KFock)+PMN(I3,KFock)*TWO*G2
      KexchN(I1,KFock)=KexchN(I1,KFock)+PMN(I2,KFock)*TWO*G2
      KexchN(I2,KFock)=KexchN(I2,KFock)+PMN(I1,KFock)*G2
      KexchN(I3,KFock)=KexchN(I3,KFock)+PMN(I4,KFock)*(G1+G2)
      KexchN(I4,KFock)=KexchN(I4,KFock)+PMN(I3,KFock)*(G1+G2)
      end do ! KFock
      end do
      end if ! length.le.0
!
! End of routine BLD_iikl_JKRN
      call PRG_manager ('exit', 'BLD_iikl_JKRN', 'UTILITY')
      return
      end subroutine BLD_iikl_JKRN
      subroutine BLD_iikk_JKRN (IIKK_IN, &
                               LENGTH)
!***********************************************************************
!     Date last modified: November 23, 2012                            *
!     Author: R. A. Poirier and Ahmad                                  *
!     Description:  MUN Version                                        *
!     Form J and K matrices in atomic orbital basis.                   *
!***********************************************************************
! Modules:
      USE program_constants
      USE QM_objects
      USE INT_objects
      USE GSCF_work

      implicit none
!
! Input scalars:
      integer LENGTH
!
! Input arrays:
      type (ints_2e_iikk) IIKK_IN(LENGTH)
!
! Local scalars:
      integer I,I1,I2,I3,I4,KFock,Iao,Jao,Kao
      double precision G1,G2
!
! Begin:
      call PRG_manager ('enter', 'BLD_iikk_JKRN', 'UTILITY')
!
      IF(LENGTH.GT.0)then
      do I=1,LENGTH
      Iao=IIKK_IN(I)%I
      Jao=IIKK_IN(I)%K
      G1=IIKK_IN(I)%iikk
      G2=IIKK_IN(I)%ikik
      I1=IJNDX(Iao+1)
      I2=IJNDX(Jao+1)
      I3=IJNDX(Iao)+Jao
      do KFock=1,NFock
      JcoulN(I1,KFock)=JcoulN(I1,KFock)+PMN(I2,KFock)*G1
      JcoulN(I2,KFock)=JcoulN(I2,KFock)+PMN(I1,KFock)*G1
      JcoulN(I3,KFock)=JcoulN(I3,KFock)+PMN(I3,KFock)*TWO*G2
      KexchN(I1,KFock)=KexchN(I1,KFock)+PMN(I2,KFock)*G2
      KexchN(I2,KFock)=KexchN(I2,KFock)+PMN(I1,KFock)*G2
      KexchN(I3,KFock)=KexchN(I3,KFock)+PMN(I3,KFock)*(G1+G2)
      end do ! KFock
      end do
      end if ! length.le.0
!
! End of routine BLD_iikk_JKRN
      call PRG_manager ('exit', 'BLD_iikk_JKRN', 'UTILITY')
      return
      end subroutine BLD_iikk_JKRN
      subroutine BLD_ijkj_JKRN (IJKJ_IN, &
                               LENGTH)
!***********************************************************************
!     Date last modified: November 23, 2012                            *
!     Author: R. A. Poirier and Ahmad                                  *
!     Description:  MUN Version                                        *
!     Form J and K matrices in atomic orbital basis.                   *
!***********************************************************************
! Modules:
      USE program_constants
      USE QM_objects
      USE INT_objects
      USE GSCF_work

      implicit none
!
! Input scalars:
      integer LENGTH
!
! Input arrays:
      type (ints_2e_ijkj) IJKJ_IN(LENGTH)
!
! Local scalars:
      integer I,I1,I2,I3,I4,KFock,Iao,Jao,Kao
      double precision G1,G2
!
! Begin:
      call PRG_manager ('enter', 'BLD_ijkj_JKRN', 'UTILITY')
!
      IF(LENGTH.GT.0)then
      do I=1,LENGTH
      Iao=IJKJ_IN(I)%I
      Jao=IJKJ_IN(I)%J
      Kao=IJKJ_IN(I)%K
      G1=IJKJ_IN(I)%ijkj
      G2=IJKJ_IN(I)%ikjj
!
      I1=IJNDX(Iao)+Jao
      I2=IJNDX(Kao)+Jao
      I3=IJNDX(Jao+1)
      I4=IJNDX(Iao)+Kao

      do KFock=1,NFock
      JcoulN(I1,KFock)=JcoulN(I1,KFock)+PMN(I2,KFock)*TWO*G1
      JcoulN(I2,KFock)=JcoulN(I2,KFock)+PMN(I1,KFock)*TWO*G1
      JcoulN(I3,KFock)=JcoulN(I3,KFock)+PMN(I4,KFock)*TWO*G2
      JcoulN(I4,KFock)=JcoulN(I4,KFock)+PMN(I3,KFock)*G2
      KexchN(I1,KFock)=KexchN(I1,KFock)+PMN(I2,KFock)*(G1+G2)
      KexchN(I2,KFock)=KexchN(I2,KFock)+PMN(I1,KFock)*(G1+G2)
      KexchN(I3,KFock)=KexchN(I3,KFock)+PMN(I4,KFock)*TWO*G1
      KexchN(I4,KFock)=KexchN(I4,KFock)+PMN(I3,KFock)*G1
      end do ! KFock
      end do
      end if ! length.le.0
!
! End of routine BLD_ijkj_JKRN
      call PRG_manager ('exit', 'BLD_ijkj_JKRN', 'UTILITY')
      return
      end subroutine BLD_ijkj_JKRN
      subroutine BLD_ijjl_JKRN (IJJL_IN, &
                               LENGTH)
!***********************************************************************
!     Date last modified: November 23, 2012                            *
!     Author: R. A. Poirier and Ahmad                                  *
!     Description:  MUN Version                                        *
!     Form J and K matrices in atomic orbital basis.                   *
!***********************************************************************
! Modules:
      USE program_constants
      USE QM_objects
      USE INT_objects
      USE GSCF_work

      implicit none
!
! Input scalars:
      integer LENGTH
!
! Input arrays:
      type (ints_2e_ijjl) IJJL_IN(LENGTH)
!
! Local scalars:
      integer I,I1,I2,I3,I4,KFock,Iao,Jao,Kao
      double precision G1,G2
!
! Begin:
      call PRG_manager ('enter', 'BLD_ijjl_JKRN', 'UTILITY')
!
      IF(LENGTH.GT.0)then
      do I=1,LENGTH
      Iao=IJJL_IN(I)%I
      Jao=IJJL_IN(I)%J
      Kao=IJJL_IN(I)%L
      G1=IJJL_IN(I)%ijjl
      G2=IJJL_IN(I)%iljj
!
      I1=IJNDX(Iao)+Jao
      I2=IJNDX(Jao)+Kao
      I3=IJNDX(Jao+1)
      I4=IJNDX(Iao)+Kao

      do KFock=1,NFock
      JcoulN(I1,KFock)=JcoulN(I1,KFock)+PMN(I2,KFock)*TWO*G1
      JcoulN(I2,KFock)=JcoulN(I2,KFock)+PMN(I1,KFock)*TWO*G1
      JcoulN(I3,KFock)=JcoulN(I3,KFock)+PMN(I4,KFock)*TWO*G2
      JcoulN(I4,KFock)=JcoulN(I4,KFock)+PMN(I3,KFock)*G2
      KexchN(I1,KFock)=KexchN(I1,KFock)+PMN(I2,KFock)*(G1+G2)
      KexchN(I2,KFock)=KexchN(I2,KFock)+PMN(I1,KFock)*(G1+G2)
      KexchN(I3,KFock)=KexchN(I3,KFock)+PMN(I4,KFock)*TWO*G1
      KexchN(I4,KFock)=KexchN(I4,KFock)+PMN(I3,KFock)*G1
      end do ! KFock
      end do
      end if ! length.le.0
!
! End of routine BLD_ijjl_JKRN
      call PRG_manager ('exit', 'BLD_ijjl_JKRN', 'UTILITY')
      return
      end subroutine BLD_ijjl_JKRN
      subroutine SET_ROHF_defaults
!********************************************************************************************************
!     Date last modified: September 18, 2000                                               Version 2.0  *
!     Author: R.A. Poirier                                                                              *
!     Description: This routine only serves as an interface - causes the energy to be calculated        *
!********************************************************************************************************
! Modules:
      USE program_files
      USE QM_defaults
      USE type_molecule

      implicit none
!
! Begin:
      call PRG_manager ('enter', 'SET_ROHF_defaults', 'UTILITY')
!
! Set defaults for ROHF based on the multiplicity (ignore degeneracy).
      NOpen=Multiplicity-1
      NCore=(Nelectrons-NOpen)/2
      write(UNIout,'(a,i4,2x,a,i4)')'NOpen= ',NOpen,'NCore= ',NCore
      write(UNIout,'(a)')'NOTE: Assuming no degeneracy amongst open-shell orbitals'
!
! End of routine SET_ROHF_defaults
      call PRG_manager ('exit', 'SET_ROHF_defaults', 'UTILITY')
      return
      end subroutine SET_ROHF_defaults
      subroutine E_Components_ROHF
!*********************************************************************************
!     Date last modified: November 13, 2002                                      *
!     Author: R.A. Poirier                                                       *
!     Description: Calculate the different energy components: potential, kinetic,*
!     coulomb and exchange                                                       *
!*********************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE INT_objects
      USE QM_defaults
      USE QM_objects
      USE GSCF_work

      implicit none
!
! Local functions:
!
! Local scalars:
      double precision :: Vee
      double precision :: FIJ,EKinetic,EVne,ETandVN
      integer :: IJ,Ibasis,Jbasis,KFock,Kopen
!
! Local parameters:
      double precision :: PT25
      parameter (PT25=0.25D0)
!
! Begin:
      I2e_type='RAW'
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction) ! Causes creating of all required objects
!
      write(UNIout,'(a)')'Energy components:'
      write(UNIout,'(a,12x,f20.9)')'Kinetic = ',E_T_ROHF
      write(UNIout,'(a,10x,f20.9)')'Potential = ',E_V_ROHF
      write(UNIout,'(a,f20.9)')'Kinetic + Potential = ',E_TandV_ROHF
      write(UNIout,'(a,2x,f20.9)')'Coulomb repulsion = ',E_COUL_ROHF
      write(UNIout,'(a,11x,f20.9)')'Exchange = ',E_EXCH_ROHF
      Vee=E_COUL_ROHF+E_EXCH_ROHF
      write(UNIout,'(a,3x,f20.9)')'Coulomb+Exchange = ',Vee
      write(UNIout,'(a,12x,f20.9)')'Nuclear = ',ENERGY_ROHF%nuclear
      write(UNIout,'(a,3x,f20.9)')'Total electronic = ',ENERGY_ROHF%elec
      write(UNIout,'(a,7x,f20.9/)')'Total energy = ',ENERGY_ROHF%total
!
! End of routine E_Components_ROHF
      return
      end subroutine E_Components_ROHF
