      subroutine LMO_BOYS
!***********************************************************************
!     Date last modified: May 10, 1995                     Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: Perform Boys localization and size calculations.    *
!***********************************************************************
! Modules:
      USE program_manager
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE INT_objects
      USE matrix_print
      USE type_molecule
      USE type_epairs

      implicit none
!
! Input scalar:
      character*8 LMOFROM,FORWHAT
!
! Local scalars:
      integer :: LENOBJ,lenstr
      integer IERROR,CMONUM,I,IND1,IP,IQ,J,K,LCOUNT,LSET,NIT,NTLSET,NMOM1,NMOSET
      double precision :: A,LEIGVK,SUM,XI,YI,ZI
      logical FOUND,FOUNDI
!
! Work arrays:
      double precision, dimension(:,:), allocatable :: XMO
      double precision, dimension(:,:), allocatable :: YMO
      double precision, dimension(:,:), allocatable :: ZMO
      double precision, dimension(:,:), allocatable :: CMOSET
      double precision, dimension(:,:), allocatable :: LMOSET
      double precision, dimension(:,:), allocatable :: TCLSET
      double precision, dimension(:,:), allocatable :: SQSM
      double precision, dimension(:), allocatable :: EIGSET
      double precision, dimension(:), allocatable :: WORK
      integer, dimension(:), allocatable :: MOLIST
!     type (electron_pairs), dimension(:), allocatable :: Epairs
!
! Begin:
      call PRG_manager ('enter', 'LMO_BOYS', 'LMO%BOYS')
!
      IP=0
      LEIGVK=ZERO
      lenstr=len_trim(LMOFROM)
!
      MXBLIT=80
      call get_object ('QM','CMO','RHF') ! Improve (Only RHF at this time)
      call get_object ('INT','1EINT','DIPOLE')
      call get_object ('INT','1EINT','AO')
!
      if(.not.allocated(LMOLST))then
        allocate (LMOLST(1:Nbasis))
        call LMOINI
        write(UNIout,'(a)')'Warning LMO_BOYS> Using default for LMOLST'
      else
        NUMLMO=size(LMOLST)
      end if
!
      allocate (XMO(NUMLMO,NUMLMO), YMO(NUMLMO,NUMLMO), ZMO(NUMLMO,NUMLMO), CMOSET(Nbasis,NUMLMO), &
                LMOSET(Nbasis,NUMLMO), TCLSET(Nbasis,NUMLMO), EIGSET(NUMLMO), WORK(NBASIS), MOLIST(NBASIS), &
                SQSM(Nbasis,NBASIS), stat=IERROR)
      if(IERROR.ne.0)then
        write(UNIout,'(a)')'ERROR> LMO_BOYS: allocate failed'
        stop 'ERROR> LMO_BOYS: allocate failed'
      end if
!
      call UPT_to_SQS (OVRLAP, MATlen, SQSM, Nbasis)

      LCOUNT=0
      write(UNIout,'(a)')'************************'
      write(UNIout,'(a,21x,a)')'* LOCALIZATION PROGRAM *','BOYS PROCEDURE'
      write(UNIout,'(a)')'************************'
!
!     allocate (LMO%coeff(1:Nbasis,1:NUMLMO), LMO%EIGVAL(NBASIS), TCLMO(1:NUMLMO,1:NUMLMO), LMOCCX(NUMLMO), LMOCCY(NUMLMO), &
!               LMOCCZ(NUMLMO))
      allocate (LMO%coeff(1:Nbasis,1:NUMLMO))
      allocate (LMO%EIGVAL(NBASIS))
      allocate (TCLMO(1:NUMLMO,1:NUMLMO))
      allocate (LMOCCX(NUMLMO))
      allocate (LMOCCY(NUMLMO))
      allocate (LMOCCZ(NUMLMO))

      LMO%coeff(1:Nbasis,1:NUMLMO)=ZERO
      TCLMO(1:NUMLMO,1:NUMLMO)=ZERO
!
! Loop over all possible subset numbers.
      NTLSET=0
      do I=1,Nbasis
      J=1
      do while (J.LE.NUMLMO)
        IF(LMOLST(J).EQ.I)then
          NTLSET=NTLSET+1
          J=NUMLMO+1
        else
          J=J+1
        end if
      end do
      end do ! I

      IF(NTLSET.LE.0)then
        write(UNIout,'(a)')'ERROR> LMO_BOYS:'
        write(UNIout,'(a)')'NO MO TO LOCALIZE> CHECK YOUR INPUT - COMMAND: GROUP'
        stop 'NO MO TO LOCALIZE> CHECK YOUR INPUT - COMMAND: GROUP'
      end if
!
      write(UNIout,'(a,I8,a)')'TOTAL OF',NTLSET,' SETS'
!
! Should only be NTLSET
      MOLIST(1:Nbasis)=0

      K=0
      do I=1,Nbasis
      FOUNDI=.FALSE.
      do J=1,NUMLMO
      IF(LMOLST(J).EQ.I)then
        CMONUM=I
        FOUNDI=.TRUE.
      end if
      end do ! J

      IF(FOUNDI)then
      FOUND=.FALSE.
      do J=1,NTLSET
        IF(MOLIST(J).EQ.CMONUM)FOUND=.TRUE.
      end do ! J
      IF(.not.FOUND)then
        K=K+1
        MOLIST(K)=CMONUM
      end if
      end if ! FOUNDI

      end do ! I
!
! Get the first MO number for the first set.
      do LSET=1,NTLSET
      CMONUM=MOLIST(LSET)
!
      CMOSET(1:Nbasis,1:NUMLMO)=ZERO
      LMOSET(1:Nbasis,1:NUMLMO)=ZERO
      TCLSET(1:Nbasis,1:NUMLMO)=ZERO
!
      call LSHFT (CMO%coeff, LMOLST, CMOSET, Nbasis, NUMLMO, CMONUM, NMOSET)
      write(UNIout,'(a,i8,a,i8)')' Localize Set ',LSET,' Number of MO in Set= ',NMOSET
      IF(NMOSET.EQ.0)then
! No MO found for current subset - Are all MO used?
        write(UNIout,'(a)')'ERROR> LMO_BOYS:'
        write(UNIout,'(a)')'LOCALIZATION SUBSET SPECIFICATION: NOT ALL MO INCLUDED'
        stop ' localization subset specification error'
      end if
!
      XMO=ZERO
      YMO=ZERO
      ZMO=ZERO
      call I1E_transform (DipoleX_AO, CMOSET, XMO, Nbasis, NUMLMO, MATlen)
      call I1E_transform (DipoleY_AO, CMOSET, YMO, Nbasis, NUMLMO, MATlen)
      call I1E_transform (DipoleZ_AO, CMOSET, ZMO, Nbasis, NUMLMO, MATlen)
!
      IF(NMOSET.GT.1)call BOYLOC (XMO, YMO, ZMO, CMOSET, NMOSET, NIT, NUMLMO, Nbasis, MXBLIT)
!
      do I=1,NMOSET
        LMOCCX(I+LCOUNT)=XMO(I,I)
        LMOCCY(I+LCOUNT)=YMO(I,I)
        LMOCCZ(I+LCOUNT)=ZMO(I,I)
      end do
!
      do I=1,NMOSET
      do J=1,Nbasis
        LMOSET(J,I)=CMOSET(J,I)
      end do ! J
      end do ! I
!
      call LSHFT (CMO%coeff, LMOLST, TCLSET, Nbasis, NUMLMO, CMONUM, NMOSET)
!
      TCLSET=matmul(transpose(TCLSET), SQSM)
      TCLSET=matmul(TCLSET, LMOSET)
!     TCLMO=matmul(matmul(transpose(TCLSET), SQSM), LMOSET)
!
! Get eigenvalues.
      K=0
      do I=1,NUMLMO
!      IF(LMOLST(I).EQ.LSET)then ! Serious error found July 24/98 (RAP)
      IF(LMOLST(I).EQ.CMONUM)then
      K=K+1
      EIGSET(K)=CMO%EIGVAL(I)
      end if ! LMOLST(I)
      end do ! I

! Fock matrix over LMO basis (diagonal elements only).
      do I=1,NMOSET
        SUM=ZERO
      do K=1,NMOSET
        SUM=SUM+TCLSET(K,I)*TCLSET(K,I)*EIGSET(K)
      end do ! K
! Save 'eigenvalues' of LMO.
        LMO%EIGVAL(I+LCOUNT)=SUM
      end do ! I
!
! Sort LMO eigenvalues into ascending order.
      IF(NMOSET.NE.1)then
      NMOM1=NMOSET-1
      do IQ=1,NMOM1
      I=IQ+1
      LEIGVK=LMO%EIGVAL(IQ+LCOUNT)
      IP=IQ

      do J=I,NMOSET
      IF(LMO%EIGVAL(J+LCOUNT).LT.LEIGVK)then
        LEIGVK=LMO%EIGVAL(J+LCOUNT)
        IP=J
      end if
      end do ! J
      end do ! IQ
!
      IF(IP.NE.IQ)then
! Switch LMO IP and IQ.
      LMO%EIGVAL(IP+LCOUNT)=LMO%EIGVAL(IQ+LCOUNT)
      LMO%EIGVAL(IQ+LCOUNT)=LEIGVK
!
! Switch centroids of charge.
      A=LMOCCX(IP+LCOUNT)
      LMOCCX(IP+LCOUNT)=LMOCCX(IQ+LCOUNT)
      LMOCCX(IQ+LCOUNT)=A
      A=LMOCCY(IP+LCOUNT)
      LMOCCY(IP+LCOUNT)=LMOCCY(IQ+LCOUNT)
      LMOCCY(IQ+LCOUNT)=A
      A=LMOCCZ(IP+LCOUNT)
      LMOCCZ(IP+LCOUNT)=LMOCCZ(IQ+LCOUNT)
      LMOCCZ(IQ+LCOUNT)=A
!
! Switch columns of LMO and TCLMO matrices.
      do I=1,Nbasis
        A=LMOSET(I,IP)
        LMOSET(I,IP)=LMOSET(I,IQ)
        LMOSET(I,IQ)=A
      end do ! I
!
      do I=1,NMOSET
        A=TCLSET(I,IP)
        TCLSET(I,IP)=TCLSET(I,IQ)
        TCLSET(I,IQ)=A
      end do ! I
!
      end if ! (IP.NE.IQ)
!
      end if ! (NMOSET.NE.1)
!
      do I=1,NMOSET
      do J=1,Nbasis
        LMO%coeff(J,I+LCOUNT)=LMOSET(J,I)
      end do ! J
      do J=1,NMOSET
        TCLMO(LCOUNT+I,LCOUNT+J)=TCLSET(I,J)
      end do ! J
      end do ! I
!
      LCOUNT=LCOUNT+NMOSET
      end do ! LSET
! End loop of sets>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
      call phase (LMO%coeff, Nbasis)
!
! Print centroids, LMO and transformation matrix.
      IF(MUN_PRTLEV.GT.0)then
        write(UNIout,'(//a/a,17x,a,33x,a/)')'Centroids of the Localized Orbitals','   MO','Angstroms','Bohr'
        do I=1,NUMLMO
          XI=LMOCCX(I)*Bohr_to_Angstrom
          YI=LMOCCY(I)*Bohr_to_Angstrom
          ZI=LMOCCZ(I)*Bohr_to_Angstrom
          write(UNIout,'(1x,i5,5x,3f10.5,5x,3f10.5)')I,XI,YI,ZI,LMOCCX(I),LMOCCY(I),LMOCCZ(I)
        end do ! I
      end if ! MUN_PRTLEV
!
      IF(MUN_PRTLEV.GT.0)then
        write(UNIout, '(a)')'Localized Orbitals'
!        call GBSOUT (LMO%coeff, LEIGVL, Nbasis, NUMLMO, NUMLMO, 1)
        write(UNIout,'(a/)')'Transformation Matrix (CMO BY LMO)'
        call PRT_matrix (TCLMO, NUMLMO, NUMLMO)
      end if ! MUN_PRTLEV

!     allocate (Epairs(Nbasis))
!     call BLD_MO_IDmatrix (LMO%coeff, EPairs, Natoms, Nbasis, CMO%NoccMO)
!
      deallocate (XMO, YMO, ZMO, CMOSET, LMOSET, TCLSET, EIGSET, WORK, MOLIST)
      deallocate (LMOLST, TCLMO, LMOCCX, LMOCCY, LMOCCZ)
!
 1100 FORMAT(' *** WARNING: LMO FROM RHF SCF RUNS ARE DETERMINED AT THE USERS RISK ***'///)
!
! End of routine LMO_BOYS
      call PRG_manager ('exit', 'LMO_BOYS', 'LMO%BOYS')
      return
CONTAINS
      subroutine LMOINI
!***********************************************************************
!     Date last modified: May 10, 1995                     Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: Build the MO group vector.                          *
!***********************************************************************
! Modules:
      USE program_files

      implicit none
!
! Local scalars:
      integer I
!
! Begin:
      call PRG_manager ('enter', 'LMOINI', 'UTILITY')
!
      IF(NUMLMO.LE.0.or.NUMLMO.GT.Nbasis)then
        NUMLMO=Nbasis
        LMOLST(1:CMO%NoccMO)=1
        LMOLST(CMO%NoccMO+1:Nbasis)=2
      end if
!
      write(UNIout,'(a)')'SETS OF MOS TO BE LOCALIZED:'
      write(UNIout,'(132i1)')(LMOLST(I),I=1,NUMLMO)
!
! End of routine LMOINI
      call PRG_manager ('exit', 'LMOINI', 'UTILITY')
      return
      end subroutine LMOINI
      end subroutine LMO_BOYS
      subroutine LSHFT (CMO, &
                        LMOLST, &
                        CMOSET, &
                        Nbasis, &
                        NUMLMO, &
                        LSET, &
                        NMOSET)
!***********************************************************************
!     Date last modified: May 10, 1995                     Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!     Compress out unwanted columns of matrix A, dimensioned           *
!     Nbasis x NBASIS.  M and N supply current dimensions.             *
!     Only columns with LMOLST(I)=LSET are retained.                   *
!***********************************************************************
! Modules:
      USE program_manager

      implicit none
!
! Input scalars:
      integer LSET,Nbasis,NUMLMO
!
! Input arrays:
      integer,intent(IN) :: LMOLST(NUMLMO)
      double precision,intent(IN) :: CMO(Nbasis,NBASIS)
!
! Output array:
      double precision,intent(OUT) :: CMOSET(Nbasis,NUMLMO)
!
! Output scalar:
      integer NMOSET
!
! Local scalars:
      integer I,J
!
! Begin:
      call PRG_manager ('enter', 'LSHFT', 'UTILITY')
!
      NMOSET=0
      do J=1,NUMLMO
      IF(LMOLST(J).EQ.LSET)then
        NMOSET=NMOSET+1
        do I=1,Nbasis
          CMOSET(I,NMOSET)=CMO(I,J)
        end do ! I
      end if
      end do ! J
!
! End of routine LSHFT
      call PRG_manager ('exit', 'LSHFT', 'UTILITY')
      return
      end subroutine LSHFT
      subroutine BOYLOC (XMO, &
                         YMO, &
                         ZMO, &
                         LMO, &
                         NMOSET, &
                         NIT, &
                         NUMLMO, &
                         Nbasis, &
                         MXBLIT)
!***********************************************************************
!     Date last modified: May 10, 1995                     Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: Perform Boys localization.                          *
!***********************************************************************
! Modules:
      USE program_manager
      USE program_constants
      USE program_files

      implicit none
!
! Input scalars:
      integer,intent(IN) :: MXBLIT,Nbasis,NMOSET,NUMLMO
!
! Input arrays:
!
! Ouput scalars/arrays:
      integer,intent(OUT) :: NIT
      double precision,intent(OUT) :: XMO(NUMLMO,NUMLMO),YMO(NUMLMO,NUMLMO),ZMO(NUMLMO,NUMLMO)
      double precision,intent(OUT) :: LMO(Nbasis,NUMLMO)
!
! Local scalars:
      integer I,IFL,IND,INDI,INDJ,IP,J,NVEC
      double precision A,B,CA,CCA,CONVCRIT,CSA,C1,C2,C4,F1,F2,PT25,PT38,PT707,PT92,SA,SSA,TEMP,TENM5,TENM8, &
                       THRD,T1,XC,XD,XI,XJ,YC,YD,YI,YJ,ZC,ZD,ZI,ZJ
!
      parameter (PT25=0.25D0,PT38=0.3826834325D0,PT707=0.70710675D0,PT92=0.9238795329D0,TENM5=1.0D-5,TENM8=1.D-8)
!
! Begin:
      call PRG_manager ('enter', 'BOYLOC', 'UTILITY')
!
      SA=ZERO
      CONVCRIT=5.0D-06
      THRD=TENM8
      NIT=1
      IFL=0
      F1=ZERO
!
   10 IND=0
      F2=F1
      do I=2,NMOSET
      NVEC=I-1
      do J=1,NVEC
      XI=XMO(I,I)
      XJ=XMO(J,J)
      XD=XI-XJ
      YI=YMO(I,I)
      YJ=YMO(J,J)
      YD=YI-YJ
      ZI=ZMO(I,I)
      ZJ=ZMO(J,J)
      ZD=ZI-ZJ
      XC=XMO(I,J)
      YC=YMO(I,J)
      ZC=ZMO(I,J)
      B=XD*XC+YD*YC+ZD*ZC
      A=PT25*(XD*XD+YD*YD+ZD*ZD)-(XC*XC+YC*YC+ZC*ZC)
      IF(DABS(A).LE.THRD)A=ZERO
      IF(DABS(B).LE.THRD)B=ZERO
! Select rotation angle.
! Haddon and Williams, Chem. Phys. Letters 42, 453, 1976.
      IF(A.NE.ZERO.or.B.NE.ZERO)then
      IF((A.LT.ZERO.and.B.GT.ZERO).or.(A.GT.ZERO.AND.B.GT.ZERO))then
        T1=ONE/DSQRT(A*A+B*B)
        C4=A*T1
        TEMP=DSQRT(TWO+C4+C4)
        CA=PT5*DSQRT(TWO+TEMP)
        SA=-PT5*DSQRT(TWO-TEMP)
      else IF(A.LT.ZERO.and.B.EQ.ZERO)then
        SA=PT707
        CA=PT707
      else IF((A.LT.ZERO.and.B.LT.ZERO).or.(A.GT.ZERO.AND.B.LT.ZERO))then
        T1=ONE/DSQRT(A*A+B*B)
        C4=A*T1
        TEMP=DSQRT(TWO+C4+C4)
        SA=PT5*DSQRT(TWO-TEMP)
        CA=PT5*DSQRT(TWO+TEMP)
      else IF(A.EQ.ZERO.and.B.LT.ZERO)then
        SA=PT38
        CA=PT92
      else IF(A.EQ.ZERO.and.B.GT.ZERO)then
        CA=PT38
        SA=PT92
      else IF(A.GT.ZERO.and.B.EQ.ZERO)then
        CA=ONE
        SA=ZERO
      end if
! Rotate the M.O. coefficients and the dipole moment integrals.
      do IP=1,Nbasis
      C1=LMO(IP,I)
      C2=LMO(IP,J)
      LMO(IP,I)=C1*CA-C2*SA
      LMO(IP,J)=C2*CA+C1*SA
      end do !IP
!
      CSA=CA*SA
      CCA=CA*CA
      SSA=SA*SA
      call ROTAT (XMO, I, J, CA, SA, NMOSET, NUMLMO)
      XMO(I,I)=XI*CCA+XJ*SSA-TWO*XC*CSA
      XMO(J,J)=XI*SSA+XJ*CCA+TWO*XC*CSA
      XMO(I,J)=XD*CSA+XC*(CCA-SSA)
      call ROTAT (YMO, I, J, CA, SA, NMOSET, NUMLMO)
      YMO(I,I)=YI*CCA+YJ*SSA-TWO*YC*CSA
      YMO(J,J)=YI*SSA+YJ*CCA+TWO*YC*CSA
      YMO(I,J)=YD*CSA+YC*(CCA-SSA)
      call ROTAT (ZMO, I, J, CA, SA, NMOSET, NUMLMO)
      ZMO(I,I)=ZI*CCA+ZJ*SSA-TWO*ZC*CSA
      ZMO(J,J)=ZI*SSA+ZJ*CCA+TWO*ZC*CSA
      ZMO(I,J)=ZD*CSA+ZC*(CCA-SSA)
      end if
      end do !J
      end do !I
!
      F1=ZERO
      do I=1,NMOSET
      F1=F1+XMO(I,I)**2+YMO(I,I)**2+ZMO(I,I)**2
      end do !I
!
      IF(MUN_PRTLEV.GT.0)write(UNIout,1070)NIT,F1
      NIT=NIT+1
      IF(NIT.GT.MXBLIT)then
        write(UNIout,1060)NIT
      else
        IF(F1-F2.GT.CONVCRIT)GO TO 10
!
        IF(F1-F2.LE.-TENM5)then
          IFL=IFL+1
          write(UNIout,1080)NIT
          IF(IFL.LE.3)GO TO 10
          write(UNIout,1010)
        else
          write(UNIout,1090)NIT
        end if
      end if ! NIT.GT.MXBLIT
!
! End of routine BOYLOC
      call PRG_manager ('exit', 'BOYLOC', 'UTILITY')
      return
!
 1010 FORMAT('03 SUCCESSIVE FUNCTION DECREASES ... LOCALIZATION ','PROCESS TERMINATED')
 1060 FORMAT('0LOCALIZATION PROCESS HAS NOT CONVERGED AFTER',I6,' ITERATIONS'/' ',61('*')/)
 1070 FORMAT(' AT ITERATION ',I3,' THE LOCALIZATION FUNCTION = ',E15.8)
 1080 FORMAT(' ',50('*'),' FUNCTION DECREASING AT ITERATION ',I4)
 1090 FORMAT('0LOCALIZATION PROCESS HAS CONVERGED AT ITERATION',I4/)
!
      end subroutine BOYLOC
      subroutine ROTAT (X, &
                        I, &
                        J, &
                        CA, &
                        SA, &
                        NMOSET, &
                        NUMLMO)
!***********************************************************************
!     Date last modified: May 10, 1995                     Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: Rotate MO off diagonal integrals.                   *
!***********************************************************************
! Modules:
      USE program_manager

      implicit none
!
! Input scalars:
      integer,intent(IN) :: I,J,NMOSET,NUMLMO
      double precision,intent(IN) :: CA,SA
!
! Input array:
      double precision,intent(OUT) :: X(NUMLMO,NUMLMO)
!
! Local scalars:
      integer IK,JK,K,KI,KJ
      double precision A,B
!
! Begin:
      do K=1,NMOSET
      IF(K.NE.I.and.K.NE.J)then
      IF(K.LT.I)then
        IK=I
        KI=K
      else
        IK=K
        KI=I
      end if
      IF(K.LT.J)then
        JK=J
        KJ=K
      else
        JK=K
        KJ=J
      end if
!
      A=X(IK,KI)
      B=X(JK,KJ)
      X(IK,KI)=CA*A-SA*B
      X(JK,KJ)=CA*B+SA*A
!
      end if
      end do !K
!
! End of routine ROTAT
      return
      end subroutine ROTAT
      subroutine BLD_MO_IDmatrix (CMO, &
                                  Epairs, &
                                  Natoms, &
                                  Nbasis, &
                                  NoccMO)
!***********************************************************************
!     Date last modified: January 30, 2008                 Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: Bond identification.                                *
!***********************************************************************
! Modules:
      USE program_manager
      USE program_constants
      USE program_files
      USE QM_defaults
      USE type_basis_set
      USE type_epairs
      USE matrix_print

      implicit none
!
! Input scalars
      integer, intent(IN) :: Natoms,Nbasis,NoccMO
!
! Input Arrays:
      double precision CMO(Nbasis,Nbasis)
      type (electron_pairs) :: Epairs(Nbasis)
!
      double precision, dimension(:,:), allocatable :: LBOND
! Local scalars:
      integer Ibasis,Jbasis,Iatom,Ifound,I1,NcoreMO
      double precision SUM
      double precision BCRIT,CCRIT,MinCRIT
      logical :: Lfound
!
! Local function:

      parameter (CCRIT=0.03D0,MinCRIT=0.10D0)
!
! Begin:
      call PRG_manager ('enter', 'BLD_MO_IDmatrix', 'UTILITY')
!
      allocate (LBOND(Natoms,Nbasis))

!     BCRIT=0.30D0
      BCRIT=0.20D0
      LBOND(1:Natoms,1:Nbasis)=ZERO
      Epairs(1:Nbasis)%type='SIGMA'

      do Ibasis=1,Nbasis
        do Jbasis=1,Nbasis
          Iatom=Basis%AOtoATOM(Jbasis)
          LBOND(Iatom,Ibasis)=LBOND(Iatom,Ibasis)+CMO(Jbasis,Ibasis)*CMO(Jbasis,Ibasis)
        end do ! Jbasis
      end do ! Ibasis
!
      do Ibasis=1,Nbasis
        SUM=ZERO
        do Iatom=1,Natoms
          SUM=SUM+LBOND(Iatom,Ibasis)
        end do ! Iatom
        do Iatom=1,Natoms
          LBOND(Iatom,Ibasis)=LBOND(Iatom,Ibasis)/SUM
        end do ! Iatom
      end do ! Ibasis

      write(UNIout,'(a)')'MO Idendification matrix:'
      call PRT_matrix (LBOND, Natoms, Nbasis)

      NcoreMO=NFrozen_cores()
      Epairs(1:Nbasis)%AtomA=0
      Epairs(1:Nbasis)%AtomB=0
      Epairs(1:Nbasis)%AtomC=0
      do Ibasis=1,Nbasis
        Ifound=0
        Lfound=.false.
        do while (.not.Lfound.and.BCRIT.gt.MinCRIT)
        do Iatom=1,Natoms
          if(LBOND(Iatom,Ibasis).LT.BCRIT)cycle
            Ifound=Ifound+1
            select case (Ifound)
            case (1)
            Epairs(Ibasis)%AtomA=Iatom
            Epairs(Ibasis)%type='LPAIR'
            if(Ibasis.le.NcoreMO)then
              Epairs(Ibasis)%type='CORE'
            end if
            case (2)
            Epairs(Ibasis)%AtomB=Iatom
            Epairs(Ibasis)%type='SIGMA'
            case (3)
            Epairs(Ibasis)%AtomC=Iatom
            Epairs(Ibasis)%type='3-CENTER'
            case default
            end select
        end do ! Iatom
!
        if(Ifound.le.0)then ! No major contributions found
          BCRIT=BCRIT-CCRIT
        else
          Lfound=.true.
        end if
        end do ! while
        I1=len_trim(Epairs(Ibasis)%type)+1
        if(Ibasis.gt.NoccMO)then
          Epairs(Ibasis)%type(I1:I1)='*'
        end if
      end do ! Ibasis

      write(UNIout,'(a)')'  PAIR     A     B     C  Pair type'
      do Ibasis=1,Nbasis
        write(UNIout,'(4i6,2x,a)')Ibasis,Epairs(Ibasis)%AtomA,Epairs(Ibasis)%AtomB,Epairs(Ibasis)%AtomC,Epairs(Ibasis)%type
      end do ! Ibasis

      deallocate (LBOND)
!
! End of routine BLD_MO_IDmatrix
      call PRG_manager ('exit', 'BLD_MO_IDmatrix', 'UTILITY')
      return
      end subroutine BLD_MO_IDmatrix
