      subroutine RHO_Nuclei
!************************************************************************************************************
!     Date last modified: April 1, 2015                                                                     *
!     Author: R. A. Poirier                                                                                 *
!     Description: Compute the terms to calculation the correlation energy using density at the nucleus.    *
!************************************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE QM_objects
      USE QM_defaults
      USE type_molecule

      implicit none
!
! Local scalars:
      integer :: Iatom
!
! Local functions:
      double precision :: TraceAB
      double precision, dimension(:), allocatable :: AOprod
!
! Begin:
      call PRG_manager ('enter', 'RHO_NUCLEI', 'DENSITY%NUCLEI')
!
      call get_object ('QM', 'CMO', Wavefunction)

      if(.not.allocated(rho_0_Atom))then
        allocate (rho_0_Atom(1:Natoms))
      else
        deallocate (rho_0_Atom)
        allocate (rho_0_Atom(1:Natoms))
      end if
      allocate (AOprod(1:MATlen))

      do Iatom=1,Natoms
        if(CARTESIAN(Iatom)%Atomic_Number.le.0)cycle
        call AO_products (CARTESIAN(Iatom)%x, CARTESIAN(Iatom)%y, CARTESIAN(Iatom)%z, AOprod, MATlen)
        rho_0_Atom(Iatom)=TraceAB (PM0, AOprod, NBasis)
!       write(UNIout,'(a,f12.6)')'rho(0): ',rho_0_Atom(Iatom)
      end do

      deallocate (AOprod)
!
! End of routine RHO_Nuclei
      call PRG_manager ('exit', 'RHO_NUCLEI', 'DENSITY%NUCLEI')
      return
      end subroutine RHO_Nuclei

