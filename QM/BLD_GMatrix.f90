      subroutine BLD_GMatrix_STO3G
!****************************************************************************************************
!     Date last modified: September 10, 2019                                                        *
!     Author: R.A. Poirier                                                                          *
!     Description: Create BLD_GMatrix_STO3G hamiltonian for an STO-NG(*) basis                      *
!****************************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE type_basis_set
      USE QM_defaults
      USE QM_objects
      USE INT_objects
      USE matrix_print

      implicit none
!
! Local scalars:
      integer :: Ishell,Jshell,Iatmshl,Jatmshl,Ifirst,Jfirst,Ilast,Jlast,Iend,Jend
      integer :: FirstBFI,FirstBFJ,Ibasis,Jbasis,Irange,Jrange,ZnumA,ZnumB
      integer :: NAelectron,NBelectron
      double precision :: XA,YA,ZA,XB,YB,ZB,RABSQ
      logical :: LcoreI,LcoreJ
      logical :: SHLI_1S,SHLJ_1S
      logical :: SHLI_2S,SHLJ_2S
      logical :: SHLI_3S,SHLJ_3S
      logical :: SHLI_4S,SHLJ_4S
      logical :: SHLI_2P,SHLJ_2P
      logical :: SHLI_3P,SHLJ_3P
      logical :: SHLI_4P,SHLJ_4P
      logical :: SHLI_3D,SHLJ_3D
      logical :: Lcore1s1s
      character(len=16) SHLtypeI,SHLtypeJ
!
! Work arrays:
      double precision, dimension(:,:), allocatable :: GMSQS,HSQS,FSQS
      double precision, dimension(:,:), allocatable :: SMhalf,SCRM1
      double precision, dimension(:), allocatable :: GMatrix
!
! Parameters:
!
! Begin
      call PRG_manager ('enter', 'BLD_GMatrix_STO3G', 'UTILITY')
!
      call GET_object ('INT', '1EINT', 'AO')
!
! Local work arrays:
      write(6,*)'GUESS_basis%Nbasis: ',GUESS_basis%Nbasis
      call flush(6)
      MATlen=GUESS_basis%Nbasis*(GUESS_basis%Nbasis+1)/2
      allocate (GMatrix(MATlen))
      allocate (GMSQS(GUESS_basis%Nbasis,GUESS_basis%Nbasis))
      allocate (HSQS(GUESS_basis%Nbasis,GUESS_basis%Nbasis))
      allocate (FSQS(GUESS_basis%Nbasis,GUESS_basis%Nbasis))
      allocate (SMhalf(GUESS_basis%Nbasis,GUESS_basis%Nbasis))
      allocate (SCRM1(GUESS_basis%Nbasis,GUESS_basis%Nbasis))
      HSQS=ZERO
      call UPT_to_SQS (HCORE, MATlen, HSQS, GUESS_basis%Nbasis)
!
!
! Loop over shells...
      do Ishell=1,GUESS_basis%Nshells
      Ifirst=GUESS_basis%shell(Ishell)%frstSHL
      Ilast=GUESS_basis%shell(Ishell)%lastSHL
      LcoreI=GUESS_basis%shell(Ishell)%LcoreSHL
      SHLtypeI=GUESS_basis%shell(Ishell)%SHLTLB
      SHLI_1S=.false.
      SHLI_2S=.false.
      SHLI_3S=.false.
      SHLI_4S=.false.
      SHLI_2P=.false.
      SHLI_3P=.false.
      SHLI_4P=.false.
      SHLI_3D=.false.
      SHELL_typeI: select case (SHLtypeI)
      case ('1S')
        if(LcoreI)SHLI_1S=.true.
        Irange=1
      case ('2S')
        SHLI_2S=.true.
        Irange=1
      case ('3S')
        SHLI_3S=.true.
        Irange=1
      case ('3S*')
        SHLI_3S=.true.
        Irange=1
      case ('4S')
        SHLI_4S=.true.
        Irange=1
      case ('2P')
        SHLI_2P=.true.
        Irange=3
      case ('3P')
        SHLI_3P=.true.
        Irange=3
      case ('3P*')
        SHLI_3P=.true.
        Irange=3
      case ('4P')
        SHLI_4P=.true.
        Irange=3
      case ('3D*')
        SHLI_3D=.true.
        Irange=6
      case default
        write(UNIout,*)'error_BLD_GMatrix_STO3G> Illegal Shell type lable: ',GUESS_basis%shell(Ishell)%SHLTLB
        stop 'error_BLD_GMatrix_STO3G> Illegal Shell type lable '
      end select SHELL_typeI
!
      do Jshell=1,GUESS_basis%Nshells
      Jfirst=GUESS_basis%shell(Jshell)%frstSHL
      Jlast= GUESS_basis%shell(Jshell)%lastSHL
      LcoreJ=GUESS_basis%shell(Jshell)%LcoreSHL
      SHLtypeJ=GUESS_basis%shell(Jshell)%SHLTLB
      SHLJ_1S=.false.
      SHLJ_2S=.false.
      SHLJ_3S=.false.
      SHLJ_4S=.false.
      SHLJ_2P=.false.
      SHLJ_3P=.false.
      SHLJ_4P=.false.
      SHLJ_3D=.false.
      SHELL_typeJ: select case (SHLtypeJ)
      case ('1S')
        if(LcoreJ)SHLJ_1S=.true.
        Jrange=1
      case ('2S')
        SHLJ_2S=.true.
        Jrange=1
      case ('3S')
        SHLJ_3S=.true.
        Jrange=1
      case ('3S*')
        SHLJ_3S=.true.
        Jrange=1
      case ('4S')
        SHLJ_4S=.true.
        Jrange=1
      case ('2P')
        SHLJ_2P=.true.
        Jrange=3
      case ('3P')
        SHLJ_3P=.true.
        Jrange=3
      case ('3P*')
        SHLJ_3P=.true.
        Jrange=3
      case ('4P')
        SHLJ_4P=.true.
        Jrange=3
      case ('3D*')
        SHLJ_3D=.true.
        Jrange=6
      case default
        write(UNIout,*)'error_BLD_GMatrix_STO3G> Illegal Shell type lable: ',GUESS_basis%shell(Ishell)%SHLTLB
        stop 'error_BLD_GMatrix_STO3G> Illegal Shell type lable '
      end select SHELL_typeJ
      Lcore1s1s=.false.
      if((LcoreI.and.LcoreJ).and.(SHLI_1S.and.SHLJ_1S))Lcore1s1s=.true.
!
! Loop over Iatmshl
      do Iatmshl=Ifirst,Ilast
      FirstBFI=GUESS_basis%atmshl(Iatmshl)%frstAO
      ZnumA=CARTESIAN(Basis%atmshl(Iatmshl)%ATMLST)%Atomic_number
      XA=CARTESIAN(Basis%atmshl(Iatmshl)%ATMLST)%X
      YA=CARTESIAN(Basis%atmshl(Iatmshl)%ATMLST)%Y
      ZA=CARTESIAN(Basis%atmshl(Iatmshl)%ATMLST)%Z

! Do some 1centre here?
      do Jatmshl=Jfirst,Jlast
      FirstBFJ=GUESS_basis%atmshl(Jatmshl)%frstAO
      ZnumB=CARTESIAN(Basis%atmshl(Jatmshl)%ATMLST)%Atomic_number
      XB=CARTESIAN(Basis%atmshl(Jatmshl)%ATMLST)%X
      YB=CARTESIAN(Basis%atmshl(Jatmshl)%ATMLST)%Y
      ZB=CARTESIAN(Basis%atmshl(Jatmshl)%ATMLST)%Z
! RABSQ will be used to determine if 1-center or 2-center
      RABSQ=(XB-XA)**2+(YB-YA)**2+(ZB-ZA)**2

      Iend=FirstBFI+Irange-1
      Jend=FirstBFJ+Jrange-1
!       write(6,*)'FirstBFI,Iend: ',FirstBFI,Iend
!       write(6,*)'FirstBFJ,Jend: ',FirstBFJ,Jend
!
!     if(ZnumA.eq.ZnumB)then ! For now assume 1centre
      if(RABSQ.eq.ZERO)then ! For now assume 1centre
        if(Iatmshl.eq.Jatmshl)then
          call GMatrix_1C_ii
        else
          call GMatrix_1C_ij
        end if
      else 
        if(ZnumA.eq.1)then
          call GMatrix_HX (FirstBFI, ZnumB)
        else if(ZnumB.eq.1)then
          call GMatrix_HX (FirstBFJ, ZnumA)
!       call GMatrix_2centre
        end if
        call GMatrix_1C_ij
      end if
!
      end do ! Jatmshl
      end do ! Iatmshl
      end do ! Jshell
      end do ! Ishell

!     write(6,'(a)')'H matrix:'
!     call flush(6)
!     call PRT_matrix (HSQS, GUESS_basis%Nbasis, GUESS_basis%Nbasis)
      write(6,'(a)')'G matrix:'
      call PRT_matrix (GMSQS, GUESS_basis%Nbasis, GUESS_basis%Nbasis)
      call flush(6)
      GMatrix=ZERO
      call SQS_to_UPT (GMSQS, GUESS_basis%Nbasis, GMatrix, MATlen)
!
! Now for the Fock matrix
      allocate (FockM(MATlen))
      FockM(1:MATlen)=HCORE(1:MATlen)+GMatrix(1:MATlen)
      write(6,'(a)')'Fock matrix:'
      call flush(6)
      call PRT_matrix (FockM, MATlen, GUESS_basis%Nbasis)
      call flush(6)
!
! Now get the MOs
      call UPT_to_SQS (FockM, MATlen, FSQS, GUESS_basis%Nbasis)
      call BLD_SMhalf (OVRLAP, SMhalf, GUESS_basis%Nbasis)
      FSQS=matmul(matmul(SMhalf, FSQS), transpose(SMhalf))
      call MATRIX_diagonalize (FSQS, CMOG%coeff, CMOG%eigval, GUESS_basis%Nbasis, 2, .true.)   ! Better ?????
      CMOG%coeff=matmul(SMhalf, CMOG%coeff)

!     call DETlinW (OVRLAP, FockM, CMOG%coeff, CMOG%EIGVAL, MATlen, GUESS_basis%Nbasis, .true.)  ! Does not work????
      call DEF_NoccMO (CMOG%NoccMO, NAelectron, NBelectron)
      CMOG%occupancy(1:CMOG%NoccMO)=TWO
      CMOG%occupancy(CMOG%NoccMO+1:Basis%Nbasis)=ZERO
      write(6,'(a)')'Guess MOs after DETlinW in BLD_GMatrix_STO3G:'
      call PRT_matrix (CMOG%coeff, GUESS_basis%Nbasis, GUESS_basis%Nbasis)

      deallocate (GMSQS,HSQS)
      deallocate (GMatrix)
!
! End of routine BLD_GMatrix_STO3G
      call PRG_manager ('exit', 'BLD_GMatrix_STO3G', 'UTILITY')
      return
!
! Error section.
  900 write(UNIout,'(A,I4,A,I4,A)')'ERROR> BLD_GMatrix_STO3G: GUESS_basis%Nbasis =',GUESS_basis%Nbasis,' BUT ',Ibasis, &
                                   ' BASIS functionS GENERATED INTERNALLY'
      call PRG_manager ('exit', 'BLD_GMatrix_STO3G', 'UTILITY')
! End of routine BLD_GMatrix_STO3G
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine GMatrix_1C_ii
!****************************************************************************************************
!     Date last modified: September 10, 2019                                                        *
!     Author: R.A. Poirier                                                                          *
!     Description: Create BLD_GMatrix_STO3G hamiltonian for an STO-NG(*) basis                      *
!****************************************************************************************************
! Modules:
!
      implicit none
!
! Local scalars:
      double precision :: C1,C0,C2_core,C1_core,C0_core
      double precision :: C2_2S,C1_2S,C0_2S
      double precision :: C2_3S,C1_3S,C0_3S
      double precision :: C2_4S,C1_4S,C0_4S
      double precision :: C2_2P,C1_2P,C0_2P   ! Note must have intercept at zero, up to K
      double precision :: C2_3P,C1_3P,C0_3P
      double precision :: C2_4P,C1_4P,C0_4P
      double precision :: C2_3D,C1_3D,C0_3D
!
! Parameters:
      parameter (C2_core=-0.0002D0,C1_core=-0.3031D0,C0_core=0.0000D0)
      parameter (C2_2S=-0.0008D0,C1_2S=-0.5616D0,C0_2S=0.0000D0)
      parameter (C2_3S=-0.0017D0,C1_3S=-0.8462D0,C0_3S=0.0000D0)
      parameter (C2_4S=-0.000D0,C1_4S=-0.9460D0,C0_4S=0.0000D0)
      parameter (C2_2P=-0.0056,C1_2P=-0.968D0,C0_2P=0.000D0)
      parameter (C2_3P=-0.0033D0,C1_3P=-0.9904D0,C0_3P=0.0000D0)
      parameter (C2_4P=-0.001D0,C1_4P=-0.9838D0,C0_4P=0.0000D0)
      parameter (C2_3D=-0.0018,C1_3D=-0.9357,C0_3D=-0.0000D0)
      parameter (C1=-0.8666D0,C0= 0.0D0)
!
      do Ibasis=FirstBFI,Iend
      do Jbasis=FirstBFJ,Jend
        if(SHLI_1S.and.ZnumA.eq.1)then
          call GMatrix_HX (FirstBFI, ZnumA)
        else if(SHLI_2S)then
          GMSQS(Ibasis,Jbasis)=C2_2S*HSQS(Ibasis,Jbasis)**2+C1_2S*HSQS(Ibasis,Jbasis)+C0_2S
        else if(SHLI_3S)then
          GMSQS(Ibasis,Jbasis)=C2_3S*HSQS(Ibasis,Jbasis)**2+C1_3S*HSQS(Ibasis,Jbasis)+C0_3S
        else if(SHLI_4S)then
          GMSQS(Ibasis,Jbasis)=C2_4S*HSQS(Ibasis,Jbasis)**2+C1_4S*HSQS(Ibasis,Jbasis)+C0_4S
        else if(SHLI_2P)then
          if(ZnumA.gt.28)then ! Cu ...
            GMSQS(Ibasis,Jbasis)=-0.0007D0*HSQS(Ibasis,Jbasis)**2-0.7268D0*HSQS(Ibasis,Jbasis)
          else
            GMSQS(Ibasis,Jbasis)=C2_2P*HSQS(Ibasis,Jbasis)**2+C1_2P*HSQS(Ibasis,Jbasis)+C0_2P
          end if
        else if(SHLI_3P)then
          GMSQS(Ibasis,Jbasis)=C2_3P*HSQS(Ibasis,Jbasis)**2+C1_3P*HSQS(Ibasis,Jbasis)+C0_3P
        else if(SHLI_4P)then
          GMSQS(Ibasis,Jbasis)=C2_4P*HSQS(Ibasis,Jbasis)**2+C1_4P*HSQS(Ibasis,Jbasis)+C0_4P
        else if(SHLI_3D)then
          if(Ibasis.eq.Jbasis)then
            GMSQS(Ibasis,Jbasis)=C2_3D*HSQS(Ibasis,Jbasis)**2+C1_3D*HSQS(Ibasis,Jbasis)+C0_3D
          else
            GMSQS(Ibasis,Jbasis)=-0.67954D0*HSQS(Ibasis,Jbasis)
          end if
        else if(Lcore1s1s)then
          GMSQS(Ibasis,Jbasis)=C2_core*HSQS(Ibasis,Jbasis)**2+C1_core*HSQS(Ibasis,Jbasis)+C0_core
        else
          GMSQS(Ibasis,Jbasis)=C1*HSQS(Ibasis,Jbasis)-C0
        end if
      end do ! Ibasis
      end do ! Jbasis
!
! End of routine GMatrix_1C_ii
      return
      end subroutine GMatrix_1C_ii
      subroutine GMatrix_1C_ij
!****************************************************************************************************
!     Date last modified: September 10, 2019                                                        *
!     Author: R.A. Poirier                                                                          *
!     Description: Create BLD_GMatrix_STO3G hamiltonian for an STO-NG(*) basis                      *
!****************************************************************************************************
! Modules:
!
      implicit none
!
! Local scalars:
      double precision :: C1,C0
      double precision :: C2_1s2s,C1_1s2s,C0_1s2s
      double precision :: C2_1s3s,C1_1s3s,C0_1s3s
      double precision :: C2_2s3s,C1_2s3s,C0_2s3s
      double precision :: C1_2p3p
      double precision :: C1_1s3d,C1_2s3d,C1_3s3d
!
! Parameters:
      parameter (C1=-0.8666D0,C0= 0.0D0)
      parameter (C2_1s2s=-0.0003D0,C1_1s2s=-0.2462D0,C0_1s2s=0.000D0)
      parameter (C2_1s3s=-0.0019D0,C1_1s3s=-0.19780,C0_1s3s=0.000D0)
      parameter (C2_2s3s=-0.0010D0,C1_2s3s=-0.513D0,C0_2s3s=0.000D0)
      parameter (C1_2p3p=-0.5809D0)
      parameter (C1_1s3d=-0.1469D0)
      parameter (C1_2s3d=-0.4888D0)
      parameter (C1_3s3d=-0.7694D0)
!
      do Ibasis=FirstBFI,Iend
      do Jbasis=FirstBFJ,Jend
        if(SHLI_1S.and.SHLJ_2S)then
          GMSQS(Ibasis,Jbasis)=C2_1s2s*HSQS(Ibasis,Jbasis)**2+C1_1s2s*HSQS(Ibasis,Jbasis)+C0_1s2s
        else if(SHLI_2S.and.SHLJ_1S)then
          GMSQS(Ibasis,Jbasis)=C2_1s2s*HSQS(Ibasis,Jbasis)**2+C1_1s2s*HSQS(Ibasis,Jbasis)+C0_1s2s
        else if(SHLI_1S.and.SHLJ_3S)then
          GMSQS(Ibasis,Jbasis)=C2_1s3s*HSQS(Ibasis,Jbasis)**2+C1_1s3s*HSQS(Ibasis,Jbasis)+C0_1s3s
        else if(SHLI_3S.and.SHLJ_1S)then
          GMSQS(Ibasis,Jbasis)=C2_1s3s*HSQS(Ibasis,Jbasis)**2+C1_1s3s*HSQS(Ibasis,Jbasis)+C0_1s3s
        else if(SHLI_2S.and.SHLJ_3S)then
          GMSQS(Ibasis,Jbasis)=C2_2s3s*HSQS(Ibasis,Jbasis)**2+C1_2s3s*HSQS(Ibasis,Jbasis)+C0_2s3s
        else if(SHLI_3S.and.SHLJ_2S)then
          GMSQS(Ibasis,Jbasis)=C2_2s3s*HSQS(Ibasis,Jbasis)**2+C1_2s3s*HSQS(Ibasis,Jbasis)+C0_2s3s
        else if(SHLI_2P.and.SHLJ_3P)then
          GMSQS(Ibasis,Jbasis)=C1_2p3p*HSQS(Ibasis,Jbasis)
        else if(SHLI_3P.and.SHLJ_2P)then
          GMSQS(Ibasis,Jbasis)=C1_2p3p*HSQS(Ibasis,Jbasis)
        else if(SHLI_1S.and.SHLJ_3D)then
          GMSQS(Ibasis,Jbasis)=C1_1s3d*HSQS(Ibasis,Jbasis)
        else if(SHLI_2S.and.SHLJ_3D)then
          GMSQS(Ibasis,Jbasis)=C1_2s3d*HSQS(Ibasis,Jbasis)
        else if(SHLI_3S.and.SHLJ_3D)then
          GMSQS(Ibasis,Jbasis)=C1_3s3d*HSQS(Ibasis,Jbasis)
        else
          GMSQS(Ibasis,Jbasis)=C1*HSQS(Ibasis,Jbasis)-C0
        end if
      GMSQS(Jbasis,Ibasis)=GMSQS(Ibasis,Jbasis)
      end do ! Jbasis
      end do ! Ibasis
!
! End of routine GMatrix_1C_ij
      return
      end subroutine GMatrix_1C_ij
      subroutine GMatrix_2centre
!****************************************************************************************************
!     Date last modified: September 10, 2019                                                        *
!     Author: R.A. Poirier                                                                          *
!     Description: Create BLD_GMatrix_STO3G hamiltonian for an STO-NG(*) basis                      *
!****************************************************************************************************
! Modules:
!
      implicit none
!
! End of routine GMatrix_2centre
      return
      end subroutine GMatrix_2centre
      subroutine GMatrix_HX (Ibasis, ZnumX)
!****************************************************************************************************
!     Date last modified: September 10, 2019                                                        *
!     Author: R.A. Poirier                                                                          *
!     Description: Create BLD_GMatrix_STO3G hamiltonian for an STO-NG(*) basis                      *
!****************************************************************************************************
! Modules:
!
      implicit none
!
! Input scalars:
      integer :: Ibasis,ZnumX
!
! Local scalars:
      double precision :: C1_row1,C0_row1
      double precision :: C1_row2,C0_row2
      double precision :: C1_row3,C0_row3
!
! Parameters:
       parameter (C1_row1=-0.8910D0,C0_row1=-0.0308D0)
       parameter (C1_row2=-0.8748D0,C0_row2=+0.038110)
       parameter (C1_row3=-0.9495D0,C0_row3=+0.2021D0)
!
      if(ZnumX.eq.1)then
      else if(ZnumX.le.9)then
      write(6,*)'Ibasis,ZnumX: ',Ibasis,ZnumX
        GMSQS(Ibasis,Ibasis)=C1_row1*HSQS(Ibasis,Ibasis)+C0_row1
      else if(ZnumX.le.17)then
        GMSQS(Ibasis,Ibasis)=C1_row2*HSQS(Ibasis,Ibasis)+C0_row2
      else if(ZnumX.le.35)then
        GMSQS(Ibasis,Ibasis)=C1_row3*HSQS(Ibasis,Ibasis)+C0_row3
      end if
!
! End of routine GMatrix_HX
      return
      end subroutine GMatrix_HX
! END CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine BLD_GMatrix_STO3G
