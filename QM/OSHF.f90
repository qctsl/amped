      subroutine OSHF
!********************************************************************
!     Date last modified: Feb 13, 2013                              *
!     Author: Ahmad Alrawashdeh                                     *
!     Description: Solution of the high spin ROHF by diagonalizing  *
!                  effictive fock operator Matrix.   (MUN Version). *
!********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE matrix_print
      USE type_molecule
      USE QM_objects
      USE INT_objects
      USE ixtrapolate
      USE type_basis_set

      implicit none
!
! Local scalars:
      integer Jcycle
      integer I,IERROR
      double precision SCFCON
      double precision EPREV,SUM,T,TEN
!
! Local function:
      double precision ETRACE
!
! Work arrays:
      double precision, dimension(:), allocatable :: FMA
      double precision, dimension(:), allocatable :: FMB
      double precision, dimension(:), allocatable :: SCRVEC
      double precision, dimension(:), allocatable :: MATLIN1
      double precision, dimension(:), allocatable :: SOVLP
      double precision, dimension(:), allocatable :: DPM1
      double precision, dimension(:,:), allocatable :: FMOA
      double precision, dimension(:,:), allocatable :: FMOB
      double precision, dimension(:,:), allocatable :: CMO_A
      double precision, dimension(:,:), allocatable :: CMO_B
      double precision, dimension(:,:), allocatable :: SMhalf
!
! Total density matrix (alpha + beta)
      double precision, dimension(:), allocatable, save, target :: PM0_total
!
! Work arrays (OSHF):
      double precision, dimension(:), allocatable :: DGMA
      double precision, dimension(:), allocatable :: DGMB
!
! Extrapolation work arrays
!
! Local parameters:
      parameter (TEN=10.0D0)
!
! Begin:
      call PRG_manager ('enter', 'OSHF', 'CMO%OSHF')
!
! Make sure not integral combinations:
      call GET_object ('INT', '1EINT', 'AO')
      call GET_object ('INT', '2EINT', 'RAW')
      call GET_object ('QM', 'CMO_GUESS', 'UHF')
!
! Objects:
      if(.not.associated(CMOA%coeff))then
        allocate (CMOA%coeff(Nbasis,Nbasis), CMOA%eigval(1:Nbasis), CMOA%occupancy(1:Nbasis), &
                  CMOB%coeff(Nbasis,Nbasis), CMOB%eigval(1:Nbasis), CMOB%occupancy(1:Nbasis), &
                  CMO_A(Nbasis,Nbasis),CMO_B(Nbasis,Nbasis),STAT=IERROR)
        if(IERROR.ne.0)then
          stop ' OSHF> allocate failure: CMOA...'
        end if
        allocate (PM0_alpha(1:MATlen), PM0_beta(1:MATlen), PM0_total(1:MATlen), &
                  FMOA(Nbasis,Nbasis), FMOB(Nbasis,Nbasis), STAT=IERROR)
        if(IERROR.ne.0)then
          stop ' OSHF> allocate failure: PM0 ...'
        end if
      else
        if(Nbasis.ne.size(CMOA%coeff,1))then
        deallocate (CMOA%coeff, CMOB%coeff, CMOA%eigval, CMOB%eigval, PM0_alpha, PM0_beta, PM0_total, &
                    CMOA%occupancy, CMOB%occupancy, CMO_A, CMO_B, FMOA, FMOB)
        allocate (CMOA%coeff(Nbasis,Nbasis), CMOA%eigval(1:Nbasis), CMOA%occupancy(1:Nbasis), &
                  CMOB%coeff(Nbasis,Nbasis), CMOB%eigval(1:Nbasis), CMOB%occupancy(1:Nbasis), &
                  CMO_A(Nbasis,Nbasis),CMO_B(Nbasis,Nbasis),STAT=IERROR)
        if(IERROR.ne.0)then
          stop ' OSHF> allocate failure: CMOA...'
        end if
        allocate (PM0_alpha(1:MATlen), PM0_beta(1:MATlen), PM0_total(1:MATlen), &
                  FMOA(Nbasis,Nbasis), FMOB(Nbasis,Nbasis), STAT=IERROR)
        if(IERROR.ne.0)then
          stop ' OSHF> allocate failure: PM0 ...'
        end if
        end if
      end if
      allocate (SMhalf(Nbasis,Nbasis))
!
      call INI_OSHF
      call INI_Xpolation_UHF (PM0_alpha, PM0_beta, MATlen)
!
      call BLD_SMhalf (OVRLAP, SMhalf, Nbasis)

! Start SCF cycling.
      converged=.FALSE.
      do while (.not.converged)   ! ________________________________________________________________
! Form two-electron part of the Fock matrix.
      call UHFDGR (DGMA, DGMB)

      do I=1,MATlen
        FMA(I)=HCORE(I)+DGMA(I)
        FMB(I)=HCORE(I)+DGMB(I)
      end do
!
      ENERGY_UHF%elec=ETRACE (HCORE, FMA, PM0_alpha, Nbasis, MATlen)
      ENERGY_UHF%elec=ENERGY_UHF%elec+ETRACE (HCORE, FMB, PM0_beta, Nbasis, MATlen)
      ENERGY_UHF%elec=ENERGY_UHF%elec*PT5
      ENERGY_UHF%HF=ENERGY_UHF%nuclear+ENERGY_UHF%elec
      ENERGY_UHF%total=ENERGY_UHF%HF
!
      if(.not.Lextrapolated)EPREV=ENERGY_UHF%elec
! Print iteration results.
      if(MUN_prtlev.GT.0)then
      if(Lextrapolated)then
        write(UNIout,1000)Jcycle,ENERGY_UHF%elec,ENERGY_UHF%total,LABEL
 1000 FORMAT(1X,I4,1X,2F20.9,20x,a8)
      else
        write(UNIout,1001)Jcycle,ENERGY_UHF%elec,ENERGY_UHF%total,SCFCON
 1001 FORMAT(1X,I4,1X,2F20.9,1PE14.5)
      end if
      end if
!
! Form Alpha and Beta Fock matrices:
      call Build_OSFM(FMA, FMB, CMOA%coeff, CMOB%coeff, FMOA, FMOB, &
                                         CMOA%NoccMO, CMOB%NoccMO, MATlen, Nbasis)

      call MATRIX_diagonalize (FMOA, CMO_A, CMOA%eigval, Nbasis, 2, .true.)
      CMOA%coeff=matmul(CMOA%coeff, CMO_A)

      call MATRIX_diagonalize (FMOB, CMO_B, CMOB%eigval, Nbasis, 2, .true.)
      CMOB%coeff=CMOA%coeff !matmul(CMOB%coeff, CMO_B)

! Form Alpha and Beta density matrices:
      call DENBLD (CMOA%coeff, CMOA%occupancy, PM0_alpha, Nbasis, MATlen, CMOA%NoccMO)
      call DENBLD (CMOB%coeff, CMOB%occupancy, PM0_beta, Nbasis, MATlen, CMOB%NoccMO)
!
! Update total:
      PnM0(1:MATlen)=PM0_alpha(1:MATlen)
      PnM0(MATlen+1:2*MATlen)=PM0_beta(1:MATlen)
      call Xpolation (SCFACC, SCFCON, NBasis)
      PM0_alpha(1:MATlen)=extra(1:MATlen)
      PM0_beta(1:MATlen)=extra(MATlen+1:2*MATlen)
      PM0_total(1:MATlen)=PM0_alpha(1:MATlen)+PM0_beta(1:MATlen)
!
      Jcycle=Jcycle+1
!
! Has convergence been met?  If so, exit.
      if(converged)then
        LSCF_converged=.true.
        if(MUN_prtlev.GT.0)then
          write(UNIout,1010)ENERGY_UHF%total
        end if

        call PHASE (CMOA%coeff, Nbasis)
        call PHASE (CMOB%coeff, Nbasis)
!
! Calculate S, S(S+1).
        T=-TEN
        call TSPIN (T, ONE)
! Save MOs so they can be used as the next initial guess
        CMOAG%coeff(1:Nbasis,1:Nbasis)=CMOA%coeff(1:Nbasis,1:Nbasis)
        CMOBG%coeff(1:Nbasis,1:Nbasis)=CMOB%coeff(1:Nbasis,1:Nbasis)
        CMOAG%eigval(1:Nbasis)=CMOA%eigval(1:Nbasis)
        CMOBG%eigval(1:Nbasis)=CMOB%eigval(1:Nbasis)
        CMOG%coeff(1:Nbasis,1:Nbasis)=CMOAG%coeff(1:Nbasis,1:Nbasis)
!
! Have we exceeded the maximum allowed number of cycles? If so, exit.
      else if(Jcycle.gt.SCFITN)then ! Maximum iteration count exceeded without convergence.
        if(LOptimization.and.LSCF_converged)then
          LSCF_converged=.false.
          write(UNIout,'(a,i4,a)')'WARNING> OSHF: SCF DID NOT CONVERGE AFTER ',SCFITN,' ITERATIONS'
          write(UNIout,'(a)')'Optimization allowed to continue'
        else
          write(UNIout,'(A,I4,1X,A)')'ERROR> OSHF: SCF DID NOT CONVERGE AFTER',SCFITN,' ITERATIONS'
          stop 'OSHF: DID NOT CONVERGE '
        end if
      end if
      end do ! WHILE   ___________________________________________________________________________

      PM0_total(1:MATlen)=PM0_alpha(1:MATlen)+PM0_beta(1:MATlen)
!
      call END_Xpolation
      deallocate (FMA, FMB, DGMA, DGMB, DPM1, SCRVEC, MATLIN1, SOVLP)
!
 1010 FORMAT('At Termination Total Energy is',F17.6,'  Hartrees')
 1030 FORMAT('COEFFICIENTS (BETA SPIN)')
 1040 FORMAT('DENSITY MATRIX (ALPHA SPIN)')
 1060 FORMAT('DENSITY MATRIX (BETA SPIN)')
 1070 FORMAT(' E =',F20.9,10X,'DELTA E =',F15.9,10X,'CONVERGENCE =',1PE13.5,10X,'RUN ABORTED')
 1080 FORMAT(' E =',F20.9,10X,'DELTA E =',F15.9,10X,'CONVERGENCE =',1PE13.5,10X,'RUN ALLOWED TO continue')
 1100 FORMAT('FOCK MATRIX (ALPHA SPIN)')
 1110 FORMAT('FOCK MATRIX (BETA SPIN)')
!
! End of routine OSHF
      call PRG_manager ('exit', 'OSHF', 'CMO%OSHF')
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine INI_OSHF
!***********************************************************************
!     Date last modified: Feb 13, 2013                                 *
!     Author: Ahmad Alrawashdeh                                        *
!     Description: Initialize all ROHF parameters.                     *
!***********************************************************************
! Modules:

      implicit none
!
! Local function:
      double precision GET_Enuclear
      character (len=8) :: States(4)
      data States/'Singlet','Doublet','Triplet','Quartet'/
!
! Begin:
      call PRG_manager ('enter', 'INI_OSHF', 'UTILITY')
!
! Work arrays:
      allocate (FMA(1:MATlen), FMB(1:MATlen), DPM1(1:MATlen))
      allocate (DGMA(MATlen), DGMB(MATlen), SCRVEC(Nbasis), MATLIN1(MATlen), SOVLP(MATlen))
!
! Set Wavefunction to OSHF
      Wavefunction='OSHF'
      Level_of_Theory='OSHF'
!
! Intialize the energies
      ENERGY=>ENERGY_UHF
      ENERGY%nuclear=GET_Enuclear()
      ENERGY%HF=ZERO
      ENERGY%elec=ZERO
      ENERGY%total=ZERO
      ENERGY%wavefunction='UHF'//Basis_name(1:len_trim(Basis_name))
!
      FMA(1:MATlen)=ZERO
      FMB(1:MATlen)=ZERO
      CMOA%coeff(1:Nbasis,1:Nbasis)=CMOAG%coeff(1:Nbasis,1:Nbasis)
      CMOB%coeff(1:Nbasis,1:Nbasis)=CMOBG%coeff(1:Nbasis,1:Nbasis)
      CMOA%eigval(1:Nbasis)=CMOG%eigval(1:Nbasis)
      CMOB%eigval(1:Nbasis)=CMOG%eigval(1:Nbasis)
!
      PM0=>PM0_total ! pointer
      PM0_total(1:MATlen)=ZERO
!
      CMOA%NoccMO=CMOAG%NoccMO
      CMOB%NoccMO=CMOBG%NoccMO
      CMOA%occupancy=ZERO
      CMOB%occupancy=ZERO
      CMOA%occupancy(1:CMOA%NoccMO)=ONE
      if(CMOB%NoccMO.gt.0)CMOB%occupancy(1:CMOB%NoccMO)=ONE
      call DENBLD (CMOA%coeff, CMOA%occupancy, PM0_alpha, Nbasis, MATlen, CMOA%NoccMO)
      call DENBLD (CMOB%coeff, CMOB%occupancy, PM0_beta, Nbasis, MATlen, CMOB%NoccMO)
      PM0_total(1:MATlen)=PM0_alpha(1:MATlen)+PM0_beta(1:MATlen)
!
      SCFCON=TEN
      Jcycle=1
      if(MUN_prtlev.GT.0)then
        write(UNIout,'(3a,F17.9,a)')'OSHF Open Shell (',States(Multiplicity),') SCF, Nuclear Repulsion Energy: ', &
                                         ENERGY_UHF%nuclear,' Hartrees'
        write(UNIout,'(a,1PE12.4)')'Convergence on Density Matrix Required is ',SCFACC
        write(UNIout,'(a,6x,a)')'Cycle    Electronic Energy',' Total Energy    Convergence   Extrapolation'
      end if
!
! End of routine INI_OSHF
      call PRG_manager ('exit', 'INI_OSHF', 'UTILITY')
      return
      end subroutine INI_OSHF
      end subroutine OSHF
      subroutine Build_OSFM (FM_alpha, &
                             FM_beta, &
                             CMOA, &
                             CMOB, &
                             FMOA, &
                             FMOB, &
                             Nalpha, &
                             Nbeta, &
                             MATlen, &
                             Nbasis)
!**********************************************************************
!     Date last modified: Feb 13, 2013                                *
!     Author: Ahmad Alrawashdeh                                       *
!     Description: Build FMOA & FMOB matrices over MO basis, where    *
!                  FM_alpha and FM_beta are the UHF alpha and beta    *
!                  F matrices, and CMOA & CMOB are the expanding      *
!                  coefficient matrices for alpha and beta electrons. * 
!                  FGaimX matrix has the form:-                       *
!                                 closed     open    virtual          *
!                       closed     F_ab       F_b     F_ab            *
!                       open       F_b        F_ab    F_a             *
!                       virtual    F_ab       F_a     F_ab            *
!    Here:                                                            *
!             F_a=FM_alpha, F_b=FM_beta, F_ab=0.5(FM_alpha+FM_beta)   *
!**********************************************************************
! Modules:
      USE program_constants

      implicit none
!
! Input scalars:
      integer :: Nbasis,MATlen,Nalpha,Nbeta
!
! Input array:
      double precision :: FM_alpha(1:MATlen),FM_beta(1:MATlen)
      double precision :: CMOA(Nbasis,Nbasis), CMOB(Nbasis,Nbasis)
!
! Output array:
      double precision :: FMOA(Nbasis,Nbasis), FMOB(Nbasis,Nbasis)
! Local array:
      double precision :: FMA_sq(Nbasis,Nbasis),FMB_sq(Nbasis,Nbasis)
! Local scalars:
      integer :: MO_I,MO_J,St_open,End_open,St_vir,Mu,Nu
!
! Begin:
      call PRG_manager ('enter', 'Build_OSFM', 'UTILITY')
! 
      call UPT_to_SQS (FM_alpha, MATlen, FMA_sq, Nbasis)
      call UPT_to_SQS (FM_beta, MATlen, FMB_sq, Nbasis)
!
      St_open=Nbeta+1
      End_open=Nalpha
      St_vir=Nalpha+1
!
      FMOA=matmul(transpose(CMOA), matmul(FMA_sq, CMOA))
      FMOB=matmul(transpose(CMOB), matmul(FMB_sq, CMOB))
!
! Closed-Closed:
      do MO_I=1,Nbeta
        do MO_J=1,MO_I
           FMOA(MO_I,MO_J)=PT5*(FMOA(MO_I,MO_J)+FMOB(MO_I,MO_J)) 
           FMOA(MO_J,MO_I)=FMOA(MO_I,MO_J)

           FMOB(MO_I,MO_J)=PT5*(FMOA(MO_I,MO_J)+FMOB(MO_I,MO_J)) 
           FMOB(MO_J,MO_I)=FMOB(MO_I,MO_J)
        end do ! MO_I
      end do ! MO_J
!
! Open_Open:
      do MO_I=St_open,End_open
        do MO_J=St_open,MO_I
           FMOA(MO_I,MO_J)=PT5*(FMOA(MO_I,MO_J)+FMOB(MO_I,MO_J)) 
           FMOA(MO_J,MO_I)=FMOA(MO_I,MO_J)

           FMOB(MO_I,MO_J)=PT5*(FMOA(MO_I,MO_J)+FMOB(MO_I,MO_J)) 
           FMOB(MO_J,MO_I)=FMOB(MO_I,MO_J)
        end do ! MO_I
      end do ! MO_J

! Virtual-Virtual:
      do MO_I=St_vir,Nbasis
        do MO_J=St_vir,MO_I
           FMOA(MO_I,MO_J)=PT5*(FMOA(MO_I,MO_J)+FMOA(MO_I,MO_J)) 
           FMOA(MO_J,MO_I)=FMOA(MO_I,MO_J)

           FMOB(MO_I,MO_J)=PT5*(FMOB(MO_I,MO_J)+FMOB(MO_I,MO_J)) 
           FMOB(MO_J,MO_I)=FMOB(MO_I,MO_J)
        end do ! MO_I
      end do ! MO_J
!
! Open-Virtual & Virtual-Open:
      do MO_I=St_open,End_open
        do MO_J=St_vir,Nbasis
           FMOA(MO_I,MO_J)=PT5*(FMOA(MO_I,MO_J)+FMOA(MO_I,MO_J)) 
           FMOA(MO_J,MO_I)=FMOA(MO_I,MO_J)

           FMOB(MO_I,MO_J)=PT5*(FMOB(MO_I,MO_J)+FMOB(MO_I,MO_J)) 
           FMOB(MO_J,MO_I)=FMOB(MO_I,MO_J)
        end do ! MO_I
      end do ! MO_J

! Closed-Open & Open-closed:
      do MO_I=1,Nbeta
        do MO_J=St_open,End_open
           FMOA(MO_I,MO_J)=PT5*(FMOB(MO_I,MO_J)+FMOB(MO_I,MO_J)) 
           FMOA(MO_J,MO_I)=FMOA(MO_I,MO_J)

           FMOB(MO_I,MO_J)=PT5*(FMOB(MO_I,MO_J)+FMOB(MO_I,MO_J)) 
           FMOB(MO_J,MO_I)=FMOB(MO_I,MO_J)
        end do ! MO_I
      end do ! MO_J
!
! Closed-Virtual & Virtual-closed:
      do MO_I=1,Nbeta
        do MO_J=St_vir,Nbasis
           FMOA(MO_I,MO_J)=PT5*(FMOA(MO_I,MO_J)+FMOB(MO_I,MO_J)) 
           FMOA(MO_J,MO_I)=FMOA(MO_I,MO_J)

           FMOB(MO_I,MO_J)=PT5*(FMOA(MO_I,MO_J)+FMOB(MO_I,MO_J)) 
           FMOB(MO_J,MO_I)=FMOB(MO_I,MO_J)
        end do ! MO_I
      end do ! MO_J
!
! End of routine Build_OSFM
      return
      end subroutine Build_OSFM
