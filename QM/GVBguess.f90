      subroutine BLD_guess_MO_GVB
!***********************************************************************
!     Date last modified: May 15, 1995                     Version 1.0 *
!     Author: Y. Wang and R.A. Poirier                                 *
!     Description: Get  guess density matrix for GVB calc.             *
!***********************************************************************
! Modules:
      USE QM_defaults
      USE QM_objects
      USE INT_objects
      USE module_GVB

      implicit none
!
! Work array:
      integer, dimension(:), allocatable :: IORDER   !
      integer, dimension(:), allocatable :: MAPNEW ! Sorted MO numbers in terms of the original MO.
      integer, dimension(:), allocatable :: MAPOLD ! Original MO numbers in terms of the sorted MO.
      integer, dimension(:,:), allocatable :: CMOpairs
      double precision, dimension(:,:), allocatable :: SMSQ
!
! LOcal scalars:
      integer I,J,Inew
!
! Begin:
      call PRG_manager ('enter', 'BLD_guess_MO_GVB', 'UTILITY')
!
      if(associated(CMOG_GVB%coeff))deallocate(CMOG_GVB%coeff)
      allocate (CMOG_GVB%coeff(Nbasis,Nbasis))

!NEW
      NPAIR=NPAIRin+NSBond+NPBond+NTBond+NCBond

      allocate (CMOpairs(Nbasis,2))
      CMOpairs(1:Nbasis,1:2)=0
!
! Select the type of inital guess
      select case(GVB_guessMO)
      case ('LMO') ! Used the LMO to generate the Perfect-Pairing
        call GET_object ('QM','CMO','RHF')
        if(GVBPBS(1:4).eq.'BOND')then
          call GVB_LMO_LIST (CMOpairs)
          call GET_object ('QM','LMO','BOYS')
          call GVBLMO (CMOpairs)
        else
          NPair=NPAIRin ! GVBpairs given by user
          call GVB_LMO_LIST (CMOpairs)
          call GET_object ('QM','LMO','BOYS') ! Only Boys for now
        end if
        CMOG_GVB%coeff=LMO%coeff
      case ('CMO') ! Used the CMO to generate the Perfect-Pairing
        call GET_object ('QM','CMO','RHF')
        if(GVBPBS(1:4).eq.'BOND')then
          call GVBCMO (GVBpairs)
        else
          NPair=NPAIRin ! GVBpairs given by user
        end if
        CMOG_GVB%coeff=CMO%coeff
      case ('HMO') ! Used the HMO to generate the Perfect-Pairing
        write(UNIout,'(a)')'ERROR> BLD_guess_MO_GVB: HMO case not available'
        stop 'BLD_guess_MO_GVB> HMO case not available'
      case default
        write(UNIout,'(2a)')'ERROR> BLD_guess_MO_GVB: Illegal MO type ',GVB_guessMO
      end select
!
      allocate (IORDER(1:Nbasis))
      call BLD_VCC (IORDER, Nbasis)

      allocate (SMSQ(Nbasis,Nbasis))
      call GET_object ('INT', '1EINT', 'AO')
      call UPT_to_SQS (OVRLAP, MATlen, SMSQ, Nbasis)

      allocate (MAPOLD(Nbasis), MAPNEW(Nbasis))
      INEW=0
! Loop over groups.
      do I=1,5
! Set Map vectors to convert between old and new numbering schemes.
        do J=1,Nbasis
          if(IORDER(J).EQ.I)then
            INEW=INEW+1
            MAPNEW(J)=INEW
            MAPOLD(INEW)=J
          end if
        end do !J
      end do ! I
!
      call GVBCCI (MAPNEW, MAPOLD, Nbasis) ! get configurations

      call SCHMDT (CMOG_GVB%coeff, SMSQ, Nbasis)
      call SGVBMO (CMOG_GVB%coeff, MAPNEW, Nbasis)
!
      deallocate (IORDER, MAPOLD, MAPNEW, SMSQ)
!
! End of routine BLD_guess_MO_GVB
      call PRG_manager ('exit', 'BLD_guess_MO_GVB', 'UTILITY')
      return
      end subroutine BLD_guess_MO_GVB
      subroutine GVB_LMO_LIST (CMOpairs)
!***********************************************************************
!     Date last modified: February 13, 2008                Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: Generates a list of MO that need to be localized    *
!***********************************************************************
! Modules:
      USE program_files
      USE QM_objects
      USE module_GVB

      implicit none
!
      integer, dimension(:,:), allocatable :: SBondPAR
! Local scalars:
      integer I,IA,J
!
      integer CMOpairs(Nbasis,2)
!
! Begin:
      call PRG_manager ('enter', 'GVB_LMO_LIST', 'UTILITY')
!
      allocate (SBondPAR(Nbasis, 2))
      allocate (LMOLST(1:Nbasis))
! For this case save in the input values and ignore GVBSBD
      SBondPAR(1:Nbasis,1:2)=GVBSBD(1:Nbasis,1:2)
      GVBSBD(1:Nbasis,1:2)=0
!
      call GVBCMO (CMOpairs)
! Now reset GVBSBD
      GVBSBD(1:Nbasis,1:2)=SBondPAR(1:Nbasis,1:2)
!
      do I=1,CMO%NoccMO
        LMOLST(I)=1
        do J=1,NPAIR
          IA=CMOpairs(J,1)
          if(I.EQ.IA)then
            LMOLST(I)=IA
          end if
        end do !J
      end do !I

      do I=CMO%NoccMO+1,Nbasis
         LMOLST(I)=Nbasis
      end do !I
      do I=CMO%NoccMO+1,Nbasis
        do J=1,NPAIR
          IA=CMOpairs(J,2)
          if(I.EQ.IA)then
             LMOLST(I)=IA
          end if
        end do !J
      end do !I
!
      write(UNIout,'(A)')' SETS OF MOS TO BE LOCALIZED (for GVB):'
      write(UNIout,'(15(1X,I4))') (LMOLST(I),I=1,Nbasis)

      deallocate (SBondPAR)
!
! End of routine GVB_LMO_LIST
      call PRG_manager ('exit', 'GVB_LMO_LIST', 'UTILITY')
      return
      end subroutine GVB_LMO_LIST
      subroutine BLD_VCC (IORDER, NbasisX)
!***********************************************************************
!     Date last modified: December 9, 1993                             *
!     Author: R.A. Poirier and Y. Wang                                 *
!     Description: Read input data for the General SCF.                *
!***********************************************************************
! Modules:
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE GSCF_work
      USE module_GVB

      implicit none
!
! Input arrays:
      integer :: NbasisX
      integer :: IORDER(NbasisX)
!
! Local scalars:
      integer :: NoccMO
      integer LNGTHB,LNGTHO
!
! Work arrays:
      integer, dimension(:), allocatable :: IDGEN       !
      integer, dimension(:), allocatable :: IaoCC       !
      integer, dimension(:), allocatable :: IBOCC       !
      character*1, dimension(:), allocatable :: IGRPC   !
!
      integer, dimension(:), allocatable :: IDOCC
      integer, dimension(:), allocatable :: NUMA
      integer, dimension(:), allocatable :: NUMB
      integer, dimension(:), allocatable :: NUMC
      integer, dimension(:), allocatable :: NUMD
      integer, dimension(:), allocatable :: NUML
      double precision, dimension(:), allocatable :: ALPHCCw
      double precision, dimension(:), allocatable :: BETACCw
      double precision, dimension(:), allocatable :: OCCA  !
      double precision, dimension(:), allocatable :: OCCB  !
! OCCA,OCCB - Alpha and Beta open shell MO occupancies (NOPEN).
      double precision A(10,10),B(10,10)
!
! Local scalars:
      integer I,IA,IAB,IB,ICOL,ID,IDOUB,IG,INNA,INNB,IRHF,ISTRT,J,K,L,last,MAXOPN,MAXOP2,NLEVEL
      double precision T
      character*1 IGI
      character*4 IOPEN
!
! Local arrays:
      integer NA(4),NB(4),NMO(4)
      character*1 ICODE(5),IDELC(2),IDELD(2),LINE(72),IFILL(3)
!
      DATA IOPEN/'OPEN'/
      DATA ICODE/'F','D','S','A','V'/
!
! Begin:
      call PRG_manager ('enter', 'BLD_VCC', 'UTILITY')
!
      Nbasis=NbasisX
      call DEF_NoccMO (NoccMO, NAelectrons, NBelectrons)

      allocate (IGRPC(1:Nbasis), IaoCC(1:Nbasis), IBOCC(1:Nbasis), IDGEN(1:Nbasis))
!
      call GVB_group
!
      LNGTHB=(Nbasis+1)*(Nbasis+2)/2
!
      allocate (IDOCC(1:Nbasis), NUMA(1:Nbasis), &
                NUMB(1:Nbasis), NUMC(1:Nbasis), NUMD(1:Nbasis), NUML(1:Nbasis))
      if(.not.allocated(OCC))then
        allocate (OCC(1:Nbasis))
      else
        deallocate (OCC)
        allocate (OCC(1:Nbasis))
      end if

      if(.not.allocated(OCCA))then
        allocate (OCCA(1:Nbasis), OCCB(1:Nbasis))
      else
        deallocate (OCCA, OCCB)
        allocate (OCCA(1:Nbasis), OCCB(1:Nbasis))
      end if
!
! Initialization.
      MAXOPN=Nbasis
      MAXOP2=((MAXOPN+1)*(MAXOPN+2))/2

      allocate (ALPHCCw(1:MAXOP2), BETACCw(1:MAXOP2))

      ALPHCCw(1:MAXOP2)=ZERO
      BETACCw(1:MAXOP2)=ZERO
!
      OCC(1:Nbasis)=ZERO
      OCCA(1:Nbasis)=ZERO
      OCCB(1:Nbasis)=ZERO
      IDOCC(1:Nbasis)=0
!
! Count Alpha and Beta electrons.
      do I=1,4
        NA(I)=0
        NB(I)=0
        NMO(I)=0
      end do

      NOPEN=0
      NLEVEL=0
!
! Interpret Level data.
      K=0
      do I=1,Nbasis
         IGI=IGRPC(I)
! Assign group numbers 1='F', 2='D', 3='S', 4='A', 5='V'.
         IG=5
         if(IGI.NE.' ')then
            do IG=1,5
              if(IGI.EQ.ICODE(IG))GO TO 30
            end do !IG
            GO TO 900
         end if
   30    ID=IDGEN(I)
         IA=IaoCC(I)
         IB=IBOCC(I)
         IDOUB=IDOCC(I)
         if(IG.NE.5)then
           if(ID.LE.0)GO TO 930
           if(IA.GT.ID.or.IB.GT.ID)GO TO 920
           do J=1,ID
             K=K+1
             if(K.GT.Nbasis)GO TO 960
             IORDER(K)=IG
           end do !J
           NMO(IG)=NMO(IG)+ID
           if(IG.EQ.4)GO TO 70
!
! Group is 'F', 'D' or 'S'.
           if(IDOUB.NE.0)GO TO 975
           if(IG.EQ.3.and.IA.EQ.0.and.IB.EQ.0)GO TO 100
           if(IG.EQ.3)GO TO 970
           if(IA.EQ.ID.and.IB.EQ.ID)GO TO 100
           if(IA.NE.0.or.IB.NE.0)GO TO 970
           IA=ID
           IB=ID
           GO TO 100
         end if
!
! Group 'V'.
   50    if(ID.LE.0)ID=1
         IDGEN(I)=ID
         if(IA.GT.0.or.IB.GT.0)GO TO 910
         if(IDOUB.NE.0)GO TO 975
         do J=1,ID
            K=K+1
            if(K.GT.Nbasis)GO TO 120
            IORDER(K)=IG
         end do !J
         GO TO 110
!
! Group 'A'.
   70    if(IA.EQ.ID.and.IB.EQ.ID)GO TO 970
         NLEVEL=NLEVEL+1
         if(NLEVEL.GT.MAXOPN)GO TO 950
         if(IA.EQ.0.and.IB.EQ.0)then
           IA=ID
           IB=0
           IaoCC(I)=IA
           IBOCC(I)=IB
         end if
! Assign Level Occupancies.
         if(IDOUB.GT.MIN0(IA,IB))GO TO 975
! If IDOCC(I) was 0, calculate the required value.
         if(IDOUB.EQ.0.and.IA+IB.GT.ID)IDOUB=IA+IB-ID
         NUML(NLEVEL)=ID
         NUMD(NLEVEL)=IDOUB
         NUMA(NLEVEL)=IA-IDOUB
         NUMB(NLEVEL)=IB-IDOUB
         NOPEN=NOPEN+ID
         if(NOPEN.GT.MAXOPN)GO TO 950
  100    NA(IG)=NA(IG)+IA
         NB(IG)=NB(IG)+IB
  110    continue
      end do !I
  120 I=Nbasis-NMO(1)-NMO(2)-NMO(3)-NMO(4)
      write(UNIout,1020)ICODE,NMO,I
!
! Get Coulomb and Exchange coupling program_constants.
      INNA=0
      INNB=0
      IAB=1
      ALPHCCw(1)=TWO
      BETACCw(1)=ONE
!
! Calculate the coupling program_constants.
      if(NLEVEL.GT.0)call AKLBKL (NUML, NUMD, NUMA, NUMB, NUMC, NLEVEL, ALPHCCw, BETACCw, MAXOP2)
!
      NOPEN=0
!
! Loop over groups.
      do I=1,5
!
      if(I.EQ.5)GO TO 390
      INNA=INNA+NA(I)
      INNB=INNB+NB(I)
      if(I.NE.4)GO TO 390
!
! Open Shell MO.
      if(NLEVEL.EQ.0)GO TO 390
      do J=1,NLEVEL
      ID=NUML(J)
      IA=NUMA(J)+NUMD(J)
      IB=NUMB(J)+NUMD(J)
      T=DBLE(ID)
      do K=1,ID
      NOPEN=NOPEN+1
      OCCA(NOPEN)=DBLE(IA)/T
      OCCB(NOPEN)=DBLE(IB)/T
      OCC(NOPEN)=PT5*DBLE(IA+IB)/T
! Get coupling coefficients into ALPHCCw and BETACCw - First Closed-Open.
      IAB=IAB+1
      ALPHCCw(IAB)=OCC(NOPEN)+OCC(NOPEN)
      BETACCw(IAB)=OCC(NOPEN)
      do L=1,NOPEN
!
      end do ! L
      end do ! K
      end do ! J
  390 continue
      end do ! I
!
      NCORE=NMO(1)
      NVAL=NMO(2)+NMO(3)
      NDOUBL=NMO(2)
      CMOG_GVB%NoccMO=NCORE+NVAL+NOPEN
      CMOG_GVB%NFcoreMO=NCORE
      IRHF=0
      NCONF=0
!
      if(INNA.NE.NAelectrons.or.INNB.NE.NBelectrons)GO TO 890

      IRHF=2

      LNGTHO=(NOPEN+1)*(NOPEN+2)/2
      if(.not.allocated(ALPHCC))then
        allocate (ALPHCC(1:LNGTHO), BETACC(1:LNGTHO))
      else
        deallocate (ALPHCC, BETACC)
        allocate (ALPHCC(1:LNGTHO), BETACC(1:LNGTHO))
      end if

      ALPHCC(1:LNGTHO)=ALPHCCw(1:LNGTHO)
      BETACC(1:LNGTHO)=BETACCw(1:LNGTHO)
!
      write(UNIout,1070)(IGRPC(I),I=1,Nbasis)
      write(UNIout,1080)(IDGEN(I),I=1,Nbasis)
      write(UNIout,1090)(IaoCC(I),I=1,Nbasis)
      write(UNIout,1140)(IBOCC(I),I=1,Nbasis)
!
! Print Coupling Coefficients.
      IAB=0
      K=NOPEN+1
      do J=1,K
      do I=1,J
      IAB=IAB+1
      A(I,J)=ALPHCC(IAB)
      A(J,I)=ALPHCC(IAB)
      B(I,J)=BETACC(IAB)
      B(J,I)=BETACC(IAB)
      end do ! I
      end do ! J
      T=DBLE(NCORE+NDOUBL)
      if(NOPEN.EQ.0)write(UNIout,1150)
      if(NOPEN.GT.0)write(UNIout,1150)(IOPEN,I,I=1,NOPEN)
      write(UNIout,1200)T,T,(A(J,1),J=1,K)
      if(NOPEN.NE.0)then
        do I=1,NOPEN
        write(UNIout,1220)I,OCCA(I),OCCB(I),(A(J,I+1),J=1,K)
        end do
      end if
      if(NOPEN.EQ.0)write(UNIout,1230)
      if(NOPEN.GT.0)write(UNIout,1230)(IOPEN,I,I=1,NOPEN)
      write(UNIout,1200)T,T,(B(J,1),J=1,K)
      if(NOPEN.NE.0)then
        do I=1,NOPEN
        write(UNIout,1220)I,OCCA(I),OCCB(I),(B(J,I+1),J=1,K)
        end do
      end if
!
      deallocate (IDOCC, NUMA, NUMB, NUMC, NUMD, NUML, ALPHCCw, BETACCw)
!
! End of routine BLD_VCC
      call PRG_manager ('exit', 'BLD_VCC', 'UTILITY')
      return
!
! No D or S MO for MC-SCF.
  870 write(UNIout,'(a)')'ERROR> BLD_VCC: NO ORBITALS IN EITHER GROUP D OR S FOR MC-SCF'
      stop
! Occupancy error.
  890 write(UNIout,'(2A)')'ERROR> BLD_VCC: INCORRECT NUMBER OF ALPHA AND BETA ELECTRONS OR INCORRECT MULTIPLICITY'
      stop
! Illegal group code.
  900 write(UNIout,'(A)')'ERROR> BLD_VCC: ILLEGAL GROUP CODE'
      GO TO 940
! Level V contains some electrons.
  910 write(UNIout,'(A)')'ERROR> BLD_VCC: GROUP V LEVELS MUST NOT CONTAIN ANY ELECTRONS'
      GO TO 940
! Too many electrons for the degeneracy.
  920 write(UNIout,'(A)')'ERROR> BLD_VCC: TOO MANY ELECTRONS FOR SPECIFIED DEGENERACY'
      GO TO 940
! Illegal degeneracy.
  930 write(UNIout,'(A)')'ERROR> BLD_VCC: ILLEGAL DEGENERACY'
      GO TO 940
! Too many Open Shells.
  950 write(UNIout,'(A)')'ERROR> BLD_VCC: TOO MANY OPEN SHELL MO'
      GO TO 940
! Too many MO.
  960 write(UNIout,'(A)')'ERROR> BLD_VCC: NUMBER OF MO IS GREATER THAN Nbasis'
      GO TO 940
! Illegal number of electrons in a Level.
  970 write(UNIout,'(A)')'ERROR> BLD_VCC: ILLEGAL NUMBER OF ELECTRONS IN A LEVEL'
      GO TO 940
! Illegal value for IDOCC.
  975 write(UNIout,'(A)')'ERROR> BLD_VCC: ILLEGAL NUMBER OF DOUBLY OCCUPIED ORBITALS'
  940 write(UNIout,'(16X,A,I3)')'ERROR> BLD_VCC: OCCURED IN COLUMN ',I
      stop
!
 1020 FORMAT(/'ORBITAL GROUP:   ',5(4X,A4)/'NUMBER OF MO:    ',5I8)
 1070 FORMAT('RHF OPEN SHELL SPECIFICATION'/'GROUP: ',100A1/(7X,100A1))
 1080 FORMAT('DEGENERACY: ',100I1/(13X,100I1))
 1090 FORMAT('ALPHA OCC : ',100I1/(13X,100I1))
 1140 FORMAT('BETA OCC  : ',100I1/(13X,100I1))
 1150 FORMAT(/'COULOMB COUPLING COEFFICIENTS   A(I,J)'/'0  MO',10X,'OCCA',6X,'OCCB',9X,'CLOSED',9(3X,A4,I2))
 1200 FORMAT('CLOSED',3X,2F10.4,5X,10F9.4)
 1220 FORMAT('OPEN',I2,3X,2F10.4,5X,10F9.4)
 1230 FORMAT(/'EXCHANGE COUPLING COEFFICIENTS   B(I,J)'/'0  MO',10X,'OCCA',6X,'OCCB',9X,'CLOSED',9(3X,A4,I2))
!
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine GVB_group
!***********************************************************************
!     Date last modified: May 15, 1995                     Version 1.0 *
!     Author: Y. Wang  and R.A. Poirier                                *
!     Description: Prepare data for the General SCF.                   *
!***********************************************************************

      implicit none
!
! Local scalars:
      integer I,NP1,NP2,II,J,MULTIPw,NOCCD,NOCCT
!
! Begin:
      call PRG_manager ('enter', 'GVB_group', 'UTILITY')
!
      MULTIPw=Multiplicity
!
      if(MULTIPw.EQ.1)then
        NOCCD=NAelectrons
        NOCCT=NOCCD
      else
        NOCCD=NBelectrons
        NOCCT=NAelectrons
      end if
!
      do I=1,Nbasis
      if(I.LE.NOCCD)then
        IGRPC(I)='F'
        IDGEN(I)=1
      else if (I.LE.NOCCT.and.NOCCT.GT.NOCCD)then
        IGRPC(I)='A'
        IDGEN(I)=1
      else
        IGRPC(I)='V'
        IDGEN(I)=0
      end if
      end do
!
      IaoCC(1:Nbasis)=0
      IBOCC(1:Nbasis)=0
!
      IaoCC(1:NAelectrons)=1
      IBOCC(1:NBelectrons)=1
!
      do I=1,NPAIR
        NP1=GVBpairs(I,1)
        NP2=GVBpairs(I,2)
        IGRPC(NP1)='D'
        IDGEN(NP1)=1
        IaoCC(NP1)=1
        IBOCC(NP1)=1
        IGRPC(NP2)='S'
        IDGEN(NP2)=1
        IaoCC(NP2)=0
        IBOCC(NP2)=0
      end do
!
      MULTIPw=MULTIPw-1
      if(MULTIPw.NE.0)then
        II=1
! Need more work!
        do J=NAelectrons,1,-1
        if(IGRPC(J).NE.'D')then
          IGRPC(J)='A'
          IDGEN(J)=1
          IaoCC(J)=1
          IBOCC(J)=0
          II=II+1
          if(II.GT.MULTIPw)exit
        end if
        end do ! J
      end if

! need to check for 'D' or 'S' and compute NDOUBL and NOPEN
!      if(NMO(2).EQ.0.or.NMO(3).EQ.0)GO TO 870
! No D or S MO for MC-SCF.
!  870 write(UNIout,'(A)')'ERROR> GVB_group: NO ORBITALS IN EITHER GROUP D OR S FOR MC-SCF'
!      stop 'ERROR> NO ORBITALS IN EITHER GROUP D OR S FOR MC-SCF'
!
! End of routine GVB_group
      call PRG_manager ('exit', 'GVB_group', 'UTILITY')
      return
      end subroutine GVB_group
! end contains
      end subroutine BLD_VCC
      subroutine GVBCCI (MAPNEW, MAPOLD, Nbasis)
!***********************************************************************
!     Date last modified: May 15, 1995                     Version 1.0 *
!     Author: R.A. Poirier and M.R. Peterson                           *
!***********************************************************************
! Modules:
      USE QM_defaults
      USE GSCF_work
      USE module_GVB

      implicit none
!
! Input arrays:
      integer :: Nbasis
      integer :: MAPNEW(Nbasis)
      integer :: MAPOLD(Nbasis)
!
! Local scalars:
      integer LNGTHB,LNGTHO
!
! Work arrays:
      integer, dimension(:), allocatable :: IGRP
      integer, dimension(:), allocatable :: KMAP
      integer, dimension(:), allocatable :: LMAP
!
! Local scalars:
      integer I,IA,IAB,IB,ICOL,ID,IDOUB,IERR,IG,INEW,ISTRT,J,K,L,last,MAXOPN,NLEVEL
      double precision T
      character*1 IGI,ISEP
      character(len=4) ::  MCSCF
!
! Local arrays:
      integer NB(4)
      character*1 ICODE(5),IDELC(2),IDELD(2),LINE(72),IFILL(3)
      character(len=4) ::  MCODE(4)
!
      DATA IDELC/',',','/,IDELD/'-','-'/,ICODE/'F','D','S','A','V'/,MCODE/'PAIR','INDP','GVB ','COMP'/
!
! Data required by the RHF SCF procedure :
! IRHF - Method code: 0 for pair type MC-SCF,
!                     1 for independent pair MC-SCF,
!                     2 for generalized valence bond,
!                     3 for complete MC-SCF.
! NCONF - Number of pair excited configurations.
! NCORE - Number of doubly occupied frozen core orbitals.
! NVAL  - Number of closed shell orbitals available for
!         pair substitutions (groups 'D' and 'S').
! NOPEN - Number of open shell MO.
! OCC   - Total open shell MO fractional occupancies (NOPEN).
! ALPHCC,BETACC  - Coulomb and Exchange program_constants (LNGTHO/2).
!
! NMO, NA and NB count MO, Alpha and Beta electrons per group,
! except for group 'V' (Dimension: 4).
!
! NUMC - Total number of configurations for Level I (NOPEN).
! NUML - Degeneracy of Level I (NOPEN).
! NUMD - Number of doubly occupied MO in Level I (NOPEN).
! NUMA - Number of Alpha Spin singly occupied MO in Level I (NOPEN).
! NUMB - Number of Beta Spin singly occupied MO in Level I (NOPEN).
! A,B  - Printing arrays (NOPEN+1,NOPEN+1).
! IFILL - Filler array for Free Format input (3).
! I1,J1,K1,L1 - Input arrays for coupling program_constants (NOPEN*(NOPEN+1)/2).
!
! Note: For GUESS=NONE/REstart, all data read in as usual and ignored.
!
!
! Begin:
      call PRG_manager ('enter', 'GVBCCI', 'UTILITY')
!
      if(.not.allocated(ICONF))then
        allocate (ICONF(1:Nbasis))
      else
        deallocate (ICONF)
        allocate (ICONF(1:Nbasis))
      end if

      ICONF(1:Nbasis)=0
!
      MCSCF='GVB'
      if(NPAIR.LE.0)then
         write(UNIout,'(A,A4,A,I4)')'ERROR> GVBCCI: ILLEGAL MC-SCF CODE: ',MCSCF,' OR VALUE FOR NPAIR:',NPAIR
         stop 'GVBCCI> NPAIR.LE.0'
      end if
!
! Create stack arrays of maximum required dimension.
! Length should be LNGTHO/2, but use Nbasis for now:
      LNGTHB=(Nbasis+1)*(Nbasis+2)/2
!
      allocate (IGRP(Nbasis), KMAP(Nbasis), LMAP(Nbasis))
!
! Initialization.
      MAXOPN=Nbasis

      KMAP(1:Nbasis)=0
      LMAP(1:Nbasis)=0
!
! Keep track of MO used by array IGRP.
  600 IGRP(1:Nbasis)=0
!
      NCONF=0
      do I=1,NPAIR
      L=GVBpairs(I,1)
      K=GVBpairs(I,2)

! Pack this configuration away if it is legal.
      NCONF=NCONF+1
      if(NCONF.GT.Nbasis*Nbasis)GO TO 985
!
! Ensure that L and K belong to groups D and S respectively.
      if(K.GT.0.and.K.LE.Nbasis)KMAP(NCONF)=MAPNEW(K)
      if(L.GT.0.and.L.LE.Nbasis)LMAP(NCONF)=MAPNEW(L)
      if(LMAP(NCONF).LE.NCORE.or.LMAP(NCONF).GT.NCORE+NDOUBL)then
        write(UNIout,1310)L,ICODE(2),MCSCF
      end if
      if(KMAP(NCONF).LE.NCORE+NDOUBL.or.KMAP(NCONF).GT.NCORE+NVAL)then
        write(UNIout,1310)K,ICODE(3),MCSCF
      end if
!
! Omit the rest of the checking if K or L was invalid.
      if(KMAP(NCONF).NE.0.and.LMAP(NCONF).NE.0)then
         ICONF(NCONF)=1000*KMAP(NCONF)+LMAP(NCONF)
         IGRP(KMAP(NCONF))=IGRP(KMAP(NCONF))+1
         IGRP(LMAP(NCONF))=IGRP(LMAP(NCONF))+1
         if(IGRP(KMAP(NCONF)).NE.1)then
            write(UNIout,1290)K
         end if
         if(IGRP(LMAP(NCONF)).NE.1)then
            write(UNIout,1290)L
         end if
      end if
!      NPAIR=NPAIR-1
      end do
!
! Ensure that no MO was omitted.
      do I=1,NVAL
         K=NCORE+I
         if(IGRP(K).EQ.0)then
            write(UNIout,1300)MAPOLD(K),MCSCF
         end if
      end do
  705 K=MIN0(Nbasis,25)
      L=K+1
      write(UNIout,1360)(I,I=1,K)
      if(Nbasis.GT.K)write(UNIout,1420)(I,I=L,Nbasis)
      write(UNIout,1430)(MAPNEW(I),I=1,K)
      if(Nbasis.GT.K)write(UNIout,1420)(MAPNEW(I),I=L,Nbasis)
!
! Check for an Open Shell singlet.
  800 continue
!      if(Multiplicity.EQ.1.and.NOPEN.GT.0)call putscB ('SCF_SCB_OPEN_SHELL_SINGLET', .TRUE.)
!
      deallocate (IGRP, KMAP, LMAP)
!
      call PRG_manager ('exit', 'GVBCCI', 'UTILITY')
      return
!
! Illegal group code.
  900 write(UNIout,'(A)')'ERROR> GVBCCI: ILLEGAL GROUP CODE'
      GO TO 940
! Level V contains some electrons.
  910 write(UNIout,'(A)')'ERROR> GVBCCI: GROUP V LEVELS MUST NOT CONTAIN ANY ELECTRONS'
      GO TO 940
! Too many electrons for the degeneracy.
  920 write(UNIout,'(A)')'ERROR> GVBCCI: TOO MANY ELECTRONS FOR SPECIFIED DEGENERACY'
      GO TO 940
! Illegal degeneracy.
  930 write(UNIout,'(A)')'ERROR> GVBCCI: ILLEGAL DEGENERACY'
      stop
  940 write(UNIout,'(16X,A,I3)')'ERROR> GVBCCI: OCCURED IN COLUMN ',I
      stop
! Too many Open Shells.
  950 write(UNIout,'(A)')'ERROR> GVBCCI: TOO MANY OPEN SHELL MO'
      GO TO 940
! Too many MO.
  960 write(UNIout,'(A)')'ERROR> GVBCCI: NUMBER OF MO IS GREATER THAN Nbasis'
      GO TO 940
! Illegal number of electrons in a Level.
  970 write(UNIout,'(A)')'ERROR> GVBCCI: ILLEGAL NUMBER OF ELECTRONS IN A LEVEL'
      GO TO 940
! Illegal value for IDOCC.
  975 write(UNIout,'(A)')'ERROR> GVBCCI: ILLEGAL NUMBER OF DOUBLY OCCUPIED ORBITALS'
      GO TO 940
! Too many configurations.
  985 write(UNIout,'(A,I4)')'ERROR> GVBCCI: TOO MANY MCSCF/GVB CONFIGURATIONS - LIMIT IS',Nbasis*Nbasis
      stop
!
 1000 FORMAT(72I1)
 1010 FORMAT(36I2)
 1020 FORMAT(/'ORBITAL GROUP :   ',5(4X,A4)/'0NUMBER OF MO  :',5I8)
 1060 FORMAT(1X,72A1)
 1110 FORMAT(/'MC-SCF type: ',A4,5X,'READ',I4,' CONFIGURATIONS'/)
 1160 FORMAT(72A1)
 1240 FORMAT(A4,I4)
 1290 FORMAT('MO',I4,' USED TWICE IN GVB')
 1300 FORMAT('MO',I4,' NOT USED IN ',A4,' MC-SCF')
 1310 FORMAT('MO',I4,' DOES NOT BELONG TO GROUP ',A1,' FOR ',A4,' MC-SCF')
 1360 FORMAT(/'NOTE: THE CONFIGURATIONS ARE SPECIFIED IN TERMS OF THE MO NUMBERS BEFORE THE MO ARE SORTED BY GROUPS.'/ &
       'MAP BETWEEN INITIAL MO NUMBERS AND RE-ORDERED MO NUMBERS:'/'INITIAL MO: ',25I4)
 1390 FORMAT('DOUBLE OCC: ',100I1/(13X,100I1))
 1410 FORMAT('RHF OPEN SHELL SPECIFICATION DATA RECOVERED FROM DISK')
 1420 FORMAT(13X,25I4)
 1430 FORMAT('ORDERED MO: ',25I4)
!
! End of routine GVBCCI
      call PRG_manager ('exit', 'GVBCCI', 'UTILITY')
      return
      end subroutine GVBCCI
      subroutine SGVBMO (CMOG, &
                         MAPNEW, &
                         Nbasis)
!***********************************************************************
!     Date last modified: May 15, 1995                     Version 1.0 *
!     Author: Y. Wang and R.A. Poirier                                 *
!     Description: Get guess MO coefficients for GVB calc.             *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalar:
      integer Nbasis
!
! Input arrays:
      double precision CMOG(Nbasis,Nbasis)
      integer MAPNEW(Nbasis)
!
! Work array:
      double precision, dimension(:,:), allocatable :: CMOW
!
! Local scalar:
      integer I,J,IMO
!
! Begin:
      call PRG_manager ('enter', 'SGVBMO', 'UTILITY')
!
      allocate (CMOW(Nbasis,Nbasis))
!
      do I=1,Nbasis
      IMO=MAPNEW(I)
      if(IMO.NE.I)then
        do J=1,Nbasis
        CMOW(J,IMO)=CMOG(J,I)
        end do
      end if
      end do
!
      do I=1,Nbasis
      IMO=MAPNEW(I)
      if(IMO.NE.I)then
        do J=1,Nbasis
        CMOG(J,I)=CMOW(J,I)
        end do
      end if
      end do
!
      deallocate (CMOW)
!
! End of routine SGVBMO
      call PRG_manager ('exit', 'SGVBMO', 'UTILITY')
      return
      end subroutine SGVBMO
      subroutine GVBCMO (MOpairs)   ! GVBpairs or CMOpairs
!***********************************************************************
!     Date last modified: May 15, 1995                     Version 1.0 *
!     Author: Y. Wang and R.A. Poirier                                 *
!     Description:                                                     *
!***********************************************************************
! Modules:
      USE QM_defaults
      USE QM_objects
      USE matrix_print
      USE module_GVB
      USE type_epairs

      implicit none
!
      integer MOpairs(Nbasis,2)
!
! Work arrays:
      type (electron_pairs), dimension(:), allocatable :: EPairs
      integer, dimension(:), allocatable :: PIBOND
      integer, dimension(:), allocatable :: TBOND
!
! Local scalars:
      integer :: Ibasis,IMO,Ipair,Ivirt
      integer IA,IB,IC,JA,JB,KA,KB,I,I1,I2,JJ,K,KC,NT,NPI,Nbasis1,GRPSET,JJA,JJB,K1,NcoreMO
      double precision SUM,SUM1,SUM2,SUM3
!
! Local parameters:
      double precision TENM5
      parameter (TENM5=1.0D-5)
!
! Begin:
      call PRG_manager ('enter', 'GVBCMO', 'UTILITY')
!
      allocate (PIBOND(Nbasis), TBOND(Nbasis))
!
      NT=0
      PIBOND(1:Nbasis)=0
      TBOND(1:Nbasis)=0
!
! Check three-center bond required
      KC=0
      do Ibasis=1,Nbasis
        if(GVBCBD(Ibasis,1).ne.0)KC=GVBCBD(Ibasis,1)
      end do
!
      allocate (EPairs(Nbasis))
      call BLD_MO_IDmatrix (CMO%coeff, EPairs, Natoms, Nbasis, CMO%NoccMO)
!
      if(GVB_guessMO(1:3).NE.'CMO')then
        do I=1,Nbasis
          KA=GVBPBD(I,1)
          KB=GVBPBD(I,2)
          if(KA.NE.0.and.KB.NE.0)NPAIR=NPAIR+1
          KA=GVBTBD(I,1)
          KB=GVBTBD(I,2)
          if(KA.NE.0.and.KB.NE.0)NPAIR=NPAIR+1
          KA=GVBCBD(I,1)
          KB=GVBCBD(I,2)
          if(KA.NE.0.and.KB.NE.0)NPAIR=NPAIR+1
        end do
        if(NPAIR.GE.1)NPAIR=Nbasis
      end if
!
      NcoreMO=NFrozen_cores()
      if(NPAIR.EQ.0)then
        call GVB_BLD_pairs (EPairs, NcoreMO)
      end if
!
! Look from triple bonds? ! For occupied
      if(NTBond.gt.0)then
      NT=1
      do Ipair=1,NPAIR
        KA=GVBTBD(Ipair,1)
        KB=GVBTBD(Ipair,2)
        if(KA.eq.0.or.KB.eq.0)cycle
!       if(KA.NE.0.and.KB.NE.0)then
        do I=CMO%NoccMO,1,-1
          do K1=1,Nbasis
            I2=OPNLST(K1) ! Skip open-shells
            if(I.NE.I2)then
              I2=MOpairs(K1,1)
            end if
            if(I.EQ.I2) GO TO 101
          end do ! K1
          IA=EPairs(I)%AtomA
          IB=EPairs(I)%AtomB
          if((KA.EQ.IA.and.KB.EQ.IB).or.(KA.EQ.IB.and.KB.EQ.IA))then
            MOpairs(Ipair,1)=I
            TBOND(NT)=I
            NT=NT+1
!           GOTO 100
            exit
          end if
 101      continue
        end do ! I
!       end if
 100    continue
      end do ! Ipair
      end if ! NTBond
!
! Look from double bonds?
      NPI=1
      do Ipair=1,NPAIR
        KA=GVBPBD(Ipair,1)
        KB=GVBPBD(Ipair,2)
        if(KA.NE.0.and.KB.NE.0)then
        do I=CMO%NoccMO,1,-1
          do K1=1,Nbasis
            I2=OPNLST(K1) ! Skip open-shells
            if(I.NE.I2)then
              I2=MOpairs(K1,1)
            end if
            if(I.EQ.I2) GO TO 111
          end do
          IA=EPairs(I)%AtomA
          IB=EPairs(I)%AtomB
          if((KA.EQ.IA.and.KB.EQ.IB).or.(KA.EQ.IB.and.KB.EQ.IA))then
            do JJ=1,NT-1
              if(I.EQ.TBOND(JJ)) GOTO 111
            end do
            MOpairs(Ipair,1)=I
            PIBOND(NPI)=I
            NPI=NPI+1
            GOTO 110
          end if
 111      continue
        end do !I
        end if
 110    continue
      end do !Ipair
!
      do Ipair=1,NPAIR
        KA=GVBSBD(Ipair,1)
        KB=GVBSBD(Ipair,2)
        if(KA.NE.0.and.KB.NE.0)then
        do I=NcoreMO+1,CMO%NoccMO
          do K1=1,Nbasis
            I2=OPNLST(K1) ! Skip open-shells
            if(I.NE.I2)then
              I2=MOpairs(K1,1)
            end if
            if(I.EQ.I2) GO TO 301
          end do
          IA=EPairs(I)%AtomA
          IB=EPairs(I)%AtomB
          if((KA.EQ.IA.and.KB.EQ.IB).or.(KA.EQ.IB.and.KB.EQ.IA))then
            do JJ=1,NT-1
              if(I.EQ.TBOND(JJ)) GOTO 301
            end do
            do JJ=1,NPI-1
              if(I.EQ.PIBOND(JJ)) GOTO 301
            end do
            MOpairs(Ipair,1)=I
            GO TO 300
          end if
 301      continue
        end do !I
        end if
 300    continue
      end do !Ipair

! Look from triple bonds?
!For virtual
      NT=1
      do Ipair=1,NPAIR
        KA=GVBTBD(Ipair,1)
        KB=GVBTBD(Ipair,2)
        if(KA.NE.0.and.KB.NE.0)then
        SUM3=ZERO
        SUM=ZERO
        IMO=MOpairs(Ipair,1)
        do Ivirt=CMO%NoccMO+1,Nbasis
          do K1=1,Nbasis
            I2=OPNLST(K1) ! Skip open-shells
            if(Ivirt.NE.I2)then
              I2=MOpairs(K1,2)
            end if
            if(Ivirt.EQ.I2) GO TO 201
          end do ! K1
          IA=EPairs(Ivirt)%AtomA
          IB=EPairs(Ivirt)%AtomB
          if((KA.EQ.IA.and.KB.EQ.IB).or.(KA.EQ.IB.and.KB.EQ.IA))then
            call GVBPPM(SUM1, SUM2, Ivirt, IMO, KA, KB, CMO%coeff, Natoms, Nbasis)
            if(SUM2.GT.SUM3)then
                SUM3=SUM2
                SUM=SUM1/10.0D00+0.10D00
                MOpairs(Ipair,2)=Ivirt
                TBOND(NT)=Ivirt
            end if
          end if ! KA.eq.IA
 201      continue
        end do ! Ipair
        NT=NT+1
        end if
 200    continue
      end do ! Ivirt
!
! Look from double bonds?
      NPI=1
      do Ipair=1,NPAIR
        KA=GVBPBD(Ipair,1)
        KB=GVBPBD(Ipair,2)
        if(KA.NE.0.and.KB.NE.0)then
        I1=MOpairs(Ipair,1)
        SUM=ZERO
        SUM3=ZERO
        do I=CMO%NoccMO+1,Nbasis
          do K1=1,Nbasis
            I2=OPNLST(K1) ! Skip open-shells
            if(I.NE.I2)then
              I2=MOpairs(K1,2)
            end if
            if(I.EQ.I2)GO TO 211
          end do
          IA=EPairs(I)%AtomA
          IB=EPairs(I)%AtomB
          if((KA.EQ.IA.and.KB.EQ.IB).or.(KA.EQ.IB.and.KB.EQ.IA))then
            do JJ=1,NT-1
              if(I.EQ.TBOND(JJ)) GOTO 211
            end do
            call GVBPPM(SUM1, SUM2, I, I1, KA, KB, CMO%coeff, Natoms, Nbasis)
            if(SUM2.GT.SUM3)then
                SUM3=SUM2
                SUM=SUM1/10.0D00+0.10D00
                MOpairs(Ipair,2)=I
                PIBOND(NPI)=I
            end if
          end if
 211      continue
        end do ! I
        NPI=NPI+1
        end if
 210    continue
      end do !Ipair
!
      do Ipair=1,NPAIR
        KA=GVBSBD(Ipair,1)
        KB=GVBSBD(Ipair,2)
        if(KA.NE.0.and.KB.NE.0)then
        I1=MOpairs(Ipair,1)
        SUM3=ZERO
        SUM=ZERO
        do I=CMO%NoccMO+1, Nbasis
          do K1=1,Nbasis
            I2=OPNLST(K1) ! Skip open-shells
            if(I.NE.I2)then
              I2=MOpairs(K1,2)
            end if
            if(I.EQ.I2) GO TO 401
          end do
          IA=EPairs(I)%AtomA
          IB=EPairs(I)%AtomB
          if((KA.EQ.IA.and.KB.EQ.IB).or.(KA.EQ.IB.and.KB.EQ.IA))then
            do JJ=1,NPI-1
              if(I.EQ.PIBOND(JJ)) GOTO 401
            end do
            do JJ=1,NT-1
              if(I.EQ.TBOND(JJ)) GOTO 401
            end do
            call GVBPPM(SUM1, SUM2, I, I1, KA, KB, CMO%coeff, Natoms, Nbasis)
            if(SUM2.GT.SUM3)then
                SUM3=SUM2
                SUM=SUM1/10.0D00+0.10D00
                MOpairs(Ipair,2)=I
            end if
          end if
 401      continue
        end do ! I
        end if
 400    continue
      end do ! Ipair
!
! For occupied
      if(KC.NE.0)then
        do Ipair=1,NPAIR
          KA=GVBCBD(Ipair,1)
          KB=GVBCBD(Ipair,2)
          KC=GVBCBD(Ipair,3)
          if(KA.NE.0.and.KB.NE.0)then
          do I=CMO%NoccMO,1,-1
            do K1=1,Nbasis
              I2=OPNLST(K1) ! Skip open-shells
              if(I.NE.I2)then
                I2=MOpairs(K1,1)
              end if
              if(I.EQ.I2) GO TO 8801
            end do
            IA=EPairs(I)%AtomA
            IB=EPairs(I)%AtomB
            IC=EPairs(I)%AtomC
            if((IA.EQ.KA.and.((IB.EQ.KB.and.IC.EQ.KC).or.(IB.EQ.KC.and.IC.EQ.KB))).or. &
               (IA.EQ.KB.and.((IB.EQ.KA.and.IC.EQ.KC).or.(IB.EQ.KC.and.IC.EQ.KA))).or. &
               (IA.EQ.KC.and.((IB.EQ.KA.and.IC.EQ.KB).or.(IB.EQ.KB.and.IC.EQ.KA))))then
              MOpairs(Ipair,1)=I
              GOTO 8800
            end if
8801        continue
          end do !I
          end if
8800      continue
        end do ! Ipair
!
! For virtual
        do Ipair=1,NPAIR
          KA=GVBCBD(Ipair,1)
          KB=GVBCBD(Ipair,2)
          KC=GVBCBD(Ipair,3)
          if(KA.NE.0.and.KB.NE.0)then
          SUM1=ZERO
          I1=MOpairs(Ipair,1)
          if(I1.EQ.0) GO TO 8810
          do I=CMO%NoccMO+1, Nbasis
            do K1=1,Nbasis
              I2=OPNLST(K1) ! Skip open-shells
              if(I.NE.I2)then
                I2=MOpairs(K1,2)
              end if
              if(I.EQ.I2) GO TO 8811
            end do
            SUM=ZERO
            IA=EPairs(I)%AtomA
            IB=EPairs(I)%AtomB
            IC=EPairs(I)%AtomC
            if((IA.EQ.KA.and.((IB.EQ.KB.and.IC.EQ.KC).or.(IB.EQ.KC.and.IC.EQ.KB))).or. &
               (IA.EQ.KB.and.((IB.EQ.KA.and.IC.EQ.KC).or.(IB.EQ.KC.and.IC.EQ.KA))).or. &
               (IA.EQ.KC.and.((IB.EQ.KA.and.IC.EQ.KB).or.(IB.EQ.KB.and.IC.EQ.KA))))then
              do K=1,Nbasis
                SUM=SUM+DABS(CMO%coeff(K,I1)*CMO%coeff(K,I))
              end do
              if(SUM.GT.SUM1)then
                SUM1=SUM
                MOpairs(Ipair,2)=I
              end if
            end if
8811        continue
          end do !I
          end if
8810      continue
        end do !Ipair
      end if
!
      K=0
      do I=1,NPAIR
        if(MOpairs(I,1).EQ.0.or.MOpairs(I,2).EQ.0)then
          IA=GVBSBD(I,1)
          IB=GVBSBD(I,2)
          if(IA.NE.0)write(UNIout,5101) i,CARTESIAN(IA)%element,CARTESIAN(IB)%element
          if(IA.EQ.0)then
            IA=GVBPBD(I,1)
            IB=GVBPBD(I,2)
            if(IA.NE.0)write(UNIout,5102) i,CARTESIAN(IA)%element,CARTESIAN(IB)%element
            if(IA.EQ.0)then
              IA=GVBTBD(I,1)
              IB=GVBTBD(I,2)
              if(IA.NE.0)write(UNIout,5103) i,CARTESIAN(IA)%element,CARTESIAN(IB)%element
              if(IA.EQ.0)then
                IA=GVBCBD(I,1)
                IB=GVBCBD(I,2)
                if(IA.NE.0)write(UNIout,5103) i,CARTESIAN(IA)%element,CARTESIAN(IB)%element
              end if
            end if
          end if
        else
          K=K+1
          if(K.LT.I)then
            MOpairs(K,1)=MOpairs(I,1)
            MOpairs(K,2)=MOpairs(I,2)
            GVBSBD(K,1)=GVBSBD(I,1)
            GVBSBD(K,2)=GVBSBD(I,2)
            GVBPBD(K,1)=GVBPBD(I,1)
            GVBPBD(K,2)=GVBPBD(I,2)
            GVBTBD(K,1)=GVBTBD(I,1)
            GVBTBD(K,2)=GVBTBD(I,2)
            GVBCBD(K,1)=GVBCBD(I,1)
            GVBCBD(K,2)=GVBCBD(I,2)
            GVBCBD(K,3)=GVBCBD(I,3)
            GVBSBD(I,1)=0
            GVBSBD(I,2)=0
            GVBPBD(I,1)=0
            GVBPBD(I,2)=0
            GVBTBD(I,1)=0
            GVBTBD(I,2)=0
            GVBCBD(I,1)=0
            GVBCBD(I,2)=0
            GVBCBD(I,3)=0
          end if
        end if
      end do
      NPAIR=K

      MOpairs(NPAIR+1:Nbasis,1)=0
      MOpairs(NPAIR+1:Nbasis,2)=0

      if(NPAIR.EQ.0.and.GVB_guessMO(1:3).EQ.'CMO')then
        write(UNIout,'(a)')'ERROR> GVBCMO: NO GVB PAIRS FOUND'
        stop 'ERROR> GVBCMO: NO GVB PAIRS FOUND'
      end if
      write(UNIout,'(/a/)')'THE FOUND GVB-PAIR --- in CMO --- USED FOR GVB-SCF ARE THE FOLLOWING:'
      do I=1,NPAIR
        IA=GVBSBD(I,1)
        IB=GVBSBD(I,2)
        if(IA.NE.0)then
          GVBSBD(I,1)=0
          GVBSBD(I,2)=0
          write(UNIout,5104)i,CARTESIAN(IA)%element,CARTESIAN(IB)%element,(MOpairs(i,k),k=1,2)
        end if
        if(IA.EQ.0)then
          IA=GVBPBD(I,1)
          IB=GVBPBD(I,2)
          if(IA.NE.0)then
            GVBPBD(I,1)=0
            GVBPBD(I,2)=0
            write(UNIout,5105)i,CARTESIAN(IA)%element,CARTESIAN(IB)%element,(MOpairs(i,k),k=1,2)
          end if
          if(IA.EQ.0)then
            IA=GVBTBD(I,1)
            IB=GVBTBD(I,2)
            if(IA.NE.0)then
              GVBTBD(I,1)=0
              GVBTBD(I,2)=0
              write(UNIout,5106)i,CARTESIAN(IA)%element,CARTESIAN(IB)%element,(MOpairs(i,k),k=1,2)
            end if
            if(IA.EQ.0)then
              IA=GVBCBD(I,1)
              IB=GVBCBD(I,2)
              IC=GVBCBD(I,3)
              if(IA.NE.0)then
                GVBCBD(I,1)=0
                GVBCBD(I,2)=0
                GVBCBD(I,3)=0
             write(UNIout,5107)i,CARTESIAN(IA)%element,CARTESIAN(IB)%element,CARTESIAN(IC)%element,(MOpairs(i,k),k=1,2)
              end if
            end if
          end if
        end if
      end do
5101  FORMAT(/'GVB-PAIR ',I4,'  for s-bond', 2X,A4,'--',2X,A4,'   not found in CMO')
5102  FORMAT(/'GVB-PAIR ',I4,'  for p-bond', 2X,A4,'--',2X,A4,'   not found in CMO')
5103  FORMAT(/'GVB-PAIR ',I4,'  for pp-bond', 2X,A4,'--',2X,A4,'   not found in CMO')
5104  FORMAT(/'GVB-PAIR ',I4,'  for s-bond', 2X,A4,'--',2X,A4,' = (', 2I4, ')')
5105  FORMAT(/'GVB-PAIR ',I4,'  for p-bond', 2X,A4,'--',2X,A4,' = (', 2I4, ')')
5106  FORMAT(/'GVB-PAIR ',I4,'  for pp-bond', 2X,A4,'--',2X,A4,' = (', 2I4, ')')
5107  FORMAT(/'GVB-PAIR ',I4,'  for tc-bond', 2X,A4,'--',2X,A4,'--',2X,A4,' = (', 2I4, ')')
!
 1000 continue
!
      deallocate (PIBOND, TBOND, EPairs)
!
! End of routine GVBCMO
      call PRG_manager ('exit', 'GVBCMO', 'UTILITY')
      return
      end subroutine GVBCMO
      subroutine GVBPPM (SUM, &
                         SUM2, &
                         IMO, &
                         JMO, &
                         KA, & ! Atom A
                         KB, & ! Atom B
                         CMO, & ! CMO or LMO coefficients
                         Natoms, &
                         Nbasis)
!***********************************************************************
!     Date last modified: May 15, 1995                     Version 1.0 *
!     Author:  R. Poirier, and Y. Wang                                 *
!     Description: Equation 21 in Y. Wang and R.A. Poirier, JCC,       *
!                  17(3), 313-325 (1996).                              *
!***********************************************************************
! Modules:
      USE program_constants
      USE type_basis_set

      implicit none
!
! Input scalars:
      integer KA,KB,IMO,JMO,K,Nbasis,Natoms
      double precision SUM,SUM2
!
! Input arrays:
      double precision CMO(Nbasis,Nbasis)
!
! Begin:
      call PRG_manager ('enter', 'GVBPPM', 'UTILITY')
!
      SUM=ZERO
      SUM2=ZERO
! Sum over all AOs belonging to atom KA
      do K=1,Nbasis
        if(Basis%AOtoATOM(K).EQ.KA)then
        SUM=SUM+DABS(CMO(K,JMO)*CMO(K,IMO))
        SUM2=SUM2+CMO(K,JMO)*CMO(K,IMO)
        end if ! KA
      end do
!
! Sum over all AOs belonging to atom KB
      do K=1,Nbasis
        if(Basis%AOtoATOM(K).EQ.KB)then
        SUM=SUM+DABS(CMO(K,JMO)*CMO(K,IMO))
        SUM2=SUM2-CMO(K,JMO)*CMO(K,IMO)
        end if ! KB
      end do
!
      SUM2=DABS(SUM2)
!
! End of function GVBPPM
      call PRG_manager ('exit', 'GVBPPM', 'UTILITY')
      return
      end subroutine GVBPPM
! No longer used, replaced by BLD_MO_IDmatrix
      subroutine GVBBFD (LBATMA, LBATMB, LBATMC, LBOND, KC, &
                         NCOREin, &
                         BCRIT0, &
                         BCDM, &
                         CMO, &
                         NoccMO, &
                         Nbasis)
!***********************************************************************
!     Date last modified: May 15, 1995                     Version 1.0 *
!     Author: R.A. Poirier and Y. Wang                                 *
!     Description: Bond identification.                                *
!***********************************************************************
! Modules:
      USE type_basis_set
      USE module_GVB

      implicit none
!
! Input scalars
      integer, intent(IN) :: Nbasis
      integer KC,NCOREin,NoccMO
      integer LBATMA(Nbasis),LBATMB(Nbasis),LBATMC(Nbasis)
      double precision LBOND(Natoms,Nbasis)
!
! Input Arrays:
      double precision CMO(Nbasis,Nbasis)
!
! Local scalars:
      integer I,IMO,J,I1,I2,K
      integer Jatom
      double precision BCRIT,BCRIT0,CCRIT,BCDM,SUM
!
! Work arrays:
      character*8, dimension(:), allocatable :: Btype !
!
! Begin:
      call PRG_manager ('enter', 'GVBBFD', 'UTILITY')
!
! Not used at the moment
      allocate (Btype(1:Nbasis))

      do IMO=1,Nbasis
        Btype(IMO)='SIGMA'
      end do ! IMO
!
      CCRIT=0.03D00
!
      do I=1,Nbasis
        do K=1,Nbasis
        Jatom=Basis%AOtoATOM(K)
        LBOND(Jatom,I)=LBOND(Jatom,I)+CMO(K,I)*CMO(K,I)
        end do ! K
      end do ! I
!
        NCOREin=0
        do I=1,Natoms
          J=CARTESIAN(I)%Atomic_number
          if(J.GT.54)then
            NCOREin=NCOREin+36
          else if(J.GT.36)then
            NCOREin=NCOREin+18
          else if(J.GT.18)then
            NCOREin=NCOREin+9
          else if(J.GT.10)then
            NCOREin=NCOREin+5
          else if(J.GT.2)then
            NCOREin=NCOREin+1
          end if
        end do
!for occupied
      do I=1,NoccMO
        SUM=ZERO
        BCRIT=BCRIT0
        do J=1,Natoms
          SUM=SUM+LBOND(J,I)
        end do ! J
        do J=1,Natoms
          LBOND(J,I)=LBOND(J,I)/SUM
        end do ! J
951     I1=0
        I2=0
        do J=1,Natoms
          if(LBOND(J,I).LT.BCRIT)GO TO 950
! Found 3 major contributions - Give up.
          if(I2.NE.0)then
            if(KC.EQ.0)then
              LBATMA(I)=0
              LBATMB(I)=0
              GO TO 900
            else
              LBATMA(I)=I1
              LBATMB(I)=I2
              LBATMC(I)=J
              Btype(I)='3-CENTER'
              GO TO 900
            end if
          end if
          if(I1.NE.0)GO TO 940
! Found first major contribution.
          I1=J
          GO TO 950
! Found second major atom for this LMO.
  940     I2=J
  950     continue
        end do !J
!
! Have we found at least 1 major contributor? - If not, give up.
        if(I1.EQ.0)GO TO 900
        if(I2.EQ.0)then
          BCRIT=BCRIT-CCRIT
          I2=I1
          if(BCRIT.GT.BCDM) GO TO 951
        end if
        LBATMA(I)=I1
        LBATMB(I)=I2
        if(I.LE.NCOREin)then
          Btype(I)='CORE'
        else
          Btype(I)='LPAIR'
        end if
  900 continue
      end do !I
!
!for virtual
      do I=NoccMO+1,Nbasis
        SUM=ZERO
        BCRIT=BCRIT0
        do J=1,Natoms
          SUM=SUM+LBOND(J,I)
        end do ! J
        do J=1,Natoms
          LBOND(J,I)=LBOND(J,I)/SUM
        end do ! J
1051    I1=0
        I2=0
        do J=1,Natoms
          if(LBOND(J,I).LT.BCRIT)GO TO 1050
! Found 3 major contributions - Give up.
          if(I2.NE.0)then
            if(KC.EQ.0)then
              LBATMA(I)=0
              LBATMB(I)=0
              GO TO 1200
            else
              LBATMA(I)=I1
              LBATMB(I)=I2
              LBATMC(I)=J
              GO TO 1200
            end if
          end if
          if(I1.NE.0)GO TO 1040
! Found first major contribution.
          I1=J
          GO TO 1050
! Found second major atom for this LMO.
 1040     I2=J
 1050     continue
        end do !J
!
! Have we found at least 1 major contributor? - If not, give up.
        if(I1.EQ.0)GO TO 1200
        if(I2.EQ.0)then
          BCRIT=BCRIT-CCRIT
          I2=I1
          if(BCRIT.GT.BCDM) GO TO 1051
        end if
        LBATMA(I)=I1
        LBATMB(I)=I2
 1200 continue
      end do !I
!
      deallocate (Btype)
!
! End of routine GVBBFD
      call PRG_manager ('exit', 'GVBBFD', 'UTILITY')
      return
      end subroutine GVBBFD
      subroutine GVBLMO (CMOpairs)
!***********************************************************************
!     Date last modified: May 15, 1995                     Version 1.0 *
!     Author: Y. Wang and R.A. Poirier                                 *
!     Description: Generate the Perfect-Pairing MOs from LMOs.         *
!***********************************************************************
! Modules:
      USE QM_defaults
      USE QM_objects
      USE matrix_print
      USE module_GVB
      USE type_epairs

      implicit none
!
! Input scalars:
      character*8 LMOFROM
      integer CMOpairs(Nbasis,2)
!
! Work arrays:
      integer, dimension(:), allocatable :: PIBOND
      integer, dimension(:), allocatable :: TBOND
      type (electron_pairs), dimension(:), allocatable :: EPairs
!
! Local scalars:
      integer :: IMO,Ipair,Ivirt
      integer IA,IB,IC,JA,JB,KA,KB,I,I1,I2,JJ,K,KC,NT,NPI,Nbasis1,JJA,JJB,K1,NcoreMO,NCORE1
      double precision SUM,SUM1,SUM2,SUM3
!
! Local parameters:
      double precision TENM5
      parameter (TENM5=1.0D-5)
!
! Begin:
      call PRG_manager ('enter', 'GVBLMO', 'UTILITY')
!
      NPAIR=NPAIRin+NSBond+NPBond+NTBond+NCBond
!
      allocate (EPairs(Nbasis))
      call BLD_MO_IDmatrix (LMO%coeff, EPairs, Natoms, Nbasis, CMO%NoccMO)
      allocate (PIBOND(Nbasis), TBOND(Nbasis))
!
! Initialize the pairs to zero.
      NT=0
      GVBpairs(1:Nbasis,1:2)=0
      PIBOND(1:Nbasis)=0
      TBOND(1:Nbasis)=0
!
      KC=0
      do I=1,Nbasis
        KC=KC+GVBSBD(I,1)+GVBPBD(I,1)+GVBCBD(I,1)+GVBTBD(I,1)
      end do
      if(KC.EQ.0.and.NPAIR.NE.0)GO TO 9901
      KC=0
      do I=1,Nbasis
        I1=GVBCBD(I,1)             ! 3-centre bonds
        if(I1.NE.0)KC=I1
      end do
!
      NcoreMO=NFrozen_cores()
      if(NPAIR.EQ.0)then
        call GVB_BLD_pairs (EPairs, NcoreMO)
      end if ! NPAIR

! For occupied Triple bonds
      if(NTBond.gt.0)then
      NT=1
      do Ipair=1,NPAIR
        KA=GVBTBD(Ipair,1)
        KB=GVBTBD(Ipair,2)
        if(KA.NE.0.and.KB.NE.0)then
        do IMO=CMO%NoccMO,1,-1
          do K1=1,Nbasis
            I2=OPNLST(K1) ! Skip open-shells
            if(IMO.NE.I2)then
              I2=CMOpairs(K1,1)
              if(IMO.NE.I2)then
                I2=GVBpairs(K1,1)
              end if
            end if
            if(IMO.EQ.I2)GO TO 101
          end do
          IA=EPairs(IMO)%AtomA
          IB=EPairs(IMO)%AtomB
          if((KA.EQ.IA.and.KB.EQ.IB).or.(KA.EQ.IB.and.KB.EQ.IA))then
            GVBpairs(Ipair,1)=IMO
            TBOND(NT)=IMO
            NT=NT+1
            GOTO 100
          end if
 101      continue
        end do ! IMO
        end if ! KA.ne.
 100    continue
      end do ! Ipair
      end if ! NTBond
!      call PRT_matrix (GVBpairs, Nbasis, 2)
!
! Pi Bonds
      NPI=1
      do Ipair=1,NPAIR
        KA=GVBPBD(Ipair,1)
        KB=GVBPBD(Ipair,2)
        if(KA.NE.0.and.KB.NE.0)then
        do I=CMO%NoccMO,1,-1
          do K1=1,Nbasis
            I2=OPNLST(K1) ! Skip open-shells
            if(I.NE.I2)then
              I2=CMOpairs(K1,1)
              if(I.NE.I2)then
                I2=GVBpairs(K1,1)
              end if
            end if
            if(I.EQ.I2)GO TO 111
          end do
          IA=EPairs(I)%AtomA
          IB=EPairs(I)%AtomB
          if((KA.EQ.IA.and.KB.EQ.IB).or.(KA.EQ.IB.and.KB.EQ.IA))then
            do JJ=1,NT-1
              if(I.EQ.TBOND(JJ))GO TO 111
            end do
            GVBpairs(Ipair,1)=I
            PIBOND(NPI)=I
            NPI=NPI+1
            GO TO 110
          end if
 111      continue
        end do !I
        end if
 110    continue
      end do ! Ipair
!
! Single bonds
      do Ipair=1,NPAIR
        KA=GVBSBD(Ipair,1)
        KB=GVBSBD(Ipair,2)
        if(KA.NE.0.and.KB.NE.0)then
        if(KA.EQ.KB)then
          NCORE1=NcoreMO+1
        else
           NCORE1=1
        end if
        do I=NCORE1,CMO%NoccMO
          do K1=1,Nbasis
            I2=OPNLST(K1) ! Skip open-shells
            if(I.NE.I2)then
              I2=CMOpairs(K1,1)
              if(I.NE.I2)then
                I2=GVBpairs(K1,1)
              end if
            end if
            if(I.EQ.I2)GO TO 301
          end do ! K1
          IA=EPairs(I)%AtomA
          IB=EPairs(I)%AtomB
          if((KA.EQ.IA.and.KB.EQ.IB).or.(KA.EQ.IB.and.KB.EQ.IA))then
            do JJ=1,NT-1
              if(I.EQ.TBOND(JJ)) GOTO 301
            end do
            do JJ=1,NPI-1
              if(I.EQ.PIBOND(JJ)) GOTO 301
            end do
            GVBpairs(Ipair,1)=I
            GO TO 300
          end if
 301      continue
        end do !I
        end if
 300    continue
      end do !Ipair
!      call PRT_matrix (GVBpairs, Nbasis, 2)
!
! For virtuals
      NT=1
      do Ipair=1,NPAIR
        KA=GVBTBD(Ipair,1)
        KB=GVBTBD(Ipair,2)
        if(KA.NE.0.and.KB.NE.0)then
        SUM3=ZERO
        SUM=ZERO
        I1=GVBpairs(Ipair,1)
        do I=CMO%NoccMO+1,Nbasis
          do K1=1,Nbasis
            I2=OPNLST(K1) ! Skip open-shells
            if(I.NE.I2)then
              I2=CMOpairs(K1,2)
              if(I.NE.I2)then
                I2=GVBpairs(K1,2)
              end if
            end if
            if(I.EQ.I2) GO TO 201
          end do
          IA=EPairs(I)%AtomA
          IB=EPairs(I)%AtomB
          if((KA.EQ.IA.and.KB.EQ.IB).or.(KA.EQ.IB.and.KB.EQ.IA))then
            call GVBPPM(SUM1, SUM2, I, I1, KA, KB, LMO%coeff, Natoms, Nbasis)
            if(SUM2.GT.SUM3)then
                SUM3=SUM2
                SUM=SUM1/10.0D00+0.10D00
                GVBpairs(Ipair,2)=I
                TBOND(NT)=I
            end if
          end if
 201      continue
        end do !I
        NT=NT+1
        end if
 200    continue
      end do ! Ipair
!
      NPI=1
      do Ipair=1,NPAIR
        KA=GVBPBD(Ipair,1)
        KB=GVBPBD(Ipair,2)
        if(KA.NE.0.and.KB.NE.0)then
        I1=GVBpairs(Ipair,1)
        SUM3=ZERO
        SUM=ZERO
        do I=CMO%NoccMO+1,Nbasis
          do K1=1,Nbasis
            I2=OPNLST(K1) ! Skip open-shells
            if(I.NE.I2)then
              I2=CMOpairs(K1,2)
              if(I.NE.I2)then
                I2=GVBpairs(K1,2)
              end if
            end if
            if(I.EQ.I2) GO TO 211
          end do
          IA=EPairs(I)%AtomA
          IB=EPairs(I)%AtomB
          if((KA.EQ.IA.and.KB.EQ.IB).or.(KA.EQ.IB.and.KB.EQ.IA))then
            do JJ=1,NT-1
              if(I.EQ.TBOND(JJ)) GOTO 211
            end do
            call GVBPPM(SUM1, SUM2, I, I1, KA, KB, LMO%coeff, Natoms, Nbasis)
            if(SUM2.GT.SUM3)then
                SUM3=SUM2
                SUM=SUM1/10.0D00+0.10D00
                GVBpairs(Ipair,2)=I
                PIBOND(NPI)=I
            end if
          end if
 211      continue
        end do !I
        NPI=NPI+1
        end if
 210    continue
      end do ! Ipair
!
      do Ipair=1,NPAIR
        KA=GVBSBD(Ipair,1)
        KB=GVBSBD(Ipair,2)
        if(KA.NE.0.and.KB.NE.0)then
        I1=GVBpairs(Ipair,1)
        SUM3=ZERO
        SUM=ZERO
        do I=CMO%NoccMO+1,Nbasis
          do K1=1,Nbasis
            I2=OPNLST(K1) ! Skip open-shells
            if(I.NE.I2)then
              I2=CMOpairs(K1,2)
              if(I.NE.I2)then
                I2=GVBpairs(K1,2)
              end if
            end if
            if(I.EQ.I2) GO TO 401
          end do
          IA=EPairs(I)%AtomA
          IB=EPairs(I)%AtomB
          if((KA.EQ.IA.and.KB.EQ.IB).or.(KA.EQ.IB.and.KB.EQ.IA))then
            do JJ=1,NPI-1
              if(I.EQ.PIBOND(JJ)) GOTO 401
            end do
            do JJ=1,NT-1
              if(I.EQ.TBOND(JJ)) GOTO 401
            end do
            call GVBPPM(SUM1, SUM2, I, I1, KA, KB, LMO%coeff, Natoms, Nbasis)
            if(SUM2.GT.SUM3)then
                SUM3=SUM2
                SUM=SUM1/10.0D00+0.10D00
                GVBpairs(Ipair,2)=I
            end if
          end if
 401      continue
        end do ! I
        end if
 400    continue
      end do ! Ipair
!
!for occupied
      if(KC.NE.0)then
        do Ipair=1,NPAIR
          KA=GVBCBD(Ipair,1)
          KB=GVBCBD(Ipair,2)
          KC=GVBCBD(Ipair,3)
          if(KA.NE.0.and.KB.NE.0)then
          do I=CMO%NoccMO,1,-1
            do K1=1,Nbasis
              I2=OPNLST(K1) ! Skip open-shells
              if(I.NE.I2)then
                I2=CMOpairs(K1,2)
                if(I.NE.I2)then
                  I2=GVBpairs(K1,2)
                end if
              end if
              if(I.EQ.I2) GO TO 8801
            end do
            IA=EPairs(I)%AtomA
            IB=EPairs(I)%AtomB
            IC=EPairs(I)%AtomC
            if((IA.EQ.KA.and.((IB.EQ.KB.and.IC.EQ.KC).or.(IB.EQ.KC.and.IC.EQ.KB))).or. &
               (IA.EQ.KB.and.((IB.EQ.KA.and.IC.EQ.KC).or.(IB.EQ.KC.and.IC.EQ.KA))).or. &
               (IA.EQ.KC.and.((IB.EQ.KA.and.IC.EQ.KB).or.(IB.EQ.KB.and.IC.EQ.KA))))then
              GVBpairs(Ipair,1)=I
              GOTO 8800
            end if
8801        continue
          end do !I
          end if
8800      continue
        end do ! Ipair
!
! For virtual
        do Ipair=1,NPAIR
          KA=GVBCBD(Ipair,1)
          KB=GVBCBD(Ipair,2)
          KC=GVBCBD(Ipair,3)
          if(KA.NE.0.and.KB.NE.0)then
          SUM1=ZERO
          I1=GVBpairs(Ipair,1)
          if(I1.EQ.0) GO TO 8810
          do I=CMO%NoccMO+1,Nbasis
            do K1=1,Nbasis
              I2=OPNLST(K1) ! Skip open-shells
              if(I.NE.I2)then
                I2=CMOpairs(K1,2)
                if(I.NE.I2)then
                  I2=GVBpairs(K1,2)
                end if
              end if
              if(I.EQ.I2) GO TO 8811
            end do
            SUM=ZERO
            IA=EPairs(I)%AtomA
            IB=EPairs(I)%AtomB
            IC=EPairs(I)%AtomC
            if((IA.EQ.KA.and.((IB.EQ.KB.and.IC.EQ.KC).or.(IB.EQ.KC.and.IC.EQ.KB))).or. &
               (IA.EQ.KB.and.((IB.EQ.KA.and.IC.EQ.KC).or.(IB.EQ.KC.and.IC.EQ.KA))).or. &
               (IA.EQ.KC.and.((IB.EQ.KA.and.IC.EQ.KB).or.(IB.EQ.KB.and.IC.EQ.KA))))then
              do K=1,Nbasis
                SUM=SUM+DABS(LMO%coeff(K,I1)*LMO%coeff(K,I))
              end do
              if(SUM.GT.SUM1)then
                SUM1=SUM
                GVBpairs(Ipair,2)=I
              end if
            end if
8811        continue
          end do !I
          end if
8810      continue
        end do ! Ipair
      end if
!
9901  Ipair=NPAIR
      do I=1,Nbasis
        I1=CMOpairs(I,1)
        I2=CMOpairs(I,2)
        if(I1.NE.0.and.I2.NE.0)then
          Ipair=Ipair+1
          GVBpairs(Ipair,1)=CMOpairs(I,1)
          GVBpairs(Ipair,2)=CMOpairs(I,2)
        end if
      end do
      NPAIR=Ipair
!
      K=0
      do I=1,NPAIR
        if(GVBpairs(I,1).EQ.0.or.GVBpairs(I,2).EQ.0)then
          IA=GVBSBD(I,1)
          IB=GVBSBD(I,2)
          if(IA.NE.0)write(UNIout,5101) i,CARTESIAN(IA)%element,CARTESIAN(IB)%element
          if(IA.EQ.0)then
            IA=GVBPBD(I,1)
            IB=GVBPBD(I,2)
            if(IA.NE.0)write(UNIout,5102) i,CARTESIAN(IA)%element,CARTESIAN(IB)%element
            if(IA.EQ.0)then
              IA=GVBTBD(I,1)
              IB=GVBTBD(I,2)
              if(IA.NE.0)write(UNIout,5103) i,CARTESIAN(IA)%element,CARTESIAN(IB)%element
            end if
          end if
        else
          K=K+1
          GVBpairs(K,1)=GVBpairs(I,1)
          GVBpairs(K,2)=GVBpairs(I,2)
          GVBSBD(K,1)=GVBSBD(I,1)
          GVBSBD(K,2)=GVBSBD(I,2)
          GVBPBD(K,1)=GVBPBD(I,1)
          GVBPBD(K,2)=GVBPBD(I,2)
          GVBTBD(K,1)=GVBTBD(I,1)
          GVBTBD(K,2)=GVBTBD(I,2)
          GVBCBD(K,1)=GVBCBD(I,1)
          GVBCBD(K,2)=GVBCBD(I,2)
          GVBCBD(K,3)=GVBCBD(I,3)
        end if
      end do
      NPAIR=K

      GVBpairs(NPAIR+1:Nbasis,1)=0
      GVBpairs(NPAIR+1:Nbasis,2)=0

      write(UNIout,*) '     '
      write(UNIout,*) 'THE FOUND GVB-PAIR USED FOR GVB_SCF ARE THE FOLLOWING:'
      write(UNIout,*) '     '
      do I=1,NPAIR
        IA=GVBSBD(I,1)
        IB=GVBSBD(I,2)
        if(IA.NE.0)write(UNIout,5104)i,CARTESIAN(IA)%element,CARTESIAN(IB)%element,(GVBpairs(i,k),k=1,2)
        if(IA.EQ.0)then
          IA=GVBPBD(I,1)
          IB=GVBPBD(I,2)
          if(IA.NE.0)write(UNIout,5105)i,CARTESIAN(IA)%element,CARTESIAN(IB)%element,(GVBpairs(i,k),k=1,2)
          if(IA.EQ.0)then
            IA=GVBTBD(I,1)
            IB=GVBTBD(I,2)
          if(IA.NE.0)write(UNIout,5106)i,CARTESIAN(IA)%element,CARTESIAN(IB)%element,(GVBpairs(i,k),k=1,2)
          if(IA.EQ.0)then
            IA=GVBCBD(I,1)
            IB=GVBCBD(I,2)
            IC=GVBCBD(I,3)
            if(IA.NE.0)write(UNIout,5107)i,CARTESIAN(IA)%element,CARTESIAN(IB)%element,CARTESIAN(IC)%element, &
                            (GVBpairs(i,k),k=1,2)
            if(IA.EQ.0)write(UNIout,5108)i,(GVBpairs(i,k),k=1,2)
          end if
        end if
      end if
      end do
5101  FORMAT(/'GVB-PAIR ',I4,'  for s-bond', 2X,A4,'--',2X,A4,'   not found in LMO')
5102  FORMAT(/'GVB-PAIR ',I4,'  for p-bond', 2X,A4,'--',2X,A4,'   not found in LMO')
5103  FORMAT(/'GVB-PAIR ',I4,'  for pp-bond', 2X,A4,'--',2X,A4,'   not found in LMO')
5104  FORMAT(/'GVB-PAIR ',I4,'  for s-bond', 2X,A4,'--',2X,A4,' = (', 2I4, ')')
5105  FORMAT(/'GVB-PAIR ',I4,'  for p-bond', 2X,A4,'--',2X,A4,' = (', 2I4, ')')
5106  FORMAT(/'GVB-PAIR ',I4,'  for pp-bond', 2X,A4,'--',2X,A4,' = (', 2I4, ')')
5107  FORMAT(/'GVB-PAIR ',I4,'  for tc-bond', 2X,A4,'--',2X,A4,'--',2X,A4,' = (', 2I4, ')')
5108  FORMAT(/'GVB-PAIR ',I4,'  from CMO  ', 10X,' = (', 2I4, ')')
!
      deallocate (PIBOND, TBOND, EPairs)
!
! End of routine GVBLMO
      call PRG_manager ('exit', 'GVBLMO', 'UTILITY')
      return
      end subroutine GVBLMO
      subroutine GVB_BLD_pairs (EPairs, NcoreMO)
!***********************************************************************
!     Date last modified: February 13, 2008                Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: Builds pairs from EPairs                            *
!***********************************************************************
! Modules:
      USE QM_objects
      USE module_GVB
      USE type_epairs

      implicit none
!
! Local scalars:
      integer, intent(IN) :: NcoreMO
      type (electron_pairs) :: EPairs(Nbasis)
      integer :: I,IA,IB,J,JA,JB,JJ,JJA,JJB,K,KA,KB
!
! Begin:
      call PRG_manager ('enter', 'GVB_BLD_pairs', 'UTILITY')

      do I=NcoreMO+1,CMO%NoccMO
        KA=EPairs(I)%AtomA
        KB=EPairs(I)%AtomB
        if(KA.EQ.0.or.KB.EQ.0)cycle
        if(KA.EQ.KB)cycle
        NPAIR=NPAIR+1
        GVBSBD(NPAIR,1)=KA
        GVBSBD(NPAIR,2)=KB
        if(NPAIR.EQ.1)cycle
        do J=1,NPAIR-1
          IA=GVBSBD(J,1)
          IB=GVBSBD(J,2)
          if((KA.EQ.IA.and.KB.EQ.IB).or.(KA.EQ.IB.and.KB.EQ.IA))then
            GVBSBD(NPAIR,1)=0
            GVBSBD(NPAIR,2)=0
            do K=1,NPAIR-1
              JA=GVBPBD(K,1)
              JB=GVBPBD(K,2)
                if((KA.EQ.JA.and.KB.EQ.JB).or.(KA.EQ.JB.and.KB.EQ.JA))then
                GVBPBD(NPAIR,1)=0
                GVBPBD(NPAIR,2)=0
                do JJ=1,NPAIR-1
                  JJA=GVBTBD(JJ,1)
                  JJB=GVBTBD(JJ,2)
                  if((KA.EQ.JJA.and.KB.EQ.JJB).or.(KA.EQ.JJB.and.KB.EQ.JJA))then
                    write(UNIout,'(a)') 'ERROR> GVBCMO: Too many bond pairs found'
                    stop
                  else
                    GVBTBD(NPAIR,1)=KA
                    GVBTBD(NPAIR,2)=KB
                  end if ! (KA.EQ.JJB...
                end do !
              else
                GVBPBD(NPAIR,1)=KA
                GVBPBD(NPAIR,2)=KB
              end if
            end do ! K
          end if
        end do ! J
      end do ! I

! End of routine GVB_BLD_pairs
      call PRG_manager ('exit', 'GVB_BLD_pairs', 'UTILITY')
      return
      end subroutine GVB_BLD_pairs
