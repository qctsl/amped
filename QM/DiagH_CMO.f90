      subroutine DiagH_CMO
!***********************************************************************
!     Date last modified: April 18, 1997                  Version 1.0  *
!     Author: R.A. Poirier                                             *
!     Description: Perform closed shell SCF calculation (also Direct). *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE type_basis_set
      USE QM_defaults
      USE QM_objects
      USE INT_objects

      implicit none
!
! Local scalars:
!
! Begin:
      call PRG_manager ('enter', 'DIAGH_CMO', 'CMO%DIAGH')
!       
      call GET_object ('QM', 'CMO_GUESS', 'RHF')
      call GET_object ('INT', '1EINT', 'AO')

      Wavefunction='DIAGH'
      Level_of_Theory='DIAGH'
      if(.not.associated(CMOH%coeff))then
        allocate (CMOH%coeff(Nbasis,Nbasis), CMOH%EIGVAL(Nbasis), CMOH%occupancy(Nbasis))
      else
        deallocate (CMOH%coeff, CMOH%EIGVAL, CMOH%occupancy)
        allocate (CMOH%coeff(Nbasis,Nbasis), CMOH%EIGVAL(Nbasis), CMOH%occupancy(Nbasis))
      end if

      if(.not.allocated(PM0G))then
        allocate (PM0G(1:MATlen)) ! always allocated together
      else
        deallocate (PM0G)
        allocate (PM0G(1:MATlen))
      end if

      CMOH%NoccMO=CMOG%NoccMO
      CMOH%occupancy(1:CMOH%NoccMO)=TWO
      CMOH%occupancy(CMOH%NoccMO+1:Nbasis)=ZERO
      call DETlinW (OVRLAP, Hcore, CMOH%coeff, CMOH%EIGVAL, MATlen, Nbasis, .true.)
      call DENBLD (CMOH%coeff, CMOH%occupancy, PM0G, Nbasis, MATlen, CMOH%NoccMO)
      PM0=>PM0G
!
! End of routine DiagH_CMO
      call PRG_manager ('exit', 'DIAGH_CMO', 'CMO%DIAGH')
      return
      end subroutine DiagH_CMO
