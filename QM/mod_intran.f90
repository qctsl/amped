      module integral_transformation
!*****************************************************************************************************************
!     Date last modified: March 6, 2000                                                             Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Work arrays and storage related arrays and scalars for integral transformation.               *
!*****************************************************************************************************************
! Modules:
      USE QM_objects
      USE INT_objects

      implicit none
!
      integer :: NTROMO,NTRVMO,NTTRMO
      integer :: NTMOMX ! Maximum number of MO that can be transfomed
      integer :: NJKL   ! Number of JKL AO integrals = Nbasis*Nbasis*(Nbasis+1)/2
      integer :: NPQRS  ! Number of MO integrals
      integer :: PQMAX  ! Maximum number of MO integrals
      integer :: PQRSMX  ! Maximum number of MO integrals
      integer :: Npasses ! Number of passes required for transformation
      integer :: TNPQRS ! total number of transformed MO integrals
!
      type (molecular_orbital) CMO_occ
      type (molecular_orbital) CMO_vrt
!
      integer, dimension(:), allocatable :: MO_group
      double precision, dimension(:,:), allocatable :: CMOS ! symmetry adapted CMO
      double precision, dimension(:), allocatable :: EIGVLS ! symmetry adapted EIGVL
      double precision, dimension(:), allocatable :: EIGVLO ! EIGVL occupied
      double precision, dimension(:), allocatable :: EIGVLV ! EIGVL virtuals
      double precision, dimension(:,:), allocatable :: DELS
      double precision, dimension(:,:), allocatable :: TMOint
      double precision, dimension(:,:), allocatable :: VMOint
!
! Integral Transformation:
      type (ints_2e_ijkl), dimension(:), allocatable :: PQRS_MOINT
!
! Work arrays:
      integer, dimension(:), allocatable :: IJNDX
      integer, dimension(:), allocatable :: NCODE
      double precision, dimension(:,:), allocatable :: BPQMO
      double precision, dimension(:), allocatable :: BPQRS
      double precision, dimension(:), allocatable :: SCRVEC
!
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine INI_intran
!***********************************************************************
!     Date last modified: June 6, 2005                                 *
!     Author: J.W. Hollett                                             *
!     Description: Initialize integral transformation parameters.      *
!***********************************************************************
! Modules:
      USE program_files
      USE type_molecule

      implicit none
!
! Local scalar:
      integer I,IG,IMO,J,NOCOLD,NTOCC,NTR,NoccMO
      double precision, dimension(:,:), allocatable :: CMOSw ! symmetry adapted CMO
      double precision, dimension(:), allocatable :: EIGVLSw ! symmetry adapted EIGVL
!
! Begin:
      call PRG_manager ('enter', 'INI_intran', 'UTILITY')
!
      allocate (CMOSw(Nbasis,Nbasis), EIGVLSw(Nbasis))
      NoccMO=CMO%NoccMO
!
      if(.not.allocated(MO_group))then ! May have been allocated and defined by the MENU
        allocate (MO_group(Nbasis))
        MO_group(1:NoccMO)=1
        MO_group(NoccMO+1:Nbasis)=0
        NTTRMO=NoccMO
      else if(size(MO_group).ne.Nbasis)then
        deallocate (MO_group)
        allocate (MO_group(Nbasis))
        MO_group(1:NoccMO)=1
        MO_group(NoccMO+1:Nbasis)=0
        NTTRMO=NoccMO
      end if
!
      write(UNIout,1000)
      write(UNIout,1020)(MO_group(I),I=1,Nbasis)
      call flush(UNIout)
      NTR=0
      NOCOLD=CMO%NoccMO
      do IG=1,99
      do I=1,Nbasis
        if(MO_group(I).EQ.IG)then
        NTR=NTR+1
        EIGVLSw(NTR)=CMO%EIGVAL(I)
        do J=1,Nbasis
          CMOSw(NTR,J)=CMO%coeff(J,I)
        end do ! J
        end if ! MO_group(I)
      end do ! I
      end do ! IG
!
      NTOCC=0
      do I=1,NOCOLD
        if(MO_group(I).NE.0)NTOCC=NTOCC+1
      end do ! I
!
      NTTRMO=NTR

      write(UNIout,1050)NTR

      if(.not.allocated(CMOS))then
        allocate (CMOS(NTTRMO,Nbasis), EIGVLS(NTTRMO))
      else if(size(CMOS,2).ne.Nbasis)then
        deallocate (CMOS, EIGVLS)
        allocate (CMOS(NTTRMO,Nbasis), EIGVLS(NTTRMO))
      end if

      do IMO=1,NTTRMO
        EIGVLS(IMO)=EIGVLSw(IMO)
        CMOS(IMO,1:Nbasis)=CMOSw(IMO,1:Nbasis)
      end do

      deallocate (CMOSw, EIGVLSw)
!
 1000 FORMAT(/33('*')/'*INTEGRAL TRANSFORMATION PACKAGE*'/33('*')/)
 1020 FORMAT(/'GROUP SPECIFICATION:'/2(1X,36I3/))
 1050 FORMAT(/'TRANSFORM',I3,' MO'/)
!
! End of routine INI_intran
      call PRG_manager ('exit', 'INI_intran', 'UTILITY')
      return
      end subroutine INI_intran
      subroutine I2EPMO (CMOS, &
                         MO1, &
                         MO2, &
                         NMO)
!***********************************************************************
!     Date last modified: June 24, 1992                                *
!     Author: R. A. Poirier                                            *
!     Description: Transform two-electron integrals.                   *
!     Form  (PJ/KL) = SUM C(P,I)*(IJ/KL), where J, K and L extend over *
!     all AO, and P over the maximum number of MO.                     *
!***********************************************************************
! Modules:
      USE program_files
!     USE QM_objects

      implicit none
!
! Input scalars:
      integer, intent(IN) :: MO1,MO2,NMO
!
! Input arrays:
      double precision CMOS(NMO,Nbasis)
!
! Local scalars:
      logical EOF
      integer I,Iao,Jao,JKLNDX,Kao,Lao,LENGTH,lennam,MOINDX,PMO
      double precision G1,G2,G3
!
! Begin:
      call PRG_manager ('enter', 'I2EPMO', 'UTILITY')
!
! Improve ------------------------>
! (IJ,KL)
      if(IJKL_count.GT.0)then
        do I=1,IJKL_count
        Iao=IJKL_INCORE(I)%I
        Jao=IJKL_INCORE(I)%J
        Kao=IJKL_INCORE(I)%K
        Lao=IJKL_INCORE(I)%L
        G1=IJKL_INCORE(I)%IJKL
        G2=IJKL_INCORE(I)%IKJL
        G3=IJKL_INCORE(I)%ILJK
!
        MOINDX=0
        do PMO=MO1,MO2
        JKLNDX=MOINDX+NCODE(Iao)+IJNDX(Kao)+Lao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Jao)*G1
        JKLNDX=MOINDX+NCODE(Jao)+IJNDX(Kao)+Lao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Iao)*G1
        JKLNDX=MOINDX+NCODE(Kao)+IJNDX(Iao)+Jao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Lao)*G1
        JKLNDX=MOINDX+NCODE(Lao)+IJNDX(Iao)+Jao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Kao)*G1
!
        JKLNDX=MOINDX+NCODE(Iao)+IJNDX(Jao)+Lao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Kao)*G2
        JKLNDX=MOINDX+NCODE(Kao)+IJNDX(Jao)+Lao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Iao)*G2
        JKLNDX=MOINDX+NCODE(Jao)+IJNDX(Iao)+Kao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Lao)*G2
        JKLNDX=MOINDX+NCODE(Lao)+IJNDX(Iao)+Kao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Jao)*G2
!
        JKLNDX=MOINDX+NCODE(Iao)+IJNDX(Jao)+Kao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Lao)*G3
        JKLNDX=MOINDX+NCODE(Lao)+IJNDX(Jao)+Kao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Iao)*G3
        JKLNDX=MOINDX+NCODE(Jao)+IJNDX(Iao)+Lao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Kao)*G3
        JKLNDX=MOINDX+NCODE(Kao)+IJNDX(Iao)+Lao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Jao)*G3
        MOINDX=MOINDX+NJKL
        end do ! PMO
        end do ! I

      if(LIJKL_ONDISK)then
        EOF=.FALSE.
        rewind IJKL_unit
        call READ_ijkl (IJKL_BUFFER, IJKL_MAX, IJKL_unit, LENGTH, EOF)
        do while(.not.EOF)
          if(LENGTH.GT.0)then
            do I=1,LENGTH
              Iao=IJKL_BUFFER(I)%I
              Jao=IJKL_BUFFER(I)%J
              Kao=IJKL_BUFFER(I)%K
              Lao=IJKL_BUFFER(I)%L
              G1=IJKL_BUFFER(I)%ijkl
              G2=IJKL_BUFFER(I)%ikjl
              G3=IJKL_BUFFER(I)%iljk
!
              MOINDX=0
              do PMO=MO1,MO2
              JKLNDX=MOINDX+NCODE(Iao)+IJNDX(Kao)+Lao
              BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Jao)*G1
              JKLNDX=MOINDX+NCODE(Jao)+IJNDX(Kao)+Lao
              BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Iao)*G1
              JKLNDX=MOINDX+NCODE(Kao)+IJNDX(Iao)+Jao
              BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Lao)*G1
              JKLNDX=MOINDX+NCODE(Lao)+IJNDX(Iao)+Jao
              BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Kao)*G1
!
              JKLNDX=MOINDX+NCODE(Iao)+IJNDX(Jao)+Lao
              BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Kao)*G2
              JKLNDX=MOINDX+NCODE(Kao)+IJNDX(Jao)+Lao
              BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Iao)*G2
              JKLNDX=MOINDX+NCODE(Jao)+IJNDX(Iao)+Kao
              BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Lao)*G2
              JKLNDX=MOINDX+NCODE(Lao)+IJNDX(Iao)+Kao
              BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Jao)*G2
!
              JKLNDX=MOINDX+NCODE(Iao)+IJNDX(Jao)+Kao
              BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Lao)*G3
              JKLNDX=MOINDX+NCODE(Lao)+IJNDX(Jao)+Kao
              BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Iao)*G3
              JKLNDX=MOINDX+NCODE(Jao)+IJNDX(Iao)+Lao
              BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Kao)*G3
              JKLNDX=MOINDX+NCODE(Kao)+IJNDX(Iao)+Lao
              BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Jao)*G3
              MOINDX=MOINDX+NJKL
              end do ! PMO
            end do ! I
          else
            stop 'I2E_transform, IJKL BUFFER LENGTH = 0'
          end if
        call READ_ijkl (IJKL_BUFFER, IJKL_MAX, IJKL_unit, LENGTH, EOF)
      end do
      end if ! ONDISK
      end if
!
! Type 2, integral S.
!
! (II,KL)
      if(IIKL_count.GT.0)then
        do I=1,IIKL_count
        Iao=IIKL_INCORE(I)%I
        Jao=IIKL_INCORE(I)%K
        Kao=IIKL_INCORE(I)%L
        G1=IIKL_INCORE(I)%iikl
        G2=IIKL_INCORE(I)%ikil
!
        MOINDX=0
        do PMO=MO1,MO2
        JKLNDX=MOINDX+NCODE(Iao)+IJNDX(Jao)+Kao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Iao)*G1
        JKLNDX=MOINDX+NCODE(Jao)+IJNDX(Iao)+Iao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Kao)*G1
        JKLNDX=MOINDX+NCODE(Kao)+IJNDX(Iao)+Iao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Jao)*G1
!
        JKLNDX=MOINDX+NCODE(Iao)+IJNDX(Iao)+Kao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Jao)*G2
        JKLNDX=MOINDX+NCODE(Jao)+IJNDX(Iao)+Kao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Iao)*G2
        JKLNDX=MOINDX+NCODE(Iao)+IJNDX(Iao)+Jao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Kao)*G2
        JKLNDX=MOINDX+NCODE(Kao)+IJNDX(Iao)+Jao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Iao)*G2
        MOINDX=MOINDX+NJKL
        end do ! PMO
        end do ! I
!
      if(LIIKL_ONDISK)then
        EOF=.FALSE.
        rewind IIKL_unit
        call READ_iikl (IIKL_BUFFER, IIKL_MAX, IIKL_unit, LENGTH, EOF)
        do while (.not.EOF)
          if(LENGTH.GT.0)then
            do I=1,LENGTH
              Iao=IIKL_BUFFER(I)%I
              Jao=IIKL_BUFFER(I)%K
              Kao=IIKL_BUFFER(I)%L
              G1=IIKL_BUFFER(I)%iikl
              G2=IIKL_BUFFER(I)%ikil
!
              MOINDX=0
              do PMO=MO1,MO2
              JKLNDX=MOINDX+NCODE(Iao)+IJNDX(Jao)+Kao
              BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Iao)*G1
              JKLNDX=MOINDX+NCODE(Jao)+IJNDX(Iao)+Iao
              BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Kao)*G1
              JKLNDX=MOINDX+NCODE(Kao)+IJNDX(Iao)+Iao
              BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Jao)*G1
!
              JKLNDX=MOINDX+NCODE(Iao)+IJNDX(Iao)+Kao
              BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Jao)*G2
              JKLNDX=MOINDX+NCODE(Jao)+IJNDX(Iao)+Kao
              BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Iao)*G2
              JKLNDX=MOINDX+NCODE(Iao)+IJNDX(Iao)+Jao
              BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Kao)*G2
              JKLNDX=MOINDX+NCODE(Kao)+IJNDX(Iao)+Jao
              BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Iao)*G2
              MOINDX=MOINDX+NJKL
              end do ! PMO
            end do ! I
          else
            stop 'I2E_transform, IIKL BUFFER LENGTH = 0'
          end if
        call READ_iikl (IIKL_BUFFER, IIKL_MAX, IIKL_unit, LENGTH, EOF)
      end do
      end if ! ONDISK
      end if
!
! (IJ,KJ)
      if(IJKJ_count.GT.0)then
        do I=1,IJKJ_count
        Iao=IJKJ_INCORE(I)%I
        Jao=IJKJ_INCORE(I)%J
        Kao=IJKJ_INCORE(I)%K
        G1=IJKJ_INCORE(I)%IJKJ
        G2=IJKJ_INCORE(I)%IKJJ
!
        MOINDX=0
        do PMO=MO1,MO2
        JKLNDX=MOINDX+NCODE(Iao)+IJNDX(Kao)+Jao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Jao)*G1
        JKLNDX=MOINDX+NCODE(Jao)+IJNDX(Kao)+Jao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Iao)*G1
        JKLNDX=MOINDX+NCODE(Jao)+IJNDX(Iao)+Jao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Kao)*G1
        JKLNDX=MOINDX+NCODE(Kao)+IJNDX(Iao)+Jao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Jao)*G1
!
        JKLNDX=MOINDX+NCODE(Iao)+IJNDX(Jao)+Jao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Kao)*G2
        JKLNDX=MOINDX+NCODE(Kao)+IJNDX(Jao)+Jao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Iao)*G2
        JKLNDX=MOINDX+NCODE(Jao)+IJNDX(Iao)+Kao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Jao)*G2
        MOINDX=MOINDX+NJKL
        end do ! PMO
        end do ! I
!
      if(LIJKJ_ONDISK)then
        EOF=.FALSE.
        rewind IJKJ_unit
        call READ_ijkj (IJKJ_BUFFER, IJKJ_MAX, IJKJ_unit, LENGTH, EOF)
      do while (.not.EOF)
        if(LENGTH.GT.0)then
          do I=1,LENGTH
            Iao=IJKJ_BUFFER(I)%I
            Jao=IJKJ_BUFFER(I)%J
            Kao=IJKJ_BUFFER(I)%K
            G1=IJKJ_BUFFER(I)%ijkj
            G2=IJKJ_BUFFER(I)%ikjj
!
            MOINDX=0
            do PMO=MO1,MO2
            JKLNDX=MOINDX+NCODE(Iao)+IJNDX(Kao)+Jao
            BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Jao)*G1
            JKLNDX=MOINDX+NCODE(Jao)+IJNDX(Kao)+Jao
            BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Iao)*G1
            JKLNDX=MOINDX+NCODE(Jao)+IJNDX(Iao)+Jao
            BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Kao)*G1
            JKLNDX=MOINDX+NCODE(Kao)+IJNDX(Iao)+Jao
            BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Jao)*G1
!
            JKLNDX=MOINDX+NCODE(Iao)+IJNDX(Jao)+Jao
            BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Kao)*G2
            JKLNDX=MOINDX+NCODE(Kao)+IJNDX(Jao)+Jao
            BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Iao)*G2
            JKLNDX=MOINDX+NCODE(Jao)+IJNDX(Iao)+Kao
            BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Jao)*G2
            MOINDX=MOINDX+NJKL
            end do ! PMO
          end do ! I
        else
          stop 'I2E_transform, IJKJ BUFFER LENGTH = 0'
        end if
        call READ_ijkj (IJKJ_BUFFER, IJKJ_MAX, IJKJ_unit, LENGTH, EOF)
      end do
      end if ! ONDISK
      end if
!
! (IJ,JL)
      if(IJJL_count.GT.0)then
        do I=1,IJJL_count
        Iao=IJJL_INCORE(I)%I
        Jao=IJJL_INCORE(I)%J
        Kao=IJJL_INCORE(I)%L
        G1=IJJL_INCORE(I)%ijjl
        G2=IJJL_INCORE(I)%iljj
!
        MOINDX=0
        do PMO=MO1,MO2
        JKLNDX=MOINDX+NCODE(Iao)+IJNDX(Jao)+Kao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Jao)*G1
        JKLNDX=MOINDX+NCODE(Jao)+IJNDX(Jao)+Kao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Iao)*G1
        JKLNDX=MOINDX+NCODE(Jao)+IJNDX(Iao)+Jao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Kao)*G1
        JKLNDX=MOINDX+NCODE(Kao)+IJNDX(Iao)+Jao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Jao)*G1
!
        JKLNDX=MOINDX+NCODE(Iao)+IJNDX(Jao)+Jao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Kao)*G2
        JKLNDX=MOINDX+NCODE(Kao)+IJNDX(Jao)+Jao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Iao)*G2
        JKLNDX=MOINDX+NCODE(Jao)+IJNDX(Iao)+Kao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Jao)*G2
        MOINDX=MOINDX+NJKL
        end do ! PMO
        end do ! I
!
      if(LIJJL_ONDISK)then
        EOF=.FALSE.
        rewind IJJL_unit
        call READ_ijjl (IJJL_BUFFER, IJJL_MAX, IJJL_unit, LENGTH, EOF)
        do while (.not.EOF)
          if(LENGTH.GT.0)then
            do I=1,LENGTH
              Iao=IJJL_BUFFER(I)%I
              Jao=IJJL_BUFFER(I)%J
              Kao=IJJL_BUFFER(I)%L
              G1=IJJL_BUFFER(I)%ijjl
              G2=IJJL_BUFFER(I)%iljj
!
              MOINDX=0
              do PMO=MO1,MO2
              JKLNDX=MOINDX+NCODE(Iao)+IJNDX(Jao)+Kao
              BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Jao)*G1
              JKLNDX=MOINDX+NCODE(Jao)+IJNDX(Jao)+Kao
              BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Iao)*G1
              JKLNDX=MOINDX+NCODE(Jao)+IJNDX(Iao)+Jao
              BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Kao)*G1
              JKLNDX=MOINDX+NCODE(Kao)+IJNDX(Iao)+Jao
              BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Jao)*G1
!
              JKLNDX=MOINDX+NCODE(Iao)+IJNDX(Jao)+Jao
              BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Kao)*G2
              JKLNDX=MOINDX+NCODE(Kao)+IJNDX(Jao)+Jao
              BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Iao)*G2
              JKLNDX=MOINDX+NCODE(Jao)+IJNDX(Iao)+Kao
              BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Jao)*G2
              MOINDX=MOINDX+NJKL
              end do ! PMO
              end do ! I
          else
            stop 'I2E_transform, IJJL BUFFER LENGTH = 0'
          end if
        call READ_ijjl (IJJL_BUFFER, IJJL_MAX, IJJL_unit, LENGTH, EOF)
      end do
      end if ! ONDISK
      end if
!
! (II,KK)
      if(IIKK_count.GT.0)then
        do I=1,IIKK_count
        Iao=IIKK_INCORE(I)%I
        Jao=IIKK_INCORE(I)%K
        G1=IIKK_INCORE(I)%iikk
        G2=IIKK_INCORE(I)%ikik
!
        MOINDX=0
        do PMO=MO1,MO2
        JKLNDX=MOINDX+NCODE(Iao)+IJNDX(Jao)+Jao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Iao)*G1
        JKLNDX=MOINDX+NCODE(Jao)+IJNDX(Iao)+Iao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Jao)*G1
!
        JKLNDX=MOINDX+NCODE(Iao)+IJNDX(Iao)+Jao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Jao)*G2
        JKLNDX=MOINDX+NCODE(Jao)+IJNDX(Iao)+Jao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Iao)*G2
        MOINDX=MOINDX+NJKL
        end do ! PMO
        end do ! I
      end if
!
! (IJ,JJ)
      if(IJJJ_count.GT.0)then
        do I=1,IJJJ_count
        Iao=IJJJ_INCORE(I)%I
        Jao=IJJJ_INCORE(I)%J
        G1=IJJJ_INCORE(I)%ijjj
!
        MOINDX=0
        do PMO=MO1,MO2
        JKLNDX=MOINDX+NCODE(Iao)+IJNDX(Jao)+Jao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Jao)*G1
        JKLNDX=MOINDX+NCODE(Jao)+IJNDX(Jao)+Jao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Iao)*G1
        JKLNDX=MOINDX+NCODE(Jao)+IJNDX(Iao)+Jao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Jao)*G1
        MOINDX=MOINDX+NJKL
        end do ! PMO
        end do ! I
      end if
!
! (II,IL)
      if(IIIL_count.GT.0)then
        do I=1,IIIL_count
        Iao=IIIL_INCORE(I)%I
        Jao=IIIL_INCORE(I)%L
        G1=IIIL_INCORE(I)%iiil
!
        MOINDX=0
        do PMO=MO1,MO2
        JKLNDX=MOINDX+NCODE(Iao)+IJNDX(Iao)+Jao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Iao)*G1
        JKLNDX=MOINDX+NCODE(Iao)+IJNDX(Iao)+Iao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Jao)*G1
        JKLNDX=MOINDX+NCODE(Jao)+IJNDX(Iao)+Iao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Iao)*G1
        MOINDX=MOINDX+NJKL
        end do ! PMO
        end do ! I
      end if
!
! (II,II)
      if(IIII_count.GT.0)then
        do I=1,IIII_count
        Iao=IIII_INCORE(I)%I
        G1=IIII_INCORE(I)%iiii
!
        MOINDX=0
        do PMO=MO1,MO2
        JKLNDX=MOINDX+NCODE(Iao)+IJNDX(Iao)+Iao
        BPQRS(JKLNDX)=BPQRS(JKLNDX)+CMOS(PMO,Iao)*G1
        MOINDX=MOINDX+NJKL
        end do ! PMO
        end do ! I
      else
        stop 'I2E_transform, IIII BUFFER LENGTH = 0'
      end if
!
      write(UNIout,1010)
!
! End of routine I2EPMO
      call PRG_manager ('exit', 'I2EPMO', 'UTILITY')
      return
!
 1010 FORMAT(3X,'FIRST STEP OF TWO-ELECTRON TRANSFORMATION COMPLETE')
!
      end subroutine I2EPMO
      subroutine I2EQMO (CMOS, &
                         MO1, &
                         MO2, &
                         NMO, &
                         PQVVorOO)
!***********************************************************************
!     Date last modified: June 24, 1992                                *
!     Author: R. A. Poirier                                            *
!     Description: Transform second index of two-electron integrals.   *
!     Form  (PQ/kl) = SUM C(Q,j)*(Pj/kl), where j, k and l             *
!     extend over all AO, and P and Q over the MOs                     *
!                                                                      *
!     Routines called:                                                 *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants

      implicit none
!
! Input scalars:
      integer MO1,MO2,NMO
      logical PQVVorOO
!
! Input arrays:
      double precision CMOS(NMO,Nbasis)
!
! Local scalars:
      integer Jao,JKLNDX,JKNDX,JNDX,KL,MOINDX,MOP,PMO,QMO,QMOMAX,RMO,SMO
      double precision SUM
!
! Begin:
      call PRG_manager ('enter', 'I2EQMO', 'UTILITY')
!
! End of atomic integral input - Start next transformation step
! for these MO.  Should have all (Pj,kl).
!
! Copy all Pj for given kl.
      do KL=1,MATlen
        MOINDX=0
        MOP=0
        do PMO=MO1,MO2
        MOP=MOP+1
        if(PQVVorOO)then
          QMOMAX=PMO
        else
          QMOMAX=NMO
        end if
          do QMO=1,QMOMAX
            SUM=ZERO
          do Jao=1,Nbasis
            JKLNDX=MOINDX+NCODE(Jao)+KL
            SUM=SUM+BPQRS(JKLNDX)*CMOS(QMO,Jao)
          end do   ! End Jao loop.

          BPQMO(MOP,QMO)=SUM
          end do ! End QMO loop.
        MOINDX=MOINDX+NJKL
        end do ! End PMO loop.

! Now have all (PQ,kl) for given kl -- Copy to BPQRS.
        MOP=0
        MOINDX=0
        do PMO=MO1,MO2
          MOP=MOP+1
        if(PQVVorOO)then
          QMOMAX=PMO
        else
          QMOMAX=NMO
        end if
          do QMO=1,QMOMAX
            JKLNDX=MOINDX+(QMO-1)*MATlen+KL
            BPQRS(JKLNDX)=BPQMO(MOP,QMO)
          end do ! End QMO loop.
        MOINDX=MOINDX+NJKL
        end do ! End PMO loop.
!
      end do  ! End KL loop.
!
! Have all (PQ,kl) for all PQ and all kl.
!
! End of routine I2EQMO
      call PRG_manager ('exit', 'I2EQMO', 'UTILITY')
      return
      end subroutine I2EQMO
      subroutine I2ERMO (CMOS, &
                         MO1, &
                         MO2, &
                         NTRMO2, &
                         NTRMO3, &
                         PQVVorOO)
!***********************************************************************
!     Date last modified: June 24, 1992                                *
!     Author: R. A. Poirier                                            *
!     Description: Transform third index of two-electron AO integrals. *
!     Form  (PQ/Rl) = SUM C(R,k)*(PQ/kl), where k and l                *
!     extend over all AO, and P, Q, R over the MOs.                    *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer NTRMO2,NTRMO3
      logical PQVVorOO
!
! Input arrays:
      double precision CMOS(NTTRMO,Nbasis)
!
! Local scalars:
      integer JKLNDX,JKNDX,JNDX,Kao,Lao,MOINDX,MOP,MO1,MO2,NPQRL,PMO,QMO,QMOMAX,RMO,SMO
!
! Begin:
      call PRG_manager ('enter', 'I2ERMO', 'UTILITY')
!
      MOINDX=0
      NPQRL=0
      do PMO=MO1,MO2
      if(PQVVorOO)then
        QMOMAX=PMO
      else
        QMOMAX=NTRMO2
      end if
        do QMO=1,QMOMAX
!
          JNDX=MOINDX+NCODE(QMO)
          do Kao=1,Nbasis
            JKNDX=JNDX+IJNDX(Kao)
            do Lao=1,Kao
              JKLNDX=JKNDX+Lao
              BPQMO(Kao,Lao)=BPQRS(JKLNDX)
              BPQMO(Lao,Kao)=BPQRS(JKLNDX)
            end do ! End Lao loop.
          end do ! End Kao loop.
!
! Multiply CMOS*BPQMO and store result in BPQMO.
           BPQMO=matmul (CMOS, BPQMO)
!
! Have all (PQ,RL) for given PQ.
!
! Copy BPQMO into BPQRS.
          do RMO=1,PMO
            do Lao=1,Nbasis
              NPQRL=NPQRL+1

              if(npqrl.ge.jklndx)then
              write(6,*)' ERROR: npqrl exceeded jklndx '
              STOP
              end if

              BPQRS(NPQRL)=BPQMO(RMO,Lao)
            end do ! End Lao loop.
          end do ! End RMO loop.
!
        end do ! End QMO loop.
      MOINDX=MOINDX+NJKL
      end do ! End PMO loop.
!
! Now have all MO integrals (PQ,RL) in BPQRS.
!
! End of routine I2ERMO
      call PRG_manager ('exit', 'I2ERMO', 'UTILITY')
      return
      end subroutine I2ERMO
      subroutine I2ESMO (CMOS, &
                         MO1, &
                         MO2, &
                         NTRMO2, &
                         NTRMO3, &
                         PQVVorOO, &
                         RSVVorOO)
!***********************************************************************
!     Date last modified: June 24, 1992                                *
!     Author: R. A. Poirier                                            *
!     Description: Transform fourth index of two-electron integrals.   *
!     Form  (PQ/RS) = SUM C(S,l)*(PQ/Rl), where l                      *
!     extend over all AO, and P, Q, R, S over the MO.                  *
!***********************************************************************
! Modules:
      USE program_constants

      implicit none
!
! Input scalars:
      integer NTRMO2,NTRMO3
      logical PQVVorOO,RSVVorOO
!
! Input arrays:
      double precision CMOS(NTTRMO,Nbasis)
!
! Local scalars:
      integer Kao,Lao,MO1,MO2,NPQR,NPQRS,PMO,QMO,QMOMAX,RMO,SMO,SMOMAX
      double precision SUM
!
! Begin:
      call PRG_manager ('enter', 'I2ESMO', 'UTILITY')
!
      NPQR=0
      NPQRS=0
      do PMO=MO1,MO2
!
      if(PQVVorOO)then
        QMOMAX=PMO
      else
        QMOMAX=NTRMO2
      end if

      do QMO=1,QMOMAX
      do RMO=1,PMO
!
      if(RSVVorOO)then
        SMOMAX=RMO
      else
        SMOMAX=NTRMO3
      end if
      if(RMO.EQ.PMO)SMOMAX=QMO
!
      do SMO=1,SMOMAX
      SUM=ZERO
!
      do Lao=1,Nbasis
        SUM=SUM+BPQRS(NPQR+Lao)*CMOS(SMO,Lao)
      end do ! Lao

      BPQMO(RMO,SMO)=SUM
      end do  ! End SMO loop.
      NPQR=NPQR+Nbasis
      end do  ! End RMO loop.
!
      do RMO=1,PMO

      if(RSVVorOO)then
        SMOMAX=RMO
      else
        SMOMAX=NTRMO3
      end if
      if(RMO.EQ.PMO)SMOMAX=QMO
!
      do SMO=1,SMOMAX
        NPQRS=NPQRS+1
        BPQRS(NPQRS)=BPQMO(RMO,SMO)
      end do  ! End SMO loop.

      end do  ! End RMO loop.
      end do  ! End QMO loop.
      end do  ! End PMO loop.
!
! Now have all MO integrals (PQ,RS) in BPQRS.
!
! End of routine I2ESMO
      call PRG_manager ('exit', 'I2ESMO', 'UTILITY')
      return
      end subroutine I2ESMO
      subroutine I2RSMO (CMOO, &
                         CMOV, &
                         MO1, &
                         MO2, &
                         NMO1, &
                         NMO2, &
                         PQVVorOO, &
                         RSVVorOO)
!***********************************************************************
!     Date last modified: June 24, 1992                                *
!     Author: R. A. Poirier                                            *
!     Description: Transform third index of two-electron AO integrals. *
!     Form  (PQ/Rl) = SUM C(R,k)*(PQ/kl), where k and l                *
!     extend over all AO, and P, Q, R over the MOs.                    *
!***********************************************************************
! Modules:
      USE program_constants

      implicit none
!
! Input scalars:
      integer MO1,MO2,NMO1,NMO2
      logical PQVVorOO,RSVVorOO
!
! Input arrays:
      double precision CMOO(NMO1,Nbasis),CMOV(NMO2,Nbasis)
!
! Local scalars:
      integer JKLNDX,JKNDX,JNDX,Kao,Lao,MOINDX,MOP,NPQRS,PMO,QMO,QMOMAX,RMO,SMO,SMOMAX
      double precision SUM
      double precision, dimension(:,:), allocatable :: BPQRMO
!
! Begin:
      call PRG_manager ('enter', 'I2RSMO', 'UTILITY')
!
      allocate (BPQRMO(NMO1,Nbasis))
!
      MOINDX=0
      NPQRS=0
      do PMO=MO1,MO2
      if(PQVVorOO)then
        QMOMAX=PMO
      else
        QMOMAX=NTRVMO
      end if
        do QMO=1,QMOMAX
!
          JNDX=MOINDX+NCODE(QMO)
          do Kao=1,Nbasis
            JKNDX=JNDX+IJNDX(Kao)
            do Lao=1,Kao
              JKLNDX=JKNDX+Lao
              BPQMO(Kao,Lao)=BPQRS(JKLNDX)
              BPQMO(Lao,Kao)=BPQRS(JKLNDX)
            end do ! End Lao loop.
          end do ! End Kao loop.
!
! Multiply CMOO*BPQMO and store result in BPQMO.
           BPQRMO=matmul (CMOO, BPQMO)
!          call MTXBXA (BPQMO, Nbasis, Nbasis, Nbasis, Nbasis, CMOO, PMO, NTROMO, SCRVEC)
!
! Have all (PQ,RL) for given PQ.
!
! Copy BPQMO into BPQRS.
      do RMO=1,PMO
      if(RSVVorOO)then
        SMOMAX=RMO
      else
        SMOMAX=NTRVMO
      end if
      if(RMO.EQ.PMO)SMOMAX=QMO
!
      do SMO=1,SMOMAX
        SUM=ZERO
        do Lao=1,Nbasis
          SUM=SUM+BPQRMO(RMO,Lao)*CMOV(SMO,Lao)
        end do ! End Lao loop.
        NPQRS=NPQRS+1
        if(npqrS.gt.jklndx)then
          write(6,*)' ERROR: npqrs exceeded jklndx '
          STOP
        end if
        BPQRS(NPQRS)=SUM
      end do ! End SMO loop.
      end do ! End RMO loop.
!
      end do ! End QMO loop.
      MOINDX=MOINDX+NJKL
      end do ! End PMO loop.
!
! Now have all MO integrals (PQ,RL) in BPQRS.
      deallocate (BPQRMO)
!
! End of routine I2RSMO
      call PRG_manager ('exit', 'I2RSMO', 'UTILITY')
      return
      end subroutine I2RSMO
      end module integral_transformation
