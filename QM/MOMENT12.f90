      module type_one_electron_properties 
!***********************************************************************
!     Date last modified: August, 2003                     Version 1.0 *
!     Author: RAP & Aaron Kelly                                        *
!     Description: Scalars and arrays associated with moments          *
!***********************************************************************
! Modules:
      
      implicit none

      integer :: DEFCASE
      integer :: INDEX,I,J,IJao
      integer :: Ishell,Jshell,Ifrst,Jfrst,Ilast,Jlast
      integer :: Istart,Jstart,Iend,Jend
      integer :: Iatmshl,Jatmshl
      integer :: LAMAX,LBMAX
      integer :: Iatom,Jatom
      integer :: Irange,Jrange
      integer :: Igauss,Jgauss
      integer :: IGBGN,JGBGN,IGend,JGend
      integer :: Iao,Jao,AOI,AOJ
      double precision Cx,Cy,Cz,Dx,Dy,Dz
      double precision PAB,PABI,PABID2,PABA,PABB,AS,BS,FACT
      double precision PAx,PAy,PAz,PBx,PBy,PBz,Px,Py,Pz,FACTOR,FACTOR3,FACTOR33
      double precision OLAP,Ax,Ay,Az,Bx,By,Bz
      double precision SHLINTSX(36) 
      double precision SHLINTSY(36)
      double precision SHLINTSZ(36)
      double precision SHLINTSXX(36) 
      double precision SHLINTSXY(36)
      double precision SHLINTSXZ(36)
      double precision SHLINTSYY(36) 
      double precision SHLINTSYZ(36)
      double precision SHLINTSZZ(36) 
      
      end module type_one_electron_properties 
      subroutine MOMENT12_AO
!***********************************************************************
!     Date last modified: August, 2003                     Version 1.0 *
!     Author: RAP & Aaron Kelly                                        *
!     Description: Calculates the total electronic component of the    *
!                  dipole & second moment                              *
!***********************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE INT_objects
      USE type_molecule
      USE type_basis_set
      USE type_one_electron_properties

      implicit none

! Local scalars:
      double precision :: AN
!
! Local routines:
      external Dipole_Mom_ss,Dipole_Mom_sp,Dipole_Mom_pp,Dipole_Mom_sd,Dipole_Mom_pd,Dipole_Mom_dd
      external Second_Mom_ss,Second_Mom_sp,Second_Mom_pp,Second_Mom_sd,Second_Mom_pd,Second_Mom_dd

! Begin:
      call PRG_manager ('enter', 'MOMENT12_AO', 'UTILITY')

      if(allocated(DipoleX_AO))then
        deallocate(DipoleX_AO, DipoleY_AO, DipoleZ_AO)
      end if
      allocate(DipoleX_AO(MATlen), DipoleY_AO(MATlen), DipoleZ_AO(MATlen))

      if(allocated(SecMomXX_AO))then
        deallocate(SecMomXX_AO, SecMomXY_AO, SecMomXZ_AO, SecMomYY_AO, SecMomYZ_AO, SecMomZZ_AO)
      end if
      allocate(SecMomXX_AO(MATlen), SecMomXY_AO(MATlen), SecMomXZ_AO(MATlen), &
               SecMomYY_AO(MATlen), SecMomYZ_AO(MATlen), SecMomZZ_AO(MATlen))

!     write(UNIout,*)' debug_Moment> NSHELL: ',Basis%Nshells
!     write(UNIout,*)' debug_Moment> NPRIM: ',Basis%Nprimitives
!     write(UNIout,*)' debug_Moment> Nbasis: ',Basis%Nbasis
!     write(UNIout,*)' debug_Moment> MATlen: ',MATlen

      DipoleX_AO(1:MATlen)=ZERO 
      DipoleY_AO(1:MATlen)=ZERO
      DipoleZ_AO(1:MATlen)=ZERO
      SecMomXX_AO(1:MATlen)=ZERO
      SecMomXY_AO(1:MATlen)=ZERO
      SecMomXZ_AO(1:MATlen)=ZERO
      SecMomYY_AO(1:MATlen)=ZERO
      SecMomYZ_AO(1:MATlen)=ZERO
      SecMomZZ_AO(1:MATlen)=ZERO
 
! Use the CENTER_OF_CHARGE for now
      if(.not.LOEPXYZ_defined)then
        call CENTER_OF_CHARGE (OEPXYZ)
        write(UNIout,'(a,3F12.6)')'Property calculated at center of charge: ',OEPXYZ
      else
        write(UNIout,'(a,3F12.6)')'Property calculated at use specified point: ',OEPXYZ
      end if
      Cx=OEPXYZ(1)  !origin coords
      Cy=OEPXYZ(2)  !Dipole is Invar. wrt origin
      Cz=OEPXYZ(3)  !But SecMom isn't 

! Loop over Ishell.
      do Ishell=1,Basis%Nshells
      LAMAX=Basis%shell(Ishell)%Xtype+1
      Istart=Basis%shell(Ishell)%Xstart
      Iend=Basis%shell(Ishell)%XEND
      Irange=Iend-Istart+1
      IGBGN=Basis%shell(Ishell)%EXPBGN
      IGEND=Basis%shell(Ishell)%EXPEND
      Ifrst=Basis%shell(Ishell)%frstSHL

! Define gaussian exponents and contraction coefficients shell
! Loop over Jshell.
      do Jshell=1,Ishell
      LBMAX=Basis%shell(Jshell)%Xtype+1
      Jstart=Basis%shell(Jshell)%Xstart
      Jend=Basis%shell(Jshell)%XEND
      Jrange=Jend-Jstart+1
      JGBGN=Basis%shell(Jshell)%EXPBGN
      JGEND=Basis%shell(Jshell)%EXPEND
      Jfrst=Basis%shell(Jshell)%frstSHL

! Define gaussian exponents and contraction coefficients shell
      Ilast= Basis%shell(Ishell)%lastSHL
      Jlast= Basis%shell(Jshell)%lastSHL

! Define CASE:
! (LAMAX,LBMAX)=(1,1) => <s|s> => DEFCASE=1  
! (LAMAX,LBMAX)=(1,2) => <s|p> => DEFCASE=2  
! (LAMAX,LBMAX)=(2,2) => <p|p> => DEFCASE=6  

      DEFCASE=4*(LAMAX-1)+LBMAX

      select case (DEFCASE)
      CASE(1)
        call Moment_Int (Dipole_Mom_ss,Second_Mom_ss)
      CASE(2)
        call Moment_Int (Dipole_Mom_sp,Second_Mom_sp)
      CASE(6)
        call Moment_Int (Dipole_Mom_pp,Second_Mom_pp)
      CASE(3)
        call Moment_Int (Dipole_Mom_sd,Second_Mom_sd)
      CASE(7)
        call Moment_Int (Dipole_Mom_pd,Second_Mom_pd)
      CASE(11)
        call Moment_Int (Dipole_Mom_dd,Second_Mom_dd)
      case default
        write(UNIout,*)'ERROR> MOMENT12_AO: invalid case'
        stop 'ERROR> MOMENT12_AO: invalid case'
      end select

      end do ! Jshell
      end do ! Ishell
! End of loop over locations.

      call PRG_manager ('exit', 'MOMENT12_AO', 'UTILITY')
      return
      end subroutine MOMENT12_AO
      subroutine Moment_Int (Dipole_Mom_xx, Second_Mom_xx)
!***********************************************************************
!     Date last modified: AUG 5  , 2003                    Version 1.0 *
!     Author(s):  Aaron Kelly                                          *
!     description: Evaluate the AO integrals for the dipole moment     *
!***********************************************************************
! Modules:
      USE program_constants
      USE QM_objects
      USE INT_objects
      USE type_molecule
      USE type_basis_set
      USE type_one_electron_properties

      implicit none

      external Dipole_Mom_xx, Second_Mom_xx
!
! Local scalars:
      double precision :: RABSQ
!
! Begin:
      call PRG_manager ('enter', 'Dipole_Mom_Int', 'UTILITY')
!
! Loop over all shells
      do Iatmshl=Ifrst,Ilast
        Iatom=Basis%atmshl(Iatmshl)%ATMLST
        Ax=CARTESIAN(Iatom)%X
        Ay=CARTESIAN(Iatom)%Y
        Az=CARTESIAN(Iatom)%Z
        AOI=Basis%atmshl(Iatmshl)%frstAO-1
        if(Ishell.EQ.Jshell)Jlast=Iatmshl
      do Jatmshl=Jfrst,Jlast
        Jatom=Basis%atmshl(Jatmshl)%ATMLST
        Bx=CARTESIAN(Jatom)%X
        By=CARTESIAN(Jatom)%Y
        Bz=CARTESIAN(Jatom)%Z
        AOJ=Basis%atmshl(Jatmshl)%frstAO-1

        SHLINTSX(1:36)=ZERO ! temporary arrays will handle up to <p|p>
        SHLINTSY(1:36)=ZERO
        SHLINTSZ(1:36)=ZERO
        SHLINTSXX(1:36)=ZERO 
        SHLINTSXY(1:36)=ZERO
        SHLINTSXZ(1:36)=ZERO
        SHLINTSYY(1:36)=ZERO 
        SHLINTSYZ(1:36)=ZERO
        SHLINTSZZ(1:36)=ZERO

! This is the square of the norm of the vector connecting nuclei A & B
        RABSQ=(Ax-Bx)*(Ax-Bx)+(Ay-By)*(Ay-By)+(Az-Bz)*(Az-Bz)

! Loop over primitive gaussians
        do Igauss=IGBGN,IGEND
        AS=Basis%gaussian(Igauss)%exp
        do Jgauss=JGBGN,JGEND
        BS=Basis%gaussian(Jgauss)%exp
!
! Define needed geometric parameters (as seen in Daudel)
          PAB=AS+BS       
          PABI=ONE/PAB     ! GAMMA in Daudel
          PABID2=PABI/TWO
          PABA=AS*PABI
          PABB=BS*PABI

          PAx= PABB*(Bx-Ax)
          PAy= PABB*(By-Ay)
          PAz= PABB*(Bz-Az)
          PBx=-PABA*(Bx-Ax)
          PBy=-PABA*(By-Ay)
          PBz=-PABA*(Bz-Az)

          Px=PABA*Ax+PABB*Bx
          Py=PABA*Ay+PABB*By
          Pz=PABA*Az+PABB*Bz

          Dx=Px-Cx
          Dy=Py-Cy
          Dz=Pz-Cz

          FACT=dexp(-AS*BS*RABSQ*PABI)
          OLAP=(dsqrt(PI_VAL*PABI))**3
          FACTOR=Basis%gaussian(Igauss)%CONTRC*Basis%gaussian(Jgauss)%CONTRC*FACT*OLAP
          FACTOR3=FACTOR*DSQRT(3.0D0)
          FACTOR33=FACTOR*3.0D0

          call Dipole_Mom_xx
          call Second_Mom_xx

        end do ! Jgauss
        end do ! Igauss
         
        if(Iatmshl.eq.Jatmshl)then
          INDEX=0
          do I=1,Irange
          do J=1,Jrange
            INDEX=INDEX+1
            if(I.GE.J)then   
              Iao=AOI+I
             Jao=AOJ+J
              IJao=Iao*(Iao-1)/2+Jao
              DipoleX_AO(IJao)=SHLINTSX(INDEX)
              DipoleY_AO(IJao)=SHLINTSY(INDEX)
              DipoleZ_AO(IJao)=SHLINTSZ(INDEX)
              SecMomXX_AO(IJao)=SHLINTSXX(INDEX)
              SecMomXY_AO(IJao)=SHLINTSXY(INDEX)
              SecMomXZ_AO(IJao)=SHLINTSXZ(INDEX)
              SecMomYY_AO(IJao)=SHLINTSYY(INDEX)
              SecMomYZ_AO(IJao)=SHLINTSYZ(INDEX)
              SecMomZZ_AO(IJao)=SHLINTSZZ(INDEX)
            end if
          end do ! J
          end do ! I
        else
         INDEX=0
          do I=1,Irange
          do J=1,Jrange
            INDEX=INDEX+1
            Iao=AOI+I
            Jao=AOJ+J
              IJao=Iao*(Iao-1)/2+Jao
              DipoleX_AO(IJao)=SHLINTSX(INDEX)
              DipoleY_AO(IJao)=SHLINTSY(INDEX)
              DipoleZ_AO(IJao)=SHLINTSZ(INDEX)
              SecMomXX_AO(IJao)=SHLINTSXX(INDEX)
              SecMomXY_AO(IJao)=SHLINTSXY(INDEX)
              SecMomXZ_AO(IJao)=SHLINTSXZ(INDEX)
              SecMomYY_AO(IJao)=SHLINTSYY(INDEX)
              SecMomYZ_AO(IJao)=SHLINTSYZ(INDEX)
              SecMomZZ_AO(IJao)=SHLINTSZZ(INDEX)
          end do ! J
          end do ! I
        end if ! LIatmshl
       
        end do ! Jatmshl
        end do ! Iatmshl
! End of loop over atmshls.
!
! End of routine Diple_Mom_Int
      Call PRG_manager ('exit', 'Dipole_Moment', 'UTILITY')
      return
      end subroutine Moment_Int

      subroutine Dipole_Mom_ss
!***********************************************************************
!     Date last modified: AUG 5  , 2003                    Version 1.0 *
!     Author: Aaron Kelly                                              *
!     description: Evaluate the AO integrals for <s|s> Dipole moment   *
!***********************************************************************
! Modules:
      USE type_one_electron_properties

      implicit none
!
! <s|s>
       SHLINTSX(1)=SHLINTSX(1)+Dx*FACTOR
       SHLINTSY(1)=SHLINTSY(1)+Dy*FACTOR
       SHLINTSZ(1)=SHLINTSZ(1)+Dz*FACTOR
      
      return
      end subroutine Dipole_Mom_ss

      subroutine Dipole_Mom_sp
!***********************************************************************
!     Date last modified: AUG 5  , 2003                    Version 1.0 *
!     Author: Aaron Kelly                                              *
!     description: Evaluate the AO integrals for <s|p> dipole moment   *
!***********************************************************************
! Modules:
      USE type_one_electron_properties

      implicit none
!
! Begin:
! <s|p>
      SHLINTSX(1)=SHLINTSX(1)+(PBx*Dx+PABID2)*FACTOR
      SHLINTSY(1)=SHLINTSY(1)+PBx*Dy*FACTOR  
      SHLINTSZ(1)=SHLINTSZ(1)+PBx*Dz*FACTOR
      SHLINTSX(2)=SHLINTSX(2)+PBy*Dx*FACTOR
      SHLINTSY(2)=SHLINTSY(2)+(PBy*Dy+PABID2)*FACTOR
      SHLINTSZ(2)=SHLINTSZ(2)+PBy*Dz*FACTOR
      SHLINTSX(3)=SHLINTSX(3)+PBz*Dx*FACTOR
      SHLINTSY(3)=SHLINTSY(3)+PBz*Dy*FACTOR
      SHLINTSZ(3)=SHLINTSZ(3)+(PBz*Dz+PABID2)*FACTOR
!
! End of subroutine Dipole_Mom_sp
      return
      end subroutine Dipole_Mom_sp

      subroutine Dipole_Mom_pp
!***********************************************************************
!     Date last modified: AUG 5  , 2003                    Version 1.0 *
!     Author: AARON KELLY                                              *
!     description: Evaluate the AO integrals for the second moment     *
!***********************************************************************
! Modules:
      USE type_one_electron_properties

      implicit none
!
! Begin:
! <x|x> *
      SHLINTSX(1)=SHLINTSX(1)+FACTOR*(PABID2*(Dx+PAx+PBx)+PAx*PBx*Dx)
      SHLINTSY(1)=SHLINTSY(1)+FACTOR*(PABID2+PAx*PBx)*Dy
      SHLINTSZ(1)=SHLINTSZ(1)+FACTOR*(PABID2+PAx*PBx)*Dz
! <x|y> *
      SHLINTSX(2)=SHLINTSX(2)+FACTOR*(PABID2+PAx*Dx)*PBy
      SHLINTSY(2)=SHLINTSY(2)+FACTOR*(PABID2+PBy*Dy)*PAx
      SHLINTSZ(2)=SHLINTSZ(2)+FACTOR*Dz*PAx*PBy
! <x|z> *
      SHLINTSX(3)=SHLINTSX(3)+FACTOR*(PABID2+PAx*Dx)*PBz
      SHLINTSY(3)=SHLINTSY(3)+FACTOR*PAx*Dy*PBz
      SHLINTSZ(3)=SHLINTSZ(3)+FACTOR*(PABID2+Dz*PBz)*PAx
! <y|x> *
      SHLINTSX(4)=SHLINTSX(4)+FACTOR*(PABID2+Dx*PBx)*PAy
      SHLINTSY(4)=SHLINTSY(4)+FACTOR*(PABID2+Dy*PAy)*PBx
      SHLINTSZ(4)=SHLINTSZ(4)+FACTOR*PBx*PAy*Dz
! <y|y> *
      SHLINTSX(5)=SHLINTSX(5)+FACTOR*(PABID2+PAy*PBy)*Dx
      SHLINTSY(5)=SHLINTSY(5)+FACTOR*(PABID2*(Dy+PAy+PBy)+PAy*PBy*Dy)
      SHLINTSZ(5)=SHLINTSZ(5)+FACTOR*(PABID2+PBy*PAy)*Dz
! <y|z> *
      SHLINTSX(6)=SHLINTSX(6)+FACTOR*PAy*PBz*Dx
      SHLINTSY(6)=SHLINTSY(6)+FACTOR*(PABID2+PAy*Dy)*PBz
      SHLINTSZ(6)=SHLINTSZ(6)+FACTOR*(PABID2+PBz*Dz)*PAy
! <z|x> *
      SHLINTSX(7)=SHLINTSX(7)+FACTOR*(PABID2+Dx*PBx)*PAz
      SHLINTSY(7)=SHLINTSY(7)+FACTOR*PAz*PBx*Dy
      SHLINTSZ(7)=SHLINTSZ(7)+FACTOR*(PABID2+Dz*PAz)*PBx
! <z|y> *
      SHLINTSX(8)=SHLINTSX(8)+FACTOR*PAz*PBy*Dx
      SHLINTSY(8)=SHLINTSY(8)+FACTOR*(PABID2+Dy*PBy)*PAz
      SHLINTSZ(8)=SHLINTSZ(8)+FACTOR*(PABID2+PAz*Dz)*PBy
! <z|z> *
      SHLINTSX(9)=SHLINTSX(9)+FACTOR*(PABID2+PAz*PBz)*Dx
      SHLINTSY(9)=SHLINTSY(9)+FACTOR*(PABID2+PAz*PBz)*Dy
      SHLINTSZ(9)=SHLINTSZ(9)+FACTOR*(PABID2*(Dz+PAz+PBz)+PAz*PBz*Dz)

      return
      end subroutine Dipole_Mom_pp
      subroutine Dipole_Mom_sd
!***********************************************************************
!     Date last modified: April 26, 2004                   Version 1.0 *
!     Author: Aaron Kelly and R. Poirier                               *
!     description: Evaluate the AO integrals for <s|d> Dipole moment   *
!***********************************************************************
! Modules:
      USE type_one_electron_properties

      implicit none
!
! <s|d>
! <S|Z|d>
      SHLINTSZ( 1)=SHLINTSZ( 1)+FACTOR*(PBx**2+PABID2)*(Dz)
      SHLINTSZ( 2)=SHLINTSZ( 2)+FACTOR*(PBy**2+PABID2)*(Dz)
      SHLINTSZ( 3)=SHLINTSZ( 3)+FACTOR*(Dz*PBz**2+PBz*PABI+Dz*PABID2)
      SHLINTSZ( 4)=SHLINTSZ( 4)+FACTOR3*(PBx)*(PBy)*(Dz)
      SHLINTSZ( 5)=SHLINTSZ( 5)+FACTOR3*(PBx)*(Dz*PBz+PABID2)
      SHLINTSZ( 6)=SHLINTSZ( 6)+FACTOR3*(PBy)*(Dz*PBz+PABID2)
! <S|Y|d>
      SHLINTSY( 1)=SHLINTSY( 1)+FACTOR*(PBx**2+PABID2)*(Dy)
      SHLINTSY( 2)=SHLINTSY( 2)+FACTOR*(Dy*PBy**2+PBy*PABI+Dy*PABID2)
      SHLINTSY( 3)=SHLINTSY( 3)+FACTOR*(Dy)*(PBz**2+PABID2)
      SHLINTSY( 4)=SHLINTSY( 4)+FACTOR3*(PBx)*(Dy*PBy+PABID2)
      SHLINTSY( 5)=SHLINTSY( 5)+FACTOR3*(PBx)*(Dy)*(PBz)
      SHLINTSY( 6)=SHLINTSY( 6)+FACTOR3*(Dy*PBy+PABID2)*(PBz)
! <S|X|d>      
      SHLINTSX( 1)=SHLINTSX( 1)+FACTOR*(Dx*PBx**2+PBx*PABI+Dx*PABID2)
      SHLINTSX( 2)=SHLINTSX( 2)+FACTOR*(Dx)*(PBy**2+PABID2)
      SHLINTSX( 3)=SHLINTSX( 3)+FACTOR*(Dx)*(PBz**2+PABID2)
      SHLINTSX( 4)=SHLINTSX( 4)+FACTOR3*(Dx*PBx+PABID2)*(PBy)
      SHLINTSX( 5)=SHLINTSX( 5)+FACTOR3*(Dx*PBx+PABID2)*(PBz)
      SHLINTSX( 6)=SHLINTSX( 6)+FACTOR3*(Dx)*(PBy)*(PBz)
      
      return
      end subroutine Dipole_Mom_sd
      subroutine Dipole_Mom_pd
!***********************************************************************
!     Date last modified: April 26, 2004                   Version 1.0 *
!     Author: Aaron Kelly and R. Poirier                               *
!     description: Evaluate the AO integrals for <p|d> Dipole moment   *
!***********************************************************************
! Modules:
      USE type_one_electron_properties

      implicit none

! Local scalars:
      double precision :: t1
!
      t1=3.0D0*PABID2**2
! <p|d>
! <p|Z|d>
      SHLINTSZ( 1)=SHLINTSZ( 1)+FACTOR*(PAx*PBx**2+PAx*PABID2+PBx*PABI)*(Dz)
      SHLINTSZ( 2)=SHLINTSZ( 2)+FACTOR*(PAx)*(PBy**2+PABID2)*(Dz)
      SHLINTSZ( 3)=SHLINTSZ( 3)+FACTOR*(PAx)*(Dz*PBz**2+PBz*PABI+Dz*PABID2)
      SHLINTSZ( 4)=SHLINTSZ( 4)+FACTOR3*(PAx*PBx+PABID2)*(PBy)*(Dz)
      SHLINTSZ( 5)=SHLINTSZ( 5)+FACTOR3*(PAx*PBx+PABID2)*(Dz*PBz+PABID2)
      SHLINTSZ( 6)=SHLINTSZ( 6)+FACTOR3*(PAx)*(PBy)*(Dz*PBz+PABID2)
      SHLINTSZ( 7)=SHLINTSZ( 7)+FACTOR*(PBx**2+PABID2)*(PAy)*(Dz)
      SHLINTSZ( 8)=SHLINTSZ( 8)+FACTOR*(PAy*PBy**2+PAy*PABID2+PBy*PABI)*(Dz)
      SHLINTSZ( 9)=SHLINTSZ( 9)+FACTOR*(PAy)*(Dz*PBz**2+PBz*PABI+Dz*PABID2)
      SHLINTSZ(10)=SHLINTSZ(10)+FACTOR3*(PBx)*(PAy*PBy+PABID2)*(Dz)
      SHLINTSZ(11)=SHLINTSZ(11)+FACTOR3*(PBx)*(PAy)*(Dz*PBz+PABID2)
      SHLINTSZ(12)=SHLINTSZ(12)+FACTOR3*(PAy*PBy+PABID2)*(Dz*PBz+PABID2)
      SHLINTSZ(13)=SHLINTSZ(13)+FACTOR*(PBx**2+PABID2)*(Dz*PAz+PABID2)
      SHLINTSZ(14)=SHLINTSZ(14)+FACTOR*(PBy**2+PABID2)*(Dz*PAz+PABID2)
      SHLINTSZ(15)=SHLINTSZ(15)+FACTOR*(Dz*PAz*PBz**2+PAz*PBz*PABI+Dz*PAz*PABID2+PBz**2*PABID2+ &
                                        Dz*PBz*PABI+t1)
      SHLINTSZ(16)=SHLINTSZ(16)+FACTOR3*(PBx)*(PBy)*(Dz*PAz+PABID2)
      SHLINTSZ(17)=SHLINTSZ(17)+FACTOR3*(PBx)*(Dz*PAz*PBz+PAz*PABID2+PBz*PABID2+Dz*PABID2)
      SHLINTSZ(18)=SHLINTSZ(18)+FACTOR3*(PBy)*(Dz*PAz*PBz+PAz*PABID2+PBz*PABID2+Dz*PABID2)
! <p|Y|d>
      SHLINTSY( 1)=SHLINTSY( 1)+FACTOR*(PAx*PBx**2+PAx*PABID2+PBx*PABI)*(Dy)
      SHLINTSY( 2)=SHLINTSY( 2)+FACTOR*(PAx)*(Dy*PBy**2+PBy*PABI+Dy*PABID2)
      SHLINTSY( 3)=SHLINTSY( 3)+FACTOR*(PAx)*(Dy)*(PBz**2+PABID2)
      SHLINTSY( 4)=SHLINTSY( 4)+FACTOR3*(PAx*PBx+PABID2)*(Dy*PBy+PABID2)
      SHLINTSY( 5)=SHLINTSY( 5)+FACTOR3*(PAx*PBx+PABID2)*(Dy)*(PBz)
      SHLINTSY( 6)=SHLINTSY( 6)+FACTOR3*(PAx)*(Dy*PBy+PABID2)*(PBz)
      SHLINTSY( 7)=SHLINTSY( 7)+FACTOR*(PBx**2+PABID2)*(Dy*PAy+PABID2)
      SHLINTSY( 8)=SHLINTSY( 8)+FACTOR*(Dy*PAy*PBy**2+PAy*PBy*PABI+Dy*PAy*PABID2+PBy**2*PABID2+ &
                                        Dy*PBy*PABI+t1)
      SHLINTSY( 9)=SHLINTSY( 9)+FACTOR*(Dy*PAy+PABID2)*(PBz**2+PABID2)
      SHLINTSY(10)=SHLINTSY(10)+FACTOR3*(PBx)*(Dy*PAy*PBy+PAy*PABID2+PBy*PABID2+Dy*PABID2)
      SHLINTSY(11)=SHLINTSY(11)+FACTOR3*(PBx)*(Dy*PAy+PABID2)*(PBz)
      SHLINTSY(12)=SHLINTSY(12)+FACTOR3*(Dy*PAy*PBy+PAy*PABID2+PBy*PABID2+Dy*PABID2)*(PBz)
      SHLINTSY(13)=SHLINTSY(13)+FACTOR*(PBx**2+PABID2)*(Dy)*(PAz)
      SHLINTSY(14)=SHLINTSY(14)+FACTOR*(Dy*PBy**2+PBy*PABI+Dy*PABID2)*(PAz)
      SHLINTSY(15)=SHLINTSY(15)+FACTOR*(Dy)*(PAz*PBz**2+PAz*PABID2+PBz*PABI)
      SHLINTSY(16)=SHLINTSY(16)+FACTOR3*(PBx)*(Dy*PBy+PABID2)*(PAz)
      SHLINTSY(17)=SHLINTSY(17)+FACTOR3*(PBx)*(Dy)*(PAz*PBz+PABID2)
      SHLINTSY(18)=SHLINTSY(18)+FACTOR3*(Dy*PBy+PABID2)*(PAz*PBz+PABID2)
! <p|X|d>      
      SHLINTSX( 1)=SHLINTSX( 1)+FACTOR*(Dx*PAx*PBx**2+PAx*PBx*PABI+Dx*PAx*PABID2+PBx**2*PABID2+ &
                                        Dx*PBx*PABI+t1)
      SHLINTSX( 2)=SHLINTSX( 2)+FACTOR*(Dx*PAx+PABID2)*(PBy**2+PABID2)
      SHLINTSX( 3)=SHLINTSX( 3)+FACTOR*(Dx*PAx+PABID2)*(PBz**2+PABID2)
      SHLINTSX( 4)=SHLINTSX( 4)+FACTOR3*(Dx*PAx*PBx+PAx*PABID2+PBx*PABID2+Dx*PABID2)*(PBy)
      SHLINTSX( 5)=SHLINTSX( 5)+FACTOR3*(Dx*PAx*PBx+PAx*PABID2+PBx*PABID2+Dx*PABID2)*(PBz)
      SHLINTSX( 6)=SHLINTSX( 6)+FACTOR3*(Dx*PAx+PABID2)*(PBy)*(PBz)
      SHLINTSX( 7)=SHLINTSX( 7)+FACTOR*(Dx*PBx**2+PBx*PABI+Dx*PABID2)*(PAy)
      SHLINTSX( 8)=SHLINTSX( 8)+FACTOR*(Dx)*(PAy*PBy**2+PAy*PABID2+PBy*PABI)
      SHLINTSX( 9)=SHLINTSX( 9)+FACTOR*(Dx)*(PAy)*(PBz**2+PABID2)
      SHLINTSX(10)=SHLINTSX(10)+FACTOR3*(Dx*PBx+PABID2)*(PAy*PBy+PABID2)
      SHLINTSX(11)=SHLINTSX(11)+FACTOR3*(Dx*PBx+PABID2)*(PAy)*(PBz)
      SHLINTSX(12)=SHLINTSX(12)+FACTOR3*(Dx)*(PAy*PBy+PABID2)*(PBz)
      SHLINTSX(13)=SHLINTSX(13)+FACTOR*(Dx*PBx**2+PBx*PABI+Dx*PABID2)*(PAz)
      SHLINTSX(14)=SHLINTSX(14)+FACTOR*(Dx)*(PBy**2+PABID2)*(PAz)
      SHLINTSX(15)=SHLINTSX(15)+FACTOR*(Dx)*(PAz*PBz**2+PAz*PABID2+PBz*PABI)
      SHLINTSX(16)=SHLINTSX(16)+FACTOR3*(Dx*PBx+PABID2)*(PBy)*(PAz)
      SHLINTSX(17)=SHLINTSX(17)+FACTOR3*(Dx*PBx+PABID2)*(PAz*PBz+PABID2)
      SHLINTSX(18)=SHLINTSX(18)+FACTOR3*(Dx)*(PBy)*(PAz*PBz+PABID2)
      
      return
      end subroutine Dipole_Mom_pd
      subroutine Dipole_Mom_dd
!***********************************************************************
!     Date last modified: April 26, 2004                   Version 1.0 *
!     Author: Aaron Kelly and R. Poirier                               *
!     description: Evaluate the AO integrals for <d|d> Dipole moment   *
!***********************************************************************
! Modules:
      USE type_one_electron_properties

      implicit none
!
! Local scalars:
      double precision :: t1,t2
!
      t1=3.0D0*PABID2**2
      t2=2.0D0*t1
! <d|d>
! <d|Z|d>
      SHLINTSZ( 1)=SHLINTSZ( 1)+FACTOR*(PAx**2*PBx**2+PAx**2*PABID2+PAx*PBx*2.0D0*PABI+ &
                                        PBx**2*PABID2+t1)*(Dz)
      SHLINTSZ( 2)=SHLINTSZ( 2)+FACTOR*(PAx**2+PABID2)*(PBy**2+PABID2)*(Dz)
      SHLINTSZ( 3)=SHLINTSZ( 3)+FACTOR*(PAx**2+PABID2)*(Dz*PBz**2+PBz*PABI+Dz*PABID2)
      SHLINTSZ( 4)=SHLINTSZ( 4)+FACTOR3*(PAx**2*PBx+PAx*PABI+PBx*PABID2)*(PBy)*(Dz)
      SHLINTSZ( 5)=SHLINTSZ( 5)+FACTOR3*(PAx**2*PBx+PAx*PABI+PBx*PABID2)*(Dz*PBz+PABID2)
      SHLINTSZ( 6)=SHLINTSZ( 6)+FACTOR3*(PAx**2+PABID2)*(PBy)*(Dz*PBz+PABID2)
      SHLINTSZ( 7)=SHLINTSZ( 7)+FACTOR*(PBx**2+PABID2)*(PAy**2+PABID2)*(Dz)
      SHLINTSZ( 8)=SHLINTSZ( 8)+FACTOR*(PAy**2*PBy**2+PAy**2*PABID2+PAy*PBy*2.0D0*PABI+ &
                                        PBy**2*PABID2+t1)*(Dz)
      SHLINTSZ( 9)=SHLINTSZ( 9)+FACTOR*(PAy**2+PABID2)*(Dz*PBz**2+PBz*PABI+Dz*PABID2)
      SHLINTSZ(10)=SHLINTSZ(10)+FACTOR3*(PBx)*(PAy**2*PBy+PAy*PABI+PBy*PABID2)*(Dz)
      SHLINTSZ(11)=SHLINTSZ(11)+FACTOR3*(PBx)*(PAy**2+PABID2)*(Dz*PBz+PABID2)
      SHLINTSZ(12)=SHLINTSZ(12)+FACTOR3*(PAy**2*PBy+PAy*PABI+PBy*PABID2)*(Dz*PBz+PABID2)
      SHLINTSZ(13)=SHLINTSZ(13)+FACTOR*(PBx**2+PABID2)*(Dz*PAz**2+PAz*PABI+Dz*PABID2)
      SHLINTSZ(14)=SHLINTSZ(14)+FACTOR*(PBy**2+PABID2)*(Dz*PAz**2+PAz*PABI+Dz*PABID2)
      SHLINTSZ(15)=SHLINTSZ(15)+FACTOR*(Dz*PAz**2*PBz**2+PAz**2*PBz*PABI+Dz*PAz**2*PABID2+ &
                                        PAz*PBz**2*PABI+Dz*PAz*PBz*2.0D0*PABI+PAz*t2+ &
                                        Dz*PBz**2*PABID2+PBz*t2+Dz*t1)
      SHLINTSZ(16)=SHLINTSZ(16)+FACTOR3*(PBx)*(PBy)*(Dz*PAz**2+PAz*PABI+Dz*PABID2)
      SHLINTSZ(17)=SHLINTSZ(17)+FACTOR3*(PBx)*(Dz*PAz**2*PBz+PAz**2*PABID2+PAz*PBz*PABI+ &
                                         Dz*PAz*PABI+Dz*PBz*PABID2+t1)
      SHLINTSZ(18)=SHLINTSZ(18)+FACTOR3*(PBy)*(Dz*PAz**2*PBz+PAz**2*PABID2+PAz*PBz*PABI+ &
                                         Dz*PAz*PABI+Dz*PBz*PABID2+t1)
      SHLINTSZ(19)=SHLINTSZ(19)+FACTOR3*(PAx*PBx**2+PAx*PABID2+PBx*PABI)*(PAy)*(Dz)
      SHLINTSZ(20)=SHLINTSZ(20)+FACTOR3*(PAx)*(PAy*PBy**2+PAy*PABID2+PBy*PABI)*(Dz)
      SHLINTSZ(21)=SHLINTSZ(21)+FACTOR3*(PAx)*(PAy)*(Dz*PBz**2+PBz*PABI+Dz*PABID2)
      SHLINTSZ(22)=SHLINTSZ(22)+FACTOR33*(PAx*PBx+PABID2)*(PAy*PBy+PABID2)*(Dz)
      SHLINTSZ(23)=SHLINTSZ(23)+FACTOR33*(PAx*PBx+PABID2)*(PAy)*(Dz*PBz+PABID2)
      SHLINTSZ(24)=SHLINTSZ(24)+FACTOR33*(PAx)*(PAy*PBy+PABID2)*(Dz*PBz+PABID2)
      SHLINTSZ(25)=SHLINTSZ(25)+FACTOR3*(PAx*PBx**2+PAx*PABID2+PBx*PABI)*(Dz*PAz+PABID2)
      SHLINTSZ(26)=SHLINTSZ(26)+FACTOR3*(PAx)*(PBy**2+PABID2)*(Dz*PAz+PABID2)
      SHLINTSZ(27)=SHLINTSZ(27)+FACTOR3*(PAx)*(Dz*PAz*PBz**2+PAz*PBz*PABI+Dz*PAz*PABID2+ &
                                         PBz**2*PABID2+Dz*PBz*PABI+t1)
      SHLINTSZ(28)=SHLINTSZ(28)+FACTOR33*(PAx*PBx+PABID2)*(PBy)*(Dz*PAz+PABID2)
      SHLINTSZ(29)=SHLINTSZ(29)+FACTOR33*(PAx*PBx+PABID2)*(Dz*PAz*PBz+PAz*PABID2+PBz*PABID2+Dz*PABID2)
      SHLINTSZ(30)=SHLINTSZ(30)+FACTOR33*(PAx)*(PBy)*(Dz*PAz*PBz+PAz*PABID2+PBz*PABID2+Dz*PABID2)
      SHLINTSZ(31)=SHLINTSZ(31)+FACTOR3*(PBx**2+PABID2)*(PAy)*(Dz*PAz+PABID2)
      SHLINTSZ(32)=SHLINTSZ(32)+FACTOR3*(PAy*PBy**2+PAy*PABID2+PBy*PABI)*(Dz*PAz+PABID2)
      SHLINTSZ(33)=SHLINTSZ(33)+FACTOR3*(PAy)*(Dz*PAz*PBz**2+PAz*PBz*PABI+Dz*PAz*PABID2+ &
                                         PBz**2*PABID2+Dz*PBz*PABI+t1)
      SHLINTSZ(34)=SHLINTSZ(34)+FACTOR33*(PBx)*(PAy*PBy+PABID2)*(Dz*PAz+PABID2)
      SHLINTSZ(35)=SHLINTSZ(35)+FACTOR33*(PBx)*(PAy)*(Dz*PAz*PBz+PAz*PABID2+PBz*PABID2+Dz*PABID2)
      SHLINTSZ(36)=SHLINTSZ(36)+FACTOR33*(PAy*PBy+PABID2)*(Dz*PAz*PBz+PAz*PABID2+PBz*PABID2+Dz*PABID2)
! <d|Y|d>
      SHLINTSY( 1)=SHLINTSY( 1)+FACTOR*(PAx**2*PBx**2+PAx**2*PABID2+PAx*PBx*2.0D0*PABI+ &
                                        PBx**2*PABID2+t1)*(Dy)
      SHLINTSY( 2)=SHLINTSY( 2)+FACTOR*(PAx**2+PABID2)*(Dy*PBy**2+PBy*PABI+Dy*PABID2)
      SHLINTSY( 3)=SHLINTSY( 3)+FACTOR*(PAx**2+PABID2)*(Dy)*(PBz**2+PABID2)
      SHLINTSY( 4)=SHLINTSY( 4)+FACTOR3*(PAx**2*PBx+PAx*PABI+PBx*PABID2)*(Dy*PBy+PABID2)
      SHLINTSY( 5)=SHLINTSY( 5)+FACTOR3*(PAx**2*PBx+PAx*PABI+PBx*PABID2)*(Dy)*(PBz)
      SHLINTSY( 6)=SHLINTSY( 6)+FACTOR3*(PAx**2+PABID2)*(Dy*PBy+PABID2)*(PBz)
      SHLINTSY( 7)=SHLINTSY( 7)+FACTOR*(PBx**2+PABID2)*(Dy*PAy**2+PAy*PABI+Dy*PABID2)
      SHLINTSY( 8)=SHLINTSY( 8)+FACTOR*(Dy*PAy**2*PBy**2+PAy**2*PBy*PABI+Dy*PAy**2*PABID2+ &
                                        PAy*PBy**2*PABI+Dy*PAy*PBy*2.0D0*PABI+PAy*t2+ &
                                        Dy*PBy**2*PABID2+PBy*t2+Dy*t1)
      SHLINTSY( 9)=SHLINTSY( 9)+FACTOR*(Dy*PAy**2+PAy*PABI+Dy*PABID2)*(PBz**2+PABID2)
      SHLINTSY(10)=SHLINTSY(10)+FACTOR3*(PBx)*(Dy*PAy**2*PBy+PAy**2*PABID2+PAy*PBy*PABI+ &
                                         Dy*PAy*PABI+Dy*PBy*PABID2+t1)
      SHLINTSY(11)=SHLINTSY(11)+FACTOR3*(PBx)*(Dy*PAy**2+PAy*PABI+Dy*PABID2)*(PBz)
      SHLINTSY(12)=SHLINTSY(12)+FACTOR3*(Dy*PAy**2*PBy+PAy**2*PABID2+PAy*PBy*PABI+Dy*PAy*PABI+ &
                                         Dy*PBy*PABID2+t1)*(PBz)
      SHLINTSY(13)=SHLINTSY(13)+FACTOR*(PBx**2+PABID2)*(Dy)*(PAz**2+PABID2)
      SHLINTSY(14)=SHLINTSY(14)+FACTOR*(Dy*PBy**2+PBy*PABI+Dy*PABID2)*(PAz**2+PABID2)
      SHLINTSY(15)=SHLINTSY(15)+FACTOR*(Dy)*(PAz**2*PBz**2+PAz**2*PABID2+PAz*PBz*2.0D0*PABI+ &
                                        PBz**2*PABID2+t1)
      SHLINTSY(16)=SHLINTSY(16)+FACTOR3*(PBx)*(Dy*PBy+PABID2)*(PAz**2+PABID2)
      SHLINTSY(17)=SHLINTSY(17)+FACTOR3*(PBx)*(Dy)*(PAz**2*PBz+PAz*PABI+PBz*PABID2)
      SHLINTSY(18)=SHLINTSY(18)+FACTOR3*(Dy*PBy+PABID2)*(PAz**2*PBz+PAz*PABI+PBz*PABID2)
      SHLINTSY(19)=SHLINTSY(19)+FACTOR3*(PAx*PBx**2+PAx*PABID2+PBx*PABI)*(Dy*PAy+PABID2)
      SHLINTSY(20)=SHLINTSY(20)+FACTOR3*(PAx)*(Dy*PAy*PBy**2+PAy*PBy*PABI+Dy*PAy*PABID2+ &
                                         PBy**2*PABID2+Dy*PBy*PABI+t1)
      SHLINTSY(21)=SHLINTSY(21)+FACTOR3*(PAx)*(Dy*PAy+PABID2)*(PBz**2+PABID2)
      SHLINTSY(22)=SHLINTSY(22)+FACTOR33*(PAx*PBx+PABID2)*(Dy*PAy*PBy+PAy*PABID2+PBy*PABID2+Dy*PABID2)
      SHLINTSY(23)=SHLINTSY(23)+FACTOR33*(PAx*PBx+PABID2)*(Dy*PAy+PABID2)*(PBz)
      SHLINTSY(24)=SHLINTSY(24)+FACTOR33*(PAx)*(Dy*PAy*PBy+PAy*PABID2+PBy*PABID2+Dy*PABID2)*(PBz)
      SHLINTSY(25)=SHLINTSY(25)+FACTOR3*(PAx*PBx**2+PAx*PABID2+PBx*PABI)*(Dy)*(PAz)
      SHLINTSY(26)=SHLINTSY(26)+FACTOR3*(PAx)*(Dy*PBy**2+PBy*PABI+Dy*PABID2)*(PAz)
      SHLINTSY(27)=SHLINTSY(27)+FACTOR3*(PAx)*(Dy)*(PAz*PBz**2+PAz*PABID2+PBz*PABI)
      SHLINTSY(28)=SHLINTSY(28)+FACTOR33*(PAx*PBx+PABID2)*(Dy*PBy+PABID2)*(PAz)
      SHLINTSY(29)=SHLINTSY(29)+FACTOR33*(PAx*PBx+PABID2)*(Dy)*(PAz*PBz+PABID2)
      SHLINTSY(30)=SHLINTSY(30)+FACTOR33*(PAx)*(Dy*PBy+PABID2)*(PAz*PBz+PABID2)
      SHLINTSY(31)=SHLINTSY(31)+FACTOR3*(PBx**2+PABID2)*(Dy*PAy+PABID2)*(PAz)
      SHLINTSY(32)=SHLINTSY(32)+FACTOR3*(Dy*PAy*PBy**2+PAy*PBy*PABI+Dy*PAy*PABID2+PBy**2*PABID2+ &
                                         Dy*PBy*PABI+t1)*(PAz)
      SHLINTSY(33)=SHLINTSY(33)+FACTOR3*(Dy*PAy+PABID2)*(PAz*PBz**2+PAz*PABID2+PBz*PABI)
      SHLINTSY(34)=SHLINTSY(34)+FACTOR33*(PBx)*(Dy*PAy*PBy+PAy*PABID2+PBy*PABID2+Dy*PABID2)*(PAz)
      SHLINTSY(35)=SHLINTSY(35)+FACTOR33*(PBx)*(Dy*PAy+PABID2)*(PAz*PBz+PABID2)
      SHLINTSY(36)=SHLINTSY(36)+FACTOR33*(Dy*PAy*PBy+PAy*PABID2+PBy*PABID2+Dy*PABID2)*(PAz*PBz+PABID2)
! <d|X|d>      
      SHLINTSX( 1)=SHLINTSX( 1)+FACTOR*(Dx*PAx**2*PBx**2+PAx**2*PBx*PABI+Dx*PAx**2*PABID2+ &
                                        PAx*PBx**2*PABI+Dx*PAx*PBx*2.0D0*PABI+PAx*t2+ &
                                        Dx*PBx**2*PABID2+PBx*t2+Dx*t1)
      SHLINTSX( 2)=SHLINTSX( 2)+FACTOR*(Dx*PAx**2+PAx*PABI+Dx*PABID2)*(PBy**2+PABID2)
      SHLINTSX( 3)=SHLINTSX( 3)+FACTOR*(Dx*PAx**2+PAx*PABI+Dx*PABID2)*(PBz**2+PABID2)
      SHLINTSX( 4)=SHLINTSX( 4)+FACTOR3*(Dx*PAx**2*PBx+PAx**2*PABID2+PAx*PBx*PABI+Dx*PAx*PABI+ &
                                         Dx*PBx*PABID2+t1)*(PBy)
      SHLINTSX( 5)=SHLINTSX( 5)+FACTOR3*(Dx*PAx**2*PBx+PAx**2*PABID2+PAx*PBx*PABI+Dx*PAx*PABI+ &
                                         Dx*PBx*PABID2+t1)*(PBz)
      SHLINTSX( 6)=SHLINTSX( 6)+FACTOR3*(Dx*PAx**2+PAx*PABI+Dx*PABID2)*(PBy)*(PBz)
      SHLINTSX( 7)=SHLINTSX( 7)+FACTOR*(Dx*PBx**2+PBx*PABI+Dx*PABID2)*(PAy**2+PABID2)
      SHLINTSX( 8)=SHLINTSX( 8)+FACTOR*(Dx)*(PAy**2*PBy**2+PAy**2*PABID2+PAy*PBy*2.0D0*PABI+ &
                                        PBy**2*PABID2+t1)
      SHLINTSX( 9)=SHLINTSX( 9)+FACTOR*(Dx)*(PAy**2+PABID2)*(PBz**2+PABID2)
      SHLINTSX(10)=SHLINTSX(10)+FACTOR3*(Dx*PBx+PABID2)*(PAy**2*PBy+PAy*PABI+PBy*PABID2)
      SHLINTSX(11)=SHLINTSX(11)+FACTOR3*(Dx*PBx+PABID2)*(PAy**2+PABID2)*(PBz)
      SHLINTSX(12)=SHLINTSX(12)+FACTOR3*(Dx)*(PAy**2*PBy+PAy*PABI+PBy*PABID2)*(PBz)
      SHLINTSX(13)=SHLINTSX(13)+FACTOR*(Dx*PBx**2+PBx*PABI+Dx*PABID2)*(PAz**2+PABID2)
      SHLINTSX(14)=SHLINTSX(14)+FACTOR*(Dx)*(PBy**2+PABID2)*(PAz**2+PABID2)
      SHLINTSX(15)=SHLINTSX(15)+FACTOR*(Dx)*(PAz**2*PBz**2+PAz**2*PABID2+PAz*PBz*2.0D0*PABI+ &
                                       PBz**2*PABID2+t1)
      SHLINTSX(16)=SHLINTSX(16)+FACTOR3*(Dx*PBx+PABID2)*(PBy)*(PAz**2+PABID2)
      SHLINTSX(17)=SHLINTSX(17)+FACTOR3*(Dx*PBx+PABID2)*(PAz**2*PBz+PAz*PABI+PBz*PABID2)
      SHLINTSX(18)=SHLINTSX(18)+FACTOR3*(Dx)*(PBy)*(PAz**2*PBz+PAz*PABI+PBz*PABID2)
      SHLINTSX(19)=SHLINTSX(19)+FACTOR3*(Dx*PAx*PBx**2+PAx*PBx*PABI+Dx*PAx*PABID2+PBx**2*PABID2+ &
                                         Dx*PBx*PABI+t1)*(PAy)
      SHLINTSX(20)=SHLINTSX(20)+FACTOR3*(Dx*PAx+PABID2)*(PAy*PBy**2+PAy*PABID2+PBy*PABI)
      SHLINTSX(21)=SHLINTSX(21)+FACTOR3*(Dx*PAx+PABID2)*(PAy)*(PBz**2+PABID2)
      SHLINTSX(22)=SHLINTSX(22)+FACTOR33*(Dx*PAx*PBx+PAx*PABID2+PBx*PABID2+Dx*PABID2)*(PAy*PBy+PABID2)
      SHLINTSX(23)=SHLINTSX(23)+FACTOR33*(Dx*PAx*PBx+PAx*PABID2+PBx*PABID2+Dx*PABID2)*(PAy)*(PBz)
      SHLINTSX(24)=SHLINTSX(24)+FACTOR33*(Dx*PAx+PABID2)*(PAy*PBy+PABID2)*(PBz)
      SHLINTSX(25)=SHLINTSX(25)+FACTOR3*(Dx*PAx*PBx**2+PAx*PBx*PABI+Dx*PAx*PABID2+PBx**2*PABID2+ &
                                         Dx*PBx*PABI+t1)*(PAz)
      SHLINTSX(26)=SHLINTSX(26)+FACTOR3*(Dx*PAx+PABID2)*(PBy**2+PABID2)*(PAz)
      SHLINTSX(27)=SHLINTSX(27)+FACTOR3*(Dx*PAx+PABID2)*(PAz*PBz**2+PAz*PABID2+PBz*PABI)
      SHLINTSX(28)=SHLINTSX(28)+FACTOR33*(Dx*PAx*PBx+PAx*PABID2+PBx*PABID2+Dx*PABID2)*(PBy)*(PAz)
      SHLINTSX(29)=SHLINTSX(29)+FACTOR33*(Dx*PAx*PBx+PAx*PABID2+PBx*PABID2+Dx*PABID2)*(PAz*PBz+PABID2)
      SHLINTSX(30)=SHLINTSX(30)+FACTOR33*(Dx*PAx+PABID2)*(PBy)*(PAz*PBz+PABID2)
      SHLINTSX(31)=SHLINTSX(31)+FACTOR3*(Dx*PBx**2+PBx*PABI+Dx*PABID2)*(PAy)*(PAz)
      SHLINTSX(32)=SHLINTSX(32)+FACTOR3*(Dx)*(PAy*PBy**2+PAy*PABID2+PBy*PABI)*(PAz)
      SHLINTSX(33)=SHLINTSX(33)+FACTOR3*(Dx)*(PAy)*(PAz*PBz**2+PAz*PABID2+PBz*PABI)
      SHLINTSX(34)=SHLINTSX(34)+FACTOR33*(Dx*PBx+PABID2)*(PAy*PBy+PABID2)*(PAz)
      SHLINTSX(35)=SHLINTSX(35)+FACTOR33*(Dx*PBx+PABID2)*(PAy)*(PAz*PBz+PABID2)
      SHLINTSX(36)=SHLINTSX(36)+FACTOR33*(Dx)*(PAy*PBy+PABID2)*(PAz*PBz+PABID2)
      
      return
      end subroutine Dipole_Mom_dd
      subroutine Second_Mom_ss
!***********************************************************************
!     Date last modified: AUG 20  , 2003                   Version 1.0 *
!     Author: Aaron Kelly                                              *
!     description: Evaluate the AO integrals for <s|s> Second moment   *
!***********************************************************************
! Modules:
      USE type_one_electron_properties

      implicit none

! <s|s> *
      SHLINTSXX(1)=SHLINTSXX(1)+(Dx**2+PABID2)*FACTOR
      SHLINTSYY(1)=SHLINTSYY(1)+(Dy**2+PABID2)*FACTOR
      SHLINTSZZ(1)=SHLINTSZZ(1)+(Dz**2+PABID2)*FACTOR
      SHLINTSXY(1)=SHLINTSXY(1)+Dx*Dy*FACTOR
      SHLINTSXZ(1)=SHLINTSXZ(1)+Dx*Dz*FACTOR
      SHLINTSYZ(1)=SHLINTSYZ(1)+Dy*Dz*FACTOR
      
      return
      end subroutine Second_Mom_ss

      subroutine Second_Mom_sp
!***********************************************************************
!     Date last modified: AUG 20 , 2003                    Version 1.0 *
!     Author: Aaron Kelly                                              *
!     description: Evaluate the AO integrals for <s|p> second moment   *
!***********************************************************************
! Modules:
      USE type_one_electron_properties

      implicit none

! Begin:
! <s|x> *
      SHLINTSXX(1)=SHLINTSXX(1)+((Dx**2)*PBx+PABID2*PBx+PABI*Dx)*FACTOR
      SHLINTSXY(1)=SHLINTSXY(1)+(Dx*PBx+PABID2)*Dy*FACTOR  
      SHLINTSXZ(1)=SHLINTSXZ(1)+(Dx*PBx+PABID2)*Dz*FACTOR
      SHLINTSYY(1)=SHLINTSYY(1)+(Dy**2+PABID2)*PBx*FACTOR
      SHLINTSYZ(1)=SHLINTSYZ(1)+Dy*Dz*PBx*FACTOR  
      SHLINTSZZ(1)=SHLINTSZZ(1)+(Dz**2+PABID2)*PBx*FACTOR
! <s|y> *
      SHLINTSXX(2)=SHLINTSXX(2)+(Dx**2+PABID2)*PBy*FACTOR
      SHLINTSXY(2)=SHLINTSXY(2)+(Dy*PBy+PABID2)*Dx*FACTOR  
      SHLINTSXZ(2)=SHLINTSXZ(2)+Dx*Dz*PBy*FACTOR
      SHLINTSYY(2)=SHLINTSYY(2)+((Dy**2)*PBy+PABID2*PBy+PABI*Dy)*FACTOR
      SHLINTSYZ(2)=SHLINTSYZ(2)+(Dy*PBy+PABID2)*Dz*FACTOR  
      SHLINTSZZ(2)=SHLINTSZZ(2)+(Dz**2+PABID2)*PBy*FACTOR
! <s|z> *
      SHLINTSXX(3)=SHLINTSXX(3)+(Dx**2+PABID2)*PBz*FACTOR
      SHLINTSXY(3)=SHLINTSXY(3)+Dx*Dy*PBz*FACTOR  
      SHLINTSXZ(3)=SHLINTSXZ(3)+(Dz*PBz+PABID2)*Dx*FACTOR
      SHLINTSYY(3)=SHLINTSYY(3)+(Dy**2+PABID2)*PBz*FACTOR
      SHLINTSYZ(3)=SHLINTSYZ(3)+(Dz*PBz+PABID2)*Dy*FACTOR  
      SHLINTSZZ(3)=SHLINTSZZ(3)+((Dz**2)*PBz+PABID2*PBz+PABI*Dz)*FACTOR

! End of subroutine Second_Mom_sp
      return
      end subroutine Second_Mom_sp

      subroutine Second_Mom_pp
!***********************************************************************
!     Date last modified: AUG 20 , 2003                    Version 1.0 *
!     Author: AARON KELLY                                              *
!     description: Evaluate the AO integrals for the second moment     *
!***********************************************************************
! Modules:
      USE type_one_electron_properties

      implicit none

! Begin:
! <x|x> *
      SHLINTSXX(1)=SHLINTSXX(1)+((Dx**2)*PAx*PBx+PABID2*(PAx*PBx+Dx**2)+ &
                                 PABI*(Dx*PAx+Dx*PBx)+3.0D0*(PABID2**2))*FACTOR
      SHLINTSXY(1)=SHLINTSXY(1)+(Dx*PAx*PBx+PABID2*(PAx+PBx+Dx))*Dy*FACTOR  
      SHLINTSXZ(1)=SHLINTSXZ(1)+(Dx*PAx*PBx+PABID2*(PAx+PBx+Dx))*Dz*FACTOR
      SHLINTSYY(1)=SHLINTSYY(1)+(PAx*PBx+PABID2)*(Dy**2+PABID2)*FACTOR
      SHLINTSYZ(1)=SHLINTSYZ(1)+(PAx*PBx+PABID2)*Dy*Dz*FACTOR  
      SHLINTSZZ(1)=SHLINTSZZ(1)+(PAx*PBx+PABID2)*(Dz**2+PABID2)*FACTOR
! <x|y> *
      SHLINTSXX(2)=SHLINTSXX(2)+((Dx**2)*PAx+PABID2*PAx+PABI*Dx)*PBy*FACTOR
      SHLINTSXY(2)=SHLINTSXY(2)+(PAx*Dx+PABID2)*(Dy*PBy+PABID2)*FACTOR  
      SHLINTSXZ(2)=SHLINTSXZ(2)+(PAx*Dx+PABID2)*Dz*PBy*FACTOR
      SHLINTSYY(2)=SHLINTSYY(2)+((Dy**2)*PBy+PABID2*PBy+PABI*Dy)*PAx*FACTOR
      SHLINTSYZ(2)=SHLINTSYZ(2)+(Dy*PBy+PABID2)*PAx*Dz*FACTOR  
      SHLINTSZZ(2)=SHLINTSZZ(2)+(Dz**2+PABID2)*PAx*PBy*FACTOR
! <x|z> *
      SHLINTSXX(3)=SHLINTSXX(3)+((Dx**2)*PAx+PABID2*PAx+PABI*Dx)*PBz*FACTOR
      SHLINTSXY(3)=SHLINTSXY(3)+(PAx*Dx+PABID2)*Dy*PBz*FACTOR  
      SHLINTSXZ(3)=SHLINTSXZ(3)+(PAx*Dx+PABID2)*(Dz*PBz+PABID2)*FACTOR
      SHLINTSYY(3)=SHLINTSYY(3)+(Dy**2+PABID2)*PAx*PBz*FACTOR
      SHLINTSYZ(3)=SHLINTSYZ(3)+(Dz*PBz+PABID2)*PAx*Dy*FACTOR  
      SHLINTSZZ(3)=SHLINTSZZ(3)+((Dz**2)*PBz+PABID2*PBz+PABI*Dz)*PAx*FACTOR
! <y|x> *
      SHLINTSXX(4)=SHLINTSXX(4)+((Dx**2)*PBx+PABID2*PBx+PABI*Dx)*PAy*FACTOR
      SHLINTSXY(4)=SHLINTSXY(4)+(PBx*Dx+PABID2)*(Dy*PAy+PABID2)*FACTOR  
      SHLINTSXZ(4)=SHLINTSXZ(4)+(PBx*Dx+PABID2)*Dz*PAy*FACTOR
      SHLINTSYY(4)=SHLINTSYY(4)+((Dy**2)*PAy+PABID2*PAy+PABI*Dy)*PBx*FACTOR
      SHLINTSYZ(4)=SHLINTSYZ(4)+(Dy*PAy+PABID2)*PBx*Dz*FACTOR  
      SHLINTSZZ(4)=SHLINTSZZ(4)+(Dz**2+PABID2)*PBx*PAy*FACTOR
! <y|y> *
      SHLINTSXX(5)=SHLINTSXX(5)+(PAy*PBy+PABID2)*(Dx**2+PABID2)*FACTOR
      SHLINTSXY(5)=SHLINTSXY(5)+(Dy*PAy*PBy+PABID2*(PAy+PBy+Dy))*Dx*FACTOR  
      SHLINTSXZ(5)=SHLINTSXZ(5)+(PAy*PBy+PABID2)*Dx*Dz*FACTOR
      SHLINTSYY(5)=SHLINTSYY(5)+((Dy**2)*PAy*PBy+PABID2*(PAy*PBy+Dy**2)+ &
                                 PABI*(Dy*PAy+Dy*PBy)+3.0D0*(PABID2**2))*FACTOR
      SHLINTSYZ(5)=SHLINTSYZ(5)+(Dy*PAy*PBy+PABID2*(PAy+PBy+Dy))*Dz*FACTOR  
      SHLINTSZZ(5)=SHLINTSZZ(5)+(Dz**2+PABID2)*(PAy*PBy+PABID2)*FACTOR
! <y|z> *
      SHLINTSXX(6)=SHLINTSXX(6)+(Dx**2+PABID2)*PAy*PBz*FACTOR
      SHLINTSXY(6)=SHLINTSXY(6)+(Dy*PAy+PABID2)*PBz*Dx*FACTOR  
      SHLINTSXZ(6)=SHLINTSXZ(6)+(Dz*PBz+PABID2)*PAy*Dx*FACTOR
      SHLINTSYY(6)=SHLINTSYY(6)+((Dy**2)*PAy+PABID2*PAy+PABI*Dy)*PBz*FACTOR
      SHLINTSYZ(6)=SHLINTSYZ(6)+(PAy*Dy+PABID2)*(Dz*PBz+PABID2)*FACTOR  
      SHLINTSZZ(6)=SHLINTSZZ(6)+((Dz**2)*PBz+PABID2*PBz+PABI*Dz)*PAy*FACTOR
! <z|x> *
      SHLINTSXX(7)=SHLINTSXX(7)+((Dx**2)*PBx+PABID2*PBx+PABI*Dx)*PAz*FACTOR
      SHLINTSXY(7)=SHLINTSXY(7)+(PBx*Dx+PABID2)*Dy*PAz*FACTOR  
      SHLINTSXZ(7)=SHLINTSXZ(7)+(PBx*Dx+PABID2)*(Dz*PAz+PABID2)*FACTOR
      SHLINTSYY(7)=SHLINTSYY(7)+(Dy**2+PABID2)*PBx*PAz*FACTOR
      SHLINTSYZ(7)=SHLINTSYZ(7)+(Dz*PAz+PABID2)*PBx*Dy*FACTOR  
      SHLINTSZZ(7)=SHLINTSZZ(7)+((Dz**2)*PAz+PABID2*PAz+PABI*Dz)*PBx*FACTOR
! <z|y> *
      SHLINTSXX(8)=SHLINTSXX(8)+(Dx**2+PABID2)*PBy*PAz*FACTOR
      SHLINTSXY(8)=SHLINTSXY(8)+(Dy*PBy+PABID2)*PAz*Dx*FACTOR  
      SHLINTSXZ(8)=SHLINTSXZ(8)+(Dz*PAz+PABID2)*PBy*Dx*FACTOR
      SHLINTSYY(8)=SHLINTSYY(8)+((Dy**2)*PBy+PABID2*PBy+PABI*Dy)*PAz*FACTOR
      SHLINTSYZ(8)=SHLINTSYZ(8)+(PBy*Dy+PABID2)*(Dz*PAz+PABID2)*FACTOR  
      SHLINTSZZ(8)=SHLINTSZZ(8)+((Dz**2)*PAz+PABID2*PAz+PABI*Dz)*PBy*FACTOR
! <z|z> *
      SHLINTSXX(9)=SHLINTSXX(9)+(PAz*PBz+PABID2)*(Dx**2+PABID2)*FACTOR
      SHLINTSXY(9)=SHLINTSXY(9)+(PAz*PBz+PABID2)*Dx*Dy*FACTOR  
      SHLINTSXZ(9)=SHLINTSXZ(9)+(Dz*PAz*PBz+PABID2*(PAz+PBz+Dz))*Dx*FACTOR
      SHLINTSYY(9)=SHLINTSYY(9)+(PAz*PBz+PABID2)*(Dy**2+PABID2)*FACTOR
      SHLINTSYZ(9)=SHLINTSYZ(9)+(Dz*PAz*PBz+PABID2*(PAz+PBz+Dz))*Dy*FACTOR  
      SHLINTSZZ(9)=SHLINTSZZ(9)+((Dz**2)*PAz*PBz+PABID2*(PAz*PBz+Dz**2)+ &
                                 PABI*(Dz*PAz+Dz*PBz)+3.0D0*(PABID2**2))*FACTOR

      return
      end subroutine Second_Mom_pp
      subroutine Second_Mom_sd
!***********************************************************************
!     Date last modified: April 26, 2004                   Version 1.0 *
!     Author: Aaron Kelly and R. Poirier                               *
!     description: Evaluate the AO integrals for <s|d> Second moment   *
!***********************************************************************
! Modules:
      USE type_one_electron_properties

      implicit none
!
! Local scalars:
      double precision :: t1
!
      t1=3.0D0*PABID2**2
! <s|d>
! <S|XX|d>
      SHLINTSXX( 1)=SHLINTSXX( 1)+FACTOR*(Dx**2*PBx**2+PBx**2*PABID2+Dx*PBx*2.0D0*PABI+ &
                                          Dx**2*PABID2+t1)
      SHLINTSXX( 2)=SHLINTSXX( 2)+FACTOR*(Dx**2+PABID2)*(PBy**2+PABID2)
      SHLINTSXX( 3)=SHLINTSXX( 3)+FACTOR*(Dx**2+PABID2)*(PBz**2+PABID2)
      SHLINTSXX( 4)=SHLINTSXX( 4)+FACTOR3*(Dx**2*PBx+PBx*PABID2+Dx*PABI)*(PBy)
      SHLINTSXX( 5)=SHLINTSXX( 5)+FACTOR3*(Dx**2*PBx+PBx*PABID2+Dx*PABI)*(PBz)
      SHLINTSXX( 6)=SHLINTSXX( 6)+FACTOR3*(Dx**2+PABID2)*(PBy)*(PBz)

! <S|YY|d>
      SHLINTSYY( 1)=SHLINTSYY( 1)+FACTOR*(PBx**2+PABID2)*(Dy**2+PABID2)
      SHLINTSYY( 2)=SHLINTSYY( 2)+FACTOR*(Dy**2*PBy**2+PBy**2*PABID2+Dy*PBy*2.0D0*PABI+Dy**2*PABID2+t1)
      SHLINTSYY( 3)=SHLINTSYY( 3)+FACTOR*(Dy**2+PABID2)*(PBz**2+PABID2)
      SHLINTSYY( 4)=SHLINTSYY( 4)+FACTOR3*(PBx)*(Dy**2*PBy+PBy*PABID2+Dy*PABI)
      SHLINTSYY( 5)=SHLINTSYY( 5)+FACTOR3*(PBx)*(Dy**2+PABID2)*(PBz)
      SHLINTSYY( 6)=SHLINTSYY( 6)+FACTOR3*(Dy**2*PBy+PBy*PABID2+Dy*PABI)*(PBz)

! <S|ZZ|d>
      SHLINTSZZ( 1)=SHLINTSZZ( 1)+FACTOR*(PBx**2+PABID2)*(Dz**2+PABID2)
      SHLINTSZZ( 2)=SHLINTSZZ( 2)+FACTOR*(PBy**2+PABID2)*(Dz**2+PABID2)
      SHLINTSZZ( 3)=SHLINTSZZ( 3)+FACTOR*(Dz**2*PBz**2+PBz**2*PABID2+Dz*PBz*2.0D0*PABI+Dz**2*PABID2+t1)
      SHLINTSZZ( 4)=SHLINTSZZ( 4)+FACTOR3*(PBx)*(PBy)*(Dz**2+PABID2)
      SHLINTSZZ( 5)=SHLINTSZZ( 5)+FACTOR3*(PBx)*(Dz**2*PBz+PBz*PABID2+Dz*PABI)
      SHLINTSZZ( 6)=SHLINTSZZ( 6)+FACTOR3*(PBy)*(Dz**2*PBz+PBz*PABID2+Dz*PABI)

! <S|XY|d>
      SHLINTSXY( 1)=SHLINTSXY( 1)+FACTOR*(Dx*PBx**2+PBx*PABI+Dx*PABID2)*(Dy)
      SHLINTSXY( 2)=SHLINTSXY( 2)+FACTOR*(Dx)*(Dy*PBy**2+PBy*PABI+Dy*PABID2)
      SHLINTSXY( 3)=SHLINTSXY( 3)+FACTOR*(Dx)*(Dy)*(PBz**2+PABID2)
      SHLINTSXY( 4)=SHLINTSXY( 4)+FACTOR3*(Dx*PBx+PABID2)*(Dy*PBy+PABID2)
      SHLINTSXY( 5)=SHLINTSXY( 5)+FACTOR3*(Dx*PBx+PABID2)*(Dy)*(PBz)
      SHLINTSXY( 6)=SHLINTSXY( 6)+FACTOR3*(Dx)*(Dy*PBy+PABID2)*(PBz)

! <S|XZ|d>
      SHLINTSXZ( 1)=SHLINTSXZ( 1)+FACTOR*(Dx*PBx**2+PBx*PABI+Dx*PABID2)*(Dz)
      SHLINTSXZ( 2)=SHLINTSXZ( 2)+FACTOR*(Dx)*(PBy**2+PABID2)*(Dz)
      SHLINTSXZ( 3)=SHLINTSXZ( 3)+FACTOR*(Dx)*(Dz*PBz**2+PBz*PABI+Dz*PABID2)
      SHLINTSXZ( 4)=SHLINTSXZ( 4)+FACTOR3*(Dx*PBx+PABID2)*(PBy)*(Dz)
      SHLINTSXZ( 5)=SHLINTSXZ( 5)+FACTOR3*(Dx*PBx+PABID2)*(Dz*PBz+PABID2)
      SHLINTSXZ( 6)=SHLINTSXZ( 6)+FACTOR3*(Dx)*(PBy)*(Dz*PBz+PABID2)

! <S|YZ|d>
      SHLINTSYZ( 1)=SHLINTSYZ( 1)+FACTOR*(PBx**2+PABID2)*(Dy)*(Dz)
      SHLINTSYZ( 2)=SHLINTSYZ( 2)+FACTOR*(Dy*PBy**2+PBy*PABI+Dy*PABID2)*(Dz)
      SHLINTSYZ( 3)=SHLINTSYZ( 3)+FACTOR*(Dy)*(Dz*PBz**2+PBz*PABI+Dz*PABID2)
      SHLINTSYZ( 4)=SHLINTSYZ( 4)+FACTOR3*(PBx)*(Dy*PBy+PABID2)*(Dz)
      SHLINTSYZ( 5)=SHLINTSYZ( 5)+FACTOR3*(PBx)*(Dy)*(Dz*PBz+PABID2)
      SHLINTSYZ( 6)=SHLINTSYZ( 6)+FACTOR3*(Dy*PBy+PABID2)*(Dz*PBz+PABID2)

      return
      end subroutine Second_Mom_sd
      subroutine Second_Mom_pd
!***********************************************************************
!     Date last modified: April 26, 2004                   Version 1.0 *
!     Author: Aaron Kelly and R. Poirier                               *
!     description: Evaluate the AO integrals for <p|d> Second moment   *
!***********************************************************************
! Modules:
      USE type_one_electron_properties

      implicit none
!
! Local scalars:
      double precision :: t1,t2
!
      t1=3.0D0*PABID2**2
      t2=2.0D0*t1
! <p|d>
! <p|XX|d>      
      SHLINTSXX( 1)=SHLINTSXX( 1)+FACTOR*(Dx**2*PAx*PBx**2+PAx*PBx**2*PABID2+Dx*PAx*PBx*2.0D0*PABI+ &
                                          Dx**2*PAx*PABID2+PAx*t1+Dx*PBx**2*PABI+ &
                                          Dx**2*PBx*PABI+PBx*t2+Dx*t2)
      SHLINTSXX( 2)=SHLINTSXX( 2)+FACTOR*(Dx**2*PAx+PAx*PABID2+Dx*PABI)*(PBy**2+PABID2)
      SHLINTSXX( 3)=SHLINTSXX( 3)+FACTOR*(Dx**2*PAx+PAx*PABID2+Dx*PABI)*(PBz**2+PABID2)
      SHLINTSXX( 4)=SHLINTSXX( 4)+FACTOR3*(Dx**2*PAx*PBx+PAx*PBx*PABID2+Dx*PAx*PABI+Dx*PBx*PABI+ &
                                          Dx**2*PABID2+t1)*(PBy)
      SHLINTSXX( 5)=SHLINTSXX( 5)+FACTOR3*(Dx**2*PAx*PBx+PAx*PBx*PABID2+Dx*PAx*PABI+Dx*PBx*PABI+ &
                                          Dx**2*PABID2+t1)*(PBz)
      SHLINTSXX( 6)=SHLINTSXX( 6)+FACTOR3*(Dx**2*PAx+PAx*PABID2+Dx*PABI)*(PBy)*(PBz)
      SHLINTSXX( 7)=SHLINTSXX( 7)+FACTOR*(Dx**2*PBx**2+PBx**2*PABID2+Dx*PBx*2.0D0*PABI+Dx**2*PABID2+t1)*(PAy)
      SHLINTSXX( 8)=SHLINTSXX( 8)+FACTOR*(Dx**2+PABID2)*(PAy*PBy**2+PAy*PABID2+PBy*PABI)
      SHLINTSXX( 9)=SHLINTSXX( 9)+FACTOR*(Dx**2+PABID2)*(PAy)*(PBz**2+PABID2)
      SHLINTSXX(10)=SHLINTSXX(10)+FACTOR3*(Dx**2*PBx+PBx*PABID2+Dx*PABI)*(PAy*PBy+PABID2)
      SHLINTSXX(11)=SHLINTSXX(11)+FACTOR3*(Dx**2*PBx+PBx*PABID2+Dx*PABI)*(PAy)*(PBz)
      SHLINTSXX(12)=SHLINTSXX(12)+FACTOR3*(Dx**2+PABID2)*(PAy*PBy+PABID2)*(PBz)
      SHLINTSXX(13)=SHLINTSXX(13)+FACTOR*(Dx**2*PBx**2+PBx**2*PABID2+Dx*PBx*2.0D0*PABI+Dx**2*PABID2+t1)*(PAz)
      SHLINTSXX(14)=SHLINTSXX(14)+FACTOR*(Dx**2+PABID2)*(PBy**2+PABID2)*(PAz)
      SHLINTSXX(15)=SHLINTSXX(15)+FACTOR*(Dx**2+PABID2)*(PAz*PBz**2+PAz*PABID2+PBz*PABI)
      SHLINTSXX(16)=SHLINTSXX(16)+FACTOR3*(Dx**2*PBx+PBx*PABID2+Dx*PABI)*(PBy)*(PAz)
      SHLINTSXX(17)=SHLINTSXX(17)+FACTOR3*(Dx**2*PBx+PBx*PABID2+Dx*PABI)*(PAz*PBz+PABID2)
      SHLINTSXX(18)=SHLINTSXX(18)+FACTOR3*(Dx**2+PABID2)*(PBy)*(PAz*PBz+PABID2)

! <p|YY|d>
      SHLINTSYY( 1)=SHLINTSYY( 1)+FACTOR*(PAx*PBx**2+PAx*PABID2+PBx*PABI)*(Dy**2+PABID2)
      SHLINTSYY( 2)=SHLINTSYY( 2)+FACTOR*(PAx)*(Dy**2*PBy**2+PBy**2*PABID2+Dy*PBy*2.0D0*PABI+ &
                                          Dy**2*PABID2+t1)
      SHLINTSYY( 3)=SHLINTSYY( 3)+FACTOR*(PAx)*(Dy**2+PABID2)*(PBz**2+PABID2)
      SHLINTSYY( 4)=SHLINTSYY( 4)+FACTOR3*(PAx*PBx+PABID2)*(Dy**2*PBy+PBy*PABID2+Dy*PABI)
      SHLINTSYY( 5)=SHLINTSYY( 5)+FACTOR3*(PAx*PBx+PABID2)*(Dy**2+PABID2)*(PBz)
      SHLINTSYY( 6)=SHLINTSYY( 6)+FACTOR3*(PAx)*(Dy**2*PBy+PBy*PABID2+Dy*PABI)*(PBz)
      SHLINTSYY( 7)=SHLINTSYY( 7)+FACTOR*(PBx**2+PABID2)*(Dy**2*PAy+PAy*PABID2+Dy*PABI)
      SHLINTSYY( 8)=SHLINTSYY( 8)+FACTOR*(Dy**2*PAy*PBy**2+PAy*PBy**2*PABID2+Dy*PAy*PBy*2.0D0*PABI+ &
                                          Dy**2*PAy*PABID2+PAy*t1+Dy*PBy**2*PABI+ &
                                          Dy**2*PBy*PABI+PBy*t2+Dy*t2)
      SHLINTSYY( 9)=SHLINTSYY( 9)+FACTOR*(Dy**2*PAy+PAy*PABID2+Dy*PABI)*(PBz**2+PABID2)
      SHLINTSYY(10)=SHLINTSYY(10)+FACTOR3*(PBx)*(Dy**2*PAy*PBy+PAy*PBy*PABID2+Dy*PAy*PABI+ &
                                          Dy*PBy*PABI+Dy**2*PABID2+t1)
      SHLINTSYY(11)=SHLINTSYY(11)+FACTOR3*(PBx)*(Dy**2*PAy+PAy*PABID2+Dy*PABI)*(PBz)
      SHLINTSYY(12)=SHLINTSYY(12)+FACTOR3*(Dy**2*PAy*PBy+PAy*PBy*PABID2+Dy*PAy*PABI+Dy*PBy*PABI+ &
                                          Dy**2*PABID2+t1)*(PBz)
      SHLINTSYY(13)=SHLINTSYY(13)+FACTOR*(PBx**2+PABID2)*(Dy**2+PABID2)*(PAz)
      SHLINTSYY(14)=SHLINTSYY(14)+FACTOR*(Dy**2*PBy**2+PBy**2*PABID2+Dy*PBy*2.0D0*PABI+Dy**2*PABID2+t1)*(PAz)
      SHLINTSYY(15)=SHLINTSYY(15)+FACTOR*(Dy**2+PABID2)*(PAz*PBz**2+PAz*PABID2+PBz*PABI)
      SHLINTSYY(16)=SHLINTSYY(16)+FACTOR3*(PBx)*(Dy**2*PBy+PBy*PABID2+Dy*PABI)*(PAz)
      SHLINTSYY(17)=SHLINTSYY(17)+FACTOR3*(PBx)*(Dy**2+PABID2)*(PAz*PBz+PABID2)
      SHLINTSYY(18)=SHLINTSYY(18)+FACTOR3*(Dy**2*PBy+PBy*PABID2+Dy*PABI)*(PAz*PBz+PABID2)

! <p|ZZ|d>
      SHLINTSZZ( 1)=SHLINTSZZ( 1)+FACTOR*(PAx*PBx**2+PAx*PABID2+PBx*PABI)*(Dz**2+PABID2)
      SHLINTSZZ( 2)=SHLINTSZZ( 2)+FACTOR*(PAx)*(PBy**2+PABID2)*(Dz**2+PABID2)
      SHLINTSZZ( 3)=SHLINTSZZ( 3)+FACTOR*(PAx)*(Dz**2*PBz**2+PBz**2*PABID2+Dz*PBz*2.0D0*PABI+ &
                                          Dz**2*PABID2+t1)
      SHLINTSZZ( 4)=SHLINTSZZ( 4)+FACTOR3*(PAx*PBx+PABID2)*(PBy)*(Dz**2+PABID2)
      SHLINTSZZ( 5)=SHLINTSZZ( 5)+FACTOR3*(PAx*PBx+PABID2)*(Dz**2*PBz+PBz*PABID2+Dz*PABI)
      SHLINTSZZ( 6)=SHLINTSZZ( 6)+FACTOR3*(PAx)*(PBy)*(Dz**2*PBz+PBz*PABID2+Dz*PABI)
      SHLINTSZZ( 7)=SHLINTSZZ( 7)+FACTOR*(PBx**2+PABID2)*(PAy)*(Dz**2+PABID2)
      SHLINTSZZ( 8)=SHLINTSZZ( 8)+FACTOR*(PAy*PBy**2+PAy*PABID2+PBy*PABI)*(Dz**2+PABID2)
      SHLINTSZZ( 9)=SHLINTSZZ( 9)+FACTOR*(PAy)*(Dz**2*PBz**2+PBz**2*PABID2+Dz*PBz*2.0D0*PABI+ &
                                          Dz**2*PABID2+t1)
      SHLINTSZZ(10)=SHLINTSZZ(10)+FACTOR3*(PBx)*(PAy*PBy+PABID2)*(Dz**2+PABID2)
      SHLINTSZZ(11)=SHLINTSZZ(11)+FACTOR3*(PBx)*(PAy)*(Dz**2*PBz+PBz*PABID2+Dz*PABI)
      SHLINTSZZ(12)=SHLINTSZZ(12)+FACTOR3*(PAy*PBy+PABID2)*(Dz**2*PBz+PBz*PABID2+Dz*PABI)
      SHLINTSZZ(13)=SHLINTSZZ(13)+FACTOR*(PBx**2+PABID2)*(Dz**2*PAz+PAz*PABID2+Dz*PABI)
      SHLINTSZZ(14)=SHLINTSZZ(14)+FACTOR*(PBy**2+PABID2)*(Dz**2*PAz+PAz*PABID2+Dz*PABI)
      SHLINTSZZ(15)=SHLINTSZZ(15)+FACTOR*(Dz**2*PAz*PBz**2+PAz*PBz**2*PABID2+Dz*PAz*PBz*2.0D0*PABI+ &
                                          Dz**2*PAz*PABID2+PAz*t1+Dz*PBz**2*PABI+Dz**2*PBz*PABI+ &
                                          PBz*t2+Dz*t2)
      SHLINTSZZ(16)=SHLINTSZZ(16)+FACTOR3*(PBx)*(PBy)*(Dz**2*PAz+PAz*PABID2+Dz*PABI)
      SHLINTSZZ(17)=SHLINTSZZ(17)+FACTOR3*(PBx)*(Dz**2*PAz*PBz+PAz*PBz*PABID2+Dz*PAz*PABI+Dz*PBz*PABI+ &
                                          Dz**2*PABID2+t1)
      SHLINTSZZ(18)=SHLINTSZZ(18)+FACTOR3*(PBy)*(Dz**2*PAz*PBz+PAz*PBz*PABID2+Dz*PAz*PABI+Dz*PBz*PABI+ &
                                          Dz**2*PABID2+t1)

! <p|XY|d>
      SHLINTSXY( 1)=SHLINTSXY( 1)+FACTOR*(Dx*PAx*PBx**2+PAx*PBx*PABI+Dx*PAx*PABID2+PBx**2*PABID2+ &
                                          Dx*PBx*PABI+t1)*(Dy)
      SHLINTSXY( 2)=SHLINTSXY( 2)+FACTOR*(Dx*PAx+PABID2)*(Dy*PBy**2+PBy*PABI+Dy*PABID2)
      SHLINTSXY( 3)=SHLINTSXY( 3)+FACTOR*(Dx*PAx+PABID2)*(Dy)*(PBz**2+PABID2)
      SHLINTSXY( 4)=SHLINTSXY( 4)+FACTOR3*(Dx*PAx*PBx+PAx*PABID2+PBx*PABID2+Dx*PABID2)*(Dy*PBy+PABID2)
      SHLINTSXY( 5)=SHLINTSXY( 5)+FACTOR3*(Dx*PAx*PBx+PAx*PABID2+PBx*PABID2+Dx*PABID2)*(Dy)*(PBz)
      SHLINTSXY( 6)=SHLINTSXY( 6)+FACTOR3*(Dx*PAx+PABID2)*(Dy*PBy+PABID2)*(PBz)
      SHLINTSXY( 7)=SHLINTSXY( 7)+FACTOR*(Dx*PBx**2+PBx*PABI+Dx*PABID2)*(Dy*PAy+PABID2)
      SHLINTSXY( 8)=SHLINTSXY( 8)+FACTOR*(Dx)*(Dy*PAy*PBy**2+PAy*PBy*PABI+Dy*PAy*PABID2+PBy**2*PABID2+ &
                                          Dy*PBy*PABI+t1)
      SHLINTSXY( 9)=SHLINTSXY( 9)+FACTOR*(Dx)*(Dy*PAy+PABID2)*(PBz**2+PABID2)
      SHLINTSXY(10)=SHLINTSXY(10)+FACTOR3*(Dx*PBx+PABID2)*(Dy*PAy*PBy+PAy*PABID2+PBy*PABID2+Dy*PABID2)
      SHLINTSXY(11)=SHLINTSXY(11)+FACTOR3*(Dx*PBx+PABID2)*(Dy*PAy+PABID2)*(PBz)
      SHLINTSXY(12)=SHLINTSXY(12)+FACTOR3*(Dx)*(Dy*PAy*PBy+PAy*PABID2+PBy*PABID2+Dy*PABID2)*(PBz)
      SHLINTSXY(13)=SHLINTSXY(13)+FACTOR*(Dx*PBx**2+PBx*PABI+Dx*PABID2)*(Dy)*(PAz)
      SHLINTSXY(14)=SHLINTSXY(14)+FACTOR*(Dx)*(Dy*PBy**2+PBy*PABI+Dy*PABID2)*(PAz)
      SHLINTSXY(15)=SHLINTSXY(15)+FACTOR*(Dx)*(Dy)*(PAz*PBz**2+PAz*PABID2+PBz*PABI)
      SHLINTSXY(16)=SHLINTSXY(16)+FACTOR3*(Dx*PBx+PABID2)*(Dy*PBy+PABID2)*(PAz)
      SHLINTSXY(17)=SHLINTSXY(17)+FACTOR3*(Dx*PBx+PABID2)*(Dy)*(PAz*PBz+PABID2)
      SHLINTSXY(18)=SHLINTSXY(18)+FACTOR3*(Dx)*(Dy*PBy+PABID2)*(PAz*PBz+PABID2)

! <p|XZ|d>
      SHLINTSXZ( 1)=SHLINTSXZ( 1)+FACTOR*(Dx*PAx*PBx**2+PAx*PBx*PABI+Dx*PAx*PABID2+PBx**2*PABID2+ &
                                          Dx*PBx*PABI+t1)*(Dz)
      SHLINTSXZ( 2)=SHLINTSXZ( 2)+FACTOR*(Dx*PAx+PABID2)*(PBy**2+PABID2)*(Dz)
      SHLINTSXZ( 3)=SHLINTSXZ( 3)+FACTOR*(Dx*PAx+PABID2)*(Dz*PBz**2+PBz*PABI+Dz*PABID2)
      SHLINTSXZ( 4)=SHLINTSXZ( 4)+FACTOR3*(Dx*PAx*PBx+PAx*PABID2+PBx*PABID2+Dx*PABID2)*(PBy)*(Dz)
      SHLINTSXZ( 5)=SHLINTSXZ( 5)+FACTOR3*(Dx*PAx*PBx+PAx*PABID2+PBx*PABID2+Dx*PABID2)*(Dz*PBz+PABID2)
      SHLINTSXZ( 6)=SHLINTSXZ( 6)+FACTOR3*(Dx*PAx+PABID2)*(PBy)*(Dz*PBz+PABID2)
      SHLINTSXZ( 7)=SHLINTSXZ( 7)+FACTOR*(Dx*PBx**2+PBx*PABI+Dx*PABID2)*(PAy)*(Dz)
      SHLINTSXZ( 8)=SHLINTSXZ( 8)+FACTOR*(Dx)*(PAy*PBy**2+PAy*PABID2+PBy*PABI)*(Dz)
      SHLINTSXZ( 9)=SHLINTSXZ( 9)+FACTOR*(Dx)*(PAy)*(Dz*PBz**2+PBz*PABI+Dz*PABID2)
      SHLINTSXZ(10)=SHLINTSXZ(10)+FACTOR3*(Dx*PBx+PABID2)*(PAy*PBy+PABID2)*(Dz)
      SHLINTSXZ(11)=SHLINTSXZ(11)+FACTOR3*(Dx*PBx+PABID2)*(PAy)*(Dz*PBz+PABID2)
      SHLINTSXZ(12)=SHLINTSXZ(12)+FACTOR3*(Dx)*(PAy*PBy+PABID2)*(Dz*PBz+PABID2)
      SHLINTSXZ(13)=SHLINTSXZ(13)+FACTOR*(Dx*PBx**2+PBx*PABI+Dx*PABID2)*(Dz*PAz+PABID2)
      SHLINTSXZ(14)=SHLINTSXZ(14)+FACTOR*(Dx)*(PBy**2+PABID2)*(Dz*PAz+PABID2)
      SHLINTSXZ(15)=SHLINTSXZ(15)+FACTOR*(Dx)*(Dz*PAz*PBz**2+PAz*PBz*PABI+Dz*PAz*PABID2+ &
                                          PBz**2*PABID2+Dz*PBz*PABI+t1)
      SHLINTSXZ(16)=SHLINTSXZ(16)+FACTOR3*(Dx*PBx+PABID2)*(PBy)*(Dz*PAz+PABID2)
      SHLINTSXZ(17)=SHLINTSXZ(17)+FACTOR3*(Dx*PBx+PABID2)*(Dz*PAz*PBz+PAz*PABID2+PBz*PABID2+Dz*PABID2)
      SHLINTSXZ(18)=SHLINTSXZ(18)+FACTOR3*(Dx)*(PBy)*(Dz*PAz*PBz+PAz*PABID2+PBz*PABID2+Dz*PABID2)

! <p|YZ|d>
      SHLINTSYZ( 1)=SHLINTSYZ( 1)+FACTOR*(PAx*PBx**2+PAx*PABID2+PBx*PABI)*(Dy)*(Dz)
      SHLINTSYZ( 2)=SHLINTSYZ( 2)+FACTOR*(PAx)*(Dy*PBy**2+PBy*PABI+Dy*PABID2)*(Dz)
      SHLINTSYZ( 3)=SHLINTSYZ( 3)+FACTOR*(PAx)*(Dy)*(Dz*PBz**2+PBz*PABI+Dz*PABID2)
      SHLINTSYZ( 4)=SHLINTSYZ( 4)+FACTOR3*(PAx*PBx+PABID2)*(Dy*PBy+PABID2)*(Dz)
      SHLINTSYZ( 5)=SHLINTSYZ( 5)+FACTOR3*(PAx*PBx+PABID2)*(Dy)*(Dz*PBz+PABID2)
      SHLINTSYZ( 6)=SHLINTSYZ( 6)+FACTOR3*(PAx)*(Dy*PBy+PABID2)*(Dz*PBz+PABID2)
      SHLINTSYZ( 7)=SHLINTSYZ( 7)+FACTOR*(PBx**2+PABID2)*(Dy*PAy+PABID2)*(Dz)
      SHLINTSYZ( 8)=SHLINTSYZ( 8)+FACTOR*(Dy*PAy*PBy**2+PAy*PBy*PABI+Dy*PAy*PABID2+PBy**2*PABID2+ &
                                          Dy*PBy*PABI+t1)*(Dz)
      SHLINTSYZ( 9)=SHLINTSYZ( 9)+FACTOR*(Dy*PAy+PABID2)*(Dz*PBz**2+PBz*PABI+Dz*PABID2)
      SHLINTSYZ(10)=SHLINTSYZ(10)+FACTOR3*(PBx)*(Dy*PAy*PBy+PAy*PABID2+PBy*PABID2+Dy*PABID2)*(Dz)
      SHLINTSYZ(11)=SHLINTSYZ(11)+FACTOR3*(PBx)*(Dy*PAy+PABID2)*(Dz*PBz+PABID2)
      SHLINTSYZ(12)=SHLINTSYZ(12)+FACTOR3*(Dy*PAy*PBy+PAy*PABID2+PBy*PABID2+Dy*PABID2)*(Dz*PBz+PABID2)
      SHLINTSYZ(13)=SHLINTSYZ(13)+FACTOR*(PBx**2+PABID2)*(Dy)*(Dz*PAz+PABID2)
      SHLINTSYZ(14)=SHLINTSYZ(14)+FACTOR*(Dy*PBy**2+PBy*PABI+Dy*PABID2)*(Dz*PAz+PABID2)
      SHLINTSYZ(15)=SHLINTSYZ(15)+FACTOR*(Dy)*(Dz*PAz*PBz**2+PAz*PBz*PABI+Dz*PAz*PABID2+PBz**2*PABID2+ &
                                          Dz*PBz*PABI+t1)
      SHLINTSYZ(16)=SHLINTSYZ(16)+FACTOR3*(PBx)*(Dy*PBy+PABID2)*(Dz*PAz+PABID2)
      SHLINTSYZ(17)=SHLINTSYZ(17)+FACTOR3*(PBx)*(Dy)*(Dz*PAz*PBz+PAz*PABID2+PBz*PABID2+Dz*PABID2)
      SHLINTSYZ(18)=SHLINTSYZ(18)+FACTOR3*(Dy*PBy+PABID2)*(Dz*PAz*PBz+PAz*PABID2+PBz*PABID2+Dz*PABID2)

      return
      end subroutine Second_Mom_pd
      subroutine Second_Mom_dd
!***********************************************************************
!     Date last modified: April 26, 2004                   Version 1.0 *
!     Author: Aaron Kelly and R. Poirier                               *
!     description: Evaluate the AO integrals for <d|d> Second moment   *
!***********************************************************************
! Modules:
      USE type_one_electron_properties

      implicit none
!
! Local scalars:
      double precision :: t1,t2
!
      t1=3.0D0*PABID2**2
      t2=2.0D0*t1
! <d|d>
! <d|XX|d>      
      SHLINTSXX( 1)=SHLINTSXX( 1)+FACTOR*(Dx**2*PAx**2*PBx**2+PAx**2*PBx**2*PABID2+Dx*PAx**2*PBx*4*pABID2+ &
                                          Dx**2*PAx**2*PABID2+PAx**2*t1+Dx*PAx*PBx**2*4*PABID2+ &
                                          Dx**2*PAx*PBx*2.0D0*PABI+PAx*PBx*12.0D0*PABID2**2+ &
                                          Dx*PAx*12.0D0*PABID2**2+Dx**2*PBx**2*PABID2+ &
                                          PBx**2*t1+Dx*PBx*12.0D0*PABID2**2+ &
                                          Dx**2*t1+(15)*PABID2**3)
      SHLINTSXX( 2)=SHLINTSXX( 2)+FACTOR*(Dx**2*PAx**2+PAx**2*PABID2+Dx*PAx*2.0D0*PABI+Dx**2*PABID2+ &
                                          t1)*(PBy**2+PABID2)
      SHLINTSXX( 3)=SHLINTSXX( 3)+FACTOR*(Dx**2*PAx**2+PAx**2*PABID2+Dx*PAx*2.0D0*PABI+Dx**2*PABID2+ &
                                          t1)*(PBz**2+PABID2)
      SHLINTSXX( 4)=SHLINTSXX( 4)+FACTOR3*(Dx**2*PAx**2*PBx+PAx**2*PBx*PABID2+Dx*PAx**2*PABI+ &
                                          Dx*PAx*PBx*2.0D0*PABI+Dx**2*PAx*PABI+PAx*t2+ &
                                          Dx**2*PBx*PABID2+PBx*t1+Dx*t2)*(PBy)
      SHLINTSXX( 5)=SHLINTSXX( 5)+FACTOR3*(Dx**2*PAx**2*PBx+PAx**2*PBx*PABID2+Dx*PAx**2*PABI+ &
                                          Dx*PAx*PBx*2.0D0*PABI+Dx**2*PAx*PABI+PAx*t2+ &
                                          Dx**2*PBx*PABID2+PBx*t1+Dx*t2)*(PBz)
      SHLINTSXX( 6)=SHLINTSXX( 6)+FACTOR3*(Dx**2*PAx**2+PAx**2*PABID2+Dx*PAx*2.0D0*PABI+Dx**2*PABID2+ &
                                          t1)*(PBy)*(PBz)
      SHLINTSXX( 7)=SHLINTSXX( 7)+FACTOR*(Dx**2*PBx**2+PBx**2*PABID2+Dx*PBx*2.0D0*PABI+Dx**2*PABID2+ &
                                          t1)*(PAy**2+PABID2)
      SHLINTSXX( 8)=SHLINTSXX( 8)+FACTOR*(Dx**2+PABID2)*(PAy**2*PBy**2+PAy**2*PABID2+PAy*PBy*2.0D0*PABI+ &
                                          PBy**2*PABID2+t1)
      SHLINTSXX( 9)=SHLINTSXX( 9)+FACTOR*(Dx**2+PABID2)*(PAy**2+PABID2)*(PBz**2+PABID2)
      SHLINTSXX(10)=SHLINTSXX(10)+FACTOR3*(Dx**2*PBx+PBx*PABID2+Dx*PABI)*(PAy**2*PBy+PAy*PABI+PBy*PABID2)
      SHLINTSXX(11)=SHLINTSXX(11)+FACTOR3*(Dx**2*PBx+PBx*PABID2+Dx*PABI)*(PAy**2+PABID2)*(PBz)
      SHLINTSXX(12)=SHLINTSXX(12)+FACTOR3*(Dx**2+PABID2)*(PAy**2*PBy+PAy*PABI+PBy*PABID2)*(PBz)
      SHLINTSXX(13)=SHLINTSXX(13)+FACTOR*(Dx**2*PBx**2+PBx**2*PABID2+Dx*PBx*2.0D0*PABI+Dx**2*PABID2+ &
                                          t1)*(PAz**2+PABID2)
      SHLINTSXX(14)=SHLINTSXX(14)+FACTOR*(Dx**2+PABID2)*(PBy**2+PABID2)*(PAz**2+PABID2)
      SHLINTSXX(15)=SHLINTSXX(15)+FACTOR*(Dx**2+PABID2)*(PAz**2*PBz**2+PAz**2*PABID2+PAz*PBz*2.0D0*PABI+ &
                                          PBz**2*PABID2+t1)
      SHLINTSXX(16)=SHLINTSXX(16)+FACTOR3*(Dx**2*PBx+PBx*PABID2+Dx*PABI)*(PBy)*(PAz**2+PABID2)
      SHLINTSXX(17)=SHLINTSXX(17)+FACTOR3*(Dx**2*PBx+PBx*PABID2+Dx*PABI)*(PAz**2*PBz+PAz*PABI+PBz*PABID2)
      SHLINTSXX(18)=SHLINTSXX(18)+FACTOR3*(Dx**2+PABID2)*(PBy)*(PAz**2*PBz+PAz*PABI+PBz*PABID2)
      SHLINTSXX(19)=SHLINTSXX(19)+FACTOR3*(Dx**2*PAx*PBx**2+PAx*PBx**2*PABID2+Dx*PAx*PBx*2.0D0*PABI+ &
                                          Dx**2*PAx*PABID2+PAx*t1+Dx*PBx**2*PABI+ &
                                          Dx**2*PBx*PABI+PBx*t2+Dx*t2)*(PAy)
      SHLINTSXX(20)=SHLINTSXX(20)+FACTOR3*(Dx**2*PAx+PAx*PABID2+Dx*PABI)*(PAy*PBy**2+PAy*PABID2+PBy*PABI)
      SHLINTSXX(21)=SHLINTSXX(21)+FACTOR3*(Dx**2*PAx+PAx*PABID2+Dx*PABI)*(PAy)*(PBz**2+PABID2)
      SHLINTSXX(22)=SHLINTSXX(22)+FACTOR33*(Dx**2*PAx*PBx+PAx*PBx*PABID2+Dx*PAx*PABI+Dx*PBx*PABI+ &
                                          Dx**2*PABID2+t1)*(PAy*PBy+PABID2)
      SHLINTSXX(23)=SHLINTSXX(23)+FACTOR33*(Dx**2*PAx*PBx+PAx*PBx*PABID2+Dx*PAx*PABI+Dx*PBx*PABI+ &
                                          Dx**2*PABID2+t1)*(PAy)*(PBz)
      SHLINTSXX(24)=SHLINTSXX(24)+FACTOR33*(Dx**2*PAx+PAx*PABID2+Dx*PABI)*(PAy*PBy+PABID2)*(PBz)
      SHLINTSXX(25)=SHLINTSXX(25)+FACTOR3*(Dx**2*PAx*PBx**2+PAx*PBx**2*PABID2+Dx*PAx*PBx*2.0D0*PABI+ &
                                          Dx**2*PAx*PABID2+PAx*t1+Dx*PBx**2*PABI+ &
                                          Dx**2*PBx*PABI+PBx*t2+Dx*t2)*(PAz)
      SHLINTSXX(26)=SHLINTSXX(26)+FACTOR3*(Dx**2*PAx+PAx*PABID2+Dx*PABI)*(PBy**2+PABID2)*(PAz)
      SHLINTSXX(27)=SHLINTSXX(27)+FACTOR3*(Dx**2*PAx+PAx*PABID2+Dx*PABI)*(PAz*PBz**2+PAz*PABID2+PBz*PABI)
      SHLINTSXX(28)=SHLINTSXX(28)+FACTOR33*(Dx**2*PAx*PBx+PAx*PBx*PABID2+Dx*PAx*PABI+Dx*PBx*PABI+ &
                                          Dx**2*PABID2+t1)*(PBy)*(PAz)
      SHLINTSXX(29)=SHLINTSXX(29)+FACTOR33*(Dx**2*PAx*PBx+PAx*PBx*PABID2+Dx*PAx*PABI+Dx*PBx*PABI+ &
                                          Dx**2*PABID2+t1)*(PAz*PBz+PABID2)
      SHLINTSXX(30)=SHLINTSXX(30)+FACTOR33*(Dx**2*PAx+PAx*PABID2+Dx*PABI)*(PBy)*(PAz*PBz+PABID2)
      SHLINTSXX(31)=SHLINTSXX(31)+FACTOR3*(Dx**2*PBx**2+PBx**2*PABID2+Dx*PBx*2.0D0*PABI+Dx**2*PABID2+ &
                                          t1)*(PAy)*(PAz)
      SHLINTSXX(32)=SHLINTSXX(32)+FACTOR3*(Dx**2+PABID2)*(PAy*PBy**2+PAy*PABID2+PBy*PABI)*(PAz)
      SHLINTSXX(33)=SHLINTSXX(33)+FACTOR3*(Dx**2+PABID2)*(PAy)*(PAz*PBz**2+PAz*PABID2+PBz*PABI)
      SHLINTSXX(34)=SHLINTSXX(34)+FACTOR33*(Dx**2*PBx+PBx*PABID2+Dx*PABI)*(PAy*PBy+PABID2)*(PAz)
      SHLINTSXX(35)=SHLINTSXX(35)+FACTOR33*(Dx**2*PBx+PBx*PABID2+Dx*PABI)*(PAy)*(PAz*PBz+PABID2)
      SHLINTSXX(36)=SHLINTSXX(36)+FACTOR33*(Dx**2+PABID2)*(PAy*PBy+PABID2)*(PAz*PBz+PABID2)

! <d|YY|d>
      SHLINTSYY( 1)=SHLINTSYY( 1)+FACTOR*(PAx**2*PBx**2+PAx**2*PABID2+PAx*PBx*2.0D0*PABI+PBx**2*PABID2+ &
                                          t1)*(Dy**2+PABID2)
      SHLINTSYY( 2)=SHLINTSYY( 2)+FACTOR*(PAx**2+PABID2)*(Dy**2*PBy**2+PBy**2*PABID2+Dy*PBy*2.0D0*PABI+ &
                                          Dy**2*PABID2+t1)
      SHLINTSYY( 3)=SHLINTSYY( 3)+FACTOR*(PAx**2+PABID2)*(Dy**2+PABID2)*(PBz**2+PABID2)
      SHLINTSYY( 4)=SHLINTSYY( 4)+FACTOR3*(PAx**2*PBx+PAx*PABI+PBx*PABID2)*(Dy**2*PBy+PBy*PABID2+Dy*PABI)
      SHLINTSYY( 5)=SHLINTSYY( 5)+FACTOR3*(PAx**2*PBx+PAx*PABI+PBx*PABID2)*(Dy**2+PABID2)*(PBz)
      SHLINTSYY( 6)=SHLINTSYY( 6)+FACTOR3*(PAx**2+PABID2)*(Dy**2*PBy+PBy*PABID2+Dy*PABI)*(PBz)
      SHLINTSYY( 7)=SHLINTSYY( 7)+FACTOR*(PBx**2+PABID2)*(Dy**2*PAy**2+PAy**2*PABID2+Dy*PAy*2.0D0*PABI+ &
                                          Dy**2*PABID2+t1)
      SHLINTSYY( 8)=SHLINTSYY( 8)+FACTOR*(Dy**2*PAy**2*PBy**2+PAy**2*PBy**2*PABID2+Dy*PAy**2*PBy*4*PABID2+ &
                                          Dy**2*PAy**2*PABID2+PAy**2*t1+Dy*PAy*PBy**2*4*PABID2+ &
                                          Dy**2*PAy*PBy*2.0D0*PABI+PAy*PBy*12.0D0*PABID2**2+ &
                                          Dy*PAy*12.0D0*PABID2**2+Dy**2*PBy**2*PABID2+PBy**2*t1+ &
                                          Dy*PBy*12.0D0*PABID2**2+Dy**2*t1+(15)*PABID2**3)
      SHLINTSYY( 9)=SHLINTSYY( 9)+FACTOR*(Dy**2*PAy**2+PAy**2*PABID2+Dy*PAy*2.0D0*PABI+Dy**2*PABID2+ &
                                          t1)*(PBz**2+PABID2)
      SHLINTSYY(10)=SHLINTSYY(10)+FACTOR3*(PBx)*(Dy**2*PAy**2*PBy+PAy**2*PBy*PABID2+Dy*PAy**2*PABI+ &
                                          Dy*PAy*PBy*2.0D0*PABI+Dy**2*PAy*PABI+PAy*t2+ &
                                          Dy**2*PBy*PABID2+PBy*t1+Dy*t2)
      SHLINTSYY(11)=SHLINTSYY(11)+FACTOR3*(PBx)*(Dy**2*PAy**2+PAy**2*PABID2+Dy*PAy*2.0D0*PABI+Dy**2*PABID2+ &
                                          t1)*(PBz)
      SHLINTSYY(12)=SHLINTSYY(12)+FACTOR3*(Dy**2*PAy**2*PBy+PAy**2*PBy*PABID2+Dy*PAy**2*PABI+Dy*PAy*PBy*2.0D0*PABI+ &
                                          Dy**2*PAy*PABI+PAy*t2+Dy**2*PBy*PABID2+ &
                                          PBy*t1+Dy*t2)*(PBz)
      SHLINTSYY(13)=SHLINTSYY(13)+FACTOR*(PBx**2+PABID2)*(Dy**2+PABID2)*(PAz**2+PABID2)
      SHLINTSYY(14)=SHLINTSYY(14)+FACTOR*(Dy**2*PBy**2+PBy**2*PABID2+Dy*PBy*2.0D0*PABI+Dy**2*PABID2+ &
                                          t1)*(PAz**2+PABID2)
      SHLINTSYY(15)=SHLINTSYY(15)+FACTOR*(Dy**2+PABID2)*(PAz**2*PBz**2+PAz**2*PABID2+PAz*PBz*2.0D0*PABI+ &
                                          PBz**2*PABID2+t1)
      SHLINTSYY(16)=SHLINTSYY(16)+FACTOR3*(PBx)*(Dy**2*PBy+PBy*PABID2+Dy*PABI)*(PAz**2+PABID2)
      SHLINTSYY(17)=SHLINTSYY(17)+FACTOR3*(PBx)*(Dy**2+PABID2)*(PAz**2*PBz+PAz*PABI+PBz*PABID2)
      SHLINTSYY(18)=SHLINTSYY(18)+FACTOR3*(Dy**2*PBy+PBy*PABID2+Dy*PABI)*(PAz**2*PBz+PAz*PABI+PBz*PABID2)
      SHLINTSYY(19)=SHLINTSYY(19)+FACTOR3*(PAx*PBx**2+PAx*PABID2+PBx*PABI)*(Dy**2*PAy+PAy*PABID2+Dy*PABI)
      SHLINTSYY(20)=SHLINTSYY(20)+FACTOR3*(PAx)*(Dy**2*PAy*PBy**2+PAy*PBy**2*PABID2+Dy*PAy*PBy*2.0D0*PABI+ &
                                          Dy**2*PAy*PABID2+PAy*t1+Dy*PBy**2*PABI+Dy**2*PBy*PABI+ &
                                          PBy*t2+Dy*t2)
      SHLINTSYY(21)=SHLINTSYY(21)+FACTOR3*(PAx)*(Dy**2*PAy+PAy*PABID2+Dy*PABI)*(PBz**2+PABID2)
      SHLINTSYY(22)=SHLINTSYY(22)+FACTOR33*(PAx*PBx+PABID2)*(Dy**2*PAy*PBy+PAy*PBy*PABID2+Dy*PAy*PABI+ &
                                          Dy*PBy*PABI+Dy**2*PABID2+t1)
      SHLINTSYY(23)=SHLINTSYY(23)+FACTOR33*(PAx*PBx+PABID2)*(Dy**2*PAy+PAy*PABID2+Dy*PABI)*(PBz)
      SHLINTSYY(24)=SHLINTSYY(24)+FACTOR33*(PAx)*(Dy**2*PAy*PBy+PAy*PBy*PABID2+Dy*PAy*PABI+Dy*PBy*PABI+ &
                                          Dy**2*PABID2+t1)*(PBz)
      SHLINTSYY(25)=SHLINTSYY(25)+FACTOR3*(PAx*PBx**2+PAx*PABID2+PBx*PABI)*(Dy**2+PABID2)*(PAz)
      SHLINTSYY(26)=SHLINTSYY(26)+FACTOR3*(PAx)*(Dy**2*PBy**2+PBy**2*PABID2+Dy*PBy*2.0D0*PABI+Dy**2*PABID2+ &
                                          t1)*(PAz)
      SHLINTSYY(27)=SHLINTSYY(27)+FACTOR3*(PAx)*(Dy**2+PABID2)*(PAz*PBz**2+PAz*PABID2+PBz*PABI)
      SHLINTSYY(28)=SHLINTSYY(28)+FACTOR33*(PAx*PBx+PABID2)*(Dy**2*PBy+PBy*PABID2+Dy*PABI)*(PAz)
      SHLINTSYY(29)=SHLINTSYY(29)+FACTOR33*(PAx*PBx+PABID2)*(Dy**2+PABID2)*(PAz*PBz+PABID2)
      SHLINTSYY(30)=SHLINTSYY(30)+FACTOR33*(PAx)*(Dy**2*PBy+PBy*PABID2+Dy*PABI)*(PAz*PBz+PABID2)
      SHLINTSYY(31)=SHLINTSYY(31)+FACTOR3*(PBx**2+PABID2)*(Dy**2*PAy+PAy*PABID2+Dy*PABI)*(PAz)
      SHLINTSYY(32)=SHLINTSYY(32)+FACTOR3*(Dy**2*PAy*PBy**2+PAy*PBy**2*PABID2+Dy*PAy*PBy*2.0D0*PABI+ &
                                          Dy**2*PAy*PABID2+PAy*t1+Dy*PBy**2*PABI+Dy**2*PBy*PABI+ &
                                          PBy*t2+Dy*t2)*(PAz)
      SHLINTSYY(33)=SHLINTSYY(33)+FACTOR3*(Dy**2*PAy+PAy*PABID2+Dy*PABI)*(PAz*PBz**2+PAz*PABID2+PBz*PABI)
      SHLINTSYY(34)=SHLINTSYY(34)+FACTOR33*(PBx)*(Dy**2*PAy*PBy+PAy*PBy*PABID2+Dy*PAy*PABI+Dy*PBy*PABI+ &
                                          Dy**2*PABID2+t1)*(PAz)
      SHLINTSYY(35)=SHLINTSYY(35)+FACTOR33*(PBx)*(Dy**2*PAy+PAy*PABID2+Dy*PABI)*(PAz*PBz+PABID2)
      SHLINTSYY(36)=SHLINTSYY(36)+FACTOR33*(Dy**2*PAy*PBy+PAy*PBy*PABID2+Dy*PAy*PABI+Dy*PBy*PABI+ &
                                          Dy**2*PABID2+t1)*(PAz*PBz+PABID2)

! <d|ZZ|d>
      SHLINTSZZ( 1)=SHLINTSZZ( 1)+FACTOR*(PAx**2*PBx**2+PAx**2*PABID2+PAx*PBx*2.0D0*PABI+PBx**2*PABID2+ &
                                          t1)*(Dz**2+PABID2)
      SHLINTSZZ( 2)=SHLINTSZZ( 2)+FACTOR*(PAx**2+PABID2)*(PBy**2+PABID2)*(Dz**2+PABID2)
      SHLINTSZZ( 3)=SHLINTSZZ( 3)+FACTOR*(PAx**2+PABID2)*(Dz**2*PBz**2+PBz**2*PABID2+Dz*PBz*2.0D0*PABI+ &
                                          Dz**2*PABID2+t1)
      SHLINTSZZ( 4)=SHLINTSZZ( 4)+FACTOR3*(PAx**2*PBx+PAx*PABI+PBx*PABID2)*(PBy)*(Dz**2+PABID2)
      SHLINTSZZ( 5)=SHLINTSZZ( 5)+FACTOR3*(PAx**2*PBx+PAx*PABI+PBx*PABID2)*(Dz**2*PBz+PBz*PABID2+Dz*PABI)
      SHLINTSZZ( 6)=SHLINTSZZ( 6)+FACTOR3*(PAx**2+PABID2)*(PBy)*(Dz**2*PBz+PBz*PABID2+Dz*PABI)
      SHLINTSZZ( 7)=SHLINTSZZ( 7)+FACTOR*(PBx**2+PABID2)*(PAy**2+PABID2)*(Dz**2+PABID2)
      SHLINTSZZ( 8)=SHLINTSZZ( 8)+FACTOR*(PAy**2*PBy**2+PAy**2*PABID2+PAy*PBy*2.0D0*PABI+PBy**2*PABID2+ &
                                          t1)*(Dz**2+PABID2)
      SHLINTSZZ( 9)=SHLINTSZZ( 9)+FACTOR*(PAy**2+PABID2)*(Dz**2*PBz**2+PBz**2*PABID2+Dz*PBz*2.0D0*PABI+ &
                                          Dz**2*PABID2+t1)
      SHLINTSZZ(10)=SHLINTSZZ(10)+FACTOR3*(PBx)*(PAy**2*PBy+PAy*PABI+PBy*PABID2)*(Dz**2+PABID2)
      SHLINTSZZ(11)=SHLINTSZZ(11)+FACTOR3*(PBx)*(PAy**2+PABID2)*(Dz**2*PBz+PBz*PABID2+Dz*PABI)
      SHLINTSZZ(12)=SHLINTSZZ(12)+FACTOR3*(PAy**2*PBy+PAy*PABI+PBy*PABID2)*(Dz**2*PBz+PBz*PABID2+Dz*PABI)
      SHLINTSZZ(13)=SHLINTSZZ(13)+FACTOR*(PBx**2+PABID2)*(Dz**2*PAz**2+PAz**2*PABID2+Dz*PAz*2.0D0*PABI+ &
                                          Dz**2*PABID2+t1)
      SHLINTSZZ(14)=SHLINTSZZ(14)+FACTOR*(PBy**2+PABID2)*(Dz**2*PAz**2+PAz**2*PABID2+Dz*PAz*2.0D0*PABI+ &
                                          Dz**2*PABID2+t1)
      SHLINTSZZ(15)=SHLINTSZZ(15)+FACTOR*(Dz**2*PAz**2*PBz**2+PAz**2*PBz**2*PABID2+Dz*PAz**2*PBz*4*PABID2+ &
                                          Dz**2*PAz**2*PABID2+PAz**2*t1+Dz*PAz*PBz**2*4*PABID2+ &
                                          Dz**2*PAz*PBz*2.0D0*PABI+PAz*PBz*12.0D0*PABID2**2+ &
                                          Dz*PAz*12.0D0*PABID2**2+Dz**2*PBz**2*PABID2+ &
                                          PBz**2*t1+Dz*PBz*12.0D0*PABID2**2+ &
                                          Dz**2*t1+(15)*PABID2**3)
      SHLINTSZZ(16)=SHLINTSZZ(16)+FACTOR3*(PBx)*(PBy)*(Dz**2*PAz**2+PAz**2*PABID2+Dz*PAz*2.0D0*PABI+ &
                                          Dz**2*PABID2+t1)
      SHLINTSZZ(17)=SHLINTSZZ(17)+FACTOR3*(PBx)*(Dz**2*PAz**2*PBz+PAz**2*PBz*PABID2+Dz*PAz**2*PABI+ &
                                          Dz*PAz*PBz*2.0D0*PABI+Dz**2*PAz*PABI+PAz*t2+ &
                                          Dz**2*PBz*PABID2+PBz*t1+Dz*t2)
      SHLINTSZZ(18)=SHLINTSZZ(18)+FACTOR3*(PBy)*(Dz**2*PAz**2*PBz+PAz**2*PBz*PABID2+Dz*PAz**2*PABI+ &
                                          Dz*PAz*PBz*2.0D0*PABI+Dz**2*PAz*PABI+PAz*t2+ &
                                          Dz**2*PBz*PABID2+PBz*t1+Dz*t2)
      SHLINTSZZ(19)=SHLINTSZZ(19)+FACTOR3*(PAx*PBx**2+PAx*PABID2+PBx*PABI)*(PAy)*(Dz**2+PABID2)
      SHLINTSZZ(20)=SHLINTSZZ(20)+FACTOR3*(PAx)*(PAy*PBy**2+PAy*PABID2+PBy*PABI)*(Dz**2+PABID2)
      SHLINTSZZ(21)=SHLINTSZZ(21)+FACTOR3*(PAx)*(PAy)*(Dz**2*PBz**2+PBz**2*PABID2+Dz*PBz*2.0D0*PABI+ &
                                          Dz**2*PABID2+t1)
      SHLINTSZZ(22)=SHLINTSZZ(22)+FACTOR33*(PAx*PBx+PABID2)*(PAy*PBy+PABID2)*(Dz**2+PABID2)
      SHLINTSZZ(23)=SHLINTSZZ(23)+FACTOR33*(PAx*PBx+PABID2)*(PAy)*(Dz**2*PBz+PBz*PABID2+Dz*PABI)
      SHLINTSZZ(24)=SHLINTSZZ(24)+FACTOR33*(PAx)*(PAy*PBy+PABID2)*(Dz**2*PBz+PBz*PABID2+Dz*PABI)
      SHLINTSZZ(25)=SHLINTSZZ(25)+FACTOR3*(PAx*PBx**2+PAx*PABID2+PBx*PABI)*(Dz**2*PAz+PAz*PABID2+Dz*PABI)
      SHLINTSZZ(26)=SHLINTSZZ(26)+FACTOR3*(PAx)*(PBy**2+PABID2)*(Dz**2*PAz+PAz*PABID2+Dz*PABI)
      SHLINTSZZ(27)=SHLINTSZZ(27)+FACTOR3*(PAx)*(Dz**2*PAz*PBz**2+PAz*PBz**2*PABID2+Dz*PAz*PBz*2.0D0*PABI+ &
                                          Dz**2*PAz*PABID2+PAz*t1+Dz*PBz**2*PABI+ &
                                          Dz**2*PBz*PABI+PBz*t2+Dz*t2)
      SHLINTSZZ(28)=SHLINTSZZ(28)+FACTOR33*(PAx*PBx+PABID2)*(PBy)*(Dz**2*PAz+PAz*PABID2+Dz*PABI)
      SHLINTSZZ(29)=SHLINTSZZ(29)+FACTOR33*(PAx*PBx+PABID2)*(Dz**2*PAz*PBz+PAz*PBz*PABID2+Dz*PAz*PABI+ &
                                          Dz*PBz*PABI+Dz**2*PABID2+t1)
      SHLINTSZZ(30)=SHLINTSZZ(30)+FACTOR33*(PAx)*(PBy)*(Dz**2*PAz*PBz+PAz*PBz*PABID2+Dz*PAz*PABI+ &
                                          Dz*PBz*PABI+Dz**2*PABID2+t1)
      SHLINTSZZ(31)=SHLINTSZZ(31)+FACTOR3*(PBx**2+PABID2)*(PAy)*(Dz**2*PAz+PAz*PABID2+Dz*PABI)
      SHLINTSZZ(32)=SHLINTSZZ(32)+FACTOR3*(PAy*PBy**2+PAy*PABID2+PBy*PABI)*(Dz**2*PAz+PAz*PABID2+Dz*PABI)
      SHLINTSZZ(33)=SHLINTSZZ(33)+FACTOR3*(PAy)*(Dz**2*PAz*PBz**2+PAz*PBz**2*PABID2+Dz*PAz*PBz*2.0D0*PABI+ &
                                          Dz**2*PAz*PABID2+PAz*t1+Dz*PBz**2*PABI+ &
                                          Dz**2*PBz*PABI+PBz*t2+Dz*t2)
      SHLINTSZZ(34)=SHLINTSZZ(34)+FACTOR33*(PBx)*(PAy*PBy+PABID2)*(Dz**2*PAz+PAz*PABID2+Dz*PABI)
      SHLINTSZZ(35)=SHLINTSZZ(35)+FACTOR33*(PBx)*(PAy)*(Dz**2*PAz*PBz+PAz*PBz*PABID2+Dz*PAz*PABI+ &
                                          Dz*PBz*PABI+Dz**2*PABID2+t1)
      SHLINTSZZ(36)=SHLINTSZZ(36)+FACTOR33*(PAy*PBy+PABID2)*(Dz**2*PAz*PBz+PAz*PBz*PABID2+Dz*PAz*PABI+ &
                                          Dz*PBz*PABI+Dz**2*PABID2+t1)

! <d|XY|d>      
      SHLINTSXY( 1)=SHLINTSXY( 1)+FACTOR*(Dx*PAx**2*PBx**2+PAx**2*PBx*PABI+Dx*PAx**2*PABID2+PAx*PBx**2*PABI+ &
                                          Dx*PAx*PBx*2.0D0*PABI+PAx*t2+Dx*PBx**2*PABID2+ &
                                          PBx*t2+Dx*t1)*(Dy)
      SHLINTSXY( 2)=SHLINTSXY( 2)+FACTOR*(Dx*PAx**2+PAx*PABI+Dx*PABID2)*(Dy*PBy**2+PBy*PABI+Dy*PABID2)
      SHLINTSXY( 3)=SHLINTSXY( 3)+FACTOR*(Dx*PAx**2+PAx*PABI+Dx*PABID2)*(Dy)*(PBz**2+PABID2)
      SHLINTSXY( 4)=SHLINTSXY( 4)+FACTOR3*(Dx*PAx**2*PBx+PAx**2*PABID2+PAx*PBx*PABI+Dx*PAx*PABI+ &
                                          Dx*PBx*PABID2+t1)*(Dy*PBy+PABID2)
      SHLINTSXY( 5)=SHLINTSXY( 5)+FACTOR3*(Dx*PAx**2*PBx+PAx**2*PABID2+PAx*PBx*PABI+Dx*PAx*PABI+ &
                                          Dx*PBx*PABID2+t1)*(Dy)*(PBz)
      SHLINTSXY( 6)=SHLINTSXY( 6)+FACTOR3*(Dx*PAx**2+PAx*PABI+Dx*PABID2)*(Dy*PBy+PABID2)*(PBz)
      SHLINTSXY( 7)=SHLINTSXY( 7)+FACTOR*(Dx*PBx**2+PBx*PABI+Dx*PABID2)*(Dy*PAy**2+PAy*PABI+Dy*PABID2)
      SHLINTSXY( 8)=SHLINTSXY( 8)+FACTOR*(Dx)*(Dy*PAy**2*PBy**2+PAy**2*PBy*PABI+Dy*PAy**2*PABID2+ &
                                          PAy*PBy**2*PABI+Dy*PAy*PBy*2.0D0*PABI+PAy*t2+ &
                                          Dy*PBy**2*PABID2+PBy*t2+Dy*t1)
      SHLINTSXY( 9)=SHLINTSXY( 9)+FACTOR*(Dx)*(Dy*PAy**2+PAy*PABI+Dy*PABID2)*(PBz**2+PABID2)
      SHLINTSXY(10)=SHLINTSXY(10)+FACTOR3*(Dx*PBx+PABID2)*(Dy*PAy**2*PBy+PAy**2*PABID2+PAy*PBy*PABI+ &
                                          Dy*PAy*PABI+Dy*PBy*PABID2+t1)
      SHLINTSXY(11)=SHLINTSXY(11)+FACTOR3*(Dx*PBx+PABID2)*(Dy*PAy**2+PAy*PABI+Dy*PABID2)*(PBz)
      SHLINTSXY(12)=SHLINTSXY(12)+FACTOR3*(Dx)*(Dy*PAy**2*PBy+PAy**2*PABID2+PAy*PBy*PABI+Dy*PAy*PABI+ &
                                          Dy*PBy*PABID2+t1)*(PBz)
      SHLINTSXY(13)=SHLINTSXY(13)+FACTOR*(Dx*PBx**2+PBx*PABI+Dx*PABID2)*(Dy)*(PAz**2+PABID2)
      SHLINTSXY(14)=SHLINTSXY(14)+FACTOR*(Dx)*(Dy*PBy**2+PBy*PABI+Dy*PABID2)*(PAz**2+PABID2)
      SHLINTSXY(15)=SHLINTSXY(15)+FACTOR*(Dx)*(Dy)*(PAz**2*PBz**2+PAz**2*PABID2+PAz*PBz*2.0D0*PABI+ &
                                          PBz**2*PABID2+t1)
      SHLINTSXY(16)=SHLINTSXY(16)+FACTOR3*(Dx*PBx+PABID2)*(Dy*PBy+PABID2)*(PAz**2+PABID2)
      SHLINTSXY(17)=SHLINTSXY(17)+FACTOR3*(Dx*PBx+PABID2)*(Dy)*(PAz**2*PBz+PAz*PABI+PBz*PABID2)
      SHLINTSXY(18)=SHLINTSXY(18)+FACTOR3*(Dx)*(Dy*PBy+PABID2)*(PAz**2*PBz+PAz*PABI+PBz*PABID2)
      SHLINTSXY(19)=SHLINTSXY(19)+FACTOR3*(Dx*PAx*PBx**2+PAx*PBx*PABI+Dx*PAx*PABID2+PBx**2*PABID2+ &
                                          Dx*PBx*PABI+t1)*(Dy*PAy+PABID2)
      SHLINTSXY(20)=SHLINTSXY(20)+FACTOR3*(Dx*PAx+PABID2)*(Dy*PAy*PBy**2+PAy*PBy*PABI+Dy*PAy*PABID2+ &
                                          PBy**2*PABID2+Dy*PBy*PABI+t1)
      SHLINTSXY(21)=SHLINTSXY(21)+FACTOR3*(Dx*PAx+PABID2)*(Dy*PAy+PABID2)*(PBz**2+PABID2)
      SHLINTSXY(22)=SHLINTSXY(22)+FACTOR33*(Dx*PAx*PBx+PAx*PABID2+PBx*PABID2+Dx*PABID2)* &
                                         (Dy*PAy*PBy+PAy*PABID2+PBy*PABID2+Dy*PABID2)
      SHLINTSXY(23)=SHLINTSXY(23)+FACTOR33*(Dx*PAx*PBx+PAx*PABID2+PBx*PABID2+Dx*PABID2)*(Dy*PAy+PABID2)*(PBz)
      SHLINTSXY(24)=SHLINTSXY(24)+FACTOR33*(Dx*PAx+PABID2)*(Dy*PAy*PBy+PAy*PABID2+PBy*PABID2+Dy*PABID2)*(PBz)
      SHLINTSXY(25)=SHLINTSXY(25)+FACTOR3*(Dx*PAx*PBx**2+PAx*PBx*PABI+Dx*PAx*PABID2+PBx**2*PABID2+ &
                                          Dx*PBx*PABI+t1)*(Dy)*(PAz)
      SHLINTSXY(26)=SHLINTSXY(26)+FACTOR3*(Dx*PAx+PABID2)*(Dy*PBy**2+PBy*PABI+Dy*PABID2)*(PAz)
      SHLINTSXY(27)=SHLINTSXY(27)+FACTOR3*(Dx*PAx+PABID2)*(Dy)*(PAz*PBz**2+PAz*PABID2+PBz*PABI)
      SHLINTSXY(28)=SHLINTSXY(28)+FACTOR33*(Dx*PAx*PBx+PAx*PABID2+PBx*PABID2+Dx*PABID2)*(Dy*PBy+PABID2)*(PAz)
      SHLINTSXY(29)=SHLINTSXY(29)+FACTOR33*(Dx*PAx*PBx+PAx*PABID2+PBx*PABID2+Dx*PABID2)*(Dy)*(PAz*PBz+PABID2)
      SHLINTSXY(30)=SHLINTSXY(30)+FACTOR33*(Dx*PAx+PABID2)*(Dy*PBy+PABID2)*(PAz*PBz+PABID2)
      SHLINTSXY(31)=SHLINTSXY(31)+FACTOR3*(Dx*PBx**2+PBx*PABI+Dx*PABID2)*(Dy*PAy+PABID2)*(PAz)
      SHLINTSXY(32)=SHLINTSXY(32)+FACTOR3*(Dx)*(Dy*PAy*PBy**2+PAy*PBy*PABI+Dy*PAy*PABID2+PBy**2*PABID2+ &
                                          Dy*PBy*PABI+t1)*(PAz)
      SHLINTSXY(33)=SHLINTSXY(33)+FACTOR3*(Dx)*(Dy*PAy+PABID2)*(PAz*PBz**2+PAz*PABID2+PBz*PABI)
      SHLINTSXY(34)=SHLINTSXY(34)+FACTOR33*(Dx*PBx+PABID2)*(Dy*PAy*PBy+PAy*PABID2+PBy*PABID2+Dy*PABID2)*(PAz)
      SHLINTSXY(35)=SHLINTSXY(35)+FACTOR33*(Dx*PBx+PABID2)*(Dy*PAy+PABID2)*(PAz*PBz+PABID2)
      SHLINTSXY(36)=SHLINTSXY(36)+FACTOR33*(Dx)*(Dy*PAy*PBy+PAy*PABID2+PBy*PABID2+Dy*PABID2)*(PAz*PBz+PABID2)

! <d|XZ|d>      
      SHLINTSXZ( 1)=SHLINTSXZ( 1)+FACTOR*(Dx*PAx**2*PBx**2+PAx**2*PBx*PABI+Dx*PAx**2*PABID2+PAx*PBx**2*PABI+ &
                                          Dx*PAx*PBx*2.0D0*PABI+PAx*t2+Dx*PBx**2*PABID2+ &
                                          PBx*t2+Dx*t1)*(Dz)
      SHLINTSXZ( 2)=SHLINTSXZ( 2)+FACTOR*(Dx*PAx**2+PAx*PABI+Dx*PABID2)*(PBy**2+PABID2)*(Dz)
      SHLINTSXZ( 3)=SHLINTSXZ( 3)+FACTOR*(Dx*PAx**2+PAx*PABI+Dx*PABID2)*(Dz*PBz**2+PBz*PABI+Dz*PABID2)
      SHLINTSXZ( 4)=SHLINTSXZ( 4)+FACTOR3*(Dx*PAx**2*PBx+PAx**2*PABID2+PAx*PBx*PABI+Dx*PAx*PABI+ &
                                          Dx*PBx*PABID2+t1)*(PBy)*(Dz)
      SHLINTSXZ( 5)=SHLINTSXZ( 5)+FACTOR3*(Dx*PAx**2*PBx+PAx**2*PABID2+PAx*PBx*PABI+Dx*PAx*PABI+ &
                                          Dx*PBx*PABID2+t1)*(Dz*PBz+PABID2)
      SHLINTSXZ( 6)=SHLINTSXZ( 6)+FACTOR3*(Dx*PAx**2+PAx*PABI+Dx*PABID2)*(PBy)*(Dz*PBz+PABID2)
      SHLINTSXZ( 7)=SHLINTSXZ( 7)+FACTOR*(Dx*PBx**2+PBx*PABI+Dx*PABID2)*(PAy**2+PABID2)*(Dz)
      SHLINTSXZ( 8)=SHLINTSXZ( 8)+FACTOR*(Dx)*(PAy**2*PBy**2+PAy**2*PABID2+PAy*PBy*2.0D0*PABI+ &
                                          PBy**2*PABID2+t1)*(Dz)
      SHLINTSXZ( 9)=SHLINTSXZ( 9)+FACTOR*(Dx)*(PAy**2+PABID2)*(Dz*PBz**2+PBz*PABI+Dz*PABID2)
      SHLINTSXZ(10)=SHLINTSXZ(10)+FACTOR3*(Dx*PBx+PABID2)*(PAy**2*PBy+PAy*PABI+PBy*PABID2)*(Dz)
      SHLINTSXZ(11)=SHLINTSXZ(11)+FACTOR3*(Dx*PBx+PABID2)*(PAy**2+PABID2)*(Dz*PBz+PABID2)
      SHLINTSXZ(12)=SHLINTSXZ(12)+FACTOR3*(Dx)*(PAy**2*PBy+PAy*PABI+PBy*PABID2)*(Dz*PBz+PABID2)
      SHLINTSXZ(13)=SHLINTSXZ(13)+FACTOR*(Dx*PBx**2+PBx*PABI+Dx*PABID2)*(Dz*PAz**2+PAz*PABI+Dz*PABID2)
      SHLINTSXZ(14)=SHLINTSXZ(14)+FACTOR*(Dx)*(PBy**2+PABID2)*(Dz*PAz**2+PAz*PABI+Dz*PABID2)
      SHLINTSXZ(15)=SHLINTSXZ(15)+FACTOR*(Dx)*(Dz*PAz**2*PBz**2+PAz**2*PBz*PABI+Dz*PAz**2*PABID2+ &
                                          PAz*PBz**2*PABI+Dz*PAz*PBz*2.0D0*PABI+PAz*t2+ &
                                          Dz*PBz**2*PABID2+PBz*t2+Dz*t1)
      SHLINTSXZ(16)=SHLINTSXZ(16)+FACTOR3*(Dx*PBx+PABID2)*(PBy)*(Dz*PAz**2+PAz*PABI+Dz*PABID2)
      SHLINTSXZ(17)=SHLINTSXZ(17)+FACTOR3*(Dx*PBx+PABID2)*(Dz*PAz**2*PBz+PAz**2*PABID2+PAz*PBz*PABI+ &
                                          Dz*PAz*PABI+Dz*PBz*PABID2+t1)
      SHLINTSXZ(18)=SHLINTSXZ(18)+FACTOR3*(Dx)*(PBy)*(Dz*PAz**2*PBz+PAz**2*PABID2+PAz*PBz*PABI+ &
                                          Dz*PAz*PABI+Dz*PBz*PABID2+t1)
      SHLINTSXZ(19)=SHLINTSXZ(19)+FACTOR3*(Dx*PAx*PBx**2+PAx*PBx*PABI+Dx*PAx*PABID2+PBx**2*PABID2+ &
                                          Dx*PBx*PABI+t1)*(PAy)*(Dz)
      SHLINTSXZ(20)=SHLINTSXZ(20)+FACTOR3*(Dx*PAx+PABID2)*(PAy*PBy**2+PAy*PABID2+PBy*PABI)*(Dz)
      SHLINTSXZ(21)=SHLINTSXZ(21)+FACTOR3*(Dx*PAx+PABID2)*(PAy)*(Dz*PBz**2+PBz*PABI+Dz*PABID2)
      SHLINTSXZ(22)=SHLINTSXZ(22)+FACTOR33*(Dx*PAx*PBx+PAx*PABID2+PBx*PABID2+Dx*PABID2)*(PAy*PBy+PABID2)*(Dz)
      SHLINTSXZ(23)=SHLINTSXZ(23)+FACTOR33*(Dx*PAx*PBx+PAx*PABID2+PBx*PABID2+Dx*PABID2)*(PAy)*(Dz*PBz+PABID2)
      SHLINTSXZ(24)=SHLINTSXZ(24)+FACTOR33*(Dx*PAx+PABID2)*(PAy*PBy+PABID2)*(Dz*PBz+PABID2)
      SHLINTSXZ(25)=SHLINTSXZ(25)+FACTOR3*(Dx*PAx*PBx**2+PAx*PBx*PABI+Dx*PAx*PABID2+PBx**2*PABID2+ &
                                          Dx*PBx*PABI+t1)*(Dz*PAz+PABID2)
      SHLINTSXZ(26)=SHLINTSXZ(26)+FACTOR3*(Dx*PAx+PABID2)*(PBy**2+PABID2)*(Dz*PAz+PABID2)
      SHLINTSXZ(27)=SHLINTSXZ(27)+FACTOR3*(Dx*PAx+PABID2)*(Dz*PAz*PBz**2+PAz*PBz*PABI+Dz*PAz*PABID2+ &
                                          PBz**2*PABID2+Dz*PBz*PABI+t1)
      SHLINTSXZ(28)=SHLINTSXZ(28)+FACTOR33*(Dx*PAx*PBx+PAx*PABID2+PBx*PABID2+Dx*PABID2)*(PBy)*(Dz*PAz+PABID2)
      SHLINTSXZ(29)=SHLINTSXZ(29)+FACTOR33*(Dx*PAx*PBx+PAx*PABID2+PBx*PABID2+Dx*PABID2)* &
                                         (Dz*PAz*PBz+PAz*PABID2+PBz*PABID2+Dz*PABID2)
      SHLINTSXZ(30)=SHLINTSXZ(30)+FACTOR33*(Dx*PAx+PABID2)*(PBy)*(Dz*PAz*PBz+PAz*PABID2+PBz*PABID2+Dz*PABID2)
      SHLINTSXZ(31)=SHLINTSXZ(31)+FACTOR3*(Dx*PBx**2+PBx*PABI+Dx*PABID2)*(PAy)*(Dz*PAz+PABID2)
      SHLINTSXZ(32)=SHLINTSXZ(32)+FACTOR3*(Dx)*(PAy*PBy**2+PAy*PABID2+PBy*PABI)*(Dz*PAz+PABID2)
      SHLINTSXZ(33)=SHLINTSXZ(33)+FACTOR3*(Dx)*(PAy)*(Dz*PAz*PBz**2+PAz*PBz*PABI+Dz*PAz*PABID2+ &
                                          PBz**2*PABID2+Dz*PBz*PABI+t1)
      SHLINTSXZ(34)=SHLINTSXZ(34)+FACTOR33*(Dx*PBx+PABID2)*(PAy*PBy+PABID2)*(Dz*PAz+PABID2)
      SHLINTSXZ(35)=SHLINTSXZ(35)+FACTOR33*(Dx*PBx+PABID2)*(PAy)*(Dz*PAz*PBz+PAz*PABID2+PBz*PABID2+Dz*PABID2)
      SHLINTSXZ(36)=SHLINTSXZ(36)+FACTOR33*(Dx)*(PAy*PBy+PABID2)*(Dz*PAz*PBz+PAz*PABID2+PBz*PABID2+Dz*PABID2)

! <d|YZ|d>
      SHLINTSYZ( 1)=SHLINTSYZ( 1)+FACTOR*(PAx**2*PBx**2+PAx**2*PABID2+PAx*PBx*2.0D0*PABI+PBx**2*PABID2+ &
                                          t1)*(Dy)*(Dz)
      SHLINTSYZ( 2)=SHLINTSYZ( 2)+FACTOR*(PAx**2+PABID2)*(Dy*PBy**2+PBy*PABI+Dy*PABID2)*(Dz)
      SHLINTSYZ( 3)=SHLINTSYZ( 3)+FACTOR*(PAx**2+PABID2)*(Dy)*(Dz*PBz**2+PBz*PABI+Dz*PABID2)
      SHLINTSYZ( 4)=SHLINTSYZ( 4)+FACTOR3*(PAx**2*PBx+PAx*PABI+PBx*PABID2)*(Dy*PBy+PABID2)*(Dz)
      SHLINTSYZ( 5)=SHLINTSYZ( 5)+FACTOR3*(PAx**2*PBx+PAx*PABI+PBx*PABID2)*(Dy)*(Dz*PBz+PABID2)
      SHLINTSYZ( 6)=SHLINTSYZ( 6)+FACTOR3*(PAx**2+PABID2)*(Dy*PBy+PABID2)*(Dz*PBz+PABID2)
      SHLINTSYZ( 7)=SHLINTSYZ( 7)+FACTOR*(PBx**2+PABID2)*(Dy*PAy**2+PAy*PABI+Dy*PABID2)*(Dz)
      SHLINTSYZ( 8)=SHLINTSYZ( 8)+FACTOR*(Dy*PAy**2*PBy**2+PAy**2*PBy*PABI+Dy*PAy**2*PABID2+ &
                                          PAy*PBy**2*PABI+Dy*PAy*PBy*2.0D0*PABI+PAy*t2+ &
                                          Dy*PBy**2*PABID2+PBy*t2+Dy*t1)*(Dz)
      SHLINTSYZ( 9)=SHLINTSYZ( 9)+FACTOR*(Dy*PAy**2+PAy*PABI+Dy*PABID2)*(Dz*PBz**2+PBz*PABI+Dz*PABID2)
      SHLINTSYZ(10)=SHLINTSYZ(10)+FACTOR3*(PBx)*(Dy*PAy**2*PBy+PAy**2*PABID2+PAy*PBy*PABI+Dy*PAy*PABI+ &
                                          Dy*PBy*PABID2+t1)*(Dz)
      SHLINTSYZ(11)=SHLINTSYZ(11)+FACTOR3*(PBx)*(Dy*PAy**2+PAy*PABI+Dy*PABID2)*(Dz*PBz+PABID2)
      SHLINTSYZ(12)=SHLINTSYZ(12)+FACTOR3*(Dy*PAy**2*PBy+PAy**2*PABID2+PAy*PBy*PABI+Dy*PAy*PABI+ &
                                          Dy*PBy*PABID2+t1)*(Dz*PBz+PABID2)
      SHLINTSYZ(13)=SHLINTSYZ(13)+FACTOR*(PBx**2+PABID2)*(Dy)*(Dz*PAz**2+PAz*PABI+Dz*PABID2)
      SHLINTSYZ(14)=SHLINTSYZ(14)+FACTOR*(Dy*PBy**2+PBy*PABI+Dy*PABID2)*(Dz*PAz**2+PAz*PABI+Dz*PABID2)
      SHLINTSYZ(15)=SHLINTSYZ(15)+FACTOR*(Dy)*(Dz*PAz**2*PBz**2+PAz**2*PBz*PABI+Dz*PAz**2*PABID2+ &
                                          PAz*PBz**2*PABI+Dz*PAz*PBz*2.0D0*PABI+PAz*t2+ &
                                          Dz*PBz**2*PABID2+PBz*t2+Dz*t1)
      SHLINTSYZ(16)=SHLINTSYZ(16)+FACTOR3*(PBx)*(Dy*PBy+PABID2)*(Dz*PAz**2+PAz*PABI+Dz*PABID2)
      SHLINTSYZ(17)=SHLINTSYZ(17)+FACTOR3*(PBx)*(Dy)*(Dz*PAz**2*PBz+PAz**2*PABID2+PAz*PBz*PABI+ &
                                          Dz*PAz*PABI+Dz*PBz*PABID2+t1)
      SHLINTSYZ(18)=SHLINTSYZ(18)+FACTOR3*(Dy*PBy+PABID2)*(Dz*PAz**2*PBz+PAz**2*PABID2+PAz*PBz*PABI+ &
                                  Dz*PAz*PABI+Dz*PBz*PABID2+t1)
      SHLINTSYZ(19)=SHLINTSYZ(19)+FACTOR3*(PAx*PBx**2+PAx*PABID2+PBx*PABI)*(Dy*PAy+PABID2)*(Dz)
      SHLINTSYZ(20)=SHLINTSYZ(20)+FACTOR3*(PAx)*(Dy*PAy*PBy**2+PAy*PBy*PABI+Dy*PAy*PABID2+PBy**2*PABID2+ &
                                          Dy*PBy*PABI+t1)*(Dz)
      SHLINTSYZ(21)=SHLINTSYZ(21)+FACTOR3*(PAx)*(Dy*PAy+PABID2)*(Dz*PBz**2+PBz*PABI+Dz*PABID2)
      SHLINTSYZ(22)=SHLINTSYZ(22)+FACTOR33*(PAx*PBx+PABID2)*(Dy*PAy*PBy+PAy*PABID2+PBy*PABID2+Dy*PABID2)*(Dz)
      SHLINTSYZ(23)=SHLINTSYZ(23)+FACTOR33*(PAx*PBx+PABID2)*(Dy*PAy+PABID2)*(Dz*PBz+PABID2)
      SHLINTSYZ(24)=SHLINTSYZ(24)+FACTOR33*(PAx)*(Dy*PAy*PBy+PAy*PABID2+PBy*PABID2+Dy*PABID2)*(Dz*PBz+PABID2)
      SHLINTSYZ(25)=SHLINTSYZ(25)+FACTOR3*(PAx*PBx**2+PAx*PABID2+PBx*PABI)*(Dy)*(Dz*PAz+PABID2)
      SHLINTSYZ(26)=SHLINTSYZ(26)+FACTOR3*(PAx)*(Dy*PBy**2+PBy*PABI+Dy*PABID2)*(Dz*PAz+PABID2)
      SHLINTSYZ(27)=SHLINTSYZ(27)+FACTOR3*(PAx)*(Dy)*(Dz*PAz*PBz**2+PAz*PBz*PABI+Dz*PAz*PABID2+ &
                                          PBz**2*PABID2+Dz*PBz*PABI+t1)
      SHLINTSYZ(28)=SHLINTSYZ(28)+FACTOR33*(PAx*PBx+PABID2)*(Dy*PBy+PABID2)*(Dz*PAz+PABID2)
      SHLINTSYZ(29)=SHLINTSYZ(29)+FACTOR33*(PAx*PBx+PABID2)*(Dy)*(Dz*PAz*PBz+PAz*PABID2+PBz*PABID2+Dz*PABID2)
      SHLINTSYZ(30)=SHLINTSYZ(30)+FACTOR33*(PAx)*(Dy*PBy+PABID2)*(Dz*PAz*PBz+PAz*PABID2+PBz*PABID2+Dz*PABID2)
      SHLINTSYZ(31)=SHLINTSYZ(31)+FACTOR3*(PBx**2+PABID2)*(Dy*PAy+PABID2)*(Dz*PAz+PABID2)
      SHLINTSYZ(32)=SHLINTSYZ(32)+FACTOR3*(Dy*PAy*PBy**2+PAy*PBy*PABI+Dy*PAy*PABID2+PBy**2*PABID2+ &
                                          Dy*PBy*PABI+t1)*(Dz*PAz+PABID2)
      SHLINTSYZ(33)=SHLINTSYZ(33)+FACTOR3*(Dy*PAy+PABID2)*(Dz*PAz*PBz**2+PAz*PBz*PABI+Dz*PAz*PABID2+ &
                                          PBz**2*PABID2+Dz*PBz*PABI+t1)
      SHLINTSYZ(34)=SHLINTSYZ(34)+FACTOR33*(PBx)*(Dy*PAy*PBy+PAy*PABID2+PBy*PABID2+Dy*PABID2)*(Dz*PAz+PABID2)
      SHLINTSYZ(35)=SHLINTSYZ(35)+FACTOR33*(PBx)*(Dy*PAy+PABID2)*(Dz*PAz*PBz+PAz*PABID2+PBz*PABID2+Dz*PABID2)
      SHLINTSYZ(36)=SHLINTSYZ(36)+FACTOR33*(Dy*PAy*PBy+PAy*PABID2+PBy*PABID2+Dy*PABID2)* &
                                         (Dz*PAz*PBz+PAz*PABID2+PBz*PABID2+Dz*PABID2)
      
      return
      end subroutine Second_Mom_dd
