      subroutine MENU_MP2
!***********************************************************************
!     Date last modified: June 24, 1992                                *
!     Author: R.A. Poirier                                             *
!     Description: Set up MP2 calculation.                             *
!***********************************************************************
! Modules:
      USE QM_defaults
      USE program_parser
      USE menu_gets

      implicit none
!
! Local scalars:
      logical done,lrun
!
! Begin:
      call PRG_manager ('enter', 'MENU_MP2', 'UTILITY')
!
! Defaults:
! Make sure not combinations:
      integral_combinations=.false.
      I2E_type='RAW'

      done=.false.
      lrun=.false.
      MP2_full=.false.
      NFrozen_core=NFrozen_cores()
      NFrozen_virt=0
!
! Menu:
      do while (.not.done)
      call new_token (' MP2:')
!
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      ' Command MP2 :', &
      '   Purpose: Calculate MP2 energy contribution', &
      '   Syntax :', &
      '   FC = <integer>: Number of frozen core orbitals, Default=0', &
      '   FV = <integer>: Number of frozen virtual orbitals, Default=0', &
      '   FULL <string> : Transform all core and virtual orbitals', &
      '   MAx_trasform = <integer>, i.e., 505000 (default)', &
      '   MAx_trasform must be a multiple of Nbasis*Nbasis*(Nbasis+1)/2', &
      '   run : start executing the MP2', &
      '   end'
!
      else if(token(1:2).EQ.'FC')then
        call GET_value (NFrozen_core)

      else if(token(1:2).EQ.'FV')then
        call GET_value (NFrozen_virt)

      else if(token(1:4).EQ.'FULL')then
        call GET_value (MP2_full)
        MP2_full=.true.

! MAx_transform
      else if(token(1:2).EQ.'MA')then
        call GET_value (MAX_transform)

      else if(token(1:3).EQ.'RUN')then
        lrun=.true.

      else
         call MENU_end (done)
      end if
!
      end do !(.not.done)
!
      if(lrun)call MP2CLC
!
! End of routine MENU_MP2
      call PRG_manager ('exit', 'MENU_MP2', 'UTILITY')
      return
      end
