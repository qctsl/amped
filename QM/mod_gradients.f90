      module gradients
!****************************************************************************************************************************
!     Date last modified: February 18, 2000                                                                    Version 2.0  *
!     Author: R.A. Poirier                                                                                                  *
!     Description: Gradient objects.                                                                                        *
!****************************************************************************************************************************
      implicit none
!
      double precision A2DF(174),CCPX(48),CCPY(48),CCPZ(48),CCQX(48),CCQY(48),CCQZ(48)

! Energy contributions:
      double precision :: VEEXX  ! G2E_SCR_VEE_TERM
      double precision :: VNNXX  ! G2E_SCR_VNN_TERM
      double precision :: VNEXX  ! G2E_SCR_VNE_TERM
      double precision :: TXX    ! G2E_SCR_TXX_TERM
!
! Gradients components (all 3*Natoms):
      type type_gradient_components
        double precision NEOPR
        double precision NEWFN
        double precision WFN
        double precision COEFF
        double precision NUCLEAR
        double precision EEWFN
      end type type_gradient_components
!
      type (type_gradient_components), dimension(:), allocatable :: Gradient

!
      double precision, dimension(:), allocatable :: GRDXYZ ! Total gradient in cartesian coordinates
      double precision, dimension(:), allocatable, save, target :: GRD_ZM
      double precision, dimension(:), allocatable, save, target :: GRD_PIC
      double precision, dimension(:), allocatable, save, target :: GRD_RIC
      double precision, dimension(:), save, pointer :: GRD_int => null() ! points to gradient in internal coordinates
      double precision, dimension(:), allocatable :: GEEWFN

!
! Basis set:
      double precision, dimension(:), allocatable :: GBASCL ! Gradients of scale factors
      double precision, dimension(:), allocatable :: DEXX1 ! Gradients of exponents
      double precision, dimension(:), allocatable :: DEXX2 ! Gradients of exponents
      double precision, dimension(:), allocatable :: D1C1 ! Gradients of contraction coefficients
      double precision, dimension(:), allocatable :: D1C2 ! Gradients of contraction coefficients
      double precision, dimension(:), allocatable :: D2C1 ! Gradients of contraction coefficients
      double precision, dimension(:), allocatable :: D2C2 ! Gradients of contraction coefficients

      end module gradients
