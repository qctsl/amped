      subroutine G2E_basis
!***********************************************************************
!     Date last modified: June 15, 1992                    Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: Evaluation of the two-electron integral             *
!                  contribution to the force.                          *
!***********************************************************************
! Modules:
      USE program_constants
      USE type_basis_set
      USE type_molecule
      USE QM_defaults
      USE gradients
      USE gamma_fn

      implicit none
!
! Input scalars:
      double precision GRDACC
!
! Output scalars:
!
! Local scalars:
      integer NPRIM
      integer :: Ishell,Jshell,Kshell,Lshell
      integer :: Iatmshl,Jatmshl,Katmshl,Latmshl
      integer :: Istart,Jstart,Kstart,Lstart
      integer :: Iend,Jend,Kend,Lend,LendL
      integer :: Ifrst,Jfrst,Kfrst,Lfrst
      integer :: Ilast,Jlast,Klast,Llast
      integer :: Irange,Jrange,Krange,Lrange
      integer :: LAMAX,LBMAX,LCMAX,LDMAX
      integer :: IGBGN,JGBGN,KGBGN,LGBGN
      integer :: IGEND,JGEND,KGEND,LGEND
      integer :: Iatom,Jatom,Katom,Latom
      integer :: Iaos,JaoS,KaoS,LaoS
      integer :: Mtype
      double precision :: AX1,TRP,PITO52,PT78
      logical :: LTWOint,OPEN,NOGSCF
      logical :: Iatmshl_EQ_Jatmshl,Iatmshl_EQ_Katmshl,Jatmshl_EQ_Katmshl,Iatmshl_EQ_Latmshl, &
                 Jatmshl_EQ_Latmshl,Katmshl_EQ_Latmshl,IJIJ,IIKK
      logical IJSAME,KLSAME,IKSMJL
!
! Local parameters:
      double precision PT25,TWOPT5,TENM2,TENM4
      parameter (PT25=0.25D0,TWOPT5=2.5D0,TENM2=0.01D0,TENM4=1.0D-4)
!
! Begin:
      call PRG_manager ('enter', 'G2E_basis', 'UTILITY')
!
      VEEXX=ZERO
      NPRIM=Basis%Nprimitives
!     write(6,*)'NPRIM: ',NPRIM
!
      TRP=TWO/DSQRT(PI_VAL)
      PT78=PI_VAL*PT25
      PITO52=TWO*(PI_VAL**TWOPT5)
      NOGSCF=.TRUE.
      OPEN=.FALSE.
!     IF(UHFOPN)then
!       OPEN=.TRUE.
!     else if(RHFOPN.or.GVBCAL)then
!       K=J*NFOCK
!       NOGSCF=.FALSE.
!     end if
      GRDACC=1.0D-7
      VTOL1=GRDACC*TENM4
      VTOL2=GRDACC*TENM2
      VTOLS=GRDACC*GRDACC
!
! Loop over Ishell.
      do Ishell=1,Basis%Nshells
      do Jshell=1,Ishell
      do Kshell=1,Jshell
      LendL=Kshell
      if(Ishell.eq.Kshell)LendL=Jshell
      do Lshell=1,Kshell
!
!     write(6,*)'Ishell,Jshell,Kshell,Lshell: ',Ishell,Jshell,Kshell,Lshell
!     call flush(6)
      Ilast= Basis%shell(Ishell)%lastSHL
      Jlast= Basis%shell(Jshell)%lastSHL
      Klast= Basis%shell(Kshell)%lastSHL

      Ifrst=Basis%shell(Ishell)%frstSHL
      do Iatmshl=Ifrst,Ilast
        Iatom=Basis%atmshl(Iatmshl)%ATMLST
        Iaos=Basis%atmshl(Iatmshl)%frstAO-1
        IF(Ishell.EQ.Jshell)Jlast=Iatmshl

      Jfrst=Basis%shell(Jshell)%frstSHL
      do Jatmshl=Jfrst,Jlast
        IF(Ishell.EQ.Kshell)Klast=Iatmshl
        IF(Jshell.EQ.Kshell)Klast=Jatmshl

      Iatmshl_EQ_Jatmshl=.FALSE.
      IF(Iatmshl.EQ.Jatmshl)Iatmshl_EQ_Jatmshl=.TRUE.

      Kfrst=Basis%shell(Kshell)%frstSHL
      do Katmshl=Kfrst,Klast
        Llast= Basis%shell(Lshell)%lastSHL
        IF(Ishell.EQ.Lshell)Llast=Iatmshl
        IF(Jshell.EQ.Lshell)Llast=Jatmshl
        IF(Kshell.EQ.Lshell)Llast=Katmshl


      Iatmshl_EQ_Katmshl=.FALSE.
      IF(Iatmshl.EQ.Katmshl)Iatmshl_EQ_Katmshl=.TRUE.
      Jatmshl_EQ_Katmshl=.FALSE.
      IF(Jatmshl.EQ.Katmshl)Jatmshl_EQ_Katmshl=.TRUE.

      Lfrst=Basis%shell(Lshell)%frstSHL
      do Latmshl=Lfrst,Llast
      LAMAX=Basis%shell(Ishell)%Xtype+1
      Istart=Basis%shell(Ishell)%Xstart
      Iend=Basis%shell(Ishell)%XEND
      Irange=Iend-Istart+1
      IGBGN=Basis%shell(Ishell)%EXPBGN
      IGEND=Basis%shell(Ishell)%EXPEND
      LBMAX=Basis%shell(Jshell)%Xtype+1
      Jstart=Basis%shell(Jshell)%Xstart
      Jend=Basis%shell(Jshell)%XEND
      Jrange=Jend-Jstart+1
      JGBGN=Basis%shell(Jshell)%EXPBGN
      JGEND=Basis%shell(Jshell)%EXPEND
      LCMAX=basis%shell(Kshell)%Xtype+1
      Kstart=basis%shell(Kshell)%Xstart
      Kend=basis%shell(Kshell)%XEND
      Krange=Kend-Kstart+1
      KGBGN=basis%shell(Kshell)%EXPBGN
      KGEND=basis%shell(Kshell)%EXPEND
      LDMAX=basis%shell(Lshell)%Xtype+1
      Lstart=basis%shell(Lshell)%Xstart
      Lend=basis%shell(Lshell)%XEND
      Lrange=Lend-Lstart+1
      LGBGN=basis%shell(Lshell)%EXPBGN
      LGEND=basis%shell(Lshell)%EXPEND
        Jatom=Basis%atmshl(Jatmshl)%ATMLST
        Jaos=Basis%atmshl(Jatmshl)%frstAO-1
        Katom=Basis%atmshl(Katmshl)%ATMLST
        Kaos=Basis%atmshl(Katmshl)%frstAO-1
        Latom=Basis%atmshl(Latmshl)%ATMLST
        Laos=Basis%atmshl(Latmshl)%frstAO-1
! Skip if 1-center integrals
!     IF(Iatom.EQ.Jatom.and.Iatom.EQ.Katom.and.Iatom.EQ.Latom)GO TO 1004

!
! Determine Mtype and LTWOint
      Mtype=1
      IF(Iatmshl.EQ.Jatmshl)Mtype=Mtype+4
      IF(Jatmshl.EQ.Katmshl)Mtype=Mtype+2
      IF(Katmshl.EQ.Latmshl)Mtype=Mtype+1
      IF(Mtype.EQ.4.or.Mtype.EQ.7.or.Mtype.EQ.8)then
        LTWOint=.FALSE.
      else
        LTWOint=.TRUE.
      end if

      Iatmshl_EQ_Latmshl=.FALSE.
      IF(Iatmshl.EQ.Latmshl)Iatmshl_EQ_Latmshl=.TRUE.
      Katmshl_EQ_Latmshl=.FALSE.
      IF(Katmshl.EQ.Latmshl)Katmshl_EQ_Latmshl=.TRUE.
      IJIJ=.FALSE.
      IF(Iatmshl_EQ_Katmshl.and.Jatmshl.EQ.Latmshl)IJIJ=.TRUE.
      IIKK=.FALSE.
      IF(Iatmshl_EQ_Jatmshl.and.Katmshl_EQ_Latmshl)IIKK=.TRUE.
      Jatmshl_EQ_Latmshl=.FALSE.
      IF(Jatmshl.EQ.Latmshl)Jatmshl_EQ_Latmshl=.TRUE.

      AX1=PT5
      IF(Iatmshl.NE.Jatmshl)AX1=AX1+AX1
      IF(Katmshl.NE.Latmshl)AX1=AX1+AX1
      IF(Iatmshl.NE.Katmshl.or.Jatmshl.NE.Latmshl)AX1=AX1+AX1
!
!     write(6,*)'Iatmshl,Jatmshl,Katmshl,Latmshl: ',Iatmshl,Jatmshl,Katmshl,Latmshl
!     call flush(6)
      IJSAME=Iatmshl.EQ.Jatmshl
      KLSAME=Katmshl.EQ.Latmshl
      IKSMJL=(Iatmshl.EQ.Katmshl).and.(Jatmshl.EQ.Latmshl)
!
      call G2EB_SPDF
      if(LTWOint)then
        Jatom=Basis%atmshl(Latmshl)%ATMLST
        Jaos=Basis%atmshl(Latmshl)%frstAO-1
        LBMAX=Basis%shell(Lshell)%Xtype+1
        JGBGN=Basis%shell(Lshell)%EXPBGN
        JGEND=Basis%shell(Lshell)%EXPEND
        Katom=Basis%atmshl(Jatmshl)%ATMLST
        Kaos=Basis%atmshl(Jatmshl)%frstAO-1
        LCMAX=basis%shell(Jshell)%Xtype+1
        KGBGN=basis%shell(Jshell)%EXPBGN
        KGEND=basis%shell(Jshell)%EXPEND
        Latom=Basis%atmshl(Katmshl)%ATMLST
        Laos=Basis%atmshl(Katmshl)%frstAO-1
        LDMAX=basis%shell(Kshell)%Xtype+1
        LGBGN=basis%shell(Kshell)%EXPBGN
        LGEND=basis%shell(Kshell)%EXPEND
        AX1=PT5
        IF(Iatmshl.NE.Latmshl)AX1=AX1+AX1
        IF(Jatmshl.NE.Katmshl)AX1=AX1+AX1
        IF(Iatmshl.NE.Jatmshl.or.Latmshl.NE.Katmshl)AX1=AX1+AX1
        IJSAME=Iatmshl.EQ.Latmshl
        KLSAME=Jatmshl.EQ.Katmshl
        IKSMJL=(Iatmshl.EQ.Jatmshl).and.(Latmshl.EQ.Katmshl)
        call G2EB_SPDF
      end if
      if(Mtype.eq.1)then
        Jatom=Basis%atmshl(Katmshl)%ATMLST
        Jaos=Basis%atmshl(Katmshl)%frstAO-1
        LBMAX=Basis%shell(Kshell)%Xtype+1
        JGBGN=Basis%shell(Kshell)%EXPBGN
        JGEND=Basis%shell(Kshell)%EXPEND
        Katom=Basis%atmshl(Jatmshl)%ATMLST
        Kaos=Basis%atmshl(Jatmshl)%frstAO-1
        LCMAX=basis%shell(Jshell)%Xtype+1
        KGBGN=basis%shell(Jshell)%EXPBGN
        KGEND=basis%shell(Jshell)%EXPEND
        Latom=Basis%atmshl(Latmshl)%ATMLST
        Laos=Basis%atmshl(Latmshl)%frstAO-1
        LDMAX=basis%shell(Lshell)%Xtype+1
        LGBGN=basis%shell(Lshell)%EXPBGN
        LGEND=basis%shell(Lshell)%EXPEND
        AX1=PT5
        IF(Iatmshl.NE.Katmshl)AX1=AX1+AX1
        IF(Jatmshl.NE.Latmshl)AX1=AX1+AX1
        IF(Iatmshl.NE.Jatmshl.or.Latmshl.NE.Katmshl)AX1=AX1+AX1
        IJSAME=Iatmshl.EQ.Katmshl
        KLSAME=Jatmshl.EQ.Latmshl
        IKSMJL=(Iatmshl.EQ.Jatmshl).and.(Latmshl.EQ.Katmshl)
        call G2EB_SPDF
      end if

 1004 continue
      end do ! Latmshl
      end do ! Katmshl
      end do ! Jatmshl
      end do ! Iatmshl
      end do ! Lshell
      end do ! Kshell
      end do ! Jshell
      end do ! Ishell
!
! End of routine G2E_basis
      call PRG_manager ('exit', 'G2E_basis', 'UTILITY')
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine G2EB_SPDF
!***********************************************************************
!     Date last modified: June 15, 1992                    Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: Evaluation of the two-electron integral             *
!                  contribution to the force.                          *
!***********************************************************************
! Modules:
      USE QM_objects

      implicit none

! Local scalars:
      integer :: IatomX,IatomY,IatomZ,JatomX,JatomY,JatomZ,KatomX,KatomY,KatomZ,LatomX,LatomY,LatomZ
      integer :: JGENDS,KGENDS,LGENDS
      integer :: IAS,JAS,KAS,LAS,LII,LJJ,LIJ,LKK,LIK,LJK,LLL,LIL,LJL,LKL
      integer :: LA,LB,LC,LD,Jtype
      integer :: I,J,K,L,IZERO,KZERO,ID,IJ,KL
      integer :: N,NFIJ,NFIK,NFIL,NFJK,NFJL,NFKL,NI,NJ,LTEMP
      double precision :: DMAX
      double precision :: AX,AY,AZ,BX,BY,BZ,CX,CY,CZ,DX,DY,DZ
      double precision :: D12,D12B,D1234,D13,D13B,D14,D14B,D23,D23B,D24,D24B,D34,D34B
      double precision :: DX1X,DX1Y,DX1Z,DX2X,DX2Y,DX2Z
      double precision :: DVEX,DVEXS,DVEY,DVEYS,DVEZ,DVEZS
      double precision :: ABX,ABY,ABZ,CDX,CDY,CDZ,R12,R34,ARGEXP
      double precision :: EPAB,EPABI,EQCD,EQCDI,EABCDI
      double precision ABVE00,RGEXP,AGexp,BGexp,CGexp,DGexp,CDVE00,CPA,CPB, &
                       CPC,CPD,CPP,CPPP,CPPPS,CPPS,CPPSS,CPS,CPSP,CPSPS,CPSS,CPSSS,CSA,CSB,CSC,CSD,CSMAB,CSP,CSPP,CSPPS, &
                       CSPS,CSPSS,CSS,CSSP,CSSPS,CSSS,CSUMA,CSUMC, &
                       EOOOO,E12,E34,E34MAX,FQ0,FQ1,FQ2,FQ3,FQ4,FQ5,FXI,FXJ,FXK,FYI,FYJ,FYK,FZI,FZJ,FZK,HEPABI,HFQ1,HFQ2, &
                       PQX,PQY,PQZ,PX,PY,PZ,QFQ1,QFQ2,QFQ3,QQ,QVE00,S1,S12,S2,S3,S34,S4,T,TEMP,TFQ2,THETA,THETA2,THETA3, &
                       THETA4,TI,VE00S,VE11S,VE12S,VE21S,VE22S,VE31S,VE32S,VP,VP1,VP2,VP3,VPA,VPB,VPC,VPD,VPPPS,VPPSS,VPSPS, &
                       VPSSS,VR12,VR34,VSPPS,VSPSS,VSSPS,VSSSS,VTEST,VTOL
      double precision QA,QA1,QA2,PQXX,PQYY,PQZZ,PQXY,PQXZ,PQYZ, &
                       V0000,V0010,V0020,V0030,V0100,V0200,V0300,V0110,V0120,V0130,V0210,V0220, &
                       V0230,V0310,V0320,V0330,V1010,V1020, &
                       V1030,V2010,V2020,V2030,V3010,V3020,V3030,V1000,V2000,V3000,V1100,V2100, &
                       V3100,V1200,V2200,V3200,V1300,V2300, &
                       V3300,V1110,V2110,V3110,V1210,V2210,V3210,V1310,V2310,V3310,V1120,V2120, &
                       V3120,V1220,V2220,V3220,V1320,V2320, &
                       V3320,V1130,V2130,V3130,V1230,V2230,V3230,V1330,V2330,V3330,C1110,C2110, &
                       C3110,C1210,C2210,C3210,C1320,C2320, &
                       C3320,C1130,C2130,C3130,C1230,C2230,C3230,C1310,C2310,C3310,C1120,C2120, &
                       C3120,C1220,C2220,C3220,C1330,C2330, &
                       C3330,OPXO,OPYO,OPZO,OPOX,OPOY,OPOZ,OPXOX,OPYOY,OPZOZ,OPXX,OPXY,OPXZ,OPYX, &
                       OPYY,OPYZ,OPZX,OPZY,OPZZ,OQXO,OQYO, &
                       OQZO,OQOX,OQOY,OQOZ,OQXOX,OQYOY,OQZOZ,OQXX,OQXY,OQXZ,OQYX,OQYY,OQYZ,OQZX, &
                       OQZY,OQZZ,GOOOO,GOOXO,GOOYO,GOOZO, &
                       GXOOO,GXOXO,GXOYO,GXOZO,GXXOO,GXXXO,GXXYO,GXXZO,GXYOO,GXYZO,GXZOO,GYOOO, &
                       GYOYO,GYOZO,GYYOO,GYYXO,GYYYO,GYYZO, &
                       GYZOO,GZOOO,GZOZO,GZZOO,GZZXO,GZZYO,GZZZO,VE00,VE11,VE12,VE13,VE14,VE21, &
                       VE22,VE23,VE24,VE31,VE32,VE33,VE34, &
                       CSSSP,CSSPP,CSPSP,CPSSP,CSPPP,CPSPP,CPPSP,CPPPP,VC12,VC13,VC14,VC23,VC24, &
                       VC34,VSSSP,VPSSP,VSPSP,VPPSP,VSSPP, &
                       VPSPP,VSPPP,VPPPP, &
                       GOOXX,GOOXY,GOOXZ,GOOYY,GOOYZ,GOOZZ,GXOXX,GXOYY,GXOYZ,GXOZZ,GXXXX,GXXXY,GXXXZ, &
                       GXXYX,GXXYY,GXXYZ,GXXZX,GXXZY,GXXZZ,GXYXO,GXYXX,GXYXY,GXYXZ,GXYYO,GXYYX,GXYYY,GXYYZ,GXYZX,GXYZY, &
                       GXYZZ,GXZXO,GXZXX,GXZXY,GXZXZ,GXZYO,GXZYX,GXZYY,GXZYZ,GXZZO,GXZZX,GXZZY,GXZZZ,GYOXX,GYOYY,GYOZZ, &
                       GYXOO,GYXXO,GYXXX,GYXXY,GYXXZ,GYXYO,GYXYX,GYXYY,GYXYZ,GYXZO,GYXZX,GYXZY,GYXZZ,GYYXX,GYYXY,GYYXZ, &
                       GYYYX,GYYYY,GYYYZ,GYYZX,GYYZY,GYYZZ,GYZXO,GYZXX,GYZXY,GYZXZ,GYZYO,GYZYX,GYZYY,GYZYZ,GYZZO,GYZZX, &
                       GYZZY,GYZZZ,GZOXX,GZOYY,GZOZZ,GZXOO,GZXXO,GZXXX,GZXXY,GZXXZ,GZXYO,GZXYX,GZXYY,GZXYZ,GZXZO,GZXZX, &
                       GZXZY,GZXZZ,GZYOO,GZYXO,GZYXX,GZYXY,GZYXZ,GZYYO,GZYYX,GZYYY,GZYYZ,GZYZO,GZYZX,GZYZY,GZYZZ,GZZXX, &
                       GZZXY,GZZXZ,GZZYX,GZZYY,GZZYZ,GZZZX,GZZZY,GZZZZ
      equivalence (GXYOO,GYXOO),(GXZOO,GZXOO),(GYZOO,GZYOO),(GYXXO,GXYXO,GXXYO), &
                       (GZXXO,GXZXO,GXXZO),(GZYYO,GYZYO,GYYZO), &
                  (GXYYO,GYXYO,GYYXO),(GXZZO,GZXZO,GZZXO),(GYZZO,GZYZO,GZZYO), &
                       (GXYZO,GYZXO,GZXYO,GZYXO,GYXZO,GXZYO), &
                  (GYYXX,GYXYX,GYXXY,GXYYX,GXYXY,GXXYY),(GZZXX,GZXZX,GZXXZ,GXZZX,GXZXZ,GXXZZ), &
                  (GZZYY,GZYZY,GZYYZ,GYZZY,GYZYZ,GYYZZ),(GYXXX,GXYXX,GXXYX,GXXXY),(GZXXX,GXZXX,GXXZX,GXXXZ), &
                  (GXYYY,GYXYY,GYYXY,GYYYX),(GZYYY,GYZYY,GYYZY,GYYYZ),(GXZZZ,GZXZZ,GZZXZ,GZZZX), &
                       (GYZZZ,GZYZZ,GZZYZ,GZZZY), &
                  (GXYZZ,GXZYZ,GXZZY,GYXZZ,GYZXZ,GYZZX,GZXYZ,GZXZY,GZYXZ,GZYZX,GZZXY,GZZYX), &
                  (GYZXX,GYXZX,GYXXZ,GZYXX,GZXYX,GZXXY,GXYZX,GXYXZ,GXZYX,GXZXY,GXXYZ,GXXZY), &
                  (GZXYY,GZYXY,GZYYX,GXZYY,GXYZY,GXYYZ,GYZXY,GYZYX,GYXZY,GYXYZ,GYYZX,GYYXZ)
      logical FLAG
!
! Local parameters:
      double precision PT78,P75,ONE5,THRPT5,FIFTEN,FORPT5,TEN,TENM7,TWENTY
      parameter (P75=0.75D0,ONE5=1.5D0,THRPT5=3.5D0,FORPT5=4.5D0,TEN=10.0D0,FIFTEN=15.0D0, &
                 TWENTY=20.0D0,TENM7=1.0D-7)
!
! Local arrays:
      double precision :: E2SP(256)
      double precision :: XE34(1:100),QX(1:100),QY(1:100),QZ(1:100),EQCDISV(1:100),TS3(1:100),TS4(1:100),TS34(1:100), &
                       CSMCD(1:100)
!
! Begin:
      call PRG_manager ('enter', 'G2EB_SPDF', 'UTILITY')
!
      LA=3*(LAmax-1)+1
      LB=3*(LBmax-1)+1
      LC=3*(LCmax-1)+1
      LD=3*(LDmax-1)+1
      Jtype=(LA+LB+LC+LC+LD-2)/3
!     write(6,*)'LA,LB,LC,LD: ',LA,LB,LC,LD
!     write(6,*)'Iatom,Jatom,Katom,Latom: ',Iatom,Jatom,Katom,Latom
!     call flush(6)
! Get the density matrix contributions.
      DMAX=ZERO
!
      E2SP(1:256)=ZERO
!
      D12B=ZERO
      D13B=ZERO
      D23B=ZERO
      do I=LAmax,LA
      IAS=Iaos+I
      LII=IAS*(IAS-1)/2 ! IJNDX(IAS)
      do J=LBmax,LB
      JAS=Jaos+J
      LJJ=JAS*(JAS-1)/2 ! IJNDX(JAS)
      LIJ=LII+JAS
      IF(IAS.LT.JAS)LIJ=LJJ+IAS
      IF(NOGSCF)D12=PM0(LIJ)
      IF(OPEN)D12B=PEWD(LIJ)
      do K=LCmax,LC
      KAS=Kaos+K
      LKK=KAS*(KAS-1)/2 ! IJNDX(KAS)
      LIK=LII+KAS
      IF(IAS.LT.KAS)LIK=LKK+IAS
      LJK=LJJ+KAS
      IF(JAS.LT.KAS)LJK=LKK+JAS
      IF(NOGSCF)then
        D13=PM0(LIK)
        D23=PM0(LJK)
      end if
      IF(OPEN)then
        D13B=PEWD(LIK)
        D23B=PEWD(LJK)
      end if
      do L=LDmax,LD
      LAS=Laos+L
      LLL=LAS*(LAS-1)/2 ! IJNDX(LAS)
      LIL=LII+LAS
      IF(IAS.LT.LAS)LIL=LLL+IAS
      LJL=LJJ+LAS
      IF(JAS.LT.LAS)LJL=LLL+JAS
      LKL=LKK+LAS
      IF(KAS.LT.LAS)LKL=LLL+KAS
      ID=64*I+16*J+4*K+L-84
      E2SP(ID)=(D12*PM0(LKL)-PT25*(D13*PM0(LJL)+D23*PM0(LIL)))*AX1
      D1234=DABS(E2SP(ID))
      IF(D1234.GT.DMAX)DMAX=D1234
      end do ! L
      end do ! K
      end do ! J
      end do ! I
!     write(6,*)'DMAX: ',DMAX
!     call flush(6)
      IF(DMAX.LT.VTOL1)GO TO 1004
      AX=CARTESIAN(Iatom)%X
      AY=CARTESIAN(Iatom)%Y
      AZ=CARTESIAN(Iatom)%Z
      BX=CARTESIAN(Jatom)%X
      BY=CARTESIAN(Jatom)%Y
      BZ=CARTESIAN(Jatom)%Z
      CX=CARTESIAN(Katom)%X
      CY=CARTESIAN(Katom)%Y
      CZ=CARTESIAN(Katom)%Z
      DX=CARTESIAN(Latom)%X
      DY=CARTESIAN(Latom)%Y
      DZ=CARTESIAN(Latom)%Z
      ABX=AX-BX
      ABY=AY-BY
      ABZ=AZ-BZ
      CDX=CX-DX
      CDY=CY-DY
      CDZ=CZ-DZ
      R12=ABX**2+ABY**2+ABZ**2
      R34=CDX**2+CDY**2+CDZ**2
! Hunt out the largest Density*Contraction and reject if possible.
      E34MAX=ZERO
      KZERO=-10
!
! Added by C. Pye Mar 27 1995
      KGENDS=KGEND
      do K=KGBGN,KGENDS
      KZERO=KZERO+10
      KL=KZERO
      CGexp=Basis%gaussian(K)%exp
      CSUMC=DABS(Basis%gaussian(K)%contrc)
      LGENDS=LGEND
      IF(KLSAME)LGENDS=K
      do L=LGBGN,LGENDS
      DGexp=Basis%gaussian(L)%exp
      KL=KL+1
      EQCDI=ONE/(CGexp+DGexp)
      S3=CGexp*EQCDI
      S4=DGexp*EQCDI
      S34=CGexp*S4
      QX(KL)=S3*CX+S4*DX
      QY(KL)=S3*CY+S4*DY
      QZ(KL)=S3*CZ+S4*DZ
      EQCDISV(KL)=EQCDI
      TS3(KL)=S3
      TS4(KL)=S4
      TS34(KL)=S34
      ARGEXP=R34*S34
      E34=ZERO
      IF(ARGEXP.LE.GRD_expcut)E34=DEXP(-ARGEXP)*EQCDI
      IF(KLSAME.and.(K.NE.L))E34=E34+E34
      XE34(KL)=E34
      E34=E34*CSUMC*DABS(Basis%gaussian(L)%contrc)
      CSMCD(KL)=E34*E34
      IF(E34.GT.E34MAX)E34MAX=E34
      end do ! K
      end do ! L
!     write(6,*)'E34MAX: ',E34MAX
!     call flush(6)
      IF(DMAX*E34MAX.LT.VTOL1)GO TO 1004
      FXI=ZERO
      FXJ=ZERO
      FXK=ZERO
      FYI=ZERO
      FYJ=ZERO
      FYK=ZERO
      FZI=ZERO
      FZJ=ZERO
      FZK=ZERO
      VE11=ZERO
      VE12=ZERO
      VE13=ZERO
      VE14=ZERO
      VE21=ZERO
      VE22=ZERO
      VE23=ZERO
      VE24=ZERO
      VE31=ZERO
      VE32=ZERO
      VE33=ZERO
      VE34=ZERO
      IZERO=-10
!
! Loop over the primitives in each shell.
      do I=IGBGN,IGEND
      IZERO=IZERO+10
      IJ=IZERO
      AGexp=Basis%gaussian(I)%exp

      if(LAmax.EQ.1)then
      CSA=Basis%gaussian(I)%contrc
      CPA=ZERO
      else
      CSA=ZERO
      CPA=Basis%gaussian(I)%contrc
      end if

      CSUMA=(DABS(CSA)+DABS(CPA))*DMAX
      JGENDS=JGEND
      IF(IJSAME)JGENDS=I
      do J=JGBGN,JGENDS
      IJ=IJ+1
      BGexp=Basis%gaussian(J)%exp

      if(LBmax.EQ.1)then
      CSB=Basis%gaussian(J)%contrc
      CPB=ZERO
      else
      CSB=ZERO
      CPB=Basis%gaussian(J)%contrc
      end if

      EPAB=AGexp+BGexp
      EPABI=ONE/EPAB
      S1=AGexp*EPABI
      S2=BGexp*EPABI
      S12=AGexp*S2
      E12=ZERO
      IF(R12*S12.LE.GRD_expcut)E12=DEXP(-R12*S12)*PITO52*EPABI
      IF(IJSAME.and.(I.NE.J))E12=E12+E12
      CSB=CSB*E12
      CPB=CPB*E12
      CSMAB=CSUMA*(DABS(CSB)+DABS(CPB))
      IF(CSMAB*E34MAX.LT.VTOL2)GO TO 2002
      CSMAB=CSMAB*CSMAB
      CSS=CSA*CSB
      CPS=CPA*CSB
      CSP=CSA*CPB
      CPP=CPA*CPB
      PX=S1*AX+S2*BX
      PY=S1*AY+S2*BY
      PZ=S1*AZ+S2*BZ
      IF(LA.GT.1)then
        OPXO=-S2*ABX
        OPYO=-S2*ABY
        OPZO=-S2*ABZ
        IF(LB.GT.1)then
          OPOX=S1*ABX
          OPOY=S1*ABY
          OPOZ=S1*ABZ
          OPXOX=OPXO+OPOX
          OPYOY=OPYO+OPOY
          OPZOZ=OPZO+OPOZ
          HEPABI=PT5*EPABI
          OPXX=OPXO*OPOX+HEPABI
          OPYY=OPYO*OPOY+HEPABI
          OPZZ=OPZO*OPOZ+HEPABI
          OPXY=OPXO*OPOY
          OPYX=OPXY
          OPXZ=OPXO*OPOZ
          OPZX=OPXZ
          OPYZ=OPYO*OPOZ
          OPZY=OPYZ
        end if
      end if
   51 VE00S=ZERO
      VE11S=ZERO
      VE12S=ZERO
      VE21S=ZERO
      VE22S=ZERO
      VE31S=ZERO
      VE32S=ZERO
      DVEXS=ZERO
      DVEYS=ZERO
      DVEZS=ZERO
      KGENDS=KGEND
      IF(IKSMJL)KGENDS=I
      KZERO=-10
      do K=KGBGN,KGENDS
      KZERO=KZERO+10
      KL=KZERO
      CGexp=Basis%gaussian(K)%exp

      if(LCmax.EQ.1)then
      CSC=Basis%gaussian(K)%contrc
      CPC=ZERO
      else
      CSC=ZERO
      CPC=Basis%gaussian(K)%contrc
      end if

      CSSS=CSS*CSC
      CSSP=CSS*CPC
      CPSS=CPS*CSC
      CPSP=CPS*CPC
      CSPS=CSP*CSC
      CSPP=CSP*CPC
      CPPS=CPP*CSC
      CPPP=CPP*CPC
      LGENDS=LGEND
      IF(KLSAME)LGENDS=K
      IF(IKSMJL.and.(I.EQ.K))LGENDS=J
      do L=LGBGN,LGENDS
      KL=KL+1
      DGexp=Basis%gaussian(L)%exp
      EQCD=CGexp+DGexp
      EABCDI=ONE/(EPAB+EQCD)
      VTEST=CSMAB*CSMCD(KL)*EABCDI
      IF(VTEST.LT.VTOLS)GO TO 2004
      QA2=EQCD*EABCDI
      QA=EPAB*QA2
      PQX=PX-QX(KL)
      PQY=PY-QY(KL)
      PQZ=PZ-QZ(KL)
      PQXX=PQX*PQX
      PQYY=PQY*PQY
      PQZZ=PQZ*PQZ
      T=QA*(PQXX+PQYY+PQZZ)
      EQCDI=EQCDISV(KL)
      S3=TS3(KL)
      S4=TS4(KL)
      S34=TS34(KL)
      E34=XE34(KL)*DSQRT(EABCDI)
      IF(IKSMJL.and.(IJ.NE.KL))E34=E34+E34
      CSD=Basis%gaussian(L)%contrc*E34
      EOOOO=E2SP(1)*CSSS*CSD
      IF(T.GT.FIFTEN)then
         TI=ONE/T
         IF(VTEST*TI.LT.VTOLS)GO TO 2004
         FQ0=DSQRT(PT78*TI)
         FQ1=PT5*FQ0*TI
         FQ2=ONE5*FQ1*TI
         FQ3=TWOPT5*FQ2*TI
         FQ4=THRPT5*FQ3*TI
         FQ5=FORPT5*FQ4*TI
      else   
         QQ=T*TWENTY
         N=IDINT(QQ)
         THETA=QQ-DBLE(N)
         THETA2=THETA*(THETA-ONE)
         THETA3=THETA2*(THETA-TWO)
         THETA4=THETA2*(THETA+ONE)
         FQ5=(FMT_val(N+1)%FM5+THETA*FMT_del(N+1)%FM5-THETA3*FMT_int(N+1)%FM5+THETA4*FMT_int(N+2)%FM5)
         FQ4=(FMT_val(N+1)%FM4+THETA*FMT_del(N+1)%FM4-THETA3*FMT_int(N+1)%FM4+THETA4*FMT_int(N+2)%FM4)
         FQ3=(FMT_val(N+1)%FM3+THETA*FMT_del(N+1)%FM3-THETA3*FMT_int(N+1)%FM3+THETA4*FMT_int(N+2)%FM3)
         FQ2=(FMT_val(N+1)%FM2+THETA*FMT_del(N+1)%FM2-THETA3*FMT_int(N+1)%FM2+THETA4*FMT_int(N+2)%FM2)
         FQ1=(FMT_val(N+1)%FM1+THETA*FMT_del(N+1)%FM1-THETA3*FMT_int(N+1)%FM1+THETA4*FMT_int(N+2)%FM1)
         FQ0=(FMT_val(N+1)%FM0+THETA*FMT_del(N+1)%FM0-THETA3*FMT_int(N+1)%FM0+THETA4*FMT_int(N+2)%FM0)
      end if
      PQXY=PQX*PQY
      PQXZ=PQX*PQZ
      PQYZ=PQY*PQZ
      QA1=EPAB*EABCDI

      if(LDmax.EQ.1)then
      CPD=ZERO
      else
      CPD=Basis%gaussian(L)%contrc*E34
      end if

      IF(LC.NE.1)then
         CSSPS=CSSP*CSD
         CPSPS=CPSP*CSD
         OQXO=-S4*CDX
         OQYO=-S4*CDY
         OQZO=-S4*CDZ
         IF(LD.NE.1)then
            CSSSP=CSSS*CPD
            CSSPP=CSSP*CPD
            CSPSP=CSPS*CPD
            CPSSP=CPSS*CPD
            CSPPP=CSPP*CPD
            CPSPP=CPSP*CPD
            CPPSP=CPPS*CPD
            CPPPP=CPPP*CPD
            OQOX=S3*CDX
            OQOY=S3*CDY
            OQOZ=S3*CDZ
            OQXX=OQXO*OQOX+PT5*EQCDI
            OQYY=OQYO*OQOY+PT5*EQCDI
            OQZZ=OQZO*OQOZ+PT5*EQCDI
            OQXY=OQXO*OQOY
            OQYX=OQXY
            OQXZ=OQXO*OQOZ
            OQZX=OQXZ
            OQYZ=OQYO*OQOZ
            OQZY=OQYZ
            OQXOX=OQXO+OQOX
            OQYOY=OQYO+OQOY
            OQZOZ=OQZO+OQOZ
         end if
      end if
   52 FLAG=.FALSE.
!
! Begin the first pass through the integral calculations.
 3001 VPSSS=ZERO
      VSPSS=ZERO
      VPPSS=ZERO
      VSSPS=ZERO
      VPSPS=ZERO
      VSPPS=ZERO
      VPPPS=ZERO
      VSSSP=ZERO
      VPSSP=ZERO
      VSPSP=ZERO
      VPPSP=ZERO
      VSSPP=ZERO
      VPSPP=ZERO
      VSPPP=ZERO
      VPPPP=ZERO
      VC12=ZERO
      VC13=ZERO
      VC14=ZERO
      VC23=ZERO
      VC24=ZERO
      VC34=ZERO
!
! Integral evaluation section.
! (SS!SS) Section.
      GOOOO=FQ0
      V0000=GOOOO
      VSSSS=V0000*EOOOO
      VE00=VSSSS
! (PS!SS) Section.
      IF(Jtype.LT.2)GO TO 4000
      QFQ1=-QA2*FQ1
      GXOOO=PQX*QFQ1
      GYOOO=PQY*QFQ1
      GZOOO=PQZ*QFQ1
      V1000=OPXO*GOOOO+GXOOO
      V2000=OPYO*GOOOO+GYOOO
      V3000=OPZO*GOOOO+GZOOO
      CPSSS=CPSS*CSD
      VPSSS=(V1000*E2SP(65)+V2000*E2SP(129)+V3000*E2SP(193))*CPSSS
      VE00=VE00+VPSSS
      TEMP=V0000*CPSSS
      VE11=TEMP*E2SP( 65)
      VE21=TEMP*E2SP(129)
      VE31=TEMP*E2SP(193)
      IF(Jtype-3)4000,4003,4004
! (PS!PS) and (SS!PS) Section.
 4004 QFQ1=QA1*FQ1
      GOOXO=PQX*QFQ1
      GOOYO=PQY*QFQ1
      GOOZO=PQZ*QFQ1
      HFQ1=PT5*FQ1*EABCDI
      QFQ2=-QA*FQ2*EABCDI
      GXOXO=PQXX*QFQ2+HFQ1
      GYOYO=PQYY*QFQ2+HFQ1
      GZOZO=PQZZ*QFQ2+HFQ1
      GXOYO=PQXY*QFQ2
      GXOZO=PQXZ*QFQ2
      GYOZO=PQYZ*QFQ2
      V0010=OQXO*GOOOO+GOOXO
      V0020=OQYO*GOOOO+GOOYO
      V0030=OQZO*GOOOO+GOOZO
      V1010=OPXO*V0010+OQXO*GXOOO+GXOXO
      V1020=OPXO*V0020+OQYO*GXOOO+GXOYO
      V1030=OPXO*V0030+OQZO*GXOOO+GXOZO
      V2010=OPYO*V0010+OQXO*GYOOO+GXOYO
      V2020=OPYO*V0020+OQYO*GYOOO+GYOYO
      V2030=OPYO*V0030+OQZO*GYOOO+GYOZO
      V3010=OPZO*V0010+OQXO*GZOOO+GXOZO
      V3020=OPZO*V0020+OQYO*GZOOO+GYOZO
      V3030=OPZO*V0030+OQZO*GZOOO+GZOZO
      VSSPS=(V0010*E2SP(5)+V0020*E2SP(9)+V0030*E2SP(13))*CSSPS
      VE00=VE00+VSSPS
      TEMP=V0000*CSSPS
      VE13=TEMP*E2SP(5)
      VE23=TEMP*E2SP(9)
      VE33=TEMP*E2SP(13)
      VPSPS=(V1010*E2SP( 69)+V1020*E2SP( 73)+V1030*E2SP( 77)+V2010*E2SP(133)+V2020*E2SP(137)+V2030*E2SP(141)+ &
             V3010*E2SP(197)+V3020*E2SP(201)+V3030*E2SP(205))*CPSPS
      VE00=VE00+VPSPS
      VE11=VE11+(V0010*E2SP( 69)+V0020*E2SP( 73)+V0030*E2SP( 77))*CPSPS
      VE13=VE13+(V1000*E2SP( 69)+V2000*E2SP(133)+V3000*E2SP(197))*CPSPS
      VE21=VE21+(V0010*E2SP(133)+V0020*E2SP(137)+V0030*E2SP(141))*CPSPS
      VE23=VE23+(V1000*E2SP( 73)+V2000*E2SP(137)+V3000*E2SP(201))*CPSPS
      VE31=VE31+(V0010*E2SP(197)+V0020*E2SP(201)+V0030*E2SP(205))*CPSPS
      VE33=VE33+(V1000*E2SP( 77)+V2000*E2SP(141)+V3000*E2SP(205))*CPSPS
      VC13=VC13+(V0000*(E2SP(69)+E2SP(137)+E2SP(205)))*CPSPS
      IF(Jtype.EQ.4)GO TO 4000
! (PP!SS) and (SP!SS) Section.
 4003 HFQ1=-PT5*QA2*FQ1*EPABI
      QFQ2=QA2*QA2*FQ2
      GXXOO=PQXX*QFQ2+HFQ1
      GYYOO=PQYY*QFQ2+HFQ1
      GZZOO=PQZZ*QFQ2+HFQ1
      GXYOO=PQXY*QFQ2
      GXZOO=PQXZ*QFQ2
      GYZOO=PQYZ*QFQ2
      V0100=OPOX*GOOOO+GXOOO
      V0200=OPOY*GOOOO+GYOOO
      V0300=OPOZ*GOOOO+GZOOO
      V1100=OPXX*GOOOO+OPXOX*GXOOO+GXXOO
      V2200=OPYY*GOOOO+OPYOY*GYOOO+GYYOO
      V3300=OPZZ*GOOOO+OPZOZ*GZOOO+GZZOO
      V1200=OPXY*GOOOO+OPXO*GYOOO+OPOY*GXOOO+GXYOO
      V1300=OPXZ*GOOOO+OPXO*GZOOO+OPOZ*GXOOO+GXZOO
      V2100=OPYX*GOOOO+OPYO*GXOOO+OPOX*GYOOO+GXYOO
      V2300=OPYZ*GOOOO+OPYO*GZOOO+OPOZ*GYOOO+GYZOO
      V3100=OPZX*GOOOO+OPZO*GXOOO+OPOX*GZOOO+GXZOO
      V3200=OPZY*GOOOO+OPZO*GYOOO+OPOY*GZOOO+GYZOO
      CSPSS=CSPS*CSD
      CPPSS=CPPS*CSD
      VSPSS=(V0100*E2SP(17)+V0200*E2SP(33)+V0300*E2SP(49))*CSPSS
      VE00=VE00+VSPSS
      TEMP=V0000*CSPSS
      VE12=TEMP*E2SP(17)
      VE22=TEMP*E2SP(33)
      VE32=TEMP*E2SP(49)
      VPPSS=(V1100*E2SP( 81)+V1200*E2SP( 97)+V1300*E2SP(113)+V2100*E2SP(145)+V2200*E2SP(161)+V2300*E2SP(177)+ &
             V3100*E2SP(209)+V3200*E2SP(225)+V3300*E2SP(241))*CPPSS
      VE00=VE00+VPPSS
      VE11=VE11+(V0100*E2SP( 81)+V0200*E2SP( 97)+V0300*E2SP(113))*CPPSS
      VE12=VE12+(V1000*E2SP( 81)+V2000*E2SP(145)+V3000*E2SP(209))*CPPSS
      VE21=VE21+(V0100*E2SP(145)+V0200*E2SP(161)+V0300*E2SP(177))*CPPSS
      VE22=VE22+(V1000*E2SP( 97)+V2000*E2SP(161)+V3000*E2SP(225))*CPPSS
      VE31=VE31+(V0100*E2SP(209)+V0200*E2SP(225)+V0300*E2SP(241))*CPPSS
      VE32=VE32+(V1000*E2SP(113)+V2000*E2SP(177)+V3000*E2SP(241))*CPPSS
      VC12=VC12+(V0000*(E2SP(81)+E2SP(161)+E2SP(241)))*CPPSS
      IF(Jtype.EQ.3)GO TO 4000
! (PP!PS) and (SP!PS) Section.
      V0110=OQXO*V0100+OPOX*GOOXO+GXOXO
      V0120=OQYO*V0100+OPOX*GOOYO+GXOYO
      V0130=OQZO*V0100+OPOX*GOOZO+GXOZO
      V0210=OQXO*V0200+OPOY*GOOXO+GXOYO
      V0220=OQYO*V0200+OPOY*GOOYO+GYOYO
      V0230=OQZO*V0200+OPOY*GOOZO+GYOZO
      V0310=OQXO*V0300+OPOZ*GOOXO+GXOZO
      V0320=OQYO*V0300+OPOZ*GOOYO+GYOZO
      V0330=OQZO*V0300+OPOZ*GOOZO+GZOZO
      CSPPS=CSPP*CSD
      CPPPS=CPPP*CSD
      VSPPS=(V0110*E2SP( 21)+V0120*E2SP( 25)+V0130*E2SP( 29)+V0210*E2SP( 37)+V0220*E2SP( 41)+V0230*E2SP( 45)+ &
             V0310*E2SP( 53)+V0320*E2SP( 57)+V0330*E2SP( 61))*CSPPS
      VE00=VE00+VSPPS
      VE12=VE12+(V0010*E2SP( 21)+V0020*E2SP( 25)+V0030*E2SP( 29))*CSPPS
      VE13=VE13+(V0100*E2SP( 21)+V0200*E2SP( 37)+V0300*E2SP( 53))*CSPPS
      VE22=VE22+(V0010*E2SP( 37)+V0020*E2SP( 41)+V0030*E2SP( 45))*CSPPS
      VE23=VE23+(V0100*E2SP( 25)+V0200*E2SP( 41)+V0300*E2SP( 57))*CSPPS
      VE32=VE32+(V0010*E2SP( 53)+V0020*E2SP( 57)+V0030*E2SP( 61))*CSPPS
      VE33=VE33+(V0100*E2SP( 29)+V0200*E2SP( 45)+V0300*E2SP( 61))*CSPPS
      VC23=VC23+(V0000*(E2SP(21)+E2SP(41)+E2SP(61)))*CSPPS
      QFQ3=QA1*QA2*QA2*FQ3
      HFQ2=-PT5*QA2*FQ2*EABCDI
      TFQ2=HFQ2+HFQ2+HFQ2
      GXYZO=PQXY*PQZ*QFQ3
      TEMP=PQXX*QFQ3
      GXXXO=PQX*(TEMP+TFQ2)
      GXXYO=PQY*(TEMP+HFQ2)
      GXXZO=PQZ*(TEMP+HFQ2)
      TEMP=PQYY*QFQ3
      GYYYO=PQY*(TEMP+TFQ2)
      GYYXO=PQX*(TEMP+HFQ2)
      GYYZO=PQZ*(TEMP+HFQ2)
      TEMP=PQZZ*QFQ3
      GZZZO=PQZ*(TEMP+TFQ2)
      GZZXO=PQX*(TEMP+HFQ2)
      GZZYO=PQY*(TEMP+HFQ2)
      C1110=OPXX*GOOXO+OPXOX*GXOXO+GXXXO
      C1120=OPXX*GOOYO+OPXOX*GXOYO+GXXYO
      C1130=OPXX*GOOZO+OPXOX*GXOZO+GXXZO
      C2210=OPYY*GOOXO+OPYOY*GXOYO+GYYXO
      C2220=OPYY*GOOYO+OPYOY*GYOYO+GYYYO
      C2230=OPYY*GOOZO+OPYOY*GYOZO+GYYZO
      C3310=OPZZ*GOOXO+OPZOZ*GXOZO+GZZXO
      C3320=OPZZ*GOOYO+OPZOZ*GYOZO+GZZYO
      C3330=OPZZ*GOOZO+OPZOZ*GZOZO+GZZZO
      C1210=OPXY*GOOXO+OPXO*GXOYO+OPOY*GXOXO+GXXYO
      C1220=OPXY*GOOYO+OPXO*GYOYO+OPOY*GXOYO+GYYXO
      C1230=OPXY*GOOZO+OPXO*GYOZO+OPOY*GXOZO+GXYZO
      C1310=OPXZ*GOOXO+OPXO*GXOZO+OPOZ*GXOXO+GXXZO
      C1320=OPXZ*GOOYO+OPXO*GYOZO+OPOZ*GXOYO+GXYZO
      C1330=OPXZ*GOOZO+OPXO*GZOZO+OPOZ*GXOZO+GZZXO
      C2110=OPYX*GOOXO+OPYO*GXOXO+OPOX*GXOYO+GXXYO
      C2120=OPYX*GOOYO+OPYO*GXOYO+OPOX*GYOYO+GYYXO
      C2130=OPYX*GOOZO+OPYO*GXOZO+OPOX*GYOZO+GXYZO
      C2310=OPYZ*GOOXO+OPYO*GXOZO+OPOZ*GXOYO+GXYZO
      C2320=OPYZ*GOOYO+OPYO*GYOZO+OPOZ*GYOYO+GYYZO
      C2330=OPYZ*GOOZO+OPYO*GZOZO+OPOZ*GYOZO+GZZYO
      C3110=OPZX*GOOXO+OPZO*GXOXO+OPOX*GXOZO+GXXZO
      C3120=OPZX*GOOYO+OPZO*GXOYO+OPOX*GYOZO+GXYZO
      C3130=OPZX*GOOZO+OPZO*GXOZO+OPOX*GZOZO+GZZXO
      C3210=OPZY*GOOXO+OPZO*GXOYO+OPOY*GXOZO+GXYZO
      C3220=OPZY*GOOYO+OPZO*GYOYO+OPOY*GYOZO+GYYZO
      C3230=OPZY*GOOZO+OPZO*GYOZO+OPOY*GZOZO+GZZYO
      V1110=OQXO*V1100+C1110
      V1120=OQYO*V1100+C1120
      V1130=OQZO*V1100+C1130
      V1210=OQXO*V1200+C1210
      V1220=OQYO*V1200+C1220
      V1230=OQZO*V1200+C1230
      V1310=OQXO*V1300+C1310
      V1320=OQYO*V1300+C1320
      V1330=OQZO*V1300+C1330
      V2110=OQXO*V2100+C2110
      V2120=OQYO*V2100+C2120
      V2130=OQZO*V2100+C2130
      V2210=OQXO*V2200+C2210
      V2220=OQYO*V2200+C2220
      V2230=OQZO*V2200+C2230
      V2310=OQXO*V2300+C2310
      V2320=OQYO*V2300+C2320
      V2330=OQZO*V2300+C2330
      V3110=OQXO*V3100+C3110
      V3120=OQYO*V3100+C3120
      V3130=OQZO*V3100+C3130
      V3210=OQXO*V3200+C3210
      V3220=OQYO*V3200+C3220
      V3230=OQZO*V3200+C3230
      V3310=OQXO*V3300+C3310
      V3320=OQYO*V3300+C3320
      V3330=OQZO*V3300+C3330
      VPPPS=    (V1110*E2SP( 85)+V1120*E2SP( 89)+V1130*E2SP( 93)+V1210*E2SP(101)+V1220*E2SP(105)+V1230*E2SP(109)+ &
                 V1310*E2SP(117)+V1320*E2SP(121)+V1330*E2SP(125)+V2110*E2SP(149)+V2120*E2SP(153)+V2130*E2SP(157)+ &
                 V2210*E2SP(165)+V2220*E2SP(169)+V2230*E2SP(173)+V2310*E2SP(181)+V2320*E2SP(185)+V2330*E2SP(189)+ &
                 V3110*E2SP(213)+V3120*E2SP(217)+V3130*E2SP(221)+V3210*E2SP(229)+V3220*E2SP(233)+V3230*E2SP(237)+ &
                 V3310*E2SP(245)+V3320*E2SP(249)+V3330*E2SP(253))*CPPPS
      VE00=VE00+VPPPS
      VE11=VE11+(V0110*E2SP( 85)+V0120*E2SP( 89)+V0130*E2SP( 93)+V0210*E2SP(101)+V0220*E2SP(105)+V0230*E2SP(109)+ &
                 V0310*E2SP(117)+V0320*E2SP(121)+V0330*E2SP(125))*CPPPS
      VE12=VE12+(V1010*E2SP( 85)+V1020*E2SP( 89)+V1030*E2SP( 93)+V2010*E2SP(149)+V2020*E2SP(153)+V2030*E2SP(157)+ &
                 V3010*E2SP(213)+V3020*E2SP(217)+V3030*E2SP(221))*CPPPS
      VE13=VE13+(V1100*E2SP( 85)+V1200*E2SP(101)+V1300*E2SP(117)+V2100*E2SP(149)+V2200*E2SP(165)+V2300*E2SP(181)+ &
                 V3100*E2SP(213)+V3200*E2SP(229)+V3300*E2SP(245))*CPPPS
      VE21=VE21+(V0110*E2SP(149)+V0120*E2SP(153)+V0130*E2SP(157)+V0210*E2SP(165)+V0220*E2SP(169)+V0230*E2SP(173)+ &
                 V0310*E2SP(181)+V0320*E2SP(185)+V0330*E2SP(189))*CPPPS
      VE22=VE22+(V1010*E2SP(101)+V1020*E2SP(105)+V1030*E2SP(109)+V2010*E2SP(165)+V2020*E2SP(169)+V2030*E2SP(173)+ &
                 V3010*E2SP(229)+V3020*E2SP(233)+V3030*E2SP(237))*CPPPS
      VE23=VE23+(V1100*E2SP( 89)+V1200*E2SP(105)+V1300*E2SP(121)+V2100*E2SP(153)+V2200*E2SP(169)+V2300*E2SP(185)+ &
                 V3100*E2SP(217)+V3200*E2SP(233)+V3300*E2SP(249))*CPPPS
      VE31=VE31+(V0110*E2SP(213)+V0120*E2SP(217)+V0130*E2SP(221)+V0210*E2SP(229)+V0220*E2SP(233)+V0230*E2SP(237)+ &
                 V0310*E2SP(245)+V0320*E2SP(249)+V0330*E2SP(253))*CPPPS
      VE32=VE32+(V1010*E2SP(117)+V1020*E2SP(121)+V1030*E2SP(125)+V2010*E2SP(181)+V2020*E2SP(185)+V2030*E2SP(189)+ &
                 V3010*E2SP(245)+V3020*E2SP(249)+V3030*E2SP(253))*CPPPS
      VE33=VE33+(V1100*E2SP( 93)+V1200*E2SP(109)+V1300*E2SP(125)+V2100*E2SP(157)+V2200*E2SP(173)+V2300*E2SP(189)+ &
                 V3100*E2SP(221)+V3200*E2SP(237)+V3300*E2SP(253))*CPPPS
      VC12=VC12+(V0010*(E2SP( 85)+E2SP(165)+E2SP(245))+V0020*(E2SP( 89)+E2SP(169)+E2SP(249))+ &
                 V0030*(E2SP( 93)+E2SP(173)+E2SP(253)))*CPPPS
      VC13=VC13+(V0100*(E2SP( 85)+E2SP(153)+E2SP(221))+V0200*(E2SP(101)+E2SP(169)+E2SP(237))+ &
                 V0300*(E2SP(117)+E2SP(185)+E2SP(253)))*CPPPS
      VC23=VC23+(V1000*(E2SP( 85)+E2SP(105)+E2SP(125))+V2000*(E2SP(149)+E2SP(169)+E2SP(189))+ &
                 V3000*(E2SP(213)+E2SP(233)+E2SP(253)))*CPPPS
      IF(Jtype.EQ.6)then
! (PP!PP), (PP!SP), (PS!PP), (SP!PP), (SS!PP), (SP!SP), (PS!SP) and (SS!SP) Section.
         CALL FPPPPB (E2SP, FQ2, FQ3, FQ4)
      end if
 4000 IF(FLAG)GO TO 3002
!
! End of first pass through the integrals.
      FLAG=.TRUE.
      FQ0=FQ1
      FQ1=FQ2
      FQ2=FQ3
      FQ3=FQ4
      FQ4=FQ5
      IF(CSA.NE.ZERO)D2C1(I)=D2C1(I)+(VSSSS+VSSSP+VSSPS+VSSPP+VSPSS+VSPSP+VSPPS+VSPPP)/CSA
      IF(CSB.NE.ZERO)D2C1(J)=D2C1(J)+(VSSSS+VSSSP+VSSPS+VSSPP+VPSSS+VPSSP+VPSPS+VPSPP)/Basis%gaussian(J)%contrc
      IF(CSC.NE.ZERO)D2C1(K)=D2C1(K)+(VSSSS+VSSSP+VSPSS+VSPSP+VPSSS+VPSSP+VPPSS+VPPSP)/CSC
      IF(CSD.NE.ZERO)D2C1(L)=D2C1(L)+(VSSSS+VSSPS+VSPSS+VSPPS+VPSSS+VPSPS+VPPSS+VPPPS)/Basis%gaussian(L)%contrc
      VPA=(VPSSS+VPSSP+VPSPS+VPSPP+VPPSS+VPPSP+VPPPS+VPPPP)*(LA/4)
      VPB=(VSPSS+VSPSP+VSPPS+VSPPP+VPPSS+VPPSP+VPPPS+VPPPP)*(LB/4)
      VPC=(VSSPS+VSSPP+VSPPS+VSPPP+VPSPS+VPSPP+VPPPS+VPPPP)*(LC/4)
      VPD=(VSSSP+VSSPP+VSPSP+VSPPP+VPSSP+VPSPP+VPPSP+VPPPP)*(LD/4)
      IF(CPA.NE.ZERO)D2C2(I)=D2C2(I)+VPA/CPA
      IF(CPB.NE.ZERO)D2C2(J)=D2C2(J)+VPB/Basis%gaussian(J)%contrc
      IF(CPC.NE.ZERO)D2C2(K)=D2C2(K)+VPC/CPC
      IF(CPD.NE.ZERO)D2C2(L)=D2C2(L)+VPD/Basis%gaussian(L)%contrc
      VR12=VE00*R12+(VE11*ABX+VE21*ABY+VE31*ABZ)/AGexp-(VE12*ABX+VE22*ABY+VE32*ABZ)/BGexp-PT5*VC12/(AGexp*BGexp)
      VR34=VE00*R34+(VE13*CDX+VE23*CDY+VE33*CDZ)/CGexp-(VE14*CDX+VE24*CDY+VE34*CDZ)/DGexp-PT5*VC34/(CGexp*DGexp)
      DEXX2(I)=DEXX2(I)+VE00*((PT5*QA2-ONE5)*EPABI)-S2**2*VR12+(P75*VE00-PT5*VPA)/AGexp
      DEXX2(J)=DEXX2(J)+VE00*((PT5*QA2-ONE5)*EPABI)-S1**2*VR12+(P75*VE00-PT5*VPB)/BGexp
      DEXX2(K)=DEXX2(K)+VE00*((PT5*QA1-ONE5)*EQCDI)-S4**2*VR34+(P75*VE00-PT5*VPC)/CGexp
      DEXX2(L)=DEXX2(L)+VE00*((PT5*QA1-ONE5)*EQCDI)-S3**2*VR34+(P75*VE00-PT5*VPD)/DGexp
      VE00S=VE00S+VE00
      VE11S=VE11S+VE11
      VE21S=VE21S+VE21
      VE31S=VE31S+VE31
      VE12S=VE12S+VE12
      VE22S=VE22S+VE22
      VE32S=VE32S+VE32
      CDVE00=S34*(VE00+VE00)
      DX2X=-VE13*S4+VE14*S3-CDX*CDVE00
      DX2Y=-VE23*S4+VE24*S3-CDY*CDVE00
      DX2Z=-VE33*S4+VE34*S3-CDZ*CDVE00
      GO TO 3001
!
! End of second pass through the integrals.
 3002 VP=PQX**2+PQY**2+PQZ**2
      VP1=(VE11+VE12)*EPABI-(VE13+VE14)*EQCDI
      VP2=(VE21+VE22)*EPABI-(VE23+VE24)*EQCDI
      VP3=(VE31+VE32)*EPABI-(VE33+VE34)*EQCDI
      VR12=VE00*(PQX*ABX+PQY*ABY+PQZ*ABZ)*TWO+VP1*ABX+VP2*ABY+VP3*ABZ+(VE11*PQX+VE21*PQY+VE31*PQZ)/AGexp- &
                 (VE12*PQX+VE22*PQY+VE32*PQZ)/BGexp+((VC12*EPABI-(VC13+VC14)*EQCDI)/AGexp-(VC12*EPABI-(VC23+VC24)*EQCDI)/BGexp)*PT5
      VR34=VE00*(PQX*CDX+PQY*CDY+PQZ*CDZ)*TWO+VP1*CDX+VP2*CDY+VP3*CDZ+(VE13*PQX+VE23*PQY+VE33*PQZ)/CGexp- &
                 (VE14*PQX+VE24*PQY+VE34*PQZ)/DGexp+(((VC13+VC23)*EPABI-VC34*EQCDI)/CGexp-((VC14+VC24)*EPABI-VC34*EQCDI)/DGexp)*PT5
      VP=VE00*VP+(VP1*PQX+VP2*PQY+VP3*PQZ)+PT5*(VC12*(EPABI**2)-(VC13+VC14+VC23+VC24)*EPABI*EQCDI+VC34*(EQCDI**2))
      DEXX2(I)=DEXX2(I)-QA2**2*VP-S2*QA2*VR12
      DEXX2(J)=DEXX2(J)-QA2**2*VP+S1*QA2*VR12
      DEXX2(K)=DEXX2(K)-QA1**2*VP+S4*QA1*VR34
      DEXX2(L)=DEXX2(L)-QA1**2*VP-S3*QA1*VR34
      QVE00=QA*(VE00+VE00)
      DVEX=-(VE11+VE12)*QA2+(VE13+VE14)*QA1-PQX*QVE00
      DVEY=-(VE21+VE22)*QA2+(VE23+VE24)*QA1-PQY*QVE00
      DVEZ=-(VE31+VE32)*QA2+(VE33+VE34)*QA1-PQZ*QVE00
      DVEXS=DVEXS+DVEX
      DVEYS=DVEYS+DVEY
      DVEZS=DVEZS+DVEZ
!
! Summation of contributions from the uncontracted Gaussians.
      FXK=FXK+DX2X-DVEX*S3
      FYK=FYK+DX2Y-DVEY*S3
      FZK=FZK+DX2Z-DVEZ*S3
 2004 continue
      end do ! Lgauss
      end do ! Kgauss
      VEEXX=VEEXX+VE00S
      ABVE00=S12*(VE00S+VE00S)
      DX1X=-VE11S*S2+VE12S*S1-ABX*ABVE00
      DX1Y=-VE21S*S2+VE22S*S1-ABY*ABVE00
      DX1Z=-VE31S*S2+VE32S*S1-ABZ*ABVE00
      FXI=FXI+DX1X+DVEXS*S1
      FYI=FYI+DX1Y+DVEYS*S1
      FZI=FZI+DX1Z+DVEZS*S1
      FXJ=FXJ-DX1X+DVEXS*S2
      FYJ=FYJ-DX1Y+DVEYS*S2
      FZJ=FZJ-DX1Z+DVEZS*S2
 2002 continue
      end do ! Jgauss
      end do ! Igauss
!     write(6,*)'FXI: ',FXI
!     write(6,*)'FXJ: ',FXJ
!     write(6,*)'FXK: ',FXK
!     write(6,*)'FYI: ',FYI
!     write(6,*)'FYJ: ',FYJ
!     write(6,*)'FYK: ',FYK
!     write(6,*)'FZI: ',FZI
!     write(6,*)'FZJ: ',FZJ
!     write(6,*)'FZK: ',FZK
!     call flush(6)
      IatomX=3*(Iatom-1)+1
      IatomY=IatomX+1
      IatomZ=IatomY+1
      JatomX=3*(Jatom-1)+1
      JatomY=JatomX+1
      JatomZ=JatomY+1
      KatomX=3*(Katom-1)+1
      KatomY=KatomX+1
      KatomZ=KatomY+1
      LatomX=3*(Latom-1)+1
      LatomY=LatomX+1
      LatomZ=LatomY+1
      gradient(IatomX)%eewfn=gradient(IatomX)%eewfn+FXI
      gradient(JatomX)%eewfn=gradient(JatomX)%eewfn+FXJ
      gradient(KatomX)%eewfn=gradient(KatomX)%eewfn+FXK
      gradient(LatomX)%eewfn=gradient(LatomX)%eewfn-FXI-FXJ-FXK
      gradient(IatomY)%eewfn=gradient(IatomY)%eewfn+FYI
      gradient(JatomY)%eewfn=gradient(JatomY)%eewfn+FYJ
      gradient(KatomY)%eewfn=gradient(KatomY)%eewfn+FYK
      gradient(LatomY)%eewfn=gradient(LatomY)%eewfn-FYI-FYJ-FYK
      gradient(IatomZ)%eewfn=gradient(IatomZ)%eewfn+FZI
      gradient(JatomZ)%eewfn=gradient(JatomZ)%eewfn+FZJ
      gradient(KatomZ)%eewfn=gradient(KatomZ)%eewfn+FZK
      gradient(LatomZ)%eewfn=gradient(LatomZ)%eewfn-FZI-FZJ-FZK
 1004 continue
!
! End of routine G2EB_SPDF
      call PRG_manager ('exit', 'G2EB_SPDF', 'UTILITY')
      return
      end subroutine G2EB_SPDF
! end CONTAINS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine G2E_basis
      subroutine FPPPPB (E2SP, FQ2, FQ3, FQ4)
!***********************************************************************
!     Date last modified: June 15, 1992                    Version 1.0 *
!     Author:                                                          *
!     Description:                                                     *
!***********************************************************************
! Modules:

      implicit none
!
! Input array:
      double precision E2SP(256)
! Input scalars:
      double precision FQ2,FQ3,FQ4
!
! Local scalars:
      double precision C1011,C1012,C1013,C1021,C1022,C1023,C1031,C1032,C1033,C2011,C2012,C2013,C2021,C2022,C2023,C2031, &
                       C2032,C2033,C3011,C3012,C3013,C3021,C3022,C3023,C3031,C3032,C3033,H,HFQ3,PTOQ,PTOQS,P25,P25FQ2,P75FQ2, &
                       QFQ4,SFQ3,TEMP,TFQ3,V0001,V0002,V0003,V0011,V0012,V0013,V0021,V0022,V0023,V0031,V0032,V0033,V0101,V0102, &
                       V0103,V0111,V0112,V0113,V0121,V0122,V0123,V0131,V0132,V0133,V0201,V0202,V0203,V0211,V0212,V0213, &
                       V0221,V0222,V0223,V0231,V0232,V0233,V0301,V0302,V0303,V0311,V0312,V0313,V0321,V0322,V0323,V0331, &
                       V0332,V0333,V1001,V1002,V1003,V1011,V1012,V1013,V1021,V1022,V1023,V1031,V1032,V1033,V1101,V1102, &
                       V1103,V1201,V1202,V1203,V1301,V1302,V1303,V2001,V2002,V2003,V2011,V2012,V2013,V2021,V2022,V2023, &
                       V2031,V2032,V2033,V2101,V2102,V2103,V2201,V2202,V2203,V2301,V2302,V2303,V3001,V3002,V3003,V3011, &
                       V3012,V3013,V3021,V3022,V3023,V3031,V3032,V3033,V3101,V3102,V3103,V3201,V3202,V3203,V3301,V3302, &
                       V3303,VP4
!
      double precision QA,QA1,QA2,EPAB,EQCDI,EABCDI,PQXX,PQYY,PQZZ,PQXY,PQXZ,PQYZ, &
                       V0000,V0010,V0020,V0030,V0100,V0200,V0300,V0110,V0120,V0130, &
                       V0210,V0220,V0230,V0310,V0320,V0330,V1010,V1020, &
                       V1030,V2010,V2020,V2030,V3010,V3020,V3030,V1000,V2000,V3000, &
                       V1100,V2100,V3100,V1200,V2200,V3200,V1300,V2300, &
                       V3300,V1110,V2110,V3110,V1210,V2210,V3210,V1310,V2310,V3310, &
                       V1120,V2120,V3120,V1220,V2220,V3220,V1320,V2320, &
                       V3320,V1130,V2130,V3130,V1230,V2230,V3230,V1330,V2330,V3330, &
                       C1110,C2110,C3110,C1210,C2210,C3210,C1320,C2320, &
                       C3320,C1130,C2130,C3130,C1230,C2230,C3230,C1310,C2310, &
                       C3310,C1120,C2120,C3120,C1220,C2220,C3220,C1330,C2330, &
                       C3330,OPXO,OPYO,OPZO,OPOX,OPOY,OPOZ,OPXOX,OPYOY,OPZOZ, &
                       OPXX,OPXY,OPXZ,OPYX,OPYY,OPYZ,OPZX,OPZY,OPZZ,OQXO,OQYO, &
                       OQZO,OQOX,OQOY,OQOZ,OQXOX,OQYOY,OQZOZ,OQXX,OQXY,OQXZ,OQYX, &
                       OQYY,OQYZ,OQZX,OQZY,OQZZ,GOOOO,GOOXO,GOOYO,GOOZO, &
                       GXOOO,GXOXO,GXOYO,GXOZO,GXXOO,GXXXO,GXXYO,GXXZO,GXYOO,GXYZO, &
                       GXZOO,GYOOO,GYOYO,GYOZO,GYYOO,GYYXO,GYYYO,GYYZO, &
                       GYZOO,GZOOO,GZOZO,GZZOO,GZZXO,GZZYO,GZZZO,VE00,VE11,VE12,VE13, &
                       VE14,VE21,VE22,VE23,VE24,VE31,VE32,VE33,VE34, &
                       CSSSP,CSSPP,CSPSP,CPSSP,CSPPP,CPSPP,CPPSP,CPPPP,VC12,VC13,VC14, &
                       VC23,VC24,VC34,VSSSP,VPSSP,VSPSP,VPPSP,VSSPP, &
                       VPSPP,VSPPP,VPPPP, &
                       GOOXX,GOOXY,GOOXZ,GOOYY,GOOYZ,GOOZZ,GXOXX,GXOYY,GXOYZ,GXOZZ,GXXXX,GXXXY,GXXXZ, &
                       GXXYX,GXXYY,GXXYZ,GXXZX,GXXZY,GXXZZ,GXYXO,GXYXX,GXYXY,GXYXZ,GXYYO,GXYYX,GXYYY,GXYYZ,GXYZX,GXYZY, &
                       GXYZZ,GXZXO,GXZXX,GXZXY,GXZXZ,GXZYO,GXZYX,GXZYY,GXZYZ,GXZZO,GXZZX,GXZZY,GXZZZ,GYOXX,GYOYY,GYOZZ, &
                       GYXOO,GYXXO,GYXXX,GYXXY,GYXXZ,GYXYO,GYXYX,GYXYY,GYXYZ,GYXZO,GYXZX,GYXZY,GYXZZ,GYYXX,GYYXY,GYYXZ, &
                       GYYYX,GYYYY,GYYYZ,GYYZX,GYYZY,GYYZZ,GYZXO,GYZXX,GYZXY,GYZXZ,GYZYO,GYZYX,GYZYY,GYZYZ,GYZZO,GYZZX, &
                       GYZZY,GYZZZ,GZOXX,GZOYY,GZOZZ,GZXOO,GZXXO,GZXXX,GZXXY,GZXXZ,GZXYO,GZXYX,GZXYY,GZXYZ,GZXZO,GZXZX, &
                       GZXZY,GZXZZ,GZYOO,GZYXO,GZYXX,GZYXY,GZYXZ,GZYYO,GZYYX,GZYYY,GZYYZ,GZYZO,GZYZX,GZYZY,GZYZZ,GZZXX, &
                       GZZXY,GZZXZ,GZZYX,GZZYY,GZZYZ,GZZZX,GZZZY,GZZZZ
!
      equivalence (GXYOO,GYXOO),(GXZOO,GZXOO),(GYZOO,GZYOO),(GYXXO,GXYXO,GXXYO), &
                       (GZXXO,GXZXO,GXXZO),(GZYYO,GYZYO,GYYZO), &
                  (GXYYO,GYXYO,GYYXO),(GXZZO,GZXZO,GZZXO),(GYZZO,GZYZO,GZZYO), &
                       (GXYZO,GYZXO,GZXYO,GZYXO,GYXZO,GXZYO), &
                  (GYYXX,GYXYX,GYXXY,GXYYX,GXYXY,GXXYY),(GZZXX,GZXZX,GZXXZ,GXZZX,GXZXZ,GXXZZ), &
                  (GZZYY,GZYZY,GZYYZ,GYZZY,GYZYZ,GYYZZ),(GYXXX,GXYXX,GXXYX,GXXXY),(GZXXX,GXZXX,GXXZX,GXXXZ), &
                  (GXYYY,GYXYY,GYYXY,GYYYX),(GZYYY,GYZYY,GYYZY,GYYYZ), &
                       (GXZZZ,GZXZZ,GZZXZ,GZZZX),(GYZZZ,GZYZZ,GZZYZ,GZZZY), &
                  (GXYZZ,GXZYZ,GXZZY,GYXZZ,GYZXZ,GYZZX,GZXYZ,GZXZY,GZYXZ,GZYZX,GZZXY,GZZYX), &
                  (GYZXX,GYXZX,GYXXZ,GZYXX,GZXYX,GZXXY,GXYZX,GXYXZ,GXZYX,GXZXY,GXXYZ,GXXZY), &
                  (GZXYY,GZYXY,GZYYX,GXZYY,GXYZY,GXYYZ,GYZXY,GYZYX,GYXZY,GYXYZ,GYYZX,GYYXZ)

! Local array:
!
      double precision :: PT5
      PARAMETER (P25=0.25D0,PT5=0.5D0)
!
! Begin:
      call PRG_manager ('enter', 'FPPPPB', 'UTILITY')
!
      V0001=OQOX*GOOOO+GOOXO
      V0002=OQOY*GOOOO+GOOYO
      V0003=OQOZ*GOOOO+GOOZO
      VSSSP=(V0001*E2SP(2)+V0002*E2SP(3)+V0003*E2SP(4))*CSSSP
      VE00=VE00+VSSSP
      TEMP=V0000*CSSSP
      VE14=TEMP*E2SP(2)
      VE24=TEMP*E2SP(3)
      VE34=TEMP*E2SP(4)
      PTOQ=-EPAB*EQCDI
      PTOQS=PTOQ*PTOQ
      GOOXX=GXXOO*PTOQS
      GOOYY=GYYOO*PTOQS
      GOOZZ=GZZOO*PTOQS
      GOOXY=GXYOO*PTOQS
      GOOXZ=GXZOO*PTOQS
      GOOYZ=GYZOO*PTOQS
      V0011=OQXX*GOOOO+OQXOX*GOOXO+GOOXX
      V0022=OQYY*GOOOO+OQYOY*GOOYO+GOOYY
      V0033=OQZZ*GOOOO+OQZOZ*GOOZO+GOOZZ
      V0012=OQXY*GOOOO+OQXO*GOOYO+OQOY*GOOXO+GOOXY
      V0013=OQXZ*GOOOO+OQXO*GOOZO+OQOZ*GOOXO+GOOXZ
      V0021=OQYX*GOOOO+OQYO*GOOXO+OQOX*GOOYO+GOOXY
      V0023=OQYZ*GOOOO+OQYO*GOOZO+OQOZ*GOOYO+GOOYZ
      V0031=OQZX*GOOOO+OQZO*GOOXO+OQOX*GOOZO+GOOXZ
      V0032=OQZY*GOOOO+OQZO*GOOYO+OQOY*GOOZO+GOOYZ
      VSSPP=    (V0011*E2SP( 6)+V0012*E2SP( 7)+V0013*E2SP( 8)+V0021*E2SP(10)+V0022*E2SP(11)+V0023*E2SP(12)+ &
                 V0031*E2SP(14)+V0032*E2SP(15)+V0033*E2SP(16))*CSSPP
      VE00=VE00+VSSPP
      VE14=VE14+(V0010*E2SP(6)+V0020*E2SP(10)+V0030*E2SP(14))*CSSPP
      VE24=VE24+(V0010*E2SP(7)+V0020*E2SP(11)+V0030*E2SP(15))*CSSPP
      VE34=VE34+(V0010*E2SP(8)+V0020*E2SP(12)+V0030*E2SP(16))*CSSPP
      VE13=VE13+(V0001*E2SP(6)+V0002*E2SP(7)+V0003*E2SP(8))*CSSPP
      VE23=VE23+(V0001*E2SP(10)+V0002*E2SP(11)+V0003*E2SP(12))*CSSPP
      VE33=VE33+(V0001*E2SP(14)+V0002*E2SP(15)+V0003*E2SP(16))*CSSPP
      VC34=VC34+(V0000*(E2SP(6)+E2SP(11)+E2SP(16)))*CSSPP
      V0101=OQOX*V0100+OPOX*GOOXO+GXOXO
      V0102=OQOY*V0100+OPOX*GOOYO+GXOYO
      V0103=OQOZ*V0100+OPOX*GOOZO+GXOZO
      V0201=OQOX*V0200+OPOY*GOOXO+GXOYO
      V0202=OQOY*V0200+OPOY*GOOYO+GYOYO
      V0203=OQOZ*V0200+OPOY*GOOZO+GYOZO
      V0301=OQOX*V0300+OPOZ*GOOXO+GXOZO
      V0302=OQOY*V0300+OPOZ*GOOYO+GYOZO
      V0303=OQOZ*V0300+OPOZ*GOOZO+GZOZO
      VSPSP=    (V0101*E2SP(18)+V0102*E2SP(19)+V0103*E2SP(20)+V0201*E2SP(34)+V0202*E2SP(35)+V0203*E2SP(36)+ &
                 V0301*E2SP(50)+V0302*E2SP(51)+V0303*E2SP(52))*CSPSP
      VE00=VE00+VSPSP
      VE14=VE14+(V0100*E2SP(18)+V0200*E2SP(34)+V0300*E2SP(50))*CSPSP
      VE24=VE24+(V0100*E2SP(19)+V0200*E2SP(35)+V0300*E2SP(51))*CSPSP
      VE34=VE34+(V0100*E2SP(20)+V0200*E2SP(36)+V0300*E2SP(52))*CSPSP
      VE12=VE12+(V0001*E2SP(18)+V0002*E2SP(19)+V0003*E2SP(20))*CSPSP
      VE22=VE22+(V0001*E2SP(34)+V0002*E2SP(35)+V0003*E2SP(36))*CSPSP
      VE32=VE32+(V0001*E2SP(50)+V0002*E2SP(51)+V0003*E2SP(52))*CSPSP
      VC24=VC24+(V0000*(E2SP(18)+E2SP(35)+E2SP(52)))*CSPSP
      V1001=OQOX*V1000+OPXO*GOOXO+GXOXO
      V1002=OQOY*V1000+OPXO*GOOYO+GXOYO
      V1003=OQOZ*V1000+OPXO*GOOZO+GXOZO
      V2001=OQOX*V2000+OPYO*GOOXO+GXOYO
      V2002=OQOY*V2000+OPYO*GOOYO+GYOYO
      V2003=OQOZ*V2000+OPYO*GOOZO+GYOZO
      V3001=OQOX*V3000+OPZO*GOOXO+GXOZO
      V3002=OQOY*V3000+OPZO*GOOYO+GYOZO
      V3003=OQOZ*V3000+OPZO*GOOZO+GZOZO
      VPSSP=    (V1001*E2SP( 66)+V1002*E2SP( 67)+V1003*E2SP( 68)+V2001*E2SP(130)+V2002*E2SP(131)+V2003*E2SP(132)+ &
                 V3001*E2SP(194)+V3002*E2SP(195)+V3003*E2SP(196))*CPSSP
      VE00=VE00+VPSSP
      VE14=VE14+(V1000*E2SP(66)+V2000*E2SP(130)+V3000*E2SP(194))*CPSSP
      VE24=VE24+(V1000*E2SP(67)+V2000*E2SP(131)+V3000*E2SP(195))*CPSSP
      VE34=VE34+(V1000*E2SP(68)+V2000*E2SP(132)+V3000*E2SP(196))*CPSSP
      VE11=VE11+(V0001*E2SP(66)+V0002*E2SP(67)+V0003*E2SP(68))*CPSSP
      VE21=VE21+(V0001*E2SP(130)+V0002*E2SP(131)+V0003*E2SP(132))*CPSSP
      VE31=VE31+(V0001*E2SP(194)+V0002*E2SP(195)+V0003*E2SP(196))*CPSSP
      VC14=VC14+(V0000*(E2SP(66)+E2SP(131)+E2SP(196)))*CPSSP
      V1101=OQOX*V1100+C1110
      V1102=OQOY*V1100+C1120
      V1103=OQOZ*V1100+C1130
      V1201=OQOX*V1200+C1210
      V1202=OQOY*V1200+C1220
      V1203=OQOZ*V1200+C1230
      V1301=OQOX*V1300+C1310
      V1302=OQOY*V1300+C1320
      V1303=OQOZ*V1300+C1330
      V2101=OQOX*V2100+C2110
      V2102=OQOY*V2100+C2120
      V2103=OQOZ*V2100+C2130
      V2201=OQOX*V2200+C2210
      V2202=OQOY*V2200+C2220
      V2203=OQOZ*V2200+C2230
      V2301=OQOX*V2300+C2310
      V2302=OQOY*V2300+C2320
      V2303=OQOZ*V2300+C2330
      V3101=OQOX*V3100+C3110
      V3102=OQOY*V3100+C3120
      V3103=OQOZ*V3100+C3130
      V3201=OQOX*V3200+C3210
      V3202=OQOY*V3200+C3220
      V3203=OQOZ*V3200+C3230
      V3301=OQOX*V3300+C3310
      V3302=OQOY*V3300+C3320
      V3303=OQOZ*V3300+C3330
      VPPSP=    (V1101*E2SP( 82)+V1102*E2SP( 83)+V1103*E2SP( 84)+V1201*E2SP( 98)+V1202*E2SP( 99)+V1203*E2SP(100)+ &
                 V1301*E2SP(114)+V1302*E2SP(115)+V1303*E2SP(116)+V2101*E2SP(146)+V2102*E2SP(147)+V2103*E2SP(148)+ &
                 V2201*E2SP(162)+V2202*E2SP(163)+V2203*E2SP(164)+V2301*E2SP(178)+V2302*E2SP(179)+V2303*E2SP(180)+ &
                 V3101*E2SP(210)+V3102*E2SP(211)+V3103*E2SP(212)+V3201*E2SP(226)+V3202*E2SP(227)+V3203*E2SP(228)+ &
                 V3301*E2SP(242)+V3302*E2SP(243)+V3303*E2SP(244))*CPPSP
      VE00=VE00+VPPSP
      VE14=VE14+(V1100*E2SP( 82)+V1200*E2SP( 98)+V1300*E2SP(114)+V2100*E2SP(146)+V2200*E2SP(162)+V2300*E2SP(178)+ &
                 V3100*E2SP(210)+V3200*E2SP(226)+V3300*E2SP(242))*CPPSP
      VE24=VE24+(V1100*E2SP( 83)+V1200*E2SP( 99)+V1300*E2SP(115)+V2100*E2SP(147)+V2200*E2SP(163)+V2300*E2SP(179)+ &
                 V3100*E2SP(211)+V3200*E2SP(227)+V3300*E2SP(243))*CPPSP
      VE34=VE34+(V1100*E2SP( 84)+V1200*E2SP(100)+V1300*E2SP(116)+V2100*E2SP(148)+V2200*E2SP(164)+V2300*E2SP(180)+ &
                 V3100*E2SP(212)+V3200*E2SP(228)+V3300*E2SP(244))*CPPSP
      VE12=VE12+(V1001*E2SP( 82)+V1002*E2SP( 83)+V1003*E2SP( 84)+V2001*E2SP(146)+V2002*E2SP(147)+V2003*E2SP(148)+ &
                 V3001*E2SP(210)+V3002*E2SP(211)+V3003*E2SP(212))*CPPSP
      VE22=VE22+(V1001*E2SP( 98)+V1002*E2SP( 99)+V1003*E2SP(100)+V2001*E2SP(162)+V2002*E2SP(163)+V2003*E2SP(164)+ &
                 V3001*E2SP(226)+V3002*E2SP(227)+V3003*E2SP(228))*CPPSP
      VE32=VE32+(V1001*E2SP(114)+V1002*E2SP(115)+V1003*E2SP(116)+V2001*E2SP(178)+V2002*E2SP(179)+V2003*E2SP(180)+ &
                 V3001*E2SP(242)+V3002*E2SP(243)+V3003*E2SP(244))*CPPSP
      VE11=VE11+(V0101*E2SP( 82)+V0102*E2SP( 83)+V0103*E2SP( 84)+V0201*E2SP( 98)+V0202*E2SP( 99)+V0203*E2SP(100)+ &
                 V0301*E2SP(114)+V0302*E2SP(115)+V0303*E2SP(116))*CPPSP
      VE21=VE21+(V0101*E2SP(146)+V0102*E2SP(147)+V0103*E2SP(148)+V0201*E2SP(162)+V0202*E2SP(163)+V0203*E2SP(164)+ &
                 V0301*E2SP(178)+V0302*E2SP(179)+V0303*E2SP(180))*CPPSP
      VE31=VE31+(V0101*E2SP(210)+V0102*E2SP(211)+V0103*E2SP(212)+V0201*E2SP(226)+V0202*E2SP(227)+V0203*E2SP(228)+ &
                 V0301*E2SP(242)+V0302*E2SP(243)+V0303*E2SP(244))*CPPSP
      VC12=VC12+(V0001*(E2SP( 82)+E2SP(162)+E2SP(242))+V0002*(E2SP( 83)+E2SP(163)+E2SP(243))+ &
                 V0003*(E2SP( 84)+E2SP(164)+E2SP(244)))*CPPSP
      VC14=VC14+(V0100*(E2SP( 82)+E2SP(147)+E2SP(212))+V0200*(E2SP( 98)+E2SP(163)+E2SP(228))+ &
                 V0300*(E2SP(114)+E2SP(179)+E2SP(244)))*CPPSP
      VC24=VC24+(V1000*(E2SP( 82)+E2SP( 99)+E2SP(116))+V2000*(E2SP(146)+E2SP(163)+E2SP(180))+ &
                 V3000*(E2SP(210)+E2SP(227)+E2SP(244)))*CPPSP
      GXOYZ=GXYZO*PTOQ
      GXOXX=GXXXO*PTOQ
      GYOYY=GYYYO*PTOQ
      GZOZZ=GZZZO*PTOQ
      GYOXX=GXXYO*PTOQ
      GZOXX=GXXZO*PTOQ
      GXOYY=GYYXO*PTOQ
      GZOYY=GYYZO*PTOQ
      GXOZZ=GZZXO*PTOQ
      GYOZZ=GZZYO*PTOQ
      C1011=OQXX*GXOOO+OQXOX*GXOXO+GXOXX
      C1022=OQYY*GXOOO+OQYOY*GXOYO+GXOYY
      C1033=OQZZ*GXOOO+OQZOZ*GXOZO+GXOZZ
      C2011=OQXX*GYOOO+OQXOX*GXOYO+GYOXX
      C2022=OQYY*GYOOO+OQYOY*GYOYO+GYOYY
      C2033=OQZZ*GYOOO+OQZOZ*GYOZO+GYOZZ
      C3011=OQXX*GZOOO+OQXOX*GXOZO+GZOXX
      C3022=OQYY*GZOOO+OQYOY*GYOZO+GZOYY
      C3033=OQZZ*GZOOO+OQZOZ*GZOZO+GZOZZ
      C1012=OQXY*GXOOO+OQXO*GXOYO+OQOY*GXOXO+GYOXX
      C1013=OQXZ*GXOOO+OQXO*GXOZO+OQOZ*GXOXO+GZOXX
      C1021=OQYX*GXOOO+OQYO*GXOXO+OQOX*GXOYO+GYOXX
      C1023=OQYZ*GXOOO+OQYO*GXOZO+OQOZ*GXOYO+GXOYZ
      C1031=OQZX*GXOOO+OQZO*GXOXO+OQOX*GXOZO+GZOXX
      C1032=OQZY*GXOOO+OQZO*GXOYO+OQOY*GXOZO+GXOYZ
      C2012=OQXY*GYOOO+OQXO*GYOYO+OQOY*GXOYO+GXOYY
      C2013=OQXZ*GYOOO+OQXO*GYOZO+OQOZ*GXOYO+GXOYZ
      C2021=OQYX*GYOOO+OQYO*GXOYO+OQOX*GYOYO+GXOYY
      C2023=OQYZ*GYOOO+OQYO*GYOZO+OQOZ*GYOYO+GZOYY
      C2031=OQZX*GYOOO+OQZO*GXOYO+OQOX*GYOZO+GXOYZ
      C2032=OQZY*GYOOO+OQZO*GYOYO+OQOY*GYOZO+GZOYY
      C3012=OQXY*GZOOO+OQXO*GYOZO+OQOY*GXOZO+GXOYZ
      C3013=OQXZ*GZOOO+OQXO*GZOZO+OQOZ*GXOZO+GXOZZ
      C3021=OQYX*GZOOO+OQYO*GXOZO+OQOX*GYOZO+GXOYZ
      C3023=OQYZ*GZOOO+OQYO*GZOZO+OQOZ*GYOZO+GYOZZ
      C3031=OQZX*GZOOO+OQZO*GXOZO+OQOX*GZOZO+GXOZZ
      C3032=OQZY*GZOOO+OQZO*GYOZO+OQOY*GZOZO+GYOZZ
      V0111=OPOX*V0011+C1011
      V0112=OPOX*V0012+C1012
      V0113=OPOX*V0013+C1013
      V0121=OPOX*V0021+C1021
      V0122=OPOX*V0022+C1022
      V0123=OPOX*V0023+C1023
      V0131=OPOX*V0031+C1031
      V0132=OPOX*V0032+C1032
      V0133=OPOX*V0033+C1033
      V0211=OPOY*V0011+C2011
      V0212=OPOY*V0012+C2012
      V0213=OPOY*V0013+C2013
      V0221=OPOY*V0021+C2021
      V0222=OPOY*V0022+C2022
      V0223=OPOY*V0023+C2023
      V0231=OPOY*V0031+C2031
      V0232=OPOY*V0032+C2032
      V0233=OPOY*V0033+C2033
      V0311=OPOZ*V0011+C3011
      V0312=OPOZ*V0012+C3012
      V0313=OPOZ*V0013+C3013
      V0321=OPOZ*V0021+C3021
      V0322=OPOZ*V0022+C3022
      V0323=OPOZ*V0023+C3023
      V0331=OPOZ*V0031+C3031
      V0332=OPOZ*V0032+C3032
      V0333=OPOZ*V0033+C3033
      VSPPP=    (V0111*E2SP( 22)+V0112*E2SP( 23)+V0113*E2SP( 24)+V0121*E2SP( 26)+V0122*E2SP( 27)+V0123*E2SP( 28)+ &
                 V0131*E2SP( 30)+V0132*E2SP( 31)+V0133*E2SP( 32)+V0211*E2SP( 38)+V0212*E2SP( 39)+V0213*E2SP( 40)+ &
                 V0221*E2SP( 42)+V0222*E2SP( 43)+V0223*E2SP( 44)+V0231*E2SP( 46)+V0232*E2SP( 47)+V0233*E2SP( 48)+ &
                 V0311*E2SP( 54)+V0312*E2SP( 55)+V0313*E2SP( 56)+V0321*E2SP( 58)+V0322*E2SP( 59)+V0323*E2SP( 60)+ &
                 V0331*E2SP( 62)+V0332*E2SP( 63)+V0333*E2SP( 64))*CSPPP
      VE00=VE00+VSPPP
      VE14=VE14+(V0110*E2SP( 22)+V0120*E2SP( 26)+V0130*E2SP( 30)+V0210*E2SP( 38)+V0220*E2SP( 42)+V0230*E2SP( 46)+ &
                 V0310*E2SP( 54)+V0320*E2SP( 58)+V0330*E2SP( 62))*CSPPP
      VE24=VE24+(V0110*E2SP( 23)+V0120*E2SP( 27)+V0130*E2SP( 31)+V0210*E2SP( 39)+V0220*E2SP( 43)+V0230*E2SP( 47)+ &
                 V0310*E2SP( 55)+V0320*E2SP( 59)+V0330*E2SP( 63))*CSPPP
      VE34=VE34+(V0110*E2SP( 24)+V0120*E2SP( 28)+V0130*E2SP( 32)+V0210*E2SP( 40)+V0220*E2SP( 44)+V0230*E2SP( 48)+ &
                 V0310*E2SP( 56)+V0320*E2SP( 60)+V0330*E2SP( 64))*CSPPP
      VE13=VE13+(V0101*E2SP( 22)+V0102*E2SP( 23)+V0103*E2SP( 24)+V0201*E2SP( 38)+V0202*E2SP( 39)+V0203*E2SP( 40)+ &
                 V0301*E2SP( 54)+V0302*E2SP( 55)+V0303*E2SP( 56))*CSPPP
      VE23=VE23+(V0101*E2SP( 26)+V0102*E2SP( 27)+V0103*E2SP( 28)+V0201*E2SP( 42)+V0202*E2SP( 43)+V0203*E2SP( 44)+ &
                 V0301*E2SP( 58)+V0302*E2SP( 59)+V0303*E2SP( 60))*CSPPP
      VE33=VE33+(V0101*E2SP( 30)+V0102*E2SP( 31)+V0103*E2SP( 32)+V0201*E2SP( 46)+V0202*E2SP( 47)+V0203*E2SP( 48)+ &
                 V0301*E2SP( 62)+V0302*E2SP( 63)+V0303*E2SP( 64))*CSPPP
      VE12=VE12+(V0011*E2SP( 22)+V0012*E2SP( 23)+V0013*E2SP( 24)+V0021*E2SP( 26)+V0022*E2SP( 27)+V0023*E2SP( 28)+ &
                 V0031*E2SP( 30)+V0032*E2SP( 31)+V0033*E2SP( 32))*CSPPP
      VE22=VE22+(V0011*E2SP( 38)+V0012*E2SP( 39)+V0013*E2SP( 40)+V0021*E2SP( 42)+V0022*E2SP( 43)+V0023*E2SP( 44)+ &
                 V0031*E2SP( 46)+V0032*E2SP( 47)+V0033*E2SP( 48))*CSPPP
      VE32=VE32+(V0011*E2SP( 54)+V0012*E2SP( 55)+V0013*E2SP( 56)+V0021*E2SP( 58)+V0022*E2SP( 59)+V0023*E2SP( 60)+ &
                 V0031*E2SP( 62)+V0032*E2SP( 63)+V0033*E2SP( 64))*CSPPP
      VC23=VC23+(V0001*(E2SP(22)+E2SP(42)+E2SP(62))+V0002*(E2SP(23)+E2SP(43)+E2SP(63))+ &
                 V0003*(E2SP(24)+E2SP(44)+E2SP(64)))*CSPPP
      VC24=VC24+(V0010*(E2SP(22)+E2SP(39)+E2SP(56))+V0020*(E2SP(26)+E2SP(43)+E2SP(60))+ &
                 V0030*(E2SP(30)+E2SP(47)+E2SP(64)))*CSPPP
      VC34=VC34+(V0100*(E2SP(22)+E2SP(27)+E2SP(32))+V0200*(E2SP(38)+E2SP(43)+E2SP(48))+ &
                 V0300*(E2SP(54)+E2SP(59)+E2SP(64)))*CSPPP
      V1011=OPXO*V0011+C1011
      V1012=OPXO*V0012+C1012
      V1013=OPXO*V0013+C1013
      V1021=OPXO*V0021+C1021
      V1022=OPXO*V0022+C1022
      V1023=OPXO*V0023+C1023
      V1031=OPXO*V0031+C1031
      V1032=OPXO*V0032+C1032
      V1033=OPXO*V0033+C1033
      V2011=OPYO*V0011+C2011
      V2012=OPYO*V0012+C2012
      V2013=OPYO*V0013+C2013
      V2021=OPYO*V0021+C2021
      V2022=OPYO*V0022+C2022
      V2023=OPYO*V0023+C2023
      V2031=OPYO*V0031+C2031
      V2032=OPYO*V0032+C2032
      V2033=OPYO*V0033+C2033
      V3011=OPZO*V0011+C3011
      V3012=OPZO*V0012+C3012
      V3013=OPZO*V0013+C3013
      V3021=OPZO*V0021+C3021
      V3022=OPZO*V0022+C3022
      V3023=OPZO*V0023+C3023
      V3031=OPZO*V0031+C3031
      V3032=OPZO*V0032+C3032
      V3033=OPZO*V0033+C3033
      VPSPP=(V1011*E2SP( 70)+V1012*E2SP( 71)+V1013*E2SP( 72)+V1021*E2SP( 74)+V1022*E2SP( 75)+V1023*E2SP( 76)+ &
             V1031*E2SP( 78)+V1032*E2SP( 79)+V1033*E2SP( 80)+V2011*E2SP(134)+V2012*E2SP(135)+V2013*E2SP(136)+ &
             V2021*E2SP(138)+V2022*E2SP(139)+V2023*E2SP(140)+V2031*E2SP(142)+V2032*E2SP(143)+V2033*E2SP(144)+ &
             V3011*E2SP(198)+V3012*E2SP(199)+V3013*E2SP(200)+V3021*E2SP(202)+V3022*E2SP(203)+V3023*E2SP(204)+ &
             V3031*E2SP(206)+V3032*E2SP(207)+V3033*E2SP(208))*CPSPP
      VE00=VE00+VPSPP
      VE14=VE14+(V1010*E2SP( 70)+V1020*E2SP( 74)+V1030*E2SP( 78)+ &
                 V2010*E2SP(134)+V2020*E2SP(138)+V2030*E2SP(142)+ &
                 V3010*E2SP(198)+V3020*E2SP(202)+V3030*E2SP(206))*CPSPP
      VE24=VE24+(V1010*E2SP( 71)+V1020*E2SP( 75)+V1030*E2SP( 79)+ &
                 V2010*E2SP(135)+V2020*E2SP(139)+V2030*E2SP(143)+ &
                 V3010*E2SP(199)+V3020*E2SP(203)+V3030*E2SP(207))*CPSPP
      VE34=VE34+(V1010*E2SP( 72)+V1020*E2SP( 76)+V1030*E2SP( 80)+ &
                 V2010*E2SP(136)+V2020*E2SP(140)+V2030*E2SP(144)+ &
                 V3010*E2SP(200)+V3020*E2SP(204)+V3030*E2SP(208))*CPSPP
      VE13=VE13+(V1001*E2SP( 70)+V1002*E2SP( 71)+V1003*E2SP( 72)+ &
                 V2001*E2SP(134)+V2002*E2SP(135)+V2003*E2SP(136)+ &
                 V3001*E2SP(198)+V3002*E2SP(199)+V3003*E2SP(200))*CPSPP
      VE23=VE23+(V1001*E2SP( 74)+V1002*E2SP( 75)+V1003*E2SP( 76)+ &
                 V2001*E2SP(138)+V2002*E2SP(139)+V2003*E2SP(140)+ &
                 V3001*E2SP(202)+V3002*E2SP(203)+V3003*E2SP(204))*CPSPP
      VE33=VE33+(V1001*E2SP( 78)+V1002*E2SP( 79)+V1003*E2SP( 80)+ &
                 V2001*E2SP(142)+V2002*E2SP(143)+V2003*E2SP(144)+ &
                 V3001*E2SP(206)+V3002*E2SP(207)+V3003*E2SP(208))*CPSPP
      VE11=VE11+(V0011*E2SP( 70)+V0012*E2SP( 71)+V0013*E2SP( 72)+ &
                 V0021*E2SP( 74)+V0022*E2SP( 75)+V0023*E2SP( 76)+ &
                 V0031*E2SP( 78)+V0032*E2SP( 79)+V0033*E2SP( 80))*CPSPP
      VE21=VE21+(V0011*E2SP(134)+V0012*E2SP(135)+V0013*E2SP(136)+ &
                 V0021*E2SP(138)+V0022*E2SP(139)+V0023*E2SP(140)+ &
                 V0031*E2SP(142)+V0032*E2SP(143)+V0033*E2SP(144))*CPSPP
      VE31=VE31+(V0011*E2SP(198)+V0012*E2SP(199)+V0013*E2SP(200)+ &
                 V0021*E2SP(202)+V0022*E2SP(203)+V0023*E2SP(204)+ &
                 V0031*E2SP(206)+V0032*E2SP(207)+V0033*E2SP(208))*CPSPP
      VC13=VC13+(V0001*(E2SP( 70)+E2SP(138)+E2SP(206))+ &
                 V0002*(E2SP( 71)+E2SP(139)+E2SP(207))+ &
                 V0003*(E2SP( 72)+E2SP(140)+E2SP(208)))*CPSPP
      VC14=VC14+(V0010*(E2SP( 70)+E2SP(135)+E2SP(200))+ &
                 V0020*(E2SP( 74)+E2SP(139)+E2SP(204))+ &
                 V0030*(E2SP( 78)+E2SP(143)+E2SP(208)))*CPSPP
      VC34=VC34+(V1000*(E2SP( 70)+E2SP( 75)+E2SP( 80))+ &
                 V2000*(E2SP(134)+E2SP(139)+E2SP(144))+ &
                 V3000*(E2SP(198)+E2SP(203)+E2SP(208)))*CPSPP
      QFQ4=QA1*QA1*QA2*QA2*FQ4
      TEMP=EABCDI*EABCDI
      HFQ3=PT5*QA*FQ3*TEMP
      TFQ3=HFQ3+HFQ3+HFQ3
      SFQ3=TFQ3+TFQ3
      P25FQ2=P25*FQ2*TEMP
      P75FQ2=P25FQ2+P25FQ2+P25FQ2
      TEMP=PQXX*QFQ4
      GXXXX=PQXX*(TEMP-SFQ3)+P75FQ2
      GYXXX=PQXY*(TEMP-TFQ3)
      GZXXX=PQXZ*(TEMP-TFQ3)
      GZYXX=PQYZ*(TEMP-HFQ3)
      GYYXX=PQYY*(TEMP-HFQ3)-PQXX*HFQ3+P25FQ2
      TEMP=PQYY*QFQ4
      GYYYY=PQYY*(TEMP-SFQ3)+P75FQ2
      GXYYY=PQXY*(TEMP-TFQ3)
      GZYYY=PQYZ*(TEMP-TFQ3)
      GZXYY=PQXZ*(TEMP-HFQ3)
      GZZYY=PQZZ*(TEMP-HFQ3)-PQYY*HFQ3+P25FQ2
      TEMP=PQZZ*QFQ4
      GZZZZ=PQZZ*(TEMP-SFQ3)+P75FQ2
      GXZZZ=PQXZ*(TEMP-TFQ3)
      GYZZZ=PQYZ*(TEMP-TFQ3)
      GXYZZ=PQXY*(TEMP-HFQ3)
      GXXZZ=PQXX*(TEMP-HFQ3)-PQZZ*HFQ3+P25FQ2
 1111 VP4=(OPXX*V0011+OPXOX*C1011+OQXX*GXXOO+OQXOX*GXXXO+GXXXX)*E2SP(86)
 1112 VP4=(OPXX*V0012+OPXOX*C1012+OQXY*GXXOO+OQXO*GXXYO+OQOY*GXXXO+GXXXY)*E2SP(87)+VP4
 1113 VP4=(OPXX*V0013+OPXOX*C1013+OQXZ*GXXOO+OQXO*GXXZO+OQOZ*GXXXO+GXXXZ)*E2SP(88)+VP4
 1121 VP4=(OPXX*V0021+OPXOX*C1021+OQYX*GXXOO+OQYO*GXXXO+OQOX*GXXYO+GXXYX)*E2SP(90)+VP4
 1122 VP4=(OPXX*V0022+OPXOX*C1022+OQYY*GXXOO+OQYOY*GXXYO+GXXYY)*E2SP(91)+VP4
 1123 VP4=(OPXX*V0023+OPXOX*C1023+OQYZ*GXXOO+OQYO*GXXZO+OQOZ*GXXYO+GXXYZ)*E2SP(92)+VP4
 1131 VP4=(OPXX*V0031+OPXOX*C1031+OQZX*GXXOO+OQZO*GXXXO+OQOX*GXXZO+GXXZX)*E2SP(94)+VP4
 1132 VP4=(OPXX*V0032+OPXOX*C1032+OQZY*GXXOO+OQZO*GXXYO+OQOY*GXXZO+GXXZY)*E2SP(95)+VP4
 1133 VP4=(OPXX*V0033+OPXOX*C1033+OQZZ*GXXOO+OQZOZ*GXXZO+GXXZZ)*E2SP(96)+VP4
 1211 VP4=(OPXY*V0011+OPXO*C2011+OPOY*C1011+OQXX*GXYOO+OQXOX*GXYXO+GXYXX)*E2SP(102)+VP4
 1212 VP4=(OPXY*V0012+OPXO*C2012+OPOY*C1012+OQXY*GXYOO+OQXO*GXYYO+OQOY*GXYXO+GXYXY)*E2SP(103)+VP4
 1213 VP4=(OPXY*V0013+OPXO*C2013+OPOY*C1013+OQXZ*GXYOO+OQXO*GXYZO+OQOZ*GXYXO+GXYXZ)*E2SP(104)+VP4
 1221 VP4=(OPXY*V0021+OPXO*C2021+OPOY*C1021+OQYX*GXYOO+OQYO*GXYXO+OQOX*GXYYO+GXYYX)*E2SP(106)+VP4
 1222 VP4=(OPXY*V0022+OPXO*C2022+OPOY*C1022+OQYY*GXYOO+OQYOY*GXYYO+GXYYY)*E2SP(107)+VP4
 1223 VP4=(OPXY*V0023+OPXO*C2023+OPOY*C1023+OQYZ*GXYOO+OQYO*GXYZO+OQOZ*GXYYO+GXYYZ)*E2SP(108)+VP4
 1231 VP4=(OPXY*V0031+OPXO*C2031+OPOY*C1031+OQZX*GXYOO+OQZO*GXYXO+OQOX*GXYZO+GXYZX)*E2SP(110)+VP4
 1232 VP4=(OPXY*V0032+OPXO*C2032+OPOY*C1032+OQZY*GXYOO+OQZO*GXYYO+OQOY*GXYZO+GXYZY)*E2SP(111)+VP4
 1233 VP4=(OPXY*V0033+OPXO*C2033+OPOY*C1033+OQZZ*GXYOO+OQZOZ*GXYZO+GXYZZ)*E2SP(112)+VP4
 1311 VP4=(OPXZ*V0011+OPXO*C3011+OPOZ*C1011+OQXX*GXZOO+OQXOX*GXZXO+GXZXX)*E2SP(118)+VP4
 1312 VP4=(OPXZ*V0012+OPXO*C3012+OPOZ*C1012+OQXY*GXZOO+OQXO*GXZYO+OQOY*GXZXO+GXZXY)*E2SP(119)+VP4
 1313 VP4=(OPXZ*V0013+OPXO*C3013+OPOZ*C1013+OQXZ*GXZOO+OQXO*GXZZO+OQOZ*GXZXO+GXZXZ)*E2SP(120)+VP4
 1321 VP4=(OPXZ*V0021+OPXO*C3021+OPOZ*C1021+OQYX*GXZOO+OQYO*GXZXO+OQOX*GXZYO+GXZYX)*E2SP(122)+VP4
 1322 VP4=(OPXZ*V0022+OPXO*C3022+OPOZ*C1022+OQYY*GXZOO+OQYOY*GXZYO+GXZYY)*E2SP(123)+VP4
 1323 VP4=(OPXZ*V0023+OPXO*C3023+OPOZ*C1023+OQYZ*GXZOO+OQYO*GXZZO+OQOZ*GXZYO+GXZYZ)*E2SP(124)+VP4
 1331 VP4=(OPXZ*V0031+OPXO*C3031+OPOZ*C1031+OQZX*GXZOO+OQZO*GXZXO+OQOX*GXZZO+GXZZX)*E2SP(126)+VP4
 1332 VP4=(OPXZ*V0032+OPXO*C3032+OPOZ*C1032+OQZY*GXZOO+OQZO*GXZYO+OQOY*GXZZO+GXZZY)*E2SP(127)+VP4
 1333 VP4=(OPXZ*V0033+OPXO*C3033+OPOZ*C1033+OQZZ*GXZOO+OQZOZ*GXZZO+GXZZZ)*E2SP(128)+VP4
 2111 VP4=(OPYX*V0011+OPYO*C1011+OPOX*C2011+OQXX*GYXOO+OQXOX*GYXXO+GYXXX)*E2SP(150)+VP4
 2112 VP4=(OPYX*V0012+OPYO*C1012+OPOX*C2012+OQXY*GYXOO+OQXO*GYXYO+OQOY*GYXXO+GYXXY)*E2SP(151)+VP4
 2113 VP4=(OPYX*V0013+OPYO*C1013+OPOX*C2013+OQXZ*GYXOO+OQXO*GYXZO+OQOZ*GYXXO+GYXXZ)*E2SP(152)+VP4
 2121 VP4=(OPYX*V0021+OPYO*C1021+OPOX*C2021+OQYX*GYXOO+OQYO*GYXXO+OQOX*GYXYO+GYXYX)*E2SP(154)+VP4
 2122 VP4=(OPYX*V0022+OPYO*C1022+OPOX*C2022+OQYY*GYXOO+OQYOY*GYXYO+GYXYY)*E2SP(155)+VP4
 2123 VP4=(OPYX*V0023+OPYO*C1023+OPOX*C2023+OQYZ*GYXOO+OQYO*GYXZO+OQOZ*GYXYO+GYXYZ)*E2SP(156)+VP4
 2131 VP4=(OPYX*V0031+OPYO*C1031+OPOX*C2031+OQZX*GYXOO+OQZO*GYXXO+OQOX*GYXZO+GYXZX)*E2SP(158)+VP4
 2132 VP4=(OPYX*V0032+OPYO*C1032+OPOX*C2032+OQZY*GYXOO+OQZO*GYXYO+OQOY*GYXZO+GYXZY)*E2SP(159)+VP4
 2133 VP4=(OPYX*V0033+OPYO*C1033+OPOX*C2033+OQZZ*GYXOO+OQZOZ*GYXZO+GYXZZ)*E2SP(160)+VP4
 2211 VP4=(OPYY*V0011+OPYOY*C2011+OQXX*GYYOO+OQXOX*GYYXO+GYYXX)*E2SP(166)+VP4
 2212 VP4=(OPYY*V0012+OPYOY*C2012+OQXY*GYYOO+OQXO*GYYYO+OQOY*GYYXO+GYYXY)*E2SP(167)+VP4
 2213 VP4=(OPYY*V0013+OPYOY*C2013+OQXZ*GYYOO+OQXO*GYYZO+OQOZ*GYYXO+GYYXZ)*E2SP(168)+VP4
 2221 VP4=(OPYY*V0021+OPYOY*C2021+OQYX*GYYOO+OQYO*GYYXO+OQOX*GYYYO+GYYYX)*E2SP(170)+VP4
 2222 VP4=(OPYY*V0022+OPYOY*C2022+OQYY*GYYOO+OQYOY*GYYYO+GYYYY)*E2SP(171)+VP4
 2223 VP4=(OPYY*V0023+OPYOY*C2023+OQYZ*GYYOO+OQYO*GYYZO+OQOZ*GYYYO+GYYYZ)*E2SP(172)+VP4
 2231 VP4=(OPYY*V0031+OPYOY*C2031+OQZX*GYYOO+OQZO*GYYXO+OQOX*GYYZO+GYYZX)*E2SP(174)+VP4
 2232 VP4=(OPYY*V0032+OPYOY*C2032+OQZY*GYYOO+OQZO*GYYYO+OQOY*GYYZO+GYYZY)*E2SP(175)+VP4
 2233 VP4=(OPYY*V0033+OPYOY*C2033+OQZZ*GYYOO+OQZOZ*GYYZO+GYYZZ)*E2SP(176)+VP4
 2311 VP4=(OPYZ*V0011+OPYO*C3011+OPOZ*C2011+OQXX*GYZOO+OQXOX*GYZXO+GYZXX)*E2SP(182)+VP4
 2312 VP4=(OPYZ*V0012+OPYO*C3012+OPOZ*C2012+OQXY*GYZOO+ &
           OQXO*GYZYO+OQOY*GYZXO+GYZXY)*E2SP(183)+VP4
 2313 VP4=(OPYZ*V0013+OPYO*C3013+OPOZ*C2013+OQXZ*GYZOO+ &
           OQXO*GYZZO+OQOZ*GYZXO+GYZXZ)*E2SP(184)+VP4
 2321 VP4=(OPYZ*V0021+OPYO*C3021+OPOZ*C2021+OQYX*GYZOO+ &
           OQYO*GYZXO+OQOX*GYZYO+GYZYX)*E2SP(186)+VP4
 2322 VP4=(OPYZ*V0022+OPYO*C3022+OPOZ*C2022+OQYY*GYZOO+ &
           OQYOY*GYZYO+GYZYY)*E2SP(187)+VP4
 2323 VP4=(OPYZ*V0023+OPYO*C3023+OPOZ*C2023+OQYZ*GYZOO+ &
           OQYO*GYZZO+OQOZ*GYZYO+GYZYZ)*E2SP(188)+VP4
 2331 VP4=(OPYZ*V0031+OPYO*C3031+OPOZ*C2031+OQZX*GYZOO+ &
           OQZO*GYZXO+OQOX*GYZZO+GYZZX)*E2SP(190)+VP4
 2332 VP4=(OPYZ*V0032+OPYO*C3032+OPOZ*C2032+OQZY*GYZOO+ &
           OQZO*GYZYO+OQOY*GYZZO+GYZZY)*E2SP(191)+VP4
 2333 VP4=(OPYZ*V0033+OPYO*C3033+OPOZ*C2033+OQZZ*GYZOO+ &
           OQZOZ*GYZZO+GYZZZ)*E2SP(192)+VP4
 3111 VP4=(OPZX*V0011+OPZO*C1011+OPOX*C3011+OQXX*GZXOO+ &
           OQXOX*GZXXO+GZXXX)*E2SP(214)+VP4
 3112 VP4=(OPZX*V0012+OPZO*C1012+OPOX*C3012+OQXY*GZXOO+ &
           OQXO*GZXYO+OQOY*GZXXO+GZXXY)*E2SP(215)+VP4
 3113 VP4=(OPZX*V0013+OPZO*C1013+OPOX*C3013+OQXZ*GZXOO+ &
           OQXO*GZXZO+OQOZ*GZXXO+GZXXZ)*E2SP(216)+VP4
 3121 VP4=(OPZX*V0021+OPZO*C1021+OPOX*C3021+OQYX*GZXOO+ &
           OQYO*GZXXO+OQOX*GZXYO+GZXYX)*E2SP(218)+VP4
 3122 VP4=(OPZX*V0022+OPZO*C1022+OPOX*C3022+OQYY*GZXOO+ &
           OQYOY*GZXYO+GZXYY)*E2SP(219)+VP4
 3123 VP4=(OPZX*V0023+OPZO*C1023+OPOX*C3023+OQYZ*GZXOO+ &
           OQYO*GZXZO+OQOZ*GZXYO+GZXYZ)*E2SP(220)+VP4
 3131 VP4=(OPZX*V0031+OPZO*C1031+OPOX*C3031+OQZX*GZXOO+ &
           OQZO*GZXXO+OQOX*GZXZO+GZXZX)*E2SP(222)+VP4
 3132 VP4=(OPZX*V0032+OPZO*C1032+OPOX*C3032+OQZY*GZXOO+ &
           OQZO*GZXYO+OQOY*GZXZO+GZXZY)*E2SP(223)+VP4
 3133 VP4=(OPZX*V0033+OPZO*C1033+OPOX*C3033+OQZZ*GZXOO+ &
           OQZOZ*GZXZO+GZXZZ)*E2SP(224)+VP4
 3211 VP4=(OPZY*V0011+OPZO*C2011+OPOY*C3011+OQXX*GZYOO+ &
           OQXOX*GZYXO+GZYXX)*E2SP(230)+VP4
 3212 VP4=(OPZY*V0012+OPZO*C2012+OPOY*C3012+OQXY*GZYOO+ &
           OQXO*GZYYO+OQOY*GZYXO+GZYXY)*E2SP(231)+VP4
 3213 VP4=(OPZY*V0013+OPZO*C2013+OPOY*C3013+OQXZ*GZYOO+ &
           OQXO*GZYZO+OQOZ*GZYXO+GZYXZ)*E2SP(232)+VP4
 3221 VP4=(OPZY*V0021+OPZO*C2021+OPOY*C3021+OQYX*GZYOO+ &
           OQYO*GZYXO+OQOX*GZYYO+GZYYX)*E2SP(234)+VP4
 3222 VP4=(OPZY*V0022+OPZO*C2022+OPOY*C3022+OQYY*GZYOO+ &
           OQYOY*GZYYO+GZYYY)*E2SP(235)+VP4
 3223 VP4=(OPZY*V0023+OPZO*C2023+OPOY*C3023+OQYZ*GZYOO+ &
           OQYO*GZYZO+OQOZ*GZYYO+GZYYZ)*E2SP(236)+VP4
 3231 VP4=(OPZY*V0031+OPZO*C2031+OPOY*C3031+OQZX*GZYOO+ &
           OQZO*GZYXO+OQOX*GZYZO+GZYZX)*E2SP(238)+VP4
 3232 VP4=(OPZY*V0032+OPZO*C2032+OPOY*C3032+OQZY*GZYOO+ &
           OQZO*GZYYO+OQOY*GZYZO+GZYZY)*E2SP(239)+VP4
 3233 VP4=(OPZY*V0033+OPZO*C2033+OPOY*C3033+OQZZ*GZYOO+ &
           OQZOZ*GZYZO+GZYZZ)*E2SP(240)+VP4
 3311 VP4=(OPZZ*V0011+OPZOZ*C3011+OQXX*GZZOO+ &
           OQXOX*GZZXO+GZZXX)*E2SP(246)+VP4
 3312 VP4=(OPZZ*V0012+OPZOZ*C3012+OQXY*GZZOO+ &
           OQXO*GZZYO+OQOY*GZZXO+GZZXY)*E2SP(247)+VP4
 3313 VP4=(OPZZ*V0013+OPZOZ*C3013+OQXZ*GZZOO+ &
           OQXO*GZZZO+OQOZ*GZZXO+GZZXZ)*E2SP(248)+VP4
 3321 VP4=(OPZZ*V0021+OPZOZ*C3021+OQYX*GZZOO+ &
           OQYO*GZZXO+OQOX*GZZYO+GZZYX)*E2SP(250)+VP4
 3322 VP4=(OPZZ*V0022+OPZOZ*C3022+OQYY*GZZOO+ &
           OQYOY*GZZYO+GZZYY)*E2SP(251)+VP4
 3323 VP4=(OPZZ*V0023+OPZOZ*C3023+OQYZ*GZZOO+ &
           OQYO*GZZZO+OQOZ*GZZYO+GZZYZ)*E2SP(252)+VP4
 3331 VP4=(OPZZ*V0031+OPZOZ*C3031+OQZX*GZZOO+ &
           OQZO*GZZXO+OQOX*GZZZO+GZZZX)*E2SP(254)+VP4
 3332 VP4=(OPZZ*V0032+OPZOZ*C3032+OQZY*GZZOO+ &
           OQZO*GZZYO+OQOY*GZZZO+GZZZY)*E2SP(255)+VP4
 3333 VP4=(OPZZ*V0033+OPZOZ*C3033+OQZZ*GZZOO+ &
           OQZOZ*GZZZO+GZZZZ)*E2SP(256)+VP4
      VPPPP=VP4*CPPPP
      VE00=VE00+VPPPP
      VE14=VE14+(V1110*E2SP( 86)+V1120*E2SP( 90)+V1130*E2SP( 94)+ &
                 V1210*E2SP(102)+V1220*E2SP(106)+V1230*E2SP(110)+ &
                 V1310*E2SP(118)+V1320*E2SP(122)+V1330*E2SP(126)+ &
                 V2110*E2SP(150)+V2120*E2SP(154)+V2130*E2SP(158)+ &
                 V2210*E2SP(166)+V2220*E2SP(170)+V2230*E2SP(174)+ &
                 V2310*E2SP(182)+V2320*E2SP(186)+V2330*E2SP(190)+ &
                 V3110*E2SP(214)+V3120*E2SP(218)+V3130*E2SP(222)+ &
                 V3210*E2SP(230)+V3220*E2SP(234)+V3230*E2SP(238)+ &
                 V3310*E2SP(246)+V3320*E2SP(250)+V3330*E2SP(254))*CPPPP
      VE24=VE24+(V1110*E2SP( 87)+V1120*E2SP( 91)+V1130*E2SP( 95)+ &
                 V1210*E2SP(103)+V1220*E2SP(107)+V1230*E2SP(111)+ &
                 V1310*E2SP(119)+V1320*E2SP(123)+V1330*E2SP(127)+ &
                 V2110*E2SP(151)+V2120*E2SP(155)+V2130*E2SP(159)+ &
                 V2210*E2SP(167)+V2220*E2SP(171)+V2230*E2SP(175)+ &
                 V2310*E2SP(183)+V2320*E2SP(187)+V2330*E2SP(191)+ &
                 V3110*E2SP(215)+V3120*E2SP(219)+V3130*E2SP(223)+ &
                 V3210*E2SP(231)+V3220*E2SP(235)+V3230*E2SP(239)+ &
                 V3310*E2SP(247)+V3320*E2SP(251)+V3330*E2SP(255))*CPPPP
      VE34=VE34+(V1110*E2SP( 88)+V1120*E2SP( 92)+V1130*E2SP( 96)+ &
                 V1210*E2SP(104)+V1220*E2SP(108)+V1230*E2SP(112)+ &
                 V1310*E2SP(120)+V1320*E2SP(124)+V1330*E2SP(128)+ &
                 V2110*E2SP(152)+V2120*E2SP(156)+V2130*E2SP(160)+ &
                 V2210*E2SP(168)+V2220*E2SP(172)+V2230*E2SP(176)+ &
                 V2310*E2SP(184)+V2320*E2SP(188)+V2330*E2SP(192)+ &
                 V3110*E2SP(216)+V3120*E2SP(220)+V3130*E2SP(224)+ &
                 V3210*E2SP(232)+V3220*E2SP(236)+V3230*E2SP(240)+ &
                 V3310*E2SP(248)+V3320*E2SP(252)+V3330*E2SP(256))*CPPPP
      VE13=VE13+(V1101*E2SP( 86)+V1102*E2SP( 87)+V1103*E2SP( 88)+ &
                 V1201*E2SP(102)+V1202*E2SP(103)+V1203*E2SP(104)+ &
                 V1301*E2SP(118)+V1302*E2SP(119)+V1303*E2SP(120)+ &
                 V2101*E2SP(150)+V2102*E2SP(151)+V2103*E2SP(152)+ &
                 V2201*E2SP(166)+V2202*E2SP(167)+V2203*E2SP(168)+ &
                 V2301*E2SP(182)+V2302*E2SP(183)+V2303*E2SP(184)+ &
                 V3101*E2SP(214)+V3102*E2SP(215)+V3103*E2SP(216)+ &
                 V3201*E2SP(230)+V3202*E2SP(231)+V3203*E2SP(232)+ &
                 V3301*E2SP(246)+V3302*E2SP(247)+V3303*E2SP(248))*CPPPP
      VE23=VE23+(V1101*E2SP( 90)+V1102*E2SP( 91)+V1103*E2SP( 92)+ &
                 V1201*E2SP(106)+V1202*E2SP(107)+V1203*E2SP(108)+ &
                 V1301*E2SP(122)+V1302*E2SP(123)+V1303*E2SP(124)+ &
                 V2101*E2SP(154)+V2102*E2SP(155)+V2103*E2SP(156)+ &
                 V2201*E2SP(170)+V2202*E2SP(171)+V2203*E2SP(172)+ &
                 V2301*E2SP(186)+V2302*E2SP(187)+V2303*E2SP(188)+ &
                 V3101*E2SP(218)+V3102*E2SP(219)+V3103*E2SP(220)+ &
                 V3201*E2SP(234)+V3202*E2SP(235)+V3203*E2SP(236)+ &
                 V3301*E2SP(250)+V3302*E2SP(251)+V3303*E2SP(252))*CPPPP
      VE33=VE33+(V1101*E2SP( 94)+V1102*E2SP( 95)+V1103*E2SP( 96)+ &
                 V1201*E2SP(110)+V1202*E2SP(111)+V1203*E2SP(112)+ &
                 V1301*E2SP(126)+V1302*E2SP(127)+V1303*E2SP(128)+ &
                 V2101*E2SP(158)+V2102*E2SP(159)+V2103*E2SP(160)+ &
                 V2201*E2SP(174)+V2202*E2SP(175)+V2203*E2SP(176)+ &
                 V2301*E2SP(190)+V2302*E2SP(191)+V2303*E2SP(192)+ &
                 V3101*E2SP(222)+V3102*E2SP(223)+V3103*E2SP(224)+ &
                 V3201*E2SP(238)+V3202*E2SP(239)+V3203*E2SP(240)+ &
                 V3301*E2SP(254)+V3302*E2SP(255)+V3303*E2SP(256))*CPPPP
      VE12=VE12+(V1011*E2SP( 86)+V1012*E2SP( 87)+V1013*E2SP( 88)+ &
                 V1021*E2SP( 90)+V1022*E2SP( 91)+V1023*E2SP( 92)+ &
                 V1031*E2SP( 94)+V1032*E2SP( 95)+V1033*E2SP( 96)+ &
                 V2011*E2SP(150)+V2012*E2SP(151)+V2013*E2SP(152)+ &
                 V2021*E2SP(154)+V2022*E2SP(155)+V2023*E2SP(156)+ &
                 V2031*E2SP(158)+V2032*E2SP(159)+V2033*E2SP(160)+ &
                 V3011*E2SP(214)+V3012*E2SP(215)+V3013*E2SP(216)+ &
                 V3021*E2SP(218)+V3022*E2SP(219)+V3023*E2SP(220)+ &
                 V3031*E2SP(222)+V3032*E2SP(223)+V3033*E2SP(224))*CPPPP
      VE22=VE22+(V1011*E2SP(102)+V1012*E2SP(103)+V1013*E2SP(104)+ &
                 V1021*E2SP(106)+V1022*E2SP(107)+V1023*E2SP(108)+ &
                 V1031*E2SP(110)+V1032*E2SP(111)+V1033*E2SP(112)+ &
                 V2011*E2SP(166)+V2012*E2SP(167)+V2013*E2SP(168)+ &
                 V2021*E2SP(170)+V2022*E2SP(171)+V2023*E2SP(172)+ &
                 V2031*E2SP(174)+V2032*E2SP(175)+V2033*E2SP(176)+ &
                 V3011*E2SP(230)+V3012*E2SP(231)+V3013*E2SP(232)+ &
                 V3021*E2SP(234)+V3022*E2SP(235)+V3023*E2SP(236)+ &
                 V3031*E2SP(238)+V3032*E2SP(239)+V3033*E2SP(240))*CPPPP
      VE32=VE32+(V1011*E2SP(118)+V1012*E2SP(119)+V1013*E2SP(120)+ &
                 V1021*E2SP(122)+V1022*E2SP(123)+V1023*E2SP(124)+ &
                 V1031*E2SP(126)+V1032*E2SP(127)+V1033*E2SP(128)+ &
                 V2011*E2SP(182)+V2012*E2SP(183)+V2013*E2SP(184)+ &
                 V2021*E2SP(186)+V2022*E2SP(187)+V2023*E2SP(188)+ &
                 V2031*E2SP(190)+V2032*E2SP(191)+V2033*E2SP(192)+ &
                 V3011*E2SP(246)+V3012*E2SP(247)+V3013*E2SP(248)+ &
                 V3021*E2SP(250)+V3022*E2SP(251)+V3023*E2SP(252)+ &
                 V3031*E2SP(254)+V3032*E2SP(255)+V3033*E2SP(256))*CPPPP
      VE11=VE11+(V0111*E2SP( 86)+V0112*E2SP( 87)+V0113*E2SP( 88)+ &
                 V0121*E2SP( 90)+V0122*E2SP( 91)+V0123*E2SP( 92)+ &
                 V0131*E2SP( 94)+V0132*E2SP( 95)+V0133*E2SP( 96)+ &
                 V0211*E2SP(102)+V0212*E2SP(103)+V0213*E2SP(104)+ &
                 V0221*E2SP(106)+V0222*E2SP(107)+V0223*E2SP(108)+ &
                 V0231*E2SP(110)+V0232*E2SP(111)+V0233*E2SP(112)+ &
                 V0311*E2SP(118)+V0312*E2SP(119)+V0313*E2SP(120)+ &
                 V0321*E2SP(122)+V0322*E2SP(123)+V0323*E2SP(124)+ &
                 V0331*E2SP(126)+V0332*E2SP(127)+V0333*E2SP(128))*CPPPP
      VE21=VE21+(V0111*E2SP(150)+V0112*E2SP(151)+V0113*E2SP(152)+ &
                 V0121*E2SP(154)+V0122*E2SP(155)+V0123*E2SP(156)+ &
                 V0131*E2SP(158)+V0132*E2SP(159)+V0133*E2SP(160)+ &
                 V0211*E2SP(166)+V0212*E2SP(167)+V0213*E2SP(168)+ &
                 V0221*E2SP(170)+V0222*E2SP(171)+V0223*E2SP(172)+ &
                 V0231*E2SP(174)+V0232*E2SP(175)+V0233*E2SP(176)+ &
                 V0311*E2SP(182)+V0312*E2SP(183)+V0313*E2SP(184)+ &
                 V0321*E2SP(186)+V0322*E2SP(187)+V0323*E2SP(188)+ &
                 V0331*E2SP(190)+V0332*E2SP(191)+V0333*E2SP(192))*CPPPP
      VE31=VE31+(V0111*E2SP(214)+V0112*E2SP(215)+V0113*E2SP(216)+ &
                 V0121*E2SP(218)+V0122*E2SP(219)+V0123*E2SP(220)+ &
                 V0131*E2SP(222)+V0132*E2SP(223)+V0133*E2SP(224)+ &
                 V0211*E2SP(230)+V0212*E2SP(231)+V0213*E2SP(232)+ &
                 V0221*E2SP(234)+V0222*E2SP(235)+V0223*E2SP(236)+ &
                 V0231*E2SP(238)+V0232*E2SP(239)+V0233*E2SP(240)+ &
                 V0311*E2SP(246)+V0312*E2SP(247)+V0313*E2SP(248)+ &
                 V0321*E2SP(250)+V0322*E2SP(251)+V0323*E2SP(252)+ &
                 V0331*E2SP(254)+V0332*E2SP(255)+V0333*E2SP(256))*CPPPP
      VC12=VC12+(V0011*(E2SP( 86)+E2SP(166)+E2SP(246))+ &
                 V0012*(E2SP( 87)+E2SP(167)+E2SP(247))+ &
                 V0013*(E2SP( 88)+E2SP(168)+E2SP(248))+ &
                 V0021*(E2SP( 90)+E2SP(170)+E2SP(250))+ &
                 V0022*(E2SP( 91)+E2SP(171)+E2SP(251))+ &
                 V0023*(E2SP( 92)+E2SP(172)+E2SP(252))+ &
                 V0031*(E2SP( 94)+E2SP(174)+E2SP(254))+ &
                 V0032*(E2SP( 95)+E2SP(175)+E2SP(255))+ &
                 V0033*(E2SP( 96)+E2SP(176)+E2SP(256)))*CPPPP
      VC13=VC13+(V0101*(E2SP( 86)+E2SP(154)+E2SP(222))+ &
                 V0102*(E2SP( 87)+E2SP(155)+E2SP(223))+ &
                 V0103*(E2SP( 88)+E2SP(156)+E2SP(224))+ &
                 V0201*(E2SP(102)+E2SP(170)+E2SP(238))+ &
                 V0202*(E2SP(103)+E2SP(171)+E2SP(239))+ &
                 V0203*(E2SP(104)+E2SP(172)+E2SP(240))+ &
                 V0301*(E2SP(118)+E2SP(186)+E2SP(254))+ &
                 V0302*(E2SP(119)+E2SP(187)+E2SP(255))+ &
                 V0303*(E2SP(120)+E2SP(188)+E2SP(256)))*CPPPP
      VC14=VC14+(V0110*(E2SP( 86)+E2SP(151)+E2SP(216))+ &
                 V0120*(E2SP( 90)+E2SP(155)+E2SP(220))+ &
                 V0130*(E2SP( 94)+E2SP(159)+E2SP(224))+ &
                 V0210*(E2SP(102)+E2SP(167)+E2SP(232))+ &
                 V0220*(E2SP(106)+E2SP(171)+E2SP(236))+ &
                 V0230*(E2SP(110)+E2SP(175)+E2SP(240))+ &
                 V0310*(E2SP(118)+E2SP(183)+E2SP(248))+ &
                 V0320*(E2SP(122)+E2SP(187)+E2SP(252))+ &
                 V0330*(E2SP(126)+E2SP(191)+E2SP(256)))*CPPPP
      VC23=VC23+(V1001*(E2SP( 86)+E2SP(106)+E2SP(126))+ &
                 V1002*(E2SP( 87)+E2SP(107)+E2SP(127))+ &
                 V1003*(E2SP( 88)+E2SP(108)+E2SP(128))+ &
                 V2001*(E2SP(150)+E2SP(170)+E2SP(190))+ &
                 V2002*(E2SP(151)+E2SP(171)+E2SP(191))+ &
                 V2003*(E2SP(152)+E2SP(172)+E2SP(192))+ &
                 V3001*(E2SP(214)+E2SP(234)+E2SP(254))+ &
                 V3002*(E2SP(215)+E2SP(235)+E2SP(255))+ &
                 V3003*(E2SP(216)+E2SP(236)+E2SP(256)))*CPPPP
      VC24=VC24+(V1010*(E2SP( 86)+E2SP(103)+E2SP(120))+ &
                 V1020*(E2SP( 90)+E2SP(107)+E2SP(124))+ &
                 V1030*(E2SP( 94)+E2SP(111)+E2SP(128))+ &
                 V2010*(E2SP(150)+E2SP(167)+E2SP(184))+ &
                 V2020*(E2SP(154)+E2SP(171)+E2SP(188))+ &
                 V2030*(E2SP(158)+E2SP(175)+E2SP(192))+ &
                 V3010*(E2SP(214)+E2SP(231)+E2SP(248))+ &
                 V3020*(E2SP(218)+E2SP(235)+E2SP(252))+ &
                 V3030*(E2SP(222)+E2SP(239)+E2SP(256)))*CPPPP
      VC34=VC34+(V1100*(E2SP( 86)+E2SP( 91)+E2SP( 96))+ &
                 V1200*(E2SP(102)+E2SP(107)+E2SP(112))+ &
                 V1300*(E2SP(118)+E2SP(123)+E2SP(128))+ &
                 V2100*(E2SP(150)+E2SP(155)+E2SP(160))+ &
                 V2200*(E2SP(166)+E2SP(171)+E2SP(176))+ &
                 V2300*(E2SP(182)+E2SP(187)+E2SP(192))+ &
                 V3100*(E2SP(214)+E2SP(219)+E2SP(224))+ &
                 V3200*(E2SP(230)+E2SP(235)+E2SP(240))+ &
                 V3300*(E2SP(246)+E2SP(251)+E2SP(256)))*CPPPP
!
! End of routine FPPPPB
      call PRG_manager ('exit', 'FPPPPB', 'UTILITY')
      return
      END
