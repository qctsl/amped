      subroutine BLD_GUESS_MO_ROHF
!***********************************************************************
!     Date last modified: July 07, 1992                    Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE GSCF_work

      implicit none
!
! Local scalars:
      integer :: NoccMO
      integer LNGTHO
!
! Work arrays:
      integer, dimension(:), allocatable :: IaoCC       !
      integer, dimension(:), allocatable :: IBOCC       !
      character*1, dimension(:), allocatable :: IGRPC   !
!
      integer, dimension(:), allocatable :: NUMA
      integer, dimension(:), allocatable :: NUMB
      integer, dimension(:), allocatable :: NUMC
      integer, dimension(:), allocatable :: NUMD
      integer, dimension(:), allocatable :: NUML
      double precision, dimension(:), allocatable :: OCCA  !
      double precision, dimension(:), allocatable :: OCCB  !
! OCCA,OCCB - Alpha and Beta open shell MO occupancies (NOPEN).
      double precision A(10,10),B(10,10)
!
! Local scalars:
      integer I,Ibasis,IAB,J,Nlevel,TNA,TNB
      character*1 IGI
      character*4 IOPEN
!
! Local arrays:
      integer NMO(3)
      character*1 ICODE(3)
!
      DATA IOPEN/'OPEN'/
      DATA ICODE/'F','A','V'/
!
! Local scalars:
!
! Begin:
      call PRG_manager ('enter', 'BLD_GUESS_MO_ROHF', 'CMO_GUESS%ROHF')
!
      call GET_object ('QM', 'CMO_GUESS', 'RHF')
      if(Nbasis.eq.1)then
        write(UNIout,'(a)')'ERROR: BLD_GUESS_MO_ROHF>'
        call PRG_stop ('ROHF cannot handle the case of a single basis function')
      end if
      call DEF_NoccMO (NoccMO, NAelectrons, NBelectrons)

      allocate (IGRPC(1:Nbasis), IaoCC(1:Nbasis), IBOCC(1:Nbasis))
!
      call ROHF_group
      write(UNIout,1070)(IGRPC(I),I=1,Nbasis)
      write(UNIout,1090)(IaoCC(I),I=1,Nbasis)
      write(UNIout,1140)(IBOCC(I),I=1,Nbasis)
!
      allocate (NUMA(1:Nbasis), NUMB(1:Nbasis), NUMC(1:Nbasis), NUMD(1:Nbasis), NUML(1:Nbasis))
      if(.not.allocated(OCC))then
        allocate (OCC(1:Nbasis))
      else
        deallocate (OCC)
        allocate (OCC(1:Nbasis))
      end if
      if(.not.allocated(OCCA))then
        allocate (OCCA(1:Nbasis), OCCB(1:Nbasis))
      else
        deallocate (OCCA, OCCB)
        allocate (OCCA(1:Nbasis), OCCB(1:Nbasis))
      end if
!
! Initialization.
      OCC(1:Nbasis)=ZERO
      OCCA(1:Nbasis)=ZERO
      OCCB(1:Nbasis)=ZERO
!
! Count Closed shells
      NMO(1:3)=0
!
! Interpret Level data.
      do Ibasis=1,Nbasis
        IGI=IGRPC(Ibasis)
! Assign group numbers 1='F', 2='A', 3='V'.
        select case (IGI)
        case ('F')
          NMO(1)=NMO(1)+1
        case ('A')
          NMO(2)=NMO(2)+1
        case ('V')
          NMO(3)=NMO(3)+1
        case default
        end select
      end do ! Ibasis
  120 NMO(3)=Nbasis-NMO(1)-NMO(2)
      write(UNIout,1020)ICODE,NMO
!
      TNA=0
      TNB=0
      do J=Ncore+1,NAelectrons
        TNA=TNA+IaoCC(J)
        TNB=TNB+IBOCC(J)
      end do ! J
      OCCA(1)=1
      OCCB(1)=1

      if(Nopen.gt.0)then
        do J=1,Nopen
          OCCA(J+1)=DBLE(TNA)/dble(Nopen)
          OCCB(J+1)=DBLE(TNB)/dble(Nopen)
          OCC(J)=PT5*DBLE(TNA+TNB)/dble(Nopen)
        end do ! J

      LNGTHO=(NOPEN+1)*(NOPEN+2)/2
      if(.not.allocated(ALPHCC))then
        allocate (ALPHCC(1:LNGTHO), BETACC(1:LNGTHO))
      else
        deallocate (ALPHCC, BETACC)
        allocate (ALPHCC(1:LNGTHO), BETACC(1:LNGTHO))
      end if

        IAB=0
        do J=1,Nopen+1
        do I=1,J
          IAB=IAB+1
          ALPHCC(IAB)=OCCA(I)*OCCA(J)+OCCA(I)*OCCB(J)+OCCB(I)*OCCA(J)+OCCB(I)*OCCB(J)
          ALPHCC(IAB)=ALPHCC(IAB)/TWO
          BETACC(IAB)=OCCA(I)*OCCA(J)+OCCB(I)*OCCB(J)
          BETACC(IAB)=BETACC(IAB)/TWO
        end do ! I
        end do ! J

      Nlevel=1
      NUML(1)=Nopen
      NUMD(1)=NMO(1)-Ncore
      NUMA(1)=TNA-TNB
      NUMB(1)=0
      end if ! Nopen

      NDOUBL=0
      CMOG%NoccMO=NCORE+Nopen
      CMOG%NFcoreMO=NCORE
      CMOG%occupancy(1:Ncore)=TWO
      NCONF=0
!
! Calculate the coupling program_constants.
     if(Nopen.gt.0)then
      CMOG%occupancy(Ncore+1:CMOG%NoccMO)=TWO*OCC(Nopen)
      IAB=1
      ALPHCC(1)=TWO
      BETACC(1)=ONE
      call AKLBKL (NUML, NUMD, NUMA, NUMB, NUMC, Nlevel, ALPHCC, BETACC, LNGTHO)
! Print Coupling Coefficients.
      IAB=0
      do J=1,NOPEN+1
      do I=1,J
        IAB=IAB+1
        A(I,J)=ALPHCC(IAB)
        A(J,I)=ALPHCC(IAB)
        B(I,J)=BETACC(IAB)
        B(J,I)=BETACC(IAB)
      end do ! I
      end do ! J
      IF(NOPEN.EQ.0)write(UNIout,1150)
      IF(NOPEN.GT.0)write(UNIout,1150)(IOPEN,I,I=1,NOPEN)
      write(UNIout,1200)OCCA(1),OCCB(1),(A(J,1),J=1,NOPEN+1)
      IF(NOPEN.NE.0)then
        do I=2,NOPEN+1
        write(UNIout,1220)I,OCCA(I),OCCB(I),(A(J,I),J=1,NOPEN+1)
        end do
      end if
      IF(NOPEN.EQ.0)write(UNIout,1230)
      IF(NOPEN.GT.0)write(UNIout,1230)(IOPEN,I,I=1,NOPEN)
      write(UNIout,1200)OCCA(1),OCCB(1),(B(J,1),J=1,NOPEN+1)
      IF(NOPEN.NE.0)then
        do I=2,NOPEN+1
        write(UNIout,1220)I,OCCA(I),OCCB(I),(B(J,I),J=1,NOPEN+1)
        end do
      end if
      end if ! Nopen
      write(UNIout,'(/)')
!
      deallocate (NUMA, NUMB, NUMC, NUMD, NUML)
!
! End of routine BLD_GUESS_MO_ROHF
      call PRG_manager ('exit', 'BLD_GUESS_MO_ROHF', 'CMO_GUESS%ROHF')
      return
!
 1020 FORMAT(/'ORBITAL GROUP:   ',3(4X,A4)/'NUMBER OF MO:    ',3I8)
 1070 FORMAT(/'RHF OPEN SHELL SPECIFICATION'/'GROUP     : ',100A1/(7X,100A1))
 1090 FORMAT('ALPHA OCC : ',100I1/(13X,100I1))
 1140 FORMAT('BETA OCC  : ',100I1/(13X,100I1))
 1150 FORMAT(/'COULOMB COUPLING COEFFICIENTS   A(I,J)'/'  MO',10X,'OCCA',6X,'OCCB',9X,'CLOSED',9(3X,A4,I2))
 1200 FORMAT('CLOSED',3X,2F10.4,5X,10F9.4)
 1220 FORMAT('OPEN',I2,3X,2F10.4,5X,10F9.4)
 1230 FORMAT(/'EXCHANGE COUPLING COEFFICIENTS   B(I,J)'/'  MO',10X,'OCCA',6X,'OCCB',9X,'CLOSED',9(3X,A4,I2))
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine ROHF_group
!***********************************************************************
!     Date last modified: May 15, 1995                     Version 1.0 *
!     Author: Y. Wang  and R.A. Poirier                                *
!     Description: Prepare data for the General SCF.                   *
!***********************************************************************

      implicit none
!
! Local scalars:
      integer Ibasis,II,J,MULTIPw,NOCCD,NOCCT
!
! Begin:
      call PRG_manager ('enter', 'ROHF_group', 'UTILITY')
!
      MULTIPw=Multiplicity
!
      IF(MULTIPw.EQ.1)then
        NOCCD=NAelectrons
        NOCCT=NOCCD
      else
        NOCCD=NBelectrons
        NOCCT=NAelectrons
      end if
!
      do Ibasis=1,Nbasis
      IF(Ibasis.LE.NOCCD)then
        IGRPC(Ibasis)='F'
      else if (Ibasis.LE.NOCCT.and.NOCCT.GT.NOCCD) then
        IGRPC(Ibasis)='A'
      else
        IGRPC(Ibasis)='V'
      end if
      end do
!
      IaoCC(1:Nbasis)=0
      IBOCC(1:Nbasis)=0
!
      IaoCC(1:NAelectrons)=1
      IBOCC(1:NBelectrons)=1
!
! End of routine ROHF_group
      call PRG_manager ('exit', 'ROHF_group', 'UTILITY')
      return
      end subroutine ROHF_group
      end subroutine BLD_GUESS_MO_ROHF
      subroutine AKLBKL (NUML, &
                         NUMD, &
                         NUMA, &
                         NUMB, &
                         NUMC, &
                         NLEVEL, &
                         ALP, &
                         BETA, &
                         MAXOP2)
!***********************************************************************
!     Date last modified: May 15, 1995                     Version 1.0 *
!     Author:                                                          *
!     Description:                                                     *
!     Calculate the Coulomb and Exchange coupling program_constants for the    *
!     open shell levels specified.                                     *
!                                                                      *
!     NUML   - Level degeneracies.                                     *
!     NUMD   - Number of doubly occupied MO in each level.             *
!     NUMA   - Number of Alpha Spin singly occupied MO in each level.  *
!     NUMB   - Number of Beta Spin singly occupied MO in each level.   *
!     NUMC   - Number of configurations for each level.                *
!     NLEVEL - Number of levels.                                       *
!     ALP    - Coulomb coupling program_constants.                             *
!     BETA   - Exchange coupling program_constants.                            *
!     MAXOP2 - Dimension of ALP and BETA.                              *
!                                                                      *
!     NUMC, ALP and BETA are calculated by this routine.               *
!     Functions used: ICOMB                                            *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants

      implicit none
!
! Input scalars:
      integer MAXOP2,NLEVEL
!
! Input arrays:
      integer NUMA(NLEVEL),NUMB(NLEVEL),NUMC(NLEVEL),NUMD(NLEVEL),NUML(NLEVEL)
      double precision ALP(MAXOP2),BETA(MAXOP2)
!
! Local scalars:
      integer I,IAB,IAKK,IBKK,II,IOFF,I1,J,J1,K,L,MAA,MAB,MAI,MBB,MBI,MDA,MDB,MDD,MDI,NAI,NAJ,NBI,NBJ,NDGEN,NDI,NDJ,NI,NJ,NLI,NLJ
      double precision AKK,AKL,BKK,BKL,ZMIJN
      character*1 LCONF(2,9),LALPHA,LBETA,LBLANK,LUNDER
!
! Local function:
      integer ICOMB
!
      DATA LALPHA/'A'/,LBETA/'B'/,LBLANK/' '/,LUNDER/'_'/
!
! Begin:
      call PRG_manager ('enter', 'AKLBKL', 'UTILITY')
!
! Check the parameters for each level, and
! get the number of configurations for each level.
      IF(NLEVEL.EQ.0)GO TO 500
!
      write(UNIout,1000)
! NDGEN counts the number of degenerate levels.
      NDGEN=0
!
! Print Level data.
      do II=1,NLEVEL
         I=NLEVEL+1-II
         NLI=NUML(I)
         NDI=NUMD(I)
         NAI=NUMA(I)
         NBI=NUMB(I)
         NI=ICOMB (NLI, NDI, NAI, NBI)
         NUMC(I)=NI
         do J=1,NLI
            LCONF(1,J)=LBLANK
            LCONF(2,J)=LBLANK
         end do
         K=0
         IF(NDI.NE.0)then
            do J=1,NDI
               K=K+1
               LCONF(1,K)=LALPHA
               LCONF(2,K)=LBETA
            end do
         end if
  120    IF(NAI.NE.0)then
            do J=1,NAI
               K=K+1
               LCONF(1,K)=LALPHA
            end do
         end if
  140 IF(NBI.NE.0)then
         do J=1,NBI
            K=K+1
            LCONF(2,K)=LBETA
         end do
      end if
  160 write(UNIout,1010)I,NLI,NDI,NAI,NBI,NI,(LCONF(1,J),LCONF(2,J),J=1,NLI)
      write(UNIout,1020)(LUNDER,LUNDER,J=1,NLI)
      IF(NLI.GT.1)NDGEN=NDGEN+1
      end do !II

      IF(NDGEN.GT.1)write(UNIout,1030)
!
! Loop over Open Shell levels - Level I.
      IOFF=1
      do I=1,NLEVEL
      NLI=NUML(I)
      NDI=NUMD(I)
      NAI=NUMA(I)
      NBI=NUMB(I)
      NI=NUMC(I)
!
! Compute AKK and BKK:
! AKK and BKK are adjusted such that
!        AKKNEW=2*BKKNEW
!        BKKNEW=AKK-BKK
! where ICOMB (L,I,J,K) = L! / (I! J! K! (L-I-J-K)!).
!
      MDI=0
      IF(NDI.GT.0)MDI=ICOMB (NLI-1, NDI-1, NAI, NBI)
      MAI=0
      IF(NAI.GT.0)MAI=ICOMB (NLI-1, NDI, NAI-1, NBI)
      MBI=0
      IF(NBI.GT.0)MBI=ICOMB (NLI-1, NDI, NAI, NBI-1)
      IAKK=2*MDI+MAI+MBI
      IBKK=MDI+MAI+MBI
      IBKK=IAKK-IBKK
      BKK=DBLE(IBKK)/DBLE(NI)
      AKK=BKK+BKK
!
! Compute interaction terms AKL and BKL:
!        AKL=(4*MDD+2*MDA+2*MDB+MAA+MAB+MBB)*MIJ/(2*N)
!        BKL=(2*MDD+MDA+MDB+MAA-MAB+MBB)*MIJ/(2*N)
! where N = Product of all NI.
!
      IF(I.NE.1)then
!
! Loop over different open shell levels J.
!
! Formulae:
!     MDD = ICOMB (NLI-1,NDI-1,NAI,NBI) * ICOMB (NLJ-1,NDJ-1,NAJ,NBJ)
!     MDA = ICOMB (NLI-1,NDI-1,NAI,NBI) * ICOMB (NLJ-1,NDJ,NAJ-1,NBJ)+
!           ICOMB (NLI-1,NDI,NAI-1,NBI) * ICOMB (NLJ-1,NDJ-1,NAJ,NBJ)
!     MDB = ICOMB (NLI-1,NDI-1,NAI,NBI) * ICOMB (NLJ-1,NDJ,NAJ,NBJ-1)+
!           ICOMB (NLI-1,NDI,NAI,NBI-1) * ICOMB (NLJ-1,NDJ-1,NAJ,NBJ)
!     MAA = ICOMB (NLI-1,NDI,NAI-1,NBI) * ICOMB (NLJ-1,NDJ,NAJ-1,NBJ)
!     MAB = ICOMB (NLI-1,NDI,NAI-1,NBI) * ICOMB (NLJ-1,NDJ,NAJ,NBJ-1)+
!           ICOMB (NLI-1,NDI,NAI,NBI-1) * ICOMB (NLJ-1,NDJ,NAJ-1,NBJ)
!     MBB = ICOMB (NLI-1,NDI,NAI,NBI-1) * ICOMB (NLJ-1,NDJ,NAJ,NBJ-1)
!     MIJ = N/(NI*NJ)
!
      I1=I-1
      do J=1,I1
      NLJ=NUML(J)
      NDJ=NUMD(J)
      NAJ=NUMA(J)
      NBJ=NUMB(J)
      NJ=NUMC(J)
      MDD=0
      IF(NDI.GT.0.and.NDJ.GT.0)MDD=ICOMB(NLI-1, NDI-1, NAI, NBI)*ICOMB(NLJ-1, NDJ-1, NAJ, NBJ)
      MDA=0
      IF(NDI.GT.0.and.NAJ.GT.0)MDA=ICOMB(NLI-1, NDI-1, NAI, NBI)*ICOMB(NLJ-1, NDJ, NAJ-1, NBJ)
      IF(NAI.GT.0.and.NDJ.GT.0)MDA=MDA+ICOMB(NLI-1, NDI, NAI-1, NBI)*ICOMB(NLJ-1, NDJ-1, NAJ, NBJ)
      MDB=0
      IF(NDI.GT.0.and.NBJ.GT.0)MDB=ICOMB(NLI-1, NDI-1, NAI, NBI)*ICOMB(NLJ-1, NDJ, NAJ, NBJ-1)
      IF(NBI.GT.0.and.NDJ.GT.0)MDB=MDB+ICOMB(NLI-1, NDI, NAI, NBI-1)*ICOMB(NLJ-1, NDJ-1, NAJ, NBJ)
      MAA=0
      IF(NAI.GT.0.and.NAJ.GT.0)MAA=ICOMB(NLI-1, NDI, NAI-1, NBI)*ICOMB(NLJ-1, NDJ, NAJ-1, NBJ)
      MAB=0
      IF(NAI.GT.0.and.NBJ.GT.0)MAB=ICOMB(NLI-1, NDI, NAI-1, NBI)*ICOMB(NLJ-1, NDJ, NAJ, NBJ-1)
      IF(NBI.GT.0.and.NAJ.GT.0)MAB=MAB+ICOMB(NLI-1, NDI, NAI, NBI-1)*ICOMB(NLJ-1, NDJ, NAJ-1, NBJ)
      MBB=0
      IF(NBI.GT.0.and.NBJ.GT.0)MBB=ICOMB(NLI-1, NDI, NAI, NBI-1)*ICOMB(NLJ-1, NDJ, NAJ, NBJ-1)
      ZMIJN=PT5/DBLE(NI*NJ)
      AKL=DBLE(4*MDD+2*MDA+2*MDB+MAA+MAB+MBB)*ZMIJN
      BKL=DBLE(2*MDD+MDA+MDB+MAA-MAB+MBB)*ZMIJN
!
! Insert Coupling Constants into ALP and BETA.
! Note that the first row and column of ALP and BETA are reserved for the Closed-Open coupling coefficients (not calculated here),
! and that ALP and BETA are Upper Triangular matrices.
      do K=1,NLI
        IAB=IOFF+K
        IAB=(IAB*(IAB-1))/2+1
        IF(J.NE.1)then
          J1=J-1
          do L=1,J1
            IAB=IAB+NUML(L)
          end do !L
       end if

  220   do L=1,NLJ
          IAB=IAB+1
          IF(IAB.GT.MAXOP2)GO TO 900
          ALP(IAB)=AKL
          BETA(IAB)=BKL
        end do !L
      end do !K
      end do !J
      end if !IF(I.NE.1)
!
! Level I with itself.
! Formulae:
!     MDD = ICOMB (NLI-2,NDI-2,NAI,NBI)
!     MDA = 2*ICOMB (NLI-2,NDI-1,NAI-1,NBI)
!     MDB = 2*ICOMB (NLI-2,NDI-1,NAI,NBI-1)
!     MAA = ICOMB (NLI-2,NDI,NAI-2,NBI)
!     MAB = 2*ICOMB (NLI-2,NDI,NAI-1,NBI-1)
!     MBB = ICOMB (NLI-2,NDI,NAI,NBI-2)
!     MIJ = N/NI
!
  300 MDD=0
      IF(NDI.GT.1)MDD=ICOMB (NLI-2, NDI-2, NAI, NBI)
      MDA=0
      IF(NDI.GT.0.and.NAI.GT.0)MDA=2*ICOMB (NLI-2, NDI-1, NAI-1, NBI)
      MDB=0
      IF(NDI.GT.0.and.NBI.GT.0)MDB=2*ICOMB (NLI-2, NDI-1, NAI, NBI-1)
      MAA=0
      IF(NAI.GT.1)MAA=ICOMB (NLI-2, NDI, NAI-2, NBI)
      MAB=0
      IF(NAI.GT.0.and.NBI.GT.0)MAB=2*ICOMB (NLI-2, NDI, NAI-1, NBI-1)
      MBB=0
      IF(NBI.GT.1)MBB=ICOMB (NLI-2, NDI, NAI, NBI-2)
      ZMIJN=PT5/DBLE(NI)
      AKL=DBLE(4*MDD+2*MDA+2*MDB+MAA+MAB+MBB)*ZMIJN
      BKL=DBLE(2*MDD+MDA+MDB+MAA-MAB+MBB)*ZMIJN
      J=I
!
! Insert Coupling Constants into ALP and BETA.
! Note that the first row and column of ALP and BETA are reserved
! for the Closed-Open coupling coefficients (not calculated here),
! and that ALP and BETA are Upper Triangular matrices.
      do K=1,NLI
      IAB=IOFF+K
      IAB=(IAB*(IAB+1))/2-K
      do L=1,K
      IAB=IAB+1
      IF(IAB.GT.MAXOP2)GO TO 900
      IF(K.EQ.L)then
        ALP(IAB)=AKK
        BETA(IAB)=BKK
      else
        ALP(IAB)=AKL
        BETA(IAB)=BKL
      end if
      end do !L
      end do !K
      IOFF=IOFF+NLI
      end do !I
!
  500 continue
!
! End of routine AKLBKL
      call PRG_manager ('exit', 'AKLBKL', 'UTILITY')
      return
!
! Error Exit.
  900 write(UNIout,'(A/16X,A/16X,A,2I3,A,2I3,A,I5,A,I5)') &
       'ERROR> AKLBKL: INDEXING ERROR IN ARRAYS ALP/BETA IN ROUTINE AKLBKL' &
       //'> Send OUTPUT TO MUNgauss AUTHORS'// &
       '> LEVELS (I,J):',I,J,',  MO (K,L):',K,L,',  IAB =',IAB,',  MAXOP2 =',MAXOP2
      GO TO 500
!
 1000 FORMAT(/'0LEVEL    DEGENERACY    DOUBLE OCC    SINGLE ALPHA','    SINGLE BETA    CONFIGURATIONS    SAMPLE CONFIGURATION')
 1010 FORMAT('0',I3,I12,I14,I15,I16,I16,8X,9(2X,2A1))
 1020 FORMAT('+',84X,9(2X,2A1))
 1030 FORMAT(/'WARNING> AKLBKL: INTERACTION TERMS INVOLVING DEGENERATE LEVELS MAY HAVE BEEN OMITTED'/20X, &
              'IN THE CALCULATION OF THE COUPLING COEFFICIENTS FOR THIS STATE')
!
      end subroutine AKLBKL
      integer function ICOMB (L, &
                              I, &
                              J, &
                              K)
!***********************************************************************
!     Date last modified: May 15, 1995                     Version 1.0 *
!     Author:                                                          *
!     Description:                                                     *
!     This function returns the number of combinations of L objects,   *
!     taken in subgroups of size I, J and K:                           *
!     ICOMB (L, I, J, K) = L! / (I! J! K! (L-I-J-K)!)                   *
!***********************************************************************
! Modules:
      USE program_files

      implicit none
!
! Input scalars:
      integer I,J,K,L
!
! Local function:
      integer ifACT
!
! Begin:
      call PRG_manager ('enter', 'ICOMB', 'UTILITY')
!
      if(I.LT.0.or.I.GT.L.OR.J.LT.0.OR.J.GT.L.OR.K.LT.0.OR.K.GT.L.OR.I+J+K.GT.L) then
! Error Exit.
        write(UNIout,'(A/17X,4(A,I4))')'ERROR> ICOMB: function ICOMB: INVALID VALUE(S) FOR', &
              ' L =',L,',  I =',I,',  J =',J,'  OR K =',K
      else
         ICOMB=ifACT(L)/(ifACT(I)*ifACT(J)*ifACT(K)*ifACT(L-I-J-K))
      end if
!
! End of function ICOMB
      call PRG_manager ('exit', 'ICOMB', 'UTILITY')
      return
      end function ICOMB
      integer function ifACT (I)
!***********************************************************************
!     Date last modified: May 15, 1995                     Version 1.0 *
!     Author:                                                          *
!     Description:                                                     *
!     This function returns I! (I factorial), where I is a positive    *
!     integer.  Note that, for I less than or equal to one, ifACT      *
!     is returned as 1.                                                *
!***********************************************************************
!Modules:

      implicit none
!
! Input scalar:
      integer I
!
! Local scalar:
      integer J
!
! Begin:
      call PRG_manager ('enter', 'ifACT', 'UTILITY')
!
      ifACT=1
      if(I.GT.1)then
        do J=2,I
          ifACT=ifACT*J
        end do ! J
      end if
!
! End of function ifACT
      call PRG_manager ('exit', 'ifACT', 'UTILITY')
      return
      end function ifACT
