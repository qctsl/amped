      subroutine MP2CLC
!***********************************************************************
!     Date last modified: June 25, 1992                                *
!     Author: R.A. Poirier                                             *
!     Description: Transform two-electron integrals.                   *
!     First, form  (PJ/KL) = SUM C(P,I)*(IJ/KL), where J, K and L      *
!     extend over all AO, and P over the maximum number of MO.         *
!     Next, form (PQ/KL) followed by (PQ/RL) and then (PQ/RS).         *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE integral_transformation
      USE QM_defaults
      USE QM_objects
      USE matrix_print

      implicit none
!
! Local scalars:
      integer ARBS,ASBR,I,IPASS,Jao,JKLNDX,JKNDX,JNDX,Kao,KL,Lao,M,MO1,MO2,N,PMO,PQINDX,PSINDX,QMO,RMO,RQINDX, &
              RSINDX,SHIFT,SMO,SMOMX
      double precision DEABRS,EARBS,Etotal
      logical PQVVorOO,RSVVorOO
!
! Begin:
      call PRG_manager ('enter', 'MP2CLC', 'UTILITY')
!
      call GET_object ('INT', '2EINT' ,'RAW')
      call GET_object ('QM', 'CMO', 'RHF')
!
! Calculate the total space required for a single pass.
      NJKL=Nbasis*Nbasis*(Nbasis+1)/2
      if(NJKL.GT.MAX_transform)then
        write(UNIout,'(2A)')'MP2CLC> ERROR - TOO MANY MO FOR TRANSFORMATION WITH CURRENT MAX_transform'
        write(UNIout,'(2A)')'MP2CLC> MAX_transform = ',MAX_transform
        write(UNIout,'(2A)')'MP2CLC> MAX_transform must be at least Nbasis*Nbasis*(Nbasis+1)/2'
        STOP
      end if
!
! Get occupied and virtual MOs
      call BLD_CMO_ov
!
! Determine the number of passes required.
      NTMOMX=MIN0 (NTROMO, MAX_transform/NJKL)
      NPasses=(NTROMO-1)/NTMOMX+1
      write(UNIout,1000)NPasses,NTMOMX
!
      allocate (BPQMO(Nbasis,Nbasis), SCRVEC(Nbasis), NCODE(Nbasis), BPQRS(NTMOMX*NJKL))
!
      write(UNIout,1030)
 1030 FORMAT(/38('*')/'*MP2 INTEGRAL TRANSFORMATION PACKAGE*'/38('*')/)
!
! Set indexing arrays.
      allocate (IJNDX(Nbasis+1))
      call BLD_IJNDX (IJNDX, Nbasis)
      MATlen=Nbasis*(Nbasis+1)/2
      do I=1,Nbasis
        NCODE(I)=MATlen*(I-1)
      end do ! I
!
! Initialize MP2 energy.
      ENERGY%MP2=ZERO
      ENERGY%wavefunction='MP2'
!
      SHIFT=0
      do IPASS=1,NPasses
!
      MO1=(IPASS-1)*NTMOMX+1
      MO2=MIN0 (IPASS*NTMOMX, NTROMO)
!
      BPQRS(1:NTMOMX*NJKL)=ZERO
!
      call I2EPMO (CMO_occ%coeff, MO1, MO2, NTROMO)
!
! End of atomic integral input - Start next transformation step for these MO.  Should have all (Pj,kl).
!
      PQVVorOO=.false.
      call I2EQMO (CMO_vrt%coeff, MO1, MO2, NTRVMO, PQVVorOO)
!
! Have all (PQ,kl) for all PQ and all kl.
!
      RSVVorOO=.false.
      call I2RSMO (CMO_occ%coeff, CMO_vrt%coeff, MO1, MO2, NTROMO, NTRVMO, PQVVorOO, RSVVorOO)
!
! Now have all MO integrals (PQ,RS) in BPQRS.
      do PMO=MO1,MO2
!
      do QMO=1,NTRVMO
      PQINDX=(PMO-1)*NTRVMO+QMO
!
      do RMO=1,PMO
!
      SMOMX=NTRVMO
      if(PMO.EQ.RMO)SMOMX=QMO
      do SMO=1,SMOMX

      RSINDX=(RMO-1)*NTRVMO+SMO
      ARBS=PQINDX*(PQINDX-1)/2+RSINDX-SHIFT
      PSINDX=(PMO-1)*NTRVMO+SMO
      RQINDX=(RMO-1)*NTRVMO+QMO

      ASBR=PSINDX*(PSINDX-1)/2+RQINDX-SHIFT

      DEABRS=CMO_occ%eigval(PMO)+CMO_occ%eigval(RMO)-CMO_vrt%eigval(QMO)-CMO_vrt%eigval(SMO)
      if(PMO.NE.RMO)then
        EARBS=BPQRS(ARBS)*(TWO*BPQRS(ARBS)-BPQRS(ASBR))/DEABRS
      else
        EARBS=BPQRS(ARBS)*BPQRS(ARBS)/DEABRS
      end if
      ENERGY%MP2=ENERGY%MP2+EARBS
      if(PMO.NE.RMO.or.QMO.NE.SMO)ENERGY%MP2=ENERGY%MP2+EARBS
      end do ! End SMO loop.
      end do ! End RMO loop.
      end do ! End QMO loop.
      end do ! End PMO loop.

      PQMAX=(MO2-1)*NTRVMO+NTRVMO
      SHIFT=PQMAX*(PQMAX-1)/2+PQMAX
!
      end do ! IPASS
      Etotal=ENERGY%total+ENERGY%MP2
      write(UNIout,'(a,F12.8,a,F20.6)')'MP2 Energy = ',ENERGY%MP2,' Total energy = ',Etotal
!
      deallocate (BPQMO, SCRVEC, NCODE, BPQRS)
      deallocate (IJNDX)
!
 1000 FORMAT('TWO-ELECTRON TRANSFORMATION:'/3X,'NUMBER OF PASSES REQUIRED: ',I4/3X,'MAXIMUM MO PER PASS: ',I4)
 1020 FORMAT(/'TWO-ELECTRON TRANSFORMATION COMPLETE'//'ENERGY FROM MP2 CALCULATION =',F17.6)
!
! End of routine MP2CLC
      call PRG_manager ('exit', 'MP2CLC', 'UTILITY')
      return
      end subroutine MP2CLC
      subroutine BLD_CMO_ov
!***********************************************************************
!     Date last modified: June 25, 1992                                *
!     Author: R.A. Poirier                                             *
!     Description: Split MO coefficients into occupied and virtuals.   *
!***********************************************************************
! Modules:
      USE program_files
      USE integral_transformation
      USE QM_defaults

      implicit none
!
! Local scalars:
      integer I,J,NMOSFC,NMOSFV,NOCCNB,NVRTMO,NVRTNB
!
! Begin:
      call PRG_manager ('enter', 'BLD_CMO_ov', 'UTILITY')
!
      MATlen=Nbasis*(Nbasis+1)/2

      if(MP2_full)then
        NMOSFC=0
        NMOSFV=0
      else
        NFrozen_core=NFrozen_cores()
        NMOSFC=NFrozen_core
        NMOSFV=NFrozen_virt
      end if

      NVRTMO=Nbasis-CMO%NoccMO
      NTRVMO=NVRTMO-NMOSFV
      NTROMO=CMO%NoccMO-NMOSFC
      NVRTNB=NTRVMO*Nbasis
      NOCCNB=NTROMO*Nbasis
!
      write(UNIout,1040)CMO%NoccMO,NVRTMO,NMOSFC,NMOSFV
 1040 FORMAT(/1X,'NO. OF OCCUPIED MO   :',I3,6X,'NO. OF VIRTUAL MO       :',I3,/1X,'NO. OF FROZEN CORE MO:',I3, &
             6X,'NO. OF FROZEN VIRTUAL MO:',I3)
      write(UNIout,1050)NTROMO
 1050 FORMAT(/'0TRANSFORM',I3,' MO'/)
!
      if(.not.associated(CMO_occ%coeff))then
        allocate (CMO_occ%coeff(NTROMO,Nbasis))
      end if
      if(.not.associated(CMO_occ%eigval))then
        allocate (CMO_occ%eigval(NTROMO))
      end if

      do I=1,NTROMO
        CMO_occ%eigval(I)=CMO%eigval(NMOSFC+I)
      do J=1,Nbasis
        CMO_occ%coeff(I,J)=CMO%coeff(J,NMOSFC+I)
      end do ! J
      end do ! I
!
      if(.not.associated(CMO_vrt%coeff))then
        allocate (CMO_vrt%coeff(NTRVMO,Nbasis))
      end if
      if(.not.associated(CMO_vrt%eigval))then
        allocate (CMO_vrt%eigval(NTRVMO))
      end if

      do I=1,NTRVMO
      CMO_vrt%eigval(I)=CMO%eigval(I+CMO%NoccMO)
      do J=1,Nbasis
        CMO_vrt%coeff(I,J)=CMO%coeff(J,I+CMO%NoccMO)
      end do ! J
      end do ! I
!
! End of routine BLD_CMO_ov
      call PRG_manager ('exit', 'BLD_CMO_ov', 'UTILITY')
      return
      end subroutine BLD_CMO_ov
