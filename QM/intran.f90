      subroutine I2E_transform
!************************************************************************************************************
!     Date last modified: April 1, 2015                                                                     *
!     Author: R. A. Poirier                                                                                 *
!     Description: Transform two-electron integrals.                                                        *
!     First, form (PJ/KL)=SUM C(P,I)*(IJ/KL), where J,K and L are over all AO, and P over maximum no of MO. *
!     Next, form (PQ/KL) followed by (PQ/RL) and then (PQ/RS).                                              *
!     NOTE: The two-electron transformation uses CMOS = subset of CMO and ithe transpose of CMO             *
!     NOTE: The chemists notation is used.                                                                  *
!************************************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE QM_objects
      USE integral_transformation

      implicit none
!
! Local scalars:
      integer ARBS,I,IMO,IPASS,IPQRS,IPRQS,Jao,JKLNDX,JKNDX,JNDX,Kao,KL,Lao,M,MOINDX,MO1,MO2,N,PMO,PQINDX, &
              PRINDX,QMO,QMOMAX,RMO,RSINDX,QSINDX,SHIFT,SMO,SMOMAX
      double precision :: Jab,Kab,Jaa_tot,Jab_tot,J_tot,K_tot,T_tot,Vee
      double precision :: Xaa_tot,Xab_tot,X_tot,Xab,Taa,Vaa,Vne_tot,Term,E_tot
      double precision, dimension(:), allocatable :: Jaa
      logical PQVVorOO,RSVVorOO
      logical :: LPeqQ,LReqQ,LReqS,LPeqR,LQeqS
      double precision, dimension(1:CMO%NoccMO,1:CMO%NoccMO) :: occ_Jab
      double precision, dimension(1:CMO%NoccMO,1:CMO%NoccMO) :: occ_Kab
!
! Begin:
      call PRG_manager ('enter', 'I2E_transform', 'UTILITY')
!
      call get_object ('INT', '1EINT', 'MO')

      allocate (BPQMO(Nbasis,Nbasis), SCRVEC(Nbasis), NCODE(Nbasis), BPQRS(NTMOMX*NJKL))
      allocate (Jaa(NTTRMO))
!
      call occupancy_JKab (occ_Jab, Occ_Kab)
!
      allocate (IJNDX(Nbasis+1))
      call BLD_IJNDX (IJNDX, Nbasis)
      PQVVorOO=.TRUE.
      RSVVorOO=.TRUE.
! Set indexing arrays.
      do I=1,Nbasis
        NCODE(I)=MATlen*(I-1)
      end do ! I
!
      Jab_tot=ZERO
      K_tot=ZERO
      Xab_tot=ZERO
      SHIFT=0
      NPQRS=0
      do IPASS=1,Npasses
      MO1=(IPASS-1)*NTMOMX+1
      MO2=MIN0(IPASS*NTMOMX, NTTRMO)
!
      BPQRS(1:NTMOMX*NJKL)=ZERO
!
      call I2EPMO (CMOS, MO1, MO2, NTTRMO)
!
! End of atomic integral input - Start next transformation step
! for these MO.  Should have all (Pj,kl).
      call I2EQMO (CMOS, MO1, MO2, NTTRMO, PQVVorOO)
!
! Have all (PQ,kl) for all PQ and all kl.
      call I2RSMO (CMOS, CMOS, MO1, MO2, NTTRMO, NTTRMO, PQVVorOO, RSVVorOO)
!
      if(.not.allocated(PQRS_MOINT))then
        allocate (PQRS_MOINT(PQRSMX))
      end if

! Now have all MO integrals (PQ,RS) in BPQRS.
      write(UNIout,'(5x,a)')'ab       Jab          Kab          Xab'
      do PMO=MO1,MO2
      if(PQVVorOO)then
        QMOMAX=PMO
      else
        QMOMAX=NTTRMO
      end if
      do QMO=1,QMOMAX
      LPeqQ=.false.
      if(PMO.eq.QMO)LPeqQ=.true.
      do RMO=1,PMO
      LReqQ=.false.
      if(RMO.eq.QMO)LReqQ=.true.
      LPeqR=.false.
      if(PMO.eq.RMO)LPeqR=.true.
      SMOMAX=RMO
      if(PMO.EQ.RMO)SMOMAX=QMO
      do SMO=1,SMOMAX
      LReqS=.false.
      if(RMO.eq.SMO)LReqS=.true.
      LQeqS=.false.
      if(QMO.eq.SMO)LQeqS=.true.
      PQINDX=IJNDX(PMO)+QMO
      RSINDX=IJNDX(RMO)+SMO
      PRINDX=IJNDX(PMO)+RMO
      if(QMO.GT.SMO)then
      QSINDX=IJNDX(QMO)+SMO
      else
      QSINDX=IJNDX(SMO)+QMO
      end if
      IPQRS=PQINDX*(PQINDX-1)/2+RSINDX-SHIFT
      IPRQS=PRINDX*(PRINDX-1)/2+QSINDX-SHIFT
      if(DABS(BPQRS(IPQRS)).GT.1.0D-06.or.DABS(BPQRS(IPRQS)).GT.1.0D-06)then
        NPQRS=NPQRS+1
        if(QMO.ne.RMO)then
          PQRS_MOINT(NPQRS)%ijkl=BPQRS(IPQRS)
          PQRS_MOINT(NPQRS)%ikjl=BPQRS(IPRQS)
          BPQRS(IPQRS)=ZERO
          BPQRS(IPRQS)=ZERO
        else
          PQRS_MOINT(NPQRS)%ijkl=BPQRS(IPQRS)
          PQRS_MOINT(NPQRS)%ikjl=ZERO
          BPQRS(IPQRS)=ZERO
        end if
        PQRS_MOINT(NPQRS)%I=PMO
        PQRS_MOINT(NPQRS)%J=QMO
        PQRS_MOINT(NPQRS)%K=RMO
        PQRS_MOINT(NPQRS)%L=SMO
!       write(UNIout,'(1X,5I4,F12.6,2X,F12.6)')PMO,QMO,RMO,SMO,NPQRS, &
!                      PQRS_MOINT(NPQRS)%ijkl,PQRS_MOINT(NPQRS)%ikjl
        if(LPeqQ.and.LQeqS)then ! Save and print later
          Jaa(PMO)=PQRS_MOINT(NPQRS)%ijkl
        else if(LPeqR.and.LQeqS)then
          Term=CMOG%occupancy(QMO)*CMOG%occupancy(PMO)
          Jab=PQRS_MOINT(NPQRS)%ikjl
          Kab=PQRS_MOINT(NPQRS)%ijkl
!         write(6,*)'PMO,QMO, Jab, occ_Jab(PMO,QMO): ',PMO,QMO,Jab,occ_Jab(PMO,QMO)
          Jab_tot=Jab_tot+occ_Jab(PMO,QMO)*Jab
          Taa=TMOint(QMO,QMO)
!         write(UNIout,'(a,2f16.6)')'Taa: ',Taa
          K_tot=K_tot+occ_Kab(PMO,QMO)*Kab
!         write(6,*)'CMOG%occupancy(QMO),CMOG%occupancy(PMO): ',CMOG%occupancy(QMO),CMOG%occupancy(PMO)
!         write(UNIout,'(2i4,a,f12.6,a,f12.6,a,f12.6)')PMO,QMO,',', &
!                                                      Jab,',',Kab,',',Xab
        end if
      end if
      end do ! End SMO loop.
      end do ! End RMO loop.
      end do ! End QMO loop.
      end do ! End PMO loop.

      PQMAX=IJNDX(MO2)+MO2
      SHIFT=PQMAX*(PQMAX-1)/2+PQMAX
      end do ! IPASS

!     write(UNIout,'(5x,a)')'aa            Jaa             Xaa'
      Jaa_tot=ZERO
      T_tot=ZERO
      Vne_tot=ZERO
      do IMO=1,NTTRMO
        Taa=TMOint(IMO,IMO)
        Vaa=VMOint(IMO,IMO)
!       write(6,*)'IMO, Jaa, occ_Jab(IMO,IMO): ',IMO,Jaa(IMO),occ_Jab(IMO,IMO)
        if(CMOG%occupancy(IMO).eq.TWO)then
          Jaa_tot=Jaa_tot+Jaa(IMO)
        else
          Jaa_tot=Jaa_tot+occ_Jab(IMO,IMO)*Jaa(IMO)
        end if
        T_tot=T_tot+CMOG%occupancy(IMO)*Taa
        Vne_tot=Vne_tot+CMOG%occupancy(IMO)*Vaa
!       write(6,*)'IMO, Taa, Vaa, CMOG%occupancy(IMO): ',IMO,Taa,Vaa,CMOG%occupancy(IMO)
      end do
      J_tot=Jaa_tot+Jab_tot
      Vee=J_tot-K_tot 
      E_tot=T_tot+Vne_tot+J_tot-K_tot
      write(UNIout,'(a,F12.6)')'Jaa Total: ',Jaa_tot
      write(UNIout,'(a,F12.6)')'Jab Total: ',Jab_tot
      write(UNIout,'(a,F12.6)')'J_tot:     ',J_tot
      write(UNIout,'(a,F12.6)')'K_tot:     ',K_tot
      write(UNIout,'(a,F12.6)')'Vee:       ',Vee
      write(UNIout,'(a,F12.6)')'T_tot:     ',T_tot
      write(UNIout,'(a,F12.6)')'Vne_tot:   ',Vne_tot
      write(UNIout,'(a,F12.6)')'E_tot:     ',E_tot

      TNPQRS=NPQRS
      write(UNIout,'(a,i10,a)')'TRANSFORMATION COMPLETE ',NPQRS,' INTEGRALS KEPT'

      deallocate (BPQMO, SCRVEC, NCODE, BPQRS)
      deallocate (IJNDX)
      deallocate (Jaa)
!
! End of routine I2E_transform
      call PRG_manager ('exit', 'I2E_transform', 'UTILITY')
      return
!
 1020 FORMAT(/1X,'TWO-ELECTRON TRANSFORMATION COMPLETE')
!
      end subroutine I2E_transform
      subroutine I1E_transform (I1EAO, &
                                CMO, &
                                I1EMO, &
                                Nbasis, &
                                NTTRMO, &
                                MATlen)
!***********************************************************************
!     Date last modified: June 24, 1992                                *
!     Author: R.A. Poirier                                             *
!     Description: Transform one-electron integrals.                   *
!     NOTE: This uses the CMO (not CMOS)                               *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer, intent(IN) :: NTTRMO
      integer, intent(IN) :: Nbasis
      integer, intent(IN) :: MATlen
!
! Input arrays:
      double precision I1EMO(NTTRMO,NTTRMO),I1EAO(MATlen),CMO(Nbasis,NTTRMO)
! Work arrays
      double precision I1EAO2D(Nbasis,Nbasis)
!
! Local scalars:
!
! Begin:
      call PRG_manager ('enter', 'I1E_transform', 'UTILITY')
!
! Transform using matmul
      call UPT_to_SQS (I1EAO, MATlen, I1EAO2D, Nbasis)
      I1EMO=matmul (matmul(transpose(CMO), I1EAO2D), CMO)
!
! End of routine I1E_transform
      call PRG_manager ('exit', 'I1E_transform', 'UTILITY')
      return
      end subroutine I1E_transform
      subroutine I1E_AOtoMO
!***********************************************************************
!     Date last modified: June 24, 1992                                *
!     Author: R.A. Poirier                                             *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE QM_defaults
      USE QM_objects
      USE integral_transformation
      USE matrix_print

      implicit none
!
! Local scalar:
      integer :: IMO
!
! Begin:
      call PRG_manager ('enter', 'I1E_AOtoMO', 'UTILITY')
!
      call GET_object ('INT', '1EINT', 'AO')
      call GET_object ('QM', 'CMO', Wavefunction) ! Only RHF or ROHF
      call INI_intran

      allocate (DELS(NTTRMO,NTTRMO))
      allocate (TMOint(NTTRMO,NTTRMO), VMOint(NTTRMO,NTTRMO))
      call I1E_transform (OVRLAP, CMO%coeff, DELS, Nbasis, NTTRMO, MATlen)
      write(UNIout,'(a)')'MO Overlap integrals:'
      call PRT_matrix (DELS, NTTRMO, NTTRMO)

      call I1E_transform (Tint, CMO%coeff, TMOint, Nbasis, NTTRMO, MATlen)
      write(UNIout,'(a)')'MO Kinetic integrals:'
      call PRT_matrix (TMOint, NTTRMO, NTTRMO)

      call I1E_transform (Vint, CMO%coeff, VMOint, Nbasis, NTTRMO, MATlen)
      write(UNIout,'(a)')'MO Potential integrals:'
      call PRT_matrix (VMOint, NTTRMO, NTTRMO)

      write(UNIout,'(a)')' IMO        Tii           Vii'
      
      do IMO=1,NTTRMO
        write(UNIout,'(2i2,a,2x,f12.6,a,2x,f12.6)')IMO,IMO,',',TMOint(IMO,IMO),',',VMOint(IMO,IMO)
      end do

      deallocate (DELS)
!
! End of routine I1E_AOtoMO
      call PRG_manager ('exit', 'I1E_AOtoMO', 'UTILITY')
      return
      end subroutine I1E_AOtoMO
      subroutine I2E_AOtoMO
!***********************************************************************
!     Date last modified: June 24, 1992                                *
!     Author: R.A. Poirier                                             *
!***********************************************************************
! Modules:
      USE program_files
      USE QM_defaults
      USE integral_transformation

      implicit none
!
! Local scalars:
      integer SPACE
!
! Begin:
      call PRG_manager ('enter', 'I2E_AOtoMO', '2EINT%MO')
!
      call GET_object ('INT', '2EINT', 'RAW')
      call GET_object ('QM', 'CMO', Wavefunction) ! Only RHF or ROHF
      call INI_intran
!
! Calculate the total space required for a single pass.
      PQMAX=NTTRMO*(NTTRMO+1)/2
      PQRSMX=PQMAX*(PQMAX+1)/2
      NJKL=Nbasis*Nbasis*(Nbasis+1)/2
      if(NJKL.GT.MAX_transform)then
        write(UNIout,'(a)')'ERROR> I2E_AOtoMO:'
        write(UNIout,'(a)')'TOO MANY MO FOR TRANSFORMATION WITH CURRENT MAX_transform'
        write(UNIout,'(a)')'MAX_transform should be at least Nbasis*Nbasis*(Nbasis+1)/2'
        stop ' Too many MOs'
      end if
!
! Determine the number of passes required.
      NTMOMX=MIN0(NTTRMO, MAX_transform/NJKL)
      Npasses=(NTTRMO-1)/NTMOMX+1
      write(UNIout,1000)Npasses,NTMOMX
 1000 FORMAT('TWO-ELECTRON TRANSFORMATION:'/3X,'NUMBER OF PASSES REQUIRED: ',I4/3X,'MAXIMUM MO PER PASS: ',I4)
!
      call I2E_transform
!
! End of routine I2E_AOtoMO
      call PRG_manager ('exit', 'I2E_AOtoMO', '2EINT%MO')
      return
      end subroutine I2E_AOtoMO
      subroutine VDIPOLE_MO
!***********************************************************************
!     Date last modified: June 24, 1992                                *
!     Author: R.A. Poirier                                             *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE QM_defaults
      USE QM_objects
      USE integral_transformation
      USE matrix_print

      implicit none
!
! Local scalar:
      integer :: IMO
!
! Begin:
      call PRG_manager ('enter', 'VDIPOLE_MO', 'UTILITY')
!
      call GET_object ('INT', 'VDIPOLE', 'AO')
      call GET_object ('QM', 'CMO', Wavefunction) ! Only RHF or ROHF
      call INI_intran

      allocate (DELS(NTTRMO,NTTRMO))

      call I1E_transform (VDDx, CMO%coeff, DELS, Nbasis, NTTRMO, MATlen)
      write(UNIout,'(a)')'Dipole X-velocity MO integrals:'
      call PRT_matrix (DELS, NTTRMO, NTTRMO)

      call I1E_transform (VDDy, CMO%coeff, DELS, Nbasis, NTTRMO, MATlen)
      write(UNIout,'(a)')'Dipole Y-velocity MO integrals:'
      call PRT_matrix (DELS, NTTRMO, NTTRMO)

      call I1E_transform (VDDz, CMO%coeff, DELS, Nbasis, NTTRMO, MATlen)
      write(UNIout,'(a)')'Dipole Z-velocity MO integrals:'
      call PRT_matrix (DELS, NTTRMO, NTTRMO)

      deallocate (DELS)
!
! End of routine VDIPOLE_MO
      call PRG_manager ('exit', 'VDIPOLE_MO', 'UTILITY')
      return
      end subroutine VDIPOLE_MO
      subroutine MDIPOLE_MO
!***********************************************************************
!     Date last modified: June 24, 1992                                *
!     Author: R.A. Poirier                                             *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE QM_defaults
      USE QM_objects
      USE integral_transformation
      USE matrix_print

      implicit none
!
! Local scalar:
      integer :: IMO
!
! Begin:
      call PRG_manager ('enter', 'MDIPOLE_MO', 'UTILITY')
!
      call GET_object ('INT', 'MDIPOLE', 'AO')
      call GET_object ('QM', 'CMO', Wavefunction) ! Only RHF or ROHF
      call INI_intran

      allocate (DELS(NTTRMO,NTTRMO))

      call I1E_transform (XMDPOL, CMO%coeff, DELS, Nbasis, NTTRMO, MATlen)
      write(UNIout,'(a)')'Magnetic X-dipole MO integrals:'
      call PRT_matrix (DELS, NTTRMO, NTTRMO)

      call I1E_transform (YMDPOL, CMO%coeff, DELS, Nbasis, NTTRMO, MATlen)
      write(UNIout,'(a)')'Magnetic Y-dipole MO integrals:'
      call PRT_matrix (DELS, NTTRMO, NTTRMO)

      call I1E_transform (ZMDPOL, CMO%coeff, DELS, Nbasis, NTTRMO, MATlen)
      write(UNIout,'(a)')'Magnetic Z-dipole MO integrals:'
      call PRT_matrix (DELS, NTTRMO, NTTRMO)

      deallocate (DELS)
!
! End of routine MDIPOLE_MO
      call PRG_manager ('exit', 'MDIPOLE_MO', 'UTILITY')
      return
      end subroutine MDIPOLE_MO
