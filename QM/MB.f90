      subroutine MBcalc
!*********************************************************************************************
!     Date last modified: April 24, 2019                                                     *
!     Author: JWH                                                                            *
!     Desciption: Calculate the momentum-balance analytically.                               *
!*********************************************************************************************
! Modules:
      USE program_constants
      USE program_timing
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE INT_objects

      implicit none
!
! Local scalars:
      double precision :: EMaa,EMHFaa,AMaa,AMHFaa
      double precision :: EMbb,EMHFbb,AMbb,AMHFbb
      double precision :: EMab,EMHFab,AMab,AMHFab
      double precision :: EMba,EMHFba,AMba,AMHFba
!
! Begin:
      call PRG_manager ('enter', 'MBcalc', 'MB%ANA')
!
      call GET_object ('QM', '2RDM', 'AO')
!
! Calculate value analytically
!
      call GET_object ('INT', 'EAMINT', 'AO')
!
      EMHFaa = dot_product(TRDM_HF_AOaa,emAO)
      AMHFaa = dot_product(TRDM_HF_AOaa,amAO)
      EMaa = dot_product(TRDM_AOaa,emAO)
      AMaa = dot_product(TRDM_AOaa,amAO)
      EMHFbb = dot_product(TRDM_HF_AObb,emAO)
      AMHFbb = dot_product(TRDM_HF_AObb,amAO)
      EMbb = dot_product(TRDM_AObb,emAO)
      AMbb = dot_product(TRDM_AObb,amAO)
      EMHFab = dot_product(TRDM_HF_AOab,emAO)
      AMHFab = dot_product(TRDM_HF_AOab,amAO)
      EMab = dot_product(TRDM_AOab,emAO)
      AMab = dot_product(TRDM_AOab,amAO)
      EMHFba = dot_product(TRDM_HF_AOba,emAO)
      AMHFba = dot_product(TRDM_HF_AOba,amAO)
      EMba = dot_product(TRDM_AOba,emAO)
      AMba = dot_product(TRDM_AOba,amAO)
!
      write(UNIout,'(/a)')'               Momentum-balance analytically (same-spin) '
      write(UNIout,'(a)') '----------------------------------------------------------------------'
      write(UNIout,'(a)') '            Hartree-Fock                         Correlated      '
      write(UNIout,'(a)') '----------------------------------------------------------------------'
      write(UNIout,'(a)') '    alpha-alpha       beta-beta         alpha-alpha       beta-beta'
      write(UNIout,'(a)') '----------------------------------------------------------------------'
      write(UNIout,'(4(f14.8,4x))')EMHFaa-AMHFaa,EMHFbb-AMHFbb,EMaa-AMaa,EMbb-AMbb
!
      write(UNIout,'(/a)')'             Momentum-balance analytically (opposite-spin) '
      write(UNIout,'(a)') '----------------------------------------------------------------------'
      write(UNIout,'(a)') '            Hartree-Fock                         Correlated      '
      write(UNIout,'(a)') '----------------------------------------------------------------------'
      write(UNIout,'(a)') '    alpha-beta        beta-alpha        alpha-beta        beta-alpha'
      write(UNIout,'(a)') '----------------------------------------------------------------------'
      write(UNIout,'(4(f14.8,4x))')EMHFab-AMHFab,EMHFba-AMHFba,EMab-AMab,EMba-AMba
!
! End of routine MBcalc
      call PRG_manager ('exit', 'MBcalc', 'MB%ANA')
      end subroutine MBcalc
