      subroutine Vibrational_frequencies (Modality)
!************************************************************************
!     Date last modified: July 12, 2002                   Version 1.2   *
!     Author: Tammy L. Gosse and R. A. Poirier                          *
!     Description: A transformation of the force constant matrix        *
!                  from internal coordinates to cartesian               *
!                  coordinates using a B-matrix (if necessary).         *
!                  Also, the modification of the force constant         *
!                  matrix for cases that involve dummies, and a         *
!                  calculation of vibrational frequencies.              *
!************************************************************************
! Modules:
      USE program_constants
      USE QM_objects
      USE type_molecule
      USE matrix_print

      implicit none
!
! Input scalar:
      character*(*) Modality
!
! Work arrays:
      double precision, dimension(:,:), allocatable :: FCMassW
      double precision, dimension(:), allocatable :: EIGVL
      double precision, dimension(:), allocatable :: MASSES
!
! Local scalars:
      integer :: NFCelem,NFreq,Nminus,FCdim
      integer :: Iatom,I,J,IROW,Istart,JCOL,IPARAM,N
      double precision :: FC_convertion,Freq_convert
!
      parameter (FC_convertion=15.56838012D0,Freq_convert=1302.833159D0)
!
! Begin:
      call PRG_manager ('enter', 'Vibrational_frequencies', 'FREQUENCIES%'//Modality)
!
      NFreq=3*NReal_atoms
      Nminus=6 ! 3 translational and 3 rotational degrees
      if(index(molecule%point_group,'(inf)').ne.0)Nminus=5 ! Linear molecule

      write(UNIout,'(2a)')'Modality: ',Modality(1:len_trim(Modality))
!     call GET_object ('MOL', 'ATOMIC', 'MASSES') ! No longer an object
      call GET_object ('QM', 'FORCE_CONSTANT', Modality)

! Object:
      if(.not.allocated(VibFreq))then
        allocate (VibFreq(NFreq))
      end if
!
! The following prints the force constant matrix and the B-matrix.
      NFCelem=size(Force_Constant,1)

      allocate (EIGVL(1:NFreq))
      allocate (MASSES(1:NFreq), FCMassW(1:NFreq,1:NFreq))
!
! Computes mass array for the mass-weighing.
      call GET_atomic_masses
      N=1
      do Iatom=1,Natoms ! mass array
        if(MOL_atoms(Iatom)%atomic_number.le.0)cycle
        MASSES(N)=DSQRT(MOL_atoms(Iatom)%mass)
        MASSES(N+1)=DSQRT(MOL_atoms(Iatom)%mass)
        MASSES(N+2)=DSQRT(MOL_atoms(Iatom)%mass)
        N=N+3
      end do ! mass array
!
! Mass-weighs the force constant matrix.
      do IROW=1,NFreq
        do JCOL=1,NFreq
          FCMassW(IROW,JCOL)=Force_constant(IROW,JCOL)/(MASSES(IROW)*MASSES(JCOL))
        end do ! JCOL
      end do ! IROW
!
! The following converts the FC matrix from Hartree/Bohr**2 to Mdyne/Angstrom
      FCMassW=FCMassW*FC_convertion
!
! Diagonalizes the mass-weighted force program_constants.
      call MATRIX_diagonalize (FCMassW, FCMassW, EIGVL, NFreq, -2, .true.)
      call SORT_eigenvalues (EIGVL, FCMassW, NFreq)  ! from smallest abs value
! Copy and convert to cm^-1
! Indicate which frequencies are imaginary, ie. give as negative.
!     FirstVal=dabs(EIGVL(1))
!     Nsmall=0
      do I=1,NFreq
        if(EIGVL(I).lt.ZERO)then
          Vibfreq(I)=-Freq_convert*DSQRT(DABS(EIGVL(I)))      ! convertion factor from to???
!         if(dabs(EIGVL(I)).lt.FirstVal)Nsmall=Nsmall+1
        else
          Vibfreq(I)=Freq_convert*DSQRT(EIGVL(I))
        end if
      end do ! I
!
      call PRT_masses
! Calculates the vibrational frequencies.
!     write(UNIout,'(/a)')'Translational and rotational degrees for freedom:'
!     write(UNIout,'(3F10.4)')(Vibfreq(I),I=1,3)
!     write(UNIout,'(3F10.4)')(Vibfreq(I),I=4,Nminus)
      write(UNIout,'(/a)')'Vibrational_frequencies> Vibrational Frequencies (cm^-1):'
!     Istart=Nminus+1
      Istart=1   ! NOTE not first 5/6 are trans/rot!!
      do I=Istart,NFreq
        if(VibFreq(I).LT.ZERO)then
          write(UNIout,'(F10.4,a)')dabs(Vibfreq(I)),'i'
        else
          write(UNIout,'(F10.4)')Vibfreq(I)
        end if
      end do ! Vibrational Frequencies

      deallocate (FCMassW, EIGVL, MASSES)
!
! End of routine Vibrational_frequencies
      call PRG_manager ('exit', 'Vibrational_frequencies', 'FREQUENCIES%'//Modality)
      return
      end
      subroutine SORT_eigenvalues (D, Z, N)
!************************************************************************************************************
!     Date last modified: June 15, 2010                                                        Version 2.0  *
!     Author: R. A. Poirier                                                                                 *
!     Description: Sort the Eigenvalues and Eigenvectors                                                    *
!************************************************************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer :: N
      double precision :: D(N),Z(N,N)
!
! Local scalars:
      integer :: I,II,J,K
      double precision :: P

! Order eigenvalues and eigenvectors.
      do II=2,N
        I=II-1
        K=I
        P=D(I)
        do J=II,N
          if(dabs(D(J)).lt.dabs(P))then
            K=J
            P=D(J)
          end if
        end do ! J
        if(K.ne.I)then
          D(K)=D(I)
          D(I)=P
            do J=1,N
              P=Z(J,I)
              Z(J,I)=Z(J,K)
              Z(J,K)=P
            end do
        end if
      end do
!
! End of routine SORT_eigenvalues
      return
      end subroutine SORT_eigenvalues

