      subroutine BLD_GUESS_MO_RHF
!***********************************************************************
!     Date last modified: July 11, 2019                                *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!***********************************************************************
! Modules:
      USE program_files
      USE type_basis_set
      USE QM_objects
      USE INT_objects
      USE QM_defaults
      USE matrix_print

      implicit none
!
! Input scalars:
!      character*(*) ObjectIn
!
! Local scalars:
      integer I
      logical :: LPRT_basis
!
      integer :: NbasisG ! For now
!     type (molecular_orbital) CMOH
!
! Work arrays:
!
! Begin:
      call PRG_manager ('enter', 'BLD_GUESS_MO_RHF', 'UTILITY')
!
! Object:
      if(.not.associated(CMOG%coeff))then
        allocate (CMOG%coeff(Basis%Nbasis,Basis%Nbasis), CMOG%eigval(Basis%Nbasis), CMOG%occupancy(Basis%Nbasis))
      else
! Must always build guess (could be new molecule...
        deallocate (CMOG%coeff, CMOG%eigval, CMOG%occupancy)
        allocate (CMOG%coeff(Basis%Nbasis,Basis%Nbasis), CMOG%eigval(Basis%Nbasis), CMOG%occupancy(Basis%Nbasis))
      end if
!
      LPRT_basis=.false.
!
! Create basis set to project from:
      call GET_basis_set (BASIS_PROJECT, GUESS_basis, LPRT_basis)
      LBasis_guess=.true.
      NbasisG=GUESS_basis%Nbasis
!
      write(UNIout,'(2a)')'Guess type set to: ',GUESS_type(1:len_trim(GUESS_type))
      if(GUESS_type(1:10).eq.'FockMatrix')then ! Project the Fock matrix
        if(GUESS_previous)then ! make sure the basis set exist
          call PROJECT_FockM
        else
          call PROJECT_HuckelM
        end if
!
      else if(GUESS_type(1:2).eq.'MO')then ! Project the MO coefficients
        if(GUESS_previous)then ! make sure the basis set exist
          write(UNIout,'(4a)')'Projecting MOs from ', &
                         GUESS_basis%name(1:len_trim(GUESS_basis%name)),' to ',Basis%name(1:len_trim(Basis%name))
          if(.not.associated(CMOP%coeff))then
            write(UNIout,'(a)')'ERROR> BLD_GUESS_MO_RHF: CMOP not allocated (no previous guess)!'
            stop 'ERROR> BLD_GUESS_MO_RHF: CMOP not allocated!'
          end if
          if(NbasisG.ne.size(CMOP%coeff,1))then ! Check to make sure MOs match the basis set
            write(UNIout,'(a)')'ERROR> BLD_GUESS_MO_RHF: Previous MOs and basis set do not match'
            write(UNIout,'(a)')'Probable cause: MOs from previous calculation were not created'
            call PRG_stop ('Previous MOs and basis set do not match')
          end if
          call PROJECT_MO (CMOP)
          deallocate (CMOP%coeff, CMOP%EIGVAL)
        else
          write(UNIout,'(a)')' Projecting MOs from STO-3G basis set (extended Huckel)'
          call Huckel_MO
          call PROJECT_MO (CMOH)
          deallocate (CMOH%coeff, CMOH%EIGVAL)
        end if  
      else if(GUESS_type(1:3).eq.'GMA')then ! Form the Fock matrix as H+G (G=empirical)
        write(UNIout,'(a)')'Build the G matrix  '
        call BLD_GMatrix_STO3G
      end if
!
! End of routine BLD_GUESS_MO_RHF
      call PRG_manager ('exit', 'BLD_GUESS_MO_RHF', 'UTILITY')
      return
!
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine PROJECT_MO (CMO_from)
!*******************************************************************************
!     Date last modified: June 12, 2000                           Version 2.0  *
!     Author: R.A. Poirier                                                     *
!     Description: Set code defaults.                                          *
!*******************************************************************************
! Modules:
      USE program_constants
!
! Local scalars:
      integer :: MATlen,NAelectron,NBelectron,Nelem

      type (molecular_orbital) CMO_from
!
! Work arrays:
      type (molecular_orbital) CMOw
      integer, dimension(:), allocatable :: MAPVEC
      double precision, dimension(:,:), allocatable :: SQSM
      double precision, dimension(:,:), allocatable :: SMHALF
      double precision, dimension(:,:), allocatable :: SEigVec
      double precision, dimension(:), allocatable :: SEigVal
      double precision, dimension(:,:), allocatable :: BASISS
!
! Begin:
      call PRG_manager ('enter', 'PROJECT_MO', 'UTILITY')
!
      MATlen=Basis%Nbasis*(Basis%Nbasis+1)/2

      allocate (BASISS(Basis%Nbasis,NbasisG))
      call BASIS_overlap (Basis, GUESS_basis, BasisS)
!
! Now project the MOs in CMO_from to create CMOG%coeff
! Allocate work arrays:
      allocate (SQSM(Basis%Nbasis,Basis%Nbasis), &
                SEigVec(Basis%Nbasis,Basis%Nbasis), SMHALF(Basis%Nbasis,Basis%Nbasis), &
                SEigVal(Basis%Nbasis), MAPVEC(Basis%Nbasis), &
                CMOw%coeff(Basis%Nbasis,NbasisG))
!
      call GET_object ('INT', '1EINT', 'AO') ! Get the overlap integrals (S_A) for the current basis set
      call UPT_to_SQS (OVRLAP, MATlen, SQSM, Basis%Nbasis) ! Convert overlap matrix to square symmetric form
!
! Form S**(-1/2).
      call NVSQRT (SQSM, SMHALF, SEigVec, SEigVal, Basis%Nbasis)

      CMOw%coeff=matmul(SMHALF, BASISS)

      CMOw%coeff=matmul(CMOw%coeff, CMO_from%coeff)
      CMOG%coeff(1:Basis%Nbasis,1:Basis%Nbasis)=ZERO
      if(NbasisG.gt.Basis%Nbasis)then
        Nelem=Basis%Nbasis
      else
        Nelem=NbasisG
      end if
      CMOG%coeff(1:Basis%Nbasis,1:Nelem)=CMOw%coeff(1:Basis%Nbasis,1:Nelem)
      call ORTHOC (CMOG%coeff, SEigVec, Nelem, Basis%Nbasis, MAPVEC)
      CMOG%coeff=matmul(SMHALF, CMOG%coeff)
!
      do I=1,Basis%Nbasis
        IF(MAPVEC(I).LE.0.or.MAPVEC(I).GT.NbasisG)then
          CMOG%eigval(I)=ZERO
        else
          CMOG%eigval(I)=CMO_from%eigval(MAPVEC(I))
        end if
      end do
!
! Set the occupancies
      call DEF_NoccMO (CMOG%NoccMO, NAelectron, NBelectron)
      CMOG%occupancy(1:CMOG%NoccMO)=TWO
      CMOG%occupancy(CMOG%NoccMO+1:Basis%Nbasis)=ZERO
!
      deallocate (BASISS) ! Basis set overlap no longer required
      deallocate (SQSM, SEigVec, SMHALF, SEigVal, MAPVEC, CMOw%coeff)
!
      call PRG_manager ('exit', 'PROJECT_MO', 'UTILITY')
      return
      end subroutine PROJECT_MO
      subroutine ORTHOC (A, &
                         B, &
                         NCOMP, &
                         NbasisIN, &
                         MAP)
!***********************************************************************
!     Date last modified: MARCH 16, 1994                   Version 1.0 *
!     Author:                                                          *
!     Description:  U. of T. Version                                   *
!     Schmidt orthogonalization of the column vectors of A.            *
!     If the number of vectors obtained is less than Basis%Nbasis,     *
!     the set is completed by means of the complete set in B.          *
!                                                                      *
!     NCOMP is the number of vectors provided in A.                    *
!     AA and BB are work vectors.                                      *
!     MAP(I) gives the correspondence between the new (I) and old      *
!     (MAP(I)) vectors.                                                *
!***********************************************************************
! Modules:
      USE program_constants

      implicit none
!
! Input scalars:
      integer, intent(IN) :: NbasisIN,NCOMP
!
! Input arrays:
      integer MAP(NbasisIN)
      double precision A(NbasisIN,NbasisIN),B(NbasisIN,NbasisIN)
!
! Local scalars:
      integer I,J,JS,JSS,K,NC1,NN
      double precision AR,SPP,SPS,T,TENM10

      double precision, dimension(:), allocatable :: AA
      double precision, dimension(:), allocatable :: BB
!
      parameter (TENM10=1.0D-10)
!
! Begin:
      call PRG_manager ('enter', 'ORTHOC', 'UTILITY')
!
      allocate (AA(NbasisIN), BB(NbasisIN))
!
! Normalize first vector.
      T=ZERO
      do J=1,NbasisIN
        T=T+A(J,1)*A(J,1)
      end do
      T=ONE/DSQRT(T)
      do J=1,NbasisIN
        A(J,1)=A(J,1)*T
      end do
      MAP(1)=1
      IF(NbasisIN.GT.1) then
!
! Orthonormalize each vector with respect to all its predecessors.
      NN=1
      do I=2,NCOMP
! Copy vector to be orthonormalized into BB.
! Collect projections of BB onto the previous vectors in AA.
      do J=1,NbasisIN
        AA(J)=ZERO
        BB(J)=A(J,I)
      end do
! Obtain projection of BB on all predecessors.
      do J=1,NN
      AR=ZERO
      do K=1,NbasisIN
        AR=AR+BB(K)*A(K,J)
      end do !K
      do K=1,NbasisIN
        AA(K)=AA(K)+AR*A(K,J)
      end do ! K
      end do ! J
! Subtract the projections from BB.
      T=ZERO
      do K=1,NbasisIN
        BB(K)=BB(K)-AA(K)
        T=T+BB(K)*BB(K)
      end do ! K
! Normalize the new vector.
! Watch out for projections from larger to smaller, or SP to S, bases.
      IF(T.GT.TENM10) then
      T=ONE/DSQRT(T)
      NN=NN+1
      MAP(NN)=I
      do K=1,NbasisIN
        A(K,NN)=BB(K)*T
      end do ! K
      end if ! T.GT.TENM10
      end do ! I
!
! Complete the matrix.
      NC1=NN+1
      IF(NC1.LE.NbasisIN) then
      do I=NC1,NbasisIN
! Find the vector in B with the least overlap with those in A.
      SPS=ONE
      do JS=1,NbasisIN
      SPP=ZERO
      do K=1,NbasisIN
        BB(K)=B(K,JS)
      end do
      do J=1,NN
! Get dot product with the vectors in A.
      AR=ZERO
      do K=1,NbasisIN
        AR=AR+A(K,J)*BB(K)
      end do
      SPP=SPP+AR*AR
      end do !J
      IF(SPP.LT.SPS)then
      SPS=SPP
      JSS=JS
      end if
      end do !JS
! Orthonormalize vector JSS from B as above.
      do K=1,NbasisIN
        AA(K)=ZERO
        BB(K)=B(K,JSS)
      end do
      do J=1,NN
      AR=ZERO
      do K=1,NbasisIN
        AR=AR+BB(K)*A(K,J)
      end do !K
      do K=1,NbasisIN
        AA(K)=AA(K)+AR*A(K,J)
      end do ! K
      end do ! J
      T=ZERO
      do K=1,NbasisIN
        BB(K)=BB(K)-AA(K)
        T=T+BB(K)*BB(K)
      end do !K
      T=ONE/DSQRT(T)
      do K=1,NbasisIN
        A(K,I)=BB(K)*T
      end do !K
      NN=NN+1
      MAP(NN)=0
!
      end do ! I
      end if ! NC1.LE.NbasisIN
      end if ! NbasisIN.GT.1
!
      deallocate (AA, BB)
!
! End of routine ORTHOC
      call PRG_manager ('exit', 'ORTHOC', 'UTILITY')
      return
      end subroutine ORTHOC
      subroutine PROJECT_FockM
!*****************************************************************************************************************
!     Date last modified: Nov.25th, 04                                                                           *
!     Author: Aisha and R.A. Poirier                                                                             *
!     Description: Fock-matrix projection                                                                        *
!*****************************************************************************************************************
! Modules:
      USE program_constants
      USE matrix_print
!
! Local scalars:
      integer :: NAelectron,NBelectron,MATlen

! Work arrays:
      double precision, dimension(:,:), allocatable :: BASISS
      double precision, dimension(:,:), allocatable :: FQSM
      double precision, dimension(:,:), allocatable :: FQSMP
      double precision, dimension(:,:), allocatable :: PselfS ! Same as OVRLAP
      double precision, dimension(:,:), allocatable :: SMone
      double precision, dimension(:), allocatable :: FockMw
!
      MATlen=Basis%Nbasis*(Basis%Nbasis+1)/2

      if(.not.allocated(FQSM))then
        allocate (FQSM(Basis%Nbasis,Basis%Nbasis))
      else
        deallocate (FQSM)
        allocate (FQSM(Basis%Nbasis,Basis%Nbasis))
      end if
      allocate (BASISS(Basis%Nbasis,NbasisG))
      allocate (FQSMP(NbasisG,NbasisG))
      allocate (FockMw(MATlen))

      call BASIS_overlap (Basis, GUESS_basis, BasisS)
      allocate (PselfS(NbasisG,NbasisG))
      call BASIS_overlap (GUESS_basis, GUESS_basis, PselfS)

      if(GUESS_previous)then ! make sure the basis set exist
        write(UNIout,'(4a)')'Projecting Fock matrix from ', &
                         GUESS_basis%name(1:len_trim(GUESS_basis%name)),' to ',Basis%name(1:len_trim(Basis%name))
        if(.not.allocated(FockM))then
          write(UNIout,*)'ERROR> Project_FockM: Previous Fock matrix does not exist'
          call PRG_stop ('ERROR> Project_FockM: Previous Fock matrix does not exist')
        end if
        call UPT_TO_SQS (FockM, NbasisG*(NbasisG+1)/2, FQSMP, NbasisG)
        deallocate (FockM)
        allocate (SMone(NbasisG, NbasisG))
        call BLD_SMone (Pselfs, SMone, NbasisG)
        BASISS = matmul(BASISS, SMone)
        deallocate (SMone)
      else
        if(MUN_prtlev.gt.0)write(UNIout,'(4a)')'Projecting extended Huckel matrix (STO-3G) ', &
                         ' to ',Basis%name(1:len_trim(Basis%name))
        call Huckel (FQSMP, PselfS) ! Get Huckel Hamiltonian
      end if
!
! Now Project the Fock Matrix to Current Basis
      FQSM = matmul(matmul(BASISS, FQSMP), transpose(BASISS))
      call GET_object ('INT', '1EINT', 'AO') ! Get the overlap integrals (S_A) for the current basis set
      call SQS_TO_UPT (FQSM, Basis%Nbasis, FockMw, MATlen)
      call DETlinW (OVRLAP, FockMw, CMOG%coeff, CMOG%EIGVAL, MATlen, Nbasis, .true.)
      call DEF_NoccMO (CMOG%NoccMO, NAelectron, NBelectron)
      CMOG%occupancy(1:CMOG%NoccMO)=TWO
      CMOG%occupancy(CMOG%NoccMO+1:Basis%Nbasis)=ZERO

      deallocate (PselfS)
      deallocate (FockMw, FQSM, FQSMP, BASISS)
!
! End of routine PROJECT_FockM
      return
      end subroutine PROJECT_FockM
      subroutine PROJECT_HuckelM
!*****************************************************************************************************************
!     Date last modified: Nov.25th, 04                                                                           *
!     Author: Aisha and R.A. Poirier                                                                             *
!     Description: Fock-matrix projection                                                                        *
!*****************************************************************************************************************
! Modules:
      USE program_constants
      USE matrix_print
!
! Local scalars:
      integer :: NAelectron,NBelectron,MATlen

! Work arrays:
      double precision, dimension(:,:), allocatable :: BASISS
      double precision, dimension(:,:), allocatable :: FQSM
      double precision, dimension(:,:), allocatable :: FQSMP
      double precision, dimension(:,:), allocatable :: PselfS ! Same as OVRLAP
      double precision, dimension(:), allocatable :: HuckelMw
!
      MATlen=Basis%Nbasis*(Basis%Nbasis+1)/2

      if(.not.allocated(FQSM))then
        allocate (FQSM(Basis%Nbasis,Basis%Nbasis))
      else
        deallocate (FQSM)
        allocate (FQSM(Basis%Nbasis,Basis%Nbasis))
      end if
      allocate (BASISS(Basis%Nbasis,NbasisG))
      allocate (FQSMP(NbasisG,NbasisG))
      allocate (HuckelMw(MATlen))

      call BASIS_overlap (Basis, GUESS_basis, BasisS)
      allocate (PselfS(NbasisG,NbasisG))
      call BASIS_overlap (GUESS_basis, GUESS_basis, PselfS)

      if(MUN_prtlev.gt.0)write(UNIout,'(4a)')'Projecting extended Huckel matrix (STO-3G) ', &
                         ' to ',Basis%name(1:len_trim(Basis%name))
      call Huckel (FQSMP, PselfS) ! Get Huckel Hamiltonian
!
! Now Project the Huckel Matrix to Current Basis
      FQSM = matmul(matmul(BASISS, FQSMP), transpose(BASISS))
      call GET_object ('INT', '1EINT', 'AO') ! Get the overlap integrals (S_A) for the current basis set
      call SQS_TO_UPT (FQSM, Basis%Nbasis, HuckelMw, MATlen)
      call DETlinW (OVRLAP, HuckelMw, CMOG%coeff, CMOG%EIGVAL, MATlen, Nbasis, .true.)
      call DEF_NoccMO (CMOG%NoccMO, NAelectron, NBelectron)
      CMOG%occupancy(1:CMOG%NoccMO)=TWO
      CMOG%occupancy(CMOG%NoccMO+1:Basis%Nbasis)=ZERO

      deallocate (PselfS)
      deallocate (HuckelMw, FQSM, FQSMP, BASISS)
!
! End of routine PROJECT_HuckelM
      return
      end subroutine PROJECT_HuckelM
! END CONTAINS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine BLD_GUESS_MO_RHF

      subroutine BASIS_overlap (BasisA, &
                                BasisB, &
                                BASOVL)
!*****************************************************************************************************************
!     Date last modified: February 18, 2000                                                         Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Basis Projecting from (default=STO-3G).                                                       *
!     Description: Get overlap of two basis sets.                                                                *
!*****************************************************************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE type_basis_set

      implicit none
!
! Output array:
      type (basis_set) BasisA
      type (basis_set) BasisB
      double precision BASOVL(BasisA%Nbasis,BasisB%Nbasis)
!
! Local scalars:
      integer Iatmshl,Jatmshl,Iatom,Jatom,Istart,Jstart,Iend,Jend,Ishell,Jshell,LAMAX,LBMAX,LPMAX,Itype,Jtype,Iaos,JaoS, &
              Ifrst,Jfrst,Ilast,Jlast,I,Igauss,IGBEGN,IGEND,INDX1,INTC,Irange,IX,IY,IZ,J, &
              Jgauss,JGBEGN,JGEND,JLIMIT,Jrange,JX,JY,JZ,LENTQ,LIM1DS
      double precision ABX,ABY,ABZ,ARABSQ,ARGP,AS,ASXA,ASYA,ASZA,AX,AY,AZ,BS,BX,BY,BZ,CUT1,EP,EPI,EPIO2,PEXP,PX, &
                       PY,PZ,RABSQ,STERM,XAP,XBP,YAP,YBP,ZAP,ZBP
      logical IDFSHL,JDFSHL
!
! SHELLal arrays:
      integer IJend(4),IJST(4),INDIX(20),INDIY(20),INDIZ(20),INDJX(20),INDJY(20),INDJZ(20)
      double precision CA(20),CB(20),CCX(192),CCY(192),CCZ(192),SS(100),SX(32),SY(32),SZ(32),S1C(6)
!
      parameter (CUT1=-75.0D0)
!
!                s x y x
      DATA INDJX/1,2,1,1,3,1,1,2,2,1,4,1,1,2,3,3,2,1,1,2/,INDJY/1,1,2,1,1,3,1,2,1,2,1,4,1,3,2,1,1,2,3,2/, &
           INDJZ/1,1,1,2,1,1,3,1,2,2,1,1,4,1,1,2,3,3,2,2/
!
! Begin:
      BASOVL(1:BasisA%Nbasis,1:BasisB%Nbasis)=ZERO

      do I=1,20
        INDIX(I)=4*(INDJX(I)-1)
        INDIY(I)=4*(INDJY(I)-1)
        INDIZ(I)=4*(INDJZ(I)-1)
      end do
!
! Begin loop over SHELLs.
      do Ishell=1,BasisA%Nshells
      Itype=BasisA%shell(Ishell)%Xtype
      LAMAX=Itype+1
      Istart=BasisA%shell(Ishell)%Xstart
      Iend=BasisA%shell(Ishell)%XEND
      Irange=Iend-Istart+1
      IGBEGN=BasisA%shell(Ishell)%EXPBGN
      IGEND= BasisA%shell(Ishell)%EXPEND
!
! Loop over Jshell.
      do Jshell=1,BasisB%Nshells
      Jtype=BasisB%shell(Jshell)%Xtype
      LBMAX=Jtype+1
      Jstart=BasisB%shell(Jshell)%Xstart
      Jend=BasisB%shell(Jshell)%XEND
      Jrange=Jend-Jstart+1
      JGBEGN=BasisB%shell(Jshell)%EXPBGN
      JGEND=BasisB%shell(Jshell)%EXPEND
!
      Ifrst=BasisA%shell(Ishell)%frstSHL
      Ilast=BasisA%shell(Ishell)%lastSHL
      Jfrst=BasisB%shell(Jshell)%frstSHL
      Jlast=BasisB%shell(Jshell)%lastSHL

      do Iatmshl=Ifrst,Ilast
        Iatom=BasisA%atmshl(Iatmshl)%ATMLST
        AX=CARTESIAN(Iatom)%X
        AY=CARTESIAN(Iatom)%Y
        AZ=CARTESIAN(Iatom)%Z
        Iaos=BasisA%atmshl(Iatmshl)%frstAO-1

      do Jatmshl=Jfrst,Jlast
        Jatom=BasisB%atmshl(Jatmshl)%ATMLST
        BX=CARTESIAN(Jatom)%X
        BY=CARTESIAN(Jatom)%Y
        BZ=CARTESIAN(Jatom)%Z
        Jaos=BasisB%atmshl(Jatmshl)%frstAO-1

      LPMAX=LAMAX+Jtype
      LIM1DS=(LPMAX+1)/2
      LENTQ=Irange*Jrange
      ABX=BX-AX
      ABY=BY-AY
      ABZ=BZ-AZ
      RABSQ=ABX*ABX+ABY*ABY+ABZ*ABZ
      SS(1:LENTQ)=ZERO
!
! Begin loop over primitive Gaussians.
      do Igauss=IGBEGN,IGEND
      AS=BasisA%gaussian(Igauss)%exp
      ASXA=AS*AX
      ASYA=AS*AY
      ASZA=AS*AZ
      ARABSQ=AS*RABSQ
      call FILLCC (LAMAX, BasisA%gaussian(Igauss)%contrc, CA)
      do Jgauss=JGBEGN,JGEND
      BS=BasisB%gaussian(Jgauss)%exp
      call FILLCC (LBMAX, BasisB%gaussian(Jgauss)%contrc, CB)
      EP=AS+BS
      EPI=ONE/EP
      EPIO2=PT5*EPI
      ARGP=-BS*ARABSQ*EPI
      PEXP=ZERO
      IF(ARGP.GT.CUT1)PEXP=DEXP(ARGP)
      PX=(ASXA+BS*BX)*EPI
      PY=(ASYA+BS*BY)*EPI
      PZ=(ASZA+BS*BZ)*EPI
      XAP=PX-AX
      XBP=PX-BX
      YAP=PY-AY
      YBP=PY-BY
      ZAP=PZ-AZ
      ZBP=PZ-BZ
      call GETCC1 (CCX, XAP, XBP, LAMAX, LBMAX)
      call GETCC1 (CCY, YAP, YBP, LAMAX, LBMAX)
      call GETCC1 (CCZ, ZAP, ZBP, LAMAX, LBMAX)
      STERM=DSQRT(EPI*PI_VAL)
      call GET1CS (S1C, STERM, EPIO2, (LPMAX+1)/2)
      call GET2CS (SX, S1C, CCX, LAMAX, LBMAX)
      call GET2CS (SY, S1C, CCY, LAMAX, LBMAX)
      do I=1,LIM1DS
      S1C(I)=S1C(I)*PEXP
      end do
      call GET2CS (SZ, S1C, CCZ, LAMAX, LBMAX)
!
! Begin loop over atomic orbitals.
      INTC=0
      do I=Istart,Iend
      IX=INDIX(I)
      IY=INDIY(I)
      IZ=INDIZ(I)
      do J=Jstart,Jend
      JX=INDJX(J)
      JY=INDJY(J)
      JZ=INDJZ(J)
      INTC=INTC+1
      SS(INTC)=SS(INTC)+SX(IX+JX)*SY(IY+JY)*SZ(IZ+JZ)*CA(I)*CB(J)
      end do !J
      end do !I
!
! End of loop over primitive Gaussians.
      end do !Jgauss
      end do !Igauss
!
! Fill BASOVL with integrals from SS.
! Obtain correct bias for J-loop.
      INDX1=0
! Commence loop.
      do I=1,Irange
      INTC=INDX1
      do J=1,Jrange
      INTC=INTC+1
! Plant the value.
      BASOVL(I+Iaos,J+JaoS)=SS(INTC)
      end do ! J
      INDX1=INDX1+Jrange
      end do !I
!
! End of loop over Shells.
      end do ! Jshell
      end do ! Ishell

! End of loop over SHELLs.
      end do ! Jshell
      end do ! Ishell
!
! End of routine BASIS_overlap
      return
      end subroutine BASIS_overlap
      subroutine SET_MO_project
!*****************************************************************************************************************
!     Date last modified: January 31, 2001                                                          Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Set up MOs for projected guess.                                                               *
!*****************************************************************************************************************
! Modules:
      USE program_files
      USE program_objects
      USE type_basis_set
      USE QM_objects
      USE INT_objects
      USE QM_defaults

      implicit none
!
! Local scalars:
      integer :: NbasisG
!
! Begin:
      call PRG_manager ('enter', 'SET_MO_project', 'UTILITY')
!
      if(associated(CMOG%coeff))then
        GUESS_previous=.true.
        PREVIOUS_BASIS=BASIS%name
        BASIS_PROJECT=PREVIOUS_BASIS
        NbasisG=Basis%Nbasis
        write(UNIout,'(2a)')'Will be projecting from: ',BASIS_PROJECT(1:len_trim(BASIS_PROJECT))
        if(NbasisG.ne.size(CMOG%coeff,1))then ! Check to make sure MOs match the basis set
          write(UNIout,'(a)')'ERROR> SET_MO_project: Previous MOs and basis set do not match'
          write(UNIout,'(a)')'Probable cause: MOs from previous calculation were not created'
          call PRG_stop ('SET_MO_project: Previous MOs and basis set do not match')
        end if
        if(associated(CMOP%coeff))then
          deallocate(CMOP%coeff, CMOP%EIGVAL)
        end if  ! allocated(CMOP%coeff)
        allocate(CMOP%coeff(NbasisG,NbasisG), CMOP%EIGVAL(NbasisG))
        CMOP%coeff=CMOG%coeff
        CMOP%EIGVAL=CMOG%EIGVAL
      else ! CMOG not allocated
        write(UNIout,'(a)')'WARNING> SET_MO_project: Previous guess does not exist, using the default'
      end if ! allocated(CMOG%coeff)
!
! End of routine SET_MO_project
      call PRG_manager ('exit', 'SET_MO_project', 'UTILITY')
      return
      end subroutine SET_MO_project
      subroutine BLD_GUESS_MO_UHF
!***********************************************************************
!     Date last modified: July 07, 1992                    Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description:                                                     *
!***********************************************************************
! Modules:
      USE program_constants
      USE type_basis_set
      USE QM_objects
      USE INT_objects
      USE matrix_print

      implicit none
!
! Input scalars:
!
! Local scalars:
      integer :: NoccMO,NAelectron,NBelectron
      logical LPRT_basis
!
! Begin:
      call PRG_manager ('enter', 'BLD_GUESS_MO_UHF', 'CMO_GUESS%UHF')
!
      call GET_object ('QM', 'CMO_GUESS', 'RHF')
! Object:
      write(UNIout,*)'BLD_GUESS_MO_UHF> Generate UHF guess'
!
      if(.not.associated(CMOAG%coeff))then
        allocate (CMOAG%coeff(Basis%Nbasis,Basis%Nbasis), CMOAG%eigval(Basis%Nbasis), &
                  CMOAG%occupancy(Basis%Nbasis), &
                  CMOBG%coeff(Basis%Nbasis,Basis%Nbasis), CMOBG%eigval(Basis%Nbasis), &
                  CMOBG%occupancy(Basis%Nbasis))
      else
          deallocate (CMOAG%coeff, CMOAG%eigval, CMOBG%coeff, CMOBG%eigval, CMOAG%occupancy, CMOBG%occupancy)
          allocate (CMOAG%coeff(Basis%Nbasis,Basis%Nbasis), CMOAG%eigval(Basis%Nbasis), &
                    CMOAG%occupancy(Basis%Nbasis), &
                    CMOBG%coeff(Basis%Nbasis,Basis%Nbasis), CMOBG%eigval(Basis%Nbasis), &
                    CMOBG%occupancy(Basis%Nbasis))
      end if
!
      call DEF_NoccMO (NoccMO, NAelectron, NBelectron)
      CMOAG%coeff=CMOG%coeff
      CMOAG%eigval=CMOG%eigval
      CMOBG%coeff=CMOG%coeff
      CMOBG%eigval=CMOG%eigval
      CMOAG%NoccMO=NAelectron
      CMOAG%occupancy(1:NAelectron)=ONE
      CMOAG%occupancy(NAelectron+1:Basis%Nbasis)=ZERO
      CMOBG%NoccMO=NBelectron
      if(NBelectron.gt.0)CMOBG%occupancy(1:NBelectron)=ONE
      CMOBG%occupancy(NBelectron+1:Basis%Nbasis)=ZERO
!
! End of routine BLD_GUESS_MO_UHF
      call PRG_manager ('exit', 'BLD_GUESS_MO_UHF', 'CMO_GUESS%UHF')
      return
      end subroutine BLD_GUESS_MO_UHF
