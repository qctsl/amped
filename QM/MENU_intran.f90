      subroutine SET_defaults_INTRAN
!*****************************************************************************************************************
!     Date last modified: March 31, 2000                                                            Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Set code defaults.                                                                            *
!*****************************************************************************************************************
! Modules:
      USE QM_defaults

      implicit none
!
! Local functions:
      MAX_transform=871200 ! Can transform up to 120 basis functions
      Nfrozen_core=NFrozen_cores()
      NFrozen_virt=0
!
! End of routine SET_default_group
      return
      end subroutine SET_defaults_INTRAN
      subroutine MENU_intran
!***********************************************************************
!     Date last modified: June 24, 1992                                *
!     Author: R.A. Poirier                                             *
!     Description: Set up integral transformation.                     *
!***********************************************************************
! Modules:
      USE program_parser
      USE menu_gets
      USE QM_defaults
      USE QM_objects
      USE integral_transformation

      implicit none
!
! Local scalar:
      integer :: NTMO
      logical done
!
! Begin:
      call PRG_manager ('enter', 'MENU_intran', 'UTILITY')
!
      done=.false.
!
! Make sure not combinations:
      integral_combinations=.false.
      I2E_type='RAW'
!
! Menu:
      do while (.not.done)
      call new_token (' Transform:')
!
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      ' Command TRAnsform:', &
      '   Purpose: performs integral transformation', &
      '   Syntax :', &
      '   TRAnsform', &
      '     GRoup :    MO groups for tranforming and sorting ', &
      '     GRoup = ( 1,1,1,1,2,2,2,2 ) , for example', &
      '     MAx_trasform = <integer>,', &
      '     MAx_trasform must be a multiple of Nbasis*Nbasis*(Nbasis+1)/2', &
      '   end'
!
! GRoup
      else if(token(1:2).EQ.'GR')then
        if(.not.allocated(MO_group))then
          allocate (MO_group(Nbasis))
          MO_group(1:NBasis)=0
        end if
        call GET_value (MO_group, NTMO)
!
! MAx_transform
      else if(token(1:2).EQ.'MA')then
        call GET_value (MAX_transform)

      else
        call MENU_end (done)
      end if
!
      end do !(.not.done)
!
! End of routine MENU_intran
      call PRG_manager ('exit', 'MENU_intran', 'UTILITY')
      return
      end
