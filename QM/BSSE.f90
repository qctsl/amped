      subroutine BLD_ENERGY_BSSE
!************************************************************************
!     Date last modified: December 12, 2002               Version 1.2   *
!     Author: Darryl Reid and R. A. Poirier                             *
!     Description: Compute the BSSE free energy                         *
!************************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_objects
      USE type_molecule
      USE type_basis_set
      USE GRAPH_objects

      implicit none
!
! Work arrays:
      type (molecular_orbital) :: CMOW, save 
!
! Local scalars:
      double precision :: E_BSSE_int,E_BSSE_AB,E_BSSE_Astar,E_BSSE_starB
      integer :: Iatom,Ibasis
!
! Begin:
      call PRG_manager ('enter', 'BLD_ENERGY_BSSE', 'ENERGY_BSSE%RHF')

      allocate (CMOW%coeff(BASIS%Nbasis,BASIS%Nbasis), CMOW%EIGVAL(BASIS%Nbasis), CMOW%occupancy(BASIS%Nbasis))

      call GET_object ('MOL', 'GRAPH', 'CNCOMP')

! Check for number of components
      if(NComponents.ne.2)then
        write(UNIout,*)'NComponents: ',NComponents
        write(UNIout,*)'More or less than two components cannot calculate BSSE'
        stop 'More or less than two components cannot calculate BSSE'
      end if

! Get Energy for A---B complex. Must be done first to define the full basis set.
      call GET_object ('QM', 'CMO', 'RHF')
      E_BSSE_AB=ENERGY_RHF%total
      CMOW%coeff(1:BASIS%Nbasis,1:BASIS%Nbasis)=CMOG%coeff(1:BASIS%Nbasis,1:BASIS%Nbasis) ! Save A---B CMOs
      CMOW%occupancy(1:BASIS%Nbasis)=CMOG%occupancy(1:BASIS%Nbasis)
      CMOW%NoccMO=CMOG%NoccMO

! Modify molecule
      do Iatom=1,Natoms
        if(CNCOMP(Iatom).eq.2)then
          CARTESIAN(Iatom)%atomic_number=-CARTESIAN(Iatom)%atomic_number
        end if! Atom belongs to molecule/component 1(A)
      end do! Iatom

! Get Energy for A---* complex (Component 2)
      call RESET_CMO (2)
      call GET_object ('QM', 'CMO', 'RHF')
      E_BSSE_Astar=ENERGY_RHF%total

! Modify molecule
      do Iatom=1,Natoms
        CARTESIAN(Iatom)%atomic_number=-CARTESIAN(Iatom)%atomic_number
      end do! Iatom

! Get Energy for *---B complex (Component 1)
      call RESET_CMO (1)
      call GET_object ('QM', 'CMO', 'RHF')
      E_BSSE_starB=ENERGY_RHF%total

! Change component B back
      do Iatom=1,Natoms
        if(CNCOMP(Iatom).eq.1)then
          CARTESIAN(Iatom)%atomic_number=-CARTESIAN(Iatom)%atomic_number
        end if! Atom belongs to molecule/component 2(B)
      end do! Iatom

! Restore CMOs to those of the A---B complex
      CMOG%coeff(1:BASIS%Nbasis,1:BASIS%Nbasis)=CMOW%coeff(1:BASIS%Nbasis,1:BASIS%Nbasis)
      CMOG%occupancy(1:BASIS%Nbasis)=CMOW%occupancy(1:BASIS%Nbasis)
      CMOG%NoccMO=CMOW%NoccMO

! Compute BSSE free interaction energy
      E_BSSE_int=E_BSSE_AB-E_BSSE_Astar-E_BSSE_starB
      write(UNIout,'(2(a,f12.6),a)')'E_interaction BSSE-free = ',E_BSSE_int,' hartree', &
                                     E_BSSE_int*hartree_to_kJ,' kJ/mol'
      call KILL_object ('QM', 'FOCK_MATRIX', 'RHF')
      call KILL_object ('INT', '1EINT', 'AO')
      call KILL_object ('QM', 'CMO', 'RHF')
      call GET_object ('QM', 'CMO', 'RHF')

      deallocate (CMOW%coeff, CMOW%EIGVAL, CMOW%occupancy)
!
! End of routine BLD_ENERGY_BSSE
      call PRG_manager ('exit', 'BLD_ENERGY_BSSE', 'ENERGY:BSSE%RHF')
      return
contains
      subroutine RESET_CMO (Icomponent)
!************************************************************************
!     Date last modified: December 12, 2002               Version 1.2   *
!     Author: R.A. Poirier                                              *
!     Description: Reset the MO for molecular subunits (monomers) BSSE  *
!************************************************************************
! Modules:

      implicit none
!
! Input scalar:
      integer Icomponent
!
! Local scalars:
      integer Iatom,NoccMO
!
! Begin:
!     call PRG_manager ('enter', 'RESET_CMO', 'UTILITY')

      call KILL_object ('QM', 'FOCK_MATRIX', 'RHF')
      call KILL_object ('INT', '1EINT', 'AO')
      call KILL_object ('QM', 'CMO', 'RHF')

! Restore CMOs to those of the A---B complex
      CMOG%coeff(1:BASIS%Nbasis,1:BASIS%Nbasis)=CMOW%coeff(1:BASIS%Nbasis,1:BASIS%Nbasis)
      CMOG%occupancy(1:BASIS%Nbasis)=CMOW%occupancy(1:BASIS%Nbasis)
      CMOG%NoccMO=CMOW%NoccMO
      do Ibasis=1,Basis%Nbasis
        if(CNCOMP(Basis%AOtoATOM(Ibasis)).eq.Icomponent)then
          CMOG%coeff(Ibasis,1:Basis%Nbasis)=ZERO
        end if!
      end do!Ibasis

! Calculate total number of electrons and number of alpha and beta electrons
      Nelectrons=0
      do Iatom=1,Natoms
      if(CARTESIAN(Iatom)%Atomic_number.GT.0.and.IABS(CARTESIAN(Iatom)%Atomic_number).NE.999)then
        Nelectrons=Nelectrons+IABS(CARTESIAN(Iatom)%Atomic_number)
      end if
      end do ! Iatom
!
      Nelectrons=Nelectrons-MOLECULE%charge
      NAelectrons=(Nelectrons+Multiplicity-1)/2
      NBelectrons=(Nelectrons-Multiplicity+1)/2
      NoccMO=NAelectrons
!
      if(Multiplicity.LE.0.or. &
        NAelectrons+NBelectrons.NE.Nelectrons.or. &
        NAelectrons.LE.0.or. &
        NBelectrons.LT.0)then     ! Charge/Multiplicity error.
        write(UNIout,'(a)')'ERROR> DEF_NoccMO:'
        write(UNIout,'(a)')'CHARGE/MULTIPLICITY ERROR'
        call PRG_stop ('DEF_NoccMO> CHARGE/MULTIPLICITY ERROR')
      end if

      CMOG%NoccMO=NoccMO            ! For now
      CMOG%occupancy(1:CMOG%NoccMO)=TWO
      CMOG%occupancy(CMOG%NoccMO+1:BASIS%Nbasis)=ZERO
!
! End of routine RESET_CMO
!     call PRG_manager ('exit', 'RESET_CMO', 'UTILITY')
      return
      end subroutine RESET_CMO
      end subroutine BLD_ENERGY_BSSE
