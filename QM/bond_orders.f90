      subroutine BLD_Bond_order_Meyer
!***********************************************************************
!     Date last modified: April 26, 1995                   Version 1.0 *
!     Author: R.A. Poirier, James Xidos                                *
!     Description: Bond orders and valences according to I. Mayer,     *
!                  Chem. Phys. Letters, 97, 270 (1983) and             *
!                  Can. J. Chem., Special Sandorfy Issue (1985).       *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_basis_set
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE INT_objects

      implicit none
!
! Local scalars:
      integer Iatom,Jatom,Ibasis,Jbasis,IJ
      integer k,l
!
! Local work arrays:
      double precision, dimension(:,:), allocatable :: PSw
      double precision, dimension(:,:), allocatable :: Pw
      double precision, dimension(:,:), allocatable :: Sw
!
! Begin:
      call PRG_manager ('enter', 'BLD_Bond_order_Meyer', 'BOND_ORDER%MEYER')

!     call GET_object ('INT','1EINT','AO')
      call GET_object ('QM','DENSITY_1MATRIX',Wavefunction(1:len_trim(Wavefunction)))

      if(.not.allocated(Bond_Order))then
        allocate(Bond_Order(Natoms,Natoms), Valences(Natoms,2))
      else
        deallocate(Bond_Order, Valences)
        allocate(Bond_Order(Natoms,Natoms), Valences(Natoms,2))
      end if
      allocate (PSw(1:Nbasis,1:Nbasis), Pw(1:Nbasis,1:Nbasis), Sw(1:Nbasis,1:Nbasis))

      do Ibasis=1,Nbasis
      do Jbasis=1,Ibasis
        IJ=Ibasis*(Ibasis-1)/2+Jbasis
        Pw(Ibasis,Jbasis)=PM0(IJ)
        Pw(Jbasis,Ibasis)=Pw(Ibasis,Jbasis)
        Sw(Ibasis,Jbasis)=OVRLAP(IJ)
        Sw(Jbasis,Ibasis)=Sw(Ibasis,Jbasis)
      end do ! Jbasis
      end do ! Ibasis

! Form PS
      PSw=matmul(Pw, Sw)
!
      if(.not.allocated(Bond_Order))then
        allocate(Bond_Order(Natoms,Natoms), Valences(Natoms,2))
      end if
!
      Valences(1:Natoms,1)=ZERO
      Bond_order(1:Natoms,1:Natoms)=ZERO
!
!     Closed shell bond orders and valences.
      do K=1,Nbasis
        Iatom=Basis%AOtoATOM(K)
        if(Iatom.NE.0)then
        Valences(Iatom,1)=Valences(Iatom,1)+TWO*PSw(K,K)
        do L=1,Nbasis
          Jatom=Basis%AOtoATOM(L)
          if(Jatom.NE.0)then
          if(iatom.eq.jatom)then
            Valences(Iatom,1)=Valences(Iatom,1)-PSw(K,L)*PSw(L,K)
          else
            Bond_order(Iatom,Jatom)=Bond_order(Iatom,Jatom)+PSw(K,L)*PSw(L,K)
          end if
          end if ! Jatom
        end do ! L
        end if ! Iatom
      end do ! K
!
!     Open shell bond orders, valences and free valences.
!     For now, open shell valences and bond orders are unavailable.
!     For now, initialize Valences(X,2) to Valences(X,1).
      do Iatom=1,Natoms
        Valences(Iatom,2) = Valences(Iatom,1)
      end do

      deallocate (PSw, Pw, Sw)
!
! End of routine BLD_Bond_order
      call PRG_manager ('exit', 'BLD_Bond_order_Meyer', 'BOND_ORDER%MEYER')
      return
      end subroutine BLD_Bond_order_Meyer
      subroutine PRT_Bond_order_Meyer
!***********************************************************************
!     Date last modified: April 26, 1995                   Version 1.0 *
!     Author: R.A. Poirier, James Xidos                                *
!     Description: Bond orders and valences according to I. Mayer,     *
!                  Chem. Phys. Letters, 97, 270 (1983) and             *
!                  Can. J. Chem., Special Sandorfy Issue (1985).       *
!***********************************************************************
! Modules:
      USE program_files
      USE matrix_print
      USE QM_objects

      implicit none
!
! Local scalars:
      integer :: Natoms
!
      if(MUN_prtlev.gt.0)write(UNIout,1000)
 1000 FORMAT(52('*')/' BOND ORDER AND VALENCE INDICES ACCORDING TO I.MAYER'/52('*'))
      Natoms=size(Bond_order,1)
      write(UNIout,*)' BOND ORDER MATRIX FOR: '
      call PRT_matrix (Bond_order, Natoms, Natoms)
      write(UNIout,*)'VALENCE: '
      call PRT_matrix (Valences, Natoms, 1)
 
      return
      end subroutine PRT_Bond_order_Meyer
!#######################################################################################################################
      subroutine BLD_Bond_order_MOPAC_PM3
!******************************************************************
!     Date created: February 27, 2014                             *
!     Author:  O. Stueker                                         *
!     Description: Wrapper to calls BLD_Bond_order_MOPAC('PM3')   *
!                  Also checks if PM3 parameters are available    *
!                  for all elements in the molecule.              *
!******************************************************************
! Modules
      USE program_constants
      USE program_files
      USE type_molecule
      USE QM_objects

      IMPLICIT NONE
!
! Local scalars:
      logical, dimension(107) :: pm3_atoms
      integer :: Iatom

! Initialize
      pm3_atoms=.false.
!
! Begin:
      call PRG_manager ('enter', 'BLD_Bond_order_MOPAC_PM3', 'BOND_ORDER%PM3')

      ! MOPAC7 contains PM3 parameters for these atoms:
      pm3_atoms( 1)=.true. !  H
      pm3_atoms( 4)=.true. ! Be
      pm3_atoms( 6)=.true. !  C
      pm3_atoms( 7)=.true. !  N
      pm3_atoms( 8)=.true. !  O
      pm3_atoms( 9)=.true. !  F
      pm3_atoms(12)=.true. ! Mg
      pm3_atoms(13)=.true. ! Al
      pm3_atoms(14)=.true. ! Si
      pm3_atoms(15)=.true. !  P
      pm3_atoms(16)=.true. !  S
      pm3_atoms(17)=.true. ! Cl
      pm3_atoms(30)=.true. ! Zn
      pm3_atoms(31)=.true. ! Ga
      pm3_atoms(32)=.true. ! Ge
      pm3_atoms(33)=.true. ! As
      pm3_atoms(34)=.true. ! Se
      pm3_atoms(35)=.true. ! Br
      pm3_atoms(48)=.true. ! Cd
      pm3_atoms(49)=.true. ! In
      pm3_atoms(50)=.true. ! Sn
      pm3_atoms(51)=.true. ! Sb
      pm3_atoms(52)=.true. ! Te
      pm3_atoms(53)=.true. !  I
      pm3_atoms(80)=.true. ! Hg
      pm3_atoms(81)=.true. ! Tl
      pm3_atoms(82)=.true. ! Pb
      pm3_atoms(83)=.true. ! Bi
      pm3_atoms(102)=.true.!/Cb: Capped Bond

      ! check atoms
      do Iatom=1,Natoms
        if(CARTESIAN(Iatom)%Atomic_number.le.0) cycle
        if(.NOT.pm3_atoms(CARTESIAN(Iatom)%Atomic_number)) then
          write(UNIout,'(a,i2)') " ERROR> No PM3 parameter for element with atomic number ", &
                       CARTESIAN(Iatom)%Atomic_number
          call PRG_stop(' Missing PM3 Parameter')
        endif
      end do

      ! call the bond order calculation
      call BLD_Bond_order_MOPAC('PM3')

! end of routine  BLD_Bond_order_PM3
      call PRG_manager ('exit', 'BLD_Bond_order_MOPAC_PM3', 'BOND_ORDER%PM3')
      end subroutine BLD_Bond_order_MOPAC_PM3

      subroutine BLD_Bond_order_MOPAC_PM6
!******************************************************************
!     Date created: February 27, 2014                             *
!     Author:  O. Stueker                                         *
!     Description: Wrapper to calls BLD_Bond_order_MOPAC('PM6')   *
!******************************************************************
      IMPLICIT NONE
! Begin:
      call PRG_manager ('enter', 'BLD_Bond_order_MOPAC_PM6', 'BOND_ORDER%PM6')

      ! call the bond order calculation
      call BLD_Bond_order_MOPAC('PM6')

! end of routine  BLD_Bond_order_PM3
      call PRG_manager ('exit', 'BLD_Bond_order_MOPAC_PM6', 'BOND_ORDER%PM6')
      end subroutine BLD_Bond_order_MOPAC_PM6

      subroutine BLD_Bond_order_MOPAC (level)
!******************************************************************
!     Date created: November 13, 2013                             *
!     Author:  O. Stueker                                         *
!     Description: calculate bondorders and valences              *
!                  by running a singlepoint PM3 job with MOPAC    *
!******************************************************************
! Modules
      USE program_constants
      USE program_files
      USE type_molecule
      USE QM_objects

      IMPLICIT NONE
      character(len=3), intent(in) :: level

!
! Local scalars:
      integer :: datFile_Unit
      integer :: lenEnv, lenPID, rcode
      integer :: Iatom, NrealAtoms
      character(len=MAX_string) :: mopDatName, mopOutName
      logical :: Lerror
      character(len=MAX_string) :: pid

! Initialize
      NrealAtoms=0
      pid = "         "

!
! Begin:
      call PRG_manager ('enter', 'BLD_Bond_order_MOPAC', 'UTILITY')


!     Write MOPAC input file:

!TODO:  generate filename based on name of log-file
      call sysinf (pid, 'PID', lenPID, rcode)
      mopDatName = "mopac__"//pid(1:lenPID)//".dat"
      mopOutName = mopDatName
      mopOutName(index(mopOutName,".dat"):len(mopOutName))=".out"

!     get FileUnit and open the file
      call GET_unit (mopDatName, datFile_Unit, Lerror)
      PRG_file(datFile_Unit)%status='REPLACE '
      PRG_file(datFile_Unit)%name=mopDatName
      PRG_file(datFile_Unit)%form='FORMATTED'
      PRG_file(datFile_Unit)%type='MopacDat'
      call FILE_open (datFile_Unit)
!     write MOPAC Keyword line
      write(datFile_Unit,'(a)')' '//level//' XYZ NOMM GEO-OK BONDS '
!     Write two comment lines (1: molecule title, 2: empty)
      write(datFile_Unit,'(x,a/)')MOL_TITLE(1:len_trim(MOL_title))

!     Write CARTESIAN coordinates
      do Iatom=1,Natoms
        if(CARTESIAN(Iatom)%Atomic_number.le.0) cycle ! if DUMMY atom
        NrealAtoms = NrealAtoms+1
        write(datFile_Unit,'(X,a4,X,3(F14.8," 0 "))')&
              CARTESIAN(Iatom)%ELEMENT, &
              CARTESIAN(Iatom)%X*bohr_to_angstrom, &
              CARTESIAN(Iatom)%Y*bohr_to_angstrom, &
              CARTESIAN(Iatom)%Z*bohr_to_angstrom

      end do ! Iatom
      write(datFile_Unit,'()')
!     Close the MOPAC input file
      close(datFile_Unit)

!     execute MOPAC
      call EXEPRG (trim(mopac_bin), mopDatName, ">"//mopOutName, .false.)


!      Read MOPAC output file
!      and extract bond orders
      call readBondOrdersFromMopacOut(mopOutName, NrealAtoms)

!
! end of routine  BLD_Bond_order_MOPAC
      call PRG_manager ('exit', 'BLD_Bond_order_MOPAC', 'UTILITY')
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine readBondOrdersFromMopacOut(mopOutName, Natoms)
!***************************************************************************************
!     Date last modified: November 25, 2013                                            *
!     Author:  O. Stueker                                                              *
!     Description: extract bond orders and valences from MOPAC output file             *
!***************************************************************************************
! Modules
      USE program_constants
      USE program_files
      USE QM_objects
      USE ISO_FORTRAN_ENV, ONLY : ERROR_UNIT


      implicit none

      ! Input scalars:
      character(len=MAX_string), intent(in) :: mopOutName
      integer, intent(in) :: Natoms

      ! Local scalars:
      integer :: ReadUnit, io_stat
      logical :: Lerror, LfoundBonding
      character(len=MAX_string) :: line
      character(len=5) :: element
      integer :: atomNum
      integer :: iAtom, iAtom1, iAtom2, k
      integer :: iBlock, firstInBlock, lastInBlock
      integer, parameter :: nCols = 6

      ! initialize
      LfoundBonding = .false.
      if(.not.allocated(Bond_Order))then
        allocate(Bond_Order(Natoms,Natoms), Valences(Natoms,2))
      else
        deallocate(Bond_Order, Valences)
        allocate(Bond_Order(Natoms,Natoms), Valences(Natoms,2))
      end if
      !
      Valences(1:Natoms,1)=ZERO
      Bond_order(1:Natoms,1:Natoms)=ZERO

!     get FileUnit and open the file
      call GET_unit (mopOutName, ReadUnit, Lerror)
      PRG_file(ReadUnit)%status='OLD '
      PRG_file(ReadUnit)%name=mopOutName
      PRG_file(ReadUnit)%form='FORMATTED'
      PRG_file(ReadUnit)%type='MopacOut'
      PRG_file(ReadUnit)%action='READ'
      call FILE_open (ReadUnit)

      read_loop: do k=1,1
      find_bo: do
        ! read next line
        read(ReadUnit,fmt='(a80)', iostat=io_stat)line
        if (io_stat.lt.0) exit read_loop

        if (index(line,"DEGREES OF BONDING").gt.0) then
          ! FOUND MOPAC7
          LfoundBonding = .true.
          exit find_bo

        else if (index(line,"BOND ORDERS AND VALENCIES").gt.0) then
          ! FOUND MOPAC2012
          LfoundBonding = .true.
          exit find_bo

        end if
      end do find_bo

      skip1: do ! fast forward to dashed line
        read(ReadUnit,fmt='(a80)', iostat=io_stat)line
        if (io_stat.lt.0)         exit read_loop
        if (line(1:5).eq.' ----') exit skip1
      end do skip1

      if (LfoundBonding) then
      ! found Matrix with Bondorders
        iBlock=1
        readBlock: do ! read block by block
          firstInBlock=((iBlock-1)*nCols)+1
          lastInBlock=MIN(firstInBlock+nCols-1,Natoms)

          do iAtom=firstInBlock,Natoms
            read(ReadUnit,'(a80)', iostat=io_stat) line
            if (io_stat.lt.0) exit read_loop
            if (len_trim(line).lt.10) then ! sometimes the blocks are broken up
              skip2: do ! fast forward to dashed line
                read(ReadUnit,fmt='(a80)', iostat=io_stat)line
                if (io_stat.lt.0)         exit read_loop
                if (line(1:5).eq.' ----') exit skip2
              end do skip2
              read(ReadUnit,fmt='(a80)', iostat=io_stat)line
              if (io_stat.lt.0) exit read_loop
            end if
            read(line, '(X,a5,X,i4, 6(X,F10.6))')element, atomNum, &
                            Bond_Order( iAtom, firstInBlock:lastInBlock )
          end do !  i=firstInBlock,Natoms

          ! block done: skip next lines
          skip3: do ! fast forward to dashed line
            read(ReadUnit,fmt='(a80)', iostat=io_stat)line
            if (io_stat.lt.0)         exit read_loop
            if (line(1:5).eq.' ----') exit skip3
          end do skip3

          if ( Natoms.le.lastInBlock ) exit readBlock ! have all blocks
          iBlock=iBlock+1
        end do readBlock
      end if ! (LfoundBonding)

      exit read_loop
      end do read_loop
      close(ReadUnit)

      if (.NOT.LfoundBonding) then
        write(UNIout, '(a)') " ERROR> MOPAC BOND ORDERS: Can't find bond orders in MOPAC output."
        call PRG_stop("ERROR> MOPAC BOND ORDERS: Can't find bond orders in MOPAC output.")
      end if

      ! make matrix symmetrical
      ! MOPAC only stores lower half and valences on the diagonal
      do iAtom1=1,nAtoms
      do iAtom2=1,nAtoms
        if (iAtom1.eq.iAtom2) then
          ! store Valences in separate array
          Valences(iAtom1,1)=Bond_Order(iAtom1,iAtom2)
          Bond_Order(iAtom1,iAtom2)=0.0
        else if (Bond_Order(iAtom1,iAtom2).gt.0.0) then
          Bond_Order(iAtom2,iAtom1)=Bond_Order(iAtom1,iAtom2)
        end if
      end do ! iAtom2=1,nAtoms
      end do ! iAtom1=1,nAtoms

! end of subroutine readBondOrdersFromMopacOut
      end subroutine readBondOrdersFromMopacOut
      end subroutine BLD_Bond_order_MOPAC

      subroutine PRT_Bond_order_PM3
!******************************************************************
!     Date created: November 13, 2013                             *
!     Author:  O. Stueker                                         *
!     Description: Print bondorders and valences                  *
!                  from singlepoint PM3 job with MOPAC            *
!******************************************************************

! Modules:
      USE program_files
      USE matrix_print
      USE QM_objects

      implicit none
!
! Local scalars:
      integer :: Natoms
!
      if(MUN_prtlev.gt.0)write(UNIout,1000)
 1000 FORMAT(52('*')/' BOND ORDER AND VALENCE INDICES FROM PM3 (MOPAC)'/52('*'))
      Natoms=size(Bond_order,1)
      write(UNIout,*)' BOND ORDER MATRIX FOR: '
      call PRT_matrix (Bond_order, Natoms, Natoms)
      write(UNIout,*)'VALENCE: '
      call PRT_matrix (Valences, Natoms, 1)

      return
      end subroutine PRT_Bond_order_PM3

      subroutine PRT_Bond_order_PM6
!******************************************************************
!     Date created: February 27, 2013                             *
!     Author:  O. Stueker                                         *
!     Description: Print bondorders and valences                  *
!                  from singlepoint PM6 job with MOPAC            *
!******************************************************************

! Modules:
      USE program_files
      USE matrix_print
      USE QM_objects

      implicit none
!
! Local scalars:
      integer :: Natoms
!
      if(MUN_prtlev.gt.0)write(UNIout,1000)
 1000 FORMAT(52('*')/' BOND ORDER AND VALENCE INDICES FROM PM6 (MOPAC)'/52('*'))
      Natoms=size(Bond_order,1)
      write(UNIout,*)' BOND ORDER MATRIX FOR: '
      call PRT_matrix (Bond_order, Natoms, Natoms)
      write(UNIout,*)'VALENCE: '
      call PRT_matrix (Valences, Natoms, 1)

      return
      end subroutine PRT_Bond_order_PM6
