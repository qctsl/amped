      MODULE QM_objects
!******************************************************************************************
!     Date last modified: August 11, 2014                                                 *
!     Author: R.A. Poirier                                                                *
!     Description: QM Objects.                                                            *
!******************************************************************************************
! Modules:
!     USE INT_objects

      implicit none

      integer :: NBasis                    ! Number of basis functions
      integer :: MATlen                    ! Nbasis*(Nbasis+1)/2
!
! Dipole moment
      double precision, dimension(:,:), allocatable :: DipoleX_MO
      double precision, dimension(:,:), allocatable :: DipoleY_MO
      double precision, dimension(:,:), allocatable :: DipoleZ_MO

      double precision, dimension(:,:), allocatable :: SecMomXX_MO
      double precision, dimension(:,:), allocatable :: SecMomXY_MO
      double precision, dimension(:,:), allocatable :: SecMomXZ_MO
      double precision, dimension(:,:), allocatable :: SecMomYY_MO
      double precision, dimension(:,:), allocatable :: SecMomYZ_MO
      double precision, dimension(:,:), allocatable :: SecMomZZ_MO

      type dipole_moment
        double precision :: X
        double precision :: Y
        double precision :: Z
        double precision :: total
      end type dipole_moment

      type (dipole_moment) Dipole
      type (dipole_moment) Dipole_elec

!
      type energy_components
        double precision :: nuclear ! Nuclear repulsion energy
        double precision :: total   ! total energy = HF+MP2+...
        double precision :: HF      ! HF energy (RHF, ROHF, UHF, GVB)
        double precision :: elec    ! Electronic energy only
        double precision :: MP2     ! MP2 energy
        character(len=32) :: Wavefunction ! Wavefunction (RHF/basis_set, ...), i.e., RHF/6-31G(d)
      end type energy_components

      type (energy_components), save, target :: ENERGY_RHF
      type (energy_components), save, target :: ENERGY_UHF
      type (energy_components), save, target :: ENERGY_ROHF
      type (energy_components), save, target :: ENERGY_GVB

      type (energy_components), save, pointer :: ENERGY => null() ! generic name
!
! Electronic energy components:
      double precision :: E_V_RHF
      double precision :: E_T_RHF
      double precision :: E_TV_RHF
      double precision :: E_COUL_RHF
      double precision :: E_EXCH_RHF

      double precision :: E_V_ROHF
      double precision :: E_T_ROHF
      double precision :: E_TandV_ROHF
      double precision :: E_COUL_ROHF
      double precision :: E_EXCH_ROHF
!
      integer :: NFOCK                    ! Number of Fock matrices

      type molecular_orbital
        integer :: NoccMO                   ! Number of occupied MO
        integer :: NFcoreMO                 ! Number of frozen core doubly occupied MO
        double precision, dimension(:,:), pointer :: coeff => null()
        double precision, dimension(:),   pointer :: eigval => null()
        double precision, dimension(:),   pointer :: occupancy => null()
      end type molecular_orbital

      type (molecular_orbital), save, pointer :: CMO => null()
      type (molecular_orbital), save :: CMOA
      type (molecular_orbital), save :: CMOB
      type (molecular_orbital), save, target :: CMO_ROHF
      type (molecular_orbital), save, target :: CMO_RHF
      type (molecular_orbital), save, target :: CMO_GVB
      type (molecular_orbital), save, target :: CMOH    ! Huckel MO or CMO for DiagH
!
! CMO used to create an intial guess
      type (molecular_orbital), save :: CMOG ! RHF Guess MO
      type (molecular_orbital), save :: CMOG_GVB ! GVB guess MO
      type (molecular_orbital), save :: CMOAG ! Alpha spin guess MO
      type (molecular_orbital), save :: CMOBG ! Beta spin guess MO
      type (molecular_orbital), save :: CMOP ! CMO to be projected
!
! Delta NO and related
      double precision, allocatable, dimension(:) :: on, vn  ! occupation and vacancy numbers
      double precision, allocatable, dimension(:,:) :: NO    ! natural orbital coefficients

! Output:
      double precision, dimension(:,:), allocatable :: LAGRGM ! 'LAGrange_AO_GVB', Nbasis,NBASIS
      double precision, dimension(:,:), allocatable :: VECCC  ! 'VECTOR_COUPLING_COEFF', NFOCK,NFOCK+2
      double precision, dimension(:,:), allocatable :: VECCCN  ! 'VECTOR_COUPLING_COEFF', NFOCK,NFOCK+2
!
! UHF Density matrices:
      double precision, dimension(:), save, allocatable, target :: PM0G           ! Guess density
      double precision, dimension(:), save, allocatable :: PM0_alpha
      double precision, dimension(:), save, allocatable :: PM0_beta
      double precision, dimension(:), save, allocatable :: PM0HF_alpha
      double precision, dimension(:), save, allocatable :: PM0HF_beta

! RHF/ROHF Density
      double precision, dimension(:), allocatable, save, target :: PM0_RHF
      double precision, dimension(:), allocatable, save, target :: PM0_ROHF
!     double precision, dimension(:), save, pointer :: PM0_ROHF

      double precision, dimension(:), pointer :: PM0  => null()           ! Points to the appropriate density

! Work arrays (SCF):
      double precision, dimension(:), save, allocatable :: PEWD           ! Energy weighted density
      double precision, dimension(:), save, allocatable :: PM0_full       ! Density matrix (GVB ...)

      double precision, dimension(:), allocatable :: FockM    ! Fock matrix
      double precision, dimension(:,:), allocatable :: FockMN    ! NEW Fock matrix
      double precision, dimension(:,:), allocatable :: FQSM  ! Projected Fock matrix

! Work arrays (used by RHCCLC, required for Direct SCF):
      double precision, dimension(:), allocatable :: DGM      ! delta G matrix
!
      double precision, dimension(:,:), allocatable :: Bond_order ! Natoms x Natoms
      double precision, dimension(:,:), allocatable :: Valences ! Natoms x 2

      double precision, dimension(:), allocatable :: VibFreq
      double precision, dimension(:,:), allocatable :: Force_constant
!
      integer, dimension(:), allocatable :: LMOLST    ! List of MO to be localized
      type (molecular_orbital), save, target :: LMO
      double precision, dimension(:,:), allocatable :: TCLMO  ! Transformation matrix
      double precision, dimension(:), allocatable :: LMOCCX
      double precision, dimension(:), allocatable :: LMOCCY
      double precision, dimension(:), allocatable :: LMOCCZ
      double precision, dimension(:), allocatable :: LEONE  !

      double precision, dimension(:), allocatable :: Atomic_Kinetic
      double precision, dimension(:), allocatable :: Atomic_Vne
      double precision, dimension(:), allocatable :: Atomic_Coulomb
      double precision, dimension(:), allocatable :: Atomic_Exchange

      double precision, dimension(:), allocatable :: J_Atomic,JHF_atomic,JAA_Atomic
      double precision, dimension(:), allocatable :: K_Atomic,KHF_atomic
      double precision, dimension(:), allocatable :: Vee_Atomic,VeeHF_Atomic
      double precision :: J_total,K_total,Vee_total,JAA_total
      double precision :: JHF_total,KHF_total,VeeHF_total
      double precision, dimension(:), allocatable :: rho_0_Atom

      type SizeandShape
        double precision :: XX
        double precision :: YY
        double precision :: ZZ
        double precision :: Size
      end type SizeandShape
      type (SizeandShape) :: SizeShape
!
! 2RDM and two-electron properties
      double precision, dimension(:), allocatable :: TRDM_HF_AOaa, TRDM_AOaa
      double precision, dimension(:), allocatable :: TRDM_HF_MBaa, TRDM_MBaa
      double precision, dimension(:), allocatable :: TRDM_HF_AObb, TRDM_AObb
      double precision, dimension(:), allocatable :: TRDM_HF_MBbb, TRDM_MBbb
      double precision, dimension(:), allocatable :: TRDM_HF_AOab, TRDM_AOab
      double precision, dimension(:), allocatable :: TRDM_HF_MBab, TRDM_MBab
      double precision, dimension(:), allocatable :: TRDM_HF_AOba, TRDM_AOba
      double precision, dimension(:), allocatable :: TRDM_HF_MBba, TRDM_MBba
      double precision, dimension(:), allocatable :: TRDM_HF_MOaa, TRDM_MOaa
      double precision, dimension(:), allocatable :: TRDM_HF_MObb, TRDM_MObb
      double precision, dimension(:), allocatable :: TRDM_HF_MOab, TRDM_MOab
      double precision, dimension(:), allocatable :: TRDM_HF_MOba, TRDM_MOba
      character(len=32) :: ONERDMfile, TWORDMfile, MOfile
      double precision, dimension(:), allocatable :: MO4prod, MOd2prod
      double precision, dimension(:,:), allocatable :: MO4prodJ, MO4prodK
      double precision, dimension(:,:), allocatable :: MO4prodJgrad, MO4prodKgrad
      double precision, dimension(:,:), allocatable :: MO4ulapJ, MO4ulapL, MO4RlapJ, MO4RlapL
      double precision, dimension(:,:,:), allocatable :: MO4ugradJ, MO4ugradL, MO4RgradJ, MO4RgradL
      double precision, dimension(:,:,:), allocatable :: MO4rfgradJ, MO4rfgradL
      double precision, dimension(:,:), allocatable :: MO4rflapJ, MO4rflapL
      double precision, dimension(:,:,:,:), allocatable :: CFT2RDMaa, CFT2RDMbb, CFT2RDMab, CFT2RDMba
      double precision, dimension(:,:), allocatable :: R2RDMaa, R2RDMbb, R2RDMJab, R2RDMLab
      double precision, dimension(:,:), allocatable :: R2RDMJba, R2RDMLba, R2RDMJos, R2RDMLos
      integer :: Nnatorb
      logical :: LRDMcore
!
! Equi- and Antimomentum
      double precision, dimension(:), allocatable :: EMaa_atomic,EMHFaa_atomic
      double precision, dimension(:), allocatable :: AMaa_atomic,AMHFaa_atomic
      double precision, dimension(:), allocatable :: MBaa_atomic,MBHFaa_atomic
      double precision :: EMaa_total, EMHFaa_total
      double precision :: AMaa_total, AMHFaa_total
      double precision :: MBaa_total, MBHFaa_total
      double precision, dimension(:), allocatable :: EMbb_atomic,EMHFbb_atomic
      double precision, dimension(:), allocatable :: AMbb_atomic,AMHFbb_atomic
      double precision, dimension(:), allocatable :: MBbb_atomic,MBHFbb_atomic
      double precision :: EMbb_total, EMHFbb_total
      double precision :: AMbb_total, AMHFbb_total
      double precision :: MBbb_total, MBHFbb_total
      double precision, dimension(:), allocatable :: EMab_atomic,EMHFab_atomic
      double precision, dimension(:), allocatable :: AMab_atomic,AMHFab_atomic
      double precision, dimension(:), allocatable :: MBab_atomic,MBHFab_atomic
      double precision :: EMab_total, EMHFab_total
      double precision :: AMab_total, AMHFab_total
      double precision :: MBab_total, MBHFab_total
      double precision, dimension(:), allocatable :: EMba_atomic,EMHFba_atomic
      double precision, dimension(:), allocatable :: AMba_atomic,AMHFba_atomic
      double precision, dimension(:), allocatable :: MBba_atomic,MBHFba_atomic
      double precision :: EMba_total, EMHFba_total
      double precision :: AMba_total, AMHFba_total
      double precision :: MBba_total, MBHFba_total
!
! Relative angular momentum
      double precision, dimension(:), allocatable :: RAMaa_atomic,RAMHFaa_atomic
      double precision :: RAMaa_total, RAMHFaa_total
      double precision, dimension(:), allocatable :: RAMbb_atomic,RAMHFbb_atomic
      double precision :: RAMbb_total, RAMHFbb_total
      double precision, dimension(:), allocatable :: RAMab_atomic,RAMHFab_atomic
      double precision :: RAMab_total, RAMHFab_total
      double precision, dimension(:), allocatable :: RAMba_atomic,RAMHFba_atomic
      double precision :: RAMba_total, RAMHFba_total
!
! Extracule density
      double precision, dimension(:), allocatable :: extaa_atomic,extHFaa_atomic
      double precision :: extaa_total, extHFaa_total
      double precision, dimension(:), allocatable :: extbb_atomic,extHFbb_atomic
      double precision :: extbb_total, extHFbb_total
      double precision, dimension(:), allocatable :: extab_atomic,extHFab_atomic
      double precision :: extab_total, extHFab_total
      double precision, dimension(:), allocatable :: extba_atomic,extHFba_atomic
      double precision :: extba_total, extHFba_total
!
! spin-density
      double precision, dimension(:), allocatable :: rhoa_atomic,rhoHFa_atomic
      double precision, dimension(:), allocatable :: rhob_atomic,rhoHFb_atomic
      double precision :: rhoa_total, rhoHFa_total
      double precision :: rhob_total, rhoHFb_total
!
! Vne density
      double precision, dimension(:), allocatable :: Vnea_atomic,VneHFa_atomic
      double precision, dimension(:), allocatable :: Vneb_atomic,VneHFb_atomic
      double precision :: Vnea_total, VneHFa_total
      double precision :: Vneb_total, VneHFb_total
!
! T density
      double precision, dimension(:), allocatable :: Tpa_atomic,TpHFa_atomic
      double precision, dimension(:), allocatable :: Tpb_atomic,TpHFb_atomic
      double precision :: Tpa_total, TpHFa_total
      double precision :: Tpb_total, TpHFb_total
!
      double precision, dimension(:), allocatable :: Tsa_atomic,TsHFa_atomic
      double precision, dimension(:), allocatable :: Tsb_atomic,TsHFb_atomic
      double precision :: Tsa_total, TsHFa_total
      double precision :: Tsb_total, TsHFb_total
!
! two-electron density functional
      double precision :: q_TDF
!
! mass of 'electron'
      double precision :: masse
!
      end MODULE QM_objects
