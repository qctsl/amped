      subroutine MENU_QM
!***********************************************************************
!     Date last modified: January  28, 1994                Version 1.0 *
!     Author: R. A. Poirier                                            *
!     Description: Set up Basis.                                       *
!***********************************************************************
! Modules:
      USE program_constants
      USE program_parser
      USE menu_gets
      USE type_basis_set
      USE QM_defaults
      USE QM_objects

      implicit none
!
! Local scalars:
      integer :: lenstr
      logical done,LPRTBAS
      character(len=8) :: Level
      character(len=128) :: string
!
! Begin:
! Menu:
      LPRTBAS=.false.
      done=.false.
      do while (.not.done)
        call new_token(' QM/MEthod:')
        if(token(1:4).eq.'HELP')then
          write(UNIout,'(a)') &
      ' Command QM/MEthod :', &
      '   Purpose: Define the level/basis set', &
      '   Syntax :', &
      '   LEvel = <name>: e.g.  HF, ROHF, B3LYP, CISD, CCSD(T) ... ', &
      '   BAsis = <name>: e.g.  3-21G, 6-31G(d,p), ... ', &
      '   PRrint : print the basis set (default=no print)', &
      '   end'
!
! LEvel
        else if(token(1:2).eq.'LE') then
          call GET_value (Level, lenstr)
          Wavefunction=Level(1:lenstr)
          Level_of_Theory=Level(1:lenstr)
! BAsis
        else if(token(1:2).eq.'BA') then
          call GET_value (BASIS_name, lenstr)
! correlation
        else if(token(1:4).eq.'CORR') then
          call GET_value (string, lenstr)
          correlation = string
! 2-RDM input file
        else if(token(1:3).EQ.'TWO')then
          call GET_value (string, lenstr)
          twordmfile = string
! 1-RDM input file
        else if(token(1:3).EQ.'ONE')then
          call GET_value (string, lenstr)
          onerdmfile = string
! 2-RDM core on/off
        else if(token(1:4).EQ.'CORE')then
          call GET_value (string, lenstr)
          if(string.eq.'OFF')then
            LRDMcore = .false. ! true by default
          end if
! MO coefficient input file
        else if(token(1:3).EQ.'MOF')then
          call GET_value (string, lenstr)
          mofile = string
! Print
        else if(token(1:2).eq.'PR') then
          LPRTBAS=.TRUE.
! Mass of 'electron'
        else if(token(1:2).EQ.'MA')then
          call GET_value (masse)
          write(UNIout,'(a,F12.6)')'Electron mass set to: ',masse
!
! TDF parameter
        else if(token(1:1).EQ.'Q')then
          call GET_value (q_TDF)
!
        else
          call MENU_end (done)
        end if
      end do ! while
!
      call GET_basis_set (BASIS_name, Current_Basis, LPRTBAS)
      Basis=>Current_Basis
      Nbasis=Basis%Nbasis
      MATlen=Nbasis*(Nbasis+1)/2

      call RESET_basis_set

      if(.not.LPRTBAS)then
        write(UNIout,'(2a,I8/)')Basis%name(1:len_trim(Basis%name)), &
           ' Basis Set - Total number of basis functions: ',Basis%Nbasis
      end if

!      if(LBasis_defined)then
!        call PRG_reset
!      end if

      LBasis_defined=.true.
!
! End of routine MENU_QM
      return
      end subroutine MENU_QM
