      subroutine Mulliken_Population
!**************************************************************************
!     Date last modified: October 5, 2000                     Version 2.0 *
!     Author: R.A. Poirier                                                *
!     Description: Mulliken population analysis.                          *
!**************************************************************************
! Modules:
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE pop_analysis 

      implicit none
!
! Local scalars:
!
! Begin:
      call PRG_manager ('enter', 'Mulliken_Population', 'AO_BY_AO%MULLIKEN')
!
      call GET_object ('INT', '1EINT', 'AO')
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction(1:len_trim(Wavefunction)))
!
      if(.not.allocated(MullAOP))then
        allocate(MullAOP(Nbasis,Nbasis))
      end if
!
      if(Wavefunction(1:3).eq.'RHF'.or.Wavefunction(1:4).eq.'ROHF')then
        call MAOPOP (PM0, MullAOP, MATlen, Nbasis)
      else if(Wavefunction(1:3).eq.'UHF')then
        if(.not.allocated(MullAOP_A))then
          allocate(MullAOP_A(Nbasis,Nbasis), MullAOP_B(Nbasis,Nbasis))
        end if
        call MAOPOP (PM0_alpha, MullAOP_A, MATlen, Nbasis)
        call MAOPOP (PM0_beta, MullAOP_B, MATlen, Nbasis)
        MullAOP=MullAOP_A+MullAOP_B
      end if
!
! End of routine MAOPOP
      call PRG_manager ('exit', 'Mulliken_Population', 'AO_BY_AO%MULLIKEN')
      return
      end subroutine Mulliken_Population
      subroutine PRT_AO_by_AO_Mulliken
!**************************************************************************
!     Date last modified: October 5, 2000                     Version 2.0 *
!     Author: R.A. Poirier                                                *
!     Description: Mulliken population analysis.                          *
!**************************************************************************
! Modules:
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE matrix_print
      USE pop_analysis 

      implicit none
!
! Local scalars:
!
! Begin:
      call PRG_manager ('enter', 'PRT_AO_by_AO_Mulliken', 'UTILITY')
!
      call GET_object ('INT', '1EINT', 'AO')
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction(1:len_trim(Wavefunction)))
!
      if(Wavefunction(1:3).eq.'RHF'.or.Wavefunction(1:4).eq.'ROHF')then
        write(UNIout,'(/A)')' Mulliken Population Matrix '
        call PRT_matrix (MullAOP, Nbasis, Nbasis)
      else if(Wavefunction(1:3).eq.'UHF')then
        write(UNIout,'(/A)')' Mulliken Population Matrix (Alpha Spin)'
        call PRT_matrix (MullAOP_A, Nbasis, Nbasis)
        write(UNIout,'(/A)')' Mulliken Population Matrix (Beta Spin)'
        call PRT_matrix (MullAOP_B, Nbasis, Nbasis)
        write(UNIout,'(/A)')' Mulliken Population Matrix (Total)'
        call PRT_matrix (MullAOP, Nbasis, Nbasis)
      end if
!
! End of routine MAOPOP
      call PRG_manager ('exit', 'PRT_AO_by_AO_Mulliken', 'UTILITY')
      return
      end subroutine PRT_AO_by_AO_Mulliken
      subroutine MAOPOP (PM, &
                         MullAOP, &
                         MATlen,  &
                         Nbasis)
!***********************************************************************
!     Date last modified: December 5, 1992                             *
!     Author: R.A. Poirier                                             *
!     Description: Mulliken population analysis.  MUN Version.         *
!     PM -  Alpha (or regular) density matrix or Beta density matrix.  *
!     SM -  Overlap matrix.                                            *
!     EIGV -  Eigenvalues.                                             *
!***********************************************************************
! Modules:
      USE INT_objects

      implicit none
!
! Input scalars:
      integer :: MATlen,Nbasis
!
! Input arrays:
      double precision PM(MATlen)
!
! Output arrays:
      double precision MullAOP(Nbasis,Nbasis)
!
! Work array:
      double precision, dimension(:), allocatable :: WORK
!
! Local scalars:
      integer I,J,JI
!
! Begin:
      call PRG_manager ('enter', 'MAOPOP', 'UTILITY')
!
! Do Mulliken population analysis.
      do I=1,Nbasis
      do J=1,I
        JI=J+I*(I-1)/2
        MullAOP(I,J)=PM(JI)*OVRLAP(JI)
        MullAOP(J,I)=MullAOP(I,J)
      end do ! J
      end do ! I
!
! End of routine MAOPOP
      call PRG_manager ('exit', 'MAOPOP', 'UTILITY')
      return
      end subroutine maopop
      subroutine Mulliken_AO_pop
!***********************************************************************
!     Date last modified: June 26, 1992                                *
!     Author: R.A. Poirier                                             *
!     Description: Mulliken population analysis.                       *
!***********************************************************************
! Modules:
      USE program_files
      USE matrix_print
      USE QM_defaults
      USE pop_analysis 

      implicit none
!
! Work array:
      double precision, dimension(:), allocatable :: WORK
!
! Local scalar:
      integer :: Nbasis
      integer ICOL,NCOLOMS
!
! Begin:
      call PRG_manager ('enter', 'Mulliken_AO_pop', 'AO%MULLIKEN')
!
      call GET_object ('QM', 'AO_BY_AO', 'MULLIKEN')

      allocate (WORK(Nbasis))
!
      if(Wavefunction(1:3).eq.'RHF'.or.Wavefunction(1:4).eq.'ROHF')then
        NCOLOMS=1
        if(.not.allocated(ORBCH))then
          allocate (ORBCH(Nbasis,NCOLOMS))
        end if
        write(UNIout,'(/A)')' Gross Orbital Charges'
        ICOL=1
        call MORBCH (MullAOP, ORBCH, ICOL, Nbasis, NCOLOMS)
        call PRT_matrix (ORBCH, Nbasis, NCOLOMS)
!
      else if(Wavefunction(1:3).eq.'UHF')then
        write(UNIout,'(/2A)')' Gross Orbital Charges (Alpha(1), Beta(2), Total(3)) and Orbital Spin Densities(4)'
        NCOLOMS=4
        if(.not.allocated(ORBCH))then
          allocate (ORBCH(Nbasis,NCOLOMS))
        end if
        ICOL=1
        call MORBCH (MullAOP, ORBCH, ICOL, Nbasis, NCOLOMS)
        ICOL=2
        call MORBCH (MullAOP, ORBCH, ICOL, Nbasis, NCOLOMS)
        call PRT_matrix (ORBCH, Nbasis, NCOLOMS)
      end if

      deallocate (WORK)
!
! End of routine Mulliken_AO_pop
      call PRG_manager ('exit', 'Mulliken_AO_pop', 'AO%MULLIKEN')
      return
      end subroutine Mulliken_AO_pop
      subroutine MORBCH (MullAOP, &
                         ORBCH, &
                         ICOL, &
                         Nbasis, &
                         NCOLOMS)
!***********************************************************************
!     Date last modified: December 5, 1992                             *
!     Author: R.A. Poirier                                             *
!     Description: Mulliken population analysis.  MUN Version.         *
!     Orbital charges                                                  *
!                                                                      *
!***********************************************************************
! Modules:
      USE program_constants

      implicit none
!
! Input scalars:
      integer ICOL,Nbasis,NCOLOMS
!
! Input arrays:
      double precision MullAOP(Nbasis,Nbasis)
!
! Output arrays:
      double precision ORBCH(Nbasis,NCOLOMS)
!
! Local scalars:
      integer I,J
      double precision SUM
!
! Begin:
      call PRG_manager ('enter', 'MORBCH', 'UTILITY')
!
! Do Mulliken population analysis (orbital).
      do I=1,Nbasis
      SUM=ZERO
        do J=1,Nbasis
          SUM=SUM+MullAOP(J,I)
        end do !J
      ORBCH(I,ICOL)=SUM
      end do !I
!
      if(ICOL.EQ.2)then
        do I=1,Nbasis
        ORBCH(I,3)=ORBCH(I,1)+ORBCH(I,2)
        ORBCH(I,4)=ORBCH(I,1)-ORBCH(I,2)
        end do
      end if

! End of routine MORBCH
      call PRG_manager ('exit', 'MORBCH', 'UTILITY')
      return
      end subroutine MORBCH
      subroutine Mulliken_Net_Atomic
!***********************************************************************
!     Date last modified: December 5, 1992                             *
!     Author: R.A. Poirier                                             *
!     Description: Mulliken population analysis.  MUN Version.         *
!     Charges by atom                                                  *
!***********************************************************************
! Modules:
      USE program_files
      USE type_molecule
      USE QM_defaults
      USE pop_analysis 

      implicit none
!
! Local scalar:
      integer Iatom,ICOL,NCOLOMS
!
! Begin:
      call PRG_manager ('enter', 'Mulliken_Net_Atomic', 'NET_ATOMIC%MULLIKEN')
!
      call GET_object ('QM','ATOMIC', 'MULLIKEN')

      if(Wavefunction(1:3).eq.'RHF'.or.Wavefunction(1:4).eq.'ROHF')then
        ICOL=1
        NCOLOMS=2
        if(.not.allocated(Atom_charges))then
          allocate(Atom_charges(Natoms,Ncoloms))
        end if
        call MNATM8 (Atom_pop, Atom_charges, ICOL, NCOLOMS)
      else if(Wavefunction(1:3).eq.'UHF')then
        ICOL=1
        NCOLOMS=2
        if(.not.allocated(Atom_charges))then
          allocate(Atom_charges(Natoms,Ncoloms))
        end if
        call MNATM8 (Atom_pop, Atom_charges, ICOL, NCOLOMS)
        ICOL=1
        call MNATM8 (Atom_pop, Atom_charges, ICOL, NCOLOMS)
      end if
!
! End of routine Mulliken_Net_Atomic
      call PRG_manager ('exit', 'Mulliken_Net_Atomic', 'NET_ATOMIC%MULLIKEN')
      return
      end subroutine Mulliken_Net_Atomic
      subroutine Matomic (MullAOP, &
                          Atom_pop, &
                          Nbasis, &
                          Natoms)
!***********************************************************************
!     Date last modified: December 5, 1992                             *
!     Author: R.A. Poirier                                             *
!     Description: Mulliken population analysis.  MUN Version.         *
!     Orbital charges                                                  *
!***********************************************************************
! Modules:
      USE program_constants
      USE type_basis_set

      implicit none
!
! Input scalars:
      integer Natoms,Nbasis
!
! Input arrays:
      double precision MullAOP(Nbasis,Nbasis)
!
! Output arrays:
      double precision Atom_pop(Natoms,Natoms)
!
! Local scalars:
      integer Iatom,Jatom,K,L
      double precision SUM
!
! Begin:
      call PRG_manager ('enter', 'Matomic', 'UTILITY')
!
      Atom_pop(1:Natoms,1:Natoms)=ZERO
!
! Do Mulliken population analysis (atoms).
      do K=1,Nbasis
        Iatom=Basis%AOtoATOM(K)
        do L=1,Nbasis
        Jatom=Basis%AOtoATOM(L)
        Atom_pop(Iatom,Jatom)=Atom_pop(Iatom,Jatom)+MullAOP(K,L)
        end do ! L
      end do ! K
!
! End of routine Matomic
      call PRG_manager ('exit', 'Matomic', 'UTILITY')
      return
      end subroutine Matomic
      subroutine MNATM8 (Atom_pop, &
                         Atom_charges, &
                         ICOL, &
                         NCOLOMS)
!***********************************************************************
!     Date last modified: December 5, 1992                             *
!     Author: R.A. Poirier                                             *
!     Description: Mulliken population analysis.  MUN Version.         *
!     Net atomic charges                                               *
!                                                                      *
!***********************************************************************
! Modules:
      USE type_molecule

      implicit none
!
! Input scalars:
      integer ICOL,NCOLOMS
!
! Input arrays:
      double precision Atom_pop(Natoms,Natoms)
!
! Output arrays:
      double precision Atom_charges(Natoms,NCOLOMS)
!
! Local scalars:
      integer I,J,JIAN
!
! Local parameters:
      double precision SUM,ZERO
      parameter (ZERO=0.0D0)
!
! Begin:
      call PRG_manager ('enter', 'MNATM8', 'UTILITY')
!
! Do Mulliken population analysis (net atomic charges).
!
      do I=1,Natoms
      SUM=ZERO
      do J=1,Natoms
      SUM=SUM+Atom_pop(J,I)
      end do !J
      Atom_charges(I,ICOL)=SUM
      end do !I
!
      if(ICOL.EQ.2)then
        do I=1,Natoms
        Atom_charges(I,4)=Atom_charges(I,1)+Atom_charges(I,2)
        Atom_charges(I,3)=Atom_charges(I,1)-Atom_charges(I,2)
        JIAN=CARTESIAN(I)%Atomic_number
        if(JIAN.LT.0)JIAN=0
        Atom_charges(I,5)=DBLE(JIAN)-Atom_charges(I,4)
        end do
      else if(ICOL.EQ.1.and.NCOLOMS.EQ.2)then
        do I=1,Natoms
        JIAN=CARTESIAN(I)%Atomic_number
        if(JIAN.LT.0)JIAN=0
        Atom_charges(I,2)=DBLE(JIAN)-Atom_charges(I,1)
        end do
      end if
!
! End of routine MNATM8
      call PRG_manager ('exit', 'MNATM8', 'UTILITY')
      return
      end subroutine MNATM8
      subroutine CHKMOS (EIG, &
                         NOCC_MO)
!***********************************************************************
!     Date last modified: June 26, 1992                                *
!     Author: R.A. Poirier                                             *
!     Description: Mulliken population analysis.  MUN Version.         *
!     Routines called: none                                            *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants

      implicit none
!
! Input scalar:
      integer NOCC_MO
!
! Input array:
      double precision EIG(NOCC_MO)
!
! Local scalars:
      integer I,K
!
! Local parameters:
!
! Begin:
      call PRG_manager ('enter', 'CHKMOS', 'UTILITY')
!
      K=0
      do I=1,NOCC_MO
      if(EIG(I).GE.ZERO) then
      if(K.EQ.0)write(UNIout,'(1X)')
      K=1
      write(UNIout,'(A,I4,A)')'WARNING> CHKMOS: MO ',I,' IS NOT BOUND -  CALCULATION IS NOT VALID'
      end if ! EIG(I).GE.ZERO
      end do ! I
!
! End of routine CHKMOS
      call PRG_manager ('exit', 'CHKMOS', 'UTILITY')
      return
      end subroutine CHKMOS
      subroutine PRT_MAT_ATM (ARRAY, &
                              DIM2 &
                              )
!***********************************************************************
!     Date last modified: February 18, 1997                            *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version                                        *
!     General output routine for matrices by atoms.                    *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_elements
      USE type_molecule

      implicit none
!
! Input scalars:
      integer DIM2
!
! Input arrays:
      double precision ARRAY(Natoms,DIM2)
!
! Local scalars:
      integer Iatom,IANK,ifLAG,ILOWER,IUPPER,J
      character*4 IELK
!
! Begin:
      call PRG_manager ('enter', 'PRT_MAT_ATM', 'UTILITY')
!
      ifLAG=1
      ILOWER=1
   10 IUPPER=ILOWER+9
      if(IUPPER.GE.DIM2)then
        IUPPER=DIM2
        ifLAG=0
      end if
      write(UNIout,1000)(Iatom,Iatom=ILOWER,IUPPER)
      write(UNIout,1004)
      do Iatom=1,Natoms
        IELK=' '
        IANK=CARTESIAN(Iatom)%Atomic_number
        if(IANK.GT.0)then
          IELK(2:3)=Element_symbols(IANK)
        else if(IANK.EQ.0)then
          IELK(2:3)=Dummy
        else if(IANK.LT.0)then
          if(IABS(IANK).EQ.999)then
            IELK(2:3)=Floating
            IELK(2:3)=Dummy
          else
            IELK(2:3)=Element_symbols(IABS(IANK))
            IELK(4:4)='*'
          end if
        end if
        write(UNIout,1003)Iatom,IELK,(ARRAY(Iatom,J),J=ILOWER,IUPPER)
      end do ! IatomS

      if(ifLAG.NE.0)then
        ILOWER=ILOWER+10
        GO TO 10
      end if
!
 1000 format(16X,10(8X,I3))
!1003 format(7X,I3,A4,5X,10F14.8)
 1003 format(3x,'NET:',I3,A4,5X,10F14.8)
 1004 format('')
!
! End of routine PRT_MAT_ATM
      call PRG_manager ('exit', 'PRT_MAT_ATM', 'UTILITY')
      return
      end
      subroutine PRT_Net_Atomic_Mulliken
!***********************************************************************
!     Date last modified: December 5, 1992                             *
!     Author: R.A. Poirier                                             *
!     Description: Mulliken population analysis.  MUN Version.         *
!     Charges by atom                                                  *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE QM_defaults
      USE pop_analysis 

      implicit none
!
! Local scalar:
      integer Iatom,NCOLOMS
      double precision :: DipoleX,DipoleY,DipoleZ,DipoleTotal
      double precision sum1,sum2
!
! Begin:
      call PRG_manager ('enter', 'PRT_Net_Atomic_Mulliken', 'UTILITY')
!
      call GET_object ('QM','ATOMIC', 'MULLIKEN')

      sum1=ZERO
      sum2=ZERO
      if(Wavefunction(1:3).eq.'RHF'.or.Wavefunction(1:4).eq.'ROHF')then
        write(UNIout,'(/A)')' Total Atomic Charges and Net Atomic Charges'
        NCOLOMS=2
        call PRT_MAT_ATM (Atom_charges, NCOLOMS)
        do Iatom=1,Natoms
        sum1=sum1+Atom_charges(Iatom,1)
        sum2=sum2+Atom_charges(Iatom,2)
        end do
        write(UNIout,'(19x,a)')'----------------------------'
        write(UNIout,'(a,13x,2F14.8)')'SUMS: ',sum1,sum2

!     Calculate X, Y and Z components independently:
      DipoleX=ZERO
      DipoleY=ZERO
      DipoleZ=ZERO

!     Find the atomic dipole.
      do Iatom=1,Natoms
        DipoleX=DipoleX+CARTESIAN(Iatom)%X*Atom_charges(Iatom,2)
        DipoleY=DipoleY+CARTESIAN(Iatom)%Y*Atom_charges(Iatom,2)
        DipoleZ=DipoleZ+CARTESIAN(Iatom)%Z*Atom_charges(Iatom,2)
      end do !Iatom
!
!     Compute total dipole moment.
      Dipoletotal=dsqrt(DipoleX*DipoleX+DipoleY*DipoleY+DipoleZ*DipoleZ)
      
      DipoleX=DipoleX*Dconst
      DipoleY=DipoleY*Dconst
      DipoleZ=DipoleZ*Dconst
      DipoleTotal=DipoleTotal*Dconst
      write(UNIout,'(/a)')'Numerical dipole moment from Mulliken (Debye):'
      write(UNIout,'(a)')'*******************************'
      write(UNIout,'(7x,a,1PE14.6,5X,a,E14.6,5X,a,E14.6,5X,a,0PF9.6/)')'X:',DipoleX,'Y:',DipoleY,'Z:',DipoleZ,'Total:',DipoleTotal

!
      else if(Wavefunction(1:3).eq.'UHF')then
        NCOLOMS=2
        write(UNIout,'(/A)')' Total Atomic Charges and Net Atomic Charges (Alpha Spin)'
        call PRT_MAT_ATM (Atom_charges, NCOLOMS)
        write(UNIout,'(/A)')' Total Atomic Charges and Net Atomic Charges (Beta Spin)'
        call PRT_MAT_ATM (Atom_charges, NCOLOMS)
      end if

 1020 format(7X,'X:',1PE14.6,5X,'Y:',E14.6,5X,'Z:',E14.6,5X,'TOTAL:',0PF9.6/)
!
! End of routine Mulliken_Net_Atomic
      call PRG_manager ('exit', 'PRT_Net_Atomic_Mulliken', 'UTILITY')
      return
      end subroutine PRT_Net_Atomic_Mulliken
      subroutine Mulliken_Atomic
!***********************************************************************
!     Date last modified: December 5, 1992                             *
!     Author: R.A. Poirier                                             *
!     Description: Mulliken population analysis.                       *
!     Charges by atom                                                  *
!***********************************************************************
! Modules:
      USE program_files
      USE type_molecule
      USE type_basis_set
      USE QM_defaults
      USE pop_analysis 

      implicit none
!
! Local scalars:
      integer :: Nbasis
!
! Begin:
      call PRG_manager ('enter', 'Mulliken_Atomic', 'UTILITY')
!
      call GET_object ('QM', 'AO_BY_AO', 'MULLIKEN')
      Nbasis=size(MullAOP,1)

      if(.not.allocated(Atom_pop))then
        allocate(Atom_pop(Natoms,Natoms))
      end if
!
      if(Wavefunction(1:3).eq.'RHF'.or.Wavefunction(1:4).eq.'ROHF')then
        call Matomic (MullAOP, Atom_pop, Nbasis, Natoms)
!
      else if(Wavefunction(1:3).eq.'UHF')then
        call Matomic (MullAOP_A, Atom_pop, Nbasis, Natoms)
        call Matomic (MullAOP_B, Atom_pop, Nbasis, Natoms)
      end if
!
! End of routine Mulliken_Atomic
      call PRG_manager ('exit', 'Mulliken_Atomic', 'UTILITY')
      return
      end subroutine Mulliken_Atomic
      subroutine PRT_Atomic_Mulliken
!***********************************************************************
!     Date last modified: December 5, 1992                             *
!     Author: R.A. Poirier                                             *
!     Description: Mulliken population analysis.                       *
!     Charges by atom                                                  *
!***********************************************************************
! Modules:
      USE program_files
      USE type_molecule
      USE type_basis_set
      USE QM_defaults
      USE pop_analysis 

      implicit none
!
! Local scalars:
      integer :: Nbasis
!
! Begin:
      call PRG_manager ('enter', 'PRT_Atomic_Mulliken', 'UTILITY')
!
      call GET_object ('QM', 'AO_BY_AO', 'MULLIKEN')
      Nbasis=size(MullAOP,1)

      if(Wavefunction(1:3).eq.'RHF'.or.Wavefunction(1:4).eq.'ROHF')then
        write(UNIout,'(/A)')' Mulliken Population Matrix Condensed To Atoms'
        call PRT_MAT_ATM (Atom_pop, Natoms)
!
      else if(Wavefunction(1:3).eq.'UHF')then
! Need Atom_pop alpha and beta
        write(UNIout,'(/A)')' Mulliken Population Matrix Condensed To Atoms (Alpha Spin)'
        write(UNIout,'(/A)')' Mulliken Population Matrix Condensed To Atoms (Beta Spin)'
      end if
!
! End of routine PRT_Atomic_Mulliken
      call PRG_manager ('exit', 'Mulliken_Atomic', 'UTILITY')
      return
      end subroutine PRT_Atomic_Mulliken
