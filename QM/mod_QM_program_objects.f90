      subroutine BLD_QM_objects
!****************************************************************************************************************
!       Date last modified June 23, 2000                                                                        *
!       Author: Darryl Reid and R.A. Poirier                                                                    *
!       Description:  Builds a complete list of all the objects which can be created in MUNgauss                *
!       When adding a class or object within a class ensure it is done in alphabetical order                    *
!****************************************************************************************************************
! Modules:
      USE program_objects

      implicit none

! Local scalar:
      integer :: Iobject
!
! Begin:
      OBJ_QM(1:Max_objects)%modality = 'other'
      OBJ_QM(1:Max_objects)%class = 'QM'
      OBJ_QM(1:Max_objects)%depend = .true.
      NQMobjects = 0
!
! Class Quantum Mechanical (QM)
! Modality=RHF
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'DENSITY_1MATRIX'
      OBJ_QM(NQMobjects)%modality = 'RHF'
      OBJ_QM(NQMobjects)%routine = 'DENSITY_1MATRIX_RHF'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'DENSITY_1MATRIX'
      OBJ_QM(NQMobjects)%modality = 'GAUSSIAN'
      OBJ_QM(NQMobjects)%routine = 'DENSITY_1MATRIX_GAUSSIAN'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'DENSITY_ENERGY_WEIGHTED'
      OBJ_QM(NQMobjects)%modality = 'RHF'
      OBJ_QM(NQMobjects)%routine = 'DENSITY_Eweighted_RHF'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'CMO'
      OBJ_QM(NQMobjects)%modality = 'RHF'
      OBJ_QM(NQMobjects)%routine = 'RHFCLC'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'CMO'
      OBJ_QM(NQMobjects)%modality = 'RHF_NI'
      OBJ_QM(NQMobjects)%routine = 'RHF_numerical'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'CMO'
      OBJ_QM(NQMobjects)%modality = 'GAIMT'
      OBJ_QM(NQMobjects)%routine = 'GAIMT'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'ENERGY'
      OBJ_QM(NQMobjects)%modality = 'RHF'
      OBJ_QM(NQMobjects)%routine = 'RHF_energy'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'ENERGY_COMPONENTS'
      OBJ_QM(NQMobjects)%modality = 'RHF'
      OBJ_QM(NQMobjects)%routine = 'E_JandK'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'ENERGY_BSSE'
      OBJ_QM(NQMobjects)%modality = 'RHF'
      OBJ_QM(NQMobjects)%routine = 'BLD_ENERGY_BSSE'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'ENERGY'
      OBJ_QM(NQMobjects)%modality = 'CISD'
      OBJ_QM(NQMobjects)%routine = 'E_CISD'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'CMO'
      OBJ_QM(NQMobjects)%modality = 'RHF'
      OBJ_QM(NQMobjects)%routine = 'BLD_CMO_OV'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'DIPOLE_MOMENT'
      OBJ_QM(NQMobjects)%modality = 'RHF'
      OBJ_QM(NQMobjects)%routine = 'CDIPOL'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'MOMENT'
      OBJ_QM(NQMobjects)%modality = 'SECOND'
      OBJ_QM(NQMobjects)%routine = 'SECOND_MOMENT_ETOT'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'SHAPE_AND_SIZE'
      OBJ_QM(NQMobjects)%modality = 'RHF'
      OBJ_QM(NQMobjects)%routine = 'BLD_shape_and_size'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'R12_AB'
      OBJ_QM(NQMobjects)%modality = 'RHF'
      OBJ_QM(NQMobjects)%routine = 'BLD_r12_ab'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'VELOCITIES'
      OBJ_QM(NQMobjects)%modality = 'RHF'
      OBJ_QM(NQMobjects)%routine = 'VELOCITIES'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'VELOCITIES'
      OBJ_QM(NQMobjects)%modality = 'NUMERICAL'
      OBJ_QM(NQMobjects)%routine = 'VELOCITIES_NI'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'ENERGY_KINETIC'
      OBJ_QM(NQMobjects)%modality = 'NUMERICAL'
      OBJ_QM(NQMobjects)%routine = 'ENERGY_KINETIC_NI'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'ENERGY_VEE'
      OBJ_QM(NQMobjects)%modality = 'NN'
      OBJ_QM(NQMobjects)%routine = 'ENERGY_Vee_NN'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'ENERGY_COULOMB'
      OBJ_QM(NQMobjects)%modality = 'NUMERICAL'
      OBJ_QM(NQMobjects)%routine = 'ENERGY_J_NI'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'ENERGY_COULOMB'
      OBJ_QM(NQMobjects)%modality = 'MO'
      OBJ_QM(NQMobjects)%routine = 'ENERGY_COULOMB_MO_NI'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'ENERGY_VEE'
      OBJ_QM(NQMobjects)%modality = 'NUMERICAL'
      OBJ_QM(NQMobjects)%routine = 'ENERGY_VEE_MO_NI'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'ENERGY_EXCHANGE'
      OBJ_QM(NQMobjects)%modality = 'NUMERICAL'
      OBJ_QM(NQMobjects)%routine = 'ENERGY_K_NI'
!                                                                                
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'JAB'                                        
      OBJ_QM(NQMobjects)%modality = 'NUMERICAL'                              
      OBJ_QM(NQMobjects)%routine = 'ENERGY_JAB_MO_NI'
!                                                                                
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'KAB'                                        
      OBJ_QM(NQMobjects)%modality = 'NUMERICAL'                              
      OBJ_QM(NQMobjects)%routine = 'ENERGY_KAB_MO_NI'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'ENERGY_VNE'
      OBJ_QM(NQMobjects)%modality = 'NUMERICAL'
      OBJ_QM(NQMobjects)%routine = 'ENERGY_VNE_NI'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'ENERGY_CORRELATION'
      OBJ_QM(NQMobjects)%modality = 'NUMERICAL'
      OBJ_QM(NQMobjects)%routine = 'ENERGY_CORRELATION'
!
! Modality=AHF
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'CMO'
      OBJ_QM(NQMobjects)%modality = 'AHF'
      OBJ_QM(NQMobjects)%routine = 'INTAPPROX'
!
! Modality=ATOMIC_POTENTIAL
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'ENERGY'
      OBJ_QM(NQMobjects)%modality = 'ATOMIC_POTENTIAL'
      OBJ_QM(NQMobjects)%routine = 'ATOMIC_potential_energy'
!
! Modality=ATOMIC_KINETIC
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'ENERGY'
      OBJ_QM(NQMobjects)%modality = 'KINETIC_ATOMIC'
      OBJ_QM(NQMobjects)%routine = 'KINETIC_ATOMIC'
!
! Modality=NUMERICAL
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'DENSITY'
      OBJ_QM(NQMobjects)%modality = 'NUMERICAL'
      OBJ_QM(NQMobjects)%routine = 'Numerical_integration'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'DENSITY_BOND'
      OBJ_QM(NQMobjects)%modality = 'NUMERICAL'
!      OBJ_QM(NQMobjects)%routine = 'NI_BONDS'
      OBJ_QM(NQMobjects)%routine = 'NI_BONDS_ENERGY("DENSITY")'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'EXCHANGE_BOND'
      OBJ_QM(NQMobjects)%modality = 'NUMERICAL'
      OBJ_QM(NQMobjects)%routine = 'NI_BONDS_ENERGY("EXCHANGE")'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'KINETIC_BOND'
      OBJ_QM(NQMobjects)%modality = 'NUMERICAL'
      OBJ_QM(NQMobjects)%routine = 'NI_BONDS_ENERGY("SCHKINETIC")'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'COULOMB_MO_BOND'
      OBJ_QM(NQMobjects)%modality = 'NUMERICAL'
      OBJ_QM(NQMobjects)%routine = 'NI_BONDS_ENERGY("MOCOULOMB")'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'COULOMB_BOND'
      OBJ_QM(NQMobjects)%modality = 'NUMERICAL'
      OBJ_QM(NQMobjects)%routine = 'NI_BONDS_ENERGY("COULOMB")'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'VNE_BOND'
      OBJ_QM(NQMobjects)%modality = 'NUMERICAL'
      OBJ_QM(NQMobjects)%routine = 'NI_BONDS_ENERGY("VNE_")'
!
! Modality=NUCLEI
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'DENSITY'
      OBJ_QM(NQMobjects)%modality = 'NUCLEI'
      OBJ_QM(NQMobjects)%routine = 'RHO_NUCLEI'
!
! Modality=UHF
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'DENSITY_1MATRIX'
      OBJ_QM(NQMobjects)%modality = 'UHF'
      OBJ_QM(NQMobjects)%routine = 'DENSITY_1MATRIX_UHF'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'DENSITY_ENERGY_WEIGHTED'
      OBJ_QM(NQMobjects)%modality = 'UHF'
      OBJ_QM(NQMobjects)%routine = 'DENSITY_Eweighted_UHF'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'CMO'
      OBJ_QM(NQMobjects)%modality = 'UHF'
      OBJ_QM(NQMobjects)%routine = 'UHFCLC'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'ENERGY'
      OBJ_QM(NQMobjects)%modality = 'UHF'
      OBJ_QM(NQMobjects)%routine = 'UHF_energy'
!
! Modality=OSHF
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'DENSITY_1MATRIX'
      OBJ_QM(NQMobjects)%modality = 'OSHF'
      OBJ_QM(NQMobjects)%routine = 'DENSITY_1MATRIX_OSHF'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'DENSITY_ENERGY_WEIGHTED'
      OBJ_QM(NQMobjects)%modality = 'OSHF'
      OBJ_QM(NQMobjects)%routine = 'DENSITY_Eweighted_OSHF'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'CMO'
      OBJ_QM(NQMobjects)%modality = 'OSHF'
      OBJ_QM(NQMobjects)%routine = 'OSHF'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'ENERGY'
      OBJ_QM(NQMobjects)%modality = 'OSHF'
      OBJ_QM(NQMobjects)%routine = 'OSHF_energy'
!
! Modality=GVB
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'DENSITY_1MATRIX'
      OBJ_QM(NQMobjects)%modality = 'GVB'
      OBJ_QM(NQMobjects)%routine = 'DENSITY_1MATRIX_GVB'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'DENSITY_ENERGY_WEIGHTED'
      OBJ_QM(NQMobjects)%modality = 'GVB'
      OBJ_QM(NQMobjects)%routine = 'DENSITY_Eweighted_GSCF'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'CMO'
      OBJ_QM(NQMobjects)%modality = 'GVB'
      OBJ_QM(NQMobjects)%routine = 'GVBCALC'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'ENERGY'
      OBJ_QM(NQMobjects)%modality = 'GVB'
      OBJ_QM(NQMobjects)%routine = 'GVB_ENERGY'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'LOCALIZED_LIST'
      OBJ_QM(NQMobjects)%modality = 'GVB'
      OBJ_QM(NQMobjects)%routine = 'GVB_LMO_LIST'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'CMO_PAIRLIST'
      OBJ_QM(NQMobjects)%modality = 'GVB'
      OBJ_QM(NQMobjects)%routine = 'GVBCMO'
!
! Modality=ROHF
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'ENERGY'
      OBJ_QM(NQMobjects)%modality = 'ROHF'
      OBJ_QM(NQMobjects)%routine = 'ROHF_energy'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'DENSITY_1MATRIX'
      OBJ_QM(NQMobjects)%modality = 'ROHF'
      OBJ_QM(NQMobjects)%routine = 'DENSITY_1MATRIX_ROHF'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'DENSITY_ENERGY_WEIGHTED'
      OBJ_QM(NQMobjects)%modality = 'ROHF'
      OBJ_QM(NQMobjects)%routine = 'DENSITY_Eweighted_GSCF'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'CMO'
      OBJ_QM(NQMobjects)%modality = 'ROHF'
      OBJ_QM(NQMobjects)%routine = 'ROHFCLC'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'ENERGY_COMPONENTS'
      OBJ_QM(NQMobjects)%modality = 'ROHF'
      OBJ_QM(NQMobjects)%routine = 'E_JandK'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'DIPOLE_MOMENT'
      OBJ_QM(NQMobjects)%modality = 'ROHF'
      OBJ_QM(NQMobjects)%routine = 'CDIPOL'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'SHAPE_AND_SIZE'
      OBJ_QM(NQMobjects)%modality = 'ROHF'
      OBJ_QM(NQMobjects)%routine = 'BLD_shape_and_size'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'R12_AB'
      OBJ_QM(NQMobjects)%modality = 'ROHF'
      OBJ_QM(NQMobjects)%routine = 'BLD_r12_ab'
!
! Class Localized MO (LMO)
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'LMO'
      OBJ_QM(NQMobjects)%modality = 'BOYS'
      OBJ_QM(NQMobjects)%routine = 'LMO_BOYS'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'LMO'
      OBJ_QM(NQMobjects)%modality = 'POP'
      OBJ_QM(NQMobjects)%routine = 'LMO_POP'
!
! Modality=Huckel
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'CMO'
      OBJ_QM(NQMobjects)%modality = 'HUCKEL'
      OBJ_QM(NQMobjects)%routine = 'HUCKEL_MO'
!
! Modality=DIAGH
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'CMO'
      OBJ_QM(NQMobjects)%modality = 'DIAGH'
      OBJ_QM(NQMobjects)%routine = 'DIAGH_CMO'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'DENSITY_1MATRIX'
      OBJ_QM(NQMobjects)%modality = 'HUCKEL'
      OBJ_QM(NQMobjects)%routine = 'DENSITY_1MATRIX_HUCKEL'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'DENSITY_1MATRIX'
      OBJ_QM(NQMobjects)%modality = 'DIAGH'
      OBJ_QM(NQMobjects)%routine = 'DIAGH_PM0'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'FOCK_MATRIX'
      OBJ_QM(NQMobjects)%modality = 'HUCKEL'
      OBJ_QM(NQMobjects)%routine = 'Huckel'
!
! Class INTEGRALS
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = '1EINT'
      OBJ_QM(NQMobjects)%modality = 'AO'
      OBJ_QM(NQMobjects)%routine = 'I1ECLC'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = '1EINT'
      OBJ_QM(NQMobjects)%modality = 'MO'
      OBJ_QM(NQMobjects)%routine = 'I1E_AOtoMO'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = '1EINT'
      OBJ_QM(NQMobjects)%modality = 'DIPOLE'
      OBJ_QM(NQMobjects)%routine = 'DIPOLE_1e'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'MDIPOLE'
      OBJ_QM(NQMobjects)%modality = 'AO'
      OBJ_QM(NQMobjects)%routine = 'MDIPOLE'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'MDIPOLE'
      OBJ_QM(NQMobjects)%modality = 'MO'
      OBJ_QM(NQMobjects)%routine = 'MDIPOLE'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'VDIPOLE'
      OBJ_QM(NQMobjects)%modality = 'AO'
      OBJ_QM(NQMobjects)%routine = 'VDIPOLE'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'VDIPOLE'
      OBJ_QM(NQMobjects)%modality = 'MO'
      OBJ_QM(NQMobjects)%routine = 'VDIPOLE_MO'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'MOMENT1'
      OBJ_QM(NQMobjects)%modality = 'AO'
      OBJ_QM(NQMobjects)%routine = 'MOMENT12_AO'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'MOMENT2'
      OBJ_QM(NQMobjects)%modality = 'AO'
      OBJ_QM(NQMobjects)%routine = 'MOMENT12_AO'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'MOMENT1'
      OBJ_QM(NQMobjects)%modality = 'MO'
      OBJ_QM(NQMobjects)%routine = 'FIRST_MOMENT_MO'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'MOMENT2'
      OBJ_QM(NQMobjects)%modality = 'MO'
      OBJ_QM(NQMobjects)%routine = 'SECOND_MOMENT_MO'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = '1EINT'
      OBJ_QM(NQMobjects)%modality = 'OVLAP_AO_A_B_BLOCK'
      OBJ_QM(NQMobjects)%routine = 'SABCHA'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = '1EINT'
      OBJ_QM(NQMobjects)%modality = 'SM1TV_AO_AB'
      OBJ_QM(NQMobjects)%routine = 'SM1HAB'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = '1EINT'
      OBJ_QM(NQMobjects)%modality = 'SSM1_AO_AB_BLOCK'
      OBJ_QM(NQMobjects)%routine = 'SSM1AB'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = '1EINT'
      OBJ_QM(NQMobjects)%modality = 'TV_AO_AB'
      OBJ_QM(NQMobjects)%routine = 'HABBLD'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = '1EINT'
      OBJ_QM(NQMobjects)%modality = 'TV_AO_MONOMER_A'
      OBJ_QM(NQMobjects)%routine = 'I1TVAB'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = '1EINT'
      OBJ_QM(NQMobjects)%modality = 'TV_AO_MONOMER_B'
      OBJ_QM(NQMobjects)%routine = 'I1TVAB'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = '1EINT'
      OBJ_QM(NQMobjects)%modality = 'V_AO_AB'
      OBJ_QM(NQMobjects)%routine = 'VABBLD'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = '1EINT'
      OBJ_QM(NQMobjects)%modality = 'V_AO_MONOMER_A'
      OBJ_QM(NQMobjects)%routine = 'HAHB'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = '1EINT'
      OBJ_QM(NQMobjects)%modality = 'V_AO_MONOMER_B'
      OBJ_QM(NQMobjects)%routine = 'HAHB'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = '2EINT'
      OBJ_QM(NQMobjects)%modality = 'COMBINATIONS'
      OBJ_QM(NQMobjects)%routine = 'I2ECLC'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = '2EINT'
      OBJ_QM(NQMobjects)%modality = 'RAW'
      OBJ_QM(NQMobjects)%routine = 'I2ECLC'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = '2EINT'
      OBJ_QM(NQMobjects)%modality = 'MO'
      OBJ_QM(NQMobjects)%routine = 'I2E_AOtoMO'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = '2EINT'
      OBJ_QM(NQMobjects)%modality = 'MO_OVOV'
      OBJ_QM(NQMobjects)%routine = 'I2E_OVOV'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'INTEGRALS'
      OBJ_QM(NQMobjects)%modality = 'GAMMA_FUNCTION'
      OBJ_QM(NQMobjects)%routine = 'GAMGEN'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'FORCE_CONSTANT'
      OBJ_QM(NQMobjects)%modality = 'NUMERICAL'
      OBJ_QM(NQMobjects)%routine = 'ForceConstant_matrix'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'FREQUENCIES'
      OBJ_QM(NQMobjects)%modality = 'NUMERICAL'
      OBJ_QM(NQMobjects)%routine = 'Vibrational_frequencies'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'FORCE_CONSTANT'
      OBJ_QM(NQMobjects)%modality = 'ANALYTICAL'
      OBJ_QM(NQMobjects)%routine = 'ForceConstant_matrix'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'FREQUENCIES'
      OBJ_QM(NQMobjects)%modality = 'ANALYTICAL'
      OBJ_QM(NQMobjects)%routine = 'Vibrational_frequencies'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'AO_BY_AO'
      OBJ_QM(NQMobjects)%modality = 'MULLIKEN'
      OBJ_QM(NQMobjects)%routine = 'Mulliken_Population'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'AO'
      OBJ_QM(NQMobjects)%modality = 'MULLIKEN'
      OBJ_QM(NQMobjects)%routine = 'Mulliken_AO_pop'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'ATOMIC'
      OBJ_QM(NQMobjects)%modality = 'MULLIKEN'
      OBJ_QM(NQMobjects)%routine = 'Mulliken_Atomic'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'NET_ATOMIC'
      OBJ_QM(NQMobjects)%modality = 'MULLIKEN'
      OBJ_QM(NQMobjects)%routine = 'Mulliken_Net_Atomic'
!
! Modality MEYER
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'BOND_ORDER'
      OBJ_QM(NQMobjects)%modality = 'MEYER'
      OBJ_QM(NQMobjects)%routine = 'BLD_Bond_order_Meyer'
!
! Modality PM3
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'BOND_ORDER'
      OBJ_QM(NQMobjects)%modality = 'PM3'
      OBJ_QM(NQMobjects)%routine = 'BLD_Bond_order_MOPAC_PM3'
!
! Modality PM6
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'BOND_ORDER'
      OBJ_QM(NQMobjects)%modality = 'PM6'
      OBJ_QM(NQMobjects)%routine = 'BLD_Bond_order_MOPAC_PM6'

!
! Atoms In Molecules
!     NQMobjects = NQMobjects + 1
!     OBJ_QM(NQMobjects)%name = 'DENSITY'
!     OBJ_QM(NQMobjects)%modality = 'AIM'
!     OBJ_QM(NQMobjects)%routine = 'AIM_RAP_V0'
!
! Class GUESS (These objects remain current, unless a new job is started)
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'DENSITY_1MATRIX'
      OBJ_QM(NQMobjects)%modality = 'RHF'
      OBJ_QM(NQMobjects)%routine = 'DENSITY_guess_RHF'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'CMO_GUESS'
      OBJ_QM(NQMobjects)%modality = 'RHF'
      OBJ_QM(NQMobjects)%routine = 'BLD_GUESS_MO_RHF'
      OBJ_QM(NQMobjects)%depend = .false.
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'CMO_GUESS'
      OBJ_QM(NQMobjects)%modality = 'UHF'
      OBJ_QM(NQMobjects)%routine = 'BLD_GUESS_MO_UHF'
      OBJ_QM(NQMobjects)%depend = .false.
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'CMO_GUESS'
      OBJ_QM(NQMobjects)%modality = 'GVB'
      OBJ_QM(NQMobjects)%routine = 'GVBMOG'
      OBJ_QM(NQMobjects)%depend = .false.
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'CMO_GUESS'
      OBJ_QM(NQMobjects)%modality = 'ROHF'
      OBJ_QM(NQMobjects)%routine = 'BLD_GUESS_MO_ROHF'
      OBJ_QM(NQMobjects)%depend = .false.
!
! Modality PARR
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'CORRELATION'
      OBJ_QM(NQMobjects)%modality = 'PARR'
      OBJ_QM(NQMobjects)%routine = 'CORRELATION_PARR'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'BOND_ELECTRONS'
      OBJ_QM(NQMobjects)%modality = 'ABIM'
      OBJ_QM(NQMobjects)%routine = 'NI_BOND_ELECTRONS'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'ALL_BONDS_ELECTRONS'
      OBJ_QM(NQMobjects)%modality = 'ABIM'
      OBJ_QM(NQMobjects)%routine = 'NI_ELECTRONS_ALL_BONDS'
!
! Modality = HF
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'SPIN_DENSITY_1MATRIX'
      OBJ_QM(NQMobjects)%modality = 'AO'
      OBJ_QM(NQMobjects)%routine = 'BLD_spin_density_matrix'
!
! Modality = NI
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'SPIN_DENSITY'
      OBJ_QM(NQMobjects)%modality = 'NI'
      OBJ_QM(NQMobjects)%routine = 'NI_spin_density'
!
! Modality = AO
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = '2RDM'
      OBJ_QM(NQMobjects)%modality = 'AO'
      OBJ_QM(NQMobjects)%routine = 'BLD_2RDM_AO'
!
! Modality = MO
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = '2RDM'
      OBJ_QM(NQMobjects)%modality = 'MO'
      OBJ_QM(NQMobjects)%routine = 'BLD_2RDM_MO'
!
! Modality = NI
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = '2RDM'
      OBJ_QM(NQMobjects)%modality = 'NI'
      OBJ_QM(NQMobjects)%routine = 'NI_2RDM'
!
! Modality = NI
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'TDFVEEOS'
      OBJ_QM(NQMobjects)%modality = 'NI'
      OBJ_QM(NQMobjects)%routine = 'NI_TDF_Vee_OS'
!
! Modality = NI
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'TDFTOS'
      OBJ_QM(NQMobjects)%modality = 'NI'
      OBJ_QM(NQMobjects)%routine = 'NI_TDF_T_OS'
!
! Modality = NI
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'OSEC'
      OBJ_QM(NQMobjects)%modality = 'NI'
      OBJ_QM(NQMobjects)%routine = 'NI_OSEC'
!
! Modality = NI
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'EAM'
      OBJ_QM(NQMobjects)%modality = 'NI'
      OBJ_QM(NQMobjects)%routine = 'NI_EAM'
!
! Modality = ANA
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'EAM'
      OBJ_QM(NQMobjects)%modality = 'ANA'
      OBJ_QM(NQMobjects)%routine = 'EAMcalc'
!
! Modality = NI
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'MB'
      OBJ_QM(NQMobjects)%modality = 'NI'
      OBJ_QM(NQMobjects)%routine = 'NI_MB'
!
! Modality = ANA
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'MB'
      OBJ_QM(NQMobjects)%modality = 'ANA'
      OBJ_QM(NQMobjects)%routine = 'MBcalc'
!
! Modality = NI
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'RAM'
      OBJ_QM(NQMobjects)%modality = 'NI'
      OBJ_QM(NQMobjects)%routine = 'NI_RAM'
!
! Modality = ANA
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'RAM'
      OBJ_QM(NQMobjects)%modality = 'ANA'
      OBJ_QM(NQMobjects)%routine = 'RAMcalc'
!
! Modality = NI
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'EXTRA'
      OBJ_QM(NQMobjects)%modality = 'NI'
      OBJ_QM(NQMobjects)%routine = 'NI_extra'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'JCOUL'
      OBJ_QM(NQMobjects)%modality = 'NI'
      OBJ_QM(NQMobjects)%routine = 'NI_Jcoul'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'KEXCH'
      OBJ_QM(NQMobjects)%modality = 'NI'
      OBJ_QM(NQMobjects)%routine = 'NI_Kexch'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'VEE'
      OBJ_QM(NQMobjects)%modality = 'NI'
      OBJ_QM(NQMobjects)%routine = 'NI_Vee'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'VNE'
      OBJ_QM(NQMobjects)%modality = 'NI'
      OBJ_QM(NQMobjects)%routine = 'NI_Vne_spin'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'TPOS'
      OBJ_QM(NQMobjects)%modality = 'NI'
      OBJ_QM(NQMobjects)%routine = 'NI_Tp_spin'
!
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = 'TSCH'
      OBJ_QM(NQMobjects)%modality = 'NI'
      OBJ_QM(NQMobjects)%routine = 'NI_Ts_spin'
!
! Dummy Class
      NQMobjects = NQMobjects + 1
      OBJ_QM(NQMobjects)%name = '?'
      OBJ_QM(NQMobjects)%modality = '?'
      OBJ_QM(NQMobjects)%routine = '?'
!
      do Iobject=1,NQMobjects
        OBJ_QM(Iobject)%exist=.false.
        OBJ_QM(Iobject)%Current=.false.
      end do

      return
      end subroutine BLD_QM_objects
