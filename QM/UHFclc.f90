      subroutine UHF_energy
!********************************************************************************************************
!     Date last modified: February 27, 2003                                                Version 2.0  *
!     Author: R.A. Poirier                                                                              *
!     Description: This routine only serves as an interface - causes the UHF energy to be calculated.   *
!********************************************************************************************************
! Modules:
      USE QM_defaults

      implicit none
!
! Begin:
      call PRG_manager ('enter', 'UHF_energy', 'ENERGY%UHF')
!
! This also creates the energy, so do nothing.
      call GET_object ('QM', 'CMO', Wavefunction)
!
! End of routine UHF_ENERGY
      call PRG_manager ('exit', 'UHF_energy', 'ENERGY%UHF')
      return
      END
      subroutine UHFCLC
!***********************************************************************
!     Date last modified: June 26, 1992                                *
!     Author: R.A. Poirier                                             *
!     Description: Solution of the Pople-Nesbet equations for          *
!                  spin-unrestricted (UHF) open shell systems.         *
!                  J. Chem. Phys., 22, 571 (1954).  (MUN Version).     *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE matrix_print
      USE type_molecule
      USE QM_objects
      USE INT_objects
      USE ixtrapolate
      USE type_basis_set

      implicit none
!
! Local scalars:
      integer Jcycle
      integer I,IERROR
      double precision :: EUHF0,EUHF1
      double precision SCFCON
      double precision EPREV,SUM,T,TEN
!
! Local function:
      double precision ETRACE
!
! Work arrays:
      double precision, dimension(:), allocatable :: FMA
      double precision, dimension(:), allocatable :: FMB
      double precision, dimension(:), allocatable :: SCRVEC
      double precision, dimension(:), allocatable :: SOVLP
      double precision, dimension(:), allocatable :: DPM1
!
! Total density matrix (alpha + beta)
      double precision, dimension(:), allocatable, save, target :: PM0_total
!
! Work arrays (UHF):
      double precision, dimension(:), allocatable :: DGMA
      double precision, dimension(:), allocatable :: DGMB
!
! Extrapolation work arrays
!
! Local parameters:
      parameter (TEN=10.0D0)
!
! Begin:
      call PRG_manager ('enter', 'UHFCLC', 'CMO%UHF')
!
      if(Lall_ONDISK)then  ! Not allow for UHF
          stop ' UHFCLC> Option all integrals ONDISK not allowed with UHF'
      end if
! Make sure not integral combinations:
      call GET_object ('INT', '1EINT', 'AO')
      call GET_object ('INT', '2EINT', 'RAW')
      call GET_object ('QM', 'CMO_GUESS', 'UHF')
!
! Objects:
      if(.not.associated(CMOA%coeff))then
        allocate (CMOA%coeff(Nbasis,Nbasis), CMOA%eigval(1:Nbasis), CMOA%occupancy(1:Nbasis), &
                  CMOB%coeff(Nbasis,Nbasis), CMOB%eigval(1:Nbasis), CMOB%occupancy(1:Nbasis), &
                  STAT=IERROR)
        if(IERROR.ne.0)then
          stop ' UHFCLC> allocate failure: CMOA...'
        end if
        allocate (PM0_alpha(1:MATlen), PM0_beta(1:MATlen), PM0_total(1:MATlen), STAT=IERROR)
        if(IERROR.ne.0)then
          stop ' UHFCLC> allocate failure: PM0 ...'
        end if
      else
        if(Nbasis.ne.size(CMOA%coeff,1))then
        deallocate (CMOA%coeff, CMOB%coeff, CMOA%eigval, CMOB%eigval, PM0_alpha, PM0_beta, PM0_total, &
                    CMOA%occupancy, CMOB%occupancy)
        allocate (CMOA%coeff(Nbasis,Nbasis), CMOA%eigval(1:Nbasis), CMOA%occupancy(1:Nbasis), &
                  CMOB%coeff(Nbasis,Nbasis), CMOB%eigval(1:Nbasis), CMOB%occupancy(1:Nbasis), &
                  STAT=IERROR)
        if(IERROR.ne.0)then
          stop ' UHFCLC> allocate failure: CMOA...'
        end if
        allocate (PM0_alpha(1:MATlen), PM0_beta(1:MATlen), PM0_total(1:MATlen), STAT=IERROR)
        if(IERROR.ne.0)then
          stop ' UHFCLC> allocate failure: PM0 ...'
        end if
        end if
      end if
!
      call INI_UHFCLC
      call INI_Xpolation_UHF (PM0_alpha, PM0_beta, MATlen)
!
! Start SCF cycling.
      converged=.FALSE.
      do while (.not.converged)
! Form two-electron part of the Fock matrix.
      call UHFDGR (DGMA, DGMB)

      do I=1,MATlen
        FMA(I)=HCORE(I)+DGMA(I)
        FMB(I)=HCORE(I)+DGMB(I)
      end do
!
      ENERGY_UHF%elec=ETRACE (HCORE, FMA, PM0_alpha, Nbasis, MATlen)
      ENERGY_UHF%elec=ENERGY_UHF%elec+ETRACE (HCORE, FMB, PM0_beta, Nbasis, MATlen)
      ENERGY_UHF%elec=ENERGY_UHF%elec*PT5
      ENERGY_UHF%HF=ENERGY_UHF%nuclear+ENERGY_UHF%elec
      ENERGY_UHF%total=ENERGY_UHF%HF
      EUHF0=ENERGY_UHF%total
!
      if(.not.Lextrapolated)EPREV=ENERGY_UHF%elec
! Print iteration results.
      if(MUN_prtlev.GT.0)then
      if(Lextrapolated)then
        write(UNIout,1000)Jcycle,ENERGY_UHF%elec,ENERGY_UHF%total,LABEL
 1000 FORMAT(1X,I4,1X,2F20.9,20x,a8)
      else
        write(UNIout,1001)Jcycle,ENERGY_UHF%elec,ENERGY_UHF%total,SCFCON
 1001 FORMAT(1X,I4,1X,2F20.9,1PE14.5)
      end if
      end if
      EUHF1=ENERGY_UHF%total
      if(EUHF1.gt.ZERO)then ! Energy is positive!
        converged=.false.
        exit
        write(UNIout,'(a)')'The energy is positive, quit and use a gaussian guess'
      else if ((EUHF1-EUHF0).gt.200.0D0)then ! E has increased by too ! much
        write(UNIout,'(a)')'The energy has increased significantly, quit and use a gaussian guess'
        converged=.false.
        exit
      end if
      EUHF0=EUHF1
!
! Form Alpha and Beta density matrices independently.
! First Alpha.
      call DETlinW (OVRLAP, FMA, CMOA%coeff, CMOA%eigval, MATlen, Nbasis, .true.)
      call DENBLD (CMOA%coeff, CMOA%occupancy, PM0_alpha, Nbasis, MATlen, CMOA%NoccMO)
!
! Now Beta.
      call DETlinW (OVRLAP, FMB, CMOB%coeff, CMOB%eigval, MATlen, Nbasis, .true.)
      call DENBLD (CMOB%coeff, CMOB%occupancy, PM0_beta, Nbasis, MATlen, CMOB%NoccMO)
!
! Update total:
      PnM0(1:MATlen)=PM0_alpha(1:MATlen)
      PnM0(MATlen+1:2*MATlen)=PM0_beta(1:MATlen)
      call Xpolation (SCFACC, SCFCON, NBasis)
      PM0_alpha(1:MATlen)=extra(1:MATlen)
      PM0_beta(1:MATlen)=extra(MATlen+1:2*MATlen)
      PM0_total(1:MATlen)=PM0_alpha(1:MATlen)+PM0_beta(1:MATlen)
!
      Jcycle=Jcycle+1
!
! Has convergence been met?  If so, exit.
      if(converged)then
        LSCF_converged=.true.
        if(MUN_prtlev.GT.0)then
          write(UNIout,1010)ENERGY_UHF%total
        end if

        call PHASE (CMOA%coeff, Nbasis)
        call PHASE (CMOB%coeff, Nbasis)
!
! Calculate S, S(S+1).
        T=-TEN
        call TSPIN (T, ONE)
! Save MOs so they can be used as the next initial guess
        CMOAG%coeff(1:Nbasis,1:Nbasis)=CMOA%coeff(1:Nbasis,1:Nbasis)
        CMOBG%coeff(1:Nbasis,1:Nbasis)=CMOB%coeff(1:Nbasis,1:Nbasis)
        CMOAG%eigval(1:Nbasis)=CMOA%eigval(1:Nbasis)
        CMOBG%eigval(1:Nbasis)=CMOB%eigval(1:Nbasis)
        CMOG%coeff(1:Nbasis,1:Nbasis)=CMOAG%coeff(1:Nbasis,1:Nbasis)! +CMOBG%coeff(1:Nbasis,1:Nbasis))/TWO
!
! Have we exceeded the maximum allowed number of cycles? If so, exit.
      else if(Jcycle.GT.SCFITN)then ! Maximum iteration count exceeded without convergence.
        if(LOptimization.and.LSCF_converged)then
          LSCF_converged=.false.
          write(UNIout,'(a,i4,a)')'WARNING> UHFCLC: SCF DID NOT CONVERGE AFTER ',SCFITN,' ITERATIONS'
          write(UNIout,'(a)')'Optimization allowed to continue'
        else
          write(UNIout,'(A,I4,1X,A)')'ERROR> UHFCLC: SCF DID NOT CONVERGE AFTER',SCFITN,' ITERATIONS'
          stop 'UHF: DID NOT CONVERGE '
        end if
      end if

      end do ! WHILE

      PM0_total(1:MATlen)=PM0_alpha(1:MATlen)+PM0_beta(1:MATlen)
!
      call END_Xpolation
      deallocate (FMA, FMB, DGMA, DGMB, DPM1, SCRVEC, SOVLP)
!
 1010 FORMAT('At Termination Total Energy is',F17.6,'  Hartrees')
 1030 FORMAT('COEFFICIENTS (BETA SPIN)')
 1040 FORMAT('DENSITY MATRIX (ALPHA SPIN)')
 1060 FORMAT('DENSITY MATRIX (BETA SPIN)')
 1070 FORMAT(' E =',F20.9,10X,'DELTA E =',F15.9,10X,'CONVERGENCE =',1PE13.5,10X,'RUN ABORTED')
 1080 FORMAT(' E =',F20.9,10X,'DELTA E =',F15.9,10X,'CONVERGENCE =',1PE13.5,10X,'RUN ALLOWED TO continue')
 1100 FORMAT('FOCK MATRIX (ALPHA SPIN)')
 1110 FORMAT('FOCK MATRIX (BETA SPIN)')
!
! End of routine UHFCLC
      call PRG_manager ('exit', 'UHFCLC', 'CMO%UHF')
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine INI_UHFCLC
!***********************************************************************
!     Date last modified: October 11, 2001                             *
!     Author: R.A. Poirier                                             *
!     Description: Initialize all RHF parameters.                      *
!***********************************************************************
! Modules:

      implicit none
!
! Local function:
      double precision GET_Enuclear
      character (len=8) :: States(4)
      data States/'Singlet','Doublet','Triplet','Quartet'/
!
! Begin:
      call PRG_manager ('enter', 'INI_UHFCLC', 'UTILITY')
!
! Work arrays:
      allocate (FMA(1:MATlen), FMB(1:MATlen), DPM1(1:MATlen))
      allocate (DGMA(MATlen), DGMB(MATlen), SCRVEC(Nbasis), SOVLP(MATlen))
!
! Intialize the energies
      ENERGY=>ENERGY_UHF
      ENERGY%nuclear=GET_Enuclear()
      ENERGY%HF=ZERO
      ENERGY%elec=ZERO
      ENERGY%total=ZERO
      ENERGY%wavefunction='UHF'//Basis_name(1:len_trim(Basis_name))
!
      FMA(1:MATlen)=ZERO
      FMB(1:MATlen)=ZERO
      CMOA%coeff(1:Nbasis,1:Nbasis)=CMOAG%coeff(1:Nbasis,1:Nbasis)
      CMOB%coeff(1:Nbasis,1:Nbasis)=CMOBG%coeff(1:Nbasis,1:Nbasis)
      CMOA%eigval(1:Nbasis)=CMOAG%eigval(1:Nbasis)
      CMOB%eigval(1:Nbasis)=CMOBG%eigval(1:Nbasis)
!
      PM0=>PM0_total ! pointer
      PM0_total(1:MATlen)=ZERO
!
      CMOA%NoccMO=CMOAG%NoccMO
      CMOB%NoccMO=CMOBG%NoccMO
      CMOA%occupancy=ZERO
      CMOB%occupancy=ZERO
      CMOA%occupancy(1:CMOA%NoccMO)=ONE
      if(CMOB%NoccMO.gt.0)CMOB%occupancy(1:CMOB%NoccMO)=ONE
      call DENBLD (CMOA%coeff, CMOA%occupancy, PM0_alpha, Nbasis, MATlen, CMOA%NoccMO)
      call DENBLD (CMOB%coeff, CMOB%occupancy, PM0_beta, Nbasis, MATlen, CMOB%NoccMO)
      PM0_total(1:MATlen)=PM0_alpha(1:MATlen)+PM0_beta(1:MATlen)
!
      SCFCON=TEN
      Jcycle=1
      if(MUN_prtlev.GT.0)then
        write(UNIout,'(3a,F17.9,a)')'UHF Open Shell (',States(Multiplicity),') SCF, Nuclear Repulsion Energy: ', &
                                         ENERGY_UHF%nuclear,' Hartrees'
        write(UNIout,'(a,1PE12.4)')'Convergence on Density Matrix Required is ',SCFACC
        write(UNIout,'(a,6x,a)')'Cycle    Electronic Energy',' Total Energy    Convergence   Extrapolation'
      end if
!
! End of routine INI_UHFCLC
      call PRG_manager ('exit', 'INI_UHFCLC', 'UTILITY')
      return
      end subroutine INI_UHFCLC
      end subroutine UHFCLC
      subroutine UHFDGR (DGMA, DGMB)
!***********************************************************************
!     Date last modified: June 26, 1992                                *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version.                                       *
!     Form the two-electron part of the Fock matrix, given the         *
!     density matrix and any or all two-electron integrals.            *
!***********************************************************************
! Modules:
      USE program_constants
      USE QM_objects
      USE INT_objects

      implicit none
!
! Input scalar:
!
! Output arrays:
      double precision DGMA(MATlen),DGMB(MATlen)
!
! Local scalars:
      integer BUFSIZ,I,LENGTH
      logical EOF
!
! Begin:
      call PRG_manager ('enter', 'UHFDGR', 'UTILITY')
!
! Initialize DGM.
      DGMA(1:MATlen)=ZERO
      DGMB(1:MATlen)=ZERO
      PM0(1:MATlen)=PM0_alpha(1:MATlen)+PM0_beta(1:MATlen)
!
! Process all INCORE integrals
      if(IJKL_count.gt.0)call BLD_ijkl_UHF (IJKL_INCORE, IJKL_count)
      if(IIKL_count.gt.0)call BLD_iikl_UHF (IIKL_INCORE, IIKL_count)
      if(IJKJ_count.gt.0)call BLD_ijkj_UHF (IJKJ_INCORE, IJKJ_count)
      if(IJJL_count.gt.0)call BLD_ijjl_UHF (IJJL_INCORE, IJJL_count)
      if(IIKK_count.gt.0)call BLD_iikk_UHF (IIKK_INCORE, IIKK_count)
      if(IJJJ_count.gt.0)call BLD_ijjj_UHF (IJJJ_INCORE, IJJJ_count)
      if(IIIL_count.gt.0)call BLD_iiil_UHF (IIIL_INCORE, IIIL_count)
      if(IIII_count.gt.0)call BLD_iiii_UHF (IIII_INCORE, IIII_count)
!
      if(ANY_ONDISK)then
      if(LIJKL_ONDISK)then
      EOF=.FALSE.
      rewind IJKL_unit
      call READ_ijkl (IJKL_BUFFER, IJKL_MAX, IJKL_unit, LENGTH, EOF)
      do while (.not.EOF)
        call BLD_ijkl_UHF (IJKL_BUFFER, LENGTH)
        call READ_ijkl (IJKL_BUFFER, IJKL_MAX, IJKL_unit, LENGTH, EOF)
      end do
      end if ! ONDISK
!
      if(LIIKL_ONDISK)then
      EOF=.FALSE.
      rewind IIKL_unit
      call READ_iikl (IIKL_BUFFER, IIKL_MAX, IIKL_unit, LENGTH, EOF)
      do while (.not.EOF)
         call BLD_iikl_UHF (IIKL_BUFFER, LENGTH)
         call READ_iikl (IIKL_BUFFER, IIKL_MAX, IIKL_unit, LENGTH, EOF)
      end do
      end if ! ONDISK
!
      if(LIJKJ_ONDISK)then
      EOF=.FALSE.
      rewind IJKJ_unit
      call READ_ijkj (IJKJ_BUFFER, IJKJ_MAX, IJKJ_unit, LENGTH, EOF)
      do while (.not.EOF)
        call BLD_ijkj_UHF (IJKJ_BUFFER, LENGTH)
        call READ_ijkj (IJKJ_BUFFER, IJKJ_MAX, IJKJ_unit, LENGTH, EOF)
      end do
      end if ! ONDISK
!
      if(LIJJL_ONDISK)then
      EOF=.FALSE.
      rewind IJJL_unit
      call READ_ijjl (IJJL_BUFFER, IJJL_MAX, IJJL_unit, LENGTH, EOF)
      do while (.not.EOF)
        call BLD_ijjl_UHF (IJJL_BUFFER, LENGTH)
        call READ_ijjl (IJJL_BUFFER, IJJL_MAX, IJJL_unit, LENGTH, EOF)
      end do
      end if ! ONDISK
      end if ! ANY_ONDISK
!
! End of routine UHFDGR
      call PRG_manager ('exit', 'UHFDGR', 'UTILITY')
      return
!
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine BLD_ijkl_UHF (IJKL_IN, &
                              LENGTH)
!***********************************************************************
!     Date last modified: June 26, 1992                                *
!     Author: F. Colonna and R.A. Poirier                              *
!     Description:  MUN Version.                                       *
!     Add IJKL contributions to Matrix G=P*(IJKL+IKJL+ILKJ).           *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer LENGTH
!
! Input arrays:
      type (ints_2e_ijkl) IJKL_IN(LENGTH)
!
! Local scalars:
      integer I,Iao,I1,I2,Jao,JS,JT,Kao,L,Lao,MAX
      double precision GT,G1,G2,G3
!
! Begin:
      call PRG_manager ('enter', 'BLD_ijkl_UHF', 'UTILITY')
!
      if(LENGTH.LE.0)stop 'BLD_ijkl_UHF, BUFFER LENGTH = 0'
!
      do I=1,LENGTH
      Iao=IJKL_IN(I)%I
      Jao=IJKL_IN(I)%J
      Kao=IJKL_IN(I)%K
      Lao=IJKL_IN(I)%L
      G1=IJKL_IN(I)%IJKL
      G2=IJKL_IN(I)%IKJL
      G3=IJKL_IN(I)%ILJK
!
      JS=Iao*(Iao-1)/2+Jao
      JT=Kao*(Kao-1)/2+Lao
      GT=G2+G3
      DGMA(JS)=DGMA(JS)+TWO*PM0(JT)*G1-PM0_alpha(JT)*GT
      DGMB(JS)=DGMB(JS)+TWO*PM0(JT)*G1-PM0_beta(JT)*GT
      DGMA(JT)=DGMA(JT)+TWO*PM0(JS)*G1-PM0_alpha(JS)*GT
      DGMB(JT)=DGMB(JT)+TWO*PM0(JS)*G1-PM0_beta(JS)*GT
      JS=Iao*(Iao-1)/2+Kao
      JT=Jao*(Jao-1)/2+Lao
      GT=G1+G3
      DGMA(JS)=DGMA(JS)+TWO*PM0(JT)*G2-PM0_alpha(JT)*GT
      DGMB(JS)=DGMB(JS)+TWO*PM0(JT)*G2-PM0_beta(JT)*GT
      DGMA(JT)=DGMA(JT)+TWO*PM0(JS)*G2-PM0_alpha(JS)*GT
      DGMB(JT)=DGMB(JT)+TWO*PM0(JS)*G2-PM0_beta(JS)*GT
      JS=Iao*(Iao-1)/2+Lao
      JT=Jao*(Jao-1)/2+Kao
      GT=G1+G2
      DGMA(JS)=DGMA(JS)+TWO*PM0(JT)*G3-PM0_alpha(JT)*GT
      DGMB(JS)=DGMB(JS)+TWO*PM0(JT)*G3-PM0_beta(JT)*GT
      DGMA(JT)=DGMA(JT)+TWO*PM0(JS)*G3-PM0_alpha(JS)*GT
      DGMB(JT)=DGMB(JT)+TWO*PM0(JS)*G3-PM0_beta(JS)*GT
      end do
!
! End of routine BLD_ijkl_UHF
      call PRG_manager ('exit', 'BLD_ijkl_UHF', 'UTILITY')
      return
      end subroutine BLD_ijkl_UHF
      subroutine BLD_ijkj_UHF (IJKJ_IN, &
                               LENGTH)
!***********************************************************************
!     Date last modified: June 26, 1992                                *
!     Author: R.A. Poirier and F. Colonna                              *
!     Description:  MUN Version.                                       *
!     Add IJKL contributions to Matrix G=P*(IJKL+IKJL).                *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer LENGTH
!
! Input arrays:
      type (ints_2e_ijkj) IJKJ_IN(LENGTH)
!
! Local scalars:
      integer I,Iao,I1,I2,Jao,Kao,L,JS,JT,MAX
      double precision GT,G1,G2
!
! Begin:
      call PRG_manager ('enter', 'BLD_ijkj_UHF', 'UTILITY')
!
        if(LENGTH.LE.0)stop 'BLD_ijkj_UHF, IJKJ LENGTH = 0'
!
        do I=1,LENGTH
        Iao=IJKJ_IN(I)%I
        Jao=IJKJ_IN(I)%J
        Kao=IJKJ_IN(I)%K
        G1=IJKJ_IN(I)%IJKJ
        G2=IJKJ_IN(I)%IKJJ
!
        JS=Iao*(Iao-1)/2+Jao
        JT=Kao*(Kao-1)/2+Jao
        GT=G1+G2
        DGMA(JS)=DGMA(JS)+TWO*PM0(JT)*G1-PM0_alpha(JT)*GT
        DGMB(JS)=DGMB(JS)+TWO*PM0(JT)*G1-PM0_beta(JT)*GT
        DGMA(JT)=DGMA(JT)+TWO*PM0(JS)*G1-PM0_alpha(JS)*GT
        DGMB(JT)=DGMB(JT)+TWO*PM0(JS)*G1-PM0_beta(JS)*GT
        JS=Iao*(Iao-1)/2+Kao
        JT=Jao*(Jao+1)/2
        DGMA(JS)=DGMA(JS)+PM0(JT)*G2-PM0_alpha(JT)*G1
        DGMB(JS)=DGMB(JS)+PM0(JT)*G2-PM0_beta(JT)*G1
        DGMA(JT)=DGMA(JT)+TWO*PM0(JS)*G2-TWO*PM0_alpha(JS)*G1
        DGMB(JT)=DGMB(JT)+TWO*PM0(JS)*G2-TWO*PM0_beta(JS)*G1
        end do
!
! End of routine BLD_ijkj_UHF
      call PRG_manager ('exit', 'BLD_ijkj_UHF', 'UTILITY')
      return
      end subroutine BLD_ijkj_UHF
      subroutine BLD_iikk_UHF (IIKK_IN, &
                               LENGTH)
!***********************************************************************
!     Date last modified: June 26, 1992                                *
!     Author: R.A. Poirier and F. Colonna                              *
!     Description:  MUN Version.                                       *
!     Add IJKL contributions to Matrix G=P*(IJKL+IKJL).                *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer LENGTH
!
! Input arrays:
      type (ints_2e_iikk) IIKK_IN(LENGTH)
!
! Local scalars:
      integer I,Iao,I1,I2,Jao,Kao,L,JS,JT,MAX
      double precision GT,G1,G2
!
! Begin:
      call PRG_manager ('enter', 'BLD_iikk_UHF', 'UTILITY')
!
        if(LENGTH.LT.0)stop 'BLD_iikk_UHF, IIKK LENGTH = 0'
!
        do I=1,LENGTH
        Iao=IIKK_IN(I)%I
        Jao=IIKK_IN(I)%K
        G1=IIKK_IN(I)%iikk
        G2=IIKK_IN(I)%ikik
        JS=Iao*(Iao+1)/2
        JT=Jao*(Jao+1)/2
        DGMA(JS)=DGMA(JS)+PM0(JT)*G1-PM0_alpha(JT)*G2
        DGMB(JS)=DGMB(JS)+PM0(JT)*G1-PM0_beta(JT)*G2
        DGMA(JT)=DGMA(JT)+PM0(JS)*G1-PM0_alpha(JS)*G2
        DGMB(JT)=DGMB(JT)+PM0(JS)*G1-PM0_beta(JS)*G2
        JS=Iao*(Iao-1)/2+Jao
        DGMA(JS)=DGMA(JS)+(PM0(JS)+PM0_beta(JS))*G2-PM0_alpha(JS)*G1
        DGMB(JS)=DGMB(JS)+(PM0(JS)+PM0_alpha(JS))*G2-PM0_beta(JS)*G1
        end do
!
! End of routine BLD_iikk_UHF
      call PRG_manager ('exit', 'BLD_iikk_UHF', 'UTILITY')
      return
      end subroutine BLD_iikk_UHF
      subroutine BLD_ijjl_UHF (IJJL_IN, &
                               LENGTH)
!***********************************************************************
!     Date last modified: June 26, 1992                                *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version.                                       *
!     Add IJKL contributions to Matrix G=P*(IJKL+IKJL).                *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer LENGTH
!
! Input arrays:
      type (ints_2e_ijjl) IJJL_IN(LENGTH)
!
! Local scalars:
      integer I,Iao,I1,I2,Jao,Kao,L,JS,JT,MAX
      double precision GT,G1,G2
!
! Begin:
      call PRG_manager ('enter', 'BLD_ijjl_UHF', 'UTILITY')
!
        if(LENGTH.LE.0)stop 'BLD_ijjl_UHF, IJJL LENGTH = 0'
!
        do I=1,LENGTH
        Iao=IJJL_IN(I)%I
        Jao=IJJL_IN(I)%J
        Kao=IJJL_IN(I)%L
        G1=IJJL_IN(I)%ijjl
        G2=IJJL_IN(I)%iljj
!
        JS=Iao*(Iao-1)/2+Jao
        JT=Jao*(Jao-1)/2+Kao
        GT=G1+G2
        DGMA(JS)=DGMA(JS)+TWO*PM0(JT)*G1-PM0_alpha(JT)*GT
        DGMB(JS)=DGMB(JS)+TWO*PM0(JT)*G1-PM0_beta(JT)*GT
        DGMA(JT)=DGMA(JT)+TWO*PM0(JS)*G1-PM0_alpha(JS)*GT
        DGMB(JT)=DGMB(JT)+TWO*PM0(JS)*G1-PM0_beta(JS)*GT
        JS=Iao*(Iao-1)/2+Kao
        JT=Jao*(Jao+1)/2
        DGMA(JS)=DGMA(JS)+PM0(JT)*G2-PM0_alpha(JT)*G1
        DGMB(JS)=DGMB(JS)+PM0(JT)*G2-PM0_beta(JT)*G1
        DGMA(JT)=DGMA(JT)+TWO*PM0(JS)*G2-TWO*PM0_alpha(JS)*G1
        DGMB(JT)=DGMB(JT)+TWO*PM0(JS)*G2-TWO*PM0_beta(JS)*G1
        end do
!
! End of routine BLD_ijjl_UHF
      call PRG_manager ('exit', 'BLD_ijjl_UHF', 'UTILITY')
      return
      end subroutine BLD_ijjl_UHF
      subroutine BLD_iikl_UHF (IIKL_IN, &
                               LENGTH)
!***********************************************************************
!     Date last modified: June 26, 1992                                *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version.                                       *
!     Add IJKL contributions to Matrix G=P*(IJKL+IKJL).                *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer LENGTH
!
! Input arrays:
      type (ints_2e_iikl) IIKL_IN(LENGTH)
!
! Local scalars:
      integer I,Iao,I1,I2,Jao,Kao,L,JS,JT,MAX
      double precision GT,G1,G2
!
! Begin:
      call PRG_manager ('enter', 'BLD_iikl_UHF', 'UTILITY')
!
        if(LENGTH.LE.0)stop 'BLD_iikl_UHF, IIKL LENGTH = 0'
!
        do I=1,LENGTH
        Iao=IIKL_IN(I)%I
        Jao=IIKL_IN(I)%K
        Kao=IIKL_IN(I)%L
        G1=IIKL_IN(I)%iikl
        G2=IIKL_IN(I)%ikil
!
        JS=Iao*(Iao+1)/2
        JT=Jao*(Jao-1)/2+Kao
        DGMA(JS)=DGMA(JS)+TWO*(PM0(JT)*G1-PM0_alpha(JT)*G2)
        DGMB(JS)=DGMB(JS)+TWO*(PM0(JT)*G1-PM0_beta(JT)*G2)
        DGMA(JT)=DGMA(JT)+PM0(JS)*G1-PM0_alpha(JS)*G2
        DGMB(JT)=DGMB(JT)+PM0(JS)*G1-PM0_beta(JS)*G2
        JS=Iao*(Iao-1)/2+Jao
        JT=Iao*(Iao-1)/2+Kao
        GT=G1+G2
        DGMA(JS)=DGMA(JS)+TWO*PM0(JT)*G2-PM0_alpha(JT)*GT
        DGMB(JS)=DGMB(JS)+TWO*PM0(JT)*G2-PM0_beta(JT)*GT
        DGMA(JT)=DGMA(JT)+TWO*PM0(JS)*G2-PM0_alpha(JS)*GT
        DGMB(JT)=DGMB(JT)+TWO*PM0(JS)*G2-PM0_beta(JS)*GT
        end do
!
! End of routine BLD_iikl_UHF
      call PRG_manager ('exit', 'BLD_iikl_UHF', 'UTILITY')
      return
      end subroutine BLD_iikl_UHF
      subroutine BLD_ijjj_UHF (IJJJ_IN, &
                               LENGTH)
!***********************************************************************
!     Date last modified: June 26, 1992                                *
!     Author: R.A. Poirier and F. Colonna                              *
!     Description:  MUN Version.                                       *
!     Add IJKL contributions to Matrix G=P*(IJKL+IKJL).                *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer LENGTH
!
! Input arrays:
      type (ints_2e_ijjj) IJJJ_IN(LENGTH)
!
! Local scalars:
      integer I,Iao,I1,I2,Jao,L,JS,JT,MAX
      double precision G1
!
! Begin:
      call PRG_manager ('enter', 'BLD_ijjj_UHF', 'UTILITY')
!
      if(LENGTH.LE.0)stop 'BLD_ijjj_UHF, IJJJ BUFFER LENGTH = 0'
!
      do I=1,LENGTH
        Iao=IJJJ_IN(I)%I
        Jao=IJJJ_IN(I)%J
        G1=IJJJ_IN(I)%ijjj
        JS=Iao*(Iao-1)/2+Jao
        JT=Jao*(Jao+1)/2
        DGMA(JS)=DGMA(JS)+PM0_beta(JT)*G1
        DGMB(JS)=DGMB(JS)+PM0_alpha(JT)*G1
        DGMA(JT)=DGMA(JT)+TWO*PM0_beta(JS)*G1
        DGMB(JT)=DGMB(JT)+TWO*PM0_alpha(JS)*G1
      end do
!
! End of routine BLD_ijjj_UHF
      call PRG_manager ('exit', 'BLD_ijjj_UHF', 'UTILITY')
      return
      end subroutine BLD_ijjj_UHF
      subroutine BLD_iiil_UHF (IIIL_IN, &
                               LENGTH)
!***********************************************************************
!     Date last modified: June 26, 1992                                *
!     Author: R.A. Poirier and F. Colonna                              *
!     Description:  MUN Version.                                       *
!     Add IJKL contributions to Matrix G=P*(IJKL+IKJL).                *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer LENGTH
!
! Input arrays:
      type (ints_2e_iiil) IIIL_IN(LENGTH)
!
! Local scalars:
      integer I,Iao,I1,I2,Jao,L,JS,JT,MAX
      double precision G1
!
! Begin:
      call PRG_manager ('enter', 'BLD_iiil_UHF', 'UTILITY')
!
      if(LENGTH.LE.0)stop 'BLD_iiil_UHF, IIIL BUFFER LENGTH = 0'
!
      do I=1,LENGTH
      Iao=IIIL_IN(I)%I
      Jao=IIIL_IN(I)%L
      G1=IIIL_IN(I)%iiil
      JS=Iao*(Iao+1)/2
      JT=Iao*(Iao-1)/2+Jao
      DGMA(JS)=DGMA(JS)+TWO*PM0_beta(JT)*G1
      DGMB(JS)=DGMB(JS)+TWO*PM0_alpha(JT)*G1
      DGMA(JT)=DGMA(JT)+PM0_beta(JS)*G1
      DGMB(JT)=DGMB(JT)+PM0_alpha(JS)*G1
      end do
!
! End of routine BLD_iiil_UHF
      call PRG_manager ('exit', 'BLD_iiil_UHF', 'UTILITY')
      return
      end subroutine BLD_iiil_UHF
      subroutine BLD_iiii_UHF (IIII_IN, &
                               LENGTH)
!***********************************************************************
!     Date last modified: June 26, 1992                                *
!     Author: R.A. Poirier and F. Colonna                              *
!     Description:  MUN Version.                                       *
!     Add IIII contributions to Matrix G=P*(IIII+IKJL).                *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer LENGTH
!
! Input arrays:
      type (ints_2e_iiii) IIII_IN(LENGTH)
!
! Local scalars:
      integer I,Iao,I1,I2,Jao,L,JS,JT,MAX
      double precision G1
!
! Begin:
      call PRG_manager ('enter', 'BLD_iiii_UHF', 'UTILITY')
!
      if(LENGTH.LE.0)stop 'BLD_iiii_UHF, IIII BUFFER LENGTH = 0'
!
      do I=1,LENGTH
      Iao=IIII_IN(I)%I
      G1=IIII_IN(I)%iiii
      JS=Iao*(Iao+1)/2
      DGMA(JS)=DGMA(JS)+PM0_beta(JS)*G1
      DGMB(JS)=DGMB(JS)+PM0_alpha(JS)*G1
      end do
!
! End of routine BLD_iiii_UHF
      call PRG_manager ('exit', 'BLD_iiii_UHF', 'UTILITY')
      return
      end subroutine BLD_iiii_UHF
      end subroutine UHFDGR
      function TRAOPN (A, B, N, MATlen)
!***********************************************************************
!     Date last modified: June 26, 1992                                *
!     Author:                                                          *
!     Description:  QCPE Version                                       *
!     Trace of a product of double symmetric matrices A and B          *
!     stored as linear vectors.                                        *
!***********************************************************************
! Modules:
      USE program_constants

      implicit none
!
! Input scalars:
      integer MATlen,N
!
! Input arrays:
      double precision A(MATlen),B(MATlen)
!
! Local scalars:
      integer I,J,K,L
      double precision TRAOPN
!
! Begin:
      call PRG_manager ('enter', 'TRAOPN', 'UTILITY')
!
      TRAOPN=ZERO
      K=0
      do L=1,2
      do J=1,N
      do I=1,J
      K=K+1
      TRAOPN=TRAOPN+TWO*A(K)*B(K)
      end do ! I
      TRAOPN=TRAOPN-A(K)*B(K)
      end do ! J
      end do ! L
!
! End of function TRAOPN
      call PRG_manager ('exit', 'TRAOPN', 'UTILITY')
      return
      END
      subroutine TSPIN (AOPEN, &
                        C0)
!***********************************************************************
!     Date last modified: May 10, 1995                     Version 1.0 *
!     Author:                                                          *
!     Description:  U. of T. Version                                   *
!     Compute S and S(S+1) by Trace (DB*S*DA*S).                       *
!     Reference: F.L. Pilar, "Elementary Quantum Chemistry",           *
!                McGraw-Hill, Toronto, 1968, PP 287-293.               *
!                                                                      *
!     AOPEN gives the total number of Alpha-Beta open shell singlet    *
!     pairs in the open shells.  A negative value indicates that       *
!     this routine is to calculate S(S+1), then subtract 1.0 if the    *
!     value deviates greatly from zero - This will work for UHF        *
!     calculations on closed shell singlets (which should give a       *
!     value of 0.0 for S(S+1)), and for open shell singlets with only  *
!     two open shell orbitals.  AOPEN is ignored except for singlet    *
!     states, if AOPEN is less than zero.                              *
!                                                                      *
!     C0 gives the coefficient of the reference configuration for this *
!     wavefunction, and would be 1.0D0 for UHF or RHF open shell SCFs, *
!     as this value is only needed for MC-SCF or GVB wavefunctions.    *
!     The total spin is usually incorrect for multi-configuration SCF  *
!     or GVB SCF calculations - The expression to calculate the spin   *
!     for such wavefunctions is under investigation, but for now       *
!     the spin is calculated using the standard formula, then replaced *
!     by the exact value derived from the multiplicity.                *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE QM_objects
      USE INT_objects
      use type_molecule

      implicit none
!
! Input scalars:
      double precision AOPEN,C0
!
! Local arrays:
      double precision SPIN(2)
!
! Work arrays:
      double precision, dimension(:,:), allocatable :: SCRM1
      double precision, dimension(:,:), allocatable :: SQSM
      double precision, dimension(:,:), allocatable :: PM0w
!
! Local scalars:
      integer :: I,J
      logical MULTIC
      double precision AMB,APB,CONTAM,PT01,PT05,PT25,SPE,TRACE,XSE,XY
!
! Local parameters:
      parameter (PT01=0.01D0,PT05=0.05D0,PT25=0.25D0)
!
! Begin:
      call PRG_manager ('enter', 'TSPIN', 'UTILITY')
!
      allocate (SCRM1(Nbasis,Nbasis), SQSM(Nbasis,Nbasis), PM0w(Nbasis,Nbasis))
!
! Create the square symmetry P_alpha in PM0w
      call UPT_TO_SQS (PM0_alpha, MATlen, PM0w, Nbasis)
      call UPT_TO_SQS (OVRLAP, MATlen, SQSM, Nbasis)
! Form matrix product SM*Alpha*SM.
      SCRM1=matmul(matmul(SQSM,PM0w), SQSM)
!
      call UPT_TO_SQS (PM0_beta, MATlen, PM0w, Nbasis)
! Form trace of the product Beta*(SM*Alpha*SM).
      TRACE=ZERO
      do I=1,Nbasis
      do J=1,I
      XY=SCRM1(I,J)*PM0w(J,I)
      TRACE=TRACE+XY+XY
      end do ! J
      TRACE=TRACE-XY
      end do ! I
!
      APB=CMOA%NoccMO+CMOB%NoccMO
      AMB=CMOA%NoccMO-CMOB%NoccMO
      SPIN(2)=PT5*APB+PT25*AMB**2-TRACE
!
! Watch for open shell singlets.
      if(AOPEN.GE.ZERO)then
! Reduce SPIN(2) by 1.0 for each Beta spin open shell electron
! in open singlet pairs.
        SPIN(2)=SPIN(2)-AOPEN
      else if(AMB.EQ.ZERO)then
! UHF Case - If SPIN(2) is close to zero, assume closed shell;
! else subtract 1.0 for one open shell singlet pair of electrons.
        SPIN(2)=DABS(SPIN(2))
        if(SPIN(2).GT.PT25)SPIN(2)=SPIN(2)-ONE
      end if
!
      SPIN(2)=DABS(SPIN(2))
      MULTIC=.FALSE.
!
      if(DABS(C0).NE.ONE)then
! Apply empirical correction for MC-SCF and GVB wavefunctions:
! The total spin seems to be too large by a factor of
! 2.0*(1.0-C0**2), so reduce S**2 by this amount.
        MULTIC=.TRUE.
        SPIN(2)=DABS(SPIN(2)-TWO*(ONE-C0*C0))
      end if
!
      SPIN(1)=DABS(-PT5+PT5*DSQRT(ONE+FOUR*SPIN(2)))
      XSE=PT5*DBLE(Multiplicity-1)
      SPE=XSE*(XSE+ONE)
      if(MUN_prtlev.GT.0)write(UNIout,1000)SPIN(1),XSE,SPIN(2),SPE
      if(.not.MULTIC) then
      CONTAM=DABS(SPIN(1)-XSE)
      if(XSE.NE.ZERO)CONTAM=CONTAM/XSE
      if(CONTAM.GE.PT05)then
        if(MUN_prtlev.GT.0)write(UNIout,1020)
      else if(CONTAM.GT.PT01)then
        if(MUN_prtlev.GT.0)write(UNIout,1010)
      end if
      else
!
! Replace calculated spin with exact spin for MC-SCF/GVB.
      SPIN(1)=XSE
      SPIN(2)=SPE
      if(MUN_prtlev.GT.0)write(UNIout,1030)
!
      end if
!
      deallocate (SCRM1, SQSM, PM0w)
!
 1000 FORMAT(/'S =',F6.3,' (',F6.3,' EXPECTED),   S(S+1) =',F8.4,' (',F8.4,' EXPECTED)')
 1010 FORMAT('*** YOU ARE SKATING ON THIN ICE DUE TO SPIN CONTAMINATION'/ &
             ' *** GEOMETRIES/ENERGIES MAY BE AFFECTED')
 1020 FORMAT('*** YOU HAVE FALLEN THROUGH THE ICE AND DROWNED DUE TO',' SPIN CONTAMINATION' &
            /' *** GEOMETRIES/ENERGIES ARE SIGNIFICANTLY AFFECTED')
 1030 FORMAT('*** THE S AND S(S+1) VALUES ABOVE HAVE BEEN REPLACED BY THE EXACT VALUES DETERMINED FROM THE MULTIPLICITY', &
       /5X,'AS THE FORMULA TO CALCULATE THE SPIN FOR MC-SCF/GVB',' WAVEfunctionS IS NOT YET AVAILABLE. ***')
!
! End of routine TSPIN
      call PRG_manager ('exit', 'TSPIN', 'UTILITY')
      return
      END
