      subroutine QM_PRINT_manager (ObjName, Modality)
!********************************************************************************
!     Date last modified: April 24, 2008                           Version 2.0 *
!     Author: R.A. Poirier                                                      *
!     Description: Calls the appropriate routine to "Command" the object nameIN *
!     Command can be "PRINT" or "KILL"                                          *
!     Pwhat can be "OBJECT", "DOCUMENTATION" or "TABLE"                         *
!********************************************************************************
! Modules:
      USE program_files
      USE program_objects

      implicit none
!
! Input scalars:
      character*(*), intent(IN) :: ObjName,Modality
!
! Local scalars:
!
! Begin:
      call PRG_manager ('enter', 'QM_PRINT_manager', 'UTILITY')

      SelectObject:   select case (ObjName)
      case ('CMO')
        select case (Modality)
          case ('RHF')
            call PRT_MO_coeff
          case ('ROHF')
            call PRT_MO_coeff
        end select
      case ('CMO_GUESS')
        select case (Modality)
          case ('RHF')
            call BLD_GMatrix_STO3G
        end select
      case ('ENERGY')
        select case (Modality)
          case ('RHF')
        end select
      case ('ENERGY_COMPONENTS')
        select case (Modality)
          case ('RHF')
        end select
      case ('DENSITY_1MATRIX')
        call PRT_DENSITY_MATRIX
      case ('DENSITY')
        select case (Modality)
          case ('NUMERICAL')
          case ('POINT')
          case ('ATOMIC')
          case ('MESH')
        end select
      case ('DEL_DENSITY')
        select case (Modality)
          case ('POINT')
          case ('ATOMIC')
          case ('MESH')
        end select
      case ('DEL2_DENSITY')
        select case (Modality)
          case ('POINT')
          case ('ATOMIC')
          case ('MESH')
        end select
      case ('RADIAL_DENSITY')
        select case (Modality)
          case ('POINT')
          case ('ATOMIC')
          case ('MESH')
        end select
      case ('DEL_RADIAL_DENSITY')
        select case (Modality)
          case ('POINT')
          case ('ATOMIC')
          case ('MESH')
        end select
      case ('DEL2_RADIAL_DENSITY')
        select case (Modality)
          case ('POINT')
          case ('ATOMIC')
          case ('MESH')
        end select
      case ('AO_BY_AO')
        select case (Modality)
          case ('MULLIKEN')
            call PRT_AO_by_AO_Mulliken
        end select
      case ('ATOMIC')
        select case (Modality)
          case ('MULLIKEN')
            call PRT_Atomic_Mulliken
        end select
      case ('NET_ATOMIC')
        select case (Modality)
          case ('MULLIKEN')
            call PRT_Net_Atomic_Mulliken
        end select
      case ('BOND_ORDER')
        select case (Modality)
          case ('MEYER')
            call PRT_BOND_ORDER_MEYER
          case ('PM3')
            call PRT_BOND_ORDER_PM3
          case ('PM6')
            call PRT_BOND_ORDER_PM6
        end select
      case ('SHAPE_AND_SIZE')
      case ('R12_AB')
      case ('DIPOLE_MOMENT')
      case ('AIM')
      case ('FREQUENCIES')
      case ('CRITICAL_POINTS')
      case ('CORRELATION')
      case ('DENSITY_BOND')
      case ('BOND_ELECTRONS')
      case ('ALL_BONDS_ELECTRONS')
      case ('LDMATRIX')
      case ('ENERGY_KINETIC')
      case ('ENERGY_VNE')
      case ('ENERGY_COULOMB')
      case ('ENERGY_ECHANGE')
      case ('ENERGY_VEE')
      case ('KAB')                                                               
      case ('JAB') 
      case ('KINETIC_BOND')
      case ('EXCHANGE_BOND')
      case ('COULOMB_MO_BOND')
      case ('COULOMB_BOND')
      case ('VNE_BOND')
      case ('EAM')
        select case(Modality)
          case('NI')
        end select
      case ('MB')
        select case(Modality)
          case('NI')
        end select
      case ('RAM')
        select case(Modality)
          case('NI')
        end select

      case default
        write(UNIout,*)'QM_PRINT_manager>'
        write(UNIout,*)'No such object "',Objname(1:len_trim(Objname)), &
                       '" for Modality "',Modality(1:len_trim(Modality)),'"'
      end select SelectObject
!
! end of function QM_PRINT_manager
      call PRG_manager ('exit', 'QM_PRINT_manager', 'UTILITY')
      return
      end subroutine QM_PRINT_manager
