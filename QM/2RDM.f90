      subroutine BLD_2RDM_AO
!***********************************************************************
!     Date last modified: April 9, 2019                                *
!     Author: J.W. Hollett                                             *
!     Description: Build the two-electron density matrix (2-RDM) for   *
!                  the evaulation of two-electron properties.          *
!***********************************************************************
! Modules:
      USE program_files
      USE QM_objects
      USE INT_objects

      implicit none
!
! Local scalars:
      double precision :: VeeHFaa, Veecorraa, Veeaa
      double precision :: VeeHFbb, Veecorrbb, Veebb
      double precision :: VeeHFab, Veecorrab, Veeab
      double precision :: VeeHFba, Veecorrba, Veeba
!
! Local arrays:
      double precision, allocatable, dimension(:) :: TRDM_corr_AOaa, TRDM_corr_AObb
      double precision, allocatable, dimension(:) :: TRDM_corr_AOab, TRDM_corr_AOba
!
! Begin:
      call PRG_manager ('enter', 'BLD_2RDM_AO', '2RDM%AO')
!
! Build HF 2RDM first
      call BLD_2RDM_AO_HF
!
! Build correlated 2RDM
      call BLD_2RDM_AO_corr
!
      allocate(TRDM_corr_AOaa(Nbasis**4),TRDM_corr_AObb(Nbasis**4)) 
      allocate(TRDM_corr_AOab(Nbasis**4),TRDM_corr_AOba(Nbasis**4)) 
!
      TRDM_corr_AOaa = TRDM_AOaa - TRDM_HF_AOaa
      TRDM_corr_AObb = TRDM_AObb - TRDM_HF_AObb
      TRDM_corr_AOab = TRDM_AOab - TRDM_HF_AOab
      TRDM_corr_AOba = TRDM_AOba - TRDM_HF_AOba
!
! Test it by computing the two-electron energy
      call GET_object ('CFT','2EINT','OS')
!
      write(UNIout,'(/a)')' Calculate two-electron energies to verify 2-RDMs '
      write(UNIout,'(a)')' (Using 2-RDMs in AO basis) '
!
      VeeHFaa = dot_product(TRDM_HF_AOaa,eriAO)
      write(UNIout,'(/a,F14.10)')'         HF two-electron energy from alpha-alpha 2RDM contraction: ',VeeHFaa
!
      Veecorraa = dot_product(TRDM_corr_AOaa,eriAO)
      write(UNIout,'(a,F14.10)') 'Two-electron correlation energy from alpha-alpha 2RDM contraction: ',Veecorraa
!
      Veeaa = dot_product(TRDM_AOaa,eriAO)
      write(UNIout,'(a,F14.10)') '      Total two-electron energy from alpha-alpha 2RDM contraction: ',Veeaa
!
      VeeHFbb = dot_product(TRDM_HF_AObb,eriAO)
      write(UNIout,'(/a,F14.10)')'         HF two-electron energy from beta-beta 2RDM contraction: ',VeeHFbb
!
      Veecorrbb = dot_product(TRDM_corr_AObb,eriAO)
      write(UNIout,'(a,F14.10)') 'Two-electron correlation energy from beta-beta 2RDM contraction: ',Veecorrbb
!
      Veebb = dot_product(TRDM_AObb,eriAO)
      write(UNIout,'(a,F14.10)') '      Total two-electron energy from beta-beta 2RDM contraction: ',Veebb
!
      VeeHFab = dot_product(TRDM_HF_AOab,eriAO)
      write(UNIout,'(/a,F14.10)')'         HF two-electron energy from alpha-beta 2RDM contraction: ',VeeHFab
!
      Veecorrab = dot_product(TRDM_corr_AOab,eriAO)
      write(UNIout,'(a,F14.10)') 'Two-electron correlation energy from alpha-beta 2RDM contraction: ',Veecorrab
!
      Veeab = dot_product(TRDM_AOab,eriAO)
      write(UNIout,'(a,F14.10)') '      Total two-electron energy from alpha-beta 2RDM contraction: ',Veeab
!
      VeeHFba = dot_product(TRDM_HF_AOba,eriAO)
      write(UNIout,'(/a,F14.10)')'         HF two-electron energy from beta-alpha 2RDM contraction: ',VeeHFba
!
      Veecorrba = dot_product(TRDM_corr_AOba,eriAO)
      write(UNIout,'(a,F14.10)') 'Two-electron correlation energy from beta-alpha 2RDM contraction: ',Veecorrba
!
      Veeba = dot_product(TRDM_AOba,eriAO)
      write(UNIout,'(a,F14.10)') '      Total two-electron energy from beta-alpha 2RDM contraction: ',Veeba
!
      write(UNIout,'(/a,F14.10)')'         HF two-electron energy from total 2RDM contraction: ',&
      VeeHFaa+VeeHFbb+VeeHFab+VeeHFba
!
      write(UNIout,'(a,F14.10)') 'Two-electron correlation energy from total 2RDM contraction: ',&
      Veecorraa+Veecorrbb+Veecorrab+Veecorrba
!
      write(UNIout,'(a,F14.10)')'      Total two-electron energy from total 2RDM contraction: ',&
      Veeaa+Veebb+Veeab+Veeba
!
      deallocate(TRDM_corr_AOaa,TRDM_corr_AObb)
      deallocate(TRDM_corr_AOab,TRDM_corr_AOba)
!
! End of routine BLD_2RDM_AO
      call PRG_manager ('exit', 'BLD_2RDM_AO', '2RDM%AO')
      return
      end subroutine BLD_2RDM_AO
      subroutine BLD_2RDM_AO_HF
!***********************************************************************
!     Date last modified: April 9, 2019                                *
!     Author: J.W. Hollett                                             *
!     Description: Build the HF two-electron density matrix (2-RDM)    *
!***********************************************************************
! Modules:
      USE program_files
      USE QM_objects
      USE QM_defaults
      USE type_basis_set

      implicit none
!
! Local scalars:
      integer :: iAO, jAO, kAO, lAO, ijkl_index
!
! Local arrays:
      double precision, allocatable, dimension(:,:) :: Palpha, Pbeta
!
! Begin:
      call PRG_manager ('enter', 'BLD_2RDM_AO_HF', 'UTILITY')
!
! 2RDMs notation
! physicist notation for MO basis <12|12>
!   chemist notation for AO basis [11|22]
!
      call GET_object ('QM','SPIN_DENSITY_1MATRIX', 'AO')
!
      if(allocated(Palpha))then
        deallocate(Palpha,Pbeta)
      end if 
      allocate(Palpha(Nbasis,Nbasis),Pbeta(Nbasis,Nbasis))
!
! Use HF density matrices
      call UPT_TO_SQS (PM0HF_alpha, MATlen, Palpha, Nbasis)
      call UPT_TO_SQS (PM0HF_beta, MATlen, Pbeta, Nbasis)
!
      if(allocated(TRDM_HF_AOaa))then
        deallocate(TRDM_HF_AOaa)
      end if
      if(allocated(TRDM_HF_AObb))then
        deallocate(TRDM_HF_AObb)
      end if
      if(allocated(TRDM_HF_AOab))then
        deallocate(TRDM_HF_AOab)
      end if
      if(allocated(TRDM_HF_AOba))then
        deallocate(TRDM_HF_AOba)
      end if
      allocate(TRDM_HF_AOaa(Nbasis**4))
      allocate(TRDM_HF_AObb(Nbasis**4))
      allocate(TRDM_HF_AOab(Nbasis**4))
      allocate(TRDM_HF_AOba(Nbasis**4))
!
      ijkl_index = 0
      do iAO = 1, Nbasis
        do jAO = 1, Nbasis
          do kAO = 1, Nbasis
            do lAO = 1, Nbasis
              ijkl_index = ijkl_index + 1
!
              TRDM_HF_AOaa(ijkl_index) = Palpha(jAO,iAO)*Palpha(lAO,kAO)&
                                       - Palpha(lAO,iAO)*Palpha(jAO,kAO)
              TRDM_HF_AObb(ijkl_index) = Pbeta(jAO,iAO)*Pbeta(lAO,kAO)&
                                       - Pbeta(lAO,iAO)*Pbeta(jAO,kAO)
              TRDM_HF_AOab(ijkl_index) = Palpha(jAO,iAO)*Pbeta(lAO,kAO)
              TRDM_HF_AOba(ijkl_index) = Pbeta(jAO,iAO)*Palpha(lAO,kAO)
!
            end do ! lAO
          end do ! kAO
        end do ! jAO
      end do ! iAO
!
      TRDM_HF_AOaa = 0.5D0*TRDM_HF_AOaa
      TRDM_HF_AObb = 0.5D0*TRDM_HF_AObb
      TRDM_HF_AOab = 0.5D0*TRDM_HF_AOab
      TRDM_HF_AOba = 0.5D0*TRDM_HF_AOba
!
! End of routine BLD_2RDM_AO_HF
      call PRG_manager ('exit', 'BLD_2RDM_AO_HF', 'UTILITY')
      return
      end subroutine BLD_2RDM_AO_HF
      subroutine BLD_spin_density_matrix
!***********************************************************************
!     Date last modified: April 9, 2019                                *
!     Author: J.W. Hollett                                             *
!     Description: Build the HF spin-density matrices.                 *
!***********************************************************************
! Modules:
      USE program_files
      USE QM_objects
      USE QM_defaults
      USE type_molecule
      USE INT_objects

      implicit none
!
! Local scalars:
      double precision :: E1e_alpha, E1e_beta
      double precision :: T_alpha, T_beta
      double precision :: THF_alpha, THF_beta
      double precision :: Vne_alpha, Vne_beta
      double precision :: VneHF_alpha, VneHF_beta
      double precision :: E1eHF_alpha, E1eHF_beta
      double precision :: Ne_alpha, Ne_beta
      double precision :: NeHF_alpha, NeHF_beta
!
! functions:
      double precision :: traclo
!
! Local arrays:
      double precision, allocatable, dimension(:) :: ona, onb
      double precision, allocatable, dimension(:,:) :: Pa, Pb, Pa2, Pb2, inpMO
      double precision, allocatable, dimension(:) :: Pcore, oncore

! Begin:
      call PRG_manager ('enter', 'BLD_spin_density_matrix', 'UTILITY')
!
! Getting 1rdm will create whatever we need
!
      call GET_object ('QM','DENSITY_1MATRIX',Wavefunction)
!
      select case(Wavefunction)
!
        case('UHF')
          ! do nothing, spin density matrices already created

        case('RHF')
          ! set-up occupancies
          NAelectrons = (Nelectrons + Multiplicity - 1)/2 ! why is this not calculated by default?
          NBelectrons = Nelectrons - NAelectrons
          allocate(ona(Nbasis),onb(Nbasis))
          ona = 0.0D0
          onb = 0.0D0
          ! closed-shell
          ona(1:NAelectrons) = 1.0D0
          onb(1:NBelectrons) = 1.0D0
          !
          if(.not.allocated(PM0_alpha))then
            allocate(PM0_alpha(MATlen),PM0_beta(MATlen))
          end if
          !
          ! build alpha density matrix
          call DENBLD(CMO%coeff,ona,PM0_alpha,Nbasis,MATlen,Nbasis)
          ! build beta density matrix
          call DENBLD(CMO%coeff,onb,PM0_beta,Nbasis,MATlen,Nbasis)

        case('ROHF')
          ! set-up occupancies
          NAelectrons = (Nelectrons + Multiplicity - 1)/2 ! why is this not calculated by default?
          NBelectrons = Nelectrons - NAelectrons
          allocate(ona(Nbasis),onb(Nbasis))
          ona = 0.0D0
          onb = 0.0D0
          ! closed-shell
          ona(1:Ncore) = 1.0D0
          onb(1:Ncore) = 1.0D0
          ! open-shell
          ona(Ncore+1:Ncore+Nopen) = dble(NAelectrons - Ncore)/dble(Nopen)
          onb(Ncore+1:Ncore+Nopen) = dble(NBelectrons - Ncore)/dble(Nopen)
          !
          if(.not.allocated(PM0_alpha))then
            allocate(PM0_alpha(MATlen),PM0_beta(MATlen))
          end if
          !
          ! build alpha density matrix
          call DENBLD(CMO%coeff,ona,PM0_alpha,Nbasis,MATlen,Nbasis)
          ! build beta density matrix
          call DENBLD(CMO%coeff,onb,PM0_beta,Nbasis,MATlen,Nbasis)

        case default
           write(UNIout,'(a,a)') 'Cannot make spin density for Wavefunction of type: ',Wavefunction
           stop

      end select
!
! Now get correlated density matrix too!
      allocate(PM0HF_alpha(MATlen),PM0HF_beta(MATlen))
      PM0HF_alpha = PM0_alpha
      PM0HF_beta = PM0_beta
!
      select case(correlation)
!
        case('NONE')
!
        case('MP2')
                ! not yet
!
        case('CCD','CCSD')
                ! not yet
!
        case('READ')
!
          write(UNIout,'(/a,1x,a)')'1-RDM input file name: ',ONERDMfile
          allocate(Pa(Nbasis,Nbasis),Pb(Nbasis,Nbasis))
          call read_1RDM(Pa,Pb)
!
! re-order rows
          allocate(Pa2(Nbasis,Nbasis),Pb2(Nbasis,Nbasis))
          call reorder_AO_for_MO(Pa,Pa2)
          call reorder_AO_for_MO(Pb,Pb2)
! re-order columns
          Pa = transpose(Pa2)
          Pb = transpose(Pb2)
          call reorder_AO_for_MO(Pa,Pa2)
          call reorder_AO_for_MO(Pb,Pb2)
          Pa = transpose(Pa2)
          Pb = transpose(Pb2)
!
          call SQS_TO_UPT(Pa, Nbasis, PM0_alpha, MATlen)
          call SQS_TO_UPT(Pb, Nbasis, PM0_beta, MATlen)
!
! Turn off core contribution if requested
        Ncore = 0
        if(.not.LRDMcore)then
          Ncore = NFrozen_cores()
          ! remove all core contributions
!
          write(6,'(/a)')'Removing contribution of core electrons to correlated 1-RDM'
          write(6,'(a,I3,a)')'(Number of core orbitals = ', Ncore,')'
!
          ! Need MOs to make core 
          allocate(inpMO(Nbasis,Nbasis))
          call read_MO(inpMO)
!
          ! make core P and subtract it
          allocate(Pcore(MATlen), oncore(Nbasis))
          oncore = 0.0D0
          oncore(1:Ncore) = 1.0D0
          !
          ! build core (alpha/beta) density matrix
          call DENBLD(inpMO,oncore,Pcore,Nbasis,MATlen,Nbasis)
!
          ! subtract from total to get the valence density matrix
          PM0_alpha = PM0_alpha - Pcore
          PM0_beta  = PM0_beta  - Pcore
!
        end if 
!
        case('DELTANO')
!
          call GET_object('CFT','2RDM','DELTANO')
!
        case default
           write(UNIout,'(a,a)') 'Cannot make 1-RDM for that correlation: ', correlation
           stop
!
      end select
!
! Test density matrices by calculating the one-electron energy
      call GET_object ('INT', '1EINT', 'AO')
!
      E1e_alpha = traclo(HCORE, PM0_alpha, Nbasis, MATlen)
      E1e_beta =  traclo(HCORE, PM0_beta, Nbasis, MATlen)
!
      Ne_alpha = traclo(OVRLAP, PM0_alpha, Nbasis, MATlen)
      Ne_beta =  traclo(OVRLAP, PM0_beta, Nbasis, MATlen)
!
      E1eHF_alpha = traclo(HCORE, PM0HF_alpha, Nbasis, MATlen)
      E1eHF_beta =  traclo(HCORE, PM0HF_beta, Nbasis, MATlen)
!
      NeHF_alpha = traclo(OVRLAP, PM0HF_alpha, Nbasis, MATlen)
      NeHF_beta =  traclo(OVRLAP, PM0HF_beta, Nbasis, MATlen)
!
! Calculate components also
      T_alpha = traclo(TINT, PM0_alpha, Nbasis, MATlen)
      T_beta =  traclo(TINT, PM0_beta, Nbasis, MATlen)
      Vne_alpha = traclo(VINT, PM0_alpha, Nbasis, MATlen)
      Vne_beta =  traclo(VINT, PM0_beta, Nbasis, MATlen)
!
      THF_alpha = traclo(TINT, PM0HF_alpha, Nbasis, MATlen)
      THF_beta =  traclo(TINT, PM0HF_beta, Nbasis, MATlen)
      VneHF_alpha = traclo(VINT, PM0HF_alpha, Nbasis, MATlen)
      VneHF_beta =  traclo(VINT, PM0HF_beta, Nbasis, MATlen)
!
      write(UNIout,'(/a)')' Calculate number of electrons to verify 1-RDMs '
!
      write(UNIout,'(/a,F18.10)')'sum of alpha electrons (HF): ',NeHF_alpha
      write(UNIout,'(a,F18.10)')' sum of beta electrons (HF): ',NeHF_beta
      write(UNIout,'(a,F18.10)')'     sum of alpha electrons: ',Ne_alpha
      write(UNIout,'(a,F18.10)')'      sum of beta electrons: ',Ne_beta
!
      write(UNIout,'(/a)')' Calculate one-electron energies to verify 1-RDMs '
!
      write(UNIout,'(/a,F18.10)')'      HF kinetic energy from alpha 1RDM contraction: ',THF_alpha
      write(UNIout,'(a,F18.10)')'       HF kinetic energy from beta 1RDM contraction: ',THF_beta
      write(UNIout,'(a,F18.10)')'HF n-e potential energy from alpha 1RDM contraction: ',VneHF_alpha
      write(UNIout,'(a,F18.10)')' HF n-e potential energy from beta 1RDM contraction: ',VneHF_beta
      write(UNIout,'(a,F18.10)')' HF one-electron energy from alpha 1RDM contraction: ',E1eHF_alpha
      write(UNIout,'(a,F18.10)')'  HF one-electron energy from beta 1RDM contraction: ',E1eHF_beta
      write(UNIout,'(a,F18.10)')'         kinetic energy from alpha 1RDM contraction: ',T_alpha
      write(UNIout,'(a,F18.10)')'          kinetic energy from beta 1RDM contraction: ',T_beta
      write(UNIout,'(a,F18.10)')'   n-e potential energy from alpha 1RDM contraction: ',Vne_alpha
      write(UNIout,'(a,F18.10)')'    n-e potential energy from beta 1RDM contraction: ',Vne_beta
      write(UNIout,'(a,F18.10)')'    one-electron energy from alpha 1RDM contraction: ',E1e_alpha
      write(UNIout,'(a,F18.10)')'     one-electron energy from beta 1RDM contraction: ',E1e_beta
      write(UNIout,'(a,F18.10)')'    total one-electron energy from 1RDM contraction: ',E1e_alpha+E1e_beta
!
! End of routine BLD_spin_density_matrix
      call PRG_manager ('exit', 'BLD_spin_density_matrix', 'UTILITY')
      return
      end subroutine BLD_spin_density_matrix
      subroutine BLD_2RDM_AO_corr
!***********************************************************************
!     Date last modified: April 10, 2019                               *
!     Author: J.W. Hollett                                             *
!     Description: Build a correlated 2-RDM.                           *
!***********************************************************************
! Modules:
      USE program_files
      USE QM_objects
      USE QM_defaults
      USE type_basis_set
      USE INT_objects

      implicit none
!
! Local scalars:
      integer :: iAO, jAO, kAO, lAO, pMO, qMO, rMO, sMO
      integer :: ij_index, ijk_index, ijkl_index, ijks_index, ijqs_index, iqrs_index, pqrs_index
      integer :: qrs_index, aabb_index, abab_index, abba_index, aMO, bMO
      double precision :: Cip, Cjr, Ckq, Cls
!
! Local arrays:
      double precision, allocatable, dimension (:,:) :: inpMO
!
! Functions:
     integer :: TEIindex
!
! Begin:
      call PRG_manager ('enter', 'BLD_2RDM_AO_corr', 'UTILITY')
!
      allocate(TRDM_MOaa(Nbasis**4),TRDM_MObb(Nbasis**4))
      allocate(TRDM_MOab(Nbasis**4),TRDM_MOba(Nbasis**4))
      TRDM_MOaa = 0.0D0
      TRDM_MObb = 0.0D0
      TRDM_MOab = 0.0D0
      TRDM_MOba = 0.0D0
!
! 2RDMs notation
! physicist notation for MO basis <12|12>
!   chemist notation for AO basis [11|22]
!
      select case(correlation)
!
!        case('MP2')
!          call GET_object('CFT','ENERGY','MP2')
!
          ! need to solve some response equations
!
!        case('CCSD','CCD')
!          call GET_object('CFT','ENERGY','CCSD')
          ! need to solve some response equations
!
        case('READ')
!
          write(UNIout,'(/a,1x,a)')'2-RDM input file name: ',TWORDMfile
          call read_2RDM
          ! If using an external 2-RDM, going to need the MOs too
          allocate(inpMO(Nbasis,Nbasis))
          call read_MO(inpMO)
!
        case('DELTANO')
!
          call GET_object('CFT','NO','DELTANO')
!
          call CFT_BLD_2RDM
!
          do aMO = 1, Nbasis
            do bMO = 1, Nbasis
              abab_index = TEIindex(Nbasis,aMO,bMO,aMO,bMO)
              TRDM_MOaa(abab_index) = CFT2RDMaa(aMO,bMO,aMO,bMO)
              TRDM_MObb(abab_index) = CFT2RDMbb(aMO,bMO,aMO,bMO)
              TRDM_MOab(abab_index) = CFT2RDMab(aMO,bMO,aMO,bMO)
              TRDM_MOba(abab_index) = CFT2RDMba(aMO,bMO,aMO,bMO)
              abba_index = TEIindex(Nbasis,aMO,bMO,bMO,aMO)
              TRDM_MOaa(abba_index) = CFT2RDMaa(aMO,bMO,bMO,aMO)
              TRDM_MObb(abba_index) = CFT2RDMbb(aMO,bMO,bMO,aMO)
              TRDM_MOab(abba_index) = CFT2RDMab(aMO,bMO,bMO,aMO)
              TRDM_MOba(abba_index) = CFT2RDMba(aMO,bMO,bMO,aMO)
              aabb_index = TEIindex(Nbasis,aMO,aMO,bMO,bMO)
              TRDM_MOab(aabb_index) = CFT2RDMab(aMO,aMO,bMO,bMO)
              TRDM_MOba(aabb_index) = CFT2RDMba(aMO,aMO,bMO,bMO)
            end do ! bMO
          end do ! aMO
!
          allocate(inpMO(Nbasis,Nbasis))
          inpMO = CMO%coeff
!
          deallocate(CFT2RDMaa,CFT2RDMbb,CFT2RDMab,CFT2RDMba)
!
        case('NONE')
!
        ! leave it as zero
!
        case default
          write(UNIout,'(a,1x,a)')'Cannot determine correlated 2-RDM for :',correlation
          inpMO = 0.0D0
!
        end select
!
! Now back-transform the 2-RDM over MOs to AO
!
        if(allocated(TRDM_AOaa))then
          deallocate(TRDM_AOaa) 
        end if
        allocate(TRDM_AOaa(Nbasis**4))
        if(allocated(TRDM_AObb))then
          deallocate(TRDM_AObb) 
        end if
        allocate(TRDM_AObb(Nbasis**4))
        if(allocated(TRDM_AOab))then
          deallocate(TRDM_AOab) 
        end if
        allocate(TRDM_AOab(Nbasis**4))
        if(allocated(TRDM_AOba))then
          deallocate(TRDM_AOba) 
        end if
        allocate(TRDM_AOba(Nbasis**4))
!
        if(correlation.ne.'NONE')then
!
! Turn off core contribution if requested
        Ncore = 0
        if(.not.LRDMcore)then
          call remove_core_2RDM
        end if !
!
! First step <pq|rs> -> <iqrs>
        TRDM_AOaa = 0.0D0
        TRDM_AObb = 0.0D0
        TRDM_AOab = 0.0D0
        TRDM_AOba = 0.0D0
        pqrs_index = 0
        do pMO = 1, Nbasis
          qrs_index = 0
          do qMO = 1, Nbasis
            do rMO = 1, Nbasis
              do sMO = 1, Nbasis
                pqrs_index = pqrs_index + 1
                qrs_index = qrs_index + 1
                do iAO = 1, Nbasis
                  iqrs_index = Nbasis**3*(iAO-1) + qrs_index
                  Cip = inpMO(iAO,pMO)
!
                  TRDM_AOaa(iqrs_index) = TRDM_AOaa(iqrs_index)&
                                             + Cip*TRDM_MOaa(pqrs_index)
                  TRDM_AObb(iqrs_index) = TRDM_AObb(iqrs_index)&
                                             + Cip*TRDM_MObb(pqrs_index)
                  TRDM_AOab(iqrs_index) = TRDM_AOab(iqrs_index)&
                                             + Cip*TRDM_MOab(pqrs_index)
                  TRDM_AOba(iqrs_index) = TRDM_AOba(iqrs_index)&
                                             + Cip*TRDM_MOba(pqrs_index)
!
                end do ! iAO
              end do ! pMO
            end do ! qMO
          end do ! rMO
        end do ! sMO
!
! Second step <iq|rs> -> [ij|qs] (change from physicist to chemist notation)
        TRDM_MOaa = 0.0D0
        TRDM_MObb = 0.0D0
        TRDM_MOab = 0.0D0
        TRDM_MOba = 0.0D0
        iqrs_index = 0
        do iAO = 1, Nbasis
          do qMO = 1, Nbasis
            do rMO = 1, Nbasis
              do sMO = 1, Nbasis
                iqrs_index = iqrs_index + 1
                do jAO = 1, Nbasis
                  ijqs_index = Nbasis**3*(iAO-1) + Nbasis**2*(jAO-1) + Nbasis*(qMO-1) + sMO
                  Cjr = inpMO(jAO,rMO)
!
                  TRDM_MOaa(ijqs_index) = TRDM_MOaa(ijqs_index)&
                                             + Cjr*TRDM_AOaa(iqrs_index)
                  TRDM_MObb(ijqs_index) = TRDM_MObb(ijqs_index)&
                                             + Cjr*TRDM_AObb(iqrs_index)
                  TRDM_MOab(ijqs_index) = TRDM_MOab(ijqs_index)&
                                             + Cjr*TRDM_AOab(iqrs_index)
                  TRDM_MOba(ijqs_index) = TRDM_MOba(ijqs_index)&
                                             + Cjr*TRDM_AOba(iqrs_index)
!
                end do ! iAO
              end do ! jAO
            end do ! qMO
          end do ! rMO
        end do ! sMO
!
! Third step [ij|qs] -> [ij|ks]
        TRDM_AOaa = 0.0D0
        TRDM_AObb = 0.0D0
        TRDM_AOab = 0.0D0
        TRDM_AOba = 0.0D0
        ijqs_index = 0
        ij_index = 0
        do iAO = 1, Nbasis
          do jAO = 1, Nbasis
            ij_index = ij_index + 1
            do qMO = 1,Nbasis  
              do sMO = 1, Nbasis
                ijqs_index = ijqs_index + 1
                do kAO = 1, Nbasis
                  ijks_index = Nbasis**2*(ij_index-1) + Nbasis*(kAO-1) + sMO
                  Ckq = inpMO(kAO,qMO)
!
                  TRDM_AOaa(ijks_index) = TRDM_AOaa(ijks_index)&
                                             + Ckq*TRDM_MOaa(ijqs_index)
                  TRDM_AObb(ijks_index) = TRDM_AObb(ijks_index)&
                                             + Ckq*TRDM_MObb(ijqs_index)
                  TRDM_AOab(ijks_index) = TRDM_AOab(ijks_index)&
                                             + Ckq*TRDM_MOab(ijqs_index)
                  TRDM_AOba(ijks_index) = TRDM_AOba(ijks_index)&
                                             + Ckq*TRDM_MOba(ijqs_index)
!
                end do ! iAO
              end do ! jAO
            end do ! qMO
          end do ! rMO
        end do ! sMO
!
! Fourth step [ij|ks] -> [ij|kl]
        TRDM_MOaa = 0.0D0
        TRDM_MObb = 0.0D0
        TRDM_MOab = 0.0D0
        TRDM_MOba = 0.0D0
        ijks_index = 0
        ijk_index = 0
        do iAO = 1, Nbasis
          do jAO = 1, Nbasis
            do kAO = 1,Nbasis  
              ijk_index = ijk_index + 1
              do sMO = 1, Nbasis
                ijks_index = ijks_index + 1
                do lAO = 1, Nbasis
                  ijkl_index = Nbasis*(ijk_index-1) + lAO
                  Cls = inpMO(lAO,sMO)
!
                  TRDM_MOaa(ijkl_index) = TRDM_MOaa(ijkl_index)&
                                             + Cls*TRDM_AOaa(ijks_index)
                  TRDM_MObb(ijkl_index) = TRDM_MObb(ijkl_index)&
                                             + Cls*TRDM_AObb(ijks_index)
                  TRDM_MOab(ijkl_index) = TRDM_MOab(ijkl_index)&
                                             + Cls*TRDM_AOab(ijks_index)
                  TRDM_MOba(ijkl_index) = TRDM_MOba(ijkl_index)&
                                             + Cls*TRDM_AOba(ijks_index)
!
                end do ! iAO
              end do ! jAO
            end do ! qMO
          end do ! rMO
        end do ! sMO
!
        TRDM_AOaa = TRDM_MOaa
        TRDM_AObb = TRDM_MObb
        TRDM_AOab = TRDM_MOab
        TRDM_AOba = TRDM_MOba
        deallocate(TRDM_MOaa)
        deallocate(TRDM_MObb)
        deallocate(TRDM_MOab)
        deallocate(TRDM_MOba)
!
        else ! no correlation
          TRDM_AOaa = TRDM_HF_AOaa
          TRDM_AObb = TRDM_HF_AObb
          TRDM_AOab = TRDM_HF_AOab
          TRDM_AOba = TRDM_HF_AOba
        end if
!
! End of routine BLD_2RDM_AO
      call PRG_manager ('exit', 'BLD_2RDM_AO_corr', 'UTILITY')
      return
      end subroutine BLD_2RDM_AO_corr
      subroutine read_1RDM(Pa,Pb)
!***********************************************************************
!     Date last modified: May 16, 2019                                 *
!     Author: J.W. Hollett                                             *
!     Description: Read the 1-RDM over AOs from an external            *
!                  calculation.                                        *
!***********************************************************************
! Modules:
      USE program_files
      USE QM_objects
!
      implicit none
!
! Local scalars:
      double precision :: ordm_val
      integer :: pAO, qAO, Iline
!
! Output arrays:
      double precision, intent(OUT) :: Pa(Nbasis,Nbasis)
      double precision, intent(OUT) :: Pb(Nbasis,Nbasis)

! Begin:
      call PRG_manager ('enter', 'read_1RDM', 'UTILITY')
!
! Read 1-RDM from data file
!
      Pa = 0.0D0
      Pb = 0.0D0
      open(10,FILE=onerdmfile)
!
! Read alpha 
      do Iline = 1, Nbasis**2
        read(10,*) pAO, qAO, ordm_val
        Pa(pAO,qAO) = ordm_val
      end do
!
! Read beta 
      do Iline = 1, Nbasis**2
        read(10,*) pAO, qAO, ordm_val
        Pb(pAO,qAO) = ordm_val
      end do
!
      close(10)
!
! End of routine read_1RDM
      call PRG_manager ('exit', 'read_1RDM', 'UTILITY')
      return
      end subroutine read_1RDM
      subroutine read_2RDM
!***********************************************************************
!     Date last modified: April 9, 2019                                *
!     Author: J.W. Hollett                                             *
!     Description: Read the 2-RDM over MOs from an external            *
!                  calculation.                                        *
!***********************************************************************
! Modules:
      USE program_files
      USE QM_objects
!
      implicit none
!
! Local scalars:
      double precision :: trdm_val
      integer :: pMO, qMO, rMO, sMO, Iline, NMO, pqrs_index
!
! Local functions:
      integer :: TEIindex ! needed for custom number of orbitals from qp2
!
! Begin:
      call PRG_manager ('enter', 'read_2RDM', 'UTILITY')
!
      if(allocated(TRDM_MOaa))then 
        deallocate(TRDM_MOaa,TRDM_MObb,TRDM_MOab,TRDM_MOba)
      end if
      allocate(TRDM_MOaa(Nbasis**4),TRDM_MObb(Nbasis**4))
      allocate(TRDM_MOab(Nbasis**4),TRDM_MOba(Nbasis**4))
!
! Read 2-RDM from data file
!
      TRDM_MOaa = 0.0D0
      TRDM_MObb = 0.0D0
      TRDM_MOab = 0.0D0
      TRDM_MOba = 0.0D0
      open(10,FILE=twordmfile)
!
! Get number of MOs
      read(10,*) NMO
      write(6,'(a,I4,a)')'Reading 2RDM from qp2 with ', NMO,' MOs.'

! Read alpha-alpha 
      do Iline = 1, NMO**4
        read(10,*) pMO, qMO, rMO, sMO, trdm_val
        !TRDM_MOaa(Iline) = trdm_val
        pqrs_index = TEIindex(Nbasis,pMO,qMO,rMO,sMO)
        TRDM_MOaa(pqrs_index) = trdm_val
      end do
! Read beta-beta 
      do Iline = 1, NMO**4
        read(10,*) pMO, qMO, rMO, sMO, trdm_val
        !TRDM_MObb(Iline) = trdm_val
        pqrs_index = TEIindex(Nbasis,pMO,qMO,rMO,sMO)
        TRDM_MObb(pqrs_index) = trdm_val
      end do
! Read alpha-beta 
      do Iline = 1, NMO**4
        read(10,*) pMO, qMO, rMO, sMO, trdm_val
        !TRDM_MOab(Iline) = trdm_val
        pqrs_index = TEIindex(Nbasis,pMO,qMO,rMO,sMO)
        TRDM_MOab(pqrs_index) = trdm_val
      end do
! Read beta-alpha 
      do Iline = 1, NMO**4
        read(10,*) pMO, qMO, rMO, sMO, trdm_val
        !TRDM_MOba(Iline) = trdm_val
        pqrs_index = TEIindex(Nbasis,pMO,qMO,rMO,sMO)
        TRDM_MOba(pqrs_index) = trdm_val
      end do
!
      close(10)
!
! End of routine read_2RDM
      call PRG_manager ('exit', 'read_2RDM', 'UTILITY')
      return
      end subroutine read_2RDM
      subroutine read_MO(coeff2)
!***********************************************************************
!     Date last modified: April 23, 2019                               *
!     Author: J.W. Hollett                                             *
!     Description: Read the MOs from an external calculation.          *
!***********************************************************************
! Modules:
      USE program_files
      USE QM_objects
!
      implicit none
!
! Output arrays:
!
      double precision :: coeff2(Nbasis,Nbasis)
!
! Local scalars:
      double precision, allocatable, dimension(:,:) :: coeff1
      integer :: aMO, iAO, NMO
!
! Begin:
      call PRG_manager ('enter', 'read_MO', 'UTILITY')
!
! Read MO from data file
!
      open(10,FILE=mofile)
      read(10,*) NMO
!
      write(6,'(/a,a)')' Reading MO coefficients from: ',mofile
      write(6,'(a,I4)')' Number of MOs read: ',NMO
      allocate(coeff1(Nbasis,Nbasis))
      coeff1 = 0.0D0
!
      do aMO = 1, NMO
        do iAO = 1, Nbasis
          read(10,'(G24.16)') coeff1(iAO,aMO)
!          write(6,'(2I4,F16.10)')aMO,iAO,coeff1(iAO,aMO)
        end do
      end do
!
      close(10)
!
      call reorder_AO_for_MO(coeff1,coeff2)
!
!      do aMO = 1, Nbasis
!        do iAO = 1, Nbasis
!          write(6,'(2I4,F16.10)')aMO,iAO,coeff2(iAO,aMO)
!        end do
!      end do
!
! End of routine read_MO
      call PRG_manager ('exit', 'read_MO', 'UTILITY')
      return
      end subroutine read_MO
      subroutine reorder_AO_for_MO(coeff1,coeff2)
!***********************************************************************
!     Date last modified: April 23, 2019                               *
!     Author: J.W. Hollett                                             *
!     Description: Read the MOs from an external calculation.          *
!***********************************************************************
! Modules:
      USE program_files
      USE type_basis_set
      USE type_molecule
      USE QM_objects
!
      implicit none
!
! Input arrays:
      double precision, intent(IN) :: coeff1(Nbasis,Nbasis) 
!
! Output arrays:
      double precision, intent(OUT) :: coeff2(Nbasis,Nbasis) 
!
! Local scalars:
      integer :: I_shell, Iatom, I_atmshl, iAO, iAOG
! 
! Local arrays:
      integer, dimension(:), allocatable :: s_atmshl, p_atmshl, d_atmshl
!
! Begin:
      call PRG_manager ('enter', 'reorder_AO_for_MO', 'UTILITY')
!
!      call BLD_basis_list
      call BLD_GAMESSbasis
!
      allocate(d_atmshl(Natoms), p_atmshl(Natoms), s_atmshl(Natoms))
      s_atmshl = 0
      p_atmshl = 0
      d_atmshl = 0
      coeff2 = 0.0D0 
!
! Cycle through MUNgauss basis set and grab entries from GAMESS format as needed
!
      do I_shell = 1, Basis%Nshells
!
        do I_atmshl = Basis%shell(I_shell)%frstshl,Basis%shell(I_shell)%lastshl
          Iatom = Basis%atmshl(I_atmshl)%atmlst
!
          ! check and increment that shell type on that atom
          if(Basis%shell(I_shell)%Xtype.eq.0)then
            s_atmshl(Iatom) = s_atmshl(Iatom) + 1
            ! identify first AO in GAMESS ordering
            iAOG = frstAOatom(Iatom) + s_atmshl(Iatom) - 1
          end if
          if(Basis%shell(I_shell)%Xtype.eq.1)then
            p_atmshl(Iatom) = p_atmshl(Iatom) + 1
            ! identify first AO in GAMESS ordering
            iAOG = frstAOatom(Iatom) + Ns_atom(Iatom) + 3*(p_atmshl(Iatom) - 1)
          end if
          if(Basis%shell(I_shell)%Xtype.eq.2)then
            d_atmshl(Iatom) = d_atmshl(Iatom) + 1
            ! identify first AO in GAMESS ordering
            iAOG = frstAOatom(Iatom) + Ns_atom(Iatom) + 3*Np_atom(Iatom)&
                                     + 6*(d_atmshl(Iatom) - 1)
          end if
          ! save to coeff array
          if(Basis%shell(I_shell)%Xtype.ne.2)then ! not a d shell
            do iAO = Basis%atmshl(I_atmshl)%frstAO,Basis%atmshl(I_atmshl)%lastAO
              coeff2(iAO,1:Nbasis) = coeff1(iAOG,1:Nbasis) 
              iAOG = iAOG + 1
            end do ! iAO
          else ! a d shell
            ! quantum package uses lexical ordering for d's
            iAO = Basis%atmshl(I_atmshl)%frstAO
            coeff2(iAO,1:Nbasis) = coeff1(iAOG,1:Nbasis) 
            coeff2(iAO+1,1:Nbasis) = coeff1(iAOG+3,1:Nbasis) 
            coeff2(iAO+2,1:Nbasis) = coeff1(iAOG+5,1:Nbasis) 
            coeff2(iAO+3,1:Nbasis) = coeff1(iAOG+1,1:Nbasis) 
            coeff2(iAO+4,1:Nbasis) = coeff1(iAOG+2,1:Nbasis) 
            coeff2(iAO+5,1:Nbasis) = coeff1(iAOG+4,1:Nbasis) 
!
             ! GAMESS doesn't            
            !do iAO = Basis%atmshl(I_atmshl)%frstAO,Basis%atmshl(I_atmshl)%lastAO
            !  coeff2(iAOG,1:Nbasis) = coeff1(iAO,1:Nbasis) 
            !  iAOG = iAOG + 1
            !end do ! iAO
!
          end if     
!
        end do ! I_atmshl
!
      end do  ! I_shell
!
! Coefficients reordered
!
! End of routine reorder_AO_for_MO
      call PRG_manager ('exit', 'reorder_AO_for_MO', 'UTILITY')
      return
      end subroutine reorder_AO_for_MO
      subroutine reorder_AO_for_MO_reverse(coeff1,coeff2)
!***********************************************************************
!     Date last modified: October 9, 2020                              *
!     Author: J.W. Hollett                                             *
!     Description: Reorder MUNgauss coefficients to GAMESS order.      *
!***********************************************************************
! Modules:
      USE program_files
      USE type_basis_set
      USE type_molecule
      USE QM_objects
!
      implicit none
!
! Input arrays:
      double precision, intent(IN) :: coeff1(Nbasis,Nbasis) 
!
! Output arrays:
      double precision, intent(OUT) :: coeff2(Nbasis,Nbasis) 
!
! Local scalars:
      integer :: I_shell, Iatom, I_atmshl, iAO, iAOG
! 
! Local arrays:
      integer, dimension(:), allocatable :: s_atmshl, p_atmshl, d_atmshl
!
! Begin:
      call PRG_manager ('enter', 'reorder_AO_for_MO', 'UTILITY')
!
!      call BLD_basis_list
      call BLD_GAMESSbasis
!
      allocate(d_atmshl(Natoms), p_atmshl(Natoms), s_atmshl(Natoms))
      s_atmshl = 0
      p_atmshl = 0
      d_atmshl = 0
      coeff2 = 0.0D0 
!
! Cycle through MUNgauss basis set and grab entries from GAMESS format as needed
!
      do I_shell = 1, Basis%Nshells
!
        do I_atmshl = Basis%shell(I_shell)%frstshl,Basis%shell(I_shell)%lastshl
          Iatom = Basis%atmshl(I_atmshl)%atmlst
!
          ! check and increment that shell type on that atom
          if(Basis%shell(I_shell)%Xtype.eq.0)then
            s_atmshl(Iatom) = s_atmshl(Iatom) + 1
            ! identify first AO in GAMESS ordering
            iAOG = frstAOatom(Iatom) + s_atmshl(Iatom) - 1
          end if
          if(Basis%shell(I_shell)%Xtype.eq.1)then
            p_atmshl(Iatom) = p_atmshl(Iatom) + 1
            ! identify first AO in GAMESS ordering
            iAOG = frstAOatom(Iatom) + Ns_atom(Iatom) + 3*(p_atmshl(Iatom) - 1)
          end if
          if(Basis%shell(I_shell)%Xtype.eq.2)then
            d_atmshl(Iatom) = d_atmshl(Iatom) + 1
            ! identify first AO in GAMESS ordering
            iAOG = frstAOatom(Iatom) + Ns_atom(Iatom) + 3*Np_atom(Iatom)&
                                     + 6*(d_atmshl(Iatom) - 1)
          end if
          ! save to coeff array
          if(Basis%shell(I_shell)%Xtype.ne.2)then ! not a d shell
            do iAO = Basis%atmshl(I_atmshl)%frstAO,Basis%atmshl(I_atmshl)%lastAO
              coeff2(iAOG,1:Nbasis) = coeff1(iAO,1:Nbasis) 
              iAOG = iAOG + 1
            end do ! iAO
          else ! a d shell
            ! quantum package uses lexical ordering for d's
            !iAO = Basis%atmshl(I_atmshl)%frstAO
            !coeff2(iAOG,1:Nbasis)   = coeff1(iAO,1:Nbasis) 
            !coeff2(iAOG+1,1:Nbasis) = coeff1(iAO+3,1:Nbasis) 
            !coeff2(iAOG+2,1:Nbasis) = coeff1(iAO+5,1:Nbasis) 
            !coeff2(iAOG+3,1:Nbasis) = coeff1(iAO+1,1:Nbasis) 
            !coeff2(iAOG+4,1:Nbasis) = coeff1(iAO+2,1:Nbasis) 
            !coeff2(iAOG+5,1:Nbasis) = coeff1(iAO+4,1:Nbasis) 
            ! but GAMESS doesn't
            do iAO = Basis%atmshl(I_atmshl)%frstAO,Basis%atmshl(I_atmshl)%lastAO
              coeff2(iAOG,1:Nbasis) = coeff1(iAO,1:Nbasis) 
              iAOG = iAOG + 1
            end do ! iAO
          end if     
!
        end do ! I_atmshl
!
      end do  ! I_shell
!
! Coefficients reordered
!
! End of routine reorder_AO_for_MO_reverse
      call PRG_manager ('exit', 'reorder_AO_for_MO_reverse', 'UTILITY')
      return
      end subroutine reorder_AO_for_MO_reverse
      subroutine BLD_2RDM_MO
!***********************************************************************
!     Date last modified: May 25, 2021                                 *
!     Author: J.W. Hollett                                             *
!     Description: Build the two-electron density matrix (2-RDM) for   *
!                  the evaulation of two-electron properties.          *
!***********************************************************************
! Modules:
      USE program_files
      USE QM_objects
      USE INT_objects

      implicit none
!
! Local scalars:
      double precision :: VeeHFaa, Veecorraa, Veeaa
      double precision :: VeeHFbb, Veecorrbb, Veebb
      double precision :: VeeHFab, Veecorrab, Veeab
      double precision :: VeeHFba, Veecorrba, Veeba
      integer :: aMO
!
! Locay arrays:
      double precision, allocatable, dimension(:) :: eriMO
      double precision, allocatable, dimension(:) :: TRDM_corr_MOaa, TRDM_corr_MObb
      double precision, allocatable, dimension(:) :: TRDM_corr_MOab, TRDM_corr_MOba
      integer, allocatable, dimension(:) :: MO_act
!
! Begin:
      call PRG_manager ('enter', 'BLD_2RDM_MO', '2RDM%MO')
!
! Build HF 2RDM first
      call BLD_2RDM_MO_HF
!
! Build correlated 2RDM
      call BLD_2RDM_MO_corr
!
      allocate(TRDM_corr_MOaa(Nbasis**4), TRDM_corr_MObb(Nbasis**4))
      allocate(TRDM_corr_MOab(Nbasis**4), TRDM_corr_MOba(Nbasis**4))
!
      TRDM_corr_MOaa = TRDM_MOaa - TRDM_HF_MOaa
      TRDM_corr_MObb = TRDM_MObb - TRDM_HF_MObb
      TRDM_corr_MOab = TRDM_MOab - TRDM_HF_MOab
      TRDM_corr_MOba = TRDM_MOba - TRDM_HF_MOba
!
! Test it by computing the two-electron energy
      call GET_object ('CFT','2EINT','OS')
!
! Transform the two-electron integrals to the MO basis
      if(allocated(eriMO))then
        deallocate(eriMO)
      end if
      allocate(eriMO(Nbasis**4))
      if(allocated(MO_act))then
        deallocate(MO_act)
      end if
      allocate(MO_act(Nbasis))
      do aMO = 1, Nbasis
        MO_act(aMO) = aMO
      end do ! aMO
!
      call CFT_gen_TEItrans(MO_act,Nbasis,MO_act,Nbasis,MO_act,Nbasis,MO_act,Nbasis,&
                             CMO%coeff,Nbasis,eriAO,eriMO)
!
      write(UNIout,'(/a)')' Calculate two-electron energies to verify 2-RDMs '
      write(UNIout,'(a)')' (Using 2-RDMs in MO basis) '
!
      VeeHFaa = dot_product(TRDM_HF_MOaa,eriMO)
      write(UNIout,'(/a,F14.10)')'         HF two-electron energy from alpha-alpha 2RDM contraction: ',VeeHFaa
!
      Veecorraa = dot_product(TRDM_corr_MOaa,eriMO)
      write(UNIout,'(a,F14.10)') 'Two-electron correlation energy from alpha-alpha 2RDM contraction: ',Veecorraa
!
      Veeaa = dot_product(TRDM_MOaa,eriMO)
      write(UNIout,'(a,F14.10)') '      Total two-electron energy from alpha-alpha 2RDM contraction: ',Veeaa
!
      VeeHFbb = dot_product(TRDM_HF_MObb,eriMO)
      write(UNIout,'(/a,F14.10)')'         HF two-electron energy from beta-beta 2RDM contraction: ',VeeHFbb
!
      Veecorrbb = dot_product(TRDM_corr_MObb,eriMO)
      write(UNIout,'(a,F14.10)') 'Two-electron correlation energy from beta-beta 2RDM contraction: ',Veecorrbb
!
      Veebb = dot_product(TRDM_MObb,eriMO)
      write(UNIout,'(a,F14.10)') '      Total two-electron energy from beta-beta 2RDM contraction: ',Veebb
!
      VeeHFab = dot_product(TRDM_HF_MOab,eriMO)
      write(UNIout,'(/a,F14.10)')'         HF two-electron energy from alpha-beta 2RDM contraction: ',VeeHFab
!
      Veecorrab = dot_product(TRDM_corr_MOab,eriMO)
      write(UNIout,'(a,F14.10)') 'Two-electron correlation energy from alpha-beta 2RDM contraction: ',Veecorrab
!
      Veeab = dot_product(TRDM_MOab,eriMO)
      write(UNIout,'(a,F14.10)') '      Total two-electron energy from alpha-beta 2RDM contraction: ',Veeab
!
      VeeHFba = dot_product(TRDM_HF_MOba,eriMO)
      write(UNIout,'(/a,F14.10)')'         HF two-electron energy from beta-alpha 2RDM contraction: ',VeeHFba
!
      Veecorrba = dot_product(TRDM_corr_MOba,eriMO)
      write(UNIout,'(a,F14.10)') 'Two-electron correlation energy from beta-alpha 2RDM contraction: ',Veecorrba
!
      Veeba = dot_product(TRDM_MOba,eriMO)
      write(UNIout,'(a,F14.10)') '      Total two-electron energy from beta-alpha 2RDM contraction: ',Veeba
!
      write(UNIout,'(/a,F14.10)')'         HF two-electron energy from total 2RDM contraction: ',&
      VeeHFaa+VeeHFbb+VeeHFab+VeeHFba
!
      write(UNIout,'(a,F14.10)') 'Two-electron correlation energy from total 2RDM contraction: ',&
      Veecorraa+Veecorrbb+Veecorrab+Veecorrba
!
      write(UNIout,'(a,F14.10)')'      Total two-electron energy from total 2RDM contraction: ',&
      Veeaa+Veebb+Veeab+Veeba
!
      deallocate(TRDM_corr_MOaa,TRDM_corr_MObb)
      deallocate(TRDM_corr_MOab,TRDM_corr_MOba)
!
! End of routine BLD_2RDM_MO
      call PRG_manager ('exit', 'BLD_2RDM_MO', '2RDM%MO')
      return
      end subroutine BLD_2RDM_MO
      subroutine BLD_2RDM_MO_HF
!***********************************************************************
!     Date last modified: April 9, 2019                                *
!     Author: J.W. Hollett                                             *
!     Description: Build the HF two-electron density matrix (2-RDM)    *
!***********************************************************************
! Modules:
      USE program_files
      USE QM_objects
      USE QM_defaults
      USE type_basis_set
      USE type_molecule

      implicit none
!
! Local scalars:
      integer :: aMO, bMO, abab_index, baba_index, abba_index, baab_index
!
! Local functions:
      integer :: TEIindex
!
! Begin:
      call PRG_manager ('enter', 'BLD_2RDM_MO_HF', 'UTILITY')
!
! 2RDMs in physicist notation <12|12>
!
      if(allocated(TRDM_HF_MOaa))then
        deallocate(TRDM_HF_MOaa)
      end if
      if(allocated(TRDM_HF_MObb))then
        deallocate(TRDM_HF_MObb)
      end if
      if(allocated(TRDM_HF_MOab))then
        deallocate(TRDM_HF_MOab)
      end if
      if(allocated(TRDM_HF_MOba))then
        deallocate(TRDM_HF_MOba)
      end if
      allocate(TRDM_HF_MOaa(Nbasis**4))
      allocate(TRDM_HF_MObb(Nbasis**4))
      allocate(TRDM_HF_MOab(Nbasis**4))
      allocate(TRDM_HF_MOba(Nbasis**4))
!
      TRDM_HF_MOaa = 0.0D0
      TRDM_HF_MObb = 0.0D0
      TRDM_HF_MOab = 0.0D0
      TRDM_HF_MOba = 0.0D0
!
! This assumes no degeneracy in the ROHF case
! (Will still work for open shell, just not symmetric)
!
! Determine number of alpha and beta electrons
!
      NAelectrons = (Nelectrons + Multiplicity - 1)/2
      NBelectrons = Nelectrons - NAelectrons
!
!
      do aMO = 1, NAelectrons
        do bMO = 1, NAelectrons
          if(aMO.eq.bMO)cycle
          ! Coulomb
          abab_index = TEIindex(Nbasis,aMO,bMO,aMO,bMO)
          baba_index = TEIindex(Nbasis,bMO,aMO,bMO,aMO)
          TRDM_HF_MOaa(abab_index) = 0.5D0
          TRDM_HF_MOaa(baba_index) = 0.5D0
          ! Exchange
          abba_index = TEIindex(Nbasis,aMO,bMO,bMO,aMO)
          baab_index = TEIindex(Nbasis,bMO,aMO,aMO,bMO)
          TRDM_HF_MOaa(abba_index) = -0.5D0
          TRDM_HF_MOaa(baab_index) = -0.5D0
        end do ! bMO 
      end do ! aMO 
!
      do aMO = 1, NBelectrons
        do bMO = 1, NBelectrons
          if(aMO.eq.bMO)cycle
          ! Coulomb
          abab_index = TEIindex(Nbasis,aMO,bMO,aMO,bMO)
          baba_index = TEIindex(Nbasis,bMO,aMO,bMO,aMO)
          TRDM_HF_MObb(abab_index) = 0.5D0
          TRDM_HF_MObb(baba_index) = 0.5D0
          ! Exchange
          abba_index = TEIindex(Nbasis,aMO,bMO,bMO,aMO)
          baab_index = TEIindex(Nbasis,bMO,aMO,aMO,bMO)
          TRDM_HF_MObb(abba_index) = -0.5D0
          TRDM_HF_MObb(baab_index) = -0.5D0
        end do ! bMO 
      end do ! aMO 
!
      do aMO = 1, NAelectrons
        do bMO = 1, NBelectrons
          ! Coulomb
          abab_index = TEIindex(Nbasis,aMO,bMO,aMO,bMO)
          baba_index = TEIindex(Nbasis,bMO,aMO,bMO,aMO)
          TRDM_HF_MOab(abab_index) = 0.5D0
          TRDM_HF_MOba(baba_index) = 0.5D0
          !
        end do ! bMO 
      end do ! aMO 
!
! End of routine BLD_2RDM_MO_HF
      call PRG_manager ('exit', 'BLD_2RDM_MO_HF', 'UTILITY')
      return
      end subroutine BLD_2RDM_MO_HF
      subroutine BLD_2RDM_MO_corr
!***********************************************************************
!     Date last modified: May 25, 2021                                 *
!     Author: J.W. Hollett                                             *
!     Description: Build a correlated 2-RDM.                           *
!***********************************************************************
! Modules:
      USE program_files
      USE QM_objects
      USE QM_defaults
      USE type_basis_set
      USE INT_objects

      implicit none
!
! Local arrays:
      double precision, allocatable, dimension (:,:) :: inpMO
!
! Local scalars:
      integer :: aMO, bMO, abab_index, abba_index, aabb_index
      integer :: pMO, qMO, rMO, sMO, pqrs_index
!
! Local functions:
      integer :: TEIindex
!
! Begin:
      call PRG_manager ('enter', 'BLD_2RDM_MO_corr', 'UTILITY')
!
      allocate(TRDM_MOaa(Nbasis**4),TRDM_MObb(Nbasis**4))
      allocate(TRDM_MOab(Nbasis**4),TRDM_MOba(Nbasis**4))
      TRDM_MOaa = 0.0D0
      TRDM_MObb = 0.0D0
      TRDM_MOab = 0.0D0
      TRDM_MOba = 0.0D0
!
! 2RDMs notation
! physicist notation for MO basis <12|12>
!   chemist notation for AO basis [11|22]
!
      select case(correlation)
!
!        case('MP2')
!          call GET_object('CFT','ENERGY','MP2')
!
          ! need to solve some response equations
!
!        case('CCSD','CCD')
!          call GET_object('CFT','ENERGY','CCSD')
          ! need to solve some response equations
!
        case('READ')
!
          write(UNIout,'(/a,1x,a)')'2-RDM input file name: ',TWORDMfile
          call read_2RDM
          ! If using an external 2-RDM, going to need the MOs too
          allocate(inpMO(Nbasis,Nbasis))
          call read_MO(inpMO)
          if(.not.associated(CMO_RHF%coeff))then
            allocate (CMO_RHF%coeff(Nbasis,Nbasis), CMO_RHF%EIGVAL(Nbasis), CMO_RHF%occupancy(Nbasis))
          else
            deallocate (CMO_RHF%coeff, CMO_RHF%EIGVAL, CMO_RHF%occupancy)
            allocate (CMO_RHF%coeff(Nbasis,Nbasis), CMO_RHF%EIGVAL(Nbasis), CMO_RHF%occupancy(Nbasis))
          end if
          CMO => CMO_RHF
!
          CMO%coeff = inpMO
!
        case('DELTANO')
!
          call GET_object('CFT','NO','DELTANO')
!
          call CFT_BLD_2RDM
!
          do aMO = 1, Nbasis
            do bMO = 1, Nbasis
              abab_index = TEIindex(Nbasis,aMO,bMO,aMO,bMO)
              TRDM_MOaa(abab_index) = CFT2RDMaa(aMO,bMO,aMO,bMO)
              TRDM_MObb(abab_index) = CFT2RDMbb(aMO,bMO,aMO,bMO)
              TRDM_MOab(abab_index) = CFT2RDMab(aMO,bMO,aMO,bMO)
              TRDM_MOba(abab_index) = CFT2RDMba(aMO,bMO,aMO,bMO)
              abba_index = TEIindex(Nbasis,aMO,bMO,bMO,aMO)
              TRDM_MOaa(abba_index) = CFT2RDMaa(aMO,bMO,bMO,aMO)
              TRDM_MObb(abba_index) = CFT2RDMbb(aMO,bMO,bMO,aMO)
              TRDM_MOab(abba_index) = CFT2RDMab(aMO,bMO,bMO,aMO)
              TRDM_MOba(abba_index) = CFT2RDMba(aMO,bMO,bMO,aMO)
              aabb_index = TEIindex(Nbasis,aMO,aMO,bMO,bMO)
              TRDM_MOab(aabb_index) = CFT2RDMab(aMO,aMO,bMO,bMO)
              TRDM_MOba(aabb_index) = CFT2RDMba(aMO,aMO,bMO,bMO)
            end do ! bMO
          end do ! aMO
!
          deallocate(CFT2RDMaa,CFT2RDMbb,CFT2RDMab,CFT2RDMba)
!
        case('NONE')
!
        ! leave it as zero
!
        case default
          write(UNIout,'(a,1x,a)')'Cannot determine correlated 2-RDM for :',correlation
!
        end select
!
! Turn off core contribution if requested
        Ncore = 0
        if(.not.LRDMcore)then
          call remove_core_2RDM
        end if !
!
! End of routine BLD_2RDM_MO_corr
      call PRG_manager ('exit', 'BLD_2RDM_MO_corr', 'UTILITY')
      return
      end subroutine BLD_2RDM_MO_corr
      subroutine remove_core_2RDM
!***********************************************************************
!     Date last modified: May 25, 2021                                 *
!     Author: J.W. Hollett                                             *
!     Description: Remove the core contributions from the 2-RDM.       *
!***********************************************************************
! Modules:
      USE program_files
      USE QM_objects
      USE QM_defaults
      USE type_basis_set
      USE INT_objects

      implicit none
!
! Local scalars:
      integer :: pMO, qMO, rMO, sMO, pqrs_index
!
! Local functions:
      integer :: TEIindex
!
! Begin:
      call PRG_manager ('enter', 'remove_core_2RDM', 'UTILITY')
!
! Turn off core contribution if requested
      Ncore = NFrozen_cores()
      ! remove all core contributions
!
      write(6,'(/a)')'Removing contribution of core electrons to correlated 2-RDM'
      write(6,'(a,I3,a)')'(Number of core orbitals = ', Ncore,')'
!
      do pMO = 1, Ncore
        do qMO = 1, Nbasis
          do rMO = 1, Nbasis
            do sMO = 1, Nbasis
              pqrs_index = Nbasis**3*(pMO-1) + Nbasis**2*(qMO-1) + Nbasis*(rMO-1) + sMO
              TRDM_MOaa(pqrs_index) = 0.0D0
              TRDM_MObb(pqrs_index) = 0.0D0
              TRDM_MOab(pqrs_index) = 0.0D0
              TRDM_MOba(pqrs_index) = 0.0D0
            end do ! sMO 
          end do ! rMO 
        end do ! qMO 
      end do ! pMO 
!
      do pMO = 1, Nbasis
        do qMO = 1, Ncore
          do rMO = 1, Nbasis
            do sMO = 1, Nbasis
              pqrs_index = Nbasis**3*(pMO-1) + Nbasis**2*(qMO-1) + Nbasis*(rMO-1) + sMO
              TRDM_MOaa(pqrs_index) = 0.0D0
              TRDM_MObb(pqrs_index) = 0.0D0
              TRDM_MOab(pqrs_index) = 0.0D0
              TRDM_MOba(pqrs_index) = 0.0D0
            end do ! sMO 
          end do ! rMO 
        end do ! qMO 
      end do ! pMO 
!
      do pMO = 1, Nbasis
        do qMO = 1, Nbasis
          do rMO = 1, Ncore
            do sMO = 1, Nbasis
              pqrs_index = Nbasis**3*(pMO-1) + Nbasis**2*(qMO-1) + Nbasis*(rMO-1) + sMO
              TRDM_MOaa(pqrs_index) = 0.0D0
              TRDM_MObb(pqrs_index) = 0.0D0
              TRDM_MOab(pqrs_index) = 0.0D0
              TRDM_MOba(pqrs_index) = 0.0D0
            end do ! sMO 
          end do ! rMO 
        end do ! qMO 
      end do ! pMO 
!
      do pMO = 1, Nbasis
        do qMO = 1, Nbasis
          do rMO = 1, Nbasis
            do sMO = 1, Ncore
              pqrs_index = Nbasis**3*(pMO-1) + Nbasis**2*(qMO-1) + Nbasis*(rMO-1) + sMO
              TRDM_MOaa(pqrs_index) = 0.0D0
              TRDM_MObb(pqrs_index) = 0.0D0
              TRDM_MOab(pqrs_index) = 0.0D0
              TRDM_MOba(pqrs_index) = 0.0D0
            end do ! sMO 
          end do ! rMO 
        end do ! qMO 
      end do ! pMO 
!
! End of routine remove_core_2RDM
      call PRG_manager ('exit', 'remove_core_2RDM', 'UTILITY')
      return
      end subroutine remove_core_2RDM
