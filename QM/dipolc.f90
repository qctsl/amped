      subroutine CDIPOL
!***************************************************************************
!     Date last modified: November 2, 2000                     Version 1.0 *
!     Author: R.A. Poirier                                                 *
!     Description: Evaluation of electric dipole moment.                   *
!***************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE INT_objects

      implicit none
!
! Local scalars:
      integer I,II,I1,I2,J,K
      double precision AN,DD,T
!
! Work array:
      double precision, dimension(:,:), allocatable :: PM0SQ
      double precision, dimension(:,:), allocatable :: XaosQ
      double precision, dimension(:,:), allocatable :: YaosQ
      double precision, dimension(:,:), allocatable :: ZaosQ
!
! Begin:
      call PRG_manager ('enter', 'CDIPOL', 'UTILITY')
!
      call GET_object ('INT', '1EINT', 'DIPOLE')
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction(1:len_trim(Wavefunction)))
!
      allocate(PM0SQ(Nbasis,Nbasis), XaosQ(Nbasis,Nbasis), YaosQ(Nbasis,Nbasis), ZaosQ(Nbasis,Nbasis))
      call UPT_to_SQS (PM0, MATlen, PM0SQ, Nbasis)
      call UPT_to_SQS (DipoleX_AO, MATlen, XaosQ, Nbasis)
      call UPT_to_SQS (DipoleY_AO, MATlen, YaosQ, Nbasis)
      call UPT_to_SQS (DipoleZ_AO, MATlen, ZaosQ, Nbasis)
!
!     Calculate X, Y and Z components independently:
      Dipole%X=ZERO
      Dipole%Y=ZERO
      Dipole%Z=ZERO
!     Form trace of product  A*DIPOLE.
      do J=1,Nbasis
      do I=1,Nbasis
        Dipole%X=Dipole%X-PM0SQ(I,J)*XaosQ(I,J)
        Dipole%Y=Dipole%Y-PM0SQ(I,J)*YaosQ(I,J)
        Dipole%Z=Dipole%Z-PM0SQ(I,J)*ZaosQ(I,J)
      end do !I
      end do !J
!
! Electronic only
      Dipole_elec%X=Dipole%X
      Dipole_elec%Y=Dipole%Y
      Dipole_elec%Z=Dipole%Z
!
!     Add in nuclear contribution.
      do I=1,Natoms
      if(CARTESIAN(I)%Atomic_number.GT.0)then
      AN=CARTESIAN(I)%Atomic_number
      Dipole%X=Dipole%X+CARTESIAN(I)%X*AN
      Dipole%Y=Dipole%Y+CARTESIAN(I)%Y*AN
      Dipole%Z=Dipole%Z+CARTESIAN(I)%Z*AN
      end if ! CARTESIAN(I)%Atomic_number.GT.0
      end do !I
!
!     Compute total dipole moment.
      Dipole%total=DSQRT(Dipole%X*Dipole%X+Dipole%Y*Dipole%Y+Dipole%Z*Dipole%Z)
!
      write(UNIout,1000)
      write(UNIout,1020)Dipole%X*DCONST,Dipole%Y*DCONST,Dipole%Z*DCONST,Dipole%total*DCONST

      deallocate(PM0SQ, XaosQ, YaosQ, ZaosQ)
!
 1000 FORMAT(/'Dipole moment (Debye):'/'**********************')
 1020 FORMAT(7X,'X:',1PE14.6,5X,'Y:',E14.6,5X,'Z:',E14.6,5X,'TOTAL:',0PF9.6/)
!
! End of routine CDIPOL
      call PRG_manager ('exit', 'CDIPOL', 'UTILITY')
      return
      end subroutine CDIPOL
