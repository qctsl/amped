      function TRARHF (A, B, Nbasis, NoccMO)
!***********************************************************************
!     Date last modified: June 26, 1992                                *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version                                        *
!     Trace of a product of two matrices A*B', each (Nbasis x NoccMO). *
!                                                                      *
!     Routines called: none                                            *
!***********************************************************************
! Modules:
      USE program_constants

      implicit none
!
! Input scalars:
      integer NoccMO,Nbasis
!
! Input arrays:
      double precision A(Nbasis,Nbasis),B(Nbasis,Nbasis)
!
! Local scalars:
      integer I,J
      double precision TRARHF
!
! Begin:
      TRARHF=ZERO
      do J=1,NoccMO
      do I=1,Nbasis
      TRARHF=TRARHF+A(I,J)*B(I,J)
      end do ! I
      end do ! J
!
! End of function TRARHF
      return
      end function TRARHF
      subroutine SCHMDT (CMO_A, SQSM, NbasisIN)
!***********************************************************************
!     Date last modified: June 26, 1992                                *
!     Author:                                                          *
!     Description:                                                     *
!     Schmidt orthogonalization of the MO vectors in CMO_A.            *
!     This is necessary to ensure the correct form of the projection   *
!     operators.  SQSM is the overlap matrix.                          *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE QM_defaults

      implicit none
!
! Input scalar:
      integer NbasisIN
!
! Input arrays:
      double precision CMO_A(NbasisIN,NbasisIN),SQSM(NbasisIN,NbasisIN)
!
! Local scalars:
      integer IMO,JMO,Kbasis,Lbasis
      double precision :: OVLP,T,TENM12
!
      parameter (TENM12=1.0D-12)
!
! Begin:
      do IMO=1,NbasisIN
      do JMO=1,IMO
      OVLP=ZERO
! Calculate overlap between MO I and MO J.
      do Kbasis=1,NbasisIN
      T=ZERO
      do Lbasis=1,NbasisIN
      T=T+SQSM(Kbasis,Lbasis)*CMO_A(Lbasis,IMO)
      end do ! Lbasis
      OVLP=OVLP+T*CMO_A(Kbasis,JMO)
      end do ! Kbasis
!
      if(IMO.ne.JMO) then
! Project component of MO J out of MO I.
        do Kbasis=1,NbasisIN
        CMO_A(Kbasis,IMO)=CMO_A(Kbasis,IMO)-OVLP*CMO_A(Kbasis,JMO)
        end do ! Kbasis
      end if ! IMO.ne.JMO
      end do ! JMO
! Normalize MO IMO.
      if(OVLP.le.TENM12)then  ! Was TENM10
! MO cannot be orthonormalized.
      write(UNIout,'(a)')'ERROR> SCHMDT:'
      write(UNIout,'(a,i4)')'UNABLE TO SCHMIDT ORTHONORMALIZE THE MO - FAILURE OCCURRED FOR MO',IMO
      stop 'ERROR> SCHMDT: UNABLE TO SCHMIDT ORTHONORMALIZE'
      end if ! OVLP.le.TENM10
!
      OVLP=ONE/DSQRT(OVLP)
      do Kbasis=1,NbasisIN
      CMO_A(Kbasis,IMO)=CMO_A(Kbasis,IMO)*OVLP
      end do ! Kbasis
      end do ! IMO
!
! End of routine SCHMDT
      return
      end subroutine SCHMDT
      subroutine BLD_Lagrange_matrix
!***********************************************************************
!     Date last modified: June 26, 1992                                *
!     Author:                                                          *
!     Description:  MUN Version                                        *
!     Calculation of Lagrange multipliers and orbital energies.        *
!     On exit, CA contains the current MO, and FockM contains the      *
!     Fock matrices.                                                   *
!***********************************************************************
! Modules:
      USE program_constants
      USE QM_defaults
      USE QM_objects
      USE GSCF_work

      implicit none
!
! Local scalars:
      integer :: Ibasis,Jbasis,IJ
      integer I,ICORE,IFIX,II,J,JFIX,JJ,K,KK,M,N,Ncore1,NOCC1,NPASS,NVIRT
      double precision DA,FMN,SUM
! Local work array:
      double precision, dimension(:,:), allocatable :: Wmatrix
      double precision, dimension(:), allocatable :: WEigs
!
! Begin:
      call PRG_manager ('enter', 'BLD_Lagrange_matrix', 'UTILITY')
!
      Ncore1=CMO%NFcoreMO+1
      call PHASE (CMO%coeff, Nbasis)
      ICORE=1-CMO%NFcoreMO
      if(CMO%NFcoreMO.gt.0)then
      NPASS=0
!
! Core-Core.
      allocate(Wmatrix(CMO%NFcoreMO,CMO%NFcoreMO))
    1 do I=1,CMO%NFcoreMO
        do J=1,I
           IJ=0
           SUM=ZERO
           do Ibasis=1,Nbasis
             FMN=ONE
             do Jbasis=1,Ibasis
               IJ=IJ+1
               DA=CMO%coeff(Ibasis,I)*CMO%coeff(Jbasis,J)+CMO%coeff(Jbasis,I)*CMO%coeff(Ibasis,J)
               if(Jbasis.eq.Ibasis)FMN=PT5
               SUM=SUM+FMN*DA*FockMN(IJ,1)
             end do ! Jbasis
           end do ! Ibasis
!
            Wmatrix(I,J)=SUM
            Wmatrix(J,I)=SUM
            LAGRGM(I,J)=SUM
            LAGRGM(J,I)=SUM
         end do ! J
         CMO%eigval(I)=SUM
      end do ! I
!
      if(NPASS.eq.0.and.CMO%NFcoreMO.gt.1)then
! Diagonalize frozen core Lagrange multipliers matrix E such that EDIAG = CB' * E * CB, where CB is a unitary matrix.
      allocate(WEigs(CMO%NFcoreMO))
      call MATRIX_diagonalize (Wmatrix, Wmatrix, WEigs, CMO%NFcoreMO, -2, .true.)
      deallocate(WEigs)
! Now the exact closed shell MO (X) are given by X = CA * CB, where CA is the current MO.
      do Ibasis=1,Nbasis
         do J=1,CMO%NFcoreMO
            SUM=ZERO
            do K=1,CMO%NFcoreMO
               SUM=SUM+CMO%coeff(Ibasis,K)*Wmatrix(K,J)
            end do ! K
!
            CMO%eigval(J)=SUM
         end do ! J
!
         do J=1,CMO%NFcoreMO
            CMO%coeff(Ibasis,J)=CMO%eigval(J)
         end do ! J
      end do ! Ibasis
!
      NPASS=1
      GO TO 1
      end if ! NPASS.eq.0.and.CMO%NFcoreMO.gt.1
!
! Core-Valence.
      if(CMO%NFcoreMO.lt.CMO%noccmo)then
      do I=Ncore1,CMO%noccmo
         IFIX=I+ICORE
         do J=1,CMO%NFcoreMO
            IJ=0
            SUM=ZERO
            do Ibasis=1,Nbasis
               FMN=ONE
               do Jbasis=1,Ibasis
                  IJ=IJ+1
                  if(Jbasis.eq.Ibasis)FMN=PT5
                  DA=CMO%coeff(Ibasis,I)*CMO%coeff(Jbasis,J)+CMO%coeff(Jbasis,I)*CMO%coeff(Ibasis,J)
                  SUM=SUM+FMN*DA*(FockMN(IJ,IFIX)+FockMN(IJ,1))
               end do ! Jbasis
            end do ! Ibasis
!
            LAGRGM(I,J)=PT5*SUM
            LAGRGM(J,I)=PT5*SUM
         end do ! J
      end do ! I
      end if ! CMO%NFcoreMO.lt.CMO%noccmo
      end if ! CMO%NFcoreMO.gt.0
!
! Valence-Valence.
      do I=Ncore1,CMO%noccmo
      IFIX=I+ICORE
      do J=Ncore1,I
      JFIX=J+ICORE
      IJ=0
      SUM=ZERO
      do Ibasis=1,Nbasis
      FMN=ONE
      do Jbasis=1,Ibasis
      IJ=IJ+1
      if(Jbasis.eq.Ibasis)FMN=PT5
      DA=CMO%coeff(Ibasis,I)*CMO%coeff(Jbasis,J)+CMO%coeff(Jbasis,I)*CMO%coeff(Ibasis,J)
      SUM=SUM+FMN*DA*(FockMN(IJ,IFIX)+FockMN(IJ,JFIX))
      end do ! Jbasis
      end do ! Ibasis
!
      LAGRGM(I,J)=PT5*SUM
      LAGRGM(J,I)=PT5*SUM
      end do ! J
!
      CMO%eigval(I)=LAGRGM(I,I)
      end do ! I
!
! Lagrange multipliers for virtual orbitals.
      NOCC1=CMO%noccmo+1
      NVIRT=Nbasis-CMO%noccmo
      if(NVIRT.le.0)then
      call PRG_manager ('exit', 'BLD_Lagrange_matrix', 'UTILITY')
      return
      end if ! NVIRT.le.0
      NPASS=0
      if(allocated(Wmatrix))deallocate(Wmatrix)
      allocate(Wmatrix(Nvirt,Nvirt))
!
! Virtual-Virtual.
   50 II=0
      SUM=ZERO
      do I=NOCC1,Nbasis
      II=II+1
      JJ=0
      do J=NOCC1,I
      IJ=0
      SUM=ZERO
      do Ibasis=1,Nbasis
      FMN=ONE
      do Jbasis=1,Ibasis
      IJ=IJ+1
      DA=CMO%coeff(Ibasis,I)*CMO%coeff(Jbasis,J)+CMO%coeff(Jbasis,I)*CMO%coeff(Ibasis,J)
      if(Ibasis.eq.Jbasis)FMN=PT5
      SUM=SUM+FMN*DA*FockMN(IJ,1)
      end do ! Jbasis
      end do ! Ibasis
!
      JJ=JJ+1
      Wmatrix(II,JJ)=SUM
      Wmatrix(JJ,II)=SUM
      LAGRGM(I,J)=SUM
      LAGRGM(J,I)=SUM
      end do ! J NOCC1
!
      CMO%eigval(I)=SUM
      end do ! I NOCC1
!
      if(NPASS.eq.0.and.NVIRT.gt.1)then
! Diagonalize virtual orbital Lagrange multiplier matrix, as above.
      allocate(WEigs(Nvirt))
      call MATRIX_diagonalize (Wmatrix, Wmatrix, WEigs, Nvirt, -2, .true.)
      deallocate(WEigs)
      do I=1,Nbasis
      do J=1,NVIRT
      SUM=ZERO
      do K=1,NVIRT
      KK=CMO%noccmo+K
      SUM=SUM+CMO%coeff(I,KK)*Wmatrix(K,J)
      end do ! K
!
      JJ=CMO%noccmo+J
      CMO%eigval(JJ)=SUM
      end do ! J
!
      do JJ=NOCC1,Nbasis
      CMO%coeff(I,JJ)=CMO%eigval(JJ)
      end do ! JJ
      end do ! I
      NPASS=1
      GO TO 50
      end if ! NPASS.eq.0.and.NVIRT.gt.1
!
! Core-Virtual.
      if(CMO%NFcoreMO.gt.0)then
      do I=NOCC1,Nbasis
      do J=1,CMO%NFcoreMO
      IJ=0
      SUM=ZERO
      do Ibasis=1,Nbasis
      FMN=ONE
      do Jbasis=1,Ibasis
      IJ=IJ+1
      DA=CMO%coeff(Ibasis,I)*CMO%coeff(Jbasis,J)+CMO%coeff(Jbasis,I)*CMO%coeff(Ibasis,J)
      if(Ibasis.eq.Jbasis)FMN=PT5
      SUM=SUM+FMN*DA*FockMN(IJ,1)
      end do ! Jbasis
      end do ! Ibasis
!
      LAGRGM(I,J)=SUM
      LAGRGM(J,I)=SUM
      end do ! J
      end do ! I
      end if ! CMO%NFcoreMO.gt.0
!
! Valence-Virtual.
      if(CMO%NFcoreMO.lt.CMO%noccmo)then
      do I=NOCC1,Nbasis
      do J=Ncore1,CMO%noccmo
      JFIX=J+ICORE
      IJ=0
      SUM=ZERO
      do Ibasis=1,Nbasis
      FMN=ONE
      do Jbasis=1,Ibasis
      IJ=IJ+1
      DA=CMO%coeff(Ibasis,I)*CMO%coeff(Jbasis,J)+CMO%coeff(Jbasis,I)*CMO%coeff(Ibasis,J)
      if(Ibasis.eq.Jbasis)FMN=PT5
      SUM=SUM+FMN*DA*(FockMN(IJ,1)+FockMN(IJ,JFIX))
      end do ! Jbasis
      end do ! Ibasis
!
      LAGRGM(I,J)=PT5*SUM
      LAGRGM(J,I)=PT5*SUM
      end do ! J
      end do ! I
      end if ! CMO%NFcoreMO.lt.CMO%noccmo

      if(allocated(Wmatrix))deallocate(Wmatrix)
!
! End of routine BLD_Lagrange_matrix
      call PRG_manager ('exit', 'BLD_Lagrange_matrix', 'UTILITY')
      return
      end subroutine BLD_Lagrange_matrix
      subroutine MATCH_MO (MATCH, &
                           CMO_A, &
                           CMO_B, &
                           SM, &
                           NoccGVBMO)
!*************************************************************************
!     Date last modified: June 26, 1992                                  *
!     Author:                                                            *
!     Description:                                                       *
!     CMO_B contains the old MO, and CMO_A contains the new MO.          *
!     On exit, CMO_A contains the new MO, matched by sign to the old MO. *
!     On exit, MATCH is set false if a matching failure occurred,        *
!     else MATCH is set true.                                            *
!*************************************************************************
! Modules:
      USE program_constants
      USE QM_objects
      USE GSCF_work
      USE ixtrapolate

      implicit none
!
! Input scalars:
      integer NoccGVBMO
!
! Input arrays:
      double precision CMO_A(Nbasis,Nbasis),CMO_B(Nbasis,Nbasis),SM(MATlen)
!
! Output scalars:
      logical MATCH
!
! Local scalars:
      integer Ibasis,Jbasis
      integer ICOUNT,IJ,IX,JCOUNT,K
      double precision PT7,SUM,XMAX,XMOT,XX,YMAX
!
      parameter (PT7=0.7D0)
!
! Begin:
      MATCH=.TRUE.
      XMAX=PT7
      YMAX=-XMAX
! Get a unitary matrix T such that CMO_A = CMO_B * T.
! Therefore, T = CMO_B' * S * CMO_A.
      do IX=1,NoccGVBMO
      do K=1,NoccGVBMO
        SUM=ZERO
        IJ=0
        do Ibasis=1,Nbasis
        do Jbasis=1,Ibasis
        IJ=IJ+1
! Ray: XMOT was not being reset!!!
        if(Ibasis.eq.Jbasis)then
          SUM=SUM+PT5*(CMO_B(Ibasis,IX)*CMO_A(Jbasis,K)+CMO_B(Jbasis,IX)*CMO_A(Ibasis,K))*SM(IJ)
        else
          SUM=SUM+(CMO_B(Ibasis,IX)*CMO_A(Jbasis,K)+CMO_B(Jbasis,IX)*CMO_A(Ibasis,K))*SM(IJ)
        end if
        end do ! Jbasis
        end do ! Ibasis
        SCMO(IX,K)=SUM
      end do ! K
      end do ! IX
!
! Scan the two-dimensional table so as to pick up the values
! larger than ABS(XMAX).
      do K=1,NoccGVBMO
      do IX=1,NoccGVBMO
      MOTB(IX,K)=0
      XX=SCMO(IX,K)
      if(XX.gt.XMAX)MOTB(IX,K)=1
      if(XX.lt.YMAX)MOTB(IX,K)=-1
      end do ! IX
      end do ! K
!
! Check whether one-to-one correspondence exists or not.
! Normally ICOUNT should be 1, otherwise out of the regular
! correspondence.
      do IX=1,NoccGVBMO
      ICOUNT=0
      JCOUNT=0
      do K=1,NoccGVBMO
      if(MOTB(IX,K).ne.0)ICOUNT=ICOUNT+1
      if(MOTB(K,IX).ne.0)JCOUNT=JCOUNT+1
      end do ! K
!
      if(ICOUNT.gt.1.or.JCOUNT.gt.1)then
      LABEL='NO MATCH'
      MATCH=.FALSE.
      return
      end if
!
      end do ! IX
!
      do K=1,NoccGVBMO
      do IX=1,NoccGVBMO
      if(MOTB(IX,K).ne.0)then
        XMOT=DBLE(MOTB(IX,K))
        do Jbasis=1,Nbasis
        SYMO(Jbasis,IX)=CMO_A(Jbasis,K)*XMOT
        end do ! Jbasis
      end if
      end do ! IX
      end do ! K
!
      do IX=1,NoccGVBMO
      do Jbasis=1,Nbasis
      CMO_A(Jbasis,IX)=SYMO(Jbasis,IX)
      end do ! Jbasis
      end do ! IX
!
! End of routine MATCH_MO
      return
      end subroutine MATCH_MO
