      subroutine G1E_BASIS
!***********************************************************************
!     Date last modified: FEBRUARY 4, 1994                 Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version                                        *
!     Calculate forces on the nuclei and basis functions for SP        *
!     functions.  The forces are calculated by the method of Pulay:    *
!     P. Pulay, Mol. Phys., 17, 197 (1969).                            *
!***********************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE type_basis_set
      USE QM_defaults
      USE QM_objects
      USE gradients
      USE gamma_fn

      implicit none
!
! Input array:

      double precision, dimension(:), allocatable :: FF0
      double precision, dimension(:), allocatable :: FF1
      double precision, dimension(:), allocatable :: FF2
      double precision, dimension(:), allocatable :: FF3
!
! Local scalars:
      integer :: Nprimitives
!
! Local scalars:
      integer I,J,Iatom,Jatom,Katom,Iaos,IAS,ID,IDX,Iend,IF1,II,IR1,IR2,Ishell,Istart,I1,I2,JaoS,JAS,Jend,JNN,Jshell,&
              Jstart,K,LIJ,LTEMP,Matom,MNN,N,NN,N1,N2,INT_type
      integer Ifrst,Jfrst,IGBGN,JGBGN,IGEND,JGEND,Ilast,Jlast,Iatmshl,Jatmshl,Irange,Jrange,LAMAX,LBMAX,Igauss,Jgauss
      double precision ABX,ABY,ABZ,AX,AX1,AY,AZ,AS,EPAB,EPABI,BS,BX,BY,BZ,CD12,CN,CPN,CPQ,CPR,CQ,CR,CCA,&
                       CCB,CCAB,DDC,DDS,DE12,DE12A,DE12B,DFQ,DFQA,DFQB,DKOO,DKOOA,DKOOB,DKOQ,DKOQA,DKOQB,DKRO,&
                       DKROA,DKROB,DKRQA,DKRQB,DLNOO,DLNOQ,DLNRO,DLNRQ,DLOO,DLOOA,DLOOB,DLOQ,DLOQA,DLOQB,DLRO,DLROA,&
                       DLROB,DLRQ,DLRQA,DLRQB,DMS,DM1,DM2,DNA,DNB,DOO,DOOA,DOOB,DOOQ,DOOQA,DOOQB,DORO,DOROA,DOROB,DORQ,&
                       DORQA,DORQB,DQN,DRN,DRQ,DS1A,DS1B,DS12A,DS12B,DS2A,DS2B,DT,DTA,DTB,DVA,DVB,DVHF,DVNE,E12,&
                       FQ0,FQ1,FQ2,FQ3,KOO,KOQ,KRO,KRQ,LOO,LOQ,LRO,LRQ,OO,OOQ,ORO,ORQ,PDM,QQ,&
                       RABSQ,R12S12,S1,S12,S2,TKOO,TA,TB,TC,THETA,THETA2,THETA3,THETA4,TT,T1,T1I,T2,V,ZJ
!
! Local arrays:
      double precision CAB(3),DPA(3),DPB(3),EDM(16),EODN(16),PAB(3)
!
! Local parameters:
      double precision P75,ONE5,TWOPT5,FIFTEN,TWENTY
      double precision PT78,TRP
      parameter (P75=0.75D0,ONE5=1.5D0,TWOPT5=2.5D0,FIFTEN=15.0D0,TWENTY=20.0D0)
!
! Begin:
      call PRG_manager ('enter', 'G1E_BASIS', 'UTILITY')
!
!     Need to check if basis set has d- or f-functions

      Nprimitives=Basis%Nprimitives
      allocate (DEXX1(Nprimitives), D1C1(Nprimitives), D1C2(Nprimitives), GBASCL(Basis%Nshells))
      allocate (DEXX2(Nprimitives), D2C1(Nprimitives), D2C2(Nprimitives)) ! For now
!
      allocate (FF0(1:Natoms), FF1(1:Natoms), FF2(1:Natoms), FF3(1:Natoms))
!
      PT78=pi_val/FOUR
      TRP=TWO/DSQRT(pi_val)
      TXX=ZERO
      VNEXX=ZERO
      VEEXX=ZERO
      VNNXX=ZERO
!
      gradient(1:3*Natoms)%NEOPR=ZERO
      gradient(1:3*Natoms)%NEWFN=ZERO
      gradient(1:3*Natoms)%WFN=ZERO
      gradient(1:3*Natoms)%COEFF=ZERO
!
! Set indexing array (for DM and DN).
      DEXX1(1:Nprimitives)=ZERO
      D1C1(1:Nprimitives)=ZERO
      D1C2(1:Nprimitives)=ZERO
      DEXX2(1:Nprimitives)=ZERO
      D2C1(1:Nprimitives)=ZERO
      D2C2(1:Nprimitives)=ZERO
      GBASCL(1:Basis%Nshells)=ZERO
!
! Begin loop over shells.
! Loop over Ishell.
      do Ishell=1,Basis%Nshells
      do Jshell=1,Ishell
!
      Ifrst=Basis%shell(Ishell)%frstSHL
      Ilast= Basis%shell(Ishell)%lastSHL
      do Iatmshl=Ifrst,Ilast
!
! Loop over Jatmshl
      Jfrst=Basis%shell(Jshell)%frstSHL
      Jlast= Basis%shell(Jshell)%lastSHL
      IF(Ishell.EQ.Jshell)Jlast=Iatmshl
      do Jatmshl=Jfrst,Jlast

      LAMAX=Basis%shell(Ishell)%Xtype+1
      Istart=Basis%shell(Ishell)%Xstart
      Iend=Basis%shell(Ishell)%XEND
      Irange=Iend-Istart+1
      IGBGN=Basis%shell(Ishell)%EXPBGN
      IGEND=Basis%shell(Ishell)%EXPEND
      Iaos=Basis%atmshl(Iatmshl)%frstAO-1
      Iatom=Basis%atmshl(Iatmshl)%ATMLST

      LBMAX=Basis%shell(Jshell)%Xtype+1
      Jstart=Basis%shell(Jshell)%Xstart
      Jend=Basis%shell(Jshell)%XEND
      Jrange=Jend-Jstart+1
      JGBGN=Basis%shell(Jshell)%EXPBGN
      JGEND=Basis%shell(Jshell)%EXPEND
      Jaos=Basis%atmshl(Jatmshl)%frstAO-1
      Jatom=Basis%atmshl(Jatmshl)%ATMLST

      AX=CARTESIAN(Iatom)%X
      AY=CARTESIAN(Iatom)%Y
      AZ=CARTESIAN(Iatom)%Z

      BX=CARTESIAN(Jatom)%X
      BY=CARTESIAN(Jatom)%Y
      BZ=CARTESIAN(Jatom)%Z
!
      AX1=ONE
      IF(Iatmshl.NE.Jatmshl)AX1=AX1+AX1
!
      ABX=AX-BX
      ABY=AY-BY
      ABZ=AZ-BZ
      RABSQ=ABX**2+ABY**2+ABZ**2
      CAB(1)=ABX
      CAB(2)=ABY
      CAB(3)=ABZ

      IF(LAMAX.EQ.2)Iaos=IaoS-1
      IF(LBMAX.EQ.2)Jaos=JaoS-1

      IDX=0
      do I=Istart,Iend
        IAS=Iaos+I
        do J=Jstart,Jend
          JAS=Jaos+J
          LIJ=IAS*(IAS-1)/2+JAS
          IF(IAS.LT.JAS)LIJ=JAS*(JAS-1)/2+IAS
          IDX=IDX+1
          EDM(IDX)=PM0(LIJ)
          EODN(IDX)=PEWD(LIJ)
        end do ! J
      end do ! I
!
! Loop over the primitive Gaussians.
      do Igauss=IGBGN,IGEND
      AS=Basis%gaussian(Igauss)%exp
      CCA=Basis%gaussian(Igauss)%contrc

      do Jgauss=JGBGN,JGEND
      BS=Basis%gaussian(Jgauss)%exp
      CCB=Basis%gaussian(Jgauss)%contrc

      EPAB=AS+BS
      EPABI=ONE/EPAB
      S1=AS*EPABI
      S2=BS*EPABI
      S12=AS*S2
      R12S12=RABSQ*S12
      E12=ZERO
      IF(R12S12.LE.GRD_expcut)E12=DEXP(-R12S12)*(pi_val*EPABI)**ONE5
      DS12A=S2*S2
      DS12B=S1*S1
      DS1A= S2*EPABI
      DS1B=-S1*EPABI
      DS2A=-S2*EPABI
      DS2B= S1*EPABI
      CCAB=CCA*CCB*E12

      PAB(1)=AX*S1+BX*S2
      PAB(2)=AY*S1+BY*S2
      PAB(3)=AZ*S1+BZ*S2
      T2=TRP*DSQRT(EPAB)

      do Katom=1,Natoms
        I1=CARTESIAN(Katom)%Atomic_number
        IF(I1.GT.0) then
          ZJ=-T2*DBLE(I1)
          TT=EPAB*((CARTESIAN(Katom)%X-PAB(1))**2+(CARTESIAN(Katom)%Y-PAB(2))**2+(CARTESIAN(Katom)%Z-PAB(3))**2)
          IF(TT.GT.FIFTEN) then
            FQ0=DSQRT(PT78/TT)
            FQ1=PT5*FQ0/TT
            FQ2=ONE5*FQ1/TT
            FQ3=TWOPT5*FQ2/TT
          else
            QQ=TT*TWENTY
            N=QQ
            THETA=QQ-N
            N1=N+1
            N2=N1+1
            THETA2=THETA*(THETA-ONE)
            THETA3=THETA2*(THETA-TWO)
            THETA4=THETA2*(THETA+ONE)
            FQ0=(FMT_val(N+1)%FM0+THETA*FMT_del(N+1)%FM0-THETA3*FMT_int(N+1)%FM0+THETA4*FMT_int(N+2)%FM0)
            FQ1=(FMT_val(N+1)%FM1+THETA*FMT_del(N+1)%FM1-THETA3*FMT_int(N+1)%FM1+THETA4*FMT_int(N+2)%FM1)
            FQ2=(FMT_val(N+1)%FM2+THETA*FMT_del(N+1)%FM2-THETA3*FMT_int(N+1)%FM2+THETA4*FMT_int(N+2)%FM2)
            FQ3=(FMT_val(N+1)%FM3+THETA*FMT_del(N+1)%FM3-THETA3*FMT_int(N+1)%FM3+THETA4*FMT_int(N+2)%FM3)
          end if
  110     FF0(Katom)=FQ0*ZJ
          FF1(Katom)=FQ1*ZJ
          FF2(Katom)=FQ2*ZJ
          FF3(Katom)=FQ3*ZJ
        end if ! I1.GT.0
      end do ! Katom

      DE12A=-RABSQ*DS12A-ONE5*EPABI
      DE12B=-RABSQ*DS12B-ONE5*EPABI
      KOO=S12*(THREE-R12S12-R12S12)
      TT=THREE-FOUR*R12S12
      DKOOA=DS12A*TT
      DKOOB=DS12B*TT
      T1=-EPAB-EPAB
      T1I=ONE/T1
      DPA(1)=DS1A*AX+DS2A*BX
      DPA(2)=DS1A*AY+DS2A*BY
      DPA(3)=DS1A*AZ+DS2A*BZ
      DPB(1)=DS1B*AX+DS2B*BX
      DPB(2)=DS1B*AY+DS2B*BY
      DPB(3)=DS1B*AZ+DS2B*BZ
!
      INT_type=LAMAX*(LAMAX-1)/2+LBMAX
      if(INT_type.eq.1)then
        call G1E_SS
      else if(INT_type.eq.2)then
        call G1E_SP
      else if(INT_type.eq.3)then
        call G1E_PP
      end if ! INT_type

      end do ! Jgauss
      end do ! Igauss
      end do ! Jatmshl
      end do ! Iatmshl
      end do ! Jshell
      end do ! Ishell
!
      deallocate (FF0, FF1, FF2, FF3)

! End of routine G1E_BASIS
      call PRG_manager ('exit', 'G1E_BASIS', 'UTILITY')
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine G1E_SS
!***********************************************************************
!     Date last modified: Dec. 8, 2005                     Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: Get addresses for printing of basis set gradients.  *
!***********************************************************************
! End of routine G1E_SS
      call PRG_manager ('exit', 'G1E_SS', 'UTILITY')

      DNA=P75/AS
      DNB=P75/BS
      TKOO=KOO
      DOOA=ZERO
      DOOB=ZERO
      DTA=DKOOA
      DTB=DKOOB
      DMS=EDM(1)*CCAB*AX1
      DDS=EODN(1)*CCAB*AX1

      TXX=TXX+TKOO*DMS
! VNE.
      V=ZERO
      DVA=ZERO
      DVB=ZERO
      do Katom=1,Natoms
      IF(CARTESIAN(Katom)%Atomic_number.le.0)cycle
      TA=CARTESIAN(Katom)%X-PAB(1)
      TB=CARTESIAN(Katom)%Y-PAB(2)
      TC=CARTESIAN(Katom)%Z-PAB(3)
      DFQ=-(TA*TA+TB*TB+TC*TC)
      DFQA=DFQ+TWO*(TA*DPA(1)+TB*DPA(2)+TC*DPA(3))*EPAB
      DFQB=DFQ+TWO*(TA*DPB(1)+TB*DPB(2)+TC*DPB(3))*EPAB
      FQ0=FF0(Katom)
      FQ1=FF1(Katom)
      LOO=FQ0
      DLOOA=FQ1*DFQA
      DLOOB=FQ1*DFQB
      V=V+LOO
      DVA=DLOOA+DVA
      DVB=DLOOB+DVB
      end do ! Katom

      VNEXX=VNEXX+V*DMS
      TA=DE12A+DNA
      TB=DE12B+DNB
      DEXX1(Igauss)=DEXX1(Igauss)+(DTA+TKOO*TA+DVA+V*(TA+PT5*EPABI))*DMS+(DOOA+TA)*DDS
      DEXX1(Jgauss)=DEXX1(Jgauss)+(DTB+TKOO*TB+DVB+V*(TB+PT5*EPABI))*DMS+(DOOB+TB)*DDS
      DDC=(TKOO+V)*DMS+DDS

      D1C1(Igauss)=D1C1(Igauss)+DDC/CCA
      D1C1(Jgauss)=D1C1(Jgauss)+DDC/CCB
!
      do IF1=1,2 ! (A'|B) + (A|B')
      MNN=3*(Iatom-1)
      IF(IF1.EQ.2)then
        MNN=3*(Jatom-1)
      end if

      do NN=1,3 ! X,Y,Z
! Kinetic and overlap derivatives.
      CN=CAB(NN)
      DM1=ZERO
      DM2=ZERO
      IF(IF1.EQ.1)DM1=ONE
      IF(IF1.EQ.2)DM2=ONE
      CD12=DM1-DM2
      DE12=-TWO*CN*CD12*S12
      DKOO=-FOUR*CN*CD12*S12*S12
!
      DOO=DE12
      DT=DE12*TKOO+DKOO

! VNE Derivatives.
      DVNE=ZERO
      PDM=-(S1*DM1+S2*DM2)

      do Katom=1,Natoms
      IF(CARTESIAN(Katom)%Atomic_number.LE.0)cycle
      JNN=3*(Katom-1)+NN
      IF(NN.EQ.1)CPN=CARTESIAN(Katom)%X-PAB(NN)
      IF(NN.EQ.2)CPN=CARTESIAN(Katom)%Y-PAB(NN)
      IF(NN.EQ.3)CPN=CARTESIAN(Katom)%Z-PAB(NN)
      FQ1=FF1(Katom)
      DLNOO=T1*CPN*FQ1
      DLOO=DLNOO*PDM
      DVNE=DVNE+DLOO
      IF(IF1.eq.1)then
        DVHF=DLNOO
        gradient(JNN)%NEOPR=gradient(JNN)%NEOPR+DVHF*DMS
      end if
      end do ! Katom

      gradient(MNN+NN)%NEWFN=gradient(MNN+NN)%NEWFN+(DE12*V+DVNE)*DMS
      gradient(MNN+NN)%WFN=gradient(MNN+NN)%WFN+DT*DMS
      gradient(MNN+NN)%COEFF=gradient(MNN+NN)%COEFF+DOO*DDS
      end do ! NN
      end do ! IF1

      return
      end subroutine G1E_SS
      subroutine G1E_SP
!***********************************************************************
!     Date last modified: Dec. 8, 2005                     Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: Get addresses for printing of basis set gradients.  *
!***********************************************************************

      implicit none

! Begin
      call PRG_manager ('enter', 'G1E_SP', 'UTILITY')

! Loop over S, PX, PY and PZ functions.
      IDX=0
      do I1=Istart,Iend
      IR1=I1-1
      do I2=Jstart,Jend
      IR2=I2-1
      DRQ=ZERO
      IF(IR1.EQ.IR2)DRQ=ONE
      IDX=IDX+1
!
! One-Electron Integrals.
! Kinetic and overlap.
      DNA=P75/AS
      DNB=P75/BS
!
      CQ=CAB(IR2)
      OOQ=S1*CQ
      DOOQA=DS1A*CQ
      DOOQB=DS1B*CQ
      TT=CQ+CQ
      KOQ=TT*S12*S1
      DKOQA=TT*(DS12A*S1+S12*DS1A)
      DKOQB=TT*(DS12B*S1+S12*DS1B)
      OO=OOQ
      TKOO=KOQ+KOO*OOQ
      DNB=DNB+PT5/BS
      DOOA=DOOQA
      DOOB=DOOQB
      DTA=DKOQA+DKOOA*OOQ+KOO*DOOQA
      DTB=DKOQB+DKOOB*OOQ+KOO*DOOQB
      DMS=EDM(IDX)*CCAB*AX1
      DDS=EODN(IDX)*CCAB*AX1
!
      TXX=TXX+TKOO*DMS
!
! VNE.
      V=ZERO
      DVA=ZERO
      DVB=ZERO
      do Katom=1,Natoms
      IF(CARTESIAN(Katom)%Atomic_number.GT.0) then
      TA=CARTESIAN(Katom)%X-PAB(1)
      TB=CARTESIAN(Katom)%Y-PAB(2)
      TC=CARTESIAN(Katom)%Z-PAB(3)
      DFQ=-(TA*TA+TB*TB+TC*TC)
      DFQA=DFQ+TWO*(TA*DPA(1)+TB*DPA(2)+TC*DPA(3))*EPAB
      DFQB=DFQ+TWO*(TA*DPB(1)+TB*DPB(2)+TC*DPB(3))*EPAB
      FQ0=FF0(Katom)
      FQ1=FF1(Katom)
      FQ2=FF2(Katom)
      FQ3=FF3(Katom)
!
      LOO=FQ0
      DLOOA=FQ1*DFQA
      DLOOB=FQ1*DFQB
!
      IF(IR2.EQ.1)CPQ=CARTESIAN(Katom)%X-PAB(IR2)
      IF(IR2.EQ.2)CPQ=CARTESIAN(Katom)%Y-PAB(IR2)
      IF(IR2.EQ.3)CPQ=CARTESIAN(Katom)%Z-PAB(IR2)
      LOQ=CPQ*FQ1
      DLOQA=CPQ*FQ2*DFQA-DPA(IR2)*FQ1
      DLOQB=CPQ*FQ2*DFQB-DPB(IR2)*FQ1
      V=V+LOQ+LOO*OOQ
      DVA=DLOQA+DLOOA*OOQ+LOO*DOOQA+DVA
      DVB=DLOQB+DLOOB*OOQ+LOO*DOOQB+DVB

      end if ! CARTESIAN(Katom)%Atomic_number
      end do ! Katom

      VNEXX=VNEXX+V*DMS
      TA=DE12A+DNA
      TB=DE12B+DNB
      DEXX1(Igauss)=DEXX1(Igauss)+(DTA+TKOO*TA+DVA+V*(TA+PT5*EPABI))*DMS+(DOOA+OO*TA)*DDS
      DEXX1(Jgauss)=DEXX1(Jgauss)+(DTB+TKOO*TB+DVB+V*(TB+PT5*EPABI))*DMS+(DOOB+OO*TB)*DDS
      DDC=(TKOO+V)*DMS+OO*DDS

      if(LAMAX.eq.1)then
        D1C1(Igauss)=D1C1(Igauss)+DDC/CCA
      else
        D1C2(Igauss)=D1C2(Igauss)+DDC/CCA
      end if
      if(LBMAX.eq.1)then
        D1C1(Jgauss)=D1C1(Jgauss)+DDC/CCB
      else
        D1C2(Jgauss)=D1C2(Jgauss)+DDC/CCB
      end if
!
! Derivatives of one-electron integrals.
      do IF1=1,2 ! (A'|B) + (A|B')
      MNN=3*(Iatom-1)
      IF(IF1.EQ.2)then
        MNN=3*(Jatom-1)
      end if

      do NN=1,3 ! X,Y,Z
! Kinetic and overlap derivatives.
      CN=CAB(NN)
      DRN=ZERO
      DQN=ZERO
      IF(NN.EQ.IR1)DRN=ONE
      IF(NN.EQ.IR2)DQN=ONE
      DM1=ZERO
      DM2=ZERO
      IF(IF1.EQ.1)DM1=ONE
      IF(IF1.EQ.2)DM2=ONE
      CD12=DM1-DM2
      DE12=-TWO*CN*CD12*S12
      DKOO=-FOUR*CN*CD12*S12*S12
!
      DOOQ=S1*DQN*CD12
      DKOQ=TWO*DQN*CD12*S12*S1
      DOO=DE12*OO+DOOQ
      DT=DE12*TKOO+DKOQ+DKOO*OOQ+KOO*DOOQ
!
! VNE Derivatives.
      DVNE=ZERO
      PDM=-(S1*DM1+S2*DM2)

      do Katom=1,Natoms
      IF(CARTESIAN(Katom)%Atomic_number.le.0)cycle
      JNN=3*(Katom-1)+NN
      IF(NN.EQ.1)CPN=CARTESIAN(Katom)%X-PAB(NN)
      IF(NN.EQ.2)CPN=CARTESIAN(Katom)%Y-PAB(NN)
      IF(NN.EQ.3)CPN=CARTESIAN(Katom)%Z-PAB(NN)
      FQ0=FF0(Katom)
      FQ1=FF1(Katom)
      FQ2=FF2(Katom)
      FQ3=FF3(Katom)
!
      LOO=FQ0
      DLNOO=T1*CPN*FQ1
      DLOO=DLNOO*PDM
!
      IF(IR2.EQ.1)CPQ=CARTESIAN(Katom)%X-PAB(IR2)
      IF(IR2.EQ.2)CPQ=CARTESIAN(Katom)%Y-PAB(IR2)
      IF(IR2.EQ.3)CPQ=CARTESIAN(Katom)%Z-PAB(IR2)
      LOQ=CPQ*FQ1
      DLNOQ=T1*CPQ*CPN*FQ2+DQN*FQ1
      DLOQ=DLNOQ*PDM
      DVNE=DVNE+DLOO*OOQ+LOO*DOOQ+DLOQ
      IF(IF1.eq.1)then
        DVHF=DLNOO*OOQ+DLNOQ
        gradient(JNN)%NEOPR=gradient(JNN)%NEOPR+DVHF*DMS
      end if
      end do ! Katom

      gradient(MNN+NN)%NEWFN=gradient(MNN+NN)%NEWFN+(DE12*V+DVNE)*DMS
      gradient(MNN+NN)%WFN=gradient(MNN+NN)%WFN+DT*DMS
      gradient(MNN+NN)%COEFF=gradient(MNN+NN)%COEFF+DOO*DDS
      end do ! NN
      end do ! IF1
!
      end do ! I2
      end do ! I1

! End of routine G1E_SP
      call PRG_manager ('exit', 'G1E_SP', 'UTILITY')
      return
      end subroutine G1E_SP
      subroutine G1E_PP
!***********************************************************************
!     Date last modified: Dec. 8, 2005                     Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: Get addresses for printing of basis set gradients.  *
!***********************************************************************

      implicit none

! Begin
      call PRG_manager ('enter', 'G1E_PP', 'UTILITY')

! Loop over S, PX, PY and PZ functions.
      IDX=0
      do I1=Istart,Iend
      IR1=I1-1
      do I2=Jstart,Jend
      IR2=I2-1
      DRQ=ZERO
      IF(IR1.EQ.IR2)DRQ=ONE
      IDX=IDX+1
!
! One-Electron Integrals.
! Kinetic and overlap.
      DNA=P75/AS
      DNB=P75/BS
!
      CR=CAB(IR1)
      ORO=-S2*CR
      DOROA=-DS2A*CR
      DOROB=-DS2B*CR
      TT=-(CR+CR)
      KRO=TT*S12*S2
      DKROA=TT*(DS12A*S2+S12*DS2A)
      DKROB=TT*(DS12B*S2+S12*DS2B)
      DKOQA=TT*(DS12A*S2+S12*DS1A) ! new
      DKOQB=TT*(DS12B*S2+S12*DS1B) ! new
      ORQ=PT5*DRQ*EPABI-S1*S2*CR*CQ
      DNA=DNA+PT5/AS
      DNB=DNB+PT5/BS
      TT=-PT5*DRQ*EPABI*EPABI
      TA=DS1A*S2+S1*DS2A
      TB=DS1B*S2+S1*DS2B
      DORQA=TT-TA*CR*CQ
      DORQB=TT-TB*CR*CQ
      KRQ=S1*S2*DRQ
      DKRQA=TA*DRQ
      DKRQB=TB*DRQ
      OO=ORQ
      TKOO=KRQ+KRO*OOQ+KOQ*ORO+KOO*ORQ
      DOOA=DORQA
      DOOB=DORQB
      DTA=DKRQA+DKROA*OOQ+KRO*DOOQA+DKOQA*ORO+KOQ*DOROA+DKOOA*ORQ+KOO*DORQA
      DTB=DKRQB+DKROB*OOQ+KRO*DOOQB+DKOQB*ORO+KOQ*DOROB+DKOOB*ORQ+KOO*DORQB
      DMS=EDM(IDX)*CCAB*AX1
      DDS=EODN(IDX)*CCAB*AX1

      TXX=TXX+TKOO*DMS
!
! VNE.
      V=ZERO
      DVA=ZERO
      DVB=ZERO

      do Katom=1,Natoms
        IF(CARTESIAN(Katom)%Atomic_number.le.0)cycle
        TA=CARTESIAN(Katom)%X-PAB(1)
        TB=CARTESIAN(Katom)%Y-PAB(2)
        TC=CARTESIAN(Katom)%Z-PAB(3)
        DFQ=-(TA*TA+TB*TB+TC*TC)
        DFQA=DFQ+TWO*(TA*DPA(1)+TB*DPA(2)+TC*DPA(3))*EPAB
        DFQB=DFQ+TWO*(TA*DPB(1)+TB*DPB(2)+TC*DPB(3))*EPAB
        FQ0=FF0(Katom)
        FQ1=FF1(Katom)
        FQ2=FF2(Katom)
        FQ3=FF3(Katom)
!
        LOO=FQ0
        DLOOA=FQ1*DFQA
        DLOOB=FQ1*DFQB
!
        IF(IR1.EQ.1)CPR=CARTESIAN(Katom)%X-PAB(IR1)
        IF(IR1.EQ.2)CPR=CARTESIAN(Katom)%Y-PAB(IR1)
        IF(IR1.EQ.3)CPR=CARTESIAN(Katom)%Z-PAB(IR1)
        IF(IR2.EQ.1)CPQ=CARTESIAN(Katom)%X-PAB(IR2) ! new
        IF(IR2.EQ.2)CPQ=CARTESIAN(Katom)%Y-PAB(IR2) ! new
        IF(IR2.EQ.3)CPQ=CARTESIAN(Katom)%Z-PAB(IR2) ! new
        LRO=CPR*FQ1
        DLROA=CPR*FQ2*DFQA-DPA(IR1)*FQ1
        DLROB=CPR*FQ2*DFQB-DPB(IR1)*FQ1
        DLOQA=CPQ*FQ2*DFQA-DPA(IR2)*FQ1 ! new
        DLOQB=CPQ*FQ2*DFQB-DPB(IR2)*FQ1 ! new

        LRQ=CPR*CPQ*FQ2+DRQ*FQ1*T1I
        TT=CPR*CPQ*FQ3+DRQ*FQ2*T1I
        TA=-DRQ*FQ1*T1I*EPABI
        DLRQA=TT*DFQA-(DPA(IR1)*CPQ+CPR*DPA(IR2))*FQ2+TA
        DLRQB=TT*DFQB-(DPB(IR1)*CPQ+CPR*DPB(IR2))*FQ2+TA
        V=V+LRQ+LRO*OOQ+LOQ*ORO+LOO*ORQ
        DVA=DLRQA+DLROA*OOQ+LRO*DOOQA+DLOQA*ORO+LOQ*DOROA+DLOOA*ORQ+LOO*DORQA+DVA
        DVB=DLRQB+DLROB*OOQ+LRO*DOOQB+DLOQB*ORO+LOQ*DOROB+DLOOB*ORQ+LOO*DORQB+DVB
      end do ! Katom

      VNEXX=VNEXX+V*DMS
      TA=DE12A+DNA
      TB=DE12B+DNB
      DEXX1(Igauss)=DEXX1(Igauss)+(DTA+TKOO*TA+DVA+V*(TA+PT5*EPABI))*DMS+(DOOA+OO*TA)*DDS
      DEXX1(Jgauss)=DEXX1(Jgauss)+(DTB+TKOO*TB+DVB+V*(TB+PT5*EPABI))*DMS+(DOOB+OO*TB)*DDS
      DDC=(TKOO+V)*DMS+OO*DDS

      if(LAMAX.eq.1)then
        D1C1(Igauss)=D1C1(Igauss)+DDC/CCA
      else
        D1C2(Igauss)=D1C2(Igauss)+DDC/CCA
      end if
      if(LBMAX.eq.1)then
        D1C1(Jgauss)=D1C1(Jgauss)+DDC/CCB
      else
        D1C2(Jgauss)=D1C2(Jgauss)+DDC/CCB
      end if
!
! Derivatives of one-electron integrals.
      do IF1=1,2 ! (A'|B) + (A|B')
      MNN=3*(Iatom-1)
      IF(IF1.EQ.2)then
        MNN=3*(Jatom-1)
      end if

      do NN=1,3 ! X,Y,Z
! Kinetic and overlap derivatives.
      CN=CAB(NN)
      DRN=ZERO
      DQN=ZERO
      IF(NN.EQ.IR1)DRN=ONE
      IF(NN.EQ.IR2)DQN=ONE
      DM1=ZERO
      DM2=ZERO
      IF(IF1.EQ.1)DM1=ONE
      IF(IF1.EQ.2)DM2=ONE
      CD12=DM1-DM2
      DE12=-TWO*CN*CD12*S12
      DKOO=-FOUR*CN*CD12*S12*S12
!
      DORQ=-S1*S2*(DRN*CQ+DQN*CR)*CD12
      DOO=DE12*ORQ+DORQ
      DT=DE12*TKOO+DKRO*OOQ+KRO*DOOQ+DKOQ*ORO+KOQ*DORO+DKOO*ORQ+KOO*DORQ
!
! VNE Derivatives.
      DVNE=ZERO
      PDM=-(S1*DM1+S2*DM2)

      do Katom=1,Natoms
        IF(CARTESIAN(Katom)%Atomic_number.LE.0)cycle
        JNN=3*(Katom-1)+NN
        IF(NN.EQ.1)CPN=CARTESIAN(Katom)%X-PAB(NN)
        IF(NN.EQ.2)CPN=CARTESIAN(Katom)%Y-PAB(NN)
        IF(NN.EQ.3)CPN=CARTESIAN(Katom)%Z-PAB(NN)
        IF(IR1.EQ.1)CPR=CARTESIAN(Katom)%X-PAB(IR1) ! new
        IF(IR1.EQ.2)CPR=CARTESIAN(Katom)%Y-PAB(IR1) ! new
        IF(IR1.EQ.3)CPR=CARTESIAN(Katom)%Z-PAB(IR1) ! new
        IF(IR2.EQ.1)CPQ=CARTESIAN(Katom)%X-PAB(IR2) ! new
        IF(IR2.EQ.2)CPQ=CARTESIAN(Katom)%Y-PAB(IR2) ! new
        IF(IR2.EQ.3)CPQ=CARTESIAN(Katom)%Z-PAB(IR2) ! new
        FQ0=FF0(Katom)
        FQ1=FF1(Katom)
        FQ2=FF2(Katom)
        FQ3=FF3(Katom)
!
        LOO=FQ0
        DLNOO=T1*CPN*FQ1
        DLOO=DLNOO*PDM
!
!NOTE: CPR and CPQ were not defined!!!!!!!!!!!!!!!!!!!!!
        LRQ=CPR*CPQ*FQ2+DRQ*FQ1*T1I
        DLNRQ=T1*CPN*CPR*CPQ*FQ3+(DRN*CPQ+DQN*CPR+DRQ*CPN)*FQ2
        DLRQ=DLNRQ*PDM
        DVNE=DVNE+DLOO*ORQ+LOO*DORQ+DLRO*OOQ+LRO*DOOQ+DLOQ*ORO+LOQ*DORO+DLRQ
        IF(IF1.eq.1)then
          DVHF=DLNOO*ORQ+DLNOQ*ORO+DLNRO*OOQ+DLNRQ
          gradient(JNN)%NEOPR=gradient(JNN)%NEOPR+DVHF*DMS
        end if
      end do ! Katom

      gradient(MNN+NN)%NEWFN=gradient(MNN+NN)%NEWFN+(DE12*V+DVNE)*DMS
      gradient(MNN+NN)%WFN=gradient(MNN+NN)%WFN+DT*DMS
      gradient(MNN+NN)%COEFF=gradient(MNN+NN)%COEFF+DOO*DDS
      end do ! NN
      end do ! IF1
!
      end do ! I2
      end do ! I1

! End of routine G1E_PP
      call PRG_manager ('exit', 'G1E_PP', 'UTILITY')
      return
      end subroutine G1E_PP
      end subroutine G1E_BASIS
      subroutine PRT_GBASIS
!***********************************************************************
!     Date last modified: Dec. 8, 2005                     Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: Get addresses for printing of basis set gradients.  *
!***********************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE type_basis_set
      USE gradients

      implicit none
!
! Input arrays:
!
! Local scalars:
      integer I,IA,IX,IXOLD,J,K,L,N1,N2
      integer :: Iatmshl,Ifrst,Ilast,IGBGN,IGEND
      double precision T,TT
!
! Begin:
      call PRG_manager ('enter', 'PRT_GBASIS', 'UTILITY')
!
! Set print flags: 0 for none, 1 for totals only, 2 for components.
! Watch for derivative-free optimization methods.
! Do not print basis set gradient if not calculated.
   90 WRITE(UNIout,1040)
      call flush(6)
      K=0
      IXOLD=0
      do I=1,Basis%NSHELLs
      IGBGN=Basis%shell(I)%EXPBGN
      IGEND=Basis%shell(I)%EXPEND
      Ifrst= Basis%shell(I)%frstSHL
      Ilast= Basis%shell(I)%lastSHL
      do Iatmshl=Ifrst,Ilast
          IX=Basis%atmshl(Iatmshl)%ATMLST
!         IX=SHLXYZ(I)
          IF(IX.NE.IXOLD) then
            IXOLD=IXOLD+1
            IF(IX.NE.IXOLD) then
! Found dummy atom(s).
              J=IX-1
              do L=IXOLD,J
                K=K+1
!               WRITE(UNIout,1050)K,Dummy
              end do
              IXOLD=IX
            end if
            IA=CARTESIAN(IX)%Atomic_number
            K=K+1
            IF(IA.LT.0)then
!             WRITE(UNIout,1050)K,Floating
            else
!             WRITE(UNIout,1050)K,ELSYMB(IA)
            end if
          end if
!         WRITE(UNIout,1060)SHLNLB(I),SHLTLB(I)
!
        T=ZERO
        do L=IGBGN,IGEND
! Exponent derivatives in log space (accumulate scale factor derivative).
          WRITE(UNIout,1070)DEXX1(L),DEXX2(L),D1C1(L),D2C1(L),D1C2(L),D2C2(L)
      call flush(6)
          DEXX1(L)=Basis%gaussian(L)%exp*(DEXX1(L)+DEXX2(L))
          T=T+DEXX1(L)
! Coefficient derivatives: CONTRC (for raw coefficients).
          TT=ZERO
!         IF(C1S(L).NE.ZERO)TT=(D1C1(L)+D2C1(L))*Basis%gaussian(L)%contrc/C1S(L)
          TT=D1C1(L)+D2C1(L)
          D1C1(L)=TT
          TT=ZERO
!         IF(C2S(L).NE.ZERO)TT=(D1C2(L)+D2C2(L))*Basis%gaussian(L)%contrc/C2S(L)
          TT=D1C2(L)+D2C2(L)
          D1C2(L)=TT
        end do !L
        GBASCL(I)=T+T
      end do ! Iatmshl
      end do ! Ishell
!
! Print reduced basis set derivatives.
      WRITE(UNIout,1120)
      K=0
      IXOLD=0
      do I=1,Basis%NSHELLs
      IGBGN=Basis%shell(I)%EXPBGN
      IGEND=Basis%shell(I)%EXPEND
      Ifrst= Basis%shell(I)%frstSHL
      Ilast= Basis%shell(I)%lastSHL
      do Iatmshl=Ifrst,Ilast
      IX=Basis%atmshl(Iatmshl)%ATMLST
!     IX=SHLXYZ(I)
      IF(IX.NE.IXOLD) then
      IXOLD=IXOLD+1
      IF(IX.NE.IXOLD) then
! Dummy atom(s).
      J=IX-1
      do L=IXOLD,J
      K=K+1
!     WRITE(UNIout,1050)K,Dummy
      end do
      IXOLD=IX
      end if
      IA=CARTESIAN(IX)%Atomic_number
      K=K+1
      IF(IA.LT.0)then
!       WRITE(UNIout,1050)K,Floating
      else
!       WRITE(UNIout,1050)K,ELSYMB(IA)
      end if
      end if
      WRITE(UNIout,1130)Basis%shell(I)%shltlb,GBASCL(I)
      do L=IGBGN,IGEND
      IF(Basis%shell(I)%Xtype.EQ.0)D1C2(L)=ZERO
      WRITE(UNIout,1140)DEXX1(L),D1C1(L),D1C2(L)
      end do !L
      end do ! Iatmshl
      end do ! Ishell
!
! End of routine PRT_GBASIS
      call PRG_manager ('exit', 'PRT_GBASIS', 'UTILITY')
      return
!
 1040 FORMAT('BASIS SET DERIVATIVE COMPONENTS (FOR NORMALIZED COEFS)'//'0CENTER   SHELL',8X, &
             'EXX (1E)',7X,'EXX (2E)',11X,'C1 (1E)',8X,'C1 (2E)',12X,'C2 (1E)',8X,'C2 (2E)')
 1050 FORMAT('0',I2,A2)
 1060 FORMAT(10X,A2,A4)
 1070 FORMAT(14X,3(1PD19.6,D15.6))
 1120 FORMAT('BASIS SET DERIVATIVES (IN LOG SPACE FOR SCALE FACTORS',' AND EXPONENTS, OR FOR RAW COEFFICIENTS)'// &
             'CENTER   type','    SCALE FACTOR',4X,'EXPONENT',8X,'S COEF',9X,'P COEF'/)
 1130 FORMAT(10X,A4,F13.6)
 1140 FORMAT(31X,1PD14.7,2D15.7)
!
      END
