      subroutine BLD_shape_and_size
!***********************************************************************
!     Date last modified: July 28, 2009                    Version 1.0 *
!     Author: Joshua Hollett                                           *
!     description: Calculate the origin invariant shape and size of    *
!                  a molecule.                                         *
!***********************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE INT_objects
      USE type_molecule

      implicit none

! Local scalars:
      integer :: Iatom
      double precision :: N_e,AN
      double precision :: DipoleX, DipoleY, DipoleZ
      double precision :: TDipoleX, TDipoleY, TDipoleZ
      double precision :: SecondXX, SecondYY,  SecondZZ
      double precision :: SecondXY, SecondXZ,  SecondYZ
      double precision, allocatable :: shape(:,:),eigvec(:,:),eigval(:)
! Local routines:
      double precision traclo

! Begin:
      call PRG_manager ('enter', 'BLD_shape_and_size', 'UTILITY')

      call GET_object ('INT', 'MOMENT1', 'AO')
      call GET_object ('QM', 'CMO', Wavefunction)

      DipoleX=-traclo(DipoleX_AO, PM0, NBasis, MATlen)
      DipoleY=-traclo(DipoleY_AO, PM0, NBasis, MATlen)
      DipoleZ=-traclo(DipoleZ_AO, PM0, NBasis, MATlen)

! Print electronic dipole moment
      write(UNIout,'(a)')'Electronic Dipole Moment'
      write(UNIout,'(a)')'    <x>         <y>         <z>        (a.u.)'
      write(UNIout,'(3F12.6,a/)')DipoleX,DipoleY,DipoleZ,' '
!
!     Add in nuclear contribution.
      TDipoleX=DipoleX
      TDipoleY=DipoleY
      TDipoleZ=DipoleZ
      do Iatom=1,Natoms
      if(CARTESIAN(Iatom)%Atomic_number.le.0)cycle
      AN=CARTESIAN(Iatom)%Atomic_number
      TDipoleX=TDipoleX+CARTESIAN(Iatom)%X*AN
      TDipoleY=TDipoleY+CARTESIAN(Iatom)%Y*AN
      TDipoleZ=TDipoleZ+CARTESIAN(Iatom)%Z*AN
      end do ! Iatom

! Print electronic dipole moment
      write(UNIout,'(a)')'Electronic Dipole Moment (including nuclear contribution)'
      write(UNIout,'(a)')'    <x>         <y>         <z>'
      write(UNIout,'(3F12.6,a/)')TDipoleX,TDipoleY,TDipoleZ,' (a.u.)'
      write(UNIout,'(3F12.6,a/)')TDipoleX*DCONST,TDipoleY*DCONST,TDipoleZ*DCONST,' (Debye)'
!      
! Calculate electronic second moment
      SecondXX=traclo(SecMomXX_AO, PM0, NBasis, MATlen)
      SecondXY=traclo(SecMomXY_AO, PM0, NBasis, MATlen)
      SecondXZ=traclo(SecMomXZ_AO, PM0, NBasis, MATlen)
      SecondYY=traclo(SecMomYY_AO, PM0, NBasis, MATlen)
      SecondYZ=traclo(SecMomYZ_AO, PM0, NBasis, MATlen)
      SecondZZ=traclo(SecMomZZ_AO, PM0, NBasis, MATlen)
!
! Print electronic second moment tensor
      write(UNIout,'(a)')'Electronic Second Moment Tensor (a.u.)'
      write(UNIout,'(a)')'       X           Y           Z'
      write(UNIout,'(a,3F12.6)')'X ',SecondXX, SecondXY, SecondXZ  
      write(UNIout,'(a,3F12.6)')'Y ',SecondXY, SecondYY, SecondYZ
      write(UNIout,'(a,3F12.6,a/)')'Z ',SecondXZ, SecondYZ, SecondZZ,' '
!
! Calculate origin invariant electronic second moment tensor
      allocate(shape(3,3),eigvec(3,3),eigval(3))
      N_e=dble(Nelectrons)
!
      shape(1,1)=SecondXX-DipoleX**2/Nelectrons
      shape(2,1)=SecondXY-DipoleX*DipoleY/Nelectrons
      shape(1,2)=shape(2,1)
      shape(2,2)=SecondYY-DipoleY**2/Nelectrons
      shape(3,2)=SecondYZ-DipoleY*DipoleZ/Nelectrons
      shape(2,3)=shape(3,2)
      shape(3,1)=SecondXZ-DipoleX*DipoleZ/Nelectrons
      shape(1,3)=shape(3,1)
      shape(3,3)=SecondZZ-DipoleZ**2/Nelectrons
! Print origin invariant electronic second moment tensor
      write(UNIout,'(a)')'Origin Invariant Electronic Second Moment Tensor (a.u.)'
      write(UNIout,'(a)')'       X           Y           Z'
      write(UNIout,'(a,3F12.6)')'X ',shape(1,1), shape(1,2), shape(1,3)  
      write(UNIout,'(a,3F12.6)')'Y ',shape(2,1), shape(2,2), shape(2,3)  
      write(UNIout,'(a,3F12.6,a/)')'Z ',shape(3,1), shape(3,2), shape(3,3),' '
!
! Diagonalize 
      call MATRIX_diagonalize (shape, eigvec, eigval, 3, 2, .false.)
! Print eigenvalues and eigenvectors of shape tensor
      write(UNIout,'(a)')'Eigenvectors and Eigenvalues of Shape Tensor (a.u.)'
      write(UNIout,'(a/)')'                    <xx>        <yy>        <zz>      '
      write(UNIout,'(a,3F12.6,a/)') 'eigenvalues     ', eigval(1), eigval(2), eigval(3),' '
      write(UNIout,'(a,3F12.6)') 'eigenvectors  x ', eigvec(1,1), eigvec(1,2), eigvec(1,3)
      write(UNIout,'(a,3F12.6)') '              y ', eigvec(2,1), eigvec(2,2), eigvec(2,3)
      write(UNIout,'(a,3F12.6,a/)') '              z ', eigvec(3,1), eigvec(3,2), eigvec(3,3),' '
!
! Print value of R
      write(UNIout,'(a,F12.6,a/)')'Theoretical measure of molecular size, R:', &
                 (eigval(1)*eigval(2)*eigval(3))**(1.0D0/6.0D0),' (a.u.)'
! Save the values
      SizeShape%XX=dsqrt(eigval(1))
      SizeShape%YY=dsqrt(eigval(2))
      SizeShape%ZZ=dsqrt(eigval(3))
      SizeShape%Size=(eigval(1)*eigval(2)*eigval(3))**(1.0D0/6.0D0)
! 
      deallocate(shape, eigvec, eigval)

      call PRG_manager ('exit', 'BLD_shape_and_size', 'UTILITY')
!
      end subroutine BLD_shape_and_size
