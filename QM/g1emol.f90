      subroutine G1EMOL
!*****************************************************************************************************************
!     Date last modified: February 18, 2000                                                         Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Evaluation of the one-electron integral contribution to the Hartree-Fock first derivative.    *
!                  initial version from QCPE gaussIAN 80                                                         *
!*****************************************************************************************************************
! Modules:
      USE program_constants
      USE type_molecule
      USE type_basis_set
      USE gradients
      USE QM_objects

      implicit none
!
! Local scalars:
      integer :: Iao,Iatmshl,Iatom,ICC,Ifrst,Ilast,Ishell,Irange,Istart,Iend,LAMAX,Iaos,Igauss,IGBGN,IGEND,Itype
      integer :: Jao,Jatmshl,Jatom,JCC,Jfrst,Jlast,Jshell,Jrange,Jstart,Jend,LBMAX,Jaos,Jgauss,JGBGN,JGEND,Jtype
!
      integer I,IA,IAIND,II,IJ,IJX,IJY,IJZ,ILOC,IMJ,INTC,IX,IXYZ,IY,IZ,IZERO,I1,J,JJ,JND,JZ,K,LAX,LAXM,LAXP,LAY,LAYM,LAYP, &
              LAZ,LAZM,LAZP,LBX,LBY,LBZ,LIM1DS,LPMAX,MU,MUNU,NU,NZERO
      integer IP,IPI
      integer IfrstAO,IlastAO,JfrstAO,JlastAO
      double precision AIAB,ABX,ABY,ABZ,ARABSQ,ARG,AS,ASXA,ASYA,ASZA,BIAB,BS,COEF,DABX,DABY,DABZ,DTEMP,DTEMPS, &
                       DXK,DXS,DXV1,DXV2,DYK,DYS,DYV1,DYV2,DZK,DZS,DZV1,DZV2,EP,EPI,EPIO2,FXC,FXT,FXVNE2,FXVNE3, &
                       FYC,FYT,FYVNE2,FYVNE3,FZC,FZT,FZVNE2,FZVNE3,PCX,PCY,PCZ,PEXP,PX,PY,PZ,RPCSQ, &
                       R12,S,STERM,T,TEMP,TEMP1,TEMP2,TWOAS,TWOASQ,TWOP,TWOPI,TWOPT2,VTEMP,XAP,XBP,XYIP, &
                       XZIP,YAP,YBP,YZIP,ZAP,ZBP,ZCONST,ZT,ZTEMP
      double precision ATEMP,AXCNST,AYCNST,AZCNST,CTEMP,CXCNST,CYCNST,CZCNST,T2,XIP1T,XIP2T,YIP1T,YIP2T,ZIP1T,ZIP2T
      double precision :: XA,XB,XC,YA,YB,YC,ZA,ZB,ZC,RABSQ
!
! Local arrays:
      integer IJend(4),IJST(4),INDIX(20),INDIY(20),INDIZ(20),INDJX(20),INDJY(20),INDJZ(20)
      double precision A(45),CA(20),CB(20),CCX(192),CCY(192),CCZ(192),D12(100),D12S(100),D12C(100),D12SC(100),SX(32), &
                       SY(32),SZ(32),S1C(6),TWOCX(9),TWOCY(9),TWOCZ(9),XINT(3),XIP(80),XIP1(80),XIP2(80), &
                       YIP(80),YIP1(80),YIP2(80),ZIP(80),ZIP1(80),ZIP2(80)
      double precision XYZA1(4),XYZA2(4),XYZA3(4)
      double precision :: TP(7),WP(7)
!
! Local parameters:
      double precision CUT1
      parameter (CUT1=-100.0D0)
!
      DATA XINT/1.0D0,2.0D0,3.0D0/,INDJX/1,2,1,1,3,1,1,2,2,1,4,1,1,2,3,3,2,1,1,2/, &
           INDJY/1,1,2,1,1,3,1,2,1,2,1,4,1,3,2,1,1,2,3,2/,INDJZ/1,1,1,2,1,1,3,1,2,2,1,1,4,1,1,2,3,3,2,2/
!
! Begin:
      call PRG_manager ('enter', 'G1EMOL', 'UTILITY')
!
      TWOPI=pi_val+pi_val
!
! Fill INDIX.
      do I=1,20
        INDIX(I)=4*(INDJX(I)-1)
        INDIY(I)=4*(INDJY(I)-1)
        INDIZ(I)=4*(INDJZ(I)-1)
      end do ! I
!
! Loop over Ishell.
      do Ishell=1,Basis%Nshells
      Itype=Basis%shell(Ishell)%Xtype
      LAMAX=Itype+1
      Istart=Basis%shell(Ishell)%Xstart
      Iend=Basis%shell(Ishell)%XEND
      IGBGN=Basis%shell(Ishell)%EXPBGN
      IGEND=Basis%shell(Ishell)%EXPEND
      Ifrst=Basis%shell(Ishell)%frstSHL
!
! Loop over Jshell.
      do Jshell=1,Ishell
      Jtype=Basis%shell(Jshell)%Xtype
      LBMAX=Jtype+1
      Jstart=Basis%shell(Jshell)%Xstart
      Jend=Basis%shell(Jshell)%XEND
      JGBGN=Basis%shell(Jshell)%EXPBGN
      JGEND=Basis%shell(Jshell)%EXPEND
      Jfrst=Basis%shell(Jshell)%frstSHL
!
! NOTE: These must be re-initialized everytime!!
      Ilast= Basis%shell(Ishell)%lastSHL
      Jlast= Basis%shell(Jshell)%lastSHL
!
      do Iatmshl=Ifrst,Ilast
        Iatom=Basis%atmshl(Iatmshl)%ATMLST
        XA=CARTESIAN(Iatom)%X
        YA=CARTESIAN(Iatom)%Y
        ZA=CARTESIAN(Iatom)%Z
        Iaos=Basis%atmshl(Iatmshl)%frstAO-1
        IF(Ishell.EQ.Jshell)Jlast=Iatmshl
      IfrstAO=Basis%atmshl(Iatmshl)%frstAO
      IlastAO=Basis%atmshl(Iatmshl)%lastAO

      do Jatmshl=Jfrst,Jlast
        Jatom=Basis%atmshl(Jatmshl)%ATMLST
        XB=CARTESIAN(Jatom)%X
        YB=CARTESIAN(Jatom)%Y
        ZB=CARTESIAN(Jatom)%Z
        Jaos=Basis%atmshl(Jatmshl)%frstAO-1
      JfrstAO=Basis%atmshl(Jatmshl)%frstAO
      JlastAO=Basis%atmshl(Jatmshl)%lastAO
!
      LPMAX=LAMAX+Jtype
      LIM1DS=(LPMAX+4)/2
      IMJ=Iatmshl-Jatmshl
      NZERO=(Itype+Jtype+1)/2+1
      ABX=XB-XA
      ABY=YB-YA
      ABZ=ZB-ZA
      RABSQ=ABX*ABX+ABY*ABY+ABZ*ABZ
!
! Pluck out density matrix pieces for this atmshl combination
! (Iatmshl,Jatmshl) into the temporary arrays D12 and D12S.
      INTC=0
      do MU=IfrstAO,IlastAO
      JND=JlastAO
      IF(IMJ.EQ.0)JND=MU
      do NU=JfrstAO,JND
      if(mu.gt.nu)then
        MUNU=MU*(MU-1)/2+NU
      else
        MUNU=NU*(NU-1)/2+MU
      end if
      INTC=INTC+1
      D12(INTC)=PM0(MUNU)
      D12S(INTC)=PEWD(MUNU)
      IF(MU.NE.NU)then
        D12(INTC)=D12(INTC)+D12(INTC)
        D12S(INTC)=D12S(INTC)+D12S(INTC)
      end if
      end do ! J
      end do ! I
!
      FXVNE2=ZERO
      FYVNE2=ZERO
      FZVNE2=ZERO
      FXVNE3=ZERO
      FYVNE3=ZERO
      FZVNE3=ZERO
      FXT=ZERO
      FYT=ZERO
      FZT=ZERO
      FXC=ZERO
      FYC=ZERO
      FZC=ZERO
!
! **********************************************************************
! Loop over primitive Gaussians.
      do Igauss=Basis%shell(Ishell)%EXPBGN,Basis%shell(Ishell)%EXPEND
      AS=Basis%gaussian(Igauss)%exp
      TWOASQ=TWO*AS*AS
      TWOAS=AS+AS
      ASXA=AS*XA
      ASYA=AS*YA
      ASZA=AS*ZA
      ARABSQ=AS*RABSQ
      call FILLCC (LAMAX, Basis%gaussian(Igauss)%CONTRC, CA)
      do Jgauss=Basis%shell(Jshell)%EXPBGN,Basis%shell(Jshell)%EXPEND
      BS=Basis%gaussian(Jgauss)%exp
      call FILLCC (LBMAX, Basis%gaussian(Jgauss)%CONTRC, CB)
      EP=AS+BS
      EPI=ONE/EP
      EPIO2=PT5*EPI
      TWOP=EP+EP
      ARG=-BS*ARABSQ*EPI
      PEXP=ZERO
      IF(ARG.GT.CUT1)PEXP=DEXP(ARG)
      ZTEMP=TWOPI*EPI*PEXP
      PX=(ASXA+BS*XB)*EPI
      PY=(ASYA+BS*YB)*EPI
      PZ=(ASZA+BS*ZB)*EPI
      XAP=PX-XA
      XBP=PX-XB
      YAP=PY-YA
      YBP=PY-YB
      ZAP=PZ-ZA
      ZBP=PZ-ZB
      call GETCC1 (CCX, XAP, XBP, LAMAX+3, LBMAX)
      call GETCC1 (CCY, YAP, YBP, LAMAX+3, LBMAX)
      call GETCC1 (CCZ, ZAP, ZBP, LAMAX+3, LBMAX)
      AIAB=AS*EPI
      BIAB=BS*EPI
      TEMP=(AS+AS)*BIAB
      DABX=ABX*TEMP
      DABY=ABY*TEMP
      DABZ=ABZ*TEMP
!
! Combine density matrix components with basis contraction coefficients and store in temporary arrays D12C and D12SC.
      INTC=0
      do I=Istart,Iend
      JND=Jend
      IF(IMJ.EQ.0)JND=I
      do J=Jstart,JND
      INTC=INTC+1
      COEF=CA(I)*CB(J)
      D12C(INTC)=D12(INTC)*COEF
      D12SC(INTC)=D12S(INTC)*COEF
      end do ! J
      end do ! I
!
! Loop over atoms.
      IAIND=-3
      do Iatom=1,Natoms
! Pick up coordinates of the current nuclear center.
      IAIND=IAIND+3
      IA=CARTESIAN(Iatom)%Atomic_number
      IF(IA.LE.0)GO TO 800
      XC=CARTESIAN(Iatom)%X
      YC=CARTESIAN(Iatom)%Y
      ZC=CARTESIAN(Iatom)%Z
      ZT=-ZTEMP*DBLE(IA)
      PCX=XC-PX
      PCY=YC-PY
      PCZ=ZC-PZ
      RPCSQ=PCX*PCX+PCY*PCY+PCZ*PCZ
      ARG=EP*RPCSQ
      call RPOLX (NZERO, ARG, TP, WP)
      call GETA1 (A, EPIO2, 0, LPMAX)
!
! Loop over zeroes of Rys polynomial.
! Note: The array element offset, IXYZ, has been reduced by 1 from the QCPE gaussIAN 80 version -
! This eliminates array elements as (illegal) subroutine arguments.
      IXYZ=-16
      do IZERO=1,NZERO
      TWOPT2=TWOP*TP(IZERO)
      ZCONST=ZT*WP(IZERO)
      call GET2C (TWOCX, PCX, ONE, A, TWOPT2, 0, LPMAX)
      call GET2C (TWOCY, PCY, ONE, A, TWOPT2, 0, LPMAX)
      call GET2C (TWOCZ, PCZ, ZCONST, A, TWOPT2, 0, LPMAX)
      IXYZ=IXYZ+16
      call GET3C (XIP, IXYZ, TWOCX, CCX, LAMAX, LBMAX)
      call GET3C (YIP, IXYZ, TWOCY, CCY, LAMAX, LBMAX)
      call GET3C (ZIP, IXYZ, TWOCZ, CCZ, LAMAX, LBMAX)
!      call DRVIP1 (XIP,YIP,ZIP,XIP1,YIP1,ZIP1,XIP2,YIP2,ZIP2,TWOPT2,IXYZ, LAMAX, LBMAX)
      T2=TWOPT2*AIAB
      AXCNST=DABX+PCX*T2
      AYCNST=DABY+PCY*T2
      AZCNST=DABZ+PCZ*T2
      CXCNST=-PCX*TWOPT2
      CYCNST=-PCY*TWOPT2
      CZCNST=-PCZ*TWOPT2
      XYZA1(2)=-BIAB-T2*EPIO2
      XYZA1(3)=XYZA1(2)+XYZA1(2)
      XYZA1(4)=XYZA1(3)+XYZA1(2)
      XYZA2(2)=AIAB-T2*EPIO2
      XYZA2(3)=XYZA2(2)+XYZA2(2)
      XYZA2(4)=XYZA2(3)+XYZA2(2)
      XYZA3(2)=TWOPT2*EPIO2
      XYZA3(3)=XYZA3(2)+XYZA3(2)
      XYZA3(4)=XYZA3(3)+XYZA3(2)
      do I=1,LAMAX
      IPI=(I-1)*4+IXYZ
      do J=1,LBMAX
      IP=IPI+J
      XIP1T=CXCNST*XIP(IP)
      YIP1T=CYCNST*YIP(IP)
      ZIP1T=CZCNST*ZIP(IP)
      XIP2T=AXCNST*XIP(IP)
      YIP2T=AYCNST*YIP(IP)
      ZIP2T=AZCNST*ZIP(IP)
      IF(I.NE.1)then
        K=IP-4
        ATEMP=XYZA1(I)
        CTEMP=XYZA3(I)
        XIP1T=XIP1T+CTEMP*XIP(K)
        YIP1T=YIP1T+CTEMP*YIP(K)
        ZIP1T=ZIP1T+CTEMP*ZIP(K)
        XIP2T=XIP2T+ATEMP*XIP(K)
        YIP2T=YIP2T+ATEMP*YIP(K)
        ZIP2T=ZIP2T+ATEMP*ZIP(K)
      end if
!
      IF(J.NE.1)then
        K=IP-1
        ATEMP=XYZA2(J)
        CTEMP=XYZA3(J)
        XIP1T=XIP1T+CTEMP*XIP(K)
        YIP1T=YIP1T+CTEMP*YIP(K)
        ZIP1T=ZIP1T+CTEMP*ZIP(K)
        XIP2T=XIP2T+ATEMP*XIP(K)
        YIP2T=YIP2T+ATEMP*YIP(K)
        ZIP2T=ZIP2T+ATEMP*ZIP(K)
      end if
!
      XIP1(IP)=XIP1T
      YIP1(IP)=YIP1T
      ZIP1(IP)=ZIP1T
      ZIP2(IP)=ZIP2T
      YIP2(IP)=YIP2T
      XIP2(IP)=XIP2T
      end do ! J
      end do ! I
!
      end do ! IZERO
!
! Loop over atomic orbitals.
      INTC=0
      do I=Istart,Iend
      IX=INDIX(I)-16
      IY=INDIY(I)-16
      IZ=INDIZ(I)-16
      JND=Jend
      IF(IMJ.EQ.0)JND=I
      do J=Jstart,JND
      IJX=INDJX(J)+IX
      IJY=INDJY(J)+IY
      IJZ=INDJZ(J)+IZ
      INTC=INTC+1
      DTEMP=D12C(INTC)
      VTEMP=ZERO
      DXV1=ZERO
      DYV1=ZERO
      DZV1=ZERO
      DZV2=ZERO
      DYV2=ZERO
      DXV2=ZERO
      do IZERO=1,NZERO
      IJX=IJX+16
      IJY=IJY+16
      IJZ=IJZ+16
      XYIP=XIP(IJX)*YIP(IJY)
      XZIP=XIP(IJX)*ZIP(IJZ)
      YZIP=YIP(IJY)*ZIP(IJZ)
      VTEMP=VTEMP+XYIP*ZIP(IJZ)
      DXV1=DXV1+XIP1(IJX)*YZIP
      DYV1=DYV1+YIP1(IJY)*XZIP
      DZV1=DZV1+ZIP1(IJZ)*XYIP
      DXV2=DXV2+XIP2(IJX)*YZIP
      DYV2=DYV2+YIP2(IJY)*XZIP
      DZV2=DZV2+ZIP2(IJZ)*XYIP
      end do ! IZERO
!
      VNEXX=VNEXX+VTEMP*DTEMP
      TEMP1=DXV1*DTEMP
      TEMP2=DXV2*DTEMP
      Gradient(IAIND+1)%NEOPR=Gradient(IAIND+1)%NEOPR+TEMP1
      FXVNE2=FXVNE2+TEMP2
      FXVNE3=FXVNE3-TEMP1-TEMP2
      TEMP1=DYV1*DTEMP
      TEMP2=DYV2*DTEMP
      Gradient(IAIND+2)%NEOPR=Gradient(IAIND+2)%NEOPR+TEMP1
      FYVNE2=FYVNE2+TEMP2
      FYVNE3=FYVNE3-TEMP1-TEMP2
      TEMP1=DZV1*DTEMP
      TEMP2=DZV2*DTEMP
      Gradient(IAIND+3)%NEOPR=Gradient(IAIND+3)%NEOPR+TEMP1
      FZVNE2=FZVNE2+TEMP2
      FZVNE3=FZVNE3-TEMP1-TEMP2
      end do ! J
      end do ! I
! End of AO loop.
!
  800 continue
      end do ! Iatom
! End of loop over atoms.
!
! Calculate the overlap and kinetic energy integrals.
      STERM=DSQRT(EPI*pi_val)
      call GET1CS (S1C, STERM, EPIO2, (LPMAX+4)/2)
      call GET2CS (SX, S1C, CCX, LAMAX+3, LBMAX)
      call GET2CS (SY, S1C, CCY, LAMAX+3, LBMAX)
      do I=1,LIM1DS
      S1C(I)=S1C(I)*PEXP
      end do ! I
      call GET2CS (SZ, S1C, CCZ, LAMAX+3, LBMAX)
!
! Begin loop over atomic orbitals for overlap and kinetic energy integrals.
      INTC=0
      do I=Istart,Iend
      LAX=INDJX(I)
      LAY=INDJY(I)
      LAZ=INDJZ(I)
      LAXP=LAX+1
      LAYP=LAY+1
      LAZP=LAZ+1
      LAXM=LAX-1
      LAYM=LAY-1
      LAZM=LAZ-1
      JND=Jend
      IF(IMJ.EQ.0)JND=I
      do J=Jstart,JND
      LBX=INDJX(J)
      LBY=INDJY(J)
      LBZ=INDJZ(J)
      INTC=INTC+1
      DTEMP=D12C(INTC)
      DTEMPS=D12SC(INTC)
      call RAWST (S,T,LAX,LAY,LAZ,LBX,LBY,LBZ,AS,TWOASQ,SX,SY,SZ)
      TXX=TXX+T*DTEMP
      call RAWST (S,T,LAXP,LAY,LAZ,LBX,LBY,LBZ,AS,TWOASQ,SX,SY,SZ)
      DXS=TWOAS*S
      DXK=TWOAS*T
      call RAWST (S,T,LAX,LAYP,LAZ,LBX,LBY,LBZ,AS,TWOASQ,SX,SY,SZ)
      DYS=TWOAS*S
      DYK=TWOAS*T
      call RAWST (S,T,LAX,LAY,LAZP,LBX,LBY,LBZ,AS,TWOASQ,SX,SY,SZ)
      DZS=TWOAS*S
      DZK=TWOAS*T
      IF(LAX.GT.1) then
      call RAWST (S,T,LAXM,LAY,LAZ,LBX,LBY,LBZ,AS,TWOASQ,SX,SY,SZ)
      DXS=DXS-XINT(LAXM)*S
      DXK=DXK-XINT(LAXM)*T
      end if
      IF (LAY.GT.1) then
      call RAWST (S,T,LAX,LAYM,LAZ,LBX,LBY,LBZ,AS,TWOASQ,SX,SY,SZ)
      DYS=DYS-XINT(LAYM)*S
      DYK=DYK-XINT(LAYM)*T
      end if
      IF(LAZ.GT.1) then
      call RAWST (S,T,LAX,LAY,LAZM,LBX,LBY,LBZ,AS,TWOASQ,SX,SY,SZ)
      DZS=DZS-XINT(LAZM)*S
      DZK=DZK-XINT(LAZM)*T
      end if
      FXT=FXT+DXK*DTEMP
      FYT=FYT+DYK*DTEMP
      FZT=FZT+DZK*DTEMP
      FXC=FXC+DXS*DTEMPS
      FYC=FYC+DYS*DTEMPS
      FZC=FZC+DZS*DTEMPS
      end do ! J
      end do ! I
!
      end do ! Jgauss
      end do ! Igauss
! End of loop over Gaussians.
!
      IJ=3*(Basis%atmshl(Iatmshl)%ATMLST-1)
      Gradient(IJ+1)%NEWFN=Gradient(IJ+1)%NEWFN+FXVNE2
      Gradient(IJ+2)%NEWFN=Gradient(IJ+2)%NEWFN+FYVNE2
      Gradient(IJ+3)%NEWFN=Gradient(IJ+3)%NEWFN+FZVNE2
      Gradient(IJ+1)%WFN=Gradient(IJ+1)%WFN+FXT
      Gradient(IJ+2)%WFN=Gradient(IJ+2)%WFN+FYT
      Gradient(IJ+3)%WFN=Gradient(IJ+3)%WFN+FZT
      Gradient(IJ+1)%COEFF=Gradient(IJ+1)%COEFF+FXC
      Gradient(IJ+2)%COEFF=Gradient(IJ+2)%COEFF+FYC
      Gradient(IJ+3)%COEFF=Gradient(IJ+3)%COEFF+FZC
      IJ=3*(Basis%atmshl(Jatmshl)%ATMLST-1)
      Gradient(IJ+1)%NEWFN=Gradient(IJ+1)%NEWFN+FXVNE3
      Gradient(IJ+2)%NEWFN=Gradient(IJ+2)%NEWFN+FYVNE3
      Gradient(IJ+3)%NEWFN=Gradient(IJ+3)%NEWFN+FZVNE3
      Gradient(IJ+1)%WFN=Gradient(IJ+1)%WFN-FXT
      Gradient(IJ+2)%WFN=Gradient(IJ+2)%WFN-FYT
      Gradient(IJ+3)%WFN=Gradient(IJ+3)%WFN-FZT
      Gradient(IJ+1)%COEFF=Gradient(IJ+1)%COEFF-FXC
      Gradient(IJ+2)%COEFF=Gradient(IJ+2)%COEFF-FYC
      Gradient(IJ+3)%COEFF=Gradient(IJ+3)%COEFF-FZC

      end do ! Jatmshl
      end do ! Iatmshl
! End of loop over atmshls.
      end do ! Jshell
      end do ! Ishell
! End of loop over shells
!
! End of routine G1EMOL
      call PRG_manager ('exit', 'G1EMOL', 'UTILITY')
      return
      END
      subroutine RAWST (S, &
                        T, &
                        LAX, &
                        LAY, &
                        LAZ, &
                        LBX, &
                        LBY, &
                        LBZ, &
                        AS, &
                        TWOASQ, &
                        SX, &
                        SY, &
                        SZ)
!***********************************************************************
!     Date last modified: July 06, 1992                    Version 1.0 *
!     Author:                                                          *
!     Description:  QCPE gaussIAN 80                                   *
!***********************************************************************
      implicit none
!
! Input scalars:
      integer LAX,LAY,LAZ,LBX,LBY,LBZ
      double precision AS,S,T,TWOASQ
!
! Input arrays:
      double precision SX(32),SY(32),SZ(32)
!
! Local scalars:
      integer IJX,IJY,IJZ
      double precision SXY,SXZ,SYZ
!
! Local array:
      double precision CNST(6)
!
      DATA CNST/0.0D0,0.0D0,1.0D0,3.0D0,6.0D0,10.0D0/
!
      IJX=4*(LAX-1)+LBX
      IJY=4*(LAY-1)+LBY
      IJZ=4*(LAZ-1)+LBZ
      SXY=SX(IJX)*SY(IJY)
      SXZ=SX(IJX)*SZ(IJZ)
      SYZ=SY(IJY)*SZ(IJZ)
      S=SXY*SZ(IJZ)
      T=DBLE(2*(LAX+LAY+LAZ)-3)*AS*S-TWOASQ*(SX(IJX+8)*SYZ+SY(IJY+8)*SXZ+SZ(IJZ+8)*SXY)
      IF(LAX.GT.2)T=T-CNST(LAX)*SX(IJX-8)*SYZ
      IF(LAY.GT.2)T=T-CNST(LAY)*SY(IJY-8)*SXZ
      IF(LAZ.GT.2)T=T-CNST(LAZ)*SZ(IJZ-8)*SXY
!
! End of routine RAWST
      return
      END
      subroutine GNUCLC
!****************************************************************************************************************************
!     Date last modified: February 18, 2000                                                                    Version 2.0  *
!     Author: R.A. Poirier                                                                                                  *
!     Description: Evaluation of the nuclear contribution to the first derivative.                                          *
!****************************************************************************************************************************
! Modules:
      USE type_molecule
      USE gradients

      implicit none
!
! Local scalars:
      integer I,II,IZ,I1,J,JJ,JZ
      double precision ABX,ABY,ABZ,FNN,R12
!
! Begin:
      call PRG_manager ('enter', 'GNUCLC', 'UTILITY')
!
! Calculate nuclear contribution.
      IF(Natoms.GT.1) then
      do I=2,Natoms
      IZ=CARTESIAN(I)%Atomic_number
      IF(IZ.GT.0) then
!      IF(IPSEUD(I).NE.0)IZ=NPSEUD(I)
      I1=I-1
      II=3*I1+1
      do J=1,I1
      JZ=CARTESIAN(J)%Atomic_number
      IF(JZ.GT.0) then
!      IF(IPSEUD(J).NE.0)JZ=NPSEUD(J)
      JJ=3*(J-1)+1
      ABX=CARTESIAN(I)%X-CARTESIAN(J)%X
      ABY=CARTESIAN(I)%Y-CARTESIAN(J)%Y
      ABZ=CARTESIAN(I)%Z-CARTESIAN(J)%Z
      R12=ABX**2+ABY**2+ABZ**2
      FNN=DBLE(IZ*JZ)/DSQRT(R12)
      VNNXX=VNNXX+FNN
      FNN=FNN/R12
      Gradient(II)%NUCLEAR=Gradient(II)%NUCLEAR-ABX*FNN
      Gradient(JJ)%NUCLEAR=Gradient(JJ)%NUCLEAR+ABX*FNN
      Gradient(II+1)%NUCLEAR=Gradient(II+1)%NUCLEAR-ABY*FNN
      Gradient(JJ+1)%NUCLEAR=Gradient(JJ+1)%NUCLEAR+ABY*FNN
      Gradient(II+2)%NUCLEAR=Gradient(II+2)%NUCLEAR-ABZ*FNN
      Gradient(JJ+2)%NUCLEAR=Gradient(JJ+2)%NUCLEAR+ABZ*FNN
      end if ! JZ.GT.0
      end do ! J
      end if ! IZ.GT.0
      end do ! I
!
      end if ! Natoms.GT.1
!
! End of routine GNUCLC
      call PRG_manager ('exit', 'GNUCLC', 'UTILITY')
      return
      END
