      subroutine RHFCLC
!***********************************************************************
!     Date last modified: April 18, 1997                  Version 1.0  *
!     Author: R.A. Poirier                                             *
!     Description: Perform closed shell SCF calculation (also Direct). *
!***********************************************************************
! Modules:
      USE program_files
      USE type_molecule
      USE type_basis_set
      USE QM_defaults
      USE QM_objects
      USE INT_objects
      USE ixtrapolate

      implicit none
!
! Local scalars:
      logical :: LFirst,LGaussian_guess
      double precision :: SCFACC_save

      type (basis_set), target :: FROM_basis ! FROM basis set
!
! Begin:
      call PRG_manager ('enter', 'RHFCLC', 'CMO%RHF')
!
      SCFACC_save=SCFACC
      LGaussian_guess=.false.
! Check to make sure this is a closed shell if not just print a warning (could be a guess only).
      if(Multiplicity.ne.1)write(UNIout,*)'WARNING> RHFCLC: Multiplicity is not equal 1'
      LFirst=.true.
      if(GUESS_from_basis(1:4).ne.'NONE')then
        call GET_basis_set (GUESS_from_basis, FROM_basis, .false.)
        Basis=>FROM_basis
        Nbasis=Basis%Nbasis
        MAtlen=Nbasis*(Nbasis+1)/2
        call RESET_basis_set
        call SCF_RHF (LFirst, LGaussian_guess)
!        if(LGaussian_guess)then
!          SCFACC_save=SCFACC
!          SCFACC=3.0D-04
!          LFirst=.true.
!          write(UNIout,'(a)')'WARNING> Rerunning SCF with gaussian guess'
!          call BLD_Gaussian_guess
!          call SCF_RHF (LFirst, LGaussian_guess)
!          SCFACC=SCFACC_save
!          LGaussian_guess=.false.
!        end if

        if(.not.converged)then ! Maximum iteration count exceeded without convergence?
          write(UNIout,'(a,i4,a)')'WARNING> SCF_RHF: SCF DID NOT CONVERGE'
          !call PRG_stop ('SCF_RHF> SCF DID NOT CONVERGE')
        end if

        call SET_MO_project
        call KILL_object ('INT', '1EINT', 'AO')
        call KILL_object ('INT', '2EINT', I2E_type)
        call I2E_FILE_CLOSE
        LI2E_exist=.false. ! does not belong here
!       call CLEAR_objects ! CANNOT do THIS!!!!!!!!!!!!!!!!!
        MUN_prtlev=MUN_prtlev+1
        Basis=>Current_basis
        Nbasis=Basis%Nbasis
        MAtlen=Nbasis*(Nbasis+1)/2
        call RESET_basis_set
        call KILL_object ('QM', 'CMO_GUESS', 'RHF') ! Must build guess for second SCF
      end if

      GUESS_from_basis(1:6)='NONE  '

      LFirst=.true.
      call SCF_RHF (LFirst, LGaussian_guess)
!      if(LGaussian_guess)then
!        LFirst=.true.
!        write(UNIout,'(a)')'WARNING> Rerunning SCF with gaussian guess'
!        call BLD_Gaussian_guess
!        SCFACC_save=SCFACC
!        SCFACC=3.0D-05
!        call SCF_RHF (LFirst, LGaussian_guess)
!        SCFACC=SCFACC_save
!        LGaussian_guess=.false.
!      end if ! LGaussian_guess

      if(.not.converged)then ! Maximum iteration count exceeded without convergence?
        write(UNIout,'(a,i4,a)')'WARNING> SCF_RHF: SCF DID NOT CONVERGE AFTER ',SCFITN,' ITERATIONS'
        !call PRG_stop ('SCF_RHF> SCF DID NOT CONVERGE')
      end if
!
! End of routine RHFCLC
      call PRG_manager ('exit', 'RHFCLC', 'CMO%RHF')
      return
      end subroutine RHFCLC
      subroutine SCF_RHF (LFirst, LGaussian_guess)
!**********************************************************************
!     Date last modified: April 18, 1997                  Version 1.0  *
!     Author: R.A. Poirier                                             *
!     Description: Perform closed shell Direct SCF calculation.        *
!***********************************************************************
! Modules:
      USE program_constants
      USE matrix_print
      USE type_basis_set
      USE QM_defaults
      USE QM_objects
      USE INT_objects
      USE ixtrapolate

      implicit none
!
! Input scalar:
      logical :: LFirst,LGaussian_guess
!
! Local scalars:
      integer :: JCYCLE
      integer :: Ibasis,Jbasis,IJ,IERROR,ILUMO
      double precision :: EHF0,EHF1
      double precision :: SCFCON             ! actual SCF convergence of density matrix
      double precision :: TEN
!
! Work arrays:
      double precision, dimension(:), allocatable :: SCRVEC
      double precision, dimension(:), allocatable, target :: DPME
      double precision, dimension(:,:), allocatable :: SMhalf
!
! RHF Objects:
!      type (molecular_orbital), save, target :: CMO_RHF
!
! Local function:
      double precision ETRACE
!
      parameter (TEN=10.0D0)
!
! Begin:
      call PRG_manager ('enter', 'SCF_RHF', 'UTILITY')
!
      if(.not.LGaussian_guess)then
        allocate (SMhalf(Nbasis,Nbasis))
        call ALLOC_SCF
        call GET_object ('INT', '1EINT', 'AO')
        call INI_SCF_RHF
        call GET_object ('INT', '2EINT', I2E_type)
        call BLD_SMhalf (OVRLAP, SMhalf, Nbasis)
      else
        write(UNIout,'(a)')'Restart the SCF with a Gaussian guess'
        call flush(UNIout)
      end if

     allocate (SCRVEC(1:Nbasis), DPME(1:MATlen), STAT=IERROR)
     SCFCON=TEN
!
! Build the first Fock matrix
      DGM=ZERO
      if(LDirect_SCF)then
        PM0=>PM0_RHF ! IDFCLC uses PM0, so must point to DPME
        call DG_direct   ! calls IDFCLC
        PM0=>PM0_RHF
      end if
      call BLD_GMatrix (PM0_RHF, DGM, MATlen)     ! Add contributions from integrals INCORE and ONDISK if any
      FockM(1:MATlen)=HCORE(1:MATlen)+PT5*DGM(1:MATlen) ! F = H+2J-K = H+dG
!
      if(LShift)then
        ILUMO=CMO_RHF%NoccMO+1
        do Ibasis=ILUMO,Nbasis
        do Jbasis=Ibasis,Nbasis
          IJ=Jbasis*(Jbasis-1)/2+Ibasis
          FockM(IJ)=FockM(IJ)+ShiftLevel
        end do ! Jbasis
        end do ! Ibasis
      end if
!
      if(LGMatrix_dump)then
        write(UNIout,'(a)')'Initial G Matrix:'
        call PRT_matrix (PT5*DGM, MATlen, Nbasis)
        write(UNIout,'(a)')'Initial Fock Matrix:'
        call PRT_matrix (FockM, MATlen, Nbasis)
      end if
!     call P_iteration
      call INI_Xpolation (PM0_RHF, MATlen)

      ENERGY_RHF%elec=PT5*ETRACE (HCORE, FockM, PM0_RHF, Nbasis, MATlen)
      ENERGY_RHF%HF=ENERGY_RHF%nuclear+ENERGY_RHF%elec
      ENERGY_RHF%total=ENERGY_RHF%HF
      EHF0=ENERGY_RHF%total

      JCYCLE=1
      if(MUN_prtlev.gt.0.and.LFirst)then
        IF(LDirect_SCF)write(UNIout,*)' ***** Using the Direct SCF *****'
        write(UNIout,'(/a,15x,a,F17.9,a/a,1PE12.4//10x,a,6x,a)') &
        'CLOSED SHELL SCF','Nuclear Repulsion Energy is',ENERGY_RHF%nuclear,' Hartrees', &
        'Convergence on Density Matrix Required to Exit is',SCFACC, &
        'CYCLE   ELECTRONIC ENERGY', 'TOTAL ENERGY    CONVERGENCE   EXTRAPOLATION'
         write(UNIout,'(a,i4,1x,2f20.9)')' SCF_CYCLE:',JCYCLE,ENERGY_RHF%elec,ENERGY_RHF%total
      end if

      DPME(1:MATlen)=PM0_RHF(1:MATlen) ! Initialize DeltaP to the guess density
!
! Start SCF cycling.
      converged=.false.
      do while (JCYCLE.le.SCFITN)
      JCYCLE=JCYCLE+1
!
!     call GET_RHF_MOs
      call DETlinW (OVRLAP, FockM, CMO_RHF%coeff, CMO_RHF%EIGVAL, MATlen, Nbasis, .true.)
!
      call DENBLD (CMO_RHF%coeff, CMO_RHF%occupancy, PM0_RHF, Nbasis, MATlen, CMO_RHF%NoccMO)
      PnM0=PM0_RHF
!      delta(1:MATlen)=DPME(1:MATlen)
      call Xpolation (SCFACC, SCFCON, Nbasis) ! call the extrapolation routine
      if(Converged)exit
!
      if(Lextrapolated)then
        PM0_RHF(1:MATlen)=extra(1:MATlen)
      end if
!
!     if(.not.converged)then ! Build a new Fock matrix
        DPME(1:MATlen)=delta(1:MATlen)
        DGM(1:MATlen)=ZERO
        if(LDirect_SCF)then
          PM0=>DPME ! IDFCLC uses PM0, so must point to DPME
          call DG_direct   ! calls IDFCLC
          PM0=>PM0_RHF
          call BLD_GMatrix (DPME, DGM, MATlen)     ! Process the integrals in core and on disk if any
          FockM(1:MATlen)=FockM(1:MATlen)+PT5*DGM(1:MATlen) ! F = H+2J-K = H+dG
        else
          call BLD_GMatrix (PM0_RHF, DGM, MATlen)     ! Process the integrals in core and on disk if any
          FockM(1:MATlen)=HCORE(1:MATlen)+PT5*DGM(1:MATlen) ! F = H+2J-K = H+dG
        end if
!     end if
!
      if(LShift)then
        do Ibasis=ILUMO,Nbasis
        do Jbasis=Ibasis,Nbasis
          IJ=Jbasis*(Jbasis-1)/2+Ibasis
          FockM(IJ)=FockM(IJ)+ShiftLevel
        end do ! Jbasis
        end do ! Ibasis
      end if
!
! Calculate the energy.
      ENERGY_RHF%elec=PT5*ETRACE (HCORE, FockM, PM0_RHF, Nbasis, MATlen)
      ENERGY_RHF%HF=ENERGY_RHF%nuclear+ENERGY_RHF%elec
      ENERGY_RHF%total=ENERGY_RHF%HF
!
! Print iteration results.
      IF(MUN_prtlev.gt.0)then
        if(Lextrapolated)then
          write(UNIout,'(a,i4,1x,2f20.9,20x,a8)')' SCF_CYCLE:',JCYCLE,ENERGY_RHF%elec,ENERGY_RHF%total,LABEL
        else
          write(UNIout,'(a,i4,1x,2f20.9,1PE14.5)')' SCF_CYCLE:',JCYCLE,ENERGY_RHF%elec,ENERGY_RHF%total,SCFCON
        end if
      end if
!
      EHF1=ENERGY_RHF%total
      if(EHF1.gt.ZERO)then ! Energy is positive!
        write(UNIout,'(a)')'The energy is positive, quit and use a gaussian guess'
        converged=.false.
        exit
      else if (dabs(EHF1-EHF0).gt.200.0D0)then ! E has increased/decreased by too much
        write(UNIout,'(a)')'The energy has increased/decreased significantly, quit and use a gaussian guess'
        converged=.false.
        exit
      end if
      EHF0=EHF1
!
      end do ! while SCF cycle

      if(converged)then
        call PHASE (CMO_RHF%coeff, Nbasis)
        call DENBLD (CMO_RHF%coeff, CMO_RHF%occupancy, PM0_RHF, Nbasis, MATlen, CMO_RHF%NoccMO)
        LSCF_converged=.true.
        IF(MUN_prtlev.GT.0)then
          write(UNIout,'(A,F17.6,A/)')'At termination total energy is',ENERGY_RHF%total,'  Hartrees'
        end if ! MUN_prtlev.GT.0
!
        CMOG%coeff=CMO_RHF%coeff ! Copy the coefficients so they form the next guess
        if(LGMatrix_dump)then
!         write(UNIout,'(a)')'S Matrix:'
!         call PRT_matrix (OVRLAP, MATlen, Nbasis)
!         write(UNIout,'(a)')'HCORE Matrix:'
!         call PRT_matrix (HCORE, MATlen, Nbasis)
          call BLD_GMatrix (PM0_RHF, DGM, MATlen)
          write(UNIout,'(a)')'G Matrix:'
          DGM=PT5*DGM
          call PRT_matrix (DGM, MATlen, Nbasis)
          FockM(1:MATlen)=HCORE(1:MATlen)+DGM(1:MATlen) ! F = H+2J-K = H+dG
          write(UNIout,'(a)')'Fock Matrix:'
          call PRT_matrix (FockM, MATlen, Nbasis)
          ENERGY_RHF%elec=PT5*ETRACE (HCORE, FockM, PM0_RHF, Nbasis, MATlen)
          ENERGY_RHF%HF=ENERGY_RHF%nuclear+ENERGY_RHF%elec
          ENERGY_RHF%total=ENERGY_RHF%HF
         write(UNIout,'(1x,2f20.9)')ENERGY_RHF%elec,ENERGY_RHF%total
        end if
      else
!        LGaussian_guess=.true.
      end if ! if(converged)
!
! Maximum iteration count exceeded without convergence?
      if(.not.converged.and..not.LFirst)then
        write(UNIout,'(a,i4,a)')'ERROR> SCF_RHF: SCF DID NOT CONVERGE AFTER ',SCFITN,' ITERATIONS'
        LSCF_converged=.false.
!         call OPT_failure
!         call OPT_terminate
!       else
        if(.not.LOptimization.and.LNCI)then
          write(UNIout,'(a,i4,a)')'WARNING> SCF DID NOT CONVERGE continue for NCI data'
        else
          call PRG_stop ('SCF_RHF> SCF DID NOT CONVERGE')
        end if
      end if

      call END_Xpolation
!
! Transform the p-orbitals to pur Px,Py and Pz
      if(Lpurifyp)then
        call purify_p
        call DENBLD (CMO_RHF%coeff, CMO_RHF%occupancy, PM0_RHF, Nbasis, MATlen, CMO_RHF%NoccMO)
        write(UNIout,'(a)')'P-orbitals have been transformed to pure Ps and the density matrix has been updated'
      end if
      if(.not.LGaussian_guess)deallocate (SMhalf)
      deallocate (SCRVEC, DPME)
!
! End of routine SCF_RHF
      call PRG_manager ('exit', 'SCF_RHF', 'UTILITY')
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine INI_SCF_RHF
!***********************************************************************
!     Date last modified: October 11, 2001                             *
!     Author: R.A. Poirier                                             *
!     Description: Initialize all RHF parameters.                      *
!***********************************************************************
! Modules:

      implicit none
!
! Local function:
      double precision GET_Enuclear
!
! Begin:
      call PRG_manager ('enter', 'INI_SCF_RHF', 'UTILITY')
!
! Object:
      if(.not.associated(CMO_RHF%coeff))then
        allocate (CMO_RHF%coeff(Nbasis,Nbasis), CMO_RHF%EIGVAL(Nbasis), CMO_RHF%occupancy(Nbasis), STAT=IERROR)
        if(IERROR.ne.0)then
          stop ' SCF_RHF> allocate failure: CMO...'
        end if
      else
        deallocate (CMO_RHF%coeff, CMO_RHF%EIGVAL, CMO_RHF%occupancy, STAT=IERROR)
        if(IERROR.ne.0)then
          stop ' SCF_RHF> deallocate failure: CMO...'
        end if
        allocate (CMO_RHF%coeff(Nbasis,Nbasis), CMO_RHF%EIGVAL(Nbasis), CMO_RHF%occupancy(Nbasis), STAT=IERROR)
        if(IERROR.ne.0)then
          stop ' SCF_RHF> Re-allocate failure: CMO...'
        end if
      end if

      if(.not.allocated(PM0_RHF))then
        allocate (PM0_RHF(MATlen), STAT=IERROR)
        if(IERROR.ne.0)then
          stop ' SCF_RHF> allocate failure: PMO...'
        end if
      else
        deallocate (PM0_RHF, STAT=IERROR)
        if(IERROR.ne.0)then
          stop ' SCF_RHF> deallocate failure: PMO...'
        end if
        allocate (PM0_RHF(MATlen), STAT=IERROR)
        if(IERROR.ne.0)then
          stop ' SCF_RHF> Re-allocate failure: PMO...'
        end if
      end if
!
! Work arrays
      allocate (SCRVEC(1:Nbasis), DPME(1:MATlen), STAT=IERROR)
      if(IERROR.ne.0)then
        write(UNIout,*)'INI_SCF_RHF> allocate failure: SCRVEC...'
        stop ' INI_SCF_RHF> allocate failure: SCRVEC...'
      end if

      if(.not.allocated(FockM))then
        allocate (FockM(1:MATlen), STAT=IERROR)
      else
        deallocate (FockM)
        allocate (FockM(1:MATlen), STAT=IERROR)
      end if
!
! Initialization:
      CMO => CMO_RHF
      PM0 => PM0_RHF
!
! Intialize the energies
      ENERGY=>ENERGY_RHF
      ENERGY%nuclear=GET_Enuclear()
      ENERGY%HF=ZERO
      ENERGY%elec=ZERO
      ENERGY%total=ZERO
      ENERGY%wavefunction='RHF'//Basis_name(1:len_trim(Basis_name))
!
! Initialize OCCVEC
      CMO_RHF%NoccMO=CMOG%NoccMO
      CMO_RHF%occupancy(1:CMO_RHF%NoccMO)=TWO
      CMO_RHF%occupancy(CMO_RHF%NoccMO+1:Nbasis)=ZERO
!
! Copy arrays:
      PM0_RHF(1:MATlen)=PM0G(1:MATlen)
      FockM(1:MATlen)=HCORE(1:MATlen)
!     if(Wavefunction(1:5).eq.'DandC')then ! For a Divide-and-Conquer calculation at G(STO-3G)
!       FockM(1:MATlen)=FockM(1:MATlen)+GMatrixP(1:MATlen)
!     end if
!
      Lextrapolated=.true.
!
! End of routine INI_SCF_RHF
      call PRG_manager ('exit', 'INI_SCF_RHF', 'UTILITY')
      return
      end subroutine INI_SCF_RHF
      subroutine END_SCF_RHF
!***********************************************************************
!     Date last modified: October 11, 2001                             *
!     Author: R.A. Poirier                                             *
!     Description: Initialize all RHF parameters.                      *
!***********************************************************************
! Modules:

      implicit none
!
! Deallocate the work arrays
      deallocate (SCRVEC, DPME)

      return
      end subroutine END_SCF_RHF
      subroutine P_iteration
!***********************************************************************
!     Date last modified: April 18, 1997                  Version 1.0  *
!     Author: R.A. Poirier                                             *
!     Description: Perform closed shell Direct SCF calculation.        *
!***********************************************************************
! Modules:

      implicit none
!
! Local scalars:
      integer :: Ibasis
      double precision :: eta
!
! Work arrays:
      double precision, dimension(:,:), allocatable :: FSQM
      double precision, dimension(:,:), allocatable :: PSQM
      double precision, dimension(:,:), allocatable :: QSQM
      double precision, dimension(:,:), allocatable :: SMhalf
!
      parameter (eta=0.00D0)
! Begin:
      call PRG_manager ('enter', 'P_iteration', 'UTILITY')
!
! Start SCF cycling.
       allocate (FSQM(Nbasis,Nbasis), PSQM(Nbasis,Nbasis), QSQM(Nbasis,Nbasis), SMhalf(Nbasis,Nbasis))
       call UPT_TO_SQS (PM0_RHF, MATlen, PSQM, Nbasis)
       call UPT_TO_SQS (FockM, MATlen, FSQM, Nbasis)
       call BLD_SMhalf (OVRLAP, SMhalf, Nbasis)
! Transform Fock matrix to orthogonal basis
       FSQM = matmul(SMhalf,matmul(FSQM,SMhalf))
       QSQM = -PSQM
       do Ibasis=1,Nbasis
         QSQM(Ibasis,Ibasis)=ONE+QSQM(Ibasis,Ibasis)
       end do
       PSQM = PSQM + eta*matmul(QSQM,matmul(FSQM,PSQM))
       QSQM = -PSQM
       do Ibasis=1,Nbasis
         QSQM(Ibasis,Ibasis)=ONE+QSQM(Ibasis,Ibasis)
       end do
       PSQM = PSQM + eta*matmul(PSQM,matmul(FSQM,QSQM))
       call SQS_TO_UPT (PSQM, Nbasis, PM0_RHF, MATlen)
!
! End of routine P_iteration
      call PRG_manager ('exit', 'P_iteration', 'UTILITY')
      return
      end subroutine P_iteration
      subroutine GET_RHF_MOs
!********************************************************************************************
!     Date last modified: October 24, 2019                                                  *
!     Author: R.A. Poirier                                                                  *
!********************************************************************************************
! Modules:

      implicit none
!
! Input scalars:
! Input arrays:
!
! Output arrays:
!
! Work arrays:
      double precision, dimension(:,:), allocatable :: FSQS
!
! Local scalars:
!
! Begin:
!      call PRG_manager ('enter', 'GET_RHF_MOs', 'UTILITY')
!
! Now get the MOs
      allocate (FSQS(Nbasis,Nbasis))
      call UPT_to_SQS (FockM, MATlen, FSQS, Nbasis)
      FSQS=matmul(matmul(SMhalf, FSQS), transpose(SMhalf))
      call MATRIX_diagonalize (FSQS, CMO_RHF%coeff, CMO_RHF%eigval, Nbasis, 2, .true.)   ! Better ?????
      CMO_RHF%coeff=matmul(SMhalf, CMO_RHF%coeff)
      deallocate (FSQS)
!
! End of routine GET_RHF_MOs
!      call PRG_manager ('exit', 'GET_RHF_MOs', 'UTILITY')
!     stop
      return
      end subroutine GET_RHF_MOs
      end subroutine SCF_RHF
! END CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine BLD_Gaussian_guess
!***********************************************************************
!     Date last modified: October 11, 2001                             *
!     Author: R.A. Poirier                                             *
!     Description: Initialize all RHF parameters.                      *
!***********************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE matrix_print
      USE QM_defaults
      USE QM_objects
      USE INT_objects

      implicit none
!
      integer :: UNIT_log
      logical :: Lerror
      double precision :: Nelec,TraceAB
!
! Local function:
      double precision :: ETRACE
!
      call GET_gaussian_guess (UNIT_log)
      call GaussianReader (UNIT_log, PM0_RHF, MATlen, Lerror) ! PM0 now points to PM_gaussian
      if(Lerror)then
!       call FILE_delete ('Gaussian_guess.com', Lerror)
!       call FILE_delete ('Gaussian_guess.log', Lerror)
        call PRG_stop ('Gaussian log file is empty (no gaussian code)')
      end if
      Nelec=TraceAB (PM0_RHF, OVRLAP, NBasis)
      write(UNIout,'(a,f20.9)')'TracePS = ',Nelec
      DGM=ZERO
      call BLD_GMatrix (PM0_RHF, DGM, MATlen)! Add contributions from integrals INCORE and ONDISK if any
      FockM(1:MATlen)=HCORE(1:MATlen)+PT5*DGM(1:MATlen) ! F = H+2J-K = H+dG
      call DETlinW (OVRLAP, FockM, CMOG%coeff, CMOG%EIGVAL, MATlen, Nbasis, .true.)
      ENERGY_RHF%elec=PT5*ETRACE (HCORE, FockM, PM0_RHF, Nbasis, MATlen)
      ENERGY_RHF%HF=ENERGY_RHF%nuclear+ENERGY_RHF%elec
      ENERGY_RHF%total=ENERGY_RHF%HF
      write(UNIout,'(a,2f20.9)')'Gaussian energy:',ENERGY_RHF%elec,ENERGY_RHF%total

!     call FILE_delete ('Gaussian_guess.log', Lerror)

      return
      end subroutine BLD_Gaussian_guess
      function TRACLO (A, B, N, MATlen)
!***********************************************************************
!     Date last modified: July 07, 1992                                *
!     Author: R.A. Poirier                                             *
!     Description: Trace of a product of symmetric matrices A and B    *
!                  stored in linear vectors.                           *
!***********************************************************************
! Modules:
      USE program_constants

      implicit none
!
! Input scalars:
      integer MATlen,N
!
! Input arrays:
      double precision A(MATlen),B(MATlen)
!
! Local scalars:
      integer I,J,K
      double precision TRACLO
!
! Begin:
      TRACLO=ZERO
      K=0
      do J=1,N
      do I=1,J
        K=K+1
        TRACLO=TRACLO+TWO*A(K)*B(K)
      end do !I
      TRACLO=TRACLO-A(K)*B(K)
      end do !J
!
! End of function TRACLO
      return
      end
      function ETRACE (A, B, C, N, MATlen)
!***********************************************************************
!     Date last modified: July 07, 1992                                *
!     Author:                                                          *
!     Description: Trace of a product of symmetric matrices A+B and C  *
!                  stored in linear vectors.                           *
!***********************************************************************
! Modules:
      USE program_constants

      implicit none
!
! Input scalars:
      integer MATlen,N
!
! Input arrays:
      double precision A(MATlen),B(MATlen),C(MATlen)
!
! Local scalars:
      integer I,J,K
      double precision ETRACE
!
! Begin:
      ETRACE=ZERO
      K=0
      do J=1,N
      do I=1,J
        K=K+1
        ETRACE=ETRACE+TWO*(A(K)+B(K))*C(K)
      end do !I
      ETRACE=ETRACE-(A(K)+B(K))*C(K)
      end do !J
!
! End of function ETRACE
      return
      end
      subroutine E_JandK
!*********************************************************************************
!     Date last modified: September 12, 2023                                     *
!     Author: R.A. Poirier and J.W. Hollett                                      *
!     Description: Calculate the different energy components: potential, kinetic,*
!     coulomb and exchange                                                       *
!*********************************************************************************
! Modules:
      USE program_constants
      USE QM_defaults
      USE QM_objects
      USE INT_objects
      USE type_basis_set
      USE type_molecule

      implicit none
!
! Local functions:
      double precision :: TRACLO
!
! Local arrays:
      double precision, allocatable, dimension(:) :: eriMO
      double precision, allocatable, dimension(:,:) :: J_MO, K_MO, Jfocc, Kfocc
      integer, allocatable, dimension(:) :: MO_occ
!
! Local scalars:
      integer :: aMO, bMO, Nocc
!
! Work arrays
!
! Begin:
      call PRG_manager ('enter', 'E_JandK', 'ENERGY_COMPONENTS%'//Wavefunction(1:len_trim(Wavefunction)))
!
      I2e_type='RAW'
      call GET_object ('QM', 'DENSITY_1MATRIX', Wavefunction) ! Causes creating of all required objects

! Object:
!
! Get two-electron integrals
      call GET_object ('CFT', '2EINT', 'OS')
!
      allocate(eriMO(Nbasis**4))
!
! Create list of all occupied orbitals
!      NAelectrons = (Nelectrons + Multiplicity - 1)/2
!      NBelectrons = Nelectrons - NAelectrons
!
      Nocc = CMO%NoccMO
      allocate(MO_occ(Nocc))
      do aMO = 1, Nocc
        MO_occ(aMO) = aMO
      end do
!
      call CFT_gen_TEItrans(MO_occ,Nocc,MO_occ,Nocc,MO_occ,Nocc,MO_occ,Nocc,&
                             CMO%coeff,Nbasis,eriAO,eriMO)
!
      allocate(J_MO(Nbasis,Nbasis),K_MO(Nbasis,Nbasis))
!
! HSC now includes open shell electrons
      call CFT_gen_BLD_J(MO_occ,Nocc,MO_occ,Nocc,eriMO,J_MO,Nbasis)
      call CFT_gen_BLD_K(MO_occ,Nocc,MO_occ,Nocc,eriMO,K_MO,Nbasis)
!
! Get ROHF coefficients 
      allocate(Jfocc(Nbasis,Nbasis),Kfocc(Nbasis,Nbasis))
      call BLD_occ_and_coupling(Jfocc,Kfocc)
!
      E_COUL_RHF = ZERO
      E_EXCH_RHF = ZERO
      do aMO = 1, Nocc
        do bMO = 1, Nocc
          E_COUL_RHF = E_COUL_RHF + Jfocc(aMO,bMO)*J_MO(aMO,bMO)
          E_EXCH_RHF = E_EXCH_RHF - Kfocc(aMO,bMO)*K_MO(aMO,bMO)
        end do ! bMO
      end do ! aMO
!
      E_V_RHF  = TRACLO (Vint, PM0, Nbasis, MATlen)
      E_T_RHF  = TRACLO (Tint, PM0, Nbasis, MATlen)
      E_TV_RHF = TRACLO (HCORE, PM0, Nbasis, MATlen)
      ENERGY_RHF%elec  = E_TV_RHF + E_COUL_RHF + E_EXCH_RHF
      ENERGY_RHF%HF    = ENERGY_RHF%elec+ENERGY_RHF%nuclear
      ENERGY_RHF%total = ENERGY_RHF%HF
      ENERGY => ENERGY_RHF
!
      call PRT_Energy_components
!
! End of routine E_JandK
      call PRG_manager ('exit', 'E_JandK', 'ENERGY_COMPONENTS%'//Wavefunction(1:len_trim(Wavefunction)))
      return
      end subroutine E_JandK
!
      subroutine PRT_Energy_components
!*********************************************************************************
!     Date last modified: November 13, 2002                                      *
!     Author: R.A. Poirier                                                       *
!     Description: Calculate the different energy components: potential, kinetic,*
!     coulomb and exchange                                                       *
!*********************************************************************************
! Modules:
      USE program_files
      USE QM_objects
      USE INT_objects

      implicit none
!
! Local scalars:
      double precision :: Virial,Vee
!
! Begin:
      call PRG_manager ('enter', 'PRT_Energy_components', 'UTILITY')
!
      write(UNIout,'(a)')'Energy components:'
      write(UNIout,'(a,12x,f20.9)')'Kinetic = ',E_T_RHF
      write(UNIout,'(a,10x,f20.9)')'Potential = ',E_V_RHF
      write(UNIout,'(a,f20.9)')'Kinetic + Potential = ',E_TV_RHF
      write(UNIout,'(a,2x,f20.9)')'Coulomb repulsion = ',E_COUL_RHF
      write(UNIout,'(a,11x,f20.9)')'Exchange = ',E_EXCH_RHF
      Vee=E_COUL_RHF+E_EXCH_RHF
      write(UNIout,'(a,3x,f20.9)')'Coulomb+Exchange = ',Vee
      write(UNIout,'(a,12x,f20.9)')'Nuclear = ',ENERGY_RHF%nuclear
      write(UNIout,'(a,3x,f20.9)')'Total electronic = ',ENERGY_RHF%elec
      write(UNIout,'(a,7x,f20.9/)')'Total energy = ',ENERGY_RHF%total
      Virial=-(E_V_RHF+E_COUL_RHF+E_EXCH_RHF+ENERGY_RHF%nuclear)/E_T_RHF
      write(UNIout,'(a,12x,F20.9)')'Virial = ',Virial
!
! End of routine PRT_Energy_components
      call PRG_manager ('exit', 'PRT_Energy_components', 'UTILITY')
      return
      end subroutine PRT_Energy_components
      subroutine BLD_JandK (Jcoul, Kexch)
!*****************************************************************************
!     Date last modified: July 31, 1998                                      *
!     Author: R.A. Poirier                                                   *
!     Description: Form the two-electron part of the Fock matrix, given the  *
!     density matrix and any or all two-electron integrals.                  *
!*****************************************************************************
! Modules:
      USE program_constants
      USE QM_defaults
      USE QM_objects
      USE INT_objects

      implicit none
!
! Input scalars:
!
! Input arrays:
      double precision :: Jcoul(MATlen),Kexch(MATlen)
!
! Local scalars:
      integer I,LENGTH
      logical EOF
!
! Begin:
      call PRG_manager ('enter', 'BLD_JandK', 'UTILITY')
!
      call GET_object ('INT', '2EINT', 'RAW')
!
! Initialize Jcoul/Kexch.
      Jcoul(1:MATlen)=ZERO
      Kexch(1:MATlen)=ZERO
!
      IF(LIJKL_ONDISK)then
        EOF=.FALSE.
        rewind IJKL_unit
        call READ_ijkl (IJKL_BUFFER, IJKL_MAX, IJKL_unit, LENGTH, EOF)
        do while (.not.EOF)
          call BLD_JKM_ijkl (PM0, IJKL_BUFFER, LENGTH)
          call READ_ijkl (IJKL_BUFFER, IJKL_MAX, IJKL_unit, LENGTH, EOF)
        end do
      end if ! ONDISK
!
      IF(LIIKL_ONDISK)then
        EOF=.FALSE.
        rewind IIKL_unit
        call READ_iikl (IIKL_BUFFER, IIKL_MAX, IIKL_unit, LENGTH, EOF)
        do while (.not.EOF)
          call BLD_JKM_iikl (PM0, IIKL_BUFFER, LENGTH)
          call READ_iikl (IIKL_BUFFER, IIKL_MAX, IIKL_unit, LENGTH, EOF)
        end do
      end if ! ONDISK
!
      IF(LIJKJ_ONDISK)then
        EOF=.FALSE.
        rewind IJKJ_unit
        call READ_ijkj (IJKJ_BUFFER, IJKJ_MAX, IJKJ_unit, LENGTH, EOF)
        do while (.not.EOF)
          call BLD_JKM_ijkj (PM0, IJKJ_BUFFER, LENGTH)
          call READ_ijkj (IJKJ_BUFFER, IJKJ_MAX, IJKJ_unit, LENGTH, EOF)
        end do
      end if ! ONDISK
!
      IF(LIJJL_ONDISK)then
        EOF=.FALSE.
        rewind IJJL_unit
        call READ_ijjl (IJJL_BUFFER, IJJL_MAX, IJJL_unit, LENGTH, EOF)
        do while (.not.EOF)
          call BLD_JKM_ijjl (PM0, IJJL_BUFFER, LENGTH)
          call READ_ijjl (IJJL_BUFFER, IJJL_MAX, IJJL_unit, LENGTH, EOF)
        end do
      end if ! ONDISK

! Now add contributions from INCORE arrays
      if(Lall_ONDISK)then
        call READ_iiii ! IIII, IIIL, IJJJ and IIKK integrals
      else ! Were kept INCORE
        call BLD_JKM_ijkl (PM0, IJKL_INCORE, IJKL_count)
        call BLD_JKM_iikl (PM0, IIKL_INCORE, IIKL_count)
        call BLD_JKM_ijkj (PM0, IJKJ_INCORE, IJKJ_count)
        call BLD_JKM_ijjl (PM0, IJJL_INCORE, IJJL_count)
      end if
        call BLD_JKM_iikk (PM0, IIKK_INCORE, IIKK_count)
        call BLD_JKM_ijjj (PM0, IJJJ_INCORE, IJJJ_count)
        call BLD_JKM_iiil (PM0, IIIL_INCORE, IIIL_count)
        call BLD_JKM_iiii (PM0, IIII_INCORE, IIII_count)
!
! end of routine BLD_JandK
      call PRG_manager ('exit', 'BLD_JandK', 'UTILITY')
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine BLD_JKM_ijkl (PM, &
                               IJKL_IN, &
                               LENGTH)
!***********************************************************************
!     Date last modified: July 07, 1992                                *
!     Author: F. Colonna and R.A. Poirier                              *
!     Description:  MUN Version.                                       *
!     Add IJKL contributions to Matrix G=P*(IJKL+IKJL+ILKJ).           *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer, intent(IN) :: LENGTH
!
! Input arrays:
      double precision PM(MATlen)
      type (INTS_2e_ijkl) IJKL_IN(LENGTH)
!
! Local scalars:
      integer I,JS,JT,Iao,Jao,Kao,Lao
      double precision A1,A2,A3,G1,G2,G3
!
! Begin:
      call PRG_manager ('enter', 'BLD_JKM_ijkl', 'UTILITY')
!
      IF(LENGTH.GT.0)then
      do I=1,LENGTH
      Iao=IJKL_IN(I)%I
      Jao=IJKL_IN(I)%J
      Kao=IJKL_IN(I)%K
      Lao=IJKL_IN(I)%L
      A1=IJKL_IN(I)%IJKL
      A2=IJKL_IN(I)%IKJL
      A3=IJKL_IN(I)%ILJK
      G1=(A2+A3)
      G2=(A1+A3)
      G3=(A1+A2)
!
      JS=Iao*(Iao-1)/2+Jao
      JT=Kao*(Kao-1)/2+Lao
      Jcoul(JS)=Jcoul(JS)+PM(JT)*A1
      Jcoul(JT)=Jcoul(JT)+PM(JS)*A1
      Kexch(JS)=Kexch(JS)+PM(JT)*G1
      Kexch(JT)=Kexch(JT)+PM(JS)*G1
      JS=Iao*(Iao-1)/2+Kao
      JT=Jao*(Jao-1)/2+Lao
      Jcoul(JS)=Jcoul(JS)+PM(JT)*A2
      Jcoul(JT)=Jcoul(JT)+PM(JS)*A2
      Kexch(JS)=Kexch(JS)+PM(JT)*G2
      Kexch(JT)=Kexch(JT)+PM(JS)*G2
      JS=Iao*(Iao-1)/2+Lao
      JT=Jao*(Jao-1)/2+Kao
      Jcoul(JS)=Jcoul(JS)+PM(JT)*A3
      Jcoul(JT)=Jcoul(JT)+PM(JS)*A3
      Kexch(JS)=Kexch(JS)+PM(JT)*G3
      Kexch(JT)=Kexch(JT)+PM(JS)*G3
      end do
      end if
!
! End of routine BLD_JKM_ijkl
      call PRG_manager ('exit', 'BLD_JKM_ijkl', 'UTILITY')
      return
      end subroutine BLD_JKM_ijkl
      subroutine BLD_JKM_iiii (PM, &
                               IIII_IN, &
                               LENGTH)
!***********************************************************************
!     Date last modified: December 16, 1997                            *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version.                                       *
!     Add IJKL contributions to Matrix G=P*(IJKL+IKJL).                *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer, intent(IN) :: LENGTH
!
! Input arrays:
      double precision PM(MATlen)
      type (ints_2e_iiii) IIII_IN(LENGTH)
!
! Local scalars:
      integer I,JS,JT,Iao
      double precision G1
!
! Begin:
      call PRG_manager ('enter', 'BLD_JKM_iiii', 'UTILITY')
!
      IF(LENGTH.GT.0)then
        do I=1,LENGTH
        Iao=IIII_IN(I)%I
        G1=IIII_IN(I)%iiii
        JS=Iao*(Iao+1)/2
        Jcoul(JS)=Jcoul(JS)+PM(JS)*G1*PT5
        Kexch(JS)=Kexch(JS)+PM(JS)*G1
        end do
      end if
!
! End of routine BLD_JKM_iiii
      call PRG_manager ('exit', 'BLD_JKM_iiii', 'UTILITY')
      return
      end subroutine BLD_JKM_iiii
      subroutine BLD_JKM_ijkj (PM, &
                               IJKJ_IN, &
                               LENGTH)
!***********************************************************************
!     Date last modified: December 16, 1997                            *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version.                                       *
!     Add IJKJ contributions to Matrix G=P*(IJKJ+IKJJ).                *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer, intent(IN) :: LENGTH
!
! Input arrays:
      double precision PM(MATlen)
      type (ints_2e_ijkj) IJKJ_IN(LENGTH)
!
! Local scalars:
      integer I,L,JS,JT,Iao,Jao,Kao
      double precision A1,A2,G1,G2
!
! Begin:
      call PRG_manager ('enter', 'BLD_JKM_ijkj', 'UTILITY')
!
      if(LENGTH.gt.0)then
      do I=1,LENGTH
        Iao=IJKJ_IN(I)%I
        Jao=IJKJ_IN(I)%J
        Kao=IJKJ_IN(I)%K
        A1=IJKJ_IN(I)%IJKJ
        A2=IJKJ_IN(I)%IKJJ
!
        JS=Iao*(Iao-1)/2+Jao
        JT=Kao*(Kao-1)/2+Jao
        Jcoul(JS)=Jcoul(JS)+PM(JT)*A1
        Jcoul(JT)=Jcoul(JT)+PM(JS)*A1
        Kexch(JS)=Kexch(JS)+PM(JT)*(A1+A2)
        Kexch(JT)=Kexch(JT)+PM(JS)*(A1+A2)
        JS=Jao*(Jao+1)/2
        JT=Iao*(Iao-1)/2+Kao
        Jcoul(JS)=Jcoul(JS)+PM(JT)*A2
        Jcoul(JT)=Jcoul(JT)+PM(JS)*A2*PT5
        Kexch(JS)=Kexch(JS)+PM(JT)*A1*TWO
        Kexch(JT)=Kexch(JT)+PM(JS)*A1
      end do
      end if ! LENGTH.gt.0
!
! End of routine BLD_JKM_ijkj
      call PRG_manager ('exit', 'BLD_JKM_ijkj', 'UTILITY')
      return
      end subroutine BLD_JKM_ijkj
      subroutine BLD_JKM_iikl (PM, &
                               IIKL_IN, &
                               LENGTH)
!***********************************************************************
!     Date last modified: December 16, 1997                            *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version.                                       *
!     Add IIKL contributions to Matrix G=P*(IIKL+IKJJ).                *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer, intent(IN) :: LENGTH
!
! Input arrays:
      double precision PM(MATlen)
      type (ints_2e_iikl) IIKL_IN(LENGTH)
!
! Local scalars:
      integer I,JS,JT,Iao,Jao,Kao
      double precision A1,A2
!
! Begin:
      call PRG_manager ('enter', 'BLD_JKM_iikl', 'UTILITY')
!
      if(LENGTH.gt.0)then
      do I=1,LENGTH
        Iao=IIKL_IN(I)%I
        Jao=IIKL_IN(I)%K
        Kao=IIKL_IN(I)%L
        A1=IIKL_IN(I)%iikl
        A2=IIKL_IN(I)%ikil
!
        JS=Iao*(Iao+1)/2
        JT=Jao*(Jao-1)/2+Kao
        Jcoul(JS)=Jcoul(JS)+PM(JT)*A1
        Jcoul(JT)=Jcoul(JT)+PM(JS)*A1*PT5
        Kexch(JS)=Kexch(JS)+PM(JT)*TWO*A2
        Kexch(JT)=Kexch(JT)+PM(JS)*A2
        JS=Iao*(Iao-1)/2+Jao
        JT=Iao*(Iao-1)/2+Kao
        Jcoul(JS)=Jcoul(JS)+PM(JT)*A2
        Jcoul(JT)=Jcoul(JT)+PM(JS)*A2
        Kexch(JS)=Kexch(JS)+PM(JT)*(A1+A2)
        Kexch(JT)=Kexch(JT)+PM(JS)*(A1+A2)
      end do
      end if ! LENGTH.gt.0
!
! End of routine BLD_JKM_iikl
      call PRG_manager ('exit', 'BLD_JKM_iikl', 'UTILITY')
      return
      end subroutine BLD_JKM_iikl
      subroutine BLD_JKM_iikk (PM, &
                               IIKK_IN, &
                               LENGTH)
!***********************************************************************
!     Date last modified: December 16, 1997                            *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version.                                       *
!     Add IIKK contributions to Matrix G=P*(IIKK+IKIK).                *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer, intent(IN) :: LENGTH
!
! Input arrays:
      double precision PM(MATlen)
      type (ints_2e_iikk) IIKK_IN(LENGTH)
!
! Local scalars:
      integer I,JS,JT,Iao,Jao
      double precision A1,A2
!
! Begin:
      call PRG_manager ('enter', 'BLD_JKM_iikk', 'UTILITY')
!
      if(LENGTH.gt.0)then
      do I=1,LENGTH
        Iao=IIKK_IN(I)%I
        Jao=IIKK_IN(I)%K
        A1=IIKK_IN(I)%iikk
        A2=IIKK_IN(I)%ikik

        JS=Iao*(Iao+1)/2
        JT=Jao*(Jao+1)/2
        Jcoul(JS)=Jcoul(JS)+PM(JT)*A1*PT5
        Jcoul(JT)=Jcoul(JT)+PM(JS)*A1*PT5
        Kexch(JS)=Kexch(JS)+PM(JT)*A2
        Kexch(JT)=Kexch(JT)+PM(JS)*A2
        JS=Iao*(Iao-1)/2+Jao
        Jcoul(JS)=Jcoul(JS)+PM(JS)*A2
        Kexch(JS)=Kexch(JS)+PM(JS)*(A1+A2)
      end do
      end if ! LENGTH.gt.0
!
! End of routine BLD_JKM_iikk
      call PRG_manager ('exit', 'BLD_JKM_iikk', 'UTILITY')
      return
      end subroutine BLD_JKM_iikk
      subroutine BLD_JKM_ijjj (PM, &
                               IJJJ_IN, &
                               LENGTH)
!***********************************************************************
!     Date last modified: December 16, 1997                            *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version.                                       *
!     Add IJKL contributions to Matrix G=P*(IJKL+IKJL).                *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer, intent(IN) :: LENGTH
!
! Input arrays:
      double precision PM(MATlen)
      type (ints_2e_ijjj) IJJJ_IN(LENGTH)
!
! Local scalars:
      integer I,JS,JT,Iao,Jao
      double precision G1
!
! Begin:
      call PRG_manager ('enter', 'BLD_JKM_ijjj', 'UTILITY')
!
      IF(LENGTH.GT.0)then
        do I=1,LENGTH
        Iao=IJJJ_IN(I)%I
        Jao=IJJJ_IN(I)%J
        G1=IJJJ_IN(I)%ijjj
        JS=Iao*(Iao-1)/2+Jao
        JT=Jao*(Jao+1)/2
        Jcoul(JS)=Jcoul(JS)+PM(JT)*G1*PT5
        Jcoul(JT)=Jcoul(JT)+PM(JS)*G1
        Kexch(JS)=Kexch(JS)+PM(JT)*G1
        Kexch(JT)=Kexch(JT)+PM(JS)*G1*TWO
        end do
      end if
!
! End of routine BLD_JKM_ijjj
      call PRG_manager ('exit', 'BLD_JKM_ijjj', 'UTILITY')
      return
      end subroutine BLD_JKM_ijjj
      subroutine BLD_JKM_iiil (PM, &
                               IIIL_IN, &
                               LENGTH)
!***********************************************************************
!     Date last modified: December 16, 1997                            *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version.                                       *
!     Add IJKL contributions to Matrix G=P*(IJKL+IKJL).                *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer, intent(IN) :: LENGTH
!
! Input arrays:
      double precision PM(MATlen)
      type (ints_2e_iiil) IIIL_IN(LENGTH)
!
! Local scalars:
      integer I,JS,JT,Iao,Jao
      double precision G1
!
! Begin:
      call PRG_manager ('enter', 'BLD_JKM_iiil', 'UTILITY')
!
      IF(LENGTH.GT.0)then
        do I=1,LENGTH
        Iao=IIIL_IN(I)%I
        Jao=IIIL_IN(I)%L
        G1=IIIL_IN(I)%iiil
        JS=Iao*(Iao+1)/2
        JT=Iao*(Iao-1)/2+Jao
        Jcoul(JS)=Jcoul(JS)+PM(JT)*G1
        Jcoul(JT)=Jcoul(JT)+PM(JS)*G1*PT5
        Kexch(JS)=Kexch(JS)+PM(JT)*G1*TWO
        Kexch(JT)=Kexch(JT)+PM(JS)*G1
        end do
      end if
!
! End of routine BLD_JKM_iiil
      call PRG_manager ('exit', 'BLD_JKM_iiil', 'UTILITY')
      return
      end subroutine BLD_JKM_iiil
      subroutine BLD_JKM_ijjl (PM, &
                               IJJL_IN, &
                               LENGTH)
!***********************************************************************
!     Date last modified: December 16, 1997                            *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version.                                       *
!     Add IJJL contributions to Matrix G=P*(IJJL+ILJJ).                *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
      integer, intent(IN) :: LENGTH
!
! Input arrays:
      double precision PM(MATlen)
      type (ints_2e_ijjl) IJJL_IN(LENGTH)
!
! Local scalars:
      integer I,JS,JT,Iao,Jao,Kao
      double precision A1,A2
!
! Begin:
      call PRG_manager ('enter', 'BLD_JKM_ijjl', 'UTILITY')
!
      if(LENGTH.gt.0)then
      do I=1,LENGTH
        Iao=IJJL_IN(I)%I
        Jao=IJJL_IN(I)%J
        Kao=IJJL_IN(I)%L
        A1=IJJL_IN(I)%ijjl
        A2=IJJL_IN(I)%iljj
!
        JS=Iao*(Iao-1)/2+Jao
        JT=Jao*(Jao-1)/2+Kao
        Jcoul(JS)=Jcoul(JS)+PM(JT)*A1
        Jcoul(JT)=Jcoul(JT)+PM(JS)*A1
        Kexch(JS)=Kexch(JS)+PM(JT)*(A1+A2)
        Kexch(JT)=Kexch(JT)+PM(JS)*(A1+A2)
        JS=Jao*(Jao+1)/2
        JT=Iao*(Iao-1)/2+Kao
        Jcoul(JS)=Jcoul(JS)+PM(JT)*A2
        Jcoul(JT)=Jcoul(JT)+PM(JS)*A2*PT5
        Kexch(JS)=Kexch(JS)+PM(JT)*A1*TWO
        Kexch(JT)=Kexch(JT)+PM(JS)*A1
      end do
      end if ! LENGTH.gt.0
!
! End of routine BLD_JKM_ijjl
      call PRG_manager ('exit', 'BLD_JKM_ijjl', 'UTILITY')
      return
      end subroutine BLD_JKM_ijjl
! END CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine BLD_JandK
      subroutine BLD_GMatrix (PMat, GMat, MATlen)
!************************************************************************
!     Date last modified: November 13, 2002                Version 1.0  *
!     Author: R.A. Poirier                                              *
!     Description:  Form the two-electron part of the Fock matrix, given*
!     the density matrix and any or all two-electron integrals.         *
!************************************************************************
! Modules:
      USE program_constants
      USE QM_defaults
      USE INT_objects

      implicit none
!
! Input scalars:
      integer :: MATlen
!
! Input arrays:
      double precision, dimension(MATlen) :: PMat,GMat
!
! Local scalars:
!
! Begin:
      call PRG_manager ('enter', 'BLD_GMatrix', 'UTILITY')
!
! Process integrals on disk, if any:
      if(Lall_INCORE)then
        call DG_INCORE
      else if(Lall_ONDISK)then
        call DG_ONDISK
      else
        call DG_INCORE_ONDISK
      end if
!
! End of routine BLD_GMatrix
      call PRG_manager ('exit', 'BLD_GMatrix', 'UTILITY')
      return
!
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine DG_INCORE
!************************************************************************
!     Date last modified: August 4, 1998                   Version 1.0  *
!     Author: R.A. Poirier                                              *
!     Description: Form the two-electron part of the Fock matrix, given *
!     the density matrix and the two-electron integrals ONDISK.         *
!************************************************************************
! Modules:

      if(integral_combinations)then ! integrals stored as combination
        call BLD_DGC_ijkl (IJKL_INCORE, IJKL_count)
        call BLD_DGC_iikl (IIKL_INCORE, IIKL_count)
        call BLD_DGC_ijkj (IJKJ_INCORE, IJKJ_count)
        call BLD_DGC_ijjl (IJJL_INCORE, IJJL_count)
        call BLD_DGC_iikk (IIKK_INCORE, IIKK_count)
      else ! raw integrals
        call BLD_DGR_ijkl (IJKL_INCORE, IJKL_count, PMat, GMat)
        call BLD_DGR_iikl (IIKL_INCORE, IIKL_count, PMat, GMat)
        call BLD_DGR_ijkj (IJKJ_INCORE, IJKJ_count, PMat, GMat)
        call BLD_DGR_ijjl (IJJL_INCORE, IJJL_count, PMat, GMat)
        call BLD_DGR_iikk (IIKK_INCORE, IIKK_count, PMat, GMat)
      end if ! integral_combinations
      call BLD_DGC_ijjj (IJJJ_INCORE, IJJJ_count)
      call BLD_DGC_iiil (IIIL_INCORE, IIIL_count)
      call BLD_DGC_iiii (IIII_INCORE, IIII_count)
!
! End of routine DG_INCORE
      call PRG_manager ('exit', 'DG_INCORE', 'UTILITY')
      return
      end subroutine DG_INCORE
      subroutine DG_ONDISK
!************************************************************************
!     Date last modified: August 4, 1998                   Version 1.0  *
!     Author: R.A. Poirier                                              *
!     Description: Form the two-electron part of the Fock matrix, given *
!     the density matrix and the two-electron integrals ONDISK.         *
!************************************************************************
! Modules:

      implicit none
!
! Input scalar:
!
! Input arrays:
!
! Local scalars:
      integer length
      logical EOF
!
! Begin:
      call PRG_manager ('enter', 'DG_ONDISK', 'UTILITY')
!
      EOF=.FALSE.
      rewind IJKL_unit
      call READ_ijkl (IJKL_BUFFER, IJKL_MAX, IJKL_unit, length, EOF)
      do while (.not.EOF)
      if(integral_combinations)then
        call BLD_DGC_ijkl (IJKL_BUFFER, length)
      else
        call BLD_DGR_ijkl (IJKL_BUFFER, length, PMat, GMat)
      end if
      call READ_ijkl (IJKL_BUFFER, IJKL_MAX, IJKL_unit, length, EOF)
      end do
!
      EOF=.FALSE.
      rewind IIKL_unit
      call READ_iikl (IIKL_BUFFER, IIKL_MAX, IIKL_unit, length, EOF)
      do while (.not.EOF)
      if(integral_combinations)then
        call BLD_DGC_iikl (IIKL_BUFFER, length)
      else
        call BLD_DGR_iikl (IIKL_BUFFER, length, PMat, GMat)
      end if
       call READ_iikl (IIKL_BUFFER, IIKL_MAX, IIKL_unit, length, EOF)
      end do
!
      EOF=.FALSE.
      rewind IJKJ_unit
      call READ_ijkj (IJKJ_BUFFER, IJKJ_MAX, IJKJ_unit, length, EOF)
      do while (.not.EOF)
      if(integral_combinations)then
        call BLD_DGC_ijkj (IJKJ_BUFFER, length)
      else
        call BLD_DGR_ijkj (IJKJ_BUFFER, length, PMat, GMat)
      end if
        call READ_ijkj (IJKJ_BUFFER, IJKJ_MAX, IJKJ_unit, length, EOF)
      end do
!
      EOF=.FALSE.
      rewind IJJL_unit
      call READ_ijjl (IJJL_BUFFER, IJJL_MAX, IJJL_unit, length, EOF)
      do while (.not.EOF)
      if(integral_combinations)then
        call BLD_DGC_ijjl (IJJL_BUFFER, length)
      else
        call BLD_DGR_ijjl (IJJL_BUFFER, length, PMat, GMat)
      end if
        call READ_ijjl (IJJL_BUFFER, IJJL_MAX, IJJL_unit, length, EOF)
      end do

      call READ_iiii ! IIII, IIIL, IJJJ and IIKK integrals
      if(integral_combinations)then ! integrals stored as combination
        call BLD_DGC_iikk (IIKK_INCORE, IIKK_count)
      else ! raw integrals
        call BLD_DGR_iikk (IIKK_INCORE, IIKK_count, PMat, GMat)
      end if ! integral_combinations
      call BLD_DGC_ijjj (IJJJ_INCORE, IJJJ_count)
      call BLD_DGC_iiil (IIIL_INCORE, IIIL_count)
      call BLD_DGC_iiii (IIII_INCORE, IIII_count)

!
! End of routine DG_ONDISK
      call PRG_manager ('exit', 'DG_ONDISK', 'UTILITY')
      return
      end subroutine DG_ONDISK
      subroutine DG_INCORE_ONDISK
!************************************************************************
!     Date last modified: August 4, 1998                   Version 1.0  *
!     Author: R.A. Poirier                                              *
!     Description: Form the two-electron part of the Fock matrix, given *
!     the density matrix and the two-electron integrals ONDISK.         *
!************************************************************************
! Modules:

      implicit none
!
! Input scalar:
!
! Input arrays:
!
! Local scalars:
      integer length
      logical EOF
!
! Begin:
      call PRG_manager ('enter', 'DG_INCORE_ONDISK', 'UTILITY')
!
! Process integrals INCORE first
      if(integral_combinations)then ! integrals stored as combination
        call BLD_DGC_ijkl (IJKL_INCORE, IJKL_count)
        call BLD_DGC_iikl (IIKL_INCORE, IIKL_count)
        call BLD_DGC_ijkj (IJKJ_INCORE, IJKJ_count)
        call BLD_DGC_ijjl (IJJL_INCORE, IJJL_count)
        call BLD_DGC_iikk (IIKK_INCORE, IIKK_count)
      else ! raw integrals
        call BLD_DGR_ijkl (IJKL_INCORE, IJKL_count, PMat, GMat)
        call BLD_DGR_iikl (IIKL_INCORE, IIKL_count, PMat, GMat)
        call BLD_DGR_ijkj (IJKJ_INCORE, IJKJ_count, PMat, GMat)
        call BLD_DGR_ijjl (IJJL_INCORE, IJJL_count, PMat, GMat)
        call BLD_DGR_iikk (IIKK_INCORE, IIKK_count, PMat, GMat)
      end if ! integral_transformation
!
      call BLD_DGC_ijjj (IJJJ_INCORE, IJJJ_count)
      call BLD_DGC_iiil (IIIL_INCORE, IIIL_count)
      call BLD_DGC_iiii (IIII_INCORE, IIII_count)
!
! Process integrals ONDISK if any
      IF(LIJKL_ONDISK)then
        EOF=.FALSE.
        rewind IJKL_unit
        call READ_ijkl (IJKL_BUFFER, IJKL_MAX, IJKL_unit, length, EOF)
        do while (.not.EOF)
        if(integral_combinations)then
          call BLD_DGC_ijkl (IJKL_BUFFER, length)
        else
          call BLD_DGR_ijkl (IJKL_BUFFER, length, PMat, GMat)
        end if
        call READ_ijkl (IJKL_BUFFER, IJKL_MAX, IJKL_unit, length, EOF)
        end do
      end if ! ONDISK
!
      IF(LIIKL_ONDISK)then
        EOF=.FALSE.
        rewind IIKL_unit
         call READ_iikl (IIKL_BUFFER, IIKL_MAX, IIKL_unit, length, EOF)
        do while (.not.EOF)
        if(integral_combinations)then
         call BLD_DGC_iikl (IIKL_BUFFER, length)
        else
           call BLD_DGR_iikl (IIKL_BUFFER, length, PMat, GMat)
        end if
         call READ_iikl (IIKL_BUFFER, IIKL_MAX, IIKL_unit, length, EOF)
        end do
      end if ! ONDISK
!
      IF(LIJKJ_ONDISK)then
        EOF=.FALSE.
        rewind IJKJ_unit
         call READ_ijkj (IJKJ_BUFFER, IJKJ_MAX, IJKJ_unit, length, EOF)
        do while (.not.EOF)
        if(integral_combinations)then
         call BLD_DGC_ijkj (IJKJ_BUFFER, length)
        else
         call BLD_DGR_ijkj (IJKJ_BUFFER, length, PMat, GMat)
        end if
         call READ_ijkj (IJKJ_BUFFER, IJKJ_MAX, IJKJ_unit, length, EOF)
        end do
      end if ! ONDISK
!
      IF(LIJJL_ONDISK)then
        EOF=.FALSE.
          rewind IJJL_unit
         call READ_ijjl (IJJL_BUFFER, IJJL_MAX, IJJL_unit, length, EOF)
        do while (.not.EOF)
        if(integral_combinations)then
         call BLD_DGC_ijjl (IJJL_BUFFER, length)
        else
         call BLD_DGR_ijjl (IJJL_BUFFER, length, PMat, GMat)
        end if
         call READ_ijjl (IJJL_BUFFER, IJJL_MAX, IJJL_unit, length, EOF)
        end do
      end if ! ONDISK
!
! End of routine DG_INCORE_ONDISK
      call PRG_manager ('exit', 'DG_INCORE_ONDISK', 'UTILITY')
      return
      end subroutine DG_INCORE_ONDISK
      subroutine BLD_DGC_ijkl (IJKL_IN, &
                               LENGTH)
!***********************************************************************
!     Date last modified: July 31, 1998                   Version 1.0  *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version.                                       *
!     Add IJKL contributions to Matrix G=P*(IJKL+IKJL+ILKJ).           *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
!
! Input arrays:
!
! Input scalars:
      integer, intent(IN) :: LENGTH
!
! Input arrays:
      type (INTS_2e_ijkl), dimension(:) :: IJKL_IN
!
! Local scalars:
      integer Iao,Jao,Kao,Lao
      integer I,JS,JT
      double precision A1,A2,A3
!
! Begin:
      call PRG_manager ('enter', 'BLD_DGC_ijkl', 'UTILITY')
!
      MATlen=size(PMat)

      if(LENGTH.gt.0)then
      do I=1,LENGTH
      Iao=IJKL_IN(I)%I
      Jao=IJKL_IN(I)%J
      Kao=IJKL_IN(I)%K
      Lao=IJKL_IN(I)%L
      A1=IJKL_IN(I)%IJKL
      A2=IJKL_IN(I)%IKJL
      A3=IJKL_IN(I)%ILJK
!
      JS=Iao*(Iao-1)/2+Jao
      JT=Kao*(Kao-1)/2+Lao

      GMat(JS)=GMat(JS)+PMat(JT)*A1
      GMat(JT)=GMat(JT)+PMat(JS)*A1
      JS=Iao*(Iao-1)/2+Kao
      JT=Jao*(Jao-1)/2+Lao
      GMat(JS)=GMat(JS)+PMat(JT)*A2
      GMat(JT)=GMat(JT)+PMat(JS)*A2
      JS=Iao*(Iao-1)/2+Lao
      JT=Jao*(Jao-1)/2+Kao
      GMat(JS)=GMat(JS)+PMat(JT)*A3
      GMat(JT)=GMat(JT)+PMat(JS)*A3
      end do
! Do the same with WRI_ijkl
      end if ! LENGTH.gt.0
!
! End of routine BLD_DGC_ijkl
      call PRG_manager ('exit', 'BLD_DGC_ijkl', 'UTILITY')
      return
      end subroutine BLD_DGC_ijkl
      subroutine BLD_DGC_iikl (IIKL_IN, &
                               LENGTH)
!***********************************************************************
!     Date last modified: July 31, 1998                   Version 1.0  *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version.                                       *
!     Add IIKL contributions to Matrix G=P*(IIKL+IKIL).                *
!                                                                      *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
!
! Input arrays:
!
! Input scalars:
      integer, intent(IN) :: LENGTH
!
! Input arrays:
      type (ints_2e_iikl) IIKL_IN(LENGTH)
!
! Local scalars:
      integer Iao,Jao,Kao
      integer I,JS,JT
!
! Begin:
      call PRG_manager ('enter', 'BLD_DGC_iikl', 'UTILITY')
!
      if(LENGTH.gt.0)then
      do I=1,LENGTH
        Iao=IIKL_IN(I)%I
        Jao=IIKL_IN(I)%K
        Kao=IIKL_IN(I)%L
!
        JS=Iao*(Iao+1)/2
        JT=Jao*(Jao-1)/2+Kao
        GMat(JS)=GMat(JS)+PMat(JT)*IIKL_IN(I)%iikl
        GMat(JT)=GMat(JT)+PMat(JS)*IIKL_IN(I)%iikl*PT5
        JS=Iao*(Iao-1)/2+Jao
        JT=Iao*(Iao-1)/2+Kao
        GMat(JS)=GMat(JS)+PMat(JT)*IIKL_IN(I)%ikil
        GMat(JT)=GMat(JT)+PMat(JS)*IIKL_IN(I)%ikil
      end do
      end if ! LENGTH.gt.0
!
! End of routine BLD_DGC_iikl
      call PRG_manager ('exit', 'BLD_DGC_iikl', 'UTILITY')
      return
      end subroutine BLD_DGC_iikl
      subroutine BLD_DGC_ijjj (IJJJ_IN, &
                               LENGTH)
!***********************************************************************
!     Date last modified: July 31, 1998                   Version 1.0  *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version.                                       *
!     Add IJKL contributions to Matrix G=P*(IJKL+IKJL).                *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
!
! Input arrays:
!
! Input scalars:
      integer, intent(IN) :: LENGTH
!
! Input arrays:
      type (ints_2e_ijjj) IJJJ_IN(LENGTH)
!
! Local scalars:
      integer Iao,Jao
      integer I,JS,JT
!
! Begin:
      call PRG_manager ('enter', 'BLD_DGC_ijjj', 'UTILITY')
!
      if(LENGTH.gt.0)then
        do I=1,LENGTH
        Iao=IJJJ_IN(I)%I
        Jao=IJJJ_IN(I)%J
        JS=Iao*(Iao-1)/2+Jao
        JT=Jao*(Jao+1)/2
        GMat(JS)=GMat(JS)+PMat(JT)*IJJJ_IN(I)%ijjj
        GMat(JT)=GMat(JT)+PMat(JS)*TWO*IJJJ_IN(I)%ijjj
        end do
      end if ! LENGTH.gt.0
!
! End of routine BLD_DGC_ijjj
      call PRG_manager ('exit', 'BLD_DGC_ijjj', 'UTILITY')
      return
      end subroutine BLD_DGC_ijjj
      subroutine BLD_DGC_iiil (IIIL_IN, &
                               LENGTH)
!***********************************************************************
!     Date last modified: July 31, 1998                   Version 1.0  *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version.                                       *
!     Add IJKL contributions to Matrix G=P*(IJKL+IKJL).                *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
!
! Input arrays:
!
! Input scalars:
      integer, intent(IN) :: LENGTH
!
! Input arrays:
      type (ints_2e_iiil) IIIL_IN(LENGTH)
!
! Local scalars:
      integer Iao,Jao
      integer I,JS,JT
!
! Begin:
      call PRG_manager ('enter', 'BLD_DGC_iiil', 'UTILITY')
!
      if(LENGTH.gt.0)then
        do I=1,LENGTH
        Iao=IIIL_IN(I)%I
        Jao=IIIL_IN(I)%L
        JS=Iao*(Iao+1)/2
        JT=Iao*(Iao-1)/2+Jao
        GMat(JS)=GMat(JS)+PMat(JT)*TWO*IIIL_IN(I)%iiil
        GMat(JT)=GMat(JT)+PMat(JS)*IIIL_IN(I)%iiil
        end do
      end if ! LENGTH.gt.0
!
! End of routine BLD_DGC_iiil
      call PRG_manager ('exit', 'BLD_DGC_iiil', 'UTILITY')
      return
      end subroutine BLD_DGC_iiil
      subroutine BLD_DGC_iiii (IIII_IN, &
                               LENGTH)
!***********************************************************************
!     Date last modified: July 31, 1998                   Version 1.0  *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version.                                       *
!     Add IJKL contributions to Matrix G=P*(IJKL+IKJL).                *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
!
! Input arrays:
!
! Input scalars:
      integer, intent(IN) :: LENGTH
!
! Input arrays:
      type (ints_2e_iiii) IIII_IN(LENGTH)
!
! Local scalars:
      integer :: I,Iao,JS
!
! Begin:
      call PRG_manager ('enter', 'BLD_DGC_iiii', 'UTILITY')
!
      if(LENGTH.gt.0)then
        do I=1,LENGTH
          Iao=IIII_IN(I)%I
          JS=Iao*(Iao+1)/2
          GMat(JS)=GMat(JS)+PMat(JS)*IIII_IN(I)%iiii
        end do
      end if ! LENGTH.gt.0
!
! End of routine BLD_DGC_iiii
      call PRG_manager ('exit', 'BLD_DGC_iiii', 'UTILITY')
      return
      end subroutine BLD_DGC_iiii
      subroutine BLD_DGC_ijkj (IJKJ_IN, &
                               LENGTH)
!***********************************************************************
!     Date last modified: July 31, 1998                   Version 1.0  *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version.                                       *
!     Add IJKJ contributions to Matrix G=P*(IJKJ+IKJJ).                *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
!
! Input arrays:
!
! Input scalars:
      integer, intent(IN) :: LENGTH
!
! Input arrays:
      type (ints_2e_ijkj) IJKJ_IN(LENGTH)
!
! Local scalars:
      integer Iao,Jao,Kao
      integer I,JS,JT
!
! Begin:
      call PRG_manager ('enter', 'BLD_DGC_ijkj', 'UTILITY')
!
      if(LENGTH.gt.0)then
      do I=1,LENGTH
        Iao=IJKJ_IN(I)%I
        Jao=IJKJ_IN(I)%J
        Kao=IJKJ_IN(I)%K
!
        JS=Iao*(Iao-1)/2+Jao
        JT=Kao*(Kao-1)/2+Jao
        GMat(JS)=GMat(JS)+PMat(JT)*IJKJ_IN(I)%IJKJ
        GMat(JT)=GMat(JT)+PMat(JS)*IJKJ_IN(I)%IJKJ
        JS=Jao*(Jao+1)/2
        JT=Iao*(Iao-1)/2+Kao
        GMat(JS)=GMat(JS)+PMat(JT)*IJKJ_IN(I)%IKJJ
        GMat(JT)=GMat(JT)+PMat(JS)*IJKJ_IN(I)%IKJJ*PT5
      end do
      end if ! LENGTH.gt.0
!
! End of routine BLD_DGC_ijkj
      call PRG_manager ('exit', 'BLD_DGC_ijkj', 'UTILITY')
      return
      end subroutine BLD_DGC_ijkj
      subroutine BLD_DGC_iikk (IIKK_IN, &
                               LENGTH)
!***********************************************************************
!     Date last modified: July 31, 1998                   Version 1.0  *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version.                                       *
!     Add IIKK contributions to Matrix G=P*(IIKK+IKIK).                *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
!
! Input arrays:
!
! Input scalars:
      integer, intent(IN) :: LENGTH
!
! Input arrays:
      type (ints_2e_iikk) IIKK_IN(LENGTH)
!
! Local scalars:
      integer Iao,Jao
      integer I,JS,JT
!
! Begin:
      call PRG_manager ('enter', 'BLD_DGC_iikk', 'UTILITY')
!
      if(LENGTH.gt.0)then
      do I=1,LENGTH
        Iao=IIKK_IN(I)%I
        Jao=IIKK_IN(I)%K
        JS=Iao*(Iao+1)/2
        JT=Jao*(Jao+1)/2
        GMat(JS)=GMat(JS)+PMat(JT)*IIKK_IN(I)%iikk
        GMat(JT)=GMat(JT)+PMat(JS)*IIKK_IN(I)%iikk
        JS=Iao*(Iao-1)/2+Jao
        GMat(JS)=GMat(JS)+PMat(JS)*IIKK_IN(I)%ikik
      end do
      end if ! LENGTH.gt.0
!
! End of routine BLD_DGC_iikk
      call PRG_manager ('exit', 'BLD_DGC_iikk', 'UTILITY')
      return
      end subroutine BLD_DGC_iikk
      subroutine BLD_DGC_ijjl (IJJL_IN, &
                               LENGTH)
!***********************************************************************
!     Date last modified: July 31, 1998                   Version 1.0  *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version.                                       *
!     Add IJJL contributions to Matrix G=P*(IJJL+ILJJ).                *
!***********************************************************************
! Modules:

      implicit none
!
! Input scalars:
!
! Input arrays:
!
! Input scalars:
      integer, intent(IN) :: LENGTH
!
! Input arrays:
      type (ints_2e_ijjl) IJJL_IN(LENGTH)
!
! Local scalars:
      integer Iao,Jao,Kao
      integer I,JS,JT
!
! Begin:
      call PRG_manager ('enter', 'BLD_DGC_ijjl', 'UTILITY')
!
      if(LENGTH.gt.0)then
      do I=1,LENGTH
        Iao=IJJL_IN(I)%I
        Jao=IJJL_IN(I)%J
        Kao=IJJL_IN(I)%L
!
        JS=Iao*(Iao-1)/2+Jao
        JT=Jao*(Jao-1)/2+Kao
        GMat(JS)=GMat(JS)+PMat(JT)*IJJL_IN(I)%ijjl
        GMat(JT)=GMat(JT)+PMat(JS)*IJJL_IN(I)%ijjl
        JS=Jao*(Jao+1)/2
        JT=Iao*(Iao-1)/2+Kao
        GMat(JS)=GMat(JS)+PMat(JT)*IJJL_IN(I)%iljj
        GMat(JT)=GMat(JT)+PMat(JS)*IJJL_IN(I)%iljj*PT5
      end do
      end if ! LENGTH.gt.0
!
! End of routine BLD_DGC_ijjl
      call PRG_manager ('exit', 'BLD_DGC_ijjl', 'UTILITY')
      return
      end subroutine BLD_DGC_ijjl
! END CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      end subroutine BLD_GMatrix
      subroutine BLD_DGR_ijkl (IJKL_IN, &
                               LENGTH, &
                               PMat, &
                               GMat)
!*************************************************************************
!     Date last modified: July 31, 1998                     Version 1.0  *
!     Author: R.A. Poirier                                               *
!     Description:                                                       *
!     Add IJKL contributions to Matrix deltaG=deltaP*(4*IJKL+IKJL+ILKJ). *
!     The BLD_DGR routines use RAW integrals.                            *
!*************************************************************************
! Modules:
      USE program_constants
      USE QM_objects
      USE INT_objects

      implicit none
!
! Input scalars:
      integer, intent(IN) :: LENGTH
!
! Input arrays:
      type (INTS_2e_ijkl) IJKL_IN(LENGTH)
      double precision :: PMat(MATlen)
      double precision :: GMat(MATlen)
!
! Local scalars:
      integer Iao,Jao,Kao,Lao
      integer I,JS,JT
      double precision A1,A2,A3,G1,G2,G3
!
! Local paraneters:
! Begin:
      call PRG_manager ('enter', 'BLD_DGR_ijkl', 'UTILITY')
!
      if(LENGTH.gt.0)then
      do I=1,LENGTH
      Iao=IJKL_IN(I)%I
      Jao=IJKL_IN(I)%J
      Kao=IJKL_IN(I)%K
      Lao=IJKL_IN(I)%L
      A1=IJKL_IN(I)%IJKL
      A2=IJKL_IN(I)%IKJL
      A3=IJKL_IN(I)%ILJK
      G1=FOUR*A1-(A2+A3)
      G2=FOUR*A2-(A1+A3)
      G3=FOUR*A3-(A1+A2)
!
      JS=Iao*(Iao-1)/2+Jao
      JT=Kao*(Kao-1)/2+Lao
      GMat(JS)=GMat(JS)+PMat(JT)*G1
      GMat(JT)=GMat(JT)+PMat(JS)*G1
      JS=Iao*(Iao-1)/2+Kao
      JT=Jao*(Jao-1)/2+Lao
      GMat(JS)=GMat(JS)+PMat(JT)*G2
      GMat(JT)=GMat(JT)+PMat(JS)*G2
      JS=Iao*(Iao-1)/2+Lao
      JT=Jao*(Jao-1)/2+Kao
      GMat(JS)=GMat(JS)+PMat(JT)*G3
      GMat(JT)=GMat(JT)+PMat(JS)*G3
      end do
      end if ! LENGTH.gt.0
!
! End of routine BLD_DGR_ijkl
      call PRG_manager ('exit', 'BLD_DGR_ijkl', 'UTILITY')
      return
      end subroutine BLD_DGR_ijkl
      subroutine BLD_DGR_ijkj (IJKJ_IN, &
                               LENGTH, &
                               PMat, &
                               GMat)
!***********************************************************************
!     Date last modified: July 31, 1998                   Version 1.0  *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version.                                       *
!     Add IJKJ contributions to Matrix G=P*(IJKJ+IKJJ).                *
!     The BLD_DGR routines use RAW integrals.                          *
!***********************************************************************
! Modules:
      USE program_constants
      USE QM_objects
      USE INT_objects

      implicit none
!
! Input scalars:
      integer, intent(IN) :: LENGTH
!
! Input arrays:
      type (ints_2e_ijkj) IJKJ_IN(LENGTH)
      double precision :: PMat(MATlen)
      double precision :: GMat(MATlen)
!
! Local scalars:
      integer Iao,Jao,Kao
      integer I,JS,JT
      double precision A1,A2,G1,G2
!
! Begin:
      call PRG_manager ('enter', 'BLD_DGR_ijkj', 'UTILITY')
!
      if(length.gt.0)then
      do I=1,LENGTH
        Iao=IJKJ_IN(I)%I
        Jao=IJKJ_IN(I)%J
        Kao=IJKJ_IN(I)%K
        A1=IJKJ_IN(I)%ijkj
        A2=IJKJ_IN(I)%ikjj
        G1=THREE*A1-A2
        G2=-TWO*A1+FOUR*A2
!
        JS=Iao*(Iao-1)/2+Jao
        JT=Kao*(Kao-1)/2+Jao
        GMat(JS)=GMat(JS)+PMat(JT)*G1
        GMat(JT)=GMat(JT)+PMat(JS)*G1
        JS=Jao*(Jao+1)/2
        JT=Iao*(Iao-1)/2+Kao
        GMat(JS)=GMat(JS)+PMat(JT)*G2
        GMat(JT)=GMat(JT)+PMat(JS)*G2*PT5
      end do
      end if
!
! End of routine BLD_DGR_ijkj
      call PRG_manager ('exit', 'BLD_DGR_ijkj', 'UTILITY')
      return
      end subroutine BLD_DGR_ijkj
      subroutine BLD_DGR_iikl (IIKL_IN, &
                               LENGTH, &
                               PMat, &
                               GMat)
!***********************************************************************
!     Date last modified: July 31, 1998                   Version 1.0  *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version.                                       *
!     Add IIKL contributions to Matrix G=P*(IIKL+IKIL).                *
!     The BLD_DGR routines use RAW integrals.                          *
!***********************************************************************
! Modules:
      USE program_constants
      USE QM_objects
      USE INT_objects

      implicit none
!
! Input scalars:
      integer, intent(IN) :: LENGTH
!
! Input arrays:
      type (ints_2e_iikl) IIKL_IN(LENGTH)
      double precision :: PMat(MATlen)
      double precision :: GMat(MATlen)
!
! Local scalars:
      integer Iao,Jao,Kao
      integer I,JS,JT
      double precision A1,A2,G1,G2
!
! Begin:
      call PRG_manager ('enter', 'BLD_DGR_iikl', 'UTILITY')
!
      if(length.gt.0)then
      do I=1,LENGTH
        Iao=IIKL_IN(I)%I
        Jao=IIKL_IN(I)%K
        Kao=IIKL_IN(I)%L
        A1=IIKL_IN(I)%iikl
        A2=IIKL_IN(I)%ikil
        G1=FOUR*A1-TWO*A2
        G2=-A1+THREE*A2
!
        JS=Iao*(Iao+1)/2
        JT=Jao*(Jao-1)/2+Kao
        GMat(JS)=GMat(JS)+PMat(JT)*G1
        GMat(JT)=GMat(JT)+PMat(JS)*G1*PT5
        JS=Iao*(Iao-1)/2+Jao
        JT=Iao*(Iao-1)/2+Kao
        GMat(JS)=GMat(JS)+PMat(JT)*G2
        GMat(JT)=GMat(JT)+PMat(JS)*G2
      end do
      end if
!
! End of routine BLD_DGR_iikl
      call PRG_manager ('exit', 'BLD_DGR_iikl', 'UTILITY')
      return
      end subroutine BLD_DGR_iikl
      subroutine BLD_DGR_iikk (IIKK_IN, &
                               LENGTH, &
                               PMat, &
                               GMat)
!***********************************************************************
!     Date last modified: July 31, 1998                   Version 1.0  *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version.                                       *
!     Add IIKK contributions to Matrix G=P*(IIKK+IKIK).                *
!     The BLD_DGR routines use RAW integrals.                          *
!***********************************************************************
! Modules:
      USE program_constants
      USE QM_objects
      USE INT_objects

      implicit none
!
! Input scalars:
      integer, intent(IN) :: LENGTH
!
! Input arrays:
      type (ints_2e_iikk) IIKK_IN(LENGTH)
      double precision :: PMat(MATlen)
      double precision :: GMat(MATlen)
!
! Local scalars:
      integer Iao,Kao
      integer I,JS,JT
      double precision A1,A2,G1,G2
!
! Begin:
      call PRG_manager ('enter', 'BLD_DGR_iikk', 'UTILITY')
!
      if(length.gt.0)then
      do I=1,LENGTH
        Iao=IIKK_IN(I)%I
        Kao=IIKK_IN(I)%K
        A1=IIKK_IN(I)%iikk
        A2=IIKK_IN(I)%ikik
        G1=TWO*A1-A2
        G2=-A1+THREE*A2

        JS=Iao*(Iao+1)/2
        JT=Kao*(Kao+1)/2
        GMat(JS)=GMat(JS)+PMat(JT)*G1
        GMat(JT)=GMat(JT)+PMat(JS)*G1
        JS=Iao*(Iao-1)/2+Kao
        GMat(JS)=GMat(JS)+PMat(JS)*G2
      end do
      end if
!
! End of routine BLD_DGR_iikk
      call PRG_manager ('exit', 'BLD_DGR_iikk', 'UTILITY')
      return
      end subroutine BLD_DGR_iikk
      subroutine BLD_DGR_ijjl (IJJL_IN, &
                               LENGTH, &
                               PMat, &
                               GMat)
!***********************************************************************
!     Date last modified: July 31, 1998                   Version 1.0  *
!     Author: R.A. Poirier                                             *
!     Description:  MUN Version.                                       *
!     Add IJJL contributions to Matrix G=P*(IJJL+ILJJ).                *
!     The BLD_DGR routines use RAW integrals.                          *
!***********************************************************************
! Modules:
      USE program_constants
      USE QM_objects
      USE INT_objects

      implicit none
!
! Input scalars:
      integer, intent(IN) :: LENGTH
!
! Input arrays:
      type (ints_2e_ijjl) IJJL_IN(LENGTH)
      double precision :: PMat(MATlen)
      double precision :: GMat(MATlen)
!
! Local scalars:
      integer Iao,Jao,Kao
      integer I,JS,JT
      double precision A1,A2,G1,G2
!
! Begin:
      call PRG_manager ('enter', 'BLD_DGR_ijjl', 'UTILITY')
!
      if(length.gt.0)then
      do I=1,LENGTH
        Iao=IJJL_IN(I)%I
        Jao=IJJL_IN(I)%J
        Kao=IJJL_IN(I)%L
        A1=IJJL_IN(I)%ijjl
        A2=IJJL_IN(I)%iljj
        G1=THREE*A1-A2
        G2=-TWO*A1+FOUR*A2
!
        JS=Iao*(Iao-1)/2+Jao
        JT=Jao*(Jao-1)/2+Kao
        GMat(JS)=GMat(JS)+PMat(JT)*G1
        GMat(JT)=GMat(JT)+PMat(JS)*G1
        JS=Jao*(Jao+1)/2
        JT=Iao*(Iao-1)/2+Kao
        GMat(JS)=GMat(JS)+PMat(JT)*G2
        GMat(JT)=GMat(JT)+PMat(JS)*G2*PT5
      end do
      end if
!
! End of routine BLD_DGR_ijjl
      call PRG_manager ('exit', 'BLD_DGR_ijjl', 'UTILITY')
      return
      end subroutine BLD_DGR_ijjl
      subroutine DG_direct
!***********************************************************************
!     Date last modified: June 12, 1997                    Version 1.0 *
!     Author: R.A. Poirier                                             *
!     Description: For the Direct SCF - call IDFCLC and re-calculate   *
!     the two-electron integrals                                       *
!***********************************************************************
! Modules:
      USE QM_objects
      USE INT_objects
      USE mod_idfclc ! improve

      implicit none
!
! Begin:
      call PRG_manager ('enter', 'DG_direct', 'UTILITY')
!
      if(.not.Lall_INCORE)then
! Initialize ONDISK buffer counters.
      IJJL_count=0
      IJKJ_count=0
      IIKL_count=0
      IJKL_count=0
! Initialize buffer numbers.
      IJJL_Nbuffers=0
      IJKJ_Nbuffers=0
      IIKL_Nbuffers=0
      IJKL_Nbuffers=0
!
! Count the number of two-electron integrals.
      NTINT(1:8)=0
      Ishell_start=Ishell_save
      Jshell_start=Jshell_save
      Kshell_start=Kshell_save
      Lshell_start=Lshell_save

      call IDFCLC

      IF(IJKL_count.GT.0)then
        call I2E_COMB_ijkl (IJKL_INCORE, IJKL_count)
      end if !IF(IJKL_count.GT.0)

      IF(IJKJ_count.GT.0)then
        call I2E_COMB_ijkj (IJKJ_INCORE, IJKJ_count)
      end if !IF(IJKJ_count.GT.0)

      IF(IJJL_count.GT.0)then
        call I2E_COMB_ijjl (IJJL_INCORE, IJJL_count)
      end if !IF(IJJL_count.GT.0)

      IF(IIKL_count.GT.0)then
        call I2E_COMB_iikl (IIKL_INCORE, IIKL_count)
      end if !IF(IIKL_count.GT.0)
!
      end if ! .not.Lall_INCORE
!
! End of routine DG_direct
      call PRG_manager ('exit', 'DG_direct', 'UTILITY')
      return
      end
      subroutine RHF_energy
!********************************************************************************************************
!     Date last modified: September 18, 2000                                               Version 2.0  *
!     Author: R.A. Poirier                                                                              *
!     Description: This routine only serves as an interface - causes the energy to be calculated        *
!********************************************************************************************************
! Modules:
      USE QM_defaults

      implicit none
!
! Begin:
      call PRG_manager ('enter', 'RHF_energy', 'ENERGY%RHF')
!
! This also creates the energy, so do nothing.
      call GET_object ('QM', 'CMO', Wavefunction)
!
! End of routine RHF_energy
      call PRG_manager ('exit', 'RHF_energy', 'ENERGY%RHF')
      return
      end
      subroutine BLD_FockM_RHF
!********************************************************************************************************
!     Date last modified: September 18, 2000                                               Version 2.0  *
!     Author: R.A. Poirier                                                                              *
!     Description: This routine only serves as an interface - causes the energy to be calculated        *
!********************************************************************************************************
! Modules:
      USE program_constants
      USE QM_defaults
      USE QM_objects
      USE INT_objects

      implicit none
!
! Local scalar:
      integer IERROR
!
! Begin:
      call PRG_manager ('enter', 'BLD_FockM_RHF', 'UTILITY')
!
      if(.not.allocated(DGM))then
        allocate (DGM(1:MATlen), stat=IERROR) ! always allocated together
        if(IERROR.ne.0)then
          stop ' ERROR> BLD_FockM_RHF: allocate failure: DGM...'
        end if
      else
        deallocate (DGM, stat=IERROR)
        allocate (DGM(1:MATlen), stat=IERROR)
        if(IERROR.ne.0)then
          stop ' ERROR> BLD_FockM_RHF: allocate failure: DGM...'
        end if
      end if

      if(.not.allocated(PM0G))then
        allocate (PM0G(1:MATlen), stat=IERROR) ! always allocated together
        if(IERROR.ne.0)then
          stop ' ERROR> BLD_FockM_RHF: allocate failure: PM0G...'
        end if
      else
        deallocate (PM0G, stat=IERROR)
        allocate (PM0G(1:MATlen), stat=IERROR)
        if(IERROR.ne.0)then
          stop ' ERROR> BLD_FockM_RHF: allocate failure: PM0G...'
        end if
      end if

      call DENBLD (CMOG%coeff, CMOG%occupancy, PM0G, Nbasis, MATlen, CMOG%NoccMO)

      select case (I2e_type)
        case ('RAW')
          integral_combinations=.false.
          call GET_object ('INT', '2EINT', 'RAW')  ! Get integrals (for a direct SCF, this created DGM)
        case default
          integral_combinations=.true.
          I2e_type='COMBINATIONS'
          call GET_object ('INT', '2EINT', 'COMBINATIONS')  ! Get integrals (for a direct SCF, this created DGM)
      end select
!
      call BLD_GMatrix (PM0G, DGM, MATlen)     ! Add contributions from integrals INCORE and ONDISK if any
!
! End of routine BLD_FockM_RHF
      call PRG_manager ('exit', 'BLD_FockM_RHF', 'UTILITY')
      return
      end
      subroutine DETLIN (OVRLAP, &
                         FockM, &
                         CMO, &
                         VA, &
                         EIGVL, &
                         SCRVEC, &
                         Nbasis, &
                         MATlen)
!********************************************************************************************************
!     Date last modified: July 07, 1992                                                                 *
!     Author: R.A. Poirier and D. Keefe                                                                 *
!     Description:  MUN Version.                                                                        *
!     H  (HC=VBCE)  or F  (FC=VBCE)   is converted to                                                   *
!     H' (H'C'=C'E) or F' (F'C'=C'E), and VA (C=VAC') is found.                                         *
!     OVRLAP  : Initially, contains the overlap matrix in its linear form.                              * 
!     FockM   : Initially, contains the Fock matrix (in ab initio LCAO-MO-SCF) in its linear form.      *
!               Finally, FockM' is in its linear form.                                                  *
!     CMO,CMO': Eigenvectors.                                                                           *
!     VA    : VA is an upper triangular matrix; there is a special matrix mutliplication routine used   *
!             at the end of this routine to take advantage of this fact.                                *
!             Finally, VA is in its 'linear' form.                                                      *
!     EIGVL : Eigenvalues.                                                                              *
!     SCRVEC: Scratch vector.                                                                           *
!     Nbasis: The number of atomic orbitals, basis functions, or contracted functions.                  *
!********************************************************************************************************
! Modules:
      USE program_constants
      USE matrix_print ! new

      implicit none
!
! Input scalars:
      integer, intent(IN) :: MATlen,Nbasis
!
! Input arrays:
      double precision CMO(Nbasis,Nbasis),EIGVL(Nbasis),FockM(MATlen),SCRVEC(Nbasis),OVRLAP(MATlen),VA(MATlen)
!
! Local scalars:
      integer I,II,IJ,IK,INDX1,J,K,KJ,KK,ITER
      double precision D,DEPS,E,T,VV
!
      parameter (DEPS=0.5D-8)
!
! Begin:
!      call PRG_manager ('enter', 'DETLIN', 'UTILITY')
!
! VAO is a unit matrix.
      VA(1:MATlen)=ZERO
!
      do I=1,Nbasis
        II=I*(I-1)/2+I
        VA(II)=ONE
      end do ! I
!
! By the following steps, HO(=H OF CMO), VBO(=OVRLAP) and VAO are
! converted to HK, VBK and VAK, respectively.
      do K=1,Nbasis-1
      KK=K*(K-1)/2+K
!
! By the following steps, FockM(K-1), VB(K-1) and VA(K-1) are converted to HK, VBK and VAK, respectively.
      do J=Nbasis,K+1,-1
      KJ=J*(J-1)/2+K
      D=OVRLAP(KJ)/OVRLAP(KK)
! If |D|<DEPS, then D is considered to be zero, and the following calculations can be omitted.
      IF(DABS(D).GT.DEPS)then
        do I=K+1,J
          IJ=J*(J-1)/2+I
          IK=I*(I-1)/2+K
          OVRLAP(IJ)=OVRLAP(IJ)-D*OVRLAP(IK)
          FockM(IJ)=FockM(IJ)-D*FockM(IK)
        end do ! I

        do I=1,K
          IJ=J*(J-1)/2+I
          IK=K*(K-1)/2+I
          VA(IJ)=VA(IJ)-D*VA(IK)
          FockM(IJ)=FockM(IJ)-D*FockM(IK)
        end do ! I

        do I=J,Nbasis
          IK=I*(I-1)/2+K
          FockM(I*(I-1)/2+J)=FockM(I*(I-1)/2+J)-D*FockM(IK)
        end do ! I
      end if
      end do ! J
      end do ! K
!
! By the following steps, final H' or CMO' can be obtained.
      do I=1,Nbasis
      II=I*(I-1)/2+I
!
! If OVRLAP(I,I)<0, then something is wrong.
! In normal cases, OVRLAP(I,I)<0 does not occur.
      IF(OVRLAP(II).LT.ZERO)then
        write(UNIout,'(//a)')'ERROR> DETLIN: OVERLAP LESS THAN ZERO'
        stop
      end if
!
      E=ONE/DSQRT(OVRLAP(II))
! Mutliply the Ith row of FockM by E.
      do J=I,Nbasis
        IJ=J*(J-1)/2+I
        FockM(IJ)=FockM(IJ)*E
      end do ! J
!
! Mutliply the Ith column of FockM and VA by E.
      do J=1,I
        FockM(I*(I-1)/2+J)=FockM(I*(I-1)/2+J)*E
        VA(I*(I-1)/2+J)=VA(I*(I-1)/2+J)*E
      end do ! J
      end do ! I
!
! CMO' (H') is squared into CMO.
      call UPT_to_SQS (FockM, MATlen, CMO, Nbasis)
!
! Diagonalize CMO and find eigenvectors and eigenvalues.
      call MATRIX_diagonalize (CMO, CMO, EIGVL, Nbasis, -2, .true.)
!
! Special matrix multiplication routine.
! Calculate CMO=VA*CMO (Note that VA is in upper triangular matrix).
      do J=1,Nbasis
      do I=1,Nbasis
        T=0
        do  K=I,Nbasis
          IK=K*(K-1)/2+I
          T=T+VA(IK)*CMO(K,J)
        end do ! K
        SCRVEC(I)=T
      end do ! I
      CMO(1:Nbasis,J)=SCRVEC(1:Nbasis)
      end do ! J
!
! End of routine DETLIN
!      call PRG_manager ('exit', 'DETLIN', 'UTILITY')
!     stop
      return
      end subroutine DETLIN
      subroutine ALLOC_SCF
!********************************************************************************************************
!     Date last modified: September 18, 2000                                               Version 2.0  *
!     Author: R.A. Poirier                                                                              *
!     Description: This routine only serves as an interface - causes the energy to be calculated        *
!********************************************************************************************************
! Modules:
      USE program_constants
      USE QM_defaults
      USE QM_objects
      USE INT_objects
      USE matrix_print

      implicit none
!
! Local scalar:
      integer IERROR, AMO, IAO
!
! Begin:
      call PRG_manager ('enter', 'ALLOC_SCF', 'UTILITY')
!
      call GET_object ('QM', 'CMO_GUESS', 'RHF')
!
      if(.not.allocated(DGM))then
        allocate (DGM(1:MATlen), stat=IERROR) ! always allocated together
        if(IERROR.ne.0)then
          stop ' ERROR> ALLOC_SCF: allocate failure: DGM...'
        end if
      else
        deallocate (DGM, stat=IERROR)
        allocate (DGM(1:MATlen), stat=IERROR)
        if(IERROR.ne.0)then
          stop ' ERROR> ALLOC_SCF: allocate failure: DGM...'
        end if
      end if

      if(.not.allocated(PM0G))then
        allocate (PM0G(1:MATlen), stat=IERROR) ! always allocated together
        if(IERROR.ne.0)then
          stop ' ERROR> ALLOC_SCF: allocate failure: PM0G...'
        end if
      else
        deallocate (PM0G, stat=IERROR)
        allocate (PM0G(1:MATlen), stat=IERROR)
        if(IERROR.ne.0)then
          stop ' ERROR> ALLOC_SCF: allocate failure: PM0G...'
        end if
      end if

      call DENBLD (CMOG%coeff, CMOG%occupancy, PM0G, Nbasis, MATlen, CMOG%NoccMO)
!
      DGM(1:MATlen)=ZERO

      select case (I2e_type)
        case ('RAW')
          integral_combinations=.false.
        case default
          integral_combinations=.true.
          I2e_type='COMBINATIONS'
      end select
!
! End of routine ALLOC_SCF
      call PRG_manager ('exit', 'ALLOC_SCF', 'UTILITY')
      return
      end
      subroutine PRT_MO_COEFF
!******************************************************************************************************
!     Date last modified: August 25, 2000                                                 Version 2.0 *
!     Author: R.A. Poirier                                                                            *
!     Description: Print RHF Eigenvalues and CMO coefficients (only closed-shell at the moment)       *
!******************************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE type_basis_set
      USE type_elements
      USE QM_defaults
      USE QM_objects
      USE INT_objects

      implicit none
!
! Local scalars:
      integer AOI,AOJ,frst,last,I,IANK,Iatmshl,Iatom,Ibasis,Iflag,ILOWER,IUPPER,Ishell,J,L,NAOI
      character*4 IELK
!
! Begin:
      call PRG_manager ('enter', 'PRT_MO_COEFF', 'UTILITY')
!
      call GET_object ('QM', 'CMO', Wavefunction)

      Iflag=1
      ILOWER=1
      do WHILE (Iflag.NE.0)
      IUPPER=ILOWER+9

      if(IUPPER.GE.Nbasis)then
        IUPPER=Nbasis
        Iflag=0
      end if

      write(UNIout,'(16X,10(8X,I3))')(I,I=ILOWER,IUPPER)
      write(UNIout,'(a,10F11.5)')'    Eigenvalues--- ',(CMO%EIGVAL(I),I=ILOWER,IUPPER)
       write(UNIout,'(a)')' '

      Ibasis=0
      do Ishell=1,Basis%Nshells
        frst=Basis%shell(Ishell)%frstSHL
        last=Basis%shell(Ishell)%lastSHL
        do Iatmshl=frst,last
        Iatom=Basis%atmshl(Iatmshl)%ATMLST
        IANK=CARTESIAN(Iatom)%Atomic_number
        AOI=Basis%atmshl(Iatmshl)%frstAO
        AOJ=Basis%atmshl(Iatmshl)%lastAO
        IELK=' '
!
        if(IANK.NE.0)then
        if(IANK.LT.0)then
          if(IABS(IANK).EQ.999)then
            IELK(2:3)='F*'
          else
            IELK(2:3)=Element_symbols(IABS(IANK))(1:2)
            IELK(4:4)='*'
          end if ! IABS(IANK).EQ.999
        else if(IANK.GT.0)then
          IELK(2:3)=Element_symbols(IANK)(1:2)
        end if ! IANK.LT.0

        do L=AOI,AOJ
          Ibasis=Ibasis+1
          write(UNIout,'(1X,i4,a4,i4,1X,a5,10f11.5)') &
                       Ibasis,IELK,Iatom,Basis%AOtype(Ibasis)(1:5),(CMO%coeff(Ibasis,J),J=ILOWER,IUPPER)
        end do ! L
        write(UNIout,'(a)')' '
        end if ! IANK.NE.0
        end do ! Iatmshl
      end do ! Ishell
!
        ILOWER=ILOWER+10
      end do ! WHILE
!
! End of routine PRT_MO_COEFF
      call PRG_manager ('exit', 'PRT_MO_COEFF', 'UTILITY')
      return
      end
      subroutine DETlinW (OVRLAP, & ! Input
                          FockM, &  ! Input
                          CMO, &    ! Output
                          EIGVL, &  ! Output
                          MATlen, &
                          Nbasis, &
                          Lsort)    ! Input
!********************************************************************************************************
!     Date last modified: July 07, 1992                                                                 *
!     Author: R.A. Poirier and D. Keefe                                                                 *
!     Description:  MUN Version.                                                                        *
!     H  (HC=VBCE)  or F  (FC=VBCE)   is converted to                                                   *
!     H' (H'C'=C'E) or F' (F'C'=C'E), and VA (C=VAC') is found.                                         *
!     OVRLAP  : Initially, contains the overlap matrix in its linear form.                              * 
!     FockM   : Initially, contains the Fock matrix (in ab initio LCAO-MO-SCF) in its linear form.      *
!               Finally, FockM' is in its linear form.                                                  *
!     CMO,CMO': Eigenvectors.                                                                           *
!     EIGVL : Eigenvalues.                                                                              *
!********************************************************************************************************
! Modules:
      USE program_constants
      USE matrix_print ! new

      implicit none
!
! Input scalars:
      integer :: MATlen,Nbasis
      logical :: Lsort
! Input arrays:
      double precision, dimension(MATlen) ::  FockM,OVRLAP
!
! Output arrays:
      double precision, dimension(Nbasis,Nbasis) ::  CMO
      double precision, dimension(Nbasis) ::  EIGVL
!
! Work arrays:
      double precision, dimension(:), allocatable :: SCRVEC
      double precision, dimension(:), allocatable :: VA
      double precision, dimension(:), allocatable :: FockMw
      double precision, dimension(:), allocatable :: SovlPw
!
! Local scalars:
      integer I,II,IJ,IK,INDX1,J,K,KJ,KK,ITER
      double precision D,DEPS,E,T,VV
!
      parameter (DEPS=0.5D-8)
!
! Begin:
!      call PRG_manager ('enter', 'DETlinW', 'UTILITY')
!
!     Nbasis=size(EIGVL)
!     MATlen=Size(FockM)
      allocate (SCRVEC(Nbasis))
      allocate (VA(MATlen))
      allocate (FockMw(MATlen))
      allocate (SovlPw(MATlen))
      FockMw(1:MATlen)=FockM(1:MATlen)
      SovlPw(1:MATlen)=OVRLAP(1:MATlen)
! VA is set to a unit matrix.
      VA(1:MATlen)=ZERO
      do I=1,Nbasis
        II=I*(I-1)/2+I
        VA(II)=ONE
      end do ! I
!
! By the following steps, HO(=H OF CMO), VBO(=SovlPw) and VAO are
! converted to HK, VBK and VAK, respectively.
      do K=1,Nbasis-1
      KK=K*(K-1)/2+K
!
! By the following steps, FockMw(K-1), VB(K-1) and VA(K-1) are converted to HK, VBK and VAK, respectively.
      do J=Nbasis,K+1,-1
      KJ=J*(J-1)/2+K
      D=SovlPw(KJ)/SovlPw(KK)
! If |D|<DEPS, then D is considered to be zero, and the following calculations can be omitted.
      IF(DABS(D).GT.DEPS)then
        do I=K+1,J
          IJ=J*(J-1)/2+I
          IK=I*(I-1)/2+K
          SovlPw(IJ)=SovlPw(IJ)-D*SovlPw(IK)
          FockMw(IJ)=FockMw(IJ)-D*FockMw(IK)
        end do ! I

        do I=1,K
          IJ=J*(J-1)/2+I
          IK=K*(K-1)/2+I
          VA(IJ)=VA(IJ)-D*VA(IK)
          FockMw(IJ)=FockMw(IJ)-D*FockMw(IK)
        end do ! I

        do I=J,Nbasis
          IK=I*(I-1)/2+K
          FockMw(I*(I-1)/2+J)=FockMw(I*(I-1)/2+J)-D*FockMw(IK)
        end do ! I
      end if
      end do ! J
      end do ! K
!
! By the following steps, final H' or CMO' can be obtained.
      do I=1,Nbasis
      II=I*(I-1)/2+I
!
! If SovlPw(I,I)<0, then something is wrong.
! In normal cases, SovlPw(I,I)<0 does not occur.
      IF(SovlPw(II).LT.ZERO)then
        write(UNIout,'(//a)')'ERROR> DETlinW: OVERLAP LESS THAN ZERO'
        STOP
      end if
!
      E=ONE/DSQRT(SovlPw(II))
! Mutliply the Ith row of FockMw by E.
      do J=I,Nbasis
        IJ=J*(J-1)/2+I
        FockMw(IJ)=FockMw(IJ)*E
      end do ! J
!
! Mutliply the Ith column of FockMw and VA by E.
      do J=1,I
        FockMw(I*(I-1)/2+J)=FockMw(I*(I-1)/2+J)*E
        VA(I*(I-1)/2+J)=VA(I*(I-1)/2+J)*E
      end do ! J
      end do ! I
!
! CMO' (H') is squared into CMO.
      call UPT_to_SQS (FockMw, MATlen, CMO, Nbasis)
!
! Diagonalize CMO and find eigenvectors and eigenvalues.
      call MATRIX_diagonalize (CMO, CMO, EIGVL, Nbasis, -2, .true.)
!
! Special matrix multiplication routine.
! Calculate CMO=VA*CMO (Note that VA is in upper triangular matrix).
      do J=1,Nbasis
      do I=1,Nbasis
        T=0
        do  K=I,Nbasis
          IK=K*(K-1)/2+I
          T=T+VA(IK)*CMO(K,J)
        end do ! K
        SCRVEC(I)=T
      end do ! I
      CMO(1:Nbasis,J)=SCRVEC(1:Nbasis)
      end do ! J
!
      deallocate (SCRVEC)
      deallocate (VA)
      deallocate (FockMw)
      deallocate (SovlPw)
! End of routine DETlinW
!      call PRG_manager ('exit', 'DETlinW', 'UTILITY')
!     stop
      return
      end subroutine DETlinW
