      subroutine THERMOdynamics
!*****************************************************************************************************
!     Date last modified: June 21, 2004                                                              *
!     Author: J. Hollet and R.A. Poirier                                                             *
!     Description: Compute thermodynamics properties.                                                *
!*****************************************************************************************************
! Modules:
      USE program_files
      USE program_constants
      USE type_molecule
      USE QM_defaults
      USE QM_objects
      USE OPT_defaults
      USE matrix_print
      
      implicit none

      integer :: I,Iatom,NFreq,firstFreq
      double precision :: qtrans,qtrans_indist,qrot,qvib
      double precision :: Etrans,Atrans,Strans,Cvtrans
      double precision :: Erot,Arot,Srot,Cvrot
      double precision :: Evib,Avib,Svib,Cvvib,thetavib,ZPE
      double precision :: Ix,Iy,Iz,thetarotx,thetaroty,thetarotz,thetarot
      double precision :: H,G,Cp,V,T,symm,toler
      double precision, allocatable :: shiftx(:),shifty(:),shiftz(:) 
      double precision :: CM(3),Inertia(3,3),PMOI(3)
      double precision :: Iconvert
      logical :: Linear
!
! Begin:
      call PRG_manager ('enter', 'THERMOdynamics', 'UTILITY')
!
      call GET_object ('QM', 'FREQUENCIES', Hessian_method(1:len_trim(Hessian_method)))

      NFreq=size(VibFreq)
      write(6,'(a,i0)')' NFreq:  ',NFreq

      toler=0.00001
      symm=2.0
      T=Temperature
      V=Avogadro*Boltz_k*T/Pressure ! Volume

!  Translation-Indistinguishibility:
      qtrans=V*(molecule%mass*amu_to_kg*Boltz_k*T/(2.0*PI_VAL*Planck_h_bar**2.0))**(1.5)
      qtrans_indist=dexp(ONE)/Avogadro*qtrans

      Etrans=1.5*Avogadro*Boltz_k*T

      Atrans=-Avogadro*Boltz_k*T*LOG(qtrans_indist)

      Strans=Avogadro*Boltz_k*(LOG(qtrans_indist)+1.5)

      Cvtrans=1.5*Avogadro*Boltz_k

      write(UNIout,'(/a)')'Translational contributions:    J/mol        cal/mol'
      write(UNIout,'(a,2(f20.5))')' E_Translational:',Etrans,Etrans/4.184
      write(UNIout,'(a,2(f20.5))')' A_Translational:',Atrans,Atrans/4.184
      write(UNIout,'(a,2(f20.5))')' S_Translational:',Strans,Strans/4.184
      write(UNIout,'(a,2(f20.5))')'Cv_Translational:',Cvtrans,Cvtrans/4.184

!  Moments of Inertia:
      allocate (shiftx(Natoms), shifty(Natoms), shiftz(Natoms))
!
      call CENTER_OF_MASS (CM)
      do Iatom=1,Natoms
        if(CARTESIAN(Iatom)%Atomic_number.gt.0)then
          shiftx(Iatom)=CARTESIAN(Iatom)%X-CM(1) 
          shifty(Iatom)=CARTESIAN(Iatom)%Y-CM(2)
          shiftz(Iatom)=CARTESIAN(Iatom)%Z-CM(3)
        end if
      end do !Iatom
!     
      Inertia(1:3,1:3)=0.0D0
!
      do Iatom=1,Natoms
        if(CARTESIAN(Iatom)%Atomic_number.gt.0)then
          Inertia(1,1)=Inertia(1,1)+MOL_atoms(Iatom)%mass&
                       *(shifty(Iatom)**2+shiftz(Iatom)**2)
          Inertia(2,2)=Inertia(2,2)+MOL_atoms(Iatom)%mass&
                       *(shiftx(Iatom)**2+shiftz(Iatom)**2)
          Inertia(3,3)=Inertia(3,3)+MOL_atoms(Iatom)%mass&
                       *(shiftx(Iatom)**2+shifty(Iatom)**2)
          Inertia(2,1)=Inertia(2,1)-MOL_atoms(Iatom)%mass&
                       *shiftx(Iatom)*shifty(Iatom)
          Inertia(3,1)=Inertia(3,1)-MOL_atoms(Iatom)%mass&
                       *shiftx(Iatom)*shiftz(Iatom)
          Inertia(3,2)=Inertia(3,2)-MOL_atoms(Iatom)%mass&
                       *shifty(Iatom)*shiftz(Iatom)
        end if
      end do !Iatom

      Inertia(1,2)=Inertia(2,1)
      Inertia(1,3)=Inertia(3,1)
      Inertia(2,3)=Inertia(3,2)

      write(UNIout,*)'Inertia matrix:'
      call PRT_matrix (Inertia, 3, 3)
!  Diagonalize:
      call MATRIX_diagonalize (Inertia, Inertia, PMOI, 3, 1, .false.)
!
      Ix=PMOI(1)
      Iy=PMOI(2)
      Iz=PMOI(3)
!
      Linear=.false.
      if(((Ix.LT.toler).and.(dabs(Iy-Iz).LT.toler)).or.((Iy.LT.toler).and.(dabs(Ix-Iz).LT.toler))&
        .or.((Iz.LT.toler).and.(dabs(Ix-Iy).LT.toler)))then
        Linear=.true.
        write(UNIout,'(2(a))') 'Molecule is Linear'
      else
        write(UNIout,'(2(a))') 'Molecule is Non-linear'
      end if!Linear or Non-linear

!  Rotation:
      write(UNIout,'(a)')'Moments of inertia (amu Bohr^2)'
      write(UNIout,'(a,3F16.5)')'Ix,Iy,Iz: ',Ix,Iy,Iz
      Iconvert=1.0D-20*amu_to_kg*(Bohr_to_Angstrom)**2 ! convert from a.u. to kg.m**2
      Ix=Ix*Iconvert
      Iy=Iy*Iconvert
      Iz=Iz*Iconvert
      write(UNIout,'(a)')'Moments of inertia (kg m^2)'
      write(UNIout,'(a,1PE16.5,2E16.5)')'Ix,Iy,Iz: ',Ix,Iy,Iz
!
      if(Linear)then
        if(Ix.LT.toler)then! One of the PMOI is zero, the other two are equal
          thetarot=Planck_h_bar**2/(2.0*Ix*Boltz_k)
        else
          thetarot=Planck_h_bar**2/(2.0*Iy*Boltz_k)
        end if! Calculation of thetarot
!
        qrot=T/(symm*thetarot)
        Erot=Avogadro*Boltz_k*T
        Arot=-Avogadro*Boltz_k*T*LOG(qrot)
        Srot=Avogadro*Boltz_k*(1.0+LOG(qrot)) 
        Cvrot=Avogadro*Boltz_k
      else ! Non-linear
        thetarotx=Planck_h_bar**2/(2.0*Ix*Boltz_k)
        thetaroty=Planck_h_bar**2/(2.0*Iy*Boltz_k)
        thetarotz=Planck_h_bar**2/(2.0*Iz*Boltz_k)
!
        qrot=PI_VAL**(0.5)/symm*T**(1.5)/(thetarotx*thetaroty*thetarotz)**(0.5)
        Erot=1.5*Avogadro*Boltz_k*T
        Arot=-Avogadro*Boltz_k*T*LOG(qrot)
        Srot=Avogadro*Boltz_k*(1.5+LOG(qrot))
        Cvrot=1.5*Avogadro*Boltz_k
      end if!Linear or Non-linear rotational calculations

      write(UNIout,'(/a)')'Rotational contributions:    J/mol        cal/mol'
      write(UNIout,'(a,2(f20.5))')' E_Rotational:',Erot,Erot/4.184
      write(UNIout,'(a,2(f20.5))')' A_Rotational:',Arot,Arot/4.184
      write(UNIout,'(a,2(f20.5))')' S_Rotational:',Srot,Srot/4.184
      write(UNIout,'(a,2(f20.5))')'Cv_Rotational:',Cvrot,Cvrot/4.184
  
!  Vibration:
      VibFreq(1:NFreq)=VibFreq(1:NFreq)*100.0*C_speed
      qvib=1.0
      Evib=0.0
      Avib=0.0
      Svib=0.0
      Cvvib=0.0
      ZPE=0.0

      if(Linear)then
        firstFreq=6
      else
        firstFreq=7
      end if! First frequency

      do I=firstFreq,NFreq ! Skip first six 
        if(VibFreq(I).LT.0.0)cycle
        thetavib=Planck_h*VibFreq(I)/Boltz_k
        qvib=qvib*dexp(-thetavib/(2.0*T))/(1-dexp(-thetavib/T))
        Evib=Evib+Avogadro*Boltz_k*thetavib*(0.5+1.0/(dexp(thetavib/T)-1.0))
        Avib=Avib+Avogadro*Boltz_k*(thetavib/2.0+T*LOG(1-dexp(-thetavib/T)))
        Svib=Svib+Avogadro*Boltz_k*(-LOG(1-dexp(-thetavib/T))&
             +thetavib/(T*(dexp(thetavib/T)-1))) 
        Cvvib=Cvvib+Avogadro*Boltz_k*(thetavib)**2.0*dexp(thetavib/T)& 
              /(T**2.0*(dexp(thetavib/T)-1.0)**2)
        ZPE=ZPE+Avogadro*Boltz_k*thetavib*0.5
      end do

      write(UNIout,'(/a)')'Vibrational contributions:    J/mol         cal/mol'
      write(UNIout,'(a,2(f20.5))')'Zero-Point Energy:',ZPE,ZPE/4.184
      write(UNIout,'(a,2(f20.5))')'    E_Vibrational:',Evib,Evib/4.184
      write(UNIout,'(a,2(f20.5))')'    A_Vibrational:',Avib,Avib/4.184
      write(UNIout,'(a,2(f20.5))')'    S_Vibrational:',Svib,Svib/4.184
      write(UNIout,'(a,2(f20.5))')'   Cv_Vibrational:',Cvvib,Cvvib/4.184
!
!  Enthalpy and Gibbs Energy:
      H=Etrans+Evib+Erot+Avogadro*Boltz_k*T
      G=Atrans+Avib+Arot+Avogadro*Boltz_k*T 

!  Printing for Testing Purposes
      write(6,'(a,2X,F20.5)')'H: ',H
      write(6,'(a,2X,F20.5)')'G: ',G

!     deallocate(shiftx,shifty,shiftz)

      call PRG_manager ('exit', 'THERMOdynamics', 'UTILITY')
      return
      end subroutine THERMOdynamics
