      subroutine purify_p
!***********************************************************************
!     Date last modified: March 19, 2019                               *
!     Author: J.W. Hollett                                             *
!     Description: Rotate the p-orbitals from an atomic RHF calculation*
!                  such that we have pure px, py, pz (no lin. combos)  *
!***********************************************************************
! Modules:
      USE program_files
      USE QM_objects
      USE QM_defaults
      USE type_basis_set

      implicit none
!
! Local scalars:
      integer :: NPsets,IPset,FirstP,LastP,pAOx,pAOy,pAOz,Ishell,IAO
      integer :: IER
      logical :: LCPinv_made
!
! Local arrays:
      integer :: Porb_List(4) ! Up to 5P
      data Porb_List/3,7,16,25/
      double precision :: CPinv(3,3), Cnew(3,3)

! Begin:
      call PRG_manager ('enter', 'purify_p', 'UTILITY')
!
      write(UNIout,'(a,i2)')'NPorbitals ',NPorbitals
      NPsets=NPorbitals/3
      write(UNIout,'(a,i2,a)')'A total of ',NPsets,' set will be transformed to pure Ps'
      do IPset=1,NPsets
      ! New transformation matrix for every set of p's
        LCPinv_made=.false.
!
        FirstP=Porb_list(IPset)
        LastP=FirstP+2
!
! Loop through basis functions and transform p-AO coefficients
      do Ishell = 1, Basis%Nshells
        if(Basis%shell(Ishell)%Xtype.ne.1)cycle ! cycle if not a p shell
        pAOx= Basis%atmshl(Ishell)%frstAO
        pAOy= pAOx+1
        pAOz= pAOy+1
!
        if(.not.LCPinv_made)then
        CPinv=CMO%coeff(pAOx:pAOz,FirstP:LastP)
        call INVERT (CPinv, 3, 3, IER)
!
! Normalize vectors
        do IAO=1,3
          CPinv(1:3,IAO)=CPinv(1:3,IAO)/dsqrt(dot_product(CPinv(1:3,IAO),CPinv(1:3,IAO)))
        end do
        LCPinv_made=.true.
        end if ! LCPinv_made
!
        Cnew = matmul(CPinv,CMO%coeff(pAOx:pAOz,FirstP:LastP))
        CMO%coeff(pAOx:pAOz,FirstP:LastP)=Cnew
!
      end do ! Ishell
      end do ! IPset
!
! End of routine purify_p
      call PRG_manager ('exit', 'purify_p', 'UTILITY')
      return
      end subroutine purify_p
