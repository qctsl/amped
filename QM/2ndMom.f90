      subroutine SECOND_MOMENT_ETOT
!***********************************************************************
!     Date last modified: August, 2003                     Version 1.0 *
!     Author: RAP & Aaron Kelly                                        *
!     Description: Calculates the total electronic component of the    *
!                  dipole & second moment                              *
!***********************************************************************
! Modules:
      USE program_constants
      USE program_files
      USE QM_defaults
      USE QM_objects
      USE INT_objects
      USE type_molecule
      USE type_basis_set

      implicit none

! Local scalars:
      integer :: Iatom
      double precision :: AN
      double precision :: Cx,Cy,Cz
      double precision :: DipoleX,DipoleY,DipoleZ
      double precision :: SecondXX,SecondYY,SecondZZ,SecondXY,SecondXZ,SecondYZ
! Local routines:
      double precision traclo
      double precision :: SecMom(3,3)

! Begin:
      call PRG_manager ('enter', 'SECOND_MOMENT_ETOT', 'UTILITY')

      call GET_object ('INT', 'MOMENT1', 'AO')
      call GET_object ('QM', 'CMO', 'RHF')

      SecMom(1:3,1:3)=ZERO
 
      DipoleX=-traclo(DipoleX_AO, PM0, NBasis, MATlen)
      DipoleY=-traclo(DipoleY_AO, PM0, NBasis, MATlen)
      DipoleZ=-traclo(DipoleZ_AO, PM0, NBasis, MATlen)
      
      Cx=OEPXYZ(1)
      CY=OEPXYZ(2)
      Cz=OEPXYZ(3)

! Add in nuclear contribution.
      do Iatom=1,Natoms
        if(CARTESIAN(Iatom)%Atomic_number.GT.0)then
        AN=CARTESIAN(Iatom)%Atomic_number
        DipoleX=DipoleX+(CARTESIAN(Iatom)%X-Cx)*AN
        DipoleY=DipoleY+(CARTESIAN(Iatom)%Y-Cy)*AN
        DipoleZ=DipoleZ+(CARTESIAN(Iatom)%Z-Cz)*AN
      end if ! CARTESIAN(Iatom)%Atomic_number.GT.0
      end do ! Iatom

      DipoleX=DipoleX*Dconst  ! Convert to Debye
      DipoleY=DipoleY*Dconst
      DipoleZ=DipoleZ*Dconst
      write(UNIout,'(a,3f12.6/)')'Dipole Moment (Debyes): ',DipoleX,DipoleY,DipoleZ

      SecondXX=-traclo(SecMomXX_AO, PM0, NBasis, MATlen)
      SecondXY=-traclo(SecMomXY_AO, PM0, NBasis, MATlen)
      SecondXZ=-traclo(SecMomXZ_AO, PM0, NBasis, MATlen)
      SecondYY=-traclo(SecMomYY_AO, PM0, NBasis, MATlen)
      SecondYZ=-traclo(SecMomYZ_AO, PM0, NBasis, MATlen)
      SecondZZ=-traclo(SecMomZZ_AO, PM0, NBasis, MATlen)

      write(UNIout,'(a,3F12.6,2a)')'Second Moment (a.u.) at (',OEPXYZ, &
           ') for: ',MOL_TITLE(1:len_trim(MOL_title))
      write(UNIout,'(18x,6(a,10x))')'XX','YY','ZZ','XY','XZ','YZ'
      write(UNIout,'(a,6F12.6)')'Electronic: ',SecondXX,SecondYY,SecondZZ,SecondXY,SecondXZ,SecondYZ

! Add in nuclear contribution.
       do Iatom=1,Natoms
         if(CARTESIAN(Iatom)%Atomic_number.GT.0)then
         AN=CARTESIAN(Iatom)%Atomic_number
         SecondXX=SecondXX+(CARTESIAN(Iatom)%X-Cx)*(CARTESIAN(Iatom)%X-Cx)*AN
         SecondXY=SecondXY+(CARTESIAN(Iatom)%X-Cx)*(CARTESIAN(Iatom)%Y-Cy)*AN
         SecondYY=SecondYY+(CARTESIAN(Iatom)%Y-Cy)*(CARTESIAN(Iatom)%Y-Cy)*AN
         SecondYZ=SecondYZ+(CARTESIAN(Iatom)%Y-Cy)*(CARTESIAN(Iatom)%Z-Cz)*AN
         SecondXZ=SecondXZ+(CARTESIAN(Iatom)%X-Cx)*(CARTESIAN(Iatom)%Z-Cz)*AN
         SecondZZ=SecondZZ+(CARTESIAN(Iatom)%Z-Cz)*(CARTESIAN(Iatom)%Z-Cz)*AN
         end if ! CARTESIAN(Iatom)%Atomic_number.GT.0
       end do ! Iatom

      SecondXX=SecondXX*Bohr_to_Angstrom*Dconst
      SecondXY=SecondXY*Bohr_to_Angstrom*Dconst
      SecondXZ=SecondXZ*Bohr_to_Angstrom*Dconst
      SecondYY=SecondYY*Bohr_to_Angstrom*Dconst
      SecondYZ=SecondYZ*Bohr_to_Angstrom*Dconst
      SecondZZ=SecondZZ*Bohr_to_Angstrom*Dconst
      
      write(UNIout,'(a,3F12.6,2a)')'Second Moment (Angstroms*Debye) at (',OEPXYZ, &
           ') for: ',MOL_TITLE(1:len_trim(MOL_title))
      write(UNIout,'(18x,6(a,10x))')'XX','YY','ZZ','XY','XZ','YZ'
      write(UNIout,'(a,6F12.6/)')'Total: ',SecondXX,SecondYY,SecondZZ,SecondXY,SecondXZ,SecondYZ

      SecMom(1,1)=SecondXX
      SecMom(2,1)=SecondXY
      SecMom(3,1)=SecondXZ
      SecMom(2,2)=SecondYY
      SecMom(3,2)=SecondYZ
      SecMom(3,3)=SecondZZ 

      call PRG_manager ('exit', 'SECOND_MOMENT_ETOT', 'UTILITY')
      return
      end subroutine SECOND_MOMENT_ETOT
