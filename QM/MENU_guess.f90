      subroutine MENU_guess
!***********************************************************************
!     Date last modified: June 24, 1992                                *
!     Author: R.A. Poirier and F. Colonna                              *
!     Description: Prepare for the initial guess.                      *
!***********************************************************************
! Modules:
      USE program_parser
      USE menu_gets
      USE QM_defaults

      implicit none
!
! Local scalar:
      integer lenstr
      logical done
      logical :: LType,LUSE_previous
      integer, parameter :: MAX_string=132
      character(len=MAX_string) string
!
! Begin:
      call PRG_manager ('enter', 'MENU_guess', 'UTILITY')
      done=.false.
      LType=.false.
      LUSE_previous=.false.
!
! Menu:
      do while (.not.done)
      call new_token (' GUESS:')
!
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      ' Command GUESS:', &
      '   Purpose: Prepare initial guess ', &
      '   Syntax :', &
      '   From = <string>, Basis set name, i.e., STO-3G, 3-21G or 6-31G', &
      '   PREvious = <logical>, Use MOs or a Fock matrix (default)from a previous calculation', &
      '   TYpe =  <string> : available options are, ', &
      '            DIAgonalize H (not available)', &
      '            EXTended Huckel ', &
      '            G matrix ', &
      '            PREvious MO', &
      '            PROjected Huckel MO', &
      '            PFock, Project the Fock matrix from some other basis (default)', &
      '            R12, Use the approximate coulomb integrals', &
      '   VSCale or VAL_scale = <real>, valence scale factor for HUCKEL matrix', &
      '   CSCale or CORE_scale = <real>, core scale factor for HUCKEL matrix', &
      '   Recommend that both the core and valence shell scaling factors be defined', &
      '   end'
!
! From
      else if(token(1:1).EQ.'F')then
        call GET_value (string, lenstr)
        if(string(1:3).eq.'STO')then
          GUESS_from_basis=string(1:lenstr)
!         GUESS_from_basis='STO-3G '
        else if(string(1:3).eq.'3-2')then
          GUESS_from_basis=string(1:lenstr)
!         GUESS_from_basis='3-21G'
        else if(string(1:3).eq.'6-3')then
          GUESS_from_basis=string(1:lenstr)
!         GUESS_from_basis='6-31G'
        else
          write(UNIout,'(a)')'MENU_guess> Can only project from STO-3G, 3-21G or 6-31G'
          stop 'MENU_guess> Can only project from STO-3G, 3-21G or 6-31G'
        end if

! CScale
      else if(token(1:2).EQ.'CS'.or.token(1:2).EQ.'CO')then
        call GET_value (Core_scale)
        GUESS_Lscale=.false.
        write(UNIout,'(a,f12.6)')'MENU_guess> Core scale set to ',CORE_scale

! VScale
      else if(token(1:2).EQ.'VS'.or.token(1:2).EQ.'VA')then
        call GET_value (Val_scale)
        GUESS_Lscale=.false.
        write(UNIout,'(a,f12.6)')'MENU_guess> Valence scale set to ',VAL_scale

! PREvious
      else if(token(1:3).EQ.'PRE')then
        call GET_value (LUSE_previous)
        write(UNIout,'(a,f12.6)')'MENU_guess> Use previous MO guess'

! TYpe
      else if(token(1:2).EQ.'TY')then
        call GET_value (string, lenstr)
        if(string(1:2).eq.'PF')then
          GUESS_type='FockMatrix'
        else if(string(1:3).eq.'DIA')then
          GUESS_type='DIAGH'
        else if(string(1:3).eq.'EXT')then
          GUESS_type='HuckelMatrix'
        else if(string(1:3).eq.'GMA')then
          GUESS_type='GMATRIX'
        else if(string(1:3).eq.'PRO')then
          GUESS_type='MO  '
        else if(string(1:2).eq.'MO')then
          GUESS_type='MO  '
        else if(string(1:3).eq.'HCO')then
          GUESS_type='HCORE'
        else if(string(1:3).eq.'R12')then
          GUESS_type='R12'
        end if
        write(UNIout,'(9x,3a)')'MENU_guess> SCF Initial guess set to project "',GUESS_type(1:len_trim(GUESS_type)),'"'
        LType=.true.

      else
           call MENU_end (done)
      end if
!
      end do !(.not.done)

      if(LUSE_previous)then
        if(.not.LType)GUESS_type='FockMatrix  '
        call SET_MO_project ! If a structure has just been archived to not set MO
      end if

!
! End of routine MENU_guess
      call PRG_manager ('exit', 'MENU_guess', 'UTILITY')
      return
      end
