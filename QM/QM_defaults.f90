      subroutine SET_defaults_QM
!*****************************************************************************************************************
!     Date last modified: March 31, 2000                                                            Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Set SCF defaults.                                                                             *
!*****************************************************************************************************************
! Modules:
      USE QM_defaults
      USE QM_objects
      USE INT_objects
!
      integer GET_maxint

! Begin:
      NPorbitals=0
      Wavefunction='RHF'
      Level_of_Theory='RHF'
      EXTRAPOLATION_MET='3,4 POINT'
      LSCF_converged=.true.
      LDirect_SCF=.false.
      LDandC_menu=.false.
      LDandC=.false.
      LPFockOP_menu=.false.
      LGMatrix_dump=.false.
      LPFockOP=.false.
      SCFITN=60
      SCFACC=5.0D-06
      SCFDMP=0.70D0
      ShiftLevel=0.0D0
      LShift=.false.
      NCore=0
      Nopen=0
      ORBACC=5.0D-6
      MAXPAR=0 ! for now
      SCALST=0.0D0
      ROTMET='RESET'
      ROHFGMO='LMO'
      ROHFPPT='NOPPT'
      correlation = 'DELTANO'
      LRDMcore = .true.
      TWORDMfile = ''
      Nnatorb = Nbasis
      q_TDF = 1.00D0
      masse = 1.00D0 ! FYI muon mass = 206.768283 m_e
!
! MOM defaults
      LMOM =  .false.
      MOMstart = 20
! MO read
      LMOread = .false.
!
      I2E_ACCF=1.0D-07
      I2E_ACC=I2E_ACCF
      I2E_EXPCUT=20.0D0
!     I2E_EXPCUT=45.0D0
      I2E_PQCUT1=1.0D-06
      I2E_PQCUT2=1.0D-16
      ERROR1=1.0D-06
      ERROR2=1.0D-08
      VAR1=15.0D0
      VAR2=5.0D0
      Li2ESP=.true.
      Li2EDF=.false.
      LI2E_exist=.false.
      Lall_ONDISK=.false.
      I2E_type='?'
      IIII_store='INCORE'
      IIKL_store='INCORE'
      IJKJ_store='INCORE'
      IJJL_store='INCORE'
      IJKL_store='INCORE'
!
! NOTE: Too large a value of I2EBUF can cause allocation errors
!     I2EBUF=47239010   ! 09/08/20 up to 185 basis functions
      I2EBUF=26294360   ! 13/04/21 up to 160 basis functions
!     I2EBUF=1581580 ! 02/02/18 up to 80 basis functions
!     I2EBUF=487635 ! up to 60 basis functions (N*(N-1)*(N-2)*(N-3)/24)
!
! Preliminary entry.
!     call I2E_DEALLOCATES      ! this does not belong here - can cause problems
!
      MAX_transform=871200 ! Can transform up to 120 basis functions
      Nfrozen_core=NFrozen_cores()
      NFrozen_virt=0
!
!     GUESS_type='EXTENDED_HUCKEL'  ! EXtended_Huckel, Projected
      GUESS_type='FockMatrix   '  ! Project the Fock matrix (Huckel or from previous calculation)
!     GUESS_type='MO   '          ! Project the MOs.
      BASIS_PROJECT='STO-3G'        ! Project from STO-3G basis set (Extended Huckel)
!     GUESS_from_basis='STO-3G            '    ! Project from STO-3G basis set
      GUESS_from_basis='NONE            '    ! Project from STO-3G basis set
      GUESS_previous=.false.        ! Previous MOs or Fock matrix available
      GUESS_Lscale=.true.           ! Use defaults if .true., use menu specified values if .false.
!     CORE_scale=0.875              ! Scale the core orbitals by this factor
!     VAL_scale=0.875               ! Scale the valence orbitals by this factor
      CORE_scale=0.400D0            ! Scale the core orbitals by this factor
      VAL_scale=1.000D0             ! Scale the valence orbitals by this factor
      Lpurifyp=.false.
!
      return
      end subroutine SET_defaults_QM
      subroutine SET_STO3G_acc
!*****************************************************************************************************************
!     Date last modified: March 31, 2000                                                            Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Set SCF defaults.                                                                             *
!*****************************************************************************************************************
! Modules:
      USE QM_defaults
!
! Begin:
      SCFACC=3.0D-04
      ORBACC=5.0D-5
!
      I2E_ACCF=1.0D-07
      I2E_ACC=I2E_ACCF
      I2E_EXPCUT=20.0D0
      I2E_PQCUT1=1.0D-06
      I2E_PQCUT2=1.0D-16
      ERROR1=1.0D-06
      ERROR2=1.0D-08
      VAR1=15.0D0
      VAR2=5.0D0
!
! NOTE: Too large a value of I2EBUF can cause allocation errors
      I2EBUF=487635 ! up to 60 basis functions (N*(N-1)*(N-2)*(N-3)/24)
!
      return
      end subroutine SET_STO3G_acc
