      module pop_analysis
!*****************************************************************************************************************
!     Date last modified: February 16, 2000                                                         Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Contains this and that                                                                        *
!*****************************************************************************************************************
! Modules:

      implicit none
!
! Scalars
!      integer
!
! Arrays
      double precision, dimension(:,:), allocatable :: MullAOP   ! Mulliken Population AO by AO
      double precision, dimension(:,:), allocatable :: MullAOP_A ! alpha spin
      double precision, dimension(:,:), allocatable :: MullAOP_B ! beta spin

      double precision, dimension(:,:), allocatable :: ORBCH ! Orbital charge, Nbasis x Ncolumns

      double precision, dimension(:,:), allocatable :: Atom_pop ! Populations condensed to atoms, Natoms x Natoms
      double precision, dimension(:,:), allocatable :: Atom_charges ! Net atomic charges, Natoms x Ncolums

      double precision, dimension(:,:), allocatable :: Bond_order ! Natoms x Natoms
      double precision, dimension(:,:), allocatable :: Valences ! Natoms x 2
!
      end
