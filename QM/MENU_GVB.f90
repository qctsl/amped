      MODULE module_GVB
!*****************************************************************************************************************
!     Date last modified: February 16, 2000                                                         Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: GVB Objects and scalars.                                                                      *
!*****************************************************************************************************************
! Modules:
      USE MENU_gets
      USE program_constants
      USE type_molecule

      implicit none
!
      integer :: NPAIR                    ! Number of perfect pair
      integer NSbond,NPBond,NTBond,NCBond
      integer :: NPAIRin                   ! Number of perfect pair read-in
      double precision :: BondCRIT
      double precision :: PiBondCRIT
      logical :: BANANA
      logical :: Lauto_PPairs
      character*8 :: GVBPBS ! =BOND (pairs from CMO or LMO) or =PAIR (from user)
!
! Arrays:
      integer, dimension(:,:), allocatable :: GVBpairs !
      integer, dimension(:,:), allocatable :: GVBSBD !
      integer, dimension(:,:), allocatable :: GVBPBD !
      integer, dimension(:,:), allocatable :: GVBCBD !
      integer, dimension(:,:), allocatable :: GVBTBD !
      integer, dimension(:), allocatable :: OPNLST   !

      end MODULE module_GVB
      subroutine MENU_GVB
!***********************************************************************
!     Date last modified: May 15, 1995                     Version 1.0 *
!     Author: R. Poirier and Y. Wang                                   *
!     Description: Set up GVB calculation.                             *
!***********************************************************************
! Modules:
      USE program_constants
      USE program_parser
      USE MENU_gets
      USE type_molecule
      USE module_GVB
      USE QM_defaults
      USE QM_objects

      implicit none
!
! Local scalars:
      integer :: CIMXIT,CNVLEN,CIELEN,dimlist,NLIST,I,J,LENGVB,nbbyte
      logical done,lrun
!
      character*8, dimension(:), allocatable :: GVBLST ! putobj('GVB_BOND_LIST ', Nbasis, 'CHAR', 4)
!
! Begin:
      call PRG_manager ('enter', 'MENU_GVB', 'UTILITY')
!
      Wavefunction='GVB '
      Level_of_Theory='GVB'
      NSBond=0
      NPBond=0
      NTBond=0
      NCBond=0
      NPAIRin=0
      NPAIR=0
      GVBPBS='BOND'
      BANANA=.true.
      BondCRIT=0.22D0
      PiBondCRIT=0.45D0
      NCORE=1
!
      allocate (GVBLST(Nbasis), OPNLST(Nbasis), GVBpairs(Nbasis,2), &
                GVBSBD(Nbasis,2), GVBPBD(Nbasis,2), GVBTBD(Nbasis,2), GVBCBD(Nbasis,3))
!
      GVBLST(1:Nbasis)='0'
      call GVB_DEF
!
! Make sure not combinations:
      integral_combinations=.false.

! Defaults:
      done=.false.
      lrun=.false.
!
! Menu:
      do while (.not.done)
      call new_token (' GVB:')
!
      if(token(1:4).eq.'HELP')then
         write(UNIout,'(a)') &
      ' Command GVB:', &
      '   Purpose: Perform GVB-PP calculation ', &
      '   Syntax :', &
      '   ACCuracy  = <real*8> ', &
      '   BANANABONDS = <logical> default=.true.', &
      '   BTHreshold =<real*8>, i.e. ', &
      '   CIC =<real*8>, i.e. ', &
      '   FCOre =<integer>, i.e. ', &
      '   GVBExtrapolation = <string> ', &
      '   Iteration = <integer> ', &
      '   GVBRotation = <string> ', &
      '   LMO = <command>  ', &
      '   LMOGuess = <string>, i.e.:  ', &
      '   MO = <string>, i.e.: CMO or LMO ', &
      '   MXPar = <integer> ', &
      '   ORBaccuracy  = <real*8> ', &
      '   PAIR =<string list>: example: ( 1 8 ) occupied MO 1 and virtual MO 8', &
      '   PBOND =<string list>: example: ( 1 2 ) or ( C1 C2 );', &
      '          pi bond between atom 1 and 2;', &
      '   PBThreshold =<real*8>, i.e. ', &
      '   SBond =<string list>: examples: ( 1 2 ) or ( C1 C2 )' , &
      '          sigma bond between atom 1 and 2;', &
      '   SCAle = <real*8>, i.e. ', &
      '   SCF = <command>  ', &
      '   TBond =<string list>: examples: ( 1 2 ) or ( C1 C2 );', &
      '          triple bond between atom 1 and 2;', &
      '   TCbond =<string list>: examples: ( 1 2 3) or ( C1 C2 C3 );', &
      '          three center bond between atom 1 2 and 3;', &
      '   end'
!
! BANANA Bonds
      else if(token(1:2).EQ.'BA')then
        call GET_value (BANANA)

! GVB_guessMO
      else if(token(1:2).EQ.'MO')then
        call GET_value (GVB_guessMO, lengvb)

! GVBPBS
      else if(token(1:4).EQ.'LMOG')then
        call GET_value (GVBPBS, lengvb)
!
! BondCRIT
      else if(token(1:3).EQ.'BTH')then
        call GET_value (BondCRIT)

! PiBondCRIT
      else if(token(1:3).EQ.'PBT')then
        call GET_value (PiBondCRIT)

! SCALST
      else if(token(1:3).EQ.'SCA')then
        call GET_value (SCALST)
        write(UNIout,'(9x,a,E22.15)')'MENU_GVB> Scaling factor =',SCALST

! COEFF
      else if(token(1:3).EQ.'CIC')then
        call GET_value (GVB_COEFF)

! NCORE
      else if(token(1:3).EQ.'FCO')then
        call GET_value (NCORE)

! Maximum iterations
      else if(token(1:2).EQ.'IT')then
        call GET_value (SCFITN)

! MAXPAR
      else if(token(1:3).EQ.'MXP')then
        call GET_value (MAXPAR)

! ROTMET
      else if(token(1:4).EQ.'GVBR')then
        call GET_value (ROTMET, cnvlen)

! Extrapolation_met
      else if(token(1:4).EQ.'GVBE')then
        call GET_value (Extrapolation_met, cielen)
        write(UNIout,'(9x,3a)')'MENU_GVB> SCF Convergence method set to   "',Extrapolation_met(1:cielen),'"'

! SCFACC
      else if(token(1:3).EQ.'ACC')then
        call GET_value (SCFACC)
        write(UNIout,'(9x,a,d22.15)')'MENU_GVB> SCF Convergence Accuracy =',SCFACC

! ORBACC
      else if(token(1:3).EQ.'ORB')then
        call GET_value (ORBACC)
!
      else if(token(1:3).EQ.'LMO')then
!        call setlmo

      else if(token(1:3).EQ.'SCF')then
        call MENU_SCF
!
      else if(token(1:4).EQ.'PAIR') then
        NPAIRin=NPAIRin+1
        nlist=2
        nbbyte=4
        call GET_Clist (GVBLST, nbbyte, Nbasis, nlist)
        call GVBSTR (GVBpairs, GVBLST, NLIST, NPAIRin, Nbasis)
       if(GVBPBS(1:2).EQ.'BO')then
         GVBPBS='PAIR'
       end if

      else if(token(1:2).EQ.'SB') then
        NSBond=NSBond+1
        nlist=2
        nbbyte=4
        call GET_Clist (GVBLST, nbbyte, Nbasis, nlist)
        call GVBSTR (GVBSBD, GVBLST, NLIST, NSBond, Nbasis)

      else if(token(1:2).EQ.'PB') then
        NPBond=NPBond+1
        nlist=2
        nbbyte=4
        call GET_Clist (GVBLST, nbbyte, Nbasis, nlist)
        call GVBSTR (GVBPBD, GVBLST, NLIST, NPBond, Nbasis)
!
      else if(token(1:2).EQ.'TB') then
        NTBond=NTBond+1
        nlist=2
        nbbyte=4
        call GET_Clist (GVBLST, nbbyte, Nbasis, nlist)
        call GVBSTR (GVBTBD, GVBLST, NLIST, NTBond, Nbasis)

      else if(token(1:2).EQ.'TC') then
        NCBond=NCBond+1
        nlist=3
        nbbyte=4
        call GET_Clist (GVBLST, nbbyte, Nbasis, nlist)
        call GVBSTR (GVBCBD, GVBLST, NLIST, NCBond, Nbasis)
!
      else if(token(1:3).EQ.'RUN')then
        lrun=.true.

      else
        call MENU_end (done)
      end if

      end do
!
      deallocate (GVBLST)
!
! Sum number of GVB pairs defined by the user
      NPAIR=NPAIRin+NSBond+NPBond+NTBond+NCBond
!
      if(lrun)then
        call GET_object ('QM','DENSITY_1MATRIX','GVB')
      end if
!
! End of routine MENU_GVB
      call PRG_manager ('exit', 'MENU_GVB', 'UTILITY')
      return
CONTAINS ! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine GVBSTR (GVBBND,  & ! Output
                         PARLST,  & ! Input
                         NLIST,   & ! Input
                         NPAIR,   & ! Input
                         Nbasis) ! Input
!***********************************************************************
!     Date last modified: May 15, 1995                     Version 1.0 *
!     Author: R.A. Poirier, Youliang Wang                              *
!     Description: Assign default bond type to the MOs                 *
!***********************************************************************
! Modules:

      implicit none
!
! Local scalars:
      integer Nbasis,NLIST,NPAIR
      integer GVBBND(Nbasis,NLIST)
      character*4 PARLST(NLIST)
!
! Local scalars:
      integer Iatom,JMO,ATMNUM
      character*4 Catom
!
! Begin:
      call PRG_manager ('enter', 'GVBSTR', 'UTILITY')
!
      do JMO=1,NLIST
        Catom=PARLST(JMO)(1:4)
        if(CHK_string(Catom,'0123456789'))then
          call CNV_StoI (Catom, ATMNUM)
          GVBBND(NPAIR,JMO)=ATMNUM
        else
          do Iatom=1,Natoms
            IF(CARTESIAN(Iatom)%element.EQ.PARLST(JMO)) GVBBND(NPAIR,JMO)=Iatom
          end do ! (Iatom)
        end if
      end do ! JMO
!
! End of routine GVBSTR
      call PRG_manager ('exit', 'GVBSTR', 'UTILITY')
      return
      end subroutine GVBSTR
      subroutine GVB_DEF
!***********************************************************************
!     Date last modified: May 15, 1995                     Version 1.0 *
!     Author: R.A. Poirier, Youliang Wang                              *
!     Description: Assign default bond type to the MOs                 *
!***********************************************************************
! Modules:

      implicit none
!
! Local scalars:
      integer IMO,JMO
!
! Begin:
      call PRG_manager ('enter', 'GVB_DEF', 'UTILITY')
!
      do IMO=1,Nbasis
        do JMO=1,2
          GVBpairs(IMO,JMO)=0
          GVBSBD(IMO,JMO)=0
          GVBPBD(IMO,JMO)=0
          GVBTBD(IMO,JMO)=0
        end do ! JMO
        OPNLST(IMO)=0
      end do ! IMO
!
      do IMO=1,Nbasis
        do JMO=1,3
          GVBCBD(IMO,JMO)=0
        end do ! JMO
      end do ! IMO
!
! End of routine GVB_DEF
      call PRG_manager ('exit', 'GVB_DEF', 'UTILITY')
      return
      end subroutine GVB_DEF
      subroutine GVBPST (GVBpairs,  & ! Output
                         PARLST,  & ! Intput
                         NLIST,   & ! Intput
                         NPAIR,   & ! Intput
                         Nbasis) ! Intput
!***********************************************************************
!     Date last modified: May 15, 1995                     Version 1.0 *
!     Author: R. Poirier and Y. Wang                                   *
!     Description: Set up GVB pairs.                                   *
!***********************************************************************
      implicit none
!
! Input scalars:
      integer Nbasis,NPAIR,nlist
!
! Input arrays:
      integer parlst(2)
!
! Local scalars:
      integer I
!
! Local arrays:
      integer GVBpairs(Nbasis,2)
!
! Begin:
      call PRG_manager ('enter', 'GVBPST', 'UTILITY')
! Defaults:
!
      do I=1,NLIST
        GVBpairs(NPAIR,I)=PARLST(I)
      end do
!
! End of routine GVBPST
      call PRG_manager ('exit', 'GVBPST', 'UTILITY')
      return
      end subroutine GVBPST
      subroutine GVBBST (GVBBND, &
                         PARLST, &
                         NLIST, &
                         NPAIR, &
                         Nbasis)
!***********************************************************************
!     Date last modified: May 15, 1995                     Version 1.0 *
!     Author: R. Poirier and Y. Wang                                   *
!     Description: Set up GVB pair by bond.                            *
!***********************************************************************
      implicit none
!
! Input scalars:
      integer Nbasis,NPAIR,nlist
!
! Input arrays:
      integer parlst(NLIST)
!
! Local scalars:
      integer I
!
! Local arrays:
      integer GVBBND(Nbasis,NLIST)
!
! Begin:
      call PRG_manager ('enter', 'GVBBST', 'UTILITY')
! Defaults:
!
      do I=1,NLIST
        GVBBND(NPAIR,I)=PARLST(I)
      end do
!
! End of routine GVBBST
      call PRG_manager ('exit', 'GVBBST', 'UTILITY')
      return
      end subroutine GVBBST
      end subroutine MENU_GVB
