      subroutine SET_defaults_GVB
!*****************************************************************************************************************
!     Date last modified: March 31, 2000                                                            Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Set code defaults.                                                                            *
!*****************************************************************************************************************
! Modules:
      USE QM_defaults

      implicit none
!
      ORBACC=5.0D-6
      MAXPAR=0 ! for now
      SCALST=0.0D0
      GVB_COEFF=0.7071070D0
      ROTMET='RESET'
      GVB_guessMO='LMO'
!
      return
      end subroutine SET_defaults_GVB
      subroutine GVB_ENERGY
!********************************************************************************************************
!     Date last modified: February 27, 2003                                                Version 2.0  *
!     Author: R.A. Poirier                                                                              *
!     Description: This routine only serves as an interface - causes the GVB energy to be calculated.   *
!********************************************************************************************************
! Modules:
      USE QM_defaults

      implicit none
!
! Begin:
      call PRG_manager ('enter', 'GVB_ENERGY', 'ENERGY%GVB')
!
! This also creates the energy, so do nothing.
      call GET_object ('QM', 'CMO', Wavefunction)
!
! End of routine GVB_ENERGY
      call PRG_manager ('exit', 'GVB_ENERGY', 'ENERGY%GVB')
      return
      END
      module gvbcalc_work
!*****************************************************************************************************************
!     Date last modified: February 16, 2000                                                         Version 2.0  *
!     Author: R.A. Poirier                                                                                       *
!     Description: Used locally by the GVB for work arrays and scalars.                                          *
!*****************************************************************************************************************
! Modules:
      USE program_constants
      USE matrix_print
      USE QM_objects
      USE INT_objects
      USE type_basis_set
      USE ixtrapolate
      USE GSCF_work

      implicit none
!
! Scalars
      double precision FACTUP,SCRIT,SLIMUP
!
! Work arrays:
      double precision, dimension(:), allocatable, target :: PM0_GVB
      double precision, dimension(:), allocatable :: EIGVLB
      double precision, dimension(:,:), allocatable :: DCM1
      double precision, dimension(:,:), allocatable :: DCM2
      double precision, dimension(:,:), allocatable :: DCM3
      double precision, dimension(:), allocatable :: CI_coeff !  (Nbasis)
!
      end module gvbcalc_work
      subroutine GVBcalc
!*****************************************************************************************************************
!     Date last modified: February 16, 2000                                                         Version 2.0  *
!     Author: R.A. Poirier and Y. Wang                                                                           *
!     Description: Performs a GVB calculation.                                                                   *
!     Originally modified from routine GSCF (author: K. Morokuma et al.)                                         *
!*****************************************************************************************************************
! Modules:
      USE QM_defaults
      USE gvbcalc_work

      implicit none
!
! Local scalars:
      logical MATCH,TROUBL
      integer CNTCI,DWCCI,I,ICIEXT,Iend,IEXT,ISCF,J,K,IPAIR,ITR,MAXITR,MAXCYC,KEY,KEY1
      double precision ACC,CCONV,C0,CRIT,EPREV,E_ELEC,EPREV1,SCFACC1,T,ACC1,ACC2,ACC3,ACC4
      double precision DEWTOL
      double precision :: SCFCON
      character*4 INCR,IRESET
!
! Local function:
      double precision TRACLO
!
! Local parameters:
      double precision PT8,PT1,PT3,TEN,TENM5,TENM10,TENM4,TENM6
      parameter (TEN=10.0D0,TENM5=1.0D-5,TENM10=1.0D-10,TENM4=1.0D-04,PT8=0.80D00,PT1=0.10D00,PT3=0.30D00,TENM6=1.0D-6)
!
! Begin:
      call PRG_manager ('enter', 'GVBcalc', 'UTILITY')
!
      call GET_object ('QM','CMO_GUESS','GVB')
! Make sure not combinations:
      call GET_object ('INT','1EINT','AO')  ! Get integrals (for a direct SCF, this created DGM)
      call GET_object ('INT','2EINT','RAW')
      Nbasis=size(CMOG_GVB%coeff,1)
      MATlen=Nbasis*(Nbasis+1)/2

      allocate(IJNDX(Nbasis+1))
      call BLD_IJNDX (IJNDX, Nbasis)
!
! Set parameters controlling the scale factor modification.
      FACTDN=0.666666666666667D0
      SCRIT=2.0D-3
      FACTUP=1.2D0
      SLIMDN=0.00050D0
      SLIMUP=0.80D0
!
      NFOCK=NVAL+NOPEN+1
!
! NOTE: EIGVL is dimensioned to Nbasis+1 to avoid a problem with the special case when CMO_GVB%NFcoreMO=0.
      if(.not.allocated(PM0_GVB))then
        allocate (PM0_GVB(1:MATlen), LAGRGM(Nbasis,Nbasis), VECCCN(NFOCK,NFOCK+2))
      end if
      if(.not.associated(CMO_GVB%eigval))then
        allocate (CMO_GVB%coeff(Nbasis,Nbasis), CMO_GVB%eigval(Nbasis+1), CMO_GVB%occupancy(NFOCK))
      else
        deallocate (CMO_GVB%coeff, CMO_GVB%eigval, CMO_GVB%occupancy)
        allocate (CMO_GVB%coeff(Nbasis,Nbasis), CMO_GVB%eigval(Nbasis+1), CMO_GVB%occupancy(NFOCK))
      end if

      if(.not.allocated(CI_coeff))then
        allocate (CI_coeff(1:Nbasis))
        CI_coeff(1:Nbasis)=-GVB_coeff
      end if

      CMO_GVB%noccmo=CMOG_GVB%noccmo
      CMO_GVB%NFcoreMO=CMOG_GVB%NFcoreMO

      allocate (CMOB%coeff(Nbasis,Nbasis), CMOC(Nbasis,Nbasis), DCM1(Nbasis,Nbasis), DCM2(Nbasis,Nbasis), &
                DCM3(Nbasis,Nbasis), SCRMAT(Nbasis,Nbasis), SCRVEC(Nbasis), &
                SQSM(Nbasis,Nbasis), MOTB(CMO_GVB%noccmo,CMO_GVB%noccmo), SYMO(Nbasis,CMO_GVB%noccmo), &
                SCMO(CMO_GVB%noccmo,CMO_GVB%noccmo), EIGVLB(Nbasis+1))
      allocate (EJKN(NFOCK,NFOCK), PM0D(MATlen*NFock), PMN(MATLEN,NFock), &
                JcoulN(MATlen,NFock), KexchN(MATlen,NFock))
!
      if(.not.allocated(FockMN))then ! May have been allocated by RHF, ...
        allocate (FockMN(MATlen,NFock))
      else
        deallocate (FockMN)
        allocate (FockMN(MATlen,NFock))
      end if

      call UPT_to_SQS (OVRLAP, MATlen, SQSM, Nbasis)

      call NULL_GVBCLC
!
      if(MAXPAR.EQ.0) then
        MAXPAR=10
        if(Nbasis.LT.100) MAXPAR=Nbasis/20+5
        if(ROTMET(1:2).EQ.'IN'.or.ROTMET(1:2).EQ.'NO'.or.GVB_guessMO(1:2).EQ.'RE') MAXPAR=1
        if(ROTMET(1:2).EQ.'R2')then
          MAXPAR=10
          ROTMET(1:2)='RE'
          if(Nbasis.LT.100) MAXPAR=Nbasis/20+15
        end if
      end if

      if(SCALST.EQ.0.0D00)then
        SCALST=1.0D00
        if(Nbasis.GT.100) SCALST=PT5+0.1D00*(Nbasis/50)
        if(ROTMET(1:2).EQ.'RE') then
          SCALST=0.8D00
          if(Nbasis.LT.100) SCALST=PT5+0.1D00*(Nbasis/30)
        end if
      end if
! Regular open shell RHF - Allow only one CI cycle.
      if(NOPEN.EQ.0.and.ROTMIX)ROTMIX=.FALSE.
! Need more proved!
      if(NOPEN.NE.0)ROTMIX=.TRUE.
      if(MUN_PRTLEV.GT.0)write(UNIout,1100)ENERGY_GVB%nuclear
      if(MUN_PRTLEV.GT.0)write(UNIout,1070)SCFACC,SCALST,FACTDN,FACTUP,SLIMDN,SLIMUP,SCRIT
      if(MUN_PRTLEV.GT.0)write(UNIout,1190)
!
! Initialize for first GVB cycle.
      MAXCYC=10
      call RHODENN (CMO_GVB%coeff, PMN, Nbasis, MATlen, NFOCK, CMO_GVB%NFcoreMO, NFOCK)
      call BLD_JKRN
      call GVBCIC (E_ELEC, MAXCYC)
      ENERGY_GVB%elec=E_ELEC
      ENERGY_GVB%HF=ENERGY_GVB%nuclear+ENERGY_GVB%elec
      ENERGY_GVB%total=ENERGY_GVB%HF
!
      do I=1,CMO_GVB%noccmo
        SCRVEC(I)=ONE+ONE
        if(I.GT.CMO_GVB%NFcoreMO) then
          SCRVEC(I)=CMO_GVB%occupancy(I-CMO_GVB%NFcoreMO+1)+CMO_GVB%occupancy(I-CMO_GVB%NFcoreMO+1)
        end if
      end do

      call DENBLD (CMO_GVB%coeff, SCRVEC, PM0_GVB, Nbasis, MATlen, CMO_GVB%noccmo)
!
      ISCF=0
      IXpolation_count=0
      IDEWIT=0
      CNTCI=0
      DWCCI=0
      CCONV=TENM10
      EPREV1=TENM10
      SCFACC1=SCFACC/TEN
      ACC=TEN
      ACC3=TEN
      Iend=1
      TROUBL=.FALSE.
      IRESET='NO  '
      if(ROTMET(1:2).EQ.'RE')IRESET='YES '
      if(ROTMET(1:2).EQ.'OM')IRESET='OMIT'
      IEXT=1
      CCscale=SCALST
      ICUT=0
      ITR=0
      MAXITR=30
      MAXCYC=30
      KEY=0
! Print energy with initial guess coefficients.
      if(MUN_PRTLEV.GT.0) write(UNIout,1091) ISCF,ENERGY_GVB%elec,ENERGY_GVB%total
!
! *************************** SCF Cycles *******************************
      do while(ISCF.LE.SCFITN.and.KEY.NE.-1)
      ISCF=ISCF+1
! Switch from Noreset to Increase mode if density convergence is
! small enough (and ROTMET='INCREMENT').
!
      if(ROTMET(1:2).NE.'IN'.or.ACC.GT.SCRIT.or.IEXT.NE.0)then
!
! Reset scale factor and ICUT counter in Reset/Omit mode.
        if(ROTMET(1:2).EQ.'OM'.or.ROTMET(1:2).EQ.'RE')then
          CCscale=SCALST
          ICUT=0
        end if
      else
        IRESET='INCR'
        CCscale=FACTUP*CCscale
        if(CCscale.GE.SLIMUP)then
          CCscale=SLIMUP
          ICUT=0
        else if(CCscale.LT.PT5)then
          CCscale=PT5
          ICUT=0
        end if
      end if
!
      call MCFockM
!
      JCUT=0
      IPAIR=0
      KEY1=0
! Get coefficient corrections.
      do while(IPAIR.LE.MAXPAR.and.KEY1.EQ.0)
        call PAIRSN (Iend, SCFCON)
! IF Iend =0, SCF has converged, check it.
        if(Iend.GT.0.or.ISCF.LE.2.or.ACC.GT.SCFACC)then
! Get C(NEW)=C(OLD)+CCscale*CC, where CC is the coefficient correction matrix.
          call ROTSCFN (E_ELEC, EPREV, TROUBL, ENERGY_GVB%elec)
          CMO_GVB%coeff(1:Nbasis,1:Nbasis)=SCRMAT(1:Nbasis,1:Nbasis)
          call phase (CMO_GVB%coeff, Nbasis)
        end if
        if(TROUBL.or.ACC.LT.TENM10) then
          ITR=ITR+1
          KEY1=1
        else
          call CONGVB (CMO_GVB%coeff, CMOB%coeff, OVRLAP, IEXT, CCONV, MATCH, CMO_GVB%noccmo, DEWTOL)
          KEY1=1
          if(ROTMET(1:2).EQ.'RE'.and.GVB_guessMO(1:3).EQ.'LMO') then
            if(CCONV.GT.TENM6.or.ISCF.LE.2) KEY1=0
          else if(CCONV.GT.TENM6) then
            KEY1=0
          end if
        end if
        IPAIR=1+IPAIR
      end do
! optimize gvb-ci coefficents
      call RHODENN (CMO_GVB%coeff, PMN, Nbasis, MATlen, NFOCK, CMO_GVB%NFcoreMO, NFOCK)
      call BLD_JKRN
      if(ACC3.LT.SCRIT.and.ISCF.GT.1.or.(ITR.GT.1.and.ACC3.GE.TEN)) then
        call GVBCIC (E_ELEC, MAXCYC)
        ENERGY_GVB%elec=E_ELEC
        ENERGY_GVB%HF=ENERGY_GVB%nuclear+ENERGY_GVB%elec
        ENERGY_GVB%total=ENERGY_GVB%HF
      end if
!
! Recalculate density for new GVB-CI coefficients.
      PM0D(1:MATlen)=PM0_GVB(1:MATlen)
      do I=1,CMO_GVB%noccmo
        SCRVEC(I)=ONE+ONE
         if(I.GT.CMO_GVB%NFcoreMO) then
          SCRVEC(I)=CMO_GVB%occupancy(I-CMO_GVB%NFcoreMO+1)+CMO_GVB%occupancy(I-CMO_GVB%NFcoreMO+1)
        end if
      end do
      call DENBLD (CMO_GVB%coeff, SCRVEC, PM0_GVB, Nbasis, MATlen, CMO_GVB%noccmo)
      do I=1,MATlen
        PM0D(I)=PM0_GVB(I)-PM0D(I)
      end do
      ACC=DSQRT(TRACLO (PM0D, PM0D, Nbasis, MATlen))/DBLE(Nbasis)
      ENERGY_GVB%HF=ENERGY_GVB%nuclear+ENERGY_GVB%elec
      ENERGY_GVB%total=ENERGY_GVB%HF
      ACC4=DABS(ENERGY_GVB%total-EPREV1)
      EPREV1=ENERGY_GVB%total
!
      if(IEXT.LE.0)then
        if(MUN_PRTLEV.GT.0.) &
          write(UNIout,1090)ISCF,ENERGY_GVB%elec,ENERGY_GVB%total,CCONV,ACC,LABEL,CCscale,IRESET
      else
        if(MUN_PRTLEV.GT.0) &
          write(UNIout,1091)ISCF,ENERGY_GVB%elec,ENERGY_GVB%total,ACC,LABEL,CCscale,IRESET
      end if

! If Iend=0, SCF has converged normally.
      if(Iend.LE.0.and.ISCF.GT.1.and..not.TROUBL) then
! SCF coefficients have converged . ACC < SCFACC
! IF Iend<0, and with energy and coefficients changed small enough
! still considering as converged
        if(ROTMET(1:2).NE.'NO') then
          if(ACC4.LT.SCFACC.or.ACC.LT.SCFACC) KEY=-1
        else
          ROTMET(1:2)='RE'
        end if
      else if(ROTMET(1:2).NE.'NO') then
        if(ACC4.LT.SCFACC1.and.CCscale.GT.PT1) KEY=-1
      end if
!
      if(TROUBL) then
        if(ITR.LE.MAXITR)then
          TROUBL=.FALSE.
! Scale factor cut too often - Switch from Noreset to Reset mode
! if this is possible, else abort.
          if(ROTMET(1:2).EQ.'IN'.or.ROTMET(1:2).EQ.'NO')then
            ROTMET(1:2)='RE'
            IRESET='YES '
            SCALST=0.60D00
          else
            ROTMET(1:4)='IN'
            IRESET='INCR'
            SCALST=0.80D00
          end if
          if(ROTMIX)ROTMIX=.FALSE.
        else
          TROUBL=.TRUE.
          KEY=-1
        end if
      else if(KEY.NE.-1) then
! reset the scaling method
        ACC1=CCONV*1.0D-01
        ACC2=CCONV*1.0D02
        ACC3=CCONV*1.0D-02
        if(ISCF.GT.2)then
! switch extrapolation method to Dewar's
!          if(ACC4.LT.TENM5.and.ACC.LT.TENM4) Extrapolation_met(1:5)='DEWAR'
          if(ACC2.LT.SCRIT)then
            ROTMET(1:2)='RE'
            IRESET='YES'
            SCALST=PT5
            if(ACC.GT.TENM4) SCALST=PT3
            if(ROTMIX)ROTMIX=.FALSE.
          else
            if(ACC4.LT.TENM4)then
              if(ROTMET(1:2).EQ.'RE')then
                SCALST=PT3
              else
                ROTMET(1:2)='RE'
                IRESET='YES'
                SCALST=0.60D00
                if(ACC4.LT.TENM5)SCALST=PT8
              end if
              if(ROTMIX)ROTMIX=.FALSE.
            else if(ACC1.GT.SCRIT)then
              ROTMET(1:2)='IN'
              IRESET='INCR'
            end if
          end if
        end if
      end if
!
      end do ! SCF cycle
!
! GSCF is terminated.
!
  600 if(.not.MATCH)then
        write(UNIout,1180)
      end if
      if(TROUBL)then
        write(UNIout,1210)
        stop
      end if
!      if(MUN_PRTLEV.GT.0)write(UNIout,1090)
!     .ISCF,ENERGY_GVB%elec,ENERGY_GVB%total,CCONV,ACC,LABEL,CCscale,IRESET
      if(MUN_PRTLEV.GT.0)write(UNIout,1040)ENERGY_GVB%total,SCFCON
!
! Put coefficient of reference configuration in C0.
! For GVB, sum the squares of the coefficients and subtract from 1.0.
      C0=ONE
      do I=1,NCONF
        C0=C0-CI_COEFF(I)*CI_COEFF(I)
      end do
!
      call MCFockM
      call BLD_Lagrange_matrix
!
      if(MUN_PRTLEV.GT.0) then
!        write(UNIout,*)' GVBcalc> MO coefficients:'
!        call GBSOUT (CMO_GVB%coeff, CMO_GVB%eigval, Nbasis, Nbasis, Nbasis, 1)
!        call MO_COEFF_OUT (CMO_GVB%coeff, CMO_GVB%eigval, Nbasis, Nbasis, Nbasis)
!        call PRT_matrix (CMO_GVB%coeff, Nbasis, Nbasis)

        write(UNIout,1270)
        if(CMO_GVB%NFcoreMO.NE.0)write(UNIout,1280)CMO_GVB%NFcoreMO
        write(UNIout,1330)CMO_GVB%NFcoreMO+1, CMO_GVB%NFcoreMO+NCONF*2
        write(UNIout,'(a)') 'GVB PAIRS COEFFICIENTS:'
        do K=1,NCONF
          I=ICONF(K)/1000
          J=ICONF(K)-1000*I
          write(UNIout,1320) J,I,CI_COEFF(K)
        end do
      end if
!
! Too many iterations - Check if run can be allowed to continue.
      if(ISCF.GT.SCFITN)then
        write(UNIout,'(A)')'ERROR> GVBCLC: MAXIMUM NUMBER OF SCF CYCLES EXCEEDED'
        stop 'ERROR> GVBCLC: MAXIMUM NUMBER OF SCF CYCLES EXCEEDED'
      end if
!
! Form the total density matrices in PM0_GVB
      do I=1,CMO_GVB%noccmo
        SCRVEC(I)=ONE+ONE
        if(I.GT.CMO_GVB%NFcoreMO) then
          SCRVEC(I)=CMO_GVB%occupancy(I-CMO_GVB%NFcoreMO+1)+CMO_GVB%occupancy(I-CMO_GVB%NFcoreMO+1)
        end if
      end do
      call DENBLD (CMO_GVB%coeff, SCRVEC, PM0_GVB, Nbasis, MATlen, CMO_GVB%noccmo)
!
      deallocate(IJNDX)
      deallocate (CMOB%coeff, CMOC, DCM1, DCM2, DCM3, EJKN, PM0D, SCRMAT, SCRVEC, SQSM, MOTB, SYMO, SCMO, EIGVLB, &
                  PMN, JcoulN, KexchN, FockMN)
!
! Save the coefficient matrix for possible use as an initial guess in a subsequent calculation
      CMOG_GVB%coeff(1:BASIS%Nbasis,1:BASIS%Nbasis)=CMO_GVB%coeff(1:BASIS%Nbasis,1:BASIS%Nbasis)
!
! End of routine GVBcalc
      call PRG_manager ('exit', 'GVBcalc', 'UTILITY')
      return
!
176   format('***C  51',2I8,'   1   1')
177   format(6f12.8)
178   format(6f12.6)
 1040 FORMAT('0AT TERMINATION TOTAL ENERGY IS',F16.6,'  HARTREES',/1X,'ORBITAL CONVERGENCE IS',1PE12.5)
 1070 FORMAT('0CONVERGENCE ON DENSITY MATRIX REQUIRED TO EXIT IS',1PE12.4,',  INITIAL SCALE FACTOR (SCALST) IS',0PF8.5/ &
       ' SCALE FACTOR CUT RATIO (FACTDN) IS',F8.5,',  SCALE FACTOR INCREASE RATIO (FACTUP) IS',F8.5/ &
       ' SCALE FACTOR LOWER LIMIT (SLIMDN) IS',F8.5,',  SCALE FACTOR UPPER LIMIT (SLIMUP) IS',F8.5/ &
       ' SWITCH TO INCREASE MODE AT D CONVERGENCE',1PE12.4,' (SCRIT, RHF=INCR MODE ONLY).')
 1080 FORMAT('1VECTOR COUPLING COEFFICIENTS  (NFOCK X NFOCK+2)'//)
 1090 FORMAT('GSCF:  ',I4,F19.9,F19.9,2X,2(1PE11.4),10X,A8,4X,E11.4,4X,A4)
 1091 FORMAT('GSCF:  ',I4,F19.9,F19.9,13X,1PE11.4,10X,A8,4X,E11.4,4X,A4)
 1100 FORMAT('0NUCLEAR REPULSION ENERGY IS',F17.9,'  HARTREES')
 1110 FORMAT(' E =',F20.9,10X,'DELTA E =',F15.9,10X,'SCF D CONV =',1PE13.5,10X,'CI D CONV =',E13.5/' RUN ABORTED')
 1120 FORMAT(' E =',F20.9,10X,'DELTA E =',F15.9,10X,'SCF D CONV =',1PE13.5,10X,'CI D CONV =',E13.5/' RUN ALLOWED TO continue')
 1140 FORMAT('TOTAL DENSITY MATRIX')
 1150 FORMAT('LAGrange MULTIPLIERS MATRIX'//)
 1160 FORMAT(1X,I4,F19.9,F19.9,26X,2(1PE11.4),8X,A8,4X,E11.4,4X,A4)
 1161 FORMAT(1X,I4,F19.9,F19.9,37X,1PE11.4,8X,A8,4X,E11.4,4X,A4)
 1170 FORMAT(' Iend=',I5,'   ICUT=',I3,'   JCUT=',I3,'  SCFCON=',1PE12.4,'   TROUBL=',L2)
 1180 FORMAT('*** MOLECULAR ORBITAL MATCHING FAILURE IN GSCF')
 1190 FORMAT('          ',19X,'ENERGY',20X,'SCF CONVERGENCE',9X,'EXTRAPOLATION',5X,'SCALE',6X,'RESET'/ &
       '  CYCLE   ',7X,'ELECTRONIC',11X,'TOTAL',10X,'COEFF',6X,'DENSITY'/)
 1220 FORMAT('*** SCF CONVERGED IN NORESET/INCREASE MODE',/' *** REstartING IN RESET MODE FOR CONVERGENCE CHECK'/)
 1210 FORMAT('*** TOO MANY SCALE FACTOR CUTS or',' *** DENSITY HAVE CONVERGED TO 1.0E-10'/)
 1270 FORMAT(1X)
 1280 FORMAT(' THE FIRST',I4,' MO ARE DOUBLY OCCUPIED FROZEN CORE ORBITALS')
 1330 FORMAT('MO',I4,'  TO',I4,' ARE USED FOR GVB PAIRS:'/)
 1320 FORMAT(1X,I3,'-',I3,F11.6/)
 1325 FORMAT(2I5)
!
      contains
      subroutine NULL_GVBCLC
!***********************************************************************
!     Date last modified: October 11, 2001                             *
!     Author: R.A. Poirier                                             *
!     Description: Initialize all RHF parameters.                      *
!***********************************************************************
! Modules:

      implicit none
!
! Local function:
      double precision GET_Enuclear
!
! Begin:
      call PRG_manager ('enter', 'NULL_GVBCLC', 'UTILITY')
!
! Intialize the energies
      ENERGY=>ENERGY_GVB
      ENERGY%nuclear=GET_Enuclear()
      ENERGY%HF=ZERO
      ENERGY%elec=ZERO
      ENERGY%total=ZERO
      ENERGY%wavefunction='GVB'//Basis_name(1:len_trim(Basis_name))

      PM0=>PM0_GVB
      CMO=>CMO_GVB
!
! Input:
      CMO_GVB%coeff=CMOG_GVB%coeff
!
      PM0_GVB(1:MATlen)=ZERO
      LAGRGM(1:Nbasis,1:Nbasis)=ZERO
      JcoulN(MATlen,NFock)=ZERO
      KexchN(MATlen,NFock)=ZERO
      CMO_GVB%eigval(1:Nbasis+1)=ZERO
!
      call phase (CMO_GVB%coeff, Nbasis)
      CMOB%coeff(1:Nbasis,1:Nbasis)=CMO_GVB%coeff(1:Nbasis,1:Nbasis)

      PM0D=ZERO
      PMN=ZERO
      DCM1=ZERO
      DCM2=ZERO
      DCM3=ZERO
!
      MATCH=.TRUE.
      LABEL='        '
!
! End of routine NULL_GVBCLC
      call PRG_manager ('exit', 'NULL_GVBCLC', 'UTILITY')
      return
      end subroutine NULL_GVBCLC
      end subroutine GVBcalc
!
      subroutine GVBCIC (E_ELEC, MAXCYC)
!***********************************************************************
!     Date last modified: OCT. 26, 1994                    Version 1.0 *
!     Author:                                                          *
!     Description:                                                     *
!     Perform CI calculation for GVB.                                  *
!     On entry, PM0D contains the density matrix, JcoulN contains the   *
!     coulomb matrix, and KexchN contains the exchange matrix.          *
!***********************************************************************
! Modules:
      USE gvbcalc_work

      implicit none
!
! Input scalars:
      integer MAXCYC
      double precision E_ELEC
!
! Local scalars:
      integer I,IJ,IROOT,I1,J,JJ,K,KK,KL,K1,NCLOSE,NOCC
      double precision EX11,FIJ
!
! Begin:
      call PRG_manager ('enter', 'GVBCIC', 'UTILITY')
!
      call EJKBLDN
!
      if(CMO_GVB%NFcoreMO.NE.0)then
        EX11=ZERO
        JJ=0
        do I=1,Nbasis
          FIJ=TWO
          do J=1,I
            JJ=JJ+1
            if(I.EQ.J)FIJ=ONE
            IJ=(JJ-1)*NFOCK+1
            EX11=EX11+FIJ*PMN(JJ,1)*KexchN(JJ,1)
          end do !J
        end do !I
      end if
!
      call GVBCI (E_ELEC, EX11, MAXCYC)
!
! End of routine GVBCIC
      call PRG_manager ('exit', 'GVBCIC', 'UTILITY')
      return
      end subroutine GVBCIC
      subroutine GVBCI (E_ELEC, EX11, &
                        MAXCYC)
!***********************************************************************
!     Date last modified: OCT. 26, 1994                    Version 1.0 *
!     Author:                                                          *
!     Description:                                                     *
!***********************************************************************
! Modules:
      USE gvbcalc_work

      implicit none
!
! Input scalars:
      integer MAXCYC
      double precision E_ELEC,EX11
!
! Local scalars:
      integer I,ICORE,ICYCLE,II,IJ,IL,IU,IX,J,JJ,JL,JU,K,KK,NCLOSE,KEY
      double precision CC,EDIF,FIJ,SUM,SUME1,SUME2,SUME3,TANX,EDIF0
!
! Local parameters:
      double precision THRES
      parameter (THRES=1.0D-8)
!
! Begin:
      call PRG_manager ('enter', 'GVBCI', 'UTILITY')
!
! Successive 2x2 CI procedure.
!
      EDIF0=100.0D00
      ICORE=1-CMO_GVB%NFcoreMO
      CMO_GVB%eigval(1:NFOCK)=ZERO
! H-Core Part.
      JJ=0
      do I=1,Nbasis
         FIJ=TWO
         do J=1,I
            JJ=JJ+1
            IJ=(JJ-1)*NFOCK
            if(J.EQ.I)FIJ=ONE
            do K=1,NFOCK
               IL=IJ+K
               CMO_GVB%eigval(K)=CMO_GVB%eigval(K)+FIJ*PMN(JJ,K)*HCORE(JJ)
            end do !K
         end do !J
      end do !I
      ICYCLE=0
      KEY=0
!
      do while(ICYCLE.LT.MAXCYC.and.KEY.EQ.0)
      KEY=1
      ICYCLE=ICYCLE+1
      do I=1,NCONF
         II=ICONF(I)/1000
         IL=ICONF(I)-1000*II+ICORE
         IU=II+ICORE
! One-Electron Part.
         SUME1=CMO_GVB%eigval(IL)
         SUME2=ZERO
         SUME3=CMO_GVB%eigval(IU)
! Two-Electron Part.
         if(CMO_GVB%NFcoreMO.NE.0)then
            SUME1=SUME1+(TWO*EJKN(IL,1)-EJKN(1,IL))
            SUME3=SUME3+(TWO*EJKN(IU,1)-EJKN(1,IU))
         end if
         do J=1,NCONF
            if(J.NE.I)then
               JJ=ICONF(J)/1000
               JL=ICONF(J)-1000*JJ+ICORE
               JU=JJ+ICORE
               CC=CI_coeff(J)*CI_coeff(J)
               SUME1=SUME1+CC*(TWO*EJKN(JU,IL)-EJKN(IL,JU))
               if(IL.LE.JL)then
                  SUME1=SUME1+(ONE-CC)*(TWO*EJKN(JL,IL)-EJKN(IL,JL))
               else
                  SUME1=SUME1+(ONE-CC)*(TWO*EJKN(IL,JL)-EJKN(JL,IL))
               end if
               SUME3=SUME3+(ONE-CC)*(TWO*EJKN(IU,JL)-EJKN(JL,IU))
               if(IU.LE.JU)then
                  SUME3=SUME3+CC*(TWO*EJKN(JU,IU)-EJKN(IU,JU))
               else
                  SUME3=SUME3+CC*(TWO*EJKN(IU,JU)-EJKN(JU,IU))
               end if
            else
               SUME1=SUME1+PT5*EJKN(IL,IL)
               SUME3=SUME3+PT5*EJKN(IU,IU)
               SUME2=SUME2+PT5*EJKN(IL,IU)
            end if
            continue
         end do !J
         if(NOPEN.NE.0)then
            do J=1,NOPEN
               NCLOSE=NVAL+1
               IJ=NCLOSE+J
               JJ=J*(J+1)/2+1
               SUME1=SUME1+(ALPHCC(JJ)*EJKN(IJ,IL)-BETACC(JJ)*EJKN(IL,IJ))
               SUME3=SUME3+(ALPHCC(JJ)*EJKN(IJ,IU)-BETACC(JJ)*EJKN(IU,IJ))
            end do !J
         end if
  650    SUME1=SUME1+SUME1
         SUME2=SUME2+SUME2
         SUME3=SUME3+SUME3
         EDIF=SUME3-SUME1
         TANX=PT5*(EDIF-DSQRT(EDIF*EDIF+FOUR*SUME2*SUME2))/SUME2
         TANX=DATAN(TANX)
         EIGVLB(I)=-DABS(DSIN(TANX))
      end do !I
! Check of convergence of CI coefficients.
      EDIF=ZERO
      do I=1,NCONF
         EDIF=EDIF+(CI_coeff(I)-EIGVLB(I))**2
         CI_coeff(I)=EIGVLB(I)
      end do
      EDIF=DSQRT(EDIF)
      if(EDIF.GT.THRES) then
        if(EDIF.LT.EDIF0) then
           EDIF0=EDIF
           if(ICYCLE.LT.MAXCYC)KEY=0
        end if
      end if
      end do ! CYCYLE

      call GVBVCC

      E_ELEC=ZERO
      JJ=0
      do I=1,Nbasis
      FIJ=TWO
      do J=1,I
      JJ=JJ+1
      if(I.EQ.J)FIJ=ONE
      IJ=(JJ-1)*NFOCK
      SUM=ZERO
      do K=1,NFOCK
      KK=IJ+K
      SUM=SUM+CMO_GVB%occupancy(K)*PMN(JJ,K)
      end do
      E_ELEC=E_ELEC+FIJ*SUM*HCORE(JJ)
      end do !J
      end do !I
      E_ELEC=E_ELEC+E_ELEC
      do I=1,NFOCK
      E_ELEC=E_ELEC+VECCCN(I,I)*EJKN(I,I)
      end do !I
      if(CMO_GVB%NFcoreMO.NE.0)E_ELEC=E_ELEC-EX11+EJKN(1,1)
      SUM=ZERO
      do I=2,NFOCK
      IX=I-1
      do J=1,IX
      SUM=SUM+VECCCN(I,J)*EJKN(I,J)+VECCCN(J,I)*EJKN(J,I)
      end do !J
      end do !I
      E_ELEC=E_ELEC+SUM+SUM
!
! End of routine GVBCI
      call PRG_manager ('exit', 'GVBCI', 'UTILITY')
      return
      end subroutine GVBCI
      subroutine GVBVCC
!***********************************************************************
!     Date last modified: MARCH 16, 1994                   Version 1.0 *
!     Author:                                                          *
!     Description:                                                     *
!     Calculate GVB vector coupling program_constants.  This routine was       *
!     modified  by Mike Peterson to permit gradient calculation.       *
!                                                                      *
!     The coupling program_constants are collected in VECCCN(I,J), where the    *
!     lower triangle stores A(I,J), the upper triangle stores B(I,J)   *
!     and the diagonal elements are A(I,I)+B(I,I).  However, the energy*
!     gradient (force) requires A(I,I) and B(I,I) separately, so       *
!     these are kept in columns NFOCK+1 and NFOCK+2 respectively.      *
!                                                                      *
!     Useful reference:  F.W. Bobrowicz and W.A. Goddard III,          *
!     Modern Theoretical Chemistry, Vol. 3, H.F. Schaefer III (ed.),   *
!     1977, P 79.                                                      *
!                                                                      *
!***********************************************************************
! Modules:
      USE gvbcalc_work

      implicit none
!
! Local scalars:
      integer I,ICORE,II,IL,IU,IX,J,JJ,J1,J2,NFOCK1,NFOCK2
      double precision CC
!
! Begin:
      call PRG_manager ('enter', 'GVBVCC', 'UTILITY')
!
      NFOCK1=NFOCK+1
      NFOCK2=NFOCK+2
      ICORE=1-CMO_GVB%NFcoreMO
      CMO_GVB%occupancy(1)=ONE

      do I=1,NCONF
        II=ICONF(I)/1000
        IL=ICONF(I)-1000*II+ICORE
        IU=II+ICORE
        CC=CI_coeff(I)*CI_coeff(I)
        CMO_GVB%occupancy(IL)=ONE-CC
        CMO_GVB%occupancy(IU)=CC
      end do !I

      JJ=NFOCK-NOPEN
      if(NOPEN.NE.0)then
        do I=1,NOPEN
          II=JJ+I
          CMO_GVB%occupancy(II)=OCC(I)
        end do
      end if

      do I=2,JJ
        IX=I-1
        do J=1,IX
           VECCCN(I,J)=TWO*CMO_GVB%occupancy(I)*CMO_GVB%occupancy(J)
           VECCCN(J,I)=-CMO_GVB%occupancy(I)*CMO_GVB%occupancy(J)
        end do !J
      end do !I
      do I=1,JJ
        VECCCN(I,I)=CMO_GVB%occupancy(I)*CMO_GVB%occupancy(I)
        VECCCN(I,NFOCK1)=TWO*CMO_GVB%occupancy(I)*CMO_GVB%occupancy(I)
        VECCCN(I,NFOCK2)=-CMO_GVB%occupancy(I)*CMO_GVB%occupancy(I)
      end do
      do I=1,NCONF
        II=ICONF(I)/1000
        IL=ICONF(I)-1000*II+ICORE
        IU=II+ICORE
        VECCCN(IL,IL)=CMO_GVB%occupancy(IL)
        VECCCN(IL,NFOCK1)=CMO_GVB%occupancy(IL)
        VECCCN(IL,NFOCK2)=ZERO
        VECCCN(IU,IU)=CMO_GVB%occupancy(IU)
        VECCCN(IU,NFOCK1)=CMO_GVB%occupancy(IU)
        VECCCN(IU,NFOCK2)=ZERO
        VECCCN(IU,IL)=ZERO
        VECCCN(IL,IU)=CI_coeff(I)*DSQRT(ONE-CI_coeff(I)*CI_coeff(I))
      end do !I
      if(NOPEN.NE.0)then
         do I=1,NOPEN
           II=JJ+I
           J1=IJNDX(I+1)+1
           do J=1,JJ
             VECCCN(II,J)=ALPHCC(J1)*CMO_GVB%occupancy(J)
             VECCCN(J,II)=-BETACC(J1)*CMO_GVB%occupancy(J)
           end do !J
         end do !I
         do I=1,NOPEN
           J1=JJ+I
           II=(I+1)*(I+2)/2
           VECCCN(J1,J1)=ALPHCC(II)-BETACC(II)
           VECCCN(J1,NFOCK1)=ALPHCC(II)
           VECCCN(J1,NFOCK2)=-BETACC(II)
         end do !I
         if(NOPEN.NE.1)then
            do I=2,NOPEN
              J1=JJ+I
              IX=I-1
              do J=1,IX
                J2=JJ+J
                II=IJNDX(I+1)+J+1
                VECCCN(J1,J2)=ALPHCC(II)
                VECCCN(J2,J1)=-BETACC(II)
              end do !J
           end do !I
         end if
      end if
      continue
!
! End of routine GVBVCC
      call PRG_manager ('exit', 'GVBVCC', 'UTILITY')
      return
      end subroutine GVBVCC
      subroutine CONGVB (CMO_A, &
                         CMO_B, &
                         SM, &
                         IEXT, &
                         CCONV, &
                         MATCH, &
                         NoccGVBMO, &
                         DEWTOL)
!***********************************************************************
!     Date last modified: OCT. 26, 1994                    Version 1.0 *
!     Author:                                                          *
!     Description:  MUN Version                                        *
!     RHF SCF/CI convergence routines:                                 *
!     RHF CMO_A, CMO_B, DCM1, DCM2, DCM3 refer to SCF.                 *
!     On return, IEXT is -1 if C has just been extrapolated, 0 for no  *
!     extrapolation, 1 for the first iteration after extrapolation.    *
!     On return, MATCH is normally true, but is set false if the       *
!     MO symmetry matching failed.                                     *
!***********************************************************************
! Modules:
      USE QM_defaults
      USE gvbcalc_work

      implicit none
!
! Input scalars:
      integer NoccGVBMO
      double precision DEWTOL
!
! Input arrays:
      double precision CMO_A(Nbasis,Nbasis),CMO_B(Nbasis,Nbasis),SM(MATlen)
!
! Output scalars:
      logical MATCH
      integer IEXT
      double precision CCONV
!
! Local scalars:
      integer I,IEXTRP,J
      double precision COSPHI,COSPSI,DP1,DP2,DP3,SP11,SP12,SP13,SP22,SP23,SP33,X,XXX,XY,Y,YYY,Y2
!
! Local function:
      double precision TRARHF
!
! Local parameters:
! LOWBND: Any value smaller than LOWBND is considered ZERO
      double precision ONEPT9,PT06,PT99,PT995,LOWBND
      parameter (PT06=0.06D0,PT99=0.99D0,PT995=0.995D0,ONEPT9=1.9D0,LOWBND=1.0D-30)
!
      SAVE SP12,SP22,Y
! Begin:
      Y2=10.0D0
      IEXT=1
      LABEL='        '
      MATCH=.TRUE.
      IXpolation_count=IXpolation_count+1
      if(IXpolation_count.EQ.1)then
        CMO_B(1:Nbasis,1:Nbasis)=CMO_A(1:Nbasis,1:Nbasis)
        return
      end if
      IEXT=0
!
      if(GVB_guessMO(1:3).EQ.'CMO') then
        call MATCH_MO (MATCH, CMO_A, CMO_B, SM, NoccGVBMO)
      end if
! Failure in CSD,  no extrapolation.
      if(.not.MATCH)then
        IEXT=1
        IDEWIT=0
        IXpolation_count=1
        CMO_B(1:Nbasis,1:Nbasis)=CMO_A(1:Nbasis,1:Nbasis)
        return
      end if
!
! Get difference of current and previous coefficients.
      do J=1,NoccGVBMO
        do I=1,Nbasis
          DCM3(I,J)=CMO_A(I,J)-CMO_B(I,J)
        end do ! I
      end do ! J
!
! Find length DP1.
      SP11=TRARHF (DCM3, DCM3, Nbasis, NoccGVBMO)
      DP1=DSQRT(SP11)
      CCONV=DP1/DSQRT(DBLE(Nbasis*NoccGVBMO))
!
! What type of extrapolation?
!
      if(Extrapolation_met(1:9).EQ.'3,4 POINT')then
! Pople 3,4 point extrapolation.
        if(IXpolation_count.LE.2)GO TO 385
        if(IXpolation_count.EQ.3)then
          SP12=TRARHF (DCM2, DCM3, Nbasis, NoccGVBMO)
          SP22=TRARHF (DCM2, DCM2, Nbasis, NoccGVBMO)
! Find length DP2.
          DP2=DSQRT(SP22)
          GO TO 375
        end if
        SP23=SP12
        SP33=SP22
        SP13=TRARHF (DCM1, DCM3, Nbasis, NoccGVBMO)
! Find length DP3.
        DP3=DSQRT(SP33)
! DCM1 contains  C(N-1) - C(N-2).
        SP12=TRARHF (DCM2, DCM3, Nbasis, NoccGVBMO)
        SP22=TRARHF (DCM2, DCM2, Nbasis, NoccGVBMO)
!
        if(SP11.LE.LOWBND.or.SP22.LE.LOWBND.or.SP33.LE.LOWBND)GO TO 375
!
! Find length DP2.
        DP2=DSQRT(SP22)
! Find cosine of angle between successive displacements.
        COSPHI=SP12/(DP1*DP2)
! Find cosine of angle between DP(3) and plane of DP(1) and DP(2).
        X=(SP13*SP22-SP12*SP23)/(SP11*SP22-SP12*SP12)
        Y=(SP23*SP11-SP12*SP13)/(SP11*SP22-SP12*SP12)
        COSPSI=DSQRT(X*X*SP11+Y*Y*SP22+TWO*X*Y*SP12)/DP3
!
! Do not extrapolate unless 4 consecutive points are nearly coplanar.
        if(COSPSI.LE.PT99)GO TO 375
!
! Express DP(1) as  X*DP(3)(Projected) + Y*DP(2).
        Y=-Y/X
        X=ONE/X
! Test if 2x2 matrix has real eigenvalues between -.95 and +.95.
        XY=Y*Y+FOUR*X
        if(XY.GE.ZERO)then
          XY=DABS(Y)+DSQRT(XY)
          if(XY.LE.ONEPT9)then
            XXX=X/(ONE-X-Y)
            YYY=(X+Y)/(ONE-X-Y)
            do J=1,NoccGVBMO
            do I=1,Nbasis
            CMO_A(I,J)=CMO_A(I,J)+XXX*DCM2(I,J)+YYY*DCM3(I,J)
            end do ! I
            end do ! J
!
            LABEL='4-POINT '
            IXpolation_count=0
            IEXT=-1
            call SCHMDT (CMO_A, SQSM, Nbasis)
            return
          end if
        end if
!
! If 4-point extrapolation is not possible, try 3-point.
        if(DABS(COSPHI).GT.PT995)then
          X=DP1/(DP2*COSPHI-DP1)
          do J=1,NoccGVBMO
          do I=1,Nbasis
          CMO_A(I,J)=CMO_A(I,J)+X*DCM3(I,J)
          end do ! I
          end do ! J
!
          LABEL='3-POINT '
          IXpolation_count=0
          IEXT=-1
          call SCHMDT (CMO_A, SQSM, Nbasis)
          return
        end if
!
      else if(Extrapolation_met(1:5).EQ.'DEWAR')then
!
! Dewar extrapolation: M.J.S. Dewar and P.K. Weiner,
!                      Comp. & Chem., 2, 31 (1978).
! Calculate a new value for Lambda and save previous value in Y2.
! IDEWIT controls the extrapolation procedure:
! 1 - Just extrapolated the density matrix.
! 2 - Calculate LAMBDA2.
! 3 - Calculate LAMBDA3 and extrapolate.
!
        IDEWIT=IDEWIT+1
!
        if(IDEWIT.LT.2)GO TO 385
!
        if(IDEWIT.GT.2)Y2=Y
        SP22=TRARHF (DCM2, DCM2, Nbasis, NoccGVBMO)
        SP12=TRARHF (DCM2, DCM3, Nbasis, NoccGVBMO)
        if(SP22.LE.LOWBND.or.SP11.LE.LOWBND) GO TO 385
        Y=SP12/SP22
! Check for divergence.
        if(DABS(Y).GE.ONE)then
! Special value of Lambda for divergent cases.
          Y=SP12/SP11
          LABEL=' DEWAR2 '
        else
! Extrapolate if 2 successive Lambda's agree within DEWTOL.
          if(IDEWIT.EQ.2)GO TO 385
          if(DABS(Y-Y2).GT.DEWTOL)GO TO 385
          LABEL=' DEWAR1 '
        end if
        X=Y/(ONE-Y)
        do J=1,NoccGVBMO
        do I=1,Nbasis
        CMO_A(I,J)=CMO_A(I,J)+X*DCM3(I,J)
        end do ! I
        end do ! J
!
        IXpolation_count=1
        IDEWIT=0
        IEXT=-1
        call SCHMDT (CMO_A, SQSM, Nbasis)
        return
!
      else
!
! No extrapolation.
        IXpolation_count=1
        CMO_B(1:Nbasis,1:Nbasis)=CMO_A(1:Nbasis,1:Nbasis)
        return
      end if
!
  375 DCM1(1:Nbasis,1:Nbasis)=DCM2(1:Nbasis,1:Nbasis)
  385 DCM2(1:Nbasis,1:Nbasis)=DCM3(1:Nbasis,1:Nbasis)
  400 CMO_B(1:Nbasis,1:Nbasis)=CMO_A(1:Nbasis,1:Nbasis)
!
! End of routine CONGVB
      return
      end subroutine CONGVB
